SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-10-09
-- Description:	Import Backbook of data
-- AHOD 2018-11-08 Set @ObfuscateEmails to false
-- JEL 2018-11-12 Defect 1161 Insert accounts as inactive. We will locate this account on renewal and activate
-- JEL 2018-11-12 added Sales date to tracking
-- GPR 2018-11-12 Defect 1171 - Insert into 'Claims - Paid' with the value held in 'Total' on Claim Detail
-- GPR 2018-11-13 Changed status from 43002 to 74536 WHEN 1 to resolve issue where oldest HistPolicyRow policy row was set to Live incorrectly
-- GPR 2018-11-15 Defect 1171 - Insert into 'Claims - Paid' with the value held in 'Total' and the original claim amount.
-- UAH 2018-11-28 Amended line 1193 to ensure the end date of the historical policy record is 1 day before inception the following year
-- AHOD 2018-12-04 Added New Section from Line 1539 to cater for Discount Codes 
-- GPR	2019-01-02 Added update to add the value of Claim Payment Confirmed Date for #1342
-- AHOD 2019-01-21 Added AquariumStatusID = 2 to first Customer Update Block to set customers to Accepted
-- JEL  2019-05-14 56921, Added so we do not create more than one COL lead for a customer if we receive two files with the same customer but different policies.
-- GPR	2019-07-09 #57451 Added PostCode to Customer update
-- PL   2019-09-18 #59708 - Added existing collections lead to imported customer
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_FullImport]
	@BatchID INT,
	@ObfuscateEmails BIT = 0
AS
BEGIN

	SET NOCOUNT ON;

	/*
		1. Match on imported Policy ID if we can, then work the id's back from there.
		2. If not match on Lastname, Postcode and DOB.
	*/

	DECLARE  @ImportDate			DATE = dbo.fn_GetDate_Local()
			,@ClaimCount			INT = 0
			,@Counter				INT = 1
			,@EventToApply			INT
			,@MatterID				INT
			,@ImportedClaimStatus	VARCHAR(2000)
			,@LeadEventID			INT 
			,@ClientID				INT = dbo.fnGetPrimaryClientID()
	
	CREATE TABLE #InsertedObjects (TableName VARCHAR(100), RowCounter INT, TimeStamp DATETIME)
	CREATE TABLE #ImportedCustomers (ImportID INT, CustomerID INT)
	CREATE TABLE #CreatedCustomers (CustomerID INT, LastName VARCHAR(100), PostCode VARCHAR(100), DateOfBirth DATE)
	CREATE TABLE #FieldHistory (CustomerID INT, LeadID INT, MatterID INT, DetailFieldID INT, DetailValue VARCHAR(2000), LeadORMatter INT)
	CREATE TABLE #ImportedLeads (AquariumCustomerID INT, PAleadID INT, PolicyID VARCHAR(2000))
	CREATE TABLE #ImportedPolicyCases (LeadID INT, CaseID INT)
	CREATE TABLE #ImportedPolicyMatters(CaseID INT, AquariumMatterID INT)
	CREATE TABLE #ImportedCollectionsLeads (AquariumCustomerID INT, CollectionsLeadID INT)
	CREATE TABLE #ImportedCollectionsCases (LeadID INT, CaseID INT, ImportedPetID VARCHAR(100))
	CREATE TABLE #ImportedCollectionsMatters(CaseID INT, AquariumMatterID INT)
	CREATE TABLE #ImportedAccounts (CollectionsMatterID INT, AccountID INT)
	CREATE TABLE #ImportedClaimsLeads (CustomerID INT, LeadID INT, ImportID INT)
	CREATE TABLE #ImportedClaimsCases (LeadID INT, CaseID INT, ImportedClaimID VARCHAR(100))
	CREATE TABLE #ImportedClaimsMatters (CaseID INT, MatterID INT, ImportedClaimID INT)
	CREATE TABLE #ImportedCDDTableRowIDs (MatterID INT, TableRowID INT)
	CREATE TABLE #ImportedHPTableRowIDs (MatterID INT, TableRowID INT)
	CREATE TABLE #ImportedPDHTableRowIDs (MatterID INT, TableRowID INT, ImportedPetID INT)
	CREATE TABLE #ImportedClaimRelationshipTableRowIDs (ImportedClaimID INT, TableRowID INT)
	CREATE TABLE #ImportedPP (PurchasedProductID INT, ImportedPetID INT)
	CREATE TABLE #ImportedHistoricalHPTableRowIDs (MatterID INT, TableRowID INT, PolicyYear INT)

	/*Match based on Policy ID*/
	UPDATE p
	SET CustomerID = m.CustomerID, LeadID = m.LeadID, CaseID = m.CaseID, MatterID = m.MatterID, ImportDate = @ImportDate
	FROM [dbo].[_C600_ImportPolicy] p
	INNER JOIN MatterDetailValues mdv_policyID WITH (NOLOCK) ON mdv_policyID.DetailFieldID = 180168
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = mdv_policyID.MatterID 
	INNER JOIN Customers cu WITH (NOLOCK) ON cu.CustomerID = m.CustomerID AND cu.Test = 0
	WHERE mdv_policyID.DetailValue = p.[PolicyID]
	AND p.ImportDate IS NULL
	AND p.BatchID = @BatchID
	AND p.[PolicyID] > ''

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Matched Policies', @@ROWCOUNT, CURRENT_TIMESTAMP)

	INSERT INTO #ImportedCustomers (ImportID, CustomerID)
	SELECT DISTINCT ImportID, CustomerID
	FROM [_C600_ImportPolicy] p
	WHERE p.ImportDate = @ImportDate
	AND p.BatchID = @BatchID

	UPDATE c
	SET CustomerID = i.CustomerID, ImportDate = @ImportDate
	FROM _C600_ImportCustomerData c
	INNER JOIN #ImportedCustomers i ON i.ImportID = c.ImportID
	WHERE c.CustomerID IS NULL
	AND c.BatchID = @BatchID

	PRINT 'Update c Complete'

	/*Match based on Name etc*/
	UPDATE c
	SET c.CustomerID = cu.CustomerID, c.ImportDate = @ImportDate
	FROM _C600_ImportCustomerData c
	INNER JOIN Customers cu ON cu.Test = 0 AND cu.LastName = c.LastName AND cu.PostCode = c.PostCode AND cu.DateOfBirth = c.DateOfBirth
	WHERE c.CustomerID IS NULL
	AND c.BatchID = @BatchID

	UPDATE c
	SET c.IsInFile = 1
	FROM _C600_ImportCustomerData c
	WHERE c.CustomerID IS NULL
	AND c.BatchID = @BatchID
	AND EXISTS ( 
		SELECT *
		FROM dbo._C600_ImportCustomerData cu
		WHERE cu.LastName = c.LastName 
		AND cu.PostCode = c.PostCode 
		AND cu.DateOfBirth = c.DateOfBirth
		AND cu.ImportID > c.ImportID
		AND cu.BatchID = c.BatchID
	)

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Matches on Name, Postcode etc', @@ROWCOUNT, CURRENT_TIMESTAMP)

	PRINT 'Update c2 Complete'

	UPDATE pet
	SET [CustomerID] = pol.CustomerID, LeadID = pol.LeadID, CaseID = pol.CaseID, MatterID = pol.MatterID
	FROM [dbo].[_C600_ImportPetData] pet
	INNER JOIN [dbo].[_C600_ImportPolicy] pol ON pol.ImportID = pet.ImportID
	AND pet.BatchID = @BatchID
	/*
		 2019-12-23 ACE
		 Removed this as we dont get a breed id. I suspect this was causing duplicated claims leads..
	AND pet.BreedID > ''*/

	PRINT 'Update pet Complete'

	/*Create the customers that havent been updated yet*/
	INSERT INTO Customers (ClientID, TitleID, LastName, PostCode, DateOfBirth, ChangeSource, IsBusiness)
	OUTPUT inserted.CustomerID, inserted.LastName, inserted.PostCode, inserted.DateOfBirth INTO #CreatedCustomers (CustomerID, LastName, PostCode, DateOfBirth)
	SELECT DISTINCT @ClientID, 0, c.LastName, c.PostCode, c.DateOfBirth, 'LegacyImport', 0
	FROM _C600_ImportCustomerData c
	WHERE c.CustomerID IS NULL
	AND c.BatchID = @BatchID
	AND c.isInFile = 0
	AND c.TitleID <> ''


	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Created customer records', @@ROWCOUNT, CURRENT_TIMESTAMP)

	PRINT 'Customers Inserted'

	UPDATE i
	SET CustomerID = c.CustomerID, ImportDate = @ImportDate
	FROM _C600_ImportCustomerData i
	INNER JOIN #CreatedCustomers c ON c.LastName = i.LastName COLLATE Latin1_General_CI_AS
		AND c.PostCode = i.PostCode COLLATE Latin1_General_CI_AS
		AND c.DateOfBirth = i.DateOfBirth COLLATE Latin1_General_CI_AS
	WHERE i.CustomerID IS NULL
	AND i.BatchID = @BatchID

	/* 56328 JEL & PL Added to keep customers in the same file up to date  */ 
	UPDATE i
	SET CustomerID = c.CustomerID, ImportDate = @ImportDate
	FROM _C600_ImportCustomerData i
	INNER JOIN _C600_ImportCustomerData c ON c.LastName = i.LastName COLLATE Latin1_General_CI_AS
		AND c.PostCode = i.PostCode COLLATE Latin1_General_CI_AS
		AND c.DateOfBirth = i.DateOfBirth COLLATE Latin1_General_CI_AS
	WHERE i.CustomerID IS NULL
	AND i.BatchID = @BatchID
	AND i.IsinFile = 1 
	AND c.BatchID = @BatchID
	AND i.ImportID <> c.ImportID 


	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Created customer records', @@ROWCOUNT, CURRENT_TIMESTAMP)

	PRINT 'Created customers Updated'

	UPDATE c
	SET FirstName = REPLACE(i.FirstName,CHAR(0),''), 
		MiddleName = REPLACE(i.MiddleName,CHAR(0),''), 
		Address1 = REPLACE(i.Address1,CHAR(0),''), 
		Address2 = REPLACE(i.Address2,CHAR(0),''), 
		Town = REPLACE(i.Town,CHAR(0),''), 
		County = REPLACE(i.County,CHAR(0),''), 
		PostCode = REPLACE(i.PostCode,CHAR(0),''), /*GPR 2019-07-09 #57451*/
		EmailAddress = CASE WHEN @ObfuscateEmails = 1 THEN 'mark.colonnese@aquarium-software.com' ELSE i.EmailAddress END, 
		TitleID = i.TitleID,
		DayTimeTelephoneNumber = i.DayTimeTelephoneNumber,
		HomeTelephone = i.HomeTelephone,
		MobileTelephone = i.MobileTelephone,
		CountryID = i.CountryID,
		AquariumStatusID = 2 /*AHOD 2019-01-21 Updated to set customers to Accepted*/
	FROM Customers c
	INNER JOIN _C600_ImportCustomerData i ON i.CustomerID = c.CustomerID
	WHERE 
	(ISNULL(c.FirstName, '') <>  ISNULL(i.FirstName, '') COLLATE Latin1_General_CI_AS
	OR ISNULL(c.Address1, '') <> ISNULL(i.Address1, '') COLLATE Latin1_General_CI_AS
	OR ISNULL(c.Address2, '') <> ISNULL(i.Address2 ,'') COLLATE Latin1_General_CI_AS
	OR ISNULL(c.Town,'') <> ISNULL(i.Town ,'') COLLATE Latin1_General_CI_AS
	OR ISNULL(c.County,'') <> ISNULL(i.County ,'') COLLATE Latin1_General_CI_AS
	OR ISNULL(c.EmailAddress,'') <> ISNULL(CASE WHEN @ObfuscateEmails = 1 THEN 'mark.colonnese@aquarium-software.com' ELSE i.EmailAddress END, '')
	OR c.DateOfBirth <> i.DateOfBirth  COLLATE Latin1_General_CI_AS
	OR ISNULL(c.LastName, '') <> ISNULL(i.LastName, '') COLLATE Latin1_General_CI_AS
	OR ISNULL(c.PostCode, '') <> ISNULL(i.PostCode, '') COLLATE Latin1_General_CI_AS
	OR ISNULL(c.DayTimeTelephoneNumber, '') <> ISNULL(i.DayTimeTelephoneNumber, '') 
	OR ISNULL(c.HomeTelephone, '') <> ISNULL(i.HomeTelephone, '') 
	OR ISNULL(c.MobileTelephone, '') <> ISNULL(i.MobileTelephone, '') 
	)
	AND NOT EXISTS (
		SELECT *
		FROM Lead l WITH (NOLOCK) 
		INNER JOIN LeadEvent le WITH (NOLOCK) ON le.LeadID = l.LeadID AND le.EventDeleted = 0 AND le.EventTypeID = 155196 /*Create linked leads and complete set-up*/
		WHERE l.CustomerID = c.CustomerID
	)
	AND i.CountryID <> ''
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Customers updated', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Select out the differences where there is another policy on record.*/
	SELECT
		c.FirstName, REPLACE(i.FirstName,CHAR(0),'') AS [ImportFirstName], 
		c.MiddleName, REPLACE(i.MiddleName,CHAR(0),'') AS [ImportMiddleName], 
		c.Address1, REPLACE(i.Address1,CHAR(0),'') AS [ImportAddress1], 
		c.Address2, REPLACE(i.Address2,CHAR(0),'') AS [ImportAddress2], 
		c.Town, REPLACE(i.Town,CHAR(0),'') AS [ImportTown], 
		c.County, REPLACE(i.County,CHAR(0),'') AS [ImportCounty], 
		c.EmailAddress, CASE WHEN @ObfuscateEmails = 1 THEN 'mark.colonnese@aquarium-software.com' ELSE i.EmailAddress END AS [ImportEmailAddress], 
		c.TitleID, i.TitleID AS [ImportTitleID],
		c.DayTimeTelephoneNumber, i.DayTimeTelephoneNumber AS [ImportDayTimeTelephoneNumber],
		c.HomeTelephone, i.HomeTelephone AS [ImportHomeTelephone],
		c.MobileTelephone, i.MobileTelephone AS [ImportMobileTelephone],
		c.CountryID, i.CountryID AS [ImportCountryID]
	FROM Customers c
	INNER JOIN _C600_ImportCustomerData i ON i.CustomerID = c.CustomerID
	WHERE 
	(ISNULL(c.FirstName, '') <>  ISNULL(i.FirstName, '') COLLATE Latin1_General_CI_AS
	OR ISNULL(c.Address1, '') <> ISNULL(i.Address1, '') COLLATE Latin1_General_CI_AS
	OR ISNULL(c.Address2, '') <> ISNULL(i.Address2 ,'') COLLATE Latin1_General_CI_AS
	OR ISNULL(c.Town,'') <> ISNULL(i.Town ,'') COLLATE Latin1_General_CI_AS
	OR ISNULL(c.County,'') <> ISNULL(i.County ,'') COLLATE Latin1_General_CI_AS
	OR ISNULL(c.EmailAddress,'') <> ISNULL(CASE WHEN @ObfuscateEmails = 1 THEN 'mark.colonnese@aquarium-software.com' ELSE i.EmailAddress END, '')
	OR c.DateOfBirth <> i.DateOfBirth  COLLATE Latin1_General_CI_AS
	OR ISNULL(c.LastName, '') <> ISNULL(i.LastName, '') COLLATE Latin1_General_CI_AS
	OR ISNULL(c.PostCode, '') <> ISNULL(i.PostCode, '') COLLATE Latin1_General_CI_AS
	OR ISNULL(c.CountryID, '') <> ISNULL(i.CountryID, '') COLLATE Latin1_General_CI_AS
	)
	AND EXISTS (
		SELECT *
		FROM Lead l WITH (NOLOCK) 
		INNER JOIN LeadEvent le WITH (NOLOCK) ON le.LeadID = l.LeadID AND le.EventDeleted = 0 AND le.EventTypeID = 155196 /*Create linked leads and complete set-up*/
		WHERE l.CustomerID = c.CustomerID
	)
	AND i.CountryID <> ''
	AND i.BatchID = @BatchID

	PRINT 'Customers Updated'

	UPDATE cdv
	SET DetailValue = c.[Senddocumentsby]
	OUTPUT inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue, 10 INTO #FieldHistory (CustomerID, DetailFieldID, DetailValue, LeadORMatter)
	FROM CustomerDetailValues cdv WITH (NOLOCK)
	INNER JOIN _C600_ImportCustomerData c ON cdv.CustomerID = c.CustomerID
	WHERE cdv.DetailFieldID = 170257
	AND c.CustomerID > 0
	AND cdv.DetailValue <> c.[Senddocumentsby]
	AND c.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Send docs by updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	INSERT INTO CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
	OUTPUT inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue, 10 INTO #FieldHistory (CustomerID, DetailFieldID, DetailValue, LeadORMatter)
	SELECT @ClientID, c.CustomerID, 170257, c.[Senddocumentsby]
	FROM _C600_ImportCustomerData c 
	WHERE c.CustomerID > 0
	AND c.ImportDate = @ImportDate
	AND NOT EXISTS (
		SELECT *
		FROM CustomerDetailValues cdv WITH (NOLOCK)
		WHERE cdv.CustomerID = c.CustomerID
		AND cdv.DetailFieldID = 170257
	)
	AND c.CustomerID > 0
	AND c.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Send docs by adds', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE cdv
	SET DetailValue = c.[CustomerCareEmail]
	OUTPUT inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue, 10 INTO #FieldHistory (CustomerID, DetailFieldID, DetailValue, LeadORMatter)
	FROM CustomerDetailValues cdv WITH (NOLOCK)
	INNER JOIN _C600_ImportCustomerData c ON cdv.CustomerID = c.CustomerID
	WHERE cdv.DetailFieldID = 170259
	AND c.CustomerID > 0
	AND cdv.DetailValue <> c.[CustomerCareEmail]
	AND c.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Send customer care email', @@ROWCOUNT, CURRENT_TIMESTAMP)

	INSERT INTO CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
	OUTPUT inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue, 10 INTO #FieldHistory (CustomerID, DetailFieldID, DetailValue, LeadORMatter)
	SELECT @ClientID, c.CustomerID, 170259, c.[CustomerCareEmail]
	FROM _C600_ImportCustomerData c 
	WHERE c.CustomerID > 0
	AND c.ImportDate = @ImportDate
	AND NOT EXISTS (
		SELECT *
		FROM CustomerDetailValues cdv WITH (NOLOCK)
		WHERE cdv.CustomerID = c.CustomerID
		AND cdv.DetailFieldID = 170259
	)
	AND c.CustomerID > 0
	AND c.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Send customer care email inserts', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE cdv
	SET DetailValue = c.[CustomerCareSMS]
	OUTPUT inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue, 10 INTO #FieldHistory (CustomerID, DetailFieldID, DetailValue, LeadORMatter)
	FROM CustomerDetailValues cdv WITH (NOLOCK)
	INNER JOIN _C600_ImportCustomerData c ON cdv.CustomerID = c.CustomerID
	WHERE cdv.DetailFieldID = 170260
	AND c.CustomerID > 0
	AND cdv.DetailValue <> c.[CustomerCareSMS]
	AND c.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Send customer care sms', @@ROWCOUNT, CURRENT_TIMESTAMP)

	INSERT INTO CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
	OUTPUT inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue, 10 INTO #FieldHistory (CustomerID, DetailFieldID, DetailValue, LeadORMatter)
	SELECT @ClientID, c.CustomerID, 170260, c.[CustomerCareSMS]
	FROM _C600_ImportCustomerData c 
	WHERE c.CustomerID > 0
	AND c.ImportDate = @ImportDate
	AND NOT EXISTS (
		SELECT *
		FROM CustomerDetailValues cdv WITH (NOLOCK)
		WHERE cdv.CustomerID = c.CustomerID
		AND cdv.DetailFieldID = 170260
	)
	AND c.CustomerID > 0
	AND c.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Send customer care sms inserts', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE cdv
	SET DetailValue = ISNULL(c.[ContactMarketingCommsSMS], '')
	OUTPUT inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue, 10 INTO #FieldHistory (CustomerID, DetailFieldID, DetailValue, LeadORMatter)
	FROM CustomerDetailValues cdv WITH (NOLOCK)
	INNER JOIN _C600_ImportCustomerData c ON cdv.CustomerID = c.CustomerID
	WHERE cdv.DetailFieldID = 177152
	AND c.CustomerID > 0
	AND cdv.DetailValue <> ISNULL(c.[ContactMarketingCommsSMS], '')
	AND c.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Send customer care MarketingCommsSMS', @@ROWCOUNT, CURRENT_TIMESTAMP)

	INSERT INTO CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
	OUTPUT inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue, 10 INTO #FieldHistory (CustomerID, DetailFieldID, DetailValue, LeadORMatter)
	SELECT @ClientID, c.CustomerID, 177152, ISNULL(c.[ContactMarketingCommsSMS], '')
	FROM _C600_ImportCustomerData c 
	WHERE c.CustomerID > 0
	AND c.ImportDate = @ImportDate
	AND NOT EXISTS (
		SELECT *
		FROM CustomerDetailValues cdv WITH (NOLOCK)
		WHERE cdv.CustomerID = c.CustomerID
		AND cdv.DetailFieldID = 177152
	)
	AND c.CustomerID > 0
	AND c.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Send customer care MarketingCommsSMS inserts', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE cdv
	SET DetailValue = ISNULL(c.[ContactMarketingCommsPhone], '')
	OUTPUT inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue, 10 INTO #FieldHistory (CustomerID, DetailFieldID, DetailValue, LeadORMatter)
	FROM CustomerDetailValues cdv WITH (NOLOCK)
	INNER JOIN _C600_ImportCustomerData c ON cdv.CustomerID = c.CustomerID
	WHERE cdv.DetailFieldID = 177371
	AND c.CustomerID > 0
	AND cdv.DetailValue <> ISNULL(c.[ContactMarketingCommsPhone], '')
	AND c.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Send customer care MarketingCommsPhone', @@ROWCOUNT, CURRENT_TIMESTAMP)

	INSERT INTO CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
	OUTPUT inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue, 10 INTO #FieldHistory (CustomerID, DetailFieldID, DetailValue, LeadORMatter)
	SELECT @ClientID, c.CustomerID, 177371, ISNULL(c.[ContactMarketingCommsPhone], '')
	FROM _C600_ImportCustomerData c 
	WHERE c.CustomerID > 0
	AND c.ImportDate = @ImportDate
	AND NOT EXISTS (
		SELECT *
		FROM CustomerDetailValues cdv WITH (NOLOCK)
		WHERE cdv.CustomerID = c.CustomerID
		AND cdv.DetailFieldID = 177371
	)
	AND c.CustomerID > 0
	AND c.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Send customer care MarketingCommsPhone inserts', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE cdv
	SET DetailValue = ISNULL(c.[ContactMarketingCommsEmail], '')
	OUTPUT inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue, 10 INTO #FieldHistory (CustomerID, DetailFieldID, DetailValue, LeadORMatter)
	FROM CustomerDetailValues cdv WITH (NOLOCK)
	INNER JOIN _C600_ImportCustomerData c ON cdv.CustomerID = c.CustomerID
	WHERE cdv.DetailFieldID = 177153
	AND c.CustomerID > 0
	AND cdv.DetailValue <> ISNULL(c.[ContactMarketingCommsEmail], '')
	AND c.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Send customer care MarketingCommsEmail', @@ROWCOUNT, CURRENT_TIMESTAMP)

	INSERT INTO CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
	OUTPUT inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue, 10 INTO #FieldHistory (CustomerID, DetailFieldID, DetailValue, LeadORMatter)
	SELECT @ClientID, c.CustomerID, 177153, ISNULL(c.[ContactMarketingCommsEmail], '')
	FROM _C600_ImportCustomerData c 
	WHERE c.CustomerID > 0
	AND c.ImportDate = @ImportDate
	AND NOT EXISTS (
		SELECT *
		FROM CustomerDetailValues cdv WITH (NOLOCK)
		WHERE cdv.CustomerID = c.CustomerID
		AND cdv.DetailFieldID = 177153
	)
	AND c.CustomerID > 0
	AND c.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Send customer care MarketingCommsEmail inserts', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE cdv
	SET DetailValue = ISNULL(c.[ContactMarketingCommsPost], '')
	OUTPUT inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue, 10 INTO #FieldHistory (CustomerID, DetailFieldID, DetailValue, LeadORMatter)
	FROM CustomerDetailValues cdv WITH (NOLOCK)
	INNER JOIN _C600_ImportCustomerData c ON cdv.CustomerID = c.CustomerID
	WHERE cdv.DetailFieldID = 177154
	AND c.CustomerID > 0
	AND cdv.DetailValue <> ISNULL(c.[ContactMarketingCommsPost], '')
	AND c.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Send customer care MarketingCommsPost', @@ROWCOUNT, CURRENT_TIMESTAMP)

	INSERT INTO CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
	OUTPUT inserted.CustomerID, inserted.DetailFieldID, inserted.DetailValue, 10 INTO #FieldHistory (CustomerID, DetailFieldID, DetailValue, LeadORMatter)
	SELECT @ClientID, c.CustomerID, 177154, ISNULL(c.[ContactMarketingCommsPost], '')
	FROM _C600_ImportCustomerData c 
	WHERE c.CustomerID > 0
	AND c.ImportDate = @ImportDate
	AND NOT EXISTS (
		SELECT *
		FROM CustomerDetailValues cdv WITH (NOLOCK)
		WHERE cdv.CustomerID = c.CustomerID
		AND cdv.DetailFieldID = 177154
	)
	AND c.CustomerID > 0
	AND c.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Send customer care MarketingCommsPost inserts', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE pd
	SET CustomerID = cd.CustomerID
	FROM _C600_ImportPetData pd
	INNER JOIN [dbo].[_C600_ImportCustomerData] cd ON cd.ImportID = pd.ImportID

	UPDATE pd
	SET CustomerID = cd.CustomerID
	FROM _C600_ImportPolicy pd
	INNER JOIN [dbo].[_C600_ImportCustomerData] cd ON cd.ImportID = pd.ImportID

	/************************************************************/
	/*			Customer Complete, Move On To Pet Lead			*/
	/************************************************************/

	/*Create missing policy leads*/
	INSERT INTO Lead (ClientID, LeadRef, CustomerID, LeadTypeID, AquariumStatusID, ClientStatusID, BrandNew, Assigned, AssignedTo, AssignedBy, AssignedDate, RecalculateEquations, Password, Salt, WhenCreated)
	OUTPUT inserted.CustomerID, inserted.LeadID, inserted.LeadRef INTO #ImportedLeads (AquariumCustomerID, PAleadID, PolicyID)
	SELECT DISTINCT @ClientID, p.PolicyID, c.CustomerID, 1492, 2, NULL, 0, 0, NULL, NULL, NULL, 1, NULL, NULL, dbo.fn_GetDate_Local()
	FROM _C600_ImportPetData c
	INNER JOIN [dbo].[_C600_ImportPolicy] p ON p.ImportID = c.ImportID
	WHERE c.LeadID IS NULL 
	AND c.CustomerID > 0
	AND c.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Lead', @@ROWCOUNT, CURRENT_TIMESTAMP)

	PRINT 'Lead insert Complete'

	UPDATE c
	SET LeadID = i.PALeadID
	FROM _C600_ImportPetData c
	INNER JOIN _C600_ImportPolicy p ON p.ImportID = c.ImportID
	INNER JOIN #ImportedLeads i ON i.PolicyID = p.PolicyID COLLATE Latin1_General_CI_AS 
	AND c.BatchID = @BatchID

	/*Create missing policy cases*/
	INSERT INTO Cases (LeadID, ClientID, CaseNum, CaseRef, ClientStatusID, AquariumStatusID, DefaultContactID, LatestLeadEventID, LatestInProcessLeadEventID, LatestOutOfProcessLeadEventID, LatestNonNoteLeadEventID, LatestNoteLeadEventID, WhoCreated, WhenCreated, WhoModified, WhenModified, ProcessStartLeadEventID)
	OUTPUT inserted.LeadID, inserted.CaseID INTO #ImportedPolicyCases(LeadID, CaseID)
	SELECT i.PAleadID, @ClientID, 1, i.PolicyID AS [PolicyNo], NULL AS [Status], 2, NULL, NULL, NULL, NULL, NULL, NULL, 44412, CONVERT(DATE,dbo.fn_GetDate_Local()), 44412, dbo.fn_GetDate_Local(), NULL
	FROM #ImportedLeads i

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Cases', @@ROWCOUNT, CURRENT_TIMESTAMP)

	PRINT 'Case insert Complete'

	UPDATE c
	SET CaseID = i.CaseID
	FROM _C600_ImportPetData c
	INNER JOIN #ImportedPolicyCases i ON i.LeadID = c.LeadID
	AND c.BatchID = @BatchID

	/*Create missing policy matters*/
	INSERT INTO Matter (ClientID, MatterRef, CustomerID, LeadID, MatterStatus, RefLetter, BrandNew, CaseID, WhoCreated, WhenCreated, WhoModified, WhenModified, SourceID)
	OUTPUT inserted.CaseID, inserted.MatterID INTO #ImportedPolicyMatters(CaseID, AquariumMatterID)
	SELECT @ClientID, i.PolicyID, i.AquariumCustomerID, i.PALeadID, 1, dbo.fnRefLetterFromCaseNum(ROW_NUMBER() OVER (Partition BY i.PALeadID ORDER BY i.PALeadID)), 0, p.CaseID, 44412, CONVERT(DATE,dbo.fn_GetDate_Local()), 44412, dbo.fn_GetDate_Local(), NULL
	FROM #ImportedLeads i
	INNER JOIN #ImportedPolicyCases p ON p.LeadID = i.PALeadID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Matter', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE c
	SET MatterID = i.AquariumMatterID
	FROM _C600_ImportPetData c
	INNER JOIN #ImportedPolicyMatters i ON i.CaseID = c.CaseID
	AND c.BatchID = @BatchID

	UPDATE p
	SET LeadID = pd.LeadID, CaseID = pd.CaseID, MatterID = pd.MatterID, [PDHTableRowID] = tr_pdh.TableRowID, [HPTableRowID] = tr_hp.TableRowID
	FROM _C600_ImportPolicy p
	INNER JOIN [dbo].[_C600_ImportPetData] pd ON pd.ImportID = p.ImportID
	LEFT JOIN TableRows tr_pdh WITH (NOLOCK) ON tr_pdh.MatterID = pd.MatterID AND tr_pdh.DetailFieldID = 175336
	LEFT JOIN TableRows tr_hp WITH (NOLOCK) ON tr_hp.MatterID = pd.MatterID AND tr_hp.DetailFieldID = 170033
	AND p.BatchID = @BatchID

	/*Create pet type field*/
	EXEC dbo._C00_CreateDetailValues 144272, '', 100000

	/*
	UPDATE ldv
	SET DetailValue = [BreedID]
	OUTPUT inserted.LeadID, inserted.DetailFieldID, inserted.DetailValue, 1 INTO #FieldHistory (LeadID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPetData] i 
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = i.LeadID
	WHERE ldv.DetailFieldID = 144272
	AND ldv.DetailValue <> [BreedID]
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('BreedID updates', @@ROWCOUNT, CURRENT_TIMESTAMP)
	*/

	/*2019-12-23 ACE for now we will look up the breed based on the text thats comming in..*/
	UPDATE ldv
	SET DetailValue = rldv.ResourceListID
	OUTPUT inserted.LeadID, inserted.DetailFieldID, inserted.DetailValue, 1 INTO #FieldHistory (LeadID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPetData] i 
	INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.DetailFieldID = 144270 AND rldv.DetailValue = i.BreedName
	INNER JOIN ResourceListDetailValues rldv_species WITH (NOLOCK) ON rldv_species.DetailFieldID = 144269 AND rldv_species.ResourceListID = rldv.ResourceListID 
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = i.LeadID
	WHERE ldv.DetailFieldID = 144272
	AND ldv.ValueInt <> rldv.ResourceListID
	AND rldv_species.DetailValue = i.Species
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('BreedID updates', @@ROWCOUNT, CURRENT_TIMESTAMP)


	/*Any Existing Conditions?*/
	EXEC dbo._C00_CreateDetailValues 175496, '', 100000

	UPDATE ldv
	SET DetailValue = [Pre_ExistingConditionExists]
	OUTPUT inserted.LeadID, inserted.DetailFieldID, inserted.DetailValue, 1 INTO #FieldHistory (LeadID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPetData] i 
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = i.LeadID
	WHERE ldv.DetailFieldID = 175496
	AND ldv.DetailValue <> [Pre_ExistingConditionExists]
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Pre_ExistingConditionExists updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Vaccinations up to date?*/
	EXEC dbo._C00_CreateDetailValues 175497, '', 100000

	UPDATE ldv
	SET DetailValue = [VaccinationStatus]
	OUTPUT inserted.LeadID, inserted.DetailFieldID, inserted.DetailValue, 1 INTO #FieldHistory (LeadID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPetData] i 
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = i.LeadID
	WHERE ldv.DetailFieldID = 175497
	AND ldv.DetailValue <> [VaccinationStatus]
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('VaccinationStatus updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Pet Neutered*/
	EXEC dbo._C00_CreateDetailValues 152783, '', 100000

	UPDATE ldv
	SET DetailValue = SPAYEDNEUTERED
	OUTPUT inserted.LeadID, inserted.DetailFieldID, inserted.DetailValue, 1 INTO #FieldHistory (LeadID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPetData] i 
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = i.LeadID
	WHERE ldv.DetailFieldID = 152783
	AND ldv.DetailValue <> SPAYEDNEUTERED
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('SPAYEDNEUTERED updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*DatePurchase*/
	EXEC dbo._C00_CreateDetailValues 170178, '', 100000

	UPDATE ldv
	SET DetailValue = [DatePurchase]
	OUTPUT inserted.LeadID, inserted.DetailFieldID, inserted.DetailValue, 1 INTO #FieldHistory (LeadID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPetData] i 
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = i.LeadID
	WHERE ldv.DetailFieldID = 170178
	AND ldv.DetailValue <> [DatePurchase]
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('DatePurchase updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*DateLastVaccination*/
	EXEC dbo._C00_CreateDetailValues 170179, '', 100000

	UPDATE ldv
	SET DetailValue = DateLastVaccination
	OUTPUT inserted.LeadID, inserted.DetailFieldID, inserted.DetailValue, 1 INTO #FieldHistory (LeadID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPetData] i 
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = i.LeadID
	WHERE ldv.DetailFieldID = 170179
	AND ldv.DetailValue <> DateLastVaccination
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('DateLastVaccination updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[Microchipnumber]*/
	EXEC dbo._C00_CreateDetailValues 170030, '', 100000

	UPDATE ldv
	SET DetailValue = [Microchipnumber]
	OUTPUT inserted.LeadID, inserted.DetailFieldID, inserted.DetailValue, 1 INTO #FieldHistory (LeadID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPetData] i 
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = i.LeadID
	WHERE ldv.DetailFieldID = 170030
	AND ldv.DetailValue <> [Microchipnumber]
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Microchipnumber updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[AnimalName]*/
	EXEC dbo._C00_CreateDetailValues 144268, '', 100000

	UPDATE ldv
	SET DetailValue = [AnimalName]
	OUTPUT inserted.LeadID, inserted.DetailFieldID, inserted.DetailValue, 1 INTO #FieldHistory (LeadID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPetData] i 
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = i.LeadID
	WHERE ldv.DetailFieldID = 144268
	AND ldv.DetailValue <> [AnimalName]
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('AnimalName updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[DateBirth]*/
	EXEC dbo._C00_CreateDetailValues 144274, '', 100000

	UPDATE ldv
	SET DetailValue = CASE WHEN i.[DateBirth] LIKE '%/%' THEN RIGHT(i.[DateBirth], 4) + '-' + SUBSTRING (i.[DateBirth], 4, 2) + '-' + LEFT(i.[DateBirth], 2) ELSE i.[DateBirth] END
	OUTPUT inserted.LeadID, inserted.DetailFieldID, inserted.DetailValue, 1 INTO #FieldHistory (LeadID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPetData] i 
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = i.LeadID
	WHERE ldv.DetailFieldID = 144274
	AND ldv.DetailValue <> [DateBirth]
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('DateBirth updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[PrimaryColour]*/
	EXEC dbo._C00_CreateDetailValues 144273, '', 100000

	UPDATE ldv
	SET DetailValue = [PrimaryColour]
	OUTPUT inserted.LeadID, inserted.DetailFieldID, inserted.DetailValue, 1 INTO #FieldHistory (LeadID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPetData] i 
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = i.LeadID
	WHERE ldv.DetailFieldID = 144273
	AND ldv.DetailValue <> [PrimaryColour]
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('PrimaryColour updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[Sex]*/
	EXEC dbo._C00_CreateDetailValues 144275, '', 100000

	UPDATE ldv
	SET DetailValue = [Sex]
	OUTPUT inserted.LeadID, inserted.DetailFieldID, inserted.DetailValue, 1 INTO #FieldHistory (LeadID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPetData] i 
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = i.LeadID
	WHERE ldv.DetailFieldID = 144275
	AND ldv.DetailValue <> [Sex]
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Sex updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[PurchasePrice]*/
	EXEC dbo._C00_CreateDetailValues 144339, '', 100000

	UPDATE ldv
	SET DetailValue = PurchasePrice
	OUTPUT inserted.LeadID, inserted.DetailFieldID, inserted.DetailValue, 1 INTO #FieldHistory (LeadID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPetData] i 
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = i.LeadID
	WHERE ldv.DetailFieldID = 144339
	AND ldv.DetailValue <> PurchasePrice
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('PurchasePrice updates', @@ROWCOUNT, CURRENT_TIMESTAMP)
	
	/*Species is part of the breed!?!?*/

	/*As is breed name!?!?*/

	/************************************************************/
	/*			Pet Complete, Move On To Policy Matter			*/
	/************************************************************/

	/*[Legacy Policy Number]*/
	EXEC dbo._C00_CreateDetailValues 180168, '', 100000
	
	UPDATE mdv
	SET DetailValue = PolicyID
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 180168
	AND mdv.DetailValue <> PolicyID
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('PolicyID updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[Legacy Policy Number]*/
	EXEC dbo._C00_CreateDetailValues 170049, '', 100000
	
	UPDATE mdv
	SET DetailValue = PolicyID
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 170049
	AND mdv.DetailValue <> PolicyID
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('PolicyID 2 updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Customer level affinity details..*/
	EXEC dbo._C00_CreateDetailValues 170144, '', 100000
	
	UPDATE cdv
	SET DetailValue = 2000184 /*LPC*/
	FROM CustomerDetailValues cdv 
	INNER JOIN _C600_ImportPolicy i ON i.CustomerID = cdv.CustomerID
	WHERE cdv.DetailFieldID = 170144
	AND i.BatchID = @BatchID

	/*
	UPDATE cdv
	SET DetailValue = rldv_sc_aff.ResourceListID
	FROM CustomerDetailValues cdv 
	INNER JOIN _C600_ImportPolicy i ON i.CustomerID = cdv.CustomerID
	INNER JOIN ResourceListDetailValues rldv_pol WITH (NOLOCK) ON rldv_pol.ResourceListID = i.Product AND rldv_pol.DetailFieldID = 175269 /*Short code*/
	INNER JOIN ResourceListDetailValues rldv_sc_aff WITH (NOLOCK) ON rldv_sc_aff.DetailFieldID = 170127 AND rldv_sc_aff.ValueInt = rldv_pol.ValueInt
	WHERE cdv.DetailFieldID = 170144
	AND cdv.DetailValue <> rldv_sc_aff.ResourceListID
	AND i.BatchID = @BatchID
	*/

	/*[Inception]*/
	EXEC dbo._C00_CreateDetailValues 170035, '', 100000
	
	UPDATE mdv
	SET DetailValue = Inception
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 170035
	AND mdv.DetailValue <> Inception
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Inception updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[Start]*/
	EXEC dbo._C00_CreateDetailValues 170036, '', 100000
	
	UPDATE mdv
	SET DetailValue = [Start]
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 170036
	AND mdv.DetailValue <> [Start]
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Start updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[End]*/
	EXEC dbo._C00_CreateDetailValues 170037, '', 100000
	
	UPDATE mdv
	SET DetailValue = [End]
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 170037
	AND mdv.DetailValue <> [End]
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('End updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*2018-11-12 JEL added Sales date to tracking*/
	/*[Sales_Date]*/
	EXEC dbo._C00_CreateDetailValues 177418, '', 100000
	
	UPDATE mdv
	SET DetailValue = [SalesDate]
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 177418
	AND mdv.DetailValue <> [SalesDate]
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('SalesDate updates', @@ROWCOUNT, CURRENT_TIMESTAMP)


	/*[Terms_Date]*/
	EXEC dbo._C00_CreateDetailValues 176925, '', 100000
	
	UPDATE mdv
	SET DetailValue = i.Inception /*[Terms_Date]*/
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 176925
	AND mdv.DetailValue <> Inception
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Terms_Date updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[Current Policy]*/
	EXEC dbo._C00_CreateDetailValues 170034, '', 100000
	
	UPDATE mdv
	SET DetailValue = Product
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 170034
	AND mdv.DetailValue <> Product
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Current Policy updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*
	UPDATE mdv
	SET DetailValue = rldv.ResourceListID
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.DetailFieldID = 144314 and i.Affinity LIKE rldv.DetailValue + '%'
	INNER JOIN ResourceListDetailValues rldv_prod WITH (NOLOCK) ON rldv_prod.ResourceListID = rldv.ResourceListID and rldv_prod.DetailFieldID = 146200 AND rldv_prod.DetailValue = i.Product
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 170034
	AND mdv.DetailValue <> rldv.ResourceListID
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Current Policy updates', @@ROWCOUNT, CURRENT_TIMESTAMP)
	*/
	/*Not sure if we need product_text, underwriter, Policy_Type_Description or [Policy_Excess_Description]*/

	/*[Policy Status]*/
	EXEC dbo._C00_CreateDetailValues 170038, '', 100000
	
	UPDATE mdv
	SET DetailValue = '43002' /*2018-10-26 - ACE Now "Live" Was [Status] from policy table*/
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 170038
	AND mdv.DetailValue <> [Status]
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Status updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Renewal Type - Manual*/
	EXEC dbo._C00_CreateDetailValues 177110, '', 100000
	
	UPDATE mdv
	SET DetailValue = '74338' 
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 177110
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Renewal Type', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Quote Source RL*/
	EXEC dbo._C00_CreateDetailValues 180112, '', 100000
	
	UPDATE mdv
	SET DetailValue = '158009' /*2018-10-26 - ACE Now "Live" Was [Status] from policy table*/
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 180112
	AND mdv.ValueInt <> '158009'
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Quote Source RL updates', @@ROWCOUNT, CURRENT_TIMESTAMP)
	
	/*SignUpMethod GPR 2018-11-17 CR0060*/
	EXEC dbo._C00_CreateDetailValues 176964, '', 100000 /*ID change from '46 to '69 Defect 1301*/
	
	UPDATE mdv
	SET DetailValue = '76536'
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 176964
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('SignUpMethod updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	EXEC dbo._C00_CreateDetailValues 170114, '', 100000 /*Payment Method*/
	
	UPDATE mdv
	SET DetailValue = CASE WHEN i.Account_Number <> '' THEN '76618' ELSE '' END
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 170114
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Payment Method', @@ROWCOUNT, CURRENT_TIMESTAMP)

	EXEC dbo._C00_CreateDetailValues 170176, '', 100000 /*Payment Interval*/
	
	UPDATE mdv
	SET DetailValue = CASE WHEN i.Account_Number <> '' THEN '' ELSE '193289' END
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 170176
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Payment Interval', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*
		Need clarification on where the following go:
		TPL Entitlement	Co Pay Rate	Excess (tabke historical policy)
	*/

	/*Need collections matter ID for the account...*/

	/*following on billing history premium detail history tab.*/

	/*Leadtype relationship is used between the PA matter and collections matter.*/
	UPDATE p
	SET CollectionsLeadID = ltr.ToLeadID, CollectionsMatterID = ltr.ToMatterID
	FROM [dbo].[_C600_ImportPolicy] p
	INNER JOIN LeadTypeRelationship ltr ON ltr.FromMatterID = p.MatterID AND ltr.ToLeadTypeID = 1493
	AND p.BatchID = @BatchID

	/*JEL 56921, Added so we do not create more than one COL lead for a customer if we receive two files with the same customer but different policies.*/
	UPDATE p
	SET CollectionsLeadID = pp.CollectionsLeadID, CollectionsMatterID = pp.CollectionsMatterID
	FROM [dbo].[_C600_ImportPolicy] p 
	INNER JOIN _C600_ImportPolicy pp on pp.CustomerID = p.CustomerID and pp.BatchID <> p.BatchID 
	WHERE pp.CollectionsLeadID IS NOT NULL 
	AND p.BatchID = @BatchID


	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Policies matched to collections updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	PRINT 'Update p Done'

    /*PL #59708 - Added existing collections lead to imported customer*/
    UPDATE p
    SET p.CollectionsLeadID = l.LeadID
    FROM [_C600_ImportPolicy] p
    INNER JOIN Lead l WITH (NOLOCK) ON l.CustomerID = p.CustomerID
    WHERE l.LeadTypeID = 1493
    
	INSERT INTO Lead (ClientID, LeadRef, CustomerID, LeadTypeID, AquariumStatusID, ClientStatusID, BrandNew, Assigned, AssignedTo, AssignedBy, AssignedDate, RecalculateEquations, Password, Salt, WhenCreated)
	OUTPUT inserted.CustomerID, inserted.LeadID INTO #ImportedCollectionsLeads (AquariumCustomerID, CollectionsLeadID)
	SELECT DISTINCT @ClientID, NULL, p.CustomerID, 1493, 2, NULL, 0, 0, NULL, NULL, NULL, 1, NULL, NULL, dbo.fn_GetDate_Local()
	FROM [_C600_ImportPolicy] p
	WHERE p.CollectionsLeadID IS NULL
	AND p.CustomerID > 0
	AND p.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Inserted Collections Leads', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE p
	SET CollectionsLeadID = c.CollectionsLeadID
	FROM [dbo].[_C600_ImportPolicy] p
	INNER JOIN #ImportedCollectionsLeads c ON c.AquariumCustomerID = p.CustomerID
	AND p.BatchID = @BatchID

	PRINT 'Update p2 Done'

	INSERT INTO Cases (LeadID, ClientID, CaseNum, CaseRef, ClientStatusID, AquariumStatusID, DefaultContactID, LatestLeadEventID, LatestInProcessLeadEventID, LatestOutOfProcessLeadEventID, LatestNonNoteLeadEventID, LatestNoteLeadEventID, WhoCreated, WhenCreated, WhoModified, WhenModified, ProcessStartLeadEventID)
	OUTPUT inserted.LeadID, inserted.CaseID, inserted.CaseRef INTO #ImportedCollectionsCases(LeadID, CaseID, ImportedPetID)
	SELECT DISTINCT i.CollectionsLeadID, @ClientID, ROW_NUMBER() OVER (PARTITION BY i.CollectionsLeadID ORDER BY Account_Number), ImportedPetID AS [PolicyNo], NULL AS [Status], 2, NULL, NULL, NULL, NULL, NULL, NULL, 44412, CONVERT(DATE,dbo.fn_GetDate_Local()), 44412, dbo.fn_GetDate_Local(), NULL
	FROM #ImportedCollectionsLeads i
	INNER JOIN [dbo].[_C600_ImportPolicy] p ON p.CollectionsLeadID = i.CollectionsLeadID
	WHERE p.CollectionsMatterID IS NULL
	AND p.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Inserted Collections Cases', @@ROWCOUNT, CURRENT_TIMESTAMP)

	PRINT 'Cases Inserted'

	INSERT INTO Matter (ClientID, MatterRef, CustomerID, LeadID, MatterStatus, RefLetter, BrandNew, CaseID, WhoCreated, WhenCreated, WhoModified, WhenModified, SourceID)
	OUTPUT inserted.CaseID, inserted.MatterID INTO #ImportedCollectionsMatters(CaseID, AquariumMatterID)
	SELECT @ClientID, NULL, l.CustomerID, ic.LeadID, 1, dbo.fnRefLetterFromCaseNum(ROW_NUMBER() OVER (Partition BY ic.LeadID ORDER BY ic.LeadID)), 0, ic.CaseID, 44412, CONVERT(DATE,dbo.fn_GetDate_Local()), 44412, dbo.fn_GetDate_Local(), NULL
	FROM #ImportedCollectionsCases ic
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = ic.LeadID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Inserted Collections Matters', @@ROWCOUNT, CURRENT_TIMESTAMP)

	PRINT 'Matters Inserted'

	UPDATE p
	SET CollectionsMatterID = m.AquariumMatterID
	FROM [_C600_ImportPolicy] p
	INNER JOIN #ImportedCollectionsCases i ON i.LeadID = p.CollectionsLeadID AND p.ImportedPetID = i.ImportedPetID
	INNER JOIN #ImportedCollectionsMatters m ON m.CaseID = i.CaseID
	AND p.BatchID = @BatchID

	PRINT 'Update p3 Done'

	/*Lets add the LTR records where needed*/
	INSERT INTO LeadTypeRelationship ([FromLeadTypeID], [ToLeadTypeID], [FromLeadID], [ToLeadID], [FromCaseID], [ToCaseID], [FromMatterID], [ToMatterID], [ClientID])
	SELECT 1492, 1493, p.LeadID, p.CollectionsLeadID, NULL, NULL, p.MatterID, p.CollectionsMatterID, @ClientID
	FROM [_C600_ImportPolicy] p
	WHERE NOT EXISTS (
		SELECT *
		FROM LeadTypeRelationship ltr WITH (NOLOCK)
		WHERE ltr.FromMatterID = p.MatterID
		AND ltr.ToMatterID = p.CollectionsMatterID
	)
	AND p.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Inserted Collections Lead type relationships', @@ROWCOUNT, CURRENT_TIMESTAMP)

	PRINT 'LTR Done'
	/*JEL 2018-11-12 Defect 1161 Insert accounts as inactive. We will locate this account on renewal and activate*/
	/*JEL 2018-11-12 Defect 1183 added case for account type*/ 
	INSERT INTO Account (ClientID, CustomerID, AccountHolderName, FriendlyName, AccountNumber, Sortcode, Reference, AccountTypeID, Active, WhoCreated, WhenCreated, WhoModified, WhenModified, ObjectID, ObjectTypeID, ClientAccountID, AccountUseID )
	OUTPUT inserted.AccountID, Inserted.ObjectID INTO #ImportedAccounts (AccountID, CollectionsMatterID)
	SELECT @ClientID, p.CustomerID, p.Account_Holder_Name, p.Account_Holder_Name, p.Account_Number, p.Sort_Code, dbo.fn_C600_GetMandateReference(p.CollectionsMatterID), CASE p.Preferred_Collection_Date WHEN '0' THEN '2' ELSE '4' /*GPR 2020-01-10 Premium Funding*/ END , 0 /*Active*/, 58552, dbo.fn_GetDate_Local(), 58552, dbo.fn_GetDate_Local(), p.CollectionsMatterID, 2, 1, 1 
	FROM #ImportedCollectionsMatters m
	INNER JOIN [_C600_ImportPolicy] p ON p.[CollectionsMatterID] = m.AquariumMatterID
	AND p.BatchID = @BatchID
	
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Inserted Accounts', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*
	INSERT INTO AccountMandate ([ClientID], [CustomerID], [AccountID], [MandateStatusID], [DateRequested], [FirstAcceptablePaymentDate], [DirectDebitInstructionID], [Reference], [FailureCode], [FailureDate], [WhoCreated], [WhenCreated], [WhoModified], [WhenModified], [Comments])
	SELECT 600 AS [ClientID], m.[CustomerID], i.[AccountID], 3, dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local(), NULL AS [DirectDebitInstructionID], a.[Reference], NULL, NULL, a.[WhoCreated], a.[WhenCreated], a.[WhoModified], a.[WhenModified], 'Created by import'
	FROM #ImportedAccounts i
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = i.CollectionsMatterID 
	INNER JOIN Account a ON a.AccountID = i.AccountID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Inserted Account Mandates', @@ROWCOUNT, CURRENT_TIMESTAMP)
	*/

	/*Account ID*/
	EXEC dbo._C00_CreateDetailValues 176973, '', 100000
	
	UPDATE mdv
	SET DetailValue = i.AccountID
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM #ImportedAccounts i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.CollectionsMatterID
	WHERE mdv.DetailFieldID = 176973
	AND mdv.DetailValue <> i.AccountID

	/*Account Name*/ 
	EXEC dbo._C00_CreateDetailValues 170188, '', 100000
	
	UPDATE mdv
	SET DetailValue = i.Account_Holder_Name
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.CollectionsMatterID
	WHERE mdv.DetailFieldID = 170188
	AND mdv.DetailValue <> i.Account_Holder_Name

	/*Account Number*/ 
	EXEC dbo._C00_CreateDetailValues 170190, '', 100000
	
	UPDATE mdv
	SET DetailValue = i.Account_Number
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.CollectionsMatterID
	WHERE mdv.DetailFieldID = 170190
	AND mdv.DetailValue <> i.Account_Number

	/*Sort Code*/ 
	EXEC dbo._C00_CreateDetailValues 170189, '', 100000
	
	UPDATE mdv
	SET DetailValue = i.Sort_Code
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.CollectionsMatterID
	WHERE mdv.DetailFieldID = 170189
	AND mdv.DetailValue <> i.Sort_Code

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Matter Account updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Payment Method*/
	EXEC dbo._C00_CreateDetailValues 170115, '', 100000
	
	UPDATE mdv
	SET DetailValue = CASE Preferred_Collection_Date
		WHEN 0 THEN 69931 /*Credit Card*/
		ELSE 76618 /*GPR 2020-01-10 Premium Funding*/ --69930 /*BACS*/
		END
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPolicy] p 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = p.CollectionsMatterID
	WHERE mdv.DetailFieldID = 170115
	AND mdv.DetailValue <>  CASE Preferred_Collection_Date
		WHEN 0 THEN 69931 /*Credit Card*/
		ELSE 76618 /*GPR 2020-01-10 Premium Funding*/ --69930 /*BACS*/
		END
	AND p.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Payment method updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Preferred_Collection_Date*/
	EXEC dbo._C00_CreateDetailValues 170168, '', 100000
	
	UPDATE mdv
	SET DetailValue = Preferred_Collection_Date
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPolicy] p 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = p.MatterID
	WHERE mdv.DetailFieldID = 170168
	AND mdv.DetailValue <> p.Preferred_Collection_Date
	AND p.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Preferred_Collection_Date updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Payment Method*/
	EXEC dbo._C00_CreateDetailValues 170114, '', 100000
	
	UPDATE mdv
	SET DetailValue = CASE Preferred_Collection_Date
		WHEN 0 THEN 69931 /*Credit Card*/
		ELSE  76618 /*GPR 2020-01-10 Premium Funding*/ --69930 /*BACS*/
		END
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPolicy] p 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = p.MatterID
	WHERE mdv.DetailFieldID = 170114
	AND mdv.DetailValue <> CASE Preferred_Collection_Date
		WHEN 0 THEN 69931 /*Credit Card*/
		ELSE  76618 /*GPR 2020-01-10 Premium Funding*/ --69930 /*BACS*/
		END
	AND p.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Payment Method updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Payment Method*/
	EXEC dbo._C00_CreateDetailValues 170176, '', 100000
	
	UPDATE mdv
	SET DetailValue = CASE Preferred_Collection_Date
		WHEN 0 THEN 69944 /*Annually*/
		ELSE 193289 /*GPR 2020-01-10 Premium Funding*/ --69943 /*Monthly*/
		END
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPolicy] p 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = p.MatterID
	WHERE mdv.DetailFieldID = 170176
	AND mdv.DetailValue <> CASE Preferred_Collection_Date
		WHEN 0 THEN 69944 /*Annually*/
		ELSE 193289 /*GPR 2020-01-10 Premium Funding*/ --69943 /*Monthly*/
		END
	AND p.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Payment Method updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*TPL_Entitlement*/
	EXEC dbo._C00_CreateDetailValues 180210, '', 100000
	
	UPDATE mdv
	SET DetailValue = TPL_Entitlement
	OUTPUT inserted.LeadID, inserted.DetailFieldID, inserted.DetailValue, 1 INTO #FieldHistory (LeadID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPolicy] p 
	INNER JOIN LeadDetailValues mdv ON mdv.LeadID = p.LeadID
	WHERE mdv.DetailFieldID = 180210
	AND mdv.DetailValue <> p.TPL_Entitlement
	AND p.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Preferred_Collection_Date updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Historical Policy*/
	INSERT INTO TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete)
	OUTPUT inserted.MatterID, inserted.TableRowID INTO #ImportedHPTableRowIDs (MatterID, TableRowID)
	SELECT @ClientID, p.MatterID, 170033, 19010, 1, 1
	FROM [dbo].[_C600_ImportPolicy] p 
	WHERE p.MatterID > 0
	AND p.BatchID = @BatchID
	AND p.HPTableRowID IS NULL

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Inserted PA Table Rows', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE i
	SET HPTableRowID = r.TableRowID
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN #ImportedHPTableRowIDs r ON r.MatterID = i.MatterID 
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.HPTableRowID, 180216, 'true'
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.HPTableRowID > 0
	AND i.BatchID = @BatchID


	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.HPTableRowID, 145662, i.Inception
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.HPTableRowID > 0
	AND i.BatchID = @BatchID

	/*Start*/
	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.HPTableRowID, 145663, i.Inception
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.HPTableRowID > 0
	AND i.BatchID = @BatchID

	/*End*/
	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.HPTableRowID, 145664, CONVERT(VARCHAR(10),(DATEADD(YYYY, 1, i.Inception)-1),120) --CONVERT(VARCHAR(10),DATEADD(YYYY, 1, i.Inception),120)
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.HPTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.HPTableRowID, 170092, i.Terms_Date
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.HPTableRowID > 0
	AND i.BatchID = @BatchID

	/*
		Need to make sure the last one is the year is the "live" one.
		We will be inserting any other years in order later on.

		So if this is the first and only row (IE policy is in first year and renewing onto year 2 then it'll be live 
			or whatever we receive
		If its being created as a historical row (IE from 2015) then we want to have it as not live we we will be 
			addin the other rows in the next section.
	*/
	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.HPTableRowID, 145666, --CASE ROW_NUMBER() OVER (PARTITION BY i.MatterID ORDER BY i.[End] DESC) 
	CASE WHEN  i.[Inception]  <   DATEADD(YEAR,-1,dbo.fn_GetDate_Local())
		--WHEN 1
		 THEN '74536'
		/*Last row*/ /*GPR 2018-11-13 #1177 Changed from 43002 to resolve issue where oldest HistPolicyRow policy row was set to Live incorrectly*/
		/*JEL  changed to check if inception date is more than a year ago as the later fabrication of tablerows will set the most recent one to live*/ 
		ELSE '43002' /*Not-live(not the last row)*/
		END
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.HPTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.HPTableRowID, 177414, CASE WHEN i.TPL_Entitlement = '5144' THEN 'No' ELSE 'Yes' END
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.HPTableRowID > 0
	AND i.BatchID = @BatchID


	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.HPTableRowID, 177415, i.Co_Pay_Rate
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.HPTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.HPTableRowID, 177416, i.Excess
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.HPTableRowID > 0
	AND i.BatchID = @BatchID
	
	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, ResourceListID)
	SELECT @ClientID, i.MatterID, i.HPTableRowID, 145665, i.Product
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.HPTableRowID > 0
	AND i.BatchID = @BatchID

	/*Do the resource on the table*/
	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, ResourceListID)
	SELECT @ClientID, i.MatterID, i.HPTableRowID, 177416, mdv.ValueInt
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = i.MatterID AND mdv.DetailFieldID = 170034
	WHERE i.HPTableRowID > 0
	AND i.BatchID = @BatchID

	/*Legacy made up policy years*/
	INSERT INTO TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, SourceID)
	OUTPUT inserted.MatterID, inserted.TableRowID, inserted.SourceID INTO #ImportedHistoricalHPTableRowIDs (MatterID, TableRowID, PolicyYear)
	SELECT @ClientID, p.MatterID, 170033, 19010, 1, 1, t.N as [Start]
	FROM [dbo].[_C600_ImportPolicy] p 
	INNER JOIN Tally t with (nolock) on t.N BETWEEN datepart(yyyy,p.Inception)+1 AND DATEPART(yyyy, p.[End])-1
	WHERE p.BatchID = @BatchID
	ORDER BY p.ImportID, t.N

	/*GPR 2018-10-31 Populate 'Is Migrated Policy Term'*/
	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, h.TableRowID, 180216, 'true'
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN #ImportedHistoricalHPTableRowIDs h ON h.MatterID = i.MatterID
	WHERE h.TableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, h.TableRowID, 145662, i.Inception
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN #ImportedHistoricalHPTableRowIDs h ON h.MatterID = i.MatterID
	WHERE h.TableRowID > 0
	AND i.BatchID = @BatchID

	/*Start*/
	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, h.TableRowID, 145663, CONVERT(varchar, h.PolicyYear) + '-' + case when DATEPART(MM, i.Start) < 10 THEN '0' else '' END + CONVERT(varchar, DATEPART(MM, i.Start)) + '-' + case when DATEPART(DD, i.Start) < 10 THEN '0' else '' END + CONVERT(varchar, DATEPART(DD, i.Start))
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN #ImportedHistoricalHPTableRowIDs h ON h.MatterID = i.MatterID
	WHERE h.TableRowID > 0
	AND i.BatchID = @BatchID

	/*END*/
	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, h.TableRowID, 145664, CONVERT(varchar, h.PolicyYear+1) + '-' + case when DATEPART(MM, i.Start) < 10 THEN '0' else '' END + CONVERT(varchar, DATEPART(MM, i.Start)) + '-' + case when DATEPART(DD, i.Start) < 10 THEN '0' else '' END + CONVERT(varchar, DATEPART(DD, i.Start)-1)
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN #ImportedHistoricalHPTableRowIDs h ON h.MatterID = i.MatterID
	WHERE h.TableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, h.TableRowID, 170092, i.Terms_Date
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN #ImportedHistoricalHPTableRowIDs h ON h.MatterID = i.MatterID
	WHERE h.TableRowID > 0
	AND i.BatchID = @BatchID

	/*Status should be the last one from the file. All of the old ones should be not live as they have been superceeded.*/
	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, h.TableRowID, 145666, CASE ROW_NUMBER() OVER (PARTITION BY h.MatterID ORDER BY h.PolicyYear DESC) 
		WHEN 1 THEN '43002' /*Last row*/ 
		ELSE '74536' /*Not-live(not the last row)*/
		END
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN #ImportedHistoricalHPTableRowIDs h ON h.MatterID = i.MatterID
	WHERE h.TableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, h.TableRowID, 177414, i.TPL_Entitlement
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN #ImportedHistoricalHPTableRowIDs h ON h.MatterID = i.MatterID
	WHERE h.TableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, h.TableRowID, 177415, i.Co_Pay_Rate
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN #ImportedHistoricalHPTableRowIDs h ON h.MatterID = i.MatterID
	WHERE h.TableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, h.TableRowID, 177416, i.Excess
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN #ImportedHistoricalHPTableRowIDs h ON h.MatterID = i.MatterID
	WHERE h.TableRowID > 0
	AND i.BatchID = @BatchID

	/*Update XML Column with table row IDs that were inserted.*/
	UPDATE ipol
	SET ImportedHistoricalHPTableRowIDs = (select *
	from #ImportedHistoricalHPTableRowIDs ii
	WHERE ii.MatterID = i.MatterID
	for xml path ('TRID')
	)
	FROM #ImportedHistoricalHPTableRowIDs i 
	INNER JOIN _C600_ImportPolicy ipol ON ipol.MatterID = i.MatterID

	/*Product should be a resource value.*/
	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, ResourceListID)
	SELECT @ClientID, i.MatterID, h.TableRowID, 145665, i.Product
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN #ImportedHistoricalHPTableRowIDs h ON h.MatterID = i.MatterID
	WHERE h.TableRowID > 0
	AND i.BatchID = @BatchID

	/*Premium Detail History*/
	INSERT INTO TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, SourceID)
	OUTPUT inserted.MatterID, inserted.TableRowID, inserted.SourceID INTO #ImportedPDHTableRowIDs (MatterID, TableRowID, ImportedPetID)
	SELECT @ClientID, p.MatterID, 175336, 19011, 1, 1, p.ImportedPetID
	FROM [dbo].[_C600_ImportPolicy] p 
	WHERE p.MatterID > 0
	AND p.BatchID = @BatchID
	AND p.PDHTableRowID IS NULL

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Inserted Premium Detail History Table Rows', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE i
	SET PDHTableRowID = r.TableRowID
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN #ImportedPDHTableRowIDs r ON r.ImportedPetID = i.ImportedPetID
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175446, i.[Rating_Date_Time]
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175346, i.[Start] /*i.[From]*/
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175347, i.[End] /*i.[To]*/
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175348, i.[Event]
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175349, CONVERT(NUMERIC(18,2),i.[Previous_Annual])
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175375, CONVERT(NUMERIC(18,2),i.[Previous_Annual_IPT])
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 178291, CONVERT(NUMERIC(18,2),i.[New_Annual_WP])
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175350, CONVERT(NUMERIC(18,2),i.[First_month])
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175351, CONVERT(NUMERIC(18,2),i.[Other_Month])
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175709, i.[PremiumCalculationID]
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	/*2020-01-07 ACE hard coded
	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175722, i.[Premium_Detail_Status]
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID
	*/

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175722, '76514'
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175396, i.Adjustment_Date
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175397, i.Adjustment_Amount
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175398, i.[Adjustment_Type]
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175400, i.[Adjustment_Application_Date]
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175371, i.[Cancellation_Reason]
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175373, i.[Discount]
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 177899, i.[Premiumless_Discount]
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 177688, i.[TPL_CoIns_Net]
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 177689, i.[TPL_CoIns_Gross]
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 177690, i.[TPL_CoIns_IPT]
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 177903, i.[Adjustment_Admin_Fee]
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 179430, i.[Sequence_Number]
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 179718, i.[Written_Premium_Affecting]
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 179719, i.[Non_Written_Premium_Affecting]
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175377, i.HPTableRowID
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID

	/*Adjustment Type*/
	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.PDHTableRowID, 175398, CASE ROW_NUMBER() OVER (PARTITION BY i.MatterID ORDER BY i.[From]) WHEN 1 THEN 74527 /*New*/
		ELSE 74528 /*Renewal*/
		END
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.PDHTableRowID > 0
	AND i.BatchID = @BatchID
	
	/*Create Discount Code field*/
	EXEC dbo._C00_CreateDetailValues 175488, '', 100000

	UPDATE mdv
	SET DetailValue = [Discount_Code]
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 175488
	AND mdv.DetailValue <> [Discount_Code]
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Discount_Codes updates', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/************************************************************/
	/*			Pet Lead complete move on to claims 			*/
	/************************************************************/

	/*
		1. Match based on claim number first. 
		If we cant then we will create the link between PA and Claims, if none then we will need to create claims lead, case and matters etc.
	*/

	UPDATE p
	SET CustomerID = m.CustomerID, LeadID = m.LeadID, CaseID = m.CaseID, MatterID = m.MatterID, ImportedDate = @ImportDate, CDDTableRowID = tr.TableRowID 
	FROM [dbo]._C600_ImportClaims p
	INNER JOIN MatterDetailValues mdv_claimID WITH (NOLOCK) ON mdv_claimID.DetailFieldID = 180186
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = mdv_claimID.MatterID 
	INNER JOIN Customers cu WITH (NOLOCK) ON cu.CustomerID = m.CustomerID AND cu.Test = 0
	LEFT JOIN TableRows tr WITH (NOLOCK) ON tr.MatterID = m.MatterID AND tr.DetailFieldID = 144355
	WHERE mdv_claimID.DetailValue = p.ClaimID
	AND p.ImportedDate IS NULL
	AND p.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Matched claims to PA', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE c
	SET CustomerID = p.CustomerID, LeadID = ltr.ToLeadID, ImportedDate = @ImportDate
	FROM [dbo]._C600_ImportClaims c
	INNER JOIN _C600_ImportPetData p ON p.ImportID = c.ImportID
	LEFT JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.FromMatterID = p.MatterID AND ltr.ToLeadTypeID = 1490
	AND c.BatchID = @BatchID

	/*Now for claims only imports... We will need to get the policy admin lead, get the customer, traverse through ltr to get to the claim lead..*/
	UPDATE i
	SET CustomerID = m.CustomerID, LeadID = ltr.ToLeadID, ImportedDate = @ImportDate
	FROM _C600_ImportClaims i
	INNER JOIN MatterDetailValues mdv with (nolock) on mdv.DetailFieldID = 180168 and mdv.DetailValue = i.PolicyID
	INNER JOIN Matter m with (nolock) on m.MatterID = mdv.MatterID
	INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = m.CustomerID AND c.Test = 0
	INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.FromMatterID = m.MatterID AND ltr.ToLeadTypeID = 1490
	WHERE i.LeadID IS NULL

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Matched new claims using PolicyID', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*
		So, we have matched any claims or claim leads where available.
		Now we will need to create any missing claims leads, link them back to the claims and create cases/matters for each one left over

		NB-----2019-12-23 We seem to have an issue with the XML data we are getting from Trent. They are supplying multiple customer and policy records each with a seperate claim.
		Ive asked if this is likely to happen in reality, will have to wait for the outcome before we decide what needs doing here if anything..
	*/

	INSERT INTO Lead (ClientID, LeadRef, CustomerID, LeadTypeID, AquariumStatusID, ClientStatusID, BrandNew, Assigned, AssignedTo, AssignedBy, AssignedDate, RecalculateEquations, Password, Salt, WhenCreated)
	OUTPUT inserted.CustomerID, inserted.LeadID, inserted.LeadRef INTO #ImportedClaimsLeads (CustomerID, LeadID, ImportID)
	SELECT DISTINCT @ClientID, c.ImportID, c.CustomerID, 1490, 2, NULL /*ACE 2019-12-13 Set back to null as we get text that doesnt match*/ /*c.ClaimStatus*/ /*GPR 2018-11-17*/, 0, 0, NULL, NULL, NULL, 1, NULL, NULL, dbo.fn_GetDate_Local()
	FROM [dbo]._C600_ImportClaims c
	WHERE c.LeadID IS NULL
	AND c.CustomerID > 0
	AND c.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Inserted Claims Leads', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE c
	SET LeadID = i.LeadID
	FROM [dbo]._C600_ImportClaims c
	INNER JOIN #ImportedClaimsLeads i ON i.CustomerID = c.CustomerID AND i.ImportID = c.ImportID
	WHERE c.LeadID IS NULL
	AND c.BatchID = @BatchID

	INSERT INTO Cases (LeadID, ClientID, CaseNum, CaseRef, ClientStatusID, AquariumStatusID, DefaultContactID, LatestLeadEventID, LatestInProcessLeadEventID, LatestOutOfProcessLeadEventID, LatestNonNoteLeadEventID, LatestNoteLeadEventID, WhoCreated, WhenCreated, WhoModified, WhenModified, ProcessStartLeadEventID)
	OUTPUT inserted.LeadID, inserted.CaseID, inserted.CaseRef INTO #ImportedClaimsCases(LeadID, CaseID, ImportedClaimID)
	SELECT DISTINCT i.LeadID, @ClientID, ROW_NUMBER() OVER (Partition BY i.LeadID ORDER BY i.LeadID), i.ImportedClaimID AS [PolicyNo], NULL /*ACE 2019-12-13 As above test status wont work*//*i.ClaimStatus*/ /*GPR 2018-11-17 Defect 1157*/ AS [Status], 2, NULL, NULL, NULL, NULL, NULL, NULL, 44412, CONVERT(DATE,dbo.fn_GetDate_Local()), 44412, dbo.fn_GetDate_Local(), NULL
	FROM _C600_ImportClaims i
	WHERE i.CaseID IS NULL
	AND i.LeadID > 0
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Inserted Claims Cases', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE c
	SET CaseID = i.CaseID
	FROM [dbo]._C600_ImportClaims c
	INNER JOIN #ImportedClaimsCases i ON i.ImportedClaimID = c.ImportedClaimID
	WHERE c.CaseID IS NULL
	AND c.BatchID = @BatchID

	INSERT INTO Matter (ClientID, MatterRef, CustomerID, LeadID, MatterStatus, RefLetter, BrandNew, CaseID, WhoCreated, WhenCreated, WhoModified, WhenModified, SourceID)
	OUTPUT inserted.CaseID, inserted.MatterID INTO #ImportedClaimsMatters(CaseID, MatterID)
	SELECT @ClientID, c.ImportedClaimID, c.CustomerID, c.LeadID, 1, dbo.fnRefLetterFromCaseNum(ROW_NUMBER() OVER (Partition BY c.LeadID ORDER BY c.LeadID)), 0, c.CaseID, 44412, CONVERT(DATE,dbo.fn_GetDate_Local()), 44412, dbo.fn_GetDate_Local(), NULL
	FROM _C600_ImportClaims c
	WHERE c.MatterID IS NULL
	AND c.LeadID > 0
	AND c.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Inserted Claims Matters', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE c
	SET MatterID = i.MatterID
	FROM [dbo]._C600_ImportClaims c
	INNER JOIN #ImportedClaimsMatters i ON i.CaseID = c.CaseID
	WHERE c.MatterID IS NULL
	AND c.BatchID = @BatchID

	/*Lets add the LTR records where needed*/
	INSERT INTO LeadTypeRelationship ([FromLeadTypeID], [ToLeadTypeID], [FromLeadID], [ToLeadID], [FromCaseID], [ToCaseID], [FromMatterID], [ToMatterID], [ClientID])
	SELECT DISTINCT 1492, 1490, p.LeadID, c.LeadID, NULL, NULL, p.MatterID, c.MatterID, @ClientID
	FROM [_C600_ImportPolicy] p
	INNER JOIN _C600_ImportClaims c ON c.ImportID = p.ImportID
	WHERE NOT EXISTS (
		SELECT *
		FROM LeadTypeRelationship ltr WITH (NOLOCK)
		WHERE ltr.FromMatterID = p.MatterID
		AND ltr.ToMatterID = c.MatterID
	)
	AND c.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Inserted PA to Claims LTRs', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[Imported ClaimID]*/
	EXEC dbo._C00_CreateDetailValues 180186, '', 100000
	
	UPDATE mdv
	SET DetailValue = ClaimID
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 180186
	AND mdv.DetailValue <> i.ClaimID
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Claim ID', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Start of claim data..*/

	/*[Imported Claim Parent ID]*/
	EXEC dbo._C00_CreateDetailValues 180187, '', 100000
	
	UPDATE mdv
	SET DetailValue = ISNULL([ClaimParentID], '')
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 180187
	AND mdv.DetailValue <> ISNULL(i.[ClaimParentID], '')
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated ClaimParentID', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*GPR 2019-01-02 #1342 - Date Payment Confirmed*/
	EXEC dbo._C00_CreateDetailValues 162644, '', 100000
	
	/*2019-12-13 DetailValue check was using claim parent id for the update..*/
	UPDATE mdv
	SET DetailValue = ISNULL(Date_payment_confirmed, '')
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 162644
	AND mdv.DetailValue <> ISNULL(Date_payment_confirmed, '')
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Date Payment Confirmed', @@ROWCOUNT, CURRENT_TIMESTAMP)



	/*[FNOL_Description_of_loss]*/
	EXEC dbo._C00_CreateDetailValues 144332, '', 100000
	
	UPDATE mdv
	SET DetailValue = FNOL_Description_of_loss
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 144332
	AND mdv.DetailValue <> i.FNOL_Description_of_loss
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated FNOL_Description_of_loss', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[Claims_Advisor_Description_Of_Loss]*/
	EXEC dbo._C00_CreateDetailValues 176908, '', 100000
	
	UPDATE mdv
	SET DetailValue = [Claims_Advisor_Description_Of_Loss]
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 176908
	AND mdv.DetailValue <> i.[Claims_Advisor_Description_Of_Loss]
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Claims_Advisor_Description_Of_Loss', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[Claim_Type]*/
	EXEC dbo._C00_CreateDetailValues 144483, '', 100000
	
	UPDATE mdv
	SET DetailValue = [Claim_Type]
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 144483
	AND mdv.DetailValue <> i.[Claim_Type]
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Claim_Type', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[Claim_Form_Type]*/
	EXEC dbo._C00_CreateDetailValues 170031, '', 100000
	
	UPDATE mdv
	SET DetailValue = [Claim_Form_Type]
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 170031
	AND mdv.DetailValue <> i.[Claim_Form_Type]
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Claim_Form_Type', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*
	/*[Claim Status???]*/
	EXEC dbo._C00_CreateDetailValues 170031, '', 100000
	
	UPDATE mdv
	SET DetailValue = [Claim_Form_Type]
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 170031
	*/
	
	
	/*UAH / GPR 2018-11-15  [Claim Status]*/
	--Query the migrated claims table and get the count. 
	/*JEL added @BatchID filter so we don't do this every time for every claim*/ 
	SELECT @ClaimCount = COUNT(ImportedClaimID) 
	FROM _C600_ImportClaims WITH (NOLOCK)
	WHERE BatchID = @BatchID 

	--While the counter is less than the claim count
	WHILE (@Counter <= @ClaimCount)
	BEGIN
		--get the claim status of the current imported claim row
		SELECT @ImportedClaimStatus =  ClaimStatus
		FROM (SELECT ROW_NUMBER() OVER (ORDER BY importedclaimID asc) AS RowNumber,* FROM  _C600_ImportClaims WHERE BatchID = @BatchID ) AS ImportedClaims
		WHERE  @Counter = RowNumber 


		--get the matter id of the current imported claim row
		SELECT @MatterID =  MatterID
		FROM (SELECT ROW_NUMBER() OVER (ORDER BY importedclaimID asc) AS RowNumber,* FROM  _C600_ImportClaims WHERE BatchID = @BatchID ) AS ImportedClaims
		WHERE  @Counter = RowNumber 
			
		--Based on the status we have stored in the 'claim status' field variable above, we need to find out what event to apply. 
		--We can do this by using a case statement to determine which event to assign to @EventToApply based on the status itself
		/*
		
			NB - Some of the events below do not exist. We need to clean this list down before go live or some imports could fail.

		*/
		SELECT @EventToApply = 
			CASE
					WHEN @ImportedClaimStatus = '3818' THEN 158877
					WHEN @ImportedClaimStatus = '4467' THEN 158878
					WHEN @ImportedClaimStatus = '4468' THEN 158879
					WHEN @ImportedClaimStatus = '4028' THEN 158880
					--WHEN @ImportedClaimStatus = '4469' THEN 158881 /*Doesnt exist*/
					WHEN @ImportedClaimStatus = '4029' THEN 158882
					WHEN @ImportedClaimStatus = '4470' THEN 158883 /*Migration – Closed - Paid*/
					WHEN @ImportedClaimStatus = '3829' THEN 158884
					WHEN @ImportedClaimStatus = '3819' THEN 158885
					WHEN @ImportedClaimStatus = '4687' THEN 158886
					WHEN @ImportedClaimStatus = '4688' THEN 158887
					WHEN @ImportedClaimStatus = '4706' THEN 158888
					WHEN @ImportedClaimStatus = '4685' THEN 158889
					WHEN @ImportedClaimStatus = '4703' THEN 158890
					WHEN @ImportedClaimStatus = '4704' THEN 158891
					--WHEN @ImportedClaimStatus = '4721' THEN 158892 /*Doesnt exist*/
					WHEN @ImportedClaimStatus = '4689' THEN 158893
					WHEN @ImportedClaimStatus = '4690' THEN 158894
					WHEN @ImportedClaimStatus = '4547' THEN 158895
				ELSE 150002 END
				FROM _C600_ImportClaims WITH ( NOLOCK ) WHERE @MatterID = MatterID

		/*GPR 2018-11-17 Apply process start to Claims*/
		DECLARE @CaseID INT
		SELECT @CaseID =  CaseID 
		FROM _C600_ImportClaims WITH ( NOLOCK )  
		WHERE @MatterID = MatterID
			
		/*GPR 2018-11-17 We only want to apply it the once*/
		IF (SELECT COUNT(le.LeadEventID) FROM LeadEvent le WITH ( NOLOCK ) WHERE le.CaseID = @CaseID AND le.EventTypeID = 150002) = 0
		BEGIN
			EXEC dbo._C00_AddProcessStart @CaseID,58552,150002
		END
				
		--Apply the selected event on the respective matter that is in the _C600_ImportClaims table 
		/*GPR 2018-11-17 there was some duplication of events so I've wrapped it in an IF block*/
		IF (SELECT COUNT(le.LeadEventID) FROM LeadEvent le WITH ( NOLOCK ) WHERE le.CaseID = @CaseID AND le.EventTypeID = @EventToApply) = 0
		BEGIN
			SELECT @LeadEventID = c.LatestLeadEventID 
			FROM Cases c with (NOLOCK) 
			WHERE c.CaseID = @CaseID 

			EXEC [dbo].[_C00_ApplyLeadEventByAutomatedEventQueue] @LeadEventID,@EventToApply,58552,-1
		END			
			 
		--increment the counter so it moves to process the next row
		SELECT @Counter += 1 
	END	
	
	/*[Body_Part]*/
	EXEC dbo._C00_CreateDetailValues 144333, '', 100000
	
	UPDATE mdv
	SET DetailValue = Body_Part
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 144333
	AND mdv.DetailValue <> i.Body_Part
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Body_Part', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[Body_Part_Other]*/
	EXEC dbo._C00_CreateDetailValues 176934, '', 100000
	
	UPDATE mdv
	SET DetailValue = Body_Part_Other
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 176934
	AND mdv.DetailValue <> i.Body_Part_Other
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Body_Part_Other', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[Date_of_Loss]*/
	EXEC dbo._C00_CreateDetailValues 144892, '', 100000
	
	UPDATE mdv
	SET DetailValue = Date_of_Loss
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 144892
	AND mdv.DetailValue <> i.Date_of_Loss
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Date_of_Loss', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[Time_of_loss_HH_MM]*/
	EXEC dbo._C00_CreateDetailValues 175303, '', 100000
	
	UPDATE mdv
	SET DetailValue = Time_of_loss_HH_MM
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 175303
	AND mdv.DetailValue <> i.Time_of_loss_HH_MM
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Time_of_loss_HH_MM', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[Treatment_Start]*/
	EXEC dbo._C00_CreateDetailValues 144366, '', 100000
	
	UPDATE mdv
	SET DetailValue = Treatment_Start
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 144366
	AND mdv.DetailValue <> i.Treatment_Start
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Treatment_Start', @@ROWCOUNT, CURRENT_TIMESTAMP)

	PRINT 'Treatment_Start Done'

	/*[Treatment_End]*/
	EXEC dbo._C00_CreateDetailValues 145674, '', 100000
	
	UPDATE mdv
	SET DetailValue = Treatment_End
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 145674
	AND mdv.DetailValue <> i.Treatment_End
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Treatment_End', @@ROWCOUNT, CURRENT_TIMESTAMP)

	PRINT 'TreatmentEnd Done'

	/*[Diet_food_start_date]*/
	EXEC dbo._C00_CreateDetailValues 162695, '', 100000
	
	UPDATE mdv
	SET DetailValue = Diet_food_start_date
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 162695
	AND mdv.DetailValue <> i.Diet_food_start_date
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Diet_food_start_date', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[Diet_food_start_date]*/
	EXEC dbo._C00_CreateDetailValues 149850, '', 100000
	
	UPDATE mdv
	SET DetailValue = CONVERT(NUMERIC(18,2),Total_Claim_Amount)
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 149850
	AND mdv.ValueMoney <> CONVERT(NUMERIC(18,2),Total_Claim_Amount)
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Total_Claim_Amount', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[Accident_Illness]*/
	EXEC dbo._C00_CreateDetailValues 175576, '', 100000
	
	UPDATE mdv
	SET DetailValue = Accident_Illness
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 175576
	AND mdv.DetailValue <> i.Accident_Illness
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Accident_Illness', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*[AilmentID]*/
	EXEC dbo._C00_CreateDetailValues 144504, '', 100000
	
	UPDATE mdv
	SET DetailValue = CASE ISNULL(i.AilmentID,'') WHEN '' THEN 135140 ELSE i.AilmentID END /*GPR 2020-01-16 default to 'DIAGNOSIS NOT MADE (No diagnosis)'*/
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 144504
	--AND mdv.DetailValue <> i.AilmentID /*GPR removed, if AilmentID is blank this line prevents the UPDATE*/
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated AilmentID', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Help!!!*/
	/*[VetID]*/

	EXEC dbo._C00_CreateDetailValues 146215, '', 100000
	
	UPDATE ldv
	SET DetailValue = VetID
	OUTPUT inserted.LeadID, inserted.DetailFieldID, inserted.DetailValue, 1 INTO #FieldHistory (LeadID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN LeadTypeRelationship ltr on ltr.ToLeadID = i.LeadID
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = ltr.FromLeadID
	WHERE ldv.DetailFieldID = 146215
	AND ldv.DetailValue <> i.VetID
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated VetID', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Claim Details Data*/
	INSERT INTO TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete)
	OUTPUT inserted.MatterID, inserted.TableRowID INTO #ImportedCDDTableRowIDs (MatterID, TableRowID)
	SELECT @ClientID, i.MatterID, 144355, 16157, 1, 1
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.MatterID > 0
	AND i.BatchID = @BatchID
	AND i.CDDTableRowID IS NULL

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Inserted Claim Details Data TableRows', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE i
	SET CDDTableRowID = r.TableRowID
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN #ImportedCDDTableRowIDs r ON r.MatterID = i.MatterID 
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.CDDTableRowID, 144349, i.[Treat_Start]
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.CDDTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.CDDTableRowID, 144351, i.Treatment_End2
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.CDDTableRowID > 0
	AND i.BatchID = @BatchID

	/*JEL 2018-11-12 L&G have change to sending us a mapped RLID, not luli IDs. Defect 1157*/
	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, ResourceListID)
	SELECT @ClientID, i.MatterID, i.CDDTableRowID, 144350, rl.ResourceListID
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN ResourceList rl with (NOLOCK) on rl.ResourceListID = i.Policy_Section
	--INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.DetailFieldID = 146189 AND rldv.DetailValue = i.Policy_Section
	--INNER JOIN ResourceListDetailValues rldv_subsec WITH (NOLOCK) ON rldv_subsec.ResourceListID = rldv.ResourceListID AND rldv_subsec.DetailFieldID = 146190
	--	AND rldv_subsec.ValueInt = 74283 /*-*/
	WHERE i.CDDTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.CDDTableRowID, 144353, i.Claim_Detail_Data
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.CDDTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.CDDTableRowID, 146179, i.User_Deductions
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.CDDTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.CDDTableRowID, 145678, i.Settle
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.CDDTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.CDDTableRowID, 147001, i.No_Cover
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.CDDTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.CDDTableRowID, 146406, CONVERT(NUMERIC(18,2),i.Excess)
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.CDDTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.CDDTableRowID, 158802, i.Vol_Excess
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.CDDTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.CDDTableRowID, 147434, i.Rebate
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.CDDTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.CDDTableRowID, 146407, i.Co_Ins
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.CDDTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.CDDTableRowID, 146408, i.Limit
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.CDDTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.CDDTableRowID, 144352, i.Total
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.CDDTableRowID > 0
	AND i.BatchID = @BatchID
	
	/*GPR 2020-01-16 create DetailValues for 'Claim - Paid'*/
	EXEC dbo._C00_CreateDetailValues 147608, '', 100000

	/*GPR 2018-11-12 Defect 1171 - Populate Claims Paid value with the Total from Claim Detail
	  GPR 2018-11-15 Updated to Include Claim Amount*/
	INSERT INTO MatterDetailValues (ClientID, DetailFieldID, DetailValue, MatterID, LeadID)
	--SELECT @ClientID, 147608, i.Total_Claim_Amount + ' - ' + ISNULL(i.Total,'0.00'), i.MatterID, i.LeadID /*GPR 2020-01-16 adjusted to use Total_Claim_Amount in place of i.Claim_Detail_Data*/
	SELECT @ClientID, 147608, i.Total_Claim_Amount + ' - ' + ISNULL(i.Total_Claim_Amount,'0.00'), i.MatterID, i.LeadID /*GPR 2020-01-16 adjusted to use Total_Claim_Amount in place of i.Total - looks like we're given the settlement amount only*/
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.CDDTableRowID > 0
	--AND i.Total IS NOT NULL
	AND i.BatchID = @BatchID
		
	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.CDDTableRowID, 144362, i.Date_claim_approved
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.CDDTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.CDDTableRowID, 179564, i.CoinsurancePercentage
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.CDDTableRowID > 0
	AND i.BatchID = @BatchID
	
	/*JEL remove this for other clients. This is based on remaining limit actually being a running total of limit used*/ 

	--INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	--SELECT @ClientID, i.MatterID, i.CDDTableRowID, 179568, limit.ValueMoney - i.RemainingLimit
	--FROM [dbo]._C600_ImportClaims i 
	--INNER JOIN TableRows tr WITH (NOLOCK) on tr.MatterID = dbo.fn_C00_1272_GetPolicyFromClaim(i.MatterID)   and  tr.DetailFieldID = 145692
	--INNER JOIN TableDetailValues sec WITH (NOLOCK) on sec.TableRowID = tr.TableRowID  AND sec.DetailFieldID = 144357  AND i.Policy_Section = sec.ResourceListID 
	--INNER JOIN TableDetailValues limit with (NOLOCK) on limit.TableRowID = tr.TableRowID  AND limit.DetailFieldID = 144358 
	--WHERE i.CDDTableRowID > 0
	--AND i.BatchID = @BatchID */ 

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.CDDTableRowID, 179568, i.RemainingLimit
	FROM [dbo]._C600_ImportClaims i 
	WHERE i.CDDTableRowID > 0
	AND i.BatchID = @BatchID

		/*

	DECLARE @Limits TABLE (Scheme INT, Species INT, Sec INT, Limit NUMERIC(18,2))

	INSERT INTO @Limits (Scheme, Species, Sec, Limit)
	SELECT mdvscheme.ValueInt as [Scheme], mdvspecies.ValueInt AS [Species], sec.ResourceListID as [Sec], limit.ValueMoney as [Limit]
	FROM dbo.MatterDetailValues mdvScheme WITH (NOLOCK) 
	INNER JOIN MatterDetailValues mdvSpecies WITH ( NOLOCK ) on mdvSpecies.MatterID = mdvScheme.MatterID AND mdvSpecies.DetailFieldID = 177283 --and mdvSpecies.ValueInt = rldv.ValueInt /*Scheme Species*/ 
	INNER JOIN TableRows tr WITH (NOLOCK) on tr.MatterID = mdvSpecies.MatterID and  tr.DetailFieldID = 145692
	INNER JOIN TableDetailValues sec WITH (NOLOCK) on sec.TableRowID = tr.TableRowID  AND sec.DetailFieldID = 144357  --AND i.Policy_Section = sec.ResourceListID 
	INNER JOIN TableDetailValues limit with (NOLOCK) on limit.TableRowID = tr.TableRowID  AND limit.DetailFieldID = 144358 
	WHERE mdvScheme.DetailFieldID IN (145689) 


	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)	
	SELECT  @ClientID, i.MatterID, i.CDDTableRowID, 179568, limit.ValueMoney - i.RemainingLimit
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN LeadTypeRelationship ltr with (NOLOCK) on ltr.ToMatterID = i.MatterID
	INNER JOIN LeadDetailValues ldv with (NOLOCK) on ldv.LeadID = ltr.FromLeadID AND ldv.DetailFieldID = 144272 /*Breed*/ 
	INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = ldv.ValueInt AND rldv.DetailFieldID = 144269 /*Breed Species*/ 
	INNER JOIN MatterDetailValues mdv with (NOLOCK) on mdv.LeadID = ldv.LeadID AND mdv.DetailFieldID = 170034 /*Policy Scheme*/ 
	INNER JOIN dbo.MatterDetailValues mdvScheme WITH (NOLOCK) ON mdvScheme.DetailFieldID IN (145689) AND mdvScheme.ValueInt = mdv.ValueINT 
	INNER JOIN MatterDetailValues mdvSpecies WITH ( NOLOCK ) on mdvSpecies.MatterID = mdvScheme.MatterID AND mdvSpecies.DetailFieldID = 177283 and mdvSpecies.ValueInt = rldv.ValueInt /*Scheme Species*/ 
	INNER JOIN TableRows tr WITH (NOLOCK) on tr.MatterID = mdvSpecies.MatterID and  tr.DetailFieldID = 145692
	INNER JOIN TableDetailValues sec WITH (NOLOCK) on sec.TableRowID = tr.TableRowID  AND sec.DetailFieldID = 144357  AND i.Policy_Section = sec.ResourceListID 
	INNER JOIN TableDetailValues limit with (NOLOCK) on limit.TableRowID = tr.TableRowID  AND limit.DetailFieldID = 144358 
	WHERE i.CDDTableRowID > 0
	AND i.BatchID =  @BatchID 


	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)	
	SELECT  @ClientID, i.MatterID, i.CDDTableRowID, 179568, s.Limit - CASE WHEN dbo.fnIsMoney(i.RemainingLimit) = 1 AND i.RemainingLimit <> '' THEN CONVERT(NUMERIC(18,2), ISNULL(i.RemainingLimit, 0.00)) ELSE 0.00 END
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN LeadTypeRelationship ltr with (NOLOCK) on ltr.ToMatterID = i.MatterID
	INNER JOIN LeadDetailValues ldv with (NOLOCK) on ldv.LeadID = ltr.FromLeadID AND ldv.DetailFieldID = 144272 /*Breed*/ 
	INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = ldv.ValueInt AND rldv.DetailFieldID = 144269 /*Breed Species*/ 
	INNER JOIN MatterDetailValues mdv with (NOLOCK) on mdv.LeadID = ldv.LeadID AND mdv.DetailFieldID = 170034 /*Policy Scheme*/ 
	INNER JOIN @Limits s ON s.Scheme =  mdv.ValueINT AND s.Species = rldv.ValueInt AND i.Policy_Section = s.sec
	WHERE i.CDDTableRowID > 0
	AND i.BatchID =  @BatchID 	*/

	/*Date_claim_received*/
	EXEC dbo._C00_CreateDetailValues 179760, '', 100000
	
	UPDATE mdv
	SET DetailValue = Date_claim_received
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 179760
	AND mdv.DetailValue <> i.Date_claim_received
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Date_claim_received', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Date_claim_form_sent*/
	EXEC dbo._C00_CreateDetailValues 144519, '', 100000
	
	UPDATE mdv
	SET DetailValue = Date_claim_form_sent
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 144519
	AND mdv.DetailValue <> i.Date_claim_form_sent
	AND i.BatchID = @BatchID
	
	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Date_claim_form_sent', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Date_Third_Party_Claim_Form_Sent*/
	EXEC dbo._C00_CreateDetailValues 179751, '', 100000
	
	UPDATE mdv
	SET DetailValue = Date_Third_Party_Claim_Form_Sent
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 179751
	AND mdv.DetailValue <> i.Date_Third_Party_Claim_Form_Sent
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Date_Third_Party_Claim_Form_Sent', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Date_claim_form_received*/
	EXEC dbo._C00_CreateDetailValues 144520, '', 100000
	
	UPDATE mdv
	SET DetailValue = Date_claim_form_received
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 144520
	AND mdv.DetailValue <> i.Date_claim_form_received
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Date_claim_form_received', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Date_claim_details_entered*/
	EXEC dbo._C00_CreateDetailValues 153017, '', 100000
	
	UPDATE mdv
	SET DetailValue = Date_claim_details_entered
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 153017
	AND mdv.DetailValue <> i.Date_claim_details_entered
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Date_claim_details_entered', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Date_missing_info_requested*/
	EXEC dbo._C00_CreateDetailValues 144522, '', 100000
	
	UPDATE mdv
	SET DetailValue = Date_missing_info_requested
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 144522
	AND mdv.DetailValue <> i.Date_missing_info_requested
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Date_missing_info_requested', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Date_missing_info_received*/
	EXEC dbo._C00_CreateDetailValues 144523, '', 100000
	
	UPDATE mdv
	SET DetailValue = Date_missing_info_received
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 144523
	AND mdv.DetailValue <> i.Date_missing_info_received
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Date_missing_info_received', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Date_claim_approved*/
	EXEC dbo._C00_CreateDetailValues 144524, '', 100000
	
	UPDATE mdv
	SET DetailValue = Date_claim_approved
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 144524
	AND mdv.DetailValue <> i.Date_claim_approved
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Date_claim_approved', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Date_claim_rejected*/
	EXEC dbo._C00_CreateDetailValues 147609, '', 100000
	
	UPDATE mdv
	SET DetailValue = Date_claim_rejected
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 147609
	AND mdv.DetailValue <> i.Date_claim_rejected
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Date_claim_rejected', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Date_payment_revision_requested*/
	EXEC dbo._C00_CreateDetailValues 158562, '', 100000
	
	UPDATE mdv
	SET DetailValue = Date_payment_revision_requested
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 158562
	AND mdv.DetailValue <> i.Date_payment_revision_requested
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Date_payment_revision_requested', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Date_claim_reapproved*/
	EXEC dbo._C00_CreateDetailValues 144485, '', 100000
	
	UPDATE mdv
	SET DetailValue = Date_claim_reapproved
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 144485
	AND mdv.DetailValue <> i.Date_claim_reapproved
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Date_claim_reapproved', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Date_first_settled_approved_or_rejected*/
	EXEC dbo._C00_CreateDetailValues 144526, '', 100000
	
	UPDATE mdv
	SET DetailValue = Date_first_settled_approved_or_rejected
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 144526
	AND mdv.DetailValue <> i.Date_first_settled_approved_or_rejected
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Date_first_settled_approved_or_rejected', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Date_reopened*/
	EXEC dbo._C00_CreateDetailValues 159562, '', 100000
	
	UPDATE mdv
	SET DetailValue = Date_reopened
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 159562
	AND mdv.DetailValue <> i.Date_reopened
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Date_reopened', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Date_payment_confirmed*/
	EXEC dbo._C00_CreateDetailValues 162644, '', 100000
	
	UPDATE mdv
	SET DetailValue = Date_payment_confirmed
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportClaims i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 162644
	AND mdv.DetailValue <> i.Date_payment_confirmed
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Updated Date_payment_confirmed', @@ROWCOUNT, CURRENT_TIMESTAMP)
	
	/*
		Parent child claims
		1. Set the master based on previous imports
		2. If not set then set the master as the first claim orderd by date of loss
		3. Insert the claim relationships for the group of claims
	*/

	UPDATE c
	SET IsMasterClaim = c_prevImport.IsMasterClaim
	FROM _C600_ImportClaims c
	INNER JOIN _C600_ImportClaims c_prevImport ON c_prevImport.ClaimID = c.ClaimID AND c_prevImport.IsMasterClaim IS NOT NULL AND c_prevImport.ImportedClaimID < c.ImportedClaimID
	WHERE c.IsMasterClaim IS NULL
	AND c.BatchID = @BatchID

	;
	WITH ParentClaims AS (
		SELECT *, ROW_NUMBER() OVER (PARTITION BY c.ClaimParentID ORDER BY c.Date_Of_Loss) AS [RN]
		FROM [dbo].[_C600_ImportClaims] c
		WHERE c.BatchID = @BatchID
	)

	UPDATE p
	SET IsMasterClaim = CASE p.RN WHEN 1 THEN 1 ELSE 0 END
	FROM ParentClaims p
	WHERE p.IsMasterClaim IS NULL

	/*Any missing claim relationships*/
	INSERT INTO TableRows (ClientID, LeadID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, SourceID)
	OUTPUT inserted.SourceID, inserted.TableRowID INTO #ImportedClaimRelationshipTableRowIDs (ImportedClaimID, TableRowID)
	SELECT  DISTINCT 603, c_child.LeadID, 146357, 16766, 1, 1, c_child.ImportedClaimID
	FROM _C600_ImportClaims c_child
	INNER JOIN _C600_ImportClaims c_parent ON c_child.ClaimParentID = c_parent.ClaimParentID 
	WHERE c_child.BatchID = @BatchID
	AND c_parent.IsMasterClaim = 1
	AND c_child.IsMasterClaim = 0
	AND NOT EXISTS (
		SELECT *
		FROM TableRows tr WITH (nolock) 
		INNER JOIN TableDetailValues tdv_claimID WITH (NOLOCK) ON tr.TableRowID = tdv_claimID.TableRowID
		INNER JOIN TableDetailValues tdv_parentClaimID WITH (NOLOCK) ON tdv_parentClaimID.TableRowID = tr.TableRowID AND tdv_parentClaimID.DetailFieldID = 146356
		WHERE tr.LeadID = c_parent.LeadID
		AND tdv_claimID.DetailFieldID = 146355
		AND tdv_claimID.ValueInt = c_parent.MatterID
		AND tdv_parentClaimID.ValueInt = c_Parent.MatterID
	)

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Claim Relationship TableRows', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*Claim*/
	INSERT INTO TableDetailValues (ClientID, LeadID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, c_child.LeadID, i.TableRowID, 146355, c_child.MatterID
	FROM _C600_ImportClaims c_child
	INNER JOIN _C600_ImportClaims c_parent ON c_child.ClaimParentID = c_parent.ClaimParentID 
	INNER JOIN #ImportedClaimRelationshipTableRowIDs i ON i.ImportedClaimID = c_child.ImportedClaimID
	WHERE c_child.BatchID = @BatchID
	AND c_parent.IsMasterClaim = 1
	AND c_child.IsMasterClaim = 0

	/*Parent*/
	INSERT INTO TableDetailValues (ClientID, LeadID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, c_child.LeadID, i.TableRowID, 146356, c_Parent.MatterID
	FROM _C600_ImportClaims c_child
	INNER JOIN _C600_ImportClaims c_parent ON c_child.ClaimParentID = c_parent.ClaimParentID 
	INNER JOIN #ImportedClaimRelationshipTableRowIDs i ON i.ImportedClaimID = c_child.ImportedClaimID
	WHERE c_child.BatchID = @BatchID
	AND c_parent.IsMasterClaim = 1
	AND c_child.IsMasterClaim = 0

	INSERT INTO PurchasedProduct ([ClientID], [CustomerID], [AccountID], [PaymentFrequencyID], [NumberOfInstallments], [ProductPurchasedOnDate], [ValidFrom], [ValidTo], [PreferredPaymentDay], [FirstPaymentDate], [RemainderUpFront], [ProductName], [ProductDescription], [ProductCostNet], [ProductCostVAT], [ProductCostGross], [ProductCostCalculatedWithRuleSetID], [ProductCostCalculatedOn], [ProductCostBreakdown], [ObjectID], [ObjectTypeID], [WhoCreated], [WhenCreated], [WhoModified], [WhenModified], [PaymentScheduleSuccessfullyCreated], [PaymentScheduleCreatedOn], [PaymentScheduleFailedDataLoaderLogID], [PremiumCalculationDetailID], [ProductAdditionalFee], [ClientAccountID], [LegacyRef])
	output inserted.PurchasedProductID, inserted.LegacyRef INTO #ImportedPP (PurchasedProductID, ImportedPetID)
	SELECT @ClientID, i.CustomerID, ia.AccountID, CASE Preferred_Collection_Date
		WHEN 0 THEN 5 /*Annually*/
		ELSE 4 /*Monthly*/
		END,
		CASE Preferred_Collection_Date
		WHEN 0 THEN 1 /*Annually*/
		ELSE 12 /*Monthly*/
		END, i.[SalesDate], i.[Start],  DATEADD(Day,-1, DATEADD(Year,1,i.[Start])),
		Preferred_Collection_Date AS [PreferredPaymentDay], 
		i.[From] AS [FirstPaymentDate], 
		1 AS [RemainderUpFront], 
		rldv.DetailValue AS [ProductName], 
		p.AnimalName + ' (Policy No: ' + i.PolicyID + ')' AS [ProductDescription], 
		CONVERT(NUMERIC(18,2),i.Previous_Annual) - CONVERT(NUMERIC(18,2),i.Previous_Annual_IPT) AS [ProductCostNet], 
		CONVERT(NUMERIC(18,2),i.Previous_Annual_IPT) AS [ProductCostVAT], 
		CONVERT(NUMERIC(18,2),i.Previous_Annual) AS [ProductCostGross], 
		NULL AS [ProductCostCalculatedWithRuleSetID], 
		NULL AS [ProductCostCalculatedOn], 
		NULL AS [ProductCostBreakdown], 
		i.MatterID AS [ObjectID], 
		2 AS [ObjectTypeID], 
		58552 AS [WhoCreated], 
		dbo.fn_GetDate_Local() AS [WhenCreated], 
		58552 AS [WhoModified], 
		dbo.fn_GetDate_Local() AS [WhenModified], 
		0 AS [PaymentScheduleSuccessfullyCreated], 
		NULL AS [PaymentScheduleCreatedOn], 
		NULL AS [PaymentScheduleFailedDataLoaderLogID], 
		NULL AS [PremiumCalculationDetailID], 
		NULL AS [ProductAdditionalFee], 
		1 AS [ClientAccountID], 
		i.ImportedPetID [LegacyRef]
	FROM _C600_ImportPolicy i
	INNER JOIN #ImportedAccounts ia ON ia.CollectionsMatterID = i.CollectionsMatterID
	INNER JOIN _C600_ImportPetData p on p.MatterID = i.MatterID 
	INNER JOIN ResourceListDetailValues rldv on rldv.ResourceListID = i.Product 
	WHERE p.BatchID = @BatchID 
	AND rldv.DetailFieldID = 146200

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('InsertedPP', @@ROWCOUNT, CURRENT_TIMESTAMP)

	UPDATE i
	SET PurchasedProductID = p.PurchasedProductID, ImportDate = dbo.fn_GetDate_Local()
	FROM _C600_ImportPolicy i
	INNER JOIN #ImportedPP p ON p.ImportedPetID = i.ImportedPetID

	/*PurchasedProductID*/
	EXEC dbo._C00_CreateDetailValues 177074, '', 100000

	UPDATE mdv
	SET DetailValue = PurchasedProductID
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 177074
	AND mdv.ValueInt <> i.PurchasedProductID
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('PurchasedProductIDs', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*
		We need to do the historical products as well. Cant do them above so will do them here..
	*/
	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, i.HPTableRowID, 177419, i.PurchasedProductID
	FROM [dbo].[_C600_ImportPolicy] i 
	WHERE i.HPTableRowID > 0
	AND i.BatchID = @BatchID

	INSERT INTO TableDetailValues (ClientID, MatterID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, i.MatterID, h.TableRowID, 177419, i.PurchasedProductID
	FROM [dbo].[_C600_ImportPolicy] i 
	INNER JOIN #ImportedHistoricalHPTableRowIDs h ON h.MatterID = i.MatterID
	WHERE h.TableRowID > 0
	AND i.BatchID = @BatchID

	/*
		Disable Auto-Renew - If card payer then disable auto renew
	*/
	EXEC dbo._C00_CreateDetailValues 180067, '', 100000

	UPDATE mdv
	SET DetailValue = CASE Preferred_Collection_Date
		WHEN 0 THEN 'true' /*Credit Card*/
		ELSE 'false' /*BACS*/
		END
	OUTPUT inserted.MatterID, inserted.DetailFieldID, inserted.DetailValue, 2 INTO #FieldHistory (MatterID, DetailFieldID, DetailValue, LeadORMatter)
	FROM [dbo]._C600_ImportPolicy i 
	INNER JOIN MatterDetailValues mdv ON mdv.MatterID = i.MatterID
	WHERE mdv.DetailFieldID = 180067
	AND mdv.ValueInt <> CASE Preferred_Collection_Date
		WHEN 0 THEN 1 /*Credit Card*/
		ELSE 0 /*BACS*/
		END
	AND i.BatchID = @BatchID

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('Diable AutoRenew', @@ROWCOUNT, CURRENT_TIMESTAMP)

	/*History*/
	INSERT INTO DetailValueHistory (ClientID, CustomerID, LeadID, MatterID, LeadOrMatter, DetailFieldID, FieldValue, ClientPersonnelID, WhenSaved)
	SELECT @ClientID, f.CustomerID, f.LeadID, f.MatterID, f.LeadORMatter, f.DetailFieldID, f.DetailValue, 58552, dbo.fn_GetDate_Local()
	FROM #FieldHistory f

	INSERT INTO #InsertedObjects (TableName, RowCounter, TimeStamp)
	VALUES ('DetailValueHistory', @@ROWCOUNT, CURRENT_TIMESTAMP)

	SELECT *
	FROM _C600_ImportPolicy p
	WHERE p.BatchID = @BatchID

	UPDATE AutomatedTask
	SET NextRunDateTime = dbo.fn_GetDate_Local()
	WHERE TaskID = 22890

	UPDATE ib
	SET EndDateTime = CURRENT_TIMESTAMP, TimeTaken = DATEDIFF(MS, StartDateTime, CURRENT_TIMESTAMP)
	FROM _C600_ImportBatch ib 
	WHERE ib.ImportBatchID = @BatchID

	SELECT *
	FROM #InsertedObjects as iob

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_FullImport] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_FullImport] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_FullImport] TO [sp_executeall]
GO
