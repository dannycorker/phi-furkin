SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the CustomerQuestionnaires table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerQuestionnaires_Update]
(

	@CustomerQuestionnaireID int   ,

	@ClientQuestionnaireID int   ,

	@CustomerID int   ,

	@SubmissionDate datetime   ,

	@TrackingID int   ,

	@Referrer varchar (512)  ,

	@SearchTerms varchar (512)  ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[CustomerQuestionnaires]
				SET
					[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[CustomerID] = @CustomerID
					,[SubmissionDate] = @SubmissionDate
					,[TrackingID] = @TrackingID
					,[Referrer] = @Referrer
					,[SearchTerms] = @SearchTerms
					,[ClientID] = @ClientID
				WHERE
[CustomerQuestionnaireID] = @CustomerQuestionnaireID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerQuestionnaires_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerQuestionnaires_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerQuestionnaires_Update] TO [sp_executeall]
GO
