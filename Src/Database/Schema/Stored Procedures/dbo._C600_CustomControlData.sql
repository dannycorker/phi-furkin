SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Robin Hall
-- Create date: 2015-02-05
-- Description:	Generic proc to return table of stuff for a custom detail field.
-- Used by DetailFieldID 175475 --- Policy section waiting periods
-- CPS 2017-06-06 Replace Waiting Periods check with Function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_CustomControlData]
	@DetailFieldID INT,
	@CustomerID INT = NULL,
	@LeadID INT = NULL,
	@CaseID INT = NULL,
	@MatterID INT = NULL
AS
BEGIN

	-- Policy section waiting periods
	IF @DetailFieldID = 175475
	BEGIN
	
		-- Return table of policy section waiting periods	
		SELECT	 fn.PolicySectionDisplay	[Policy section]
				,fn.WaitingPeriodNarrative	[Waiting period]
		FROM dbo.fn_C600_Claims_GetWaitPeriodsForClaim(@MatterID) fn

	END
	

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CustomControlData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_CustomControlData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CustomControlData] TO [sp_executeall]
GO
