SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientOffices table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOffices_Delete]
(

	@ClientOfficeID int   
)
AS


				DELETE FROM [dbo].[ClientOffices] WITH (ROWLOCK) 
				WHERE
					[ClientOfficeID] = @ClientOfficeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOffices_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOffices_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOffices_Delete] TO [sp_executeall]
GO
