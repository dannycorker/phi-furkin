SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SMSOutgoingPhoneNumber table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSOutgoingPhoneNumber_GetBySMSQuestionnaireID]
(

	@SMSQuestionnaireID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SMSOutgoingPhoneNumberID],
					[ClientID],
					[SMSQuestionnaireID],
					[LeadTypeID],
					[OutgoingPhoneNumber],
					[SMSSurveyStartEventTypeID],
					[MMSLetterInEvent],
					[SmsGatewayID],
					[HasIntroductionText],
					[IntroductionTextFrom],
					[IntroductionText],
					[DelayInSeconds],
					[IntroductionTextEventTypeID],
					[IntroductionTextDocumentTypeID],
					[NoCLIRejectText]
				FROM
					[dbo].[SMSOutgoingPhoneNumber] WITH (NOLOCK) 
				WHERE
					[SMSQuestionnaireID] = @SMSQuestionnaireID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber_GetBySMSQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSOutgoingPhoneNumber_GetBySMSQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber_GetBySMSQuestionnaireID] TO [sp_executeall]
GO
