SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2014-09-24
-- Description:	Get Quote Values
-- 27/11/2014   Jan Wilson
--              Updated to round the Annual Premium Value
--              to 2 decimal places.
-- 2014/12/17 DCM added extra columns to @Premiums
-- 2015-01-26 DCM added pet number
-- 2015-01-29 DCM updated policies list lookup
-- 2015-03-12 SB  Added quote logging 
-- 2015-03-20 SB  Updated logging
--	2015-05-14 DCM Ticket #31733 Entire contents moved to new proc to facilitate renaming in prelive
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Policy_GetQuoteValues]
(
	@XmlRequest XML
)
AS
BEGIN

	EXEC _C600_PA_Policy_GetQuoteValues @XmlRequest
		
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_GetQuoteValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Policy_GetQuoteValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_GetQuoteValues] TO [sp_executeall]
GO
