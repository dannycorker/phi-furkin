SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Robin Hall
-- Create date: 2013-06-27
-- Description:	Confirms or aborts pending unconfirmed cancellations and reissued payments
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Payments_ConfirmOrAbortCancellation] 
(
	@CaseID INT
	,@Action INT -- 1=Confirm, 2=Abort
)
AS
BEGIN

	DECLARE @ClientID INT
		,@LeadID INT
		,@MatterID INT
		
	SELECT TOP 1 @ClientID = m.ClientID, @LeadID = m.LeadID, @MatterID = m.MatterID
	FROM Matter m WHERE m.CaseID = @CaseID	

	DECLARE @LeadEventTvp tvpLeadEvent
	
	DECLARE @AutomationUser INT
	SELECT @AutomationUser = dbo.fn_C00_GetAutomationUser(@ClientID)
	
	IF @Action = 1
	BEGIN
	
		DECLARE @CancellationRows TABLE (CancellationRowID INT)

		INSERT @CancellationRows (CancellationRowID)	
		SELECT r.TableRowID
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvPaymentType WITH (NOLOCK) ON r.TableRowID = tdvPaymentType.TableRowID AND tdvPaymentType.DetailFieldID = 154517
		INNER JOIN dbo.TableDetailValues tdvOriginalReference WITH (NOLOCK) ON r.TableRowID = tdvOriginalReference.TableRowID AND tdvOriginalReference.DetailFieldID = 159479
		LEFT JOIN dbo.TableDetailValues tdvSAPResponse WITH (NOLOCK) ON r.TableRowID = tdvSAPResponse.TableRowID AND tdvSAPResponse.DetailFieldID = 158539
		WHERE r.MatterID = @MatterID
		AND tdvPaymentType.DetailValue = '2' -- Cancellation
		AND tdvSAPResponse.ValueDate IS NULL
		
		-- Write SAP sent and response date to Cancellation row(s)
		INSERT dbo.TableDetailValues (TableRowID, DetailFieldID, MatterID, ClientID, DetailValue)
		SELECT CancellationRowID, 154521, @MatterID, @ClientID, CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
		FROM @CancellationRows

		INSERT dbo.TableDetailValues (TableRowID, DetailFieldID, MatterID, ClientID, DetailValue)
		SELECT CancellationRowID, 158539, @MatterID, @ClientID, CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
		FROM @CancellationRows

		-- Write result to Cancellation row(s)		
		INSERT dbo.TableDetailValues (TableRowID, DetailFieldID, MatterID, ClientID, DetailValue)
		SELECT CancellationRowID, 158540, @MatterID, @ClientID, 'Confirmed'
		FROM @CancellationRows
		
		-- Raise SAP Cancellation confirmed event
		INSERT @LeadEventTvp (ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, FollowupDateTime, CaseID, Eventdeleted)
		SELECT @ClientID, @LeadID, dbo.fn_GetDate_Local(), @AutomationUser, 0.00, 'Created by _C00_1272_Payments_ConfirmOrAbortCancellation', 142098, NULL, @CaseID, 0

		EXEC _C00_ApplyLeadEvent @LeadEventTvp
	
	END
	ELSE IF @Action = 2
	BEGIN
	
		DELETE r
		FROM dbo.TableRows r
		LEFT JOIN dbo.TableDetailValues tdvSent WITH (NOLOCK) ON r.TableRowID = tdvSent.TableRowID AND tdvSent.DetailFieldID = 154521
		WHERE r.DetailFieldID = 154485
		AND r.DetailFieldPageID = 17324
		AND r.MatterID = @MatterID
		AND r.ClientID = @ClientID
		AND tdvSent.ValueDate IS NULL
	
		-- Raise Unsent payment rows aborted event
		INSERT @LeadEventTvp (ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, FollowupDateTime, CaseID, Eventdeleted)
		SELECT @ClientID, @LeadID, dbo.fn_GetDate_Local(), @AutomationUser, 0.00, 'Created by _C00_1272_Payments_ConfirmOrAbortCancellation', 142107, NULL, @CaseID, 0

		EXEC _C00_ApplyLeadEvent @LeadEventTvp

	END


END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Payments_ConfirmOrAbortCancellation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Payments_ConfirmOrAbortCancellation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Payments_ConfirmOrAbortCancellation] TO [sp_executeall]
GO
