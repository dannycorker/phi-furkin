SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By:  ()
-- Purpose: Select records from the DetailFieldAlias table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldAlias__GetResourceFieldAliases]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[DetailFieldAliasID],
					[ClientID],
					[LeadTypeID],
					[DetailFieldID],
					[DetailFieldAlias]
				FROM
					[dbo].[DetailFieldAlias]
				WHERE
					[ClientID] = @ClientID
					AND LeadTypeID is null
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias__GetResourceFieldAliases] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldAlias__GetResourceFieldAliases] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias__GetResourceFieldAliases] TO [sp_executeall]
GO
