SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 2012-12-10
-- Description:	Gets the user credential by the third party system id and the client personnel id
-- includes the ThirdPartyClientID
-- =============================================
CREATE PROCEDURE [dbo].[ThirdPartyMappingUserCredential__GetByUserID]
(

	@ThirdPartySystemID int,
	@UserID int
	
)
AS
BEGIN

		SELECT *
		FROM
			[dbo].[ThirdPartyMappingUserCredential]
		WHERE
			[ThirdPartySystemID] = @ThirdPartySystemID AND
			[UserID] = @UserID
			
		SELECT @@ROWCOUNT

END





GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential__GetByUserID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingUserCredential__GetByUserID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingUserCredential__GetByUserID] TO [sp_executeall]
GO
