SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ObjectLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectLink_Insert]
(

	@ObjectLinkID int    OUTPUT,

	@FromObjectID int   ,

	@ToObjectID int   ,

	@ObjectTypeRelationshipID int   
)
AS


				
				INSERT INTO [dbo].[ObjectLink]
					(
					[FromObjectID]
					,[ToObjectID]
					,[ObjectTypeRelationshipID]
					)
				VALUES
					(
					@FromObjectID
					,@ToObjectID
					,@ObjectTypeRelationshipID
					)
				-- Get the identity value
				SET @ObjectLinkID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectLink_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectLink_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectLink_Insert] TO [sp_executeall]
GO
