SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ThirdPartyFieldGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldGroup_Delete]
(

	@ThirdPartyFieldGroupID int   
)
AS


				DELETE FROM [dbo].[ThirdPartyFieldGroup] WITH (ROWLOCK) 
				WHERE
					[ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldGroup_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldGroup_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldGroup_Delete] TO [sp_executeall]
GO
