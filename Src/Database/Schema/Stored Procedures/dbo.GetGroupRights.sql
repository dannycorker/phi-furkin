SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-24
-- Description:	Get all rights for this Group
-- =============================================
CREATE PROCEDURE [dbo].[GetGroupRights]
	-- Add the parameters for the stored procedure here
	@GroupID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select functiontypeid, leadtypeid, objectid, rightid 
	from grouprightsdynamic 
	where clientpersonneladmingroupid = @GroupID

END







GO
GRANT VIEW DEFINITION ON  [dbo].[GetGroupRights] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetGroupRights] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetGroupRights] TO [sp_executeall]
GO
