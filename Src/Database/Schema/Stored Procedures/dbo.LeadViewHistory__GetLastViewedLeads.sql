SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-02-2015
-- Description:	Gets the last 10 recently view leads for the outlook addin
-- The outlook addin relies on the columns returned from this stored procedure being 
-- in the correct order, with the correct names. You may add columns but not remove or reorder them.
-- =============================================
CREATE PROCEDURE [dbo].[LeadViewHistory__GetLastViewedLeads]
	@ClientPersonnelID INT
AS
BEGIN

	SET NOCOUNT ON;


	SELECT TOP 10 c.CustomerID, c.Fullname, c.Address1, c.PostCode, c.MobileTelephone, c.CompanyName,
		   l.LeadID, l.LeadTypeID, lt.LeadTypeName, l.LeadRef,
		   m.MatterID
	FROM LeadViewHistory lvh WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=lvh.LeadID
	INNER JOIN LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID
	INNER JOIN Matter m WITH (NOLOCK) ON m.LeadID = lvh.LeadID
	INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID
	WHERE ClientPersonnelID=@ClientPersonnelID
	ORDER BY WhenViewed DESC

END


GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory__GetLastViewedLeads] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadViewHistory__GetLastViewedLeads] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory__GetLastViewedLeads] TO [sp_executeall]
GO
