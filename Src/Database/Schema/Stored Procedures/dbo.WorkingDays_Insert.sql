SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the WorkingDays table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkingDays_Insert]
(

	@WorkingDayID int    OUTPUT,

	@Year int   ,

	@Month int   ,

	@Day int   ,

	@DayNumber int   ,

	@IsWorkDay bit   ,

	@IsWeekDay bit   ,

	@IsBankHoliday bit   ,

	@Date datetime   ,

	@WeekNumber tinyint   ,

	@DayNumberOfWeek tinyint   ,

	@CharDate char (10)  
)
AS


				
				INSERT INTO [dbo].[WorkingDays]
					(
					[Year]
					,[Month]
					,[Day]
					,[DayNumber]
					,[IsWorkDay]
					,[IsWeekDay]
					,[IsBankHoliday]
					,[Date]
					,[WeekNumber]
					,[DayNumberOfWeek]
					,[CharDate]
					)
				VALUES
					(
					@Year
					,@Month
					,@Day
					,@DayNumber
					,@IsWorkDay
					,@IsWeekDay
					,@IsBankHoliday
					,@Date
					,@WeekNumber
					,@DayNumberOfWeek
					,@CharDate
					)
				-- Get the identity value
				SET @WorkingDayID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkingDays_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkingDays_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkingDays_Insert] TO [sp_executeall]
GO
