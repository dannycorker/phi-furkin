SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 27/07/2017
-- Description:	Insert into WorldPay Request
-- =============================================
CREATE PROCEDURE [dbo].[WorldPayRequest__Insert]

	@ClientID INT,
	@CustomerID INT,
	@MatterID INT,
	@RequestXml VARCHAR(MAX),
	@WhenCreated DATETIME,
    @WhoCreated INT

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @XML XML = CONVERT (XML, @RequestXml,2)
	
	INSERT INTO WorldPayRequest ([ClientID],[CustomerID],[MatterID],[RequestXml],[WhenCreated],[WhoCreated])
    VALUES (@ClientID, @CustomerID, @MatterID, @RequestXml, @WhenCreated, @WhoCreated)
     
    SELECT SCOPE_IDENTITY() AS WorldPayRequestID
     
END
GO
GRANT VIEW DEFINITION ON  [dbo].[WorldPayRequest__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorldPayRequest__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorldPayRequest__Insert] TO [sp_executeall]
GO
