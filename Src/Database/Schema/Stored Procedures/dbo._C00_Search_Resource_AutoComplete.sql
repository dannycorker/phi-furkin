SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2019-07-09
-- Description:	Return data from resource list filter
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Search_Resource_AutoComplete]
	@SearchColumn INT,
	@AdditionalColumn INT,
	@SearchValue VARCHAR(2000),
	@BracketColumn INT = NULL
AS
BEGIN

	SET NOCOUNT ON;

	SELECT @SearchValue = REPLACE(@SearchValue, ' ', '%')

	SELECT rldv_srch.ResourceListID, rldv_srch.DetailValue + ISNULL(' ' + rldv_add.DetailValue, '') + CASE WHEN @BracketColumn > 0 THEN ' (' + luli.ItemValue + ')' ELSE '' END AS [SearchValue]
	FROM ResourceListDetailValues rldv_srch WITH (NOLOCK)
	LEFT JOIN ResourceListDetailValues rldv_add WITH (NOLOCK) ON rldv_add.ResourceListID = rldv_srch.ResourceListID AND rldv_add.DetailFieldID = @AdditionalColumn
	LEFT JOIN ResourceListDetailValues rldv_brCol WITH (NOLOCK) ON rldv_brCol.ResourceListID = rldv_srch.ResourceListID AND rldv_brCol.DetailFieldID = @BracketColumn
	LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = rldv_brCol.ValueInt
	WHERE rldv_srch.DetailFieldID = @SearchColumn
	AND (rldv_srch.DetailValue + ' ' + rldv_add.DetailValue) LIKE '%' + @SearchValue + '%'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Search_Resource_AutoComplete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Search_Resource_AutoComplete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Search_Resource_AutoComplete] TO [sp_executeall]
GO
