SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ScriptLock table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptLock_Insert]
(

	@ScriptLockID int    OUTPUT,

	@ScriptID int   ,

	@ClientID int   ,

	@WhoIsEditing int   ,

	@EditStartedAt datetime   
)
AS


				
				INSERT INTO [dbo].[ScriptLock]
					(
					[ScriptID]
					,[ClientID]
					,[WhoIsEditing]
					,[EditStartedAt]
					)
				VALUES
					(
					@ScriptID
					,@ClientID
					,@WhoIsEditing
					,@EditStartedAt
					)
				-- Get the identity value
				SET @ScriptLockID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptLock_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptLock_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptLock_Insert] TO [sp_executeall]
GO
