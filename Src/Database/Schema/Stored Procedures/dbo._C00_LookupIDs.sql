SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-07-12
-- Description:	Looks up a single set of IDs based on what is passed in
-- JWG 2014-05-13 #26507 LatestRecordsFirst 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_LookupIDs]
(
	@ClientID INT,
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@UserID INT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @MatchedLeadID INT = 0, 
	@LatestRecordsFirst BIT = 0
	
	/* JWG 2014-05-13 #26507 LatestRecordsFirst */
	SELECT TOP 1 @LatestRecordsFirst = ISNULL(cl.LatestRecordsFirst, 0)
	FROM dbo.ClientOption cl WITH (NOLOCK) 
	WHERE cl.ClientID = @ClientID
	
	IF @MatterID > 0
	BEGIN
		
		SELECT @MatchedLeadID = l.LeadID
		FROM dbo.Lead l WITH (NOLOCK) 
		INNER JOIN dbo.Cases c WITH (NOLOCK) ON l.LeadID = c.LeadID 
		INNER JOIN dbo.Matter m WITH (NOLOCK) ON c.CaseID = m.CaseID 
		WHERE m.MatterID = @MatterID
		AND m.ClientID = @ClientID
		
		SELECT m.MatterID, c.CaseID, l.LeadID, l.CustomerID
		FROM dbo.Lead l WITH (NOLOCK) 
		INNER JOIN dbo.Cases c WITH (NOLOCK) ON l.LeadID = c.LeadID 
		INNER JOIN dbo.fnGetUserLeadCaseAccess(@UserID, @MatchedLeadID) ca ON c.CaseID = ca.CaseID
		INNER JOIN dbo.Matter m WITH (NOLOCK) ON c.CaseID = m.CaseID 
		WHERE m.MatterID = @MatterID
		AND m.ClientID = @ClientID
			
	END
	ELSE IF @CaseID > 0
	BEGIN

		SELECT @MatchedLeadID = l.LeadID
		FROM dbo.Lead l WITH (NOLOCK) 
		INNER JOIN dbo.Cases c WITH (NOLOCK) ON l.LeadID = c.LeadID 
		WHERE c.CaseID = @CaseID
		AND c.ClientID = @ClientID
		
		SELECT TOP 1 m.MatterID, c.CaseID, l.LeadID, l.CustomerID
		FROM dbo.Lead l WITH (NOLOCK) 
		INNER JOIN dbo.Cases c WITH (NOLOCK) ON l.LeadID = c.LeadID
		INNER JOIN dbo.fnGetUserLeadCaseAccess(@UserID, @MatchedLeadID) ca ON c.CaseID = ca.CaseID 
		INNER JOIN dbo.Matter m WITH (NOLOCK) ON c.CaseID = m.CaseID 
		WHERE c.CaseID = @CaseID
		AND c.ClientID = @ClientID
		/*ORDER BY m.MatterID*/
		ORDER BY 
			/*
				JWG 2014-05-13 #26507 LatestRecordsFirst
				I apologise for this horrible syntax. 
				The dynamic part is really the ASC/DESC rather than the column to order by, but this is the only way to do it at present.
			*/
			CASE @LatestRecordsFirst WHEN 1 THEN m.MatterID END DESC, 
			CASE @LatestRecordsFirst WHEN 0 THEN m.MatterID END ASC 

	END
	ELSE IF @LeadID > 0
	BEGIN

		SELECT @MatchedLeadID = l.LeadID
		FROM dbo.Lead l WITH (NOLOCK) 
		WHERE l.LeadID = @LeadID
		AND l.ClientID = @ClientID
		
		SELECT TOP 1 m.MatterID, c.CaseID, l.LeadID, l.CustomerID
		FROM dbo.Lead l WITH (NOLOCK) 
		INNER JOIN dbo.Cases c WITH (NOLOCK) ON l.LeadID = c.LeadID 
		INNER JOIN dbo.fnGetUserLeadCaseAccess(@UserID, @MatchedLeadID) ca ON c.CaseID = ca.CaseID
		INNER JOIN dbo.Matter m WITH (NOLOCK) ON c.CaseID = m.CaseID 
		WHERE l.LeadID = @LeadID
		AND l.ClientID = @ClientID
		/*ORDER BY c.CaseNum, m.MatterID*/
		ORDER BY 
			/*
				JWG 2014-05-13 #26507 LatestRecordsFirst
			*/
			CASE @LatestRecordsFirst WHEN 1 THEN c.CaseNum END DESC, 
			CASE @LatestRecordsFirst WHEN 0 THEN c.CaseNum END ASC, 
			CASE @LatestRecordsFirst WHEN 1 THEN m.MatterID END DESC, 
			CASE @LatestRecordsFirst WHEN 0 THEN m.MatterID END ASC 

	END
	ELSE IF @CustomerID > 0
	BEGIN

		SELECT TOP 1 @MatchedLeadID = l.LeadID
		FROM dbo.Lead l WITH (NOLOCK) 
		WHERE l.CustomerID = @CustomerID
		AND l.ClientID = @ClientID
		ORDER BY l.LeadID
		
		SELECT TOP 1 m.MatterID, c.CaseID, l.LeadID, l.CustomerID
		FROM dbo.Lead l WITH (NOLOCK) 
		INNER JOIN dbo.Cases c WITH (NOLOCK) ON l.LeadID = c.LeadID 
		INNER JOIN dbo.fnGetUserLeadCaseAccess(@UserID, @MatchedLeadID) ca ON c.CaseID = ca.CaseID
		INNER JOIN dbo.Matter m WITH (NOLOCK) ON c.CaseID = m.CaseID 
		WHERE l.CustomerID = @CustomerID
		AND l.ClientID = @ClientID
		/*ORDER BY l.LeadID, c.CaseNum, m.MatterID*/
		ORDER BY 
			/*
				JWG 2014-05-13 #26507 LatestRecordsFirst
			*/
			CASE @LatestRecordsFirst WHEN 1 THEN l.LeadID END DESC, 
			CASE @LatestRecordsFirst WHEN 0 THEN l.LeadID END ASC, 
			CASE @LatestRecordsFirst WHEN 1 THEN c.CaseNum END DESC, 
			CASE @LatestRecordsFirst WHEN 0 THEN c.CaseNum END ASC, 
			CASE @LatestRecordsFirst WHEN 1 THEN m.MatterID END DESC, 
			CASE @LatestRecordsFirst WHEN 0 THEN m.MatterID END ASC 

	END

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LookupIDs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_LookupIDs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LookupIDs] TO [sp_executeall]
GO
