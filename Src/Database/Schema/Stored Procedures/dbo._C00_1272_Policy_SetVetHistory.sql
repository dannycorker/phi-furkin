SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-09-25
-- Description:	Handles the change of vet from SAS
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Policy_SetVetHistory] 
(
	@LeadID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	-- If the curent vet RL has a value in then check the last table row for previous vets
	-- if different then we are saving a new one so write the row into the history
	DECLARE @CurrentVet INT
	SELECT @CurrentVet = ValueInt
	FROM dbo.LeadDetailValues WITH (NOLOCK) 
	WHERE LeadID = @LeadID
	AND DetailFieldID = 146215
	
	IF @CurrentVet IS NOT NULL
	BEGIN
	
		DECLARE @LastVet INT
		SELECT TOP 1 @LastVet = tdv.ResourceListID
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON r.TableRowID = tdv.TableRowID AND tdv.DetailFieldID = 146203 
		WHERE r.LeadID = @LeadID
		ORDER BY r.TableRowID DESC
		
		IF ISNULL(@LastVet, 0) != @CurrentVet
		BEGIN
		
			INSERT TableRows (ClientID, LeadID, DetailFieldID, DetailFieldPageID)
			VALUES (321, @LeadID, 145677, 16172)
			
			DECLARE @InsertedRow INT
			SELECT @InsertedRow = SCOPE_IDENTITY()
			
			INSERT TableDetailValues (ClientID, LeadID, TableRowID, DetailFieldID, ResourceListID)
			VALUES (321, @LeadID, @InsertedRow, 146203, @CurrentVet)					
		
		END
		
	
	END

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_SetVetHistory] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Policy_SetVetHistory] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_SetVetHistory] TO [sp_executeall]
GO
