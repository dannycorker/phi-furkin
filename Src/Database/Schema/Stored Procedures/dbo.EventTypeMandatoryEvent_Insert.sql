SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventTypeMandatoryEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryEvent_Insert]
(

	@EventTypeMandatoryEventID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@ToEventTypeID int   ,

	@RequiredEventID int   
)
AS


				
				INSERT INTO [dbo].[EventTypeMandatoryEvent]
					(
					[ClientID]
					,[LeadTypeID]
					,[ToEventTypeID]
					,[RequiredEventID]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@ToEventTypeID
					,@RequiredEventID
					)
				-- Get the identity value
				SET @EventTypeMandatoryEventID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryEvent_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent_Insert] TO [sp_executeall]
GO
