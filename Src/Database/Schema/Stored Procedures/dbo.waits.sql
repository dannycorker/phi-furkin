SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-05-23
-- Description:	Processes that are waiting for resources, locks or blocks
-- =============================================
CREATE PROCEDURE [dbo].[waits]
AS
BEGIN
	SET NOCOUNT ON;
	
	/* Quick overview */
	SELECT 
	d.blocking_session_id, 
	d.session_id, 
	d.resource_address, 
	d.resource_description, 
	d.wait_type, 
	d.wait_duration_ms, 
	d.waiting_task_address, 
	d.blocking_task_address 
	FROM sys.dm_os_waiting_tasks d 
	ORDER BY d.session_id 
	
	/* Who is who */
	EXEC sys.sp_who2 
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[waits] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[waits] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[waits] TO [sp_executeall]
GO
