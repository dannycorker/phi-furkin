SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PanelItems table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItems_GetByPanelItemID]
(

	@PanelItemID int   
)
AS


				SELECT
					[PanelItemID],
					[ClientID],
					[PanelItemName],
					[PanelID],
					[PanelItemTypeID],
					[CreatedBy],
					[QueryID],
					[IsGlobal],
					[XAxisColumn],
					[Orientation],
					[Margin]
				FROM
					[dbo].[PanelItems] WITH (NOLOCK) 
				WHERE
										[PanelItemID] = @PanelItemID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItems_GetByPanelItemID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItems_GetByPanelItemID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItems_GetByPanelItemID] TO [sp_executeall]
GO
