SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DataLoaderLog table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderLog_Delete]
(

	@DataLoaderLogID int   
)
AS


				DELETE FROM [dbo].[DataLoaderLog] WITH (ROWLOCK) 
				WHERE
					[DataLoaderLogID] = @DataLoaderLogID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderLog_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderLog_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderLog_Delete] TO [sp_executeall]
GO
