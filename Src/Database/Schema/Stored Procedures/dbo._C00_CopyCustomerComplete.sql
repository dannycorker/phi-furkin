SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2014-05-28
-- Description:	Create a complete copy of a customer with all appropriate sub-tables
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CopyCustomerComplete]
	@ClientID INT,
	@ExistingCustomerID INT,
	@LeadDocumentESignatureTakeover BIT = 0,	/* 0=ignore, 1=take over the existing record */
	@DetailValueHistoryOption TINYINT = 1,		/* 0=ignore, 1=full copy, 2=latest only */
	@Debug BIT = 1
	/*@IncludeECatcher BIT = 0*/
AS
BEGIN
	SET NOCOUNT ON;


	/*
	JWG 2014-05-28
	
	The purpose of this script is to create a complete copy of a Customer, Lead, Case, Matter, Event history and all detail values.
	
	Tables included are as follows:
		
		Customers 
			CustomerDetailValues 
			CustomerMatchKey 
			CustomerOffice 
			Department 
				Contact
					ContactDetailValues
			Partner 
				PartnerMatchKey 
			For each Lead 
				LeadDetailValues 
				LeadDocument/LeadDocumentFS 
					LeadDocumentEsignatureStatus 
				For each Case 
					CaseDetailValues 
					LeadEvent (no trigger please!) 
					LeadEventThreadCompletion 
					For each Matter 
						MatterDetailValues 
						MatterPageChoice 
					Loop (Matter)
				Loop (Case)
			Loop (Lead)
			PortalUser 
				PortalUserCase 
				PortalUserOption 
			TableRows 
				TableDetailValues 
				
		eCatcher tables might need to be included one day, but no requirement for them as yet.
	*/
	
	DECLARE 
	@ExistingLeadID INT = 0,					/* These default values of zero control the looping in the code below. */
	@ExistingCaseID INT = 0,
	@ExistingMatterID INT = 0,
	@ExistingPortalUserID INT = 0,
	@ExistingLeadDocumentID INT = 0,
	@ExistingDepartmentID INT = 0,
	@ExistingContactID INT = 0,
	
	@NewCustomerID INT,							/* These are all transient apart from NewCustomerID. Might have many Leads, hence the @Tables below */
	@NewLeadID INT, 
	@NewCaseID INT, 
	@NewMatterID INT, 
	@NewPortalUserID INT, 
	@NewLeadDocumentID INT,
	@NewDepartmentID INT,
	@NewContactID INT,
	
	@HasMoreLeads BIT = 1,						/* There is always at least one lead per customer. */
	@HasMoreCases BIT,							/* This gets set for each Lead. */
	@HasMoreMatters BIT,						/* This gets set for each Case. */
	@HasMoreLeadDocuments BIT = 0,				/* This gets set for each Lead. Default to none. */
	@HasMoreDepartments BIT = 1,
	@HasMoreContacts BIT = 1,
		
	/* Portal User variables */
	@Username VARCHAR(50),
	@Password VARCHAR(65),
	@Salt VARCHAR (25),
	@ClientPersonnelID INT,
	@Enabled BIT,
	@AttemptedLogins INT,
	@PortalUserGroupID INT
	
	DECLARE @LeadIDs TABLE (					/* Store the mappings between the existing and the newly-copied records */
		ExistingLeadID INT, 
		NewLeadID INT
	)
	
	DECLARE @CaseIDs TABLE (
		ExistingCaseID INT, 
		NewCaseID INT
	)
	
	DECLARE @MatterIDs TABLE (
		ExistingMatterID INT, 
		NewMatterID INT
	)
	
	DECLARE @LeadDocumentIDs TABLE (
		ExistingLeadDocumentID INT, 
		NewLeadDocumentID INT
	)
	
	DECLARE @DepartmentIDs TABLE (
		ExistingDepartmentID INT, 
		NewDepartmentID INT
	)

	DECLARE @ContactIDs TABLE (
		ExistingContactID INT, 
		NewContactID INT
	)
	
	DECLARE @LeadEventCopyTo TABLE (
		ToInt INT IDENTITY(1, 1), 
		NewLeadEventID INT
	)
	
	DECLARE @LeadEventCopyFrom TABLE (
		FromInt INT IDENTITY(1, 1), 
		OldLeadEventID INT
	)
	
	
	/* Check that this customer exists and belongs to the stated client */
	IF NOT EXISTS ( SELECT * FROM dbo.Customers c WITH (NOLOCK) WHERE c.CustomerID = @ExistingCustomerID AND c.ClientID = @ClientID)
	BEGIN
		IF @Debug = 1
		BEGIN
			PRINT 'No matching customer found for this client'
		END
		RETURN -1
	END
	
	
	/* Copy the Customer first of all */
	INSERT INTO dbo.Customers(ClientID, TitleID, IsBusiness, FirstName, MiddleName, LastName, EmailAddress, DayTimeTelephoneNumber, DayTimeTelephoneNumberVerifiedAndValid, HomeTelephone, HomeTelephoneVerifiedAndValid, MobileTelephone, MobileTelephoneVerifiedAndValid, CompanyTelephone, CompanyTelephoneVerifiedAndValid, WorksTelephone, WorksTelephoneVerifiedAndValid, Address1, Address2, Town, County, PostCode, Website, HasDownloaded, DownloadedOn, AquariumStatusID, ClientStatusID, Test, CompanyName, Occupation, Employer, PhoneNumbersVerifiedOn, DoNotEmail, DoNotSellToThirdParty, AgreedToTermsAndConditions, DateOfBirth, DefaultContactID, DefaultOfficeID, AddressVerified, CountryID, SubClientID, CustomerRef, WhoChanged, WhenChanged, ChangeSource, EmailAddressVerifiedAndValid, EmailAddressVerifiedOn, Comments, AllowSmsCommandProcessing)	
	SELECT ClientID, TitleID, IsBusiness, FirstName, MiddleName, LastName, EmailAddress, DayTimeTelephoneNumber, DayTimeTelephoneNumberVerifiedAndValid, HomeTelephone, HomeTelephoneVerifiedAndValid, MobileTelephone, MobileTelephoneVerifiedAndValid, CompanyTelephone, CompanyTelephoneVerifiedAndValid, WorksTelephone, WorksTelephoneVerifiedAndValid, Address1, Address2, Town, County, PostCode, Website, HasDownloaded, DownloadedOn, AquariumStatusID, ClientStatusID, Test, CompanyName, Occupation, Employer, PhoneNumbersVerifiedOn, DoNotEmail, DoNotSellToThirdParty, AgreedToTermsAndConditions, DateOfBirth, DefaultContactID, DefaultOfficeID, AddressVerified, CountryID, SubClientID, CustomerRef, WhoChanged, dbo.fn_GetDate_Local(), ChangeSource, EmailAddressVerifiedAndValid, EmailAddressVerifiedOn, Comments, AllowSmsCommandProcessing
	FROM dbo.Customers c WITH (NOLOCK)
	WHERE c.CustomerID = @ExistingCustomerID
	
	SELECT @NewCustomerID = SCOPE_IDENTITY()
	
	
	/* CustomerDetailValues */
	INSERT dbo.CustomerDetailValues(ClientID, CustomerID, DetailFieldID, DetailValue, EncryptedValue, SourceID)
	SELECT ClientID, @NewCustomerID, DetailFieldID, DetailValue, EncryptedValue, CustomerDetailValueID
	FROM dbo.CustomerDetailValues c WITH (NOLOCK) 
	WHERE c.CustomerID = @ExistingCustomerID 
	
	
	/* CustomerMatchKey */
	INSERT dbo.CustomerMatchKey(ClientID, CustomerID, FirstName, LastName, Address1, Postcode, DateOfBirth)
	SELECT ClientID, @NewCustomerID, FirstName, LastName, Address1, Postcode, DateOfBirth
	FROM dbo.CustomerMatchKey c WITH (NOLOCK) 
	WHERE c.CustomerID = @ExistingCustomerID 
	
	
	/* CustomerOffice */
	INSERT dbo.CustomerOffice(ClientID, CustomerID, OfficeName, MainPhoneNumber, FaxNumber, Address1, Address2, Town, County, Postcode, Country, Notes, CountryID)
	SELECT ClientID, @NewCustomerID, OfficeName, MainPhoneNumber, FaxNumber, Address1, Address2, Town, County, Postcode, Country, Notes, CountryID
	FROM dbo.CustomerOffice c WITH (NOLOCK) 
	WHERE c.CustomerID = @ExistingCustomerID 
	
	
	/* Department */
	WHILE @HasMoreDepartments = 1
	BEGIN
		
		SELECT TOP 1 @ExistingDepartmentID = l.DepartmentID 
		FROM dbo.Department l WITH (NOLOCK) 
		WHERE l.CustomerID = @ExistingCustomerID 
		AND l.DepartmentID > @ExistingDepartmentID 
		ORDER BY l.DepartmentID
		
		IF @@ROWCOUNT > 0
		BEGIN

			INSERT dbo.Department(ClientID, CustomerID, DepartmentName, DepartmentDescription)
			SELECT ClientID, @NewCustomerID, DepartmentName, DepartmentDescription
			FROM dbo.Department c WITH (NOLOCK) 
			WHERE c.CustomerID = @ExistingCustomerID 
			
			SET @NewDepartmentID = SCOPE_IDENTITY()
			
			INSERT @DepartmentIDs(ExistingDepartmentID, NewDepartmentID) 
			VALUES (@ExistingDepartmentID, @NewDepartmentID)
		END
		ELSE
		BEGIN
			SET @HasMoreDepartments = 0
		END

	END /* Department loop */
	
	IF @Debug = 1
	BEGIN
		PRINT 'Departments done'
	END
	
	
	/* Contact */
	WHILE @HasMoreContacts = 1
	BEGIN
		
		SELECT TOP 1 @ExistingContactID = l.ContactID 
		FROM dbo.Contact l WITH (NOLOCK) 
		WHERE l.CustomerID = @ExistingCustomerID 
		AND l.ContactID > @ExistingContactID 
		ORDER BY l.ContactID
		
		IF @@ROWCOUNT > 0
		BEGIN

			/* Use new Department details */
			INSERT dbo.Contact(ClientID, CustomerID, TitleID, Firstname, Middlename, Lastname, EmailAddressWork, EmailAddressOther, DirectDial, MobilePhoneWork, MobilePhoneOther, Address1, Address2, Town, County, Postcode, Country, OfficeID, DepartmentID, JobTitle, Notes, CountryID, LanguageID)
			SELECT c.ClientID, @NewCustomerID, c.TitleID, c.Firstname, c.Middlename, c.Lastname, c.EmailAddressWork, c.EmailAddressOther, c.DirectDial, c.MobilePhoneWork, c.MobilePhoneOther, c.Address1, c.Address2, c.Town, c.County, c.Postcode, c.Country, c.OfficeID, d.NewDepartmentID, c.JobTitle, c.Notes, c.CountryID, c.LanguageID
			FROM dbo.Contact c WITH (NOLOCK) 
			INNER JOIN @DepartmentIDs d ON d.ExistingDepartmentID = c.DepartmentID
			WHERE c.CustomerID = @ExistingCustomerID 
			
			SET @NewContactID = SCOPE_IDENTITY()
			
			INSERT @ContactIDs(ExistingContactID, NewContactID) 
			VALUES (@ExistingContactID, @NewContactID)
			
			/* ContactDetailValues */
			INSERT dbo.ContactDetailValues(ClientID, ContactID, DetailFieldID, DetailValue, EncryptedValue, SourceID)
			SELECT ClientID, @NewContactID, DetailFieldID, DetailValue, EncryptedValue, ContactDetailValueID
			FROM dbo.ContactDetailValues l WITH (NOLOCK) 
			WHERE l.ContactID = @ExistingContactID 
			
		END
		ELSE
		BEGIN
			SET @HasMoreContacts = 0
		END

	END /* Contact loop */
	
	
	/* Copy the partner record if one exists */
	INSERT INTO dbo.Partner (CustomerID, ClientID, UseCustomerAddress, TitleID, FirstName, MiddleName, LastName, EmailAddress, DayTimeTelephoneNumber, HomeTelephone, MobileTelephone, Address1, Address2, Town, County, PostCode, Occupation, Employer, DateOfBirth, CountryID)
	SELECT @NewCustomerID, ClientID, UseCustomerAddress, TitleID, FirstName, MiddleName, LastName, EmailAddress, DayTimeTelephoneNumber, HomeTelephone, MobileTelephone, Address1, Address2, Town, County, PostCode, Occupation, Employer, DateOfBirth, CountryID
	FROM Partner p WITH (NOLOCK)
	WHERE p.CustomerID = @ExistingCustomerID
	
	
	/* PartnerMatchKey */
	/* If the new customer has a partner, get the new Partner's matchkey */
	INSERT dbo.PartnerMatchKey(ClientID, PartnerID, FirstName, LastName, Address1, Postcode, DateOfBirth)
	SELECT p.ClientID, p.PartnerID, p.FirstName, p.LastName, p.Address1, p.Postcode, p.DateOfBirth
	FROM dbo.PartnerMatchKey p WITH (NOLOCK) 
	INNER JOIN dbo.Partner c ON c.PartnerID = p.PartnerID 
	WHERE c.CustomerID = @NewCustomerID 
	
	

	/* Loop round all Leads, Cases and Matters */
	/* There is always at least one of each. */
	WHILE @HasMoreLeads = 1
	BEGIN
		
		SELECT TOP 1 @ExistingLeadID = l.LeadID 
		FROM dbo.Lead l WITH (NOLOCK) 
		WHERE l.CustomerID = @ExistingCustomerID 
		AND l.LeadID > @ExistingLeadID 
		ORDER BY l.LeadID
		
		IF @@ROWCOUNT > 0
		BEGIN
	
			IF @Debug = 1
			BEGIN
				PRINT 'Starting Lead ' + CAST(@ExistingLeadID AS VARCHAR) 
			END
				
			/* A(nother) Lead was found. Copy it and all its dependent tables */
			INSERT dbo.Lead(ClientID, LeadRef, CustomerID, LeadTypeID, AquariumStatusID, ClientStatusID, BrandNew, Assigned, AssignedTo, AssignedBy, AssignedDate, RecalculateEquations, Password, Salt, WhenCreated)
			SELECT ClientID, LeadRef, @NewCustomerID, LeadTypeID, AquariumStatusID, ClientStatusID, BrandNew, Assigned, AssignedTo, AssignedBy, AssignedDate, RecalculateEquations, Password, Salt, dbo.fn_GetDate_Local()
			FROM dbo.Lead l WITH (NOLOCK) 
			WHERE l.LeadID = @ExistingLeadID 
			
			SELECT @NewLeadID = SCOPE_IDENTITY()
			
			/* Make a note of all the Leads copied */
			INSERT @LeadIDs (ExistingLeadID, NewLeadID)
			VALUES (@ExistingLeadID, @NewLeadID)
			
			/* LeadDetailValues */
			INSERT dbo.LeadDetailValues(ClientID, LeadID, DetailFieldID, DetailValue, EncryptedValue, SourceID)
			SELECT ClientID, @NewLeadID, DetailFieldID, DetailValue, EncryptedValue, LeadDetailValueID
			FROM dbo.LeadDetailValues l WITH (NOLOCK) 
			WHERE l.LeadID = @ExistingLeadID 
			
			/* LeadDocument and LeadDocumentFS combined */
			INSERT @LeadDocumentIDs(ExistingLeadDocumentID) 
			SELECT ld.LeadDocumentID 
			FROM dbo.LeadDocument ld WITH (NOLOCK) 
			WHERE ld.LeadID = @ExistingLeadID 
			ORDER BY ld.LeadDocumentID 
			
			IF @@ROWCOUNT > 0
			BEGIN
				SET @HasMoreLeadDocuments = 1
			END
			
			WHILE @HasMoreLeadDocuments = 1
			BEGIN
				/*
					For each existing document, call our own _C00_CopyLeadDocument proc which
					handles the dual inserts into LeadDocument and LeadDocumentFS
				*/
				SELECT TOP 1 @ExistingLeadDocumentID = ld.ExistingLeadDocumentID 
				FROM @LeadDocumentIDs ld 
				WHERE ld.ExistingLeadDocumentID > @ExistingLeadDocumentID 
				ORDER BY ld.ExistingLeadDocumentID 
				
				IF @@ROWCOUNT > 0
				BEGIN
					IF @Debug = 1
					BEGIN
						PRINT 'Starting LeadDocument ' + CAST(@ExistingLeadDocumentID AS VARCHAR) 
					END
					
					EXEC @NewLeadDocumentID = dbo._C00_CopyLeadDocument @From_LeadDocumentID = @ExistingLeadDocumentID, @To_LeadID = @NewLeadID, @To_ClientID = NULL, @FromLeadEventID = NULL 
					
					/* Update the working table with the new ID */ 
					UPDATE @LeadDocumentIDs 
					SET NewLeadDocumentID = @NewLeadDocumentID 
					WHERE ExistingLeadDocumentID = @ExistingLeadDocumentID
				END
				ELSE
				BEGIN
					SET @HasMoreLeadDocuments = 0
				END				
			END /* Document loop */
			
			/* LeadDocumentEsignatureStatus */
			/*
				The user needs to choose what to do with this status.  The document has not actually been echoSigned, 
				but they might want to pretend that it has in order to do aged-data testing.
				
				There is no concept of blank records in this table.
				
				The choices are:
					Do not copy these records at all
					Copy and destroy the originals, because the unique GUID is the link to echoSign and the PK on our table
			*/
			/* IF @LeadDocumentESignatureChoice = 0 then do nothing with this table */
			/* IF @LeadDocumentESignatureChoice = 1 then take over the existing records */
			IF @LeadDocumentESignatureTakeover = 1
			BEGIN
				UPDATE dbo.LeadDocumentEsignatureStatus 
				SET LeadDocumentID = lds.NewLeadDocumentID 
				FROM dbo.LeadDocumentEsignatureStatus esig 
				INNER JOIN @LeadDocumentIDs lds ON lds.ExistingLeadDocumentID = esig.LeadDocumentID 
			END
			
			
			SET @HasMoreCases = 1
	
			/* Cases */
			WHILE @HasMoreCases = 1
			BEGIN
				
				SELECT TOP 1 @ExistingCaseID = l.CaseID 
				FROM dbo.Cases l WITH (NOLOCK) 
				WHERE l.LeadID = @ExistingLeadID 
				AND l.CaseID > @ExistingCaseID 
				ORDER BY l.CaseID 
				
				IF @@ROWCOUNT > 0
				BEGIN
				
					IF @Debug = 1
					BEGIN
						PRINT 'Starting Case ' + CAST(@ExistingCaseID AS VARCHAR) 
					END
					
					/* A(nother) Case was found. Copy it and all its dependent tables */
					INSERT dbo.Cases(LeadID, ClientID, CaseNum, CaseRef, ClientStatusID, AquariumStatusID, DefaultContactID, LatestLeadEventID, LatestInProcessLeadEventID, LatestOutOfProcessLeadEventID, LatestNonNoteLeadEventID, LatestNoteLeadEventID, WhoCreated, WhenCreated, WhoModified, WhenModified, ProcessStartLeadEventID)
					SELECT @NewLeadID, ClientID, CaseNum, CaseRef, ClientStatusID, AquariumStatusID, DefaultContactID, LatestLeadEventID, LatestInProcessLeadEventID, LatestOutOfProcessLeadEventID, LatestNonNoteLeadEventID, LatestNoteLeadEventID, WhoCreated, dbo.fn_GetDate_Local(), WhoModified, dbo.fn_GetDate_Local(), NULL
					FROM dbo.Cases l WITH (NOLOCK) 
					WHERE l.CaseID = @ExistingCaseID 
					
					SELECT @NewCaseID = SCOPE_IDENTITY()
					
					/* Make a note of all the Cases copied */
					INSERT @CaseIDs (ExistingCaseID, NewCaseID)
					VALUES (@ExistingCaseID, @NewCaseID)
					
					/* CaseDetailValues */
					INSERT dbo.CaseDetailValues(ClientID, CaseID, DetailFieldID, DetailValue, EncryptedValue, SourceID)
					SELECT ClientID, @NewCaseID, DetailFieldID, DetailValue, EncryptedValue, CaseDetailValueID
					FROM dbo.CaseDetailValues l WITH (NOLOCK) 
					WHERE l.CaseID = @ExistingCaseID 

					/* LeadEvent & LETC */
					IF @Debug = 1
					BEGIN
						PRINT 'Starting LeadEvents'
					END
					
					/* Set CONTEXT_INFO so that the LeadEvent trigger knows we are just copying some records here */
					DECLARE @ContextInfo VARBINARY(100) = CAST('NoSAE' AS VARBINARY)
					SET CONTEXT_INFO @ContextInfo
					
					/* Create the new LeadEvent records first of all */
					INSERT INTO LeadEvent (ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, NoteTypeID, FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, CaseID, LeadDocumentID, NotePriority, DocumentQueueID, EventDeleted, WhoDeleted, DeletionComments, ContactID, BaseCost, DisbursementCost, DisbursementDescription, ChargeOutRate, UnitsOfEffort, CostEnteredManually, IsOnHold, HoldLeadEventID)
					SELECT ClientID, @NewLeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, NoteTypeID, FollowupDateTime, WhenFollowedUp, AquariumEventType, NextEventID, @NewCaseID, LeadDocumentID, NotePriority, DocumentQueueID, EventDeleted, WhoDeleted, DeletionComments, ContactID, BaseCost, DisbursementCost, DisbursementDescription, ChargeOutRate, UnitsOfEffort, CostEnteredManually, IsOnHold, HoldLeadEventID
					FROM LeadEvent le 
					WHERE CaseID = @ExistingCaseID
					ORDER BY le.LeadEventID ASC
					
					/* Note the order of the new events */
					INSERT INTO @LeadEventCopyTo (NewLeadEventID)
					SELECT LeadEventID
					FROM LeadEvent le 
					WHERE CaseID = @NewCaseID
					ORDER BY le.LeadEventID ASC
					
					/* Note the order of the old events */
					INSERT INTO @LeadEventCopyFrom (OldLeadEventID)
					SELECT LeadEventID
					FROM LeadEvent le 
					WHERE CaseID = @ExistingCaseID
					ORDER BY le.LeadEventID ASC
					
					/* Use the two table variables to match up the old and new events in order (1, 2, 3 etc) */
					UPDATE LeadEvent
					SET NextEventID = NewLeadEventID
					FROM LeadEvent
					INNER JOIN @LeadEventCopyFrom lecf ON lecf.OldLeadEventID = LeadEvent.NextEventID
					INNER JOIN @LeadEventCopyTo etct ON etct.ToInt = lecf.FromInt
					WHERE CaseID = @NewCaseID
					
					/* Clear the CONTEXT_INFO  */
					SET CONTEXT_INFO 0x0
					
					
					/* Matters */
					SET @HasMoreMatters = 1
					
					WHILE @HasMoreMatters = 1
					BEGIN
						
						SELECT TOP 1 @ExistingMatterID = l.MatterID 
						FROM dbo.Matter l WITH (NOLOCK) 
						WHERE l.CaseID = @ExistingCaseID 
						AND l.MatterID > @ExistingMatterID 
						ORDER BY l.MatterID
						
						IF @@ROWCOUNT > 0
						BEGIN
						
							IF @Debug = 1
							BEGIN
								PRINT 'Starting Matter ' + CAST(@ExistingMatterID AS VARCHAR) 
							END
							
							/* A(nother) Matter was found. Copy it and all its dependent tables */
							INSERT dbo.Matter(ClientID, MatterRef, CustomerID, LeadID, MatterStatus, RefLetter, BrandNew, CaseID, WhoCreated, WhenCreated, WhoModified, WhenModified, SourceID)
							SELECT ClientID, MatterRef, @NewCustomerID, @NewLeadID, MatterStatus, RefLetter, BrandNew, @NewCaseID, WhoCreated, dbo.fn_GetDate_Local(), WhoModified, dbo.fn_GetDate_Local(), @ExistingMatterID
							FROM dbo.Matter l WITH (NOLOCK) 
							WHERE l.MatterID = @ExistingMatterID 
							
							SELECT @NewMatterID = SCOPE_IDENTITY()
							
							/* Make a note of all the Matters copied */
							INSERT @MatterIDs (ExistingMatterID, NewMatterID)
							VALUES (@ExistingMatterID, @NewMatterID)
							
							/* MatterDetailValues */
							INSERT dbo.MatterDetailValues(ClientID, LeadID, MatterID, DetailFieldID, DetailValue, EncryptedValue, SourceID)
							SELECT ClientID, @NewLeadID, @NewMatterID, DetailFieldID, DetailValue, EncryptedValue, MatterDetailValueID
							FROM dbo.MatterDetailValues l WITH (NOLOCK) 
							WHERE l.MatterID = @ExistingMatterID 
							
							/* MatterPageChoice */
							INSERT dbo.MatterPageChoice(ClientID, MatterID, DetailFieldPageID, WhoModified, WhenModified) 
							SELECT ClientID, @NewMatterID, DetailFieldPageID, WhoModified, dbo.fn_GetDate_Local() 
							FROM dbo.MatterPageChoice l WITH (NOLOCK) 
							WHERE l.MatterID = @ExistingMatterID 
							
						END
						ELSE
						BEGIN
							SET @HasMoreMatters = 0
						END /* Matter loop */
					END
				END
				ELSE
				BEGIN
					SET @HasMoreCases = 0
				END
				
			END /* Case loop */
			
		END
		ELSE
		BEGIN
			SET @HasMoreLeads = 0
		END
		
	END /* Lead loop */
	
	/* Now the tables that need some or all of the new IDs */
	
	/* PortalUser */
	IF @Debug = 1
	BEGIN
		PRINT 'Starting PortalUser'
	END
	
	SELECT @ExistingPortalUserID = p.PortalUserID, 
	@Username = p.Username + CAST(@NewCustomerID AS VARCHAR), /* Username has to be unique */
	@Password = p.Password,
	@Salt = p.Salt,
	@ClientPersonnelID = p.ClientPersonnelID,
	@Enabled = p.Enabled,
	@AttemptedLogins = p.AttemptedLogins,
	@PortalUserGroupID = p.PortalUserGroupID
	FROM dbo.PortalUser p WITH (NOLOCK) 
	WHERE p.CustomerID = @ExistingCustomerID
	
	IF @ExistingPortalUserID > 0
	BEGIN
		/* We cannot insert into this directly.  Call the helper proc to set up an account in the AquariusMaster database first. */
		EXEC @NewPortalUserID = [dbo].[PortalUser_Insert_Helper] @ClientID, @NewCustomerID, @Username, @Password, @Salt, @ClientPersonnelID, @Enabled, @AttemptedLogins, @PortalUserGroupID
		
		IF @NewPortalUserID > 0
		BEGIN
			
			IF @Debug = 1
			BEGIN
				PRINT 'Starting PortalUserCase'
			END
			
			/* PortalUserCase */
			INSERT INTO dbo.PortalUserCase(PortalUserID, LeadID, CaseID, ClientID)
			SELECT @NewPortalUserID, el.NewLeadID, ec.NewCaseID, puc.ClientID 
			FROM dbo.PortalUserCase puc WITH (NOLOCK) 
			INNER JOIN @LeadIDs el ON el.ExistingLeadID = puc.LeadID
			INNER JOIN @CaseIDs ec ON ec.ExistingCaseID = puc.CaseID
			WHERE puc.PortalUserID = @ExistingPortalUserID
			
			/* PortalUserOption */
			INSERT INTO dbo.PortalUserOption(ClientID, PortalUserID, PortalUserOptionTypeID, OptionValue)
			SELECT p.ClientID, @NewPortalUserID, p.PortalUserOptionTypeID, p.OptionValue 
			FROM dbo.PortalUserOption p WITH (NOLOCK) 
			WHERE p.PortalUserID = @ExistingPortalUserID
			
		END
	END /* PortalUser block */
	

	/* Table Rows */
	IF @Debug = 1
	BEGIN
		PRINT 'Starting TableRows'
	END
	
	/* These can be held at a variety of levels now, but ignore ClientPersonnel */
	/* Create the new rows first, using SourceID to hold the old TableRowID */
	INSERT INTO dbo.TableRows (ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID, SourceID) 
	SELECT ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, @NewCustomerID, TableRowID 
	FROM dbo.TableRows tr WITH (NOLOCK) 
	WHERE tr.CustomerID = @ExistingCustomerID

	INSERT INTO dbo.TableRows (ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, LeadID, SourceID) 
	SELECT ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, ids.NewLeadID, TableRowID 
	FROM dbo.TableRows tr WITH (NOLOCK) 
	INNER JOIN @LeadIDs ids ON ids.ExistingLeadID = tr.LeadID 

	INSERT INTO dbo.TableRows (ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CaseID, SourceID) 
	SELECT ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, ids.NewCaseID, TableRowID 
	FROM dbo.TableRows tr WITH (NOLOCK) 
	INNER JOIN @CaseIDs ids ON ids.ExistingCaseID = tr.CaseID 

	INSERT INTO dbo.TableRows (ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, MatterID, SourceID) 
	SELECT ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, ids.NewMatterID, TableRowID 
	FROM dbo.TableRows tr WITH (NOLOCK) 
	INNER JOIN @MatterIDs ids ON ids.ExistingMatterID = tr.MatterID 

	INSERT INTO dbo.TableRows (ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, ContactID, SourceID) 
	SELECT ClientID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, ids.NewContactID, TableRowID 
	FROM dbo.TableRows tr WITH (NOLOCK) 
	INNER JOIN @ContactIDs ids ON ids.ExistingContactID = tr.ContactID 
	
	/* TableDetailValues */
	IF @Debug = 1
	BEGIN
		PRINT 'Starting TableDetailValues'
	END
	
	/* Now insert the values from each level above - Customer/Lead/Case/Matter */
	/*
		The new rows exist with the new Customer/Lead/Case/Matter/Contact IDs
		They also have the old TableRowID stored in the SourceID column, so use that to get all the old TableDetailValues and 
		copy them to the new rows 
	*/
	INSERT INTO dbo.TableDetailValues(TableRowID, ResourceListID, DetailFieldID, DetailValue, ClientID, EncryptedValue, CustomerID) 
	SELECT tr.TableRowID, tdv.ResourceListID, tdv.DetailFieldID, tdv.DetailValue, tdv.ClientID, tdv.EncryptedValue, @NewCustomerID 
	FROM dbo.TableRows tr WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.SourceID /* Join to SourceID not TableRowID, as we want to attach the old values to the new row */
	WHERE tr.CustomerID = @NewCustomerID
	
	INSERT INTO dbo.TableDetailValues(TableRowID, ResourceListID, DetailFieldID, DetailValue, ClientID, EncryptedValue, LeadID) 
	SELECT tr.TableRowID, tdv.ResourceListID, tdv.DetailFieldID, tdv.DetailValue, tdv.ClientID, tdv.EncryptedValue, ids.NewLeadID 
	FROM dbo.TableRows tr WITH (NOLOCK) 
	INNER JOIN @LeadIDs ids ON ids.NewLeadID = tr.LeadID 
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.SourceID /* Join to SourceID not TableRowID, as we want to attach the old values to the new row */
	
	INSERT INTO dbo.TableDetailValues(TableRowID, ResourceListID, DetailFieldID, DetailValue, ClientID, EncryptedValue, CaseID) 
	SELECT tr.TableRowID, tdv.ResourceListID, tdv.DetailFieldID, tdv.DetailValue, tdv.ClientID, tdv.EncryptedValue, ids.NewCaseID 
	FROM dbo.TableRows tr WITH (NOLOCK) 
	INNER JOIN @CaseIDs ids ON ids.NewCaseID = tr.CaseID 
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.SourceID /* Join to SourceID not TableRowID, as we want to attach the old values to the new row */
	
	INSERT INTO dbo.TableDetailValues(TableRowID, ResourceListID, DetailFieldID, DetailValue, ClientID, EncryptedValue, MatterID) 
	SELECT tr.TableRowID, tdv.ResourceListID, tdv.DetailFieldID, tdv.DetailValue, tdv.ClientID, tdv.EncryptedValue, ids.NewMatterID 
	FROM dbo.TableRows tr WITH (NOLOCK) 
	INNER JOIN @MatterIDs ids ON ids.NewMatterID = tr.MatterID 
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.SourceID /* Join to SourceID not TableRowID, as we want to attach the old values to the new row */
	
	INSERT INTO dbo.TableDetailValues(TableRowID, ResourceListID, DetailFieldID, DetailValue, ClientID, EncryptedValue, ContactID) 
	SELECT tr.TableRowID, tdv.ResourceListID, tdv.DetailFieldID, tdv.DetailValue, tdv.ClientID, tdv.EncryptedValue, ids.NewContactID 
	FROM dbo.TableRows tr WITH (NOLOCK) 
	INNER JOIN @ContactIDs ids ON ids.NewContactID = tr.ContactID 
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.SourceID /* Join to SourceID not TableRowID, as we want to attach the old values to the new row */
	
	
	IF @Debug = 1
	BEGIN
		SELECT * FROM @LeadIDs
		
		SELECT * FROM @CaseIDs
		
		SELECT * FROM @MatterIDs
		
		SELECT * FROM @LeadDocumentIDs
		
		SELECT * FROM @DepartmentIDs
		
		SELECT * FROM @ContactIDs
		
		SELECT * FROM @LeadEventCopyTo
		
		SELECT * FROM @LeadEventCopyFrom
	END
	
	/* DetailValueHistory */
	/* IF @DetailValueHistoryOption = 0 then do nothing with this table */
	/* IF @DetailValueHistoryOption = 1 then copy all history records */
	/* IF @DetailValueHistoryOption = 2 then only insert the latest history record to give it an initial value */
	IF @DetailValueHistoryOption = 1
	BEGIN
		INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, CustomerID) 
		SELECT ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, @NewCustomerID 
		FROM dbo.DetailValueHistory h WITH (NOLOCK) 
		WHERE h.LeadOrMatter = 10 
		AND h.CustomerID = @ExistingCustomerID 
		
		INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, LeadID) 
		SELECT ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, ids.NewLeadID 
		FROM dbo.DetailValueHistory h WITH (NOLOCK) 
		INNER JOIN @LeadIDs ids ON ids.ExistingLeadID = h.LeadID 
		WHERE h.LeadOrMatter = 1 
		
		INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, CaseID) 
		SELECT ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, ids.NewCaseID 
		FROM dbo.DetailValueHistory h WITH (NOLOCK) 
		INNER JOIN @CaseIDs ids ON ids.ExistingCaseID = h.CaseID 
		WHERE h.LeadOrMatter = 11 
		
		INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, MatterID) 
		SELECT ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, ids.NewMatterID 
		FROM dbo.DetailValueHistory h WITH (NOLOCK) 
		INNER JOIN @MatterIDs ids ON ids.ExistingMatterID = h.MatterID 
		WHERE h.LeadOrMatter = 2 
		
		INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, ContactID) 
		SELECT ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, ids.NewContactID 
		FROM dbo.DetailValueHistory h WITH (NOLOCK) 
		INNER JOIN @ContactIDs ids ON ids.ExistingContactID = h.ContactID 
		WHERE h.LeadOrMatter = 14 
	END

	IF @DetailValueHistoryOption = 2
	BEGIN
		/* Latest Customer history record per field. Simpler than the rest because we know there is only one customer. */
		;WITH InnerSql AS 
		(
			SELECT ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue,  
			ROW_NUMBER() OVER(PARTITION BY DetailFieldID ORDER BY DetailValueHistoryID DESC) as rn 
			FROM dbo.DetailValueHistory h WITH (NOLOCK) 
			WHERE h.LeadOrMatter = 10 
			AND h.CustomerID = @ExistingCustomerID 
		)
		INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, CustomerID) 
		SELECT i.ClientID, i.DetailFieldID, i.LeadOrMatter, i.FieldValue, i.WhenSaved, i.ClientPersonnelID, i.EncryptedValue, @NewCustomerID 
		FROM InnerSql i 
		WHERE i.rn = 1 
		
		/* Latest Lead history record per Lead and field */
		;WITH InnerSql AS 
		(
			SELECT ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, LeadID, 
			ROW_NUMBER() OVER(PARTITION BY LeadID, DetailFieldID ORDER BY DetailValueHistoryID DESC) as rn 
			FROM dbo.DetailValueHistory h WITH (NOLOCK) 
			INNER JOIN @LeadIDs ids ON ids.ExistingLeadID = h.LeadID 
			WHERE h.LeadOrMatter = 1 
		)
		INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, CustomerID) 
		SELECT i.ClientID, i.DetailFieldID, i.LeadOrMatter, i.FieldValue, i.WhenSaved, i.ClientPersonnelID, i.EncryptedValue, ids.NewLeadID 
		FROM InnerSql i 
		INNER JOIN @LeadIDs ids ON ids.ExistingLeadID = i.LeadID 
		WHERE i.rn = 1 
		
		/* Latest Case history record per Case and field */
		;WITH InnerSql AS 
		(
			SELECT ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, CaseID, 
			ROW_NUMBER() OVER(PARTITION BY CaseID, DetailFieldID ORDER BY DetailValueHistoryID DESC) as rn 
			FROM dbo.DetailValueHistory h WITH (NOLOCK) 
			INNER JOIN @CaseIDs ids ON ids.ExistingCaseID = h.CaseID 
			WHERE h.LeadOrMatter = 11 
		)
		INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, CustomerID) 
		SELECT i.ClientID, i.DetailFieldID, i.LeadOrMatter, i.FieldValue, i.WhenSaved, i.ClientPersonnelID, i.EncryptedValue, ids.NewCaseID 
		FROM InnerSql i 
		INNER JOIN @CaseIDs ids ON ids.ExistingCaseID = i.CaseID 
		WHERE i.rn = 1 
		
		/* Latest Matter history record per Matter and field */
		;WITH InnerSql AS 
		(
			SELECT ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, MatterID, 
			ROW_NUMBER() OVER(PARTITION BY MatterID, DetailFieldID ORDER BY DetailValueHistoryID DESC) as rn 
			FROM dbo.DetailValueHistory h WITH (NOLOCK) 
			INNER JOIN @MatterIDs ids ON ids.ExistingMatterID = h.MatterID 
			WHERE h.LeadOrMatter = 11 
		)
		INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, CustomerID) 
		SELECT i.ClientID, i.DetailFieldID, i.LeadOrMatter, i.FieldValue, i.WhenSaved, i.ClientPersonnelID, i.EncryptedValue, ids.NewMatterID 
		FROM InnerSql i 
		INNER JOIN @MatterIDs ids ON ids.ExistingMatterID = i.MatterID 
		WHERE i.rn = 1 
		
		/* Latest Contact history record per Contact and field */
		;WITH InnerSql AS 
		(
			SELECT ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, ContactID, 
			ROW_NUMBER() OVER(PARTITION BY ContactID, DetailFieldID ORDER BY DetailValueHistoryID DESC) as rn 
			FROM dbo.DetailValueHistory h WITH (NOLOCK) 
			INNER JOIN @ContactIDs ids ON ids.ExistingContactID = h.ContactID 
			WHERE h.LeadOrMatter = 14 
		)
		INSERT INTO dbo.DetailValueHistory(ClientID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue, CustomerID) 
		SELECT i.ClientID, i.DetailFieldID, i.LeadOrMatter, i.FieldValue, i.WhenSaved, i.ClientPersonnelID, i.EncryptedValue, ids.NewContactID 
		FROM InnerSql i 
		INNER JOIN @ContactIDs ids ON ids.ExistingContactID = i.ContactID 
		WHERE i.rn = 1 

	END
		
	RETURN @NewCustomerID
	
	/*
		Tables we are ignoring because they are only for eCatcher:

		ClientCustomerMails
		CustomerAnswers
		CustomerOutcomes
		CustomerQuestionnaires
		DroppedOutCustomers
		DroppedOutCustomerAnswers
		DroppedOutCustomerMessages
		DroppedOutCustomerQuestionnaires
		OutcomeDelayedEmails
	*/

	/*
		Tables we are ignoring because they are only for history, that has not happened to the new records yet:

		AutomatedEventQueue
		CaseTransferMapping
		ClientPersonnelFavourite
		CustomerHistory
		DetailFieldLock
		DiaryAppointment
		DiaryReminder
		DocumentBatchDetail
		FileExportEvent
		IncomingPostEvent
		IncomingPostEventValue 
		LeadViewHistory
		LeadViewHistoryArchive
		RPIClaimStatus
		TableDetailValuesHistory (multi)
		WorkflowTask
		WorkflowTaskCompleted
		
		{ClientOfficeLead is trigger-maintained}
		{LeadAssignment is trigger-maintained}

	*/

	/*
		Tables we are ignoring because they are defunct:

		Bill
		QuillCaseMapping
		QuillCustomerMapping
		SmsMessages
	*/

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyCustomerComplete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CopyCustomerComplete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyCustomerComplete] TO [sp_executeall]
GO
