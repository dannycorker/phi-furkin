SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-10
-- Description:	Sql After Event - Prepare Renewal
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Renewal_150143]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@PrepareRenewalDate			DATE
			,@RenewalDateVC					VARCHAR(10)
			,@ValueInt						INT
			,@AccountID						INT
			,@RenewalStartDate				DATE
			,@RenewalEndDate				DATE
/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */
	
	IF @EventTypeID IN (150143) /*Prepare Renewal*/
	BEGIN
		
		DECLARE @OptionGroupID INT,
				@AgeInYears INT,
				@Deductible DECIMAL(18,2),
				@SpeciesID INT,
				@BirthDate DATE,
				@RenewalDate DATE

		SELECT @PrepareRenewalDate = CAST(dbo.fn_GetDate_Local() AS DATE)
		EXEC _C00_SimpleValueIntoField 180231, @PrepareRenewalDate, @MatterID, @WhoCreated	


		/*2018-06-11 ACE - Make sure the date is a varchar!*/
		/*2018-11-29 JEL - And get the right date*/ 
		SELECT @RenewalDateVC=CONVERT(VARCHAR(10),DATEADD(DAY,1,dbo.fnGetDvAsDate(170037,@CaseID)),120)
		
		/*2018-06-14 ACE - Actually use the value.*/
		EXEC dbo._C00_SimpleValueIntoField 175307, @RenewalDateVC, @MatterID, @WhoCreated		
		
		/*2020-11-16 NG populate fields for documentation*/
		SELECT @RenewalStartDate = dbo.fnGetDvAsDate (175307,@CaseID)
		EXEC _C00_SimpleValueIntoField 176383,@RenewalStartDate,@MatterID,@AqAutomation
		SELECT @RenewalEndDate =DATEADD(DAY,-1,DATEADD(YEAR,1,@RenewalStartDate))
		EXEC _C00_SimpleValueIntoField 176384,@RenewalEndDate,@MatterID,@AqAutomation

		-- clear the marketing code if it is no longer applicable
		-- get the code's lifetime
		SELECT @ValueInt=rllt.ValueInt 
		FROM MatterDetailValues mc WITH (NOLOCK) 
		INNER JOIN ResourceListDetailValues rlmc WITH (NOLOCK) ON mc.ValueInt=rlmc.ValueInt AND rlmc.DetailFieldID=170057
		INNER JOIN ResourceListDetailValues rllt WITH (NOLOCK) ON rlmc.ResourceListID=rllt.ResourceListID AND rllt.DetailFieldID=170062
		WHERE mc.MatterID=@MatterID 
		AND mc.DetailFieldID=175488
		
		IF @ValueInt=69908 -- Once pet lifetime
		BEGIN
		
			EXEC dbo._C00_SimpleValueIntoField 175488, '', @MatterID, @WhoCreated		
		
		END

		/*JEL Store the Account ID paying for this policy so we can renew onto this account. This was taken directly from the PP at 
		Renewal, but change payment interval needs to make the change in readiness for renewal and not before. So we default this to 
		the current payment method, and allow the change payment interval prcess to change this*/
		SELECT @AccountID = dbo.fnGetSimpleDvAsInt(180219,@MatterID) 
		IF ISNULL(@AccountID,0) = 0 
		BEGIN 

			SELECT top 1 @AccountID = pp.AccountID FROM PurchasedProduct pp with (NOLOCK) 
			WHERE pp.ObjectID = @MatterID 
			ORDER by pp.PurchasedProductID DESC 

			EXEC _C00_SimpleValueIntoField 180219,@AccountID,@MatterID, @WhoCreated 

		END

		/*GPR 2021-01-27 for FURKIN-142*/
		IF [dbo].[fnGetSimpleDvAsInt](170144,@CustomerID) = 2000184 /*FURKIN*/
		BEGIN
		
			SELECT @OptionGroupID = [dbo].[fnGetSimpleDvAsInt](315882,@LeadID)
			SELECT @SpeciesID =  [dbo].[fnGetSimpleRLDv](144272, 144269 ,@LeadID, 0)
			SELECT @BirthDate =  [dbo].[fnGetSimpleDvAsDate](144274,@LeadID)
			SELECT @RenewalDate =  [dbo].[fnGetSimpleDvAsDate](175307,@MatterID)
			SELECT @AgeInYears = DATEDIFF(YEAR, @BirthDate, @RenewalDate)
			
			SELECT @Deductible = [dbo].[fn_C600_UpdateValue_DeductibleOptionGroup](@OptionGroupID, @AgeInYears, @SpeciesID)

			EXEC _C00_SimpleValueIntoField 313932,@Deductible, @LeadID

			EXEC _C00_logit 'Info','_SAE_Renewal_150143','@OptionGroupID',@OptionGroupID, 58550
			EXEC _C00_logit 'Info','_SAE_Renewal_150143','@SpeciesID',@SpeciesID, 58550
			EXEC _C00_logit 'Info','_SAE_Renewal_150143','@BirthDate',@BirthDate, 58550
			EXEC _C00_logit 'Info','_SAE_Renewal_150143','@RenewalDate',@RenewalDate, 58550
			EXEC _C00_logit 'Info','_SAE_Renewal_150143','@AgeInYears',@AgeInYears, 58550
			EXEC _C00_logit 'Info','_SAE_Renewal_150143','@Deductible',@Deductible, 58550

		END



	END


	PRINT OBJECT_NAME(@@ProcID) + ' END'

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Renewal_150143] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_Renewal_150143] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_Renewal_150143] TO [sp_executeall]
GO
