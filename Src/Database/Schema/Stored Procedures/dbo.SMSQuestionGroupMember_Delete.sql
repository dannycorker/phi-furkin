SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SMSQuestionGroupMember table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionGroupMember_Delete]
(

	@SMSQuestionGroupMemberID int   
)
AS


				DELETE FROM [dbo].[SMSQuestionGroupMember] WITH (ROWLOCK) 
				WHERE
					[SMSQuestionGroupMemberID] = @SMSQuestionGroupMemberID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroupMember_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroupMember_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroupMember_Delete] TO [sp_executeall]
GO
