SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[EventTypeAttachment__GetDocumentAttachments]  
(  
	@CaseID INT,  
	@EventTypeID INT,  
	@IncludeDocumentBlob BIT = 1     
)  
AS  
BEGIN  
   
 -- This will hold all the possible AttachmentEventTypeIDs we are interested in  
 DECLARE @EventTypes TABLE(  
  AttachmentID INT IDENTITY(1,1),   
  AttachmentEventTypeID INT,   
  [ALL] BIT,  
  AttachmentEventGroupID INT  
  PRIMARY KEY (AttachmentID)  
 )  
  
 -- This will hold all the individual LeadDocuments that we are going to attach.  
 -- If batch (scheduler) then this is the definitive list, but if a  
 -- user is running the app, they get to override these default selections.  
 DECLARE @Documents TABLE (  
  [WhenCreated] DATETIME,   
  [DocumentBLOB] IMAGE,   
  [FileName] VARCHAR(255),  
  [DocumentTypeID] INT,   
  [InputFormat] VARCHAR(24),   
  [OutputFormat] VARCHAR(24),   
  [DocumentFormat] VARCHAR(24),   
  EventTypeID INT,  
  CaseID INT,  
  LeadEventID INT,   
  LeadDocumentID INT,  
  LeadDocumentTitle VARCHAR(500),  
  ZipFormat VARCHAR(50),  
  EventTypeName VARCHAR(50),  
  AttachmentID INT  
 )  
  
 -- An alternative to a cursor loop  
 DECLARE @LoopEventTypeID INT,  
   @LoopEventAttachmentID INT,  
   @LeadID INT,  
   @AllForLead BIT = 0,  
   @SinceLastSummary BIT = 0,  
   @SQLOverrideQuery NVARCHAR(MAX),  
   @ClientID INT,  
   @CustomerID INT,  
   @EventSubtypeID INT,
   @MatterID INT
     
 SELECT @LeadID = c.LeadID, @MatterID = m.MatterID  
 FROM Cases c WITH (NOLOCK) 
 JOIN Matter m WITH (NOLOCK) ON m.CaseID = c.CaseID  
 WHERE c.CaseID = @CaseID  
   
 SELECT @EventSubtypeID = et.EventSubtypeID  
 FROM EventType et WITH ( NOLOCK )   
 WHERE et.EventTypeID = @EventTypeID  
  
  
 /*CS 2015-08-25*/  
 /*SELECT @LogEntry = '@CaseID = ' + CONVERT(VARCHAR,@CaseID) + ', @EventTypeID = ' + CONVERT(VARCHAR,@EventTypeID) + ', Count = ' +  
 CONVERT(VARCHAR,(  
     SELECT COUNT(*) /*Can't say NOT EXISTS, because that will exclude this event as well!*/  
     FROM LeadEvent sle WITH (NOLOCK)   
     WHERE (sle.[CaseID] = @CaseID OR (sle.LeadID = @LeadID AND @AllForLead = 1) )  
     and sle.EventDeleted = 0  
     and sle.EventTypeID = @EventTypeID  
     ))  
 EXEC _C00_LogIt 'Info', 'EventTypeAttachment__GetDocumentAttachments', 'Call Info', @LogEntry, 3848  */  
  
  SELECT @ClientID=ClientID   
  FROM Cases WITH (NOLOCK)   
  WHERE CaseID=@CaseID  
 
 SELECT @SQLOverrideQuery = REPLACE(REPLACE(REPLACE(sq.QueryText,'@CaseID',ISNULL(CONVERT(VARCHAR(MAX),@CaseID),'')),'@EventTypeID',ISNULL(CONVERT(VARCHAR,@EventTypeID),'')),'@MatterID',ISNULL(CONVERT(VARCHAR,@MatterID),''))   
 FROM dbo.Overrides ovr WITH ( NOLOCK )  
 INNER JOIN dbo.SqlQuery sq WITH ( NOLOCK ) ON sq.QueryID = ovr.QueryID AND sq.ClientID = @ClientID  
 WHERE ovr.EventTypeID = @EventTypeID  
 AND ovr.OverrideTypeID = 4 /*Event type document type attachment override*/  
   
 /* --NG commenting out as we're on t-pet and therefore don't need client 427 - but the text is quite useful
 if @clientid = 427 and @SQLOverrideQuery is null   
 and exists (  
  select *   
  from TableDetailValues tdv with(nolock)   
  where tdv.DetailFieldID = 304535 -- 304534 table field  
  and tdv.valueint = @EventTypeID  
 )
 begin  
  
  select @SQLOverrideQuery =   
   'SELECT le.WhenCreated,  
   d.documentBlob,  
   d.FileName,  
   dt.DocumentTypeID,  
   dt.InputFormat,  
   dt.OutputFormat,  
   ''RTF'' AS [DocumentFormat],  
   tdv.DetailValue As [EventTypeID],  
   c.CaseID,  
   le.LeadEventID,  
   le.LeadDocumentID,  
   d.LeadDocumentTitle,  
   dt.DocumentTypeName,  
   d.ZipFormat,  
   et.EventTypeName  
   FROM dbo.Customers cu WITH (NOLOCK)   
   INNER JOIN dbo.Lead l WITH (NOLOCK) on l.CustomerID = cu.CustomerID   
   INNER JOIN dbo.Cases c WITH (NOLOCK) on c.LeadID = l.LeadID   
   INNER JOIN dbo.Matter m WITH (NOLOCK) on m.CaseID = c.CaseID   
   INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.CaseID = c.CaseID  
   INNER JOIN EventType et WITH (NOLOCK) ON le.EventTypeID = et.EventTypeID  
   INNER JOIN [vLeadDocumentList] d ON  le.LeadDocumentID = d.LeadDocumentID  
   LEFT JOIN DocumentType dt WITH (NOLOCK) ON et.DocumentTypeID = dt.DocumentTypeID  
   CROSS APPLY TableRows tr WITH (NOLOCK)   
   INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 304531 /*Event Type ID*/  
   INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tr.TableRowID AND tdv1.DetailFieldID = 304533 /*sort order*/  
   INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = tr.TableRowID AND tdv2.DetailFieldID = 304535 /*case summary eventtypeid*/  
   AND tdv2.valueint = @EventTypeID  
   WHERE cu.ClientID = 427  
   AND le.EventTypeID IN (tdv.DetailValue)  
   AND l.LeadID = @LeadID 
   AND ((l.LeadTypeID = 2046 AND c.CaseID = @CaseID ) OR l.LeadTypeID != 2046 )
   AND le.EventDeleted = 0  
   AND le.LeadDocumentID IS NOT NULL  
   AND NOT EXISTS (Select * from LeadEvent le_sub WITH (NOLOCK) where le_sub.LeadID = l.LeadID and le_sub.EventTypeID = et.EventTypeID and le_sub.LeadEventID > le.LeadEventID  )
   ORDER BY tdv1.DetailValue ASC'  
   
   /*CW 2018-09-21 Adding the not exists subquery, since the le join is not case specific it picks up all versions of the document across all cases.  
     Subquery restricts it to the latest version  
   */  
   /* TD 2018-10-01 Filter on @CaseID to prevent documents from other claims being picked up*/
 END  
 */
   
 IF @SQLOverrideQuery IS NOT NULL  
 BEGIN  
   
  EXEC _C00_LogIt 'Info', 'EventTypeAttachment__GetDocumentAttachments', 'SQLOverride', '', NULL  
  
  EXECUTE sp_executesql   
   @SQLOverrideQuery,   
   N'@CaseID INT, @EventTypeID INT, @IncludeDocumentBlob BIT, @LeadID INT, @ClientID INT, @EventSubtypeID INT',   
   @CaseID, @EventTypeID, @IncludeDocumentBlob, @LeadID, @ClientID, @EventSubtypeID  
  
 END  
 ELSE  
 BEGIN  
    
 /* Inserts all attachment types except gathering all of an event group type*/  
 INSERT INTO @EventTypes (AttachmentEventTypeID, [ALL], AttachmentEventGroupID)  
 SELECT AttachmentEventTypeID, [ALL], AttachmentEventGroupID  
 FROM EventTypeAttachment WITH (NOLOCK)  
 WHERE EventTypeID = @EventTypeID  
 AND ([ALL] = 0 OR AttachmentEventGroupID IS NULL)  
    
 /*If using Group ID, update AttachmentEventTypeID to the most recent event type of that group on that case.   
  IsNull to prevent cases where no events of that group exist being passed through as null*/  
  UPDATE et  
  SET AttachmentEventTypeID = ISNULL(  
  (SELECT TOP 1 EventTypeID  
  FROM LeadEvent le WITH ( NOLOCK )  
  WHERE CaseID = @CaseID  
   AND EventDeleted = 0  
   AND LeadDocumentID <> ''  
      AND EventTypeID IN (SELECT EventTypeID  
   FROM EventGroupMember WITH ( NOLOCK )  
   WHERE EventGroupID = et.AttachmentEventGroupID)  
  ORDER BY Le.WhenCreated DESC),(SELECT TOP 1 EventTypeID   
  FROM EventGroupMember WITH ( NOLOCK )  
  WHERE EventGroupID = et.AttachmentEventGroupID))  
  FROM @EventTypes et  
  WHERE et.AttachmentEventGroupID IS NOT NULL  
    
 /* Inserts all eventtypes of an event group type*/   
   
 INSERT INTO @EventTypes (AttachmentEventTypeID, [ALL], AttachmentEventGroupID)  
 SELECT et.EventTypeID, 1, AttachmentEventGroupID  
 FROM EventTypeAttachment eta WITH (NOLOCK)  
 INNER JOIN EventGroupMember et WITH ( NOLOCK ) on et.EventGroupID = eta.AttachmentEventGroupID  
 WHERE eta.EventTypeID = @EventTypeID  
 AND [ALL] = 1   
 AND AttachmentEventGroupID IS NOT NULL  
  
  -- From the list of all possible AttachmentEventTypeIDs, use the value of the   
  -- [ALL] flag in the table to get either:   
  -- 1 = every single LeadEvent of that EventTypeID for the CaseID passed in, or  
  -- 0 = just the latest LeadEvent of each EventTypeID for the CaseID passed in.  
  
  -- Scenario 1: get all of the [ALL] LeadEvents in one hit  
  INSERT INTO @Documents ([WhenCreated], [DocumentBLOB], [FileName], [DocumentTypeID], [InputFormat], [OutputFormat],[DocumentFormat], EventTypeID, CaseID,  LeadEventID, LeadDocumentID, LeadDocumentTitle, ZipFormat, EventTypeName, AttachmentID)  
  SELECT e.[WhenCreated]   ,  
  CASE @IncludeDocumentBlob WHEN 1 THEN d.DocumentBLOB ELSE NULL END         ,  
  ISNULL(d.[FileName], d.LeadDocumentTitle + CONVERT(VARCHAR, d.LeadDocumentID) )         ,   
  ISNULL(d.[DocumentTypeID], -1)     ,  
  ISNULL(dt.[InputFormat], '')   ,  
  ISNULL(dt.[OutputFormat], '')  ,  
  ISNULL(d.[DocumentFormat], '')  ,   
  e.EventTypeID,   
  e.CaseID,  
  e.LeadEventID,  
  d.LeadDocumentID,  
  d.LeadDocumentTitle,  
  d.[ZipFormat],  
  et.[EventTypeName],  
  at.AttachmentID  
  FROM @EventTypes AT   
  INNER JOIN [LeadEvent] e WITH (NOLOCK)ON  e.[EventTypeID] = at.[AttachmentEventTypeID]  
  INNER JOIN dbo.vLeadDocumentList d ON  e.[LeadDocumentID] = d.[LeadDocumentID]  
  INNER JOIN [EventType] et WITH (NOLOCK) ON et.EventTypeID = e.EventTypeID  
  LEFT JOIN DocumentType dt WITH (NOLOCK) ON  dt.[DocumentTypeID]   = d.[DocumentTypeID]  
  WHERE (e.[CaseID]             = @CaseID OR (e.LeadID = @LeadID AND @AllForLead = 1) )  
  AND at.[ALL]                  = 1  
  AND e.EventDeleted     = 0  
  and   
  (  
  @SinceLastSummary = 0  
  or (  
    (CASE WHEN @EventSubtypeID = 20 THEN 1 ELSE 0 END)  
    =   
    (  
    SELECT COUNT(*) /*Can't say NOT EXISTS, because that will exclude this event as well!*/  
    FROM LeadEvent sle WITH (NOLOCK)   
    WHERE (sle.[CaseID] = @CaseID OR (sle.LeadID = @LeadID AND @AllForLead = 1) )  
    and sle.EventDeleted = 0  
    and sle.EventTypeID = @EventTypeID  
    and sle.LeadEventID > e.LeadEventID   
    )  
   ) 
  ) 
  
  -- Scenario 2: loop round all of the "latest only" LeadEvents, one event type at a time  
  SELECT TOP 1 @LoopEventTypeID = AttachmentEventTypeID,  
   @LoopEventAttachmentID    = AttachmentID  
  FROM @EventTypes  
  WHERE [ALL] = 0  
  
  WHILE @LoopEventTypeID > -1  
  BEGIN  
   -- Get the latest instance of a LeadEvent of this event type  
   INSERT INTO @Documents  
   SELECT TOP 1 e.[WhenCreated] ,  
   CASE @IncludeDocumentBlob WHEN 1 THEN d.DocumentBLOB ELSE NULL END         ,  
   CASE WHEN d.FileName IS NULL THEN ISNULL(d.[FileName], d.LeadDocumentTitle + CONVERT(VARCHAR, d.LeadDocumentID) )   
     WHEN d.FileName LIKE '%.rtf' AND d.DocumentFormat = 'PDF' THEN dbo.fnGetCharsBeforeOrAfterSeparator([FileName], '.', 0, 1) ELSE d.FileName END,  
   ISNULL(d.[DocumentTypeID], -1)     ,  
   ISNULL(dt.[InputFormat], '')   ,  
   ISNULL(dt.[OutputFormat], '')  ,  
   ISNULL(d.[DocumentFormat], '')  ,  
   e.EventTypeID,   
   e.LeadEventID,  
   e.CaseID,  
   d.LeadDocumentID,  
   d.LeadDocumentTitle,  
   d.ZipFormat,  
   et.[EventTypeName],  
   @LoopEventAttachmentID /*Attachment ID From the loop start and get next*/  
   FROM  [LeadEvent] e WITH (NOLOCK)  
   INNER JOIN dbo.vLeadDocumentList d ON  e.[LeadDocumentID] = d.[LeadDocumentID]  
   INNER JOIN [EventType] et WITH (NOLOCK) ON et.EventTypeID = e.EventTypeID  
   LEFT JOIN DocumentType dt WITH (NOLOCK) ON  dt.[DocumentTypeID]   = d.[DocumentTypeID]  
   WHERE e.[CaseID]              = @CaseID  
   AND   e.[EventTypeID]         = @LoopEventTypeID  
   AND   e.EventDeleted    = 0  
   ORDER BY e.[WhenCreated] DESC   
  
   -- Handle the loop processing  
   DELETE   
   FROM @EventTypes   
   WHERE AttachmentID   = @LoopEventAttachmentID  
  
   -- Get the next event type in the loop  
   SELECT TOP 1 @LoopEventTypeID = AttachmentEventTypeID,  
    @LoopEventAttachmentID    = AttachmentID  
   FROM @EventTypes  
   WHERE [ALL] = 0  
     
   -- Exit when there are no more event types in the list  
   IF @@ROWCOUNT = 0  
   BEGIN  
    SELECT @LoopEventTypeID = -1  
   END  
  
  END  
  
  -- Send the results back to the calling app    
 SELECT * FROM @Documents   
 ORDER BY WhenCreated ASC; 
  
 END  
  
END


GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment__GetDocumentAttachments] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAttachment__GetDocumentAttachments] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment__GetDocumentAttachments] TO [sp_executeall]
GO
