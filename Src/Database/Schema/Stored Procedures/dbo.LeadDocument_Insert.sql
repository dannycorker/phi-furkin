SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LeadDocument table
-- JWG 2011-06-01 Quadra Filename fix (email-in events arrive with all sorts of invalid chars in the filename)
-- JWG 2012-07-12 Use all fields for LeadDocument again.
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[LeadDocument_Insert]
(

	@LeadDocumentID INT OUTPUT,
	@ClientID INT,
	@LeadID INT,
	@DocumentTypeID INT,
	@LeadDocumentTitle VARCHAR(1000),
	@UploadDateTime DATETIME,
	@WhoUploaded INT,
	@DocumentBLOB IMAGE,
	@FileName VARCHAR(255),
	@EmailBLOB IMAGE,
	@DocumentFormat VARCHAR(24) ,
	@EmailFrom VARCHAR(512),
	@EmailTo VARCHAR(MAX),
	@CcList VARCHAR(MAX),
	@BccList VARCHAR(MAX),
	@ElectronicSignatureDocumentKey VARCHAR(50),
	@Encoding VARCHAR(50),
	@ContentFormat VARCHAR(50),
	@ZipFormat VARCHAR(50), 
	@DocumentBlobSize INT = NULL, 
	@EmailBlobSize INT = NULL, 
	@DocumentDatabaseID INT = NULL, 
	@WhenArchived DATETIME = NULL, 
	@DocumentTypeVersionID INT = NULL 
)
AS
BEGIN
	
	/* JWG 2011-06-01 Quadra Filename fix (email-in events arrive with all sorts of invalid chars in the filename) */
	/* JWG 2013-02-26 Include C168 as well until the problem chars are removed at source by the Outlook addin */
	/*IF @ClientID = 173 AND @FileName > ''*/
	IF @ClientID IN (168, 173) AND @FileName > ''
	BEGIN
		SELECT @FileName = dbo.fnGetValidFileName(@FileName, 0) 
	END
	/* JWG #25150 Check for Word extended hyphen (CHAR 150) and replace all invalid chars if one is found */
	IF @FileName LIKE '%' + CHAR(150) + '%'
	BEGIN
		SELECT @FileName = dbo.fnGetValidFileName(@FileName, 0) 
	END	
	
	-- JWG 2012-07-12 Use all fields for LeadDocument again. Temp size calc until app sends these through.
	SELECT @DocumentBlobSize = DATALENGTH(@DocumentBLOB), @EmailBlobSize = DATALENGTH(@EmailBLOB)
	
	/* Use the new table when we are all caught up... */
	INSERT INTO [dbo].[LeadDocument]
		(
			ClientID, 
			LeadID, 
			DocumentTypeID, 
			LeadDocumentTitle, 
			UploadDateTime, 
			WhoUploaded, 
			DocumentBLOB, 
			FileName, 
			EmailBLOB, 
			DocumentFormat, 
			EmailFrom, 
			EmailTo, 
			CcList, 
			BccList, 
			ElectronicSignatureDocumentKey, 
			Encoding, 
			ContentFormat, 
			ZipFormat, 
			DocumentBlobSize, 
			EmailBlobSize, 
			DocumentDatabaseID, 
			WhenArchived, 
			DocumentTypeVersionID		
		)
	VALUES
		(
			@ClientID,
			@LeadID,
			@DocumentTypeID,
			@LeadDocumentTitle,
			@UploadDateTime,
			@WhoUploaded,
			CAST('' AS varbinary),
			@FileName,
			NULL,
			@DocumentFormat,
			@EmailFrom,
			@EmailTo,
			@CcList,
			@BccList,
			@ElectronicSignatureDocumentKey,
			@Encoding,
			@ContentFormat,
			@ZipFormat, 
			@DocumentBlobSize, 
			@EmailBlobSize, 
			NULL, 
			NULL, 
			@DocumentTypeVersionID 
		)
	
	-- Get the identity value
	SET @LeadDocumentID = SCOPE_IDENTITY()

	SET IDENTITY_INSERT [dbo].[LeadDocumentFS] ON
	
	INSERT INTO [dbo].[LeadDocumentFS]
		([LeadDocumentID]
		,[ClientID]
		,[LeadID]
		,[DocumentTypeID]
		,[LeadDocumentTitle]
		,[UploadDateTime]
		,[WhoUploaded]
		,[DocumentBLOB]
		,[FileName]
		,[EmailBLOB]
		,[DocumentFormat]
		,[EmailFrom]
		,[EmailTo]
		,[CcList]
		,[BccList]
		,[ElectronicSignatureDocumentKey]
		,[Encoding]
		,[ContentFormat]
		,[ZipFormat]
		)
	VALUES
		(@LeadDocumentID
		,@ClientID
		,@LeadID
		,@DocumentTypeID
		,@LeadDocumentTitle
		,@UploadDateTime
		,@WhoUploaded
		,CASE 
			WHEN @DocumentBLOB LIKE 0x THEN NULL 
			ELSE @DocumentBLOB 
		END
		,@FileName
		,CASE 
			WHEN @EmailBLOB LIKE 0x THEN NULL 
			ELSE @EmailBLOB 
		END
		,@DocumentFormat
		,@EmailFrom
		,@EmailTo
		,@CcList
		,@BccList
		,@ElectronicSignatureDocumentKey
		,@Encoding
		,@ContentFormat
		,@ZipFormat
		)

	SET IDENTITY_INSERT [dbo].[LeadDocumentFS] OFF
	
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocument_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_Insert] TO [sp_executeall]
GO
