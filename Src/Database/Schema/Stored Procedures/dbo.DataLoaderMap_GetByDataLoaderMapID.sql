SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderMap table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderMap_GetByDataLoaderMapID]
(

	@DataLoaderMapID int   
)
AS


				SELECT
					[DataLoaderMapID],
					[ClientID],
					[MapName],
					[MapDescription],
					[WhenCreated],
					[CreatedBy],
					[LastUpdated],
					[UpdatedBy],
					[EnabledForUse],
					[Deleted],
					[LeadTypeID],
					[UseDefaultLeadForCustomer],
					[UseDefaultCaseForLead],
					[UseDefaultMatterForCase]
				FROM
					[dbo].[DataLoaderMap] WITH (NOLOCK) 
				WHERE
										[DataLoaderMapID] = @DataLoaderMapID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMap_GetByDataLoaderMapID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMap_GetByDataLoaderMapID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMap_GetByDataLoaderMapID] TO [sp_executeall]
GO
