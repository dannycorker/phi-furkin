SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-10-07
-- Description:	Add a payment or refund row for either BACS (DD) or Datacash (CC)
-- Mods
-- DCM 2015-06-11 #32773 Monthly premium refund
-- DCM 2015-07-23 select latest ready row by sorting on from date
-- DCM 2015-12-21 record premium calc ID
-- DCM 2016-03-02 clear date paid if adjustment row is made from a copied row
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600DEL_AddAdjustmentRow] 
(
	@MatterID INT,
	@WhoModified INT = NULL 
)

AS
BEGIN

	SET NOCOUNT ON;


	DECLARE	
			@AdjustmentType VARCHAR(50),
			@AdjustmentTypeID INT,
			@AccName VARCHAR(100),
			@AccNumber VARCHAR(10),
			@AccSortCode VARCHAR(10),
			@Amount VARCHAR(20),
			@AmountMoney MONEY,
			@BACSCode VARCHAR(5),
			@ClientID INT = dbo.fnGetPrimaryClientID(),
			@EffectiveDate DATE,
			@AffinitySC VARCHAR(10),
			@PaymentType VARCHAR(10),
			@PaymentInterval INT,
			@PayRef VARCHAR(100),
			@PremiunCalcID INT,
			@TableRowID INT
			
	

	-- Get affinity short code for pay ref
	SELECT @AffinitySC=scli.ItemValue, 
			@WhoModified=CASE WHEN @WhoModified IS NULL THEN dbo.fnGetKeyValueAsIntFromThirdPartyIDs (sc.ClientID,53,'CFG|AqAutomationCPID',0) ELSE @WhoModified END
	FROM CustomerDetailValues sc WITH (NOLOCK) 
	INNER JOIN Lead cl WITH (NOLOCK) ON sc.CustomerID=cl.CustomerID
	INNER JOIN Matter cm WITH (NOLOCK) ON cm.LeadID=cl.LeadID AND cm.MatterID=@MatterID
	INNER JOIN ResourceListDetailValues scrl WITH (NOLOCK) ON sc.ValueInt=scrl.ResourceListID and scrl.DetailFieldID=170127
	INNER JOIN LookupListItems scli WITH (NOLOCK) ON scrl.ValueInt=scli.LookupListItemID
	WHERE sc.DetailFieldID=170144

	-- Get relevant info from collections matter
	SELECT @PayRef= @AffinitySC + CONVERT(VARCHAR(30),@MatterID), 
	@AccName=CASE WHEN ptype.ValueInt=69930 THEN an.DetailValue ELSE ccref.DetailValue END, 
	@AccSortCode=acs.DetailValue, @AccNumber=anum.DetailValue,
	@PaymentType=ptype.DetailValue, @PaymentInterval=pint.ValueInt,
	@BACSCode=	CASE WHEN ptype.ValueInt=69930 THEN
					CASE WHEN atype.ValueInt=70019 THEN '17' ELSE '99' END
				ELSE '' END,
	@Amount=amt.DetailValue,
	@AmountMoney=amt.ValueMoney,
	@AdjustmentType=ISNULL(atypeli.ItemValue,''),
	@AdjustmentTypeID=atypeli.LookupListItemID
	FROM MatterDetailValues an WITH (NOLOCK)
	INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON an.MatterID=ltr.ToMatterID AND ltr.FromLeadTypeID=1492 AND ltr.ToLeadTypeID=1493 
	INNER JOIN MatterDetailValues acs WITH (NOLOCK) ON acs.MatterID=an.MatterID and acs.DetailFieldID=170189
	INNER JOIN MatterDetailValues ptype WITH (NOLOCK) ON ptype.MatterID=an.MatterID and ptype.DetailFieldID=170115
	INNER JOIN MatterDetailValues amt WITH (NOLOCK) ON amt.MatterID=ltr.FromMatterID and amt.DetailFieldID=175379
	INNER JOIN MatterDetailValues atype WITH (NOLOCK) ON atype.MatterID=ltr.FromMatterID and atype.DetailFieldID=175345
	LEFT JOIN LookupListItems atypeli WITH (NOLOCK) ON atype.ValueInt=atypeli.LookupListItemID
	LEFT JOIN MatterDetailValues ccref WITH (NOLOCK) ON ccref.MatterID=an.MatterID and ccref.DetailFieldID=170191
	INNER JOIN MatterDetailValues pint WITH (NOLOCK) ON pint.MatterID=an.MatterID and pint.DetailFieldID=170145
	INNER JOIN MatterDetailValues anum WITH (NOLOCK) ON anum.MatterID=an.MatterID and anum.DetailFieldID=170190
	WHERE an.DetailFieldID=170188 
	AND ltr.FromMatterID=@MatterID
	
	-- If there is a row that is still at ready, just adjust the premium value in the row & clear the adjustment amount (if any)
	SELECT TOP 1 @TableRowID=stat.TableRowID 
	FROM TableDetailValues stat WITH (NOLOCK) 
	INNER JOIN TableDetailValues fd WITH (NOLOCK) ON stat.TableRowID=fd.TableRowID AND fd.DetailFieldID=170076
	WHERE stat.MatterID=@MatterID AND stat.DetailFieldID=170078 AND stat.ValueInt=72154
	ORDER BY fd.ValueDate DESC

	IF @TableRowID IS NOT NULL
	BEGIN
	
		-- clear adjustment value
		EXEC _C00_SimpleValueIntoField 175379,'',@MatterID,@WhoModified
		
		-- update premium value
		SELECT @Amount=CASE WHEN pt.ValueInt=72053 THEN ann.DetailValue
					ELSE
					CASE WHEN pm.ValueInt=69896 THEN mf.DetailValue ELSE mr.DetailValue END
					END,
			   @PremiunCalcID=pcid.DetailValue
		FROM MatterDetailValues ann WITH (NOLOCK) 
		INNER JOIN MatterDetailValues pcid WITH (NOLOCK) ON ann.MatterID=pcid.MatterID AND pcid.DetailFieldID=176175
		INNER JOIN MatterDetailValues mf WITH (NOLOCK) ON ann.MatterID=mf.MatterID AND mf.DetailFieldID=175338
		INNER JOIN MatterDetailValues mr WITH (NOLOCK) ON ann.MatterID=mr.MatterID AND mr.DetailFieldID=175339
		INNER JOIN TableDetailValues pt WITH (NOLOCK) ON ann.MatterID=pt.MatterID AND pt.TableRowID=@TableRowID AND pt.DetailFieldID=175383
		LEFT JOIN TableDetailValues pm WITH (NOLOCK) ON pt.TableRowID=pm.TableRowID AND pm.DetailFieldID=170067
		WHERE ann.MatterID=@MatterID AND ann.DetailFieldID=175337 

		EXEC _C00_SimpleValueIntoField 170072,@Amount,@TableRowID,@WhoModified -- premium table
		EXEC _C00_SimpleValueIntoField 170086,@Amount,@MatterID,@WhoModified -- latest premium
		
		EXEC _C00_SimpleValueIntoField 175710,@PremiunCalcID,@TableRowID,@WhoModified -- premium table
		EXEC _C600_SetPremiumDetailHistoryPCID @MatterID -- premium detail history table
	
	END
	
	-- zen#32773
	--ELSE IF @PaymentInterval=69944 AND @AmountMoney <> 0 -- only add row for annually paid premiums and amount is not zero
	ELSE IF @AmountMoney <> 0
	BEGIN
		
		-- Add row to premiums table
		-- copy most recent
		DECLARE @PremiumTableRowID INT,
				@NewPremiumTableRowID INT,
				@Today VARCHAR(10),
				@RunAsAqAutomation INT = 33147
				
		SELECT TOP 1 @PremiumTableRowID=tr.TableRowID 
		FROM TableRows tr WITH (NOLOCK) 
		WHERE tr.DetailFieldID=170088 AND tr.MatterID=@MatterID
		ORDER BY TableRowID DESC
			
		EXEC @NewPremiumTableRowID=_C600_CopyTableRow @PremiumTableRowID, NULL, NULL
		 
		SELECT @Today=CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120)
		EXEC dbo._C00_SimpleValueIntoField 170066, @Today, @NewPremiumTableRowID, @RunAsAqAutomation
		EXEC dbo._C00_SimpleValueIntoField 170073, '', @NewPremiumTableRowID, @RunAsAqAutomation -- CRID
		EXEC dbo._C00_SimpleValueIntoField 170078, 72154, @NewPremiumTableRowID, @RunAsAqAutomation -- status
		EXEC dbo._C00_SimpleValueIntoField 170067, '', @NewPremiumTableRowID, @RunAsAqAutomation -- month
		EXEC dbo._C00_SimpleValueIntoField 175383, 72140, @NewPremiumTableRowID, @RunAsAqAutomation -- pay type (adjustment)
		SELECT @AmountMoney=CASE WHEN @AdjustmentTypeID=70020 THEN -(@AmountMoney) ELSE @AmountMoney END
		EXEC dbo._C00_SimpleValueIntoField 170072, @AmountMoney, @NewPremiumTableRowID, @RunAsAqAutomation -- amount
		EXEC dbo._C00_SimpleValueIntoField 175597, @Today, @NewPremiumTableRowID, @RunAsAqAutomation -- due date 
		EXEC dbo._C00_SimpleValueIntoField 175473, '', @NewPremiumTableRowID, @RunAsAqAutomation -- date paid

		select @NewPremiumTableRowID		
		
	END
				
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600DEL_AddAdjustmentRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600DEL_AddAdjustmentRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600DEL_AddAdjustmentRow] TO [sp_executeall]
GO
