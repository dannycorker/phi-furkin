SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the LogLevel table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LogLevel_Get_List]

AS


				
				SELECT
					[LogLevelID],
					[LogLevelName],
					[LogLevelDescription]
				FROM
					[dbo].[LogLevel] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LogLevel_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LogLevel_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LogLevel_Get_List] TO [sp_executeall]
GO
