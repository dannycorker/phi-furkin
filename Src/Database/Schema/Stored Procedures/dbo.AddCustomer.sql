SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









/****** Object:  Stored Procedure dbo.AddCustomer    Script Date: 08/09/2006 12:22:37 ******/

CREATE PROCEDURE [dbo].[AddCustomer]
@ClientID int,
@TitleID int,
@FirstName nvarchar(100),
@MiddleName nvarchar(100),
@LastName nvarchar(100),
@EmailAddress nvarchar(255),
@DayTimeTelephoneNumber nvarchar(50),
@DayTimeTelephoneNumberVerifiedAndValid bit,
@HomeTelephone nvarchar(50),
@HomeTelephoneVerifiedAndValid bit,
@MobileTelephone nvarchar(50),
@MobileTelephoneVerifiedAndValid bit,
@Address1 nvarchar(200),
@Address2 nvarchar(200), 
@Town nvarchar(200),
@County nvarchar(200),
@PostCode nvarchar(10),
@Test bit,
@CompanyName nvarchar(100),
@CompanyTelephone nvarchar(50),
@CompanyTelephoneVerifiedAndValid bit,
@WorksTelephone nvarchar(50),
@WorksTelephoneVerifiedAndValid bit,
@Occupation nvarchar(100),
@Employer nvarchar(100),
@DoNotEmail bit,
@DoNotSellToThirdParty bit,
@AgreedToTermsAndConditions bit,
@DateOfBirth dateTime = null,
@IsBusiness bit,
@DefaultContactID int


as

insert into Customers (ClientID, TitleID, FirstName, MiddleName, LastName, EmailAddress, DayTimeTelephoneNumber, DayTimeTelephoneNumberVerifiedAndValid,
HomeTelephone, HomeTelephoneVerifiedAndValid, 
MobileTelephone, MobileTelephoneVerifiedAndValid,
Address1, Address2, Town, County, PostCode, Test, CompanyName, 
CompanyTelephone, CompanyTelephoneVerifiedAndValid,
WorksTelephone, WorksTelephoneVerifiedAndValid, 
Occupation, Employer, DoNotEmail, DoNotSellToThirdParty, AgreedToTermsAndConditions, DateOfBirth, IsBusiness, DefaultContactID)
values (@ClientID, @TitleID, @FirstName, @MiddleName, @LastName, @EmailAddress, 
@DayTimeTelephoneNumber, @DayTimeTelephoneNumberVerifiedAndValid, 
@HomeTelephone, @HomeTelephoneVerifiedAndValid, 
@MobileTelephone, @MobileTelephoneVerifiedAndValid,
@address1,@Address2, @Town, @County, @PostCode, @Test, @CompanyName, 
@CompanyTelephone, @CompanyTelephoneVerifiedAndValid, 
@WorksTelephone, @WorksTelephoneVerifiedAndValid,
@Occupation, @Employer, @DoNotEmail, @DoNotSellToThirdParty, @AgreedToTermsAndConditions, @DateOfBirth, @IsBusiness, @DefaultContactID)

declare @CustomerID int

set @CustomerID = SCOPE_IDENTITY()

Select CustomerID from Customers where CustomerID = @CustomerID







GO
GRANT VIEW DEFINITION ON  [dbo].[AddCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddCustomer] TO [sp_executeall]
GO
