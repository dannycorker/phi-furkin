SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the QuillCaseMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillCaseMapping_Update]
(

	@QuillCaseMappingID int   ,

	@ClientID int   ,

	@CaseID int   ,

	@QuillCaseCode varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[QuillCaseMapping]
				SET
					[ClientID] = @ClientID
					,[CaseID] = @CaseID
					,[QuillCaseCode] = @QuillCaseCode
				WHERE
[QuillCaseMappingID] = @QuillCaseMappingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCaseMapping_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillCaseMapping_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCaseMapping_Update] TO [sp_executeall]
GO
