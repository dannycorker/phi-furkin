SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
	Undocumented old proc from 2006-09-08. Please comment it if you recognise it.
*/
CREATE PROCEDURE [dbo].[GetCustomersForClient] 
	@ClientID INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT CustomerID, 
	ClientID, 
	TitleID, 
	FirstName, 
	MiddleName, 
	LastName, 
	EmailAddress, 
	DayTimeTelephoneNumber, 
	HomeTelephone, 
	MobileTelephone, 
	Address1, 
	Address2, 
	Town, 
	County, 
	PostCode 
	FROM dbo.Customers c WITH (NOLOCK) 
	WHERE c.ClientID = @ClientID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomersForClient] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetCustomersForClient] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomersForClient] TO [sp_executeall]
GO
