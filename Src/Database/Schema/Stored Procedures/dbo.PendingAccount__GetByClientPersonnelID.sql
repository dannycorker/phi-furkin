SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 09-01-2014
-- Description:	Gets a pending account record by the clientpersonnelID
-- =============================================
CREATE PROCEDURE [dbo].[PendingAccount__GetByClientPersonnelID]

	@ClientPersonnelID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT * FROM PendingAccount WITH (NOLOCK) WHERE ClientPersonnelID=@ClientPersonnelID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[PendingAccount__GetByClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PendingAccount__GetByClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PendingAccount__GetByClientPersonnelID] TO [sp_executeall]
GO
