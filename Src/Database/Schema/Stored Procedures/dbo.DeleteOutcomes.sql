SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.DeleteOutcomes    Script Date: 08/09/2006 12:22:41 ******/

CREATE PROCEDURE [dbo].[DeleteOutcomes] @OutcomeID int
AS
Delete From Outcomes
Where OutcomeID = @OutcomeID





GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteOutcomes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteOutcomes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteOutcomes] TO [sp_executeall]
GO
