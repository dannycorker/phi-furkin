SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ObjectDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectDetailValues_Delete]
(

	@ObjectDetailValueID int   
)
AS


				DELETE FROM [dbo].[ObjectDetailValues] WITH (ROWLOCK) 
				WHERE
					[ObjectDetailValueID] = @ObjectDetailValueID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectDetailValues_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectDetailValues_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectDetailValues_Delete] TO [sp_executeall]
GO
