SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the BillItem table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BillItem_Get_List]

AS


				
				SELECT
					[BillItemID],
					[ClientID],
					[BillID],
					[Description],
					[ClientPersonnelID],
					[WorkUnits],
					[Total],
					[ChargeOutRate],
					[TotalIsCustomValue]
				FROM
					[dbo].[BillItem] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[BillItem_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillItem_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillItem_Get_List] TO [sp_executeall]
GO
