SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-09-23
-- Description:	Splits the claim onto a new case
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_SplitClaim] 
(
	@CaseID INT,
	@ClientPersonnelID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	-- New matter
	DECLARE @NewMatterID INT
	EXEC @NewMatterID = dbo._C00_CreateNewMatterForCase @CaseID, @ClientPersonnelID
	
	DECLARE @IDs TABLE
	(
		MatterID INT,
		CaseID INT
	)
	
	-- Move to new case
	DECLARE @NewMatterIDs dbo.tvpInt
	INSERT @NewMatterIDs(AnyID) VALUES (@NewMatterID)
	
	INSERT @IDs (CaseID)
	EXEC dbo._C00_SplitMatterToNewCase @NewMatterIDs
	
	UPDATE @IDs
	SET MatterID = @NewMatterID
	
	SELECT * 
	FROM @IDs

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_SplitClaim] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_SplitClaim] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_SplitClaim] TO [sp_executeall]
GO
