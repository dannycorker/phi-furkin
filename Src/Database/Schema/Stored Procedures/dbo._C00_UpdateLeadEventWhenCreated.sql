SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		TD
-- Create date: 2015-11-25
-- Description:	Change whencreated time
-- =============================================
CREATE PROCEDURE [dbo].[_C00_UpdateLeadEventWhenCreated] 
	-- Add the parameters for the stored procedure here
@LeadeventID INT
,@Whencreated DATETIME  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF @LeadeventID > 0 AND @Whencreated IS NOT NULL 
    BEGIN
		UPDATE LeadEvent
		SET WhenCreated = @Whencreated
		WHERE LeadEventID = @LeadeventID
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_UpdateLeadEventWhenCreated] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_UpdateLeadEventWhenCreated] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_UpdateLeadEventWhenCreated] TO [sp_executeall]
GO
