SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the CurrencyRate table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CurrencyRate_Insert]
(

	@CurrencyRateID int    OUTPUT,

	@CurrencyId int   ,

	@CurrencyCode char (3)  ,

	@FromGBPRate decimal (18, 10)  ,

	@ToGBPRate decimal (18, 10)  ,

	@ConversionDate datetime   ,

	@IsLatest bit   
)
AS


				
				INSERT INTO [dbo].[CurrencyRate]
					(
					[CurrencyId]
					,[CurrencyCode]
					,[FromGBPRate]
					,[ToGBPRate]
					,[ConversionDate]
					,[IsLatest]
					)
				VALUES
					(
					@CurrencyId
					,@CurrencyCode
					,@FromGBPRate
					,@ToGBPRate
					,@ConversionDate
					,@IsLatest
					)
				-- Get the identity value
				SET @CurrencyRateID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CurrencyRate_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate_Insert] TO [sp_executeall]
GO
