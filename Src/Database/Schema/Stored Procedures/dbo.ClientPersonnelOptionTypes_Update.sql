SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientPersonnelOptionTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelOptionTypes_Update]
(

	@ClientPersonnelOptionTypeID int   ,

	@OptionTypeName nchar (100)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientPersonnelOptionTypes]
				SET
					[OptionTypeName] = @OptionTypeName
				WHERE
[ClientPersonnelOptionTypeID] = @ClientPersonnelOptionTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptionTypes_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelOptionTypes_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptionTypes_Update] TO [sp_executeall]
GO
