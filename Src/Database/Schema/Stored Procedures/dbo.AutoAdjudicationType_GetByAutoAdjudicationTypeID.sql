SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AutoAdjudicationType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudicationType_GetByAutoAdjudicationTypeID]
(

	@AutoAdjudicationTypeID int   
)
AS


				SELECT
					[AutoAdjudicationTypeID],
					[AutoAdjudicationTypeName]
				FROM
					[dbo].[AutoAdjudicationType] WITH (NOLOCK) 
				WHERE
										[AutoAdjudicationTypeID] = @AutoAdjudicationTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationType_GetByAutoAdjudicationTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudicationType_GetByAutoAdjudicationTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationType_GetByAutoAdjudicationTypeID] TO [sp_executeall]
GO
