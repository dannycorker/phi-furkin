SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the CheckMateFieldMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateFieldMapping_Update]
(

	@CheckMateFieldMappingID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@CheckMateFieldID int   ,

	@FieldID int   ,

	@ColumnFieldID int   ,

	@DataLoaderObjectTypeID int   ,

	@CheckMateFieldGroupID int   ,

	@IsEnabled bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[CheckMateFieldMapping]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[CheckMateFieldID] = @CheckMateFieldID
					,[FieldID] = @FieldID
					,[ColumnFieldID] = @ColumnFieldID
					,[DataLoaderObjectTypeID] = @DataLoaderObjectTypeID
					,[CheckMateFieldGroupID] = @CheckMateFieldGroupID
					,[IsEnabled] = @IsEnabled
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[CheckMateFieldMappingID] = @CheckMateFieldMappingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldMapping_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateFieldMapping_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldMapping_Update] TO [sp_executeall]
GO
