SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-11-02
-- Description:	Copy event type with event choices, fields and attachments
-- JWG 2015-02-11 #30930 ForceEditableOverride
-- AMG 2015-06-25 Added new EventTypeOptions
-- =============================================
CREATE PROCEDURE [dbo].[EventType__Copy]
	@EventTypeID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @ToEventTypeID int
	
	INSERT EventType (ClientID, EventTypeName, EventTypeDescription, Enabled, UnitsOfEffort, FollowupTimeUnitsID, FollowupQuantity, AvailableManually, StatusAfterEvent, AquariumEventAfterEvent, EventSubtypeID, DocumentTypeID, LeadTypeID, AllowCustomTimeUnits, InProcess, KeyEvent, UseEventCosts, UseEventUOEs, UseEventDisbursements, UseEventComments, SignatureRequired, SignatureOverride, VisioX, VisioY, AquariumEventSubtypeID, WhoCreated, WhenCreated, WhoModified, WhenModified, FollowupWorkingDaysOnly, CalculateTableRows, SmsGatewayID)
	SELECT ClientID, left(EventTypeName,44) + '__Copy', left(EventTypeDescription,144) + '__Copy', 0, UnitsOfEffort, FollowupTimeUnitsID, FollowupQuantity, AvailableManually, StatusAfterEvent, AquariumEventAfterEvent, EventSubtypeID, DocumentTypeID, LeadTypeID, AllowCustomTimeUnits, InProcess, KeyEvent, UseEventCosts, UseEventUOEs, UseEventDisbursements, UseEventComments, SignatureRequired, SignatureOverride, VisioX, VisioY, AquariumEventSubtypeID, WhoCreated, WhenCreated, WhoModified, WhenModified, FollowupWorkingDaysOnly, CalculateTableRows, SmsGatewayID
	FROM EventType et 
	WHERE et.EventTypeID = @EventTypeID
	
	SELECT @ToEventTypeID = SCOPE_IDENTITY()
	
	IF @ToEventTypeID > 0 
	BEGIN

		INSERT INTO EventChoice (EventTypeID, Description, NextEventTypeID, ClientID, LeadTypeID, ThreadNumber, EscalationEvent, Field, LogicalOperator, Value1, Value2, SqlClauseForInclusion, Weighting)
		SELECT @ToEventTypeID, Description, NextEventTypeID, ClientID, LeadTypeID, ThreadNumber, EscalationEvent, Field, LogicalOperator, Value1, Value2, SqlClauseForInclusion, Weighting
		FROM EventChoice ec WITH (NOLOCK)
		WHERE ec.EventTypeID = @EventTypeID

		INSERT INTO EventChoice (EventTypeID, Description, NextEventTypeID, ClientID, LeadTypeID, ThreadNumber, EscalationEvent, Field, LogicalOperator, Value1, Value2, SqlClauseForInclusion, Weighting)
		SELECT EventTypeID, Description, @ToEventTypeID, ClientID, LeadTypeID, ThreadNumber, EscalationEvent, Field, LogicalOperator, Value1, Value2, SqlClauseForInclusion, Weighting
		FROM EventChoice ec WITH (NOLOCK)
		WHERE ec.NextEventTypeID = @EventTypeID
		
		INSERT EventTypeHelperField (EventTypeID, DetailFieldID, LeadTypeID, ClientID, ForceEditableOverride)
		SELECT @ToEventTypeID, DetailFieldID, LeadTypeID, ClientID, ForceEditableOverride
		FROM EventTypeHelperField hf
		WHERE hf.EventTypeID = @EventTypeID

		INSERT EventTypeMandatoryField (EventTypeID, DetailFieldID, LeadTypeID, ClientID)
		SELECT @ToEventTypeID, DetailFieldID, LeadTypeID, ClientID
		FROM EventTypeMandatoryField hf
		WHERE hf.EventTypeID = @EventTypeID
		
		INSERT EventTypeFieldCompletion (ClientID, EventTypeID, DetailFieldID, InsertOptionID, DeleteOptionID)
		SELECT ClientID, @ToEventTypeID, DetailFieldID, InsertOptionID, DeleteOptionID
		FROM EventTypeFieldCompletion fc
		WHERE fc.EventTypeID = @EventTypeID
		
		INSERT EventTypeAttachment (EventTypeID, AttachmentEventTypeID, [All], [AttachmentEventGroupID])
		SELECT @ToEventTypeID, AttachmentEventTypeID, [All], AttachmentEventGroupID
		FROM EventTypeAttachment ea 
		WHERE ea.EventTypeID = @EventTypeID
		
		INSERT EventGroupMember (ClientID, EventGroupID, EventTypeID, EventGroupMemberDescription, WhoCreated, WhenCreated, WhoModified, WhenModified, ChangeNotes, SourceID)
		SELECT ClientID, EventGroupID, @ToEventTypeID, EventGroupMemberDescription, WhoCreated, WhenCreated, WhoModified, WhenModified, 'Copied from ' + convert(varchar,@EventTypeID) , @EventTypeID
		FROM EventGroupMember em WITH (NOLOCK) 
		WHERE em.EventTypeID = @EventTypeID
		
		-- AMG 2015-06-25 Added new EventTypeOptions
		INSERT EventTypeOption (ClientID, EventTypeID, AquariumOptionID, OptionValue, WhoCreated, WhenCreated, WhoModified, WhenModified, SourceID)
		SELECT ClientID, @ToEventTypeID, AquariumOptionID, OptionValue, WhoCreated, WhenCreated, WhoModified, WhenModified, SourceID
		FROM EventTypeOption eto WITH (NOLOCK)
		WHERE eto.EventTypeID = @EventTypeID
		
	END
	ELSE
	BEGIN
		SELECT @ToEventTypeID = -1
	END

	SELECT @ToEventTypeID ToEventTypeID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__Copy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__Copy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__Copy] TO [sp_executeall]
GO
