SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-08-10
-- Description:	SP To Update detailfield with new reference number
-- =============================================
CREATE PROCEDURE [dbo].[_C00_UpdateDVRefInt]
	@CaseID int,
	@DetailFieldID int
AS
BEGIN

	SET NOCOUNT ON;

	Declare @LeadOrMatter int,
			@LastReferenceInteger int,
			@LeadID int
			

	Select @LeadOrMatter = LeadOrMatter, @LastReferenceInteger = LastReferenceInteger
	From DetailFields
	Where DetailFieldID = @DetailFieldID

	Update DetailFields
	Set LastReferenceInteger = @LastReferenceInteger + 1
	Where DetailFieldID = @DetailFieldID

	Select @LeadID = LeadID
	From Cases
	Where CaseID = @CaseID

	exec dbo._C00_CreateDetailFields @DetailFieldID,@LeadID

	If @LeadOrMatter = 1
	Begin

		Update LeadDetailValues
		Set DetailValue = @LastReferenceInteger + 1
		From LeadDetailValues
		Where LeadDetailValues.DetailFieldID = @DetailFieldID
		And LeadDetailValues.LeadID = @LeadID

	End

	If @LeadOrMatter = 2
	Begin

		Update MatterDetailValues
		Set DetailValue = @LastReferenceInteger + 1
		From MatterDetailValues
		Inner Join Matter m on m.MatterID = MatterDetailValues.MatterID and m.CaseID = @CaseID
		Where MatterDetailValues.DetailFieldID = @DetailFieldID

	End


END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_UpdateDVRefInt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_UpdateDVRefInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_UpdateDVRefInt] TO [sp_executeall]
GO
