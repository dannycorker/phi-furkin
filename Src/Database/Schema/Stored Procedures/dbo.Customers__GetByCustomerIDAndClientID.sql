SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 13-04-2016
-- Description:	Gets a customer via its identity and clientID
-- =============================================
CREATE PROCEDURE [dbo].[Customers__GetByCustomerIDAndClientID]
	@ClientID INT,
	@CustomerID INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT * FROM Customers WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND CustomerID=@CustomerID    
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetByCustomerIDAndClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__GetByCustomerIDAndClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetByCustomerIDAndClientID] TO [sp_executeall]
GO
