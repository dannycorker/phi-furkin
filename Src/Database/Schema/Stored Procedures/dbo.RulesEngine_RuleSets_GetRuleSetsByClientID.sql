SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:        Simon Brushett
-- Create date:   2013-05-29
-- Description:   Returns all rule sets for a specific client

-- Modified By:   Austin Davies
-- Modified:      2014-05-23
-- Modifications: Added a count for Rules which are children
--                of each RulesSet
-- Modified By:   Jan Wilson
-- Modified:      2014-05-28
--                Added column ExposeForUse
-- CS 2016-11-10  cast as VARCHAR when comparing against RulesEngine_RuleParameters.Value
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleSets_GetRuleSetsByClientID]
(
      @ClientID INT,
      @RuleSetID INT = NULL
)


AS
BEGIN
      
      SET NOCOUNT ON;
      
      -- TODO Replace with a function call?
      --;WITH RuleSetUsage AS 
      --(
      --	SELECT ValueInt AS RuleSetID, COUNT(*) AS NumReferences
      --	FROM dbo.TableDetailValues WITH (NOLOCK) 
      --	WHERE DetailFieldID = 180108
      --	GROUP BY ValueInt
      --)
      
      

      SELECT      rs.RuleSetID, rs.Name, rs.InputTypeID, dtIn.Name AS InputType, rs.OutputTypeID, dtOut.Name AS OutputType,
                  rs.WhenCreated, cpCreate.UserName AS WhoCreated, rs.WhenModified, cpMod.UserName AS WhoModified, rs.Description,
                  rs.ExposeForUse, ISNULL(rs.RuleSetTypeID, 0) AS RuleSetTypeID,
                  (SELECT COUNT(*) FROM dbo.RulesEngine_Rules RE_R WITH (NOLOCK) WHERE RE_R.RuleSetID = rs.RuleSetID ) AS RulesCount,
                  (SELECT COUNT(*) FROM dbo.RulesEngine_RuleParameters WHERE ParameterTypeID = 3 AND Value = CAST(rs.RuleSetID as VARCHAR)) AS NumReferences --+ ISNULL(u.NumReferences, 0) NumReferences /*CS 2016-11-10 cast as VARCHAR when comparing against RuleParameters*/
      FROM dbo.RulesEngine_RuleSets rs WITH (NOLOCK) 
      INNER JOIN dbo.RulesEngine_DataTypes dtIn WITH (NOLOCK) ON rs.InputTypeID = dtIn.TypeID
      INNER JOIN dbo.RulesEngine_DataTypes dtOut WITH (NOLOCK) ON rs.OutputTypeID = dtOut.TypeID
      INNER JOIN dbo.ClientPersonnel cpCreate WITH (NOLOCK) ON rs.WhoCreated = cpCreate.ClientPersonnelID
      INNER JOIN dbo.ClientPersonnel cpMod WITH (NOLOCK) ON rs.WhoModified = cpMod.ClientPersonnelID
      --LEFT JOIN RuleSetUsage u ON rs.RuleSetID = u.RuleSetID
      WHERE rs.ClientID = @ClientID
      AND (@RuleSetID IS NULL OR rs.RuleSetID = @RuleSetID)
      
      SELECT 'Premiums Engine' AS Title, '#/' AS Path
      
END



GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleSets_GetRuleSetsByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleSets_GetRuleSetsByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleSets_GetRuleSetsByClientID] TO [sp_executeall]
GO
