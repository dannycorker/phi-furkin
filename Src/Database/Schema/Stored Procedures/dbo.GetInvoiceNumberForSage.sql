SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-06-29
-- Description:	Get Invoice number (from TDV) optimised for the Sage Bridge app.
-- =============================================
CREATE PROCEDURE [dbo].[GetInvoiceNumberForSage] 
	@ClientID int, 
	@LeadID int, 
	@InvoiceTableDetailFieldID int, 
	@InvoiceNumberDetailFieldID int 
AS
BEGIN
	SET NOCOUNT ON

	/*
		The same table can appear more than once on a display page.
		This is the case for Sage Invoice and Sage Payments.
		This proc is used to return the Invoice Number to our Sage Bridge app.
	*/
	SELECT tr.TableRowID, 
	tr.ClientID, 
	tr.LeadID, 
	tr.DetailFieldID, 
	tr.DetailFieldPageID, 
	tdv.DetailFieldID AS Expr1, 
	tdv.DetailValue, 
	df.FieldName 
	FROM dbo.TableRows tr (nolock) 
	INNER JOIN dbo.TableDetailValues tdv (nolock) ON tr.TableRowID = tdv.TableRowID 
	INNER JOIN dbo.DetailFields df (nolock) ON tdv.DetailFieldID = df.DetailFieldID 
	WHERE (tr.ClientID = @ClientID ) 
	AND (tr.LeadID = @LeadID ) 
	AND (tr.DetailFieldID = @InvoiceTableDetailFieldID )	
	AND (tdv.DetailFieldID = @InvoiceNumberDetailFieldID ) 
	ORDER BY tdv.DetailValue 
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[GetInvoiceNumberForSage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetInvoiceNumberForSage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetInvoiceNumberForSage] TO [sp_executeall]
GO
