SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ScriptEventTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptEventTarget_Get_List]

AS


				
				SELECT
					[ScriptEventTargetID],
					[ScriptID],
					[SectionID],
					[ClientID],
					[Target],
					[EventTypeID],
					[EventTypeName],
					[EventSubtypeID],
					[Format]
				FROM
					[dbo].[ScriptEventTarget] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptEventTarget_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget_Get_List] TO [sp_executeall]
GO
