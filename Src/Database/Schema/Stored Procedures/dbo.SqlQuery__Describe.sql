SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2008-08-28
-- Description:	Describe all attributes of a sql query
--				CS 2013-09-10 - Added fn_C00_GetUrlByDatabase() to control URLs
-- 2019-10-03 CPS for JIRA LPC-14 | Added CustomTableSQL support
-- =============================================
CREATE PROCEDURE [dbo].[SqlQuery__Describe]
	@SqlQueryID int, 
	@ShowFullQueryInHistory bit = 0
AS
BEGIN
	
	DECLARE @FolderID int,
			@SqlQueryTemplateID int,
			@ClientID int
	
	SELECT @SqlQueryTemplateID = SqlQueryTemplateID, @FolderID = FolderID, @ClientID = ClientID
	FROM SqlQuery WITH (NOLOCK)  
	WHERE QueryID = @SqlQueryID
	
	SELECT sq.*, cp.UserName 
	FROM dbo.SqlQuery sq WITH (NOLOCK) 
	LEFT JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = sq.ModifiedBy 
	WHERE sq.QueryID = @SqlQueryID 

	IF EXISTS ( SELECT * FROM CustomTableSQL cts WITH (NOLOCK) WHERE cts.SQLQueryID = @SqlQueryID )
	BEGIN
		SELECT 'CustomTableSQL' [CustomTableSQL], cts.*, df.FieldName, df.Enabled
			, dbo.fn_C00_GetUrlByDatabase() + 'DetailFieldEdit.aspx?MenuID=pnl5m&ltid=' + convert(varchar,df.LeadTypeID) + '&lom=2&dfid=' + convert(varchar,df.DetailFieldID) + '&dfp=' + convert(varchar,df.DetailFieldPageID) [Edit URL]
		FROM CustomTableSQL cts WITH (NOLOCK) 
		INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = cts.DetailFieldID
		WHERE cts.SQLQueryID = @SqlQueryID
	END
	
	IF EXISTS ( SELECT * FROM dbo.SqlQueryTemplate t WITH (NOLOCK) WHERE t.SqlQueryTemplateID = @SqlQueryTemplateID )
	BEGIN
		SELECT	dbo.fn_C00_GetUrlByDatabase() + 'iReporting2/ReportBuilder.aspx?qid=' + convert(varchar,@SqlQueryID) + '&fid=' + convert(varchar,@FolderID) + '&a=Edit' [Edit URL],
				'SqlQueryDecode ' + convert(varchar,@SqlQueryID) [Decode],
				dbo.fn_C00_GetUrlByDatabase() + 'ClientImages/Client_' + convert(varchar,@ClientID) + '_Images/SqlQueryTemplates/' + t.TemplatePath [Download Template]
		FROM dbo.SqlQueryTemplate t WITH (NOLOCK) 
		WHERE t.ClientID = @ClientID
		and t.Enabled = 1
		and t.SqlQueryTemplateID = @SqlQueryTemplateID
	END
	ELSE
	BEGIN
		SELECT	dbo.fn_C00_GetUrlByDatabase() + 'iReporting2/ReportBuilder.aspx?qid=' + convert(varchar,@SqlQueryID) + '&fid=' + convert(varchar,@FolderID) + '&a=Edit' [Edit URL],
				'exec dbo.SqlQueryDecode ' + convert(varchar,@SqlQueryID) [Decode]
	END

	SELECT 'SqlQuery Tables' as [Sql Query Tables], * 
	FROM dbo.SqlQueryTables WITH (NOLOCK) 
	WHERE SqlQueryID = @SqlQueryID

	SELECT 'SqlQuery Columns' as [Sql Query Columns], * 
	FROM dbo.SqlQueryColumns WITH (NOLOCK) 
	WHERE SqlQueryID = @SqlQueryID

	SELECT 'SqlQuery Criteria' as [Sql Query Criteria], * 
	FROM dbo.SqlQueryCriteria WITH (NOLOCK) 
	WHERE SqlQueryID = @SqlQueryID

	SELECT * FROM SqlQueryEditing WHERE SqlQueryID = @SqlQueryID

	SELECT 'User Info' as [Who Is Editing], * 
	FROM dbo.SqlQueryEditing e WITH (NOLOCK)
	INNER JOIN dbo.ClientPersonnel c WITH (NOLOCK) ON e.WhoIsEditing = c.ClientPersonnelID
	WHERE SqlQueryID = @SqlQueryID

	-- Automated Task:Sql Query
	SELECT 'Automated Tasks that apply me' as [Automated Tasks that apply me], at.*
	FROM dbo.AutomatedTask at WITH (NOLOCK) 
	INNER JOIN dbo.AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = at.TaskID 
	WHERE atp.ParamName = 'SQL_QUERY'
	AND atp.ParamValue = convert(varchar, @SqlQueryID)
	
	-- History
	IF @ShowFullQueryInHistory = 1
	BEGIN
		SELECT 'Sql Query History' as [Sql Query History], sqh.* 
		FROM dbo.SqlQueryHistory sqh WITH (NOLOCK) 
		WHERE sqh.QueryID = @SqlQueryID 
		ORDER BY sqh.SqlQueryHistoryID 
	END
	ELSE
	BEGIN
		SELECT 'Sql Query History' as [Sql Query History], 
		sqh.[SqlQueryHistoryID],
		sqh.[QueryID],
		sqh.[ClientID],
		sqh.[QueryTitle],
		sqh.[AutorunOnline],
		sqh.[OnlineLimit],
		sqh.[BatchLimit],
		sqh.[SqlQueryTypeID],
		sqh.[FolderID],
		sqh.[IsEditable],
		sqh.[IsTemplate],
		sqh.[IsDeleted],
		sqh.[WhenCreated],
		sqh.[CreatedBy],
		sqh.[OwnedBy],
		sqh.[RunCount],
		sqh.[LastRundate],
		sqh.[LastRuntime],
		sqh.[LastRowcount],
		sqh.[MaxRuntime],
		sqh.[MaxRowcount],
		sqh.[AvgRuntime],
		sqh.[AvgRowcount],
		sqh.[Comments],
		sqh.[WhenModified],
		sqh.[ModifiedBy],
		sqh.[LeadTypeID],
		sqh.[ParentQueryID],
		sqh.[IsParent],
		sqh.[WhoChanged],
		sqh.[WhenChanged],
		sqh.[ChangeAction],
		sqh.[ChangeNotes],
		sqh.[SqlQueryTemplateID],
		sqh.[OutputFormat],
		sqh.[ShowInCustomSearch]
		FROM dbo.SqlQueryHistory sqh WITH (NOLOCK) 
		WHERE sqh.QueryID = @SqlQueryID 
		ORDER BY sqh.SqlQueryHistoryID 
	END
	
	EXEC dbo.folder__describe @FolderID 
	
	EXEC dbo.SQLQueryDecode @SqlQueryID
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__Describe] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery__Describe] TO [sp_executehelper]
GO
