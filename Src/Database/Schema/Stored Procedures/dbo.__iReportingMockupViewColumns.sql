SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-08-19
-- Description:	Demo for iReporting Mockup
-- =============================================
CREATE PROCEDURE [dbo].[__iReportingMockupViewColumns]
	@TableName varchar(100),
	@SqlQueryEditingTableID int = null
AS
BEGIN
	SET NOCOUNT ON;


	SELECT 20, isc.COLUMN_NAME as [ColumnName]
	FROM INFORMATION_SCHEMA.COLUMNS isc 
	--LEFT JOIN dbo.SqlQueryEditingColumns sqec ON isc.TABLE_NAME = sqec.ReportTableName 
	WHERE isc.TABLE_NAME = @TableName 
	UNION ALL 
	SELECT 10, 'Just join the table with no columns' 
	ORDER BY 1, 2 
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[__iReportingMockupViewColumns] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__iReportingMockupViewColumns] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__iReportingMockupViewColumns] TO [sp_executeall]
GO
