SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the HyperlinkStandardTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkStandardTarget__DeleteByHyperlinkFieldID]
(

	@HyperlinkDetailFieldID int   
)
AS


				DELETE FROM [dbo].[HyperlinkStandardTarget] WITH (ROWLOCK) 
				WHERE
					HyperlinkDetailFieldID = @HyperlinkDetailFieldID
			



GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkStandardTarget__DeleteByHyperlinkFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkStandardTarget__DeleteByHyperlinkFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkStandardTarget__DeleteByHyperlinkFieldID] TO [sp_executeall]
GO
