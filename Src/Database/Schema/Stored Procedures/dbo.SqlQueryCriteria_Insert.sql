SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlQueryCriteria table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteria_Insert]
(

	@SqlQueryCriteriaID int    OUTPUT,

	@ClientID int   ,

	@SqlQueryID int   ,

	@SqlQueryTableID int   ,

	@SqlQueryColumnID int   ,

	@CriteriaText varchar (1000)  ,

	@Criteria1 varchar (250)  ,

	@Criteria2 varchar (250)  ,

	@CriteriaName varchar (250)  ,

	@SubQueryID int   ,

	@SubQueryLinkType varchar (50)  ,

	@ParamValue varchar (250)  ,

	@TempTableID int   ,

	@TempColumnID int   ,

	@TempCriteriaID int   ,

	@IsSecurityClause bit   ,

	@CriteriaSubstitutions varchar (2000)  ,

	@IsParameterizable bit   ,

	@Comparison varchar (50)  ,

	@IsJoinClause bit   ,

	@SourceID int   
)
AS


				
				INSERT INTO [dbo].[SqlQueryCriteria]
					(
					[ClientID]
					,[SqlQueryID]
					,[SqlQueryTableID]
					,[SqlQueryColumnID]
					,[CriteriaText]
					,[Criteria1]
					,[Criteria2]
					,[CriteriaName]
					,[SubQueryID]
					,[SubQueryLinkType]
					,[ParamValue]
					,[TempTableID]
					,[TempColumnID]
					,[TempCriteriaID]
					,[IsSecurityClause]
					,[CriteriaSubstitutions]
					,[IsParameterizable]
					,[Comparison]
					,[IsJoinClause]
					,[SourceID]
					)
				VALUES
					(
					@ClientID
					,@SqlQueryID
					,@SqlQueryTableID
					,@SqlQueryColumnID
					,@CriteriaText
					,@Criteria1
					,@Criteria2
					,@CriteriaName
					,@SubQueryID
					,@SubQueryLinkType
					,@ParamValue
					,@TempTableID
					,@TempColumnID
					,@TempCriteriaID
					,@IsSecurityClause
					,@CriteriaSubstitutions
					,@IsParameterizable
					,@Comparison
					,@IsJoinClause
					,@SourceID
					)
				-- Get the identity value
				SET @SqlQueryCriteriaID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteria_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteria_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteria_Insert] TO [sp_executeall]
GO
