SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeAccessRule table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAccessRule_GetByAccessRuleID]
(

	@AccessRuleID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EventTypeAccessRuleID],
					[ClientID],
					[EventTypeID],
					[AccessRuleID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[EventTypeAccessRule] WITH (NOLOCK) 
				WHERE
					[AccessRuleID] = @AccessRuleID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAccessRule_GetByAccessRuleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAccessRule_GetByAccessRuleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAccessRule_GetByAccessRuleID] TO [sp_executeall]
GO
