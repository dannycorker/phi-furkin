SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.DeleteClientPersonnel    Script Date: 08/09/2006 12:22:49 ******/

CREATE PROCEDURE [dbo].[DeleteClientPersonnel] @ClientPersonnelID int
AS
Delete From ClientPersonnel
Where ClientPersonnelID = @ClientPersonnelID





GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteClientPersonnel] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteClientPersonnel] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteClientPersonnel] TO [sp_executeall]
GO
