SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-02-04
-- Description:	Gets the pet information to replace the normal lead info
-- Modified:	2014-11-12	SB	Added in collection lead ID to output
--				2014-11-20	SB	Bug fix for other pets
-- =============================================
CREATE PROCEDURE [dbo].[_C384_LeadInfo_GetPetInfo] 
(
	@LeadID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	EXEC _C600_LeadInfo_GetPetInfo @LeadID
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C384_LeadInfo_GetPetInfo] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C384_LeadInfo_GetPetInfo] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C384_LeadInfo_GetPetInfo] TO [sp_executeall]
GO
