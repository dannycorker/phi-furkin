SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SearchQuery table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchQuery_Update]
(

	@SearchQueryID int   ,

	@ClientID int   ,

	@SubClientID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@ClientPersonnelID int   ,

	@SearchQueryName varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SearchQuery]
				SET
					[ClientID] = @ClientID
					,[SubClientID] = @SubClientID
					,[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
					,[ClientPersonnelID] = @ClientPersonnelID
					,[SearchQueryName] = @SearchQueryName
				WHERE
[SearchQueryID] = @SearchQueryID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQuery_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchQuery_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQuery_Update] TO [sp_executeall]
GO
