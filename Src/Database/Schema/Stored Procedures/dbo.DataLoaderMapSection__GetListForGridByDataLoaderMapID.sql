SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------

-- Created By: Aaran Gravestock
-- Created On: 20090105
-- Purpose: Select records from the DataLoaderMapSection table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderMapSection__GetListForGridByDataLoaderMapID]
(

	@DataLoaderMapID int   
)
AS
BEGIN

	SET NOCOUNT ON
	
	SELECT
		DataLoaderMapSectionID,
		ClientID,
		DataLoaderMapID,
		dlot.ObjectTypeName AS ObjectType,
		dloa.DataLoaderObjectActionName AS ObjectAction,
		dlslt.LocaterTypeName AS LocaterType,
		IsNull(dfs.DetailFieldSubTypeName, '') AS [FieldType],
		HasAquariumID,
		NumberOfFooterRowsToSkip,
		ImportIntoLeadManager,
		IsFixedLengthSection,
		FixedLengthNumberOfRows,
		IsMandatory,
		IsMultipleAllowedWithinFile,
		Notes
	FROM
		[dbo].[DataLoaderMapSection] dlms
	INNER JOIN DataLoaderObjectType dlot ON dlot.DataLoaderObjectTypeID = dlms.DataLoaderObjectTypeID
	LEFT JOIN DataLoaderObjectAction dloa ON dloa.DataLoaderObjectActionID = dlms.DataLoaderObjectActionID
	INNER JOIN DataLoaderSectionLocaterType dlslt ON dlslt.DataLoaderSectionLocaterTypeID = dlms.DataLoaderSectionLocaterTypeID
	LEFT JOIN DetailFieldSubType dfs ON dfs.DetailFieldSubTypeID = dlms.DetailFieldSubTypeID
	WHERE
		[DataLoaderMapID] = @DataLoaderMapID
	
END











GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection__GetListForGridByDataLoaderMapID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMapSection__GetListForGridByDataLoaderMapID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection__GetListForGridByDataLoaderMapID] TO [sp_executeall]
GO
