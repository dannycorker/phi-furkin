SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientQuestionnaireLayout table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnaireLayout_Update]
(

	@ClientQuestionnaireLayoutID int   ,

	@ClientQuestionnaireID int   ,

	@ClientID int   ,

	@PageNumber int   ,

	@LayoutCss varchar (MAX)  ,

	@LayoutCssFileName varchar (512)  ,

	@QuestionColumns int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientQuestionnaireLayout]
				SET
					[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[ClientID] = @ClientID
					,[PageNumber] = @PageNumber
					,[LayoutCss] = @LayoutCss
					,[LayoutCssFileName] = @LayoutCssFileName
					,[QuestionColumns] = @QuestionColumns
				WHERE
[ClientQuestionnaireLayoutID] = @ClientQuestionnaireLayoutID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaireLayout_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnaireLayout_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaireLayout_Update] TO [sp_executeall]
GO
