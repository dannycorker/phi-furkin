SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Logs table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Logs_GetByTypeOfLogEntry]
(

	@TypeOfLogEntry varchar (6)  
)
AS


				SELECT
					[LogID],
					[LogDateTime],
					[TypeOfLogEntry],
					[ClassName],
					[MethodName],
					[LogEntry],
					[ClientPersonnelID]
				FROM
					[dbo].[Logs] WITH (NOLOCK) 
				WHERE
										[TypeOfLogEntry] = @TypeOfLogEntry
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Logs_GetByTypeOfLogEntry] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Logs_GetByTypeOfLogEntry] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Logs_GetByTypeOfLogEntry] TO [sp_executeall]
GO
