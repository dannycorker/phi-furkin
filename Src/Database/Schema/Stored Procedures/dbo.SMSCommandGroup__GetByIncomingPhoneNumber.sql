SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 03-01-2014
-- Description:	Gets and SMS Command Group by the SMSIncomingPhoneNumber and ClientID
-- =============================================
CREATE PROCEDURE [dbo].[SMSCommandGroup__GetByIncomingPhoneNumber]

	@SMSIncomingPhoneNumber VARCHAR(16),
	@ClientID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT * FROM SMSCommandGroup WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND SMSIncomingPhoneNumber = @SMSIncomingPhoneNumber

END



GO
GRANT VIEW DEFINITION ON  [dbo].[SMSCommandGroup__GetByIncomingPhoneNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSCommandGroup__GetByIncomingPhoneNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSCommandGroup__GetByIncomingPhoneNumber] TO [sp_executeall]
GO
