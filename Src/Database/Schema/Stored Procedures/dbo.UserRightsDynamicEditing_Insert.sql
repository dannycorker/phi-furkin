SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the UserRightsDynamicEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserRightsDynamicEditing_Insert]
(

	@UserRightsDynamicEditingID int    OUTPUT,

	@ClientPersonnelID int   ,

	@FunctionTypeID int   ,

	@LeadTypeID int   ,

	@ObjectID int   ,

	@RightID int   
)
AS


				
				INSERT INTO [dbo].[UserRightsDynamicEditing]
					(
					[ClientPersonnelID]
					,[FunctionTypeID]
					,[LeadTypeID]
					,[ObjectID]
					,[RightID]
					)
				VALUES
					(
					@ClientPersonnelID
					,@FunctionTypeID
					,@LeadTypeID
					,@ObjectID
					,@RightID
					)
				-- Get the identity value
				SET @UserRightsDynamicEditingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamicEditing_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserRightsDynamicEditing_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamicEditing_Insert] TO [sp_executeall]
GO
