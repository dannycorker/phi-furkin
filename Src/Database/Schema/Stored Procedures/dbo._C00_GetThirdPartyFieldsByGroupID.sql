SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-04-20
-- Description:	Get Third Party Fields By Group
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetThirdPartyFieldsByGroupID]
	@ClientID INT,
	@ThirdPartyGroupID INT,
	@LeadTypeID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT t.FieldName, tpm.ThirdPartyFieldID, tpm.DetailFieldID, tpm.ColumnFieldID
	FROM ThirdPartyField t WITH (NOLOCK)
	INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.ThirdPartyFieldID = t.ThirdPartyFieldID AND tpm.ThirdPartyFieldGroupID = @ThirdPartyGroupID
	WHERE tpm.IsEnabled = 1
	AND tpm.LeadTypeID = @LeadTypeID
	AND tpm.ClientID = @ClientID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetThirdPartyFieldsByGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetThirdPartyFieldsByGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetThirdPartyFieldsByGroupID] TO [sp_executeall]
GO
