SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptEventTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptEventTarget_GetByEventTypeID]
(

	@EventTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ScriptEventTargetID],
					[ScriptID],
					[SectionID],
					[ClientID],
					[Target],
					[EventTypeID],
					[EventTypeName],
					[EventSubtypeID],
					[Format]
				FROM
					[dbo].[ScriptEventTarget] WITH (NOLOCK) 
				WHERE
					[EventTypeID] = @EventTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget_GetByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptEventTarget_GetByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptEventTarget_GetByEventTypeID] TO [sp_executeall]
GO
