SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2013-04-22
-- Description:	Gets a list of Responses for each Questionnaire formatted for UI display
-- Modified By PR 24/01/2014 - add sentiment confidence range
-- =============================================
CREATE PROCEDURE [dbo].[SMSResponseMap__GetListBySMSQuestionnaireID]
	@SMSQuestionnaireID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		g.SMSQuestionnaireID,
		m.SMSResponseMapID,
		g.SMSQuestionGroupID as FromGroupID, 
		g.GroupName as FromGroupName,
		q.SMSQuestionID as FromQuestionID,
		q.Question as FromQuestionText,
		m.FromAnswer as FromAnswer,
		m.ToSMSQuestionGroupID as ToGroupID,
		toSMSQuestionGroup.GroupName as ToGroupName,
		m.ToSMSQuestionID as ToQuestionID,
		toSMSQuestion.Question as ToQuestionText,
		m.EndOfQuestionnaire,
		m.ConfidenceRangeStart,
		m.ConfidenceRangeEnd
	FROM SMSQuestionGroup g WITH (NOLOCK) 
	INNER JOIN dbo.SMSQuestionGroupMember gm WITH (NOLOCK) ON gm.SMSQuestionGroupID = g.SMSQuestionGroupID
	INNER JOIN dbo.SMSQuestion q WITH (NOLOCK) ON q.SMSQuestionID = gm.SMSQuestionID
	INNER JOIN dbo.SMSResponseMap m WITH (NOLOCK) ON m.FromSMSQuestionID = q.SMSQuestionID
	LEFT JOIN dbo.SMSQuestion toSMSQuestion WITH (NOLOCK) on toSMSQuestion.SMSQuestionID = m.ToSMSQuestionID
	LEFT JOIN dbo.SMSQuestionGroup toSMSQuestionGroup WITH (NOLOCK) on toSMSQuestionGroup.SMSQuestionGroupID = m.ToSMSQuestionGroupID
	WHERE g.SMSQuestionnaireID = @SMSQuestionnaireID
	order by g.GroupOrder, q.QuestionOrder, m.ProcessingOrder
END


GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap__GetListBySMSQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSResponseMap__GetListBySMSQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap__GetListBySMSQuestionnaireID] TO [sp_executeall]
GO
