SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DroppedOutCustomerQuestionnaires table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomerQuestionnaires_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[CustomerQuestionnaireID],
					[ClientQuestionnaireID],
					[CustomerID],
					[SubmissionDate],
					[TrackingID],
					[ClientID]
				FROM
					[dbo].[DroppedOutCustomerQuestionnaires] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerQuestionnaires_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomerQuestionnaires_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerQuestionnaires_GetByClientID] TO [sp_executeall]
GO
