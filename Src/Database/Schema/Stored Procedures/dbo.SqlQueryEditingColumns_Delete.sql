SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SqlQueryEditingColumns table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingColumns_Delete]
(

	@SqlQueryEditingColumnID int   
)
AS


				DELETE FROM [dbo].[SqlQueryEditingColumns] WITH (ROWLOCK) 
				WHERE
					[SqlQueryEditingColumnID] = @SqlQueryEditingColumnID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingColumns_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns_Delete] TO [sp_executeall]
GO
