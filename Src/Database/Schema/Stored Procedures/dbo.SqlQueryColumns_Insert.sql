SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlQueryColumns table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryColumns_Insert]
(

	@SqlQueryColumnID int    OUTPUT,

	@ClientID int   ,

	@SqlQueryID int   ,

	@SqlQueryTableID int   ,

	@CompleteOutputText varchar (500)  ,

	@ColumnAlias varchar (50)  ,

	@ShowColumn bit   ,

	@DisplayOrder int   ,

	@SortOrder int   ,

	@SortType varchar (10)  ,

	@TempColumnID int   ,

	@TempTableID int   ,

	@IsAggregate bit   ,

	@ColumnDataType varchar (50)  ,

	@ManipulatedDataType varchar (50)  ,

	@ColumnNaturalName varchar (50)  ,

	@SourceID int   ,

	@ComplexValue nvarchar (MAX)  
)
AS


				
				INSERT INTO [dbo].[SqlQueryColumns]
					(
					[ClientID]
					,[SqlQueryID]
					,[SqlQueryTableID]
					,[CompleteOutputText]
					,[ColumnAlias]
					,[ShowColumn]
					,[DisplayOrder]
					,[SortOrder]
					,[SortType]
					,[TempColumnID]
					,[TempTableID]
					,[IsAggregate]
					,[ColumnDataType]
					,[ManipulatedDataType]
					,[ColumnNaturalName]
					,[SourceID]
					,[ComplexValue]
					)
				VALUES
					(
					@ClientID
					,@SqlQueryID
					,@SqlQueryTableID
					,@CompleteOutputText
					,@ColumnAlias
					,@ShowColumn
					,@DisplayOrder
					,@SortOrder
					,@SortType
					,@TempColumnID
					,@TempTableID
					,@IsAggregate
					,@ColumnDataType
					,@ManipulatedDataType
					,@ColumnNaturalName
					,@SourceID
					,@ComplexValue
					)
				-- Get the identity value
				SET @SqlQueryColumnID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryColumns_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryColumns_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryColumns_Insert] TO [sp_executeall]
GO
