SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ReferenceValueDelimiters table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueDelimiters_Find]
(

	@SearchUsingOR bit   = null ,

	@ReferenceValueDelimiterID int   = null ,

	@Delimiter char (1)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ReferenceValueDelimiterID]
	, [Delimiter]
    FROM
	[dbo].[ReferenceValueDelimiters] WITH (NOLOCK) 
    WHERE 
	 ([ReferenceValueDelimiterID] = @ReferenceValueDelimiterID OR @ReferenceValueDelimiterID IS NULL)
	AND ([Delimiter] = @Delimiter OR @Delimiter IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ReferenceValueDelimiterID]
	, [Delimiter]
    FROM
	[dbo].[ReferenceValueDelimiters] WITH (NOLOCK) 
    WHERE 
	 ([ReferenceValueDelimiterID] = @ReferenceValueDelimiterID AND @ReferenceValueDelimiterID is not null)
	OR ([Delimiter] = @Delimiter AND @Delimiter is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDelimiters_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueDelimiters_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDelimiters_Find] TO [sp_executeall]
GO
