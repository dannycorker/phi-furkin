SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-20
-- Description:	List all LeadType records for a client
-- =============================================
CREATE PROCEDURE [dbo].[lt] 
	@ClientID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF @ClientID IS NULL
	BEGIN
		SELECT * 
		FROM dbo.LeadType lt (nolock) 
		ORDER BY lt.LeadTypeID DESC
	END
	ELSE
	BEGIN
		SELECT * 
		FROM dbo.LeadType lt (nolock) 
		WHERE lt.ClientID = @ClientID 
		ORDER BY lt.LeadTypeName ASC 
	END
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[lt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[lt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[lt] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[lt] TO [sp_executehelper]
GO
