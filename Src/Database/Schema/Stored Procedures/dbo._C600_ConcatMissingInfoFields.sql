SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		James Lewis
-- Create date: 2016-11-07
-- Description:	Concatanates all missing information requests to a single line
-- Mods
-- =============================================
CREATE PROCEDURE [dbo].[_C600_ConcatMissingInfoFields] 
(
	@MatterID INT
)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @MissingInfoPH VARCHAR (MAX) = 'Missing documents:',
			@MissingInfoVet VARCHAR(MAX) = 'Missing documents:'

	/*VET*/
	IF dbo.fnGetSimpleDvAsInt(150417,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoVET = @MissingInfoVET + ' ' + 'Full clinical history, '
	END

	IF dbo.fnGetSimpleDvAsInt(150418,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoVET = @MissingInfoVET + ' ' + 'Itemised invoice, '
	END

	IF dbo.fnGetSimpleDvAsInt(150419,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoVET = @MissingInfoVET + ' ' + 'Vet Signature, '
	END

	IF dbo.fnGetSimpleDvAsInt(150420,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoVET = @MissingInfoVET + ' ' + 'Practice stamp / headed paper, '
	END

	IF dbo.fnGetSimpleDvAsInt(170120,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoVET = @MissingInfoVET + ' ' + 'Referral report (if applicable), '
	END

	IF dbo.fnGetSimpleDvAsInt(170121,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoVET = @MissingInfoVET + ' ' + 'Clinical history for treatment period, '
	END

	IF dbo.fnGetSimpleDvAsInt(170124,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoVET = @MissingInfoVET + ' ' + 'Vets prescription, '
	END

	IF dbo.fnGetSimpleDvAsInt(170125,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoVET = @MissingInfoVET + ' ' + 'Additional information (Vet), '
	END

	/*PolicyHolder*/ 

	IF dbo.fnGetSimpleDvAsInt(170126,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoPH = @MissingInfoPH + ' ' + 'Additional information (Policyholder), '
	END

	IF dbo.fnGetSimpleDvAsInt(175286,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoPH = @MissingInfoPH + ' ' + 'Policyholder - Itemised vet receipt, '
	END

	IF dbo.fnGetSimpleDvAsInt(175287,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoPH = @MissingInfoPH + ' ' + 'Policyholder - Previous vet practice details, '
	END

	IF dbo.fnGetSimpleDvAsInt(175288,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoPH = @MissingInfoPH + ' ' + 'Policyholder - Boarding kennel receipt, '
	END

	IF dbo.fnGetSimpleDvAsInt(175289,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoPH = @MissingInfoPH + ' ' + 'Policyholder - Certificate of hospitalisation, '
	END

	IF dbo.fnGetSimpleDvAsInt(175290,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoPH = @MissingInfoPH + ' ' + 'Policyholder - Animal purchase receipt, '
	END

	IF dbo.fnGetSimpleDvAsInt(175291,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoPH = @MissingInfoPH + ' ' + 'Policyholder - Vaccination certificate, '
	END

	IF dbo.fnGetSimpleDvAsInt(175293,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoPH = @MissingInfoPH + ' ' + 'Policyholder - Signature, '
	END

	IF dbo.fnGetSimpleDvAsInt(175294,@MatterID) = 63964
	BEGIN 
		SELECT @MissingInfoPH = @MissingInfoPH + ' ' + 'Policyholder - Pedigree certificate, '
	END
	
	EXEC _C00_SimpleValueIntoField  177358,@MissingInfoPH,@MatterID,44412
	EXEC _C00_SimpleValueIntoField  177359,@MissingInfoVET,@MatterID,44412

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ConcatMissingInfoFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_ConcatMissingInfoFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ConcatMissingInfoFields] TO [sp_executeall]
GO
