SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-12-02
-- Description:	Update adjustment fields
-- Mods:
--	2015-07-13 DCM MTA on monthly payment where first payment not yet taken.  Update premium row which has ready status 
--  2015-07-29 DCM @ENDDATE is now 1 day after payment period date on annual payments 
--	2016-11-15 DCM updated MTA for new billing system
--	2017-02-22 DCM zero adjustment fields if no change in MTA premium
--  2017-07-13 JEL Added ISNULLs and Cast to two DP on premiums to protect from NULL and Rounding Errors
--  2017-07-25 CPS use Premium Less Discount field 
--  CW 2018-04-17 OMF Status support
--  2019-10-23 CPS for JIRA LPC-35 | Added nullable LeadEventID rather than picking up from the Case
-- 2020-03-18 GPR for AAG-512	| Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- 2020-03-20 GPR | Updated tax related column names for AAG-507
-- 2020-07-14 GPR | Updated for SDAAG-60 and SDAAG-84
-- 2020-09-10 ACE | PPET-73 - Add Transaction Fee
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Policy_CalculateAdjustments] 
(
	 @MatterID		 INT
	,@StartDate		 DATE
	,@EndDate		 DATE
	,@AdjustmentDate DATE
	,@CalculateType  INT = 1 -- 1=monthly premium, 2=renewal, 3=MTA, 4=Quote, 5=cancellation 
	,@DisplayOnly	 INT = 0
	,@LeadEventID	 INT = NULL
)

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE  @CaseID			INT
			,@ClientID			INT
			,@Delta				MONEY
			,@IPTNew			MONEY
			,@IPTOld			MONEY
			,@LeadTypeID		INT
			,@NetNew			MONEY
			,@NetOld			MONEY
			,@NewRate			MONEY
			,@NextMonth			MONEY
			,@ObjectID			INT
			,@PremiumOld		MONEY
			,@PremiumNew		MONEY
			,@PurchasedProduct	INT
			,@Month				INT
			,@TableRowID		INT
			,@EventComments		VARCHAR(2000) = CHAR(13)+CHAR(10) + '| ' + OBJECT_NAME(@@ProcID) + '| ' + CHAR(13)+CHAR(10)-- 2017-08-10 AG - added in Merged in changes from 384Dev
			,@XmlLog			XML

	DECLARE @Premiums TABLE ( RNO INT, Annual MONEY, IPT MONEY, Net MONEY, TableRowID INT )

	DECLARE @Adjustment TABLE ( AccountID INT, 
								PurchasedProductID INT, 
								NewCoverFrom DATE, 
								NewCoverTo DATE, 
								NewPaymentDate DATE, 
								OneOffPaymentNET MONEY, 
								OneOffPaymentVAT MONEY, 
								OneOffPaymentGross MONEY,
								TransactionFee NUMERIC(18,2),
								WriteOffNet NUMERIC(18,2),
								WriteOffTax NUMERIC(18,2),
								WriteOffGross NUMERIC(18,2)
								)
	
	/*2017-07-28 ACE Removed the redundant leadevent join as we have the leadevent id we need here "ca.LatestInProcessLeadEventID"*/
	SELECT @ClientID = m.ClientID, @CaseID = m.CaseID, @LeadEventID = ISNULL(@LeadEventID, ca.LatestInProcessLeadEventID), @leadTypeID = l.LeadTypeID
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN Cases ca WITH (NOLOCK) ON m.CaseID = ca.CaseID
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = ca.LeadID
	--INNER JOIN LeadEvent le WITH (NOLOCK) ON le.LeadEventID=ca.LatestInProcessLeadEventID
	WHERE m.MatterID = @MatterID

	-- get old and new premiums
	INSERT INTO @Premiums
	SELECT TOP 2 ROW_NUMBER() OVER( PARTITION BY (SELECT 1) ORDER BY wp.WrittenPremiumID DESC )
				--,wp.AnnualPriceForRiskGross, (wp.AdjustmentValueNationalTax + AdjustmentValueLocalTax) /*GPR 2020-05-27*/, (wp.AnnualPriceForRiskGross - (wp.AdjustmentValueNationalTax + wp.AdjustmentValueLocalTax)), wp.WrittenPremiumID
				,wp.AnnualPriceForRiskGross, (wp.AnnualPriceForRiskNationalTax + AnnualPriceForRiskLocalTax) /*GPR 2020-05-27*/, (wp.AnnualPriceForRiskGross - (wp.AnnualPriceForRiskNationalTax + AnnualPriceForRiskLocalTax)), wp.WrittenPremiumID /*GPR 2020-07-14*/
	FROM WrittenPremium wp
	WHERE wp.MatterID = @MatterID
	ORDER BY wp.WrittenPremiumID DESC

	/*
	SELECT ROW_NUMBER() OVER( PARTITION BY r.ClientID ORDER BY r.TableRowID DESC ) [RowNum]
				,ann.ValueMoney, ipt.ValueMoney [Diff1], ann.ValueMoney - ipt.ValueMoney [Diff2], r.TableRowID
				,stat.ValueInt
	FROM TableRows r WITH ( NOLOCK )
	INNER JOIN TableDetailValues ann	WITH (NOLOCK) ON r.TableRowID=ann.TableRowID	AND ann.DetailFieldID	=177899 /*Premium less Discount*/
	INNER JOIN TableDetailValues ipt	WITH (NOLOCK) ON r.TableRowID=ipt.TableRowID	AND ipt.DetailFieldID	=175375 /*IPT*/
	INNER JOIN TableDetailValues om		WITH (NOLOCK) ON r.TableRowID=om.TableRowID		AND om.DetailFieldID	=175351	/*Other Month*/
	INNER JOIN TableDetailValues stat	WITH (NOLOCK) ON r.TableRowID=stat.TableRowID	AND stat.DetailFieldID	=175722	/*Status*/
	WHERE r.DetailFieldID=175336 /*Premium Detail History*/
	AND r.MatterID=@MatterID 
	AND stat.ValueInt IN (72326,72327,74514) /*Live, Ex-Live, MTA*/
	ORDER BY r.TableRowID DESC
	*/

	SELECT	 @PremiumNew=CAST(ISNULL(Annual,0) as DECIMAL(18,2)), @IPTNew=CAST(ISNULL(IPT,0) as DECIMAL(18,2)), @NetNew=CAST(ISNULL(Net,0) as DECIMAL(18,2)) 
			,@EventComments += 'Premium New: ' + ISNULL(CONVERT(VARCHAR,Annual),'NULL') + '.  TableRowID ' + CONVERT(VARCHAR,TableRowID) + CHAR(13)+CHAR(10)
	FROM @Premiums 
	WHERE RNO=1  

	SELECT	 @PremiumOld=CAST(ISNULL(Annual,0) as DECIMAL(18,2)), @IPTOld=CAST(ISNULL(IPT,0) as DECIMAL(18,2)), @NetOld=CAST(ISNULL(Net,0) as DECIMAL(18,2))
			,@EventComments += 'Premium Old: ' + ISNULL(CONVERT(VARCHAR,Annual),'NULL') + '.  TableRowID ' + CONVERT(VARCHAR,TableRowID) + CHAR(13)+CHAR(10)
	FROM @Premiums
	WHERE RNO=2  

	SELECT @XmlLog = 
	(
		SELECT ROW_NUMBER() OVER( PARTITION BY (SELECT 1) ORDER BY wp.WrittenPremiumID DESC ) AS [RN]
					,wp.AnnualPriceForRiskGross, wp.AdjustmentValueNationalTax, (wp.AnnualPriceForRiskGross - wp.AdjustmentValueNationalTax - wp.AdjustmentValueLocalTax) AS [Net], wp.WrittenPremiumID
		FROM WrittenPremium wp
		WHERE wp.MatterID = @MatterID
		--AND wp.AdjustmentTypeID IN (1)
		FOR XML AUTO
	)

	EXEC _C00_LogItXML @ClientID, @LeadEventID, '_C00_1273_Policy_CalculateAdjustments', @XmlLog

	SELECT @EventComments += 'Calculate Type ' + ISNULL(CONVERT(VARCHAR,@CalculateType),'NULL') + '. ' + CHAR(13)+CHAR(10)

	IF @CalculateType IN ( 3,9 )
	BEGIN
		
		-- set up third party fields
		SELECT TOP 1 @PurchasedProduct=pp.ValueInt
		FROM Matter m WITH (NOLOCK)
		INNER JOIN MatterDetailValues pp WITH (NOLOCK) ON m.MatterID=pp.MatterID AND pp.DetailFieldID=177074 /*Purchased Policy ID*/
		WHERE m.MatterID=@MatterID

		SELECT @EventComments += 'PurchasedProductID ' + ISNULL(CONVERT(VARCHAR,@PurchasedProduct),'NULL') + '. ' + CHAR(13)+CHAR(10)

		-- clear helper or update fields
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4399)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @PremiumNew, @MatterID -- Premium Amount
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4414)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @NetNew, @MatterID -- Premium Net
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4415)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @IPTNew, @MatterID -- Premium VAT
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4401)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @AdjustmentDate, @MatterID -- Adjustment Date
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4410)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, 74329, @MatterID -- MTA Calculation Method ID
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4413)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, @PurchasedProduct, @MatterID -- Purchased Product ID
		SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4530)
		EXEC dbo._C00_SimpleValueIntoField @ObjectID, 1, @MatterID -- AdjustmentValueOnly

		-- calculate adjustment
		EXEC dbo.MidTermAdjustment__CreateFromThirdPartyFields @ClientID, @CaseID, @LeadEventID

		IF @CalculateType=9
		BEGIN
			SELECT @EventComments += 'PremiumNew = ' + ISNULL(CONVERT(VARCHAR,@PremiumNew),'NULL') + '. ' + CHAR(13)+CHAR(10)
								   + 'PremiumOld = ' + ISNULL(CONVERT(VARCHAR,@PremiumOld),'NULL') + '. ' + CHAR(13)+CHAR(10)
		
			-- set adjustment type
			IF @PremiumNew=@PremiumOld -- no change
			BEGIN

				EXEC dbo._C00_SimpleValueIntoField 175345, 70021, @MatterID	-- no change
				-- if there has been no change clear the adjustment fields and set next value
				SELECT @NextMonth=dbo.fnGetDvAsMoney(177405,@CaseID)-dbo.fnGetDvAsMoney(175379,@CaseID)
				EXEC dbo._C00_SimpleValueIntoField 175379, 0.00		 , @MatterID	/*Adjustment Value  (for adjustment cover period)*/
				--EXEC dbo._C00_SimpleValueIntoField 175404, 0.00	 , @MatterID	/*Get well soon text*/  
				EXEC dbo._C00_SimpleValueIntoField 177404, 0.00		 , @MatterID	/*Adjustment Amount for remaining policy term*/ -- CPS 2017-07-25 Surely that was the wrong DetailFieldID
				EXEC dbo._C00_SimpleValueIntoField 177405, @NextMonth, @MatterID	/*First month following MTA total*/
			
			END
			ELSE IF @PremiumNew > @PremiumOld -- additional payment
			BEGIN
			
				EXEC dbo._C00_SimpleValueIntoField 175345, 70019, @MatterID	-- increase	
			
			END
			ELSE -- reduction, but not necessarily a refund (they may not have paid yet)
			BEGIN
			
				EXEC dbo._C00_SimpleValueIntoField 175345, 70020, @MatterID	-- reduction	
			
			END
			
		END
		
	END
	
	IF @CalculateType=5
	BEGIN
	
		-- get refund value
		INSERT INTO @Adjustment
		EXEC dbo.Cancellation__CalculateAdjustmentValue @ClientID, @CaseID, @LeadEventID

		SELECT @Delta=a.OneOffPaymentGross
		FROM @Adjustment a	

		SELECT @EventComments += 'Delta = ' + ISNULL(CONVERT(VARCHAR,@Delta),'NULL') + '. ' + CHAR(13)+CHAR(10)
		
		IF @Delta > 0
		BEGIN
			EXEC dbo._C00_SimpleValueIntoField 175448, '', @MatterID	-- adjustment comments			
			EXEC dbo._C00_SimpleValueIntoField 175345, 70019, @MatterID	-- increase
		END
		ELSE IF @Delta < 0
		BEGIN
			EXEC dbo._C00_SimpleValueIntoField 175448, '', @MatterID	-- adjustment comments			
			EXEC dbo._C00_SimpleValueIntoField 175345, 70020, @MatterID	-- reduction
		END
		ELSE	
		BEGIN
			EXEC dbo._C00_SimpleValueIntoField 175345, 70021, @MatterID	-- no change
		END
		
		EXEC dbo._C00_SimpleValueIntoField 175379, @Delta, @MatterID	
		-- BEU.  This is probably incorrect & should probably be the total remaining in period unless no refund at all is to be given 
		-- in which case for annual payers who have paid it is zero and for monthly payers it equals the total of whole unpaid months
		EXEC dbo._C00_SimpleValueIntoField 177404, @Delta, @MatterID	
	
	END
	
	IF @LeadEventID > 0
	BEGIN
		EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1
	END
	ELSE
	BEGIN
		EXEC _C00_LogIt 'Info', '_C600_LOG', '_C00_1273_Policy_CalculateAdjustments', @EventComments, 44412 /*Aquarium Automation*/
	END
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_CalculateAdjustments] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Policy_CalculateAdjustments] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_CalculateAdjustments] TO [sp_executeall]
GO
