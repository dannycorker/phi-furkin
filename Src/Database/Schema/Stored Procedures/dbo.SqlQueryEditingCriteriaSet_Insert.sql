SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlQueryEditingCriteriaSet table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteriaSet_Insert]
(

	@SqlQueryEditingCriteriaSetID int    OUTPUT,

	@ClientID int   ,

	@SqlQueryEditingID int   ,

	@SetName varchar (250)  ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[SqlQueryEditingCriteriaSet]
					(
					[ClientID]
					,[SqlQueryEditingID]
					,[SetName]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@SqlQueryEditingID
					,@SetName
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @SqlQueryEditingCriteriaSetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSet_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteriaSet_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSet_Insert] TO [sp_executeall]
GO
