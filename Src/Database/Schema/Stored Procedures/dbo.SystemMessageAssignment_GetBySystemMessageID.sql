SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SystemMessageAssignment table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SystemMessageAssignment_GetBySystemMessageID]
(

	@SystemMessageID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SystemMessageAssignmentID],
					[ClientID],
					[ClientPersonnelAdminGroupID],
					[ClientPersonnelID],
					[SystemMessageID]
				FROM
					[dbo].[SystemMessageAssignment] WITH (NOLOCK) 
				WHERE
					[SystemMessageID] = @SystemMessageID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessageAssignment_GetBySystemMessageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SystemMessageAssignment_GetBySystemMessageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessageAssignment_GetBySystemMessageID] TO [sp_executeall]
GO
