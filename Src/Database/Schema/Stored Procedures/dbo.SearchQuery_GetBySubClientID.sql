SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SearchQuery table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchQuery_GetBySubClientID]
(

	@SubClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SearchQueryID],
					[ClientID],
					[SubClientID],
					[ClientPersonnelAdminGroupID],
					[ClientPersonnelID],
					[SearchQueryName]
				FROM
					[dbo].[SearchQuery] WITH (NOLOCK) 
				WHERE
					[SubClientID] = @SubClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQuery_GetBySubClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchQuery_GetBySubClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQuery_GetBySubClientID] TO [sp_executeall]
GO
