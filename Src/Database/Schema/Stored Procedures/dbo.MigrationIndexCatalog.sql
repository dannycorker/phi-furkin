SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-02-10
-- Description:	Script Indexes ready for escrow creation.
-- =============================================
CREATE PROCEDURE [dbo].[MigrationIndexCatalog] 
	@TableName SYSNAME = NULL,
	@NewFilegroupName SYSNAME = NULL,
	@DoNotScriptExists BIT = 1,
	@SuppressFSTables BIT = 1			/* These tables get scripted out and populated separately by default */
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
	@idxTableName SYSNAME,
	@idxTableID INT,
	@idxname SYSNAME,
	@idxid INT,
	@colCount INT,
	@IxColumn SYSNAME,
	@IxFirstColumn BIT,
	@ColumnIDInTable INT,
	@ColumnIDInIndex INT,
	@IsIncludedColumn INT,
	@sIncludeCols VARCHAR(MAX),
	@sIndexCols VARCHAR(MAX),
	@sSQL VARCHAR(MAX),
	@sParamSQL VARCHAR(MAX),
	@sFilterSQL VARCHAR(MAX),
	@location SYSNAME,
	@IndexCount INT,
	@CurrentIndex INT,
	@CurrentCol INT,
	@MaxColNotAnInclude INT,
	@Name VARCHAR(128),
	@IsPrimaryKey TINYINT,
	@FillFactor INT,
	@FilterDefinition VARCHAR(MAX),
	@IsClustered BIT -- used solely for putting information into the result table


	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE id = OBJECT_ID(N'[tempdb].[dbo].[#IndexSQL]'))
	DROP TABLE [dbo].[#IndexSQL]

	CREATE TABLE #IndexSQL
	( 
		TableName SYSNAME NOT NULL,
		IndexName SYSNAME NOT NULL,
		IsClustered BIT NOT NULL,
		IsPrimaryKey BIT NOT NULL,
		IndexCreateSQL VARCHAR(MAX) NOT NULL
	)
	
	/* TableDetailValues filtered indexes require ANSI_NULLS ON and QUOTED_IDENTIFIER ON */
	/* Some other indexes fail if they are subsequently set to OFF again */
	INSERT INTO #IndexSQL (TableName, IndexName, IsClustered, IsPrimaryKey, IndexCreateSQL) VALUES ('', '', 0, 0, 'SET ANSI_NULLS ON')
	INSERT INTO #IndexSQL (TableName, IndexName, IsClustered, IsPrimaryKey, IndexCreateSQL) VALUES ('', '', 0, 0, 'GO')
	INSERT INTO #IndexSQL (TableName, IndexName, IsClustered, IsPrimaryKey, IndexCreateSQL) VALUES ('', '', 0, 0, 'SET QUOTED_IDENTIFIER ON')
	INSERT INTO #IndexSQL (TableName, IndexName, IsClustered, IsPrimaryKey, IndexCreateSQL) VALUES ('', '', 0, 0, 'GO')
	
	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE id = OBJECT_ID(N'[tempdb].[dbo].[#IndexListing]'))
	DROP TABLE [dbo].[#IndexListing]

	CREATE TABLE #IndexListing
	(
		[IndexListingID] INT IDENTITY(1,1) PRIMARY KEY CLUSTERED,
		[TableName] SYSNAME COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ObjectID] INT NOT NULL,
		[IndexName] SYSNAME COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IndexID] INT NOT NULL,
		[IsPrimaryKey] TINYINT NOT NULL,
		[FillFactor] INT,
		[FilterDefinition] NVARCHAR(MAX) NULL
	)

	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE id = OBJECT_ID(N'[tempdb].[dbo].[#ColumnListing]'))
	DROP TABLE [dbo].[#ColumnListing]

	CREATE TABLE #ColumnListing
	(
		[ColumnListingID] INT IDENTITY(1,1) PRIMARY KEY CLUSTERED,
		[ColumnIDInTable] INT NOT NULL,
		[Name] SYSNAME COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ColumnIDInIndex] INT NOT NULL,
		[IsIncludedColumn] BIT NULL
	)

	INSERT INTO #IndexListing( [TableName], [ObjectID], [IndexName], [IndexID], [IsPrimaryKey], [FILLFACTOR], [FilterDefinition] )
	SELECT OBJECT_NAME(si.object_id), si.object_id, si.name, si.index_id, si.Is_Primary_Key, si.Fill_Factor, si.filter_definition
	FROM sys.indexes si
	LEFT OUTER JOIN information_schema.table_constraints tc ON si.name = tc.constraint_name AND OBJECT_NAME(si.object_id) = tc.table_name 
	WHERE OBJECTPROPERTY(si.object_id, 'IsUserTable') = 1
	AND ((@TableName IS NULL) OR (@TableName = OBJECT_NAME(si.object_id))) /* All tables or just the one specified */
	AND (@SuppressFSTables = 0 OR (OBJECT_NAME(si.object_id) NOT IN ('LeadDocumentFS', 'UploadedFile') ))
	AND si.name IS NOT NULL
	ORDER BY OBJECT_NAME(si.object_id), si.index_id

	SELECT @IndexCount = @@ROWCOUNT, @CurrentIndex = 1

	WHILE @CurrentIndex <= @IndexCount
	BEGIN

		SELECT @idxTableName = [TableName],
		@idxTableID = [ObjectID],
		@idxname = [IndexName],
		@idxid = [IndexID],
		@IsPrimaryKey = [IsPrimaryKey],
		@FillFactor = [FillFactor],
		@FilterDefinition = [FilterDefinition]
		FROM #IndexListing
		WHERE [IndexListingID] = @CurrentIndex

		-- So - it is either an index or a constraint
		-- Check if the index is unique
		IF (@IsPrimaryKey = 1) AND (@NewFilegroupName IS NULL)
		BEGIN
			SET @sSQL = 'ALTER TABLE [dbo].[' + @idxTableName + '] ADD CONSTRAINT [' + @idxname + '] PRIMARY KEY '
			-- Check if the index is clustered
			IF (INDEXPROPERTY(@idxTableID, @idxname, 'IsClustered') = 0)
			BEGIN
				SET @sSQL = @sSQL + 'NON'
				SET @IsClustered = 0
			END
			ELSE
			BEGIN
				SET @IsClustered = 1
			END
			SET @sSQL = @sSQL + 'CLUSTERED' + CHAR(13) + '(' + CHAR(13)
		END
		ELSE
		BEGIN
			SET @sSQL = 'CREATE '
			-- Check if the index is unique
			IF (INDEXPROPERTY(@idxTableID, @idxname, 'IsUnique') = 1)
			BEGIN
				SET @sSQL = @sSQL + 'UNIQUE '
			END
			-- Check if the index is clustered
			IF (INDEXPROPERTY(@idxTableID, @idxname, 'IsClustered') = 1)
			BEGIN
				SET @sSQL = @sSQL + 'CLUSTERED '
				SET @IsClustered = 1
			END
			ELSE
			BEGIN
				SET @IsClustered = 0
			END

			SELECT
			@sSQL = @sSQL + 'INDEX [' + @idxname + '] ON [dbo].[' + @idxTableName + ']' + CHAR(13) + '(' + CHAR(13),
			@colCount = 0
		END

		-- Get the number of cols in the index
		SELECT @colCount = COUNT(*)
		FROM sys.index_columns ic
		INNER JOIN sys.columns sc ON ic.object_id = sc.object_id AND ic.column_id = sc.column_id
		WHERE ic.object_id = @idxtableid AND index_id = @idxid 

		-- Get the file group info
		SELECT @location = f.[name]
		FROM sys.indexes i
		INNER JOIN sys.filegroups f ON i.data_space_id = f.data_space_id
		INNER JOIN sys.all_objects o ON i.[object_id] = o.[object_id]
		WHERE o.object_id = @idxTableID AND i.index_id = @idxid

		-- Get all columns of the index
		INSERT INTO #ColumnListing( [ColumnIDInTable], [Name], [ColumnIDInIndex], [IsIncludedColumn] )
		SELECT sc.column_id, sc.name, ic.index_column_id, ic.is_included_column
		FROM sys.index_columns ic
		INNER JOIN sys.columns sc ON ic.object_id = sc.object_id AND ic.column_id = sc.column_id
		WHERE ic.object_id = @idxTableID AND index_id = @idxid
		ORDER BY ic.index_column_id

		SELECT @MaxColNotAnInclude = NULL
		
		SELECT @MaxColNotAnInclude = MAX([ColumnIDInIndex])
		FROM #ColumnListing
		WHERE [IsIncludedColumn] = 0

		IF @@ROWCOUNT > 0
		BEGIN

			SELECT @IxFirstColumn = 1, @sIncludeCols = '', @sIndexCols = ''

			SELECT @CurrentCol = 1

			WHILE @CurrentCol <= @colCount
			BEGIN
			
				SELECT @ColumnIDInTable = ColumnIDInTable,
				@Name = Name,
				@ColumnIDInIndex = ColumnIDInIndex,
				@IsIncludedColumn = IsIncludedColumn
				FROM #ColumnListing
				WHERE [ColumnListingID] = @CurrentCol
				
				IF @IsIncludedColumn = 0
				BEGIN

					SET @sIndexCols = CHAR(9) + @sIndexCols + '[' + @Name + '] '

					-- Check the sort order of the index cols 
					IF (INDEXKEY_PROPERTY (@idxTableID,@idxid,@ColumnIDInIndex,'IsDescending')) = 0
					BEGIN
						SET @sIndexCols = @sIndexCols + ' ASC '
					END
					ELSE
					BEGIN
						SET @sIndexCols = @sIndexCols + ' DESC '
					END

					IF @CurrentCol < @MaxColNotAnInclude
					BEGIN
						SET @sIndexCols = @sIndexCols + ', '
					END

				END
				ELSE
				BEGIN
					-- Check for any include columns
					IF LEN(@sIncludeCols) > 0
					BEGIN
						SET @sIncludeCols = @sIncludeCols + ','
					END

					SET @sIncludeCols = @sIncludeCols + '[' + @Name + ']'

				END

				SET @CurrentCol = @CurrentCol + 1
			END /* WHILE loop for index columns */

			TRUNCATE TABLE #ColumnListing

			-- Append INCLUDE statement to the index definition
			IF LEN(@sIncludeCols) > 0
			BEGIN
				SET @sIndexCols = @sSQL + @sIndexCols + CHAR(13) + ') ' + ' INCLUDE ( ' + @sIncludeCols + ' ) '
			END
			ELSE
			BEGIN
				SET @sIndexCols = @sSQL + @sIndexCols + CHAR(13) + ') '
			END

			-- Add filtering
			IF @FilterDefinition IS NOT NULL
			BEGIN
				SET @sFilterSQL = ' WHERE ' + @FilterDefinition + ' ' + CHAR(13)
			END
			ELSE
			BEGIN
				SET @sFilterSQL = ''
			END

			-- Build the options
			SET @sParamSQL = 'WITH ( PAD_INDEX = '

			IF INDEXPROPERTY(@idxTableID, @idxname, 'IsPadIndex') = 1
			BEGIN
				SET @sParamSQL = @sParamSQL + 'ON,'
			END
			ELSE
			BEGIN
				SET @sParamSQL = @sParamSQL + 'OFF,'
			END

			SET @sParamSQL = @sParamSQL + ' ALLOW_PAGE_LOCKS = '


			IF INDEXPROPERTY(@idxTableID, @idxname, 'IsPageLockDisallowed') = 0
			BEGIN
				SET @sParamSQL = @sParamSQL + 'ON,'
			END
			ELSE
			BEGIN
				SET @sParamSQL = @sParamSQL + 'OFF,'
			END

			SET @sParamSQL = @sParamSQL + ' ALLOW_ROW_LOCKS = '

			IF INDEXPROPERTY(@idxTableID, @idxname, 'IsRowLockDisallowed') = 0
			BEGIN
				SET @sParamSQL = @sParamSQL + 'ON,'
			END
			ELSE
			BEGIN
				SET @sParamSQL = @sParamSQL + 'OFF,'
			END


			SET @sParamSQL = @sParamSQL + ' STATISTICS_NORECOMPUTE = '

			-- THIS DOES NOT WORK PROPERLY; IsStatistics only says what generated the last set, not what it was set to do.
			IF (INDEXPROPERTY(@idxTableID, @idxname, 'IsStatistics') = 1)
			BEGIN
				SET @sParamSQL = @sParamSQL + 'ON'
			END
			ELSE
			BEGIN
				SET @sParamSQL = @sParamSQL + 'OFF'
			END

			/* Fillfactor is only relevant if padding is enabled */
			IF INDEXPROPERTY(@idxTableID, @idxname, 'IsPadIndex') = 1 AND ISNULL( @FillFactor, 0 ) <> 0 
			BEGIN
				SET @sParamSQL = @sParamSQL + ' ,FILLFACTOR = ' + CAST( ISNULL( @FillFactor, 0 ) AS VARCHAR(3) )
			END

			/*
				JWG 2011-05-26
				If we are doing this to move a table & all its indexes to a new filegroup, specify DROP_EXISTING 
				to remove the old copy afterwards.
			*/
			IF @NewFilegroupName IS NOT NULL
			BEGIN
				SET @sParamSQL = @sParamSQL + ' , DROP_EXISTING = ON '
			END
			
			SET @sParamSQL = @sParamSQL + ' ) '

			SET @sSQL = @sIndexCols + CHAR(13) + @sFilterSQL + CHAR(13) + @sParamSQL

			-- 2008 R2 allows ON [filegroup] for primary keys as well, negating the old "IF THE INDEX IS NOT A PRIMARY KEY - ADD THIS - ELSE DO NOT" IsPrimaryKey IF statement
			SET @sSQL = @sSQL + ' ON [' + COALESCE(@NewFilegroupName, @location) + ']'

			/*IF @FilterDefinition IS NOT NULL
			BEGIN
				INSERT INTO #IndexSQL (TableName, IndexName, IsClustered, IsPrimaryKey, IndexCreateSQL) VALUES (@idxTableName, @idxName, @IsClustered, @IsPrimaryKey, 'SET ANSI_NULLS ON')
				INSERT INTO #IndexSQL (TableName, IndexName, IsClustered, IsPrimaryKey, IndexCreateSQL) VALUES (@idxTableName, @idxName, @IsClustered, @IsPrimaryKey, 'GO')
				INSERT INTO #IndexSQL (TableName, IndexName, IsClustered, IsPrimaryKey, IndexCreateSQL) VALUES (@idxTableName, @idxName, @IsClustered, @IsPrimaryKey, 'SET QUOTED_IDENTIFIER ON')
				INSERT INTO #IndexSQL (TableName, IndexName, IsClustered, IsPrimaryKey, IndexCreateSQL) VALUES (@idxTableName, @idxName, @IsClustered, @IsPrimaryKey, 'GO')
			END*/
			IF COALESCE(@DoNotScriptExists, 0) = 0
			BEGIN
				INSERT INTO #IndexSQL (TableName, IndexName, IsClustered, IsPrimaryKey, IndexCreateSQL) VALUES (@idxTableName, @idxName, @IsClustered, @IsPrimaryKey, 'IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(''' + @idxTableName + ''') ) ')
			END
			INSERT INTO #IndexSQL (TableName, IndexName, IsClustered, IsPrimaryKey, IndexCreateSQL) VALUES (@idxTableName, @idxName, @IsClustered, @IsPrimaryKey, @sSQL)
			--IF COALESCE(@DoNotScriptExists, 0) = 0
			--BEGIN
				INSERT INTO #IndexSQL (TableName, IndexName, IsClustered, IsPrimaryKey, IndexCreateSQL) VALUES (@idxTableName, @idxName, @IsClustered, @IsPrimaryKey, 'GO')
			--END
			/*IF @FilterDefinition IS NOT NULL
			BEGIN
				INSERT INTO #IndexSQL (TableName, IndexName, IsClustered, IsPrimaryKey, IndexCreateSQL) VALUES (@idxTableName, @idxName, @IsClustered, @IsPrimaryKey, 'SET ANSI_NULLS OFF')
				INSERT INTO #IndexSQL (TableName, IndexName, IsClustered, IsPrimaryKey, IndexCreateSQL) VALUES (@idxTableName, @idxName, @IsClustered, @IsPrimaryKey, 'GO')
				INSERT INTO #IndexSQL (TableName, IndexName, IsClustered, IsPrimaryKey, IndexCreateSQL) VALUES (@idxTableName, @idxName, @IsClustered, @IsPrimaryKey, 'SET QUOTED_IDENTIFIER OFF')
				INSERT INTO #IndexSQL (TableName, IndexName, IsClustered, IsPrimaryKey, IndexCreateSQL) VALUES (@idxTableName, @idxName, @IsClustered, @IsPrimaryKey, 'GO')
			END*/

		END

		SET @CurrentIndex = @CurrentIndex + 1
	END
	
	/* Output the results */
	SELECT i.IndexCreateSQL 
	FROM #IndexSQL i 

	DROP TABLE #ColumnListing
	DROP TABLE #IndexListing
	DROP TABLE #IndexSQL
	
END			





GO
GRANT VIEW DEFINITION ON  [dbo].[MigrationIndexCatalog] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MigrationIndexCatalog] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MigrationIndexCatalog] TO [sp_executeall]
GO
