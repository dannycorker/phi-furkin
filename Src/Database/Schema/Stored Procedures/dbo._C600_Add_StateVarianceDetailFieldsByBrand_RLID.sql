SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Nessa Green
-- Create date: 2021-04-27
-- Description: Sets the valueint of DetailFieldID: 314232 'State Variance Detail Fields'(Policy Admin Matter, Internal)
--				Based on _C605_Add_StateVarianceDetailFields_RLID
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Add_StateVarianceDetailFieldsByBrand_RLID]
	/*switches*/
	@BrandId INT,
	@MatterID INT


AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ClientID INT = (SELECT ClientID FROM Matter WITH (NOLOCK) WHERE MatterID = @MatterID)

	IF @ClientID <> 607 /*For FURKIN/PHI ONLY*/
	BEGIN

		RETURN;

	END

    DECLARE @StateVarianceDetailFieldsRLID INT
	
	/*Furkin*/
	IF @BrandId = 2000184
	BEGIN

		SELECT @StateVarianceDetailFieldsRLID = 2002824

	END

	/*PHI*/
	IF @BrandId = 2002314
	BEGIN

		SELECT @StateVarianceDetailFieldsRLID = 2002825

	END

	/*having identified the brand, insert correct RLID into matter detail value*/
	IF @StateVarianceDetailFieldsRLID IS NOT NULL
	BEGIN
		EXEC _C00_SimpleValueIntoField 314232, @StateVarianceDetailFieldsRLID, @MatterID, 58552 /*Aquarium Automation*/
	END
	ELSE
	BEGIN
		RETURN;
	END

END

GO
