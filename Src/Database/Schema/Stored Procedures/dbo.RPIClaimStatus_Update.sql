SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the RPIClaimStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClaimStatus_Update]
(

	@RPIClaimStatusID int   ,

	@MatterID int   ,

	@ClientID int   ,

	@ApplicationID varchar (150)  ,

	@ActivityEngineGuid varchar (250)  ,

	@PhaseCacheID varchar (250)  ,

	@PhaseCacheName varchar (250)  ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@RPIAsUser varchar (250)  ,

	@RPIUser varchar (250)  ,

	@RPIPass varchar (250)  ,

	@Notes varchar (500)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[RPIClaimStatus]
				SET
					[MatterID] = @MatterID
					,[ClientID] = @ClientID
					,[ApplicationID] = @ApplicationID
					,[ActivityEngineGuid] = @ActivityEngineGuid
					,[PhaseCacheID] = @PhaseCacheID
					,[PhaseCacheName] = @PhaseCacheName
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[RPIAsUser] = @RPIAsUser
					,[RPIUser] = @RPIUser
					,[RPIPass] = @RPIPass
					,[Notes] = @Notes
				WHERE
[RPIClaimStatusID] = @RPIClaimStatusID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClaimStatus_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClaimStatus_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClaimStatus_Update] TO [sp_executeall]
GO
