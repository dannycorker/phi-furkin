SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the GroupAndUserEditingControl table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupAndUserEditingControl_Update]
(

	@EditingID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@ClientPersonnelID int   ,

	@WhoIsEditing int   ,

	@EditStartedAt datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[GroupAndUserEditingControl]
				SET
					[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
					,[ClientPersonnelID] = @ClientPersonnelID
					,[WhoIsEditing] = @WhoIsEditing
					,[EditStartedAt] = @EditStartedAt
				WHERE
[EditingID] = @EditingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupAndUserEditingControl_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupAndUserEditingControl_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupAndUserEditingControl_Update] TO [sp_executeall]
GO
