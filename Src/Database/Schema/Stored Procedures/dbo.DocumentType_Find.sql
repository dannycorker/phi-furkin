SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DocumentType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentType_Find]
(

	@SearchUsingOR bit   = null ,

	@DocumentTypeID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@DocumentTypeName varchar (50)  = null ,

	@DocumentTypeDescription varchar (250)  = null ,

	@Header varchar (MAX)  = null ,

	@Template varchar (MAX)  = null ,

	@Footer varchar (MAX)  = null ,

	@CanBeAutoSent bit   = null ,

	@EmailSubject varchar (255)  = null ,

	@EmailBodyText varchar (MAX)  = null ,

	@InputFormat varchar (24)  = null ,

	@OutputFormat varchar (24)  = null ,

	@Enabled bit   = null ,

	@RecipientsTo varchar (MAX)  = null ,

	@RecipientsCC varchar (MAX)  = null ,

	@RecipientsBCC varchar (MAX)  = null ,

	@ReadOnlyTo bit   = null ,

	@ReadOnlyCC bit   = null ,

	@ReadOnlyBCC bit   = null ,

	@SendToMultipleRecipients bit   = null ,

	@MultipleRecipientDataSourceType int   = null ,

	@MultipleRecipientDataSourceID int   = null ,

	@SendToAllByDefault bit   = null ,

	@ExcelTemplatePath varchar (1024)  = null ,

	@FromDetails varchar (500)  = null ,

	@ReadOnlyFrom bit   = null ,

	@SourceID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@FolderID int   = null ,

	@IsThunderheadTemplate bit   = null ,

	@ThunderheadUniqueTemplateID varchar (200)  = null ,

	@ThunderheadDocumentFormat varchar (10)  = null ,

	@DocumentTitleTemplate varchar (MAX)  = null ,

	@CreateNewVersionWhenSaved bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DocumentTypeID]
	, [ClientID]
	, [LeadTypeID]
	, [DocumentTypeName]
	, [DocumentTypeDescription]
	, [Header]
	, [Template]
	, [Footer]
	, [CanBeAutoSent]
	, [EmailSubject]
	, [EmailBodyText]
	, [InputFormat]
	, [OutputFormat]
	, [Enabled]
	, [RecipientsTo]
	, [RecipientsCC]
	, [RecipientsBCC]
	, [ReadOnlyTo]
	, [ReadOnlyCC]
	, [ReadOnlyBCC]
	, [SendToMultipleRecipients]
	, [MultipleRecipientDataSourceType]
	, [MultipleRecipientDataSourceID]
	, [SendToAllByDefault]
	, [ExcelTemplatePath]
	, [FromDetails]
	, [ReadOnlyFrom]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [FolderID]
	, [IsThunderheadTemplate]
	, [ThunderheadUniqueTemplateID]
	, [ThunderheadDocumentFormat]
	, [DocumentTitleTemplate]
	, [CreateNewVersionWhenSaved]
    FROM
	[dbo].[DocumentType] WITH (NOLOCK) 
    WHERE 
	 ([DocumentTypeID] = @DocumentTypeID OR @DocumentTypeID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([DocumentTypeName] = @DocumentTypeName OR @DocumentTypeName IS NULL)
	AND ([DocumentTypeDescription] = @DocumentTypeDescription OR @DocumentTypeDescription IS NULL)
	AND ([Header] = @Header OR @Header IS NULL)
	AND ([Template] = @Template OR @Template IS NULL)
	AND ([Footer] = @Footer OR @Footer IS NULL)
	AND ([CanBeAutoSent] = @CanBeAutoSent OR @CanBeAutoSent IS NULL)
	AND ([EmailSubject] = @EmailSubject OR @EmailSubject IS NULL)
	AND ([EmailBodyText] = @EmailBodyText OR @EmailBodyText IS NULL)
	AND ([InputFormat] = @InputFormat OR @InputFormat IS NULL)
	AND ([OutputFormat] = @OutputFormat OR @OutputFormat IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([RecipientsTo] = @RecipientsTo OR @RecipientsTo IS NULL)
	AND ([RecipientsCC] = @RecipientsCC OR @RecipientsCC IS NULL)
	AND ([RecipientsBCC] = @RecipientsBCC OR @RecipientsBCC IS NULL)
	AND ([ReadOnlyTo] = @ReadOnlyTo OR @ReadOnlyTo IS NULL)
	AND ([ReadOnlyCC] = @ReadOnlyCC OR @ReadOnlyCC IS NULL)
	AND ([ReadOnlyBCC] = @ReadOnlyBCC OR @ReadOnlyBCC IS NULL)
	AND ([SendToMultipleRecipients] = @SendToMultipleRecipients OR @SendToMultipleRecipients IS NULL)
	AND ([MultipleRecipientDataSourceType] = @MultipleRecipientDataSourceType OR @MultipleRecipientDataSourceType IS NULL)
	AND ([MultipleRecipientDataSourceID] = @MultipleRecipientDataSourceID OR @MultipleRecipientDataSourceID IS NULL)
	AND ([SendToAllByDefault] = @SendToAllByDefault OR @SendToAllByDefault IS NULL)
	AND ([ExcelTemplatePath] = @ExcelTemplatePath OR @ExcelTemplatePath IS NULL)
	AND ([FromDetails] = @FromDetails OR @FromDetails IS NULL)
	AND ([ReadOnlyFrom] = @ReadOnlyFrom OR @ReadOnlyFrom IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([FolderID] = @FolderID OR @FolderID IS NULL)
	AND ([IsThunderheadTemplate] = @IsThunderheadTemplate OR @IsThunderheadTemplate IS NULL)
	AND ([ThunderheadUniqueTemplateID] = @ThunderheadUniqueTemplateID OR @ThunderheadUniqueTemplateID IS NULL)
	AND ([ThunderheadDocumentFormat] = @ThunderheadDocumentFormat OR @ThunderheadDocumentFormat IS NULL)
	AND ([DocumentTitleTemplate] = @DocumentTitleTemplate OR @DocumentTitleTemplate IS NULL)
	AND ([CreateNewVersionWhenSaved] = @CreateNewVersionWhenSaved OR @CreateNewVersionWhenSaved IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DocumentTypeID]
	, [ClientID]
	, [LeadTypeID]
	, [DocumentTypeName]
	, [DocumentTypeDescription]
	, [Header]
	, [Template]
	, [Footer]
	, [CanBeAutoSent]
	, [EmailSubject]
	, [EmailBodyText]
	, [InputFormat]
	, [OutputFormat]
	, [Enabled]
	, [RecipientsTo]
	, [RecipientsCC]
	, [RecipientsBCC]
	, [ReadOnlyTo]
	, [ReadOnlyCC]
	, [ReadOnlyBCC]
	, [SendToMultipleRecipients]
	, [MultipleRecipientDataSourceType]
	, [MultipleRecipientDataSourceID]
	, [SendToAllByDefault]
	, [ExcelTemplatePath]
	, [FromDetails]
	, [ReadOnlyFrom]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [FolderID]
	, [IsThunderheadTemplate]
	, [ThunderheadUniqueTemplateID]
	, [ThunderheadDocumentFormat]
	, [DocumentTitleTemplate]
	, [CreateNewVersionWhenSaved]
    FROM
	[dbo].[DocumentType] WITH (NOLOCK) 
    WHERE 
	 ([DocumentTypeID] = @DocumentTypeID AND @DocumentTypeID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([DocumentTypeName] = @DocumentTypeName AND @DocumentTypeName is not null)
	OR ([DocumentTypeDescription] = @DocumentTypeDescription AND @DocumentTypeDescription is not null)
	OR ([Header] = @Header AND @Header is not null)
	OR ([Template] = @Template AND @Template is not null)
	OR ([Footer] = @Footer AND @Footer is not null)
	OR ([CanBeAutoSent] = @CanBeAutoSent AND @CanBeAutoSent is not null)
	OR ([EmailSubject] = @EmailSubject AND @EmailSubject is not null)
	OR ([EmailBodyText] = @EmailBodyText AND @EmailBodyText is not null)
	OR ([InputFormat] = @InputFormat AND @InputFormat is not null)
	OR ([OutputFormat] = @OutputFormat AND @OutputFormat is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([RecipientsTo] = @RecipientsTo AND @RecipientsTo is not null)
	OR ([RecipientsCC] = @RecipientsCC AND @RecipientsCC is not null)
	OR ([RecipientsBCC] = @RecipientsBCC AND @RecipientsBCC is not null)
	OR ([ReadOnlyTo] = @ReadOnlyTo AND @ReadOnlyTo is not null)
	OR ([ReadOnlyCC] = @ReadOnlyCC AND @ReadOnlyCC is not null)
	OR ([ReadOnlyBCC] = @ReadOnlyBCC AND @ReadOnlyBCC is not null)
	OR ([SendToMultipleRecipients] = @SendToMultipleRecipients AND @SendToMultipleRecipients is not null)
	OR ([MultipleRecipientDataSourceType] = @MultipleRecipientDataSourceType AND @MultipleRecipientDataSourceType is not null)
	OR ([MultipleRecipientDataSourceID] = @MultipleRecipientDataSourceID AND @MultipleRecipientDataSourceID is not null)
	OR ([SendToAllByDefault] = @SendToAllByDefault AND @SendToAllByDefault is not null)
	OR ([ExcelTemplatePath] = @ExcelTemplatePath AND @ExcelTemplatePath is not null)
	OR ([FromDetails] = @FromDetails AND @FromDetails is not null)
	OR ([ReadOnlyFrom] = @ReadOnlyFrom AND @ReadOnlyFrom is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([FolderID] = @FolderID AND @FolderID is not null)
	OR ([IsThunderheadTemplate] = @IsThunderheadTemplate AND @IsThunderheadTemplate is not null)
	OR ([ThunderheadUniqueTemplateID] = @ThunderheadUniqueTemplateID AND @ThunderheadUniqueTemplateID is not null)
	OR ([ThunderheadDocumentFormat] = @ThunderheadDocumentFormat AND @ThunderheadDocumentFormat is not null)
	OR ([DocumentTitleTemplate] = @DocumentTitleTemplate AND @DocumentTitleTemplate is not null)
	OR ([CreateNewVersionWhenSaved] = @CreateNewVersionWhenSaved AND @CreateNewVersionWhenSaved is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType_Find] TO [sp_executeall]
GO
