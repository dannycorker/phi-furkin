SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DataLoaderObjectType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectType_Update]
(

	@DataLoaderObjectTypeID int   ,

	@OriginalDataLoaderObjectTypeID int   ,

	@ObjectTypeName varchar (250)  ,

	@ObjectTypeDescription varchar (2000)  ,

	@ProcessingOrder int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DataLoaderObjectType]
				SET
					[DataLoaderObjectTypeID] = @DataLoaderObjectTypeID
					,[ObjectTypeName] = @ObjectTypeName
					,[ObjectTypeDescription] = @ObjectTypeDescription
					,[ProcessingOrder] = @ProcessingOrder
				WHERE
[DataLoaderObjectTypeID] = @OriginalDataLoaderObjectTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectType_Update] TO [sp_executeall]
GO
