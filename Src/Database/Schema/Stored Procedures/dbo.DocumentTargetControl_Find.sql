SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DocumentTargetControl table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTargetControl_Find]
(

	@SearchUsingOR bit   = null ,

	@DocumentTargetControlID int   = null ,

	@ClientID int   = null ,

	@DocumentTypeID int   = null ,

	@LastParsed datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DocumentTargetControlID]
	, [ClientID]
	, [DocumentTypeID]
	, [LastParsed]
    FROM
	[dbo].[DocumentTargetControl] WITH (NOLOCK) 
    WHERE 
	 ([DocumentTargetControlID] = @DocumentTargetControlID OR @DocumentTargetControlID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([DocumentTypeID] = @DocumentTypeID OR @DocumentTypeID IS NULL)
	AND ([LastParsed] = @LastParsed OR @LastParsed IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DocumentTargetControlID]
	, [ClientID]
	, [DocumentTypeID]
	, [LastParsed]
    FROM
	[dbo].[DocumentTargetControl] WITH (NOLOCK) 
    WHERE 
	 ([DocumentTargetControlID] = @DocumentTargetControlID AND @DocumentTargetControlID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([DocumentTypeID] = @DocumentTypeID AND @DocumentTypeID is not null)
	OR ([LastParsed] = @LastParsed AND @LastParsed is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTargetControl_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTargetControl_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTargetControl_Find] TO [sp_executeall]
GO
