SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-07-31
-- Description:	Remove duplicate LDV and/or MDV. 
--              Scope for RLDV and TDV to be added later.
-- =============================================
CREATE PROCEDURE [dbo].[_C00_RemoveDuplicateDetailValues]
	@WhatToRemove varchar(10),
	@ClientID int = NULL 
AS
BEGIN
	SET NOCOUNT ON;

	/* CaseDetailValues */
	IF @WhatToRemove IN ('CADV', 'ALL')
	BEGIN
		IF @ClientID IS NULL
		BEGIN
			DELETE dv1 
			FROM dbo.CaseDetailValues dv1 
			INNER JOIN dbo.CaseDetailValues dv2 ON dv2.CaseID = dv1.CaseID AND dv2.DetailFieldID = dv1.DetailFieldID 
			WHERE dv2.CaseDetailValueID > dv1.CaseDetailValueID 
		END
		ELSE
		BEGIN
			DELETE dv1 
			FROM dbo.CaseDetailValues dv1 
			INNER JOIN dbo.CaseDetailValues dv2 ON dv2.CaseID = dv1.CaseID AND dv2.DetailFieldID = dv1.DetailFieldID 
			WHERE dv2.CaseDetailValueID > dv1.CaseDetailValueID 
			AND dv1.ClientID = @ClientID
		END
	END

	/* CustomerDetailValues */
	IF @WhatToRemove IN ('CDV', 'ALL')
	BEGIN
		IF @ClientID IS NULL
		BEGIN
			DELETE dv1 
			FROM dbo.CustomerDetailValues dv1 
			INNER JOIN dbo.CustomerDetailValues dv2 ON dv2.CustomerID = dv1.CustomerID AND dv2.DetailFieldID = dv1.DetailFieldID 
			WHERE dv2.CustomerDetailValueID > dv1.CustomerDetailValueID 
		END
		ELSE
		BEGIN
			DELETE dv1 
			FROM dbo.CustomerDetailValues dv1 
			INNER JOIN dbo.CustomerDetailValues dv2 ON dv2.CustomerID = dv1.CustomerID AND dv2.DetailFieldID = dv1.DetailFieldID 
			WHERE dv2.CustomerDetailValueID > dv1.CustomerDetailValueID 
			AND dv1.ClientID = @ClientID
		END
	END

	/* ClientDetailValues */
	IF @WhatToRemove IN ('CLDV', 'ALL')
	BEGIN
		IF @ClientID IS NULL
		BEGIN
			DELETE dv1 
			FROM dbo.ClientDetailValues dv1 
			INNER JOIN dbo.ClientDetailValues dv2 ON dv2.ClientID = dv1.ClientID AND dv2.DetailFieldID = dv1.DetailFieldID 
			WHERE dv2.ClientDetailValueID > dv1.ClientDetailValueID 
		END
		ELSE
		BEGIN
			DELETE dv1 
			FROM dbo.ClientDetailValues dv1 
			INNER JOIN dbo.ClientDetailValues dv2 ON dv2.ClientID = dv1.ClientID AND dv2.DetailFieldID = dv1.DetailFieldID 
			WHERE dv2.ClientDetailValueID > dv1.ClientDetailValueID 
			AND dv1.ClientID = @ClientID
		END
	END
	
	/* ContactDetailValues */
	IF @WhatToRemove IN ('CODV', 'ALL')
	BEGIN
		IF @ClientID IS NULL
		BEGIN
			DELETE dv1 
			FROM dbo.ContactDetailValues dv1 
			INNER JOIN dbo.ContactDetailValues dv2 ON dv2.ContactID = dv1.ContactID AND dv2.DetailFieldID = dv1.DetailFieldID 
			WHERE dv2.ContactDetailValueID > dv1.ContactDetailValueID 
		END
		ELSE
		BEGIN
			DELETE dv1 
			FROM dbo.ContactDetailValues dv1 
			INNER JOIN dbo.ContactDetailValues dv2 ON dv2.ContactID = dv1.ContactID AND dv2.DetailFieldID = dv1.DetailFieldID 
			WHERE dv2.ContactDetailValueID > dv1.ContactDetailValueID 
			AND dv1.ClientID = @ClientID
		END
	END
	
	/* ClientPersonnelDetailValues */
	IF @WhatToRemove IN ('CPDV', 'ALL')
	BEGIN
		IF @ClientID IS NULL
		BEGIN
			DELETE dv1 
			FROM dbo.ClientPersonnelDetailValues dv1 
			INNER JOIN dbo.ClientPersonnelDetailValues dv2 ON dv2.ClientPersonnelID = dv1.ClientPersonnelID AND dv2.DetailFieldID = dv1.DetailFieldID 
			WHERE dv2.ClientPersonnelDetailValueID > dv1.ClientPersonnelDetailValueID 
		END
		ELSE
		BEGIN
			DELETE dv1 
			FROM dbo.ClientPersonnelDetailValues dv1 
			INNER JOIN dbo.ClientPersonnelDetailValues dv2 ON dv2.ClientPersonnelID = dv1.ClientPersonnelID AND dv2.DetailFieldID = dv1.DetailFieldID 
			WHERE dv2.ClientPersonnelDetailValueID > dv1.ClientPersonnelDetailValueID 
			AND dv1.ClientID = @ClientID
		END
	END


	/* LeadDetailValues */
	IF @WhatToRemove IN ('LDV', 'ALL')
	BEGIN
		IF @ClientID IS NULL
		BEGIN
			DELETE ldv1 
			FROM dbo.LeadDetailValues ldv1 
			INNER JOIN dbo.LeadDetailValues ldv2 ON ldv2.leadID = ldv1.leadID AND ldv2.DetailFieldID = ldv1.DetailFieldID 
			WHERE ldv2.leadDetailValueID > ldv1.LeadDetailValueID 
		END
		ELSE
		BEGIN
			DELETE ldv1 
			FROM dbo.LeadDetailValues ldv1 
			INNER JOIN dbo.LeadDetailValues ldv2 ON ldv2.leadID = ldv1.leadID AND ldv2.DetailFieldID = ldv1.DetailFieldID 
			WHERE ldv2.LeadDetailValueID > ldv1.LeadDetailValueID 
			AND ldv1.ClientID = @ClientID
		END
	END

	/* MatterDetailValues */
	IF @WhatToRemove IN ('MDV', 'ALL')
	BEGIN
		IF @ClientID IS NULL
		BEGIN
			DELETE mdv1 
			FROM dbo.MatterDetailValues mdv1
			INNER JOIN dbo.MatterDetailValues mdv2 ON mdv2.MatterID = mdv1.MatterID AND mdv2.DetailFieldID = mdv1.DetailFieldID
			WHERE mdv2.MatterDetailValueID > mdv1.MatterDetailValueID
		END
		ELSE
		BEGIN
			DELETE mdv1 
			FROM dbo.MatterDetailValues mdv1
			INNER JOIN dbo.MatterDetailValues mdv2 ON mdv2.MatterID = mdv1.MatterID AND mdv2.DetailFieldID = mdv1.DetailFieldID
			WHERE mdv2.MatterDetailValueID > mdv1.MatterDetailValueID
			AND mdv1.ClientID = @ClientID
		END
	END

	/* ResourceListDetailValues */
	IF @WhatToRemove IN ('RLDV', 'ALL')
	BEGIN
		IF @ClientID IS NULL
		BEGIN
			DELETE dv 
			OUTPUT 4, deleted.ResourceListDetailValueID, deleted.ResourceListID, deleted.DetailValue, deleted.DetailFieldID 
			INTO dbo.__DeletedValue (DetailFieldSubTypeID, DetailValueID, ParentID, DetailValue, DetailFieldID)
			FROM dbo.ResourceListDetailValues dv 
			INNER JOIN ResourceListDetailValues i ON i.ResourceListID = dv.ResourceListID AND i.DetailFieldID = dv.DetailFieldID AND i.ResourceListDetailValueID > dv.ResourceListDetailValueID 
		END
		ELSE
		BEGIN
			DELETE dv 
			OUTPUT 4, deleted.ResourceListDetailValueID, deleted.ResourceListID, deleted.DetailValue, deleted.DetailFieldID 
			INTO dbo.__DeletedValue (DetailFieldSubTypeID, DetailValueID, ParentID, DetailValue, DetailFieldID)
			FROM dbo.ResourceListDetailValues dv 
			INNER JOIN ResourceListDetailValues i ON i.ResourceListID = dv.ResourceListID AND i.DetailFieldID = dv.DetailFieldID AND i.ResourceListDetailValueID > dv.ResourceListDetailValueID 
			AND dv.ClientID = @ClientID
		END
	END
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RemoveDuplicateDetailValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_RemoveDuplicateDetailValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RemoveDuplicateDetailValues] TO [sp_executeall]
GO
