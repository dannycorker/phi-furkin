SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Author:		Ben Crinion
-- Create date: 02/10/2007
-- Description:	Select ClientPersonnelOptions by ClientPersonnelID and ClientPersonnelOptionTypeID
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelOptions__GetByClientPersonnelIDAndOptionTypeID]
(

	@ClientPersonnelID int,
	@ClientPersonneOptionTypeID int
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ClientPersonnelOptionID],
					[ClientPersonnelID],
					[ClientPersonnelOptionTypeID],
					[OptionValue]
				FROM
					[dbo].[ClientPersonnelOptions]
				WHERE
					[ClientPersonnelID] = @ClientPersonnelID
					AND [ClientPersonnelOptionTypeID] = @ClientPersonneOptionTypeID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptions__GetByClientPersonnelIDAndOptionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelOptions__GetByClientPersonnelIDAndOptionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptions__GetByClientPersonnelIDAndOptionTypeID] TO [sp_executeall]
GO
