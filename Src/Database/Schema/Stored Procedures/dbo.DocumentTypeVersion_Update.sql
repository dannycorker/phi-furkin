SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DocumentTypeVersion table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTypeVersion_Update]
(

	@DocumentTypeVersionID int   ,

	@DocumentTypeID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@DocumentTypeName varchar (50)  ,

	@DocumentTypeDescription varchar (250)  ,

	@Header varchar (MAX)  ,

	@Template varchar (MAX)  ,

	@Footer varchar (MAX)  ,

	@CanBeAutoSent bit   ,

	@EmailSubject varchar (255)  ,

	@EmailBodyText varchar (MAX)  ,

	@InputFormat varchar (24)  ,

	@OutputFormat varchar (24)  ,

	@Enabled bit   ,

	@RecipientsTo varchar (MAX)  ,

	@RecipientsCC varchar (MAX)  ,

	@RecipientsBCC varchar (MAX)  ,

	@ReadOnlyTo bit   ,

	@ReadOnlyCC bit   ,

	@ReadOnlyBCC bit   ,

	@SendToMultipleRecipients bit   ,

	@MultipleRecipientDataSourceType int   ,

	@MultipleRecipientDataSourceID int   ,

	@SendToAllByDefault bit   ,

	@ExcelTemplatePath varchar (1024)  ,

	@FromDetails varchar (500)  ,

	@ReadOnlyFrom bit   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@FolderID int   ,

	@IsThunderheadTemplate bit   ,

	@ThunderheadUniqueTemplateID varchar (200)  ,

	@ThunderheadDocumentFormat varchar (10)  ,

	@DocumentTitleTemplate varchar (MAX)  ,

	@CreatedBy int   ,

	@CreatedOn datetime   ,

	@VersionNumber int   ,

	@ActiveFromDate datetime   ,

	@Archived bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DocumentTypeVersion]
				SET
					[DocumentTypeID] = @DocumentTypeID
					,[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[DocumentTypeName] = @DocumentTypeName
					,[DocumentTypeDescription] = @DocumentTypeDescription
					,[Header] = @Header
					,[Template] = @Template
					,[Footer] = @Footer
					,[CanBeAutoSent] = @CanBeAutoSent
					,[EmailSubject] = @EmailSubject
					,[EmailBodyText] = @EmailBodyText
					,[InputFormat] = @InputFormat
					,[OutputFormat] = @OutputFormat
					,[Enabled] = @Enabled
					,[RecipientsTo] = @RecipientsTo
					,[RecipientsCC] = @RecipientsCC
					,[RecipientsBCC] = @RecipientsBCC
					,[ReadOnlyTo] = @ReadOnlyTo
					,[ReadOnlyCC] = @ReadOnlyCC
					,[ReadOnlyBCC] = @ReadOnlyBCC
					,[SendToMultipleRecipients] = @SendToMultipleRecipients
					,[MultipleRecipientDataSourceType] = @MultipleRecipientDataSourceType
					,[MultipleRecipientDataSourceID] = @MultipleRecipientDataSourceID
					,[SendToAllByDefault] = @SendToAllByDefault
					,[ExcelTemplatePath] = @ExcelTemplatePath
					,[FromDetails] = @FromDetails
					,[ReadOnlyFrom] = @ReadOnlyFrom
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[FolderID] = @FolderID
					,[IsThunderheadTemplate] = @IsThunderheadTemplate
					,[ThunderheadUniqueTemplateID] = @ThunderheadUniqueTemplateID
					,[ThunderheadDocumentFormat] = @ThunderheadDocumentFormat
					,[DocumentTitleTemplate] = @DocumentTitleTemplate
					,[CreatedBy] = @CreatedBy
					,[CreatedOn] = @CreatedOn
					,[VersionNumber] = @VersionNumber
					,[ActiveFromDate] = @ActiveFromDate
					,[Archived] = @Archived
				WHERE
[DocumentTypeVersionID] = @DocumentTypeVersionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeVersion_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTypeVersion_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeVersion_Update] TO [sp_executeall]
GO
