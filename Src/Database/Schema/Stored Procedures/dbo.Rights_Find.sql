SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Rights table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Rights_Find]
(

	@SearchUsingOR bit   = null ,

	@RightID int   = null ,

	@RightName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [RightID]
	, [RightName]
    FROM
	[dbo].[Rights] WITH (NOLOCK) 
    WHERE 
	 ([RightID] = @RightID OR @RightID IS NULL)
	AND ([RightName] = @RightName OR @RightName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [RightID]
	, [RightName]
    FROM
	[dbo].[Rights] WITH (NOLOCK) 
    WHERE 
	 ([RightID] = @RightID AND @RightID is not null)
	OR ([RightName] = @RightName AND @RightName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Rights_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Rights_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Rights_Find] TO [sp_executeall]
GO
