SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2016-04-27
-- Description:	Updates the AutomatedTaskCommandLine Table
-- =============================================
CREATE PROCEDURE [dbo].[_C00_AutomatedTaskCommandLine_Update] 
	@AutomatedTaskCommandLineID INT,
	@ClientID INT,
	@AutomatedTaskCommandControlID INT,
	@LineType VARCHAR(200),
	@LineText VARCHAR(2000),
	@LineNotes VARCHAR(2000),
	@LineOrder VARCHAR(2000),
	@Enabled INT

AS
BEGIN

	SET NOCOUNT ON;

	UPDATE [AutomatedTaskCommandLine]	
	SET ClientID = @ClientID,
	AutomatedTaskCommandControlID = @AutomatedTaskCommandControlID,
	LineType = @LineType,
	LineText = @LineText,
	LineNotes = @LineNotes,
	LineOrder = @LineOrder,
	[Enabled] = @Enabled
	WHERE AutomatedTaskCommandLineID = @AutomatedTaskCommandLineID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AutomatedTaskCommandLine_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_AutomatedTaskCommandLine_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AutomatedTaskCommandLine_Update] TO [sp_executeall]
GO
