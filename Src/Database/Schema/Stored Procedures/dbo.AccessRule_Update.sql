SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the AccessRule table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AccessRule_Update]
(

	@AccessRuleID int   ,

	@ClientID int   ,

	@AccessRuleName varchar (100)  ,

	@AccessRuleDescription varchar (250)  ,

	@AccessRuleEnabled bit   ,

	@ClientPersonnelAdminGroupID int   ,

	@ClientPersonnelID int   ,

	@PortalUserID int   ,

	@DataLoaderObjectTypeID int   ,

	@LeadTypeID int   ,

	@DetailFieldSubTypeID int   ,

	@DetailFieldPageID int   ,

	@DetailFieldID int   ,

	@ClientQuestionnaireID int   ,

	@ValueDecoder varchar (50)  ,

	@ValueDecoderColumnFieldID int   ,

	@ValueToCompare int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@AccessLevel tinyint   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[AccessRule]
				SET
					[ClientID] = @ClientID
					,[AccessRuleName] = @AccessRuleName
					,[AccessRuleDescription] = @AccessRuleDescription
					,[AccessRuleEnabled] = @AccessRuleEnabled
					,[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
					,[ClientPersonnelID] = @ClientPersonnelID
					,[PortalUserID] = @PortalUserID
					,[DataLoaderObjectTypeID] = @DataLoaderObjectTypeID
					,[LeadTypeID] = @LeadTypeID
					,[DetailFieldSubTypeID] = @DetailFieldSubTypeID
					,[DetailFieldPageID] = @DetailFieldPageID
					,[DetailFieldID] = @DetailFieldID
					,[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[ValueDecoder] = @ValueDecoder
					,[ValueDecoderColumnFieldID] = @ValueDecoderColumnFieldID
					,[ValueToCompare] = @ValueToCompare
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[AccessLevel] = @AccessLevel
				WHERE
[AccessRuleID] = @AccessRuleID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AccessRule_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AccessRule_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AccessRule_Update] TO [sp_executeall]
GO
