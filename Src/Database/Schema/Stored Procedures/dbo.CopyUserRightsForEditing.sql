SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-29
-- Description:	Copy all User Rights etc to temp/work table
-- =============================================
CREATE PROCEDURE [dbo].[CopyUserRightsForEditing]
	-- Add the parameters for the stored procedure here
	@GroupID int,
	@UserID int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT UserFunctionControlEditing (ClientPersonnelID, ModuleID, FunctionTypeID, HasDescendants, RightID, LeadTypeID)
	SELECT @UserID, ModuleID, FunctionTypeID, HasDescendants, RightID, LeadTypeID
	FROM GroupFunctionControl
	WHERE ClientPersonnelAdminGroupID = @GroupID
	AND NOT EXISTS (SELECT * FROM UserFunctionControl WHERE UserFunctionControl.ClientPersonnelID = @UserID AND UserFunctionControl.ModuleID = GroupFunctionControl.ModuleID AND UserFunctionControl.FunctionTypeID = GroupFunctionControl.FunctionTypeID)
	UNION
	SELECT ClientPersonnelID, ModuleID, FunctionTypeID, HasDescendants, RightID, LeadTypeID
	FROM UserFunctionControl
	WHERE ClientPersonnelID = @UserID

	INSERT UserRightsDynamicEditing (ClientPersonnelID, FunctionTypeID, LeadTypeID, ObjectID, RightID)
	SELECT @UserID, FunctionTypeID, LeadTypeID, ObjectID, RightID
	FROM GroupRightsDynamic
	WHERE ClientPersonnelAdminGroupID = @GroupID
	AND NOT EXISTS (SELECT * FROM UserRightsDynamic WHERE UserRightsDynamic.ClientPersonnelID = @UserID AND UserRightsDynamic.FunctionTypeID = GroupRightsDynamic.FunctionTypeID AND IsNull(UserRightsDynamic.LeadTypeID, -1) = IsNull(GroupRightsDynamic.LeadTypeID, -1) AND UserRightsDynamic.ObjectID = GroupRightsDynamic.ObjectID)
	UNION
	SELECT ClientPersonnelID, FunctionTypeID, LeadTypeID, ObjectID, RightID
	FROM UserRightsDynamic
	WHERE ClientPersonnelID = @UserID
	
END












GO
GRANT VIEW DEFINITION ON  [dbo].[CopyUserRightsForEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CopyUserRightsForEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CopyUserRightsForEditing] TO [sp_executeall]
GO
