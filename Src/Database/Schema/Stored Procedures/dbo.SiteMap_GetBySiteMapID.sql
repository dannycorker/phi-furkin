SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SiteMap table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SiteMap_GetBySiteMapID]
(

	@SiteMapID int   
)
AS


				SELECT
					[SiteMapID],
					[Url],
					[Title],
					[Description],
					[ParentID],
					[Secured]
				FROM
					[dbo].[SiteMap] WITH (NOLOCK) 
				WHERE
										[SiteMapID] = @SiteMapID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SiteMap_GetBySiteMapID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SiteMap_GetBySiteMapID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SiteMap_GetBySiteMapID] TO [sp_executeall]
GO
