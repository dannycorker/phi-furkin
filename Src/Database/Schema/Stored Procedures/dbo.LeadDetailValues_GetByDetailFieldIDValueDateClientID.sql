SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadDetailValues table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadDetailValues_GetByDetailFieldIDValueDateClientID]
(

	@DetailFieldID int   ,

	@ValueDate date   ,

	@ClientID int   
)
AS


				SELECT
					[LeadDetailValueID],
					[ClientID],
					[LeadID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[OriginalDetailValueID],
					[OriginalLeadID],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[LeadDetailValues] WITH (NOLOCK) 
				WHERE
										[DetailFieldID] = @DetailFieldID
					AND [ValueDate] = @ValueDate
					AND [ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDetailValues_GetByDetailFieldIDValueDateClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDetailValues_GetByDetailFieldIDValueDateClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDetailValues_GetByDetailFieldIDValueDateClientID] TO [sp_executeall]
GO
