SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Cathal Sherry
-- ALTER date: 2014-05-19
-- Description:	Update the Customer's ContactId for greenlight
-- =============================================
CREATE PROCEDURE [dbo].[_C272_Greenlight_UpdateContactID]
	 @CustomerID		INT
	,@ContactID			VARCHAR(2000)
	,@ClientPersonnelID	INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @DetailFieldID	INT,
			@ClientID		INT
	
	/*
	Every Greenlight customer ContactID in a customer detail values record.
	First, find the detail field ID
	Next, update the security key
	Return the saved values for confirmation
	*/
	
	SELECT @ClientID = cp.ClientID
	FROM AquariusMaster.dbo.ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.ClientPersonnelID = @ClientPersonnelID	
	
	/*
	TODO - third party map, and cross-database check
	*/
	SELECT @DetailFieldID = df.DetailFieldID 
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.ClientID = @ClientID
	and df.DetailFieldID = 212929 /*Greenlight ContactID*/ /*Temporary for testing*/
	
	IF @DetailFieldID > 0 and @CustomerID > 0
	BEGIN
		EXEC dbo._C00_SimpleValueIntoField @DetailFieldID, @ContactID, @CustomerID, @ClientPersonnelID
	END

	SELECT cdv.DetailValue [ContactID]
	FROM CustomerDetailValues cdv WITH (NOLOCK) 
	WHERE cdv.CustomerID = @CustomerID
	and cdv.DetailFieldID = @DetailFieldID
END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C272_Greenlight_UpdateContactID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C272_Greenlight_UpdateContactID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C272_Greenlight_UpdateContactID] TO [sp_executeall]
GO
