SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Paul Richardson>
-- Create date: <04 February 2010>
-- Description:	<Gets Lead documents that are rtf for conversion into word>
-- =============================================
CREATE PROCEDURE [dbo].[GetLeadDocumentsToConvertFromRtfToWord]

	@ClientID int,
	@MaxRows int = 10

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT TOP (@MaxRows) LeadDocumentID, LeadDocumentTitle, DocumentFormat from LeadDocument with(NOLOCK)
    WHERE documentformat = 'RTF' and ZipFormat is null and ClientID = @ClientID
    
END




GO
GRANT VIEW DEFINITION ON  [dbo].[GetLeadDocumentsToConvertFromRtfToWord] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetLeadDocumentsToConvertFromRtfToWord] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetLeadDocumentsToConvertFromRtfToWord] TO [sp_executeall]
GO
