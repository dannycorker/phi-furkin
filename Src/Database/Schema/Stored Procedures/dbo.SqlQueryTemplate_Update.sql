SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlQueryTemplate table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryTemplate_Update]
(

	@SqlQueryTemplateID int   ,

	@ClientID int   ,

	@Name varchar (150)  ,

	@Description varchar (500)  ,

	@TemplatePath varchar (255)  ,

	@DataWorksheetNumber int   ,

	@DataStartCell varchar (10)  ,

	@TemplateFormat varchar (20)  ,

	@FolderID int   ,

	@CreatedBy int   ,

	@CreatedOn datetime   ,

	@ModifiedBy int   ,

	@ModifiedOn datetime   ,

	@Enabled bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlQueryTemplate]
				SET
					[ClientID] = @ClientID
					,[Name] = @Name
					,[Description] = @Description
					,[TemplatePath] = @TemplatePath
					,[DataWorksheetNumber] = @DataWorksheetNumber
					,[DataStartCell] = @DataStartCell
					,[TemplateFormat] = @TemplateFormat
					,[FolderID] = @FolderID
					,[CreatedBy] = @CreatedBy
					,[CreatedOn] = @CreatedOn
					,[ModifiedBy] = @ModifiedBy
					,[ModifiedOn] = @ModifiedOn
					,[Enabled] = @Enabled
				WHERE
[SqlQueryTemplateID] = @SqlQueryTemplateID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTemplate_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate_Update] TO [sp_executeall]
GO
