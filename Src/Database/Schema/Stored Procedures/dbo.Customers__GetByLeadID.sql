SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Ben Crinion
-- Create date: 2nd April 2008
-- Description:	Get a Customer by one of its Lead records
-- =============================================
CREATE PROCEDURE  [dbo].[Customers__GetByLeadID]
(
	@LeadID int
)
AS
BEGIN

	SELECT c.*
	FROM Customers c WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) on c.CustomerID = l.CustomerID
	WHERE leadid = @LeadID

END







GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetByLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__GetByLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetByLeadID] TO [sp_executeall]
GO
