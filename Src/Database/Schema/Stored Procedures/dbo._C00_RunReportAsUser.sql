SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-12-12
-- Description:	Adapted from _C00_RunReport to include @CurrentUser replacement
-- =============================================
CREATE PROCEDURE [dbo].[_C00_RunReportAsUser]
	 @QueryID			INT
	,@ClientPersonnelID	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @SqlQuery varchar(max),
			@ClientID INT
	
	SELECT @SqlQuery = sq.QueryText, @ClientID = sq.ClientID
	FROM SqlQuery sq WITH (NOLOCK)
	WHERE sq.QueryID = @QueryID

	SELECT @SqlQuery = REPLACE(@SqlQuery, '@ClientID', @ClientID)
	SELECT @SqlQuery = REPLACE(@SqlQuery, '@CurrentUser', @ClientPersonnelID)
	SELECT @SqlQuery = REPLACE(@SqlQuery, '@Today', CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),121) )

	exec _C00_LogIt 'Info', '[_C00_RunReportAsUser]', '@QueryID', @QueryID, @ClientPersonnelID
	EXEC (@SqlQuery)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RunReportAsUser] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_RunReportAsUser] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RunReportAsUser] TO [sp_executeall]
GO
