SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2018-06-04
-- Description:	Save details of a quoted produce for a specific pet and return the ID
-- CPS 2018-08-03 NULL out the Premium Split save
-- GPR 2019-10-10 created for Aquarius603Dev / LPC-36
-- =============================================
CREATE PROCEDURE [dbo].[Quote_SaveQuotePetProduct]
(
	 @QuotePetID				INT
	,@QuoteSessionID			INT
	,@QuoteID					INT
	,@WhoCreated				INT				= NULL
	,@SourceID					INT				= NULL
	,@ChangeNotes				VARCHAR(2000)	= NULL
	,@ProductID					INT				= NULL
	,@RuleSetID					INT				= NULL
	,@RulesEngineOutcome		DECIMAL(18,2)	= NULL
	,@SaveCheckpoints			BIT				= 0
	,@EvaluatedXml				XML				= NULL
	,@ParentQuotePetProductID	INT				= NULL
	,@Excess					DECIMAL(18,2)	= NULL
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE  @QuotePetProductID		INT
			,@WhenCreated			DATETIME = dbo.fn_GetDate_Local()
			,@ProductName			VARCHAR(2000)
			,@AlternativeRuleSetID	INT
			,@ABControllerRuleSetID	INT
			,@ProcName				VARCHAR(2000) = OBJECT_NAME(@@ProcID)
			,@OverridesXml			XML
			,@CaseID				INT
			,@LeadEventID			INT
			,@MatterID				INT

	SELECT	 @OverridesXml	= qp.OverridesXml
			,@CaseID		= m.CaseID
			,@LeadEventID	= qp.LeadEventID
			,@MatterID		= m.MatterID
	FROM QuotePet qp WITH ( NOLOCK ) 
	LEFT JOIN Matter m WITH (NOLOCK) on m.MatterID = qp.MatterID
	WHERE qp.QuotePetID = @QuotePetID

	IF (@ProductID is NULL or @ProductID = 0) AND @MatterID <> 0
	BEGIN
		SELECT @ProductID = dbo.fnGetSimpleDvAsInt(170034,@MatterID) /*Current Policy*/
	END

	--/*Pick up the AB information from the ratings engine tables*/
	--SELECT	 @AlternativeRuleSetID  = rsAlt.RuleSetID
	--		,@ABControllerRuleSetID = rsCtl.RuleSetID
	--FROM RulesEngine_RuleSets rs WITH ( NOLOCK )
	--INNER JOIN RulesEngine_RuleSets rsAlt WITH ( NOLOCK ) on rsAlt.ChangeSetID = rs.ChangeSetID AND rsAlt.Name like 'Pricing Set:%' AND rsAlt.RuleSetID <> @RuleSetID
	--INNER JOIN RulesEngine_RuleSets rsCtl WITH ( NOLOCK ) on rsCtl.ChangeSetID = rs.ChangeSetID AND rsCtl.Name like 'Control Set:%'
	--WHERE rs.RuleSetID = @RuleSetID

	/*GPR 2019-10-10*/
	SELECT	@AlternativeRuleSetID	= NULL,
			@ABControllerRuleSetID	= NULL

	/*Find the product name*/
	SELECT @ProductName = rdv.DetailValue
	FROM ResourceListDetailValues rdv WITH (NOLOCK) 
	WHERE rdv.DetailFieldID = 146200 /*Product*/
	AND rdv.ResourceListID = @ProductID

	/*Create the QuotePetProduct record*/
	INSERT QuotePetProduct ( QuoteSessionID, QuoteID, QuotePetID, WhenCreated, WhoCreated, SourceID, ChangeNotes, ProductID, ProductName, RuleSetID, RulesEngineOutcome, EvaluatedXml, AbControllerRuleSetID, AlternativeRuleSetID, ParentQuotePetProductID,Excess )
	VALUES ( @QuoteSessionID, @QuoteID, @QuotePetID, @WhenCreated, @WhoCreated, @SourceID, @ChangeNotes, @ProductID, @ProductName, @RuleSetID, @RulesEngineOutcome, NULL, @ABControllerRuleSetID, @AlternativeRuleSetID, @ParentQuotePetProductID,@Excess )

	/*Pick up the newly created ID*/
	SELECT @QuotePetProductID = SCOPE_IDENTITY()

	/*GPR 2019-10-10 switch on 'SaveCheckpoints' in the DEV environment for debugging purposes*/
	IF DB_NAME() = 'Aquarius603Dev' /*DEV ONLY*/
	BEGIN
		SET @SaveCheckpoints = 1
	END 

	/*If necessary, save the checkpoints*/
	IF @SaveCheckpoints = 1
	BEGIN
		EXEC Quote_SaveQuotePetProductCheckpoint @QuotePetProductID,@WhoCreated,@SourceID,@ChangeNotes,@EvaluatedXml
	END

	/*Return the ID to the caller*/
	RETURN @QuotePetProductID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_SaveQuotePetProduct] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Quote_SaveQuotePetProduct] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_SaveQuotePetProduct] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_SaveQuotePetProduct] TO [sp_executehelper]
GO
