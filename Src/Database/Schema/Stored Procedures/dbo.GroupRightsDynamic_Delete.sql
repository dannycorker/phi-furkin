SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the GroupRightsDynamic table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupRightsDynamic_Delete]
(

	@GroupRightsDynamicID int   
)
AS


				DELETE FROM [dbo].[GroupRightsDynamic] WITH (ROWLOCK) 
				WHERE
					[GroupRightsDynamicID] = @GroupRightsDynamicID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamic_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupRightsDynamic_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamic_Delete] TO [sp_executeall]
GO
