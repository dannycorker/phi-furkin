SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 18-04-2013
-- Description:	Gets The first SMSQuestion in the given group
-- =============================================
CREATE PROCEDURE [dbo].[SMSQuestion__GetFirstQuestion]

	@SMSQuestionGroupID INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT TOP 1 q.* FROM SMSQuestionGroupMember g WITH (NOLOCK) 
	INNER JOIN dbo.SMSQuestion q WITH (NOLOCK) ON q.SMSQuestionID = g.SMSQuestionID
	WHERE g.SMSQuestionGroupID=@SMSQuestionGroupID ORDER BY QuestionOrder ASC
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion__GetFirstQuestion] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestion__GetFirstQuestion] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion__GetFirstQuestion] TO [sp_executeall]
GO
