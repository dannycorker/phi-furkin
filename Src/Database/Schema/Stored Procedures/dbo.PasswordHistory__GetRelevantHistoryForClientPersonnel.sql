SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-03-21
-- Description:	Gets the relevant password history for a client personnel record
-- =============================================
CREATE PROCEDURE [dbo].[PasswordHistory__GetRelevantHistoryForClientPersonnel] 
	@ClientPersonnelID INT
AS
BEGIN

	DECLARE @PasswordHistoryCount INT
	SELECT @PasswordHistoryCount = PasswordHistoryCount
	FROM dbo.ClientPasswordRule cpr WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cpr.ClientID = cp.ClientID
	WHERE cp.ClientPersonnelID = @ClientPersonnelID
	
	
	;WITH InnerSql AS 
	(
	SELECT Password, Salt, ROW_NUMBER() OVER(ORDER BY DateChanged DESC) as rn 
	FROM dbo.PasswordHistory WITH (NOLOCK)
	WHERE ClientPersonnelID = @ClientPersonnelID 
	)
	SELECT Password, Salt
	FROM InnerSql i 
	WHERE i.rn <= @PasswordHistoryCount

END




GO
GRANT VIEW DEFINITION ON  [dbo].[PasswordHistory__GetRelevantHistoryForClientPersonnel] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PasswordHistory__GetRelevantHistoryForClientPersonnel] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PasswordHistory__GetRelevantHistoryForClientPersonnel] TO [sp_executeall]
GO
