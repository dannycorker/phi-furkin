SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Ian Slack	
-- Create date: 2012-07-18
-- Description:	Aquarium Config Getter
-- =============================================
CREATE PROCEDURE [dbo].[AquariumConfig__Get] 
	@ClientID INT, 
	@SubClientID INT,
	@CategoryID INT,
	@NameID INT,
	@AccessTypeID INT,
	@AccessTypeValueID INT
AS
BEGIN
	
	SELECT
		TOP 1
		ac.ConfigID, 
		ac.ClientID, 
		ac.SubClientID, 
		ac.CategoryID,
		ac.NameID,
		ac.AccessTypeID,
		ac.AccessTypeValueID,
		ac.Encrypted
	FROM	AquariumConfig ac WITH (NOLOCK)
	WHERE	
		(ac.ClientID = 0 OR ac.ClientID = @ClientID)
	AND	(ac.SubClientID = 0 OR ac.SubClientID = @SubClientID)
	AND	(ac.CategoryID = @CategoryID)
	AND	(ac.NameID = @NameID)
	AND	(ac.AccessTypeID = @AccessTypeID)
	AND	(ac.AccessTypeValueID = 0 OR ac.AccessTypeValueID = @AccessTypeValueID)
	ORDER BY ac.ClientID DESC
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumConfig__Get] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumConfig__Get] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumConfig__Get] TO [sp_executeall]
GO
