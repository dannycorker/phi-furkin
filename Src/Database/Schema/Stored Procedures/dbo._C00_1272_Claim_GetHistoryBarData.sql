SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2014-06-27
-- Description:	Gets the additional data for the claim history scarves
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_GetHistoryBarData] 
(
	@MatterID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PolicyLeadID INT
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@MatterID)
	
	DECLARE @LeadID INT
	SELECT @LeadID = LeadID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	-- TODO - We can get this from a stamped down policy ID when we have it but for now we call the function for each one.
	
	SELECT m.MatterID AS ClaimID, dbo.fn_C00_1272_GetPolicyFromClaim(m.MatterID) AS PolicyID
	FROM dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 0) m
	INNER JOIN dbo.Cases ca WITH (NOLOCK) ON m.CaseID = ca.CaseID
	WHERE (ca.ClientStatusID IS NULL OR ca.ClientStatusID NOT IN (3819))
	
	
	DECLARE @PolicyYears TABLE
	(
		FromDate DATE,
		ToDate DATE,
		Affinity VARCHAR(2000),
		Product VARCHAR(2000),
		IsReinstatement BIT,
		PolicyID INT
	)

	;WITH Schemes AS 
	(
		SELECT m.MatterID, mdvScheme.ValueInt AS SchemeID, mdvStart.ValueDate AS StartDate, ISNULL(mdvEnd.ValueDate,'2999-12-31') AS EndDate
		FROM dbo.Matter m WITH (NOLOCK) 
		INNER JOIN dbo.MatterDetailValues mdvScheme WITH (NOLOCK) ON m.MatterID = mdvScheme.MatterID AND mdvScheme.DetailFieldID = 145689
		INNER JOIN dbo.MatterDetailValues mdvStart WITH (NOLOCK) ON m.MatterID = mdvStart.MatterID AND mdvStart.DetailFieldID = 145690
		INNER JOIN dbo.MatterDetailValues mdvEnd WITH (NOLOCK) ON m.MatterID = mdvEnd.MatterID AND mdvEnd.DetailFieldID = 145691
	)
	
	INSERT @PolicyYears (FromDate, ToDate, Affinity, Product, IsReinstatement, PolicyID)
	SELECT	tdvFrom.ValueDate AS FromDate, DATEADD(DAY, -1, tdvTo.ValueDate) AS ToDate, rdvAffinity.DetailValue AS Affinity, rdvProduct.DetailValue AS Product,
			CASE WHEN rdvType.ValueInt = 42933 THEN 1 ELSE 0 END AS IsReinstatement, s.MatterID AS PolicyID
	FROM dbo.TableRows r WITH (NOLOCK) 	
	INNER JOIN dbo.TableDetailValues tdvFrom WITH (NOLOCK) ON r.TableRowID = tdvFrom.TableRowID AND tdvFrom.DetailFieldID = 145663
	INNER JOIN dbo.TableDetailValues tdvTo WITH (NOLOCK) ON r.TableRowID = tdvTo.TableRowID AND tdvTo.DetailFieldID = 145664
	INNER JOIN dbo.TableDetailValues tdvPolicy WITH (NOLOCK) ON r.TableRowID = tdvPolicy.TableRowID AND tdvPolicy.DetailFieldID = 145665
	INNER JOIN dbo.ResourceListDetailValues rdvAffinity WITH (NOLOCK) ON tdvPolicy.ResourceListID = rdvAffinity.ResourceListID AND rdvAffinity.DetailFieldID = 144314
	INNER JOIN dbo.ResourceListDetailValues rdvProduct WITH (NOLOCK) ON tdvPolicy.ResourceListID = rdvProduct.ResourceListID AND rdvProduct.DetailFieldID = 146200
	INNER JOIN dbo.ResourceListDetailValues rdvType WITH (NOLOCK) ON tdvPolicy.ResourceListID = rdvType.ResourceListID AND rdvType.DetailFieldID = 144319
	INNER JOIN Schemes s ON tdvPolicy.ResourceListID = s.SchemeID
	WHERE r.LeadID = @LeadID
	AND tdvFrom.ValueDate BETWEEN s.StartDate AND s.EndDate
	ORDER BY tdvFrom.ValueDate
	
	
	SELECT *
	FROM @PolicyYears
	
	DECLARE @Species INT
	SELECT @Species = rdvSpecies.ValueInt
	FROM dbo.LeadDetailValues ldvPet WITH (NOLOCK) 
	INNER JOIN dbo.ResourceListDetailValues rdvSpecies WITH (NOLOCK) ON ldvPet.ValueInt = rdvSpecies.ResourceListID AND rdvSpecies.DetailFieldID = 144269
	WHERE ldvPet.LeadID = @PolicyLeadID
	AND ldvPet.DetailFieldID = 144272
	
	;WITH Limits AS 
	(
		SELECT	p.PolicyID, p.FromDate, s.Out_ResourceListID AS ResourceListID, llSection.ItemValue AS Section, llSubSection.ItemValue AS SubSection, tdvSumInsured.ValueMoney AS SumInsured,
				ROW_NUMBER() OVER(PARTITION BY p.PolicyID, p.FromDate, s.Out_ResourceListID ORDER BY tdvSpecies.ValueInt DESC) as rn 
		FROM @PolicyYears p
		CROSS APPLY dbo.fn_C00_1272_GetPolicySectionRelationships(p.PolicyID) s
		INNER JOIN dbo.TableDetailValues tdvRLID WITH (NOLOCK) ON tdvRLID.ResourceListID = s.ResourceListID AND tdvRLID.DetailFieldID = 144350
		INNER JOIN dbo.ResourceListDetailValues rdvSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189
		INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rdvSection.ValueInt = llSection.LookupListItemID 
		INNER JOIN dbo.ResourceListDetailValues rdvSubSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSubSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190
		INNER JOIN dbo.LookupListItems llSubSection WITH (NOLOCK) ON rdvSubSection.ValueInt = llSubSection.LookupListItemID 
		INNER JOIN dbo.TableDetailValues tdvRL WITH (NOLOCK) ON s.Out_ResourceListID = tdvRL.ResourceListID AND tdvRL.DetailFieldID = 144357 AND p.PolicyID = tdvRL.MatterID
		INNER JOIN dbo.TableDetailValues tdvSumInsured WITH (NOLOCK) ON tdvRL.TableRowID = tdvSumInsured.TableRowID AND tdvSumInsured.DetailFieldID = 144358
		INNER JOIN dbo.TableRows r WITH (NOLOCK) ON r.TableRowID = tdvRLID.TableRowID
		INNER JOIN dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 1) m ON r.MatterID = m.MatterID
		LEFT JOIN dbo.TableDetailValues tdvSpecies WITH (NOLOCK) ON tdvRL.TableRowID = tdvSpecies.TableRowID AND tdvSpecies.DetailFieldID = 170013 
																	AND (tdvSpecies.ValueInt = @Species OR tdvSpecies.ValueInt = 0 OR tdvSpecies.ValueInt IS NULL)
	)
	
	SELECT * 
	FROM Limits
	WHERE rn = 1

	
	
	 
	

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetHistoryBarData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_GetHistoryBarData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetHistoryBarData] TO [sp_executeall]
GO
