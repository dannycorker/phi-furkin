SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[getNewCustomersWithAnswersAndOutcomes] 

@ClientQuestionnaireID int,
@FromDate datetime,
@ToDate dateTime

AS

/*2012-11-14 Nolocked By CS*/

SELECT     dbo.Customers.CustomerID, dbo.Customers.ClientID, dbo.Customers.TitleID, dbo.Customers.FirstName, dbo.Customers.MiddleName, 
dbo.Customers.LastName, dbo.Customers.EmailAddress, 
dbo.Customers.DayTimeTelephoneNumber, dbo.Customers.DayTimeTelephoneNumberVerifiedAndValid, 
dbo.Customers.HomeTelephone, dbo.Customers.HomeTelephoneVerifiedAndValid, 
dbo.Customers.MobileTelephone, dbo.Customers.MobileTelephoneVerifiedAndValid, 
dbo.Customers.Address1, dbo.Customers.Address2, dbo.Customers.Town, dbo.Customers.County, 
dbo.Customers.PostCode, dbo.Customers.HasDownloaded, dbo.Customers.DownloadedOn, dbo.Customers.Test, dbo.Customers.CompanyName, 
dbo.Customers.CompanyTelephone, dbo.Customers.CompanyTelephoneVerifiedAndValid, 
dbo.Customers.WorksTelephone, dbo.Customers.WorksTelephoneVerifiedAndValid, 
dbo.Customers.Occupation, dbo.Customers.Employer, dbo.Customers.DoNotEmail, dbo.Customers.DoNotSellToThirdParty, dbo.Customers.AgreedToTermsAndConditions,dbo.Customers.PhoneNumbersVerifiedOn, 
dbo.Customers.DateOfBirth, dbo.Customers.IsBusiness, dbo.Customers.DefaultContactID,
dbo.CustomerQuestionnaires.CustomerQuestionnaireID, dbo.CustomerQuestionnaires.ClientQuestionnaireID, dbo.CustomerQuestionnaires.SubmissionDate
FROM          dbo.Customers WITH (NOLOCK)  INNER JOIN
                      dbo.CustomerQuestionnaires WITH (NOLOCK)  ON dbo.Customers.CustomerID = dbo.CustomerQuestionnaires.CustomerID
WHERE     (dbo.CustomerQuestionnaires.ClientQuestionnaireID = @ClientQuestionnaireID) AND (dbo.CustomerQuestionnaires.SubmissionDate >= @FromDate)  AND (dbo.CustomerQuestionnaires.SubmissionDate <= @ToDate)





GO
GRANT VIEW DEFINITION ON  [dbo].[getNewCustomersWithAnswersAndOutcomes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[getNewCustomersWithAnswersAndOutcomes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[getNewCustomersWithAnswersAndOutcomes] TO [sp_executeall]
GO
