SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ClientPersonnelOptions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelOptions_Get_List]

AS


				
				SELECT
					[ClientPersonnelOptionID],
					[ClientPersonnelID],
					[ClientPersonnelOptionTypeID],
					[OptionValue],
					[ClientID]
				FROM
					[dbo].[ClientPersonnelOptions] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptions_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelOptions_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptions_Get_List] TO [sp_executeall]
GO
