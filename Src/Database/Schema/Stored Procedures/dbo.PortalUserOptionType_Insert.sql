SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PortalUserOptionType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserOptionType_Insert]
(

	@PortalUserOptionTypeID int    OUTPUT,

	@OptionTypeName nchar (100)  
)
AS


				
				INSERT INTO [dbo].[PortalUserOptionType]
					(
					[OptionTypeName]
					)
				VALUES
					(
					@OptionTypeName
					)
				-- Get the identity value
				SET @PortalUserOptionTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOptionType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserOptionType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOptionType_Insert] TO [sp_executeall]
GO
