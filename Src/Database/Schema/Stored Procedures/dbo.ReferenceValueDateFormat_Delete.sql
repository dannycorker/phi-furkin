SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ReferenceValueDateFormat table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueDateFormat_Delete]
(

	@ReferenceValueDateFormatID int   
)
AS


				DELETE FROM [dbo].[ReferenceValueDateFormat] WITH (ROWLOCK) 
				WHERE
					[ReferenceValueDateFormatID] = @ReferenceValueDateFormatID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDateFormat_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueDateFormat_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDateFormat_Delete] TO [sp_executeall]
GO
