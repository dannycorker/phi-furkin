SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DataLoaderFileFormat table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileFormat_Delete]
(

	@DataLoaderFileFormatID int   
)
AS


				DELETE FROM [dbo].[DataLoaderFileFormat] WITH (ROWLOCK) 
				WHERE
					[DataLoaderFileFormatID] = @DataLoaderFileFormatID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileFormat_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileFormat_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileFormat_Delete] TO [sp_executeall]
GO
