SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[DeleteFrameSourceForPage] @QuestionnaireFrameSourceID int

AS

Delete From QuestionnaireFrameSources
Where QuestionnaireFrameSourceID = @QuestionnaireFrameSourceID



GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteFrameSourceForPage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteFrameSourceForPage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteFrameSourceForPage] TO [sp_executeall]
GO
