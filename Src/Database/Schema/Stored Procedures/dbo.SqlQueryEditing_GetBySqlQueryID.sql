SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditing table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditing_GetBySqlQueryID]
(

	@SqlQueryID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SqlQueryEditingID],
					[ClientID],
					[SqlQueryID],
					[WhoIsEditing],
					[EditStartedAt],
					[QueryText],
					[QueryTitle],
					[LeadTypeID],
					[FolderID],
					[ParentQueryID],
					[LockAllTables]
				FROM
					[dbo].[SqlQueryEditing] WITH (NOLOCK) 
				WHERE
					[SqlQueryID] = @SqlQueryID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditing_GetBySqlQueryID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditing_GetBySqlQueryID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditing_GetBySqlQueryID] TO [sp_executeall]
GO
