SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-10-15
-- Description:	Quick check of the most shameful timing entries in reverse order
-- =============================================
CREATE PROCEDURE [dbo].[TimingCheck]
	@ThisManyMilliseconds int = 1000
AS
BEGIN
	SET NOCOUNT ON;

	select max(timingms) / 1000 as [RunTime], count(*) as [RunCount], et.EventTypeID, et.EventTypeName, et.ClientID
	from timing 
	inner join EventType et on et.EventTypeID = id2
	where timingms > @ThisManyMilliseconds 
	group by et.EventTypeID, et.EventTypeName, et.ClientID 
	order by 1 desc, 2 desc, 3 ascEND



GO
GRANT VIEW DEFINITION ON  [dbo].[TimingCheck] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TimingCheck] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TimingCheck] TO [sp_executeall]
GO
