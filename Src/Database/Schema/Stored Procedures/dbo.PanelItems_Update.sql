SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PanelItems table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItems_Update]
(

	@PanelItemID int   ,

	@ClientID int   ,

	@PanelItemName varchar (50)  ,

	@PanelID int   ,

	@PanelItemTypeID int   ,

	@CreatedBy int   ,

	@QueryID int   ,

	@IsGlobal bit   ,

	@XAxisColumn varchar (50)  ,

	@Orientation varchar (50)  ,

	@Margin int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PanelItems]
				SET
					[ClientID] = @ClientID
					,[PanelItemName] = @PanelItemName
					,[PanelID] = @PanelID
					,[PanelItemTypeID] = @PanelItemTypeID
					,[CreatedBy] = @CreatedBy
					,[QueryID] = @QueryID
					,[IsGlobal] = @IsGlobal
					,[XAxisColumn] = @XAxisColumn
					,[Orientation] = @Orientation
					,[Margin] = @Margin
				WHERE
[PanelItemID] = @PanelItemID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItems_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItems_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItems_Update] TO [sp_executeall]
GO
