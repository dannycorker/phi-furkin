SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DataLoaderObjectAction table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectAction_Insert]
(

	@DataLoaderObjectActionID int   ,

	@DataLoaderObjectActionName varchar (250)  ,

	@DataLoaderObjectActionDescription varchar (2000)  
)
AS


				
				INSERT INTO [dbo].[DataLoaderObjectAction]
					(
					[DataLoaderObjectActionID]
					,[DataLoaderObjectActionName]
					,[DataLoaderObjectActionDescription]
					)
				VALUES
					(
					@DataLoaderObjectActionID
					,@DataLoaderObjectActionName
					,@DataLoaderObjectActionDescription
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectAction_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectAction_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectAction_Insert] TO [sp_executeall]
GO
