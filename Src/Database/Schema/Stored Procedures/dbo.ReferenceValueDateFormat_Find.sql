SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ReferenceValueDateFormat table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueDateFormat_Find]
(

	@SearchUsingOR bit   = null ,

	@ReferenceValueDateFormatID int   = null ,

	@DateFormat varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ReferenceValueDateFormatID]
	, [DateFormat]
    FROM
	[dbo].[ReferenceValueDateFormat] WITH (NOLOCK) 
    WHERE 
	 ([ReferenceValueDateFormatID] = @ReferenceValueDateFormatID OR @ReferenceValueDateFormatID IS NULL)
	AND ([DateFormat] = @DateFormat OR @DateFormat IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ReferenceValueDateFormatID]
	, [DateFormat]
    FROM
	[dbo].[ReferenceValueDateFormat] WITH (NOLOCK) 
    WHERE 
	 ([ReferenceValueDateFormatID] = @ReferenceValueDateFormatID AND @ReferenceValueDateFormatID is not null)
	OR ([DateFormat] = @DateFormat AND @DateFormat is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDateFormat_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueDateFormat_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDateFormat_Find] TO [sp_executeall]
GO
