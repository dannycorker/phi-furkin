SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2020-08-09
-- Description:	CPS for JIRA PPET-154 | Return pet details for a policy
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GetPetDataByPolicy]
	@MatterID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ClientID INT = [dbo].[fnGetPrimaryClientID]()

	SELECT	 m.MatterID
			,mdvEffect.ValueDate				AS [PolicyEffectiveDate]
			,mdvExpire.ValueDate				AS [PolicyExpirationDate]
			,lliStatus.ItemValue				AS [PolicyStatus] -- In force or active, cancelled, etc… 
			,lliMethod.ItemValue				AS [PaymentPlan] -- monthly or annual 
			,ldvBirthDat.ValueDate				AS [PetDateOfBirth]
			,rdvPetBreed.DetailValue			AS [PetBreed]
			,lliPetSex.ItemValue				AS [PetSex]
	FROM Matter m WITH (NOLOCK)
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID
	LEFT JOIN MatterDetailValues mdvStatus WITH (NOLOCK) on mdvStatus.MatterID = m.MatterID AND mdvStatus.DetailFieldID = 170038 /*Policy Status*/
	LEFT JOIN LookupListItems 	 lliStatus WITH (NOLOCK) on lliStatus.LookupListItemID = mdvStatus.ValueInt
	LEFT JOIN MatterDetailValues mdvEffect WITH (NOLOCK) on mdvEffect.MatterID = m.MatterID AND mdvEffect.DetailFieldID = 170035 /*Policy Inception Date*/
	LEFT JOIN MatterDetailValues mdvExpire WITH (NOLOCK) on mdvExpire.MatterID = m.MatterID AND mdvExpire.DetailFieldID = 170037 /*Policy End Date*/
	LEFT JOIN MatterDetailValues mdvMethod WITH (NOLOCK) on mdvMethod.MatterID = m.MatterID AND mdvMethod.DetailFieldID = 170114 /*Payment Method*/
	LEFT JOIN LookupListItems 	 lliMethod WITH (NOLOCK) on lliMethod.LookupListItemID = mdvMethod.ValueInt
	LEFT JOIN LeadDetailValues ldvBirthDat WITH (NOLOCK) on ldvBirthDat.LeadID = m.LeadID AND ldvBirthDat.DetailFieldID = 144274 /*Pet Date of Birth*/
	LEFT JOIN LeadDetailValues ldvPetBreed WITH (NOLOCK) on ldvPetBreed.LeadID = m.LeadID AND ldvPetBreed.DetailFieldID = 144272 /*Pet Type*/
	LEFT JOIN ResourceListDetailValues rdvPetBreed WITH (NOLOCK) on rdvPetBreed.ResourceListID = ldvPetBreed.ValueInt AND rdvPetBreed.DetailFieldID = 144270 /*Pet Breed*/
	LEFT JOIN LeadDetailValues   ldvPetSex WITH (NOLOCK) on ldvPetSex.LeadID = l.LeadID AND ldvPetSex.DetailFieldID = 144275 /*Pet Sex*/
	LEFT JOIN LookupListItems    lliPetSex WITH (NOLOCK) on lliPetSex.LookupListItemID = ldvPetSex.ValueInt
	WHERE m.MatterID = @MatterID
	AND l.LeadTypeID = 1492 /*Policy Admin*/
	AND m.ClientID = @ClientID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetPetDataByPolicy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_GetPetDataByPolicy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetPetDataByPolicy] TO [sp_executeall]
GO
