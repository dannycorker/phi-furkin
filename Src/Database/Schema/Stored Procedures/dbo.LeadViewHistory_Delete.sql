SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadViewHistory table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadViewHistory_Delete]
(

	@LeadViewHistoryID int   
)
AS


				DELETE FROM [dbo].[LeadViewHistory] WITH (ROWLOCK) 
				WHERE
					[LeadViewHistoryID] = @LeadViewHistoryID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadViewHistory_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_Delete] TO [sp_executeall]
GO
