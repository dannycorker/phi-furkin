SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2010-10-26
-- Description:	SQL After Dataload for all clients
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SAD]
	@DataLoaderFileID int,
	@ClientPersonnelID int,
	@ClientID int
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SubClientID int,
			@DataLoaderMapID INT
	
	SELECT @SubClientID = cp.SubClientID 
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.ClientPersonnelID = @ClientPersonnelID 
	
	/*2015-02-10 ACE Added Automated task after dataload*/
	SELECT @DataLoaderMapID = dlf.DataLoaderMapID
	FROM DataLoaderFile dlf WITH (NOLOCK)
	WHERE dlf.DataLoaderFileID = @DataLoaderFileID
	
	IF EXISTS (
		SELECT *
		FROM DataLoaderAutomatedTask dla WITH (NOLOCK)
		WHERE dla.DataloaderMapID = @DataLoaderMapID
	)
	BEGIN

		UPDATE at
		SET NextRunDateTime = DATEADD(SS,ISNULL(dla.DelaySeconds,0),dbo.fn_GetDate_Local())
		FROM AutomatedTask at
		INNER JOIN DataLoaderAutomatedTask dla WITH (NOLOCK) ON dla.AutomatedTaskID = at.TaskID
		WHERE dla.DataloaderMapID = @DataLoaderMapID
	
	END

	--EXEC dbo._C600_SAD @DataLoaderFileID, @ClientPersonnelID, @ClientID, @SubClientID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SAD] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SAD] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SAD] TO [sp_executeall]
GO
