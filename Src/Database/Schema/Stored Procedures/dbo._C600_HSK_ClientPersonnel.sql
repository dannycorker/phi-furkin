SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-08-21
-- Description:	Sync User Accounts between Live, Dev, and QA
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_HSK_ClientPersonnel]
AS
BEGIN

	/*Populate a master user list*/
	DECLARE @AllUsers TABLE ( ClientPersonnelID INT, EmailAddress VARCHAR(2000), Live BIT, QA BIT, Dev BIT )
	INSERT @AllUsers ( ClientPersonnelID, EmailAddress, Live, Qa, Dev )
	SELECT fn.ClientPersonnelID, fn.EmailAddress, fn.Live, fn.Qa, fn.Dev
	FROM dbo.fn_C600_GetUserListWithDatabases() fn

	DECLARE  @RowCount		INT
			,@Section		VARCHAR(2000)
			,@ErrorMessage	VARCHAR(2000)
			,@XmlLog		XML
			,@ClientID		INT = dbo.fnGetPrimaryClientID()
			,@LogXmlID		INT
			,@UpdatedCPIDs	dbo.tvpInt
			,@ContextInfo	VARBINARY(MAX)
			
	SELECT	 @ContextInfo = CAST( 'ClientPersonnel' as VARBINARY(MAX) )
	SET CONTEXT_INFO @ContextInfo /*Set Context Info so that we can insert ClientPersonnel records*/

	DECLARE @OutputLog TABLE ( Section VARCHAR(2000), RowsAffected INT, ErrorMessage VARCHAR(2000) )

	--/*---------------------*/
	--/* Update QA from Live */
	--/*---------------------*/
	--SELECT @Section = 'Qa from Live', @RowCount = 0, @ErrorMessage= NULL
		
	--BEGIN TRY
	--	IF	EXISTS ( SELECT * FROM sys.servers sd WITH ( NOLOCK ) WHERE sd.name = '878574-SQLCLUS1\CPSQL1' )
	--	AND EXISTS ( SELECT * FROM sys.databases sd WITH ( NOLOCK ) WHERE sd.name = 'Aquarius600' )
	--	BEGIN
	--		SET IDENTITY_INSERT Aquarius600.dbo.ClientPersonnel ON
			
	--		--INSERT Aquarius600.dbo.ClientPersonnel ( ClientPersonnelID, ClientID, ClientOfficeID, TitleID, FirstName, MiddleName, LastName, JobTitle, Password, ClientPersonnelAdminGroupID, MobileTelephone, HomeTelephone, OfficeTelephone, OfficeTelephoneExtension, EmailAddress, ChargeOutRate, Salt, AttemptedLogins, AccountDisabled, ManagerID, LanguageID, SubClientID, ForcePasswordChangeOn, ThirdPartySystemId, CustomerID, IsAquarium, AllowSmsCommandProcessing, MemorableWord, MemorableWordSalt, MemorableWordAttempts, PendingActivation )
	--		--	OUTPUT inserted.ClientPersonnelID
	--		--	INTO @UpdatedCPIDs (AnyID)
	--		--SELECT ClientPersonnelID, ClientID, ClientOfficeID, TitleID, FirstName, MiddleName, LastName, JobTitle, Password, ClientPersonnelAdminGroupID, MobileTelephone, HomeTelephone, OfficeTelephone, OfficeTelephoneExtension, EmailAddress, ChargeOutRate, Salt, AttemptedLogins, AccountDisabled, ManagerID, LanguageID, SubClientID, ForcePasswordChangeOn, ThirdPartySystemId, CustomerID, IsAquarium, AllowSmsCommandProcessing, MemorableWord, MemorableWordSalt, MemorableWordAttempts, PendingActivation
	--		--FROM [878574-SQLCLUS1\CPSQL1].Aquarius600.dbo.ClientPersonnel cp WITH ( NOLOCK )
	--		--WHERE cp.ClientPersonnelID IN ( SELECT au.ClientPersonnelID
	--		--								FROM @AllUsers au 
	--		--								WHERE au.Live = 1
	--		--								AND au.QA = 0 )
	--		--and not exists ( SELECT * 
	--		                 --FROM Aquarius600.dbo.ClientPersonnel qa WITH ( NOLOCK ) 
	--		                 --WHERE qa.EmailAddress = cp.EmailAddress )

	--		SELECT @RowCount = @@ROWCOUNT
			
	--		SET IDENTITY_INSERT Aquarius600.dbo.ClientPersonnel OFF
			
	--		/*Mark up the working table*/
	--		UPDATE au
	--		SET QA = 1
	--		FROM @AllUsers au 
	--		INNER JOIN @UpdatedCPIDs id on id.AnyID = au.ClientPersonnelID
			
	--		DELETE @UpdatedCPIDs
	--	END
	--END TRY
	--BEGIN CATCH
	--	SELECT @ErrorMessage = ERROR_MESSAGE()
	--	PRINT @ErrorMessage
	--	SET IDENTITY_INSERT Aquarius600.dbo.ClientPersonnel OFF
	--END CATCH
	
	--/*Keep track of progress*/
	--INSERT @OutputLog ( Section, RowsAffected, ErrorMessage )
	--VALUES ( @Section, @RowCount, @ErrorMessage )
	
	/*---------------------*/
	/* Update Dev from QA  */
	/*---------------------*/
	SELECT @Section = 'Dev from Qa', @RowCount = 0, @ErrorMessage= NULL

	BEGIN TRY	
		IF	EXISTS ( SELECT * FROM sys.databases sd WITH ( NOLOCK ) WHERE sd.name = 'Aquarius603Dev' )
		AND EXISTS ( SELECT * FROM sys.databases sd WITH ( NOLOCK ) WHERE sd.name = 'Aquarius603Qa' )
		BEGIN
			SET IDENTITY_INSERT Aquarius603Dev.dbo.ClientPersonnel ON
			
			INSERT Aquarius603Dev.dbo.ClientPersonnel ( ClientPersonnelID, ClientID, ClientOfficeID, TitleID, FirstName, MiddleName, LastName, JobTitle, Password, ClientPersonnelAdminGroupID, MobileTelephone, HomeTelephone, OfficeTelephone, OfficeTelephoneExtension, EmailAddress, ChargeOutRate, Salt, AttemptedLogins, AccountDisabled, ManagerID, LanguageID, SubClientID, ForcePasswordChangeOn, ThirdPartySystemId, CustomerID, IsAquarium, AllowSmsCommandProcessing, MemorableWord, MemorableWordSalt, MemorableWordAttempts, PendingActivation )
				OUTPUT inserted.ClientPersonnelID
				INTO @UpdatedCPIDs (AnyID)
			SELECT ClientPersonnelID, ClientID, ClientOfficeID, TitleID, FirstName, MiddleName, LastName, JobTitle, Password, ClientPersonnelAdminGroupID, MobileTelephone, HomeTelephone, OfficeTelephone, OfficeTelephoneExtension, EmailAddress, ChargeOutRate, Salt, AttemptedLogins, 1-IsAquarium /*CPS 2017-09-29 only allow AQ users to remain enabled*/, ManagerID, LanguageID, SubClientID, ForcePasswordChangeOn, ThirdPartySystemId, CustomerID, IsAquarium, AllowSmsCommandProcessing, MemorableWord, MemorableWordSalt, MemorableWordAttempts, PendingActivation
			FROM Aquarius603Qa.dbo.ClientPersonnel cp WITH ( NOLOCK )
			WHERE cp.ClientPersonnelID IN ( SELECT au.ClientPersonnelID
											FROM @AllUsers au 
											WHERE au.QA = 1
											AND au.Dev = 0 )

			SELECT @RowCount = @@ROWCOUNT
			
			SET IDENTITY_INSERT Aquarius603Dev.dbo.ClientPersonnel OFF
			
			/*Mark up the working table*/
			UPDATE au
			SET Dev = 1
			FROM @AllUsers au 
			INNER JOIN @UpdatedCPIDs id on id.AnyID = au.ClientPersonnelID
			
			DELETE @UpdatedCPIDs
		END
	END TRY
	BEGIN CATCH
		SELECT @ErrorMessage = ERROR_MESSAGE()
		PRINT @ErrorMessage
		SET IDENTITY_INSERT Aquarius603Dev.dbo.ClientPersonnel OFF
	END CATCH

	/*Keep track of progress*/
	INSERT @OutputLog ( Section, RowsAffected, ErrorMessage )
	VALUES ( @Section, @RowCount, @ErrorMessage )
	
	/*---------------------*/
	/*Update Live from Dev */
	/*---------------------*/
	--SELECT @Section = 'Live from Dev', @RowCount = 0, @ErrorMessage= NULL
		
	--BEGIN TRY
	--	IF	EXISTS ( SELECT * FROM sys.databases sd WITH ( NOLOCK ) WHERE sd.name = 'Aquarius600' )
	--	AND DB_NAME() = 'Aquarius600'
	--	BEGIN
	--		SET IDENTITY_INSERT Aquarius600.dbo.ClientPersonnel ON

	--		--INSERT Aquarius600.dbo.ClientPersonnel ( ClientPersonnelID, ClientID, ClientOfficeID, TitleID, FirstName, MiddleName, LastName, JobTitle, Password, ClientPersonnelAdminGroupID, MobileTelephone, HomeTelephone, OfficeTelephone, OfficeTelephoneExtension, EmailAddress, ChargeOutRate, Salt, AttemptedLogins, AccountDisabled, ManagerID, LanguageID, SubClientID, ForcePasswordChangeOn, ThirdPartySystemId, CustomerID, IsAquarium, AllowSmsCommandProcessing, MemorableWord, MemorableWordSalt, MemorableWordAttempts, PendingActivation )
	--		--	OUTPUT inserted.ClientPersonnelID
	--		--	INTO @UpdatedCPIDs (AnyID)
	--		--SELECT ClientPersonnelID, ClientID, ClientOfficeID, TitleID, FirstName, MiddleName, LastName, JobTitle, Password, ClientPersonnelAdminGroupID, MobileTelephone, HomeTelephone, OfficeTelephone, OfficeTelephoneExtension, EmailAddress, ChargeOutRate, Salt, AttemptedLogins, AccountDisabled, ManagerID, LanguageID, SubClientID, ForcePasswordChangeOn, ThirdPartySystemId, CustomerID, IsAquarium, AllowSmsCommandProcessing, MemorableWord, MemorableWordSalt, MemorableWordAttempts, PendingActivation
	--		--FROM [892950-CPTSDB1].Aquarius600.dbo.ClientPersonnel cp WITH ( NOLOCK )
	--		--WHERE cp.ClientPersonnelID IN ( SELECT au.ClientPersonnelID
	--		--								FROM @AllUsers au 
	--		--								WHERE au.Dev = 1
	--		--								AND au.Live = 0 )
			
	--		SELECT @RowCount = @@ROWCOUNT
											
	--		SET IDENTITY_INSERT Aquarius600.dbo.ClientPersonnel OFF
			
	--		/*Mark up the working table*/
	--		UPDATE au
	--		SET Live = 1
	--		FROM @AllUsers au 
	--		INNER JOIN @UpdatedCPIDs id on id.AnyID = au.ClientPersonnelID
			
	--		DELETE @UpdatedCPIDs
	--	END
	--END TRY
	--BEGIN CATCH
	--	SELECT @ErrorMessage = ERROR_MESSAGE()
	--	PRINT @ErrorMessage
	--	SET IDENTITY_INSERT Aquarius600.dbo.ClientPersonnel OFF
	--END CATCH

	--/*Keep track of progress*/
	--INSERT @OutputLog ( Section, RowsAffected, ErrorMessage )
	--VALUES ( @Section, @RowCount, @ErrorMessage )


	/*------------*/
	/* Log output */
	/*------------*/
	SELECT @XmlLog =
	(
	SELECT * 
	FROM @OutputLog ol
	FOR XML AUTO
	)
	
	SELECT @RowCount =  CASE 
						WHEN EXISTS ( SELECT * FROM @OutputLog ol  WHERE ol.ErrorMessage <> '' )
						THEN -1
						ELSE  1
						END
	
	INSERT LogXML ( ClientID, LogDateTime, ContextID, ContextVarchar, LogXMLEntry )
	SELECT @ClientID, CURRENT_TIMESTAMP, @RowCount, '_C600_HSK_ClientPersonnel', @XmlLog
	
	SELECT @LogXmlID = SCOPE_IDENTITY()
	
	RETURN @LogXmlID
	
END











GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_HSK_ClientPersonnel] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_HSK_ClientPersonnel] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_HSK_ClientPersonnel] TO [sp_executeall]
GO
