SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 18-06-2009
-- Description:	Get Table Rows and Pivot
-- 2010-11-17 JWG Remove all clauses that we can do without now. Use TableDetailFieldPageID instead of DetailFieldID IN (lists). Only pivot output IF required.
-- 2010-12-12 ACE Added CaseID and CustomerID where clauses
-- 2011-08-01 SB Added field to return the look up list name so filtering works correctly
-- 2011-08-08 PR Moved the Basic field Columns to after the resource list fields.
--			  Fields have to be returned in field order with the resource fields before the basic fields
--            Any other order will break the document parser.
-- 2011-10-28 ACE Replaced ", tr.DenyEdit, tr.DenyDelete" with ", tr.DenyEdit as [DenyDelete], tr.DenyDelete as [DenyEdit]"
--			  Temporaraly as the app was showing the buttons the wrong way around
-- 2013-11-04 ACE Updated SP to use config table for order by's and replacements of entire SQL
-- =============================================
CREATE PROCEDURE [dbo].[TableRows__GetByDetailFieldIDLeadIDMatterID_New] 
	@DetailFieldID INT, 
	@LeadID INT = 0, 
	@MatterID INT = 0,
	@MatterIDList VARCHAR(MAX) = NULL,
	@PrintOnly BIT = 0,
	@PivotOutput BIT = 1,
	@DetailFieldSubtypeID INT = NULL,
	@CustomerID INT = NULL,
	@CaseID INT = NULL,
	@ClientID INT = NULL,
	@ClientPersonnelID INT = NULL,
	@ContactID INT = NULL
AS
BEGIN
	SET NOCOUNT ON;

--declare
--	@DetailFieldID INT = 152991, 
--	@LeadID INT = 0, 
--	@MatterID INT = NULL,
--	@MatterIDList VARCHAR(MAX) = NULL,
--	@PrintOnly BIT = 1,
--	@PivotOutput BIT = 1,
--	@DetailFieldSubtypeID INT = NULL,
--	@CustomerID INT = 3032209,
--	@CaseID INT = NULL,
--	@ClientID INT = 223,
--	@ClientPersonnelID INT = NULL,
--	@ContactID INT = NULL

	DECLARE @cols VARCHAR(MAX),
			@fieldIdents VARCHAR(MAX),
			@rlfieldIdents VARCHAR(MAX),
			@query VARCHAR(MAX) = '',
			@rlQuery VARCHAR(MAX) = '',
			@llQuery VARCHAR(MAX) = '',
			@rlllQuery VARCHAR(MAX) = '',
			@TableDetailFieldPageID INT,
			@basicQuery VARCHAR(MAX),
			@ResourceTable INT,
			@ResourceListDetailFieldPageID INT,
			@ResourceListDetailFieldID INT,
			@pvtStatement VARCHAR(MAX) = '',
			@queryheader VARCHAR(MAX) = '',
			@LeadOrMatterClause VARCHAR(MAX),
			@LookupListColumns INT,
			@DFPString varchar(100),
			@DFString varchar(100)

	IF @MatterID > 0
	BEGIN
		SET @LeadOrMatterClause = 'MatterID = ' + CAST(@MatterID AS VARCHAR)
	END
	ELSE
	IF @CaseID > 0
	BEGIN
		SET @LeadOrMatterClause = 'CaseID = ' + CAST(@CaseID AS VARCHAR)
	END
	ELSE
	IF @LeadID > 0
	BEGIN
		SET @LeadOrMatterClause = 'LeadID = ' + CAST(@LeadID AS VARCHAR)
	END
	ELSE
	IF @CustomerID > 0
	BEGIN
		SET @LeadOrMatterClause = 'CustomerID = ' + CAST(@CustomerID AS VARCHAR)
	END
	ELSE
	IF @ClientPersonnelID > 0
	BEGIN
		SET @LeadOrMatterClause = 'ClientPersonnelID = ' + CAST(@ClientPersonnelID AS VARCHAR)
	END
	ELSE
	IF @ContactID > 0
	BEGIN
		SET @LeadOrMatterClause = 'ContactID = ' + CAST(@ContactID AS VARCHAR)
	END
	ELSE
	IF @MatterIDList IS NOT NULL
	BEGIN
		SET @LeadOrMatterClause = 'MatterID IN (' + @MatterIDList + ') '	
	END
	ELSE
	IF @ClientID > 0
	BEGIN
		SET @LeadOrMatterClause = 'ClientID = ' + CAST(@ClientID AS VARCHAR)
	END

	SELECT @TableDetailFieldPageID = df.TableDetailFieldPageID
	FROM dbo.DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @DetailFieldID

	IF @DetailFieldID = 166164
	BEGIN

		/*Quadra Customer table overview for the matter payment table (yes one and only) for that customer*/
		SELECT TOP 1 @LeadOrMatterClause = 'MatterID = ' + CAST(m.MatterID AS VARCHAR), @TableDetailFieldPageID = 12821, @DetailFieldID = 115616
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID and l.LeadTypeID = 887
		WHERE m.CustomerID = @CustomerID
		ORDER BY m.MatterID DESC

	END
	
	IF EXISTS(SELECT * FROM dbo.DetailFields df WITH (NOLOCK) WHERE df.DetailFieldPageID = @TableDetailFieldPageID AND df.QuestionTypeID = 14) 
	BEGIN
		SET @ResourceTable = 1
	END
	
	IF EXISTS(SELECT * FROM dbo.DetailFields df WITH (NOLOCK) WHERE df.DetailFieldPageID = @TableDetailFieldPageID AND df.QuestionTypeID IN (2, 4)) 
	BEGIN
		SET @LookupListColumns = 1
	END

	-- Client 210 Storage & Removal Tables DCM 24/05/2012
	IF @DetailFieldID IN (151304,  -- Details
							151309, -- Subcontract work
							151306 -- Contents cleaning 
						)
	BEGIN
	
		SELECT TOP 1 @LeadOrMatterClause = 'MatterID = ' + CAST(mc.MatterID AS VARCHAR)
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) ON m.LeadID=l.LeadID
		INNER JOIN Lead lc WITH (NOLOCK) ON l.CustomerID=lc.CustomerID AND lc.LeadTypeID=1113
		INNER JOIN Matter mc WITH (NOLOCK) ON lc.LeadID=mc.LeadID
		WHERE m.MatterID=@MatterID
		ORDER BY mc.MatterID
			
		SET @DetailFieldID = CASE @DetailFieldID
					WHEN 151304 THEN 138789	
					WHEN 151309 THEN 138798	
					WHEN 151306 THEN 138791	
					END	
			
	END
		   
	SET @basicQuery = 'SELECT tdv.TableRowID as [TableRowID], df.FieldCaption, ISNULL(tdv.DetailValue, '''') as DetailValue, tr.DenyEdit as [DenyEdit], tr.DenyDelete as [DenyDelete]
		FROM dbo.TableDetailValues tdv WITH (NOLOCK)
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON tdv.DetailFieldID = df.DetailFieldID AND df.Enabled = 1 AND df.DetailFieldPageID = ' + CAST(@TableDetailFieldPageID AS VARCHAR) + '
		INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID = ' + CAST(@DetailFieldID AS VARCHAR) + '
		WHERE tdv.' + @LeadOrMatterClause 

		

	/* This seems to be done below now
	IF @PivotOutput = 1 
	BEGIN
		SELECT @pvtStatement = ') as src pivot (MAX(DetailValue) for FieldCaption in (' + @cols + '))	as pvt'
	END	*/

	IF @ResourceTable = 1
	BEGIN

		SELECT @ResourceListDetailFieldPageID = ResourceListDetailFieldPageID, @ResourceListDetailFieldID = DetailFieldID
		FROM dbo.DetailFields df WITH (NOLOCK)
		WHERE df.DetailFieldPageID = @TableDetailFieldPageID 
		AND df.QuestionTypeID = 14

		SELECT @cols = COALESCE(@cols + ',[' + FieldCaption + ']', '[' + FieldCaption + ']'),
			   @rlfieldIdents = COALESCE(@rlfieldIdents + ',' + CAST(DetailFieldID AS VARCHAR(50)), CAST(DetailFieldID AS VARCHAR(50)))
		FROM dbo.DetailFields df WITH (NOLOCK)		
		WHERE df.DetailFieldPageID = @ResourceListDetailFieldPageID 
		AND df.Enabled = 1
		ORDER BY FieldOrder

		SET @rlQuery = 'SELECT tdv.TableRowID as [TableRowID], df.FieldCaption, rldv.DetailValue as DetailValue, tr.DenyEdit as [DenyEdit], tr.DenyDelete as [DenyDelete]
		FROM dbo.ResourceListDetailValues rldv WITH (NOLOCK)
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON rldv.DetailFieldID = df.DetailFieldID AND df.DetailFieldID IN (' + @rlfieldIdents + ')
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.ResourceListID = rldv.ResourceListID AND tdv.DetailFieldID = ' + CAST(@ResourceListDetailFieldID AS VARCHAR) + ' AND tdv.' + @LeadOrMatterClause + '
		INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID = ' + CAST(@DetailFieldID AS VARCHAR) 
		
	END 
	
	SELECT @cols = COALESCE(@cols + ',[' + df.FieldCaption + ']', '[' + df.FieldCaption + ']'),
		   @fieldIdents = COALESCE(@fieldIdents + ',' + CAST(df.DetailFieldID AS VARCHAR(50)), CAST(df.DetailFieldID AS VARCHAR(50)))
	FROM dbo.DetailFields df WITH (NOLOCK)  
	WHERE df.DetailFieldPageID = @TableDetailFieldPageID 
	AND df.Enabled = 1
	AND df.QuestionTypeID <> 14
	ORDER BY df.FieldOrder 
	
	IF @ResourceTable = 1
	BEGIN
		-- Now handle any lookup lists in resource list
		SELECT @cols = COALESCE(@cols + ',[' + df.FieldCaption + '__NAME]', '[' + df.FieldCaption + '__NAME]')
		FROM dbo.DetailFields df WITH (NOLOCK)		
		WHERE df.DetailFieldPageID = @ResourceListDetailFieldPageID 
		AND df.Enabled = 1
		AND df.QuestionTypeID IN (2, 4)
		ORDER BY df.FieldOrder 
	
		SELECT @rlllQuery = 'SELECT tdv.TableRowID as [TableRowID], df.FieldCaption + ''__NAME'' AS FieldCaption, ISNULL(ll.ItemValue, '''') as DetailValue, tr.DenyEdit as [DenyEdit], tr.DenyDelete as [DenyDelete]
		FROM dbo.ResourceListDetailValues rldv WITH (NOLOCK)
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON rldv.DetailFieldID = df.DetailFieldID AND df.DetailFieldID IN (' + @rlfieldIdents + ')
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.ResourceListID = rldv.ResourceListID AND tdv.DetailFieldID = ' + CAST(@ResourceListDetailFieldID AS VARCHAR) + ' AND tdv.' + @LeadOrMatterClause + '
		INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID = ' + CAST(@DetailFieldID AS VARCHAR) + '
		LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON rldv.ValueInt = ll.LookupListItemID AND df.LookupListID = ll.LookupListID
		AND df.QuestionTypeID IN (2, 4)'

	END
	
	IF @LookupListColumns = 1
	BEGIN
	
		SELECT @cols = COALESCE(@cols + ',[' + df.FieldCaption + '__NAME]', '[' + df.FieldCaption + '__NAME]')
		FROM dbo.DetailFields df WITH (NOLOCK)  
		WHERE df.DetailFieldPageID = @TableDetailFieldPageID 
		AND df.Enabled = 1
		AND df.QuestionTypeID IN (2, 4)
		ORDER BY df.FieldOrder 
	
		SELECT @llQuery = 'SELECT tdv.TableRowID as [TableRowID], df.FieldCaption + ''__NAME'' AS FieldCaption, ISNULL(ll.ItemValue, '''') as DetailValue, tr.DenyEdit as [DenyEdit], tr.DenyDelete as [DenyDelete]
		FROM dbo.TableDetailValues tdv WITH (NOLOCK)
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON tdv.DetailFieldID = df.DetailFieldID AND df.Enabled = 1 AND df.DetailFieldPageID = ' + CAST(@TableDetailFieldPageID AS VARCHAR) + '
		INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID = ' + CAST(@DetailFieldID AS VARCHAR) + '
		LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON tdv.ValueInt = ll.LookupListItemID AND df.LookupListID = ll.LookupListID
		WHERE tdv.' + @LeadOrMatterClause + '
		AND df.QuestionTypeID IN (2, 4)'
	END
	
	
	IF @PivotOutput = 1 
	BEGIN
		SET @queryheader = 'SELECT pvt.TableRowID, ' + @cols + ', pvt.DenyEdit, pvt.DenyDelete FROM ('
	END
	
	IF @rlQuery != ''
	BEGIN
		SELECT @query = @rlQuery
	END
	
	IF @BasicQuery != ''
	BEGIN
		IF @query != ''
		BEGIN
			SELECT @query = @query + '
			 UNION 
			'
		END
		SELECT @query = @query + @BasicQuery	
	END
	
	IF @llQuery != ''
	BEGIN
		IF @query != ''
		BEGIN
			SELECT @query = @query + '
			 UNION 
			'
		END
		SELECT @query = @query + @llQuery	
	END
	
	IF @rlllQuery != ''
	BEGIN
		IF @query != ''
		BEGIN
			SELECT @query = @query + '
			 UNION 
			'
		END
		SELECT @query = @query + @rlllQuery	
	END
	
	
	IF @PivotOutput = 1 BEGIN
		SELECT @PvtStatement = ') as src pivot (MAX(DetailValue) for FieldCaption in (' + @cols + '))	as pvt'	
	END
	

	SELECT @query = @queryheader + @query + @PvtStatement

	/*
		2013-11-01 - ACE
		Added the following lookup instead of using the hard coded SQL in this SP
	*/
	SELECT @query = CASE WHEN CompleteSQLOverride = 0 THEN @query + ' ' + SqlToUse ELSE SqlToUse END
	FROM TableRowsClientOption tro WITH (NOLOCK)
	WHERE tro.DetailFieldID = @DetailFieldID
	
	IF @@ROWCOUNT > 0
	BEGIN
	
		/*
			If we have a full query from TableRowsClientOption then we will
			need to replace any @CustomerID/@LeadID/@CaseID/@MatterID's out
			with the correct and real ID.
		*/
		SELECT @query = REPLACE(@query, '@CustomerID', CONVERT(VARCHAR(100),@CustomerID))
		SELECT @query = REPLACE(@query, '@LeadID', CONVERT(VARCHAR(100),@LeadID))
		SELECT @query = REPLACE(@query, '@CaseID', CONVERT(VARCHAR(100),@CaseID))
		SELECT @query = REPLACE(@query, '@MatterID', CONVERT(VARCHAR(100),@MatterID))
	
	END

	IF @PrintOnly = 1
	BEGIN
		PRINT @query
	END
	ELSE
	BEGIN
	
		EXECUTE(@query)	 
		
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetByDetailFieldIDLeadIDMatterID_New] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows__GetByDetailFieldIDLeadIDMatterID_New] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetByDetailFieldIDLeadIDMatterID_New] TO [sp_executeall]
GO
