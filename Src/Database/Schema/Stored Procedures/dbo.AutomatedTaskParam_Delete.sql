SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the AutomatedTaskParam table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTaskParam_Delete]
(

	@AutomatedTaskParamID int   
)
AS


				DELETE FROM [dbo].[AutomatedTaskParam] WITH (ROWLOCK) 
				WHERE
					[AutomatedTaskParamID] = @AutomatedTaskParamID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskParam_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_Delete] TO [sp_executeall]
GO
