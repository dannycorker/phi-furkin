SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-05-19
-- Description:	Get Dataloader Maps by ClientID
-- =============================================
CREATE PROCEDURE [dbo].[dlm]
	@ClientID int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	m.DataLoaderMapID, 
			m.MapName, 
			m.WhenCreated, 
			cp.UserName [Created By], 
			m.LastUpdated, 
			cp2.UserName [Updated By], 
			m.EnabledForUse [Enabled], 
			lt.LeadTypeID [LeadType], 
			lt.LeadTypeName,
			'aq 1,7,' + convert(varchar,m.DataLoaderMapID) [Describe]
	FROM dbo.DataLoaderMap m WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = m.CreatedBy
	LEFT JOIN dbo.ClientPersonnel cp2 WITH (NOLOCK) on cp2.ClientPersonnelID = m.UpdatedBy
	INNER JOIN dbo.LeadType lt WITH (NOLOCK) on lt.LeadTypeID = m.LeadTypeID
	WHERE m.ClientID = @ClientID
	
	IF EXISTS 
		(
		SELECT OBJECT_NAME(object_id)
		FROM sys.sql_modules WITH (NOLOCK) 
		WHERE OBJECT_NAME(object_id) = '_C00_SAD'
		and definition like '%@ClientID = %' + convert(varchar,@ClientID) + '%'
		)
	BEGIN
		SELECT 'This Client uses Sql After Dataload' [Sql After Dataload]
	END
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[dlm] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[dlm] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[dlm] TO [sp_executeall]
GO
