SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-12-09
-- Description:	Manually Pay Check - 'Check Paid' EventTypeID: 159024 for PPET-784
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Collections_159024]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@TermsDate						DATE
			,@AdjustmentDate				DATE
			,@Count							INT
			,@ValueInt						INT
			,@DetailValueData				dbo.tvpDetailValueData
			,@EventComments					VARCHAR(2000)
			,@NextEventID					INT
			,@IdId							dbo.tvpIntInt
			,@WorkflowTaskID				INT

			DECLARE @NumberedValues TABLE ( ID INT Identity, Value VARCHAR(2000) )


/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

	DECLARE @PurchasedProductPaymentScheduleID INT,
	@PurchasedProductID INT,
	@CustomerPaymentScheduleID INT,
	@CustomerLedgerID INT,
	@PaymentID INT,
	@Comments VARCHAR(2000),
	@Notes VARCHAR(250) =  'Off System Check Created',
	@Now DATE = dbo.fn_GetDate_Local(),
	@PaymentTypeID INT = 3 /*Cheque*/,
	@PaymentGross DECIMAL(18,2),
	@PaymentIPT DECIMAL(18,2),
	@PaymentNet DECIMAL(18,2),
	@AccountRef VARCHAR(20),
	@AccountID INT,
	@PaymentStatusID INT = 6,
	@CollectionsMatterID INT

	IF @EventTypeID = 159024
	BEGIN
	
		
		SELECT @PurchasedProductPaymentScheduleID = dbo.fnGetSimpleDvAsInt(314357,@MatterID)

		SELECT @PurchasedProductID = PurchasedProductID, @AccountID = AccountID, @PaymentGross = PaymentGross, @PaymentIPT = PaymentVAT, @PaymentNet = PaymentNet, @CustomerPaymentScheduleID = CustomerPaymentScheduleID
		FROM PurchasedProductPaymentSchedule WITH (NOLOCK)
		WHERE PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

		SELECT @AccountRef = a.Reference, @CollectionsMatterID = a.ObjectID
		FROM Account a with (NOLOCK)
		INNER JOIN Customers c with (NOLOCK) on c.CustomerID = a.CustomerID 
		WHERE a.AccountID = @AccountID 

		/*IF user can't perform this action throw error*/
		IF ISNULL((dbo.fnGetSimpleDvAsInt(314355,@WhoCreated)),0) <> 1
		BEGIN
			SELECT @ErrorMessage = '<font color="red"></br></br>User permission not set or user is not authorized to apply this Event.</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN -1
		END

		/*IF PPPS Value > 0.00 throw error*/
		IF (SELECT ppps.PaymentGross
			FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
			WHERE ppps.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID) > 0.00
		BEGIN
			SELECT @ErrorMessage = '<font color="red"></br></br>PurchasedProductPaymentScheduleGross is greater than 0.00.</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN -1
		END

		/*IF PPPS is already Paid throw Error*/
		IF (SELECT ppps.PaymentStatusID
			FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
			WHERE ppps.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID) = 6
		BEGIN
			SELECT @ErrorMessage = '<font color="red"></br></br>PurchasedProductPaymentSchedule record is already Paid.</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN -1
		END

		/*IF PP does not belong to this Policy throw error*/
		IF (SELECT COUNT(*)
			FROM PurchasedProduct pp WITH (NOLOCK)
			WHERE pp.PurchasedProductID = @PurchasedProductID
			AND pp.ObjectID = @MatterID) < 1
		BEGIN
			SELECT @ErrorMessage = '<font color="red"></br></br>PurchasedProductPaymentScheduleID does not belong to this Policy.</br></font><font color="#edf4fa">'

			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN -1
		END



		/*Payment - Insert to Payment*/
		INSERT INTO Payment (ClientID, CustomerID, PaymentDateTime, PaymentTypeID, DateReceived, PaymentDescription,PaymentReference,PaymentGross, PaymentNet, PaymentVAT, PaymentStatusID, customerPaymentScheduleID, WhoCreated, WhenCreated, WhoModified, WhenModified)
		Values (@ClientID,@CustomerID,@Now, @PaymentTypeID,@Now, @Notes, @AccountRef, @PaymentGross, @PaymentNet, @PaymentIPT, @PaymentStatusID, @CustomerPaymentScheduleID,58552,@Now, 58552,@Now)

		SELECT @PaymentID = SCOPE_IDENTITY()

		/*CustomerLedger - Insert to CustomerLedger*/
		INSERT INTO CustomerLedger (ClientID,	CustomerID,	EffectivePaymentDate,TransactionDate,	TransactionReference,	TransactionDescription, TransactionGross,TransactionNet,TransactionVAT, ObjectID,	ObjectTypeID,	PaymentID,	WhoCreated,	WhenCreated)
		VALUES (@ClientID,@CustomerID, @Now, @Now, @AccountRef,@Notes, @PaymentGross, @PaymentNet,@PaymentIPT,@CollectionsMatterID,2,	@PaymentID,	58552,@Now)

		SELECT @CustomerLedgerID = SCOPE_IDENTITY()

		/*CPS - Update to Paid and add CLID*/
		UPDATE cps
		SET PaymentStatusID = @PaymentStatusID, CustomerLedgerID = @CustomerLedgerID
		FROM CustomerPaymentSchedule cps WITH (NOLOCK)
		WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID

		/*PPPS - Update to Paid and add CLID*/
		UPDATE ppps
		SET PaymentStatusID = @PaymentStatusID,CustomerLedgerID = @CustomerLedgerID
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		WHERE PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

		/*Clear out PPPS DetailField*/
		EXEC _C00_SimpleValueIntoField 314357,'',@MatterID

		/*LeadEvent Comments*/
		SELECT @Comments='Payment set to Paid for PurchasedProductPaymentScheduleID: ' + CAST(@PurchasedProductPaymentScheduleID AS VARCHAR) + '.'

		EXEC [dbo].[_C00_SetLeadEventComments] @LeadEventID, @Comments, 1
				
	END
		
	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
