SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DataLoaderFieldDefinition table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFieldDefinition_Update]
(

	@DataLoaderFieldDefinitionID int   ,

	@ClientID int   ,

	@DataLoaderMapID int   ,

	@DataLoaderObjectTypeID int   ,

	@DataLoaderMapSectionID int   ,

	@DataLoaderObjectFieldID int   ,

	@DetailFieldID int   ,

	@DetailFieldAlias varchar (500)  ,

	@NamedValue varchar (250)  ,

	@Keyword varchar (250)  ,

	@DataLoaderKeywordMatchTypeID int   ,

	@RowRelativeToKeyword int   ,

	@ColRelativeToKeyword int   ,

	@SectionRelativeRow int   ,

	@SectionAbsoluteCol int   ,

	@ValidationRegex varchar (250)  ,

	@Equation varchar (2000)  ,

	@IsMatchField bit   ,

	@DecodeTypeID int   ,

	@DefaultLookupItemID int   ,

	@SourceDataLoaderFieldDefinitionID int   ,

	@Notes varchar (2000)  ,

	@AllowErrors bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DataLoaderFieldDefinition]
				SET
					[ClientID] = @ClientID
					,[DataLoaderMapID] = @DataLoaderMapID
					,[DataLoaderObjectTypeID] = @DataLoaderObjectTypeID
					,[DataLoaderMapSectionID] = @DataLoaderMapSectionID
					,[DataLoaderObjectFieldID] = @DataLoaderObjectFieldID
					,[DetailFieldID] = @DetailFieldID
					,[DetailFieldAlias] = @DetailFieldAlias
					,[NamedValue] = @NamedValue
					,[Keyword] = @Keyword
					,[DataLoaderKeywordMatchTypeID] = @DataLoaderKeywordMatchTypeID
					,[RowRelativeToKeyword] = @RowRelativeToKeyword
					,[ColRelativeToKeyword] = @ColRelativeToKeyword
					,[SectionRelativeRow] = @SectionRelativeRow
					,[SectionAbsoluteCol] = @SectionAbsoluteCol
					,[ValidationRegex] = @ValidationRegex
					,[Equation] = @Equation
					,[IsMatchField] = @IsMatchField
					,[DecodeTypeID] = @DecodeTypeID
					,[DefaultLookupItemID] = @DefaultLookupItemID
					,[SourceDataLoaderFieldDefinitionID] = @SourceDataLoaderFieldDefinitionID
					,[Notes] = @Notes
					,[AllowErrors] = @AllowErrors
				WHERE
[DataLoaderFieldDefinitionID] = @DataLoaderFieldDefinitionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFieldDefinition_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFieldDefinition_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFieldDefinition_Update] TO [sp_executeall]
GO
