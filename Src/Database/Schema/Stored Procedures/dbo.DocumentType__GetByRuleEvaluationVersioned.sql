SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2011-11-14
-- Description:	Evaluate rules to decide which document template to use for an event
-- =============================================
CREATE PROCEDURE [dbo].[DocumentType__GetByRuleEvaluationVersioned]
(
	@EventTypeID int,
	@CaseID int = NULL,
	@RunAsUserID int = NULL
	--@ReturnDocTypeIdOnly INT = 0 /*CS 2015-01-12*/
)
AS
BEGIN

	DECLARE @DocumentTypeID INT, 
	@BaseDocumentTypeID INT, 
	@CustomerID INT, 
	@LeadID INT, 
	@SqlStatement NVARCHAR(MAX) = ''
	
	/* Get default details */
	SELECT	TOP 1 
			@BaseDocumentTypeID = ISNULL(et.DocumentTypeID, 0),
			@DocumentTypeID  = ISNULL(et.DocumentTypeID, 0)
	FROM	EventType et WITH (NOLOCK)
	WHERE	et.EventTypeID = @EventTypeID
	
	/* If there are any rules for this event, evaluate them next */
	IF EXISTS(SELECT * FROM PredicateRuleAssigned pra WITH (NOLOCK) WHERE	pra.EventTypeID = @EventTypeID) 
	BEGIN

		/* Get extra details for the Customer & Lead */
		SELECT 
			@CustomerID = l.CustomerID,
			@LeadID = l.LeadID
		FROM Cases ca WITH (NOLOCK)
		INNER JOIN  Lead l WITH (NOLOCK) ON ca.LeadID = l.LeadID
		WHERE ca.CaseID = @CaseID
		
		/* Build up the list of   WHEN...THEN clauses */
		SELECT	@SqlStatement +=  'WHEN (' + ISNULL(pr.RuleSQL, '1=1')+ ') THEN '+ CAST(pra.OverrideDocumentTypeID AS VARCHAR) + ' '
		FROM	PredicateRuleAssigned pra WITH (NOLOCK) 
		LEFT JOIN PredicateRule pr WITH (NOLOCK) ON pr.PredicateRuleID = pra.PredicateRuleID
		WHERE	pra.EventTypeID = @EventTypeID
		ORDER BY pra.RuleOrder, pra.PredicateRuleAssignedID ASC
	
		/* Combine all the above into a CASE  WHEN...THEN WHEN...THEN WHEN...THEN ELSE n END statement */
		SELECT @SqlStatement = 
			'SELECT	@DocumentTypeID = CASE '+@SqlStatement+' ELSE @BaseDocumentTypeID END
			FROM Customers WITH (NOLOCK) 
			INNER JOIN Lead WITH (NOLOCK) ON Customers.CustomerID = Lead.CustomerID
			INNER JOIN Cases WITH (NOLOCK) ON Cases.LeadID = Lead.LeadID
			WHERE Cases.CaseID = @CaseID'

		/* Run the sql and get the final DocumentTypeID to use */
		EXECUTE sp_executesql 
			@SqlStatement, 
			N'@CustomerID INT, @LeadID INT, @CaseID INT, @BaseDocumentTypeID INT, @DocumentTypeID INT OUTPUT', 
			@CustomerID, @LeadID, @CaseID, @BaseDocumentTypeID,
			@DocumentTypeID OUTPUT
		
		/* If the sql failed to return any data, assign an invalid DocumentTypeID */
		IF (@@ROWCOUNT = 0) 
		BEGIN
			SELECT @DocumentTypeID = -1
		END
		
	END
	
	/* Send back a DAL DocumentType object */
	
	/* Gets a versioned document type - zen desk ticket number : 44286 */
	EXEC dbo.DocumentType__GetByDocumentTypeIDVersioned @DocumentTypeID
	
	--SELECT	*
	--FROM	DocumentType WITH (NOLOCK)
	--WHERE	DocumentTypeID = @DocumentTypeID
		
	SELECT @@ROWCOUNT

END		


GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetByRuleEvaluationVersioned] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType__GetByRuleEvaluationVersioned] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetByRuleEvaluationVersioned] TO [sp_executeall]
GO
