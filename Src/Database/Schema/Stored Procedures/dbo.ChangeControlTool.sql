SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2017-02-28
-- Description:	ChangeControlTool
--
-- EXEC ChangeControlTool @Method,@ChangeControlID,@ObjectName,@DateTime
-- 
-- * Change Branch: EXEC ChangeControlTool 'Branch',null,'BranchName'
--
--
-- * Revert an object by ChangeControlID:
--		EXEC ChangeControlTool 'Revert',1201
-- 
-- * Revert an object by DateTime - finds the last version of the object equal to or before the datetime:
--		EXEC ChangeControlTool 'Revert',null,'fnConvertToCamelCase','2017-01-12 19:23:18.120'
-- 
-- * Revert all DB objects by DateTime - outputs a list of exec statements - finds the last version of all objects equal to or before the datetime: 
--		EXEC ChangeControlTool 'RevertAll',null,null,'2017-01-12 19:23:18.120'
--
-- * Revert all DB objects by DateTime - JUST DOES IT - finds the last version of all objects equal to or before the datetime: 
--		EXEC ChangeControlTool 'RevertAllDANGER',null,null,'2017-01-12 19:23:18.120'
--
--
-- * Show all history for a function or proc
--		EXEC ChangeControlTool 'HistoryAll',null,'_C427_Claim_Reserves_Update'

--
-- =============================================
CREATE PROCEDURE [dbo].[ChangeControlTool]
	@Method				Varchar(200) -- 'Revert','CaptureAll','History','HistoryAll','Branch'
	,@ProcName			Varchar(200) = NULL
	,@ChangeControlID	int			 = NULL	
	,@DateTime			DATETIME     = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ObjectRef  varchar(MAX) 
	,@ObjectName		varchar(200) 
	,@ObjectID			int 
	,@Type				varchar(200)
	,@CurrentChangeControlID int = 0 
	,@ChangeUser		varchar(200)
	,@ChangeDate		DATETIME
	,@ChangeControlIDA	int
	,@ChangeControlIDB	int
	,@obja varchar(max)
	,@objb varchar(max)
	,@ChangeobjA dbo.tvpVarcharInt 
	,@ChangeobjB dbo.tvpVarcharInt 
	,@i int = 0
	,@UserName Varchar(200)
	,@UserBranchID int
	,@BranchName varchar(200)
	,@hasContent BIT = 0

	DECLARE @ChangesAll table (LineID int,EditType varchar(1),Text nVarchar(max),ChangeDate DATETIME, ModifiedBy varchar(200),Branch Varchar(200))

	/*------------------------------------------------------------------------------------------------*/
	
	IF @Method in ('HistoryAll','History') and @ProcName > ''
	BEGIN	
		SELECT @ChangeControlIDB = MAX(cc.changecontrolid) ,@ChangeDate = cc.ChangeDateTime,@ChangeUser	= cc.UserName
		FROM ChangeControl cc with(nolock) 
		WHERE cc.ObjectName = @ProcName				
		group by cc.ChangeDateTime,cc.UserName

		SELECT @ChangeControlIDA = MAX(cc.changecontrolid) 
		FROM ChangeControl cc with(nolock) 
		WHERE cc.ObjectName = @ProcName
		and cc.ChangeControlID < @ChangeControlIDB
					
		WHILE ( @ChangeControlIDB > 0  
				AND ( @i < 20 and @Method = 'HistoryAll')  -- get last 20 changes... can take a while to run
				OR
				(	@Method = 'History' 
					AND ( @i < 20 and @hasContent = 0 ) -- if you find a change, then stop looking
				) 
			)
		BEGIN
			SELECT @obja = cc.objectreference from ChangeControl cc with(nolock) WHERE cc.ChangeControlID = @ChangeControlIDA
			SELECT @objb = cc.objectreference,@BranchName = cc.Branch from ChangeControl cc with(nolock) WHERE cc.ChangeControlID = @ChangeControlIDB

			insert into @ChangeobjA (AnyValue,AnyID)
			exec [dbo].[_C00_helptext] @objA

			insert into @Changeobjb (AnyValue,AnyID)
			exec [dbo].[_C00_helptext] @objb

			if @ChangeControlIDA > 0 and @ChangeControlIDB > 0 
			begin			
				INSERT @ChangesAll (LineID,EditType,Text)
				EXEC [dbo].[ChangeControlDiff] null,null,null,@ChangeobjA,@ChangeobjB

				update c
				set c.modifiedby = @ChangeUser,c.changeDate = @ChangeDate,c.Branch = @BranchName
				from @changesAll c
				WHERE c.modifiedby is null 
				and c.changedate is null			
			end

			set @ChangeControlIDB = @ChangeControlIDA
			set @ChangeControlIDA = null

			SELECT @ChangeControlIDA = MAX(cc.changecontrolid) 
			FROM ChangeControl cc with(nolock) 
			WHERE cc.ObjectName = @ProcName
			and cc.ChangeControlID < @ChangeControlIDB	

			SELECT @ChangeDate = cc.ChangeDateTime, @ChangeUser	= cc.UserName
			FROM ChangeControl cc with(nolock) 
			WHERE cc.ChangeControlID = @ChangeControlIDB 		

			-- no previous change was stored for this object
			if @ChangeControlIDA is null
			begin
				set @ChangeControlIDB = null
			end


			if exists (select * from @ChangesAll) begin set @hasContent = 1 end
					
			set @i += 1 
		END
		
		--Output everything captured
		SELECT * FROM @ChangesAll ca
	END

	/*------------------------------------------------------------------------------------------------*/

	IF @Method = 'Revert' and @ChangeControlID > 0
	BEGIN	
		select @ObjectRef = cc.ObjectReference ,@ObjectName = cc.ObjectName, @Type  = cc.ObjectType 
		from ChangeControl cc with(nolock) 
		where cc.ChangeControlID = @ChangeControlID

		select @CurrentChangeControlID = MAX(cc.changecontrolid)
		from ChangeControl cc with(nolock) 
		where cc.ObjectType = @Type 
		and cc.ObjectName = @ObjectName

		SELECT @ObjectID = s.object_id from sys.objects s with(nolock) 
		WHERE s.name = @ObjectName

		-- object exists and object ref contains create statement
		if @ObjectID > 0 and @ObjectRef LIKE '%CREATE ' + @Type  + '%'
		begin

			SELECT @ObjectRef = REPLACE(@ObjectRef,'CREATE ' + @TYPE,'ALTER ' + @TYPE)

		end
		-- Object does not exist and object ref contains alter statement 
		if @ObjectID IS NULL and @ObjectRef LIKE '%ALTER ' + @Type  + '%'
		begin

			SELECT @ObjectRef = REPLACE(@ObjectRef,'ALTER ' + @TYPE,'CREATE ' + @TYPE)

		end

		EXEC(@ObjectRef)

		select 'Changed ' + @Type + ': ' + @ObjectName + ' from change controlID: ' + cast(@CurrentChangeControlID as varchar(200))
		+ ' to ChangeControlID: '  + cast(@ChangeControlID as varchar(200))
	END

	/*------------------------------------------------------------------------------------------------*/

	IF @Method = 'CaptureAll'
	BEGIN		
		insert into ChangeControl ([Instance],[DatabaseName],[Branch],[UserName],[ChangeDateTime],[ObjectType],[ObjectName],[ObjectDescription],[ObjectReference],[Latest])
			SELECT @@servername,DB_NAME(),'default','ChangeControlTool',dbo.fn_GetDate_Local()
			,replace(replace(replace(replace(obj.type_desc,'SQL_STORED_PROCEDURE','PROCEDURE'),'SQL_TABLE_VALUED_FUNCTION','FUNCTION'),'SQL_SCALAR_FUNCTION','FUNCTION'),'SQL_INLINE_TABLE_VALUED_FUNCTION','FUNCTION')
			,obj.name,'ObjectDescription',md.definition,0 
			FROM  sys.sql_modules md
			inner join sys.objects obj on obj.object_id=md.object_id
			WHERE obj.type_desc in ('SQL_STORED_PROCEDURE','SQL_TABLE_VALUED_FUNCTION','SQL_SCALAR_FUNCTION','SQL_INLINE_TABLE_VALUED_FUNCTION')
			and not exists ( 
				select * 
				from ChangeControl cc with(nolock) 
				WHERE cc.ObjectName = obj.name
				and cc.ObjectReference = md.definition
				and cc.UserName = 'ChangeControlTool'
			)
	END

	if @Method = 'Branch'
	begin
		select @UserName = SUSER_SNAME()

		SELECT top 1 @UserBranchID = cb.userbranchID 
		from ChangeControlBranch cb with(nolock) 
		WHERE cb.UserName = @UserName
		order by cb.UserBranchID desc

		IF @UserBranchID is not null
		begin
			UPDATE TOP (1) cb
			SET cb.BranchName = @ProcName,cb.CreatedDateTime = dbo.fn_GetDate_Local()
			from ChangeControlBranch cb
			WHERE cb.UserBranchID = @UserBranchID
		end 
		else
		begin
			insert into ChangeControlBranch (UserName, BranchName, CreatedDateTime)
			values (@UserName,@ProcName,dbo.fn_GetDate_Local())
		end
		
	end 
	
	/*------------------------------------------------------------------------------------------------*/

	IF @Method NOT IN ('Revert','test','CaptureAll','History','HistoryAll','Branch')
	BEGIN		
		select 'A valid method was not provided as an input parameter'		
	END

	/*------------------------------------------------------------------------------------------------*/


END
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeControlTool] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeControlTool] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeControlTool] TO [sp_executeall]
GO
