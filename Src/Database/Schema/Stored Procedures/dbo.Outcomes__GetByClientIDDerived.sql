SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









/*
----------------------------------------------------------------------------------------------------

-- Created By:  Jim Green
-- Purpose: Simulate having ClientID as a foreign key
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[Outcomes__GetByClientIDDerived]
(
	@ClientID int   
)
AS	
				SELECT *					
				FROM
					[dbo].[Outcomes]
				WHERE 
					--[ClientQuestionnaireID] IN (SELECT ClientQuestionnaireID FROM ClientQuestionnaires WHERE ClientID = @ClientID)
					[ClientID] = @ClientID
				
				Select @@ROWCOUNT






GO
GRANT VIEW DEFINITION ON  [dbo].[Outcomes__GetByClientIDDerived] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Outcomes__GetByClientIDDerived] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Outcomes__GetByClientIDDerived] TO [sp_executeall]
GO
