SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SMSResponseMap table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSResponseMap_GetBySMSQuestionnaireID]
(

	@SMSQuestionnaireID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SMSResponseMapID],
					[ClientID],
					[SMSQuestionnaireID],
					[FromSMSQuestionGroupID],
					[FromSMSQuestionID],
					[FromAnswer],
					[ToSMSQuestionGroupID],
					[ToSMSQuestionID],
					[EndOfQuestionnaire],
					[Score],
					[ConfidenceRangeStart],
					[ConfidenceRangeEnd],
					[RuleID],
					[ProcessingOrder]
				FROM
					[dbo].[SMSResponseMap] WITH (NOLOCK) 
				WHERE
					[SMSQuestionnaireID] = @SMSQuestionnaireID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap_GetBySMSQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSResponseMap_GetBySMSQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap_GetBySMSQuestionnaireID] TO [sp_executeall]
GO
