SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the CaseDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseDetailValues_Delete]
(

	@CaseDetailValueID int   
)
AS


				DELETE FROM [dbo].[CaseDetailValues] WITH (ROWLOCK) 
				WHERE
					[CaseDetailValueID] = @CaseDetailValueID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseDetailValues_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseDetailValues_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseDetailValues_Delete] TO [sp_executeall]
GO
