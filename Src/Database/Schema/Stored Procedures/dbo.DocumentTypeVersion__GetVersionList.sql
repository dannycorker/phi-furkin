SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 10/07/2017
-- Description:	Gets a list of versions fopr the given document type
-- =============================================
CREATE PROCEDURE [dbo].[DocumentTypeVersion__GetVersionList]
	@DocumentTypeID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	SELECT DocumentTypeVersionID, CONVERT(varchar(10), VersionNumber) + ', ' + LEFT(CONVERT(VARCHAR, CreatedOn, 100), 22) VersionName FROM DocumentTypeVersion WITH (NOLOCK) 
	WHERE DocumentTypeID=@DocumentTypeID
	ORDER BY VersionNumber DESC
    
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeVersion__GetVersionList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTypeVersion__GetVersionList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeVersion__GetVersionList] TO [sp_executeall]
GO
