SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2010-11-18
-- Description:	List all AutomatedTaskInfo for a queue, scheduler or server
-- 2017-08-23 CPS added TaskName
-- 2017-08-24 CPS added queued tasks
-- =============================================
CREATE PROCEDURE [dbo].[ati] 
	@QueueID int = NULL, 
	@SchedulerID int = NULL, 
	@Server varchar(250) = NULL 
AS
BEGIN

	SET NOCOUNT ON;
	
	/*
		Show any tasks that are running right now.
	*/
	SELECT 'Already Running' as [Running Now], at.Taskname, ati.* 
	FROM dbo.AutomatedTaskInfo ati WITH (NOLOCK)  
	INNER JOIN dbo.AutomatedTask at WITH (NOLOCK) ON at.TaskID = ati.TaskID
	WHERE at.AlreadyRunning = 1 
	AND (@QueueID IS NULL OR @QueueID = ati.QueueID)
	AND (@SchedulerID IS NULL OR @SchedulerID = ati.SchedulerID)
	AND (@Server IS NULL OR @Server = ati.ServerName)
	ORDER BY ati.QueueID, ati.SchedulerID

	/*
		Show other tasks that are locked to run next.
	*/
	SELECT 'Locked In' as [Locked In], at.Taskname, ati.* 
	FROM dbo.AutomatedTaskInfo ati WITH (NOLOCK)  
	INNER JOIN dbo.AutomatedTask at WITH (NOLOCK) ON at.TaskID = ati.TaskID
	WHERE ati.LockDateTime IS NOT NULL
	AND at.AlreadyRunning = 0 
	AND (@QueueID IS NULL OR @QueueID = ati.QueueID)
	AND (@SchedulerID IS NULL OR @SchedulerID = ati.SchedulerID)
	AND (@Server IS NULL OR @Server = ati.ServerName)
	ORDER BY ati.QueueID, ati.SchedulerID, ati.LockDateTime 
	
	/*
	Show non-locked, pending tasks
	*/
	SELECT TOP 10 'Queued' [Queued], at.Taskname, ati.* 
	FROM AutomatedTaskInfo ati WITH ( NOLOCK ) 
	INNER JOIN AutomatedTask at WITH ( NOLOCK ) on at.TaskID = ati.TaskID 
	WHERE ati.LockDateTime is NULL
	AND at.AlreadyRunning = 0
	AND at.Enabled = 1
	AND at.NextRunDateTime < dbo.fn_GetDate_Local()
	ORDER BY at.NextRunDateTime
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ati] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ati] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ati] TO [sp_executeall]
GO
