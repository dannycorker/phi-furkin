SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-12-04
-- Description:	Delete all notes of  type within note type scope
-- Mods
-- DCM 2016-07-06 Add LeadEventID argument & cascade delete based on LeadEventID in ContactID
-- =============================================
CREATE PROCEDURE [dbo].[_C600_DeleteANote] 
(
	@CaseID INT, 
	@NoteTypeID INT = NULL,
	@LeadEventID INT = NULL
)

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @NoteLeadEventID INT,
			@WhoCreated INT,
			@ApplyToAll INT, -- 0=this case only; 1=all cases in this lead; 2=all cases in all leads of this lead type; 3=all of the policy holder's cases
			@AllowMultiple BIT
			
			
	DECLARE @Notes TABLE (NoteLeadEventID INT, Done INT)

	/* Apply Note to all cases required for the Note type*/
	SELECT @ApplyToAll = CASE ata.ValueInt
							WHEN 73988 THEN 0
							WHEN 73989 THEN 1
							WHEN 73990 THEN 2
							WHEN 73991 THEN 3
							WHEN 73992 THEN 4 
							END,
		 @AllowMultiple = CASE WHEN amn.ValueInt = 5144 THEN 1 ELSE 0 END
	FROM ResourceListDetailValues ntid WITH (NOLOCK) 
	INNER JOIN ResourceListDetailValues amn WITH (NOLOCK) ON ntid.ResourceListID=amn.ResourceListID AND amn.DetailFieldID=176939
	INNER JOIN ResourceListDetailValues ata WITH (NOLOCK) ON ntid.ResourceListID=ata.ResourceListID AND ata.DetailFieldID=176938
	WHERE ntid.DetailFieldID=176936 AND ntid.ValueInt=@NoteTypeID
				
	-- get the note LeadEventID to use
	IF @NoteTypeID IS NOT NULL
	BEGIN
		SELECT top 1 @NoteLeadEventID=LeadEventID, 
					@WhoCreated=dbo.fnGetKeyValueAsIntFromThirdPartyIDs (ClientID,53,'CFG|AqAutomationCPID',0)
		FROM LeadEvent WITH (NOLOCK) 
		WHERE CaseID=@CaseID and EventDeleted=0 and NoteTypeID=@NoteTypeID
	END
	ELSE
	BEGIN
		SELECT @NoteLeadEventID=LeadEventID,
				@WhoCreated=WhoCreated
		FROM LeadEvent WITH (NOLOCK) 
		WHERE LeadEventID=@LeadEventID
	END

	-- Delete the banner note, if not already deleted
	IF EXISTS ( SELECT * FROM LeadEvent WHERE LeadEventID=@NoteLeadEventID AND EventDeleted=0 )
	BEGIN
		EXEC DeleteLeadEvent @NoteLeadEventID, @WhoCreated, 'Auto delete', 4, 0
	END
	
	-- Delete any related cascaded notes
	INSERT INTO @Notes (NoteLeadEventID,Done)
	SELECT snk.LeadEventID,0
	FROM LeadEvent snk WITH (NOLOCK) 
	INNER JOIN LeadEvent src WITH (NOLOCK) ON src.HoldLeadEventID=snk.HoldLeadEventID 
											AND src.LeadEventID<>snk.LeadEventID 
											AND snk.EventDeleted=0
	WHERE src.LeadEventID=@NoteLeadEventID 

	
	WHILE EXISTS ( SELECT * FROM @Notes where Done=0 )
	BEGIN
	
		SELECT TOP 1 @NoteLeadEventID=NoteLeadEventID FROM @Notes where Done=0	

		EXEC DeleteLeadEvent @NoteLeadEventID, @WhoCreated, 'Auto delete', 4, 0

		UPDATE @Notes SET Done=1 WHERE NoteLeadEventID=@NoteLeadEventID
		
	END

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_DeleteANote] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_DeleteANote] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_DeleteANote] TO [sp_executeall]
GO
