SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeAutomatedTask table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAutomatedTask_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EventTypeAutomatedTaskID],
					[ClientID],
					[EventTypeID],
					[AutomatedTaskID],
					[TimeDelayInSeconds],
					[AddTimeDelayIfAboutToRun]
				FROM
					[dbo].[EventTypeAutomatedTask] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedTask_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedTask_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedTask_GetByClientID] TO [sp_executeall]
GO
