SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 06-01-2014
-- Description:	Gets a list of parameters for the given stored procedure name
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetStoredProcedureParameters] 

	@StoredProcedureName VARCHAR(2000)

AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT PARAMETER_NAME FROM information_schema.parameters
	WHERE specific_name=@StoredProcedureName ORDER BY ORDINAL_POSITION
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetStoredProcedureParameters] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetStoredProcedureParameters] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetStoredProcedureParameters] TO [sp_executeall]
GO
