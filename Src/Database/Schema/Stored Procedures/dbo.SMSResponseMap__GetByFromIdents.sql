SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 22-04-2013
-- Description:	Gets a list of Responses by there from idents
-- =============================================
CREATE PROCEDURE [dbo].[SMSResponseMap__GetByFromIdents]
(

	@SMSQuestionnaireID INT,
	@SMSQuestionGroupID INT,
	@SMSQuestionID INT
	
)
AS


				SET ANSI_NULLS ON
				
				SELECT *
					
				FROM
					[dbo].[SMSResponseMap] WITH (NOLOCK) 
				WHERE
					[SMSQuestionnaireID] = @SMSQuestionnaireID AND
					[FromSMSQuestionGroupID] = @SMSQuestionGroupID AND
					[FromSMSQuestionID] = @SMSQuestionID
				ORDER BY ProcessingOrder ASC
				
				
				SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap__GetByFromIdents] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSResponseMap__GetByFromIdents] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSResponseMap__GetByFromIdents] TO [sp_executeall]
GO
