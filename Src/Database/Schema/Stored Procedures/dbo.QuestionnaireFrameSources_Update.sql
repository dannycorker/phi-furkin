SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the QuestionnaireFrameSources table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireFrameSources_Update]
(

	@QuestionnaireFrameSourceID int   ,

	@ClientID int   ,

	@ClientQuestionnaireID int   ,

	@PageNumber int   ,

	@SourceUrl varchar (555)  ,

	@FrameType int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[QuestionnaireFrameSources]
				SET
					[ClientID] = @ClientID
					,[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[PageNumber] = @PageNumber
					,[SourceUrl] = @SourceUrl
					,[FrameType] = @FrameType
				WHERE
[QuestionnaireFrameSourceID] = @QuestionnaireFrameSourceID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFrameSources_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireFrameSources_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFrameSources_Update] TO [sp_executeall]
GO
