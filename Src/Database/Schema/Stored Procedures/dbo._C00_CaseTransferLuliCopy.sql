SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Alex ELger
-- Create date: 2008-09-09
-- Description:	Copy lookup list items from one case transfer to another
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CaseTransferLuliCopy]
(
@LeadEventID int
)
	
AS
BEGIN

	SET NOCOUNT ON

	Declare	@myERROR int,
	@LeadID int,
	@ClientID int,
	@CaseID int,
	@NewCaseID int

	Select
	@LeadID = LeadEvent.LeadID,
	@ClientID = LeadEvent.ClientID,
	@CaseID = ctm.CaseID,
	@NewCaseID = ctm.NewCaseID
	from LeadEvent
	Inner Join CaseTransferMapping ctm on ctm.NewCaseID = LeadEvent.CaseID
	where leadevent.leadeventid = @LeadEventID 

	Delete matterdetailvalues
	from matterdetailvalues
	inner join matter m on m.matterid = matterdetailvalues.matterid and m.caseid = @NewCaseID
	inner join detailfields df on df.detailfieldid = matterdetailvalues.detailfieldid and df.questiontypeid = 4
	Where isnumeric(matterdetailvalues.detailvalue) = 0 and matterdetailvalues.ValueInt IS NULL
	and encryptedvalue is null

	insert into matterdetailvalues(clientid, leadid, matterid, detailfieldid, detailvalue, errormsg)
	select ctm.NewClientID, ctm.NewLeadID, ctm.NewMatterID, df1_toclient.detailfieldid, luli1_toclient.lookuplistitemid, mdv_fromclient.errormsg
	from detailfields df_fromclient
	inner join casetransfermapping ctm on ctm.caseid = @CaseID
	inner join matterdetailvalues mdv_fromclient on mdv_fromclient.detailfieldid = df_fromclient.detailfieldid and mdv_fromclient.matterid = ctm.matterid
	inner join detailfieldalias dfa_fromclient on dfa_fromclient.detailfieldid = df_fromclient.detailfieldid and df_fromclient.leadormatter = 2
	inner join detailfieldalias dfa2_toclient on dfa2_toclient.DetailFieldAlias = dfa_fromclient.DetailFieldAlias and dfa2_toclient.detailfieldid <> dfa_fromclient.detailfieldid
	inner join detailfields df1_toclient on df1_toclient.detailfieldid = dfa2_toclient.detailfieldid and df1_toclient.questiontypeid = 4 and df1_toclient.leadormatter = 2
	inner join lookuplistitems luli1_toclient on luli1_toclient.lookuplistid = df1_toclient.lookuplistid
	inner join lookuplistitems luli2_fromclient on luli2_fromclient.itemvalue = luli1_toclient.itemvalue and luli2_fromclient.lookuplistid = df_fromclient.lookuplistid and luli2_fromclient.lookuplistitemid = case df_fromclient.questiontypeid when 4 then mdv_fromclient.detailvalue else '0' end
	where df_fromclient.questiontypeid = 4
	

	insert into matterdetailvalues(clientid, leadid, matterid, detailfieldid, detailvalue, errormsg)
	select ctm.NewClientID, ctm.NewLeadID, ctm.NewMatterID, df1_toclient.detailfieldid, luli1_toclient.lookuplistitemid, ldv_fromclient.errormsg
	from detailfields df_fromclient
	inner join casetransfermapping ctm on ctm.caseid = @CaseID
	inner join leaddetailvalues ldv_fromclient on ldv_fromclient.detailfieldid = df_fromclient.detailfieldid and ldv_fromclient.leadid = ctm.leadid
	inner join detailfieldalias dfa_fromclient on dfa_fromclient.detailfieldid = df_fromclient.detailfieldid 
	inner join detailfieldalias dfa2_toclient on dfa2_toclient.DetailFieldAlias = dfa_fromclient.DetailFieldAlias and dfa2_toclient.detailfieldid <> dfa_fromclient.detailfieldid
	inner join detailfields df1_toclient on df1_toclient.detailfieldid = dfa2_toclient.detailfieldid 
	inner join lookuplistitems luli1_toclient on luli1_toclient.lookuplistid = df1_toclient.lookuplistid
	inner join lookuplistitems luli2_fromclient on luli2_fromclient.itemvalue = luli1_toclient.itemvalue and luli2_fromclient.lookuplistid = df_fromclient.lookuplistid and luli2_fromclient.lookuplistitemid = case df_fromclient.questiontypeid when 4 then ldv_fromclient.detailvalue else '0' end
	where df_fromclient.questiontypeid = 4
	and df_fromclient.leadormatter = 1
	and df1_toclient.leadormatter = 2

	insert into Leaddetailvalues(clientid, leadid, detailfieldid, detailvalue, errormsg)
	select distinct ctm.NewClientID, ctm.NewLeadID, df1_toclient.detailfieldid, luli1_toclient.lookuplistitemid, ldv_fromclient.errormsg
	from detailfields df_fromclient
	inner join casetransfermapping ctm on ctm.caseid = @CaseID
	inner join leaddetailvalues ldv_fromclient on ldv_fromclient.detailfieldid = df_fromclient.detailfieldid and ldv_fromclient.leadid = ctm.leadid
	inner join detailfieldalias dfa_fromclient on dfa_fromclient.detailfieldid = df_fromclient.detailfieldid 
	inner join detailfieldalias dfa2_toclient on dfa2_toclient.DetailFieldAlias = dfa_fromclient.DetailFieldAlias and dfa2_toclient.detailfieldid <> dfa_fromclient.detailfieldid
	inner join detailfields df1_toclient on df1_toclient.detailfieldid = dfa2_toclient.detailfieldid 
	inner join lookuplistitems luli1_toclient on luli1_toclient.lookuplistid = df1_toclient.lookuplistid
	inner join lookuplistitems luli2_fromclient on luli2_fromclient.itemvalue = luli1_toclient.itemvalue and luli2_fromclient.lookuplistid = df_fromclient.lookuplistid and luli2_fromclient.lookuplistitemid = case df_fromclient.questiontypeid when 4 then ldv_fromclient.detailvalue else '0' end
	where df_fromclient.questiontypeid = 4
	and df_fromclient.leadormatter = 1
	and df1_toclient.leadormatter = 1

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CaseTransferLuliCopy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CaseTransferLuliCopy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CaseTransferLuliCopy] TO [sp_executeall]
GO
