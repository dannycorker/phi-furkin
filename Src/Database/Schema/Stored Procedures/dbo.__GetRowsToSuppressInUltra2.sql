SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 07/12/2012
-- Description:	Gets a list of rows to supress in ultra
-- 354	RowID
-- 355	ListID
-- 356	SchemaName
-- 357	CaseID
-- 358	Terminal
-- 359	UploadedToUltraDateTime
-- 360	LastSuppressedDateTime
-- 361	LastUnsuppressedDateTime
-- 362	Error
-- 363	Suppressed
-- =============================================
CREATE PROCEDURE [dbo].[__GetRowsToSuppressInUltra2]
	@ClientID INT,
	@LeadID INT,
	@CaseID INT = NULL
AS
BEGIN

	DECLARE @CustomerID INT
	SELECT @CustomerID=CustomerID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID

	DECLARE @TableRowID INT,			
			@DetailFieldID INT,
			@354FieldID INT, -- RowID
			@355FieldID INT, -- ListID
			@356FieldID INT, -- SchemaName
			@357FieldID	INT, -- CaseID
			@363FieldID	INT  -- Suppressed
	
	DECLARE @LogEntry VARCHAR(2000)
	
	SELECT @354FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 354
	AND t.ClientID = @ClientID
	
	SELECT @355FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 355
	AND t.ClientID = @ClientID
	
	SELECT @356FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 356
	AND t.ClientID = @ClientID
	
	SELECT @357FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 357
	AND t.ClientID = @ClientID
	
	SELECT @363FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 363
	AND t.ClientID = @ClientID
	
	SELECT tdv.TableRowID, tdv.DetailValue RowID, tdv2.DetailValue Suppressed, tdv3.DetailValue ListID, tdv4.DetailValue SchemaName
	FROM dbo.TableDetailValues tdv WITH (NOLOCK) 
	INNER JOIN TableDetailValues tdv2 ON tdv2.TableRowID = tdv.TableRowID AND tdv2.DetailFieldID = @363FieldID
	INNER JOIN TableDetailValues tdv3 ON tdv3.TableRowID = tdv.TableRowID AND tdv3.DetailFieldID = @355FieldID AND tdv3.DetailValue IS NOT NULL AND tdv3.DetailValue <> ''
	INNER JOIN TableDetailValues tdv4 ON tdv4.TableRowID = tdv.TableRowID AND tdv4.DetailFieldID = @356FieldID AND tdv4.DetailValue IS NOT NULL AND tdv4.DetailValue <> ''
	INNER JOIN TableDetailValues tdv5 ON tdv5.TableRowID = tdv.TableRowID AND tdv5.DetailFieldID = @357FieldID AND tdv5.ValueInt=@CaseID
	WHERE tdv.ClientID=@ClientID AND tdv.CustomerID=@CustomerID AND tdv.DetailFieldID =@354FieldID
	ORDER BY tdv.TableRowID DESC
	
	SELECT @LogEntry = 
			'@ClientID = ' + ISNULL(CONVERT(VARCHAR,@ClientID),'NULL')
			 + '@LeadID = ' + ISNULL(CONVERT(VARCHAR,@LeadID),'NULL')
			 + '@CaseID = ' + ISNULL(CONVERT(VARCHAR,@CaseID),'NULL')
			 + + ' ' + ISNULL(CONVERT(VARCHAR,@@ROWCOUNT),'NULL') + ' rows'

	exec _C00_LogIt 'Info', '__GetRowsToSuppressInUltra2', 'TestLog', @LogEntry, 26932 /*CS*/
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[__GetRowsToSuppressInUltra2] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__GetRowsToSuppressInUltra2] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__GetRowsToSuppressInUltra2] TO [sp_executeall]
GO
