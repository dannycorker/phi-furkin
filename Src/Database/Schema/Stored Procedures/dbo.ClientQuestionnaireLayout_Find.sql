SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientQuestionnaireLayout table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnaireLayout_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientQuestionnaireLayoutID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@ClientID int   = null ,

	@PageNumber int   = null ,

	@LayoutCss varchar (MAX)  = null ,

	@LayoutCssFileName varchar (512)  = null ,

	@QuestionColumns int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientQuestionnaireLayoutID]
	, [ClientQuestionnaireID]
	, [ClientID]
	, [PageNumber]
	, [LayoutCss]
	, [LayoutCssFileName]
	, [QuestionColumns]
    FROM
	[dbo].[ClientQuestionnaireLayout] WITH (NOLOCK) 
    WHERE 
	 ([ClientQuestionnaireLayoutID] = @ClientQuestionnaireLayoutID OR @ClientQuestionnaireLayoutID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([PageNumber] = @PageNumber OR @PageNumber IS NULL)
	AND ([LayoutCss] = @LayoutCss OR @LayoutCss IS NULL)
	AND ([LayoutCssFileName] = @LayoutCssFileName OR @LayoutCssFileName IS NULL)
	AND ([QuestionColumns] = @QuestionColumns OR @QuestionColumns IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientQuestionnaireLayoutID]
	, [ClientQuestionnaireID]
	, [ClientID]
	, [PageNumber]
	, [LayoutCss]
	, [LayoutCssFileName]
	, [QuestionColumns]
    FROM
	[dbo].[ClientQuestionnaireLayout] WITH (NOLOCK) 
    WHERE 
	 ([ClientQuestionnaireLayoutID] = @ClientQuestionnaireLayoutID AND @ClientQuestionnaireLayoutID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([PageNumber] = @PageNumber AND @PageNumber is not null)
	OR ([LayoutCss] = @LayoutCss AND @LayoutCss is not null)
	OR ([LayoutCssFileName] = @LayoutCssFileName AND @LayoutCssFileName is not null)
	OR ([QuestionColumns] = @QuestionColumns AND @QuestionColumns is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaireLayout_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnaireLayout_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaireLayout_Find] TO [sp_executeall]
GO
