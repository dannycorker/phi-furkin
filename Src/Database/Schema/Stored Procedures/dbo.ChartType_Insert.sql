SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ChartType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartType_Insert]
(

	@ChartTypeID int   ,

	@ChartTypeName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[ChartType]
					(
					[ChartTypeID]
					,[ChartTypeName]
					)
				VALUES
					(
					@ChartTypeID
					,@ChartTypeName
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartType_Insert] TO [sp_executeall]
GO
