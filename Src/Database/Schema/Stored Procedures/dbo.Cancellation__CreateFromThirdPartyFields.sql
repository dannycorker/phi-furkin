SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgaan
-- Create date: 2016-09-07
-- Description:	Performs a billing cancellation.
--				Adapted from MidTermAdjustment__CreateFromThirdPartyFields but only for method 3
-- Mods
-- 2016/09/16 DCM Added setting PurchasedProductPaymentScheduleParentID
-- 2016/10/05 DCM Split out calculation of one-off payment amount to separate SP (Cancellation__CalculateAdjustmentValue)
-- 2020-09-10 ACE PPET-73    - Add Transaction Fee
-- 2021-01-22 ACE FURKIN-130 - Added enrollment fee
-- 2021-04-08 ACE Furkin-429 - Add write off, standardised formatting 
-- =============================================
CREATE PROCEDURE [dbo].[Cancellation__CreateFromThirdPartyFields]
	@ClientID INT,
	@CaseID INT,
	@LeadEventID INT,
	@Debug INT = 0
AS
BEGIN

--declare
--	@ClientID INT = 384,
--	@CaseID INT = 1321,
--	@LeadEventID INT = 20669

	SET NOCOUNT ON;

	DECLARE @CustomerID INT, 
			@LeadID INT, @MatterID INT, 
			@LeadTypeID INT, 
			@WhoCreated INT,
			@GETDATENowLocal DATETIME = dbo.fn_GetDate_Local(),
			/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
			@EnrollmentFeeRefundNet NUMERIC(18,2),
			@EnrollmentFeeRefundTax NUMERIC(18,2),
			@EnrollmentFeeRefundGross NUMERIC(18,2),
			@CoolingOffPeriod INT,
			@AccountTypeID INT,
			@DaysActive INT,
			@County VARCHAR(100)

	-- there can only be one matter per case
	SELECT TOP 1 @MatterID = m.MatterID, @LeadID = m.LeadID
	FROM Matter m WITH (NOLOCK) 
	WHERE m.CaseID = @CaseID
	
	SELECT @CustomerID = l.CustomerID, @LeadTypeID = l.LeadTypeID 
	FROM Lead l WITH (NOLOCK) 
	WHERE l.LeadID = @LeadID

	SELECT @County = c.County 
	FROM Customers c WITH (NOLOCK)
	WHERE c.CustomerID = @CustomerID

	SELECT @WhoCreated = le.WhoCreated 
	FROM LeadEvent le WITH (NOLOCK) 
	WHERE le.LeadEventID=@LeadEventID
	
	DECLARE @WhenCreated VARCHAR(10) = CONVERT(VARCHAR(10),@GETDATENowLocal,120) -- current date time for when created
	
	--4397	Description
	DECLARE @Description_DV VARCHAR(2000)
	SELECT @Description_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4397)		
	
	--4401	CancellationDate	!!!Cancellation Date
	DECLARE @CancellationDate_DV VARCHAR(2000), @IsDateTime_AdjustmentDate BIT, @CancellationDate DATETIME
	SELECT @CancellationDate_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4401)		
	SELECT @IsDateTime_AdjustmentDate = dbo.fnIsDateTime(@CancellationDate_DV)		
	IF(@IsDateTime_AdjustmentDate=1)
	BEGIN
		SELECT @CancellationDate = CAST(@CancellationDate_DV AS DATETIME)
	END

	--4410	MidTermAdjustmentCalculationMethodID
	DECLARE @MidTermAdjustmentCalculationMethodID_DV VARCHAR(2000), @IsInt_MidTermAdjustmentCalculationMethodID BIT, @MidTermAdjustmentCalculationMethodID INT, @ItemID_MidTermAdjustmentCalculationMethodID INT, @MidTermAdjustmentCalculationMethodID_ItemValue VARCHAR(500)
	SELECT @MidTermAdjustmentCalculationMethodID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4410)	
	SELECT @IsInt_MidTermAdjustmentCalculationMethodID = dbo.fnIsInt(@MidTermAdjustmentCalculationMethodID_DV)		
	IF(@IsInt_MidTermAdjustmentCalculationMethodID=1)
	BEGIN
		SELECT @ItemID_MidTermAdjustmentCalculationMethodID = CAST(@MidTermAdjustmentCalculationMethodID_DV AS INT) -- this is the lookuplist item id that has to be referenced back to an actual mid term adjustment calculation method
		SELECT @MidTermAdjustmentCalculationMethodID_ItemValue = llItem.ItemValue FROM LookupListItems llItem WITH (NOLOCK) WHERE LookupListItemID = @ItemID_MidTermAdjustmentCalculationMethodID
		SELECT @MidTermAdjustmentCalculationMethodID = mtam.MidTermAdjustmentCalculationMethodID FROM MidTermAdjustmentCalculationMethod mtam WITH (NOLOCK) WHERE mtam.Method = @MidTermAdjustmentCalculationMethodID_ItemValue
	END		
	
	--4413	PurchasedProductID
	DECLARE @AccountID INT
	DECLARE @PurchasedProductID_DV VARCHAR(2000), @IsInt_PurchasedProductID BIT, @PurchasedProductID INT
	SELECT @PurchasedProductID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4413)	
	SELECT @IsInt_PurchasedProductID = dbo.fnIsInt(@PurchasedProductID_DV)		
	IF(@IsInt_PurchasedProductID=1)
	BEGIN

		SELECT @PurchasedProductID = CAST(@PurchasedProductID_DV AS INT)	
		SELECT @AccountID=AccountID 
		FROM PurchasedProduct WITH (NOLOCK) 
		WHERE PurchasedProductID=@PurchasedProductID

	END	
	
	/*Pick up the ClientAccount Record*/ 
	DECLARE @ClientAccountID INT 
	SELECT @ClientAccountID = p.ClientAccountID 
	FROM PurchasedProduct p WITH (NOLOCK) 
	WHERE p.PurchasedProductID = @PurchasedProductID
	
	SELECT @AccountTypeID = act.AccountTypeID 
	FROM Account act WITH (NOLOCK)
	WHERE act.AccountID = @AccountID

	DECLARE @DifferenceInMonthlyPremium NUMERIC(18,2),
			@DifferenceInMonthlyPremiumVAT NUMERIC(18,2),
			@CoverFrom DATETIME,
			@CoverTo DATETIME,
			@CancellationCoverPeriodGross NUMERIC(18,2),
			@WithinAdjustmentDatePaymentVAT NUMERIC(18,2),
			@NumberOfDaysInPeriod INT,
			@NumberOfDaysLeftInPeriod INT,
			@OneOffPaymentForCancellationPeriod NUMERIC(18,2),
			@OneOffPaymentForCancellationPeriodVAT NUMERIC(18,2),
			@PaymentForRemainderOfSchedule NUMERIC(18,2),
			@PaymentNetForRemainderOfSchedule NUMERIC(18,2),
			@PaymentVATForRemainderOfSchedule NUMERIC(18,2),
			@PurchasedProductPaymentScheduleID INT,
			@OneOffPaymentGross NUMERIC(18,2),
			@OneOffPaymentNet NUMERIC(18,2),
			@OneOffPaymentVAT NUMERIC(18,2),
			@NewCoverTo DATETIME,
			@NewCoverFrom DATETIME,
			@NewPaymentDate DATETIME
	

	DECLARE @Adjustment TABLE ( AccountID INT, 
								PurchasedProductID INT, 
								NewCoverFrom DATE, 
								NewCoverTo DATE, 
								NewPaymentDate DATE, 
								OneOffPaymentNET MONEY, 
								OneOffPaymentVAT MONEY, 
								OneOffPaymentGross MONEY,
								TransactionFee NUMERIC(18,2),
								WriteOffNet NUMERIC(18,2),
								WriteOffTax NUMERIC(18,2),
								WriteOffGross NUMERIC(18,2)
								)

	/* Procedure 
	
		1. Calculate refund
		2. Set all existing non-paid rows to Ignored
		3. Add refund payment into the schedule
		
		or in more detail

		For payments already taken, pro-rata the cancellation adjustment for the period since cancellation (but only for the current live policy)  
		and make a single refund for that adjustment value, 
		and set all uncollected premiums to ignored
		and adjust the amount in the customer payment schedule records (if this has become zero, set this payment status to ignored)
		This logic should work for all increments (including single increments, i.e. annual payers)
			
	*/

	EXEC dbo._C00_LogIt 'Proc', 'Cancellation__CreateFromThirdPartyFields', '@MidTermAdjustmentCalculationMethodID', @MidTermAdjustmentCalculationMethodID, @WhoCreated
	
	IF(@MidTermAdjustmentCalculationMethodID = 3) --Collect/Refund in single payment
	BEGIN
	
		-- this is the cancellation date
		/*If we have already done the cancellation thing then we dont really need to do it again*/
		IF (dbo.fnIsDateTime(@CancellationDate_DV) = 1) 
		AND NOT EXISTS (
			SELECT *
			FROM PurchasedProductPaymentSchedule WITH (NOLOCK)
			WHERE CoverTo >= @CancellationDate
			AND PaymentStatusID = 1 
			AND PurchasedProductID = @PurchasedProductID
			AND PurchasedProductPaymentScheduleTypeID = 7
			)
		BEGIN

			EXEC dbo._C00_LogIt 'Proc', 'Cancellation__CreateFromThirdPartyFields', 'Calc Adjustment', '', @WhoCreated

			INSERT INTO @Adjustment
			EXEC dbo.Cancellation__CalculateAdjustmentValue @ClientID, @CaseID, @LeadEventID, @WhoCreated

			-- Set all PPPS records including or beyond the period containing the cancellation to ignore
			-- NB if the cancellation is during a period that hasn't yet been collected, a collection of the 
			-- pro-rated amount due will be made
			
			/*Dont set the cancellation adjustment to ignored*/
			UPDATE PurchasedProductPaymentSchedule
			SET PaymentStatusID = 3
			WHERE CoverTo > @CancellationDate
			AND PaymentStatusID =  1 
			AND PurchasedProductID = @PurchasedProductID
			AND PurchasedProductPaymentScheduleTypeID <> 7
			
			-- Now deal with the Admin Fee, but only if it hasnt been paid 
			UPDATE PurchasedProductPaymentSchedule
			SET PaymentStatusID = 3 
			WHERE PurchasedProductPaymentScheduleTypeID = 5
			AND PaymentStatusID = 1 
			AND PurchasedProductID = @PurchasedProductID
			
			IF 0 <> ( 
				SELECT ISNULL(a.OneOffPaymentGross,0) 
				FROM @Adjustment a 
			) 
			BEGIN

				SELECT @PurchasedProductID = PurchasedProductID, @CoverFrom = NewCoverFrom, @CoverTo = NewCoverTo, 
						@OneOffPaymentNet = OneOffPaymentNET, @OneOffPaymentVAT = OneOffPaymentVAT, @OneOffPaymentGross = OneOffPaymentGross
				FROM @Adjustment a

				PRINT 'PPID'
				PRINT @PurchasedProductID

				/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
				SELECT @CoolingOffPeriod = b.CoolingOffPeriod
				FROM BillingConfiguration b WITH (NOLOCK)
				WHERE b.ClientID = @ClientID

				/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
				SELECT @DaysActive = DATEDIFF(DD, @CancellationDate, CONVERT(DATE, @GETDATENowLocal))

				PRINT '@CoolingOffPeriod'
				PRINT @CoolingOffPeriod

				PRINT '@CancellationDate'
				PRINT @CancellationDate

				/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
				IF EXISTS (
					SELECT *
					FROM PurchasedProduct pp WITH (NOLOCK)
					WHERE pp.PurchasedProductID = @PurchasedProductID 
					AND pp.ValidFrom = @CancellationDate
				) AND @DaysActive <= @CoolingOffPeriod
				BEGIN

					EXEC dbo._C00_LogIt 'Canc', 'Cancellation__CreateFromThirdPartyFields', 'PP', 'ValidFrom=To', @WhoCreated

					/*
						ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee
						If the start date and end date are the same (cancel to inception) and the 
						number of days from the cover from to today is less than the cooling off period
						sum the enrollment fee(s). Summing here so any refunded ones are contra'd off..
					*/
					SELECT	@EnrollmentFeeRefundNet = SUM(s.PaymentNet)*-1.00,
							@EnrollmentFeeRefundTax = SUM(s.PaymentVat)*-1.00,
							@EnrollmentFeeRefundGross = SUM(s.PaymentGross)*-1.00
					FROM PurchasedProductPaymentSchedule s WITH (NOLOCK)
					WHERE s.PurchasedProductID = @PurchasedProductID
					AND s.PurchasedProductPaymentScheduleTypeID = 5 /*Transaction Fee/Enrollment Fee*/
					AND s.PaymentStatusID = 6 /*Paid only!*/

				END

				PRINT '@AccountTypeID'
				PRINT @AccountTypeID

				/*ALM 2021-04-09 FURKIN-448*/
				IF @AccountTypeID = 2 AND @OneOffPaymentGross < 0.00 /*Card*/
				BEGIN

					/*2020-09-15 ACE call transactional refund proc..*/
					EXEC Billing__CreateTransactionalRefunds @CustomerID, @MatterID, @PurchasedProductID, @OneOffPaymentNet, @OneOffPaymentVAT, @OneOffPaymentGross,
									1 /*Premium*/, @CoverFrom, @CoverTo, @GETDATENowLocal, 7, @WhoCreated, @Debug, NULL, 0.00, 'Cancellation__CreateFromThirdPartyFields_1'

					EXEC dbo._C00_LogIt 'Canc', 'Cancellation__CreateFromThirdPartyFields', 'Type', 'Card', @WhoCreated
					EXEC dbo._C00_LogIt 'Canc', 'Cancellation__CreateFromThirdPartyFields', '@EnrollmentFeeRefundGross', @EnrollmentFeeRefundGross, @WhoCreated

					PRINT '@EnrollmentFeeRefundGross'
					PRINT @EnrollmentFeeRefundGross

					/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
					IF @EnrollmentFeeRefundGross <> 0.00
					BEGIN

						/*Make sure we havent queued this up already*/
						IF NOT EXISTS (
							SELECT *
							FROM PurchasedProductPaymentSchedule s WITH (NOLOCK)
							WHERE s.PurchasedProductID = @PurchasedProductID
							AND s.PurchasedProductPaymentScheduleTypeID = 5 /*Transaction Fee/Enrollment Fee*/
							AND s.PaymentStatusID = 1 /*New*/
						)
						BEGIN

							EXEC dbo._C00_LogIt 'Canc', 'Cancellation__CreateFromThirdPartyFields', 'PurchasedProductPaymentSchedule', 'NoNew', @WhoCreated
		
							PRINT 'Create Enrollment Fee Refund'

							/*2021-01-22 ACE call transactional refund proc..*/
							EXEC Billing__CreateTransactionalRefunds @CustomerID, @MatterID, @PurchasedProductID, @EnrollmentFeeRefundNet, @EnrollmentFeeRefundTax, @EnrollmentFeeRefundGross,
											1 /*Premium*/, @CoverFrom, @CoverTo, @GETDATENowLocal, 5, @WhoCreated, @Debug, NULL, 0.00, 'Cancellation__CreateFromThirdPartyFields_2'

						END

					END
					/*End add enrollment fee*/

				END
				ELSE
				BEGIN

					-- Insert the one-off adjustment record into PPPS with the payment date of the period containing the cancellation 
					INSERT INTO PurchasedProductPaymentSchedule(ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, PurchasedProductPaymentScheduleTypeID, ClientAccountID, TransactionFee)
					SELECT @ClientID,@CustomerID,AccountID,PurchasedProductID, NewPaymentDate, NewCoverFrom, NewCoverTo, NewPaymentDate, OneOffPaymentNET, OneOffPaymentVAT, OneOffPaymentGross, 1, NULL, NULL, NULL, @WhoCreated, @WhenCreated, 7, @ClientAccountID, a.TransactionFee
					FROM @Adjustment a

					SELECT @PurchasedProductPaymentScheduleID = SCOPE_IDENTITY()
				
					UPDATE PurchasedProductPaymentSchedule
					SET PurchasedProductPaymentScheduleParentID = @PurchasedProductPaymentScheduleID
					WHERE PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

					/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
					IF @EnrollmentFeeRefundGross <> 0.00
					BEGIN

						INSERT INTO PurchasedProductPaymentSchedule(ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, 
								PaymentNet, PaymentVAT, PaymentGross, 
								PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, 
								PurchasedProductPaymentScheduleTypeID, ClientAccountID, TransactionFee)
						SELECT @ClientID,@CustomerID, @AccountID, @PurchasedProductID, @GETDATENowLocal, @NewCoverFrom, @NewCoverTo, @GETDATENowLocal, 
								@EnrollmentFeeRefundNet, @EnrollmentFeeRefundTax, @EnrollmentFeeRefundGross, 
								1, NULL, NULL, NULL, @WhoCreated, @WhenCreated, 
								5, @ClientAccountID, 0.00

						SELECT @PurchasedProductPaymentScheduleID = SCOPE_IDENTITY()
				
						UPDATE PurchasedProductPaymentSchedule
						SET PurchasedProductPaymentScheduleParentID = @PurchasedProductPaymentScheduleID
						WHERE PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

					END
					/*End add enrollment fee*/

					-- rebuild the CPS
					EXEC Billing__RebuildCustomerPaymentSchedule @AccountID, @NewPaymentDate, @WhoCreated
						
				END

				SELECT @NewPaymentDate = NewPaymentDate 
				FROM @Adjustment

			END
			
		END		
	
		-- 2020-02-13 CPS for JIRA AAG-28 | Update adjustment amounts
		EXEC WrittenPremium_UpdateAdjustment @ClientID, @CaseID, @LeadEventID

	END 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Cancellation__CreateFromThirdPartyFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Cancellation__CreateFromThirdPartyFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Cancellation__CreateFromThirdPartyFields] TO [sp_executeall]
GO
