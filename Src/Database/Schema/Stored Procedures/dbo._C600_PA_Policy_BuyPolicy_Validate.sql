SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Ian Slack
-- Create date: 2014-09-24
-- Description:	Validate BuyPolicy Request
-- Modified   : 28/11/2014
-- By         : Jan Wilson
-- Comments   : Remove requirement for home phone number    
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Policy_BuyPolicy_Validate]
(
	@XmlRequest XML,
	@XmlResponse XML OUTPUT,
	@Result INT OUTPUT
)
AS
BEGIN

	DECLARE @ErrorMessages TABLE (OrderID INT, Code VARCHAR(50), Message VARCHAR(2000))

	IF NOT EXISTS (SELECT * FROM fn_C00_1273_CustomerInfo_ShredXML(@XmlRequest))
	BEGIN
		INSERT INTO @ErrorMessages
		SELECT	1, 'VM_101', 'Customer is required'
	END
	
	;WITH SherdXml AS
	(
		SELECT *
		FROM fn_C00_1273_CustomerInfo_ShredXML(@XmlRequest)
	)
	INSERT INTO @ErrorMessages
	SELECT	1, 'VM_102', 'Customer Title is required'
	FROM	SherdXml
	WHERE	NOT (TitleId > 0) 
		UNION ALL
	SELECT	1, 'VM_103', 'Customer FirstName is required'
	FROM	SherdXml
	WHERE	ISNULL(FirstName,'') = ''
		UNION ALL
	SELECT	1, 'VM_104', 'Customer LastName is required'
	FROM	SherdXml
	WHERE	ISNULL(LastName,'') = ''
		UNION ALL
	--SELECT	1, 'VM_105', 'Customer HomePhone is required'
	--FROM	SherdXml
	--WHERE	ISNULL(HomePhone,'') = ''
	--	UNION ALL
	SELECT	1, 'VM_106', 'Customer Email is required'
	FROM	SherdXml
	WHERE	ISNULL(Email,'') = ''
		UNION ALL
	SELECT	1, 'VM_107', 'Customer Address1 is required'
	FROM	SherdXml
	WHERE	ISNULL(Address1,'') = ''
		UNION ALL
	SELECT	1, 'VM_108', 'Customer TownCity is required'
	FROM	SherdXml
	WHERE	ISNULL(TownCity,'') = ''
		UNION ALL
	SELECT	1, 'VM_109', 'Customer Postcode is required'
	FROM	SherdXml
	WHERE	ISNULL(Postcode,'') = ''
	

	IF NOT EXISTS (SELECT * FROM fn_C00_1273_PetInfo_ShredXML(@XmlRequest))
	BEGIN
		INSERT INTO @ErrorMessages
		SELECT	1, 'VM_201', 'Pet is required'
	END
	
	;WITH SherdXml AS
	(
		SELECT *
		FROM fn_C00_1273_PetInfo_ShredXML(@XmlRequest)
	)
	INSERT INTO @ErrorMessages
	SELECT	RowID, 'VM_202', 'Pet Species / PetType is required'
	FROM	SherdXml
	WHERE	NOT(SpeciesId > 0)
		UNION ALL
	SELECT	RowID, 'VM_203', 'Pet Breed is required'
	FROM	SherdXml
	WHERE	NOT(BreedId > 0)
		UNION ALL
	SELECT	RowID, 'VM_204', 'Pet Sex is required'
	FROM	SherdXml
	WHERE	NOT (ISNULL(Gender,'') IN('M','F'))
		UNION ALL
	SELECT	RowID, 'VM_205', 'Pet Birth Date is required'
	FROM	SherdXml
	WHERE	BirthDate NOT BETWEEN DATEADD(year, -30, dbo.fn_GetDate_Local()) AND dbo.fn_GetDate_Local()


	IF NOT EXISTS (SELECT * FROM fn_C00_1273_PaymentInfo_ShredXML(@XmlRequest) WHERE PaymentMethodTypeId > 0)
	BEGIN
		INSERT INTO @ErrorMessages
		SELECT	1, 'VM_301', 'Payment is required'
	END
	

	
	IF EXISTS (SELECT * FROM fn_C00_1273_PaymentInfo_ShredXML(@XmlRequest) WHERE PaymentMethodTypeId = 69930)
	BEGIN
	
		;WITH SherdXml AS
		(
			SELECT *
			FROM fn_C00_1273_PaymentInfo_ShredXML(@XmlRequest)
		)
		INSERT INTO @ErrorMessages
		SELECT	RowID, 'VM_302', 'Payment Payment Day is required'
		FROM	SherdXml
		WHERE	NOT(PaymentDay > 0)
			UNION ALL
		SELECT	RowID, 'VM_303', 'Payment Payment Method is required'
		FROM	SherdXml
		WHERE	NOT(PaymentMethodTypeId > 0)
			UNION ALL
		SELECT	RowID, 'VM_304', 'Payment Payment Interval is required'
		FROM	SherdXml
		WHERE	NOT(PaymentIntervalId > 0)
	
		;WITH SherdXml AS
		(
			SELECT *
			FROM fn_C00_1273_PaymentInfo_ShredXML(@XmlRequest)
		)
		INSERT INTO @ErrorMessages
		SELECT	RowID, 'VM_305', 'Payment Sort Code is required'
		FROM	SherdXml
		WHERE	ISNULL(SortCode,'') = ''
			UNION ALL
		SELECT	RowID, 'VM_306', 'Payment Account Number is required'
		FROM	SherdXml
		WHERE	ISNULL(AccountNumber,'') = ''
			UNION ALL
		SELECT	RowID, 'VM_307', 'Payment Bank Name is required'
		FROM	SherdXml
		WHERE	ISNULL(BankName,'') = ''
			UNION ALL
		SELECT	RowID, 'VM_308', 'Payment BankAccount Name is required'
		FROM	SherdXml
		WHERE	ISNULL(BankAccountName,'') = ''
		
	END
	
	/*
	IF EXISTS (SELECT * FROM fn_C00_1273_PaymentInfo_ShredXML(@XmlRequest) WHERE PaymentMethodTypeId = 69931)
	BEGIN
	
		;WITH SherdXml AS
		(
			SELECT *
			FROM fn_C00_1273_PaymentInfo_ShredXML(@XmlRequest)
		)
		INSERT INTO @ErrorMessages
		SELECT	RowID, 'VM_309', 'Payment Bank Name is required'
		FROM	SherdXml
		WHERE	ISNULL(BankName,'') = ''
			UNION ALL
		SELECT	RowID, 'VM_310', 'Payment Credit Card Token is required'
		FROM	SherdXml
		WHERE	ISNULL(CreditCardToken,'') = ''
			UNION ALL
		SELECT	RowID, 'VM_311', 'Payment Credit CardHolder Name is required'
		FROM	SherdXml
		WHERE	ISNULL(CreditCardHolderName,'') = ''
			UNION ALL
		SELECT	RowID, 'VM_312', 'Payment CreditCard Number Masked is required'
		FROM	SherdXml
		WHERE	ISNULL(CreditCardNumberMasked,'') = ''
			UNION ALL
		SELECT	RowID, 'VM_313', 'Payment Credit Card Expire Date is required'
		FROM	SherdXml
		WHERE	ISNULL(CreditCardExpireDate,'') = ''
		
	END
	*/
	
	IF EXISTS (SELECT * FROM @ErrorMessages)
	BEGIN 
		SELECT @Result = 0
	END
	ELSE
	BEGIN
		SELECT @Result = 1
	END
	
	SELECT @XmlResponse = 
	(
		SELECT
		(
			SELECT	Code, Message
			FROM	@ErrorMessages MessageList
			ORDER BY OrderID, Code
			FOR XML PATH ('ApiMessage'), ROOT ('MessageList'), TYPE
		),
		@Result Result
		FOR XML PATH ('BuyPolicyResponse')
	)
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_BuyPolicy_Validate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_BuyPolicy_Validate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_BuyPolicy_Validate] TO [sp_executeall]
GO
