SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-20
-- Description:	Get Equation tasks for the scheduler
-- =============================================
CREATE PROCEDURE [dbo].[EquationTasks__GetEquationTasksInOrder]

AS
BEGIN

	SET NOCOUNT ON;

	WITH Task AS (
		SELECT TOP (100) e.EquationTaskID
		FROM EquationTasks e WITH (HOLDLOCK)
		WHERE e.LockDateTime IS NULL
		AND e.WhenCompleted IS NULL
		ORDER BY e.EquationTaskID
	)

	UPDATE EquationTasks
	SET LockDateTime = dbo.fn_GetDate_Local()
	OUTPUT 'true' AS [RecalculateMatterEquations], inserted.EquationTaskID, inserted.ClientID, inserted.CustomerID, inserted.LeadID, inserted.CaseID, inserted.MatterID, inserted.RunAsUserID AS [ClientPersonnelID], inserted.ContactID, inserted.LeadTypeID, 'true' AS [CalculateTableRows], 0 AS [EventTypeID], inserted.DetailFieldID AS [DirtyFields]
	FROM EquationTasks e WITH (HOLDLOCK)
	INNER JOIN Task t ON t.EquationTaskID = e.EquationTaskID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTasks__GetEquationTasksInOrder] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EquationTasks__GetEquationTasksInOrder] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTasks__GetEquationTasksInOrder] TO [sp_executeall]
GO
