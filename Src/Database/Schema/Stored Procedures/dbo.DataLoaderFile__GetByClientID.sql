SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2008-12-05
-- Description:	Get DataLoaderFile information by Client (and other options)
-- =============================================
CREATE PROCEDURE [dbo].[DataLoaderFile__GetByClientID]
	
	@ClientID int, 
	@FileStatusID int = null,           -- Waiting in line, processing right now, passed, failed
	@LocationName varchar(20) = null,   -- 'Inbox', 'Archive', 'Samples', 'Passed', 'Failed' folder
	@ScheduledDateFrom datetime = null,
	@ScheduledDateTo datetime = null,
	@DataLoadedDateFrom datetime = null,
	@DataLoadedDateTo datetime = null,
	@FileFormatID int = null
AS
BEGIN

	SET NOCOUNT ON;

	SELECT dlf.DataLoaderFileID,
	dlf.ClientID,
	dlf.DataLoaderMapID,
	dlf.FileStatusID,
	dlf.SourceFileNameAndPath,
	dlf.TargetFileName,
	dlf.TargetFileLocation,
	dlf.FileFormatID,
	dlf.ScheduledDateTime,
	dlf.DataLoadedDateTime,
	dlf.UploadedBy,
	dlf.RowsInFile
	FROM dbo.DataLoaderFile dlf 
	WHERE dlf.ClientID = @ClientID 
	AND (@FileStatusID IS NULL OR dlf.FileStatusID = @FileStatusID)
	AND (@FileFormatID IS NULL OR dlf.FileFormatID = @FileFormatID)
	AND (@LocationName IS NULL OR dlf.TargetFileLocation = @LocationName)
	AND (@ScheduledDateFrom IS NULL OR dlf.ScheduledDateTime >= @ScheduledDateFrom)
	AND (@ScheduledDateTo IS NULL OR dlf.ScheduledDateTime <= @ScheduledDateTo)
	AND (@DataLoadedDateFrom IS NULL OR dlf.DataLoadedDateTime >= @DataLoadedDateFrom)
	AND (@DataLoadedDateTo IS NULL OR dlf.DataLoadedDateTime <= @DataLoadedDateTo)
	ORDER BY dlf.ScheduledDateTime 

	SELECT @@ROWCOUNT

END


GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile__GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFile__GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile__GetByClientID] TO [sp_executeall]
GO
