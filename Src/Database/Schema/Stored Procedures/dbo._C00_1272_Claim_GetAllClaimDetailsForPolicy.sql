SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-02-15
-- Description:	Gets all the claim details rows from all matters in a lead
-- Used by:		DF 145688
-- TODO:		CHANGED THE PAID FIELD FROM 144354 TO 144362 TO USE APPROVED DATE UNTIL WE GET THE PAID DATA FROM SAP
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_GetAllClaimDetailsForPolicy] 
(
	@LeadID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT * FROM (
		SELECT r.MatterID, r.TableRowID, 
		CASE 
			WHEN ISNULL(rlDf.DetailFieldID, df.DetailFieldID) IN (144362) THEN CONVERT(VARCHAR, tdv.ValueDate, 120)
			ELSE ISNULL(ll.ItemValue, tdv.DetailValue)
		END AS DetailValue,
		CASE ISNULL(rlDf.DetailFieldID, df.DetailFieldID)
			WHEN 144362 THEN 'Paid'
			WHEN 146189 THEN 'Section'
			WHEN 146190 THEN 'SubSection'
			WHEN 144352 THEN 'Total'
			WHEN 145679 THEN 'Payee'
		END AS FieldCaption
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN Matter m ON r.MatterID = m.MatterID 
		INNER JOIN dbo.Cases ca WITH (NOLOCK) ON m.CaseID = ca.CaseID
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON r.TableRowID = tdv.TableRowID 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON tdv.DetailFieldID = df.DetailFieldID 
		LEFT JOIN dbo.ResourceListDetailValues rdv WITH (NOLOCK) ON tdv.ResourceListID = rdv.ResourceListID AND rdv.DetailFieldID IN (146189, 146190)
		LEFT JOIN dbo.DetailFields rlDf WITH (NOLOCK) ON rdv.DetailFieldID = rlDf.DetailFieldID 
		LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON rdv.ValueInt = ll.LookupListItemID AND rlDf.LookupListID = ll.LookupListID AND rlDf.QuestionTypeID IN (2, 4)
		WHERE r.DetailFieldID = 144355
		AND r.DetailFieldPageID = 16157
		AND (ca.ClientStatusID IS NULL OR ca.ClientStatusID NOT IN (3819)) -- Exclude rejected claims
		AND m.LeadID = @LeadID) src
	PIVOT (MAX(DetailValue) FOR FieldCaption IN (Paid, Section, SubSection, Total, Payee)) AS pvt
	WHERE CAST(Total AS MONEY) > 0
	AND Paid IS NOT NULL
	ORDER BY Paid DESC


	SELECT m.MatterID, r.ParentID, rdvAilment1.DetailValue AS Ailment1, rdvAilment2.DetailValue AS Ailment2
	FROM dbo.Matter m WITH (NOLOCK) 
	INNER JOIN dbo.fn_C00_1272_GetClaimRelationships(@LeadID) r ON m.MatterID = r.ClaimID 
	LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND mdv.DetailFieldID = 144504
	LEFT JOIN dbo.ResourceListDetailValues rdvAilment1 WITH (NOLOCK) ON rdvAilment1.ResourceListID = mdv.ValueInt AND rdvAilment1.DetailFieldID = 144340
	LEFT JOIN dbo.ResourceListDetailValues rdvAilment2 WITH (NOLOCK) ON rdvAilment2.ResourceListID = mdv.ValueInt AND rdvAilment2.DetailFieldID = 144341
	

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetAllClaimDetailsForPolicy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_GetAllClaimDetailsForPolicy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetAllClaimDetailsForPolicy] TO [sp_executeall]
GO
