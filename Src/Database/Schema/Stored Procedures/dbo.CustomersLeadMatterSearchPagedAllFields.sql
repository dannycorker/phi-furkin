SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: 08 May 2008

-- Created By:   Jim Green
-- Description:  Gets records from the Customers, Lead and Matter tables passing page index and page count params.
--               Also allows users to search for LeadDetailValues and MatterDetailValues.
--               JWG 2010-07-14 CanViewUnassignedLeads & CanViewLeadsAssignedToOthers 
--               JWG 2010-07-31 Replace vLDV/vMDV with LDV/MDV and LULI if required. 
--               JWG 2013-02-11 Include Partner details in searches 
--               JWG 2013-11-01 Globalisation of terms like town, county, postcode 
--               JWG 2014-05-13 #26507 LatestRecordsFirst 
--				 ALM/GRP 2020-09-28 SDPRU-51
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[CustomersLeadMatterSearchPagedAllFields] 
(
	@UserID int,
	@WhereClause varchar (2000) = '',
	@SortCol varchar (250) = '',
	@SortOrder varchar (10) = '',
	@PageIndex int = 0,
	@PageSize int = 25,
	@ShowBusiness bit = 0, 
	@ShowIndividual bit = 1, 
	@ShowContact bit = 0, 
	@ShowTests bit = 0,
	@ShowCustomerOnly tinyint = 0, 
	@V1DetailFieldID int = NULL, 
	@V2DetailFieldID int = NULL, 
	@V3DetailFieldID int = NULL,
	@V1DetailValue varchar(250) = NULL, 
	@V2DetailValue varchar(250) = NULL, 
	@V3DetailValue varchar(250) = NULL,
	@V1LeadOrMatter char(1) = NULL, 
	@V2LeadOrMatter char(1) = NULL, 
	@V3LeadOrMatter char(1) = NULL,
	@LeadsBelongToOffice bit = 0,
	@ClientOfficeID int = NULL, 
	@CanViewUnassignedLeads bit = 1, 
	@CanViewLeadsAssignedToOthers bit = 1
)
AS
BEGIN
	DECLARE @PageLowerBound int,
	@PageUpperBound int,
	@LeadOrMatterTable varchar(15) = NULL


	-- Set the page bounds
	SELECT @PageLowerBound = (@PageSize * @PageIndex) + 1
	SELECT @PageUpperBound = (@PageIndex + 1) * @PageSize
	
	-- This section will be dynamic at runtime
	--SELECT @V1DetailFieldID = 4833, @V1DetailValue = 'Legal Serv%', @V1LeadOrMatter = 'L'
	--SELECT @V2DetailFieldID = 4835, @V2DetailValue = 'FS%', @V2LeadOrMatter = 'L'
	--SELECT @V3DetailFieldID = 85382, @V3DetailValue = 'G%', @V3LeadOrMatter = 'L'

	DECLARE @SelectPart1 varchar(max), 
	@SelectPart1Contacts varchar(max), 
	@SelectPart2Contacts varchar(max), 
	@SelectWhere varchar(max), 
	@SelectWhereContacts varchar(max), 
	@SelectPart2 varchar(max), 
	@SelectFull varchar(max), 
	@SelectFullContacts varchar(max), 
	@SpecialCols varchar(max), 
	@SpecialJoins varchar(max), 
	@SortColContacts varchar (250),
	@SortColContactsLen tinyint,
	@SortColContactsDotPos tinyint,
	@LeadBelongsToOfficeJoin varchar(250),
	@LeadBelongsToOfficeWhere varchar(250),
	@CustomersTestOrNot varchar(100),
	@NoDuplicatesClause varchar(200) = '', 
	@NoDuplicatesContactsClause varchar(200) = '', 
	@LatestRecordsFirst BIT = 0, 
	@ClientID INT,
	@Locale	VARCHAR(5),
	@CountryID	INT

	/*ALM/GPR 2020-09-28 SDPRU-51*/
	SELECT @ClientID = [dbo].[fnGetPrimaryClientID]()

	SELECT @CountryID = (SELECT CountryID FROM Clients WITH (NOLOCK) WHERE ClientID = @ClientID)
	
	/* JWG 2014-05-13 #26507 LatestRecordsFirst */
	SELECT TOP 1 @LatestRecordsFirst = ISNULL(cl.LatestRecordsFirst, 0)
	FROM dbo.ClientOption cl WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientID = cl.ClientID
	WHERE cp.ClientPersonnelID = @UserID
	
	IF @SortCol = ''
	BEGIN
		IF @LatestRecordsFirst = 1 
		BEGIN
			SELECT @SortOrder = 'DESC'
			SELECT @SortCol = 'Customers.[CustomerID] DESC, Cases.[LeadID] DESC, Cases.[CaseID] DESC, Matter.[RefLetter] ' /* Miss off the final DESC because the search builder adds it in later */
		END
		ELSE
		BEGIN
			SELECT @SortCol = 'Customers.[CustomerID], Cases.[LeadID], Cases.[CaseID], Matter.[RefLetter]'
		END
	END
	
	/* JWG 2010-04-15 New option to show just 1 Matter per Case / Case per Lead / Lead per Customer */
	SELECT @NoDuplicatesClause = CASE @ShowCustomerOnly 
							WHEN 1 THEN 'ROW_NUMBER() OVER(PARTITION BY Customers.[CustomerID] ORDER BY Matter.MatterID)' 
							WHEN 2 THEN 'ROW_NUMBER() OVER(PARTITION BY Lead.[LeadID] ORDER BY Matter.MatterID)' 
							WHEN 3 THEN 'ROW_NUMBER() OVER(PARTITION BY Cases.[CaseID] ORDER BY Matter.MatterID)' 
							ELSE '1' 
							END

	SELECT @NoDuplicatesContactsClause = CASE @ShowCustomerOnly 
							WHEN 1 THEN 'ROW_NUMBER() OVER(PARTITION BY [CustomerID] ORDER BY [MatterID])' 
							WHEN 2 THEN 'ROW_NUMBER() OVER(PARTITION BY [LeadID] ORDER BY [MatterID])' 
							WHEN 3 THEN 'ROW_NUMBER() OVER(PARTITION BY [CaseID] ORDER BY [MatterID])' 
							ELSE '1' 
							END 
	
	
	SELECT @SpecialCols = '', 
	@SpecialJoins = '', 
	@SelectWhere = ' WHERE Customers.[AquariumStatusID] IN (2,4,5) ',
	@SelectWhereContacts = ' WHERE Customers.[AquariumStatusID] IN (2,4,5) '
	
	/* JWG 2013-11-01 Globalisation of terms like town, county, postcode */
	/* This needs a TVF really so this code doesn't get repeated anywhere else, but the demo is in a couple of hours... */
	DECLARE @TownLabel VARCHAR(30) = 'Town', 
	@CountyLabel VARCHAR(30) = 'County', 
	@PostcodeLabel VARCHAR(30) = 'Post Code'

	SET @CountryID = 232 /*ALM/GPR Declared further up, so altered to a SET*/
	
	SELECT @CountryID = co.CountryID 
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	INNER JOIN dbo.ClientOffices co WITH (NOLOCK) ON co.ClientOfficeID = cp.ClientOfficeID 
	WHERE cp.ClientPersonnelID = @UserID 
	
	/* Canada */
	IF @CountryID = 39
	BEGIN
		SELECT @TownLabel = 'City', @CountyLabel = 'Province', @PostcodeLabel = 'Zip'
	END
	/* Ireland */
	IF @CountryID = 105
	BEGIN
		SELECT @PostcodeLabel = 'Country'
	END
	/* USA */
	IF @CountryID = 233
	BEGIN
		SELECT @TownLabel = 'City', @CountyLabel = 'State', @PostcodeLabel = 'Zip'
	END
	
	
	IF LEN(@WhereClause) > 0
	BEGIN
		-- Now add the original "where" criteria to the original base "where" settings 
		SET @SelectWhere = @SelectWhere + ' AND ' + @WhereClause

		-- Setup the Contacts where clause a bit differently, replacing all subsequent 
		-- references to "Customers" with "Contact".
		/* 
			JWG 2013-02-12 
			The app calls the proc like this: EXEC [dbo].[CustomersLeadMatterSearchPagedAllFields] 	@UserID = 93, 	@WhereClause = 'Customers.ClientID = 4 AND (Customers.LastName LIKE ''GREEN-PartnerTest%'' OR Partner.LastName LIKE ''GREEN-PartnerTest%'') AND (Customers.EmailAddress LIKE ''jim.green@aquarium-software.com%'' OR Partner.EmailAddress LIKE ''jim.green@aquarium-software.com%'')', @ShowContact=1
			The replacements for the contact search go like this:
			1) Replace "Customers." with "Contact." throughout the where clause, which works for almost all fields
			2) Set "Contact.CompanyName" back to "Customers.CompanyName"
			3) Set "Contact.EmailAddress" to "Contact.EmailAddressWork"
			4) We don't want "Partner.EmailAddress" at all, so just set that to "Contact.EmailAddressWork" as well. The optimiser will see that it is redundant and ignore it.
		*/
		SET @SelectWhereContacts = @SelectWhereContacts + ' AND ' + replace(replace(replace(replace(@WhereClause,'Customers.','Contact.'), 'Contact.CompanyName', 'Customers.CompanyName'), 'Contact.EmailAddress', 'Contact.EmailAddressWork'), 'Partner.EmailAddress', 'Contact.EmailAddressWork')

		SET @SelectWhereContacts = replace(@SelectWhereContacts,'DayTimeTelephoneNumber','DirectDial')
		SET @SelectWhereContacts = replace(@SelectWhereContacts,'HomeTelephone','DirectDial')
		SET @SelectWhereContacts = replace(@SelectWhereContacts,'MobileTelephone','DirectDial')
		SET @SelectWhereContacts = replace(@SelectWhereContacts,'CompanyTelephone','MobilePhoneWork')
		SET @SelectWhereContacts = replace(@SelectWhereContacts,'WorksTelephone','MobilePhoneOther')
		SET @SelectWhereContacts = replace(@SelectWhereContacts,'Occupation','JobTitle')
		SET @SelectWhereContacts = replace(@SelectWhereContacts,'Employer','JobTitle')

	END
	
	-- Merge in special fields 1, 2 and 3 if the user has specified any.
	IF @V1DetailFieldID > 0 
	BEGIN
		SELECT @LeadOrMatterTable = CASE @V1LeadOrMatter WHEN 'C' THEN 'Customer' WHEN 'L' THEN 'Lead' WHEN 'A' THEN 'Case' ELSE 'Matter' END, 
		@SpecialCols += CASE df.QuestionTypeID WHEN 4 THEN 'luliV1.ItemValue' ELSE 'V1.DetailValue' END + ' as [V1DetailValue], ' ,
		@SpecialJoins += '
			INNER JOIN dbo.' + @LeadOrMatterTable + 'DetailValues V1 (NOLOCK) on V1.' + @LeadOrMatterTable + 'ID = ' + @LeadOrMatterTable + CASE @LeadOrMatterTable WHEN 'Customer' THEN 's' WHEN 'Case' THEN 's' ELSE '' END + '.' + @LeadOrMatterTable + 'ID AND V1.DetailFieldID = ' + convert(varchar, @V1DetailFieldID) 
			+ CASE df.QuestionTypeID WHEN 4 THEN '
			INNER JOIN dbo.LookupListItems luliV1 WITH (NOLOCK) ON luliV1.LookupListItemID = V1.ValueInt AND luliV1.ItemValue' ELSE ' AND V1.DetailValue' END 
			+ ' LIKE ''' + @V1DetailValue + ''' '
		FROM dbo.DetailFields df WITH (NOLOCK) 
		WHERE df.DetailFieldID = @V1DetailFieldID 
	END
	IF @V2DetailFieldID > 0 
	BEGIN
		SELECT @LeadOrMatterTable = CASE @V2LeadOrMatter WHEN 'C' THEN 'Customer' WHEN 'L' THEN 'Lead' WHEN 'A' THEN 'Case' ELSE 'Matter' END, 
		@SpecialCols += CASE df.QuestionTypeID WHEN 4 THEN 'luliV2.ItemValue' ELSE 'V2.DetailValue' END + ' as [V2DetailValue], ' ,
		@SpecialJoins += '
			INNER JOIN dbo.' + @LeadOrMatterTable + 'DetailValues V2 (NOLOCK) on V2.' + @LeadOrMatterTable + 'ID = ' + @LeadOrMatterTable + CASE @LeadOrMatterTable WHEN 'Customer' THEN 's' WHEN 'Case' THEN 's' ELSE '' END + '.' + @LeadOrMatterTable + 'ID AND V2.DetailFieldID = ' + convert(varchar, @V2DetailFieldID) 
			+ CASE df.QuestionTypeID WHEN 4 THEN '
			INNER JOIN dbo.LookupListItems luliV2 WITH (NOLOCK) ON luliV2.LookupListItemID = V2.ValueInt AND luliV2.ItemValue' ELSE ' AND V2.DetailValue' END 
			+ ' LIKE ''' + @V2DetailValue + ''' '
		FROM dbo.DetailFields df WITH (NOLOCK) 
		WHERE df.DetailFieldID = @V2DetailFieldID 
	END
	IF @V3DetailFieldID > 0 
	BEGIN
		SELECT @LeadOrMatterTable = CASE @V3LeadOrMatter WHEN 'C' THEN 'Customer' WHEN 'L' THEN 'Lead' WHEN 'A' THEN 'Case' ELSE 'Matter' END, 
		@SpecialCols += CASE df.QuestionTypeID WHEN 4 THEN 'luliV3.ItemValue' ELSE 'V3.DetailValue' END + ' as [V3DetailValue], ' ,
		@SpecialJoins += '
			INNER JOIN dbo.' + @LeadOrMatterTable + 'DetailValues V3 (NOLOCK) on V3.' + @LeadOrMatterTable + 'ID = ' + @LeadOrMatterTable + CASE @LeadOrMatterTable WHEN 'Customer' THEN 's' WHEN 'Case' THEN 's' ELSE '' END + '.' + @LeadOrMatterTable + 'ID AND V3.DetailFieldID = ' + convert(varchar, @V3DetailFieldID) 
			+ CASE df.QuestionTypeID WHEN 4 THEN '
			INNER JOIN dbo.LookupListItems luliV3 WITH (NOLOCK) ON luliV3.LookupListItemID = V3.ValueInt AND luliV3.ItemValue' ELSE ' AND V3.DetailValue' END 
			+ ' LIKE ''' + @V3DetailValue + ''' '
		FROM dbo.DetailFields df WITH (NOLOCK) 
		WHERE df.DetailFieldID = @V3DetailFieldID 
	END
	
	-- If Client has LeadsBelongToOffice switched on, only return the rows which belong to the current User's ClientOfficeID
	IF @LeadsBelongToOffice = 1
	BEGIN
	
		/* 
			JWG 2010-07-01 New security option to restrict users from seeing unassigned leads. 
			Some clients want offices to see their own leads AND any new ones,
			but others want new leads to be invisible until an office has been selected, which
			happens when the lead is assigned to a user, who works at one office by definition. 
		*/
		SELECT @LeadBelongsToOfficeJoin = '	LEFT JOIN dbo.ClientOfficeLead COL (NOLOCK) on COL.LeadID = Lead.LeadID ',--AND COL.ClientOfficeID = ' + Convert(varchar(50), @ClientOfficeID) + ' ',
		@SpecialJoins = @SpecialJoins + @LeadBelongsToOfficeJoin,
		@LeadBelongsToOfficeWhere = ' AND (COL.ClientOfficeID = ' + Convert(varchar(11), @ClientOfficeID) + CASE @CanViewUnassignedLeads WHEN 1 THEN ' OR Lead.Assigned = 0) ' ELSE ')' END ,
		@SelectWhere = @SelectWhere + @LeadBelongsToOfficeWhere

		PRINT @SelectWhere
	END

	/* 
		JWG 2010-07-01 New security option to restrict users from seeing unassigned leads. 
		This overlaps somewhat with the office security clause above, but if anyone tries to use 
		this without having "LeadsBelongToOffice" switched on, they will still expect it to work.
	*/
	IF @CanViewUnassignedLeads = 0
	BEGIN
		SELECT @SelectWhere += ' AND Lead.Assigned = 1 '
		
		SELECT @SelectWhereContacts += ' AND Lead.Assigned = 1 '
	END
	
	/* 
		JWG 2010-07-01 New security option to restrict users from seeing leads assigned to others, 
		regardless of whether or not the other user works within the same office.
	*/	
	IF @CanViewLeadsAssignedToOthers = 0
	BEGIN
		SELECT @SelectWhere += ' AND Lead.AssignedTo = ' + convert(varchar(11), @UserID)
		
		SELECT @SelectWhereContacts += ' AND Lead.AssignedTo = ' + convert(varchar(11), @UserID)
	END
	
	SELECT @SelectPart2 = '
	),
	Counts AS (
		SELECT count(*) as [TotalRows] FROM Matches
		WHERE [nodupsrowno] = 1 
	),
	RowsToReturn AS (
		SELECT Matches.*, row_number() over (order by Matches.[rowno]) as [rownotoreturn] 
		FROM Matches 
		WHERE [nodupsrowno] = 1 
	)
	SELECT Counts.TotalRows, RowsToReturn.*  
	FROM Counts 
	CROSS JOIN RowsToReturn 
	WHERE RowsToReturn.rownotoreturn BETWEEN ' + convert(varchar, @PageLowerBound) + ' AND ' + convert(varchar, @PageUpperBound) + ' 
	AND RowsToReturn.[nodupsrowno] = 1 
	ORDER BY RowsToReturn.rownotoreturn '
	
	
	/* JWG 2009-07-09 New option to let people see their test records */
	IF @ShowTests = 1
	BEGIN
		SET @CustomersTestOrNot = ''
	END
	ELSE
	BEGIN
		SET @CustomersTestOrNot = ' AND (Customers.[Test] = 0 OR Customers.[Test] IS NULL)'
	END
	
	IF @ShowBusiness = 1 OR @ShowIndividual = 1
	BEGIN
		
		/*ALM/GPR SDPRU-51*/
		SELECT @SelectPart1 = 'WITH Matches AS (
		SELECT Customers.[CustomerID], Customers.[ClientID], Customers.[TitleID], Customers.[FirstName], Customers.[MiddleName], Customers.[LastName], 
		Customers.[Fullname] + CASE WHEN Partner.PartnerID IS NOT NULL THEN '' and '' + Partner.[Fullname] ELSE '''' END as [Name], 
		Matter.[LeadID] as [ID], Customers.[EmailAddress] as ''Email Address'', Customers.[DayTimeTelephoneNumber], Customers.[HomeTelephone], Customers.[MobileTelephone], Customers.[Address1],
		Customers.[Address2], Customers.[Town], Customers.[County], Customers.[PostCode] as ''Post Code'', Customers.[HasDownloaded], Customers.[DownloadedOn], Customers.[Test],
		Customers.[Occupation], Customers.[Employer], ClientPersonnel.[UserName] as ''Assigned To'', Lead.[LeadID], IsNull(Lead.LeadRef,'''') as [LeadRef],
		LeadType.LeadTypeName as [Lead Type], Cases.[CaseID], Cases.[CaseNum] as ''Case'', Matter.[MatterID], Customers.[IsBusiness], IsNull(Titles.Title,'''') as [Title],
		IsNull(LeadStatus.StatusName,'''') as [Status], AquariumStatus.AquariumStatusName, Matter.RefLetter as [Ref], convert(char(10), FORMAT(Cases.[WhenCreated], ''d'', ''en-US''), 103) as [WhenCreated],  
		' + @SpecialCols + '
		row_number() over (order by ' + @SortCol + ' ' + @SortOrder + ') as [rowno], ' + @NoDuplicatesClause + ' AS [nodupsrowno] ' + 
		'FROM dbo.[Lead] (NOLOCK) 
		INNER JOIN dbo.[Customers] (NOLOCK) on Lead.[CustomerID] = Customers.[CustomerID] ' + @CustomersTestOrNot + CASE WHEN @ShowBusiness = 0 AND @ShowIndividual = 1 THEN ' AND Customers.[IsBusiness] = 0 ' WHEN @ShowBusiness = 1 AND @ShowIndividual = 0 THEN ' AND Customers.[IsBusiness] = 1 ' ELSE '' END + 
		'INNER JOIN dbo.[LeadType] (NOLOCK) on LeadType.[LeadTypeID] = Lead.[LeadTypeID] 
		INNER JOIN dbo.fnLeadTypeSecure(' + convert(varchar, @UserID) + ') f ON LeadType.LeadTypeID = f.LeadTypeID
		INNER JOIN dbo.[Cases] (NOLOCK) on Cases.[LeadID] = Lead.[LeadID] 
		INNER JOIN dbo.[Matter] (NOLOCK) on Matter.[CaseID] = Cases.[CaseID] 
		INNER JOIN dbo.[AquariumStatus] (NOLOCK) on AquariumStatus.[AquariumStatusID] = Customers.[AquariumStatusID] 
		LEFT OUTER JOIN dbo.[Titles] (NOLOCK) on Titles.TitleID = Customers.[TitleID] 
		LEFT OUTER JOIN dbo.[LeadStatus] (NOLOCK) on LeadStatus.StatusID = Cases.[ClientStatusID] 
		LEFT OUTER JOIN dbo.[ClientPersonnel] (NOLOCK) on ClientPersonnel.ClientPersonnelID = Lead.[AssignedTo] 
		LEFT OUTER JOIN dbo.[Partner] (NOLOCK) on Partner.CustomerID = Customers.CustomerID '
		/* JWG 2013-02-11 Include Partner details in searches */

		SELECT @SelectFull = @SelectPart1 + @SpecialJoins + @SelectWhere + @SelectPart2
		PRINT @SelectFull
		EXEC(@SelectFull)	
	END
	ELSE
	BEGIN
		SELECT 0 as [TotalRows] FROM AquariumStatus WHERE AquariumStatusID = 1
	END
	
	
	IF @ShowContact = 1
	BEGIN
	
		SELECT @SortColContacts = replace(@SortCol, 'EmailAddress', 'EmailAddressWork')
		SELECT @SortColContacts = replace(@SortColContacts, 'UserName', 'Fullname')
		SELECT @SortColContacts = replace(@SortColContacts, 'Cases.', '')
		SELECT @SortColContacts = replace(@SortColContacts, 'Matter.', '')
		
		SELECT @SortColContactsLen = len(@SortColContacts), @SortColContactsDotPos = charindex('.', @SortColContacts)
		
		IF @SortColContactsDotPos > 0
		BEGIN
			SELECT @SortColContacts = substring(@SortColContacts, @SortColContactsDotPos + 1, @SortColContactsLen)
		END
		
		/*ALM/GPR SDPRU-51*/
		SELECT @SelectPart1Contacts = 'WITH RawData AS (
		SELECT DISTINCT Customers.[CustomerID], Customers.[ClientID], Contact.[ContactID], Contact.[TitleID], Contact.[FirstName], Contact.[MiddleName], Contact.[LastName], Contact.[Fullname], 
		Contact.[EmailAddressWork], Contact.[EmailAddressOther], Contact.[DirectDial], Contact.[MobilePhoneWork], Contact.[MobilePhoneOther], 
		Contact.[Address1], Contact.[Address2], Contact.[Town], Contact.[County], Contact.[PostCode], Contact.[Country], 
		Lead.[LeadID], IsNull(Lead.LeadRef,'''') as [LeadRef], 
		LeadType.LeadTypeName as [LeadTypeName], Customers.[IsBusiness], Customers.[CompanyName], Contact.[Notes], Contact.[OfficeID], Contact.[DepartmentID],  
		IsNull(Titles.Title,'''') as [Title], IsNull(DepartmentName,'''') as [DepartmentName], IsNull(OfficeName,'''') as [OfficeName], 
		Cases.[CaseID], Matter.[MatterID], Matter.[RefLetter], convert(char(10), FORMAT(Cases.[WhenCreated], ''d'', ''en-US''), 103) as [WhenCreated], Customers.Test  
		FROM dbo.[Lead] (NOLOCK) 
		INNER JOIN dbo.[Customers] (NOLOCK) on Customers.[CustomerID] = Lead.[CustomerID] ' + @CustomersTestOrNot + '
		INNER JOIN dbo.[Contact] (NOLOCK) on Contact.CustomerID = Customers.[CustomerID] 
		INNER JOIN dbo.[LeadType] (NOLOCK) on LeadType.[LeadTypeID] = Lead.[LeadTypeID] 
		INNER JOIN dbo.[Cases] (NOLOCK) on Cases.[LeadID] = Lead.[LeadID]
		INNER JOIN dbo.[Matter] (NOLOCK) on Matter.[CaseID] = Cases.[CaseID] 
		INNER JOIN fnLeadTypeSecure(' + convert(varchar, @UserID) + ') f ON LeadType.LeadTypeID = f.LeadTypeID 
		LEFT OUTER JOIN dbo.[Titles] (NOLOCK) on Titles.TitleID = Customers.[TitleID] 
		LEFT OUTER JOIN dbo.[Department] (NOLOCK) on Department.DepartmentID = Contact.[DepartmentID] 
		LEFT OUTER JOIN dbo.[CustomerOffice] (NOLOCK) on CustomerOffice.OfficeID = Contact.[OfficeID] 
		LEFT OUTER JOIN dbo.[Partner] (NOLOCK) on Partner.CustomerID = Customers.CustomerID 
		'
		
		SELECT @SelectPart2Contacts = '),
		Matches AS (
		SELECT [CustomerID], [ClientID], [ContactID], [TitleID], [FirstName], [MiddleName], [LastName], [Fullname], 
		[EmailAddressWork], [EmailAddressOther], [DirectDial], [MobilePhoneWork], [MobilePhoneOther], 
		[Address1], [Address2], [Town], [County], [PostCode], [Country], 
		[LeadID], [LeadRef], 
		[LeadTypeName], [IsBusiness], [CompanyName] as ''Company Name'', [Notes], [OfficeID], [DepartmentID],  
		[Title], [DepartmentName], [OfficeName], 
		row_number() over (order by ' + @SortColContacts + ' ' + @SortOrder + ') as [rowno], ' + @NoDuplicatesContactsClause + ' AS [nodupsrowno] ' + ' 
		FROM RawData
		'

		/* If Client has LeadsBelongToOffice switched on, only return the rows which belong to the current User's ClientOfficeID */
		IF @LeadsBelongToOffice = 1
		BEGIN
			SELECT @SelectPart1Contacts = @SelectPart1Contacts + @LeadBelongsToOfficeJoin,
				@SelectWhereContacts = @SelectWhereContacts + @LeadBelongsToOfficeWhere 
		END

		SELECT @SelectFullContacts = @SelectPart1Contacts + @SelectWhereContacts + @SelectPart2Contacts + @SelectPart2
		PRINT @SelectFullContacts
		EXEC(@SelectFullContacts)
		
	END
	ELSE
	BEGIN
	
		SELECT 0 as [TotalRows] 
		
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomersLeadMatterSearchPagedAllFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomersLeadMatterSearchPagedAllFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomersLeadMatterSearchPagedAllFields] TO [sp_executeall]
GO
