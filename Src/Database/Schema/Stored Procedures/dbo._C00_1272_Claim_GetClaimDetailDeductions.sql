SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-03-05
-- Description:	Gets all the user added deductions for a single claim row
-- Used by:		Popup called from DF 147723
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_GetClaimDetailDeductions] 
(
	@TableRowID INT
)

AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT	tdvClaimRowID.ValueInt AS ClaimTableRowID, tdvReason.ValueInt AS DeductionReasonID, llReason.ItemValue AS DeductionReason, tdvAmount.ValueMoney AS DeductionAmount,
			tdvItemNotIncluded.DetailValue AS ItemNotCovered
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvClaimRowID WITH (NOLOCK) ON r.TableRowID = tdvClaimRowID.TableRowID AND tdvClaimRowID.DetailFieldID = 147299 
	INNER JOIN dbo.TableDetailValues tdvReason WITH (NOLOCK) ON r.TableRowID = tdvReason.TableRowID AND tdvReason.DetailFieldID = 147300
	INNER JOIN dbo.LookupListItems llReason WITH (NOLOCK) ON tdvReason.ValueInt = llReason.LookupListItemID 
	INNER JOIN dbo.TableDetailValues tdvAmount WITH (NOLOCK) ON r.TableRowID = tdvAmount.TableRowID AND tdvAmount.DetailFieldID = 147301
	LEFT JOIN dbo.TableDetailValues tdvItemNotIncluded WITH (NOLOCK) ON r.TableRowID = tdvItemNotIncluded.TableRowID AND tdvItemNotIncluded.DetailFieldID = 148432
	WHERE tdvClaimRowID.ValueInt = @TableRowID
	AND r.DetailFieldID = 147302
	AND r.DetailFieldPageID = 16157
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetClaimDetailDeductions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_GetClaimDetailDeductions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_GetClaimDetailDeductions] TO [sp_executeall]
GO
