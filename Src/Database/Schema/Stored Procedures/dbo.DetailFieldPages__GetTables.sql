SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2008-10-06
-- Description:	Merge Aquarium resource lists with those for the specific ClientID passed in
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared pages
-- =============================================
CREATE PROCEDURE [dbo].[DetailFieldPages__GetTables]
(
	@ClientID int, 
	@TypeToShow int = 0,    -- Default to all types
	@ShowDisabled bit = 0,  -- Only show Enabled lists by default
	@LeadTypeID int = 0     -- Default to all lead types
)
AS
BEGIN

	SELECT dfp.*
	FROM
		dbo.fnDetailFieldPagesShared(@ClientID) dfp
	WHERE
		([ClientID] = @ClientID OR [ClientID] = 0)
	AND ([LeadOrMatter] = @TypeToShow OR @TypeToShow = 0)
	AND ([Enabled] = 1 OR @ShowDisabled = 1)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID = 0)

	ORDER BY 
		ClientID, [PageName] 
				

	SELECT @@ROWCOUNT

END



GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages__GetTables] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldPages__GetTables] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages__GetTables] TO [sp_executeall]
GO
