SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-06-15
-- Description:	Test of a custom proc for objects 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Objects_2_GetPeopleTestProc]
(	
	@ClientID INT,
	@Name VARCHAR(2000),
	@IsOld BIT,
	@Birthday DATETIME,
	@Cost MONEY	
)
AS
BEGIN
	
	

DECLARE @Objects TABLE
(
	ObjectID INT,
	Name VARCHAR(250),
	Details VARCHAR(2000),
	ObjectTypeID INT,
	ObjectTypeRelationshipID INT,
	FromObjectID INT
)

-- Primary objects
INSERT INTO @Objects (ObjectID, Name, Details, ObjectTypeID, ObjectTypeRelationshipID, FromObjectID)
SELECT ObjectID, Name, Details, ObjectTypeID, NULL, NULL
FROM dbo.Objects WITH (NOLOCK) 
WHERE ObjectTypeID = 2

-- Linked obejcts (needs to be recursive based on what is passed in)
INSERT INTO @Objects (ObjectID, Name, Details, ObjectTypeID, ObjectTypeRelationshipID, FromObjectID)
SELECT o2.ObjectID, o2.Name, o2.Details, o2.ObjectTypeID, ol.ObjectTypeRelationshipID, ol.FromObjectID
FROM dbo.Objects o WITH (NOLOCK) 
INNER JOIN dbo.ObjectLink ol WITH (NOLOCK) ON o.ObjectID = ol.FromObjectID 
INNER JOIN dbo.Objects o2 WITH (NOLOCK) ON ol.ToObjectID = o2.ObjectID 
WHERE o.ObjectTypeID = 2

-- Return objects
SELECT * 
FROM @Objects

-- Return detail fields
SELECT o.ObjectID, df.DetailFieldID, df.DetailValue 
FROM dbo.ObjectDetailValues df WITH (NOLOCK) 
INNER JOIN @Objects o ON df.ObjectID = o.ObjectID 

-- Return top level data of related objects (based on what is passed in)
SELECT l.FromObjectID, l.ToObjectID, l.ObjectTypeRelationshipID, o2.Name, o2.Details
FROM @Objects o 
INNER JOIN dbo.ObjectLink l WITH (NOLOCK) ON o.ObjectID = l.FromObjectID
INNER JOIN dbo.Objects o2 WITH (NOLOCK) ON l.ToObjectID = o2.ObjectID 

	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Objects_2_GetPeopleTestProc] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Objects_2_GetPeopleTestProc] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Objects_2_GetPeopleTestProc] TO [sp_executeall]
GO
