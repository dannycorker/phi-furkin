SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeSql table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeSql_GetByEventTypeID]
(

	@EventTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EventTypeSqlID],
					[ClientID],
					[EventTypeID],
					[PostUpdateSql],
					[IsNativeSql]
				FROM
					[dbo].[EventTypeSql] WITH (NOLOCK) 
				WHERE
					[EventTypeID] = @EventTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeSql_GetByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeSql_GetByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeSql_GetByEventTypeID] TO [sp_executeall]
GO
