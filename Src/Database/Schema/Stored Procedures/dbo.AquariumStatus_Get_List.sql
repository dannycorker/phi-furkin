SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the AquariumStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumStatus_Get_List]

AS


				
				SELECT
					[AquariumStatusID],
					[AquariumStatusName],
					[AquariumStatusDescription],
					[Usable]
				FROM
					[dbo].[AquariumStatus] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumStatus_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumStatus_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumStatus_Get_List] TO [sp_executeall]
GO
