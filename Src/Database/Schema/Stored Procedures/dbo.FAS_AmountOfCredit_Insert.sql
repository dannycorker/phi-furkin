SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the FAS_AmountOfCredit table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_AmountOfCredit_Insert]
(

	@FasAmountOfCreditID int    OUTPUT,

	@FasCheckerDataID int   ,

	@Description varchar (512)  ,

	@CostAmount decimal (19, 4)  
)
AS


				
				INSERT INTO [dbo].[FAS_AmountOfCredit]
					(
					[FasCheckerDataID]
					,[Description]
					,[CostAmount]
					)
				VALUES
					(
					@FasCheckerDataID
					,@Description
					,@CostAmount
					)
				-- Get the identity value
				SET @FasAmountOfCreditID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_AmountOfCredit_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_AmountOfCredit_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_AmountOfCredit_Insert] TO [sp_executeall]
GO
