SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ThirdPartyField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyField_Insert]
(

	@ThirdPartyFieldID int    OUTPUT,

	@ThirdPartySystemId int   ,

	@FieldName varchar (250)  ,

	@FieldDescription varchar (250)  ,

	@IsEnabled bit   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[ThirdPartyField]
					(
					[ThirdPartySystemId]
					,[FieldName]
					,[FieldDescription]
					,[IsEnabled]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@ThirdPartySystemId
					,@FieldName
					,@FieldDescription
					,@IsEnabled
					,@WhenCreated
					,@WhenModified
					)
				-- Get the identity value
				SET @ThirdPartyFieldID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyField_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyField_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyField_Insert] TO [sp_executeall]
GO
