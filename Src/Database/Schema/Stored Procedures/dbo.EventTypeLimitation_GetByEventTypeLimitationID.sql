SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeLimitation table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeLimitation_GetByEventTypeLimitationID]
(

	@EventTypeLimitationID int   
)
AS


				SELECT
					[EventTypeLimitationID],
					[ClientID],
					[LeadTypeID],
					[EventTypeIDFrom],
					[EventTypeIDTo],
					[LimitationDays],
					[Description]
				FROM
					[dbo].[EventTypeLimitation] WITH (NOLOCK) 
				WHERE
										[EventTypeLimitationID] = @EventTypeLimitationID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeLimitation_GetByEventTypeLimitationID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeLimitation_GetByEventTypeLimitationID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeLimitation_GetByEventTypeLimitationID] TO [sp_executeall]
GO
