SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ScriptTag table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptTag_Update]
(

	@ScriptTagID int   ,

	@Tag varchar (50)  ,

	@Template varchar (MAX)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ScriptTag]
				SET
					[Tag] = @Tag
					,[Template] = @Template
				WHERE
[ScriptTagID] = @ScriptTagID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptTag_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptTag_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptTag_Update] TO [sp_executeall]
GO
