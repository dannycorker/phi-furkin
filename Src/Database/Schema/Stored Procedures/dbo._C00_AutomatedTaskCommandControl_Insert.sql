SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-09-03
-- Description:	Inserts into the AutomatedTaskCommandControl Table
-- =============================================
CREATE PROCEDURE [dbo].[_C00_AutomatedTaskCommandControl_Insert] 
	@ClientID INT,
	@JobName VARCHAR(200),
	@JobNotes VARCHAR(200),
	@FiringOrder VARCHAR(200),
	@SuppressAllLaterJobs VARCHAR(200),
	@IsConfigFileRequired VARCHAR(200),
	@ConfigFileName VARCHAR(200),
	@IsBatFileRequired VARCHAR(200),
	@BatFileName VARCHAR(200),
	@Enabled INT,
	@MaxWaitTimeMS INT,
	@TaskID VARCHAR(200),
	@OutputExtension VARCHAR(200)
	
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [AutomatedTaskCommandControl] 
	([ClientID],[JobName],[JobNotes],[FiringOrder],[SuppressAllLaterJobs],[IsConfigFileRequired],[ConfigFileName],[IsBatFileRequired],[BatFileName],[Enabled],[MaxWaitTimeMS],[TaskID],[OutputExtension])
	VALUES ( @ClientID,@JobName,@JobNotes,@FiringOrder,@SuppressAllLaterJobs,@IsConfigFileRequired,@ConfigFileName,@IsBatFileRequired,@BatFileName,@Enabled,@MaxWaitTimeMS,@TaskID,@OutputExtension)

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AutomatedTaskCommandControl_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_AutomatedTaskCommandControl_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AutomatedTaskCommandControl_Insert] TO [sp_executeall]
GO
