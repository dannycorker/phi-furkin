SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SMSQuestionGroupMember table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionGroupMember_Insert]
(

	@SMSQuestionGroupMemberID int    OUTPUT,

	@ClientID int   ,

	@SMSQuestionnaireID int   ,

	@SMSQuestionID int   ,

	@SMSQuestionGroupID int   
)
AS


				
				INSERT INTO [dbo].[SMSQuestionGroupMember]
					(
					[ClientID]
					,[SMSQuestionnaireID]
					,[SMSQuestionID]
					,[SMSQuestionGroupID]
					)
				VALUES
					(
					@ClientID
					,@SMSQuestionnaireID
					,@SMSQuestionID
					,@SMSQuestionGroupID
					)
				-- Get the identity value
				SET @SMSQuestionGroupMemberID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroupMember_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroupMember_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroupMember_Insert] TO [sp_executeall]
GO
