SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the MatterPageChoice table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterPageChoice_Find]
(

	@SearchUsingOR bit   = null ,

	@MatterPageChoiceID int   = null ,

	@ClientID int   = null ,

	@MatterID int   = null ,

	@DetailFieldPageID int   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [MatterPageChoiceID]
	, [ClientID]
	, [MatterID]
	, [DetailFieldPageID]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[MatterPageChoice] WITH (NOLOCK) 
    WHERE 
	 ([MatterPageChoiceID] = @MatterPageChoiceID OR @MatterPageChoiceID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([MatterID] = @MatterID OR @MatterID IS NULL)
	AND ([DetailFieldPageID] = @DetailFieldPageID OR @DetailFieldPageID IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [MatterPageChoiceID]
	, [ClientID]
	, [MatterID]
	, [DetailFieldPageID]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[MatterPageChoice] WITH (NOLOCK) 
    WHERE 
	 ([MatterPageChoiceID] = @MatterPageChoiceID AND @MatterPageChoiceID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([MatterID] = @MatterID AND @MatterID is not null)
	OR ([DetailFieldPageID] = @DetailFieldPageID AND @DetailFieldPageID is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterPageChoice_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterPageChoice_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterPageChoice_Find] TO [sp_executeall]
GO
