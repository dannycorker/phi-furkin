SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 22/10/08
-- Description:	Used to return a list of ClientQuestionnaires for the given LeadTypeID
-- =============================================
CREATE PROCEDURE [dbo].[ClientQuestionnaires__GetByLeadTypeID] 
	@LeadTypeID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		SELECT cq.*
		FROM
			[dbo].[ClientQuestionnaires] cq
		WHERE EXISTS (
			SELECT * FROM dbo.Outcomes
			INNER JOIN LeadTypeLink ltl ON ltl.OutcomeID = Outcomes.OutcomeID
			WHERE ltl.LeadTypeID = @LeadTypeID
			AND	Outcomes.ClientQuestionnaireID = cq.ClientQuestionnaireID
		)
END



GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaires__GetByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnaires__GetByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaires__GetByLeadTypeID] TO [sp_executeall]
GO
