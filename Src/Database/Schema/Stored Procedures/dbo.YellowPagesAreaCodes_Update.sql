SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the YellowPagesAreaCodes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[YellowPagesAreaCodes_Update]
(

	@YellowPagesAreaCodeID int   ,

	@YellowPagesAreaCode varchar (10)  ,

	@PostCode varchar (10)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[YellowPagesAreaCodes]
				SET
					[YellowPagesAreaCode] = @YellowPagesAreaCode
					,[PostCode] = @PostCode
				WHERE
[YellowPagesAreaCodeID] = @YellowPagesAreaCodeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[YellowPagesAreaCodes_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[YellowPagesAreaCodes_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[YellowPagesAreaCodes_Update] TO [sp_executeall]
GO
