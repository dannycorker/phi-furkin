SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientOfficeLead table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOfficeLead_GetByClientOfficeLeadID]
(

	@ClientOfficeLeadID int   
)
AS


				SELECT
					[ClientOfficeLeadID],
					[ClientID],
					[ClientOfficeID],
					[LeadID]
				FROM
					[dbo].[ClientOfficeLead] WITH (NOLOCK) 
				WHERE
										[ClientOfficeLeadID] = @ClientOfficeLeadID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeLead_GetByClientOfficeLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOfficeLead_GetByClientOfficeLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeLead_GetByClientOfficeLeadID] TO [sp_executeall]
GO
