SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Stored Procedure

-- =============================================
-- Author:		Paul Richardson
-- Create date: 11-07-2017
-- Description:	Deletes all the document detail field target
--              records that have the give document type version id
-- =============================================
CREATE PROCEDURE [dbo].[DocumentDetailFieldTarget__DeleteByDocumentTypeVersionID]
(

@DocumentTypeVersionID int
)
AS


DELETE FROM [dbo].[DocumentDetailFieldTarget] WITH (ROWLOCK)
WHERE
[DocumentTypeVersionID] = @DocumentTypeVersionID

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentDetailFieldTarget__DeleteByDocumentTypeVersionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentDetailFieldTarget__DeleteByDocumentTypeVersionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentDetailFieldTarget__DeleteByDocumentTypeVersionID] TO [sp_executeall]
GO
