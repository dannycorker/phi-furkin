SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-04-28
-- Description:	List all AquariumOption records
-- =============================================
CREATE PROCEDURE [dbo].[ao] 
	@AquariumOptionTypeID int = null 
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT ao.* 
	FROM dbo.AquariumOption ao WITH (NOLOCK)
	WHERE (@AquariumOptionTypeID IS NULL OR ao.AquariumOptionTypeID = @AquariumOptionTypeID)
	ORDER BY ao.AquariumOptionTypeID, ao.AquariumOptionID 

END






GO
GRANT VIEW DEFINITION ON  [dbo].[ao] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ao] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ao] TO [sp_executeall]
GO
