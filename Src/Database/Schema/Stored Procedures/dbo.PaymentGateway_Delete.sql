SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PaymentGateway table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PaymentGateway_Delete]
(

	@PaymentGatewayID int   
)
AS


				DELETE FROM [dbo].[PaymentGateway] WITH (ROWLOCK) 
				WHERE
					[PaymentGatewayID] = @PaymentGatewayID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway_Delete] TO [sp_executeall]
GO
