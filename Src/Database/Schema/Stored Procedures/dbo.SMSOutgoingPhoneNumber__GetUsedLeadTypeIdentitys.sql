SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 16-04-2013
-- Description:	Gets a list of LeadTypes that have been used on this SMSQuestionnaire
-- =============================================
CREATE PROCEDURE [dbo].[SMSOutgoingPhoneNumber__GetUsedLeadTypeIdentitys]

	@SMSQuestionnaireID INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT LeadTypeID FROM SMSOutgoingPhoneNumber WITH (NOLOCK) 
	WHERE SMSQuestionnaireID = @SMSQuestionnaireID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber__GetUsedLeadTypeIdentitys] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSOutgoingPhoneNumber__GetUsedLeadTypeIdentitys] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber__GetUsedLeadTypeIdentitys] TO [sp_executeall]
GO
