SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.CountQuestionPossibleAnswers    Script Date: 08/09/2006 12:22:37 ******/
CREATE PROCEDURE [dbo].[CountQuestionPossibleAnswers] @ClientQuestionnaireID int

 AS


SELECT     dbo.ClientQuestionnaires.ClientQuestionnaireID AS AnswerCount, COUNT(dbo.QuestionPossibleAnswers.QuestionPossibleAnswerID) AS Expr2
FROM         dbo.ClientQuestionnaires INNER JOIN
                      dbo.QuestionPossibleAnswers ON dbo.ClientQuestionnaires.ClientQuestionnaireID = dbo.QuestionPossibleAnswers.ClientQuestionnaireID
GROUP BY dbo.ClientQuestionnaires.ClientQuestionnaireID
HAVING      (dbo.ClientQuestionnaires.ClientQuestionnaireID = @ClientQuestionnaireID)




GO
GRANT VIEW DEFINITION ON  [dbo].[CountQuestionPossibleAnswers] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CountQuestionPossibleAnswers] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CountQuestionPossibleAnswers] TO [sp_executeall]
GO
