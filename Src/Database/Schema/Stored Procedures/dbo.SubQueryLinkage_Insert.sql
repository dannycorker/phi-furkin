SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SubQueryLinkage table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubQueryLinkage_Insert]
(

	@SubQueryLinkageID int    OUTPUT,

	@ClientID int   ,

	@SubQueryID int   ,

	@LinksToTableName varchar (50)  ,

	@LinksToColumnName varchar (50)  ,

	@LinkageDataType varchar (50)  ,

	@ParamTableName varchar (50)  ,

	@ParamColumnName varchar (50)  ,

	@ParamColumnDataType varchar (50)  ,

	@ParamColumnHelperSql varchar (2000)  ,

	@LeadTypeFilterSql varchar (50)  
)
AS


				
				INSERT INTO [dbo].[SubQueryLinkage]
					(
					[ClientID]
					,[SubQueryID]
					,[LinksToTableName]
					,[LinksToColumnName]
					,[LinkageDataType]
					,[ParamTableName]
					,[ParamColumnName]
					,[ParamColumnDataType]
					,[ParamColumnHelperSql]
					,[LeadTypeFilterSql]
					)
				VALUES
					(
					@ClientID
					,@SubQueryID
					,@LinksToTableName
					,@LinksToColumnName
					,@LinkageDataType
					,@ParamTableName
					,@ParamColumnName
					,@ParamColumnDataType
					,@ParamColumnHelperSql
					,@LeadTypeFilterSql
					)
				-- Get the identity value
				SET @SubQueryLinkageID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SubQueryLinkage_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubQueryLinkage_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubQueryLinkage_Insert] TO [sp_executeall]
GO
