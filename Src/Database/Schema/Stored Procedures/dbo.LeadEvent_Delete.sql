SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEvent_Delete]
(

	@LeadEventID int   
)
AS


				DELETE FROM [dbo].[LeadEvent] WITH (ROWLOCK) 
				WHERE
					[LeadEventID] = @LeadEventID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEvent_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent_Delete] TO [sp_executeall]
GO
