SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-09-10
-- Description:	Create New Customer from Customer
--				2011-06-11 ACE Nolocked where possible and added Partner
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateCustomerFromCustomer]
	@CustomerID int,
	@NewClientID int,
	@NewLeadTypeID int, 
	@WhoCreated int
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @NewCustomerID int,
			@NewLeadID int

	INSERT INTO Customers(ClientID,TitleID,IsBusiness,FirstName,MiddleName,LastName,EmailAddress,DayTimeTelephoneNumber,DayTimeTelephoneNumberVerifiedAndValid,HomeTelephone,HomeTelephoneVerifiedAndValid,MobileTelephone,MobileTelephoneVerifiedAndValid,CompanyTelephone,CompanyTelephoneVerifiedAndValid,WorksTelephone,WorksTelephoneVerifiedAndValid,Address1,Address2,Town,County,PostCode,Website,HasDownloaded,DownloadedOn,AquariumStatusID,ClientStatusID,Test,CompanyName,Occupation,Employer,PhoneNumbersVerifiedOn,DoNotEmail,DoNotSellToThirdParty,AgreedToTermsAndConditions,DateOfBirth,DefaultContactID,DefaultOfficeID)	
	SELECT @NewClientID,TitleID,IsBusiness,FirstName,MiddleName,LastName,EmailAddress,DayTimeTelephoneNumber,DayTimeTelephoneNumberVerifiedAndValid,HomeTelephone,HomeTelephoneVerifiedAndValid,MobileTelephone,MobileTelephoneVerifiedAndValid,CompanyTelephone,CompanyTelephoneVerifiedAndValid,WorksTelephone,WorksTelephoneVerifiedAndValid,Address1,Address2,Town,County,PostCode,Website,HasDownloaded,DownloadedOn,AquariumStatusID,ClientStatusID,Test,CompanyName,Occupation,Employer,PhoneNumbersVerifiedOn,DoNotEmail,DoNotSellToThirdParty,AgreedToTermsAndConditions,DateOfBirth,DefaultContactID,DefaultOfficeID	
	FROM Customers WITH (NOLOCK)
	Where CustomerID = @CustomerID

	SELECT @NewCustomerID = SCOPE_IDENTITY()

	INSERT INTO Partner (CustomerID,ClientID,UseCustomerAddress,TitleID,FirstName,MiddleName,LastName,EmailAddress,DayTimeTelephoneNumber,HomeTelephone,MobileTelephone,Address1,Address2,Town,County,PostCode,Occupation,Employer,DateOfBirth,CountryID)
	SELECT @NewCustomerID,@NewClientID,UseCustomerAddress,TitleID,FirstName,MiddleName,LastName,EmailAddress,DayTimeTelephoneNumber,HomeTelephone,MobileTelephone,Address1,Address2,Town,County,PostCode,Occupation,Employer,DateOfBirth,CountryID
	FROM Partner p WITH (NOLOCK)
	WHERE p.CustomerID = @CustomerID
	
	exec @NewLeadID = dbo._C00_CreateNewLeadForCust @NewCustomerID, @NewLeadTypeID, @NewClientID, @WhoCreated

	RETURN @NewLeadID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateCustomerFromCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateCustomerFromCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateCustomerFromCustomer] TO [sp_executeall]
GO
