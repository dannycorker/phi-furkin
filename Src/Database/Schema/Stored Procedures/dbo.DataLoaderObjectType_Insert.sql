SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DataLoaderObjectType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectType_Insert]
(

	@DataLoaderObjectTypeID int   ,

	@ObjectTypeName varchar (250)  ,

	@ObjectTypeDescription varchar (2000)  ,

	@ProcessingOrder int   
)
AS


				
				INSERT INTO [dbo].[DataLoaderObjectType]
					(
					[DataLoaderObjectTypeID]
					,[ObjectTypeName]
					,[ObjectTypeDescription]
					,[ProcessingOrder]
					)
				VALUES
					(
					@DataLoaderObjectTypeID
					,@ObjectTypeName
					,@ObjectTypeDescription
					,@ProcessingOrder
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectType_Insert] TO [sp_executeall]
GO
