SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the AssetSubType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetSubType_Update]
(

	@AssetSubTypeID int   ,

	@AssetSubTypeName varchar (255)  ,

	@Note varchar (255)  ,

	@Enabled bit   ,

	@Deleted bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[AssetSubType]
				SET
					[AssetSubTypeName] = @AssetSubTypeName
					,[Note] = @Note
					,[Enabled] = @Enabled
					,[Deleted] = @Deleted
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[AssetSubTypeID] = @AssetSubTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetSubType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetSubType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetSubType_Update] TO [sp_executeall]
GO
