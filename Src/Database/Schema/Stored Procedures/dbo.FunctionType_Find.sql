SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the FunctionType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FunctionType_Find]
(

	@SearchUsingOR bit   = null ,

	@FunctionTypeID int   = null ,

	@ModuleID int   = null ,

	@ParentFunctionTypeID int   = null ,

	@FunctionTypeName varchar (50)  = null ,

	@Level int   = null ,

	@IsVirtual bit   = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [FunctionTypeID]
	, [ModuleID]
	, [ParentFunctionTypeID]
	, [FunctionTypeName]
	, [Level]
	, [IsVirtual]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[FunctionType] WITH (NOLOCK) 
    WHERE 
	 ([FunctionTypeID] = @FunctionTypeID OR @FunctionTypeID IS NULL)
	AND ([ModuleID] = @ModuleID OR @ModuleID IS NULL)
	AND ([ParentFunctionTypeID] = @ParentFunctionTypeID OR @ParentFunctionTypeID IS NULL)
	AND ([FunctionTypeName] = @FunctionTypeName OR @FunctionTypeName IS NULL)
	AND ([Level] = @Level OR @Level IS NULL)
	AND ([IsVirtual] = @IsVirtual OR @IsVirtual IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [FunctionTypeID]
	, [ModuleID]
	, [ParentFunctionTypeID]
	, [FunctionTypeName]
	, [Level]
	, [IsVirtual]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[FunctionType] WITH (NOLOCK) 
    WHERE 
	 ([FunctionTypeID] = @FunctionTypeID AND @FunctionTypeID is not null)
	OR ([ModuleID] = @ModuleID AND @ModuleID is not null)
	OR ([ParentFunctionTypeID] = @ParentFunctionTypeID AND @ParentFunctionTypeID is not null)
	OR ([FunctionTypeName] = @FunctionTypeName AND @FunctionTypeName is not null)
	OR ([Level] = @Level AND @Level is not null)
	OR ([IsVirtual] = @IsVirtual AND @IsVirtual is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[FunctionType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FunctionType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FunctionType_Find] TO [sp_executeall]
GO
