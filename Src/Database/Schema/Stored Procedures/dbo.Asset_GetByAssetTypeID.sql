SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Asset table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Asset_GetByAssetTypeID]
(

	@AssetTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[AssetID],
					[ClientID],
					[AssetTypeID],
					[AssetSubTypeID],
					[LocationID],
					[AssetName],
					[Note],
					[SecureNote],
					[SecureUserName],
					[SecurePassword],
					[SecureOther],
					[Version],
					[ValidFrom],
					[ValidTo],
					[ReminderDue],
					[ReminderTimeUnitID],
					[ReminderTimeUnitQuantity],
					[ReminderNotificationGroupID],
					[Enabled],
					[Deleted],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[Asset] WITH (NOLOCK) 
				WHERE
					[AssetTypeID] = @AssetTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Asset_GetByAssetTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Asset_GetByAssetTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Asset_GetByAssetTypeID] TO [sp_executeall]
GO
