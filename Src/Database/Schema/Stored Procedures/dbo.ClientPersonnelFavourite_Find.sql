SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientPersonnelFavourite table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelFavourite_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientPersonnelFavouriteID int   = null ,

	@ClientID int   = null ,

	@ClientPersonnelID int   = null ,

	@LeadID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientPersonnelFavouriteID]
	, [ClientID]
	, [ClientPersonnelID]
	, [LeadID]
    FROM
	[dbo].[ClientPersonnelFavourite] WITH (NOLOCK) 
    WHERE 
	 ([ClientPersonnelFavouriteID] = @ClientPersonnelFavouriteID OR @ClientPersonnelFavouriteID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientPersonnelFavouriteID]
	, [ClientID]
	, [ClientPersonnelID]
	, [LeadID]
    FROM
	[dbo].[ClientPersonnelFavourite] WITH (NOLOCK) 
    WHERE 
	 ([ClientPersonnelFavouriteID] = @ClientPersonnelFavouriteID AND @ClientPersonnelFavouriteID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelFavourite_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelFavourite_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelFavourite_Find] TO [sp_executeall]
GO
