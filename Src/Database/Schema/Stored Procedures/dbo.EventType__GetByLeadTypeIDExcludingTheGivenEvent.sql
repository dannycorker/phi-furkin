SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 06-11-2012
-- Description:	Gets a list of event types excluding the given event type
--              for the given leadtype
-- SB  2014-07-10 Updated to use view which includes shared event types
-- =============================================
CREATE PROCEDURE [dbo].[EventType__GetByLeadTypeIDExcludingTheGivenEvent]
	@ClientID INT,
	@LeadTypeID INT,	
	@EventTypeID INT
AS
BEGIN

	SELECT EventTypeID AutomatedEventTypeID, EventTypeName 
	FROM dbo.fnEventTypeShared(@ClientID)
	WHERE ClientID = @ClientID
	AND LeadTypeID = @LeadTypeID 
	AND EventTypeID <> @EventTypeID 
	ORDER BY EventTypeName
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetByLeadTypeIDExcludingTheGivenEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetByLeadTypeIDExcludingTheGivenEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetByLeadTypeIDExcludingTheGivenEvent] TO [sp_executeall]
GO
