SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventTypeMandatoryField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryField_Delete]
(

	@EventTypeMandatoryFieldID int   
)
AS


				DELETE FROM [dbo].[EventTypeMandatoryField] WITH (ROWLOCK) 
				WHERE
					[EventTypeMandatoryFieldID] = @EventTypeMandatoryFieldID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryField_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryField_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryField_Delete] TO [sp_executeall]
GO
