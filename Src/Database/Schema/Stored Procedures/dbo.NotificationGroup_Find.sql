SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the NotificationGroup table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NotificationGroup_Find]
(

	@SearchUsingOR bit   = null ,

	@NotificationGroupID int   = null ,

	@ClientID int   = null ,

	@NotificationGroupName varchar (255)  = null ,

	@Enabled bit   = null ,

	@Deleted bit   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [NotificationGroupID]
	, [ClientID]
	, [NotificationGroupName]
	, [Enabled]
	, [Deleted]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[NotificationGroup] WITH (NOLOCK) 
    WHERE 
	 ([NotificationGroupID] = @NotificationGroupID OR @NotificationGroupID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([NotificationGroupName] = @NotificationGroupName OR @NotificationGroupName IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([Deleted] = @Deleted OR @Deleted IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [NotificationGroupID]
	, [ClientID]
	, [NotificationGroupName]
	, [Enabled]
	, [Deleted]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[NotificationGroup] WITH (NOLOCK) 
    WHERE 
	 ([NotificationGroupID] = @NotificationGroupID AND @NotificationGroupID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([NotificationGroupName] = @NotificationGroupName AND @NotificationGroupName is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([Deleted] = @Deleted AND @Deleted is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroup_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationGroup_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationGroup_Find] TO [sp_executeall]
GO
