SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-03-05
-- Description:	Get enabled lookup values
-- Modified:	2014-07-25	SB	Added function for shared lead types
-- =============================================
CREATE PROCEDURE [dbo].[LookupListItems__GetEnabledItems]
@LookupListID int,
@ClientID INT = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT *
	FROM dbo.fnLookupListItemsShared(@ClientID) luli 
	WHERE luli.LookupListID = @LookupListID
	AND luli.Enabled = 1

END


GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems__GetEnabledItems] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupListItems__GetEnabledItems] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems__GetEnabledItems] TO [sp_executeall]
GO
