SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PortalUserCase table
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[PortalUserCase_Insert]
(

	@PortalUserCaseID INT    OUTPUT,

	@PortalUserID INT   ,

	@LeadID INT   ,

	@CaseID INT   ,

	@ClientID INT   
)
AS
BEGIN

	IF @ClientID IS NULL
	BEGIN
		SELECT @ClientID = ClientID 
		FROM dbo.PortalUser pu WITH (NOLOCK) 
		WHERE pu.PortalUserID = @PortalUserID 
	END
					
	INSERT INTO [dbo].[PortalUserCase]
		(
		[PortalUserID]
		,[LeadID]
		,[CaseID]
		,[ClientID]
		)
	VALUES
		(
		@PortalUserID
		,@LeadID
		,@CaseID
		,@ClientID
		)
	
	-- Get the identity value
	SET @PortalUserCaseID = SCOPE_IDENTITY()
						
						
END			
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserCase_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase_Insert] TO [sp_executeall]
GO
