SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-01-19
-- Description:	Populate the CurrencyRate table after the CurrencyRateImport data has been loaded from XE
-- =============================================
CREATE PROCEDURE [dbo].[CurrencyRate__ImportLatestRates] 
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @RateCount int,
	@ResultMessage varchar(1024) 
	
	/*
		This gets called by a Windows scheduled task on DB2.
		
		It uses logparser to get the data from XE and load it into the CurrencyRateImport table.
		
		This proc needs to add a join to get the unique CurrencyID from the Currency table,
		then perform the insert into the CurrencyRate table.
	*/
	SELECT @RateCount = COUNT(*) 
	FROM dbo.CurrencyRateImport cri WITH (NOLOCK) 
	
	IF @RateCount > 0
	BEGIN
		SELECT @ResultMessage = 'Success : ' + CAST(@RateCount as varchar) + ' rates found in the CurrencyRateImport table'
		
		INSERT INTO dbo.CurrencyRate (CurrencyId, CurrencyCode, ConversionDate, FromGBPRate, ToGBPRate, IsLatest)
		SELECT c.CurrencyId, c.CurrencyCode, CAST(cri.hvalue as datetime), cri.crate, (1/cri.crate), 1
		FROM dbo.CurrencyRateImport cri 
		INNER JOIN dbo.Currency c ON c.CurrencyCode = cri.csymbol 
		
		/* Clear the work table ready for the next day */
		DELETE FROM dbo.CurrencyRateImport
	END
	ELSE
	BEGIN
		SELECT @ResultMessage = 'Error : No rates found in the CurrencyRateImport table!'
	END
	EXEC dbo._C00_LogIt 'Info', 'CurrencyRate__ImportLatestRates', 'CurrencyRateImport', @ResultMessage, NULL
	SELECT @ResultMessage AS [Result]
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate__ImportLatestRates] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CurrencyRate__ImportLatestRates] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate__ImportLatestRates] TO [sp_executeall]
GO
