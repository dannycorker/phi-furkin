SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadViewHistory table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadViewHistory_GetByLeadViewHistoryID]
(

	@LeadViewHistoryID int   
)
AS


				SELECT
					[LeadViewHistoryID],
					[ClientPersonnelID],
					[ClientID],
					[LeadID],
					[WhenViewed]
				FROM
					[dbo].[LeadViewHistory] WITH (NOLOCK) 
				WHERE
										[LeadViewHistoryID] = @LeadViewHistoryID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_GetByLeadViewHistoryID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadViewHistory_GetByLeadViewHistoryID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_GetByLeadViewHistoryID] TO [sp_executeall]
GO
