SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-02-25
-- Description:	Show all valid date/time conversion formats
-- =============================================
CREATE PROCEDURE [dbo].[dmy] 
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @style_no_century tinyint, @style_century tinyint

	SET @style_no_century = 0
	SET @style_century = 100

	WHILE @style_no_century <= 25 
	BEGIN
		BEGIN TRY
			PRINT CONVERT (varchar, @style_no_century) + ' - ' + CONVERT(varchar, dbo.fn_GetDate_Local(), @style_no_century)
		END TRY
		BEGIN CATCH
			PRINT CONVERT (varchar, @style_no_century) --+ ' is not a valid format'
		END CATCH
	 PRINT ' ' --'----------------------------------'
	 SET @style_no_century = @style_no_century + 1
	END

	PRINT ' ' 
	 
	WHILE @style_century <= 127
	BEGIN
		BEGIN TRY
			PRINT CONVERT (varchar, @style_century) + ' - ' + CONVERT(varchar, dbo.fn_GetDate_Local(), @style_century)
		END TRY
		BEGIN CATCH
			PRINT CONVERT (varchar, @style_century) --+ ' is not a valid format'
		END CATCH
	 PRINT ' ' --'----------------------------------'
	 SET @style_century = @style_century + 1
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[dmy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[dmy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[dmy] TO [sp_executeall]
GO
