SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 28/07/2017
-- Description:	Inserts a CardTransactionLeadEventLink record
-- Modified By PR 21/08/2017 Added CardTransactionID to Payment Record
-- Modified By TD 29/03/2019 
-- Updating to populate the OrderCode, in the cardTransaction table this is named as "OrderID", I don't know if these are supposed to be different or have just been named differently.
-- I don't know where the order code was intended to be populated. This proc is called immediately after a world pay response 
-- from project: DLGTRAVEL, WorldPayService.cs - SendPaymentRequest.
-- =============================================
CREATE PROCEDURE [dbo].[CardTransactionLeadEventLink__Insert]

	@ClientID INT, 
	@CardTransactionID INT, 
	@LeadEventID INT = NULL, 
	@WorldPayRequestID INT = NULL, 
	@WorldPayResponseID INT = NULL,
	@WorldPayPaymentUrl VARCHAR(MAX) = NULL,
	@OrderCode VARCHAR(100) = NULL
AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO [CardTransactionLeadEventLink]
           ([ClientID]
           ,[CardTransactionID]
           ,[LeadEventID]
           ,[WorldPayRequestID]
           ,[WorldPayResponseID]
           ,[WorldPayPaymentUrl])
     VALUES
           (@ClientID,
           @CardTransactionID,
           @LeadEventID, 
           @WorldPayRequestID, 
           @WorldPayResponseID, 
           @WorldPayPaymentUrl)
		   		   
	DECLARE @CardTransactionLeadEventID INT 
	SELECT @CardTransactionLeadEventID = SCOPE_IDENTITY()

	IF @OrderCode IS NOT NULL
	BEGIN

		UPDATE TOP (1) CT
		SET CT.orderID = @OrderCode
		FROM CardTransaction CT
		WHERE CT.CardTransactionID = @CardTransactionID

	END 
	DECLARE @PaymentID INT
	SELECT @PaymentID=PaymentID FROM Payment WITH (NOLOCK) 
	WHERE LeadEventID=@LeadEventID
	
	IF @PaymentID>0
	BEGIN
	
		UPDATE Payment
		SET CardTransactionID=@CardTransactionID
		WHERE PaymentID=@PaymentID
	
	END	
	

	SELECT @CardTransactionLeadEventID [CardTransactionLeadEventID]

END

GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransactionLeadEventLink__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CardTransactionLeadEventLink__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransactionLeadEventLink__Insert] TO [sp_executeall]
GO
