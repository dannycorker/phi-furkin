SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ObjectType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectType_Insert]
(

	@ObjectTypeID int    OUTPUT,

	@ClientID int   ,

	@SubClientID int   ,

	@ObjectTypeName varchar (50)  ,

	@ObjectTypeDescription varchar (255)  ,

	@Enabled bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[ObjectType]
					(
					[ClientID]
					,[SubClientID]
					,[ObjectTypeName]
					,[ObjectTypeDescription]
					,[Enabled]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@SubClientID
					,@ObjectTypeName
					,@ObjectTypeDescription
					,@Enabled
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @ObjectTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectType_Insert] TO [sp_executeall]
GO
