SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ThirdPartyFieldMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldMapping_Insert]
(

	@ThirdPartyFieldMappingID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@ThirdPartyFieldID int   ,

	@DetailFieldID int   ,

	@ColumnFieldID int   ,

	@ThirdPartyFieldGroupID int   ,

	@IsEnabled bit   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[ThirdPartyFieldMapping]
					(
					[ClientID]
					,[LeadTypeID]
					,[ThirdPartyFieldID]
					,[DetailFieldID]
					,[ColumnFieldID]
					,[ThirdPartyFieldGroupID]
					,[IsEnabled]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@ThirdPartyFieldID
					,@DetailFieldID
					,@ColumnFieldID
					,@ThirdPartyFieldGroupID
					,@IsEnabled
					,@WhenCreated
					,@WhenModified
					)
				-- Get the identity value
				SET @ThirdPartyFieldMappingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMapping_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping_Insert] TO [sp_executeall]
GO
