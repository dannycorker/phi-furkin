SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the AquariumOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumOption_Get_List]

AS


				
				SELECT
					[AquariumOptionID],
					[AquariumOptionTypeID],
					[AquariumOptionName],
					[AquariumOptionDescription]
				FROM
					[dbo].[AquariumOption] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOption_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumOption_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumOption_Get_List] TO [sp_executeall]
GO
