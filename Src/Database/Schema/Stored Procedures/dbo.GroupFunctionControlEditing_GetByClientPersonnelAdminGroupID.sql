SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the GroupFunctionControlEditing table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupFunctionControlEditing_GetByClientPersonnelAdminGroupID]
(

	@ClientPersonnelAdminGroupID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[GroupFunctionControlEditingID],
					[ClientPersonnelAdminGroupID],
					[ModuleID],
					[FunctionTypeID],
					[HasDescendants],
					[RightID],
					[LeadTypeID]
				FROM
					[dbo].[GroupFunctionControlEditing] WITH (NOLOCK) 
				WHERE
					[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControlEditing_GetByClientPersonnelAdminGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupFunctionControlEditing_GetByClientPersonnelAdminGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControlEditing_GetByClientPersonnelAdminGroupID] TO [sp_executeall]
GO
