SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-03-18
-- Description:	Log Backup start/end
-- =============================================
CREATE PROCEDURE [dbo].[_C00_LogBackupStartEnd]
	@ServerName VARCHAR(100),
	@LogMessage VARCHAR(200)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO _SystemApplicationLog (Source,EventLog,RecordNumber,TimeGenerated,TimeWritten,EventID,EventType,EventTypeName,EventCategory,EventCategoryName,SourceName,Strings,ComputerName,SID,Message,Data)
	VALUES (@ServerName,'Backup',1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local(),7036,4,'Information event',0,'None','Backup',@LogMessage,@ServerName,'HGKHJ',@LogMessage,'00')

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LogBackupStartEnd] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_LogBackupStartEnd] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LogBackupStartEnd] TO [sp_executeall]
GO
