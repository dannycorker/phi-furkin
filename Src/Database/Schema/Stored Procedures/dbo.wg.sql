SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-02-22
-- Description:	List all Workflow Group records for a client
-- =============================================
CREATE PROCEDURE [dbo].[wg] 
	@ClientID int,
	@IncludeEmpty bit = 1,
	@IncludeDisabled bit = 1
AS
BEGIN
	SET NOCOUNT ON;
	
	with GroupsAndAssignments as (	SELECT a.WorkflowGroupID, COUNT(a.WorkflowGroupAssignmentID) [Users]
									FROM dbo.WorkflowGroupAssignment a WITH (NOLOCK) 
									WHERE a.ClientID = @ClientID
									GROUP BY a.WorkflowGroupID )
	SELECT wg.WorkflowGroupID, wg.Name, COUNT(wt.WorkflowTaskID) [Tasks], isnull(ga.Users,0) [Assigned Users], cp.UserName [Manager], wg.Enabled, wg.AssignedLeadsOnly, wg.SortOrder, 'aq 1,12,' + CONVERT(varchar,wg.WorkflowGroupID) [Describe]
	FROM dbo.WorkflowGroup wg WITH (NOLOCK) 
	LEFT JOIN dbo.WorkflowTask wt WITH (NOLOCK) on wt.WorkflowGroupID = wg.WorkflowGroupID 
	LEFT JOIN GroupsAndAssignments ga WITH (NOLOCK) on ga.WorkflowGroupID = wg.WorkflowGroupID
	LEFT JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = wg.ManagerID
	WHERE wg.ClientID = @ClientID 
	and ( wt.WorkflowTaskID is not null or @IncludeEmpty = 1 )
	and ( wg.Enabled = 1 or @IncludeDisabled = 1 )
	GROUP BY wg.WorkflowGroupID, wg.Name, ga.Users, wg.Enabled, cp.UserName,AssignedLeadsOnly, wg.SortOrder
	ORDER BY wg.WorkflowGroupID 	
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[wg] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[wg] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[wg] TO [sp_executeall]
GO
