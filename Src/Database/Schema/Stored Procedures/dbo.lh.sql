SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-15
-- Description:	Quick check of the last [n] LoginHistory entries in reverse order
-- =============================================
CREATE PROCEDURE [dbo].[lh]
	@rows int = 50,
	@clientid int = null,
	@userid int = null,
	@FailedOnly bit = 0
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP (@rows) lh.*, cp.username, cp.clientid 
	FROM dbo.LoginHistory lh (nolock) 
	LEFT JOIN dbo.ClientPersonnel cp (nolock) ON cp.ClientPersonnelID = lh.ClientPersonnelID 
	WHERE (@clientid IS NULL OR cp.ClientID = @clientid)
	AND (@userid IS NULL OR lh.ClientPersonnelID = @userid) 
	AND (@FailedOnly = 0 OR lh.IsSuccess = 0)
	ORDER BY 1 DESC 

END




GO
GRANT VIEW DEFINITION ON  [dbo].[lh] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[lh] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[lh] TO [sp_executeall]
GO
