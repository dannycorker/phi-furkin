SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChartSeriesType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeriesType_GetByChartSeriesTypeID]
(

	@ChartSeriesTypeID int   
)
AS


				SELECT
					[ChartSeriesTypeID],
					[ChartSeriesTypeName]
				FROM
					[dbo].[ChartSeriesType] WITH (NOLOCK) 
				WHERE
										[ChartSeriesTypeID] = @ChartSeriesTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesType_GetByChartSeriesTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeriesType_GetByChartSeriesTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesType_GetByChartSeriesTypeID] TO [sp_executeall]
GO
