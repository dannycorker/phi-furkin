SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DocumentQueue table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentQueue_Find]
(

	@SearchUsingOR bit   = null ,

	@DocumentQueueID int   = null ,

	@ClientID int   = null ,

	@CustomerID int   = null ,

	@LeadID int   = null ,

	@CaseID int   = null ,

	@DocumentTypeID int   = null ,

	@WhoCreated int   = null ,

	@WhenStored datetime   = null ,

	@WhenCreated datetime   = null ,

	@RequiresApproval bit   = null ,

	@ParsedDocumentTitle varchar (1000)  = null ,

	@BasedUponLeadEventID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DocumentQueueID]
	, [ClientID]
	, [CustomerID]
	, [LeadID]
	, [CaseID]
	, [DocumentTypeID]
	, [WhoCreated]
	, [WhenStored]
	, [WhenCreated]
	, [RequiresApproval]
	, [ParsedDocumentTitle]
	, [BasedUponLeadEventID]
    FROM
	[dbo].[DocumentQueue] WITH (NOLOCK) 
    WHERE 
	 ([DocumentQueueID] = @DocumentQueueID OR @DocumentQueueID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([DocumentTypeID] = @DocumentTypeID OR @DocumentTypeID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenStored] = @WhenStored OR @WhenStored IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([RequiresApproval] = @RequiresApproval OR @RequiresApproval IS NULL)
	AND ([ParsedDocumentTitle] = @ParsedDocumentTitle OR @ParsedDocumentTitle IS NULL)
	AND ([BasedUponLeadEventID] = @BasedUponLeadEventID OR @BasedUponLeadEventID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DocumentQueueID]
	, [ClientID]
	, [CustomerID]
	, [LeadID]
	, [CaseID]
	, [DocumentTypeID]
	, [WhoCreated]
	, [WhenStored]
	, [WhenCreated]
	, [RequiresApproval]
	, [ParsedDocumentTitle]
	, [BasedUponLeadEventID]
    FROM
	[dbo].[DocumentQueue] WITH (NOLOCK) 
    WHERE 
	 ([DocumentQueueID] = @DocumentQueueID AND @DocumentQueueID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([DocumentTypeID] = @DocumentTypeID AND @DocumentTypeID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenStored] = @WhenStored AND @WhenStored is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([RequiresApproval] = @RequiresApproval AND @RequiresApproval is not null)
	OR ([ParsedDocumentTitle] = @ParsedDocumentTitle AND @ParsedDocumentTitle is not null)
	OR ([BasedUponLeadEventID] = @BasedUponLeadEventID AND @BasedUponLeadEventID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentQueue_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue_Find] TO [sp_executeall]
GO
