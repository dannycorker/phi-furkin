SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the FolderType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FolderType_Delete]
(

	@FolderTypeID int   
)
AS


				DELETE FROM [dbo].[FolderType] WITH (ROWLOCK) 
				WHERE
					[FolderTypeID] = @FolderTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FolderType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FolderType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FolderType_Delete] TO [sp_executeall]
GO
