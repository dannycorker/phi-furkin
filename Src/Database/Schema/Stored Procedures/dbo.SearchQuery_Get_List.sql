SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SearchQuery table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchQuery_Get_List]

AS


				
				SELECT
					[SearchQueryID],
					[ClientID],
					[SubClientID],
					[ClientPersonnelAdminGroupID],
					[ClientPersonnelID],
					[SearchQueryName]
				FROM
					[dbo].[SearchQuery] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQuery_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchQuery_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchQuery_Get_List] TO [sp_executeall]
GO
