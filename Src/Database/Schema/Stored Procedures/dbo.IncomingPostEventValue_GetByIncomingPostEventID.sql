SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the IncomingPostEventValue table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncomingPostEventValue_GetByIncomingPostEventID]
(

	@IncomingPostEventID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[IncomingPostEventValueID],
					[IncomingPostEventID],
					[PostKey],
					[PostValue],
					[ClientID],
					[CustomerID],
					[LeadID],
					[CaseID],
					[MatterID]
				FROM
					[dbo].[IncomingPostEventValue] WITH (NOLOCK) 
				WHERE
					[IncomingPostEventID] = @IncomingPostEventID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEventValue_GetByIncomingPostEventID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncomingPostEventValue_GetByIncomingPostEventID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEventValue_GetByIncomingPostEventID] TO [sp_executeall]
GO
