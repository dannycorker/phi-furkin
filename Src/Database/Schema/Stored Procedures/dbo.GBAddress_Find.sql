SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the GBAddress table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GBAddress_Find]
(

	@SearchUsingOR bit   = null ,

	@GBAddressID int   = null ,

	@ClientID int   = null ,

	@WebServiceURL varchar (500)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [GBAddressID]
	, [ClientID]
	, [WebServiceURL]
    FROM
	[dbo].[GBAddress] WITH (NOLOCK) 
    WHERE 
	 ([GBAddressID] = @GBAddressID OR @GBAddressID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([WebServiceURL] = @WebServiceURL OR @WebServiceURL IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [GBAddressID]
	, [ClientID]
	, [WebServiceURL]
    FROM
	[dbo].[GBAddress] WITH (NOLOCK) 
    WHERE 
	 ([GBAddressID] = @GBAddressID AND @GBAddressID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([WebServiceURL] = @WebServiceURL AND @WebServiceURL is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[GBAddress_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GBAddress_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GBAddress_Find] TO [sp_executeall]
GO
