SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 23-02-2015
-- Description:	Gets the script detail field target by the detail field id and the scriptid and the sectionid
-- =============================================
CREATE PROCEDURE [dbo].[ScriptDetailFieldTarget__GetByDetailFieldIDAndScriptIDAndSectionID]
	@DetailFieldID INT  ,
	@ScriptID INT,
	@SectionID INT
AS
BEGIN

		SET ANSI_NULLS ON
		
		SELECT *			
		FROM
			[dbo].[ScriptDetailFieldTarget] WITH (NOLOCK) 
		WHERE
			[DetailFieldID] = @DetailFieldID AND
			[ScriptID] = @ScriptID AND
			[SectionID] = @SectionID
		
		SELECT @@ROWCOUNT
			
END


GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget__GetByDetailFieldIDAndScriptIDAndSectionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDetailFieldTarget__GetByDetailFieldIDAndScriptIDAndSectionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget__GetByDetailFieldIDAndScriptIDAndSectionID] TO [sp_executeall]
GO
