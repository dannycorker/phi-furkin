SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-01-13
-- Description: Gets the next collection date for an annual account
--              by calculating next date for all of the policies on
--              the account and choosing the earliest
-- Mods
-- 2016-03-03 DCM	Removed increment of from date (its already been incremented!)
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GetNextAnnualCollectionDate]
	@MatterID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CollectionsCaseID INT,
			@CollectionsMatterID INT,
			@FromDate DATE,
			@PaymentDay INT,
			@PolicyAdminMatterID INT
	
	DECLARE	@CandidateDates TABLE ( MatterID INT, NextDate DATE )
	
	DECLARE @Data TABLE ( FromDate DATE, Type INT )
	
	SELECT @CollectionsMatterID=dbo.fn_C600_GetCollectionsMatterID(@MatterID)
	SELECT @CollectionsCaseID=CaseID FROM Matter WITH (NOLOCK) WHERE MatterID=@CollectionsMatterID

	SELECT @PaymentDay=dbo.fnGetDvAsInt(170184,@CollectionsCaseID)
	
	-- Get all policy matters using this account where policy is live
	INSERT INTO @CandidateDates
	SELECT ltr.FromMatterID, NULL 
	FROM LeadTypeRelationship ltr WITH (NOLOCK) 
	INNER JOIN MatterDetailValues pstat WITH (NOLOCK) ON pstat.MatterID=ltr.FromMatterID AND pstat.DetailFieldID=170038
	WHERE ltr.ToMatterID=@CollectionsMatterID 
	AND ltr.FromLeadTypeID=1492 -- Policy Admin
	AND pstat.ValueInt=43002 -- status=live
	
	WHILE EXISTS ( SELECT * FROM @CandidateDates WHERE NextDate IS NULL )
	BEGIN
	
		SELECT @PolicyAdminMatterID=MatterID, @FromDate=NULL 
		FROM @CandidateDates WHERE NextDate IS NULL
			
		INSERT @Data (FromDate, Type)
		SELECT FromDate, Type
		FROM dbo.fn_C600_GetNextPremiumFromDate(@PolicyAdminMatterID, 69897)

		SELECT @FromDate = FromDate
		FROM @Data
		
		--SELECT @FromDate=DATEADD(YEAR,1,@FromDate)				
		
		IF @PaymentDay<DATEPART(DAY,@FromDate) -- next payment day is in next month
			SELECT @FromDate=DATEADD(MONTH,1,@FromDate)	
		
		UPDATE @CandidateDates 
		SET NextDate=CASE WHEN @FromDate IS NULL THEN '' ELSE @FromDate END 
		WHERE MatterID=@PolicyAdminMatterID
	
	END
	
	SELECT TOP 1 NextDate,MatterID 
	FROM @CandidateDates 
	ORDER BY NextDate
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetNextAnnualCollectionDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_GetNextAnnualCollectionDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetNextAnnualCollectionDate] TO [sp_executeall]
GO
