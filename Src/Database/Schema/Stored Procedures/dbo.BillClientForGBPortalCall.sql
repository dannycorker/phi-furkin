SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 28-01-2014
-- Description:	Adds a record to the client billing table for each gb portal call
-- =============================================
CREATE PROCEDURE [dbo].[BillClientForGBPortalCall]

	@ClientID INT,
	@ClientPersonnelID INT,
	@Message VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @ClientID = 2
	BEGIN
		INSERT INTO ClientBillingDetail(ClientID, SourceAquariumOptionID, ModuleID, WhenCreated, WhoCreated, ChargeInPence, Notes)
		VALUES (@ClientID, 63, 1,dbo.fn_GetDate_Local() ,@ClientPersonnelID, 0.0, @Message)
	END
	ELSE
	BEGIN
		INSERT INTO ClientBillingDetail(ClientID, SourceAquariumOptionID, ModuleID, WhenCreated, WhoCreated, ChargeInPence, Notes)
		VALUES (@ClientID, 63, 1,dbo.fn_GetDate_Local() ,@ClientPersonnelID, 4.0, @Message)	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[BillClientForGBPortalCall] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillClientForGBPortalCall] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillClientForGBPortalCall] TO [sp_executeall]
GO
