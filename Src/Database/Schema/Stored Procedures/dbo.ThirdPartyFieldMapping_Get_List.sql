SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ThirdPartyFieldMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldMapping_Get_List]

AS


				
				SELECT
					[ThirdPartyFieldMappingID],
					[ClientID],
					[LeadTypeID],
					[ThirdPartyFieldID],
					[DetailFieldID],
					[ColumnFieldID],
					[ThirdPartyFieldGroupID],
					[IsEnabled],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ThirdPartyFieldMapping] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMapping_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping_Get_List] TO [sp_executeall]
GO
