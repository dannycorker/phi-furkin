SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EsignatureApiKey table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EsignatureApiKey_Update]
(

	@EsignatureApiKeyID int   ,

	@ClientID int   ,

	@ApiKey varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EsignatureApiKey]
				SET
					[ClientID] = @ClientID
					,[ApiKey] = @ApiKey
				WHERE
[EsignatureApiKeyID] = @EsignatureApiKeyID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureApiKey_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EsignatureApiKey_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureApiKey_Update] TO [sp_executeall]
GO
