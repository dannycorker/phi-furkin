SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:        Jan Wilson
-- Create date:   2014-06-04
-- Description:   Returns all the rules which 
--                reference a specific ruleset
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleSets_GetReferences]
(
      @ClientID INT,
      @RuleSetID INT 
)
AS
BEGIN
      
    SET NOCOUNT ON;

	SELECT
		RS.Name + ' > ' + R.Name Name, RP.Value RuleSetID, RP.RuleID, RP.RuleParameterID
	FROM 
		dbo.RulesEngine_RuleParameters RP WITH (NOLOCK) 
		INNER JOIN dbo.RulesEngine_Rules R WITH (NOLOCK) 
		ON RP.RuleID = R.RuleID
		
		INNER JOIN dbo.RulesEngine_RuleSets RS WITH (NOLOCK) 
		ON R.RuleSetID = RS.RuleSetID AND RS.ClientID = @ClientID
		
	WHERE
		RP.ParameterTypeID = 3 AND RP.Value = @RuleSetID
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleSets_GetReferences] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleSets_GetReferences] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleSets_GetReferences] TO [sp_executeall]
GO
