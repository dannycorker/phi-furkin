SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the GBAddress table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GBAddress_Get_List]

AS


				
				SELECT
					[GBAddressID],
					[ClientID],
					[WebServiceURL]
				FROM
					[dbo].[GBAddress] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GBAddress_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GBAddress_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GBAddress_Get_List] TO [sp_executeall]
GO
