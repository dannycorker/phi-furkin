SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the EsignatureStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EsignatureStatus_Get_List]

AS


				
				SELECT
					[EsignatureStatusID],
					[EsignatureStatusDescription],
					[EsignatureStatus]
				FROM
					[dbo].[EsignatureStatus] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureStatus_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EsignatureStatus_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureStatus_Get_List] TO [sp_executeall]
GO
