SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger/Paul Richardson
-- Create date: 16/11/2016
-- Description:	Gets an account mandate
-- 2020-03-02	GPR for JIRA AAG-202 | Added check on CountryID from ClientID as the switch for removing the AccountMandate check
-- =============================================
CREATE PROCEDURE [dbo].[AccountMandate__GetByAccountID]
	@AccountID INT, 
	@ClientID INT
AS
BEGIN
	
	
	SET NOCOUNT ON;

	/*GPR 2020-03-02 for AAG-202*/
	DECLARE @CountryID INT
	SELECT @CountryID = CountryID FROM Clients WITH (NOLOCK) WHERE ClientID = @ClientID

	IF @CountryID = 14 /*Australia*/
	BEGIN
		RETURN 
	END
	ELSE
	BEGIN
	
		SELECT * FROM AccountMandate WITH (NOLOCK) 
		WHERE AccountID=@AccountID AND ClientID=@ClientID
    END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AccountMandate__GetByAccountID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AccountMandate__GetByAccountID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AccountMandate__GetByAccountID] TO [sp_executeall]
GO
