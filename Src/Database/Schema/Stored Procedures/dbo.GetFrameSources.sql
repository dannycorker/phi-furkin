SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetFrameSources] @ClientQuestionnaireID int

AS

Select QuestionnaireFrameSourceID, ClientID, ClientQuestionnaireID, PageNumber, SourceUrl, FrameType
From QuestionnaireFrameSources
Where ClientQuestionnaireID = @ClientQuestionnaireID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetFrameSources] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetFrameSources] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetFrameSources] TO [sp_executeall]
GO
