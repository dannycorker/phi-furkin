SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 01-May-2009
-- Description:	Proc 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_DuplicateCheck]
(
@LeadEventID int,
@NoOfDupes int,
@DetailFieldIDForCustID int = NULL,
@StoreAllDuplicateIDs int = NULL
)

AS
BEGIN
	SET NOCOUNT ON;

	Declare @ClientID int,
			@FirstName varchar(1000),
			@LastName varchar(1000),
			@Address1 varchar(1000),
			@PostCode varchar(1000),
			@DOB varchar(1000),
			@CustomerID int,
			@DuplicateCustomerIDs varchar(1000),
			@LeadOrMatter int,
			@LeadID int,
			@CaseID int

	Select @ClientID = l.ClientID, 
			@FirstName = FirstName, 
			@LastName = LastName, 
			@Address1 = Address1, 
			@PostCode = PostCode, 
			@DOB = DateOfBirth, 
			@LeadID = l.LeadID, 
			@CaseID = le.CaseID,
			@CustomerID = l.CustomerID
	From Customers with (nolock)
	Inner Join lead l on l.CustomerID = Customers.CustomerID
	Inner Join LeadEvent le on le.leadid = l.leadid and le.LeadEventID = @LeadEventID

	Select @DuplicateCustomerIDs = ''

	If @NoOfDupes = 2
	Begin

		Select @DuplicateCustomerIDs = convert(varchar,l.LeadID) + ', ' + @DuplicateCustomerIDs
		From Customers with (nolock)
		Inner Join lead l with (nolock) on l.customerid = customers.customerid
		Where LastName = @LastName
		And PostCode = @PostCode
		and l.ClientID = @ClientID
		and (Test = 0 or Test IS NULL)
		And l.CustomerID <> @CustomerID

	End

	If @NoOfDupes = 3
	Begin

		Select @DuplicateCustomerIDs = convert(varchar,l.LeadID) + ', ' + @DuplicateCustomerIDs
		From Customers with (nolock)
		Inner Join lead l with (nolock) on l.customerid = customers.customerid
		Where FirstName = @FirstName
		And LastName = @LastName
		And ((PostCode = @PostCode) or (DateOfBirth = @DOB))
		and l.ClientID = @ClientID
		and (Test = 0 or Test IS NULL)
		And l.CustomerID <> @CustomerID

	End

	If @NoOfDupes = 4
	Begin

		Select @DuplicateCustomerIDs = convert(varchar,l.LeadID) + ', ' + @DuplicateCustomerIDs
		From Customers with (nolock)
		Inner Join lead l with (nolock) on l.customerid = customers.customerid
		Where FirstName = @FirstName
		And LastName = @LastName
		And PostCode = @PostCode
		And DateOfBirth = @DOB
		and l.ClientID = @ClientID
		and (Test = 0 or Test IS NULL)
		And l.CustomerID <> @CustomerID

	End

	If @NoOfDupes = 5
	Begin

		Select @DuplicateCustomerIDs = convert(varchar,l.LeadID) + ', ' + @DuplicateCustomerIDs
		From Customers with (nolock)
		Inner Join lead l with (nolock) on l.customerid = customers.customerid
		Where FirstName = @FirstName
		And Address1 = @Address1
		And LastName = @LastName
		And PostCode = @PostCode
		And DateOfBirth = @DOB
		and l.ClientID = @ClientID
		and (Test = 0 or Test IS NULL)
		And l.CustomerID <> @CustomerID

	End

	Update LeadEvent
	Set Comments = Comments + char(10) + Char(13) + '<Br>Duplicate Lead Id''s: ' + @DuplicateCustomerIDs
	Where LeadEventID = @LeadEventID

	If @DetailFieldIDForCustID IS NOT NULL
	Begin

		Select @LeadOrMatter = LeadOrMatter
		From DetailFields
		Where DetailFieldID = @DetailFieldIDForCustID

		exec dbo._C00_CreateDetailFields @DetailFieldIDForCustID, @LeadID

		If @LeadOrMatter = 1
		Begin
	
			If @StoreAllDuplicateIDs is not null
			Begin
				Update LeadDetailValues
				Set DetailValue = @DuplicateCustomerIDs
				Where DetailFieldID = @DetailFieldIDForCustID
				And LeadID = @LeadID
			End
			Else
			Begin
				Update LeadDetailValues
				Set DetailValue = left(@DuplicateCustomerIDs,charindex(',',@DuplicateCustomerIDs + ',', 0)-1)
				Where DetailFieldID = @DetailFieldIDForCustID
				And LeadID = @LeadID
			End

		End

		If @LeadOrMatter = 2
		Begin

			If @StoreAllDuplicateIDs is not null
			Begin
				Update MatterDetailValues
				Set DetailValue = @DuplicateCustomerIDs
				From MatterDetailValues
				Inner Join Matter m on m.MatterID = MatterDetailValues.MatterID and m.CaseID = @CaseID
				Where DetailFieldID = @DetailFieldIDForCustID
			End
			Else
			Begin
				Update MatterDetailValues
				Set DetailValue = left(@DuplicateCustomerIDs,charindex(',',@DuplicateCustomerIDs + ',', 0)-1)
				From MatterDetailValues
				Inner Join Matter m on m.MatterID = MatterDetailValues.MatterID and m.CaseID = @CaseID
				Where DetailFieldID = @DetailFieldIDForCustID
			End

		End

	End

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DuplicateCheck] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_DuplicateCheck] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DuplicateCheck] TO [sp_executeall]
GO
