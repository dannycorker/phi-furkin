SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-04-06
-- Description:	Describe/create/copy EventTypeSql as required
-- 2020-01-16 Updated to use dynamic ClientID from function
-- =============================================
CREATE PROCEDURE [dbo].[ets]
(
	@ClientID int = null, 
	@EventTypeID int = null, 
	@CreateDefaultSql bit = null, 
	@CopyFromEventTypeID int = null, 
	@PostUpdateSql varchar (MAX) = null 
)
AS
BEGIN

	SELECT @ClientID = [dbo].[fnGetPrimaryClientID]() /*GPR 2020-01-16*/

	/* List all ETS by default */
	IF @CreateDefaultSql IS NULL AND @CopyFromEventTypeID IS NULL AND @PostUpdateSql IS NULL 
	BEGIN
	
		/* List all ETS by default */
		SELECT ets.* , et.EventTypeName
		FROM dbo.EventTypeSql ets WITH (NOLOCK) 
		INNER JOIN dbo.EventType et WITH (NOLOCK) on et.EventTypeID = ets.EventTypeID /*CS 2012-05-05 - so I can label my SAE with the event type name*/
		WHERE (ets.ClientID = @ClientID OR @ClientID IS NULL)
		AND (ets.EventTypeID = @EventTypeID OR @EventTypeID IS NULL) 
		ORDER BY ets.EventTypeID
	
	END
	ELSE
	BEGIN
		/* Copy one ETS record from another */
		IF @EventTypeID IS NOT NULL AND @CopyFromEventTypeID IS NOT NULL 
		BEGIN
		
			/* Upsert ETS */
			DELETE dbo.EventTypeSql 
			WHERE EventTypeID = @EventTypeID 
			
			INSERT INTO dbo.EventTypeSql (ClientID, EventTypeID, PostUpdateSql, IsNativeSql) 
			SELECT ets.ClientID, @EventTypeID, ets.PostUpdateSql, ets.IsNativeSql 
			FROM dbo.EventTypeSql ets WITH (NOLOCK) 
			WHERE ets.EventTypeID = @CopyFromEventTypeID 
			
		END
		ELSE
		BEGIN
			/* Create a default ETS record */
			IF @EventTypeID IS NOT NULL AND @CreateDefaultSql = 1
			BEGIN
			
				/* Upsert ETS */
				DELETE dbo.EventTypeSql 
				WHERE EventTypeID = @EventTypeID 
				
				INSERT INTO dbo.EventTypeSql (ClientID, EventTypeID, PostUpdateSql, IsNativeSql) 
				SELECT et.ClientID, @EventTypeID, 'EXEC dbo._C' + CAST(@ClientID AS VARCHAR) /*CASE WHEN et.ClientID BETWEEN 1 AND 10 THEN '0' ELSE '' END + CAST(et.ClientID as varchar)*/ + '_SAE @LeadEventID', 0 
				FROM dbo.EventType et WITH (NOLOCK) 
				WHERE et.EventTypeID = @EventTypeID 
			
			END
			ELSE
			BEGIN
				/* Create ETS record from value passed in */
				IF @EventTypeID IS NOT NULL AND @PostUpdateSql IS NOT NULL
				BEGIN
				
					/* Upsert ETS */
					DELETE dbo.EventTypeSql 
					WHERE EventTypeID = @EventTypeID 
					
					INSERT INTO dbo.EventTypeSql (ClientID, EventTypeID, PostUpdateSql, IsNativeSql) 
					SELECT et.ClientID, @EventTypeID, @PostUpdateSql, 0 
					FROM dbo.EventType et WITH (NOLOCK) 
					WHERE et.EventTypeID = @EventTypeID 
				
				END
			END
		END
		
		/* Show the results of any changes made */	
		EXEC dbo.ets @ClientID, @EventTypeID
		
	END
		
END



GO
GRANT VIEW DEFINITION ON  [dbo].[ets] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ets] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ets] TO [sp_executeall]
GO
