SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderFileStatus table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileStatus_GetByDataLoaderFileStatusID]
(

	@DataLoaderFileStatusID int   
)
AS


				SELECT
					[DataLoaderFileStatusID],
					[FileStatusName],
					[FileStatusDescription]
				FROM
					[dbo].[DataLoaderFileStatus] WITH (NOLOCK) 
				WHERE
										[DataLoaderFileStatusID] = @DataLoaderFileStatusID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileStatus_GetByDataLoaderFileStatusID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileStatus_GetByDataLoaderFileStatusID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileStatus_GetByDataLoaderFileStatusID] TO [sp_executeall]
GO
