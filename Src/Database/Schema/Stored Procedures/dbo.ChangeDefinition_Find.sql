SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ChangeDefinition table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeDefinition_Find]
(

	@SearchUsingOR bit   = null ,

	@ChangeDefinitionID int   = null ,

	@ClientID int   = null ,

	@ChangeDescription varchar (255)  = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@RequiredDate datetime   = null ,

	@Deleted bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ChangeDefinitionID]
	, [ClientID]
	, [ChangeDescription]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [RequiredDate]
	, [Deleted]
    FROM
	[dbo].[ChangeDefinition] WITH (NOLOCK) 
    WHERE 
	 ([ChangeDefinitionID] = @ChangeDefinitionID OR @ChangeDefinitionID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ChangeDescription] = @ChangeDescription OR @ChangeDescription IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([RequiredDate] = @RequiredDate OR @RequiredDate IS NULL)
	AND ([Deleted] = @Deleted OR @Deleted IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ChangeDefinitionID]
	, [ClientID]
	, [ChangeDescription]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [RequiredDate]
	, [Deleted]
    FROM
	[dbo].[ChangeDefinition] WITH (NOLOCK) 
    WHERE 
	 ([ChangeDefinitionID] = @ChangeDefinitionID AND @ChangeDefinitionID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ChangeDescription] = @ChangeDescription AND @ChangeDescription is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([RequiredDate] = @RequiredDate AND @RequiredDate is not null)
	OR ([Deleted] = @Deleted AND @Deleted is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDefinition_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeDefinition_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDefinition_Find] TO [sp_executeall]
GO
