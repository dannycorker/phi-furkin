SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the QuillConfigurationClient table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillConfigurationClient_Insert]
(

	@QuillConfigurationClientID int    OUTPUT,

	@AquariumClientID int   ,

	@QuillClientID int   ,

	@Username varchar (50)  ,

	@Password varchar (50)  ,

	@URI varchar (150)  
)
AS


				
				INSERT INTO [dbo].[QuillConfigurationClient]
					(
					[AquariumClientID]
					,[QuillClientID]
					,[Username]
					,[Password]
					,[URI]
					)
				VALUES
					(
					@AquariumClientID
					,@QuillClientID
					,@Username
					,@Password
					,@URI
					)
				-- Get the identity value
				SET @QuillConfigurationClientID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationClient_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillConfigurationClient_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationClient_Insert] TO [sp_executeall]
GO
