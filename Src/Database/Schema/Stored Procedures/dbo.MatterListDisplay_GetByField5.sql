SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the MatterListDisplay table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterListDisplay_GetByField5]
(

	@Field5 int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[MatterListDisplayID],
					[ClientID],
					[LeadTypeID],
					[MatterRef],
					[MatterStatus],
					[MatterID],
					[Field1],
					[Field2],
					[Field3],
					[Field4],
					[Field5],
					[Field1ColumnDetailFieldID],
					[Field2ColumnDetailFieldID],
					[Field3ColumnDetailFieldID],
					[Field4ColumnDetailFieldID],
					[Field5ColumnDetailFieldID],
					[SqlQueryText]
				FROM
					[dbo].[MatterListDisplay] WITH (NOLOCK) 
				WHERE
					[Field5] = @Field5
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterListDisplay_GetByField5] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterListDisplay_GetByField5] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterListDisplay_GetByField5] TO [sp_executeall]
GO
