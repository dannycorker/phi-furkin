SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Folder table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Folder_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[FolderID],
					[ClientID],
					[FolderTypeID],
					[FolderParentID],
					[FolderName],
					[FolderDescription],
					[WhenCreated],
					[WhoCreated],
					[Personal],
					[SourceID]
				FROM
					[dbo].[Folder] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Folder_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Folder_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Folder_GetByClientID] TO [sp_executeall]
GO
