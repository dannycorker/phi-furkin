SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the UserGroupMenuOption table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserGroupMenuOption_Find]
(

	@SearchUsingOR bit   = null ,

	@UserGroupMenuOptionID int   = null ,

	@ClientPersonnelAdminGroupID int   = null ,

	@ClientID int   = null ,

	@MasterPageName varchar (20)  = null ,

	@PanelItemAction varchar (10)  = null ,

	@PanelItemName varchar (50)  = null ,

	@PanelItemCaption varchar (50)  = null ,

	@PanelItemIcon varchar (50)  = null ,

	@PanelItemURL varchar (250)  = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@Comments varchar (250)  = null ,

	@ItemOrder int   = null ,

	@ShowInFancyBox bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [UserGroupMenuOptionID]
	, [ClientPersonnelAdminGroupID]
	, [ClientID]
	, [MasterPageName]
	, [PanelItemAction]
	, [PanelItemName]
	, [PanelItemCaption]
	, [PanelItemIcon]
	, [PanelItemURL]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [Comments]
	, [ItemOrder]
	, [ShowInFancyBox]
    FROM
	[dbo].[UserGroupMenuOption] WITH (NOLOCK) 
    WHERE 
	 ([UserGroupMenuOptionID] = @UserGroupMenuOptionID OR @UserGroupMenuOptionID IS NULL)
	AND ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID OR @ClientPersonnelAdminGroupID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([MasterPageName] = @MasterPageName OR @MasterPageName IS NULL)
	AND ([PanelItemAction] = @PanelItemAction OR @PanelItemAction IS NULL)
	AND ([PanelItemName] = @PanelItemName OR @PanelItemName IS NULL)
	AND ([PanelItemCaption] = @PanelItemCaption OR @PanelItemCaption IS NULL)
	AND ([PanelItemIcon] = @PanelItemIcon OR @PanelItemIcon IS NULL)
	AND ([PanelItemURL] = @PanelItemURL OR @PanelItemURL IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([Comments] = @Comments OR @Comments IS NULL)
	AND ([ItemOrder] = @ItemOrder OR @ItemOrder IS NULL)
	AND ([ShowInFancyBox] = @ShowInFancyBox OR @ShowInFancyBox IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [UserGroupMenuOptionID]
	, [ClientPersonnelAdminGroupID]
	, [ClientID]
	, [MasterPageName]
	, [PanelItemAction]
	, [PanelItemName]
	, [PanelItemCaption]
	, [PanelItemIcon]
	, [PanelItemURL]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [Comments]
	, [ItemOrder]
	, [ShowInFancyBox]
    FROM
	[dbo].[UserGroupMenuOption] WITH (NOLOCK) 
    WHERE 
	 ([UserGroupMenuOptionID] = @UserGroupMenuOptionID AND @UserGroupMenuOptionID is not null)
	OR ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID AND @ClientPersonnelAdminGroupID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([MasterPageName] = @MasterPageName AND @MasterPageName is not null)
	OR ([PanelItemAction] = @PanelItemAction AND @PanelItemAction is not null)
	OR ([PanelItemName] = @PanelItemName AND @PanelItemName is not null)
	OR ([PanelItemCaption] = @PanelItemCaption AND @PanelItemCaption is not null)
	OR ([PanelItemIcon] = @PanelItemIcon AND @PanelItemIcon is not null)
	OR ([PanelItemURL] = @PanelItemURL AND @PanelItemURL is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([Comments] = @Comments AND @Comments is not null)
	OR ([ItemOrder] = @ItemOrder AND @ItemOrder is not null)
	OR ([ShowInFancyBox] = @ShowInFancyBox AND @ShowInFancyBox is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuOption_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption_Find] TO [sp_executeall]
GO
