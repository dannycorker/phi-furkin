SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.AddTracking    Script Date: 08/09/2006 12:22:41 ******/

CREATE PROCEDURE [dbo].[AddTracking]
@TrackingName nvarchar(100),
@TrackingDescription nvarchar(255),
@StartDate datetime,
@EndDate datetime,
@ClientQuestionnaireID int,
@ClientID int

as

insert into Tracking ( TrackingName, TrackingDescription, StartDate, EndDate, ClientQuestionnaireID, ClientID )
values ( @TrackingName, @TrackingDescription, @StartDate, @EndDate, @ClientQuestionnaireID, @ClientID )




GO
GRANT VIEW DEFINITION ON  [dbo].[AddTracking] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddTracking] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddTracking] TO [sp_executeall]
GO
