SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Update documents
-- =============================================
CREATE PROCEDURE [dbo].[_C00_RenameEvents]
(
	@EventTypeID nvarchar(100),
	@EventTypeName nvarchar(1000)
)

AS
BEGIN
	SET NOCOUNT ON;

	Update EventType
	Set EventTypeName = @EventTypeName, EventTypeDescription = @EventTypeName
	From EventType
	Where EventTypeID = @EventTypeID

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RenameEvents] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_RenameEvents] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RenameEvents] TO [sp_executeall]
GO
