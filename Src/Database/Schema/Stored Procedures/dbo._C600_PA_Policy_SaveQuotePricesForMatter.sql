SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-01-23
-- Description:	Store some quote values for a policy
-- Modified:	2018-03-13 Mark Beaumont - modified Cathal's original Buddies implementation for L&G
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Policy_SaveQuotePricesForMatter]
(
	 @MatterID		INT
	,@LeadEventID	INT
)
AS
BEGIN

	SET NoCount ON
	
	DECLARE  @CustomerID						INT
			,@LeadID							INT
			,@XmlRequest						XML
			,@Success							INT
			,@DetailFieldID						INT
			,@WhoCreated						INT
			,@ErrorMessage						VARCHAR(500)
			,@DiscountID						INT
			,@ProductID							INT
			,@SaveAnnualQuotePriceInFieldID		INT
			,@SaveMonthlyQuotePriceInFieldID	INT
			,@XmlRequestStub					XML
			,@XMLProductRequestNode				XML
			,@NumProductsToQuote				INT = 0
			,@ProductCounter					INT
			,@XmlProductResponse				XML
			,@AnnualQuotePrice					MONEY
			,@MonthlyQuotePrice					MONEY
			,@VoluntaryExcess					DECIMAL (3,2)
			,@OMFQuoteEndDate					DATE
			,@QuoteSessionID					INT
			,@NumBuddiesPolicies				INT
			,@ClientID							INT = dbo.fnGetPrimaryClientID()

	DECLARE @FieldsAndValues TABLE ( SchemeID INT, PremiumValue DECIMAL(18,2), Monthly DECIMAL(18,2) )
	DECLARE @DetailValueData dbo.tvpDetailValueData
	DECLARE @BlackHole TABLE ( PassedID INT, DetailValueID INT )
	DECLARE	@ProductsToQuote TABLE ( ProductID INT, SaveAnnualQuotePriceInFieldID INT, SaveMonthlyQuotePriceInFieldID INT )
	
	INSERT INTO @ProductsToQuote ( ProductID, SaveAnnualQuotePriceInFieldID, SaveMonthlyQuotePriceInFieldID ) 
		VALUES
			( 151573, 179942, 179944 ),	/* Buddies Standard */
			( 151575, 179945, 179946 ),	/* Buddies Premium */
			( 151576, 179947, 179948 )	/* Buddies Premium Plus */

	/* Pick up necessary details to prepare a quote request */
	SELECT	 @MatterID	 = m.MatterID
			,@WhoCreated = le.WhoCreated
			,@CustomerID = m.CustomerID
			,@LeadID	 = m.LeadID
			,@ClientID	 = m.ClientID
	FROM 	LeadEvent le WITH ( NOLOCK ) 
			INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID = le.CaseID
	WHERE 	( le.LeadEventID = @LeadEventID OR ( @LeadEventID IS NULL AND m.MatterID = @MatterID ) )

	IF @MatterID IS NULL
	BEGIN
		SELECT @ErrorMessage = '<BR><BR><font color="red">Error:  MatterID is empty</font>'
		RAISERROR( @ErrorMessage, 16, 1 )
		RETURN
	END

	/* If Customer has no affinity, set to the client detail affinity */
	IF ISNULL(dbo.fnGetSimpleDvAsInt(170144, @CustomerID), 0) = 0	
	BEGIN
		DECLARE @AffinityResourceListID INT = dbo.fnGetSimpleDvAsInt(177910,@ClientID) /* Default Affinity ResourceListID for Client 600 */
		EXEC dbo._C00_SimpleValueIntoField 170144, @AffinityResourceListID, @CustomerID, @WhoCreated /* Set Customer Affinity Details */
	END
	
	/* Get the number of Buddies live policies */
	SELECT		@NumBuddiesPolicies = COUNT (l.LeadID)
	FROM 		Lead l WITH (NOLOCK)
				INNER JOIN Matter m WITH ( NOLOCK ) ON l.LeadID = m.LeadID
				INNER JOIN MatterDetailValues mdv_PolicyStatus WITH ( NOLOCK ) ON mdv_PolicyStatus.MatterID = m.MatterID 
					AND mdv_PolicyStatus.DetailFieldID = 170038			-- Policy Status
				INNER JOIN MatterDetailValues mdv_CurrentPolicy WITH ( NOLOCK ) ON mdv_CurrentPolicy.MatterID = m.MatterID 
					AND mdv_CurrentPolicy.DetailFieldID = 170034		-- Current Policy
				INNER JOIN ResourceListDetailValues rldv_AffinityShortCode WITH ( NOLOCK ) ON rldv_AffinityShortCode.ResourceListID = mdv_CurrentPolicy.ValueInt
					AND rldv_AffinityShortCode.DetailFieldID = 175269
	WHERE 		l.CustomerID = @CustomerID
				AND l.ClientID = @ClientID
				AND l.LeadTypeID = 1492		/* Policy Admin */
				AND mdv_PolicyStatus.ValueInt IN ( 43002 /* Live */, 74573 /* With Underwriting */ )
				AND rldv_AffinityShortCode.ValueInt = 74539 	/* Buddies */
	
	/*Construct a stub XML Request*/
	SELECT @XmlRequestStub  =
	(
	SELECT	@ClientID 									[ClientId]
			,0		 									[ClientPersonnelId]
			,'' 										[Password]
			,ISNULL(NULLIF(dbo.fnGetSimpleDvAsInt(170144,@CustomerID),0),153391) [BrandingId] /* Customer Affinity Details (default to Buddies) */
			,0 											[QuoteId]
			,0 											[SessionId]
			, CAST (
			(
				SELECT 	cu.CustomerID 					[CustomerID]
						,ISNULL(cu.TitleID,0) 			[TitleID]
						,cu.FirstName 					[FirstName]
						,cu.LastName					[LastName]
						,ISNULL(cu.HomeTelephone,'')	[HomePhone]
						,ISNULL(cu.MobileTelephone,'')	[MobilePhone]
						,ISNULL(cu.EmailAddress,'')		[Email]
						,cu.Address1					[Address1]
						,ISNULL(cu.Address2,'')			[Address2]
						,cu.Town						[TownCity]
						,cu.PostCode 					[Postcode]
						,0								[SecondaryTitleId]
						,''								[SecondaryFirstName]
						,''								[SecondaryLastName]
						,''								[SecondaryEmail]
						,''								[SecondaryHomePhone]
						,''								[SecondaryMobilePhone]
						,'true'							[ExistingPolicyHolder]
						,CASE WHEN ISNULL(cdv_DoNotSms.ValueInt,1) = 1 THEN 'true' ELSE cdv_DoNotSms.ValueInt END	[DoNotSms] /*2019-05-07 1690 AMG, GPR*/
						,CASE WHEN ISNULL(cdv_DoNotEmail.ValueInt,1) = 1 THEN 'true' ELSE cdv_DoNotEmail.ValueInt END	[DoNotEmail]
						,CASE WHEN ISNULL(cdv_DoNotPhone.ValueInt,1) = 1 THEN 'true' ELSE cdv_DoNotPhone.ValueInt END	[DoNotPhone]
						,CASE WHEN ISNULL(cdv_DoNotPost.ValueInt,1) = 1 THEN 'true' ELSE cdv_DoNotPost.ValueInt END	[DoNotPost]
						,cu.DateOfBirth					[DateOfBirth]
						,ISNULL(cdv_ContactMethod.ValueInt,60789)		[ContactMethodId]
						,ISNULL(cdv_MarketingPref.ValueInt,5145)		[MarketingPreferenceId]
				FROM	Customers cu WITH ( NOLOCK )
						LEFT JOIN CustomerDetailValues cdv_DoNotSms WITH (NOLOCK) ON cu.CustomerID = cdv_DoNotSms.CustomerID
							AND cdv_DoNotSms.DetailFieldID = 177152			-- Customer allows marketing by SMS
						LEFT JOIN CustomerDetailValues cdv_DoNotEmail WITH (NOLOCK) ON cu.CustomerID = cdv_DoNotEmail.CustomerID
							AND cdv_DoNotEmail.DetailFieldID = 177153		-- Customer allows marketing by Email
						LEFT JOIN CustomerDetailValues cdv_DoNotPhone WITH (NOLOCK) ON cu.CustomerID = cdv_DoNotPhone.CustomerID
							AND cdv_DoNotPhone.DetailFieldID = 177371		-- Customer allows marketing by Phone
						LEFT JOIN CustomerDetailValues cdv_DoNotPost WITH (NOLOCK) ON cu.CustomerID = cdv_DoNotPost.CustomerID
							AND cdv_DoNotPost.DetailFieldID = 177154		-- Customer allows marketing by Post
						LEFT JOIN CustomerDetailValues cdv_ContactMethod WITH (NOLOCK) ON cu.CustomerID = cdv_ContactMethod.CustomerID
							AND cdv_ContactMethod.DetailFieldID = 170257	-- Send documents by
						LEFT JOIN CustomerDetailValues cdv_MarketingPref WITH (NOLOCK) ON cu.CustomerID = cdv_MarketingPref.CustomerID
							AND cdv_MarketingPref.DetailFieldID = 176970	-- Customer allows marketing
				WHERE cu.CustomerID = @CustomerID
				FOR XML PATH ('')
			 ) as XML ) [CustomerInfo]
			, CAST (
			(
				SELECT	 0																							[PetInfo/PetRef]
						,ldv_PetName.DetailValue																	[PetInfo/PetName]
						,rdv.ValueInt																				[PetInfo/SpeciesId]
						,rdv.ResourceListID																			[PetInfo/BreedId]
						,LEFT(dbo.fnGetSimpleDvLuli(144275,l.LeadID),1)												[PetInfo/Gender]		/*Gender*/
						,CONVERT(VARCHAR,CAST(dbo.fnGetSimpleDvAsDate(144274,l.LeadID) as DATETIME),127)			[PetInfo/BirthDate]		/*Pet Date of Birth*/
						,CASE WHEN dbo.fnGetSimpleDv(152783,l.LeadID) = 5144 THEN 'true' ELSE 'false' END			[PetInfo/IsNeutered]	/*Pet Neutered*/
						,CASE WHEN ISNULL(ldv_HasMicrochip.ValueInt, 5145) = 5144 THEN 'true' ELSE 'false' END		[PetInfo/HasMicrochip]
						,ISNULL(ldv_MicrochipNum.DetailValue,'')													[PetInfo/MicrochipNo]
						,dbo.fnGetSimpleDv(144339,l.LeadID)															[PetInfo/PurchasePrice] /*PurchasePrice*/
						,CASE WHEN ISNULL(ldv_Vaccinations.ValueInt, 5145) = 5144 THEN 'true' ELSE 'false' END		[PetInfo/VaccinationsUpToDate]
						,CASE WHEN ISNULL(ldv_ExistingConds.ValueInt, 5145) = 5144 THEN 'true' ELSE 'false' END		[PetInfo/HasExistingConditions]
						,0																							[PetInfo/PetColorId]
						,'false'																					[PetInfo/CoInsuranceNextYear]
						,0																							[VetID]
						,DATEADD(DAY, 1, dbo.fnGetSimpleDvAsDate(170037,m.MatterID))								[StartDate]				/* Policy Start Date = OMF End Date + 1 day */
						,CAST (
								(
								SELECT
									CAST (
										(
										SELECT  5144																[AnswerId]
												,147277																[QuestionId]
										FOR XML PATH ('UnderwritingAnswerItemType')
										)
									AS XML)
									,
									CAST (
										(
										SELECT  5144																[AnswerId]
												,154602																[QuestionId]
										FOR XML PATH ('UnderwritingAnswerItemType')
										)
									AS XML)
								FOR XML PATH ('UnderwritingList')
								)
						AS XML)
						,''																							[Conditions]
      			FROM Lead l WITH ( NOLOCK )
				INNER JOIN Matter m ON m.LeadID = l.LeadID
				LEFT JOIN LeadDetailValues ldv WITH ( NOLOCK ) ON ldv.LeadID = l.LeadID AND ldv.DetailFieldID = 144272 /*Pet Type*/
				LEFT JOIN LeadDetailValues ldv_PetName WITH ( NOLOCK ) ON ldv_PetName.LeadID = l.LeadID AND ldv_PetName.DetailFieldID = 144268 /* Pet Name */
				LEFT JOIN LeadDetailValues ldv_HasMicrochip WITH ( NOLOCK ) ON ldv_HasMicrochip.LeadID = l.LeadID AND ldv_HasMicrochip.DetailFieldID = 177372 /* Microchip */
				LEFT JOIN LeadDetailValues ldv_MicrochipNum WITH ( NOLOCK ) ON ldv_MicrochipNum.LeadID = l.LeadID AND ldv_MicrochipNum.DetailFieldID = 170030 /* Microchip Number */
				LEFT JOIN LeadDetailValues ldv_Vaccinations WITH ( NOLOCK ) ON ldv_Vaccinations.LeadID = l.LeadID AND ldv_Vaccinations.DetailFieldID = 175497 /* Vaccinations up to date */
				LEFT JOIN LeadDetailValues ldv_ExistingConds WITH ( NOLOCK ) ON ldv_ExistingConds.LeadID = l.LeadID AND ldv_ExistingConds.DetailFieldID = 175496 /* Any existing conditions? */
				LEFT JOIN ResourceListDetailValues rdv WITH ( NOLOCK ) ON rdv.ResourceListID = ldv.ValueInt AND rdv.DetailFieldID = 144269 /*Pet Type within resource list*/
				WHERE l.LeadID = @LeadID
				FOR XML PATH (''), ROOT ('PetQuote')
			 ) AS XML ) [PetQuotes]
			,@NumBuddiesPolicies		[OtherPetsInsured]	/* Actually represents number of policies */
			,''							[DiscountCodes]
	FOR XML PATH('GetQuoteValuesRequest')
	)

	/* Check that the assembled request stub is valid */
	EXEC _C600_PA_Policy_GetQuoteValues_Validate @XmlRequestStub, NULL, @Success  OUTPUT

	IF @Success = 1
	BEGIN
	
		/* 
			Get a quote for each of the Buddies products. We have to process each product individually as _C600_PA_Policy_GetQuoteValues
			only returns a quote for one product per call. Extract each returned quote node and consolidate into one XML response.
		*/
		SELECT 	@ProductID = 0,
				@ProductCounter = 1,
				@VoluntaryExcess = dbo.fnGetSimpleDv(170039, @MatterID)
		
		SELECT		@NumProductsToQuote = COUNT(*)
		FROM		@ProductsToQuote
	
		WHILE @ProductCounter <= @NumProductsToQuote
		BEGIN

			/* Get the next product to quote for */
			SELECT 		TOP (1) @ProductID = ptq.ProductID,
						@SaveAnnualQuotePriceInFieldID = ptq.SaveAnnualQuotePriceInFieldID,
						@SaveMonthlyQuotePriceInFieldID = ptq.SaveMonthlyQuotePriceInFieldID
			FROM		@ProductsToQuote ptq
			WHERE		ptq.ProductID > @ProductID
			ORDER BY	ptq.ProductID

			/* Construct the PolicyValues node for the current product */
			SELECT	@XMLProductRequestNode = 	
						(
						SELECT		@ProductID 			[PolicyValue/ProductId]
									,@VoluntaryExcess	[PolicyValue/VoluntaryExcess]
						FOR XML PATH ( 'PolicyValues' )
						)

			/* Insert the PolicyValues node into the request stub */
			SELECT	@XmlRequest = @XmlRequestStub
			SET @XmlRequest.modify('insert sql:variable("@XMLProductRequestNode") as first into (/GetQuoteValuesRequest/PetQuotes/PetQuote)[1]') 
			
			/* Get a quote for this product */
			SELECT @XmlProductResponse = ''	/* Have to initialise @XmlResponse to not null to force GetQuoteValues to output response to @XmlResponse */
			EXEC _C600_PA_Policy_GetQuoteValues @XmlRequest, @XmlOut = @XmlProductResponse OUTPUT

			/* Save quote prices to OMF Details --> Quotes section of Policy Overview page for later use in docs */
			SELECT	@AnnualQuotePrice = @XmlProductResponse.value('(//PolicyInfo/AnnualPremium)[1]','money'),
					@MonthlyQuotePrice = @XmlProductResponse.value('(//PolicyInfo/RecurringMonthly)[1]','money')
			
			EXEC _C00_SimpleValueIntoField @SaveAnnualQuotePriceInFieldID, @AnnualQuotePrice, @MatterID
			EXEC _C00_SimpleValueIntoField @SaveMonthlyQuotePriceInFieldID, @MonthlyQuotePrice, @MatterID
			
			SELECT @ProductCounter = @ProductCounter + 1

		END		/* Get quote for next product */
		
	END
	ELSE
	BEGIN
		PRINT 'Validation Failed'
		PRINT 'DECLARE @Response XML EXEC _C600_PA_Policy_GetQuoteValues_Validate ''' + CONVERT(VARCHAR(MAX),@XmlRequest) + ''', @Response OUTPUT, NULL SELECT @Response'
	END

	RETURN @NumProductsToQuote - 1
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_SaveQuotePricesForMatter] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_SaveQuotePricesForMatter] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_SaveQuotePricesForMatter] TO [sp_executeall]
GO
