SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-10-13
-- Description:	Get any sequence number by ID or Name
-- =============================================
CREATE PROCEDURE [dbo].[GetNextSequenceNumber] 
	@SequenceNumberID int = null,
	@SequenceNumberName varchar(50) = null
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @NewValueToUseInt int = -1,
	@ErrorMessage varchar(250) = ''
	
	IF @SequenceNumberID IS NULL AND @SequenceNumberName IS NULL
	BEGIN
		SELECT @ErrorMessage = 'SequenceNumberID and SequenceNumberName not supplied'
		EXEC dbo._C00_LogIt 'Error', 'Stored Proc GetNextSequenceNumber', 'GetNextSequenceNumber', @ErrorMessage, 0
		RAISERROR('@SequenceNumberID or @SequenceNumberName must be supplied!', 16, 1)
	END
	ELSE
	BEGIN
		
		IF @SequenceNumberID IS NULL
		BEGIN
			SELECT @SequenceNumberID = s.SequenceNumberID 
			FROM dbo.SequenceNumber s WITH (NOLOCK) 
			WHERE SequenceNumberName = @SequenceNumberName 
		END
		
		IF @SequenceNumberID > 0 
		BEGIN
		
			BEGIN TRAN
			
				/* 
					Increment the last used value by 1 first. 
					Do this within a transaction so that nothing else can read it
					until we have finished.
				*/
				UPDATE dbo.SequenceNumber 
				SET LastValueInt = COALESCE(LastValueInt, 0) + 1 
				WHERE SequenceNumberID = @SequenceNumberID 
				
				/*
					Now read the new value, commit the tran, and return the new value.
				*/
				SELECT @NewValueToUseInt = s.LastValueInt 
				FROM dbo.SequenceNumber s 
				WHERE s.SequenceNumberID = @SequenceNumberID 
				
			COMMIT
			
		END
		
		IF @NewValueToUseInt IS NULL OR @NewValueToUseInt < 1
		BEGIN
			SELECT @ErrorMessage = 'SequenceNumber not found for ID ' + CASE WHEN @SequenceNumberID IS NULL THEN 'NULL' ELSE CAST(@SequenceNumberID as varchar) END + ', Name ' + CASE WHEN @SequenceNumberName IS NULL THEN 'NULL' ELSE @SequenceNumberName END 
			EXEC dbo._C00_LogIt 'Error', 'Stored Proc GetNextSequenceNumber', 'GetNextSequenceNumber', @ErrorMessage, 0
			RAISERROR('SequenceNumber not found!', 16, 1)
		END
		
	END
	
	RETURN @NewValueToUseInt 
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[GetNextSequenceNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetNextSequenceNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetNextSequenceNumber] TO [sp_executeall]
GO
