SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.AddClientQuestionnaire    Script Date: 08/09/2006 12:22:46 ******/
-- JWG 2017-03-17 #42393 Cannot have these distributed transactions from other DB clusters

CREATE PROCEDURE [dbo].[AddClientQuestionnaire] 
	@ClientID INT,
	@QuestionnaireTitle NVARCHAR(255), 
	@QuestionnaireDescription TEXT,
	@QuestionsPerPage INT,
	@QuestionnaireIntroductionText TEXT,
	@HideIntro BIT,
	@QuestionnaireFooterText TEXT,
	@HideFooter BIT,
	@QuestionnaireFooterIframe TEXT,
	@QuestionnaireHeaderIframe TEXT,
	@QuestionnaireFooterInternal BIT,
	@QuestionnaireHeaderInternal BIT,
	@QuestionnaireFooterIframeHeight INT,
	@QuestionnaireHeaderIframeHeight INT,
	@QuestionnaireFooterIframeWidth INT,
	@QuestionnaireHeaderIframeWidth INT,
	@FrameMode BIT,
	@LayoutCss TEXT,
	@LayoutCssFileName NVARCHAR(512),
	@ImportDirectlyIntoLeadManager BIT,
	@RunAsClientPersonnelID INT,
	@RememberAnswers BIT
AS
BEGIN

	DECLARE @ClientQuestionnaireID INT

	/*
	INSERT INTO ClientQuestionnaires (ClientID, QuestionnaireTitle, QuestionnaireDescription, QuestionsPerPage, QuestionnaireIntroductionText, HideIntro, QuestionnaireFooterText, HideFooter, QuestionnaireFooterIframe, QuestionnaireHeaderIframe, QuestionnaireFooterInternal, QuestionnaireHeaderInternal, QuestionnaireFooterIframeHeight, QuestionnaireHeaderIframeHeight, QuestionnaireFooterIframeWidth, QuestionnaireHeaderIframeWidth, FrameMode, LayoutCss, LayoutCssFileName, ImportDirectlyIntoLeadManager, RunAsClientPersonnelID, RememberAnswers)
	VALUES (@ClientID, @QuestionnaireTitle, @QuestionnaireDescription,@QuestionsPerPage, @QuestionnaireIntroductionText, @HideIntro, @QuestionnaireFooterText, @HideFooter, @QuestionnaireFooterIframe, @QuestionnaireHeaderIframe, @QuestionnaireFooterInternal, @QuestionnaireHeaderInternal,@QuestionnaireFooterIframeHeight, @QuestionnaireHeaderIframeHeight, @QuestionnaireFooterIframeWidth, @QuestionnaireHeaderIframeWidth, @FrameMode, @LayoutCss, @LayoutCssFileName, @ImportDirectlyIntoLeadManager, @RunAsClientPersonnelID, @RememberAnswers)
	*/
	
	-- If we don't have an outer tranasction e.g. from the lead event 
	/*DECLARE @TranCount INT = @@TRANCOUNT
	IF @TranCount = 0
	BEGIN
		BEGIN TRAN
	END*/
	
	BEGIN TRY
	
		-- Get a new key from the master DB
		DECLARE @MasterClientQuestionnaireID INT,
		@DatabaseName SYSNAME = DB_NAME(),
		@ServerName SYSNAME = NULL
		
		EXEC @MasterClientQuestionnaireID = AquariusMaster.dbo.AQ_ClientQuestionnaire_Insert @ClientID, @DatabaseName, @ServerName
		
		IF @MasterClientQuestionnaireID > 0
		BEGIN
		
			-- Set context info to allow access to table
			DECLARE @ContextInfo VARBINARY(100) = CAST('ClientQuestionnaires' AS VARBINARY)
			SET CONTEXT_INFO @ContextInfo
			
			SET IDENTITY_INSERT [dbo].[ClientQuestionnaires] ON
			
			INSERT INTO ClientQuestionnaires (ClientQuestionnaireID, ClientID, QuestionnaireTitle, QuestionnaireDescription, QuestionsPerPage, QuestionnaireIntroductionText, HideIntro, QuestionnaireFooterText, HideFooter, QuestionnaireFooterIframe, QuestionnaireHeaderIframe, QuestionnaireFooterInternal, QuestionnaireHeaderInternal, QuestionnaireFooterIframeHeight, QuestionnaireHeaderIframeHeight, QuestionnaireFooterIframeWidth, QuestionnaireHeaderIframeWidth, FrameMode, LayoutCss, LayoutCssFileName, ImportDirectlyIntoLeadManager, RunAsClientPersonnelID, RememberAnswers)
			VALUES (@MasterClientQuestionnaireID, @ClientID, @QuestionnaireTitle, @QuestionnaireDescription,@QuestionsPerPage, @QuestionnaireIntroductionText, @HideIntro, @QuestionnaireFooterText, @HideFooter, @QuestionnaireFooterIframe, @QuestionnaireHeaderIframe, @QuestionnaireFooterInternal, @QuestionnaireHeaderInternal,@QuestionnaireFooterIframeHeight, @QuestionnaireHeaderIframeHeight, @QuestionnaireFooterIframeWidth, @QuestionnaireHeaderIframeWidth, @FrameMode, @LayoutCss, @LayoutCssFileName, @ImportDirectlyIntoLeadManager, @RunAsClientPersonnelID, @RememberAnswers)
			
			-- If we don't have an outer tran we can commit here
			/*IF @TranCount = 0
			BEGIN
				COMMIT
			END*/
			
			-- Clear the things we set above
			SET IDENTITY_INSERT [dbo].[ClientQuestionnaires] OFF
			SET CONTEXT_INFO 0x0
			
		END
		
	END TRY			
	BEGIN CATCH    
		
		-- If we have any tran open then we need to rollback
		/*IF @@TRANCOUNT > 0
		BEGIN
			ROLLBACK
		END*/
		
		-- Clear the things we set above
		SET IDENTITY_INSERT [dbo].[ClientQuestionnaires] OFF
		SET CONTEXT_INFO 0x0
		
		DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE()
		DECLARE @ErrorSeverity INT = ERROR_SEVERITY()
		DECLARE @ErrorState INT = ERROR_STATE()

		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)
		
	END CATCH
	
	-- This scope_identity should match the @MasterClientQuestionnaireID
	SET @ClientQuestionnaireID = SCOPE_IDENTITY()

	-- Select a record the old eCatcher way to prove the insert worked
	SELECT @ClientQuestionnaireID 
	FROM ClientQuestionnaires 
	WHERE ClientQuestionnaireID = @ClientQuestionnaireID 
	AND @ClientQuestionnaireID = @MasterClientQuestionnaireID
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[AddClientQuestionnaire] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddClientQuestionnaire] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddClientQuestionnaire] TO [sp_executeall]
GO
