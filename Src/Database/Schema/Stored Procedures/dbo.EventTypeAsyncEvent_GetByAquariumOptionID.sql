SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeAsyncEvent table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAsyncEvent_GetByAquariumOptionID]
(

	@AquariumOptionID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EventTypeAsyncEventID],
					[ClientID],
					[SourceEventTypeID],
					[AsyncEventTypeID],
					[FollowupThreadNumber],
					[AquariumOptionID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[EventTypeAsyncEvent] WITH (NOLOCK) 
				WHERE
					[AquariumOptionID] = @AquariumOptionID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAsyncEvent_GetByAquariumOptionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAsyncEvent_GetByAquariumOptionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAsyncEvent_GetByAquariumOptionID] TO [sp_executeall]
GO
