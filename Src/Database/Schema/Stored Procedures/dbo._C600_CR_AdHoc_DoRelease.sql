SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date:	2017-10-04
-- Description:	Execute queued ad-hoc config release items
-- Used by ReportID 38523 to execute CR pushes
-- =============================================
CREATE PROCEDURE [dbo].[_C600_CR_AdHoc_DoRelease]
	 @AllowBatchJobs		BIT = 0
	,@AllowDetailFields		BIT = 0
	,@AllowEventTypes		BIT = 0
	,@AllowReports			BIT = 0
	,@AllowDocumentTypes	BIT = 0
AS
BEGIN

	DECLARE  @ThisTableRowID	INT
			,@ThisObjectID		INT

	DECLARE @AllReleaseItems TABLE ( ObjectTypeID INT, ObjectTypeName VARCHAR(2000), ObjectID INT, TableRowID INT, ObjectName VARCHAR(2000), Completed BIT, EmailOnCompletion VARCHAR(2000) )
	INSERT @AllReleaseItems ( ObjectTypeID, ObjectTypeName, ObjectID, TableRowID, ObjectName, Completed, EmailOnCompletion )
	
	SELECT tdvType.ValueInt, liType.ItemValue, tdvObjectID.ValueInt, tr.TableRowID, tdvObjectName.DetailValue, 0, ISNULL(tdvEmail.DetailValue,'cathal.sherry@aquarium-software.com')
	FROM TableRows tr WITH ( NOLOCK ) 
	INNER JOIN TableDetailValues tdvType		WITH ( NOLOCK ) on tdvType.TableRowID		= tr.TableRowID AND tdvType.DetailFieldID = 179402 /*Object Type*/
	INNER JOIN LookupListItems	 liType			WITH ( NOLOCK ) on liType.LookupListItemID	= tdvType.ValueInt
	INNER JOIN TableDetailValues tdvObjectID	WITH ( NOLOCK ) on tdvObjectID.TableRowID	= tr.TableRowID AND tdvObjectID.DetailFieldID = 179403 /*Object ID*/
	INNER JOIN TableDetailValues tdvObjectName	WITH ( NOLOCK ) on tdvObjectName.TableRowID	= tr.TableRowID AND tdvObjectName.DetailFieldID = 179409 /*Object Name*/
	INNER JOIN TableDetailValues tdvReleased	WITH ( NOLOCK ) on tdvReleased.TableRowID	= tr.TableRowID AND tdvReleased.DetailFieldID = 179408 /*Date Released*/
	 LEFT JOIN TableDetailValues tdvEmail		WITH ( NOLOCK ) on tdvEmail.TableRowID		= tr.TableRowID AND tdvEmail.DetailFieldID = 179410 /*Email On Completion*/
	WHERE tr.DetailFieldID = 179406 /*Ad Hoc Release Queue*/
	AND tdvObjectName.DetailValue <> ''
	AND tdvReleased.ValueDate is NULL

	WHILE @AllowDocumentTypes = 1
		AND EXISTS ( SELECT * FROM @AllReleaseItems ari WHERE ari.ObjectTypeID = 76053 /*Document Template*/ AND ari.Completed = 0 )
	BEGIN
		SELECT TOP 1 @ThisTableRowID = ari.TableRowID, @ThisObjectID = ari.ObjectID
		FROM @AllReleaseItems ari 
		WHERE ari.ObjectTypeID = 76053 /*Document Template*/
		AND ari.Completed = 0
		
		EXEC _C600_CR_AdHoc_DocumentType_Update @ThisObjectID, @ThisTableRowID
		
		UPDATE ari
		SET Completed = 1
		FROM @AllReleaseItems ari 
		WHERE ari.TableRowID = @ThisTableRowID
		
		SELECT @ThisObjectID = NULL, @ThisTableRowID = NULL
	END
	
	--IF @AllowDocumentTypes = 1 AND DB_NAME() = 'Aquarius600'
	--BEGIN
	--	EXEC dbo._C600_DatabaseConfig_EmailUpdates 0
	--END
	
	SELECT * 
	FROM @AllReleaseItems ari	

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CR_AdHoc_DoRelease] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_CR_AdHoc_DoRelease] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_CR_AdHoc_DoRelease] TO [sp_executeall]
GO
