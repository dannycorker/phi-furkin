SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the OutcomeCriterias table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[OutcomeCriterias_GetByOutcomeCriteriaID]
(

	@OutcomeCriteriaID int   
)
AS


				SELECT
					[OutcomeCriteriaID],
					[OutcomeID],
					[QuestionPossibleAnswerID],
					[ClientID]
				FROM
					[dbo].[OutcomeCriterias] WITH (NOLOCK) 
				WHERE
										[OutcomeCriteriaID] = @OutcomeCriteriaID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeCriterias_GetByOutcomeCriteriaID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutcomeCriterias_GetByOutcomeCriteriaID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeCriterias_GetByOutcomeCriteriaID] TO [sp_executeall]
GO
