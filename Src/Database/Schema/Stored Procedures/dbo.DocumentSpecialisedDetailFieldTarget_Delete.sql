SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DocumentSpecialisedDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentSpecialisedDetailFieldTarget_Delete]
(

	@DocumentSpecialisedDetailFieldTargetID int   
)
AS


				DELETE FROM [dbo].[DocumentSpecialisedDetailFieldTarget] WITH (ROWLOCK) 
				WHERE
					[DocumentSpecialisedDetailFieldTargetID] = @DocumentSpecialisedDetailFieldTargetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSpecialisedDetailFieldTarget_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentSpecialisedDetailFieldTarget_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentSpecialisedDetailFieldTarget_Delete] TO [sp_executeall]
GO
