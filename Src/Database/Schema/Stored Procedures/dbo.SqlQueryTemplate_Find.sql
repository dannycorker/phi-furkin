SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SqlQueryTemplate table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryTemplate_Find]
(

	@SearchUsingOR bit   = null ,

	@SqlQueryTemplateID int   = null ,

	@ClientID int   = null ,

	@Name varchar (150)  = null ,

	@Description varchar (500)  = null ,

	@TemplatePath varchar (255)  = null ,

	@DataWorksheetNumber int   = null ,

	@DataStartCell varchar (10)  = null ,

	@TemplateFormat varchar (20)  = null ,

	@FolderID int   = null ,

	@CreatedBy int   = null ,

	@CreatedOn datetime   = null ,

	@ModifiedBy int   = null ,

	@ModifiedOn datetime   = null ,

	@Enabled bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SqlQueryTemplateID]
	, [ClientID]
	, [Name]
	, [Description]
	, [TemplatePath]
	, [DataWorksheetNumber]
	, [DataStartCell]
	, [TemplateFormat]
	, [FolderID]
	, [CreatedBy]
	, [CreatedOn]
	, [ModifiedBy]
	, [ModifiedOn]
	, [Enabled]
    FROM
	[dbo].[SqlQueryTemplate] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryTemplateID] = @SqlQueryTemplateID OR @SqlQueryTemplateID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([Name] = @Name OR @Name IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([TemplatePath] = @TemplatePath OR @TemplatePath IS NULL)
	AND ([DataWorksheetNumber] = @DataWorksheetNumber OR @DataWorksheetNumber IS NULL)
	AND ([DataStartCell] = @DataStartCell OR @DataStartCell IS NULL)
	AND ([TemplateFormat] = @TemplateFormat OR @TemplateFormat IS NULL)
	AND ([FolderID] = @FolderID OR @FolderID IS NULL)
	AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
	AND ([CreatedOn] = @CreatedOn OR @CreatedOn IS NULL)
	AND ([ModifiedBy] = @ModifiedBy OR @ModifiedBy IS NULL)
	AND ([ModifiedOn] = @ModifiedOn OR @ModifiedOn IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SqlQueryTemplateID]
	, [ClientID]
	, [Name]
	, [Description]
	, [TemplatePath]
	, [DataWorksheetNumber]
	, [DataStartCell]
	, [TemplateFormat]
	, [FolderID]
	, [CreatedBy]
	, [CreatedOn]
	, [ModifiedBy]
	, [ModifiedOn]
	, [Enabled]
    FROM
	[dbo].[SqlQueryTemplate] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryTemplateID] = @SqlQueryTemplateID AND @SqlQueryTemplateID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([Name] = @Name AND @Name is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([TemplatePath] = @TemplatePath AND @TemplatePath is not null)
	OR ([DataWorksheetNumber] = @DataWorksheetNumber AND @DataWorksheetNumber is not null)
	OR ([DataStartCell] = @DataStartCell AND @DataStartCell is not null)
	OR ([TemplateFormat] = @TemplateFormat AND @TemplateFormat is not null)
	OR ([FolderID] = @FolderID AND @FolderID is not null)
	OR ([CreatedBy] = @CreatedBy AND @CreatedBy is not null)
	OR ([CreatedOn] = @CreatedOn AND @CreatedOn is not null)
	OR ([ModifiedBy] = @ModifiedBy AND @ModifiedBy is not null)
	OR ([ModifiedOn] = @ModifiedOn AND @ModifiedOn is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTemplate_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate_Find] TO [sp_executeall]
GO
