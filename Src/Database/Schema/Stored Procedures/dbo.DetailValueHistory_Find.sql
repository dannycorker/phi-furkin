SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DetailValueHistory table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailValueHistory_Find]
(

	@SearchUsingOR bit   = null ,

	@DetailValueHistoryID int   = null ,

	@ClientID int   = null ,

	@DetailFieldID int   = null ,

	@LeadOrMatter tinyint   = null ,

	@LeadID int   = null ,

	@MatterID int   = null ,

	@FieldValue varchar (2000)  = null ,

	@WhenSaved datetime   = null ,

	@ClientPersonnelID int   = null ,

	@EncryptedValue varchar (3000)  = null ,

	@ValueInt int   = null ,

	@ValueMoney money   = null ,

	@ValueDate date   = null ,

	@ValueDateTime datetime2   = null ,

	@CustomerID int   = null ,

	@CaseID int   = null ,

	@ContactID int   = null ,

	@ClientPersonnelDetailValueID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DetailValueHistoryID]
	, [ClientID]
	, [DetailFieldID]
	, [LeadOrMatter]
	, [LeadID]
	, [MatterID]
	, [FieldValue]
	, [WhenSaved]
	, [ClientPersonnelID]
	, [EncryptedValue]
	, [ValueInt]
	, [ValueMoney]
	, [ValueDate]
	, [ValueDateTime]
	, [CustomerID]
	, [CaseID]
	, [ContactID]
	, [ClientPersonnelDetailValueID]
    FROM
	[dbo].[DetailValueHistory] WITH (NOLOCK) 
    WHERE 
	 ([DetailValueHistoryID] = @DetailValueHistoryID OR @DetailValueHistoryID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([LeadOrMatter] = @LeadOrMatter OR @LeadOrMatter IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([MatterID] = @MatterID OR @MatterID IS NULL)
	AND ([FieldValue] = @FieldValue OR @FieldValue IS NULL)
	AND ([WhenSaved] = @WhenSaved OR @WhenSaved IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([EncryptedValue] = @EncryptedValue OR @EncryptedValue IS NULL)
	AND ([ValueInt] = @ValueInt OR @ValueInt IS NULL)
	AND ([ValueMoney] = @ValueMoney OR @ValueMoney IS NULL)
	AND ([ValueDate] = @ValueDate OR @ValueDate IS NULL)
	AND ([ValueDateTime] = @ValueDateTime OR @ValueDateTime IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([ContactID] = @ContactID OR @ContactID IS NULL)
	AND ([ClientPersonnelDetailValueID] = @ClientPersonnelDetailValueID OR @ClientPersonnelDetailValueID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DetailValueHistoryID]
	, [ClientID]
	, [DetailFieldID]
	, [LeadOrMatter]
	, [LeadID]
	, [MatterID]
	, [FieldValue]
	, [WhenSaved]
	, [ClientPersonnelID]
	, [EncryptedValue]
	, [ValueInt]
	, [ValueMoney]
	, [ValueDate]
	, [ValueDateTime]
	, [CustomerID]
	, [CaseID]
	, [ContactID]
	, [ClientPersonnelDetailValueID]
    FROM
	[dbo].[DetailValueHistory] WITH (NOLOCK) 
    WHERE 
	 ([DetailValueHistoryID] = @DetailValueHistoryID AND @DetailValueHistoryID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([LeadOrMatter] = @LeadOrMatter AND @LeadOrMatter is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([MatterID] = @MatterID AND @MatterID is not null)
	OR ([FieldValue] = @FieldValue AND @FieldValue is not null)
	OR ([WhenSaved] = @WhenSaved AND @WhenSaved is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([EncryptedValue] = @EncryptedValue AND @EncryptedValue is not null)
	OR ([ValueInt] = @ValueInt AND @ValueInt is not null)
	OR ([ValueMoney] = @ValueMoney AND @ValueMoney is not null)
	OR ([ValueDate] = @ValueDate AND @ValueDate is not null)
	OR ([ValueDateTime] = @ValueDateTime AND @ValueDateTime is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([ContactID] = @ContactID AND @ContactID is not null)
	OR ([ClientPersonnelDetailValueID] = @ClientPersonnelDetailValueID AND @ClientPersonnelDetailValueID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValueHistory_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailValueHistory_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValueHistory_Find] TO [sp_executeall]
GO
