SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ScriptSection table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptSection_Find]
(

	@SearchUsingOR bit   = null ,

	@ScriptSectionID int   = null ,

	@ScriptID int   = null ,

	@ClientID int   = null ,

	@SectionTitle varchar (250)  = null ,

	@SectionOrder int   = null ,

	@WhenCreated datetime   = null ,

	@WhoCreated int   = null ,

	@WhenModified datetime   = null ,

	@WhoModified int   = null ,

	@SourceID int   = null ,

	@NextEventTypeID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ScriptSectionID]
	, [ScriptID]
	, [ClientID]
	, [SectionTitle]
	, [SectionOrder]
	, [WhenCreated]
	, [WhoCreated]
	, [WhenModified]
	, [WhoModified]
	, [SourceID]
	, [NextEventTypeID]
    FROM
	[dbo].[ScriptSection] WITH (NOLOCK) 
    WHERE 
	 ([ScriptSectionID] = @ScriptSectionID OR @ScriptSectionID IS NULL)
	AND ([ScriptID] = @ScriptID OR @ScriptID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SectionTitle] = @SectionTitle OR @SectionTitle IS NULL)
	AND ([SectionOrder] = @SectionOrder OR @SectionOrder IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([NextEventTypeID] = @NextEventTypeID OR @NextEventTypeID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ScriptSectionID]
	, [ScriptID]
	, [ClientID]
	, [SectionTitle]
	, [SectionOrder]
	, [WhenCreated]
	, [WhoCreated]
	, [WhenModified]
	, [WhoModified]
	, [SourceID]
	, [NextEventTypeID]
    FROM
	[dbo].[ScriptSection] WITH (NOLOCK) 
    WHERE 
	 ([ScriptSectionID] = @ScriptSectionID AND @ScriptSectionID is not null)
	OR ([ScriptID] = @ScriptID AND @ScriptID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SectionTitle] = @SectionTitle AND @SectionTitle is not null)
	OR ([SectionOrder] = @SectionOrder AND @SectionOrder is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([NextEventTypeID] = @NextEventTypeID AND @NextEventTypeID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSection_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection_Find] TO [sp_executeall]
GO
