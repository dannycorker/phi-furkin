SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2009-02-18
-- Description:	Get all AutomatedTaskCommandLine records for a control record
--              Very similar to DocumentZipFtpCommand.
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedTaskCommandLine__GetByControlIDAndType]
	@ControlID int,
	@LineType char(3)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT com.LineText 
	FROM dbo.AutomatedTaskCommandLine com 
	WHERE com.AutomatedTaskCommandControlID = @ControlID 
	AND com.LineType = @LineType 
	AND com.Enabled = 1 
	ORDER BY com.LineOrder 
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskCommandLine__GetByControlIDAndType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskCommandLine__GetByControlIDAndType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskCommandLine__GetByControlIDAndType] TO [sp_executeall]
GO
