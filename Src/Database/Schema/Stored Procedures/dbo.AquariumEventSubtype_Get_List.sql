SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the AquariumEventSubtype table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumEventSubtype_Get_List]

AS


				
				SELECT
					[AquariumEventSubtypeID],
					[AquariumEventTypeID],
					[AquariumEventSubtypeName],
					[AquariumEventSubtypeDescription],
					[WhenCreated],
					[WhenModified],
					[ClientID],
					[StoredProcedure]
				FROM
					[dbo].[AquariumEventSubtype] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventSubtype_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumEventSubtype_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventSubtype_Get_List] TO [sp_executeall]
GO
