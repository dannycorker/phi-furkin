SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2012-02-14
-- Description:	Get all event types that a user can add, optionally by subtype and leadtype 
-- SB  2014-07-10 Updated to use view which includes shared event types
-- =============================================
CREATE PROCEDURE [dbo].[EventType__GetSecure]
	@ClientID INT,
	@ClientPersonnelID INT,
	@EventSubtypeID INT,
	@LeadTypeID INT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	/* 
		Pass in a ClientPersonnelID and this will find all EventTypes that the user/group has full access to. 
		This can be filtered by EventSubtypeID ("Letter In" etc)
	*/
	SELECT et.EventTypeID 
	FROM dbo.fnEventTypeShared(@ClientID) et
	WHERE et.ClientID = @ClientID 
	AND et.EventSubtypeID = @EventSubtypeID 
	AND (@LeadTypeID IS NULL OR et.LeadTypeID = @LeadTypeID)
	ORDER BY et.EventTypeID
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetSecure] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetSecure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetSecure] TO [sp_executeall]
GO
