SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------

-- Created By: Chris Townsend
-- Purpose: Gets a dataset of SqlQueryIDs and a count of their SqlQueryColumns
			Used to determine whether or not a summary report can be created
			from a parent report.
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GetSqlQueryColumnCountListByClientID]
(
	@ClientID int   
)
AS
		SELECT
			SqlQueryID,
			Count(SqlQueryColumnID) AS SqlQueryColumnCount
		FROM
			SqlQueryColumns
		WHERE
			ClientID = @ClientID
		GROUP BY SqlQueryID
					
			






GO
GRANT VIEW DEFINITION ON  [dbo].[GetSqlQueryColumnCountListByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetSqlQueryColumnCountListByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetSqlQueryColumnCountListByClientID] TO [sp_executeall]
GO
