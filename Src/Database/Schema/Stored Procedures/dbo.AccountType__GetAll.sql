SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 21-06-2016
-- Description:	Gets a list of account types
-- =============================================
CREATE PROCEDURE [dbo].[AccountType__GetAll]
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * FROM AccountType WITH (NOLOCK) 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AccountType__GetAll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AccountType__GetAll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AccountType__GetAll] TO [sp_executeall]
GO
