SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Ian Slack
-- Create date: 22-11-2011
-- Description:	Get an external login account record
-- which will be used for authenticating a user
-- =============================================
CREATE PROCEDURE [dbo].[ExternalApplicationLogin__GetByClientIDUsername] 
	@ClientID int, 
	@Username varchar(255)
AS
BEGIN

	SET ANSI_NULLS ON

	SELECT	* 
	FROM	ExternalApplicationLogin WITH (NOLOCK)
	WHERE	ClientID = @ClientID
	AND		Username = @Username
	
	SELECT @@ROWCOUNT

END




GO
GRANT VIEW DEFINITION ON  [dbo].[ExternalApplicationLogin__GetByClientIDUsername] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ExternalApplicationLogin__GetByClientIDUsername] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ExternalApplicationLogin__GetByClientIDUsername] TO [sp_executeall]
GO
