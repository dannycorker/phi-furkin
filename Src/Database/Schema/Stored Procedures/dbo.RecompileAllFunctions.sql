SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2016-02-03
-- Description:	Mark all functions for recompilation
-- =============================================
CREATE PROCEDURE [dbo].[RecompileAllFunctions]
	@PrintOnly bit = 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @stmnt varchar(500)

	DECLARE SPCursor CURSOR DYNAMIC READ_ONLY FOR
		SELECT 'EXEC sp_recompile [' + i.ROUTINE_NAME + ']'
		FROM INFORMATION_SCHEMA.ROUTINES i 
		WHERE i.ROUTINE_TYPE = 'FUNCTION' 
		AND i.ROUTINE_SCHEMA = 'dbo' 
		AND i.ROUTINE_NAME NOT IN ('RegexGroups', 'RegexMatches')
		ORDER BY i.ROUTINE_NAME

	OPEN SPCursor

	FETCH RELATIVE 1 FROM SPCursor INTO @stmnt

	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		IF @PrintOnly = 0
		BEGIN
			EXEC(@stmnt)
		END
		ELSE
		BEGIN
			PRINT @stmnt
		END

		FETCH NEXT FROM SPCursor INTO @stmnt
		
	END

	CLOSE SPCursor
	DEALLOCATE SPCursor
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[RecompileAllFunctions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RecompileAllFunctions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RecompileAllFunctions] TO [sp_executeall]
GO
