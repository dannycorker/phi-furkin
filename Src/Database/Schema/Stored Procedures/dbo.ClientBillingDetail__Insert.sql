SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 10-07-2013
-- Description:	Inserts a client billing detail record
-- Modified By PR 28-04-2014 added SmsGatewayID
-- Modified By PR 01-05-2014 added MobileTelephone
-- =============================================
CREATE PROCEDURE [dbo].[ClientBillingDetail__Insert] 
	@ClientID INT, 
	@SourceAquariumOptionID INT, 
	@ModuleID INT, 
	@WhenCreated DATETIME = NULL, 
	@WhoCreated INT = NULL, 
	@ChargeInPence DECIMAL(18, 4) = NULL, 
	@Notes VARCHAR(250) = NULL, 
	@SourceObjectUID INT = NULL, 
	@SourceTableName VARCHAR(250) = NULL,
	@MessageGUID VARCHAR(36) = NULL,
	@SmsGatewayID INT = NULL,
	@MobileTelephone VARCHAR(50) = NULL
AS
BEGIN

	SET NOCOUNT ON;

	IF @ChargeInPence IS NULL
	BEGIN
		/* 
			Charge the appropriate client for the lookup 
			eCatcher = 8.0 pence
			Lead Manager = same for now
			Correct
		*/
		SET @ChargeInPence = CASE @SourceAquariumOptionID 
									WHEN 51 THEN CASE @ModuleID 
														WHEN 3 THEN 15.0 
														ELSE 15.0 
												 END /* ModuleID within SourceAquariumOptionID 51 */
							 END /* SourceAquariumOptionID */
	END

	INSERT dbo.ClientBillingDetail 
	(
		ClientID, 
		SourceAquariumOptionID, 
		ModuleID, 
		WhenCreated, 
		WhoCreated, 
		ChargeInPence, 
		ClientBillingSummaryID, 
		Notes, 
		SourceObjectUID, 
		SourceTableName,
		MessageGUID, 
		MobileTelephone,
		SmsGatewayID
	)
	VALUES 
	(
		@ClientID, 
		@SourceAquariumOptionID, 
		@ModuleID, 
		COALESCE(@WhenCreated, dbo.fn_GetDate_Local()),
		@WhoCreated, 
		COALESCE(@ChargeInPence, 0), 
		NULL, /* ClientBillingSummaryID only gets set when this item has been assigned to a monthly summary report */
		@Notes, 
		@SourceObjectUID, 
		@SourceTableName,
		@MessageGUID,
		@MobileTelephone,
		@SmsGatewayID
	)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientBillingDetail__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientBillingDetail__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientBillingDetail__Insert] TO [sp_executeall]
GO
