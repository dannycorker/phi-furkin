SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields

CREATE PROCEDURE [dbo].[DetailFields__GetEnabledResourceListFields] 
	@DetailFieldPageID int,
	@ClientID int
	
AS
	SELECT
		*
	FROM
		dbo.fnDetailFieldsShared(@ClientID)
	WHERE
		[DetailFieldPageID] = @DetailFieldPageID
		AND [QuestionTypeID] = 14
		AND [ClientID] = @ClientID
		AND [Enabled] = 1

	Select @@ROWCOUNT


GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetEnabledResourceListFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetEnabledResourceListFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetEnabledResourceListFields] TO [sp_executeall]
GO
