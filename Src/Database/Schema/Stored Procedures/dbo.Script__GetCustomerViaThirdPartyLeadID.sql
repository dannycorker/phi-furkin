SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 05-05-2015
-- Description:	Get customer via thirdparty lead ID
-- =============================================
CREATE PROCEDURE [dbo].[Script__GetCustomerViaThirdPartyLeadID]
	@ClientID INT,
	@ThirdPartyLeadID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ThirdPartyLeadID_DetailFieldID INT
	
	SELECT @ThirdPartyLeadID_DetailFieldID=DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=4200 AND LeadTypeID=0 -- Customer level field

    IF @ThirdPartyLeadID_DetailFieldID>0 --ThirdPartyLeadID has been mapped
    BEGIN
    
		DECLARE @CustomerID INT
		
		-- This should return a single record since thirdPartyLeadID is unique to each customer
		SELECT @CustomerID=cdv.CustomerID FROM CustomerDetailValues cdv WITH (NOLOCK) 
		INNER JOIN Customers c WITH (NOLOCK) on c.CustomerID=cdv.CustomerID AND c.Test=0
		WHERE cdv.DetailFieldID = @ThirdPartyLeadID_DetailFieldID AND  cdv.ClientID = @ClientID AND ValueInt = @ThirdPartyLeadID

		IF @CustomerID>0
		BEGIN
			SELECT * FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID AND ClientID=@ClientID AND Test=0
		END
		
    END	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__GetCustomerViaThirdPartyLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script__GetCustomerViaThirdPartyLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__GetCustomerViaThirdPartyLeadID] TO [sp_executeall]
GO
