SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-08-30
-- Description:	Returns all the columns for the tables in a query and whether they are currently chosen
--				ACE 2014-07-30 Added in Memorable word stuff
-- =============================================

CREATE PROCEDURE [dbo].[SqlQueryEditingColumns__GetTableColumns]
(
	@SqlQueryEditingID INT   
)
AS

DECLARE @IsChildQuery BIT
SELECT @IsChildQuery = CASE WHEN ParentQueryID IS NULL THEN 0 ELSE 1 END
FROM dbo.SqlQueryEditing WITH (NOLOCK) 
WHERE SqlQueryEditingID = @SqlQueryEditingID

IF @IsChildQuery = 1
BEGIN

	;WITH ParentQueryColumns AS 
	(
	SELECT 
		DISTINCT CASE WHEN c.ColumnAlias > '' THEN c.ColumnAlias ELSE c.ColumnNaturalName END AS ColumnName, 
		'Parent Query' AS TableName, 
		0 AS SqlQueryEditingTableID,
		CASE WHEN ec.SqlQueryEditingColumnID IS NULL THEN 0 ELSE 1 END AS Selected, 
		c.SqlQueryColumnID,
		ec.SqlQueryEditingColumnID,
		ec.ColumnAlias
	FROM dbo.SqlQueryTables t WITH (NOLOCK)
	INNER JOIN dbo.SqlQueryColumns c WITH (NOLOCK) ON t.SqlQueryTableID = c.SqlQueryTableID
	INNER JOIN dbo.SqlQueryEditing e WITH (NOLOCK) ON c.SqlQueryID = e.ParentQueryID AND t.SqlQueryID = e.ParentQueryID  
	LEFT JOIN dbo.SqlQueryEditingColumns ec WITH (NOLOCK) ON e.SqlQueryEditingID = ec.SqlQueryEditingID AND 
															 ec.ColumnNaturalName = CASE WHEN c.ColumnAlias > '' THEN c.ColumnAlias ELSE c.ColumnNaturalName END
	WHERE c.ShowColumn = 1
	AND e.SqlQueryEditingID = @SqlQueryEditingID
	),
	OrphanedColumns AS 
	(
	SELECT 
		ec.ColumnNaturalName ColumnName, 
		'Parent Query' AS TableName, 
		0 AS SqlQueryEditingTableID,
		1 AS Selected, 
		c.SqlQueryColumnID,
		ec.SqlQueryEditingColumnID,
		ec.ColumnAlias
	FROM dbo.SqlQueryEditing e WITH (NOLOCK) 
	INNER JOIN dbo.SqlQueryEditingColumns ec WITH (NOLOCK) ON e.SqlQueryEditingID = ec.SqlQueryEditingID
	LEFT JOIN dbo.SqlQueryColumns c WITH (NOLOCK) ON e.ParentQueryID = c.SqlQueryID AND ec.ColumnNaturalName = CASE WHEN c.ColumnAlias > '' THEN c.ColumnAlias ELSE c.ColumnNaturalName END 
	WHERE e.SqlQueryEditingID = @SqlQueryEditingID
	)


SELECT * 
FROM ParentQueryColumns
WHERE ColumnName NOT IN ('Password','Salt','SourceID','EncryptedValue', 'MemorableWord', 'MemorableWordAttempts', 'MemorableWordSalt')
UNION
SELECT * 
FROM OrphanedColumns
WHERE ColumnName NOT IN ('Password','Salt','SourceID','EncryptedValue', 'MemorableWord', 'MemorableWordAttempts', 'MemorableWordSalt')
ORDER BY ColumnName

END
ELSE
BEGIN

	SELECT	i.COLUMN_NAME AS ColumnName, 
			CASE WHEN t.TableAlias > '' THEN t.TableAlias ELSE t.SqlQueryTableName END AS TableName, 
			t.SqlQueryEditingTableID,
			CASE WHEN c.SqlQueryEditingColumnID IS NULL THEN 0 ELSE 1 END AS Selected, 1 AS SqlQueryColumnID,
			c.ColumnAlias,
			c.SqlQueryEditingColumnID
	FROM dbo.SqlQueryEditingTable t WITH (NOLOCK)
	INNER JOIN INFORMATION_SCHEMA.COLUMNS i WITH (NOLOCK) ON t.SqlQueryTableName = i.TABLE_NAME 
	LEFT JOIN dbo.SqlQueryEditingColumns c WITH (NOLOCK) ON t.SqlQueryEditingTableID = c.SqlQueryEditingTableID AND c.ColumnNaturalName = i.COLUMN_NAME
	WHERE t.SqlQueryEditingID = @SqlQueryEditingID
	AND i.data_type NOT IN ('binary','image','ntext','sql_variant','sysname','text','timestamp','uniqueidentifier','varbinary','xml')
	AND i.COLUMN_NAME NOT IN ('Password','Salt','SourceID','EncryptedValue', 'MemorableWord', 'MemorableWordAttempts', 'MemorableWordSalt')
	ORDER BY t.SqlQueryEditingTableID, CASE WHEN t.TableAlias > '' THEN t.TableAlias ELSE t.SqlQueryTableName END, i.COLUMN_NAME
	
END
	


GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns__GetTableColumns] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingColumns__GetTableColumns] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns__GetTableColumns] TO [sp_executeall]
GO
