SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuestionPossibleAnswers table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionPossibleAnswers_GetByClientID]
(

	@ClientID int   
)
AS


				SELECT
					[QuestionPossibleAnswerID],
					[ClientQuestionnaireID],
					[MasterQuestionID],
					[AnswerText],
					[Branch],
					[LinkedQuestionnaireQuestionPossibleAnswerID],
					[ClientID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[QuestionPossibleAnswers] WITH (NOLOCK) 
				WHERE
										[ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionPossibleAnswers_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionPossibleAnswers_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionPossibleAnswers_GetByClientID] TO [sp_executeall]
GO
