SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------
-- Date Created: 04 April 2007

-- Created By:  ()
-- Purpose: Select records from the ClientPersonnel table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnel_GetByClientOfficeID]
(

	@ClientOfficeID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[ClientPersonnelID],
					[ClientID],
					[ClientOfficeID],
					[TitleID],
					[FirstName],
					[MiddleName],
					[LastName],
					[JobTitle],
					[Password],
					[ClientPersonnelAdminGroupID],
					[MobileTelephone],
					[HomeTelephone],
					[OfficeTelephone],
					[OfficeTelephoneExtension],
					[EmailAddress],
					[ChargeOutRate],
					[UserName],
					[Salt],
					[AttemptedLogins],
					[ManagerID]
				FROM
					dbo.[ClientPersonnel]
				WHERE
					[ClientOfficeID] = @ClientOfficeID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_GetByClientOfficeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel_GetByClientOfficeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_GetByClientOfficeID] TO [sp_executeall]
GO
