SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 07-12-2012
-- Description:	Inserts a record in the mapped call history table
--				This is called from the UltraManagement Soap Service which in turn
--				is called by the Ultra Call Bar
--				CS 2015-10-29 stamp SourceID of new TableRows
-- =============================================
CREATE PROCEDURE [dbo].[__InsertCallHistoryRecord_2]

	@CaseID INT,
	@RowID INT,
	@ListID INT,
	@SchemaName VARCHAR(2000),
	@TransactionID INT,
	@AgentsName VARCHAR(2000),
	@ApproximateDateTimeOfCall VARCHAR(2000)

AS
BEGIN

	DECLARE @TableRowID INT,
			@DetailFieldPageID INT,
			@DetailFieldID INT,
			@364FieldID INT, -- RowID
			@365FieldID INT, -- ListID
			@366FieldID INT, -- Schema Name
			@367FieldID INT, -- CaseID
			@368FieldID INT, -- TransactionID
			@369FieldID INT, -- AgentsName
			@370FieldID INT, -- Approximate Date Time Of Call
			@ClientID INT,
			@LeadTypeID INT,
			@LeadID INT,
			@CustomerID INT,
			@LogID		INT
			
	SELECT @LeadID=LeadID, @ClientID=ClientID FROM Cases WITH (NOLOCK) WHERE CaseID = @CaseID;
	SELECT @LeadTypeID = LeadTypeID, @CustomerID = CustomerID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID						
	
	SELECT @364FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 364
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
	
	SELECT @365FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 365
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @366FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 366
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @367FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 367
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @368FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 368
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @369FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 369
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID

	SELECT @370FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 370
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
	
	SELECT @DetailFieldID = DetailFieldID 
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 364
	AND t.ClientID = @ClientID
	AND t.LeadTypeID = @LeadTypeID
		
	SELECT @DetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @DetailFieldID
	
	DECLARE @LogEntry varchar(2000)
	
	SELECT @LogEntry = ' @LeadID = ' + ISNULL(CONVERT(VARCHAR,@LeadID),'NULL') + CHAR(13) + CHAR(10)
					 + ',@LeadTypeID = ' + ISNULL(CONVERT(VARCHAR,@LeadTypeID),'NULL') + CHAR(13) + CHAR(10)
					 + ',@DetailFieldID = ' + ISNULL(CONVERT(VARCHAR,@DetailFieldID),'NULL') + CHAR(13) + CHAR(10)
					 + ',@TransactionID = ' + ISNULL(CONVERT(VARCHAR,@TransactionID),'NULL') + CHAR(13) + CHAR(10)
					 
	EXEC @LogID = _C00_LogIt 'Info', '__InsertCallHistoryRecord_2', 'Test Log', @LogEntry, 26932 /*CS*/
	
	INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit, SourceID)
	VALUES (@ClientID, @CustomerID, @DetailFieldID, @DetailFieldPageID, 1, 1, @LogID)
	
	SELECT @TableRowID = SCOPE_IDENTITY()
	
	INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
	VALUES  (@ClientID, @CustomerID, @TableRowID, @364FieldID, CONVERT(VARCHAR(2000),@RowID)),
			(@ClientID, @CustomerID, @TableRowID, @365FieldID, CONVERT(VARCHAR(2000),@ListID)),
			(@ClientID, @CustomerID, @TableRowID, @366FieldID, @SchemaName),
			(@ClientID, @CustomerID, @TableRowID, @367FieldID, CONVERT(VARCHAR(2000),@CaseID)),
			(@ClientID, @CustomerID, @TableRowID, @368FieldID, CONVERT(VARCHAR(2000),@TransactionID)),
			(@ClientID, @CustomerID, @TableRowID, @369FieldID, @AgentsName),
			(@ClientID, @CustomerID, @TableRowID, @370FieldID, CONVERT(VARCHAR(2000),@ApproximateDateTimeOfCall))
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[__InsertCallHistoryRecord_2] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__InsertCallHistoryRecord_2] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__InsertCallHistoryRecord_2] TO [sp_executeall]
GO
