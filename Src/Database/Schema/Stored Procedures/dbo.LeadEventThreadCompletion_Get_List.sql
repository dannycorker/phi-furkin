SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the LeadEventThreadCompletion table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEventThreadCompletion_Get_List]

AS


				
				SELECT
					[LeadEventThreadCompletionID],
					[ClientID],
					[LeadID],
					[CaseID],
					[FromLeadEventID],
					[FromEventTypeID],
					[ThreadNumberRequired],
					[ToLeadEventID],
					[ToEventTypeID]
				FROM
					[dbo].[LeadEventThreadCompletion] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEventThreadCompletion_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_Get_List] TO [sp_executeall]
GO
