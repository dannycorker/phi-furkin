SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-10-04
-- Description:	Create Ultra User Based on Normal User
-- UPDATES:		Simon Brushett, 2011-12-08, Changed to call into the CP_Insert_Helper proc as direct insert into CP no longer allowed
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateUltraUser]
@ClientPersonnelID INT,
@UltraUsername VARCHAR(200),
@UltraPassword VARCHAR(200),
@Domain VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @NewClientPersonnelID INT


	DECLARE @ClientID INT,
			@TitleID INT,
			@Firstname VARCHAR(100),
			@LastName VARCHAR(100),
			@JobTitle VARCHAR(100),
			@Password VARCHAR(65),
			@ClientPersonnelAdminGroupID INT,
			@EmailAddress VARCHAR(255),
			@Salt VARCHAR(50),
			@ClientOfficeID INT
			
	SELECT	@ClientID = ClientID,
			@TitleID = TitleID,
			@Firstname = FirstName,
			@LastName = LastName,
			@JobTitle = JobTitle,
			@Password = Password,
			@ClientPersonnelAdminGroupID = ClientPersonnelAdminGroupID,
			@EmailAddress = EmailAddress,
			@Salt = Salt,
			@ClientOfficeID = ClientOfficeID
	FROM ClientPersonnel WITH (NOLOCK)
	WHERE ClientPersonnelID = @ClientPersonnelID
	
	EXEC @NewClientPersonnelID = dbo.ClientPersonnel_Insert_Helper @ClientID, @FirstName, @LastName, @EmailAddress, @Password, @Salt, @ClientOfficeID, @TitleID, NULL, @JobTitle, @ClientPersonnelAdminGroupID

	INSERT INTO ThirdPartyMappingUserCredential (ThirdPartySystemID, ClientID, UserID, UserName, Password, Salt, DOMAIN)
	VALUES (4,136,@NewClientPersonnelID, @UltraUsername, @UltraPassword, 1,@Domain)
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateUltraUser] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateUltraUser] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateUltraUser] TO [sp_executeall]
GO
