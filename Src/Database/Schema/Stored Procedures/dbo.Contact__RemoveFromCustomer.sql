SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green, Tito Neves
-- Create date: 2015-08-11
-- Description:	#33722 Pretend to delete a contact by moving it to a new (test) customer
-- =============================================
CREATE PROCEDURE [dbo].[Contact__RemoveFromCustomer] 
	@ContactID INT, 
	@WhoChanged INT, 
	@DeletionClientID INT = 20,
	@ChangeSource VARCHAR(200) = 'ContactList Delete'
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @OldCustomerID INT, 
	@NewCustomerID INT, 
	@CurrentDefaultContactID INT
	
	/* Find the old customer so that we can update history and possibly remove this as the default ContactID */
	SELECT @OldCustomerID = co.CustomerID
	FROM dbo.Contact co WITH (NOLOCK) 
	WHERE co.ContactID = @ContactID 
	
	/* Create a new, dummy customer to hold the unwanted contact details */
	INSERT INTO dbo.Customers(ClientID, TitleID, IsBusiness, FirstName, LastName, AquariumStatusID, Test, DoNotEmail, DoNotSellToThirdParty, WhoChanged, WhenChanged, ChangeSource, Comments)
	VALUES (@DeletionClientID, 0, 1, @OldCustomerID, @ContactID, 3, 1, 0, 0, @WhoChanged, dbo.fn_GetDate_Local(), @ChangeSource, 'Created to store unwanted contact details from CustomerID = ' + CAST(@OldCustomerID AS VARCHAR) + '  and old ContactID = ' + CAST(@ContactID AS VARCHAR))
	
	SET @NewCustomerID = SCOPE_IDENTITY()
	
	/* Check to see if the contact we are "deleting" was the default contact for the old customer record */
	SELECT @CurrentDefaultContactID = c.DefaultContactID 
	FROM dbo.Customers c WITH (NOLOCK) 
	WHERE c.CustomerID = @OldCustomerID 
	
	IF @CurrentDefaultContactID = @ContactID
	BEGIN
		UPDATE dbo.Customers 
		SET WhoChanged = @WhoChanged, 
		WhenChanged = dbo.fn_GetDate_Local(), 
		ChangeSource = @ChangeSource, 
		DefaultContactID = NULL, 
		Comments = CASE WHEN Comments IS NULL THEN '' ELSE '; ' END + 'Deleted the default Contact record ' + CAST(@ContactID AS VARCHAR) + ' and moved it to CustomerID = ' + CAST(@NewCustomerID AS VARCHAR)
		WHERE CustomerID = @OldCustomerID
	END
	
	/* Finally move the Contact record to the new Customer */
	UPDATE dbo.Contact 
	SET CustomerID = @NewCustomerID 
	WHERE ContactID = @ContactID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Contact__RemoveFromCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Contact__RemoveFromCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Contact__RemoveFromCustomer] TO [sp_executeall]
GO
