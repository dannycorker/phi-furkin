SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-11-2014
-- Description:	Gets a script by its name
-- =============================================
Create PROCEDURE [dbo].[Script__GetByScriptName]
	@ClientID INT,
	@ScriptName varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM Script WITH (NOLOCK) WHERE ClientID=@ClientID and ScriptName = @ScriptName
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[Script__GetByScriptName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script__GetByScriptName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__GetByScriptName] TO [sp_executeall]
GO
