SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes records in the QuestionPossibleAnswers
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionPossibleAnswers__DeleteByMasterQuestion]


	@MasterQuestionID int, @ClientID int   

AS


				DELETE FROM [dbo].[QuestionPossibleAnswers] 
				WHERE
					[MasterQuestionID] = @MasterQuestionID and ClientID = @ClientID
					
			



GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionPossibleAnswers__DeleteByMasterQuestion] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionPossibleAnswers__DeleteByMasterQuestion] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionPossibleAnswers__DeleteByMasterQuestion] TO [sp_executeall]
GO
