SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2018-07-16
-- Description:	throttle taskexecutor if there is nothing to process
-- @QueueID : Small = 1,Large = 2,Workflow = 3,Massive = 4,Equation = 5,Events = 6,Scripts = 7,Test = 8,BulkDocumentUploader = 9,DataLoader = 10,Zip = 11,Internal = 12
-- return 'true' if there is something to process
-- return 'false' if nothing to do
-- 2020-01-07 CPS on request of ACE | Ensure that new batch jobs will run as required
-- =============================================
CREATE PROCEDURE [dbo].[SchedulerCommand__ThingsInQueue] 
	@QueueID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @QueueID IN (1,2,3,4,8,12) AND EXISTS (
		SELECT	TOP 1 at.TaskID 
		FROM	AutomatedTask at WITH (NOLOCK) 
		/*JOIN	AutomatedTaskInfo ati WITH (NOLOCK) ON ati.TaskID = at.TaskID*/
		LEFT JOIN	AutomatedTaskInfo ati WITH (NOLOCK) ON ati.TaskID = at.TaskID AND IsNull(ati.QueueID, CASE at.WorkflowTask WHEN 1 THEN 3 ELSE 1 END) = @QueueID
		WHERE	/*ati.QueueID = @QueueID
		AND		*/at.NextRunDateTime < dbo.fn_GetDate_Local()
		AND		at.Enabled = 1
	)
	BEGIN
		SELECT 'true'
	END
	ELSE IF @QueueID = 6 AND EXISTS (
		SELECT	TOP 1 at.AutomatedEventQueueID
		FROM	AutomatedEventQueue at WITH (NOLOCK) 
		WHERE	at.LockDateTime IS NULL 
		AND		at.ErrorMessage IS NULL
		AND		dbo.fn_GetDate_Local() > ISNULL(at.EarliestRunDateTime,DATEADD(MS,-10,dbo.fn_GetDate_Local()))
	)
	BEGIN
		SELECT 'true'
	END
	ELSE IF @QueueID = 10 AND EXISTS (
		SELECT TOP 1 dlf.DataLoaderFileID
		FROM DataLoaderFile dlf WITH (NOLOCK)
		JOIN DataLoaderMap dm WITH (NOLOCK) on dm.DataLoaderMapID = dlf.DataLoaderMapID
		WHERE dlf.FileStatusID = 2 
		AND	dm.EnabledForUse = 1
		AND dlf.TargetFileLocation = 'Inbox'
		AND dlf.ScheduledDateTime < dbo.fn_GetDate_Local()
		AND dlf.ScheduledDateTime > DATEADD(WEEK,-1,dbo.fn_GetDate_Local())
	)
	BEGIN
		SELECT 'true'
	END
	ELSE IF @QueueID = 11 AND EXISTS (
		SELECT TOP 1 dzi.DocumentZipInformationID
		FROM DocumentZipInformation dzi WITH (NOLOCK)
		JOIN DocumentZip dz WITH (NOLOCK) ON dz.DocumentZipInformationID = dzi.DocumentZipInformationID
		JOIN DocumentQueue dq WITH (NOLOCK) ON dq.DocumentQueueID = dz.DocumentQueueID
		WHERE dzi.[StatusID] IN (35,39) /* Pending or Interupted */
	)
	BEGIN
		SELECT 'true'
	END
	ELSE
	BEGIN
		SELECT 'false' [Response]
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[SchedulerCommand__ThingsInQueue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SchedulerCommand__ThingsInQueue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SchedulerCommand__ThingsInQueue] TO [sp_executeall]
GO
