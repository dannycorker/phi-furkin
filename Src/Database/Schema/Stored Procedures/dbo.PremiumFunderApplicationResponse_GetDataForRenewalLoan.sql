SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2019-12-04
-- Description:	Gets data for new Premium Funding application renewal loan.
-- =============================================
CREATE PROCEDURE [dbo].[PremiumFunderApplicationResponse_GetDataForRenewalLoan]
	@MatterID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	DECLARE @AccountID INT,
			@BankAccountName VARCHAR(35),
			@AccountNumber INT,
			@SortCode VARCHAR(6),
			@BankName VARCHAR(35) = ''

	SELECT @AccountID = ppp.AccountID
	FROM PurchasedProduct p WITH ( NOLOCK ) 
	INNER JOIN PurchasedProductPaymentSchedule ppp WITH ( NOLOCK ) on ppp.PurchasedProductID = p.PurchasedProductID 
	INNER JOIN PurchasedProductPaymentScheduleType ppt WITH ( NOLOCK ) on ppt.PurchasedProductPaymentScheduleTypeID = ppp.PurchasedProductPaymentScheduleTypeID
	WHERE p.ObjectID = @MatterID
	AND ppp.PaymentStatusID in (1) 
	AND ppp.PurchasedProductPaymentScheduleTypeID in (1) 
	AND NOT EXISTS (SELECT * FROM PurchasedProductPaymentSchedule p WITH ( NOLOCK ) WHERE p.PurchasedProductPaymentScheduleParentID = ppp.PurchasedProductPaymentScheduleID
	AND  ppp.PurchasedProductPaymentScheduleID <>  p.PurchasedProductPaymentScheduleID)

	IF @AccountID IS NULL
	BEGIN

		SELECT TOP(1) @AccountID = a.AccountID
		FROM Matter matter WITH (NOLOCK)
		INNER JOIN Account a WITH (NOLOCK) ON a.CustomerID = matter.CustomerID		
		WHERE matter.MatterID = @MatterID
		AND a.AccountTypeID = 4
		ORDER BY AccountID DESC
	
	END

	SELECT @BankAccountName = a.AccountHolderName, @AccountNumber = a.AccountNumber, @SortCode = a.Sortcode, @BankName = a.Bankname
	FROM Account a WITH (NOLOCK)
	WHERE a.AccountID = @AccountID

	SELECT
	customer.Address1,
	CASE ISNULL(customer.Address2,'') WHEN '' THEN customer.Town ELSE customer.Address2 END AS Address2,
	CASE ISNULL(customer.Address2,'') WHEN '' THEN '' ELSE customer.Town END AS City,
	ISNULL(customer.County,'') AS County,
	customer.Postcode AS Postcode,
	@AccountNumber AS AccountNumber,
	@BankAccountName AS BankAccountName,
	@BankName AS BankName,
	@SortCode AS Sortcode,
	customer.CustomerID AS CustomerID,
	customer.DateOfBirth AS DOB,
	customer.EmailAddress AS Email,
	customer.FirstName AS FirstName,
	ISNULL(customer.HomeTelephone,'') AS HomePhone,
	customer.LastName AS LastName,
	ISNULL(customer.MobileTelephone,'') AS Mobile,
	customer.TitleID AS TitleID
	FROM Matter matter WITH (NOLOCK)
	INNER JOIN Customers customer WITH (NOLOCK) ON customer.CustomerID = matter.CustomerID
	WHERE matter.MatterID = @MatterID

END







GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderApplicationResponse_GetDataForRenewalLoan] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PremiumFunderApplicationResponse_GetDataForRenewalLoan] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PremiumFunderApplicationResponse_GetDataForRenewalLoan] TO [sp_executeall]
GO
