SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-01-07
-- Description:	Rollback dates by n days or 1 month (for testing)
-- Mods 
-- JEL 2018-03-18 Added Billing system param to complete billing
-- AHOD 2018-12-05 Removed - sign from filter where m.MatterID =- @MatterID (Line 46)
-- 2020-02-13 CPS for JIRA AAG-108 | Included PurchasedProductPaymentScheduleDetail
-- 2020-03-02 GPR for JIRA AAG-202 | Added check on CountryID from ClientID as the switch for removing the AccountMandate check
-- 2020-03-18 GPR for AAG-512		| Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- 2020-09-21 GPR					| Rollback Wellness History Start Date / End Date
-- 2021-04-27 ACE for FURKIN-514	| Fix cps updates as transactional refunds uses account that isnt on the PP (same with any change account)
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Testing_RollBackAllDates] 
(
	@MatterID INT
)

AS
BEGIN

	SET NOCOUNT ON;

--declare	
--	@MatterID INT = 771
	
		
	declare @PolicyAdminCaseID INT,
			@PolicyAdminMatterID INT,
			@PolicyAdminLeadID INT,
			@PassedMatterID INT,
			@NewDate DATE,
			@CustomerID INT,
			@LeadID INT,
			@CaseID INT,
			@Interval VARCHAR(10),
			@IntervalAmount INT=0, -- 0 = 1 month; > 0 = number days
			@PolicyMatters tvpIntInt,
			@IncludeCollections INT = 0,
			@ClientID INT
	
	SELECT @PolicyAdminLeadID = m.leadid, @CaseID=CaseID, @CustomerID = m.CustomerID, @ClientID = m.ClientID 
	FROM Matter m with (NOLOCK)
	where m.MatterID = @MatterID /*AHOD 2018-12-05 removed =-*/

	SELECT @IntervalAmount=dbo.fnGetDvAsInt(175431,@CaseID)
	-- clear interval amount field
	EXEC _C00_SimpleValueIntoField 175431, '', @MatterID
	
	SELECT @Interval=CASE WHEN @IntervalAmount=0 THEN 'm' ELSE 'd' END
	SELECT @IntervalAmount=CASE WHEN @Interval='m' THEN -1 ELSE -(@IntervalAmount) END
	
	SELECT @PassedMatterID = @MatterID
			
	SELECT @MatterID = dbo.fn_C600_GetCollectionsMatterID (@MatterID)
	
	/* WARNING!!!! The case ID is re-used as is the matter ID. This can cause confusion!!! */
	SELECT @LeadID = m.LeadID, @CaseID = m.CaseID 
	FROM Matter m WITH (NOLOCK) 
	WHERE m.MatterID=@MatterID		

	-- Collections lead dates

	-- all events & notes
	UPDATE LeadEvent SET
		WhenCreated=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,WhenCreated) ELSE  DATEADD(MONTH,@IntervalAmount,WhenCreated) END,
		WhenFollowedUp=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,WhenFollowedUp) ELSE  DATEADD(MONTH,@IntervalAmount,WhenFollowedUp) END,
		FollowupDateTime=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,FollowupDateTime) ELSE  DATEADD(MONTH,@IntervalAmount,FollowupDateTime) END
	WHERE CaseID=@CaseID

	-- MANDATE
	-- date lodgement made
	update MatterDetailValues set DetailValue=ISNULL(CONVERT(VARCHAR(10),CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END,120),'') where MatterID=@MatterID AND DetailFieldID=170182
	--update MatterDetailValues set DetailValue='2015-01-08' where MatterID=@MatterID AND DetailFieldID=170182

	-- mandate request history - bacs file date
	update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@MatterID AND DetailFieldID=170080
	-- mandate request history - request effective date
	update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@MatterID AND DetailFieldID=170081
			
	-- PAYMENT DETAILS
	-- date of first or next
	update MatterDetailValues set DetailValue=ISNULL(CONVERT(VARCHAR(10),CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END,120),'') where MatterID=@MatterID AND DetailFieldID=170185
	-- date of next regular payment
	update MatterDetailValues set DetailValue=ISNULL(CONVERT(VARCHAR(10),CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END,120),'') where MatterID=@MatterID AND DetailFieldID=175599

	-- lead details

	-- customer collection history (itemised) - bacs file date
	update tdv set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END 
	from TableDetailValues tdv 
	INNER JOIN TableRows tr WITH (NOLOCK) ON tdv.TableRowID=tr.TableRowID and tr.DetailFieldID=170119
	where tdv.LeadID=@LeadID AND tdv.DetailFieldID=170080
	-- customer collection history (itemised) - request effective date
	update tdv set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END 
	from TableDetailValues tdv 
	INNER JOIN TableRows tr WITH (NOLOCK) ON tdv.TableRowID=tr.TableRowID and tr.DetailFieldID=170119
	where tdv.LeadID=@LeadID AND tdv.DetailFieldID=170081

	-- customer details
	-- customer collection history (summarised) - bacs file date
	update tdv set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate)  END
	from TableDetailValues tdv 
	INNER JOIN TableRows tr WITH (NOLOCK) ON tdv.TableRowID=tr.TableRowID and tr.DetailFieldID=170245
	where tdv.CustomerID=@CustomerID AND tdv.DetailFieldID=170080
	-- customer collection history (summarised) - request effective date
	update tdv set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate)  END
	from TableDetailValues tdv 
	INNER JOIN TableRows tr WITH (NOLOCK) ON tdv.TableRowID=tr.TableRowID and tr.DetailFieldID=170245
	where tdv.CustomerID=@CustomerID AND tdv.DetailFieldID=170081

	-- POLICY ADMIN
	
	-- all related policy admin leads
	
			
	INSERT INTO @PolicyMatters (ID1,ID2)
	SELECT @PassedMatterID, 0
	/*GPR 2020-01-21 use the PassedInMatterID only for SDLPC-108 - this prevents a double roll when Policies share the same Collections MatterID*/
	--SELECT ltr.FromMatterID,0 FROM LeadTypeRelationship ltr
	--WITH (NOLOCK) WHERE ltr.ToMatterID= @MatterID and ltr.FromLeadTypeID=1492
	
	IF NOT EXISTS ( SELECT * FROM @PolicyMatters ) 
	BEGIN 
	
		INSERT INTO @PolicyMatters (ID1,ID2)
		SELECT ltr.FromMatterID,0 FROM LeadTypeRelationship ltr
		WITH (NOLOCK) WHERE ltr.FromLeadID=@PolicyAdminLeadID and ltr.FromLeadTypeID=1492 
		
	
	END
	
	WHILE EXISTS ( SELECT * FROM @PolicyMatters WHERE ID2=0 )
	BEGIN
	
		SELECT TOP 1 @PolicyAdminMatterID=ID1 FROM @PolicyMatters WHERE ID2=0
		
		SELECT @IncludeCollections+=1
	
		SELECT @PolicyAdminLeadID=LeadID, @PolicyAdminCaseID=CaseID FROM Matter WITH (NOLOCK) WHERE MatterID=@PolicyAdminMatterID	
		
		-- all events & notes
		UPDATE LeadEvent SET
			WhenCreated=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,WhenCreated) ELSE  DATEADD(MONTH,@IntervalAmount,WhenCreated) END,
			WhenFollowedUp=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,WhenFollowedUp) ELSE  DATEADD(MONTH,@IntervalAmount,WhenFollowedUp) END,
			FollowupDateTime=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,FollowupDateTime) ELSE  DATEADD(MONTH,@IntervalAmount,FollowupDateTime) END
		WHERE CaseID=@PolicyAdminCaseID

		-- pet age 
		update LeadDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where LeadID=@PolicyAdminLeadID AND DetailFieldID=144274

		-- Policy Sale Date
		update MatterDetailValues set DetailValue=ISNULL(CONVERT(VARCHAR(10),CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END,120),'') where MatterID=@PolicyAdminMatterID AND DetailFieldID=177418

		-- Policy Inception Date
		update MatterDetailValues set DetailValue=ISNULL(CONVERT(VARCHAR(10),CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END,120),'') where MatterID=@PolicyAdminMatterID AND DetailFieldID=170035

		-- Policy start Date
		update MatterDetailValues set DetailValue=ISNULL(CONVERT(VARCHAR(10),CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END,120),'') where MatterID=@PolicyAdminMatterID AND DetailFieldID=170036

		-- Policy end Date
		--update MatterDetailValues set DetailValue=ISNULL(CONVERT(VARCHAR(10),CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END,120),'') where MatterID=@PolicyAdminMatterID AND DetailFieldID=170037

		-- Non-Pay Cancellation Date and cancel cover back to date / NG 2020-11-04
		update MatterDetailValues set DetailValue=ISNULL(CONVERT(VARCHAR(10),CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END,120),'') where MatterID=@PolicyAdminMatterID AND DetailFieldID=314260
		update MatterDetailValues set DetailValue=ISNULL(CONVERT(VARCHAR(10),CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END,120),'') where MatterID=@PolicyAdminMatterID AND DetailFieldID=314261
		
		-- Policy end Date
		/*GPR 2020-01-07, 2020-01-22 Adjusted to 364*/
		-- 2020-02-16 CPS for JIRA AAG-110 | Changed to ISNULL(DATEADD(YEAR,1,DATEADD(DAY,-1,@StartDate)),'') so that we don't get screwed by leap years
		DECLARE @StartDate DATE
		SELECT @StartDate = [dbo].[fnGetSimpleDvAsDate](170036, @PolicyAdminMatterID) /*Policy Start Date*/
		update MatterDetailValues set DetailValue=ISNULL(DATEADD(YEAR,1,DATEADD(DAY,-1,@StartDate)),'') where MatterID=@PolicyAdminMatterID AND DetailFieldID = 170037 /*Policy End Date*/

		-- Covered Until Date /*leap years are evil. Covered until date = policy end date NG 2020-06-22*/
		update MatterDetailValues set DetailValue=ISNULL(DATEADD(YEAR,1,DATEADD(DAY,-1,@StartDate)),'')  where MatterID=@PolicyAdminMatterID AND DetailFieldID=177287 /*covered until date*/

		-- Policy terms Date
		update MatterDetailValues set DetailValue=ISNULL(CONVERT(VARCHAR(10),CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END,120),'') where MatterID=@PolicyAdminMatterID AND DetailFieldID=176925

		-- Imported update date
		update MatterDetailValues set DetailValue=ISNULL(CONVERT(VARCHAR(10),CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END,120),'') where MatterID=@PolicyAdminMatterID AND DetailFieldID=170044

		-- Policy Renewal date
		update MatterDetailValues set DetailValue=ISNULL(CONVERT(VARCHAR(10),CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END,120),'') where MatterID=@PolicyAdminMatterID AND DetailFieldID=175307

		-- historical policy - inception
		update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@PolicyAdminMatterID AND DetailFieldID=145662
		-- historical policy - start
		update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@PolicyAdminMatterID AND DetailFieldID=145663
		-- historical policy - end
		update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@PolicyAdminMatterID AND DetailFieldID=145664
		-- historical policy - terms date
		update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@PolicyAdminMatterID AND DetailFieldID=170092

		-- policy premiums - date
		update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@PolicyAdminMatterID AND DetailFieldID=170066
		-- policy premiums - date paid
		update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@PolicyAdminMatterID AND DetailFieldID=175473
		-- policy premiums - from
		update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@PolicyAdminMatterID AND DetailFieldID=170076
		-- policy premiums - to
		update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@PolicyAdminMatterID AND DetailFieldID=170077
		-- policy premiums - date due
		update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@PolicyAdminMatterID AND DetailFieldID=175597

		-- premium adjustment history - from 
		update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@PolicyAdminMatterID AND DetailFieldID=175346
		-- premium adjustment history - to 
		update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@PolicyAdminMatterID AND DetailFieldID=175347
		-- premium adjustment history - adjustment date 
		update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@PolicyAdminMatterID AND DetailFieldID=175396
		-- premium adjustment history - adjustment application date 
		update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@PolicyAdminMatterID AND DetailFieldID=175400

		-- Wellness History / GPR 2020-09-21
		update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@PolicyAdminMatterID AND DetailFieldID=314237
		update TableDetailValues set DetailValue=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END where MatterID=@PolicyAdminMatterID AND DetailFieldID=314239
		
		-- Projected Cancellation Date / GPR 2021-03-15
		update MatterDetailValues set DetailValue=ISNULL(CONVERT(VARCHAR(10),CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END,120),'') where MatterID=@PolicyAdminMatterID AND DetailFieldID=315274 

		
		-- 2020-09-23 GPR for ALM - get latest wellness history row if it's active and update it to the Policy end date (to adjust for Leaps)
		DECLARE @tablerowid INT
		SELECT @tablerowid = (SELECT TOP(1) tr.tablerowid
								FROM TableRows tr WITH (NOLOCK)
								INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 314236
								WHERE tr.MatterID = @PolicyAdminMatterID
								AND tr.DetailFieldID = 314240
								AND tdv.ValueInt = 74577 /*Active*/
								ORDER BY 1 DESC)

		IF @tablerowid > 0
		BEGIN
			UPDATE TableDetailValues SET DetailValue= @StartDate WHERE MatterID=@PolicyAdminMatterID AND DetailFieldID=314237 AND TableRowID = @tablerowid /*Start*/
			UPDATE TableDetailValues SET DetailValue=ISNULL(DATEADD(YEAR,1,DATEADD(DAY,-1,@StartDate)),'') WHERE MatterID=@PolicyAdminMatterID AND DetailFieldID=314239 AND TableRowID = @tablerowid /*End*/
		END


		UPDATE wp
		SET  RatingDateTime			= CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,RatingDateTime		) ELSE  DATEADD(MONTH,@IntervalAmount,RatingDateTime		) END
			,ValidFrom				= CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValidFrom			) ELSE  DATEADD(MONTH,@IntervalAmount,ValidFrom				) END
			,ValidTo				= CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValidTo				) ELSE  DATEADD(MONTH,@IntervalAmount,ValidTo				) END
			,AdjustmentRequestDate	= CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,AdjustmentRequestDate) ELSE  DATEADD(MONTH,@IntervalAmount,AdjustmentRequestDate	) END
			--,WhenCreated			= Deliberately omitted for diagnosis
			--,WhenChanged			= Deliberately omitted for diagnosis
		FROM WrittenPremium wp WITH (NOLOCK) 
		WHERE wp.MatterID = @PolicyAdminMatterID


		-- Cancellation Date
		update MatterDetailValues set DetailValue=ISNULL(CONVERT(VARCHAR(10),CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END,120),'') where MatterID=@PolicyAdminMatterID AND DetailFieldID=170055

		-- Adjustment Date
		update MatterDetailValues set DetailValue=ISNULL(CONVERT(VARCHAR(10),CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValueDate) ELSE  DATEADD(MONTH,@IntervalAmount,ValueDate) END,120),'') where MatterID=@PolicyAdminMatterID AND DetailFieldID=175442

		-- billing stuff

		UPDATE PurchasedProduct 
		SET 
			ValidFrom=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValidFrom) ELSE DATEADD(MONTH,@IntervalAmount,ValidFrom) END,
			ValidTo=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ValidTo) ELSE DATEADD(MONTH,@IntervalAmount,ValidTo) END,
			FirstPaymentDate=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,FirstPaymentDate) ELSE DATEADD(MONTH,@IntervalAmount,FirstPaymentDate) END,
			PaymentScheduleCreatedOn=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,PaymentScheduleCreatedOn) ELSE DATEADD(MONTH,@IntervalAmount,PaymentScheduleCreatedOn) END,
			ProductPurchasedOnDate=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ProductPurchasedOnDate) ELSE DATEADD(MONTH,@IntervalAmount,ProductPurchasedOnDate) END
		WHERE ObjectID=@PolicyAdminMatterID 

		UPDATE ppps
		SET 
			CoverFrom=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,CoverFrom) ELSE DATEADD(MONTH,@IntervalAmount,CoverFrom) END,
			CoverTo=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,CoverTo) ELSE DATEADD(MONTH,@IntervalAmount,CoverTo) END,
			ActualCollectionDate=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ActualCollectionDate) ELSE DATEADD(MONTH,@IntervalAmount,ActualCollectionDate) END,
			PaymentDate=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,PaymentDate) ELSE DATEADD(MONTH,@IntervalAmount,PaymentDate) END
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
		INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON ppps.PurchasedProductID=pp.PurchasedProductID
		WHERE pp.ObjectID=@PolicyAdminMatterID

		IF @IncludeCollections = 1
		BEGIN
		
			UPDATE cps
			set PaymentDate=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,cps.PaymentDate) ELSE DATEADD(MONTH,@IntervalAmount,cps.PaymentDate) END,
				ActualCollectionDate=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,cps.ActualCollectionDate) ELSE DATEADD(MONTH,@IntervalAmount,cps.ActualCollectionDate) END
			FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
			INNER JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
			/*INNER JOIN Account a WITH (NOLOCK) ON cps.AccountID=a.AccountID*/
			INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON pp.PurchasedProductID = ppps.PurchasedProductID /*a.AccountID=pp.AccountID*/
			WHERE pp.ObjectID=@PolicyAdminMatterID	
		
			UPDATE cl
			set TransactionDate=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,cps.PaymentDate) ELSE DATEADD(MONTH,@IntervalAmount,cps.PaymentDate) END,
				EffectivePaymentDate=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,EffectivePaymentDate) ELSE DATEADD(MONTH,@IntervalAmount,EffectivePaymentDate) END
			FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
			INNER JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
			/*INNER JOIN Account a WITH (NOLOCK) ON cps.AccountID=a.AccountID*/
			INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON pp.PurchasedProductID = ppps.PurchasedProductID /*a.AccountID=pp.AccountID*/
			INNER JOIN CustomerLedger cl WITH (NOLOCK) ON cps.CustomerLedgerID=cl.CustomerLedgerID
			WHERE pp.ObjectID=@PolicyAdminMatterID	
		
			UPDATE p
			set PaymentDateTime=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,cps.PaymentDate) ELSE DATEADD(MONTH,@IntervalAmount,cps.PaymentDate) END
			FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
			INNER JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
			/*INNER JOIN Account a WITH (NOLOCK) ON cps.AccountID=a.AccountID*/
			INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON pp.PurchasedProductID = ppps.PurchasedProductID /*a.AccountID=pp.AccountID*/
			INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
			WHERE pp.ObjectID=@PolicyAdminMatterID	

			/*ALM 2021-03-11 FURKIN-397*/
			UPDATE cps
			set PaymentDate=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,PaymentDate) ELSE DATEADD(MONTH,@IntervalAmount,PaymentDate) END,
				ActualCollectionDate=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,ActualCollectionDate) ELSE DATEADD(MONTH,@IntervalAmount,ActualCollectionDate) END
			FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
			INNER JOIN PaymentStatus ps WITH (NOLOCK) ON ps.PaymentStatusID = cps.PaymentStatusID
			INNER JOIN Account a WITH (NOLOCK) ON a.AccountID = cps.AccountID AND AccountUseID = 2
			INNER JOIN OneVisionClaimPayment claimdata WITH (NOLOCK) ON claimdata.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
			INNER JOIN OneVisionClaim visionclaim WITH (NOLOCK) ON visionclaim.OneVisionClaimID = claimdata.OneVisionClaimID
			WHERE visionclaim.PAMatterID = @PolicyAdminMatterID
			
			/*GPR 2020-03-02 for AAG-202*/
			DECLARE @CountryID INT

			SELECT @CountryID = cl.CountryID 
			FROM Clients cl WITH (NOLOCK) 
			WHERE cl.ClientID = @ClientID

			/*GPR 2020-03-02 for AAG-202*/
			IF @CountryID <> 14 /*Australia*/
			BEGIN

				UPDATE am
				set 
				DateRequested=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,DateRequested) ELSE DATEADD(MONTH,@IntervalAmount,DateRequested) END,
				FirstAcceptablePaymentDate=CASE WHEN @Interval='d' THEN DATEADD(DAY,@IntervalAmount,FirstAcceptablePaymentDate) ELSE DATEADD(MONTH,@IntervalAmount,FirstAcceptablePaymentDate) END
				FROM AccountMandate	am
				INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON am.AccountID=pp.AccountID
				WHERE pp.ObjectID=@PolicyAdminMatterID
				AND am.MandateStatusID IN (1,2,3)
			END

		END
		
		UPDATE @PolicyMatters 
		SET ID2=1 
		WHERE ID1=@PolicyAdminMatterID
		
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Testing_RollBackAllDates] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Testing_RollBackAllDates] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Testing_RollBackAllDates] TO [sp_executeall]
GO
