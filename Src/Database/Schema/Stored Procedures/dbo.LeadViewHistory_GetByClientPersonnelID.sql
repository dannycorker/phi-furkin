SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadViewHistory table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadViewHistory_GetByClientPersonnelID]
(

	@ClientPersonnelID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[LeadViewHistoryID],
					[ClientPersonnelID],
					[ClientID],
					[LeadID],
					[WhenViewed]
				FROM
					[dbo].[LeadViewHistory] WITH (NOLOCK) 
				WHERE
					[ClientPersonnelID] = @ClientPersonnelID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_GetByClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadViewHistory_GetByClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_GetByClientPersonnelID] TO [sp_executeall]
GO
