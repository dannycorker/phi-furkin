SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*
----------------------------------------------------------------------------------------------------
-- Date Created: 10 May 2007

-- Created By: Jim
-- Purpose: Select records from the Matter, Cases and LeadStatus tables by LeadID
-- 2007-08-05 JWG ANSI Joins
-- 2007-12-04 JWG Return user-defined columns as well (if this user has rights to see fields from those pages).
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GetMatterAndCaseDetailsByLeadID_SafeCopy]
(

	@LeadID int, 
	@UserID int = null, 
	@CaseID int = null 
)
AS
BEGIN


	DECLARE @SqlStatement varchar(8000), @CaseClause varchar(100), @BitFields varchar(1000)

	DECLARE @FieldHeader varchar(102), @LookupListID int

	DECLARE @FieldList varchar(2000), @TableList varchar(2000) 

	DECLARE @Field1 int, @Field2 int, @Field3 int, @Field4 int 

	DECLARE @ShowMatterRef bit, @ShowMatterStatus bit, @ShowMatterID bit

	DECLARE @LeadTypeID int, @FieldTypeID int


	SET NOCOUNT ON


	IF @CaseID IS NULL  
	BEGIN
		SET @CaseClause = ''
	END
	ELSE
	BEGIN
		SET @CaseClause = ' AND m.CaseID = ' + convert(varchar, @CaseID)
	END

	SELECT @FieldList = '', @TableList = '' 

	SELECT @Field1 = Field1, @Field2 = Field2, @Field3 = Field3, @Field4 = Field4, 
	@ShowMatterRef = mld.MatterRef, @ShowMatterStatus = mld.MatterStatus, @ShowMatterID = mld.MatterID,
	@LeadTypeID = l.LeadTypeID  
	FROM Lead l 
	LEFT JOIN MatterListDisplay mld ON mld.LeadTypeID = l.LeadTypeID
	WHERE l.LeadID = @LeadID

	IF @ShowMatterRef IS NULL
	BEGIN
		SELECT @BitFields = '' 
	END
	ELSE
	BEGIN
		SELECT @BitFields = ',' + convert(varchar, @ShowMatterRef) + ' as ShowMatterRef,' + convert(varchar, @ShowMatterStatus) + ' as ShowMatterStatus,' + convert(varchar, @ShowMatterID) + ' as ShowMatterID'
	END

	/* Just-in-time declaration and population of @AllowedPages should speed this proc up a lot */
	IF @Field1 IS NOT NULL OR @Field2 IS NOT NULL OR @Field3 IS NOT NULL OR @Field4 IS NOT NULL
	BEGIN

		DECLARE @AllowedPages TABLE (ObjectID int, RightID int)

		/* We are only going to show this user fields from pages that they can view. */
		INSERT INTO @AllowedPages (ObjectID, RightID) 
		SELECT f.ObjectID, f.RightID 
		FROM fnDetailFieldPagesSecure(@UserID, @LeadTypeID) f

		IF @Field1 IS NOT NULL
		BEGIN
			SELECT @FieldHeader = IsNull('[' + FieldCaption + ']',''), @LookupListID = LookupListID, @FieldTypeID = QuestionTypeID 
			FROM dbo.DetailFields (nolock) INNER JOIN @AllowedPages ap ON DetailFields.DetailFieldPageID = ap.ObjectID AND ap.RightID > 0  
			WHERE DetailFieldID = @Field1
			
			IF @@ROWCOUNT = 1
			BEGIN
				IF @LookupListID IS NULL
				BEGIN
					IF @FieldTypeID = 5
					BEGIN
						SELECT @FieldList = ',CASE IsNull(mdv1.DetailValue, '''') WHEN '''' THEN '''' ELSE convert(char(2), substring(mdv1.DetailValue, 9, 2)) + ''/'' + convert(char(2), substring(mdv1.DetailValue, 6, 2)) + ''/'' + convert(char(4), substring(mdv1.DetailValue, 1, 4))  END as ' + @FieldHeader
					END
					ELSE
					BEGIN
						SELECT @FieldList = ',IsNull(mdv1.DetailValue,'''') as ' + @FieldHeader
					END

					SELECT @TableList = 'LEFT JOIN dbo.MatterDetailValues mdv1 (nolock) on mdv1.MatterID = m.MatterID AND mdv1.DetailFieldID = ' + convert(varchar, @Field1)
				END
				ELSE
				BEGIN
					SELECT @FieldList = ',IsNull(luli1.ItemValue,'''') as ' + @FieldHeader

					SELECT @TableList = 'LEFT JOIN dbo.MatterDetailValues mdv1 (nolock) on mdv1.MatterID = m.MatterID AND mdv1.DetailFieldID = ' + convert(varchar, @Field1) + ' LEFT JOIN dbo.LookupListItems luli1 (nolock) ON luli1.LookupListItemID = mdv1.ValueInt '
				END
			END
		END

		IF @Field2 IS NOT NULL
		BEGIN
			SELECT @FieldHeader = IsNull('[' + FieldCaption + ']',''), @LookupListID = LookupListID, @FieldTypeID = QuestionTypeID
			FROM dbo.DetailFields (nolock) INNER JOIN @AllowedPages ap ON DetailFields.DetailFieldPageID = ap.ObjectID AND ap.RightID > 0  
			WHERE DetailFieldID = @Field2

			IF @@ROWCOUNT = 1
			BEGIN
				IF @LookupListID IS NULL
				BEGIN
					IF @FieldTypeID = 5
					BEGIN
						SELECT @FieldList = @FieldList + ',CASE IsNull(mdv2.DetailValue, '''') WHEN '''' THEN '''' ELSE convert(char(2), substring(mdv2.DetailValue, 9, 2)) + ''/'' + convert(char(2), substring(mdv2.DetailValue, 6, 2)) + ''/'' + convert(char(4), substring(mdv2.DetailValue, 1, 4))  END as ' + @FieldHeader
					END
					ELSE
					BEGIN
						SELECT @FieldList = @FieldList + ',IsNull(mdv2.DetailValue,'''') as ' + @FieldHeader
					END

					SELECT @TableList = @TableList + 'LEFT JOIN dbo.MatterDetailValues mdv2 (nolock) on mdv2.MatterID = m.MatterID AND mdv2.DetailFieldID = ' + convert(varchar, @Field2)
				END
				ELSE
				BEGIN
					SELECT @FieldList = @FieldList + ',IsNull(luli2.ItemValue,'''') as ' + @FieldHeader

					SELECT @TableList = @TableList + 'LEFT JOIN dbo.MatterDetailValues mdv2 (nolock) on mdv2.MatterID = m.MatterID AND mdv2.DetailFieldID = ' + convert(varchar, @Field2) + ' LEFT JOIN dbo.LookupListItems luli2 (nolock) ON luli2.LookupListItemID = mdv2.ValueInt '
				END
			END
		END

		IF @Field3 IS NOT NULL
		BEGIN
			SELECT @FieldHeader = IsNull('[' + FieldCaption + ']',''), @LookupListID = LookupListID, @FieldTypeID = QuestionTypeID 
			FROM dbo.DetailFields (nolock) INNER JOIN @AllowedPages ap ON DetailFields.DetailFieldPageID = ap.ObjectID AND ap.RightID > 0  
			WHERE DetailFieldID = @Field3

			IF @@ROWCOUNT = 1
			BEGIN
				IF @LookupListID IS NULL
				BEGIN
					IF @FieldTypeID = 5
					BEGIN
						SELECT @FieldList = @FieldList + ',CASE IsNull(mdv3.DetailValue, '''') WHEN '''' THEN '''' ELSE convert(char(2), substring(mdv3.DetailValue, 9, 2)) + ''/'' + convert(char(2), substring(mdv3.DetailValue, 6, 2)) + ''/'' + convert(char(4), substring(mdv3.DetailValue, 1, 4))  END as ' + @FieldHeader
					END
					ELSE
					BEGIN
						SELECT @FieldList = @FieldList + ',IsNull(mdv3.DetailValue,'''') as ' + @FieldHeader
					END

					SELECT @TableList = @TableList + 'LEFT JOIN dbo.MatterDetailValues mdv3 (nolock) on mdv3.MatterID = m.MatterID AND mdv3.DetailFieldID = ' + convert(varchar, @Field3)
				END
				ELSE
				BEGIN
					SELECT @FieldList = @FieldList + ',IsNull(luli3.ItemValue,'''') as ' + @FieldHeader

					SELECT @TableList = @TableList + 'LEFT JOIN dbo.MatterDetailValues mdv3 (nolock) on mdv3.MatterID = m.MatterID AND mdv3.DetailFieldID = ' + convert(varchar, @Field3) + ' LEFT JOIN dbo.LookupListItems luli3 (nolock) ON luli3.LookupListItemID = mdv3.ValueInt '
				END
			END
		END

		IF @Field4 IS NOT NULL
		BEGIN
			SELECT @FieldHeader = IsNull('[' + FieldCaption + ']',''), @LookupListID = LookupListID, @FieldTypeID = QuestionTypeID 
			FROM dbo.DetailFields (nolock) INNER JOIN @AllowedPages ap ON DetailFields.DetailFieldPageID = ap.ObjectID AND ap.RightID > 0  
			WHERE DetailFieldID = @Field4

			IF @@ROWCOUNT = 1
			BEGIN
				IF @LookupListID IS NULL
				BEGIN
					IF @FieldTypeID = 5
					BEGIN
						SELECT @FieldList = @FieldList + ',CASE IsNull(mdv4.DetailValue, '''') WHEN '''' THEN '''' ELSE convert(char(2), substring(mdv4.DetailValue, 9, 2)) + ''/'' + convert(char(2), substring(mdv4.DetailValue, 6, 2)) + ''/'' + convert(char(4), substring(mdv4.DetailValue, 1, 4))  END as ' + @FieldHeader
					END
					ELSE
					BEGIN
						SELECT @FieldList = @FieldList + ',IsNull(mdv4.DetailValue,'''') as ' + @FieldHeader
					END

					SELECT @TableList = @TableList + 'LEFT JOIN dbo.MatterDetailValues mdv4 (nolock) on mdv4.MatterID = m.MatterID AND mdv4.DetailFieldID = ' + convert(varchar, @Field4)
				END
				ELSE
				BEGIN
					SELECT @FieldList = @FieldList + ',IsNull(luli4.ItemValue,'''') as ' + @FieldHeader

					SELECT @TableList = @TableList + 'LEFT JOIN dbo.MatterDetailValues mdv4 (nolock) on mdv4.MatterID = m.MatterID AND mdv4.DetailFieldID = ' + convert(varchar, @Field4) + ' LEFT JOIN dbo.LookupListItems luli4 (nolock) ON luli4.LookupListItemID = mdv4.ValueInt '
				END
			END
		END

	END


	SELECT @SqlStatement = 'SELECT m.MatterID,m.ClientID,m.MatterRef AS [Matter Ref],m.CustomerID,m.LeadID,m.MatterStatus,
	m.RefLetter AS [Ref Letter],m.BrandNew,m.CaseID,a.CaseNum,a.CaseRef,IsNull(ls.StatusName, '''') as CaseStatusName ' + @BitFields + @FieldList + ' FROM dbo.Lead l (nolock) INNER JOIN dbo.Matter m (nolock) ON l.LeadID = m.LeadID
	INNER JOIN dbo.Cases a (nolock) ON m.CaseID = a.CaseID 
	LEFT JOIN dbo.LeadStatus ls (nolock) ON a.ClientStatusID = ls.StatusID ' + @TableList + ' WHERE m.LeadID = ' + convert(varchar,@LeadID) + @CaseClause + ' ORDER BY a.CaseNum, m.RefLetter '

	/*PRINT @FieldList*/

	/*PRINT @SqlStatement*/

	EXEC(@SqlStatement)

END






GO
GRANT VIEW DEFINITION ON  [dbo].[GetMatterAndCaseDetailsByLeadID_SafeCopy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetMatterAndCaseDetailsByLeadID_SafeCopy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetMatterAndCaseDetailsByLeadID_SafeCopy] TO [sp_executeall]
GO
