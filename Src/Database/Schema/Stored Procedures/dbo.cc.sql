SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2017-06-27
-- Description: get top 100 rows from changecontrol table
-- =============================================
CREATE PROCEDURE [dbo].[cc]

AS
BEGIN

	SET NOCOUNT ON;
	
	select top 100 * 
	From ChangeControl cc with(nolock) 
	Order by cc.ChangeControlID desc

END
GO
GRANT VIEW DEFINITION ON  [dbo].[cc] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[cc] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[cc] TO [sp_executeall]
GO
