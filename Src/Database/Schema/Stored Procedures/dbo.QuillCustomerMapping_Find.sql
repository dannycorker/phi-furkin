SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the QuillCustomerMapping table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillCustomerMapping_Find]
(

	@SearchUsingOR bit   = null ,

	@QuillCustomerMappingID int   = null ,

	@ClientID int   = null ,

	@CustomerID int   = null ,

	@QuillClientCode varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [QuillCustomerMappingID]
	, [ClientID]
	, [CustomerID]
	, [QuillClientCode]
    FROM
	[dbo].[QuillCustomerMapping] WITH (NOLOCK) 
    WHERE 
	 ([QuillCustomerMappingID] = @QuillCustomerMappingID OR @QuillCustomerMappingID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([QuillClientCode] = @QuillClientCode OR @QuillClientCode IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [QuillCustomerMappingID]
	, [ClientID]
	, [CustomerID]
	, [QuillClientCode]
    FROM
	[dbo].[QuillCustomerMapping] WITH (NOLOCK) 
    WHERE 
	 ([QuillCustomerMappingID] = @QuillCustomerMappingID AND @QuillCustomerMappingID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([QuillClientCode] = @QuillClientCode AND @QuillClientCode is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCustomerMapping_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillCustomerMapping_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCustomerMapping_Find] TO [sp_executeall]
GO
