SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 03-04-2014
-- Description:	Gets the account number using the phone number - if the phone number is not found it will
--				default to EX0035771
-- Modifed By PR 28-04-2014 - the application now handles default accounts and tokens.
-- =============================================
CREATE PROCEDURE [dbo].[SmsAccountNumber_GetAccountNumber]

	@PhoneNumber VARCHAR(50)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @AccountNumber VARCHAR(250)
	
	-- Attempt to find the account number
	SELECT * FROM SmsAccountNumber WITH (NOLOCK) 
	WHERE PhoneNumber = @PhoneNumber
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[SmsAccountNumber_GetAccountNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SmsAccountNumber_GetAccountNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SmsAccountNumber_GetAccountNumber] TO [sp_executeall]
GO
