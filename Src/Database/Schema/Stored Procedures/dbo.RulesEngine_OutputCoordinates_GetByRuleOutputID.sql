SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jan Wilson
-- Create date: 2014-05-27
-- Description:	Returns a set of OutputCoordinates for
--              a specific RuleOutputId
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_OutputCoordinates_GetByRuleOutputID]
(
	@ClientID INT,
	@RuleOutputID INT
)

AS
BEGIN
	
	SET NOCOUNT ON;
	
	SELECT
		OC.OutputCoordinateID,
		OC.RuleOutputID,
		OC.ParameterOptionID
	FROM
		dbo.RulesEngine_OutputCoordinates OC
		INNER JOIN dbo.RulesEngine_RuleOutputs RO
		ON OC.RuleOutputID = RO.RuleOutputID
		
		INNER JOIN dbo.RulesEngine_Rules R
		ON RO.RuleID = R.RuleID
		AND R.ClientID = @ClientID
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_OutputCoordinates_GetByRuleOutputID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_OutputCoordinates_GetByRuleOutputID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_OutputCoordinates_GetByRuleOutputID] TO [sp_executeall]
GO
