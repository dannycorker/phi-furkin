SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SqlQueryCriteria table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteria_Delete]
(

	@SqlQueryCriteriaID int   
)
AS


				DELETE FROM [dbo].[SqlQueryCriteria] WITH (ROWLOCK) 
				WHERE
					[SqlQueryCriteriaID] = @SqlQueryCriteriaID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteria_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteria_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteria_Delete] TO [sp_executeall]
GO
