SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ReportTableRelationships table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReportTableRelationships_Insert]
(

	@ReportTableRelationshipID int    OUTPUT,

	@TableFrom varchar (50)  ,

	@TableTo varchar (50)  ,

	@TableJoin varchar (2000)  ,

	@Description varchar (250)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[ReportTableRelationships]
					(
					[TableFrom]
					,[TableTo]
					,[TableJoin]
					,[Description]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@TableFrom
					,@TableTo
					,@TableJoin
					,@Description
					,@WhenCreated
					,@WhenModified
					)
				-- Get the identity value
				SET @ReportTableRelationshipID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTableRelationships_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReportTableRelationships_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTableRelationships_Insert] TO [sp_executeall]
GO
