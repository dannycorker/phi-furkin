SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2011-02-28
-- Description:	Data integrity checks on all major tables
-- =============================================
CREATE PROCEDURE [dbo].[HskDataIntegrity]
	@Report BIT = 1,
	@AutoFix BIT = 0,
	@FullVersion BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	SET DEADLOCK_PRIORITY 10;
	
	IF @Report = 1
	BEGIN
		/* General tables */
		SELECT t.ClientID, p.* FROM dbo.Contact t WITH (NOLOCK) INNER JOIN dbo.Customers p WITH (NOLOCK) ON p.CustomerID = t.CustomerID WHERE t.ClientID = 0 
		SELECT t.ClientID, p.* FROM dbo.CustomerOffice t WITH (NOLOCK) INNER JOIN dbo.Customers p WITH (NOLOCK) ON p.CustomerID = t.CustomerID WHERE t.ClientID = 0 
		SELECT t.ClientID, p.* FROM dbo.Department t WITH (NOLOCK) INNER JOIN dbo.Customers p WITH (NOLOCK) ON p.CustomerID = t.CustomerID WHERE t.ClientID = 0 
		SELECT t.ClientID, p.* FROM dbo.DetailValueHistory t WITH (NOLOCK) INNER JOIN dbo.Matter p WITH (NOLOCK) ON p.MatterID = t.MatterID WHERE t.ClientID = 0 
		SELECT t.LeadDocumentID, p.LeadID, p.ClientID FROM dbo.LeadDocument t WITH (NOLOCK) INNER JOIN dbo.Lead p WITH (NOLOCK) ON p.LeadID = t.LeadID WHERE t.ClientID = 0 AND p.ClientID <> 0

		SELECT t.ClientID, p.* FROM dbo.CaseDetailValues t WITH (NOLOCK) INNER JOIN dbo.DetailFields p WITH (NOLOCK) ON p.DetailFieldID = t.DetailFieldID WHERE p.ClientID <> t.ClientID 
		SELECT t.ClientID, p.* FROM dbo.DiaryAppointment t WITH (NOLOCK) INNER JOIN dbo.Lead p WITH (NOLOCK) ON p.LeadID = t.LeadID WHERE t.ClientID <> p.ClientID 
		SELECT t.ClientID, p.* FROM dbo.DiaryReminder t WITH (NOLOCK) INNER JOIN dbo.ClientPersonnel p WITH (NOLOCK) ON p.ClientPersonnelID = t.ClientPersonnelID WHERE t.ClientID <> p.ClientID 
		SELECT 'DocumentTypeFolderLink_Folder' as [DocumentTypeFolderLink], t.*, p2.ClientID, p.ClientID, p.* FROM dbo.DocumentTypeFolderLink t WITH (NOLOCK) INNER JOIN dbo.Folder p WITH (NOLOCK) ON p.FolderID = t.FolderID INNER JOIN dbo.DocumentType p2 WITH (NOLOCK) ON p2.DocumentTypeID = t.DocumentTypeID WHERE t.FolderID > 0 AND p2.ClientID <> p.ClientID 

		IF @FullVersion = 1 
		BEGIN
			SELECT t.ClientID, p.ClientID, t.*, p.* FROM dbo.LeadEvent t WITH (NOLOCK) INNER JOIN dbo.Cases p WITH (NOLOCK) ON p.CaseID = t.CaseID WHERE t.ClientID <> p.ClientID 
			SELECT t.ClientID, p.ClientID, t.*, p.* FROM dbo.LeadEvent t WITH (NOLOCK) INNER JOIN dbo.ClientPersonnel p WITH (NOLOCK) ON p.ClientPersonnelID = t.WhoDeleted WHERE t.ClientID <> p.ClientID 
			SELECT * FROM dbo.LeadEvent t WITH (NOLOCK) INNER JOIN dbo.ClientPersonnel p WITH (NOLOCK) ON p.ClientPersonnelID = t.WhoDeleted WHERE t.EventDeleted = 1 AND t.WhoDeleted <> 2795 AND p.ClientID <> t.ClientID 
			SELECT * FROM dbo.LeadEventThreadCompletion t WITH (NOLOCK) INNER JOIN dbo.Cases p WITH (NOLOCK) ON p.CaseID = t.CaseID WHERE p.ClientID <> t.ClientID

			SELECT CASE WHEN df.LeadTypeID = p.LeadTypeID THEN 'LorM' WHEN df.LeadOrMatter = p.LeadOrMatter THEN 'LeadTypeID' ELSE 'Both' END as [DiffType], p.DetailFieldPageID, p.PageName, p.Enabled, p.ResourceList, p.ClientID, p.LeadTypeID, p.LeadOrMatter, df.ClientID, df.LeadTypeID, df.LeadOrMatter, df.Enabled, df.QuestionTypeID, df.FieldName, df.LookupListID, df.DetailFieldID FROM dbo.DetailFields df WITH (NOLOCK) INNER JOIN dbo.DetailFieldPages p WITH (NOLOCK) ON df.DetailFieldPageID = p.DetailFieldPageID WHERE (df.LeadTypeID <> p.LeadTypeID OR df.LeadOrMatter <> p.LeadOrMatter) ORDER BY [DiffType], p.DetailFieldPageID, df.DetailFieldID 

			SELECT * FROM dbo.Matter t WITH (NOLOCK) INNER JOIN dbo.Cases p WITH (NOLOCK) ON p.CaseID = t.CaseID  WHERE p.ClientID <> t.ClientID
			SELECT * FROM dbo.Matter t WITH (NOLOCK) INNER JOIN dbo.Customers p WITH (NOLOCK) ON p.CustomerID = t.CustomerID  WHERE p.ClientID <> t.ClientID
			SELECT * FROM dbo.Matter t WITH (NOLOCK) INNER JOIN dbo.Lead p WITH (NOLOCK) ON p.LeadID = t.LeadID WHERE p.ClientID <> t.ClientID

			SELECT * FROM dbo.MatterDetailValues mdv WITH (NOLOCK) INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.MatterID = mdv.MatterID WHERE mdv.LeadID <> m.LeadID
			SELECT * FROM dbo.MatterDetailValues mdv WITH (NOLOCK) INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.MatterID = mdv.MatterID WHERE mdv.ClientID <> m.ClientID

			SELECT * FROM dbo.SqlQuery t WITH (NOLOCK) INNER JOIN dbo.ClientPersonnel p WITH (NOLOCK) ON p.ClientPersonnelID = t.CreatedBy WHERE p.ClientID <> t.ClientID
			SELECT * FROM dbo.SqlQuery t WITH (NOLOCK) INNER JOIN dbo.ClientPersonnel p WITH (NOLOCK) ON p.ClientPersonnelID = t.OwnedBy WHERE p.ClientID <> t.ClientID
			SELECT * FROM dbo.SqlQuery t WITH (NOLOCK) INNER JOIN dbo.ClientPersonnel p WITH (NOLOCK) ON p.ClientPersonnelID = t.ModifiedBy WHERE p.ClientID <> t.ClientID

			SELECT * FROM dbo.SqlQueryColumns t WITH (NOLOCK) INNER JOIN dbo.SqlQuery p WITH (NOLOCK) ON p.QueryID = t.SqlQueryID WHERE p.ClientID <> t.ClientID
			SELECT * FROM dbo.SqlQueryCriteria t WITH (NOLOCK) INNER JOIN dbo.SqlQuery p WITH (NOLOCK) ON p.QueryID = t.SqlQueryID WHERE p.ClientID <> t.ClientID
			SELECT * FROM dbo.SqlQueryTables t WITH (NOLOCK) INNER JOIN dbo.SqlQuery p WITH (NOLOCK) ON p.QueryID = t.SqlQueryID WHERE p.ClientID <> t.ClientID

			SELECT * FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) INNER JOIN dbo.DetailFields p WITH (NOLOCK) ON p.DetailFieldID = t.DetailFieldID WHERE (p.ClientID <> t.ClientID OR p.LeadTypeID <> t.LeadTypeID)
			SELECT * FROM dbo.ThirdPartySystemEvent t WITH (NOLOCK) INNER JOIN dbo.EventType p WITH (NOLOCK) ON p.EventTypeID = t.EventTypeID WHERE (p.ClientID <> t.ClientID OR p.LeadTypeID <> t.LeadTypeID)
			SELECT * FROM dbo.ThirdPartySystemEvent t WITH (NOLOCK) INNER JOIN dbo.LeadType p WITH (NOLOCK) ON p.LeadTypeID = t.LeadTypeID WHERE (p.ClientID <> t.ClientID OR p.LeadTypeID <> t.LeadTypeID)
		END
		
		/* Special cases around ClientQuestionnaires */
		SELECT 'DroppedOutCustomerQuestionnaires_ClQ' as [DroppedOutCustomerQuestionnaires], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.DroppedOutCustomerQuestionnaires t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID IS NULL OR t.ClientID <> p.ClientID OR p.ClientID IS NULL)
		SELECT 'DroppedOutCustomerAnswers_CuQ' as [DroppedOutCustomerAnswers], p.ClientQuestionnaireID, p.CustomerQuestionnaireID, p.ClientID, t.* FROM dbo.DroppedOutCustomerAnswers t LEFT JOIN CustomerQuestionnaires p ON t.CustomerQuestionnaireID = p.CustomerQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID IS NULL OR t.ClientID <> p.ClientID OR p.ClientID IS NULL)
		SELECT 'DroppedOutCustomerAnswers_ClQ' as [DroppedOutCustomerAnswers], p.ClientQuestionnaireID, p.CustomerQuestionnaireID, p.ClientID, t.* FROM dbo.DroppedOutCustomerAnswers t INNER JOIN dbo.DroppedOutCustomerQuestionnaires p ON p.CustomerQuestionnaireID = p.CustomerQuestionnaireID WHERE NOT EXISTS (SELECT * FROM dbo.ClientQuestionnaires cq WITH (NOLOCK) WHERE cq.ClientQuestionnaireID = p.ClientQuestionnaireID)
		SELECT 'QuestionPossibleAnswers_MQ' as [QuestionPossibleAnswers], p.ClientQuestionnaireID, p.MasterQuestionID, p.ClientID, t.* FROM dbo.QuestionPossibleAnswers t WITH (NOLOCK) LEFT JOIN dbo.MasterQuestions p WITH (NOLOCK) ON p.MasterQuestionID = t.MasterQuestionID WHERE (t.ClientID = 0 OR t.ClientID IS NULL OR t.ClientID <> p.ClientID OR p.MasterQuestionID IS NULL OR t.MasterQuestionID = 0)
		SELECT 'QuestionPossibleAnswers_ClQ' as [QuestionPossibleAnswers], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.QuestionPossibleAnswers t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID IS NULL OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL OR t.ClientQuestionnaireID = 0)
		
		/* ClientQuestionnaires */
		SELECT 'ClientAreaMailingLists' as [ClientAreaMailingLists], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.ClientAreaMailingLists t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'ClientCustomerMails' as [ClientCustomerMails], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.ClientCustomerMails t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'ClientQuestionnaireLayout' as [ClientQuestionnaireLayout], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.ClientQuestionnaireLayout t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'ClientQuestionnairesVisited' as [ClientQuestionnairesVisited], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.ClientQuestionnairesVisited t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'ClientQuestionnairesVisitedAndCompleted' as [ClientQuestionnairesVisitedAndCompleted], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.ClientQuestionnairesVisitedAndCompleted t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'ClientQuestionnairesVisitedButNotComplete' as [ClientQuestionnairesVisitedButNotComplete], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.ClientQuestionnairesVisitedButNotComplete t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'CustomerInformationFieldDefinitions' as [CustomerInformationFieldDefinitions], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.CustomerInformationFieldDefinitions t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'CustomerOutcomes' as [CustomerOutcomes], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.CustomerOutcomes t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'CustomerQuestionnaires' as [CustomerQuestionnaires], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.CustomerQuestionnaires t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'DroppedOutCustomerMessages' as [DroppedOutCustomerMessages], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.DroppedOutCustomerMessages t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'Equations' as [Equations], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.Equations t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'MasterQuestions' as [MasterQuestions], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.MasterQuestions t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'OutcomeDelayedEmails' as [OutcomeDelayedEmails], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.OutcomeDelayedEmails t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'Outcomes' as [Outcomes], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.Outcomes t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'PortalUserGroupQuestionnaireAccess' as [PortalUserGroupQuestionnaireAccess], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.PortalUserGroupQuestionnaireAccess t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'QuestionnaireColours' as [QuestionnaireColours], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.QuestionnaireColours t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'QuestionnaireFonts' as [QuestionnaireFonts], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.QuestionnaireFonts t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'QuestionnaireFrameSources' as [QuestionnaireFrameSources], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.QuestionnaireFrameSources t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'QuestionPossibleAnswers' as [QuestionPossibleAnswers], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.QuestionPossibleAnswers t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'Results' as [Results], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.Results t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'TextToSpeak' as [TextToSpeak], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.TextToSpeak t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)
		SELECT 'Tracking' as [Tracking], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.Tracking t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)		

		/*
			Generated from this SQL:
			
			SELECT 'SELECT ''' + ist.TABLE_NAME + ''' as [' + ist.TABLE_NAME + '], p.ClientQuestionnaireID, p.QuestionnaireTitle, p.ClientID, t.* FROM dbo.' + ist.TABLE_NAME + ' t WITH (NOLOCK) LEFT JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID <> p.ClientID OR p.ClientQuestionnaireID IS NULL)' 
			FROM INFORMATION_SCHEMA.COLUMNS ist 
			WHERE ist.COLUMN_NAME = 'ClientQuestionnaireID' 
			AND ist.TABLE_NAME NOT IN ('ClientQuestionnaires', 'PhoneNumberVerifications') 
			AND ist.TABLE_NAME NOT LIKE 'v%'
			ORDER BY ist.TABLE_NAME 
			
		*/
		
	END
	
	
	IF @AutoFix = 1
	BEGIN
		
		/* General tables */
		UPDATE dbo.Contact SET ClientID = p.ClientID FROM dbo.Contact t INNER JOIN dbo.Customers p ON p.CustomerID = t.CustomerID WHERE t.ClientID <> p.ClientID 
		UPDATE dbo.CustomerOffice SET ClientID = p.ClientID FROM dbo.CustomerOffice t INNER JOIN dbo.Customers p ON p.CustomerID = t.CustomerID WHERE t.ClientID <> p.ClientID 
		UPDATE dbo.Department SET ClientID = p.ClientID FROM dbo.Department t INNER JOIN dbo.Customers p ON p.CustomerID = t.CustomerID WHERE t.ClientID <> p.ClientID 
		UPDATE dbo.DetailValueHistory SET ClientID = p.ClientID FROM dbo.DetailValueHistory t INNER JOIN dbo.Matter p ON p.MatterID = t.MatterID WHERE t.ClientID = 0 AND t.ClientID <> p.ClientID 
		UPDATE dbo.LeadDocument SET ClientID = p.ClientID FROM dbo.LeadDocument t INNER JOIN dbo.Lead p ON p.LeadID = t.LeadID WHERE t.ClientID = 0 AND t.ClientID <> p.ClientID 

		DELETE dbo.CaseDetailValues FROM dbo.CaseDetailValues t WITH (NOLOCK) INNER JOIN dbo.DetailFields p WITH (NOLOCK) ON p.DetailFieldID = t.DetailFieldID WHERE p.ClientID <> t.ClientID 
		UPDATE dbo.DetailFields SET LeadTypeID = p.LeadTypeID, LeadOrMatter = p.LeadOrMatter FROM dbo.DetailFields df WITH (NOLOCK) INNER JOIN dbo.DetailFieldPages p WITH (NOLOCK) ON df.DetailFieldPageID = p.DetailFieldPageID WHERE (df.LeadTypeID <> p.LeadTypeID OR df.LeadOrMatter <> p.LeadOrMatter) AND df.ClientID <> 20 AND p.ClientID <> 20 -- Deletion Helper JWG 2011-12-08
		UPDATE dbo.DiaryAppointment SET ClientID = p.ClientID FROM dbo.DiaryAppointment t WITH (NOLOCK) INNER JOIN dbo.Lead p WITH (NOLOCK) ON p.LeadID = t.LeadID WHERE p.ClientID <> t.ClientID
		UPDATE dbo.DiaryReminder SET ClientID = p.ClientID FROM dbo.DiaryReminder t WITH (NOLOCK) INNER JOIN dbo.ClientPersonnel p WITH (NOLOCK) ON p.ClientPersonnelID = t.ClientPersonnelID WHERE p.ClientID <> t.ClientID
		UPDATE dbo.DocumentTypeFolderLink SET FolderID = -1 FROM dbo.DocumentTypeFolderLink t WITH (NOLOCK) INNER JOIN dbo.Folder p WITH (NOLOCK) ON p.FolderID = t.FolderID INNER JOIN dbo.DocumentType p2 WITH (NOLOCK) ON p2.DocumentTypeID = t.DocumentTypeID WHERE t.FolderID > 0 AND p2.ClientID <> p.ClientID 

		/* 
			Can't delete LeadEvents automatically without looking at all these tables:
			LeadEventThreadCompletion.FromLeadEventID
			LeadEventThreadCompletion.ToLeadEventID
			LeadEvent.NextEvent
			LeadEvent
			LeadDocument
			LeadDocumentFS
			
			Just report them for manual correction for now.
		*/
		/*SELECT t.ClientID, p.ClientID, t.*, p.* FROM dbo.LeadEvent t WITH (NOLOCK) INNER JOIN dbo.Cases p WITH (NOLOCK) ON p.CaseID = t.CaseID WHERE t.ClientID <> p.ClientID 
		SELECT t.ClientID, p.ClientID, t.*, p.* FROM dbo.LeadEvent t WITH (NOLOCK) INNER JOIN dbo.ClientPersonnel p WITH (NOLOCK) ON p.ClientPersonnelID = t.WhoDeleted WHERE t.ClientID <> p.ClientID 
		UPDATE dbo.LeadEvent SET WhoDeleted = 2795 FROM dbo.LeadEvent t WITH (NOLOCK) INNER JOIN dbo.ClientPersonnel p WITH (NOLOCK) ON p.ClientPersonnelID = t.WhoDeleted WHERE t.EventDeleted = 1 AND t.WhoDeleted <> 2795 AND p.ClientID <> t.ClientID 
		DELETE dbo.LeadEventThreadCompletion FROM dbo.LeadEventThreadCompletion t WITH (NOLOCK) INNER JOIN dbo.Cases p WITH (NOLOCK) ON p.CaseID = t.CaseID WHERE p.ClientID <> t.ClientID
		UPDATE dbo.Matter SET ClientID = p.ClientID FROM dbo.Matter t WITH (NOLOCK) INNER JOIN dbo.Cases p WITH (NOLOCK) ON p.CaseID = t.CaseID WHERE t.ClientID <> p.ClientID
		UPDATE dbo.Matter SET ClientID = p.ClientID FROM dbo.Matter t WITH (NOLOCK) INNER JOIN dbo.Lead p WITH (NOLOCK) ON p.LeadID = t.LeadID WHERE t.ClientID <> p.ClientID
		UPDATE dbo.Matter SET ClientID = p.ClientID FROM dbo.Matter t WITH (NOLOCK) INNER JOIN dbo.Customers p WITH (NOLOCK) ON p.CustomerID = t.CustomerID WHERE t.ClientID <> p.ClientID
		*/

		IF @FullVersion = 1 
		BEGIN
			;WITH InnerSql AS 
			(
				SELECT cp.*, ROW_NUMBER() OVER(PARTITION BY cp.ClientID ORDER BY cp.ClientPersonnelID ) as rn 
				FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
				WHERE cp.FirstName = 'Aquarium' AND cp.LastName = 'Automation' 
			)
			UPDATE dbo.SqlQuery 
			SET CreatedBy = i.ClientPersonnelID 
			FROM dbo.SqlQuery t WITH (NOLOCK) 
			INNER JOIN dbo.ClientPersonnel p WITH (NOLOCK) ON p.ClientPersonnelID = t.CreatedBy 
			INNER JOIN InnerSql i ON i.ClientID = t.ClientID 
			WHERE p.ClientID <> t.ClientID 
			AND i.rn = 1 


			;WITH InnerSql AS 
			(
				SELECT cp.*, ROW_NUMBER() OVER(PARTITION BY cp.ClientID ORDER BY cp.ClientPersonnelID ) as rn 
				FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
				WHERE cp.FirstName = 'Aquarium' AND cp.LastName = 'Automation' 
			)
			UPDATE dbo.SqlQuery 
			SET CreatedBy = i.ClientPersonnelID 
			FROM dbo.SqlQuery t WITH (NOLOCK) 
			INNER JOIN dbo.ClientPersonnel p WITH (NOLOCK) ON p.ClientPersonnelID = t.OwnedBy 
			INNER JOIN InnerSql i ON i.ClientID = t.ClientID 
			WHERE p.ClientID <> t.ClientID 
			AND i.rn = 1 

			;WITH InnerSql AS 
			(
				SELECT cp.*, ROW_NUMBER() OVER(PARTITION BY cp.ClientID ORDER BY cp.ClientPersonnelID ) as rn 
				FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
				WHERE cp.FirstName = 'Aquarium' AND cp.LastName = 'Automation' 
			)
			UPDATE dbo.SqlQuery 
			SET CreatedBy = i.ClientPersonnelID 
			FROM dbo.SqlQuery t WITH (NOLOCK) 
			INNER JOIN dbo.ClientPersonnel p WITH (NOLOCK) ON p.ClientPersonnelID = t.ModifiedBy 
			INNER JOIN InnerSql i ON i.ClientID = t.ClientID 
			WHERE p.ClientID <> t.ClientID 
			AND i.rn = 1 

			UPDATE dbo.MatterDetailValues SET LeadID = m.LeadID, ClientID = m.ClientID FROM dbo.MatterDetailValues mdv WITH (NOLOCK) INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.MatterID = mdv.MatterID WHERE mdv.LeadID <> m.LeadID 
			UPDATE dbo.MatterDetailValues SET ClientID = m.ClientID FROM dbo.MatterDetailValues mdv WITH (NOLOCK) INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.MatterID = mdv.MatterID WHERE mdv.ClientID <> m.ClientID 

			UPDATE dbo.SqlQueryColumns SET ClientID = p.ClientID FROM dbo.SqlQueryColumns t WITH (NOLOCK) INNER JOIN dbo.SqlQuery p WITH (NOLOCK) ON p.QueryID = t.SqlQueryID WHERE p.ClientID <> t.ClientID 
			UPDATE dbo.SqlQueryCriteria SET ClientID = p.ClientID FROM dbo.SqlQueryCriteria t WITH (NOLOCK) INNER JOIN dbo.SqlQuery p WITH (NOLOCK) ON p.QueryID = t.SqlQueryID WHERE p.ClientID <> t.ClientID 
			UPDATE dbo.SqlQueryTables SET ClientID = p.ClientID FROM dbo.SqlQueryTables t WITH (NOLOCK) INNER JOIN dbo.SqlQuery p WITH (NOLOCK) ON p.QueryID = t.SqlQueryID WHERE p.ClientID <> t.ClientID 

			UPDATE dbo.ThirdPartyFieldMapping SET ClientID = p.ClientID, LeadTypeID = p.LeadTypeID FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) INNER JOIN dbo.DetailFields p WITH (NOLOCK) ON p.DetailFieldID = t.DetailFieldID WHERE (p.ClientID <> t.ClientID OR p.LeadTypeID <> t.LeadTypeID) 
			UPDATE dbo.ThirdPartySystemEvent SET ClientID = p.ClientID, LeadTypeID = p.LeadTypeID FROM dbo.ThirdPartySystemEvent t WITH (NOLOCK) INNER JOIN dbo.EventType p WITH (NOLOCK) ON p.EventTypeID = t.EventTypeID WHERE (p.ClientID <> t.ClientID OR p.LeadTypeID <> t.LeadTypeID) 
			UPDATE dbo.ThirdPartySystemEvent SET ClientID = p.ClientID, LeadTypeID = p.LeadTypeID FROM dbo.ThirdPartySystemEvent t WITH (NOLOCK) INNER JOIN dbo.EventType p WITH (NOLOCK) ON p.LeadTypeID = t.LeadTypeID WHERE (p.ClientID <> t.ClientID OR p.LeadTypeID <> t.LeadTypeID) 
		END

		/* Mismatched CustomerQuestionnaireID and ClientQuestionnaires */
		UPDATE dbo.DroppedOutCustomerQuestionnaires SET ClientID = p.ClientID FROM dbo.DroppedOutCustomerQuestionnaires t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.DroppedOutCustomerAnswers SET ClientID = p.ClientID FROM dbo.DroppedOutCustomerAnswers t INNER JOIN dbo.CustomerQuestionnaires p ON t.CustomerQuestionnaireID = p.CustomerQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		
		/* Mismatched QuestionPossibleAnswers and ClientQuestionnaires. Update ClientID here as well, but MasterQuestions is not 100% reliable so check both tables below as well */
		UPDATE dbo.QuestionPossibleAnswers SET ClientQuestionnaireID = p.ClientQuestionnaireID, ClientID = p.ClientID FROM dbo.QuestionPossibleAnswers t WITH (NOLOCK) INNER JOIN dbo.MasterQuestions p WITH (NOLOCK) ON p.MasterQuestionID = t.MasterQuestionID WHERE (t.ClientID = 0 OR t.ClientID IS NULL OR t.ClientID <> p.ClientID OR t.ClientQuestionnaireID IS NULL OR t.ClientQuestionnaireID = 0 OR t.ClientQuestionnaireID <> p.ClientQuestionnaireID)
		UPDATE dbo.QuestionPossibleAnswers SET ClientID = p.ClientID FROM dbo.QuestionPossibleAnswers t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		
		/* ClientID from ClientQuestionnaires */
		UPDATE dbo.ClientAreaMailingLists SET ClientID = p.ClientID FROM dbo.ClientAreaMailingLists t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.ClientCustomerMails SET ClientID = p.ClientID FROM dbo.ClientCustomerMails t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.ClientQuestionnaireLayout SET ClientID = p.ClientID FROM dbo.ClientQuestionnaireLayout t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.ClientQuestionnairesVisited SET ClientID = p.ClientID FROM dbo.ClientQuestionnairesVisited t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.ClientQuestionnairesVisitedAndCompleted SET ClientID = p.ClientID FROM dbo.ClientQuestionnairesVisitedAndCompleted t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.ClientQuestionnairesVisitedButNotComplete SET ClientID = p.ClientID FROM dbo.ClientQuestionnairesVisitedButNotComplete t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.CustomerInformationFieldDefinitions SET ClientID = p.ClientID FROM dbo.CustomerInformationFieldDefinitions t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.CustomerOutcomes SET ClientID = p.ClientID FROM dbo.CustomerOutcomes t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.CustomerQuestionnaires SET ClientID = p.ClientID FROM dbo.CustomerQuestionnaires t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.DroppedOutCustomerMessages SET ClientID = p.ClientID FROM dbo.DroppedOutCustomerMessages t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.Equations SET ClientID = p.ClientID FROM dbo.Equations t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.MasterQuestions SET ClientID = p.ClientID FROM dbo.MasterQuestions t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.OutcomeDelayedEmails SET ClientID = p.ClientID FROM dbo.OutcomeDelayedEmails t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID = 0 OR t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.Outcomes SET ClientID = p.ClientID FROM dbo.Outcomes t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.PortalUserGroupQuestionnaireAccess SET ClientID = p.ClientID FROM dbo.PortalUserGroupQuestionnaireAccess t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.QuestionnaireColours SET ClientID = p.ClientID FROM dbo.QuestionnaireColours t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.QuestionnaireFonts SET ClientID = p.ClientID FROM dbo.QuestionnaireFonts t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.QuestionnaireFrameSources SET ClientID = p.ClientID FROM dbo.QuestionnaireFrameSources t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.Results SET ClientID = p.ClientID FROM dbo.Results t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.TextToSpeak SET ClientID = p.ClientID FROM dbo.TextToSpeak t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		UPDATE dbo.Tracking SET ClientID = p.ClientID FROM dbo.Tracking t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)
		
		/* ClientID from other sources */
		UPDATE dbo.ClientOfficeIPAddresses SET ClientID = p.ClientID FROM dbo.ClientOfficeIPAddresses t INNER JOIN ClientOffices p WITH (NOLOCK) ON t.ClientOfficeID = p.ClientOfficeID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID) 
		UPDATE dbo.ClientPersonnelOptions SET ClientID = p.ClientID FROM dbo.ClientPersonnelOptions t INNER JOIN ClientPersonnel p WITH (NOLOCK) ON t.ClientPersonnelID = p.ClientPersonnelID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID) 
		UPDATE dbo.CustomerAnswers SET ClientID = p.ClientID FROM dbo.CustomerAnswers t INNER JOIN CustomerQuestionnaires p WITH (NOLOCK) ON t.CustomerQuestionnaireID = p.CustomerQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID) 
		UPDATE dbo.LinkedDetailFields SET ClientID = p.ClientID FROM dbo.LinkedDetailFields t INNER JOIN LeadType p WITH (NOLOCK) ON t.LeadTypeIDFrom = p.LeadTypeID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID) 
		UPDATE dbo.[Messages] SET ClientID = p.ClientID FROM dbo.[Messages] t INNER JOIN ClientPersonnel p WITH (NOLOCK) ON t.ClientPersonnelIDFrom = p.ClientPersonnelID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID) 
		UPDATE dbo.OutcomeCriterias SET ClientID = p.ClientID FROM dbo.OutcomeCriterias t INNER JOIN Outcomes p WITH (NOLOCK) ON t.OutcomeID = p.OutcomeID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID) 
		UPDATE dbo.OutcomeEquations SET ClientID = p.ClientID FROM dbo.OutcomeEquations t INNER JOIN Outcomes p WITH (NOLOCK) ON t.OutcomeID = p.OutcomeID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID) 
		
		/* Nulls */
		UPDATE dbo.ClientPersonnelAdminGroups SET ClientID = 0 WHERE ClientID IS NULL
		
		/* Delete disposable data from invalid sources */
		DELETE dbo.ClientQuestionnairesVisited WHERE NOT EXISTS (SELECT * FROM dbo.ClientQuestionnaires cq WITH (NOLOCK) WHERE cq.ClientQuestionnaireID = ClientQuestionnairesVisited.ClientQuestionnaireID)
		DELETE dbo.CustomerInformationFieldDefinitions WHERE NOT EXISTS (SELECT * FROM dbo.ClientQuestionnaires cq WITH (NOLOCK) WHERE cq.ClientQuestionnaireID = CustomerInformationFieldDefinitions.ClientQuestionnaireID)
		DELETE dbo.DroppedOutCustomerAnswers WHERE NOT EXISTS (SELECT * FROM dbo.CustomerQuestionnaires cq WITH (NOLOCK) WHERE cq.CustomerQuestionnaireID = DroppedOutCustomerAnswers.CustomerQuestionnaireID)
		DELETE dbo.DroppedOutCustomerAnswers FROM dbo.DroppedOutCustomerAnswers doca INNER JOIN dbo.DroppedOutCustomerQuestionnaires docq ON docq.CustomerQuestionnaireID = doca.CustomerQuestionnaireID WHERE NOT EXISTS (SELECT * FROM dbo.ClientQuestionnaires cq WITH (NOLOCK) WHERE cq.ClientQuestionnaireID = docq.ClientQuestionnaireID)
		DELETE dbo.DroppedOutCustomerQuestionnaires WHERE NOT EXISTS (SELECT * FROM dbo.ClientQuestionnaires cq WITH (NOLOCK) WHERE cq.ClientQuestionnaireID = DroppedOutCustomerQuestionnaires.ClientQuestionnaireID)
		DELETE dbo.MasterQuestions WHERE NOT EXISTS (SELECT * FROM dbo.ClientQuestionnaires cq WITH (NOLOCK) WHERE cq.ClientQuestionnaireID = MasterQuestions.ClientQuestionnaireID)
		DELETE dbo.OutcomeEquations WHERE NOT EXISTS (SELECT * FROM dbo.Outcomes o WITH (NOLOCK) WHERE o.OutcomeID = OutcomeEquations.OutcomeID)
		DELETE dbo.QuestionPossibleAnswers WHERE NOT EXISTS (SELECT * FROM dbo.ClientQuestionnaires cq WITH (NOLOCK) WHERE cq.clientquestionnaireid = QuestionPossibleAnswers.ClientQuestionnaireID)
		DELETE dbo.QuestionPossibleAnswers WHERE NOT EXISTS (SELECT * FROM dbo.MasterQuestions cq WITH (NOLOCK) WHERE cq.MasterQuestionID = QuestionPossibleAnswers.MasterQuestionID)
		DELETE dbo.OutcomeDelayedEmails WHERE NOT EXISTS (SELECT * FROM dbo.ClientQuestionnaires o WITH (NOLOCK) WHERE o.ClientQuestionnaireID = OutcomeDelayedEmails.ClientQuestionnaireID)

		/* Check the app where these are not being set correctly 
		Master questions with NULL CQID (only 3 existed, deleted in Feb 2011)
		QuestionPossibleAnswers with 0 ClientQuestionnaireID
		
		ClientQuestionnaireLayout = 33
		ClientQuestionnairesVisited = 11
		ClientQuestionnairesVisitedAndCompleted = 9
		CustomerInformationFieldDefinitions = 204 (Redress 150 to 197) 
		CustomerOutcomes = 3 (Client 4 to 51) 
		CustomerQuestionnaires = 10 (Some Client 0 instead of 156 etc) 
		DroppedOutCustomers = 119 (client3/4 from 2006/7) 
		DroppedOutCustomerMessages = 1 (Client 4 to 3) 
		Equations = 74 (mainly Client 0) 
		OutcomeDelayedEmails = 186000! (Client 0) 
		Outcomes = 68
		PortalUserGroupQuestionnaireAccess = 2 (Client 0) 
		QuestionnaireColours = 49 (Client 0) 
		QuestionnaireFonts = 392 (Client 0 and 4/51) 
		*/
		
		/*
			Generated from this SQL:
			
			SELECT 'UPDATE dbo.' + ist.TABLE_NAME + ' SET ClientID = p.ClientID FROM dbo.' + ist.TABLE_NAME + ' t INNER JOIN dbo.ClientQuestionnaires p WITH (NOLOCK) ON p.ClientQuestionnaireID = t.ClientQuestionnaireID WHERE (t.ClientID IS NULL OR t.ClientID <> p.ClientID)' 
			FROM INFORMATION_SCHEMA.COLUMNS ist 
			WHERE ist.COLUMN_NAME = 'ClientQuestionnaireID' 
			AND ist.TABLE_NAME NOT IN ('ClientQuestionnaires', 'PhoneNumberVerifications') 
			AND ist.TABLE_NAME NOT LIKE 'v%'
			ORDER BY ist.TABLE_NAME 
			
		*/
		
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[HskDataIntegrity] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HskDataIntegrity] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HskDataIntegrity] TO [sp_executeall]
GO
