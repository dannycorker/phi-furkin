SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the StyleRule table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[StyleRule_Update]
(

	@StyleRuleID int   ,

	@RuleClass varchar (100)  ,

	@RuleDescription varchar (255)  ,

	@RuleValue varchar (100)  ,

	@AquariumOnly bit   ,

	@IncludeLinkText bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[StyleRule]
				SET
					[RuleClass] = @RuleClass
					,[RuleDescription] = @RuleDescription
					,[RuleValue] = @RuleValue
					,[AquariumOnly] = @AquariumOnly
					,[IncludeLinkText] = @IncludeLinkText
				WHERE
[StyleRuleID] = @StyleRuleID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[StyleRule_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[StyleRule_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[StyleRule_Update] TO [sp_executeall]
GO
