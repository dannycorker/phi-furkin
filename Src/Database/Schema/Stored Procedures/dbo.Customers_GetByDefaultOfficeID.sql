SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Customers table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Customers_GetByDefaultOfficeID]
(

	@DefaultOfficeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[CustomerID],
					[ClientID],
					[TitleID],
					[IsBusiness],
					[FirstName],
					[MiddleName],
					[LastName],
					[EmailAddress],
					[DayTimeTelephoneNumber],
					[DayTimeTelephoneNumberVerifiedAndValid],
					[HomeTelephone],
					[HomeTelephoneVerifiedAndValid],
					[MobileTelephone],
					[MobileTelephoneVerifiedAndValid],
					[CompanyTelephone],
					[CompanyTelephoneVerifiedAndValid],
					[WorksTelephone],
					[WorksTelephoneVerifiedAndValid],
					[Address1],
					[Address2],
					[Town],
					[County],
					[PostCode],
					[Website],
					[HasDownloaded],
					[DownloadedOn],
					[AquariumStatusID],
					[ClientStatusID],
					[Test],
					[CompanyName],
					[Occupation],
					[Employer],
					[Fullname],
					[PhoneNumbersVerifiedOn],
					[DoNotEmail],
					[DoNotSellToThirdParty],
					[AgreedToTermsAndConditions],
					[DateOfBirth],
					[DefaultContactID],
					[DefaultOfficeID],
					[AddressVerified],
					[CountryID],
					[SubClientID],
					[CustomerRef],
					[WhoChanged],
					[WhenChanged],
					[ChangeSource],
					[EmailAddressVerifiedAndValid],
					[EmailAddressVerifiedOn],
					[Comments],
					[AllowSmsCommandProcessing],
					[LanguageID],
					[Longitude],
					[Latitude]
				FROM
					[dbo].[Customers] WITH (NOLOCK) 
				WHERE
					[DefaultOfficeID] = @DefaultOfficeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers_GetByDefaultOfficeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers_GetByDefaultOfficeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers_GetByDefaultOfficeID] TO [sp_executeall]
GO
