SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Bill table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Bill_Insert]
(

	@BillID int    OUTPUT,

	@ClientID int   ,

	@Reference varchar (100)  ,

	@Description varchar (2000)  ,

	@CreatedDate datetime   ,

	@DiaryAppointmentID int   ,

	@CustomerID int   ,

	@LeadID int   ,

	@CaseID int   ,

	@BillStatusID int   
)
AS


				
				INSERT INTO [dbo].[Bill]
					(
					[ClientID]
					,[Reference]
					,[Description]
					,[CreatedDate]
					,[DiaryAppointmentID]
					,[CustomerID]
					,[LeadID]
					,[CaseID]
					,[BillStatusID]
					)
				VALUES
					(
					@ClientID
					,@Reference
					,@Description
					,@CreatedDate
					,@DiaryAppointmentID
					,@CustomerID
					,@LeadID
					,@CaseID
					,@BillStatusID
					)
				-- Get the identity value
				SET @BillID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Bill_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Bill_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Bill_Insert] TO [sp_executeall]
GO
