SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-09-10
-- Description:	Case Transfer By SAE
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CaseExampleReport]
@FolderID int, 
@WhoCreated int,
@NumberOfLDVs int = 0,
@NumberOfMDVs int = 0,
@NumberOfLeadResources int = 0,
@NumberOfMatterResources int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @ClientID int,
			@SqlQueryID int,
			@CustomersTableID int,
			@LeadTableID int,
			@CasesTableID int,
			@TestColID int,
			@TestID int,
			@LeadDetailTableID int,
			@MatterTableID int,
			@TableDisplayOrder int,
			@dvalias varchar(10)

		Select @ClientID = ClientID 
		From Folder
		Where FolderID = @FolderID

	/* Create Base Report */
	Insert into SqlQuery (ClientID,QueryText,QueryTitle,AutorunOnline,OnlineLimit,BatchLimit,SqlQueryTypeID,FolderID,IsEditable,IsTemplate,IsDeleted,WhenCreated,CreatedBy,OwnedBy,RunCount,LastRundate,LastRuntime,LastRowcount,MaxRuntime,MaxRowcount,AvgRuntime,AvgRowcount,Comments,WhenModified,ModifiedBy,LeadTypeID,ParentQueryID,IsParent)
	Select @ClientID,'Select ''Please add filters where required'' as [Header]','Example Report with ' + convert(varchar,@NumberOfLDVs) + ' Lead Detail Values and ' + convert(varchar,@NumberOfMDVs)+ ' Matter Detail Values',0,0,0,3,@FolderID,1,0,0,dbo.fn_GetDate_Local(),@WhoCreated,@WhoCreated,0,NULL,0,0,0,0,0,0,'',dbo.fn_GetDate_Local(),@WhoCreated,NULL,NULL,0

	Select @SqlQueryID = SCOPE_IDENTITY()

	Insert Into SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
	Select @ClientID,@SqlQueryID,'Customers','',0,'','',NULL,NULL,NULL,NULL

	Select @CustomersTableID = SCOPE_IDENTITY()

	Insert Into SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
	Select @ClientID,@SqlQueryID,'Lead','',1,'INNER JOIN','Lead.CustomerID = Customers.CustomerID',NULL,30,NULL,NULL

	Select @LeadTableID = SCOPE_IDENTITY()

	Insert Into SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
	Select @ClientID,@SqlQueryID,'Cases','',2,'INNER JOIN','Cases.LeadID = Lead.LeadID',NULL,41,NULL,NULL

	Select @CasesTableID = SCOPE_IDENTITY()

	Insert Into SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	Select @ClientID,@SqlQueryID,@CustomersTableID,'Customers.CustomerID','',1,2,NULL,NULL,NULL,NULL,0,'Number','Number','CustomerID'

	Insert Into SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	Select @ClientID,@SqlQueryID,@LeadTableID,'Lead.LeadID','',1,3,NULL,NULL,NULL,NULL,0,'Number','Number','LeadID'

	Insert Into SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	Select @ClientID,@SqlQueryID,@CasesTableID,'Cases.CaseID','',1,1,NULL,NULL,NULL,NULL,0,'Number','Number','CaseID'

	Insert Into SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	Select @ClientID,@SqlQueryID,@CustomersTableID,'Customers.Test',NULL,1,7,NULL,NULL,NULL,NULL,0,'Number','Number','Test'

	Select @TestColID = SCOPE_IDENTITY()

	Insert Into SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
	Select @ClientID,@SqlQueryID,@CustomersTableID,NULL,'Customers.ClientID = ' + convert(varchar,@ClientID),'','','Customers Security',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL

	Insert Into SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
	Select @ClientID,@SqlQueryID,@LeadTableID,NULL,'Lead.ClientID = ' + convert(varchar,@ClientID),'','','Lead Security',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL

	Insert Into SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
	Select @ClientID,@SqlQueryID,@CasesTableID,NULL,'Cases.ClientID = ' + convert(varchar,@ClientID),'','','Cases Security',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL

	Insert Into SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
	Select @ClientID,@SqlQueryID,@CustomersTableID,@TestColID,'(Customers.Test IS NULL  OR Customers.Test = 0)',0,'','',NULL,NULL,NULL,NULL,NULL,NULL,0,'',0,NULL

	Select @TestID = 0, @TableDisplayOrder = 2
	While @NumberOfLDVs > @TestID
	Begin
	
		Select @TestID = @TestID + 1, @TableDisplayOrder = @TableDisplayOrder + 1

		Select @dvalias = case @TestID-1 when 0 then '' else convert(varchar,@TestID-1) end

		Insert Into SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
		Select @ClientID,@SqlQueryID,'LeadDetailValues','LeadDetailValues' + CONVERT(varchar,@dvalias),@TableDisplayOrder,'INNER JOIN','LeadDetailValues' + CONVERT(varchar,@dvalias) + '.LeadID = Lead.LeadID',NULL,55,NULL,NULL

		Select @LeadDetailTableID = SCOPE_IDENTITY()

		Insert Into SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
		Select @ClientID,@SqlQueryID,@LeadDetailTableID,NULL,'LeadDetailValues'  + CONVERT(varchar,@dvalias) + '.ClientID = ' + convert(varchar,@ClientID),'','','LeadDetailValues'  + CONVERT(varchar,@dvalias) + ' Security',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL

		Insert Into SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
		Select @ClientID,@SqlQueryID,@LeadDetailTableID,'LeadDetailValues' + CONVERT(varchar,@dvalias) + '.DetailFieldID',NULL,1,10+@TestID,NULL,NULL,NULL,NULL,0,'Number','Number','DetailFieldID'

		Insert Into SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
		Select @ClientID,@SqlQueryID,@LeadDetailTableID,'LeadDetailValues' + CONVERT(varchar,@dvalias) + '.DetailValue',NULL,1,10+@TestID,NULL,NULL,NULL,NULL,0,'Text','Text','DetailValue'

	End

	Select @TestID = 0
	While @NumberOfMDVs > @TestID
	Begin
	
		Select @TestID = @TestID + 1, @TableDisplayOrder = @TableDisplayOrder + 1

		Select @dvalias = case @TestID-1 when '0' then '' else convert(varchar,@TestID-1) end

		If @TestID = 1
		Begin
		
			Insert Into SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
			Select @ClientID,@SqlQueryID,'Matter','',@TableDisplayOrder,'INNER JOIN','Matter.CaseID = Cases.CaseID',NULL,50,NULL,NULL

			Select @MatterTableID = SCOPE_IDENTITY()

			Insert Into SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
			Select @ClientID,@SqlQueryID,@MatterTableID,'Matter.MatterID',NULL,1,10+@TestID,NULL,NULL,NULL,NULL,0,'Number','Number','MatterID'

			Insert Into SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
			Select @ClientID,@SqlQueryID,@MatterTableID,NULL,'Matter.ClientID = ' + convert(varchar,@ClientID),'','','Matter Security',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL

			Select @TableDisplayOrder = @TableDisplayOrder + 1

		End

		Insert Into SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
		Select @ClientID,@SqlQueryID,'MatterDetailValues','MatterDetailValues' + CONVERT(varchar,@dvalias),@TableDisplayOrder,'INNER JOIN','MatterDetailValues' + CONVERT(varchar,@dvalias) + '.MatterID = Matter.MatterID',NULL,58,NULL,NULL

		Select @MatterTableID = SCOPE_IDENTITY()

		Insert Into SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
		Select @ClientID,@SqlQueryID,@MatterTableID,NULL,'MatterDetailValues'  + CONVERT(varchar,@dvalias) + '.ClientID = ' + convert(varchar,@ClientID),'','','MatterDetailValues'  + CONVERT(varchar,@dvalias) + ' Security',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL

		Insert Into SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
		Select @ClientID,@SqlQueryID,@MatterTableID,'MatterDetailValues' + CONVERT(varchar,@dvalias) + '.DetailFieldID',NULL,1,12+@TestID+@NumberOfMDVs,NULL,NULL,NULL,NULL,0,'Number','Number','DetailFieldID'

		Insert Into SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
		Select @ClientID,@SqlQueryID,@MatterTableID,'MatterDetailValues' + CONVERT(varchar,@dvalias) + '.DetailValue',NULL,1,12+@TestID+@NumberOfMDVs,NULL,NULL,NULL,NULL,0,'Text','Text','DetailValue'

	End

	Select @SqlQueryID as [QueryID]

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CaseExampleReport] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CaseExampleReport] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CaseExampleReport] TO [sp_executeall]
GO
