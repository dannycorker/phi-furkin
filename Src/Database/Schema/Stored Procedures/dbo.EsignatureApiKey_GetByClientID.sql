SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EsignatureApiKey table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EsignatureApiKey_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[EsignatureApiKeyID],
					[ClientID],
					[ApiKey]
				FROM
					[dbo].[EsignatureApiKey] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureApiKey_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EsignatureApiKey_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureApiKey_GetByClientID] TO [sp_executeall]
GO
