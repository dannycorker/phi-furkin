SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 02/08/2017
-- Description:	Gets an account by the Purchased Product Payment Schedule
-- =============================================
CREATE PROCEDURE [dbo].[Account__GetByPurchasedProductPaymentScheduleID]
	@PurchasedProductPaymentScheduleID INT,
	@ClientID INT,
	@CustomerID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @AccountID INT

	SELECT @AccountID = AccountID From PurchasedProductPaymentSchedule WITH (NOLOCK)
	WHERE PurchasedProductPaymentScheduleID=@PurchasedProductPaymentScheduleID AND ClientID=@ClientID AND CustomerID=@CustomerID

	SELECT * FROM Account WITH (NOLOCK)
	WHERE AccountID=@AccountID AND ClientID=@ClientID AND CustomerID=@CustomerID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Account__GetByPurchasedProductPaymentScheduleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Account__GetByPurchasedProductPaymentScheduleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Account__GetByPurchasedProductPaymentScheduleID] TO [sp_executeall]
GO
