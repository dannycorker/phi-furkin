SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:  Simon Brushett
-- Create date: 2016-10-14
-- Description:  Searches existing customers for new quote and buy
-- IS 2016-10-19: Include Saved Quote Data to the search results
-- JEL 2018-02-06: Changed ordering to when created
-- GPR 2018-02-13: Changed default Affinity to 153389 (Client600)
-- OA 2018-02-14: Returns county from xml
-- CW 2018-05-01 Excluded deleted quotes from output
-- AMG 2018-06-27 Added extra column to denote portal source (Internal/External)
-- GPR 2018-07-19 Added QuoteOrigin to return source in IQB quote retrieval table
-- GPR 2018-07-23 Added C600 filter
-- GPR 2018-08-01 Pull out the Affiliate as source from Tracking Code (if any), if sq.AggregatorID ISNULL
-- GPR 2018-09-12 Case statement for internal/external when trackingcode/aggid ISNULL, C600 Defect #923
-- GPR 2019-05-10 Case statement to use SavedQuote.EmailAddress WHEN QuoteSessions.Email = '' for #56664
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Customer_SearchExisting] 
(
	@BrandID INT,
	@Firstname VARCHAR(200),
	@Lastname VARCHAR(200),
	@Postcode VARCHAR(200),
	@Email VARCHAR(200),
	@Page INT = NULL,
	@PageSize INT = NULL,
	@ClientID INT = NULL
)

AS
BEGIN

	IF @ClientID is NULL
	BEGIN
		SELECT @ClientID = dbo.fnGetPrimaryClientID()
	END

  -- sort paging variables
SELECT @Page = ISNULL(@Page, 1),
   @PageSize = ISNULL(@PageSize, 10)

 DECLARE @First INT,
   @Last INT
   
 SELECT @First = ((@Page - 1) * @PageSize) + 1
SELECT @Last = @Page * @PageSize

 
 DECLARE @Results TABLE
(
  ResultID INT,
  ItemType VARCHAR(20),
  TitleID INT,
  CustomerID INT, 
  QuoteSessionID INT, 
  FirstName VARCHAR(250), 
  LastName VARCHAR(250), 
  Address1 VARCHAR(250), 
  Address2 VARCHAR(250), 
  TownCity VARCHAR(250), 
  County VARCHAR(250),
  Postcode VARCHAR(250), 
  Email VARCHAR(250), 
  HomePhone VARCHAR(250),
  Mobile VARCHAR(250),
  PetNames VARCHAR(500),
  WhenCreated DATETIME,
  WhenExpire DATETIME,
  BrandID INT,
  DateOfBirth DATETIME2,
  Total INT,
  QuoteSource VARCHAR(3),
  
  QuoteOrigin VARCHAR(50) -- GPR 2018-07-19 adopted from 433s QuoteSource, we use QuoteSource for INT/EXT
)

;WITH DataSet AS 
 (
  SELECT 'Customer' ItemType, c.TitleID, c.CustomerID, 0 QuoteSessionID, c.FirstName, c.LastName, c.Address1, c.Address2, c.Town, c.County, c.PostCode, 
    c.EmailAddress, c.HomeTelephone, c.MobileTelephone, 
    NULL PetNames,
    (
     SELECT MIN(l.WhenCreated)
     FROM dbo.Lead l WITH (NOLOCK)
     WHERE l.CustomerID = c.CustomerID
     GROUP BY l.CustomerID
    ) WhenCreated,
    NULL WhenExpire,
    b.ValueInt BrandID,
    c.DateOfBirth DateOfBirth
	,'' AS QuoteSource
	,'' [Source] -- GPR 2018-07-19
  FROM Customers c WITH (NOLOCK)
  INNER JOIN CustomerDetailValues b ON c.CustomerID = b.CustomerID AND b.DetailFieldID = 170144
  WHERE c.Test = 0
  AND b.ValueInt = ISNULL(@BrandID, 2000184) -- Default to LPC 
  AND c.ClientID = @ClientID
  UNION ALL
  SELECT 'Customer' ItemType, p.TitleID, c.CustomerID, 0 QuoteSessionID, p.FirstName, p.LastName, p.Address1, p.Address2, p.Town, p.County, p.PostCode, 
    p.EmailAddress, p.HomeTelephone, p.MobileTelephone, 
    NULL PetNames,
    (
     SELECT MIN(l.WhenCreated)
     FROM dbo.Lead l WITH (NOLOCK)
     WHERE l.CustomerID = c.CustomerID
     GROUP BY l.CustomerID
    ) WhenCreated,
    NULL WhenExpire,
    b.ValueInt BrandID,
    c.DateOfBirth DateOfBirth
	,'' AS QuoteSource
	,'' [Source] -- GPR 2018-07-19
  FROM Customers c WITH (NOLOCK)
  LEFT JOIN dbo.Partner p WITH (NOLOCK) ON c.CustomerID = p.CustomerID
  INNER JOIN CustomerDetailValues b ON c.CustomerID = b.CustomerID AND b.DetailFieldID = 170144
  WHERE c.Test = 0
  AND c.ClientID = @ClientID
  AND b.ValueInt = ISNULL(@BrandID, 2000184) -- Default to LPC 
  UNION ALL
  SELECT DISTINCT
    'Quote' ItemType,
    sq.SavedQuoteXML.value('(//CustomerInfo/TitleId)[1]', 'VARCHAR(50)')  TitleID,
    0 CustomerID,
    c.QuoteSessionID,
    c.FirstName,
    c.LastName,
    sq.SavedQuoteXML.value('(//CustomerInfo/Address1)[1]', 'VARCHAR(50)') Address1,
    sq.SavedQuoteXML.value('(//CustomerInfo/Address2)[1]', 'VARCHAR(50)') Address2,
    sq.SavedQuoteXML.value('(//CustomerInfo/TownCity)[1]', 'VARCHAR(50)') TownCity,
    sq.SavedQuoteXML.value('(//CustomerInfo/County)[1]', 'VARCHAR(50)') County,
    c.PostCode,
    CASE c.Email WHEN '' THEN sq.EmailAddress ELSE c.Email END, /*GPR 2019-05-10 for #56664*/
    c.HomeTel,
    c.MobileTel,
    STUFF((
     SELECT ',' + t.c.value('PetName[1]','VARCHAR(250)')
     FROM sq.SavedQuoteXML.nodes('//PetInfo') as t(c)
     FOR XML PATH('')
    ),1,1,'')  PetNames,
    sq.WhenCreated,
    sq.WhenExpire,
    c.BrandID,
     CONVERT(DATETIME2, sq.SavedQuoteXML.value('(//CustomerInfo/DateOfBirth)[1]', 'DATETIMEOFFSET'), 1) DateOfBirth,
	 CASE
		WHEN sq.SavedQuoteXML.exist('//QuoteDefinition') = 1 THEN 'Ext'	ELSE 'Int' END AS QuoteSource
	,ISNULL(fn.ItemValue,CASE WHEN sq.PetQuoteDefinitionXml.exist('*') <> 0 THEN 'Web'	ELSE 'Call Centre' END) AS [Source] -- GPR 2018-07-19 /*GPR 2018-09-13 CASE statement for C600Defect #923*/
  FROM _C600_QuoteSessions c WITH (NOLOCK)
  INNER JOIN _C600_SavedQuote sq WITH (NOLOCK) ON sq.QuoteSessionID = c.QuoteSessionID
  OUTER APPLY dbo.fn_C600_GetDetailsFromTrackingCode(ISNULL(sq.AggregatorID, c.TrackingCode)) fn /*GPR 2018-08-01 ISNULL so that we pull out the Affiliate (if any), if this has not come via an Aggregator*/
  WHERE  (c.CustomerID IS NULL OR c.CustomerID = 0) AND sq.WhenExpire > dbo.fn_GetDate_Local()
  AND   c.BrandID = ISNULL(@BrandID, 153389) -- Default to L&G 
),
PagingDataSet AS
(
  SELECT *,  --SELECT TOP 25 *,  --Removed by GPR 2018-02-13
    ROW_NUMBER() OVER(ORDER BY LastName, FirstName, ItemType, CustomerID, QuoteSessionID DESC) as rn,
    SUM(1) OVER (PARTITION BY 1) as total
  FROM DataSet
  WHERE
   (NULLIF(@Firstname, '') IS NULL OR FirstName LIKE @Firstname + '%')
  AND
   (NULLIF(@LastName, '') IS NULL OR LastName LIKE @LastName + '%')
  AND
   (NULLIF(@PostCode, '') IS NULL OR PostCode LIKE @PostCode + '%')
  AND
   (NULLIF(@Email, '') IS NULL OR EmailAddress LIKE @Email + '%')
)
INSERT INTO @Results
SELECT
  rn ResultID, ItemType, TitleID, CustomerID, QuoteSessionID, FirstName, LastName, Address1, Address2, Town AS TownCity, County, PostCode AS Postcode, 
  EmailAddress AS Email, HomeTelephone AS HomePhone, MobileTelephone AS Mobile, PetNames, WhenCreated, WhenExpire, BrandID, DateOfBirth,
  total, QuoteSource, [Source] AS QuoteOrigin
FROM PagingDataSet pds
WHERE  rn BETWEEN @First AND @Last
ORDER BY WhenCreated DESC

SELECT * 
 FROM @Results
ORDER BY WhenCreated DESC

 SELECT r.ResultID, sq.SavedQuoteXML, r.QuoteSessionID
FROM @Results r
INNER JOIN _C600_QuoteSessions qs WITH (NOLOCK) ON r.ItemType = 'Customer' AND r.CustomerID = qs.CustomerID AND qs.BuyXml IS NULL
INNER JOIN _C600_SavedQuote sq WITH (NOLOCK) ON qs.QuoteSessionID = sq.QuoteSessionID
WHERE sq.SavedQuoteXML.exist('/deleted') = 0 /*CW 2018-05-01*/
UNION ALL
SELECT r.ResultID, sq.SavedQuoteXML, r.QuoteSessionID
FROM @Results r
INNER JOIN _C600_SavedQuote sq WITH (NOLOCK) ON r.ItemType = 'Quote' AND r.QuoteSessionID = sq.QuoteSessionID
WHERE sq.SavedQuoteXML.exist('/deleted') = 0 /*CW 2018-05-01*/
ORDER BY r.ResultID

 DECLARE @TotalRows INT   
 SELECT TOP 1 @TotalRows = Total
 FROM @Results

SELECT ISNULL(@TotalRows, 0) AS TotalRows,@Page AS Page, @PageSize AS PageSize

 END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Customer_SearchExisting] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Customer_SearchExisting] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Customer_SearchExisting] TO [sp_executeall]
GO
