SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Ben Crinion
-- Create date: 2008-01-01
-- Description:	Work out the best-case roadmap of future events for a case
-- JWG 2012-07-12 Use LeadDocument again now that it knows about DocumentBlobSize.
-- SB  2014-07-24 Use the new event type function for shared lead types
-- =============================================
CREATE PROCEDURE [dbo].[EventChoice__GetEventRoadmap]
(
	@LeadID INT,
	@CaseID INT,
    @StartFromID INT = NULL, 
    @ClientPersonnelID INT 
)
AS
BEGIN
SET NOCOUNT ON

	/*
	* Variable Declarations
	*/
	DECLARE
		@level           INT,
		@EventTypeID     INT,
		@EventSubtypeID  INT,
		@count			 INT,
		@RowID			 INT,
		@LeadTypeID		 INT,
		@eventchoicedesc VARCHAR(250),
		@eventtypedesc   VARCHAR(250),
		@outputline      VARCHAR(2000),
		@FINISHED        BIT
		
		
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID
                
	-- Create a temp table for this event_choice_stack
	DECLARE @stack TABLE
		(
		s_eventtypeid     INT         ,
		s_eventsubtypeid  INT         ,
		s_eventchoicedesc VARCHAR(250),
		s_eventtypedesc   VARCHAR(250),
		s_level           INT
		)
	-- Create a temp table for avoiding infinite loops
	DECLARE @choices TABLE
		(
		c_eventtypeid     INT,
		c_nexteventtypeid INT
		)

	-- Roadmap table stores the existing leadEvents and future event choices
	DECLARE @roadmap TABLE
		(
		ID				INT IDENTITY(1,1),
		existing		INT NULL, 
		eventTypeID		INT,
		followedUp		BIT NULL,
		leadDocumentID	INT NULL,
		IsBlobNull		BIT NULL,
		DocumentFormat	VARCHAR(24) NULL,
		OutputFormat	VARCHAR(24) NULL,
		[FileName]		VARCHAR(255) NULL
		)

	SELECT @LeadTypeID = LeadTypeID 
	FROM Lead WITH (NOLOCK)
	Where LeadID = @LeadID

	SET @FINISHED = 0
	
	/*
	* 
	* Existing Events
	*
	
	JWG 2010-03-17 Cater for new EventTypeDisplayOption table, which limits the number
	               of events that should be returned by type 
	               (eg ALL meeting minutes, Last 1 spec, Zero quotations etc)
	
	*/
	;
	WITH es AS
	(
		SELECT
			le.LeadEventID,
			le.EventTypeID,
			le.WhenCreated,
			CASE WHEN le.WhenFollowedUp IS NOT NULL THEN 1 ELSE 0 END as WhenFollowedUp,
			le.LeadDocumentID,
			CASE 
				WHEN ld.DocumentBlobSize > 0 THEN 0 
				ELSE 1 
			END as IsBlobNull, 
			d.DocumentFormat,
			dt.OutputFormat,
			d.[FileName], 
			etdo.ShowAll,
			etdo.ShowLastN,
			ROW_NUMBER() OVER(PARTITION BY le.EventTypeID ORDER BY le.WhenCreated desc, le.LeadEventID desc ) as rn
		FROM	LeadEvent le WITH (NOLOCK)
		LEFT JOIN NoteType nt WITH (NOLOCK) ON le.NoteTypeID = nt.NoteTypeID
		LEFT JOIN LeadDocument d WITH (NOLOCK) ON le.LeadDocumentId = d.LeadDocumentID
		LEFT JOIN dbo.LeadDocument ld WITH (NOLOCK) ON ld.LeadDocumentId = d.LeadDocumentID
		LEFT JOIN DocumentType dt WITH (NOLOCK) ON d.DocumentTypeID = dt.DocumentTypeID 
		LEFT JOIN dbo.EventTypeDisplayOption etdo WITH (NOLOCK) ON etdo.EventTypeID = le.EventTypeID 
		WHERE le.LeadID = @LeadID 
		AND (le.CaseID = @CaseID OR @CaseID IS NULL) 
		AND (le.EventDeleted = 0 OR le.EventDeleted IS NULL)

	)
	INSERT INTO @RoadMap(existing, eventTypeID, followedUp, leadDocumentID,IsBlobNull,DocumentFormat,OutputFormat,[FileName])
	SELECT es.LeadEventID, es.EventTypeID, es.WhenFollowedUp, es.LeadDocumentID, es.IsBlobNull, es.DocumentFormat, es.OutputFormat, es.[FileName]
	FROM es 
	WHERE (es.ShowAll IS NULL OR es.ShowAll = 1 OR es.ShowLastN >= es.rn)
	ORDER BY es.WhenCreated asc, es.LeadEventID asc

/*	INSERT INTO @RoadMap(existing, eventTypeID, followedUp, leadDocumentID,IsBlobNull,DocumentFormat,OutputFormat,[FileName])
	SELECT
		le.LeadEventID,
		le.EventTypeID,
		CASE WHEN le.WhenFollowedUp IS NOT NULL THEN 1 ELSE 0 END,
		le.LeadDocumentID,
		CASE WHEN d.DocumentBlob like 0x THEN 1 ELSE 0 END as IsBlobNull,
		d.DocumentFormat,
		dt.OutputFormat,
		d.[FileName]
	FROM	LeadEvent le WITH (NOLOCK)
	LEFT JOIN NoteType nt WITH (NOLOCK) ON le.NoteTypeID = nt.NoteTypeID
	LEFT JOIN LeadDocument d WITH (NOLOCK) ON le.LeadDocumentId = d.LeadDocumentID
	LEFT JOIN DocumentType dt WITH (NOLOCK) ON d.DocumentTypeID = dt.DocumentTypeID
	WHERE 
		le.LeadID = @LeadID 
		AND (le.CaseID = @CaseID OR @CaseID IS NULL) 
		AND (le.EventDeleted = 0 OR le.EventDeleted IS NULL)
	ORDER BY  le.WhenCreated asc, le.LeadEventID asc
	
	*/

	-- grab the top in-process event from the roadmap
	SELECT TOP 1 @StartFromID = et.eventTypeID, @RowID = ID
	FROM @roadmap r
	INNER JOIN dbo.fnEventTypeShared(@ClientID) et on r.EventTypeID = et.EventTypeID
	WHERE et.InProcess = 1
		AND r.FollowedUp = 0
	ORDER BY ID DESC

	SELECT @RowID = max(ID) 
	FROM @RoadMap

	--Debug Statements - not deleted so they can be re added easily later
	--PRINT 'Start Event 1 ' + convert(varchar, isnull(@StartFromID,0))
	--PRINT 'Event Event 1 '+ convert(varchar, isnull(@EventTypeID,0))

	/*
	* 
	* Future Event RoadMap
	*
	*/
	-- Get the Process Start event type for this lead type, unless a specific starting LeadEventID was passed in
	IF @StartFromID > 0 
	BEGIN
		WHILE(@RowID > 0 AND @EventTypeID is null)
		BEGIN
			-- Work out next event based on last in-process event that happened
			SELECT TOP 1 @EventTypeID = NextEventTypeID 
			FROM EventChoice WITH (NOLOCK) 
			WHERE EventTypeID = @StartFromID
			AND ClientID = @ClientID
			AND (-- weighting is 1
				EventChoice.Weighting			= 1  
				OR 
				(	-- or there are no weightings and is only 1 choice
					EventChoice.Weighting IS NULL 
					AND NOT EXISTS 
					(
						SELECT Count(*), EventTypeID 
						FROM EventChoice WITH (NOLOCK) 
						WHERE EventTypeID = @EventTypeID 
						GROUP BY EventTypeID 
						HAVING Count(*) > 1 
					)
				)
			)

			-- The last event might be the final step in a thread but not the final step in the process
			-- track back through the lead events which have not been followed up and get the next events
			IF(@EventTypeID is null)
			BEGIN
				SELECT TOP 1 @StartFromID = et.eventTypeID, @RowID = ID
				FROM @roadmap r
				INNER JOIN dbo.fnEventTypeShared(@ClientID) et on r.EventTypeID = et.EventTypeID
				WHERE et.InProcess = 1
					AND r.FollowedUp = 0
					AND ID < @RowID
				ORDER BY ID DESC

				SELECT @RowID = @RowID -1
			END		

		END

		-- if we have tracked back all the way through the event process and there are no choices anywhere, then just show the lead events
		IF(@EventTypeID is null)
		BEGIN
			--PRINT 'No Choice Exists'
			
			/* JWG 2009-07-10 New flag to say no more work is required. Better than early RETURN statements buried in the code */
			SET @FINISHED = 1

			/*SELECT * 
			FROM @RoadMap r
			INNER JOIN EventType et on r.EventTypeID = et.EventTypeID
			WHERE et.KeyEvent = 1
			RETURN*/
		END
		PRINT @EventTypeID


	END 
	ELSE 
	BEGIN 
		SELECT @EventTypeID = min(EventTypeID)
		FROM    dbo.fnEventTypeShared(@ClientID)
		WHERE   LeadTypeID        = @LeadTypeID
			AND EventSubtypeID    = 10
	END 


	/* JWG 2009-07-10 New flag to say no more work is required. Better than early RETURN statements buried in the code */
	IF @FINISHED = 0
	BEGIN
		SELECT @eventtypedesc	= EventTypeName,
			   @EventSubtypeID	= EventSubtypeID
		FROM    dbo.fnEventTypeShared(@ClientID)
		WHERE   EventTypeID = @EventTypeID

		INSERT INTO @stack VALUES
		(
			@EventTypeID    ,
			@EventSubtypeID ,
			'Starting Event',
			@eventtypedesc  ,
			1
		)

		SELECT @level = 1 
		SELECT @Count = 0

		WHILE @level > 0 AND @Count < 5000
		BEGIN 
			IF EXISTS (SELECT *
					   FROM    @stack
					   WHERE   s_level = @level)
			BEGIN 
				
				SELECT @EventTypeID = s_eventtypeid    ,
						@EventSubtypeID           = s_eventsubtypeid ,
						@eventchoicedesc          = s_eventchoicedesc,
						@eventtypedesc            = s_eventtypedesc
				FROM    @stack
				WHERE   s_level = @level 
				
				insert into @roadmap(existing, eventTypeID) values(null,@eventtypeid)

				DELETE
				FROM    @stack
				WHERE   s_level       = @level
					AND s_eventtypeid = @EventTypeID

				INSERT  @stack
				SELECT  EventChoice.NextEventTypeID,
						EventType.EventSubtypeID   ,
						EventChoice.[Description]  ,
						EventType.EventTypeName    ,
						@level + 1
				FROM    
						EventChoice WITH (NOLOCK)
				INNER JOIN 
						EventType WITH (NOLOCK) ON      EventType.EventTypeID = EventChoice.NextEventTypeID
				WHERE    EventChoice.EventTypeID		= @EventTypeID
					AND EventChoice.ClientID = @ClientID
					AND EventChoice.EscalationEvent		= 0 
					AND (
						EventChoice.Weighting			= 1  -- weighting is 1
						OR (								 -- or there are no weightings and is only 1 choice
							EventChoice.Weighting IS NULL 
							AND NOT EXISTS 
								(SELECT Count(*), EventTypeID 
								FROM EventChoice WITH (NOLOCK) 
								WHERE EventTypeID = @EventTypeID
								AND ClientID = @ClientID 
								GROUP BY EventTypeID 
								HAVING Count(*) > 1 )
						)
					)
					AND NOT EXISTS -- this stops the proc from going round in a loop
						(SELECT *
						FROM    @choices
						WHERE   EventChoice.EventTypeID     = c_eventtypeid
							AND EventChoice.NextEventTypeID = c_nexteventtypeid
						)

				INSERT @choices -- remember what choices we have been through so we know not to loop
				SELECT  EventChoice.EventTypeID,
						EventChoice.NextEventTypeID
				FROM    EventChoice WITH (NOLOCK)
				WHERE   EventChoice.EventTypeID     = @EventTypeID
					AND EventChoice.ClientID = @ClientID
					AND EventChoice.EscalationEvent = 0
					AND NOT EXISTS
						(SELECT *
						FROM    @choices
						WHERE   EventChoice.EventTypeID     = c_eventtypeid
							AND EventChoice.NextEventTypeID = c_nexteventtypeid
						) 

				IF @@ROWCOUNT > 0 
				BEGIN 
					SELECT @level = @level + 1
				END
			END 
			ELSE 
			BEGIN 
				SELECT @level = @level - 1
			END

			SELECT @Count = @Count + 1

		END -- END WHILE

	END


	/* 
		JWG 2009-07-10 
		Check security before showing key events. 
		Only returns Event Types this user has at least VIEW access to. 
	*/
	SELECT r.*, et.*  
	FROM @RoadMap r 
	INNER JOIN dbo.fnEventTypeShared(@ClientID) et on r.EventTypeID = et.EventTypeID 
	INNER JOIN dbo.fnEventTypeSecure(@ClientPersonnelID, @LeadTypeID) ets on r.EventTypeID = ets.objectid 
	WHERE et.KeyEvent = 1 
	ORDER BY r.ID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice__GetEventRoadmap] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventChoice__GetEventRoadmap] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice__GetEventRoadmap] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[EventChoice__GetEventRoadmap] TO [sp_executehelper]
GO
