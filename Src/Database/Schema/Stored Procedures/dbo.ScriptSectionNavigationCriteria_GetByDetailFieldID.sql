SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptSectionNavigationCriteria table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptSectionNavigationCriteria_GetByDetailFieldID]
(

	@DetailFieldID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ScriptSectionNavigationCriteriaID],
					[ClientID],
					[FromStepSectionID],
					[ToStepSectionID],
					[WizardButton],
					[DetailFieldID],
					[CriteriaOperator],
					[Value]
				FROM
					[dbo].[ScriptSectionNavigationCriteria] WITH (NOLOCK) 
				WHERE
					[DetailFieldID] = @DetailFieldID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria_GetByDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSectionNavigationCriteria_GetByDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria_GetByDetailFieldID] TO [sp_executeall]
GO
