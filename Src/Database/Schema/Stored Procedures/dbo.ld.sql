SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-03-01
-- Description:	Quick LeadDocument selection, excluding BLOBs
-- =============================================
CREATE PROCEDURE [dbo].[ld]  
	@LeadID int = NULL,
	@DocumentTypeID int = NULL,
	@Limit int = 50
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP (@Limit) 
	'LeadDocument' as [LeadDocument],
	LeadDocumentID,
	ClientID,
	LeadID,
	DocumentTypeID,
	LeadDocumentTitle,
	UploadDateTime,
	WhoUploaded,
	[FileName],
	DocumentFormat,
	EmailFrom,
	EmailTo,
	CcList,
	BccList,
	ElectronicSignatureDocumentKey,
	Encoding,
	ContentFormat,
	ZipFormat
	FROM dbo.LeadDocument ld WITH (NOLOCK) 
	WHERE (ld.LeadID = @LeadID OR @LeadID IS NULL) 
	AND (ld.DocumentTypeID = @DocumentTypeID OR @DocumentTypeID IS NULL) 
	ORDER BY ld.LeadDocumentID 
	
	SELECT TOP (@Limit) 
	'LeadDocumentFS' as [LeadDocumentFS],
	LeadDocumentID,
	ClientID,
	LeadID,
	DocumentTypeID,
	LeadDocumentTitle,
	UploadDateTime,
	WhoUploaded,
	[FileName],
	DocumentFormat,
	EmailFrom,
	EmailTo,
	CcList,
	BccList,
	ElectronicSignatureDocumentKey,
	Encoding,
	ContentFormat,
	ZipFormat, 
	DocumentBLOBSize, 
	EmailBLOBSize 
	FROM dbo.LeadDocumentFS ld WITH (NOLOCK) 
	WHERE (ld.LeadID = @LeadID OR @LeadID IS NULL) 
	AND (ld.DocumentTypeID = @DocumentTypeID OR @DocumentTypeID IS NULL) 
	ORDER BY ld.LeadDocumentID 
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[ld] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ld] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ld] TO [sp_executeall]
GO
