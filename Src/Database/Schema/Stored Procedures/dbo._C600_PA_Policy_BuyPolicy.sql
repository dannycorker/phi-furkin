SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2014-09-24
-- Description:	Buy policy
-- Mods:
-- 2014-12-19 DCM changed policy number to MatterID
-- 2015-02-24 DCM added microchip number
-- 2015-05-14 DCM convert colour LULI to plain text
-- 2015-07-07 DCM added camel case conversion for address fields and pet name
-- 2015-10-13 DCM pass pet number rather than total number to AddPolicyDetails
-- 2016-11-10 SB  Remove call to validate as too many differences here compared to where this was copied from
-- 2016-11-10 SB  Added save for additional fields provided by AH
-- 2016-11-15 JWG Added MarketingPreferenceId
-- 2017-06-04 CPS Take document preferences from the XML
-- 2017-08-08 CPS force Postcode to UPPER
-- 2017-08-17 AHOD Swapped _C00_SimpleValueIntoFieldLuli with _C00_SimpleValueIntoField @HouseholdMaritalStatus	
-- 2017-09-28 AHOD replaced fn_C600_CamelCaseIfRequired with fnConvertCamelCase to covert the Firstname and Lastname into Proper Case
-- 2018-02-19 SA - Updated to include Marketing Preference transfer to CustomerDetailFields
-- 2018-02-27 Mark Beaumont	- Amended marketing preferences for breeder portal (via external Q&B SDK)
-- 2018-02-28 Mark Beaumont - Capture the breeder id from the XML incoming from the breeder portal (One Month Free only)
-- 2018-03-01 Mark Beaumont - Breeder supplied existing policy number via breeder portal so match to existing customer
-- 2018-03-02 Mark Beaumont - Re-factored so that OMF does not try to capture any payment info as it now does not create a Collections lead
-- 2018-03-09 Mark Beaumont - Insert customer ID node into XML request for matched existing OMF policy
-- 2018-03-23 Mark Beaumont - For OMF, attempt to match to an existing customer even if Policy Number not supplied
-- 2018-04-03 Mark Beaumont - OMF breeder portal now allows multiple pets to be simultaneously submitted
-- 2018-04-04 ACE - If no bank then add CT record.
-- 2019-02-07 JEL If we have found an existing customer with an OMF lead then it will not have a DOB, update if null 
-- 2019-02-22 GPR If we have found an existing customer and we want to change the telephone number in the IQB journey, then update the phone number.
-- 2019-05-07 GPR / AMG - moved logic comms preference logic into independent code bock
-- 2019-05-07 GPR flipped BITS in Comms Preference block when 1 THEN true ELSE false. Previously this was inverted.
-- 2019-07-02 GPR altered assignment of marketing preferences, marketing preferences will now go through the realignment function before inserting the values on the Customer
-- 2019-09-05 AMG - removed as this is now handled elsewhere
-- 2019-09-20 GPR - If this Customer has allowed Marketing and has not previously then update the Consent Date field for Defect 1902
-- 2019-10-14 CPS for LPC-14		| Removed @IsOneMonthFreePolicy support
-- 2019-10-18 GPR for LPC-36		| Add the QuotePetProductID to DetailFieldID 314296
-- 2019-11-04 CPS for LPC-109		| Include correct policy number
-- 2019-11-20 GPR for LPC-54		| Added insert for PremiumFunderApplicationPolicy
-- 2020-03-04 PR for AAG-78			| Added insert for Claims Payment Account
-- 2020-03-10 GPR for AAG-78		| Added FriendlyName and AccountReference to insert for Claims Payment Account
-- 2020-03-25 GPR for AAG			| Included CountryID to split logic for use in TPET between 603 and 604
-- 2020-07-10 ALM for AAG-72		| Wrapped Upper around County to capatalise Austrailian States
-- 2020-07-31 PL for AAG-EQB		| Remapped partner details to authorised user, added vet info from EQB, Changed PetDOB from Varchar to Date
-- 2020-09-01 GPR					| Added code for populating table from UQ Q&As
-- 2021-03-29 ACE for FURKIN-273	| Added @VisionVetName
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Policy_BuyPolicy]
(
	@XmlRequest XML,
	@Debug BIT = 0
)
AS
BEGIN

	DECLARE @ClientID INT,
			@ClientPersonnelId INT,
			@MatterID INT,
			@CardTransactionID INT,
			@TotalGrossPremium NUMERIC(18,2),
			@PremiumFunderRequestID INT, /*GPR 2019-11-20*/
			@CountryID INT,
			@XmlResponse XML,
			@Result INT = 1,
			@ProductID INT,
			@BrandingID INT

	DECLARE @DetailValueHistory TABLE (ClientID INT, CustomerID INT, LeadID INT, CaseID INT, MatterID INT, LeadOrmatter INT, DetailFieldID INT, DetailValue VARCHAR(2000), WhenSaved DATETIME)

	SELECT	@ClientID = @XmlRequest.value('(//ClientId)[1]', 'INT'),
			@ClientPersonnelId = @XmlRequest.value('(//ClientPersonnelId)[1]', 'INT')

	/*GPR 2020-03-25*/
	SELECT @CountryID = c.CountryID 
	FROM Clients c WITH (NOLOCK) 
	WHERE c.ClientID = @ClientID

	EXEC dbo.LogXML__AddEntry @ClientID, 0, 'BuyPolicy', @XmlRequest, NULL

	/* 2018-03-02 Mark Beaumont - determine if this is a One Month Free (OMF) policy */
	SELECT	@ProductID = @XmlRequest.value('(BuyPolicyRequest/PetQuotes/PetQuote/PolicyValues/PolicyValue/ProductId)[1]', 'INT')
	SELECT  @BrandingID = @XmlRequest.value('(//BrandingId)[1]', 'INT')

	-- Turning off validation as PHI have different rules
	--EXEC _C00_1273_Policy_BuyPolicy_Validate @XmlRequest, @XmlResponse OUTPUT, @Result OUTPUT

	IF @Result = 1
	BEGIN

		BEGIN TRY
			-- only start tran in debug
			IF @Debug = 0
			BEGIN
				BEGIN TRAN
			END
			ELSE
			BEGIN
				SELECT 'No tran in debug' AS ACTION
			END
			
			DECLARE @QuoteSessionID INT
			SELECT	@QuoteSessionID = @XmlRequest.value('(//SessionId)[1]', 'INT')
			
			DECLARE @PolicyNumbers TABLE (PolicyNumber VARCHAR(2000), MatterID INT)
			DECLARE @LeadIDs TABLE (LeadID INT)
			
			DECLARE @PolicyAdminLeadTypeID INT = 1492
				
			-- Look up from the shares
			SELECT @PolicyAdminLeadTypeID = s.SharedTo
			FROM dbo.LeadTypeShare s WITH (NOLOCK) 
			WHERE s.ClientID = @ClientID
			AND s.SharedFrom = 1273

			-- Get Customer Details from XML
			DECLARE @Title INT,
					@Firstname VARCHAR(2000), 
					@Lastname VARCHAR(2000), 
					@Email VARCHAR(2000), 
					@HomePhone VARCHAR(2000), 
					@Mobile VARCHAR(2000), 
					@Address1 VARCHAR(2000), 
					@Address2 VARCHAR(2000), 
					@TownCity VARCHAR(2000), 
					@County VARCHAR(2000),
					@Postcode VARCHAR(2000),
					@SecondaryTitle VARCHAR(2000),
					@SecondaryFirstName VARCHAR(2000),
					@SecondaryLastName VARCHAR(2000),
					@SecondaryEmail VARCHAR(2000),
					@SecondaryHomePhone VARCHAR(2000),
					@SecondaryMobile VARCHAR(2000),
					@SecondaryDOB DATE,
					@DoNotSms BIT,		-- Marketing preference
					@DoNotEmail BIT,	-- Marketing preference
					@CustDoB DATE,
					@MembershipNumber VARCHAR(2000),
					@FriendlyName VARCHAR(500), 
					@MarketingPref VARCHAR(2000),
					@HouseholdNumberOfPeople	VARCHAR(2000),
					@HouseholdMaritalStatus	VARCHAR(2000)	,
					@HouseholdNumberOfPets		VARCHAR(2000),
					@PolicyNumber				VARCHAR(2000) = '',
					@PHNotAccountHolder		INT,
					@AuthorityReq			INT,
					@DoNotPhone BIT,	-- Marketing preference
					@DoNotPost BIT,		-- Marketing preference
					@Now DATETIME = dbo.fn_GetDate_Local(), 
					@AmountNet DECIMAL (18,2),
					@AmountVAT DECIMAL (18,2), 
					@Amount DECIMAL (18,2), 
					@Description VARCHAR(200),
					@PaymentID INT ,
					@ClientPaymentGatewayID INT,
					@IsAgg BIT,

					@IsEQB BIT,
					@AggregatorID VARCHAR(2000) = NULL,
					@ReimbursementSortCode VARCHAR(2000),
					@ReimbursementInstitutionNumber VARCHAR(2000),
					@ReimbursementAccountNumber VARCHAR(2000),
					@ReimbursementBankName VARCHAR(2000),
					@ReimbursementBankAccountName VARCHAR(2000),
					@AccountID INT,
					@ValidMobile VARCHAR(3)
			
			DECLARE @CustomerFields TABLE (FieldID INT)	
					
			SELECT	@Title				= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/TitleId)[1]', 'int'),
					@Firstname			= dbo.fnConvertCamelCase(@XmlRequest.value('(BuyPolicyRequest/CustomerInfo/FirstName)[1]', 'varchar(2000)')), /*AHOD 2017-09-28*/
					@Lastname			= dbo.fnConvertCamelCase(@XmlRequest.value('(BuyPolicyRequest/CustomerInfo/LastName)[1]', 'varchar(2000)')), /*AHOD 2017-09-28*/
					@Email				= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/Email)[1]', 'varchar(2000)'),
					@HomePhone			= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/HomePhone)[1]', 'varchar(2000)'),
					@Mobile				= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/MobilePhone)[1]', 'varchar(2000)'),
					@Address1			= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/Address1)[1]', 'varchar(2000)'),
					@Address2			= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/Address2)[1]', 'varchar(2000)'),
					@TownCity			= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/TownCity)[1]', 'varchar(2000)'),
					@County				= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/County)[1]', 'varchar(2000)'), /*ALM 2020-07-10*/
					@Postcode			= UPPER(@XmlRequest.value('(BuyPolicyRequest/CustomerInfo/Postcode)[1]', 'varchar(2000)')), /*CPS 2017-08-08*/
					@SecondaryTitle		= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/SecondaryTitleId)[1]', 'int'),
					@SecondaryFirstName	= dbo.fnConvertCamelCase(@XmlRequest.value('(BuyPolicyRequest/CustomerInfo/SecondaryFirstName)[1]', 'varchar(2000)')), /*AHOD 2017-09-28*/
					@SecondaryLastName	= dbo.fnConvertCamelCase(@XmlRequest.value('(BuyPolicyRequest/CustomerInfo/SecondaryLastName)[1]', 'varchar(2000)')), /*AHOD 2017-09-28*/
					@SecondaryEmail		= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/SecondaryEmail)[1]', 'varchar(2000)'),
					@SecondaryHomePhone	= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/SecondaryHomePhone)[1]', 'varchar(2000)'),
					@SecondaryMobile	= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/SecondaryMobilePhone)[1]', 'varchar(2000)'),
					@SecondaryDOB		= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/SecondaryDateOfBirth)[1]', 'DATE'),
					@DoNotSms			= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/DoNotSms)[1]', 'BIT'),
					@DoNotEmail			= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/DoNotEmail)[1]', 'BIT'),
					@CustDoB			= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/DateOfBirth)[1]', 'DATE'),
					@MembershipNumber	= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/MembershipNumber)[1]', 'varchar(2000)'), 
					@MarketingPref      = @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/MarketingPreferenceId)[1]', 'varchar(2000)'),
					@CardTransactionID  = @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/CardTransactionID)[1]', 'varchar(2000)'),
					@PremiumFunderRequestID = @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/PremiumFunderRequestID)[1]', 'varchar(2000)') 
					
					/*CPS 2017-06-22*/
					,@HouseholdNumberOfPeople	= @XmlRequest.value('(BuyPolicyRequest/HouseholdInfo/NumberOfPeople)[1]', 'varchar(2000)')
					,@HouseholdMaritalStatus	= @XmlRequest.value('(BuyPolicyRequest/HouseholdInfo/MaritalStatusID)[1]', 'varchar(2000)')
					,@HouseholdNumberOfPets		= @XmlRequest.value('(BuyPolicyRequest/HouseholdInfo/NumberOfPets)[1]', 'varchar(2000)'),

					/*SA 2017-02-19 - PM108 - Marketing Preferences*/
					@DoNotPhone		= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/DoNotPhone)[1]', 'BIT'),
					@DoNotPost		= @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/DoNotPost)[1]', 'BIT')
					
					
					/*GPR 2019-06-28 Identify IsAggregator #57948*/
					SELECT @AggregatorID = s.AggregatorID
					FROM _C600_SavedQuote s WITH (NOLOCK)
					WHERE s.QuoteSessionID = @QuoteSessionID
					
					IF LEN(ISNULL(@AggregatorID,'0')) > 1
					BEGIN  
						SELECT @IsAgg = 1					
					END
					ELSE 
					BEGIN
						SELECT @IsAgg = 0
					END			
										
					/*GPR 2019-06-28 Identify IsEQB #57948*/
					SELECT @IsEQB = CASE @XmlRequest.value('(BuyPolicyRequest/PetQuotes/PetQuote/PetInfo/CreatedOnExternalQuoteAndBuy)[1]', 'varchar(5)')
									WHEN 'false' THEN 0
									ELSE 1 END /*GPR 2019-07-01 adjusted logic*/
					
					/*GPR 2019-06-28 Realign Marketing Preferences #57948*/
					SELECT @DoNotEmail	= dbo.fn_C600_MarketingPreferenceRealignment(@DoNotEmail, @IsEQB, @IsAgg)
					SELECT @DoNotSms	= dbo.fn_C600_MarketingPreferenceRealignment(@DoNotSms, @IsEQB, @IsAgg)
					SELECT @DoNotPhone	= dbo.fn_C600_MarketingPreferenceRealignment(@DoNotPhone, @IsEQB, @IsAgg)
					SELECT @DoNotPost	= dbo.fn_C600_MarketingPreferenceRealignment(@DoNotPost, @IsEQB, @IsAgg)
					
			/*ALM 2021-04-08 FURKIN-473*/
			IF @CountryID = 39 
			BEGIN
				SELECT @County =	CASE @County 
										WHEN 'AB' THEN 'Alberta'
										WHEN 'BC' THEN 'British Columbia'
										WHEN 'MB' THEN 'Manitoba'
										WHEN 'NB' THEN 'New Brunswick'
										WHEN 'NL' THEN 'Newfoundland and Labrador'
										WHEN 'NS' THEN 'Nova Scotia'
										WHEN 'NT' THEN 'Northwest Territories'
										WHEN 'NU' THEN 'Nunavut'
										WHEN 'ON' THEN 'Ontario'
										WHEN 'PE' THEN 'Prince Edward Island'
										WHEN 'QC' THEN 'Quebec'
										WHEN 'SK' THEN 'Saskatchewan'
										WHEN 'YT' THEN 'Yukon'
										ELSE @County
									END
			
			END					
				
			SELECT @Address1 = CASE @Address1 WHEN NULL THEN @Address1 ELSE [dbo].[fnConvertCamelCase] (@Address1) END, 
					@Address2 = CASE @Address2 WHEN NULL THEN @Address2 ELSE [dbo].[fnConvertCamelCase] (@Address2) END, 
					@TownCity = CASE @TownCity WHEN NULL THEN @TownCity ELSE [dbo].[fnConvertCamelCase] (@TownCity) END,
					@County = CASE @County WHEN NULL THEN @County ELSE [dbo].[fnConvertCamelCase] (@County) END 

			IF @Debug = 1
			BEGIN
			
				SELECT	@Title				AS Title,
						@Firstname			AS Firstname,
						@Lastname			AS Lastname,	
						@Email				AS Email,	
						@HomePhone			AS HomePhone,	
						@Mobile				AS Mobile,	
						@Address1			AS Address1,	
						@Address2			AS Address2,	
						@TownCity			AS TownCity,	
						@County				AS County,	
						@Postcode			AS Postcode,
						@SecondaryTitle		AS SecondaryTitle,
						@SecondaryFirstName	AS SecondaryFirstName,
						@SecondaryLastName	AS SecondaryLastName,
						@SecondaryEmail	    AS SecondaryEmail,
						@SecondaryHomePhone	AS SecondaryHomePhone,
						@SecondaryMobile	AS SecondaryCellPhone,
						@SecondaryDoB		AS SecondaryDoB,
						@DoNotSms			AS DoNotSms,
						@DoNotEmail			AS DoNotEmail,
						@CustDoB			AS CustDoB,
						@MembershipNumber	AS MembershipNumber, 
						@MarketingPref      AS MarketingPref
				
			END


			DECLARE @CustomerID INT = NULL

			/* Non OMF customer ID supplied so will require matching */
			SELECT @CustomerID = @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/CustomerId)[1]', 'INT')

			IF @Debug = 1
				SELECT @CustomerID AS CustomerID


			--/*GPR 2019-12-18 for SDLPC-51*/
			--IF @CustomerID > 1
			--BEGIN

			--	DECLARE @Address1check VARCHAR(50)
			--	DECLARE @Address2check VARCHAR(50)

			--	SELECT @Address1check = Address1, @Address2check = Address2 FROM Customers WITH (NOLOCK) WHERE CustomerID = @CustomerID

			--	IF  @Address1check IS NULL AND @Address2check IS NULL /*Then we're likely a stub Customer created for PCL or WorldPay so we can perform this update outside of an MTA*/
			--		BEGIN
			--		UPDATE Customers /*GPR 2019-12-19 modified for SDLPC-64*/
			--		SET TitleID = @Title, DateOfBirth = @CustDoB, HomeTelephone = @HomePhone, MobileTelephone = ISNULL(@Mobile,''), Address1 = @Address1, Address2 = @Address2, Town = @TownCity, County = ISNULL(@County,'') /*GPR 2019-12-17 Added County for SDLPC-52*/
			--		FROM Customers c 
			--		WHERE c.CustomerID = @CustomerID 
			--		AND ISNULL(c.DateOfBirth,'') = ''
			--	END

			--END

			IF @CustomerID > 1
			BEGIN
					
				/*GPR 2019-06-28 #57948*/
				IF 	@DoNotEmail = 1
					EXEC dbo._C00_SimpleValueIntoField	177153, 'true', @CustomerID, @ClientPersonnelID
				ELSE
					EXEC dbo._C00_SimpleValueIntoField	177153, 'false', @CustomerID, @ClientPersonnelID
				IF @DoNotSMS = 1
					EXEC dbo._C00_SimpleValueIntoField	177152, 'true', @CustomerID, @ClientPersonnelID
				ELSE
					EXEC dbo._C00_SimpleValueIntoField	177152, 'false', @CustomerID, @ClientPersonnelID
				IF @DoNotPhone = 1
					EXEC dbo._C00_SimpleValueIntoField	177371, 'true', @CustomerID, @ClientPersonnelID
				ELSE
					EXEC dbo._C00_SimpleValueIntoField	177371, 'false', @CustomerID, @ClientPersonnelID
				IF @DoNotPost = 1
					EXEC dbo._C00_SimpleValueIntoField	177154, 'true', @CustomerID, @ClientPersonnelID
				ELSE
					EXEC dbo._C00_SimpleValueIntoField	177154, 'false', @CustomerID, @ClientPersonnelID
								
			END

			/*GPR 2019-11-12 - Existing Customers, cloned out of IF @CustomerID < 1 block*/
			IF @CustomerID > 1
			BEGIN

				DECLARE @SendEmails VARCHAR(50),
						@SendSms VARCHAR(50),
						@SendDocs VARCHAR(50)
					
				SELECT @SendDocs = ISNULL(@XmlRequest.value('(BuyPolicyRequest/CustomerInfo/ContactMethodId)[1]', 'varchar(2000)'), '60788')
					
				IF @SendDocs = 0 
				BEGIN 
					SELECT @SendDocs = 60788
				END
				
					/* 2018-02-27 Mark Beaumont	- set customer care based on communication preference */
					IF @SendDocs = '60789'	-- Communication preference = Electronic
						SELECT	@SendEmails = '5144',	-- Send customer care emails = Yes
								@SendSms = '5144'		-- Send customer care SMSs = Yes
					ELSE					-- Communication preference = Paper
						SELECT	@SendEmails = '5145',	-- Send customer care emails = No
								@SendSms = '5145'		-- Send customer care SMSs = No										
								
					SELECT @FriendlyName=Title FROM Titles WITH (NOLOCK) WHERE TitleID=@Title
					SELECT @FriendlyName = CASE WHEN @FriendlyName IS NULL THEN '' ELSE @FriendlyName + ' ' + @Firstname + ' ' + @Lastname END
					
					INSERT @CustomerFields (FieldID) VALUES
					(170257), (170259), (170260), (175495) ,(170110), (176970), (170144)
					
					INSERT dbo.CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
					SELECT	@ClientID, @CustomerID, f.FieldID,
							CASE f.FieldID
								WHEN 170257 THEN @SendDocs
								WHEN 170259	THEN @SendEmails
								WHEN 170260	THEN @SendSms
								WHEN 175495 THEN ISNULL(@MembershipNumber, '')
								WHEN 170110 THEN @FriendlyName
								WHEN 176970 THEN ISNULL(@MarketingPref, '')
								WHEN 170144 THEN CAST(@BrandingID as VARCHAR(2000)) /*Affinity Details*/
								ELSE ''		
							END	
					FROM @CustomerFields f
			END
	
			IF @CustomerID < 1 OR @CustomerID IS NULL
			BEGIN		

				IF @Debug = 1
				BEGIN
					SELECT 'Insert Customer' AS ACTION
				END
				ELSE 
				BEGIN
					
					DECLARE @Test BIT = 0

					INSERT dbo.Customers (ClientID, TitleID, FirstName, LastName, Address1, Address2, Town, County, PostCode, EmailAddress, HomeTelephone, MobileTelephone, IsBusiness, AquariumStatusID, Test, DateOfBirth, CountryID)	
					VALUES (@ClientID, @Title, @Firstname, @Lastname, ISNULL(@Address1, ''), ISNULL(@Address2, ''), ISNULL(@TownCity, ''), ISNULL(@County, ''), ISNULL(@PostCode, ''), @Email, @HomePhone, @Mobile, 0, 2, @Test, @CustDoB, @CountryID)

					SELECT @CustomerID = SCOPE_IDENTITY()					
					
					SELECT @SendDocs = ISNULL(@XmlRequest.value('(BuyPolicyRequest/CustomerInfo/ContactMethodId)[1]', 'varchar(2000)'), '60788')
					
					IF @SendDocs = 0 
					BEGIN 
						SELECT @SendDocs = '60788'
					END

					INSERT @CustomerFields (FieldID) VALUES
					(170257), (170259), (170260), (175495) ,(170110), (176970), (170144)

					/* 2018-02-27 Mark Beaumont	- set customer care based on communication preference */
					IF @SendDocs = '60789'	-- Communication preference = Electronic
						SELECT	@SendEmails = '5144',	-- Send customer care emails = Yes
								@SendSms = '5144'		-- Send customer care SMSs = Yes
					ELSE					-- Communication preference = Paper
						SELECT	@SendEmails = '5145',	-- Send customer care emails = No
								@SendSms = '5145'		-- Send customer care SMSs = No										
								
					SELECT @FriendlyName=Title FROM Titles WITH (NOLOCK) WHERE TitleID=@Title
					SELECT @FriendlyName = CASE WHEN @FriendlyName IS NULL THEN '' ELSE @FriendlyName + ' ' + @Firstname + ' ' + @Lastname END
					
					INSERT dbo.CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue)
					SELECT	@ClientID, @CustomerID, f.FieldID,
							CASE f.FieldID
								WHEN 170257 THEN @SendDocs
								WHEN 170259	THEN @SendEmails
								WHEN 170260	THEN @SendSms
								WHEN 175495 THEN ISNULL(@MembershipNumber, '')
								WHEN 170110 THEN @FriendlyName
								WHEN 176970 THEN ISNULL(@MarketingPref, '')
								WHEN 170144 THEN CAST(@BrandingID as VARCHAR(2000)) /*Affinity Details*/
								ELSE ''		
							END	
					FROM @CustomerFields f
					
					/*2019-05-07 GPR AMG moved logic*/
					
					/*GPR 2019-06-28 #57948*/
					IF 	@DoNotEmail = 1
						EXEC dbo._C00_SimpleValueIntoField	177153, 'true', @CustomerID, @ClientPersonnelID
					ELSE
						EXEC dbo._C00_SimpleValueIntoField	177153, 'false', @CustomerID, @ClientPersonnelID
					IF @DoNotSMS = 1
						EXEC dbo._C00_SimpleValueIntoField	177152, 'true', @CustomerID, @ClientPersonnelID
					ELSE
						EXEC dbo._C00_SimpleValueIntoField	177152, 'false', @CustomerID, @ClientPersonnelID
					IF @DoNotPhone = 1
						EXEC dbo._C00_SimpleValueIntoField	177371, 'true', @CustomerID, @ClientPersonnelID
					ELSE
						EXEC dbo._C00_SimpleValueIntoField	177371, 'false', @CustomerID, @ClientPersonnelID
					IF @DoNotPost = 1
						EXEC dbo._C00_SimpleValueIntoField	177154, 'true', @CustomerID, @ClientPersonnelID
					ELSE
						EXEC dbo._C00_SimpleValueIntoField	177154, 'false', @CustomerID, @ClientPersonnelID
				END
				
				-- Partner details if passed
				IF @SecondaryFirstName > '' AND @SecondaryLastName > ''
				BEGIN
				
					IF @Debug = 1
					BEGIN
						SELECT 'Insert Authorised User' AS ACTION
					END
					ELSE 
					BEGIN
						
						/* PL - 2020-07-31 - AAG-EQB Change - Adding Authorised User information to Authorised User rather than to the Partner Information */
						DECLARE @AuthorisedUserFields TABLE (FieldID INT)
						INSERT @AuthorisedUserFields(FieldID) VALUES
						(178259),	-- Name
						(313932),	-- DoB
						(313933),	-- Mobile
						(178261),	-- DateActive
						(178262),	-- Active or Inactive
						(179969)	-- Access Level

						DECLARE 
							@SecondaryFullName VARCHAR(2000) = CONCAT(@SecondaryFirstName, ' ', @SecondaryLastName),
							@ActiveInactive INT = 74577,
							@AccessLevel INT = 76420,
							@AuthorisedTableRowID INT

						INSERT INTO TableRows (ClientID, DetailFieldID, DetailFieldPageID, CustomerID)
						VALUES (@ClientID, 178263, 19131, @CustomerID)

						SELECT @AuthorisedTableRowID = SCOPE_IDENTITY()

						INSERT dbo.TableDetailValues (TableRowID, ClientID, CustomerID, DetailFieldID, DetailValue)
						SELECT	@AuthorisedTableRowID, @ClientID, @CustomerID, auf.FieldID, ISNULL( -- 2020-02-12 CPS for JIRA AAG-106 | Added ISNULL()
							CASE auf.FieldID
								WHEN 178259 THEN ISNULL(@SecondaryFullName, '')
								WHEN 313932	THEN ISNULL(CONVERT(VARCHAR(10), @SecondaryDoB, 120), '')
								WHEN 313933 THEN ISNULL(@SecondaryMobile, '')
								WHEN 178261	THEN ISNULL(CONVERT(VARCHAR(10), dbo.fn_GetDate_Local(), 120), '')
								WHEN 178262 THEN ISNULL(CAST(@ActiveInactive AS VARCHAR), '')
								WHEN 179969 THEN ISNULL(CAST(@AccessLevel AS VARCHAR), '')
								ELSE ''	
							END	,'')
						FROM @AuthorisedUserFields auf
	
					END
					
				END
				
			END -- End insert customer	
			ELSE -- Existing customer
			BEGIN
				
				IF @Debug = 1
				BEGIN
					SELECT 'Existing Customer' AS ACTION
				END
				ELSE 
				BEGIN
					PRINT 'Nothing to do... yet'
				END

				/*JEL 2019-02-07 If we have found an existing customer with an OMF lead then it will not have a DOB,*/
				UPDATE Customers 
				SET DateOfBirth = @CustDoB 
				FROM Customers c 
				WHERE c.CustomerID = @CustomerID 
				AND ISNULL(c.DateOfBirth,'') = ''
				AND EXISTS (SELECT * FROM Lead l WITH (NOLOCK) 
							INNER JOIN Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID 
							INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID AND mdv.DetailFieldID = 170034	/*Product RL*/
							WHERE  mdv.ValueInt = 151577 /*One month free policy*/
							AND l.CustomerID = c.CustomerID 
							AND l.LeadTypeID = 1492)
				
				/*GPR 2019-02-22 If we have found an existing customer and we want to change the telephone number in the IQB journey, then update the phone number.*/
				UPDATE Customers 
				SET HomeTelephone = @HomePhone 
				FROM Customers c 
				WHERE c.CustomerID = @CustomerID 
				AND EXISTS (SELECT * FROM Lead l WITH (NOLOCK) 
							INNER JOIN Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID 
							INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID AND mdv.DetailFieldID = 170034	/*Product RL*/
							WHERE  mdv.ValueInt = 151577 /*One month free policy*/
							AND l.CustomerID = c.CustomerID 
							AND l.LeadTypeID = 1492)





							
			/*GPR 2019-12-23 for SDLPC-52*/
			IF @CustomerID > 1
			BEGIN
				
			DECLARE @Countycheck VARCHAR(50)
			SELECT @Countycheck = County FROM Customers WITH (NOLOCK) WHERE CustomerID = @CustomerID

			IF ISNULL(@Countycheck,'') = ''
			BEGIN

					SELECT @County = @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/County)[1]', 'varchar(2000)')
					
					UPDATE Customers
					SET County = @County
					FROM Customers c 
					WHERE c.CustomerID = @CustomerID


					/*GPR 2019-12-23 for SDLPC-69*/
					SELECT @Title = @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/TitleId)[1]', 'int')

					UPDATE Customers
					SET TitleID = @Title
					FROM Customers c 
					WHERE c.CustomerID = @CustomerID

			END
			END


							
			END	
			
					/*GPR 2019-09-20 #1902 - If this Customer has allowed Marketing and has not previously then update the Consent Date field*/
					DECLARE @ConsentDate DATE
					SELECT @ConsentDate = [dbo].[fnGetSimpleDvAsDate](180283, @CustomerID)

					IF @ConsentDate IS NULL
					BEGIN

						IF @DoNotEmail = 1 OR @DoNotSMS = 1 OR @DoNotPhone = 1 OR  @DoNotPost = 1
						BEGIN
							SELECT @ConsentDate = dbo.fn_GetDate_Local()
							EXEC [dbo].[_C00_SimpleValueIntoField] 180283, @ConsentDate, @CustomerID, @ClientPersonnelID
						END

					END



			-- Now handle the pets and leads
			DECLARE @Pets TABLE
			(
				ID INT IDENTITY,
				XML XML
			)
			
			INSERT @Pets (XML)
			SELECT	node.query('.')
			FROM @XmlRequest.nodes('//PetQuote') AS pet(node)

			IF @Debug = 1
			BEGIN
				SELECT *
				FROM @Pets
			END
			
			DECLARE @LeadFields TABLE (FieldID INT)
			INSERT @LeadFields(FieldID) VALUES
			(144268),	-- Name
			(144275),	-- Gender
			(144272),	-- Breed
			(144274),	-- DoB
			(152783),	-- Spayed or Neutered
			(145024),	-- Age in years
			(170030),	-- Microchip number
			(144339),	-- Purchase price
			(144273),	-- Colour
			(175497),	-- Vaccinations
			(175496),	-- Existing conditions
			(146215),   -- Vet RLID
			(315897),   -- Vision Vet ID
			(315898),   -- Vision Vet Name
			(313947),	-- Vet practice from EQB
			(177372),	-- Microchip yes / no
			(180288)	-- QuotePetProductID / GPR 2019-10-18 LPC-36
			
			-- Loop through the pets
			DECLARE @Count INT,
					@Index INT
					
			SELECT	@Count = COUNT(*),
					@Index = 0
			FROM @Pets
			
			DECLARE @TotalPets INT
			SELECT @TotalPets = @Count
			DECLARE @PetNumber INT
			SELECT @PetNumber=CASE WHEN @TotalPets>1 THEN 1 ELSE 0 END 
			
			WHILE @Index < @Count
			BEGIN
				
				SELECT @Index += 1,
						@PetNumber += 1	
				
				DECLARE @LeadID INT = NULL,
						@PetName VARCHAR(2000) = NULL,
						@Pet XML = NULL,
						@Gender VARCHAR(2000) = NULL,
						@SpayNeut VARCHAR(2000) = NULL,
						@Breed VARCHAR(2000) = NULL,
						@DoB DATE, -- PL - 2020-07-31 - AAG-EQB Change - Changed from VARCHAR to DATE
						@Purchase MONEY = NULL,
						@Colour VARCHAR(2000) = NULL,
						@Vaccinations VARCHAR(2000) = NULL,
						@ExistingCond VARCHAR(2000) = NULL,
						@MicrochipNumber VARCHAR(2000) = NULL,
						@VetID VARCHAR(200) = NULL,
						@VisionVetName VARCHAR(200) = NULL,
						@EQBVET VARCHAR(2000) = NULL,
						@HasMicrochip VARCHAR(2000) = NULL,
						@QuotePetProductID	INT = NULL,
						@RuleSetID INT,
						@AbRuleSetID INT
						
				SELECT @Pet = XML
				FROM @Pets
				WHERE ID = @Index
						
				SELECT	@PetName	= @Pet.value('(//PetInfo/PetName)[1]', 'varchar(2000)'),
						@Gender		= @Pet.value('(//PetInfo/Gender)[1]', 'varchar(2000)'),
						@Breed		= @Pet.value('(//PetInfo/BreedId)[1]', 'varchar(2000)'),
						@DoB		= @Pet.value('(//PetInfo/BirthDate)[1]', 'DATE'), -- PL - 2020-07-31 - AAG-EQB Change - Changed from VARCHAR to DATE
						@SpayNeut	= @Pet.value('(//PetInfo/IsNeutered)[1]', 'varchar(2000)'),
						@Purchase	= @Pet.value('(//PetInfo/PurchasePrice)[1]', 'MONEY'),
						@Colour		= @Pet.value('(//PetInfo/PetColour)[1]', 'varchar(2000)'),
						@MicrochipNumber	= @Pet.value('(//PetInfo/MicrochipNo)[1]', 'varchar(2000)'),
						@Vaccinations	= @Pet.value('(//PetInfo/VaccinationsUpToDate)[1]', 'varchar(2000)'),
						@ExistingCond	= @Pet.value('(//PetInfo/HasExistingConditions)[1]', 'varchar(2000)'),
						@VetID = @Pet.value('(PetQuote/VetID)[1]', 'varchar(2000)'),
						@VisionVetName = @Pet.value('(PetQuote/VetName)[1]', 'varchar(2000)'),
						@EQBVET = @XmlRequest.value('(BuyPolicyRequest/CustomerInfo/VetPractice)[1]', 'VARCHAR(2000)'), -- PL - 2020-07-31 - AAG-EQB Change - Freetext Vet Practice from EQB
						@HasMicrochip = @Pet.value('(//PetInfo/HasMicrochip)[1]', 'varchar(2000)')
				
				/*GPR 2019-10-11 for LPC-36*/			
				/*GPR 2019-03-05 for #55470 - /*CPS 2018-06-06 save the QuotePetProduct against the pet*/*/
				SELECT TOP 1 @QuotePetProductID = qpp.QuotePetProductID
							,@RuleSetID = qpp.RuleSetID
							,@AbRuleSetID = qpp.AbControllerRuleSetID
				FROM QuotePetProduct qpp WITH ( NOLOCK ) 
				INNER JOIN QuotePet qp WITH ( NOLOCK ) ON qp.QuotePetID = qpp.QuotePetID
				WHERE qpp.QuoteSessionID = @QuoteSessionID
				AND qp.PetName = @PetName
				AND qpp.ProductID = @Pet.value('(//ProductId)[1]', 'VARCHAR(2000)')
				ORDER BY qpp.QuotePetProductID DESC
						
				IF @Colour IS NULL OR dbo.fnIsInt(@Colour) = 0
				BEGIN
					SELECT @Colour = li.LookupListItemID /*CPS 2017-08-11*/
					FROM LookupListItems li WITH ( NOLOCK ) 
					WHERE li.LookupListID = 5007
					AND ( li.ItemValue = @Pet.value('(//PetInfo/PetColorId)[1]', 'varchar(2000)')
						OR li.LookupListItemID = @Pet.value('(//PetInfo/PetColorId)[1]', 'varchar(2000)') ) /*CPS 2017-08-15 iQ&B is sending an ID number labeled with American spelling*/
				END
 	
				SELECT @PetName = [dbo].[fnConvertCamelCase] (@PetName) 

				-- colour is plain text field in Lead Manager /*CPS 2017-08-15 no, no it isn't*/
				--SELECT @Colour=ItemValue FROM LookupListItems WITH (NOLOCK) WHERE LookupListItemID=CAST(@Colour AS INT)
							
				IF @Debug = 0
				BEGIN

					INSERT INTO dbo.Lead(ClientID, LeadRef, CustomerID, LeadTypeID, AquariumStatusID, BrandNew, Assigned, RecalculateEquations, WhenCreated)
					VALUES(@ClientID, 'Policies: ' + @PetName, @CustomerID, @PolicyAdminLeadTypeID, 2, 0, 0, 0, dbo.fn_GetDate_Local())

					SELECT @LeadID = SCOPE_IDENTITY()
					INSERT @LeadIDs(LeadID) VALUES (@LeadID)				
				END


				IF @Debug = 1
				BEGIN
				
					SELECT @PetName			AS PetName,
						   @Gender			AS Gender,		
						   @Breed			AS Breed,		
						   @DoB				AS DoB,	
						   @SpayNeut		AS SpayNeut,
						   @Purchase		AS Purchase,
						   @Colour			AS Colour,
						   @MicrochipNumber	AS MicrochipNumber,
						   @VetID           AS VetID,
						   @VisionVetName	AS VisionVetName,
						   @EQBVET			AS EQBVET,
						   @HasMicrochip	AS HasMicrochip,
						   @ActiveInactive	AS ActiveInactive,
						   @AccessLevel		AS AccessLevel
				END
				ELSE 
				BEGIN
					
					INSERT dbo.LeadDetailValues (ClientID, LeadID, DetailFieldID, DetailValue)
					OUTPUT inserted.ClientID, inserted.LeadID, 1, inserted.DetailFieldID, inserted.DetailValue, @Now INTO @DetailValueHistory(ClientID, LeadID, LeadOrmatter, DetailFieldID, DetailValue, WhenSaved)
					SELECT	@ClientID, @LeadID, f.FieldID, ISNULL( -- 2020-02-12 CPS for JIRA AAG-106 | Added ISNULL()
							CASE f.FieldID
								WHEN 144268 THEN ISNULL(@PetName, '')
								WHEN 144275	THEN	CASE @Gender
														WHEN 'M' THEN '5168'
														WHEN 'F' THEN '5169'
														ELSE ''
													END
								WHEN 144272	THEN ISNULL(@Breed, '')
								WHEN 144274	THEN ISNULL(CONVERT(VARCHAR(10), @DoB, 120), '') -- PL - 2020-07-31 - AAG-EQB Change - Changed from VARCHAR to DATE
								WHEN 152783	THEN	CASE @SpayNeut
														WHEN 'true' THEN '5144'
														WHEN 'false' THEN '5145'
														ELSE ''
													END
								WHEN 144339 THEN ISNULL(CAST(@Purchase AS VARCHAR), '0.00')
								WHEN 144273 THEN ISNULL(@Colour, '')
								WHEN 175497 THEN	CASE @Vaccinations
														WHEN 'true' THEN '5144'
														ELSE '5145'
													END
								WHEN 175496 THEN	CASE @ExistingCond
														WHEN 'true' THEN '5144'
														ELSE '5145'
													END
								WHEN 170030 THEN ISNULL(@MicrochipNumber, '')
								WHEN 146215 THEN ISNULL(@VetID,'')
								WHEN 315897 THEN ISNULL(@VetID,'')
								WHEN 315898 THEN ISNULL(@VisionVetName,'')
								WHEN 313947 THEN ISNULL(@EQBVET,'')
								--WHEN 177372 THEN ISNULL(@HasMicrochip, '') /*CPS 2017-09-20 on request of AH.  Change to a dropdown field so that document rules will behave*/
								WHEN 177372 THEN	CASE @HasMicrochip
														WHEN 'true' THEN '5144'
														ELSE '5145'
													END

								WHEN 180288 THEN CONVERT(VARCHAR,@QuotePetProductID) /*GPR 2019-10-18 LPC-36*/

								ELSE ''	
							END	,'')
					FROM @LeadFields f	
									
				END
				
				-- Now create the policy and all the fields, history, payments etc.
				EXEC dbo._C00_1273_Policy_AddPolicyDetails @LeadID, @PetNumber, @Pet, @XmlRequest, @Debug
				
				SELECT @MatterID = MatterID 
				FROM Matter WITH (NOLOCK) 
				WHERE LeadID = @LeadID
				
				/*GPR 2019-03-05 for #55470 */
				EXEC _C00_SimpleValueIntoField 180232,@RuleSetID,@MatterID		
				
				-- 2019-11-04 CPS for LPC-109.  Return @PolicyNumber as an output rather than trying to read from the Lead table.  Pass in @BrandingID rather than reading from the customer record.
				EXEC _C600_PA_Policy_AssignPolicyNumber @MatterID /*CPS 2017-08-08*/, NULL, @PolicyNumber OUTPUT, @BrandingID
				
				INSERT @PolicyNumbers(PolicyNumber, MatterID) VALUES (@PolicyNumber, @MatterID)
				
				/* 2018-02-28 Mark Beaumont - If a breeder ID was passed in the Request XML then look it up in the Breeder resource list and 
						save it's resource list ID */
				DECLARE	@BreederId VARCHAR(2000) = NULL,
						@BreederRLID INT
				SELECT	@BreederId = CASE WHEN @XmlRequest.value('(BuyPolicyRequest/KeyValues/KeyValueData/Key)[1]', 'VARCHAR(2000)') = 'BreederId' 
										THEN @XmlRequest.value('(BuyPolicyRequest/KeyValues/KeyValueData/Value)[1]', 'VARCHAR(2000)')
										ELSE NULL
										END 
				IF @BreederId IS NOT NULL
				BEGIN
					SELECT		@BreederRLID = rldv.ResourceListID
					FROM		ResourceListDetailValues rldv WITH (NOLOCK)
					WHERE		rldv.DetailFieldID = 179925
								AND rldv.ClientID = @ClientID
								AND UPPER(rldv.DetailValue) = UPPER(REPLACE(@BreederId,' ',''))
					
					EXEC _C00_SimpleValueIntoField 179930, @BreederRLID, @MatterID	-- Breeder
					EXEC _C00_SimpleValueIntoField 180236, @BreederId, @MatterID	-- BreederID /*GPR 2019-03-10 Buddies Changes Slide 30*/
				END

				/*ALM 2021-03-08 FURKIN-361*/
				/*GPR 2020-09-01 lifted code from 433 to here*/ 
				/*Existing Conditions / Underwriter Questions*/
				DECLARE    @ECRLID			INT, 
						   @ECAnsID			INT,
						   @ECText			VARCHAR(MAX),
						   @ECDate			DATE,
						   @TableRowID		INT 

				DECLARE @ExistingConditions TABLE (AnswerID INT, QuestionID INT, AnswerText VARCHAR(MAX),TableRowID INT)
				INSERT INTO @ExistingConditions (AnswerID,QuestionID,AnswerText,TableRowID) 

				SELECT b.value('AnswerId[1]','varchar(2000)')  [AnswerId]
				  ,b.value('QuestionId[1]','varchar(2000)') [QuestionId]
				  ,b.value('AnswerText[1]','varchar(2000)') [AnswerText] 
				  ,0
				FROM @Pet.nodes('(PetQuote/UnderwritingList/UnderwritingAnswerItemType)') a(b)

				WHILE EXISTS (SELECT * FROM @ExistingConditions e where e.TableRowID = 0) 
				BEGIN 

					SELECT top 1 @ECRLID = ISNULL(e.QuestionID,0), @ECAnsID = ISNULL(e.AnswerID,0), @ECText = ISNULL(e.AnswerText,'')  FROM @ExistingConditions e 
					WHERE e.TableRowID = 0 
					
					EXEC TableRows_Insert   @TableRowID OUTPUT,@ClientID,@LeadID,NULL,177128,16154,1,1,NULL,NULL,NULL,NULL,NULL 

					EXEC _C00_SimpleValueIntoField    177126,@ECRLID,@TableRowID,44412
					EXEC _C00_SimpleValueIntoField    177305,@ECText,@TableRowID,44412
					EXEC _C00_SimpleValueIntoField    177306,@ECAnsID,@TableRowID,44412
									
					UPDATE @ExistingConditions 
					SET TableRowID = @TableRowID 
					WHERE QuestionID = @ECRLID
					
				END
				
			END		/* Get next pet */



			/*GPR 2020-05-06*/
			UPDATE dbo._C600_QuoteSessions
			SET BuyDate = dbo.fn_GetDate_Local(),
			BuyXml = @XmlRequest,
			CustomerID = @CustomerID
			WHERE QuoteSessionID = @QuoteSessionID


			IF @CountryID NOT IN (14) /*2020-03-25 GPR - Not Australia*/
			BEGIN
				/*JEL 25/03 start*/
				/*2018-04-04 ACE - If there is no bank details then this is a card payer, as such we need a CardTransaction stub to create the payment request.*/
				DECLARE @CardTransactionMapID INT

				/*2021-04-09 ACE added Credit Card (Braintree), Apple pay, PayPal*/
				/*2021-04-14 ACE removed Credit Card (Braintree), Apple pay, PayPal*/
				IF  @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/PaymentMethodTypeId)[1]', 'VARCHAR(200)') IN ('69931'/*, '193355', '193365', '193366'*/)
				BEGIN

					--/*If we dont have an account number we are a card payer. As such we will need the card transaction creating and returning*/
					SELECT @TotalGrossPremium = @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/TotalGrossPremium)[1]', 'NUMERIC(18,2)')
				
					SELECT @ClientPaymentGatewayID = CASE @BrandingID WHEN 153389 THEN 5  /*LnG*/
																	  WHEN 153391 THEN 3 --ELSE 5 END  /*Buddies*/  /*GPR 2018-09-06 default to 5 (L&G) for Asda*/
																	  WHEN 2000184 THEN 12 /*GPR 2021-02-02 for Conor*/
																	  WHEN 2002314 THEN 12 /*GPR 2021-02-02 for Conor*/
																	  ELSE 12 END
								
					SELECT @AmountNet = @TotalGrossPremium/i.GrossMultiplier,
						   @AmountVAT = @TotalGrossPremium - @AmountNet
					FROM Customers c WITH ( NOLOCK ) 
					INNER JOIN Matter m WITH ( NOLOCK ) ON m.CustomerID = c.CustomerID 
					INNER JOIN 	MatterDetailValues mdv WITH ( NOLOCK ) ON mdv.MatterID = m.MatterID
					INNER JOIN IPT  i WITH ( NOLOCK ) ON i.CountryID = 232 --(i.TaxName = 'UK Standard')
					WHERE mdv.DetailFieldID = 170036
					AND m.MatterID = @MatterID 	
					AND 
					(
					(mdv.ValueDate >= ISNULL(i.[From], '1999-01-01') AND mdv.ValueDate <= ISNULL(i.[To], '2099-12-31') 
					AND i.Region IS NULL AND LEFT(c.PostCode,3) NOT IN ('GY1','GY2','GY3','GY4','GY5','GY6','GY7','GY8','GY9','GY10','IM1','IM2','IM3','IM4','IM5','IM6','IM7','IM8','IM9','JE2','JE3') ) 
					OR 
					( i.Region LIKE '%' + LEFT(c.PostCode,3) + '%' )
					)
				
					IF ISNULL(@CardTransactionID,0) = 0 
					BEGIN  
						INSERT INTO CardTransaction (ClientID, CustomerID, Amount, WhenCreated, ClientPaymentGatewayID)
						VALUES (@ClientID, @CustomerID, @TotalGrossPremium, dbo.fn_GetDate_Local(),@ClientPaymentGatewayID)

						SELECT @CardTransactionID = SCOPE_IDENTITY()

						DECLARE @DataBaseName VARCHAR(100) = DB_NAME()
						IF(@DataBaseName='Aquarius603') -- TODO Change to live environment for LTP
						BEGIN
							EXEC AquariusMaster.dbo.CardTransactionMap__Insert @CardTransactionID, @ClientID, @DataBaseName, 'TSTDB1', @CardTransactionMapID OUTPUT	
						END
						ELSE
						BEGIN
							EXEC AquariusMaster.dbo.CardTransactionMap__Insert @CardTransactionID, @ClientID, @DataBaseName, 'TSTDB1', @CardTransactionMapID OUTPUT	
						END
					
					END

					/*2018-04-16 Lets have a link between policy and card transaction*/
					INSERT INTO CardTransactionPolicy ([CardTransactionID], [PAMatterID], [WhoCreated], [WhenCreated])
					SELECT @CardTransactionID, pn.MatterID, 58552, dbo.fn_GetDate_Local()
					FROM @PolicyNumbers pn

					INSERT INTO Payment (ClientID, CustomerID, PaymentDateTime, PaymentDescription, PaymentReference, PaymentNet, PaymentVAT, PaymentGross,  DateReconciled, WhoCreated, WhenCreated, WhoModified, WhenModified,PaymentTypeID, CardTransactionID)
					VALUES (@ClientID, @CustomerID, @Now, 'Card Payment', CAST(@CardTransactionID AS VARCHAR),  @AmountNet, @AmountVat, @TotalGrossPremium,   @Now,   58552, @Now, 58552,@Now,7,@CardTransactionID)		

					SELECT  @PaymentID = SCOPE_IDENTITY()	
							
					INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
					VALUES (@ClientID, @CustomerID, @Now, NULL, NULL, @Now, CAST(@CardTransactionID AS VARCHAR), @Description, @AmountNet, @AmountVat, @TotalGrossPremium, NULL,  @PaymentID, NULL, 58552, dbo.fn_GetDate_Local())		

				END
				/*JEL 25/03 end*/
			END

			/*GPR 2019-11-20 - PCL for LPC*/
			IF  @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/PaymentMethodTypeId)[1]', 'VARCHAR(200)') = '76618' /*Premium Funding*/
			BEGIN
				-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with variable
				/*Create a link between policy and PremiumFunderRequestID*/
				INSERT INTO PremiumFunderApplicationPolicy ([ClientID], [PAMatterID], [PremiumFunderRequestID], [WhoCreated], [WhenCreated])
				SELECT @ClientID, pn.MatterID, @PremiumFunderRequestID, 58552, dbo.fn_GetDate_Local()
				FROM @PolicyNumbers pn

			END

			
			SELECT	@ReimbursementSortCode = @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/ReimbursementSortCode)[1]', 'varchar(2000)'),
					@ReimbursementInstitutionNumber = @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/ReimbursementInstitutionNumber)[1]', 'varchar(2000)'),
					@ReimbursementAccountNumber = @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/ReimbursementAccountNumber)[1]', 'varchar(2000)'),
					@ReimbursementBankName = @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/ReimbursementBankName)[1]', 'varchar(2000)'),
					@ReimbursementBankAccountName = @XmlRequest.value('(BuyPolicyRequest/PaymentInfo/ReimbursementBankAccountName)[1]', 'varchar(2000)')

			
			/* PR 2020-03-04 - AAG-78 */
			/* GPR 2020-03-10 - AAG-78 - Added FriendlyName and AccountReference*/
			IF NOT IsNull(@ReimbursementSortCode, '') = '' -- has a reimbursement sort code - so store in account table
			BEGIN

				DECLARE @AffinityID INT, @ClientAccountID INT

				SELECT @AffinityID = @XmlRequest.value('(BuyPolicyRequest/BrandingId)[1]', 'int')

				SELECT @ClientAccountID = ValueInt
				FROM ResourceListDetailValues WITH (NOLOCK)
				WHERE DetailFieldID = 179950 AND ResourceListID = @AffinityID

				INSERT INTO Account (ClientID, CustomerID, AccountHolderName, FriendlyName, AccountNumber, Sortcode, Active, AccountUseID, AccountTypeID, WhoCreated, WhenCreated, WhoModified, WhenModified, ClientAccountID, InstitutionNumber)
				VALUES (@ClientID, @CustomerID, @ReimbursementBankAccountName, @ReimbursementBankAccountName, @ReimbursementAccountNumber, @ReimbursementSortCode, 1, 2, 1, @ClientPersonnelId, dbo.fn_GetDate_Local(), @ClientPersonnelId, dbo.fn_GetDate_Local(),@ClientAccountID, @ReimbursementInstitutionNumber)

				SELECT @AccountID = SCOPE_IDENTITY()

				UPDATE a
				SET Reference = CONCAT('CL',CAST(@AccountID AS VARCHAR))
				FROM Account a WITH (NOLOCK)
				WHERE a.AccountID = @AccountID

			END

			/*2021-03-29 ACE - Lets have some history, we all love history except Robin*/
			INSERT INTO DetailValueHistory ([ClientID], [DetailFieldID], [LeadOrMatter], [LeadID], [MatterID], [FieldValue], [WhenSaved], [ClientPersonnelID], [CustomerID], [CaseID])
			SELECT d.ClientID, d.DetailFieldID, d.LeadOrmatter, d.LeadID, d.MatterID, d.DetailValue, d.WhenSaved, CASE WHEN @ClientPersonnelId = -1 THEN 58552 ELSE @ClientPersonnelId END, d.CustomerID, d.CaseID
			FROM @DetailValueHistory d

			IF @Debug = 0
			BEGIN
				COMMIT
			END
			
		END TRY
		BEGIN CATCH
			IF @Debug = 0
			BEGIN
				ROLLBACK
			END
			
			DECLARE @Error VARCHAR(2000)
			SELECT @Error = ERROR_MESSAGE()
			PRINT @Error

			EXEC dbo._C00_LogIt  'Error', @ClientID, '_C00_1273_Policy_BuyPolicy', @Error, @ClientPersonnelId
			
			SELECT @Error AS ERROR

		END CATCH
		
		--DECLARE @AccountID INT = 0
		
		IF @CountryID NOT IN (14) /*2020-03-25 GPR - Not Australia*/
		BEGIN
			SELECT @AccountID = 0
			SELECT @XmlResponse =
			(
				SELECT	 pn.MatterID	 /*CPS 2017-08-17 for PR*/
						,pn.PolicyNumber
						,1 RESULT,
						@CardTransactionID AS [CardTransactionID],
						'' AS [CardTransactionMapID],
						/*JEL 25/03*/
						@CardTransactionMapID AS [CardTransactionMapID],
						@CustomerID AS [CustomerID]					
				FROM @PolicyNumbers pn
				FOR XML PATH ('Policy'), ROOT ('BuyPolicyResponse')	
			)
		END
		ELSE /*Is Australia*/
		BEGIN
			SELECT @AccountID = 0
			SELECT @XmlResponse =
			(
				SELECT	 pn.MatterID	 /*CPS 2017-08-17 for PR*/
						,pn.PolicyNumber
						,1 RESULT,
						@CardTransactionID AS [CardTransactionID],
						'' AS [CardTransactionMapID],
						/*JEL 25/03*/
						--@CardTransactionMapID AS [CardTransactionMapID],
						@CustomerID AS [CustomerID]					
				FROM @PolicyNumbers pn
				FOR XML PATH ('Policy'), ROOT ('BuyPolicyResponse')	
			)
		END

	END
	ELSE
	BEGIN
		EXEC dbo.LogXML__AddEntry @ClientID, 0, 'BuyPolicy_Validate', @XmlResponse, NULL
	END
	/*JEL 2019-02-26 Added log for complete*/ 
	EXEC dbo._C00_LogIt  'Complete', @ClientID, '_C600_Policy_BuyPolicy', 'BuyPolicy Complete', @ClientPersonnelId
	
	SELECT @XmlResponse

END















GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_BuyPolicy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_BuyPolicy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_BuyPolicy] TO [sp_executeall]
GO
