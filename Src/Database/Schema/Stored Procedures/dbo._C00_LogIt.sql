SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2009-06-30
-- Description:	Common routine for adding entries to the Logs table
-- SLACKY-PERF-20190621- Reduce Load on Logs INSERTS
-- =============================================
CREATE PROCEDURE [dbo].[_C00_LogIt]
	@TypeOfLogEntry varchar (6), 
	@ClassName varchar (512), 
	@MethodName varchar (512), 
	@LogEntry varchar (1024), 
	@ClientPersonnelID int 
AS
BEGIN
	SET NOCOUNT ON;


	-- SLACKY-PERF-20190621- Reduce Load on LogXML INSERTS
	--IF DB_NAME() <> 'Aquarius604Dev' /*GPR 2020-02-24 - removed for 604 Dev - In DEV I want to see this detail*/
	--BEGIN
	--IF	
	--	@ClassName LIKE '%_C%_PA_Policy_GetProducts%' OR
	--	@ClassName LIKE '%_C%_PA_Policy_GetCalculatedValues%' OR
	--	@ClassName LIKE '%_C%_PA_Policy_GetQuoteValues%' OR
	--	@ClassName LIKE '%QuoteApi-GetQuoteValues%' OR
	--	@ClassName LIKE '%_C%_PA_Policy_SaveQuote%' OR
	--	@ClassName LIKE '%RulesEngine_Evaluate%'
	--BEGIN
	--	RETURN;
	--END
	--END


	DECLARE @LogID INT

	/*
		@TypeOfLogEntry should be one of:
			'Debug'
			'Error'
			'Info'
			'Unauth'
			
		@ClassName is normally for C# objects within the app. 
			If you are calling this from a stored proc then use the procname instead.
			Example: 'AquariumHousekeeping'
			
		@MethodName is normally for C# objects within the app. 
			If you are calling this from a stored proc then use a descriptive name
			for the block of code that you are attempting.
			Example: 'Remove old LeadViewHistory information'
			
		@LogEntry holds the message text (good or bad).
			Examples: 'Beginning daily sweep'
			          'Sweep failed: timeout expired'
					  'Sweep completed successfully'
		
		@ClientPersonnelID represents the person running the code/batch/stored proc.
			Use zero for housekeeping tasks and other anonymous actions.
	*/
	
	INSERT INTO [dbo].[Logs]
	(
		LogDateTime, 
		TypeOfLogEntry, 
		ClassName, 
		MethodName, 
		LogEntry, 
		ClientPersonnelID
	)
	SELECT 
		dbo.fn_GetDate_Local(), 
		@TypeOfLogEntry, 
		@ClassName, 
		@MethodName, 
		@LogEntry, 
		@ClientPersonnelID 
		
	SELECT @LogID = SCOPE_IDENTITY() /*CS 2014-12-16*/
	RETURN @LogID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LogIt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_LogIt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LogIt] TO [sp_executeall]
GO
