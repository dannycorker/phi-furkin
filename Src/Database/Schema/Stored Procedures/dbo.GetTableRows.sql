SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Jim Green
-- Create date: 2008-06-26
-- Description:	Get Table Rows for the app to pivot, filter and display
-- =============================================
CREATE PROCEDURE [dbo].[GetTableRows]
	@ClientID int,
	@TableRowDetailFieldPageID int,
	@TableRowDetailFieldID int,
	@LeadID int = null,
	@MatterID int = null,
	@MatterIDList varchar(50) = null
AS
BEGIN
	SET NOCOUNT ON;

	/* 
		Return all table fields, with resource lists exploded into all their component fields as well. 
		There is guaranteed to be exactly 1 resource list within a table. 
		Turn all dates round into dd-mm-yyyy format ready for display.
	*/
	
	WITH AllRelevantResourceListsByTable AS
	(
		SELECT DISTINCT tr.TableRowID, tdv.ResourceListID 
		FROM tablerows tr
		INNER JOIN tabledetailvalues tdv ON tdv.tablerowid = tr.tablerowid 
		WHERE tr.leadid = @LeadID 
		AND tr.DetailFieldID = @TableRowDetailFieldID
		AND tdv.resourcelistid IS NOT NULL
	),
	AllRelevantResourceLists AS
	(
		SELECT DISTINCT ar.ResourceListID 
		FROM AllRelevantResourceListsByTable ar
	),
	AllResourceDetailFields AS 
	(	
		SELECT DISTINCT(rldv.DetailFieldID)
		FROM AllRelevantResourceLists arrl 
		INNER JOIN ResourceListDetailValues rldv ON rldv.ResourceListID = arrl.ResourceListID
	),
	AllResourceDetailValues AS 
	(	
		SELECT rldv.ResourceListID, rldv.DetailFieldID, rldv.DetailValue, rldv.EncryptedValue
		FROM AllRelevantResourceLists arrl 
		INNER JOIN ResourceListDetailValues rldv ON rldv.ResourceListID = arrl.ResourceListID
	)
	SELECT tr.LeadID, tr.MatterID, tdv.TableDetailValueID, tr.TableRowID, af.DetailFieldID as [DetailFieldID],
	tdv.DetailValue as [tdvDetailValue], ardv.ResourceListID as [ResourceListID], tdv.EncryptedValue as [tdvEncryptedDetailValue], 
	tdv_df.FieldName, tdv_df.FieldCaption, tdv_df.QuestionTypeID, 
	tdv_df.Enabled, tdv_df.EquationText, tdv_df.Editable, 
	ardv.DetailValue as [rldvDetailValue], ardv.EncryptedValue as [rldvEncryptedDetailValue], 
	tdv_df.Encrypt, tdv_df.LeadTypeID 
	FROM TableRows tr 
	CROSS JOIN AllResourceDetailFields af 
	LEFT JOIN TableDetailValues tdv ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = af.DetailFieldID
	LEFT JOIN DetailFields tdv_df ON tdv_df.DetailFieldID = af.DetailFieldID 
	LEFT JOIN AllRelevantResourceListsByTable arrl ON arrl.TableRowID = tr.TableRowID 
	LEFT JOIN AllResourceDetailValues ardv ON ardv.ResourceListID = arrl.ResourceListID AND ardv.DetailFieldID = af.DetailFieldID 
	WHERE tr.ClientID = @ClientID 
	AND tr.DetailFieldPageID = @TableRowDetailFieldPageID 
	AND tr.DetailFieldID = @TableRowDetailFieldID 
	AND (@LeadID IS NULL OR tr.LeadID = @LeadID) 
	AND 
	(
		@MatterIDList IS NOT NULL AND tr.MatterID IN (SELECT * FROM dbo.fnTableOfIDsFromCSV (@MatterIDList)) 
			OR
		((@MatterIDList IS NULL AND @MatterID IS NULL) OR tr.MatterID = @MatterID) 
	)

	UNION

	SELECT tr.LeadID, tr.MatterID, tdv.TableDetailValueID, tr.TableRowID, tdv.DetailFieldID as [DetailFieldID],
	tdv.DetailValue as [tdvDetailValue], NULL as [ResourceListID], tdv.EncryptedValue as [tdvEncryptedDetailValue], 
	tdv_df.FieldName, tdv_df.FieldCaption, tdv_df.QuestionTypeID, 
	tdv_df.Enabled, tdv_df.EquationText, tdv_df.Editable, 
	NULL as [rldvDetailValue], NULL as [rldvEncryptedDetailValue], 
	tdv_df.Encrypt, tdv_df.LeadTypeID 
	FROM TableRows tr 
	INNER JOIN TableDetailValues tdv ON tdv.TableRowID = tr.TableRowID 
	INNER JOIN DetailFields tdv_df ON tdv_df.DetailFieldID = tdv.DetailFieldID 
	WHERE tr.ClientID = @ClientID 
	AND tr.DetailFieldPageID = @TableRowDetailFieldPageID 
	AND tr.DetailFieldID = @TableRowDetailFieldID 
	AND (@LeadID IS NULL OR tr.LeadID = @LeadID) 
	AND 
	(
		@MatterIDList IS NOT NULL AND tr.MatterID IN (SELECT * FROM dbo.fnTableOfIDsFromCSV (@MatterIDList)) 
			OR
		((@MatterIDList IS NULL AND @MatterID IS NULL) OR tr.MatterID = @MatterID) 
	)
	AND tdv.DetailFieldID NOT IN (SELECT af.DetailFieldID FROM AllResourceDetailFields af)

	ORDER BY tr.LeadID, tr.MatterID, tr.TableRowID, [DetailFieldID], [ResourceListID] 
END







GO
GRANT VIEW DEFINITION ON  [dbo].[GetTableRows] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetTableRows] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetTableRows] TO [sp_executeall]
GO
