SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the GroupRightsDynamic table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupRightsDynamic_GetByGroupRightsDynamicID]
(

	@GroupRightsDynamicID int   
)
AS


				SELECT
					[GroupRightsDynamicID],
					[ClientPersonnelAdminGroupID],
					[FunctionTypeID],
					[LeadTypeID],
					[ObjectID],
					[RightID]
				FROM
					[dbo].[GroupRightsDynamic] WITH (NOLOCK) 
				WHERE
										[GroupRightsDynamicID] = @GroupRightsDynamicID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamic_GetByGroupRightsDynamicID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupRightsDynamic_GetByGroupRightsDynamicID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamic_GetByGroupRightsDynamicID] TO [sp_executeall]
GO
