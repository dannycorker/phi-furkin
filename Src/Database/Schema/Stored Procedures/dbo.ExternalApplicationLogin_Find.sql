SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ExternalApplicationLogin table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ExternalApplicationLogin_Find]
(

	@SearchUsingOR bit   = null ,

	@ExternalApplicationLoginID int   = null ,

	@ThirdPartySystemID int   = null ,

	@ClientID int   = null ,

	@SubClientID int   = null ,

	@UserName varchar (255)  = null ,

	@Password varchar (65)  = null ,

	@Salt varchar (50)  = null ,

	@ClientPersonnelID int   = null ,

	@ClientPersonnelAdminGroupID int   = null ,

	@AttemptedLogins int   = null ,

	@AccountDisabled bit   = null ,

	@CustomerID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ExternalApplicationLoginID]
	, [ThirdPartySystemID]
	, [ClientID]
	, [SubClientID]
	, [UserName]
	, [Password]
	, [Salt]
	, [ClientPersonnelID]
	, [ClientPersonnelAdminGroupID]
	, [AttemptedLogins]
	, [AccountDisabled]
	, [CustomerID]
    FROM
	[dbo].[ExternalApplicationLogin] WITH (NOLOCK) 
    WHERE 
	 ([ExternalApplicationLoginID] = @ExternalApplicationLoginID OR @ExternalApplicationLoginID IS NULL)
	AND ([ThirdPartySystemID] = @ThirdPartySystemID OR @ThirdPartySystemID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SubClientID] = @SubClientID OR @SubClientID IS NULL)
	AND ([UserName] = @UserName OR @UserName IS NULL)
	AND ([Password] = @Password OR @Password IS NULL)
	AND ([Salt] = @Salt OR @Salt IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID OR @ClientPersonnelAdminGroupID IS NULL)
	AND ([AttemptedLogins] = @AttemptedLogins OR @AttemptedLogins IS NULL)
	AND ([AccountDisabled] = @AccountDisabled OR @AccountDisabled IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ExternalApplicationLoginID]
	, [ThirdPartySystemID]
	, [ClientID]
	, [SubClientID]
	, [UserName]
	, [Password]
	, [Salt]
	, [ClientPersonnelID]
	, [ClientPersonnelAdminGroupID]
	, [AttemptedLogins]
	, [AccountDisabled]
	, [CustomerID]
    FROM
	[dbo].[ExternalApplicationLogin] WITH (NOLOCK) 
    WHERE 
	 ([ExternalApplicationLoginID] = @ExternalApplicationLoginID AND @ExternalApplicationLoginID is not null)
	OR ([ThirdPartySystemID] = @ThirdPartySystemID AND @ThirdPartySystemID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SubClientID] = @SubClientID AND @SubClientID is not null)
	OR ([UserName] = @UserName AND @UserName is not null)
	OR ([Password] = @Password AND @Password is not null)
	OR ([Salt] = @Salt AND @Salt is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID AND @ClientPersonnelAdminGroupID is not null)
	OR ([AttemptedLogins] = @AttemptedLogins AND @AttemptedLogins is not null)
	OR ([AccountDisabled] = @AccountDisabled AND @AccountDisabled is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ExternalApplicationLogin_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ExternalApplicationLogin_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ExternalApplicationLogin_Find] TO [sp_executeall]
GO
