SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-05-12
-- Description:	LeadDocumentFS Filestream conversion of existing LeadDocument records
-- =============================================
CREATE PROCEDURE [dbo].[LeadDocumentFSConversion]
	@ClientID int = null
AS
BEGIN
	SET NOCOUNT ON;

	SELECT convert(varchar, dateadd(ms, 1, dbo.fn_GetDate_Local()), 120), '  Start for ClientID ' + CAST(@ClientID AS VARCHAR)

	IF EXISTS (SELECT * FROM dbo.ClientHSKOptions cho WITH (NOLOCK) WHERE cho.ClientID = @ClientID)
	BEGIN
		UPDATE dbo.ClientHSKOptions 
		SET UseLeadDocumentFS = 1 
		WHERE ClientID = @ClientID	
	END
	ELSE
	BEGIN
		INSERT INTO dbo.ClientHSKOptions (ClientID, IncludeLETCCleanUp, IncludeFollowupDateCleanUp, IncludeMobileCleanUp, UseLeadDocumentFS)
		VALUES (@ClientID, 0, 0, 0, 1)
	END
	
	DECLARE @LoopCount int = 0, 
	@LoopMax int = 100 
	
	DECLARE @DocsToDo TABLE (LeadDocumentID int)
	
	INSERT INTO @DocsToDo (LeadDocumentID) 
	SELECT TOP (@LoopMax) ld.LeadDocumentID 
	FROM dbo.LeadDocument ld WITH (NOLOCK) 
	WHERE ld.ClientID = @ClientID 
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.LeadDocumentFS fs WITH (NOLOCK) 
		WHERE fs.LeadDocumentID	= ld.LeadDocumentID 
	)

	WHILE EXISTS (SELECT * FROM @DocsToDo)
	BEGIN
		
		INSERT INTO [dbo].[LeadDocumentFS]
			([LeadDocumentID]
			,[ClientID]
			,[LeadID]
			,[DocumentTypeID]
			,[LeadDocumentTitle]
			,[UploadDateTime]
			,[WhoUploaded]
			,[DocumentBLOB]
			,[FileName]
			,[EmailBLOB]
			,[DocumentFormat]
			,[EmailFrom]
			,[EmailTo]
			,[CcList]
			,[BccList]
			,[ElectronicSignatureDocumentKey]
			,[Encoding]
			,[ContentFormat]
			,[ZipFormat]
			)
		SELECT
			ld.LeadDocumentID
			,ClientID
			,LeadID
			,DocumentTypeID
			,LeadDocumentTitle
			,UploadDateTime
			,WhoUploaded
			,DocumentBLOB
			,FileName
			,EmailBLOB
			,DocumentFormat
			,EmailFrom
			,EmailTo
			,CcList
			,BccList
			,ElectronicSignatureDocumentKey
			,Encoding
			,ContentFormat
			,ZipFormat
		FROM @DocsToDo dtd 
		INNER JOIN dbo.LeadDocument ld ON ld.LeadDocumentID = dtd.LeadDocumentID 

		UPDATE
			[dbo].[LeadDocument]
		SET
			 [DocumentBLOB] = CAST('' AS varbinary)
			,[EmailBLOB] = CAST('' AS varbinary)
			,[EmailFrom] = NULL
			,[EmailTo] = NULL
			,[CcList] = NULL
			,[BccList] = NULL
			,[ElectronicSignatureDocumentKey] = NULL
			,[Encoding] = NULL
			,[ContentFormat] = NULL
			,[ZipFormat] = NULL
		FROM @DocsToDo dtd 
		INNER JOIN dbo.LeadDocument ld ON ld.LeadDocumentID = dtd.LeadDocumentID 
		
		DELETE @DocsToDo
		
		INSERT INTO @DocsToDo (LeadDocumentID) 
		SELECT TOP (@LoopMax) ld.LeadDocumentID 
		FROM dbo.LeadDocument ld WITH (NOLOCK) 
		WHERE ld.ClientID = @ClientID 
		AND NOT EXISTS (
			SELECT * 
			FROM dbo.LeadDocumentFS fs WITH (NOLOCK) 
			WHERE fs.LeadDocumentID	= ld.LeadDocumentID 
		)


	END
	
	SELECT convert(varchar, dateadd(ms, 1, dbo.fn_GetDate_Local()), 120), ' Finished'


END
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentFSConversion] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocumentFSConversion] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentFSConversion] TO [sp_executeall]
GO
