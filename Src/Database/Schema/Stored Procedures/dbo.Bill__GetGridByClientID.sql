SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Bill table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Bill__GetGridByClientID]
(

	@ClientID int   
)
AS
SET ANSI_NULLS OFF
				
				SELECT
					b.[BillID],
					[Reference],
					b.[Description],
					[CreatedDate],
					[DiaryAppointmentID],
					[Fullname],
					bs.[Name] as [StatusName],
					sum(bi.Total) as [Total]
				FROM 
					[dbo].[Bill] b
				LEFT JOIN 
					[dbo].[Customers] c on b.[CustomerId] = c.[CustomerID]
				INNER JOIN 
					[dbo].[BillStatus] bs on b.[BillStatusID] = bs.[BillStatusID]
				LEFT JOIN 
					[dbo].[BillItem] bi on b.[BillID] = bi.[BillID]
				WHERE
					b.[ClientID] = @ClientID
				GROUP BY b.BillID, [Reference],b.[Description],[CreatedDate],[DiaryAppointmentID],[Fullname],bs.[Name]
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON






GO
GRANT VIEW DEFINITION ON  [dbo].[Bill__GetGridByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Bill__GetGridByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Bill__GetGridByClientID] TO [sp_executeall]
GO
