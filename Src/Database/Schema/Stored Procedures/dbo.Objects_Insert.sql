SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Objects table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Objects_Insert]
(

	@ObjectID int    OUTPUT,

	@ClientID int   ,

	@SubClientID int   ,

	@ObjectTypeID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@Name varchar (250)  ,

	@Details varchar (2000)  ,

	@SourceID int   
)
AS


				
				INSERT INTO [dbo].[Objects]
					(
					[ClientID]
					,[SubClientID]
					,[ObjectTypeID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[Name]
					,[Details]
					,[SourceID]
					)
				VALUES
					(
					@ClientID
					,@SubClientID
					,@ObjectTypeID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@Name
					,@Details
					,@SourceID
					)
				-- Get the identity value
				SET @ObjectID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Objects_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Objects_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Objects_Insert] TO [sp_executeall]
GO
