SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DiaryAppointment table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DiaryAppointment_Update]
(

	@DiaryAppointmentID int   ,

	@ClientID int   ,

	@DiaryAppointmentTitle varchar (250)  ,

	@DiaryAppointmentText varchar (2000)  ,

	@CreatedBy int   ,

	@DueDate datetime   ,

	@EndDate datetime   ,

	@AllDayEvent bit   ,

	@Completed bit   ,

	@LeadID int   ,

	@CaseID int   ,

	@CustomerID int   ,

	@Version int   ,

	@ExportVersion int   ,

	@RecurrenceInfo varchar (MAX)  ,

	@DiaryAppointmentEventType int   ,

	@ResourceInfo varchar (MAX)  ,

	@TempReminderTimeshiftID int   ,

	@StatusID int   ,

	@LabelID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DiaryAppointment]
				SET
					[ClientID] = @ClientID
					,[DiaryAppointmentTitle] = @DiaryAppointmentTitle
					,[DiaryAppointmentText] = @DiaryAppointmentText
					,[CreatedBy] = @CreatedBy
					,[DueDate] = @DueDate
					,[EndDate] = @EndDate
					,[AllDayEvent] = @AllDayEvent
					,[Completed] = @Completed
					,[LeadID] = @LeadID
					,[CaseID] = @CaseID
					,[CustomerID] = @CustomerID
					,[Version] = @Version
					,[ExportVersion] = @ExportVersion
					,[RecurrenceInfo] = @RecurrenceInfo
					,[DiaryAppointmentEventType] = @DiaryAppointmentEventType
					,[ResourceInfo] = @ResourceInfo
					,[TempReminderTimeshiftID] = @TempReminderTimeshiftID
					,[StatusID] = @StatusID
					,[LabelID] = @LabelID
				WHERE
[DiaryAppointmentID] = @DiaryAppointmentID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryAppointment_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment_Update] TO [sp_executeall]
GO
