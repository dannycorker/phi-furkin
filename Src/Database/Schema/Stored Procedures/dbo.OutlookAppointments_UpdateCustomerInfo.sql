SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 21-03-2014
-- Description:	Updates the DiaryAppointment with the customer information
-- =============================================
CREATE PROCEDURE [dbo].[OutlookAppointments_UpdateCustomerInfo]

	@ClientID INT,
	@DiaryAppointmentID INT,
	@EmailAddress VARCHAR(255)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @CustomerID INT,@LeadID INT, @CaseID INT

	SELECT TOP 1 @CustomerID = CustomerID 
	FROM Customers WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND EmailAddress = @EmailAddress
	
	IF @CustomerID IS NOT NULL
	BEGIN
	
		SELECT TOP 1 @LeadID = LeadID FROM Lead WHERE ClientID=2 AND CustomerID=@CustomerID
		SELECT TOP 1 @CaseID = CaseID FROM Cases WHERE ClientID=2 AND LeadID=@LeadID
	
		UPDATE DiaryAppointment
		SET CustomerID=@CustomerID, LeadID = @LeadID, CaseID=@CaseID
		WHERE DiaryAppointmentID=@DiaryAppointmentID		
	
	END
	

END


GO
GRANT VIEW DEFINITION ON  [dbo].[OutlookAppointments_UpdateCustomerInfo] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutlookAppointments_UpdateCustomerInfo] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutlookAppointments_UpdateCustomerInfo] TO [sp_executeall]
GO
