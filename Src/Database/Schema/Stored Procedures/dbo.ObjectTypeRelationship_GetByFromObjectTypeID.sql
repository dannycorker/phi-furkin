SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ObjectTypeRelationship table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectTypeRelationship_GetByFromObjectTypeID]
(

	@FromObjectTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ObjectTypeRelationshipID],
					[FromObjectTypeID],
					[ToObjectTypeID],
					[RelationshipTypeID],
					[RelationshipName]
				FROM
					[dbo].[ObjectTypeRelationship] WITH (NOLOCK) 
				WHERE
					[FromObjectTypeID] = @FromObjectTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectTypeRelationship_GetByFromObjectTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectTypeRelationship_GetByFromObjectTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectTypeRelationship_GetByFromObjectTypeID] TO [sp_executeall]
GO
