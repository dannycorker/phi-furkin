SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the HyperlinkDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkDetailFieldTarget__DeleteByHyperlinkFieldID]
(

	@HyperlinkDetailFieldID int   
)
AS


				DELETE FROM [dbo].[HyperlinkDetailFieldTarget] WITH (ROWLOCK) 
				WHERE
					[HyperlinkDetailFieldID] = @HyperlinkDetailFieldID
			



GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkDetailFieldTarget__DeleteByHyperlinkFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkDetailFieldTarget__DeleteByHyperlinkFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkDetailFieldTarget__DeleteByHyperlinkFieldID] TO [sp_executeall]
GO
