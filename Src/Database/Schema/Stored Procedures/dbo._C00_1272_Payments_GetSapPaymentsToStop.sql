SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- ============================================================
-- Author:		Simon Brushett
-- Create date: 2012-11-07
-- Description:	Provides a list of SAP payments for the user to select one to stop or return.
-- Used:		DF 159560
-- ============================================================

CREATE PROCEDURE [dbo].[_C00_1272_Payments_GetSapPaymentsToStop]
(
	@MatterID INT
)

AS
BEGIN

	SELECT r.TableRowID, tdvPayee.DetailValue AS Payee, tdvAmount.ValueMoney AS Amount, tdvSent.ValueDate AS DateSent
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvPayee WITH (NOLOCK) ON r.TableRowID = tdvPayee.TableRowID AND tdvPayee.DetailFieldID = 154490
	INNER JOIN dbo.TableDetailValues tdvAmount WITH (NOLOCK) ON r.TableRowID = tdvAmount.TableRowID AND tdvAmount.DetailFieldID = 154518
	INNER JOIN dbo.TableDetailValues tdvSent WITH (NOLOCK) ON r.TableRowID = tdvSent.TableRowID AND tdvSent.DetailFieldID = 154521
	LEFT JOIN dbo.TableDetailValues tdvStopped WITH (NOLOCK) ON r.TableRowID = tdvStopped.TableRowID AND tdvStopped.DetailFieldID = 159289
	INNER JOIN dbo.TableDetailValues tdvType WITH (NOLOCK) ON r.TableRowID = tdvType.TableRowID AND tdvType.DetailFieldID = 154517
	WHERE r.MatterID = @MatterID
	AND tdvSent.ValueDate IS NOT NULL -- Sent to SAP
	AND (tdvStopped.DetailValue IS NULL OR tdvStopped.DetailValue = '') -- Not stopped
	AND tdvType.DetailValue = '0' -- Payments

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Payments_GetSapPaymentsToStop] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Payments_GetSapPaymentsToStop] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Payments_GetSapPaymentsToStop] TO [sp_executeall]
GO
