SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Rights table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Rights_Insert]
(

	@RightID int   ,

	@RightName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[Rights]
					(
					[RightID]
					,[RightName]
					)
				VALUES
					(
					@RightID
					,@RightName
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Rights_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Rights_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Rights_Insert] TO [sp_executeall]
GO
