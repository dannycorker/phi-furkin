SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the IncendiaConfigurationClient table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncendiaConfigurationClient_GetByAquariumClientID]
(

	@AquariumClientID int   
)
AS


				SELECT
					[IncendiaConfigurationClientID],
					[AquariumClientID],
					[Username],
					[Password],
					[URI]
				FROM
					[dbo].[IncendiaConfigurationClient] WITH (NOLOCK) 
				WHERE
										[AquariumClientID] = @AquariumClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationClient_GetByAquariumClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncendiaConfigurationClient_GetByAquariumClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationClient_GetByAquariumClientID] TO [sp_executeall]
GO
