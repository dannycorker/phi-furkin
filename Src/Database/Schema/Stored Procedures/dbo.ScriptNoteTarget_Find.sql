SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ScriptNoteTarget table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptNoteTarget_Find]
(

	@SearchUsingOR bit   = null ,

	@ScriptNoteTargetID int   = null ,

	@ScriptID int   = null ,

	@ClientID int   = null ,

	@NoteTypeID int   = null ,

	@PriorityID int   = null ,

	@ApplyToAllCases bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ScriptNoteTargetID]
	, [ScriptID]
	, [ClientID]
	, [NoteTypeID]
	, [PriorityID]
	, [ApplyToAllCases]
    FROM
	[dbo].[ScriptNoteTarget] WITH (NOLOCK) 
    WHERE 
	 ([ScriptNoteTargetID] = @ScriptNoteTargetID OR @ScriptNoteTargetID IS NULL)
	AND ([ScriptID] = @ScriptID OR @ScriptID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([NoteTypeID] = @NoteTypeID OR @NoteTypeID IS NULL)
	AND ([PriorityID] = @PriorityID OR @PriorityID IS NULL)
	AND ([ApplyToAllCases] = @ApplyToAllCases OR @ApplyToAllCases IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ScriptNoteTargetID]
	, [ScriptID]
	, [ClientID]
	, [NoteTypeID]
	, [PriorityID]
	, [ApplyToAllCases]
    FROM
	[dbo].[ScriptNoteTarget] WITH (NOLOCK) 
    WHERE 
	 ([ScriptNoteTargetID] = @ScriptNoteTargetID AND @ScriptNoteTargetID is not null)
	OR ([ScriptID] = @ScriptID AND @ScriptID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([NoteTypeID] = @NoteTypeID AND @NoteTypeID is not null)
	OR ([PriorityID] = @PriorityID AND @PriorityID is not null)
	OR ([ApplyToAllCases] = @ApplyToAllCases AND @ApplyToAllCases is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptNoteTarget_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptNoteTarget_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptNoteTarget_Find] TO [sp_executeall]
GO
