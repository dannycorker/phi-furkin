SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Functions table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Functions_GetByFunctionID]
(

	@FunctionID int   
)
AS


				SELECT
					[FunctionID],
					[FunctionName],
					[FunctionDescription],
					[FunctionTypeID],
					[ModuleID],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[Functions] WITH (NOLOCK) 
				WHERE
										[FunctionID] = @FunctionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Functions_GetByFunctionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Functions_GetByFunctionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Functions_GetByFunctionID] TO [sp_executeall]
GO
