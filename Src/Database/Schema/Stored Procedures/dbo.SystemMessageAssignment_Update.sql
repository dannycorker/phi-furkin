SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SystemMessageAssignment table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SystemMessageAssignment_Update]
(

	@SystemMessageAssignmentID int   ,

	@ClientID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@ClientPersonnelID int   ,

	@SystemMessageID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SystemMessageAssignment]
				SET
					[ClientID] = @ClientID
					,[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
					,[ClientPersonnelID] = @ClientPersonnelID
					,[SystemMessageID] = @SystemMessageID
				WHERE
[SystemMessageAssignmentID] = @SystemMessageAssignmentID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessageAssignment_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SystemMessageAssignment_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessageAssignment_Update] TO [sp_executeall]
GO
