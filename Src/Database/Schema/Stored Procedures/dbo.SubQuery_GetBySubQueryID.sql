SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SubQuery table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubQuery_GetBySubQueryID]
(

	@SubQueryID int   
)
AS


				SELECT
					[SubQueryID],
					[ClientID],
					[SubQueryName],
					[SubQueryDesc],
					[SubQueryText]
				FROM
					[dbo].[SubQuery] WITH (NOLOCK) 
				WHERE
										[SubQueryID] = @SubQueryID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SubQuery_GetBySubQueryID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubQuery_GetBySubQueryID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubQuery_GetBySubQueryID] TO [sp_executeall]
GO
