SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2007-10-11
-- Description:	Clear all redundant User Rights where
-- they are now identical to the parent Group Rights.
--              JWG 2009-04-20 Logging added; Deletes rewritten.
-- =============================================
CREATE PROCEDURE [dbo].[ClearRedundantUserRights]
	@GroupID int 
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO dbo.logs (LogDateTime, TypeOfLogEntry, ClassName, MethodName, LogEntry, ClientPersonnelID)
	SELECT dbo.fn_GetDate_Local(), 'Info', 'UserRights in DB', 'ClearRedundantUserRights', 'Group ' + convert(varchar, @GroupID) + ' rights have been saved. Clearing redundant user rights.' , null 
	
	INSERT INTO dbo.logs (LogDateTime, TypeOfLogEntry, ClassName, MethodName, LogEntry, ClientPersonnelID)
	SELECT dbo.fn_GetDate_Local(), 'Info', 'UserRights in DB', 'ClearRedundantUserRights', 'UserRightsDynamic: @GroupID=' + convert(varchar, @GroupID) + ' : G=' + convert(varchar, cp.ClientPersonnelAdminGroupID) + ' : F=' + convert(varchar, urd.FunctionTypeID) + ': O=' + convert(varchar, urd.ObjectID) + ': LT=' + convert(varchar, IsNull(urd.LeadTypeID, -1)) + ': R=' + convert(varchar, urd.RightID), cp.ClientPersonnelID 
	FROM dbo.ClientPersonnel cp
	INNER JOIN dbo.GroupRightsDynamic grd ON grd.ClientPersonnelAdminGroupID = cp.ClientPersonnelAdminGroupID 
	INNER JOIN dbo.UserRightsDynamic urd ON urd.ClientPersonnelID = cp.ClientPersonnelID 
	WHERE cp.ClientPersonnelAdminGroupID = @GroupID
	AND urd.FunctionTypeID = grd.FunctionTypeID
	AND urd.ObjectID = grd.ObjectID
	AND urd.RightID = grd.RightID
	AND IsNull(urd.LeadTypeID, -1) = IsNull(grd.LeadTypeID, -1)
	
	INSERT INTO dbo.logs (LogDateTime, TypeOfLogEntry, ClassName, MethodName, LogEntry, ClientPersonnelID)
	SELECT dbo.fn_GetDate_Local(), 'Info', 'UserRights in DB', 'ClearRedundantUserRights', 'UserFunctionControl: @GroupID=' + convert(varchar, @GroupID) + ' : G=' + convert(varchar, cp.ClientPersonnelAdminGroupID) + ' : F=' + convert(varchar, ufc.FunctionTypeID) + ': LT=' + convert(varchar, IsNull(ufc.LeadTypeID, -1)) + ': R=' + convert(varchar, ufc.RightID) + ' : M=' + convert(varchar, ufc.ModuleID) + ' HD=' + convert(varchar, ufc.HasDescendants), cp.ClientPersonnelID 
	FROM dbo.ClientPersonnel cp
	INNER JOIN dbo.GroupFunctionControl gfc ON gfc.ClientPersonnelAdminGroupID = cp.ClientPersonnelAdminGroupID 
	INNER JOIN dbo.UserFunctionControl ufc ON ufc.ClientPersonnelID = cp.ClientPersonnelID 
	WHERE cp.ClientPersonnelAdminGroupID = @GroupID
	AND ufc.ModuleID = gfc.ModuleID 
	AND ufc.FunctionTypeID = gfc.FunctionTypeID
	AND ufc.RightID = gfc.RightID
	AND ufc.HasDescendants = gfc.HasDescendants
	AND IsNull(ufc.LeadTypeID, -1) = IsNull(gfc.LeadTypeID, -1)
/*	
	DELETE UserRightsDynamic 
	FROM GroupRightsDynamic 
	INNER JOIN ClientPersonnel ON ClientPersonnel.ClientPersonnelAdminGroupID = GroupRightsDynamic.ClientPersonnelAdminGroupID
	WHERE UserRightsDynamic.FunctionTypeID = GroupRightsDynamic.FunctionTypeID
	AND UserRightsDynamic.ObjectID = GroupRightsDynamic.ObjectID
	AND UserRightsDynamic.RightID = GroupRightsDynamic.RightID
	AND IsNull(UserRightsDynamic.LeadTypeID, -1) = IsNull(GroupRightsDynamic.LeadTypeID, -1)
	AND GroupRightsDynamic.ClientPersonnelAdminGroupID = @GroupID

	DELETE UserFunctionControl
	FROM GroupFunctionControl 
	INNER JOIN ClientPersonnel ON ClientPersonnel.ClientPersonnelAdminGroupID = GroupFunctionControl.ClientPersonnelAdminGroupID
	WHERE UserFunctionControl.FunctionTypeID = GroupFunctionControl.FunctionTypeID
	AND UserFunctionControl.ModuleID = GroupFunctionControl.ModuleID 
	AND UserFunctionControl.FunctionTypeID = GroupFunctionControl.FunctionTypeID
	AND UserFunctionControl.RightID = GroupFunctionControl.RightID
	AND UserFunctionControl.HasDescendants = GroupFunctionControl.HasDescendants
	AND GroupFunctionControl.ClientPersonnelAdminGroupID = @GroupID
*/

	DELETE UserRightsDynamic 
	WHERE UserRightsDynamicID IN 
	(
		SELECT urd.UserRightsDynamicID
		FROM dbo.ClientPersonnel cp
		INNER JOIN dbo.GroupRightsDynamic grd ON grd.ClientPersonnelAdminGroupID = cp.ClientPersonnelAdminGroupID 
		INNER JOIN dbo.UserRightsDynamic urd ON urd.ClientPersonnelID = cp.ClientPersonnelID 
		WHERE cp.ClientPersonnelAdminGroupID = @GroupID
		AND urd.FunctionTypeID = grd.FunctionTypeID
		AND urd.ObjectID = grd.ObjectID
		AND urd.RightID = grd.RightID
		AND IsNull(urd.LeadTypeID, -1) = IsNull(grd.LeadTypeID, -1)
	)
	
	DELETE UserFunctionControl 
	WHERE UserFunctionControlID IN 
	(
		SELECT ufc.UserFunctionControlID
		FROM dbo.ClientPersonnel cp
		INNER JOIN dbo.GroupFunctionControl gfc ON gfc.ClientPersonnelAdminGroupID = cp.ClientPersonnelAdminGroupID 
		INNER JOIN dbo.UserFunctionControl ufc ON ufc.ClientPersonnelID = cp.ClientPersonnelID 
		WHERE cp.ClientPersonnelAdminGroupID = @GroupID
		AND ufc.ModuleID = gfc.ModuleID 
		AND ufc.FunctionTypeID = gfc.FunctionTypeID
		AND ufc.RightID = gfc.RightID
		AND ufc.HasDescendants = gfc.HasDescendants
		AND IsNull(ufc.LeadTypeID, -1) = IsNull(gfc.LeadTypeID, -1)
	)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClearRedundantUserRights] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClearRedundantUserRights] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClearRedundantUserRights] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[ClearRedundantUserRights] TO [sp_executehelper]
GO
