SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CustomerAnswers table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerAnswers_GetByCustomerQuestionnaireID]
(

	@CustomerQuestionnaireID int   
)
AS


				SELECT
					[CustomerAnswerID],
					[CustomerQuestionnaireID],
					[MasterQuestionID],
					[Answer],
					[QuestionPossibleAnswerID],
					[ClientID]
				FROM
					[dbo].[CustomerAnswers] WITH (NOLOCK) 
				WHERE
										[CustomerQuestionnaireID] = @CustomerQuestionnaireID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerAnswers_GetByCustomerQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerAnswers_GetByCustomerQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerAnswers_GetByCustomerQuestionnaireID] TO [sp_executeall]
GO
