SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ResourceList table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResourceList_Get_List]

AS


				
				SELECT
					[ResourceListID],
					[ClientID],
					[DetailFieldPageID],
					[ResourceListHelperCode],
					[ResourceListHelperName],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[ResourceList] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceList_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList_Get_List] TO [sp_executeall]
GO
