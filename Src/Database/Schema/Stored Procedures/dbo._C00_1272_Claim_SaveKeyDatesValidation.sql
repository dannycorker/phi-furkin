SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-03-20
-- Description:	Saves the key dates validation result
-- Used by:		DF 147708
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_SaveKeyDatesValidation] 
(
	@MatterID INT,
	@Value VARCHAR(2000)
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	IF EXISTS(SELECT * FROM  dbo.MatterDetailValues WHERE MatterID = @MatterID AND DetailFieldID = 147708)
	BEGIN
	
		UPDATE dbo.MatterDetailValues
		SET DetailValue = @Value
		WHERE MatterID = @MatterID 
		AND DetailFieldID = 147708
		AND ClientID = @ClientID
		
	END
	ELSE 
	BEGIN
	
		INSERT dbo.MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
		SELECT @ClientID, LeadID, MatterID, 147708, @Value
		FROM dbo.Matter WITH (NOLOCK) 
		WHERE MatterID = @MatterID
		AND ClientID = @ClientID
	
	END
	
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_SaveKeyDatesValidation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_SaveKeyDatesValidation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_SaveKeyDatesValidation] TO [sp_executeall]
GO
