SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DetailFieldPages table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldPages_Insert]
(

	@DetailFieldPageID int    OUTPUT,

	@ClientID int   ,

	@LeadOrMatter tinyint   ,

	@LeadTypeID int   ,

	@PageName varchar (50)  ,

	@PageCaption varchar (50)  ,

	@PageOrder int   ,

	@Enabled bit   ,

	@ResourceList bit   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@IsShared bit   
)
AS


				
				INSERT INTO [dbo].[DetailFieldPages]
					(
					[ClientID]
					,[LeadOrMatter]
					,[LeadTypeID]
					,[PageName]
					,[PageCaption]
					,[PageOrder]
					,[Enabled]
					,[ResourceList]
					,[SourceID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[IsShared]
					)
				VALUES
					(
					@ClientID
					,@LeadOrMatter
					,@LeadTypeID
					,@PageName
					,@PageCaption
					,@PageOrder
					,@Enabled
					,@ResourceList
					,@SourceID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@IsShared
					)
				-- Get the identity value
				SET @DetailFieldPageID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldPages_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages_Insert] TO [sp_executeall]
GO
