SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentDetailFieldTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentDetailFieldTarget_GetByDetailFieldID]
(

	@DetailFieldID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[DocumentDetailFieldTargetID],
					[ClientID],
					[DocumentTypeID],
					[Target],
					[DetailFieldID]
				FROM
					[dbo].[DocumentDetailFieldTarget]
				WHERE
					[DetailFieldID] = @DetailFieldID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentDetailFieldTarget_GetByDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentDetailFieldTarget_GetByDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentDetailFieldTarget_GetByDetailFieldID] TO [sp_executeall]
GO
