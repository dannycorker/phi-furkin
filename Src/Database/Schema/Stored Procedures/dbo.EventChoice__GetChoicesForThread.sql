SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Ben Crinion
-- Create date: 2008-02-07
-- Description:	Get all event threads in progress for a case
-- WARNING: This only looks at client 3 and will be deleted soon.
-- =============================================
CREATE PROCEDURE [dbo].[EventChoice__GetChoicesForThread]
	
	@CaseID			int,
	@RunAsThisUser	int,
	@FromEventID	int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientID int, @CustomerID int, @LeadID int, @DeleteStr varchar(2000), @TestEventChoiceID int

	DECLARE @PossibleEvents1 TABLE (
		FromEventID int,
		EventChoiceID int, 
		NextEventTypeID int, 
		NextEventTypeName varchar(50), 
		ThreadNumber int, 
		ChoiceDescription varchar(50),
		SqlClause varchar(2000)
	)

	DECLARE @CursorEvents TABLE (
		EventChoiceID int,
		SqlClause varchar(2000)
	) 

	DECLARE @AAResult TABLE (
		ResultCode tinyint	
	)

	-- Get the base list of event choices leading away from the current events
	INSERT INTO @PossibleEvents1(FromEventID, EventChoiceID, NextEventTypeID, NextEventTypeName, ThreadNumber, ChoiceDescription, SqlClause)
	SELECT  le.LeadEventID	  ,	
			ec.EventChoiceID  ,
			ec.NextEventTypeID,
			net.EventTypeName ,
			ec.ThreadNumber   ,
			ec.Description	  ,
			ec.SqlClauseForInclusion
	FROM    LeadEvent le
			INNER JOIN EventType et
			ON      le.EventTypeID = et.EventTypeID
			INNER JOIN EventChoice ec
			ON      ec.EventTypeID = le.EventTypeID
			INNER JOIN EventType net
			ON      net.EventTypeID = ec.NextEventTypeID
	WHERE   le.whenfollowedup IS NULL
		AND le.EventDeleted = 0
		AND et.InProcess    = 1
		AND et.Enabled      = 1
		AND le.ClientID     = 3
		AND le.CaseID       = @CaseID
		AND le.LeadEventID	= @FromEventID

	UNION

	-- Special case where there are no events for this case so far.
	-- Show all possible process-start events for this lead type.
	SELECT  NULL              AS [LeadEventID]    ,
			NULL              AS [EventChoiceID]  ,
			fet.EventTypeID   AS [NextEventTypeID],
			fet.EventTypeName AS [EventTypeName]  ,
			1                 AS [ThreadNumber]   ,
			fet.EventTypeName AS [Description]	  ,
			NULL			  AS [SqlClause]
	FROM    EventType fet
			INNER JOIN Lead l
			ON      l.LeadTypeID = fet.LeadTypeID
			INNER JOIN Cases c
			ON      c.LeadID   = l.LeadID
	WHERE   c.CaseID           = @CaseID
		AND fet.EventSubtypeID = 10
		AND NOT EXISTS
			(
					SELECT  *
					FROM    LeadEvent
					WHERE   CaseID = @CaseID
			)

	-- Now have a crack at autoadjudication!

	-- Get Parameters for parameter substitution
	SELECT	@ClientID = Lead.ClientID, 
			@CustomerID = Lead.CustomerID, 
			@LeadID = Lead.LeadID 
	FROM	Cases 
	INNER JOIN 
			Lead ON Lead.LeadID = Cases.LeadID 
	WHERE	Cases.CaseID = @CaseID 

	-- For each event type that could now be chosen in theory,
	-- work out which are not possible, and delete them from @PossibleEvents1
	

	-- Get all the potential event types into a working table we can 
	-- browse through one record at a time.
	INSERT INTO @CursorEvents(EventChoiceID, SqlClause)
	SELECT EventChoiceID, SqlClause FROM @PossibleEvents1

	SET ROWCOUNT 1

	SELECT @TestEventChoiceID = EventChoiceID, @DeleteStr = SqlClause
	FROM @CursorEvents

	SET ROWCOUNT 0

	WHILE @TestEventChoiceID > 0 
	BEGIN

		IF @DeleteStr IS NOT NULL AND rtrim(@DeleteStr) <> ''
		BEGIN
			SELECT @DeleteStr = dbo.fnSubstituteSqlParams(@DeleteStr, @RunAsThisUser, @ClientID, @CustomerID, @LeadID, @CaseID, NULL, NULL)

			SELECT @DeleteStr = 'SELECT CASE WHEN ' + @DeleteStr + ' THEN 1 ELSE 0 END '

			INSERT @AAResult
			EXEC(@DeleteStr)

			DELETE @PossibleEvents1 
			WHERE EventChoiceID = @TestEventChoiceID AND NOT EXISTS (select * FROM @AAResult WHERE ResultCode = 1)

			DELETE @AAResult
		END

		DELETE FROM @CursorEvents
		WHERE EventChoiceID = @TestEventChoiceID

		SET ROWCOUNT 1

		SELECT @TestEventChoiceID = EventChoiceID, @DeleteStr = SqlClause
		FROM @CursorEvents

		IF @@ROWCOUNT < 1
		BEGIN
			SELECT @TestEventChoiceID = -1
		END

		SET ROWCOUNT 0
	END


	SELECT  NextEventTypeID, 
			NextEventTypeName
	FROM    @PossibleEvents1


END




GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice__GetChoicesForThread] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventChoice__GetChoicesForThread] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice__GetChoicesForThread] TO [sp_executeall]
GO
