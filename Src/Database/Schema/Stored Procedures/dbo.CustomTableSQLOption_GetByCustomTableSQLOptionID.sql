SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CustomTableSQLOption table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomTableSQLOption_GetByCustomTableSQLOptionID]
(

	@CustomTableSQLOptionID int   
)
AS


				SELECT
					[CustomTableSQLOptionID],
					[ClientID],
					[DetailFieldID],
					[OptionType],
					[OptionValue]
				FROM
					[dbo].[CustomTableSQLOption] WITH (NOLOCK) 
				WHERE
										[CustomTableSQLOptionID] = @CustomTableSQLOptionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQLOption_GetByCustomTableSQLOptionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomTableSQLOption_GetByCustomTableSQLOptionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomTableSQLOption_GetByCustomTableSQLOptionID] TO [sp_executeall]
GO
