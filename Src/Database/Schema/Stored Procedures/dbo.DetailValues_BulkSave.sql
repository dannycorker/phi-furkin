SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ======================================================================
-- Author:		Simon Brushett
-- Create date: 2011-08-16
-- Description:	Inserts or updates multiple detail values at once
-- Amended    : Jan Wilson: 12/03/2012
--              Added ClientID parameter to verify that all detail fields
--              belong to the specified client
-- Added	  : PR 29-04-2013 ClientPersonnel History 
-- MODIFIED:	2014-07-21	SB	Updated to use view
-- MODIFIED:	2015-07-30	CS	Updated to handle negative int MatterDetailValues
-- ======================================================================
CREATE PROCEDURE [dbo].[DetailValues_BulkSave]
(
	@DetailValues tvpDetailValueData READONLY,
	@ClientID INT,
	@ClientPersonnelID int = NULL
)

AS
BEGIN
		
	-- Used to link the ids passed in to the new keys generated from the insert
	DECLARE @DetailValueIDs TABLE
	(
		PassedID INT,
		DetailValueID INT
	)
	
	DECLARE @NumberOfDetailFieldNotBelongingToClient INT
	
	SELECT
		@NumberOfDetailFieldNotBelongingToClient = COUNT(*)
	FROM
		@DetailValues DV
		INNER JOIN fnDetailFieldsShared(@ClientID) DF
		ON DV.DetailFieldID = DF.DetailFieldID
		AND DF.ClientID != @ClientID
	
	IF (@NumberOfDetailFieldNotBelongingToClient >= 1)
	BEGIN
		-- Return back the expected structure, but with no rows.
		SELECT PassedID, DetailValueID
		FROM @DetailValueIDs
		WHERE 1=0
	END
	
	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 1)
	BEGIN
		
		UPDATE dv 
		SET DetailValue = t.DetailValue, EncryptedValue = t.EncryptedValue
		FROM dbo.LeadDetailValues dv 
		INNER JOIN @DetailValues t ON dv.LeadDetailValueID = t.DetailValueID AND dv.ClientID = t.ClientID AND dv.LeadID = t.ObjectID
		WHERE t.DetailFieldSubType = 1 
		
		INSERT dbo.LeadDetailValues (ClientID, LeadID, DetailFieldID, DetailValue, EncryptedValue, SourceID)
		OUTPUT inserted.SourceID, inserted.LeadDetailValueID INTO @DetailValueIDs
		SELECT ClientID, ObjectID, DetailFieldID, DetailValue, EncryptedValue, DetailValueID
		FROM @DetailValues
		WHERE DetailFieldSubType = 1 
		AND DetailValueID < 1
		
		IF @ClientPersonnelID > 0 
		BEGIN
		
			INSERT INTO DetailValueHistory (ClientID, LeadOrMatter, LeadID, DetailFieldID, FieldValue, ClientPersonnelID, WhenSaved)
			SELECT df.ClientID, 1, t.ObjectID, t.DetailFieldID, t.DetailValue, @ClientPersonnelID, dbo.fn_GetDate_Local()
			FROM fnDetailFieldsShared(@ClientID) df 
			INNER JOIN @DetailValues t ON df.ClientID = t.ClientID AND t.DetailFieldID = df.DetailFieldID
			WHERE df.LeadOrMatter = 1
			AND df.MaintainHistory = 1
		
		END

	END

	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 2)
	BEGIN
		
		IF @ClientPersonnelID > 0 
		BEGIN
		
			INSERT INTO DetailValueHistory (ClientID, LeadOrMatter, LeadID, MatterID, DetailFieldID, FieldValue, ClientPersonnelID, WhenSaved)
			SELECT df.ClientID, 2, m.LeadID, t.ObjectID, t.DetailFieldID, t.DetailValue, @ClientPersonnelID, dbo.fn_GetDate_Local()
			FROM fnDetailFieldsShared(@ClientID) df 
			INNER JOIN @DetailValues t ON df.ClientID = t.ClientID AND t.DetailFieldID = df.DetailFieldID
			INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = t.ObjectID
			WHERE df.LeadOrMatter = 2
			AND df.MaintainHistory = 1
			AND NOT EXISTS (
				SELECT *
				FROM MatterDetailValues mdv WITH (NOLOCK)
				WHERE mdv.MatterID = t.ObjectID
				AND mdv.DetailFieldID = t.DetailFieldID
				AND mdv.DetailValue = t.DetailValue
			)
		
		END

		UPDATE dv 
		SET DetailValue = t.DetailValue, EncryptedValue = t.EncryptedValue
		FROM dbo.MatterDetailValues dv 
		INNER JOIN @DetailValues t ON dv.MatterDetailValueID = t.DetailValueID AND dv.ClientID = t.ClientID AND dv.MatterID = t.ObjectID
		WHERE t.DetailFieldSubType = 2 
		
		INSERT dbo.MatterDetailValues (ClientID, MatterID, LeadID, DetailFieldID, DetailValue, EncryptedValue, SourceID)
		OUTPUT inserted.SourceID, inserted.MatterDetailValueID INTO @DetailValueIDs
		SELECT dv.ClientID, ObjectID, m.LeadID, DetailFieldID, DetailValue, EncryptedValue, DetailValueID
		FROM @DetailValues dv
		INNER JOIN dbo.Matter m WITH (NOLOCK) ON dv.ObjectID = m.MatterID 
		WHERE DetailFieldSubType = 2
		--AND DetailValueID < 1
		AND DetailValueID IN(-1,0)
		
	END
	
	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 10)
	BEGIN
		
		UPDATE dv 
		SET DetailValue = t.DetailValue, EncryptedValue = t.EncryptedValue
		FROM dbo.CustomerDetailValues dv 
		INNER JOIN @DetailValues t ON dv.CustomerDetailValueID = t.DetailValueID AND dv.ClientID = t.ClientID AND dv.CustomerID = t.ObjectID
		WHERE t.DetailFieldSubType = 10
		
		INSERT dbo.CustomerDetailValues (ClientID, CustomerID, DetailFieldID, DetailValue, EncryptedValue, SourceID)
		OUTPUT inserted.SourceID, inserted.CustomerDetailValueID INTO @DetailValueIDs
		SELECT ClientID, ObjectID, DetailFieldID, DetailValue, EncryptedValue, DetailValueID
		FROM @DetailValues
		WHERE DetailFieldSubType = 10
		AND DetailValueID < 1
		
		IF @ClientPersonnelID > 0 
		BEGIN
		
			INSERT INTO DetailValueHistory (ClientID, LeadOrMatter, CustomerID, DetailFieldID, FieldValue, ClientPersonnelID, WhenSaved)
			SELECT df.ClientID, 10, t.ObjectID, t.DetailFieldID, t.DetailValue, @ClientPersonnelID, dbo.fn_GetDate_Local()
			FROM fnDetailFieldsShared(@ClientID) df 
			INNER JOIN @DetailValues t ON df.ClientID = t.ClientID AND t.DetailFieldID = df.DetailFieldID
			WHERE df.LeadOrMatter = 10
			AND df.MaintainHistory = 1
		
		END

	END

	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 11)
	BEGIN
		
		UPDATE dv 
		SET DetailValue = t.DetailValue, EncryptedValue = t.EncryptedValue
		FROM dbo.CaseDetailValues dv 
		INNER JOIN @DetailValues t ON dv.CaseDetailValueID = t.DetailValueID AND dv.ClientID = t.ClientID AND dv.CaseID = t.ObjectID
		WHERE t.DetailFieldSubType = 11
		
		INSERT dbo.CaseDetailValues (ClientID, CaseID, DetailFieldID, DetailValue, EncryptedValue, SourceID)
		OUTPUT inserted.SourceID, inserted.CaseDetailValueID INTO @DetailValueIDs
		SELECT ClientID, ObjectID, DetailFieldID, DetailValue, EncryptedValue, DetailValueID
		FROM @DetailValues
		WHERE DetailFieldSubType = 11
		AND DetailValueID < 1
		
		IF @ClientPersonnelID > 0 
		BEGIN
		
			INSERT INTO DetailValueHistory (ClientID, LeadOrMatter, CaseID, DetailFieldID, FieldValue, ClientPersonnelID, WhenSaved)
			SELECT df.ClientID, 11, t.ObjectID, t.DetailFieldID, t.DetailValue, @ClientPersonnelID, dbo.fn_GetDate_Local()
			FROM fnDetailFieldsShared(@ClientID) df 
			INNER JOIN @DetailValues t ON df.ClientID = t.ClientID AND t.DetailFieldID = df.DetailFieldID
			WHERE df.LeadOrMatter = 11
			AND df.MaintainHistory = 1
		
		END
	END

	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 12)
	BEGIN
		
		UPDATE dv 
		SET DetailValue = t.DetailValue, EncryptedValue = t.EncryptedValue
		FROM dbo.ClientDetailValues dv 
		INNER JOIN @DetailValues t ON dv.ClientDetailValueID = t.DetailValueID AND dv.ClientID = t.ClientID
		WHERE t.DetailFieldSubType = 12
		
		INSERT dbo.ClientDetailValues (ClientID, DetailFieldID, DetailValue, EncryptedValue, SourceID)
		OUTPUT inserted.SourceID, inserted.ClientDetailValueID INTO @DetailValueIDs
		SELECT ClientID, DetailFieldID, DetailValue, EncryptedValue, DetailValueID
		FROM @DetailValues
		WHERE DetailFieldSubType = 12
		AND DetailValueID < 1
		
	END

	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 13)
	BEGIN
		
		UPDATE dv 
		SET DetailValue = t.DetailValue, EncryptedValue = t.EncryptedValue
		FROM dbo.ClientPersonnelDetailValues dv 
		INNER JOIN @DetailValues t ON dv.ClientPersonnelDetailValueID = t.DetailValueID AND dv.ClientID = t.ClientID AND dv.ClientPersonnelID = t.ObjectID
		WHERE t.DetailFieldSubType = 13
		
		INSERT dbo.ClientPersonnelDetailValues (ClientID, ClientPersonnelID, DetailFieldID, DetailValue, EncryptedValue, SourceID)
		OUTPUT inserted.SourceID, inserted.ClientPersonnelDetailValueID INTO @DetailValueIDs
		SELECT ClientID, ObjectID, DetailFieldID, DetailValue, EncryptedValue, DetailValueID
		FROM @DetailValues
		WHERE DetailFieldSubType = 13
		AND DetailValueID < 1
		
	END
	
	IF EXISTS(SELECT * FROM @DetailValues WHERE DetailFieldSubType = 14)
	BEGIN
		
		UPDATE dv 
		SET DetailValue = t.DetailValue, EncryptedValue = t.EncryptedValue
		FROM dbo.ContactDetailValues dv 
		INNER JOIN @DetailValues t ON dv.ContactDetailValueID = t.DetailValueID AND dv.ClientID = t.ClientID AND dv.ContactID = t.ObjectID
		WHERE t.DetailFieldSubType = 14
		
		INSERT dbo.ContactDetailValues (ClientID, ContactID, DetailFieldID, DetailValue, EncryptedValue, SourceID)
		OUTPUT inserted.SourceID, inserted.ContactDetailValueID INTO @DetailValueIDs
		SELECT ClientID, ObjectID, DetailFieldID, DetailValue, EncryptedValue, DetailValueID
		FROM @DetailValues
		WHERE DetailFieldSubType = 14
		AND DetailValueID < 1
		
	END	
	
	SELECT PassedID, DetailValueID
	FROM @DetailValueIDs
	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValues_BulkSave] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailValues_BulkSave] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailValues_BulkSave] TO [sp_executeall]
GO
