SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.GetUnSecuredSiteMapHeaders    Script Date: 08/09/2006 12:22:42 ******/
CREATE PROCEDURE [dbo].[GetUnSecuredSiteMapHeaders] AS


SELECT     SiteMapID, Url, Title, Description, ParentID, Secured
FROM         dbo.SiteMap
WHERE     (Url IS NULL) AND ((Secured = 0) OR
                      (Secured IS NULL)) OR
                      (Url = N'""')




GO
GRANT VIEW DEFINITION ON  [dbo].[GetUnSecuredSiteMapHeaders] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetUnSecuredSiteMapHeaders] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetUnSecuredSiteMapHeaders] TO [sp_executeall]
GO
