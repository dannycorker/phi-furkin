SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-10-25
-- Description:	Insert into EventTypeAutomatedEvent
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeAutomatedEvent__Insert]
@ClientID int, 
@EventTypeID int, 
@AutomatedEventTypeID int, 
@RunAsUserID int = NULL, 
@ThreadToFollowUp int = 1, 
@InternalPriority int = 1,
@DelaySeconds int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO EventTypeAutomatedEvent (ClientID, EventTypeID, AutomatedEventTypeID, RunAsUserID, ThreadToFollowUp, InternalPriority, DelaySeconds)
	VALUES (@ClientID, @EventTypeID, @AutomatedEventTypeID, @RunAsUserID, @ThreadToFollowUp, @InternalPriority, @DelaySeconds)

END


GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedEvent__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedEvent__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedEvent__Insert] TO [sp_executeall]
GO
