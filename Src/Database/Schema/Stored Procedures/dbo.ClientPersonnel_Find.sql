SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientPersonnel table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnel_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientPersonnelID int   = null ,

	@ClientID int   = null ,

	@ClientOfficeID int   = null ,

	@TitleID int   = null ,

	@FirstName varchar (100)  = null ,

	@MiddleName varchar (100)  = null ,

	@LastName varchar (100)  = null ,

	@JobTitle varchar (100)  = null ,

	@Password varchar (65)  = null ,

	@ClientPersonnelAdminGroupID int   = null ,

	@MobileTelephone varchar (50)  = null ,

	@HomeTelephone varchar (50)  = null ,

	@OfficeTelephone varchar (50)  = null ,

	@OfficeTelephoneExtension varchar (50)  = null ,

	@EmailAddress varchar (255)  = null ,

	@ChargeOutRate money   = null ,

	@UserName varchar (201)  = null ,

	@Salt varchar (50)  = null ,

	@AttemptedLogins int   = null ,

	@AccountDisabled bit   = null ,

	@ManagerID int   = null ,

	@Initials varchar (3)  = null ,

	@LanguageID int   = null ,

	@SubClientID int   = null ,

	@ForcePasswordChangeOn datetime   = null ,

	@ThirdPartySystemId int   = null ,

	@CustomerID int   = null ,

	@IsAquarium bit   = null ,

	@AllowSmsCommandProcessing bit   = null ,

	@MemorableWord varchar (100)  = null ,

	@MemorableWordSalt varchar (65)  = null ,

	@MemorableWordAttempts int   = null ,

	@PendingActivation bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientPersonnelID]
	, [ClientID]
	, [ClientOfficeID]
	, [TitleID]
	, [FirstName]
	, [MiddleName]
	, [LastName]
	, [JobTitle]
	, [Password]
	, [ClientPersonnelAdminGroupID]
	, [MobileTelephone]
	, [HomeTelephone]
	, [OfficeTelephone]
	, [OfficeTelephoneExtension]
	, [EmailAddress]
	, [ChargeOutRate]
	, [UserName]
	, [Salt]
	, [AttemptedLogins]
	, [AccountDisabled]
	, [ManagerID]
	, [Initials]
	, [LanguageID]
	, [SubClientID]
	, [ForcePasswordChangeOn]
	, [ThirdPartySystemId]
	, [CustomerID]
	, [IsAquarium]
	, [AllowSmsCommandProcessing]
	, [MemorableWord]
	, [MemorableWordSalt]
	, [MemorableWordAttempts]
	, [PendingActivation]
    FROM
	[dbo].[ClientPersonnel] WITH (NOLOCK) 
    WHERE 
	 ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ClientOfficeID] = @ClientOfficeID OR @ClientOfficeID IS NULL)
	AND ([TitleID] = @TitleID OR @TitleID IS NULL)
	AND ([FirstName] = @FirstName OR @FirstName IS NULL)
	AND ([MiddleName] = @MiddleName OR @MiddleName IS NULL)
	AND ([LastName] = @LastName OR @LastName IS NULL)
	AND ([JobTitle] = @JobTitle OR @JobTitle IS NULL)
	AND ([Password] = @Password OR @Password IS NULL)
	AND ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID OR @ClientPersonnelAdminGroupID IS NULL)
	AND ([MobileTelephone] = @MobileTelephone OR @MobileTelephone IS NULL)
	AND ([HomeTelephone] = @HomeTelephone OR @HomeTelephone IS NULL)
	AND ([OfficeTelephone] = @OfficeTelephone OR @OfficeTelephone IS NULL)
	AND ([OfficeTelephoneExtension] = @OfficeTelephoneExtension OR @OfficeTelephoneExtension IS NULL)
	AND ([EmailAddress] = @EmailAddress OR @EmailAddress IS NULL)
	AND ([ChargeOutRate] = @ChargeOutRate OR @ChargeOutRate IS NULL)
	AND ([UserName] = @UserName OR @UserName IS NULL)
	AND ([Salt] = @Salt OR @Salt IS NULL)
	AND ([AttemptedLogins] = @AttemptedLogins OR @AttemptedLogins IS NULL)
	AND ([AccountDisabled] = @AccountDisabled OR @AccountDisabled IS NULL)
	AND ([ManagerID] = @ManagerID OR @ManagerID IS NULL)
	AND ([Initials] = @Initials OR @Initials IS NULL)
	AND ([LanguageID] = @LanguageID OR @LanguageID IS NULL)
	AND ([SubClientID] = @SubClientID OR @SubClientID IS NULL)
	AND ([ForcePasswordChangeOn] = @ForcePasswordChangeOn OR @ForcePasswordChangeOn IS NULL)
	AND ([ThirdPartySystemId] = @ThirdPartySystemId OR @ThirdPartySystemId IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([IsAquarium] = @IsAquarium OR @IsAquarium IS NULL)
	AND ([AllowSmsCommandProcessing] = @AllowSmsCommandProcessing OR @AllowSmsCommandProcessing IS NULL)
	AND ([MemorableWord] = @MemorableWord OR @MemorableWord IS NULL)
	AND ([MemorableWordSalt] = @MemorableWordSalt OR @MemorableWordSalt IS NULL)
	AND ([MemorableWordAttempts] = @MemorableWordAttempts OR @MemorableWordAttempts IS NULL)
	AND ([PendingActivation] = @PendingActivation OR @PendingActivation IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientPersonnelID]
	, [ClientID]
	, [ClientOfficeID]
	, [TitleID]
	, [FirstName]
	, [MiddleName]
	, [LastName]
	, [JobTitle]
	, [Password]
	, [ClientPersonnelAdminGroupID]
	, [MobileTelephone]
	, [HomeTelephone]
	, [OfficeTelephone]
	, [OfficeTelephoneExtension]
	, [EmailAddress]
	, [ChargeOutRate]
	, [UserName]
	, [Salt]
	, [AttemptedLogins]
	, [AccountDisabled]
	, [ManagerID]
	, [Initials]
	, [LanguageID]
	, [SubClientID]
	, [ForcePasswordChangeOn]
	, [ThirdPartySystemId]
	, [CustomerID]
	, [IsAquarium]
	, [AllowSmsCommandProcessing]
	, [MemorableWord]
	, [MemorableWordSalt]
	, [MemorableWordAttempts]
	, [PendingActivation]
    FROM
	[dbo].[ClientPersonnel] WITH (NOLOCK) 
    WHERE 
	 ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ClientOfficeID] = @ClientOfficeID AND @ClientOfficeID is not null)
	OR ([TitleID] = @TitleID AND @TitleID is not null)
	OR ([FirstName] = @FirstName AND @FirstName is not null)
	OR ([MiddleName] = @MiddleName AND @MiddleName is not null)
	OR ([LastName] = @LastName AND @LastName is not null)
	OR ([JobTitle] = @JobTitle AND @JobTitle is not null)
	OR ([Password] = @Password AND @Password is not null)
	OR ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID AND @ClientPersonnelAdminGroupID is not null)
	OR ([MobileTelephone] = @MobileTelephone AND @MobileTelephone is not null)
	OR ([HomeTelephone] = @HomeTelephone AND @HomeTelephone is not null)
	OR ([OfficeTelephone] = @OfficeTelephone AND @OfficeTelephone is not null)
	OR ([OfficeTelephoneExtension] = @OfficeTelephoneExtension AND @OfficeTelephoneExtension is not null)
	OR ([EmailAddress] = @EmailAddress AND @EmailAddress is not null)
	OR ([ChargeOutRate] = @ChargeOutRate AND @ChargeOutRate is not null)
	OR ([UserName] = @UserName AND @UserName is not null)
	OR ([Salt] = @Salt AND @Salt is not null)
	OR ([AttemptedLogins] = @AttemptedLogins AND @AttemptedLogins is not null)
	OR ([AccountDisabled] = @AccountDisabled AND @AccountDisabled is not null)
	OR ([ManagerID] = @ManagerID AND @ManagerID is not null)
	OR ([Initials] = @Initials AND @Initials is not null)
	OR ([LanguageID] = @LanguageID AND @LanguageID is not null)
	OR ([SubClientID] = @SubClientID AND @SubClientID is not null)
	OR ([ForcePasswordChangeOn] = @ForcePasswordChangeOn AND @ForcePasswordChangeOn is not null)
	OR ([ThirdPartySystemId] = @ThirdPartySystemId AND @ThirdPartySystemId is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([IsAquarium] = @IsAquarium AND @IsAquarium is not null)
	OR ([AllowSmsCommandProcessing] = @AllowSmsCommandProcessing AND @AllowSmsCommandProcessing is not null)
	OR ([MemorableWord] = @MemorableWord AND @MemorableWord is not null)
	OR ([MemorableWordSalt] = @MemorableWordSalt AND @MemorableWordSalt is not null)
	OR ([MemorableWordAttempts] = @MemorableWordAttempts AND @MemorableWordAttempts is not null)
	OR ([PendingActivation] = @PendingActivation AND @PendingActivation is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_Find] TO [sp_executeall]
GO
