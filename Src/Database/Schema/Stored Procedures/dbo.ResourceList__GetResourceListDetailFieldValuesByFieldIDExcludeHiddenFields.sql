SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 04-02-2013
-- Description:	Get the resource list data without the hidden columns
-- =============================================
Create PROCEDURE [dbo].[ResourceList__GetResourceListDetailFieldValuesByFieldIDExcludeHiddenFields]
	-- Add the parameters for the stored procedure here
	@ClientID INT,
	@FieldID INT
	
AS
BEGIN
	
	SELECT r.ResourceListID, r.DetailFieldID AS ResourceListDetailFieldID, r.ResourceListDetailValueID, 
			r.DetailValue AS ResourceListDetailValue, COALESCE(f.Encrypt, 0) AS ResourceListEncrypt, r.EncryptedValue AS ResourceListEncryptedValue,
			li.ItemValue AS ResourceListLookupListValue
	FROM 
		dbo.Detailfields p WITH (NOLOCK) 
		INNER JOIN dbo.DetailFields f WITH (NOLOCK) ON p.ResourceListDetailFieldPageID = f.DetailFieldPageID
		INNER JOIN dbo.ResourceListDetailValues r WITH (NOLOCK) ON f.DetailFieldID = r.DetailFieldID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON f.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND r.ValueInt = li.LookupListItemID
	WHERE p.DetailFieldID = @FieldID
	AND p.ClientID = @ClientID
	AND f.Enabled = 1
	AND f.Hidden = 0 
	ORDER BY r.ResourceListID, f.FieldOrder

END



GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList__GetResourceListDetailFieldValuesByFieldIDExcludeHiddenFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceList__GetResourceListDetailFieldValuesByFieldIDExcludeHiddenFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList__GetResourceListDetailFieldValuesByFieldIDExcludeHiddenFields] TO [sp_executeall]
GO
