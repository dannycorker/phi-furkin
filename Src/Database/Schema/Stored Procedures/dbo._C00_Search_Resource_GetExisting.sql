SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2019-07-09
-- Description:	Get resource search data
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Search_Resource_GetExisting]
	@ObjectID INT,
	@DetailFieldID INT,
	@SearchColumn INT,
	@AdditionalColumn INT,
	@BracketColumn INT = NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ResourceListID INT = dbo.fnGetSimpleDvAsInt(@DetailFieldID, @ObjectID)

	SELECT rldv_srch.ResourceListID, rldv_srch.DetailValue + ISNULL(' ' + rldv_add.DetailValue, '') + CASE WHEN @BracketColumn > 0 THEN ' (' + luli.ItemValue + ')' ELSE '' END AS [ResourceValue]
	FROM ResourceListDetailValues rldv_srch WITH (NOLOCK)
	LEFT JOIN ResourceListDetailValues rldv_add WITH (NOLOCK) ON rldv_add.ResourceListID = rldv_srch.ResourceListID AND rldv_add.DetailFieldID = @AdditionalColumn
	LEFT JOIN ResourceListDetailValues rldv_brCol WITH (NOLOCK) ON rldv_brCol.ResourceListID = rldv_srch.ResourceListID AND rldv_brCol.DetailFieldID = @BracketColumn
	LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = rldv_brCol.ValueInt
	WHERE rldv_srch.DetailFieldID = @SearchColumn
	AND rldv_srch.ResourceListID = @ResourceListID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Search_Resource_GetExisting] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Search_Resource_GetExisting] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Search_Resource_GetExisting] TO [sp_executeall]
GO
