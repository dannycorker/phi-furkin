SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		James Lewis
-- Create date: 2017-11-09
-- Description:	Called from internal quote and buy when WorldPay iframe is popped. Used to create the card transaction record and a linked customer 
--				so we can track the transaction
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_BuyPolicy_CreateCustomerandCardTransaction]
(
	@Title INT,
	@Firstname VARCHAR(2000), 
	@Lastname VARCHAR(2000), 
	@Email VARCHAR(2000), 
	@HomePhone VARCHAR(2000), 
	@Mobile VARCHAR(2000), 
	@Address1 VARCHAR(2000), 
	@Address2 VARCHAR(2000), 
	@TownCity VARCHAR(2000), 
	@County VARCHAR(2000),
	@Postcode VARCHAR(2000),
	@CustDoB Date ,
	@WhoCreated INT,
	@Amount DECIMAL (18,2),
	@CustomerID INT = 0,
	@BrandID INT,
	@SessionID INT 
)
AS
BEGIN

	DECLARE @CardTransactionID INT, 
			@ClientID INT = dbo.fnGetPrimaryClientID(),
			@ClientPaymentGatewayID INT ,
			@Now DATETIME = dbo.fn_GetDate_Local(),
			@LogEntry  VARCHAR(2000)
	
	/*Sage Pay*/ 
	SELECT @ClientPaymentGatewayID= 7--CASE @BrandID WHEN 153389 THEN 4 ELSE 2 END  /*Probably*/ 
	
	--Need new Order ID each time or world pay uses the original data and not the changed data
	--SELECT @CardTransactionID = s.CardTransactionID  
	--FROM _C600_QuoteSessions s with (NOLOCK) where s.QuoteSessionID = @SessionID 

	IF ISNULL(@CardTransactionID,0) <> 0 
	BEGIN 
		
		SELECT @CustomerID = ct.CustomerID FROM CardTransaction ct
		WHERE ct.CardTransactionID = @CardTransactionID
		
	END
	
	IF ISNULL(@CustomerID,0) = 0 
	BEGIN
		/*Add just a customer here so we can link the Card transaction ID to it*/ 
		INSERT dbo.Customers (ClientID, TitleID, FirstName, LastName, Address1, Address2, Town, County, PostCode, EmailAddress, HomeTelephone, MobileTelephone, IsBusiness, AquariumStatusID, Test, DateOfBirth)	
		VALUES (@ClientID, @Title, @Firstname, @Lastname, ISNULL(@Address1, ''), ISNULL(@Address2, ''), ISNULL(@TownCity, ''), ISNULL(@County, ''), ISNULL(@PostCode, ''), @Email, @HomePhone, @Mobile, 0, 2, 0, @CustDoB)
						
		SELECT @CustomerID = SCOPE_IDENTITY()
	END

	/*GPR 2019-12-18 for SDLPC-51*/
	IF @CustomerID > 1
	BEGIN

		DECLARE @Address1check VARCHAR(50)
		DECLARE @Address2check VARCHAR(50)

		SELECT @Address1check = Address1, @Address2check = Address2 FROM Customers WITH (NOLOCK) WHERE CustomerID = @CustomerID

		IF  @Address1check IS NULL AND @Address2check IS NULL /*Then we're likely a stub Customer created for PCL or WorldPay so we can perform this update outside of an MTA*/
		BEGIN
			UPDATE Customers /*GPR 2019-12-19 modified for SDLPC-64*/
			SET TitleID = @Title, DateOfBirth = @CustDoB, HomeTelephone = @HomePhone, MobileTelephone = ISNULL(@Mobile,''), Address1 = @Address1, Address2 = @Address2, Town = @TownCity, County = ISNULL(@County,'') /*GPR 2019-12-17 Added County for SDLPC-52*/, PostCode = @Postcode /*GPR 2019-12-24 for SDLPC-76*/
			FROM Customers c 
			WHERE c.CustomerID = @CustomerID 			
		END

	END

	
	DECLARE @CardTransactionMapID INT
	DECLARE @DataBaseName VARCHAR(100) = DB_NAME()

	IF ISNULL(@CardTransactionID,0) = 0 
	BEGIN 
		/*Now only write the bare essentials to the Card transaction. We will update this record when the XML response is received*/ 	
		INSERT INTO CardTransaction (ClientID, CustomerID, ClientPaymentGatewayID, PaymentTypeID, PreValidate,   Description, Amount, CurrencyID, FirstName, LastName, Address, Address2, City, StateProvince,  ZipPostal, WhoCreated, WhenCreated)	
		VALUES (@ClientID, @CustomerID, @ClientPaymentGatewayID, 7, 0,  'WP EQB Stub', @Amount, 1, @Firstname, @Lastname,  @Address1, @Address2, @TownCity, @County,  @Postcode, @WhoCreated, @Now)
		
		SELECT @CardTransactionID = SCOPE_IDENTITY()	

		
		IF(@DataBaseName='Aquarius603') -- TODO Change to live environment for LTP
		BEGIN
			EXEC [AquariusMaster].dbo.CardTransactionMap__Insert @CardTransactionID, @ClientID, @DataBaseName, 'TSTDB1', @CardTransactionMapID OUTPUT	
		END
		ELSE
		BEGIN
			EXEC [AquariusMaster].dbo.CardTransactionMap__Insert @CardTransactionID, @ClientID, @DataBaseName, 'TSTDB1', @CardTransactionMapID OUTPUT	
		END
		
		UPDATE _C600_QuoteSessions 
		SET CardTransactionID = @CardTransactionID 
		WHERE QuoteSessionID = @SessionID
			
	END
	ELSE -- card transaction exists
	BEGIN
		
		SELECT @CardTransactionMapID=m.CardTransactionMapID FROM [AquariusMaster].dbo.CardTransactionMap m WITH (NOLOCK) 
		WHERE m.CardTransactionID=@CardTransactionID AND m.ClientID=@ClientID AND m.DatabaseName = @DataBaseName


	END
		
	SELECT @LogEntry = 'CustomerID ' + CAST(@CustomerID as VARCHAR(30)) + ' CardTransactionID ' + CAST(@CardTransactionID as VARCHAR(30)) + 'SessionID ' + CAST(@SessionID as VARCHAR(30)) 
	
	exec _C00_LogIt 'Info', '_C600_BuyPolicy_CreateCustomerandCardTransaction', '_C600_BuyPolicy_CreateCustomerandCardTransaction Complete', @LogEntry, @WhoCreated
	
	SELECT @CardTransactionID as [CardTransactionID], @CardTransactionMapID [CardTransactionMapID], @CustomerID as [CustomerID]
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BuyPolicy_CreateCustomerandCardTransaction] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_BuyPolicy_CreateCustomerandCardTransaction] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BuyPolicy_CreateCustomerandCardTransaction] TO [sp_executeall]
GO
