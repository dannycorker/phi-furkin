SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2009-07-08
-- Description:	Create a new seed value for use in any of the WorkWinNNN tables
-- =============================================
CREATE PROCEDURE [dbo].[GetWorkWinSeedID] 
AS
BEGIN
	SET NOCOUNT ON;

	-- This work table only has one column (the identity column)
	-- so use the special "DEFAULT VALUES" clause to make sql server
	-- enter a new row. This creates a new int, which SCOPE_IDENTITY() then
	-- picks up and returns to the calling code.
	INSERT dbo.WorkWinSeed DEFAULT VALUES

	RETURN SCOPE_IDENTITY()
END





GO
GRANT VIEW DEFINITION ON  [dbo].[GetWorkWinSeedID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetWorkWinSeedID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetWorkWinSeedID] TO [sp_executeall]
GO
