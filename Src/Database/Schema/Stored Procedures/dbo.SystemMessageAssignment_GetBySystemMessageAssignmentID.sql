SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SystemMessageAssignment table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SystemMessageAssignment_GetBySystemMessageAssignmentID]
(

	@SystemMessageAssignmentID int   
)
AS


				SELECT
					[SystemMessageAssignmentID],
					[ClientID],
					[ClientPersonnelAdminGroupID],
					[ClientPersonnelID],
					[SystemMessageID]
				FROM
					[dbo].[SystemMessageAssignment] WITH (NOLOCK) 
				WHERE
										[SystemMessageAssignmentID] = @SystemMessageAssignmentID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessageAssignment_GetBySystemMessageAssignmentID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SystemMessageAssignment_GetBySystemMessageAssignmentID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessageAssignment_GetBySystemMessageAssignmentID] TO [sp_executeall]
GO
