SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2011-05-27
-- Description:	AI Dashboard - Imports from external systems
-- =============================================
CREATE PROCEDURE [dbo].[AIDashboard_Imports]
	@SummaryOnly bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @SummaryOnly = 1 
	BEGIN
	
		DECLARE @Data TABLE
		(
			Currency VARCHAR(50),
			Status VARCHAR(50),
			ConversionDate DATETIME
		)
		INSERT INTO @Data (Currency, Status, ConversionDate) EXEC dbo.AIDashboard_Imports 0
		
		IF EXISTS (SELECT * FROM @Data WHERE Status = 'Red')
		BEGIN
			SELECT 'Red' AS [Status]
		END
		ELSE IF EXISTS (SELECT * FROM @Data WHERE Status = 'Amber')
		BEGIN
			SELECT 'Amber' AS [Status]
		END
		ELSE
		BEGIN
			SELECT 'Green' AS [Status]
		END
		
	END
	ELSE
	BEGIN
		
		SELECT 'EUR' as [Currency], 
		CASE 
			WHEN cr.ConversionDate < dbo.fn_GetDate_Local() - 2 THEN 'Red' 
			WHEN cr.ConversionDate < dbo.fn_GetDate_Local() - 1 THEN 'Amber' 
			ELSE 'Green' 
		END as [Status], cr.ConversionDate
		FROM dbo.CurrencyRate cr WITH (NOLOCK) 
		WHERE cr.IsLatest = 1 
		AND cr.CurrencyId = 46
		
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AIDashboard_Imports] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AIDashboard_Imports] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AIDashboard_Imports] TO [sp_executeall]
GO
