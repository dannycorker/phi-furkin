SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ObjectType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectType_Update]
(

	@ObjectTypeID int   ,

	@ClientID int   ,

	@SubClientID int   ,

	@ObjectTypeName varchar (50)  ,

	@ObjectTypeDescription varchar (255)  ,

	@Enabled bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ObjectType]
				SET
					[ClientID] = @ClientID
					,[SubClientID] = @SubClientID
					,[ObjectTypeName] = @ObjectTypeName
					,[ObjectTypeDescription] = @ObjectTypeDescription
					,[Enabled] = @Enabled
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
				WHERE
[ObjectTypeID] = @ObjectTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectType_Update] TO [sp_executeall]
GO
