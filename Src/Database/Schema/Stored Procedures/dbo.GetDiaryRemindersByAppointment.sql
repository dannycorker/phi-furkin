SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------
-- Date Created: 16 August 2007

-- Created By:   Jim
-- Purpose:		 Get DiaryReminders with full user name, sorted with Creator first
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GetDiaryRemindersByAppointment]
(

	@DiaryAppointmentID int   
)
AS
	
	SELECT
		dr.DiaryReminderID,
		dr.ClientID,
		dr.DiaryAppointmentID,
		dr.ClientPersonnelID,
		dr.ReminderTimeshiftID,
		dr.ReminderDate,
		cp.UserName, 
		CASE da.CreatedBy WHEN dr.ClientPersonnelID THEN 1 ELSE 0 END AS 'Creator' 
	FROM
		DiaryReminder dr 
		INNER JOIN ClientPersonnel cp ON dr.ClientPersonnelID = cp.ClientPersonnelID 
		INNER JOIN DiaryAppointment da ON dr.DiaryAppointmentID = da.DiaryAppointmentID 
	WHERE
		dr.DiaryAppointmentID = @DiaryAppointmentID 
	ORDER BY 8 DESC, 7 ASC
	






GO
GRANT VIEW DEFINITION ON  [dbo].[GetDiaryRemindersByAppointment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetDiaryRemindersByAppointment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetDiaryRemindersByAppointment] TO [sp_executeall]
GO
