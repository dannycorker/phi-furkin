SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlQueryGrouping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryGrouping_Insert]
(

	@SqlQueryGroupingID int    OUTPUT,

	@ClientID int   ,

	@SqlQueryID int   ,

	@GroupByClause varchar (2000)  ,

	@HavingClause varchar (500)  ,

	@HavingColumnID int   ,

	@HavingCriteria1 varchar (250)  ,

	@HavingCriteria2 varchar (250)  ,

	@TempHavingColumnID int   ,

	@TempHavingID int   ,

	@TempGroupingID int   
)
AS


				
				INSERT INTO [dbo].[SqlQueryGrouping]
					(
					[ClientID]
					,[SqlQueryID]
					,[GroupByClause]
					,[HavingClause]
					,[HavingColumnID]
					,[HavingCriteria1]
					,[HavingCriteria2]
					,[TempHavingColumnID]
					,[TempHavingID]
					,[TempGroupingID]
					)
				VALUES
					(
					@ClientID
					,@SqlQueryID
					,@GroupByClause
					,@HavingClause
					,@HavingColumnID
					,@HavingCriteria1
					,@HavingCriteria2
					,@TempHavingColumnID
					,@TempHavingID
					,@TempGroupingID
					)
				-- Get the identity value
				SET @SqlQueryGroupingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryGrouping_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryGrouping_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryGrouping_Insert] TO [sp_executeall]
GO
