SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2019-11-12
-- Description:	Take an XML package representing a pet and create the lead record within this database
-- 2019-12-20 CPS for JIRA LAGCLAIM-277 | Changed from attribute- to element-centric XML for Ian
--										| Updated to recieve and process full XML package directly rather than looking up from a table
-- 2019-12-23 CPS for JIRA LAGCLAIM-282 | Added missing filter to resource list updates
-- 2020-01-07 CR updated the proc so that Yes/No responses can be handled and added as part of the Pet transfer.
-- 2020-01-07 MAB Validate input data for LAGCLAIM-290
-- 2020-01-10 MAB corrected filter for resource list updates, removed redundant mapping table column DecodeLookupListID, 
--					added ValueIfNull, reformatted mapping table definition and population for clarity.
-- 2020-01-21 ALM refactored to call _C00_BulkValidateAndSaveDetailValuesFromXml to eliminate duplicate code in the "receive" procedures
-- 2020-01-22 MAB bug fixes following testing of above changes by ALM + expanded configuration mapping to include missing fields
-- 2020-01-24 MAB for Jira LAGCLAIM-367 | added save of petRef node data into Lead.LeadRef + removed redundant @ChangeNotes which was not being saved.
-- 2020-07-14 ALM updated [dbo].[fn_C600_Harvest_Pet_JSON] to [dbo].[fn_C600_Mediate_Harvest_Pet]
-- =============================================
CREATE PROCEDURE [dbo].[_C600_API_Receive_Pet]
(
	 @XmlPayload		XML
	,@HttpMethod		VARCHAR(50)
	,@ObjectID			UNIQUEIDENTIFIER
	,@ParentObjectID	UNIQUEIDENTIFIER
	,@RunAsUserID		INT
	,@JsonResponse		NVARCHAR(MAX) OUTPUT
	,@ShowNewPet		BIT = 0
)
AS
BEGIN

	/*
	- Collect and Read the XML
	- Make sure we have enough information to proceed including a mapped CustomerID
	- Look for existing pets and determine whether we are inserting or updating
	
	INSERT
	- Create new Lead record, using existing variables
	- Collect and return the new LeadID

	UPDATE
	- Locate and update existing Lead
	*/
	DECLARE	 
			 @PetRef				VARCHAR(2000)
			,@NewCustomerID			INT
			,@NewLeadID				INT
			,@ClientID				INT				= dbo.fnGetPrimaryClientID()
			,@DateTimeCompleted		DATETIME2		= CURRENT_TIMESTAMP
			,@ErrorMessage			VARCHAR(2000)	= ''
			,@NewLeadTypeID			INT				= 1492 /*Policy Admin*/
			,@ErrorMessageSeparator	VARCHAR(3)		= ' | '

	DECLARE	@LeadFieldMapping tvpXmlToDetailFieldMap	/*XML node to Lead Detail Field Mapping*/

	/*
		XML to Detail Field Mapping Configuration Data
		----------------------------------------------
	*/

	/*LEAD Detail Field Mapping*/
	INSERT @LeadFieldMapping
		 ( DetailFieldID,	AttributeName,				XmlDataType,	DecodeResourceListDetailFieldID,	ValueIfNull ) VALUES
		 --============================================================================================================
		 ( 144268, 			'name'					,	'TEXT'	, 		NULL	, 							NULL	)	/*Pet Name*/
		,( 144274, 			'birthDate'				,	'DATE'	, 		NULL	, 							NULL	)	/*Pet Date of Birth*/
		,( 144271, 			'deathDate'				,	'DATE'	, 		NULL	, 							NULL	)	/*Date Pet Deceased or Lost*/
		,( 144275, 			'genderName'			,	'TEXT'	, 		NULL	, 							NULL	)	/*Pet Sex*/
		,( 144272, 			'breedName'				,	'TEXT'	, 		144270	, 							NULL	)	/*Pet Type - Breed*/
		,( 170030, 			'microchipNumber'		,	'TEXT'	, 		NULL	, 							NULL	)	/*Microchip number*/
		,( 144339, 			'purchasePrice'			,	'INT'	, 		NULL	, 							NULL	)	/*Pet Purchase Cost*/
		,( 146215, 			'vetId'					,	'RESOURCE', 	NULL	, 							NULL	)	/*Current Vet*/
		,( 152783, 			'neutered'				,	'BIT'	, 		NULL	, 							NULL	)	/*Pet Neutered*/
		,( 177372, 			'microchipStatus'		,	'BIT'	, 		NULL	, 							NULL	)	/*Microchip*/
		,( 175496, 			'preExistingConditions'	,	'BIT'	, 		NULL	, 							NULL	)	/*Any Existing Conditions?*/

	/*
		Match Pet to its Parent CustomerID
		----------------------------------
	*/

	/*Pick up the NewCustomerID from its GUID. The customer will have come across in a previous transaction.*/
	SELECT TOP 1 @NewCustomerID = cdv.CustomerID
	FROM CustomerDetailValues cdv WITH (NOLOCK) 
	WHERE cdv.DetailFieldID = 313784 /*API Transfer Mapping GUID*/
	AND cdv.DetailValue = CONVERT(VARCHAR(40), @ParentObjectID)

	/*Error Handling*/
	IF @NewCustomerID IS NULL
	BEGIN
		SELECT @ErrorMessage += 'No mapped CustomerID found for ParentObjectID ' + ISNULL(CONVERT(VARCHAR(40),@ParentObjectID),'NULL') + @ErrorMessageSeparator
	END

	IF @HttpMethod = 'PUT'	/*= SQL Update*/
	BEGIN
		SELECT @NewLeadID = ldv.LeadID
		FROM LeadDetailValues ldv WITH (NOLOCK) 
		WHERE ldv.DetailFieldID = 313787 /*API Transfer Mapping GUID*/
		AND ldv.DetailValue = CONVERT(VARCHAR(40), @ObjectID)

		IF @NewLeadID IS NULL
		BEGIN
			SELECT @ErrorMessage += 'No mapped Policy Admin LeadID found for ObjectID ' + ISNULL(CONVERT(VARCHAR(40),@ObjectID),'NULL') + @ErrorMessageSeparator
		END
	END

	/*If a validation error occurred, raise it - it will be caught in the calling procedure _C00_Mediate_Pop*/
	IF @ErrorMessage > ''
	BEGIN
		SELECT @ErrorMessage = SUBSTRING(@ErrorMessage, 1, LEN(@ErrorMessage)-(LEN(@ErrorMessageSeparator)-1)) /*Remove the final error separator*/

		RAISERROR (@ErrorMessage, 16, 1)

		RETURN
	END

	IF @HttpMethod = 'POST' /*We are creating a new pet*/
	BEGIN
		/*Get the pet reference. We only do this for a new lead on the assumption that it will not change and therefore does not need to be updated for the POST method.*/
		SELECT @PetRef = @XmlPayload.value('(//petRef)[1]', 'VARCHAR(2000)')

		/*Create the new lead record*/
		INSERT Lead ( [ClientID], [LeadRef], [CustomerID], [LeadTypeID], [AquariumStatusID], [ClientStatusID], [BrandNew], [Assigned], [AssignedTo], [AssignedBy], [AssignedDate], [RecalculateEquations], [Password], [Salt], [WhenCreated] )
		VALUES ( @ClientID, @PetRef, @NewCustomerID, @NewLeadTypeID, 2/*Accepted*/, NULL, 0, 0, NULL, NULL, NULL, 0, NULL, NULL, @DateTimeCompleted )

		/*Collect the new ID*/
		SELECT @NewLeadID = SCOPE_IDENTITY()
	END

	/*
		Save Data to Detail Fields
		--------------------------
	*/

	/*Validate and save the Lead detail values*/
	EXEC _C00_BulkValidateAndSaveDetailValuesFromXml
		 @XmlDataSource			= @XmlPayload
		,@XmlToDetailFieldMap	= @LeadFieldMapping
		,@ObjectID				= @NewLeadID
		,@ObjectTypeID			= 1 /*Lead*/
		,@TableDetailFieldID	= NULL
		,@RunAsUserID			= @RunAsUserID

	/*
		Return Response, Debug Display (if required) and New AQ Object ID
		-----------------------------------------------------------------
	*/

	/*Assemble response and return it*/
	SELECT @JsonResponse = ISNULL([dbo].[fn_C600_Mediate_Harvest_Pet](@NewLeadID,0,1,1), '{}')

	/*Return the whole lead object*/
	IF @ShowNewPet = 1
	BEGIN
		SELECT	'Lead DF'
				,l.CustomerID		[CustomerID]
				,l.LeadID			[LeadID]
				,ldv.DetailFieldID	[DetailFieldID]
				,ldv.DetailValue	[DetailValue]
		FROM Lead l WITH (NOLOCK) 
		CROSS APPLY @LeadFieldMapping lfm
		LEFT JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.LeadID = l.LeadID 
		AND ldv.DetailFieldID = lfm.DetailFieldID
		WHERE l.LeadID = @NewLeadID
		ORDER BY ldv.DetailFieldID
	END

	RETURN @NewLeadID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_API_Receive_Pet] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_API_Receive_Pet] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_API_Receive_Pet] TO [sp_executeall]
GO
