SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 13-10-2014
-- Description:	Adds a client billing detail record for sentiment analysis
-- for the Analysis web service
-- =============================================
CREATE PROCEDURE [dbo].[BillClientForAnalysis]
	@ClientID INT,
	@ClientPersonnelID INT,
	@Message NVARCHAR(MAX),
	@Notes VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @ClientID = 2
	BEGIN
		INSERT INTO ClientBillingDetail(ClientID, SourceAquariumOptionID, ModuleID, WhenCreated, WhoCreated, ChargeInPence, Notes, MobileTelephone)
		VALUES (@ClientID, 64, 1,dbo.fn_GetDate_Local() ,@ClientPersonnelID, 0.0, @Message, @Notes)
	END
	ELSE
	BEGIN
		INSERT INTO ClientBillingDetail(ClientID, SourceAquariumOptionID, ModuleID, WhenCreated, WhoCreated, ChargeInPence, Notes, MobileTelephone)
		VALUES (@ClientID, 64, 1,dbo.fn_GetDate_Local() ,@ClientPersonnelID, 4.0, @Message, @Notes)	
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[BillClientForAnalysis] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillClientForAnalysis] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillClientForAnalysis] TO [sp_executeall]
GO
