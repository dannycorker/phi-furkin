SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ThirdPartyMappingQuery table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingQuery_Get_List]

AS


				
				SELECT
					[ThirdPartyMappingQueryID],
					[ClientID],
					[ThirdPartySystemID],
					[ThirdPartyFieldGroupID],
					[QueryID],
					[Method],
					[ActionURI],
					[SchemaName],
					[BaseIndex]
				FROM
					[dbo].[ThirdPartyMappingQuery] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingQuery_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingQuery_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingQuery_Get_List] TO [sp_executeall]
GO
