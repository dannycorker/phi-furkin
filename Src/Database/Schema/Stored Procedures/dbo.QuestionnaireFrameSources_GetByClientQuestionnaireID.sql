SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuestionnaireFrameSources table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireFrameSources_GetByClientQuestionnaireID]
(

	@ClientQuestionnaireID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[QuestionnaireFrameSourceID],
					[ClientID],
					[ClientQuestionnaireID],
					[PageNumber],
					[SourceUrl],
					[FrameType]
				FROM
					[dbo].[QuestionnaireFrameSources] WITH (NOLOCK) 
				WHERE
					[ClientQuestionnaireID] = @ClientQuestionnaireID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFrameSources_GetByClientQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireFrameSources_GetByClientQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFrameSources_GetByClientQuestionnaireID] TO [sp_executeall]
GO
