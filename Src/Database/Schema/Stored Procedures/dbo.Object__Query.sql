SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-06-15
-- Description:	Queries a clients objects 
-- UPDATE...	Made quick change to allow selecting a single object by ID before the dynamic querying is ready
--				And another to select the related objects requested - this only works on a single level for now
-- TODO:		1. Stop union the links and instead do something clever to link through object relationship.  Need to do this as the main object we are querying may be the "To" object.
-- =============================================
CREATE PROCEDURE [dbo].[Object__Query]
(	
	@ClientID INT,
	@SubClientID INT = NULL,
	@ObjectTypeID INT,
	@ObjectID INT = NULL,
	@Where tvpObjectWhere READONLY,
	@OrderBy tvpObjectOrderBy READONLY,
	@Skip INT = NULL,
	@Take INT = NULL,
	@LoadWith tvpInt READONLY,
	@LoadWithSummary tvpInt READONLY
)
AS
BEGIN
	
	

DECLARE @Objects TABLE
(
	ObjectID INT,
	Name VARCHAR(250),
	Details VARCHAR(2000),
	ObjectTypeID INT,
	ObjectTypeRelationshipID INT,
	FromObjectID INT
)

-- Quick hack to allow filtering to work for now.
-- Only allows one where clause and only equals for now
DECLARE @WhereCount INT
SELECT @WhereCount = COUNT(*)
FROM @Where

IF @WhereCount = 1
BEGIN

-- Primary objects
INSERT INTO @Objects (ObjectID, Name, Details, ObjectTypeID, ObjectTypeRelationshipID, FromObjectID)
SELECT o.ObjectID, o.Name, o.Details, o.ObjectTypeID, NULL, NULL
FROM dbo.Objects o WITH (NOLOCK)
INNER JOIN dbo.ObjectDetailValues v WITH (NOLOCK) ON o.ObjectID = v.ObjectID
INNER JOIN @Where w ON v.DetailFieldID = w.DetailFieldID  
WHERE o.ClientID = @ClientID
AND (@SubClientID IS NULL OR o.SubClientID = @SubClientID)
AND o.ObjectTypeID = @ObjectTypeID
AND (@ObjectID IS NULL OR o.ObjectID = @ObjectID)
AND v.DetailValue = w.Value


END
ELSE
BEGIN

-- Primary objects
INSERT INTO @Objects (ObjectID, Name, Details, ObjectTypeID, ObjectTypeRelationshipID, FromObjectID)
SELECT ObjectID, Name, Details, ObjectTypeID, NULL, NULL
FROM dbo.Objects WITH (NOLOCK)  
WHERE ClientID = @ClientID
AND (@SubClientID IS NULL OR SubClientID = @SubClientID)
AND ObjectTypeID = @ObjectTypeID
AND (@ObjectID IS NULL OR ObjectID = @ObjectID)

END

-- Linked obejcts (needs to be recursive based on what is passed in)
INSERT INTO @Objects (ObjectID, Name, Details, ObjectTypeID, ObjectTypeRelationshipID, FromObjectID)
SELECT o2.ObjectID, o2.Name, o2.Details, o2.ObjectTypeID, ol.ObjectTypeRelationshipID, o.ObjectID
FROM @Objects o 
INNER JOIN dbo.ObjectLink ol WITH (NOLOCK) ON o.ObjectID = ol.FromObjectID
INNER JOIN dbo.Objects o2 WITH (NOLOCK) ON ol.ToObjectID = o2.ObjectID
INNER JOIN @LoadWith l ON ol.ObjectTypeRelationshipID = l.AnyID
UNION
SELECT o2.ObjectID, o2.Name, o2.Details, o2.ObjectTypeID, ol.ObjectTypeRelationshipID, o.ObjectID
FROM @Objects o 
INNER JOIN dbo.ObjectLink ol WITH (NOLOCK) ON o.ObjectID = ol.ToObjectID
INNER JOIN dbo.Objects o2 WITH (NOLOCK) ON ol.FromObjectID = o2.ObjectID
INNER JOIN @LoadWith l ON ol.ObjectTypeRelationshipID = l.AnyID


-- Return objects
SELECT * 
FROM @Objects

-- Return detail fields
SELECT o.ObjectID, df.DetailFieldID, df.DetailValue 
FROM dbo.ObjectDetailValues df WITH (NOLOCK) 
INNER JOIN @Objects o ON df.ObjectID = o.ObjectID 

-- Return top level data of related objects (based on what is passed in)
SELECT o.ObjectID AS FromObjectID, o2.ObjectID AS ToObjectID, ol.ObjectTypeRelationshipID, o2.Name, o2.Details
FROM @Objects o 
INNER JOIN dbo.ObjectLink ol WITH (NOLOCK) ON o.ObjectID = ol.FromObjectID
INNER JOIN dbo.Objects o2 WITH (NOLOCK) ON ol.ToObjectID = o2.ObjectID
INNER JOIN @LoadWithSummary l ON ol.ObjectTypeRelationshipID = l.AnyID
UNION
SELECT o.ObjectID AS FromObjectID, o2.ObjectID AS ToObjectID, ol.ObjectTypeRelationshipID, o2.Name, o2.Details
FROM @Objects o 
INNER JOIN dbo.ObjectLink ol WITH (NOLOCK) ON o.ObjectID = ol.ToObjectID
INNER JOIN dbo.Objects o2 WITH (NOLOCK) ON ol.FromObjectID = o2.ObjectID
INNER JOIN @LoadWithSummary l ON ol.ObjectTypeRelationshipID = l.AnyID

	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[Object__Query] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Object__Query] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Object__Query] TO [sp_executeall]
GO
