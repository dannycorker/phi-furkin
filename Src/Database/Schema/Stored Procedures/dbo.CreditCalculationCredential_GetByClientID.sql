SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CreditCalculationCredential table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CreditCalculationCredential_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[CreditCalculationCredentialID],
					[ClientID],
					[ClientPersonnelID],
					[SecretKey]
				FROM
					[dbo].[CreditCalculationCredential] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CreditCalculationCredential_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CreditCalculationCredential_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CreditCalculationCredential_GetByClientID] TO [sp_executeall]
GO
