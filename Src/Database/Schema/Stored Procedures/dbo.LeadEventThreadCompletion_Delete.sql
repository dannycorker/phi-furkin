SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadEventThreadCompletion table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEventThreadCompletion_Delete]
(

	@LeadEventThreadCompletionID int   
)
AS


				DELETE FROM [dbo].[LeadEventThreadCompletion] WITH (ROWLOCK) 
				WHERE
					[LeadEventThreadCompletionID] = @LeadEventThreadCompletionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEventThreadCompletion_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEventThreadCompletion_Delete] TO [sp_executeall]
GO
