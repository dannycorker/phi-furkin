SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ScriptTag table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptTag_Find]
(

	@SearchUsingOR bit   = null ,

	@ScriptTagID int   = null ,

	@Tag varchar (50)  = null ,

	@Template varchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ScriptTagID]
	, [Tag]
	, [Template]
    FROM
	[dbo].[ScriptTag] WITH (NOLOCK) 
    WHERE 
	 ([ScriptTagID] = @ScriptTagID OR @ScriptTagID IS NULL)
	AND ([Tag] = @Tag OR @Tag IS NULL)
	AND ([Template] = @Template OR @Template IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ScriptTagID]
	, [Tag]
	, [Template]
    FROM
	[dbo].[ScriptTag] WITH (NOLOCK) 
    WHERE 
	 ([ScriptTagID] = @ScriptTagID AND @ScriptTagID is not null)
	OR ([Tag] = @Tag AND @Tag is not null)
	OR ([Template] = @Template AND @Template is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptTag_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptTag_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptTag_Find] TO [sp_executeall]
GO
