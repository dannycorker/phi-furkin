SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PortalUserGroup table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroup_Find]
(

	@SearchUsingOR bit   = null ,

	@PortalUserGroupID int   = null ,

	@ClientID int   = null ,

	@GroupName varchar (50)  = null ,

	@GroupDescription varchar (200)  = null ,

	@ShowGroupMessage bit   = null ,

	@MessageTitle varchar (MAX)  = null ,

	@MessageBody varchar (MAX)  = null ,

	@HideOwnCustomer bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PortalUserGroupID]
	, [ClientID]
	, [GroupName]
	, [GroupDescription]
	, [ShowGroupMessage]
	, [MessageTitle]
	, [MessageBody]
	, [HideOwnCustomer]
    FROM
	[dbo].[PortalUserGroup] WITH (NOLOCK) 
    WHERE 
	 ([PortalUserGroupID] = @PortalUserGroupID OR @PortalUserGroupID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([GroupName] = @GroupName OR @GroupName IS NULL)
	AND ([GroupDescription] = @GroupDescription OR @GroupDescription IS NULL)
	AND ([ShowGroupMessage] = @ShowGroupMessage OR @ShowGroupMessage IS NULL)
	AND ([MessageTitle] = @MessageTitle OR @MessageTitle IS NULL)
	AND ([MessageBody] = @MessageBody OR @MessageBody IS NULL)
	AND ([HideOwnCustomer] = @HideOwnCustomer OR @HideOwnCustomer IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PortalUserGroupID]
	, [ClientID]
	, [GroupName]
	, [GroupDescription]
	, [ShowGroupMessage]
	, [MessageTitle]
	, [MessageBody]
	, [HideOwnCustomer]
    FROM
	[dbo].[PortalUserGroup] WITH (NOLOCK) 
    WHERE 
	 ([PortalUserGroupID] = @PortalUserGroupID AND @PortalUserGroupID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([GroupName] = @GroupName AND @GroupName is not null)
	OR ([GroupDescription] = @GroupDescription AND @GroupDescription is not null)
	OR ([ShowGroupMessage] = @ShowGroupMessage AND @ShowGroupMessage is not null)
	OR ([MessageTitle] = @MessageTitle AND @MessageTitle is not null)
	OR ([MessageBody] = @MessageBody AND @MessageBody is not null)
	OR ([HideOwnCustomer] = @HideOwnCustomer AND @HideOwnCustomer is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroup_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup_Find] TO [sp_executeall]
GO
