SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:        Ian Slack (copy of RulesEngine_Rules_InsertUpdateRule)
-- Create date:   2016-08-15
-- Description:   Inserts or updates a rule
-- UPDATES:
-- 2020-10-01	SB	Fixed zeroing out of RuleOrder
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Rules_Save]
(
      @RuleID INT,
      @Name VARCHAR(200),
      @RuleSetID INT,
      @ClientID INT,
      @UserID INT,
      @Description VARCHAR(MAX),
      @RuleOrder INT,
      @RuleCheckpoint VARCHAR(250)
)
AS
BEGIN
      
      SET NOCOUNT ON;
      
      IF @RuleID > 0
      BEGIN
      
            UPDATE dbo.RulesEngine_Rules
            SET Name = @Name,
                  WhenModified = dbo.fn_GetDate_Local(), 
                  WhoModified = @UserID,
                  Description = @Description,
                  RuleOrder = CASE WHEN ISNULL(@RuleOrder, 0) = 0 THEN RuleOrder ELSE @RuleOrder END,
                  RuleCheckpoint = @RuleCheckpoint
            WHERE RuleID = @RuleID
      
      END
      ELSE
      BEGIN
            
            DECLARE @MaxOrder INT
            SELECT @MaxOrder = MAX(r.RuleOrder)
            FROM dbo.RulesEngine_Rules r WITH (NOLOCK) 
            WHERE r.RuleSetID = @RuleSetID
            
            INSERT INTO dbo.RulesEngine_Rules (Name, RuleSetID, ClientID, RuleOrder, WhenCreated, WhoCreated, WhenModified, WhoModified, Description, RuleCheckpoint)
            VALUES (@Name, @RuleSetID, @ClientID, ISNULL(@MaxOrder, 0) + 1, dbo.fn_GetDate_Local(), @UserID, dbo.fn_GetDate_Local(), @UserID, @Description, @RuleCheckpoint)
            
            SELECT @RuleID = SCOPE_IDENTITY()
      
      END

      EXEC dbo.RulesEngine_Rules_GetRulesByRuleSetID @ClientID, @RuleSetID, @RuleID
      
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Rules_Save] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Rules_Save] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Rules_Save] TO [sp_executeall]
GO
