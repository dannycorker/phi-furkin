SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUserCase table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserCase_GetByCaseID]
(

	@CaseID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[PortalUserCaseID],
					[PortalUserID],
					[LeadID],
					[CaseID],
					[ClientID]
				FROM
					[dbo].[PortalUserCase] WITH (NOLOCK) 
				WHERE
					[CaseID] = @CaseID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase_GetByCaseID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserCase_GetByCaseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase_GetByCaseID] TO [sp_executeall]
GO
