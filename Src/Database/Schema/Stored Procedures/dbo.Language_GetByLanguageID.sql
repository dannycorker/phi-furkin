SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Language table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Language_GetByLanguageID]
(

	@LanguageID int   
)
AS


				SELECT
					[LanguageID],
					[LanguageName],
					[Alpha2Code],
					[Alpha3Code],
					[NativeName],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[Language] WITH (NOLOCK) 
				WHERE
										[LanguageID] = @LanguageID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Language_GetByLanguageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Language_GetByLanguageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Language_GetByLanguageID] TO [sp_executeall]
GO
