SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the MatterPageChoice table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterPageChoice_Get_List]

AS


				
				SELECT
					[MatterPageChoiceID],
					[ClientID],
					[MatterID],
					[DetailFieldPageID],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[MatterPageChoice] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterPageChoice_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterPageChoice_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterPageChoice_Get_List] TO [sp_executeall]
GO
