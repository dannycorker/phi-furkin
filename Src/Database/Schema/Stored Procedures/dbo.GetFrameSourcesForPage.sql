SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetFrameSourcesForPage] @ClientQuestionnaireID int, @PageNumber int

AS

Select QuestionnaireFrameSourceID, ClientID, ClientQuestionnaireID, PageNumber, SourceUrl, FrameType
From QuestionnaireFrameSources
Where ClientQuestionnaireID = @ClientQuestionnaireID and PageNumber = @PageNumber



GO
GRANT VIEW DEFINITION ON  [dbo].[GetFrameSourcesForPage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetFrameSourcesForPage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetFrameSourcesForPage] TO [sp_executeall]
GO
