SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------
-- Date Created: 10-01-2011

-- Created By: Cathal
-- Purpose: only event types appropriate to the LeadType, EventSubtype and InProcess/OOP that are allowed for the given user
-- SB  2014-07-10 Updated to use view which includes shared event types
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[EventType__GetByClientIDLeadTypeIDEventSubTypeIDAndInProcessSecure] 
(
	@ClientID int,
	@LeadTypeID int,
	@EventSubTypeID int,
	@InProcess bit,
	@UserID int
)
AS

	SET ANSI_NULLS OFF
	
	DECLARE @LogEntry as Varchar(200)
	
	--IF @ClientID = 184 and @LeadTypeID = 0
	--BEGIN
	--	SELECT @LeadTypeID = 934 /*PPI - Temporary fix by CS.  Discussed with Paul*/
	--END

	SELECT et.*
	FROM
		dbo.fnEventTypeShared(@ClientID) et
		INNER JOIN fnEventTypeSecure(@UserID, @LeadTypeID) f ON et.EventTypeID = f.objectid
	WHERE
		ClientID = @ClientID
		AND EventSubTypeID = @EventSubTypeID
		AND InProcess = @InProcess 
		AND (Enabled = 1) 
		AND (AvailableManually = 1) 
	ORDER BY EventTypeName

	SELECT @@ROWCOUNT

	--IF @ClientID = 184 /*Temp by CS 2012-10-17*/
	--BEGIN
	--	SELECT @LogEntry = '@ClientID = 184, @LeadTypeID = '+ isnull(convert(varchar,@LeadTypeID),'null') + ', @EventSubTypeID '+ isnull(convert(varchar,@EventSubTypeID),'') +', @InProcess = '+ isnull(convert(varchar,@InProcess),'') +', @UserID = ' + isnull(convert(varchar,@UserID),'')
	--	exec _C00_LogIt 'Info', '_C184_LOG', 'GetByClientIDLeadTypeIDEventSubTypeIDAndInProcessSecure', @LogEntry, 4237
	--END

	SET ANSI_NULLS ON


GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetByClientIDLeadTypeIDEventSubTypeIDAndInProcessSecure] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetByClientIDLeadTypeIDEventSubTypeIDAndInProcessSecure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetByClientIDLeadTypeIDEventSubTypeIDAndInProcessSecure] TO [sp_executeall]
GO
