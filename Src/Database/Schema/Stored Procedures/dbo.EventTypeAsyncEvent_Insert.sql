SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventTypeAsyncEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAsyncEvent_Insert]
(

	@EventTypeAsyncEventID int    OUTPUT,

	@ClientID int   ,

	@SourceEventTypeID int   ,

	@AsyncEventTypeID int   ,

	@FollowupThreadNumber int   ,

	@AquariumOptionID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[EventTypeAsyncEvent]
					(
					[ClientID]
					,[SourceEventTypeID]
					,[AsyncEventTypeID]
					,[FollowupThreadNumber]
					,[AquariumOptionID]
					,[SourceID]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@ClientID
					,@SourceEventTypeID
					,@AsyncEventTypeID
					,@FollowupThreadNumber
					,@AquariumOptionID
					,@SourceID
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @EventTypeAsyncEventID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAsyncEvent_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAsyncEvent_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAsyncEvent_Insert] TO [sp_executeall]
GO
