SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the WorkflowTaskCompleted table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowTaskCompleted_Delete]
(

	@WorkflowTaskCompletedID int   
)
AS


				DELETE FROM [dbo].[WorkflowTaskCompleted] WITH (ROWLOCK) 
				WHERE
					[WorkflowTaskCompletedID] = @WorkflowTaskCompletedID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTaskCompleted_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowTaskCompleted_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTaskCompleted_Delete] TO [sp_executeall]
GO
