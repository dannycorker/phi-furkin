SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-01-26
-- Description:	Look for policies affected by a change in pet number
--              suspend collections on them and start MTA
-- =============================================
CREATE PROCEDURE [dbo].[_C600_ManageMultiPetMTA]
(
	@MatterID INT
)
	
AS
BEGIN

	SET NOCOUNT ON

--declare @MatterID INT=50021928

	
	DECLARE @AqAutomation INT,
			@CaseID INT,
			@CollectionsMatterID INT,
			@MaxPetNumber INT = 0,
			@PetNumber INT,
			@PolicyAdminMatterID INT,
			@PoliciesAffected tvpIntInt
	
	-- get pet number on affected policy
	SELECT @CaseID=CaseID, 
			@AqAutomation=dbo.fnGetKeyValueAsIntFromThirdPartyIDs (ClientID,53,'CFG|AqAutomationCPID',0)
	FROM Matter WITH (NOLOCK) WHERE MatterID=@MatterID
	SELECT @PetNumber=dbo.fnGetDvAsInt (175453,@CaseID)
	
	-- look for policies on the same collection matter with a higher pet number
	SELECT @CollectionsMatterID=ltr.ToMatterID 
	FROM LeadTypeRelationship ltr WITH (NOLOCK) 
	WHERE ltr.FromMatterID=@MatterID AND ltr.ToLeadTypeID=1493
	
	/* 
	   There are two methods of discounting:
		a).	Fixed discount for the second and higher policies
		b). A rate that depends on the absolute pet number
		
	   For (a) re-rating needs to be done if this is Pet Number 1 and there are other policies on this collections matter;
	   For (b) re-rating needs to be done if there are higher pet numbers than this pet number
	   		
	*/
	
	-- get maximum pet number from discounts table
	SELECT @MaxPetNumber=MAX(pn.ValueInt) FROM MatterDetailValues sch WITH (NOLOCK) 
	INNER JOIN MatterDetailValues schsch WITH (NOLOCK) ON sch.ValueInt = schsch.ValueInt AND schsch.DetailFieldID=145689
	INNER JOIN Matter m WITH (NOLOCK) ON schsch.MatterID=m.MatterID
	INNER JOIN Lead l WITH (NOLOCK) ON m.LeadID=l.LeadID AND l.LeadTypeID=1491
	INNER JOIN TableRows tr WITH (NOLOCK) ON tr.DetailFieldID=175437 AND tr.MatterID=m.MatterID
	INNER JOIN TableDetailValues fd WITH (NOLOCK) ON tr.TableRowID=fd.TableRowID AND fd.DetailFieldID=175433
	INNER JOIN TableDetailValues td WITH (NOLOCK) ON tr.TableRowID=td.TableRowID AND td.DetailFieldID=175434
	INNER JOIN TableDetailValues pn WITH (NOLOCK) ON tr.TableRowID=pn.TableRowID AND pn.DetailFieldID=175435
	WHERE sch.DetailFieldID=170034 AND sch.MatterID=@MatterID
	AND dbo.fn_GetDate_Local() BETWEEN fd.ValueDate AND td.ValueDate
	
	-- discounting type (a)
	IF @MaxPetNumber = 2 AND @PetNumber = 1
	BEGIN
	
		-- find lowest remaining Matter ID		
		INSERT INTO @PoliciesAffected (ID1,ID2)
		SELECT MIN(ltr.FromMatterID),0 
		FROM LeadTypeRelationship ltr WITH (NOLOCK) 
		INNER JOIN MatterDetailValues fd WITH (NOLOCK) ON ltr.FromMatterID=fd.MatterID AND fd.DetailFieldID=170036
		INNER JOIN MatterDetailValues td WITH (NOLOCK) ON ltr.FromMatterID=td.MatterID AND td.DetailFieldID=170037
		WHERE ltr.ToMatterID=@CollectionsMatterID 
		AND ltr.FromLeadTypeID=1492
		AND ltr.FromMatterID > @MatterID
		AND dbo.fn_GetDate_Local() BETWEEN fd.ValueDate AND td.ValueDate
	
	END
	ELSE
	BEGIN
		
		INSERT INTO @PoliciesAffected (ID1,ID2)
		SELECT ltr.FromMatterID,0 FROM LeadTypeRelationship ltr WITH (NOLOCK) 
		INNER JOIN MatterDetailValues fd WITH (NOLOCK) ON ltr.FromMatterID=fd.MatterID AND fd.DetailFieldID=170036
		INNER JOIN MatterDetailValues td WITH (NOLOCK) ON ltr.FromMatterID=td.MatterID AND td.DetailFieldID=170037
		WHERE ltr.ToMatterID=@CollectionsMatterID 
		AND ltr.FromLeadTypeID=1492
		AND ltr.FromMatterID > @MatterID
		AND dbo.fn_GetDate_Local() BETWEEN fd.ValueDate AND td.ValueDate
		
	END

	-- if there are any younger policies/policies with higher pet number:
	IF 0 < ( SELECT COUNT(*) FROM @PoliciesAffected )
	BEGIN
	
		-- suspend the collections matter
		INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
		SELECT TOP(1) ca.ClientID, l.CustomerID, l.LeadID, ca.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 155269, @AqAutomation, -1, 5, 0, 5
		FROM Cases ca WITH (NOLOCK)
		INNER JOIN Matter m WITH (NOLOCK) ON ca.CaseID=m.CaseID AND m.MatterID=@CollectionsMatterID 
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=ca.LeadID
		INNER JOIN LeadEvent le WITH (NOLOCK) ON ca.LatestNonNoteLeadEventID=le.LeadEventID
		
		--	 move all applicable policies to IP MTA Change in Pet Number
		WHILE EXISTS ( SELECT * FROM @PoliciesAffected WHERE ID2=0 )
		BEGIN
		
			SELECT TOP 1 @PolicyAdminMatterID=ID1 FROM @PoliciesAffected WHERE ID2=0

			INSERT AutomatedEventQueue (ClientID,CustomerID,LeadID,CaseID,FromLeadEventID,FromEventTypeID,WhenCreated,WhoCreated,AutomatedEventTypeID,RunAsUserID,ThreadToFollowUp,InternalPriority,BumpCount,ErrorCount)
			SELECT TOP(1) ca.ClientID, l.CustomerID, l.LeadID, ca.CaseID, le.LeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @AqAutomation, 155360, @AqAutomation, 1, 5, 0, 5
			FROM Cases ca WITH (NOLOCK)
			INNER JOIN Matter m WITH (NOLOCK) ON ca.CaseID=m.CaseID AND m.MatterID=@PolicyAdminMatterID 
			INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=ca.LeadID
			INNER JOIN LeadEvent le WITH (NOLOCK) ON ca.LatestInProcessLeadEventID=le.LeadEventID
				
			UPDATE @PoliciesAffected SET ID2=1 WHERE ID1=@PolicyAdminMatterID
		
		END

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ManageMultiPetMTA] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_ManageMultiPetMTA] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ManageMultiPetMTA] TO [sp_executeall]
GO
