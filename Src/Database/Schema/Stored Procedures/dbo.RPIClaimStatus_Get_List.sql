SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the RPIClaimStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClaimStatus_Get_List]

AS


				
				SELECT
					[RPIClaimStatusID],
					[MatterID],
					[ClientID],
					[ApplicationID],
					[ActivityEngineGuid],
					[PhaseCacheID],
					[PhaseCacheName],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[RPIAsUser],
					[RPIUser],
					[RPIPass],
					[Notes]
				FROM
					[dbo].[RPIClaimStatus] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClaimStatus_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClaimStatus_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClaimStatus_Get_List] TO [sp_executeall]
GO
