SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the RPIFieldMapping table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIFieldMapping_GetByRPIFieldID]
(

	@RPIFieldID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[RPIFieldMappingID],
					[ClientID],
					[LeadTypeID],
					[RPIFieldID],
					[DetailFieldID],
					[IsEnabled],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[ColumnDetailFieldID]
				FROM
					[dbo].[RPIFieldMapping] WITH (NOLOCK) 
				WHERE
					[RPIFieldID] = @RPIFieldID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping_GetByRPIFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIFieldMapping_GetByRPIFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIFieldMapping_GetByRPIFieldID] TO [sp_executeall]
GO
