SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditingGrouping table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingGrouping_GetBySqlQueryEditingGroupingID]
(

	@SqlQueryEditingGroupingID int   
)
AS


				SELECT
					[SqlQueryEditingGroupingID],
					[ClientID],
					[SqlQueryEditingID],
					[GroupByClause],
					[HavingClause],
					[HavingColumnID],
					[HavingCriteria1],
					[HavingCriteria2]
				FROM
					[dbo].[SqlQueryEditingGrouping] WITH (NOLOCK) 
				WHERE
										[SqlQueryEditingGroupingID] = @SqlQueryEditingGroupingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingGrouping_GetBySqlQueryEditingGroupingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingGrouping_GetBySqlQueryEditingGroupingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingGrouping_GetBySqlQueryEditingGroupingID] TO [sp_executeall]
GO
