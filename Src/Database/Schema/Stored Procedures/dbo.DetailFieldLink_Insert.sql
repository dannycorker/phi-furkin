SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DetailFieldLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldLink_Insert]
(

	@DetailFieldLinkID int    OUTPUT,

	@ClientID int   ,

	@DetailFieldID int   ,

	@MasterQuestionID int   ,

	@LeadTypeID int   
)
AS


				
				INSERT INTO [dbo].[DetailFieldLink]
					(
					[ClientID]
					,[DetailFieldID]
					,[MasterQuestionID]
					,[LeadTypeID]
					)
				VALUES
					(
					@ClientID
					,@DetailFieldID
					,@MasterQuestionID
					,@LeadTypeID
					)
				-- Get the identity value
				SET @DetailFieldLinkID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldLink_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLink_Insert] TO [sp_executeall]
GO
