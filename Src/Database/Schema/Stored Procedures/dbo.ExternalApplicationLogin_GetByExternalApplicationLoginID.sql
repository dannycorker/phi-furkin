SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ExternalApplicationLogin table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ExternalApplicationLogin_GetByExternalApplicationLoginID]
(

	@ExternalApplicationLoginID int   
)
AS


				SELECT
					[ExternalApplicationLoginID],
					[ThirdPartySystemID],
					[ClientID],
					[SubClientID],
					[UserName],
					[Password],
					[Salt],
					[ClientPersonnelID],
					[ClientPersonnelAdminGroupID],
					[AttemptedLogins],
					[AccountDisabled],
					[CustomerID]
				FROM
					[dbo].[ExternalApplicationLogin] WITH (NOLOCK) 
				WHERE
										[ExternalApplicationLoginID] = @ExternalApplicationLoginID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ExternalApplicationLogin_GetByExternalApplicationLoginID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ExternalApplicationLogin_GetByExternalApplicationLoginID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ExternalApplicationLogin_GetByExternalApplicationLoginID] TO [sp_executeall]
GO
