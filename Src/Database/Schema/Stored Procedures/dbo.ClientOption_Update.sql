SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOption_Update]
(

	@ClientOptionID int   ,

	@ClientID int   ,

	@ZenDeskOff bit   ,

	@ZenDeskURLPrefix varchar (50)  ,

	@ZenDeskText varchar (200)  ,

	@ZenDeskTitle varchar (50)  ,

	@IDiaryOff bit   ,

	@WorkflowOff bit   ,

	@UserDirectoryOff bit   ,

	@ECatcherOff bit   ,

	@LeadAssignmentOff bit   ,

	@UserMessagesOff bit   ,

	@SystemMessagesOff bit   ,

	@UserPortalOff bit   ,

	@ReportSearchOff bit   ,

	@NormalSearchOff bit   ,

	@UseXero bit   ,

	@UseUltra bit   ,

	@UseCaseSummary bit   ,

	@UseMoreProminentReminders bit   ,

	@UseSMSSurvey bit   ,

	@UseNewAddLead bit   ,

	@DiallerInsertMethod varchar (50)  ,

	@DiallerPrimaryKey varchar (50)  ,

	@UseCHARMS bit   ,

	@TextMessageService int   ,

	@UseThunderhead bit   ,

	@UseMemorableWordVerification bit   ,

	@ApplyInactivityTimeout bit   ,

	@InactivityTimeoutInSeconds int   ,

	@InactivityRedirectUrl varchar (50)  ,

	@UseSentimentAnalysis bit   ,

	@ShowProcessInfoButton bit   ,

	@LatestRecordsFirst bit   ,

	@UseSecureMessage bit   ,

	@EventCommentTooltipOff bit   ,

	@EnableScripting bit   ,

	@EnableBilling bit   ,

	@UseDocumentTypeVersioning bit   ,

	@EnableSMSSTOP bit   ,

	@UseEngageMail bit   ,

	@PreventForcedLogout bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientOption]
				SET
					[ClientID] = @ClientID
					,[ZenDeskOff] = @ZenDeskOff
					,[ZenDeskURLPrefix] = @ZenDeskURLPrefix
					,[ZenDeskText] = @ZenDeskText
					,[ZenDeskTitle] = @ZenDeskTitle
					,[iDiaryOff] = @IDiaryOff
					,[WorkflowOff] = @WorkflowOff
					,[UserDirectoryOff] = @UserDirectoryOff
					,[eCatcherOff] = @ECatcherOff
					,[LeadAssignmentOff] = @LeadAssignmentOff
					,[UserMessagesOff] = @UserMessagesOff
					,[SystemMessagesOff] = @SystemMessagesOff
					,[UserPortalOff] = @UserPortalOff
					,[ReportSearchOff] = @ReportSearchOff
					,[NormalSearchOff] = @NormalSearchOff
					,[UseXero] = @UseXero
					,[UseUltra] = @UseUltra
					,[UseCaseSummary] = @UseCaseSummary
					,[UseMoreProminentReminders] = @UseMoreProminentReminders
					,[UseSMSSurvey] = @UseSMSSurvey
					,[UseNewAddLead] = @UseNewAddLead
					,[DiallerInsertMethod] = @DiallerInsertMethod
					,[DiallerPrimaryKey] = @DiallerPrimaryKey
					,[UseCHARMS] = @UseCHARMS
					,[TextMessageService] = @TextMessageService
					,[UseThunderhead] = @UseThunderhead
					,[UseMemorableWordVerification] = @UseMemorableWordVerification
					,[ApplyInactivityTimeout] = @ApplyInactivityTimeout
					,[InactivityTimeoutInSeconds] = @InactivityTimeoutInSeconds
					,[InactivityRedirectUrl] = @InactivityRedirectUrl
					,[UseSentimentAnalysis] = @UseSentimentAnalysis
					,[ShowProcessInfoButton] = @ShowProcessInfoButton
					,[LatestRecordsFirst] = @LatestRecordsFirst
					,[UseSecureMessage] = @UseSecureMessage
					,[EventCommentTooltipOff] = @EventCommentTooltipOff
					,[EnableScripting] = @EnableScripting
					,[EnableBilling] = @EnableBilling
					,[UseDocumentTypeVersioning] = @UseDocumentTypeVersioning
					,[EnableSMSSTOP] = @EnableSMSSTOP
					,[UseEngageMail] = @UseEngageMail
					,[PreventForcedLogout] = @PreventForcedLogout
				WHERE
[ClientOptionID] = @ClientOptionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOption_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOption_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOption_Update] TO [sp_executeall]
GO
