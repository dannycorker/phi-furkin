SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadTypeLink table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypeLink_GetByLeadTypeLinkID]
(

	@LeadTypeLinkID int   
)
AS


				SELECT
					[LeadTypeLinkID],
					[ClientID],
					[LeadTypeID],
					[OutcomeID]
				FROM
					[dbo].[LeadTypeLink] WITH (NOLOCK) 
				WHERE
										[LeadTypeLinkID] = @LeadTypeLinkID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypeLink_GetByLeadTypeLinkID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypeLink_GetByLeadTypeLinkID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypeLink_GetByLeadTypeLinkID] TO [sp_executeall]
GO
