SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2010-10-15
-- Description:	List UploadedFile details without the actual file content
-- =============================================
CREATE PROCEDURE [dbo].[UploadedFile__GetByFolderIDNoBlobs]
(
	@FolderID int   
)
AS
BEGIN

	SET ANSI_NULLS OFF
	
	SELECT
		[UploadedFileID],
		[ClientID],
		[FolderID],
		[UploadedFileName],
		[UploadedFileTitle],
		[UploadedFileDescription],
		CAST('' as varbinary) AS [UploadedFile],
		[UploadedFileGUID],
		[UploadedFileSize],
		[WhoCreated],
		[WhenCreated],
		[WhoModified],
		[WhenModified]
	FROM
		[dbo].[UploadedFile]
	WHERE
		[FolderID] = @FolderID
	
	SELECT @@ROWCOUNT
	SET ANSI_NULLS ON


END
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile__GetByFolderIDNoBlobs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UploadedFile__GetByFolderIDNoBlobs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UploadedFile__GetByFolderIDNoBlobs] TO [sp_executeall]
GO
