SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DocumentStandardTarget table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentStandardTarget_Find]
(

	@SearchUsingOR bit   = null ,

	@DocumentStandardTargetID int   = null ,

	@ClientID int   = null ,

	@DocumentTypeID int   = null ,

	@Target varchar (250)  = null ,

	@ObjectName varchar (250)  = null ,

	@PropertyName varchar (250)  = null ,

	@TemplateTypeID int   = null ,

	@Notes varchar (250)  = null ,

	@IsSpecial bit   = null ,

	@ExcelSheetLocation varchar (50)  = null ,

	@DocumentTypeVersionID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DocumentStandardTargetID]
	, [ClientID]
	, [DocumentTypeID]
	, [Target]
	, [ObjectName]
	, [PropertyName]
	, [TemplateTypeID]
	, [Notes]
	, [IsSpecial]
	, [ExcelSheetLocation]
	, [DocumentTypeVersionID]
    FROM
	[dbo].[DocumentStandardTarget] WITH (NOLOCK) 
    WHERE 
	 ([DocumentStandardTargetID] = @DocumentStandardTargetID OR @DocumentStandardTargetID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([DocumentTypeID] = @DocumentTypeID OR @DocumentTypeID IS NULL)
	AND ([Target] = @Target OR @Target IS NULL)
	AND ([ObjectName] = @ObjectName OR @ObjectName IS NULL)
	AND ([PropertyName] = @PropertyName OR @PropertyName IS NULL)
	AND ([TemplateTypeID] = @TemplateTypeID OR @TemplateTypeID IS NULL)
	AND ([Notes] = @Notes OR @Notes IS NULL)
	AND ([IsSpecial] = @IsSpecial OR @IsSpecial IS NULL)
	AND ([ExcelSheetLocation] = @ExcelSheetLocation OR @ExcelSheetLocation IS NULL)
	AND ([DocumentTypeVersionID] = @DocumentTypeVersionID OR @DocumentTypeVersionID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DocumentStandardTargetID]
	, [ClientID]
	, [DocumentTypeID]
	, [Target]
	, [ObjectName]
	, [PropertyName]
	, [TemplateTypeID]
	, [Notes]
	, [IsSpecial]
	, [ExcelSheetLocation]
	, [DocumentTypeVersionID]
    FROM
	[dbo].[DocumentStandardTarget] WITH (NOLOCK) 
    WHERE 
	 ([DocumentStandardTargetID] = @DocumentStandardTargetID AND @DocumentStandardTargetID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([DocumentTypeID] = @DocumentTypeID AND @DocumentTypeID is not null)
	OR ([Target] = @Target AND @Target is not null)
	OR ([ObjectName] = @ObjectName AND @ObjectName is not null)
	OR ([PropertyName] = @PropertyName AND @PropertyName is not null)
	OR ([TemplateTypeID] = @TemplateTypeID AND @TemplateTypeID is not null)
	OR ([Notes] = @Notes AND @Notes is not null)
	OR ([IsSpecial] = @IsSpecial AND @IsSpecial is not null)
	OR ([ExcelSheetLocation] = @ExcelSheetLocation AND @ExcelSheetLocation is not null)
	OR ([DocumentTypeVersionID] = @DocumentTypeVersionID AND @DocumentTypeVersionID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentStandardTarget_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentStandardTarget_Find] TO [sp_executeall]
GO
