SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CustomerInformationFieldDefinitions table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerInformationFieldDefinitions_GetByCustomerInformationFieldDefinitionID]
(

	@CustomerInformationFieldDefinitionID int   
)
AS


				SELECT
					[CustomerInformationFieldDefinitionID],
					[ClientQuestionnaireID],
					[FieldAtBeginningOfQuestionnaire],
					[OrdinalPosition],
					[FieldName],
					[FieldLabel],
					[Mandatory],
					[VerifyPhoneNumber],
					[ClientID]
				FROM
					[dbo].[CustomerInformationFieldDefinitions] WITH (NOLOCK) 
				WHERE
										[CustomerInformationFieldDefinitionID] = @CustomerInformationFieldDefinitionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerInformationFieldDefinitions_GetByCustomerInformationFieldDefinitionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerInformationFieldDefinitions_GetByCustomerInformationFieldDefinitionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerInformationFieldDefinitions_GetByCustomerInformationFieldDefinitionID] TO [sp_executeall]
GO
