SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2015-04-27
-- Description:	Copy a Script and all its dependent tables
-- Modified By PR 23/02/2016 Added the word Copy to the end of the new script's name
-- =============================================
CREATE PROCEDURE [dbo].[Script__CopyAllTables] 
	@ExistingScriptID INT, 
	@UserID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @NewScriptID INT
	
	/* Start with the main Script table to get the new identity */
	INSERT INTO dbo.Script (ClientID, LeadTypeID, ScriptName, ScriptDescription, WhenCreated, WhoCreated, WhenModified, WhoModified, FinishEventTypeID, Published, Disabled, ScriptParentID) 
	SELECT s.[ClientID], s.[LeadTypeID], s.[ScriptName] + ' Copy', s.[ScriptDescription], dbo.fn_GetDate_Local() AS [WhenCreated], @UserID AS [WhoCreated], dbo.fn_GetDate_Local() AS [WhenModified], @UserID AS [WhoModified], s.[FinishEventTypeID], 0 AS [Published], 0 AS [Disabled], @ExistingScriptID AS [ScriptParentID] 
	FROM dbo.Script s WITH (NOLOCK) 
	WHERE s.ScriptID = @ExistingScriptID 
	
	SELECT @NewScriptID = SCOPE_IDENTITY()
	
	/* Assuming the base table copied correctly, copy all the dependent tables as well */
	IF @NewScriptID IS NOT NULL
	BEGIN
		
		/* ScriptSection */
		INSERT INTO dbo.ScriptSection([ScriptID], [ClientID], [SectionTitle], [SectionOrder], [WhenCreated], [WhoCreated], [WhenModified], [WhoModified], [SourceID])
		SELECT @NewScriptID AS [ScriptID], s.[ClientID], s.[SectionTitle], s.[SectionOrder], dbo.fn_GetDate_Local() AS [WhenCreated], @UserID AS [WhoCreated], dbo.fn_GetDate_Local() AS [WhenModified], @UserID AS [WhoModified], s.ScriptSectionID AS [SourceID]
		FROM dbo.ScriptSection s WITH (NOLOCK) 
		WHERE s.ScriptID = @ExistingScriptID 
		
		/* ScriptDialogue needs to join to ScriptSection to get the new ScriptSectionID values */
		INSERT INTO dbo.ScriptDialogue([ScriptSectionID], [ClientID], [Dialogue], [HtmlDialogue], [WhenCreated], [WhoCreated], [WhenModified], [WhoModified])
		SELECT ss.[ScriptSectionID] AS [ScriptID], s.[ClientID], s.[Dialogue], s.[HtmlDialogue], dbo.fn_GetDate_Local() AS [WhenCreated], @UserID AS [WhoCreated], dbo.fn_GetDate_Local() AS [WhenModified], @UserID AS [WhoModified]
		FROM dbo.ScriptDialogue s WITH (NOLOCK) 
		INNER JOIN dbo.ScriptSection ss WITH (NOLOCK) ON ss.SourceID = s.ScriptSectionID
		WHERE ss.ScriptID = @NewScriptID 
		
		/* ScriptDetailFieldTarget needs to join to ScriptSection to get the new ScriptSectionID values */
		INSERT INTO dbo.ScriptDetailFieldTarget([ScriptID], [ClientID], [Target], [DetailFieldID], [ColumnFieldID], [Format], [SectionID])
		SELECT @NewScriptID AS [ScriptID], s.[ClientID], s.[Target], s.[DetailFieldID], s.[ColumnFieldID], s.[Format], ss.[ScriptSectionID]
		FROM dbo.ScriptDetailFieldTarget s WITH (NOLOCK) 
		INNER JOIN dbo.ScriptSection ss WITH (NOLOCK) ON ss.SourceID = s.SectionID 
		WHERE ss.ScriptID = @NewScriptID 
		AND s.ScriptID = @ExistingScriptID
		
		/* ScriptEventTarget needs to join to ScriptSection to get the new ScriptSectionID values */
		INSERT INTO dbo.ScriptEventTarget([ScriptID], [SectionID], [ClientID], [Target], [EventTypeID], [EventTypeName], [EventSubtypeID], [Format])
		SELECT @NewScriptID AS [ScriptID], ss.[ScriptSectionID], s.[ClientID], s.[Target], s.[EventTypeID], s.[EventTypeName], [EventSubtypeID], s.[Format]
		FROM dbo.ScriptEventTarget s WITH (NOLOCK) 
		INNER JOIN dbo.ScriptSection ss WITH (NOLOCK) ON ss.SourceID = s.SectionID
		WHERE ss.ScriptID = @NewScriptID 
		AND s.ScriptID = @ExistingScriptID
		
		/* ScriptStandardTarget */
		INSERT INTO dbo.ScriptStandardTarget([ScriptID], [ClientID], [Target], [ObjectName], [PropertyName])
		SELECT @NewScriptID AS [ScriptID], s.[ClientID], s.[Target], s.[ObjectName], s.[PropertyName]
		FROM dbo.ScriptStandardTarget s WITH (NOLOCK) 
		WHERE s.ScriptID = @ExistingScriptID 
		
		/* ScriptPositionTarget needs to join to ScriptSection to get the new ScriptSectionID values */
		INSERT INTO dbo.ScriptPositionTarget([ScriptID], [SectionID], [ClientID], [PositionName], [LinkText])
		SELECT @NewScriptID AS [ScriptID], ss.[ScriptSectionID], s.[ClientID], s.[PositionName], s.[LinkText]
		FROM dbo.ScriptPositionTarget s WITH (NOLOCK) 
		INNER JOIN dbo.ScriptSection ss WITH (NOLOCK) ON ss.SourceID = s.SectionID
		WHERE ss.ScriptID = @NewScriptID 
		AND s.ScriptID = @ExistingScriptID
		
	END
	ELSE
	BEGIN
		SET @NewScriptID = 0
	END
	
	/* Return the new ID to the caller */
	RETURN @NewScriptID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__CopyAllTables] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script__CopyAllTables] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__CopyAllTables] TO [sp_executeall]
GO
