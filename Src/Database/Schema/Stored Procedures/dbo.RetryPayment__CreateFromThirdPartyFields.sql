SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgaan
-- Create date: 2016-09-15
-- Description:	Performs a billing retry.
--				When the customer has indicated that they have fixed the issue with collection
--				this stored proc adds new rows to the Purchased Product Payment Schedule for all failed collections
--				and rebuilds the customer payment schedule
-- Mods:
--	2016-11-10 DCM update accountID to that currently being used
-- 2017-03-24 JEL Changed inserts of PPPS record to positive as the failure is a negative record therefore retrys for collection were going in a refunds
-- 2018-03-19 JEL Added consderation for Client Account Table
-- 2018-05-22 JEL changed dates to simply add the three working days, check mandate wait then ensure is working day
-- 2020-03-02 GPR for JIRA AAG-202 | Added check on CountryID from ClientID as the switch for removing the AccountMandate check
-- =============================================
CREATE PROCEDURE [dbo].[RetryPayment__CreateFromThirdPartyFields]
	@ClientID INT,
	@CaseID INT,
	@LeadEventID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @CountryID INT

	DECLARE @CustomerID INT, @LeadID INT, @MatterID INT, @LeadTypeID INT, @WhoCreated INT
	SELECT @LeadID=cs.LeadID FROM Cases cs WITH (NOLOCK) WHERE cs.CaseID = @CaseID
	SELECT @CustomerID = l.CustomerID, @LeadTypeID=l.LeadTypeID FROM Lead l WITH (NOLOCK) WHERE l.LeadID=@LeadID
	-- there can only be one matter per case
	SELECT TOP 1 @MatterID = m.MatterID FROM Matter m WITH (NOLOCK) WHERE m.CaseID=@CaseID
	
	SELECT @WhoCreated = le.WhoCreated FROM LeadEvent le WITH (NOLOCK) WHERE le.LeadEventID=@LeadEventID
	DECLARE @WhenCreated VARCHAR(10) = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) -- current date time for when created
	
	--4520	AccountID
	DECLARE @AccountID_DV VARCHAR(2000), @IsInt_AccountID BIT, @AccountID INT
	SELECT @AccountID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4520)	
	SELECT @IsInt_AccountID = dbo.fnIsInt(@AccountID_DV)		
	IF(@IsInt_AccountID=1)
	BEGIN
		SELECT @AccountID = CAST(@AccountID_DV AS INT)	
	END	
	
	DECLARE @NewEntries TABLE (AnyID INT, AnyDate DATE, AnyID2 INT, AnyDate2 DATE)
	DECLARE	@ActualCollectionDate DATE, @PaymentDate DATE, @PurchasedProducts tvpInt

	-- look for any failures that have happened on this purchased product, because the collections failure may have occurred
	-- on a different account to that being used right now
	
	INSERT INTO @PurchasedProducts
	SELECT DISTINCT ppps.PurchasedProductID FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) WHERE ppps.AccountID=@AccountID
	
	INSERT INTO @NewEntries (AnyID, AnyDate)
	SELECT PurchasedProductPaymentScheduleID, PaymentDate
	FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
	INNER JOIN @PurchasedProducts pp ON ppps.PurchasedProductID=pp.AnyID
	WHERE ppps.PaymentStatusID=4
	AND NOT EXISTS ( SELECT * FROM PurchasedProductPaymentSchedule ppps2 WITH (NOLOCK) 
						WHERE ppps.PurchasedProductPaymentScheduleParentID = ppps2.PurchasedProductPaymentScheduleParentID
						AND ppps2.PurchasedProductPaymentScheduleID>ppps.PurchasedProductPaymentScheduleID
					)
	
	-- set the correct accountID to use.  A payment being retried may now be being collected on a different account
	UPDATE ne
	SET AnyID2=pp.AccountID
	FROM @NewEntries ne	
	INNER JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) ON ne.AnyID=ppps.PurchasedProductPaymentScheduleID
	INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON ppps.PurchasedProductID=pp.PurchasedProductID			

	-- set next collection date
	
	/*Start with today's date*/
	SELECT @ActualCollectionDate=dbo.fn_GetDate_Local()
	
	/*Add the correct number of working days*/ 
	SELECT @ActualCollectionDate=dbo.fnAddWorkingDays(@ActualCollectionDate,DDCollectionProcessingInterval) 
	FROM dbo.BillingConfiguration WHERE ClientID=@ClientID
	
	
	
	/*GPR 2020-03-02 for AAG-202*/
	SELECT @CountryID = cl.CountryID FROM Clients cl WITH (NOLOCK)
	WHERE cl.ClientID = @ClientID
	
	IF @CountryID <> 14 /*Australia*/
	BEGIN
		/*Check that we do not override the mandate wiat*/ 
		UPDATE ne
		SET @ActualCollectionDate=CASE WHEN @ActualCollectionDate < FirstAcceptablePaymentDate THEN am.FirstAcceptablePaymentDate ELSE @ActualCollectionDate END
		FROM @NewEntries ne	
		INNER JOIN Account a WITH (NOLOCK) ON ne.AnyID2 = a.AccountID
		LEFT JOIN AccountMandate am WITH (NOLOCK) ON ne.AnyID2 = am.AccountID
		WHERE am.MandateStatusID IN (1,2,3) 
			AND a.AccountTypeID=1	
			AND NOT EXISTS ( SELECT * FROM AccountMandate am2 WITH (NOLOCK) 
								WHERE am2.AccountID=am.AccountID
								AND am2.MandateStatusID IN (1,2,3)
								AND am2.AccountMandateID>am.AccountMandateID )	
	END						
	
	SELECT TOP 1 @ActualCollectionDate= w.[Date]
	FROM  WorkingDays w WITH ( NOLOCK )  /*2017-08-22 Change to use CP Collection Dates*/
	Where w.Date >= @ActualCollectionDate
	ORDER BY w.DATE ASC 
	
	UPDATE ne
	SET AnyDate2=@ActualCollectionDate
	FROM @NewEntries ne	
	
	EXEC dbo._C00_SimpleValueIntoField 178671, @ActualCollectionDate, @MatterID, @WhoCreated
	

	IF EXISTS ( SELECT * FROM @NewEntries )
	BEGIN
	
		INSERT dbo.PurchasedProductPaymentSchedule (ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, PurchasedProductPaymentScheduleParentID, PurchasedProductPaymentScheduleTypeID, ClientAccountID)
		SELECT ClientID, CustomerID, n.AnyID2, PurchasedProductID, n.AnyDate2, CoverFrom, CoverTo, PaymentDate, ABS(PaymentNet), ABS(PaymentVAT), ABS(PaymentGross), 5, NULL, NULL, NULL, WhoCreated, dbo.fn_GetDate_Local(), PurchasedProductPaymentScheduleParentID, 3, ClientAccountID
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
		INNER JOIN @NewEntries n ON ppps.PurchasedProductPaymentScheduleID=n.AnyID
		
		SELECT @PaymentDate=MIN(AnyDate) FROM @NewEntries
		
		--EXEC Billing__RebuildCustomerPaymentSchedule @AccountID, @PaymentDate, @WhoCreated
		
		--EXEC Account__SetDateAndAmountOfNextPayment @AccountID	
	
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[RetryPayment__CreateFromThirdPartyFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RetryPayment__CreateFromThirdPartyFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RetryPayment__CreateFromThirdPartyFields] TO [sp_executeall]
GO
