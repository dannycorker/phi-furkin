SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 28-08-2015
-- Description:	Updates a chat record
-- =============================================
CREATE PROCEDURE [dbo].[Chat__Update]

	@ChatID INT,	
	@ClientID INT,
	@ObjectID INT,
	@ChatGuid VARCHAR(50),
	@ChatTypeID TINYINT,
	@ObjectTypeID TINYINT

AS
BEGIN
		
	SET NOCOUNT ON;
	
	UPDATE Chat
	SET ClientID = @ClientID,
		ObjectID = @ObjectID, 
	    ChatGuid = @ChatGuid,
	    ChatTypeID = @ChatTypeID,
	    ObjectTypeID = @ObjectTypeID
	WHERE ChatID=@ChatID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Chat__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Chat__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Chat__Update] TO [sp_executeall]
GO
