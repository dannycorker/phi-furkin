SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the CaseDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseDetailValues_Update]
(

	@CaseDetailValueID int   ,

	@ClientID int   ,

	@CaseID int   ,

	@DetailFieldID int   ,

	@DetailValue varchar (2000)  ,

	@ErrorMsg varchar (1000)  ,

	@EncryptedValue varchar (3000)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   ,

	@SourceID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[CaseDetailValues]
				SET
					[ClientID] = @ClientID
					,[CaseID] = @CaseID
					,[DetailFieldID] = @DetailFieldID
					,[DetailValue] = @DetailValue
					,[ErrorMsg] = @ErrorMsg
					,[EncryptedValue] = @EncryptedValue
					,[ValueInt] = @ValueInt
					,[ValueMoney] = @ValueMoney
					,[ValueDate] = @ValueDate
					,[ValueDateTime] = @ValueDateTime
					,[SourceID] = @SourceID
				WHERE
[CaseDetailValueID] = @CaseDetailValueID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseDetailValues_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseDetailValues_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseDetailValues_Update] TO [sp_executeall]
GO
