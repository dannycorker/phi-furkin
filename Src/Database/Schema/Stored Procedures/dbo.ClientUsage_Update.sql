SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientUsage table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientUsage_Update]
(

	@ClientID int   ,

	@OriginalClientID int   ,

	@Validity varchar (100)  ,

	@ValidityHash varchar (250)  ,

	@Usage varchar (100)  ,

	@UsageHash varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientUsage]
				SET
					[ClientID] = @ClientID
					,[Validity] = @Validity
					,[ValidityHash] = @ValidityHash
					,[Usage] = @Usage
					,[UsageHash] = @UsageHash
				WHERE
[ClientID] = @OriginalClientID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientUsage_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientUsage_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientUsage_Update] TO [sp_executeall]
GO
