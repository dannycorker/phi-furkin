SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryCriteriaSetMember table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteriaSetMember_GetBySqlQueryCriteriaSetMemberID]
(

	@SqlQueryCriteriaSetMemberID int   
)
AS


				SELECT
					[SqlQueryCriteriaSetMemberID],
					[ClientID],
					[SqlQueryID],
					[SqlQueryCriteriaSetID],
					[SqlQueryColumnID],
					[Criteria1],
					[Criteria2]
				FROM
					[dbo].[SqlQueryCriteriaSetMember] WITH (NOLOCK) 
				WHERE
										[SqlQueryCriteriaSetMemberID] = @SqlQueryCriteriaSetMemberID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSetMember_GetBySqlQueryCriteriaSetMemberID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteriaSetMember_GetBySqlQueryCriteriaSetMemberID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSetMember_GetBySqlQueryCriteriaSetMemberID] TO [sp_executeall]
GO
