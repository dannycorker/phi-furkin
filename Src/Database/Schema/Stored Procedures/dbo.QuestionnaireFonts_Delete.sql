SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the QuestionnaireFonts table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireFonts_Delete]
(

	@QuestionnaireFontID int   
)
AS


				DELETE FROM [dbo].[QuestionnaireFonts] WITH (ROWLOCK) 
				WHERE
					[QuestionnaireFontID] = @QuestionnaireFontID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFonts_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireFonts_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFonts_Delete] TO [sp_executeall]
GO
