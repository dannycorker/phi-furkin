SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SMSQuestionGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestionGroup_Get_List]

AS


				
				SELECT
					[SMSQuestionGroupID],
					[ClientID],
					[SMSQuestionnaireID],
					[GroupName],
					[SMSQuestionOrderID],
					[GroupOrder]
				FROM
					[dbo].[SMSQuestionGroup] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroup_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup_Get_List] TO [sp_executeall]
GO
