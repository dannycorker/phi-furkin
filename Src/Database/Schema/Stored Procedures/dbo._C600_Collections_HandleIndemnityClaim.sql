SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2020-06-09
-- Description:	AAG-800 - Handle indemnity claims
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Collections_HandleIndemnityClaim] 
	@CustomerID INT,
	@MatterID INT,
	@WhoCreated INT
AS
BEGIN

	SET NOCOUNT ON;

	/*
		1 Through an event in Collections, the user is able to manually select the payments to fail.
		2 Missed payments are marked as 'failed' on the Purchased Product Payment Schedule and CPS.
		3 No payment failure comms are sent.
		4 Matching Customer Ledger payments are reversed.
		5 Reversed CL rows to be added to PPPS as contra cl
		6 The policy is automatically cancelled back to the first failed payment to the wrong account details.
	*/

	DECLARE @CustomerPaymentScheduleRows VARCHAR(2000),
			@XMLLog XML

	DECLARE @CPSRowsTable TABLE (CustomerPaymentScheduleID INT)
	DECLARE @CLMap TABLE (FromCLID INT, ToCCLID INT)
	DECLARE @PoliciesToCancel TABLE (CustomerID INT, LeadID INT, CaseID INT)

	DECLARE @UpdatedRows TABLE (Table_Name VARCHAR(100), ID INT)

	/*Get the row(s) to fail*/
	SELECT @CustomerPaymentScheduleRows = dbo.fnGetSimpleDv(313930, @MatterID)

	/*Convert the csv string to a table of values*/
	INSERT INTO @CPSRowsTable (CustomerPaymentScheduleID)
	SELECT d.AnyID
	FROM dbo.fnTableOfIDsFromCSV(@CustomerPaymentScheduleRows) d

	/*
		2a Missed payments are marked as 'failed' on the Purchased Product Payment Schedule.
	*/
	UPDATE cps
	SET PaymentStatusID = 4 /*Failed*/
	OUTPUT 'CustomerPaymentSchedule', inserted.CustomerPaymentScheduleID INTO @UpdatedRows (Table_Name, ID)
	FROM CustomerPaymentSchedule cps 
	INNER JOIN @CPSRowsTable c ON c.CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
	WHERE cps.CustomerID = @CustomerID

	/*
		2b Missed payments are marked as 'failed' on the Customer Payment Schedule.
	*/
	UPDATE ppps
	SET PaymentStatusID = 4 /*Failed*/
	OUTPUT 'PurchasedProductPaymentSchedule', inserted.PurchasedProductPaymentScheduleID INTO @UpdatedRows (Table_Name, ID)
	FROM PurchasedProductPaymentSchedule ppps 
	INNER JOIN @CPSRowsTable c ON c.CustomerPaymentScheduleID = ppps.CustomerPaymentScheduleID
	WHERE ppps.CustomerID = @CustomerID

	/*
		4 Matching Customer Ledger payments are reversed.
	*/
	INSERT INTO CustomerLedger ([ClientID], [CustomerID], [EffectivePaymentDate], [FailureCode], [FailureReason], [TransactionDate], [TransactionReference], [TransactionDescription], [TransactionNet], [TransactionVAT], [TransactionGross], [LeadEventID], [ObjectID], [ObjectTypeID], [PaymentID], [OutgoingPaymentID], [WhoCreated], [WhenCreated], [SourceID])
	OUTPUT inserted.SourceID, inserted.CustomerLedgerID INTO @CLMap(FromCLID, ToCCLID)
	SELECT cl.[ClientID], 
			cl.[CustomerID], 
			dbo.fn_GetDate_Local() AS [EffectivePaymentDate], 
			cl.[FailureCode], 
			cl.[FailureReason], 
			cl.[TransactionDate], 
			cl.[TransactionReference], 
			cl.[TransactionDescription], 
			cl.[TransactionNet]*-1, 
			cl.[TransactionVAT]*-1, 
			cl.[TransactionGross]*-1, 
			cl.[LeadEventID], 
			cl.[ObjectID], 
			cl.[ObjectTypeID], 
			cl.[PaymentID], 
			cl.[OutgoingPaymentID], 
			cl.[WhoCreated], 
			dbo.fn_GetDate_Local() AS [WhenCreated],
			cl.CustomerLedgerID AS [SourceID]
	FROM @CPSRowsTable r
	INNER JOIN CustomerPaymentSchedule cps WITH (NOLOCK) ON cps.CustomerPaymentScheduleID = r.CustomerPaymentScheduleID
	INNER JOIN CustomerLedger cl WITH (NOLOCK) ON cl.CustomerLedgerID = cps.CustomerLedgerID

	/*
		5 Reversed CL rows to be added to PPPS as contra cl
	*/
	UPDATE ppps
	SET ContraCustomerLedgerID = clm.ToCCLID
	FROM PurchasedProductPaymentSchedule ppps 
	INNER JOIN @CPSRowsTable c ON c.CustomerPaymentScheduleID = ppps.CustomerPaymentScheduleID
	INNER JOIN @CLMap clm ON clm.FromCLID = ppps.CustomerLedgerID

	/*
		6 The policy is automatically cancelled back to the first failed payment to the wrong account details.

	*/

	/*Get a distinct list of policies*/
	INSERT INTO @PoliciesToCancel(CustomerID, LeadID, CaseID)
	SELECT DISTINCT m.CustomerID, m.LeadID, m.CaseID
	FROM @CPSRowsTable c
	INNER JOIN PurchasedProductPaymentSchedule p WITH (NOLOCK) ON p.CustomerPaymentScheduleID = c.CustomerPaymentScheduleID
	INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON pp.PurchasedProductID = p.PurchasedProductID
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = pp.ObjectID

	/*Add a Policy cancelled by collections process event to each policy*/
	INSERT INTO AutomatedEventQueue ([ClientID], [CustomerID], [LeadID], [CaseID], [FromLeadEventID], [FromEventTypeID], [WhenCreated], [WhoCreated], [AutomatedEventTypeID], [RunAsUserID], [ThreadToFollowUp], [InternalPriority], BumpCount, ErrorCount)
	SELECT DISTINCT le.ClientID, p.[CustomerID], p.[LeadID], p.[CaseID], ca.LatestInProcessLeadEventID, le.EventTypeID, dbo.fn_GetDate_Local(), @WhoCreated, 155374, @WhoCreated, -1 AS [FollowupThread], 1, 0, 0
	FROM @PoliciesToCancel p
	INNER JOIN Cases ca WITH (NOLOCK) ON ca.CaseID = p.CaseID
	INNER JOIN LeadEvent le WITH (NOLOCK) ON le.LeadEventID = ca.LatestInProcessLeadEventID

	/*Lets have some logging as thats always nice*/
	SELECT @XMLLog = (
		SELECT 
			(SELECT *
			FROM @UpdatedRows
			FOR XML PATH ('UpdatedRows'), TYPE
			) AS [Rows],
			(SELECT *
			FROM @PoliciesToCancel
			FOR XML PATH ('PoliciesToCancel'), TYPE
			) AS [PoliciesToCancel],
			(SELECT *
			FROM @CLMap
			FOR XML PATH ('CLMap'), TYPE
			) AS [CLMap]
		FOR XML PATH ('Root')
	)

	EXEC dbo._C00_LogItXML 604, @MatterID, '_C600_Collections_HandleIndemnityClaim', @XMLLog

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_HandleIndemnityClaim] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Collections_HandleIndemnityClaim] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_HandleIndemnityClaim] TO [sp_executeall]
GO
