SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PortalUserCase table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserCase_Find]
(

	@SearchUsingOR bit   = null ,

	@PortalUserCaseID int   = null ,

	@PortalUserID int   = null ,

	@LeadID int   = null ,

	@CaseID int   = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PortalUserCaseID]
	, [PortalUserID]
	, [LeadID]
	, [CaseID]
	, [ClientID]
    FROM
	[dbo].[PortalUserCase] WITH (NOLOCK) 
    WHERE 
	 ([PortalUserCaseID] = @PortalUserCaseID OR @PortalUserCaseID IS NULL)
	AND ([PortalUserID] = @PortalUserID OR @PortalUserID IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PortalUserCaseID]
	, [PortalUserID]
	, [LeadID]
	, [CaseID]
	, [ClientID]
    FROM
	[dbo].[PortalUserCase] WITH (NOLOCK) 
    WHERE 
	 ([PortalUserCaseID] = @PortalUserCaseID AND @PortalUserCaseID is not null)
	OR ([PortalUserID] = @PortalUserID AND @PortalUserID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserCase_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserCase_Find] TO [sp_executeall]
GO
