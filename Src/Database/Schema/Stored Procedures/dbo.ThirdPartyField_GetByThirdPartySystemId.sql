SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyField table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyField_GetByThirdPartySystemId]
(

	@ThirdPartySystemId int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ThirdPartyFieldID],
					[ThirdPartySystemId],
					[FieldName],
					[FieldDescription],
					[IsEnabled],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ThirdPartyField] WITH (NOLOCK) 
				WHERE
					[ThirdPartySystemId] = @ThirdPartySystemId
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyField_GetByThirdPartySystemId] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyField_GetByThirdPartySystemId] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyField_GetByThirdPartySystemId] TO [sp_executeall]
GO
