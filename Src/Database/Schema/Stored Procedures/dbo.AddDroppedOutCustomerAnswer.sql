SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[AddDroppedOutCustomerAnswer]
@CustomerQuestionnaireID int,
@MasterQuestionID int,
@Answer text,
@QuestionPossibleAnswerID int,
@ClientID int

as

insert into DroppedOutCustomerAnswers ( CustomerQuestionnaireID, MasterQuestionID, Answer, QuestionPossibleAnswerID, ClientID)
values (@CustomerQuestionnaireID, @MasterQuestionID, @Answer, @QuestionPossibleAnswerID, @ClientID )



GO
GRANT VIEW DEFINITION ON  [dbo].[AddDroppedOutCustomerAnswer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddDroppedOutCustomerAnswer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddDroppedOutCustomerAnswer] TO [sp_executeall]
GO
