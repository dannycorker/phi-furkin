SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ChartSeriesType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeriesType_Insert]
(

	@ChartSeriesTypeID int   ,

	@ChartSeriesTypeName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[ChartSeriesType]
					(
					[ChartSeriesTypeID]
					,[ChartSeriesTypeName]
					)
				VALUES
					(
					@ChartSeriesTypeID
					,@ChartSeriesTypeName
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeriesType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeriesType_Insert] TO [sp_executeall]
GO
