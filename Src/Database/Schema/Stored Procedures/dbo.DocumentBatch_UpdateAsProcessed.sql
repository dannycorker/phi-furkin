SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- ==============================================================
-- Author:		Jan Wilson
-- Create date: 2012-04-10
-- Description:	Update a document batch entry to indicate that
--              all the documents have been processed.
--
-- Parameters:  @DocumentBatchId
--              The document batch entry to update
-- ==============================================================
CREATE PROCEDURE [dbo].[DocumentBatch_UpdateAsProcessed]
	@DocumentBatchId INT
AS
BEGIN
	-- Create the DocumentBatch entry
	UPDATE
		DocumentBatch
	SET
		Processed = 1
	WHERE
		DocumentBatchId = @DocumentBatchId
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatch_UpdateAsProcessed] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentBatch_UpdateAsProcessed] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatch_UpdateAsProcessed] TO [sp_executeall]
GO
