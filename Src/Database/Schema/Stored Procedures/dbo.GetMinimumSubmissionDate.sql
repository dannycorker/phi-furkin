SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetMinimumSubmissionDate]

@ClientQuestionnaireID int

AS


SELECT     ClientQuestionnaireID, MIN(SubmissionDate) AS MinSubmissionDate
FROM         dbo.CustomerQuestionnaires
GROUP BY ClientQuestionnaireID
HAVING      (ClientQuestionnaireID = @ClientQuestionnaireID)



GO
GRANT VIEW DEFINITION ON  [dbo].[GetMinimumSubmissionDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetMinimumSubmissionDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetMinimumSubmissionDate] TO [sp_executeall]
GO
