SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-10-02
-- Description:	Verifys the SAP payment data 
-- Used by:		_C00_1272_Payments_SaveSapPaymentData_ForApproval
-- Modified:	ROH 2013-05-20 Added PaymentType parameter and associated processing logic
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Payments_VerifySapPaymentData] 
(
	@Data dbo.tvpIntMoney READONLY,
	@MatterID INT,
	@PaymentType VARCHAR(1) -- 0=Payment, 1=Refund, 2=Cancellation
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PolicyLeadID INT
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@MatterID)
	
	DECLARE @ErrorMessage VARCHAR(2000)

	
	IF @PaymentType = '0'
	BEGIN
		IF EXISTS(SELECT * FROM @Data d WHERE d.Value <= 0)
		BEGIN
			SELECT @ErrorMessage = '<br /><br /><font color="red">Payments must be greater than £0.00.</font>'
			RAISERROR(@ErrorMessage,16,1)
			RETURN
		END
	END
	ELSE IF @PaymentType IN ('1','2')
	BEGIN
		IF EXISTS(SELECT * FROM @Data d WHERE d.Value >= 0)
		BEGIN
			SELECT @ErrorMessage = '<br /><br /><font color="red">Refunds/Cancellations must be less than £0.00.</font>'
			RAISERROR(@ErrorMessage,16,1)
			RETURN
		END
	END
	
	DECLARE @LeadID INT
	SELECT @LeadID = LeadID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	SELECT @ErrorMessage = '<br /><br /><font color="red">The customer name, address line 1, town and postcode cannot be empty.</font>'
	
	DECLARE @Fullname VARCHAR(2000),
			@Address1 VARCHAR(2000),
			@Postcode VARCHAR(2000),
			@Town VARCHAR(2000),
			@OK BIT
			
	SELECT @Fullname = c.Fullname, @Address1 = c.Address1, @Postcode = c.PostCode, @Town = c.Town
	FROM dbo.Lead l WITH (NOLOCK) 
	INNER JOIN dbo.Customers c WITH (NOLOCK) ON l.CustomerID = c.CustomerID
	WHERE l.LeadID = @LeadID
	
	IF @Fullname > '' AND @Address1 > '' AND @Postcode > '' AND @Town > ''
	BEGIN
		SELECT @OK = 1 -- this does nothing... just so can have the if block this way round for simplicity!
	END
	ELSE
	BEGIN
		RAISERROR(@ErrorMessage,16,1)
		RETURN
	END
	

	IF EXISTS (SELECT * FROM @Data WHERE ID = 44232) -- Pay vet
	BEGIN
		
		SELECT @Fullname = rdvOrg.DetailValue, @Address1 = rdvAddress1.DetailValue, @Postcode = rdvPostcode.DetailValue, @Town = rdvTown.DetailValue
		FROM dbo.Lead l WITH (NOLOCK) 
		LEFT JOIN dbo.LeadDetailValues ldvVet WITH (NOLOCK) ON l.LeadID = ldvVet.LeadID AND ldvVet.DetailFieldID = 146215
		LEFT JOIN dbo.ResourceListDetailValues rdvOrg WITH (NOLOCK) ON ldvVet.ValueInt = rdvOrg.ResourceListID AND rdvOrg.DetailFieldID = 144473
		LEFT JOIN dbo.ResourceListDetailValues rdvAddress1 WITH (NOLOCK) ON ldvVet.ValueInt = rdvAddress1.ResourceListID AND rdvAddress1.DetailFieldID = 144475
		LEFT JOIN dbo.ResourceListDetailValues rdvPostcode WITH (NOLOCK) ON ldvVet.ValueInt = rdvPostcode.ResourceListID AND rdvPostcode.DetailFieldID = 144479
		LEFT JOIN dbo.ResourceListDetailValues rdvTown WITH (NOLOCK) ON ldvVet.ValueInt = rdvTown.ResourceListID AND rdvTown.DetailFieldID = 144477
		WHERE l.LeadID = @PolicyLeadID
		
		SELECT @ErrorMessage = '<br /><br /><font color="red">The current vet name, address line 1, town and postcode cannot be empty.</font>'
		
		IF @Fullname > '' AND @Address1 > '' AND @Postcode > '' AND @Town > ''
		BEGIN
			SELECT @OK = 1 -- this does nothing... just so can have the if block this way round for simplicity!
		END
		ELSE
		BEGIN
			RAISERROR(@ErrorMessage,16,1)
			RETURN
		END
	END
	
	
	IF EXISTS (SELECT * FROM @Data WHERE ID = 46135) -- Pay referral vet
	BEGIN
		
		SELECT @Fullname = rdvOrg.DetailValue, @Address1 = rdvAddress1.DetailValue, @Postcode = rdvPostcode.DetailValue, @Town = rdvTown.DetailValue
		FROM dbo.Matter m WITH (NOLOCK) 
		LEFT JOIN dbo.MatterDetailValues mdvVet WITH (NOLOCK) ON m.MatterID = mdvVet.MatterID AND mdvVet.DetailFieldID IN (146236) 
		LEFT JOIN dbo.ResourceListDetailValues rdvOrg WITH (NOLOCK) ON mdvVet.ValueInt = rdvOrg.ResourceListID AND rdvOrg.DetailFieldID = 144473
		LEFT JOIN dbo.ResourceListDetailValues rdvAddress1 WITH (NOLOCK) ON mdvVet.ValueInt = rdvAddress1.ResourceListID AND rdvAddress1.DetailFieldID = 144475
		LEFT JOIN dbo.ResourceListDetailValues rdvPostcode WITH (NOLOCK) ON mdvVet.ValueInt = rdvPostcode.ResourceListID AND rdvPostcode.DetailFieldID = 144479
		LEFT JOIN dbo.ResourceListDetailValues rdvTown WITH (NOLOCK) ON mdvVet.ValueInt = rdvTown.ResourceListID AND rdvTown.DetailFieldID = 144477
		WHERE m.MatterID = @MatterID
		
		SELECT @ErrorMessage = '<br /><br /><font color="red">The referral vet name, address line 1, town and postcode cannot be empty.</font>'
		
		IF @Fullname > '' AND @Address1 > '' AND @Postcode > '' AND @Town > ''
		BEGIN
			SELECT @OK = 1 -- this does nothing... just so can have the if block this way round for simplicity!
		END
		ELSE
		BEGIN
			RAISERROR(@ErrorMessage,16,1)
			RETURN
		END
	END
	

	/*SA 2018-03-15 - added out of hours VET*/
	IF EXISTS (SELECT * FROM @Data WHERE ID = 76367) /* Pay Out of Hours VET*/
	BEGIN
		
		SELECT @Fullname = rdvOrg.DetailValue, @Address1 = rdvAddress1.DetailValue, @Postcode = rdvPostcode.DetailValue, @Town = rdvTown.DetailValue
		FROM dbo.Matter m WITH (NOLOCK) 
		LEFT JOIN dbo.MatterDetailValues mdvVet WITH (NOLOCK) ON m.MatterID = mdvVet.MatterID AND mdvVet.DetailFieldID IN (176944) 
		LEFT JOIN dbo.ResourceListDetailValues rdvOrg WITH (NOLOCK) ON mdvVet.ValueInt = rdvOrg.ResourceListID AND rdvOrg.DetailFieldID = 144473
		LEFT JOIN dbo.ResourceListDetailValues rdvAddress1 WITH (NOLOCK) ON mdvVet.ValueInt = rdvAddress1.ResourceListID AND rdvAddress1.DetailFieldID = 144475
		LEFT JOIN dbo.ResourceListDetailValues rdvPostcode WITH (NOLOCK) ON mdvVet.ValueInt = rdvPostcode.ResourceListID AND rdvPostcode.DetailFieldID = 144479
		LEFT JOIN dbo.ResourceListDetailValues rdvTown WITH (NOLOCK) ON mdvVet.ValueInt = rdvTown.ResourceListID AND rdvTown.DetailFieldID = 144477
		WHERE m.MatterID = @MatterID
		
		SELECT @ErrorMessage = '<br /><br /><font color="red">The out of hours vet name, address line 1, town and postcode cannot be empty.</font>'
		
		IF @Fullname > '' AND @Address1 > '' AND @Postcode > '' AND @Town > ''
		BEGIN
			SELECT @OK = 1 -- this does nothing... just so can have the if block this way round for simplicity!
		END
		ELSE
		BEGIN
			RAISERROR(@ErrorMessage,16,1)
			RETURN
		END
	END
	


	IF EXISTS (SELECT * FROM @Data WHERE ID = 44233) -- Pay third party
	BEGIN
		
		SELECT	@Fullname = CASE WHEN mdvType.DetailValue = '2' THEN mdvOrganisation.DetailValue ELSE ISNULL(mdvFirst.DetailValue, '') + ' ' + ISNULL(mdvLast.DetailValue, '') END, 
				@Address1 = mdvAddress1.DetailValue, @Postcode = mdvPostcode.DetailValue, @Town = mdvTown.DetailValue
		FROM dbo.Matter m WITH (NOLOCK) 
		LEFT JOIN dbo.MatterDetailValues mdvFirst WITH (NOLOCK) ON m.MatterID = mdvFirst.MatterID AND mdvFirst.DetailFieldID = 157808
		LEFT JOIN dbo.MatterDetailValues mdvLast WITH (NOLOCK) ON m.MatterID = mdvLast.MatterID AND mdvLast.DetailFieldID = 157809
		LEFT JOIN dbo.MatterDetailValues mdvType WITH (NOLOCK) ON m.MatterID = mdvType.MatterID AND mdvType.DetailFieldID = 157817
		LEFT JOIN dbo.MatterDetailValues mdvOrganisation WITH (NOLOCK) ON m.MatterID = mdvOrganisation.MatterID AND mdvOrganisation.DetailFieldID = 157818
		LEFT JOIN dbo.MatterDetailValues mdvAddress1 WITH (NOLOCK) ON m.MatterID = mdvAddress1.MatterID AND mdvAddress1.DetailFieldID = 157810
		LEFT JOIN dbo.MatterDetailValues mdvPostcode WITH (NOLOCK) ON m.MatterID = mdvPostcode.MatterID AND mdvPostcode.DetailFieldID = 157815
		LEFT JOIN dbo.MatterDetailValues mdvTown WITH (NOLOCK) ON m.MatterID = mdvTown.MatterID AND mdvTown.DetailFieldID = 157813
		WHERE m.MatterID = @MatterID
		
		SELECT @ErrorMessage = '<br /><br /><font color="red">The third party name, address line 1, town and postcode cannot be empty.</font>'
		
		IF @Fullname > '' AND @Address1 > '' AND @Postcode > '' AND @Town > ''
		BEGIN
			SELECT @OK = 1 -- this does nothing... just so can have the if block this way round for simplicity!
		END
		ELSE
		BEGIN
			RAISERROR(@ErrorMessage,16,1)
			RETURN
		END
	END
	
	
	
	

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Payments_VerifySapPaymentData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Payments_VerifySapPaymentData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Payments_VerifySapPaymentData] TO [sp_executeall]
GO
