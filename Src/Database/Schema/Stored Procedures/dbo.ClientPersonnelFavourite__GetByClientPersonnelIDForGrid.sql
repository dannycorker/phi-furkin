SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnelFavourite table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelFavourite__GetByClientPersonnelIDForGrid]
(

	@ClientPersonnelID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					cpf.ClientPersonnelFavouriteID,
					cpf.ClientID,
					cpf.ClientPersonnelID,
					cpf.LeadID,
					c.CustomerID,
					c.Fullname,
					lt.LeadTypeName
				FROM
					[dbo].[ClientPersonnelFavourite] cpf WITH (NOLOCK)
				INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = cpf.LeadID
				INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID
				INNER JOIN dbo.Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID
				WHERE
					cpf.ClientPersonnelID = @ClientPersonnelID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelFavourite__GetByClientPersonnelIDForGrid] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelFavourite__GetByClientPersonnelIDForGrid] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelFavourite__GetByClientPersonnelIDForGrid] TO [sp_executeall]
GO
