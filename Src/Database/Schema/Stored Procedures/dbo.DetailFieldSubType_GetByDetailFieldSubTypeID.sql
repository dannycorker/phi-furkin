SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailFieldSubType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldSubType_GetByDetailFieldSubTypeID]
(

	@DetailFieldSubTypeID tinyint   
)
AS


				SELECT
					[DetailFieldSubTypeID],
					[DetailFieldSubTypeName],
					[DetailFieldSubTypeDescription],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[DetailFieldSubType] WITH (NOLOCK) 
				WHERE
										[DetailFieldSubTypeID] = @DetailFieldSubTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldSubType_GetByDetailFieldSubTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldSubType_GetByDetailFieldSubTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldSubType_GetByDetailFieldSubTypeID] TO [sp_executeall]
GO
