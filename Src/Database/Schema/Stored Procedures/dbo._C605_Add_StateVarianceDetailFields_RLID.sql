SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds / Alexandra Maguire
-- Create date: 2020-09-13
-- Description: Sets the valueint of DetailFieldID: 314232 'State Variance Detail Fields'(Policy Admin Matter, Internal)
-- =============================================
CREATE PROCEDURE [dbo].[_C605_Add_StateVarianceDetailFields_RLID]
	/*switches*/
	@ProductId INT,
	@ExamFees INT,
	@WorkingDog INT,
	@Wellness INT,
	@MatterID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ClientID INT = (SELECT ClientID FROM Matter WITH (NOLOCK) WHERE MatterID = @MatterID)

	IF @ClientID <> 605 /*For PPET ONLY*/
	BEGIN

		RETURN;

	END

    DECLARE @StateVarianceDetailFieldsRLID INT
	
	SELECT @ExamFees = CASE @ExamFees WHEN 5145 THEN 1 ELSE 0 END /*No = 1*/
	SELECT @WorkingDog = CASE @WorkingDog WHEN 5144 THEN 1 ELSE 0 END /*Yes = 1*/
	SELECT @Wellness = CASE @Wellness WHEN 5144 THEN 1 ELSE 0 END /*Yes = 1*/

	/*Accident*/
	IF @ProductId = 2001386
	BEGIN

		IF @ExamFees = 1 AND @WorkingDog = 1 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002269
		END
		ELSE IF @ExamFees = 1 AND @WorkingDog = 1 AND @Wellness = 0
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002271
		END
		ELSE IF @ExamFees = 1 AND @WorkingDog = 0 AND @Wellness = 0
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002265
		END
		ELSE IF @ExamFees = 0 AND @WorkingDog = 1 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002268
		END
		ELSE IF @ExamFees = 0 AND @WorkingDog = 0 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002266
		END
		ELSE IF @ExamFees = 1 AND @WorkingDog = 0 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002267
		END
		ELSE IF @ExamFees = 0 AND @WorkingDog = 1 AND @Wellness = 0
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002270
		END
		ELSE /* @ExamFees = 0 AND @WorkingDog = 0 AND @Wellness = 0 */
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002264
		END
	
	END

	/*Essential*/
	IF @ProductId = 2001387
	BEGIN

		IF @ExamFees = 1 AND @WorkingDog = 1 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002277
		END
		ELSE IF @ExamFees = 1 AND @WorkingDog = 1 AND @Wellness = 0
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002279
		END
		ELSE IF @ExamFees = 1 AND @WorkingDog = 0 AND @Wellness = 0
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002273
		END
		ELSE IF @ExamFees = 0 AND @WorkingDog = 1 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002276
		END
		ELSE IF @ExamFees = 0 AND @WorkingDog = 0 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002274
		END
		ELSE IF @ExamFees = 1 AND @WorkingDog = 0 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002275
		END
		ELSE IF @ExamFees = 0 AND @WorkingDog = 1 AND @Wellness = 0
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002278
		END
		ELSE /* @ExamFees = 0 AND @WorkingDog = 0 AND @Wellness = 0 */
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002272
		END

	END

	/*Preferred*/
	IF @ProductId = 2001388
	BEGIN

		IF @ExamFees = 1 AND @WorkingDog = 1 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002285
		END
		ELSE IF @ExamFees = 1 AND @WorkingDog = 1 AND @Wellness = 0
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002287
		END
		ELSE IF @ExamFees = 1 AND @WorkingDog = 0 AND @Wellness = 0
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002281
		END
		ELSE IF @ExamFees = 0 AND @WorkingDog = 1 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002284
		END
		ELSE IF @ExamFees = 0 AND @WorkingDog = 0 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002282
		END
		ELSE IF @ExamFees = 1 AND @WorkingDog = 0 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002283
		END
		ELSE IF @ExamFees = 0 AND @WorkingDog = 1 AND @Wellness = 0
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002286
		END
		ELSE /* @ExamFees = 0 AND @WorkingDog = 0 AND @Wellness = 0 */
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002280
		END

	END

	/*Ultimate*/
	IF @ProductId = 2001389
	BEGIN

		IF @ExamFees = 1 AND @WorkingDog = 1 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002293
		END
		ELSE IF @ExamFees = 1 AND @WorkingDog = 1 AND @Wellness = 0
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002295
		END
		ELSE IF @ExamFees = 1 AND @WorkingDog = 0 AND @Wellness = 0
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002289
		END
		ELSE IF @ExamFees = 0 AND @WorkingDog = 1 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002292
		END
		ELSE IF @ExamFees = 0 AND @WorkingDog = 0 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002290
		END
		ELSE IF @ExamFees = 1 AND @WorkingDog = 0 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002291
		END
		ELSE IF @ExamFees = 0 AND @WorkingDog = 1 AND @Wellness = 0
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002294
		END
		ELSE /* @ExamFees = 0 AND @WorkingDog = 0 AND @Wellness = 0 */
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002288
		END

	END

	/*Ultimate Plus*/
	IF @ProductId = 2001390
	BEGIN

		IF @ExamFees = 1 AND @WorkingDog = 1 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002301
		END
		ELSE IF @ExamFees = 1 AND @WorkingDog = 1 AND @Wellness = 0
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002303
		END
		ELSE IF @ExamFees = 1 AND @WorkingDog = 0 AND @Wellness = 0
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002297
		END
		ELSE IF @ExamFees = 0 AND @WorkingDog = 1 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002300
		END
		ELSE IF @ExamFees = 0 AND @WorkingDog = 0 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002298
		END
		ELSE IF @ExamFees = 1 AND @WorkingDog = 0 AND @Wellness = 1
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002299
		END
		ELSE IF @ExamFees = 0 AND @WorkingDog = 1 AND @Wellness = 0
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002302
		END
		ELSE /* @ExamFees = 0 AND @WorkingDog = 0 AND @Wellness = 0 */
		BEGIN
			SELECT @StateVarianceDetailFieldsRLID = 2002296
		END

	END

	IF @StateVarianceDetailFieldsRLID IS NOT NULL
	BEGIN
		EXEC _C00_SimpleValueIntoField 314232, @StateVarianceDetailFieldsRLID, @MatterID, 58552 /*Aquarium Automation*/
	END
	ELSE
	BEGIN
		RETURN;
	END

END
GO
