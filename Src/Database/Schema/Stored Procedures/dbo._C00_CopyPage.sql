SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-06-24
-- Description:	Copy Pages and Fields (including Customer level pages)
-- =============================================
/*Commented added by TD 2015-01-07: This will not copy  across basic tables, you must modify the proc or manually edit the detail fields to select the same tables*/
CREATE PROCEDURE [dbo].[_C00_CopyPage]
@FromPageID int,
@ToLeadTypeID int,
@DestinationClientID int = NULL 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NewDetailFieldPageID int,
			@PageName varchar(2000),
			@PageCaption varchar(2000),
			@LeadOrMatter int,
			@PageOrder int,
			@ResourceList int			

	-- at least one of to lead type ID or client ID must be non-null
	if not (@ToLeadTypeID is null and @DestinationClientID is NULL)
	begin
	
		if @ToLeadTypeID is null
			select @ToLeadTypeID = 0
		
		if @DestinationClientID is NULL
		Begin
			Select @DestinationClientID = ClientID
			From LeadType lt with (NOLOCK)
			Where lt.LeadTypeID = @ToLeadTypeID
		end
			
		Select @PageName = PageName, @PageCaption = PageCaption, @LeadOrMatter = dfp.LeadOrMatter, @PageOrder = dfp.PageOrder, @ResourceList = dfp.ResourceList
		From DetailFieldPages dfp WITH (NOLOCK)
		Where dfp.DetailFieldPageID = @FromPageID

		Insert Into DetailFieldPages (ClientID, Enabled, LeadOrMatter, LeadTypeID, PageCaption, PageName, PageOrder, ResourceList)
		Select @DestinationClientID, 1, @LeadOrMatter, @ToLeadTypeID, @PageCaption, @PageName, @PageOrder, @ResourceList

		Select @NewDetailFieldPageID = SCOPE_IDENTITY()

		Insert Into DetailFields (ClientID,LeadOrMatter,FieldName,FieldCaption,QuestionTypeID,Required,Lookup,LookupListID,LeadTypeID,Enabled,DetailFieldPageID,FieldOrder,MaintainHistory,EquationText,MasterQuestionID,FieldSize,LinkedDetailFieldID,ValidationCriteriaFieldTypeID,ValidationCriteriaID,MinimumValue,MaximumValue,RegEx,ErrorMessage,ResourceListDetailFieldPageID,TableDetailFieldPageID,DefaultFilter,ColumnEquationText,Editable,Hidden,LastReferenceInteger,ReferenceValueFormatID,Encrypt,ShowCharacters,NumberOfCharactersToShow,TableEditMode,DisplayInTableView)
		Select @DestinationClientID,LeadOrMatter,FieldName,FieldCaption,QuestionTypeID,Required,Lookup,NULL,@ToLeadTypeID,case when QuestionTypeID = 10 then 0 else Enabled end,@NewDetailFieldPageID,FieldOrder,MaintainHistory,EquationText,MasterQuestionID,FieldSize,LinkedDetailFieldID,ValidationCriteriaFieldTypeID,ValidationCriteriaID,MinimumValue,MaximumValue,RegEx,ErrorMessage,NULL,NULL,DefaultFilter,ColumnEquationText,Editable,Hidden,LastReferenceInteger,ReferenceValueFormatID,Encrypt,ShowCharacters,NumberOfCharactersToShow,TableEditMode,DisplayInTableView
		From DetailFields
		Where DetailFieldPageID = @FromPageID
		and Enabled = 1
		
		RETURN @NewDetailFieldPageID
		
	end
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyPage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CopyPage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CopyPage] TO [sp_executeall]
GO
