SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the UserFunctionControlEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserFunctionControlEditing_Update]
(

	@UserFunctionControlEditingID int   ,

	@ClientPersonnelID int   ,

	@ModuleID int   ,

	@FunctionTypeID int   ,

	@HasDescendants int   ,

	@RightID int   ,

	@LeadTypeID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[UserFunctionControlEditing]
				SET
					[ClientPersonnelID] = @ClientPersonnelID
					,[ModuleID] = @ModuleID
					,[FunctionTypeID] = @FunctionTypeID
					,[HasDescendants] = @HasDescendants
					,[RightID] = @RightID
					,[LeadTypeID] = @LeadTypeID
				WHERE
[UserFunctionControlEditingID] = @UserFunctionControlEditingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControlEditing_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserFunctionControlEditing_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControlEditing_Update] TO [sp_executeall]
GO
