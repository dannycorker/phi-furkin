SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 2014-03-31
-- Description:	Get SMS Complaint configuaration 
-- PR Modified 16-05-2014 added  sms group table
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetComplaintScreenConfiguration]

	@ClientID INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @PolicyNumberDetailFieldID INT

    -- Insert statements for procedure here
	SELECT @PolicyNumberDetailFieldID=DetailFieldID
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=1 AND ThirdPartyFieldID=834
	
	DECLARE @TableDetailFieldID INT
	DECLARE @LeadTypeIdentDetailFieldID INT
	DECLARE @SMSEventTypeIDDetailFieldID INT

	SELECT @TableDetailFieldID=DetailFieldID, @LeadTypeIdentDetailFieldID=ColumnFieldID
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=1 AND ThirdPartyFieldID=835
	
	SELECT @SMSEventTypeIDDetailFieldID=ColumnFieldID
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=1 AND ThirdPartyFieldID=835
	
	DECLARE @DocumentTypeIDTableDetailField INT
	
	SELECT @DocumentTypeIDTableDetailField=DetailFieldID, @LeadTypeIdentDetailFieldID=ColumnFieldID
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=1 AND ThirdPartyFieldID=837
	
	DECLARE @SMSGroupDetailTableFieldID INT
	SELECT @SMSGroupDetailTableFieldID=DetailFieldID, @LeadTypeIdentDetailFieldID=ColumnFieldID
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND LeadTypeID=1 AND ThirdPartyFieldID=866
	
	
	SELECT * FROM ClientDetailValues WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND DetailFieldID=@PolicyNumberDetailFieldID
	
	EXECUTE dbo.TableRows__GetByDetailFieldIDLeadIDMatterID @TableDetailFieldID, NULL,NULL,NULL,NULL,1,8,NULL,NULL, @ClientID, NULL,NULL
	
	EXECUTE dbo.TableRows__GetByDetailFieldIDLeadIDMatterID @DocumentTypeIDTableDetailField, NULL,NULL,NULL,NULL,1,8,NULL,NULL, @ClientID, NULL,NULL
	
	IF @SMSEventTypeIDDetailFieldID IS NOT NULL
	BEGIN
	
		EXECUTE dbo.TableRows__GetByDetailFieldIDLeadIDMatterID @SMSGroupDetailTableFieldID, NULL,NULL,NULL,NULL,1,8,NULL,NULL, @ClientID, NULL,NULL
	
	END
	
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetComplaintScreenConfiguration] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetComplaintScreenConfiguration] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetComplaintScreenConfiguration] TO [sp_executeall]
GO
