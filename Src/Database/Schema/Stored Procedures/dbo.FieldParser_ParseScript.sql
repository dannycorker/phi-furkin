SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2014-06-24
-- Description:	New for #28942 Call Centre Script parsing
-- Taken from FieldParser_ParseTemplate, which was too large and unwieldy to adapt, maintain and test.
-- Modified by PR: Added SectionID
-- Modified by PR: Added QuestionTypeID on 13/07/2015
-- Modified by PR: Added QuestionTypeID to Table Select Statements on 19/10/2015
-- =============================================
CREATE PROCEDURE [dbo].[FieldParser_ParseScript] 
	@ClientPersonnelID INT, 
	@CustomerID INT, 
	@LeadID INT, 
	@CaseID INT, 
	@MatterID INT, 
	@LanguageID INT = 1, 
	@ScriptID INT 
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @LeadTypeID INT, 
	@ClientID INT, 
	@ClientOfficeID INT, 
	@CaseOwnerID INT, 
	@CaseOwnerManagerID INT, 
	@ClientPersonnelManagerID INT, 
	@ClientPersonnelManagerOfficeID INT, 
	@ContactID INT, 
	@CustomerOfficeID INT, 
	@DepartmentID INT, 
	@PartnerID INT, 
	@TargetDetailFieldSubtypeID INT  

	/* For storing the output before sending it all back to the caller */
	DECLARE @Subs TABLE (
		DetailFieldSubtypeID TINYINT NULL,
		DetailFieldID INT NULL,
		RawName VARCHAR(2000), 
		[Target] VARCHAR(2000),
		SectionID INT,  
		RealValue VARCHAR(2000),
		QuestionTypeID INT NULL
	)
	
	/* For storing TableDetailValues output before sending it all back to the caller */
	DECLARE @TDVSubs TABLE (
		PageDetailFieldID INT, 
		PageFieldName VARCHAR(50),
		PageFieldCaption VARCHAR(100), 
		ColumnDetailFieldID INT, 
		ColumnFieldName VARCHAR(50),
		ColumnFieldCaption VARCHAR(100), 
		TableRowID INT,
		FieldOrder INT,
		DetailValue VARCHAR(2000),
		QuestionTypeID INT
	)
	
	
	/* For storing all the field targets with extra lookup info */
	DECLARE @DetailFieldTargets TABLE (
		DetailFieldID INT NOT NULL,
		DetailFieldSubtypeID INT NOT NULL,
		LookupListID INT NULL, 
		[Target] VARCHAR(250) NOT NULL,
		SectionID INT, 
		DetailValue VARCHAR(2000) NULL, 
		QuestionTypeID INT NOT NULL, 
		TableDetailFieldPageID INT NULL
	)
	
	/* Put all targets into one table first, joining to DetailFields so we only have to look them up once */
	INSERT @DetailFieldTargets (DetailFieldID, DetailFieldSubtypeID, LookupListID, [Target], SectionID, QuestionTypeID, TableDetailFieldPageID) 
	SELECT DISTINCT df.DetailFieldID, df.LeadOrMatter, df.LookupListID, s.[Target], s.SectionID, df.QuestionTypeID, df.TableDetailFieldPageID 
	FROM dbo.ScriptDetailFieldTarget s WITH (NOLOCK) 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON s.DetailFieldID = df.DetailFieldID 
	WHERE s.ScriptID = @ScriptID 
	
	
	/* Gather other data about the current user */
	SELECT @ClientID = cp.ClientID, 
	@ClientOfficeID = cp.ClientOfficeID, 
	@ClientPersonnelManagerID = cp.ManagerID, 
	@ClientPersonnelManagerOfficeID = cpm.ClientOfficeID 
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	LEFT JOIN dbo.ClientPersonnel cpm WITH (NOLOCK) ON cpm.ClientPersonnelID = cp.ManagerID 
	WHERE cp.ClientPersonnelID = @ClientPersonnelID 
	
	
	/* Gather other data about Contact, Partner, Office and Case Owner */
	SELECT TOP (1) 
	@ContactID = c.DefaultContactID, 
	@CustomerOfficeID = c.DefaultOfficeID, 
	@LeadTypeID = l.LeadTypeID, 
	@PartnerID = p.PartnerID, 
	@CaseOwnerID = l.AssignedTo, 
	@CaseOwnerManagerID = cpm.ManagerID  
	FROM dbo.Lead l WITH (NOLOCK) 
	INNER JOIN dbo.Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID 
	LEFT JOIN dbo.[Partner] p WITH (NOLOCK) ON p.CustomerID = c.CustomerID 
	LEFT JOIN dbo.ClientPersonnel cpm WITH (NOLOCK) ON cpm.ClientPersonnelID = l.AssignedTo 
	WHERE l.LeadID = @LeadID 
	
	
	/*******************************************************************************************************************/
	/*                                                                                                                 */
	/*										            DetailField targets                                            */
	/*                                                                                                                 */
	/*******************************************************************************************************************/
	
	IF @ScriptID IS NOT NULL
	BEGIN
	
		/* LeadDetailValues */
		SET @TargetDetailFieldSubtypeID = 1
		
		INSERT INTO @Subs(DetailFieldSubtypeID, DetailFieldID, RawName, [Target], SectionID, RealValue, QuestionTypeID) 
		SELECT @TargetDetailFieldSubtypeID, 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], ddft.SectionID,
		CASE WHEN ddft.LookupListID > 0 THEN COALESCE (t.Translation, luli.ItemValue, '') ELSE COALESCE (dv.DetailValue, '') END AS [DetailValue],
		ddft.QuestionTypeID
		FROM @DetailFieldTargets ddft 
		LEFT JOIN dbo.LeadDetailValues AS dv WITH (NOLOCK) ON dv.DetailFieldID = ddft.DetailFieldID AND dv.LeadID = @LeadID 
		LEFT JOIN dbo.LookupListItems AS luli WITH (NOLOCK) ON luli.LookupListID = ddft.LookupListID AND luli.LookupListItemID = dv.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
		/*AND ddft.QuestionTypeID NOT IN (19) Leave tables in the result set so that the app knows it has basic table work to do */
		
		
		/* MatterDetailValues */
		SET @TargetDetailFieldSubtypeID = 2
		
		INSERT INTO @Subs(DetailFieldSubtypeID, DetailFieldID, RawName, [Target], SectionID, RealValue, QuestionTypeID) 
		SELECT @TargetDetailFieldSubtypeID, 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], ddft.SectionID,
		CASE WHEN ddft.LookupListID > 0 THEN COALESCE (t.Translation, luli.ItemValue, '') ELSE COALESCE (dv.DetailValue, '') END AS [DetailValue],
		ddft.QuestionTypeID
		FROM @DetailFieldTargets ddft 
		LEFT JOIN dbo.MatterDetailValues AS dv WITH (NOLOCK) ON dv.DetailFieldID = ddft.DetailFieldID AND dv.MatterID = @MatterID 
		LEFT JOIN dbo.LookupListItems AS luli WITH (NOLOCK) ON luli.LookupListID = ddft.LookupListID AND luli.LookupListItemID = dv.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 

		
		/* CustomerDetailValues */
		SET @TargetDetailFieldSubtypeID = 10
		
		INSERT INTO @Subs(DetailFieldSubtypeID, DetailFieldID, RawName, [Target], SectionID, RealValue, QuestionTypeID) 
		SELECT @TargetDetailFieldSubtypeID, 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], ddft.SectionID, 
		CASE WHEN ddft.LookupListID > 0 THEN COALESCE (t.Translation, luli.ItemValue, '') ELSE COALESCE (dv.DetailValue, '') END AS [DetailValue],
		ddft.QuestionTypeID
		FROM @DetailFieldTargets ddft 
		LEFT JOIN dbo.CustomerDetailValues AS dv WITH (NOLOCK) ON dv.DetailFieldID = ddft.DetailFieldID AND dv.CustomerID = @CustomerID 
		LEFT JOIN dbo.LookupListItems AS luli WITH (NOLOCK) ON luli.LookupListID = ddft.LookupListID AND luli.LookupListItemID = dv.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 


		/* CaseDetailValues */
		SET @TargetDetailFieldSubtypeID = 11
		
		INSERT INTO @Subs(DetailFieldSubtypeID, DetailFieldID, RawName, [Target], SectionID, RealValue, QuestionTypeID) 
		SELECT @TargetDetailFieldSubtypeID, 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], ddft.SectionID, 
		CASE WHEN ddft.LookupListID > 0 THEN COALESCE (t.Translation, luli.ItemValue, '') ELSE COALESCE (dv.DetailValue, '') END AS [DetailValue],
		ddft.QuestionTypeID
		FROM @DetailFieldTargets ddft 
		LEFT JOIN dbo.CaseDetailValues AS dv WITH (NOLOCK) ON dv.DetailFieldID = ddft.DetailFieldID AND dv.CaseID = @CaseID 
		LEFT JOIN dbo.LookupListItems AS luli WITH (NOLOCK) ON luli.LookupListID = ddft.LookupListID AND luli.LookupListItemID = dv.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 


		/* ClientDetailValues */
		SET @TargetDetailFieldSubtypeID = 12
		
		INSERT INTO @Subs(DetailFieldSubtypeID, DetailFieldID, RawName, [Target], SectionID, RealValue, QuestionTypeID) 
		SELECT @TargetDetailFieldSubtypeID, 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], ddft.SectionID, 
		CASE WHEN ddft.LookupListID > 0 THEN COALESCE (t.Translation, luli.ItemValue, '') ELSE COALESCE (dv.DetailValue, '') END AS [DetailValue],
		ddft.QuestionTypeID
		FROM @DetailFieldTargets ddft 
		LEFT JOIN dbo.ClientDetailValues AS dv WITH (NOLOCK) ON dv.DetailFieldID = ddft.DetailFieldID AND dv.ClientID = @ClientID 
		LEFT JOIN dbo.LookupListItems AS luli WITH (NOLOCK) ON luli.LookupListID = ddft.LookupListID AND luli.LookupListItemID = dv.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 


		/* ClientPersonnelDetailValues */
		SET @TargetDetailFieldSubtypeID = 13
		
		INSERT INTO @Subs(DetailFieldSubtypeID, DetailFieldID, RawName, [Target], SectionID, RealValue, QuestionTypeID) 
		SELECT @TargetDetailFieldSubtypeID, 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], ddft.SectionID, 
		CASE WHEN ddft.LookupListID > 0 THEN COALESCE (t.Translation, luli.ItemValue, '') ELSE COALESCE (dv.DetailValue, '') END AS [DetailValue],
		ddft.QuestionTypeID
		FROM @DetailFieldTargets ddft 
		LEFT JOIN dbo.ClientPersonnelDetailValues AS dv WITH (NOLOCK) ON dv.DetailFieldID = ddft.DetailFieldID AND dv.ClientPersonnelID = @ClientPersonnelID 
		LEFT JOIN dbo.LookupListItems AS luli WITH (NOLOCK) ON luli.LookupListID = ddft.LookupListID AND luli.LookupListItemID = dv.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 


		/* ContactDetailValues */
		SET @TargetDetailFieldSubtypeID = 14
		
		INSERT INTO @Subs(DetailFieldSubtypeID, DetailFieldID, RawName, [Target], SectionID, RealValue, QuestionTypeID) 
		SELECT @TargetDetailFieldSubtypeID, 
		ddft.[DetailFieldID], 
		ddft.[DetailFieldID], 
		ddft.[Target], ddft.SectionID, 
		CASE WHEN ddft.LookupListID > 0 THEN COALESCE (t.Translation, luli.ItemValue, '') ELSE COALESCE (dv.DetailValue, '') END AS [DetailValue],
		ddft.QuestionTypeID
		FROM @DetailFieldTargets ddft 
		LEFT JOIN dbo.ContactDetailValues AS dv WITH (NOLOCK) ON dv.DetailFieldID = ddft.DetailFieldID AND dv.ContactID = @ContactID 
		LEFT JOIN dbo.LookupListItems AS luli WITH (NOLOCK) ON luli.LookupListID = ddft.LookupListID AND luli.LookupListItemID = dv.ValueInt 
		LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
		WHERE ddft.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
		
		
		/*
			TableDetailValues section
			
			This pattern repeats for all the DetailFieldSubtypeID below:
			
			1) The script contains a DetailFieldID, which is the placeholder field on the page, eg "Jim's Spending"
			   This info gets stored in PageDetailFieldID, PageFieldName, PageFieldCaption
			   
			2) There is a table definition, possibly used by many fields, possibly even on the same Lead page 
			   and we need to get all those DetailFieldIDs in FieldOrder (stored in ColumnDetailFieldID, ColumnFieldName, ColumnFieldCaption)
			   
			3) Lookup list values get decoded into the actual text
			
			4) Translation is then applied to the lookup list value if one exists
		*/
		IF EXISTS(
			SELECT * 
			FROM @DetailFieldTargets ddft 
			WHERE ddft.QuestionTypeID IN (19)
		)
		BEGIN
			/*
				First we need to create empty definition records for each DetailField where the QuestionTypeID is 19 (basic table)
				These are required so that the app can "draw" the table even if no TableDetailValues exist for it
			*/
			INSERT INTO @TDVSubs(PageDetailFieldID, PageFieldName, PageFieldCaption, ColumnDetailFieldID, ColumnFieldName, ColumnFieldCaption, TableRowID, FieldOrder, DetailValue, QuestionTypeID)
			SELECT DISTINCT df_Page.DetailFieldID, df_Page.FieldName, df_Page.FieldCaption, 
			df_Definition.DetailFieldID, df_Definition.FieldName, df_Definition.FieldCaption, 
			0 AS TableRowID, 
			df_Definition.FieldOrder, 
			'' AS DetailValue, df_Definition.QuestionTypeID 
			FROM @DetailFieldTargets ddft 
			INNER JOIN dbo.DetailFields df_Page WITH (NOLOCK) ON df_Page.DetailFieldID = ddft.DetailFieldID /* There will be one of these */
			INNER JOIN dbo.DetailFields df_Definition WITH (NOLOCK) ON df_Definition.DetailFieldPageID = ddft.TableDetailFieldPageID /* There will be many of these */
			WHERE ddft.QuestionTypeID IN (19) 
			
			
			
			/* Lead TDV */
			SET @TargetDetailFieldSubtypeID = 1
		
			INSERT INTO @TDVSubs(PageDetailFieldID, PageFieldName, PageFieldCaption, ColumnDetailFieldID, ColumnFieldName, ColumnFieldCaption, TableRowID, FieldOrder, DetailValue, QuestionTypeID)
			SELECT DISTINCT df_Page.DetailFieldID, df_Page.FieldName, df_Page.FieldCaption, 
			df_Definition.DetailFieldID, df_Definition.FieldName, df_Definition.FieldCaption, 
			tr.TableRowID, 
			df_Definition.FieldOrder, 
			CASE WHEN df_Definition.LookupListID > 0 THEN COALESCE (t.Translation, luli.ItemValue, '') ELSE COALESCE (tdv.DetailValue, '') END AS [DetailValue], df_Definition.QuestionTypeID 
			FROM @DetailFieldTargets ddft 
			INNER JOIN dbo.DetailFields df_Page WITH (NOLOCK) ON df_Page.DetailFieldID = ddft.DetailFieldID /* There will be one of these */
			INNER JOIN dbo.TableRows tr WITH (NOLOCK) ON tr.DetailFieldID = ddft.DetailFieldID AND tr.LeadID = @LeadID 
			INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID 
			INNER JOIN dbo.DetailFields df_Definition WITH (NOLOCK) ON df_Definition.DetailFieldID = tdv.DetailFieldID 
			LEFT JOIN dbo.LookupListItems AS luli WITH (NOLOCK) ON luli.LookupListID = df_Definition.LookupListID AND luli.LookupListItemID = tdv.ValueInt 
			LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
			WHERE ddft.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND ddft.QuestionTypeID IN (19) 
			
			
			/* Matter TDV */
			SET @TargetDetailFieldSubtypeID = 2
			
			INSERT INTO @TDVSubs(PageDetailFieldID, PageFieldName, PageFieldCaption, ColumnDetailFieldID, ColumnFieldName, ColumnFieldCaption, TableRowID, FieldOrder, DetailValue, QuestionTypeID)
			SELECT DISTINCT df_Page.DetailFieldID, df_Page.FieldName, df_Page.FieldCaption, 
			df_Definition.DetailFieldID, df_Definition.FieldName, df_Definition.FieldCaption, 
			tr.TableRowID, 
			df_Definition.FieldOrder, 
			CASE WHEN df_Definition.LookupListID > 0 THEN COALESCE (t.Translation, luli.ItemValue, '') ELSE COALESCE (tdv.DetailValue, '') END AS [DetailValue], df_Definition.QuestionTypeID
			FROM @DetailFieldTargets ddft 
			INNER JOIN dbo.DetailFields df_Page WITH (NOLOCK) ON df_Page.DetailFieldID = ddft.DetailFieldID /* There will be one of these */
			INNER JOIN dbo.TableRows tr WITH (NOLOCK) ON tr.DetailFieldID = ddft.DetailFieldID AND tr.MatterID = @MatterID 
			INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID 
			INNER JOIN dbo.DetailFields df_Definition WITH (NOLOCK) ON df_Definition.DetailFieldID = tdv.DetailFieldID 
			LEFT JOIN dbo.LookupListItems AS luli WITH (NOLOCK) ON luli.LookupListID = df_Definition.LookupListID AND luli.LookupListItemID = tdv.ValueInt 
			LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
			WHERE ddft.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND ddft.QuestionTypeID IN (19) 
			
			
			/* Customer TDV */
			SET @TargetDetailFieldSubtypeID = 10
			
			INSERT INTO @TDVSubs(PageDetailFieldID, PageFieldName, PageFieldCaption, ColumnDetailFieldID, ColumnFieldName, ColumnFieldCaption, TableRowID, FieldOrder, DetailValue, QuestionTypeID)
			SELECT DISTINCT df_Page.DetailFieldID, df_Page.FieldName, df_Page.FieldCaption, 
			df_Definition.DetailFieldID, df_Definition.FieldName, df_Definition.FieldCaption, 
			tr.TableRowID, 
			df_Definition.FieldOrder, 
			CASE WHEN df_Definition.LookupListID > 0 THEN COALESCE (t.Translation, luli.ItemValue, '') ELSE COALESCE (tdv.DetailValue, '') END AS [DetailValue], df_Definition.QuestionTypeID
			FROM @DetailFieldTargets ddft 
			INNER JOIN dbo.DetailFields df_Page WITH (NOLOCK) ON df_Page.DetailFieldID = ddft.DetailFieldID /* There will be one of these */
			INNER JOIN dbo.TableRows tr WITH (NOLOCK) ON tr.DetailFieldID = ddft.DetailFieldID AND tr.CustomerID = @CustomerID 
			INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID 
			INNER JOIN dbo.DetailFields df_Definition WITH (NOLOCK) ON df_Definition.DetailFieldID = tdv.DetailFieldID 
			LEFT JOIN dbo.LookupListItems AS luli WITH (NOLOCK) ON luli.LookupListID = df_Definition.LookupListID AND luli.LookupListItemID = tdv.ValueInt 
			LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
			WHERE ddft.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND ddft.QuestionTypeID IN (19) 
			
			
			/* Case TDV */
			SET @TargetDetailFieldSubtypeID = 11
			
			INSERT INTO @TDVSubs(PageDetailFieldID, PageFieldName, PageFieldCaption, ColumnDetailFieldID, ColumnFieldName, ColumnFieldCaption, TableRowID, FieldOrder, DetailValue, QuestionTypeID)
			SELECT DISTINCT df_Page.DetailFieldID, df_Page.FieldName, df_Page.FieldCaption, 
			df_Definition.DetailFieldID, df_Definition.FieldName, df_Definition.FieldCaption, 
			tr.TableRowID, 
			df_Definition.FieldOrder, 
			CASE WHEN df_Definition.LookupListID > 0 THEN COALESCE (t.Translation, luli.ItemValue, '') ELSE COALESCE (tdv.DetailValue, '') END AS [DetailValue], df_Definition.QuestionTypeID
			FROM @DetailFieldTargets ddft 
			INNER JOIN dbo.DetailFields df_Page WITH (NOLOCK) ON df_Page.DetailFieldID = ddft.DetailFieldID /* There will be one of these */
			INNER JOIN dbo.TableRows tr WITH (NOLOCK) ON tr.DetailFieldID = ddft.DetailFieldID AND tr.CaseID = @CaseID 
			INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID 
			INNER JOIN dbo.DetailFields df_Definition WITH (NOLOCK) ON df_Definition.DetailFieldID = tdv.DetailFieldID 
			LEFT JOIN dbo.LookupListItems AS luli WITH (NOLOCK) ON luli.LookupListID = df_Definition.LookupListID AND luli.LookupListItemID = tdv.ValueInt 
			LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
			WHERE ddft.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND ddft.QuestionTypeID IN (19) 
			
			
			/* Client TDV */
			SET @TargetDetailFieldSubtypeID = 12
			
			INSERT INTO @TDVSubs(PageDetailFieldID, PageFieldName, PageFieldCaption, ColumnDetailFieldID, ColumnFieldName, ColumnFieldCaption, TableRowID, FieldOrder, DetailValue, QuestionTypeID)
			SELECT DISTINCT df_Page.DetailFieldID, df_Page.FieldName, df_Page.FieldCaption, 
			df_Definition.DetailFieldID, df_Definition.FieldName, df_Definition.FieldCaption, 
			tr.TableRowID, 
			df_Definition.FieldOrder, 
			CASE WHEN df_Definition.LookupListID > 0 THEN COALESCE (t.Translation, luli.ItemValue, '') ELSE COALESCE (tdv.DetailValue, '') END AS [DetailValue], df_Definition.QuestionTypeID
			FROM @DetailFieldTargets ddft 
			INNER JOIN dbo.DetailFields df_Page WITH (NOLOCK) ON df_Page.DetailFieldID = ddft.DetailFieldID /* There will be one of these */
			INNER JOIN dbo.TableRows tr WITH (NOLOCK) ON tr.DetailFieldID = ddft.DetailFieldID AND tr.ClientID = @ClientID 
			INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID 
			INNER JOIN dbo.DetailFields df_Definition WITH (NOLOCK) ON df_Definition.DetailFieldID = tdv.DetailFieldID 
			LEFT JOIN dbo.LookupListItems AS luli WITH (NOLOCK) ON luli.LookupListID = df_Definition.LookupListID AND luli.LookupListItemID = tdv.ValueInt 
			LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
			WHERE ddft.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND ddft.QuestionTypeID IN (19) 
			
			
			/* ClientPersonnel TDV */
			SET @TargetDetailFieldSubtypeID = 13
			
			INSERT INTO @TDVSubs(PageDetailFieldID, PageFieldName, PageFieldCaption, ColumnDetailFieldID, ColumnFieldName, ColumnFieldCaption, TableRowID, FieldOrder, DetailValue, QuestionTypeID)
			SELECT DISTINCT df_Page.DetailFieldID, df_Page.FieldName, df_Page.FieldCaption, 
			df_Definition.DetailFieldID, df_Definition.FieldName, df_Definition.FieldCaption, 
			tr.TableRowID, 
			df_Definition.FieldOrder, 
			CASE WHEN df_Definition.LookupListID > 0 THEN COALESCE (t.Translation, luli.ItemValue, '') ELSE COALESCE (tdv.DetailValue, '') END AS [DetailValue], df_Definition.QuestionTypeID
			FROM @DetailFieldTargets ddft 
			INNER JOIN dbo.DetailFields df_Page WITH (NOLOCK) ON df_Page.DetailFieldID = ddft.DetailFieldID /* There will be one of these */
			INNER JOIN dbo.TableRows tr WITH (NOLOCK) ON tr.DetailFieldID = ddft.DetailFieldID AND tr.ClientPersonnelID = @ClientPersonnelID 
			INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID 
			INNER JOIN dbo.DetailFields df_Definition WITH (NOLOCK) ON df_Definition.DetailFieldID = tdv.DetailFieldID 
			LEFT JOIN dbo.LookupListItems AS luli WITH (NOLOCK) ON luli.LookupListID = df_Definition.LookupListID AND luli.LookupListItemID = tdv.ValueInt 
			LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
			WHERE ddft.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND ddft.QuestionTypeID IN (19) 
			
			
			/* Contact TDV */
			SET @TargetDetailFieldSubtypeID = 14
			
			INSERT INTO @TDVSubs(PageDetailFieldID, PageFieldName, PageFieldCaption, ColumnDetailFieldID, ColumnFieldName, ColumnFieldCaption, TableRowID, FieldOrder, DetailValue, QuestionTypeID)
			SELECT DISTINCT df_Page.DetailFieldID, df_Page.FieldName, df_Page.FieldCaption, 
			df_Definition.DetailFieldID, df_Definition.FieldName, df_Definition.FieldCaption, 
			tr.TableRowID, 
			df_Definition.FieldOrder, 
			CASE WHEN df_Definition.LookupListID > 0 THEN COALESCE (t.Translation, luli.ItemValue, '') ELSE COALESCE (tdv.DetailValue, '') END AS [DetailValue], df_Definition.QuestionTypeID
			FROM @DetailFieldTargets ddft 
			INNER JOIN dbo.DetailFields df_Page WITH (NOLOCK) ON df_Page.DetailFieldID = ddft.DetailFieldID /* There will be one of these */
			INNER JOIN dbo.TableRows tr WITH (NOLOCK) ON tr.DetailFieldID = ddft.DetailFieldID AND tr.ContactID = @ContactID 
			INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID 
			INNER JOIN dbo.DetailFields df_Definition WITH (NOLOCK) ON df_Definition.DetailFieldID = tdv.DetailFieldID 
			LEFT JOIN dbo.LookupListItems AS luli WITH (NOLOCK) ON luli.LookupListID = df_Definition.LookupListID AND luli.LookupListItemID = tdv.ValueInt 
			LEFT JOIN dbo.Translation t WITH (NOLOCK) ON t.ID = luli.LookupListItemID AND t.DetailFieldID IS NULL AND t.LanguageID = @LanguageID
			WHERE ddft.DetailFieldSubtypeID = @TargetDetailFieldSubtypeID 
			AND ddft.QuestionTypeID IN (19) 
			
			
		END /* End of basic table section */
		
	END /* End of if-script-exists section */
	
	
	/*******************************************************************************************************************/
	/*                                                                                                                 */
	/*										             Standard targets                                              */
	/*                                                                                                                 */
	/*******************************************************************************************************************/
	
	/* ScriptStandardTarget records are all parsed by the app, so nothing more to do here. */
	
	
	/* Send the single field results to the caller */
	SELECT DISTINCT s.*, DATALENGTH(s.[Target]) AS [TargetLength] 
	FROM @Subs s 
	ORDER BY [SectionID] ASC
	
	/* Send the basic table results to the caller */
	SELECT * 
	FROM @TDVSubs 
	ORDER BY PageDetailFieldID, TableRowID, FieldOrder
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldParser_ParseScript] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldParser_ParseScript] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldParser_ParseScript] TO [sp_executeall]
GO
