SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the QuestionnaireFonts table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireFonts_Get_List]

AS


				
				SELECT
					[QuestionnaireFontID],
					[ClientQuestionnaireID],
					[PartNameID],
					[FontFamily],
					[FontSize],
					[FontColour],
					[FontWeight],
					[FontAlignment],
					[ClientID]
				FROM
					[dbo].[QuestionnaireFonts] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFonts_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireFonts_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFonts_Get_List] TO [sp_executeall]
GO
