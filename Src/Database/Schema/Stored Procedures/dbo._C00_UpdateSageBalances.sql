SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-03
-- Description:	Calculate how much is still owed for all Matters of the Case passed in.
-- =============================================
CREATE PROCEDURE [dbo].[_C00_UpdateSageBalances] 
	@ClientID int, 
	@LeadTypeID int, 
	@CaseID int 
AS
BEGIN
	
	SET NOCOUNT ON
	
	EXEC dbo._C00_LogIt 'Info', '_C00_UpdateSageBalances', 'Start for CaseID', @CaseID, 324 
	
	/* Declare all the detail fields needed to look up matter values to date */
	DECLARE @FeesToSageField int, 
	@PaidFromSageField int, 
	@FeesOtherField int, 
	@PaidOtherField int, 
	@BalanceField int, 
	@FeesToSage decimal(18, 2) = 0,
	@PaidFromSage decimal(18, 2) = 0,
	@FeesOther decimal(18, 2) = 0,
	@PaidOther decimal(18, 2) = 0,
	@BalanceOutstanding decimal(18, 2) = 0,
	@FirstMatterID int
	
	
	/* Populate the field IDs from the InvoiceMapping table */
	SELECT TOP 1 @FeesToSageField = im.FeesInvoicedToSageField, 
	@PaidFromSageField = im.FeesPaidFromSageField, 
	@FeesOtherField = im.FeesInvoicedOtherField, 
	@PaidOtherField = im.FeesPaidOtherField, 
	@BalanceField = im.CaseBalanceField 
	FROM dbo.InvoiceMapping im (nolock) 
	WHERE im.ClientID = @ClientID 
	AND im.LeadTypeID = @LeadTypeID 
	
	
	/* Get the first Matter for the Case passed in */
	SELECT TOP 1 @FirstMatterID = m.MatterID 
	FROM dbo.Matter m (nolock) 
	WHERE m.ClientID = @ClientID 
	AND m.CaseID = @CaseID 

	
	/*SELECT @BalanceOutstanding = 
	(	
			SUM(CASE IsNumeric(mdv_FeesToSage.DetailValue) 
				WHEN 1 THEN convert(decimal(18,2), mdv_FeesToSage.DetailValue) 
				ELSE 0 END) - 
			SUM(CASE IsNumeric(mdv_PaidFromSage.DetailValue) 
				WHEN 1 THEN convert(decimal(18,2), mdv_PaidFromSage.DetailValue) 
				ELSE 0 END)
	)
	FROM dbo.Matter m (nolock) 
	INNER JOIN dbo.MatterDetailValues mdv_FeesToSage (nolock) ON mdv_FeesToSage.MatterID = m.MatterID AND mdv_FeesToSage.DetailFieldID = @FeesToSageField 
	INNER JOIN dbo.MatterDetailValues mdv_PaidFromSage (nolock) ON mdv_PaidFromSage.MatterID = m.MatterID AND mdv_PaidFromSage.DetailFieldID = @PaidFromSageField 
	WHERE m.MatterID = @FirstMatterID 
	
	
	/* If there are some more fields to take into account (eg Credit Cards) then add them up too */
	IF @FeesOtherField IS NOT NULL AND @PaidOtherField IS NOT NULL
	BEGIN
		
		SELECT @BalanceOutstanding = IsNull(@BalanceOutstanding, 0) + IsNull( 
		(	
				SUM(CASE IsNumeric(mdv_FeesOther.DetailValue) 
					WHEN 1 THEN convert(decimal(18,2), mdv_FeesOther.DetailValue) 
					ELSE 0 END) - 
				SUM(CASE IsNumeric(mdv_PaidOther.DetailValue) 
					WHEN 1 THEN convert(decimal(18,2), mdv_PaidOther.DetailValue) 
					ELSE 0 END)
		), 0)
		FROM dbo.Matter m (nolock) 
		INNER JOIN dbo.MatterDetailValues mdv_FeesOther (nolock) ON mdv_FeesOther.MatterID = m.MatterID AND mdv_FeesOther.DetailFieldID = @FeesOtherField 
		INNER JOIN dbo.MatterDetailValues mdv_PaidOther (nolock) ON mdv_PaidOther.MatterID = m.MatterID AND mdv_PaidOther.DetailFieldID = @PaidOtherField 
		WHERE m.MatterID = @FirstMatterID 
		
	END
	*/

	SELECT @FeesToSage = isnull(sum(isnull(mdv_FeesToSage.ValueMoney, 0)), 0) 
	FROM dbo.Matter m (nolock) 
	INNER JOIN dbo.MatterDetailValues mdv_FeesToSage (nolock) ON mdv_FeesToSage.MatterID = m.MatterID AND mdv_FeesToSage.DetailFieldID = @FeesToSageField 
	WHERE m.MatterID = @FirstMatterID 
	
	SELECT @PaidFromSage = isnull(sum(isnull(mdv_PaidFromSage.ValueMoney, 0)), 0) 
	FROM dbo.Matter m (nolock) 
	INNER JOIN dbo.MatterDetailValues mdv_PaidFromSage (nolock) ON mdv_PaidFromSage.MatterID = m.MatterID AND mdv_PaidFromSage.DetailFieldID = @PaidFromSageField 
	WHERE m.MatterID = @FirstMatterID 

	IF @FeesOtherField IS NOT NULL
	BEGIN
		SELECT @FeesOther = isnull(sum(isnull(mdv_FeesOther.ValueMoney, 0)), 0) 
		FROM dbo.Matter m (nolock) 
		INNER JOIN dbo.MatterDetailValues mdv_FeesOther (nolock) ON mdv_FeesOther.MatterID = m.MatterID AND mdv_FeesOther.DetailFieldID = @FeesOtherField 
		WHERE m.MatterID = @FirstMatterID 
	END

	IF @PaidOtherField IS NOT NULL
	BEGIN
		SELECT @PaidOther = isnull(sum(isnull(mdv_PaidOther.ValueMoney, 0)), 0) 
		FROM dbo.Matter m (nolock) 
		INNER JOIN dbo.MatterDetailValues mdv_PaidOther (nolock) ON mdv_PaidOther.MatterID = m.MatterID AND mdv_PaidOther.DetailFieldID = @PaidOtherField 
		WHERE m.MatterID = @FirstMatterID 
	END
	
	/* 
		JWG 2010-02-26
		Changed for AOH to match how Sage is actually used.
		
		Example:
		
		Total invoice £100
		Customer to pay £75
		Solicitor to pay £25
		£15 has been received from Sage, of which
		£8 is from Solicitor and £7 is from the customer
		
		So the total outstanding balance is (100 - 15) = £85
		BUT
		Clients are only interested in how much to chase the customer for.
		£75 - £7 = £68 is the easy calculation, but we don't hold the customer details, only the solicitor figures,
		so we have to go round the houses a bit. Of the £85 outstanding, £17 is due from the solicitor, 
		giving us a final calculation of £85 - £17 = £68.
		
		The full worked example is therefore: (100 - 15) - (25 - 8)
	*/
	SELECT @BalanceOutstanding = convert(decimal(18, 2), ((@FeesToSage - @PaidFromSage) - (@FeesOther - @PaidOther)))
	
	/* 
		Final step. Update all Matters for this Case with the same result. 
		This is so that, no matter which one you are looking at, you can always
		see how much the customer still owes to the client.
	*/	
	UPDATE dbo.MatterDetailValues 
	SET DetailValue = convert(varchar, @BalanceOutstanding) 
	FROM dbo.MatterDetailValues mdv_balance 
	INNER JOIN dbo.Matter m ON m.MatterID = mdv_balance.MatterID 
	WHERE m.ClientID = @ClientID 
	AND m.CaseID = @CaseID 
	AND mdv_balance.DetailFieldID = @BalanceField 
		

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_UpdateSageBalances] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_UpdateSageBalances] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_UpdateSageBalances] TO [sp_executeall]
GO
