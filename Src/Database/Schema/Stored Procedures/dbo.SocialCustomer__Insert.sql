SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-11-2015
-- Description:	Inserts a social customer record
-- =============================================
CREATE PROCEDURE [dbo].[SocialCustomer__Insert]

	@ClientID INT, 
	@CustomerID INT, 
	@SocialTypeID INT, 
	@ScreenName VARCHAR(250), 
	@SocialUserID VARCHAR(50), 
	@WhenCreated DATETIME, 
	@WhoCreated INT, 
	@WhenModified DATETIME,
	@WhoModified INT

AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO SocialCustomer (ClientID, CustomerID, SocialTypeID, ScreenName, SocialUserID, WhenCreated, WhoCreated, WhenModified, WhoModified)
	VALUES (@ClientID, @CustomerID, @SocialTypeID, @ScreenName, @SocialUserID, @WhenCreated, @WhoCreated, @WhenModified, @WhoModified)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialCustomer__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SocialCustomer__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialCustomer__Insert] TO [sp_executeall]
GO
