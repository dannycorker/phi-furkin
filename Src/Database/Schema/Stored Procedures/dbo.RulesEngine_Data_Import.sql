SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-07-19
-- Description:	Parse and stores import, ready for committal
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Data_Import]
(
	@ClientID INT,
	@UserID INT,
	@ParentID INT,
	@ImportType VARCHAR(250),
	@ImportXml XML,
	@FileName VARCHAR(250)
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @ParsedCSV VARCHAR(MAX) ='',
			@TotalRows INT = 0,
			@ErrorCount INT = 0,
			@SuccessCount INT = 0,
			@ImportID INT = 0
	exec _C00_LogItXML @ClientID, @ParentID, @ImportType, @ImportXML
	
	SELECT @ImportID = @@IDENTITY
	
	IF @ImportType = 'Options'
	BEGIN
	
		SELECT @ParsedCSV = '"Operator","Value1","Value2","ID","Error"'+CHAR(13)+CHAR(10)
		
		SELECT	@ParsedCSV += '"'+ Operator +'","'+ Value1 +'","'+ Value2 +'","'+ ID +'","'+ Error +'"'+CHAR(13)+CHAR(10),
				@TotalRows += 1,
				@ErrorCount += CASE WHEN Error = '' THEN 0 ELSE 1 END,
				@SuccessCount += CASE WHEN Error = '' THEN 1 ELSE 0 END
		FROM	dbo.fn_RulesEngine_ParameterOptions_Parse(@ClientID, @UserID, @ParentID, @ImportXml) row
		WHERE	row.RowID > 1

	END
	ELSE IF @ImportType = 'Outputs'
	BEGIN
		SELECT @ParsedCSV = '"Transform","Value","ParameterOption","Coordinates","Error"'+CHAR(13)+CHAR(10)
		
		SELECT	@ParsedCSV += '"'+ Transform +'","'+ Value +'","'+ ParameterOption +'","'+ Coordinates +'","'+ Error +'"'+CHAR(13)+CHAR(10),
				@TotalRows += 1,
				@ErrorCount += CASE WHEN Error = '' THEN 0 ELSE 1 END,
				@SuccessCount += CASE WHEN Error = '' THEN 1 ELSE 0 END
		FROM	dbo.fn_RulesEngine_Outputs_Parse(@ClientID, @UserID, @ParentID, @ImportXml) row
		WHERE	row.RowID > 1
	END
	
	SELECT @ImportID ImportID, @ParsedCSV ResultCSV, @TotalRows TotalRows, @ErrorCount ErrorCount, @SuccessCount SuccessCount

END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Data_Import] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Data_Import] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Data_Import] TO [sp_executeall]
GO
