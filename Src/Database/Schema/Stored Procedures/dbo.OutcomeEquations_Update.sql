SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the OutcomeEquations table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[OutcomeEquations_Update]
(

	@OutcomeEquationID int   ,

	@OutcomeID int   ,

	@EquationID int   ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[OutcomeEquations]
				SET
					[OutcomeID] = @OutcomeID
					,[EquationID] = @EquationID
					,[ClientID] = @ClientID
				WHERE
[OutcomeEquationID] = @OutcomeEquationID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeEquations_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutcomeEquations_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeEquations_Update] TO [sp_executeall]
GO
