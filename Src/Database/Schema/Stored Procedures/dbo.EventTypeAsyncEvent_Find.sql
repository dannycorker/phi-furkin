SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EventTypeAsyncEvent table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAsyncEvent_Find]
(

	@SearchUsingOR bit   = null ,

	@EventTypeAsyncEventID int   = null ,

	@ClientID int   = null ,

	@SourceEventTypeID int   = null ,

	@AsyncEventTypeID int   = null ,

	@FollowupThreadNumber int   = null ,

	@AquariumOptionID int   = null ,

	@SourceID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EventTypeAsyncEventID]
	, [ClientID]
	, [SourceEventTypeID]
	, [AsyncEventTypeID]
	, [FollowupThreadNumber]
	, [AquariumOptionID]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[EventTypeAsyncEvent] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeAsyncEventID] = @EventTypeAsyncEventID OR @EventTypeAsyncEventID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SourceEventTypeID] = @SourceEventTypeID OR @SourceEventTypeID IS NULL)
	AND ([AsyncEventTypeID] = @AsyncEventTypeID OR @AsyncEventTypeID IS NULL)
	AND ([FollowupThreadNumber] = @FollowupThreadNumber OR @FollowupThreadNumber IS NULL)
	AND ([AquariumOptionID] = @AquariumOptionID OR @AquariumOptionID IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EventTypeAsyncEventID]
	, [ClientID]
	, [SourceEventTypeID]
	, [AsyncEventTypeID]
	, [FollowupThreadNumber]
	, [AquariumOptionID]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[EventTypeAsyncEvent] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeAsyncEventID] = @EventTypeAsyncEventID AND @EventTypeAsyncEventID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SourceEventTypeID] = @SourceEventTypeID AND @SourceEventTypeID is not null)
	OR ([AsyncEventTypeID] = @AsyncEventTypeID AND @AsyncEventTypeID is not null)
	OR ([FollowupThreadNumber] = @FollowupThreadNumber AND @FollowupThreadNumber is not null)
	OR ([AquariumOptionID] = @AquariumOptionID AND @AquariumOptionID is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAsyncEvent_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAsyncEvent_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAsyncEvent_Find] TO [sp_executeall]
GO
