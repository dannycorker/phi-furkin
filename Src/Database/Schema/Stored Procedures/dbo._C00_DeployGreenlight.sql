SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-08-28
-- Description:	Deploy greenlight
-- =============================================
CREATE PROCEDURE [dbo].[_C00_DeployGreenlight] 
	 @ClientID INT
	,@GreenlightPassword	VARCHAR(65)
	,@GreenlightSalt		VARCHAR(50)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE 	@FolderID				INT,
				@NewClientPersonnelID	INT,
				@GreenlightEmail		VARCHAR(2000),
				@ClientOfficeID			INT,
				@SqlQueryID				INT,
				@CustomersTableID		INT,
				@LeadTableID			INT,
				@CasesTableID			INT,
				@LeadEventTableID		INT,
				@TestColID				INT,
				@WhenCreatedColID		INT,
				@MatterTableID			INT,
				@NewDetailFieldPageID	INT,
				@RLandTLeadTypeID		INT,
				@RLandTPageID			INT,
				@EventTypeTableID		INT,
				@LeadStatusTableID		INT

	SELECT @ClientOfficeID = co.ClientOfficeID
	FROM ClientOffices co WITH (NOLOCK)
	WHERE co.ClientID = @ClientID
	
	SELECT @GreenlightEmail = 'greenlight.system' + CONVERT(VARCHAR,@ClientID) + '@greenlightcrm.com'

	EXEC @NewClientPersonnelID = dbo.ClientPersonnel_Insert_Helper @ClientID, 'Greenlight', 'System', @GreenlightEmail, @GreenlightPassword, @GreenlightSalt, @ClientOfficeID, 1, '', 'SDK User', 1, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, 1, NULL, NULL, 0, NULL, 0

	INSERT INTO __LoginExclusions (ClientID, ClientPersonnelID, Description)
	VALUES (@ClientID, @NewClientPersonnelID, 'Greenlight User')

	/* Create Automation Folder */
	INSERT INTO Folder(ClientID, FolderTypeID, FolderParentID, FolderName, FolderDescription, WhenCreated, WhoCreated)
	SELECT @ClientID, 1, NULL, 'Greenlight Reports', 'Greenlight Reports', dbo.fn_GetDate_Local(), @NewClientPersonnelID

	SELECT @FolderID = SCOPE_IDENTITY()

	/* Create Base Report */
	INSERT INTO SqlQuery (ClientID,QueryText,QueryTitle,AutorunOnline,OnlineLimit,BatchLimit,SqlQueryTypeID,FolderID,IsEditable,IsTemplate,IsDeleted,WhenCreated,CreatedBy,OwnedBy,RunCount,LastRundate,LastRuntime,LastRowcount,MaxRuntime,MaxRowcount,AvgRuntime,AvgRowcount,Comments,WhenModified,ModifiedBy,LeadTypeID,ParentQueryID,IsParent)
	SELECT @ClientID,'SELECT TOP (100000) Customers.CustomerID, Customers.Fullname, Lead.LeadID, Cases.CaseID, Matter.MatterID, LeadEvent.WhenCreated AS [Last Action Date], EventType.EventTypeName AS [Last Action], LeadStatus.StatusName AS [Status], Customers.DayTimeTelephoneNumber, Customers.HomeTelephone, Customers.MobileTelephone, Customers.WorksTelephone
	FROM Customers    
	INNER JOIN Lead  ON Lead.CustomerID = Customers.CustomerID  
	INNER JOIN Cases  ON Cases.LeadID = Lead.LeadID  
	INNER JOIN LeadEvent  ON LeadEvent.CaseID = Cases.CaseID  
	INNER JOIN Matter WITH (NOLOCK) ON Matter.CaseID = Cases.CaseID
	LEFT JOIN LeadStatus LeadStatus WITH (NOLOCK) ON Cases.ClientStatusID = LeadStatus.StatusID AND LeadStatus.ClientID = ' + CONVERT(VARCHAR,@ClientID) + '
	INNER JOIN EventType WITH (NOLOCK) ON LeadEvent.EventTypeID = EventType.EventTypeID
	WHERE Customers.ClientID = ' + CONVERT(VARCHAR,@ClientID) + '
	AND Lead.ClientID = ' + CONVERT(VARCHAR,@ClientID) + '
	AND Cases.ClientID = ' + CONVERT(VARCHAR,@ClientID) + '
	AND LeadEvent.ClientID = ' + CONVERT(VARCHAR,@ClientID) + '
	AND Matter.ClientID = ' + CONVERT(VARCHAR,@ClientID) + '
	AND (Customers.Test = 0)
	AND (LeadEvent.EventDeleted = 0)
	AND (LeadEvent.WhenCreated > dateadd(hour, -1, dbo.fn_GetDate_Local())) 
	','All Cases with Status - Actioned in last 60 minutes',0,0,0,3,@FolderID,1,0,0,dbo.fn_GetDate_Local(),@NewClientPersonnelID,@NewClientPersonnelID,0,NULL,0,0,0,0,0,0,'',dbo.fn_GetDate_Local(),@NewClientPersonnelID,NULL,NULL,0

	SELECT @SqlQueryID = SCOPE_IDENTITY()

	INSERT INTO SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
	SELECT @ClientID,@SqlQueryID,'Customers','',0,'','',NULL,NULL,NULL,NULL

	SELECT @CustomersTableID = SCOPE_IDENTITY()

	INSERT INTO SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
	SELECT @ClientID,@SqlQueryID,'Lead','',1,'INNER JOIN','Lead.CustomerID = Customers.CustomerID',NULL,30,NULL,NULL

	SELECT @LeadTableID = SCOPE_IDENTITY()

	INSERT INTO SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
	SELECT @ClientID,@SqlQueryID,'Cases','',2,'INNER JOIN','Cases.LeadID = Lead.LeadID',NULL,41,NULL,NULL

	SELECT @CasesTableID = SCOPE_IDENTITY()

	INSERT INTO SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
	SELECT @ClientID,@SqlQueryID,'Matter','',3,'INNER JOIN','Matter.CaseID = Cases.CaseID',NULL,41,NULL,NULL

	SELECT @MatterTableID = SCOPE_IDENTITY()

	INSERT INTO SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
	SELECT @ClientID,@SqlQueryID,'LeadEvent','',4,'INNER JOIN','LeadEvent.CaseID = Cases.LatestNonNoteLeadEventID',NULL,63,NULL,NULL

	SELECT @LeadEventTableID = SCOPE_IDENTITY()

	INSERT INTO SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
	SELECT @ClientID,@SqlQueryID,'EventType','',5,'INNER JOIN','LeadEvent.EventTypeID = EventType.EventTypeID',NULL,63,NULL,NULL

	SELECT @EventTypeTableID = SCOPE_IDENTITY()

	INSERT INTO SqlQueryTables(ClientID,SqlQueryID,SqlQueryTableName,TableAlias,TableDisplayOrder,JoinType,JoinText,JoinTableID,JoinRTRID,TempTableID,TempJoinTableID)
	SELECT @ClientID,@SqlQueryID,'LeadStatus','',6,'LEFT JOIN','Cases.ClientStatusID = LeadStatus.StatusID',NULL,63,NULL,NULL
	
	SELECT @LeadStatusTableID = SCOPE_IDENTITY()

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@CustomersTableID,'Customers.CustomerID','',1,2,NULL,NULL,NULL,NULL,0,'Number','Number','CustomerID'

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@LeadTableID,'Lead.LeadID','',1,3,NULL,NULL,NULL,NULL,0,'Number','Number','LeadID'

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@CasesTableID,'Cases.CaseID','',1,1,NULL,NULL,NULL,NULL,0,'Number','Number','CaseID'

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@LeadEventTableID,'LeadEvent.EventTypeID','',1,4,NULL,NULL,NULL,NULL,0,'Number','Number','EventTypeID'

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@LeadEventTableID,'LeadEvent.WhenFollowedUp',NULL,1,5,NULL,NULL,NULL,NULL,0,'DateTime','DateTime','WhenFollowedUp'

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@LeadEventTableID,'LeadEvent.WhenCreated',NULL,1,5,NULL,NULL,NULL,NULL,0,'DateTime','DateTime','WhenCreated'

	SELECT @WhenCreatedColID = SCOPE_IDENTITY()

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@LeadEventTableID,'LeadEvent.EventDeleted',NULL,1,6,NULL,NULL,NULL,NULL,0,'Number','Number','EventDeleted'

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@CustomersTableID,'Customers.Test',NULL,1,7,NULL,NULL,NULL,NULL,0,'Number','Number','Test'

	SELECT @TestColID = SCOPE_IDENTITY()

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@CustomersTableID,'Matter.MatterID',NULL,1,7,NULL,NULL,NULL,NULL,0,'Number','Number','MatterID'

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@LeadEventTableID,'LeadEvent.LeadEventID','',1,8,NULL,NULL,NULL,NULL,0,'Number','Number','LeadEventID'

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@CustomersTableID,'Customers.Fullname','',1,8,NULL,NULL,NULL,NULL,0,'Text','Text','Fullname'

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@LeadEventTableID,'LeadEvent.WhenCreated','Last Action Date',1,8,NULL,NULL,NULL,NULL,0,'DateTime','DateTime','WhenCreated'

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@LeadEventTableID,'LeadEvent.WhenCreated','Last Action Date',1,8,NULL,NULL,NULL,NULL,0,'DateTime','DateTime','WhenCreated'

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@EventTypeTableID,'EventType.EventTypeName','Last Action',1,8,NULL,NULL,NULL,NULL,0,'Text','Text','EventTypeName'

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@LeadStatusTableID,'LeadStatus.StatusName','Status',1,8,NULL,NULL,NULL,NULL,0,'Text','Text','StatusName'

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@CustomersTableID,'Customers.DayTimeTelephoneNumber','',1,8,NULL,NULL,NULL,NULL,0,'Text','Text','DayTimeTelephoneNumber'

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@CustomersTableID,'Customers.HomeTelephone','',1,8,NULL,NULL,NULL,NULL,0,'Text','Text','HomeTelephone'

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@CustomersTableID,'Customers.MobileTelephone','',1,8,NULL,NULL,NULL,NULL,0,'Text','Text','MobileTelephone'

	INSERT INTO SqlQueryColumns(ClientID,SqlQueryID,SqlQueryTableID,CompleteOutputText,ColumnAlias,ShowColumn,DisplayOrder,SortOrder,SortType,TempColumnID,TempTableID,IsAggregate,ColumnDataType,ManipulatedDataType,ColumnNaturalName)
	SELECT @ClientID,@SqlQueryID,@CustomersTableID,'Customers.WorksTelephone','',1,8,NULL,NULL,NULL,NULL,0,'Text','Text','WorksTelephone'

	/*Criteria Now..*/
	INSERT INTO SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
	SELECT @ClientID,@SqlQueryID,@CustomersTableID,NULL,'Customers.ClientID = ' + CONVERT(VARCHAR,@ClientID),'','','Customers Security',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL

	INSERT INTO SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
	SELECT @ClientID,@SqlQueryID,@LeadTableID,NULL,'Lead.ClientID = ' + CONVERT(VARCHAR,@ClientID),'','','Lead Security',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL

	INSERT INTO SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
	SELECT @ClientID,@SqlQueryID,@CasesTableID,NULL,'Cases.ClientID = ' + CONVERT(VARCHAR,@ClientID),'','','Cases Security',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL

	INSERT INTO SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
	SELECT @ClientID,@SqlQueryID,@LeadEventTableID,NULL,'LeadEvent.ClientID = ' + CONVERT(VARCHAR,@ClientID),'','','LeadEvent Security',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL

	INSERT INTO SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
	SELECT @ClientID,@SqlQueryID,@MatterTableID,NULL,'Matter.ClientID = ' + CONVERT(VARCHAR,@ClientID),'','','Matter Security',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL

	INSERT INTO SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
	SELECT @ClientID,@SqlQueryID,@CustomersTableID,@TestColID,'(Customers.Test = 0)',0,'','',NULL,NULL,NULL,NULL,NULL,NULL,0,'',0,NULL

	INSERT INTO SqlQueryCriteria(ClientID,SqlQueryID,SqlQueryTableID,SqlQueryColumnID,CriteriaText,Criteria1,Criteria2,CriteriaName,SubQueryID,SubQueryLinkType,ParamValue,TempTableID,TempColumnID,TempCriteriaID,IsSecurityClause,CriteriaSubstitutions,IsParameterizable,Comparison)
	SELECT @ClientID,@SqlQueryID,@LeadEventTableID,@WhenCreatedColID,'(LeadEvent.WhenCreated > dateadd(hour, -1, dbo.fn_GetDate_Local()))','','','',NULL,NULL,NULL,NULL,NULL,NULL,0,'',0,NULL

	INSERT INTO DetailFieldPages (ClientID, Enabled, LeadOrMatter, LeadTypeID, PageCaption, PageName, PageOrder, SourceID, WhenCreated, WhenModified, WhoCreated, WhoModified)
	VALUES (@ClientID, 1, 10, 0, 'Greenlight Call History', 'Greenlight Call History', 1, NULL, dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local(), @NewClientPersonnelID, @NewClientPersonnelID)

	SELECT @NewDetailFieldPageID = SCOPE_IDENTITY()

	SELECT @RLandTLeadTypeID = lt.LeadTypeID
	FROM LeadType lt with (nolock)
	WHERE lt.ClientID = @ClientID
	AND lt.LeadTypeName like 'Resources%and%'

	IF @RLandTLeadTypeID IS NULL
	BEGIN

		/*Ok, so there is no resources and tables leadtype, lets create one.*/
		INSERT INTO LeadType (ClientID, LeadTypeName, LeadTypeDescription, Enabled, Live, MatterDisplayName, LastReferenceInteger, ReferenceValueFormatID, IsBusiness, LeadDisplayName, CaseDisplayName, CustomerDisplayName, LeadDetailsTabImageFileName, CustomerTabImageFileName, UseEventCosts, UseEventUOEs, UseEventDisbursements, UseEventComments)
		SELECT @ClientID,'Resources and Tables','Resources and Tables',0,0,'',NULL,NULL,0,'','','',NULL,NULL,0,0,0,0
		
		SELECT @RLandTLeadTypeID = SCOPE_IDENTITY()

	END

	INSERT INTO DetailFieldPages (ClientID, Enabled, LeadOrMatter, LeadTypeID, PageCaption, PageName, PageOrder, SourceID, WhenCreated, WhenModified, WhoCreated, WhoModified)
	VALUES (@ClientID, 1, 8, 0, 'Greenlight Call History', 'Greenlight Call History', 1, NULL, dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local(), @NewClientPersonnelID, @NewClientPersonnelID)

	SELECT @RLandTPageID = SCOPE_IDENTITY()

	INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView, ObjectTypeID, SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified, DetailFieldStyleID, Hyperlink)
	VALUES (@ClientID, 8, 'Greenlight Date and Time of Call', 'Greenlight Date and Time of Call', 1, 0, 0, NULL, @RLandTLeadTypeID, 1, @RLandTPageID, 1, 1, '', NULL, 1, NULL, 1, 1, '', '', '', '', NULL, NULL, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, @NewClientPersonnelID, dbo.fn_GetDate_Local(), @NewClientPersonnelID, dbo.fn_GetDate_Local(), NULL, ''),
			(@ClientID, 8, 'Greenlight Agent Name', 'Greenlight Agent Name', 1, 0, 0, NULL, @RLandTLeadTypeID, 1, @RLandTPageID, 2, 1, '', NULL, 1, NULL, 1, 1, '', '', '', '', NULL, NULL, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, @NewClientPersonnelID, dbo.fn_GetDate_Local(), @NewClientPersonnelID, dbo.fn_GetDate_Local(), NULL, ''),
			(@ClientID, 8, 'Greenlight Outcome of Call', 'Greenlight Outcome of Call', 1, 0, 0, NULL, @RLandTLeadTypeID, 1, @RLandTPageID, 3, 1, '', NULL, 1, NULL, 1, 1, '', '', '', '', NULL, NULL, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, @NewClientPersonnelID, dbo.fn_GetDate_Local(), @NewClientPersonnelID, dbo.fn_GetDate_Local(), NULL, ''),
			(@ClientID, 8, 'Greenlight Call Recording Link', 'Greenlight Call Recording Link', 1, 0, 0, NULL, @RLandTLeadTypeID, 1, @RLandTPageID, 4, 1, '', NULL, 1, NULL, 1, 1, '', '', '', '', NULL, NULL, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, @NewClientPersonnelID, dbo.fn_GetDate_Local(), @NewClientPersonnelID, dbo.fn_GetDate_Local(), NULL, ''),
			(@ClientID, 8, 'Greenlight Dialler TransactionID', 'Greenlight Dialler TransactionID', 1, 0, 0, NULL, @RLandTLeadTypeID, 1, @RLandTPageID, 5, 1, '', NULL, 1, NULL, 1, 1, '', '', '', '', NULL, NULL, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, @NewClientPersonnelID, dbo.fn_GetDate_Local(), @NewClientPersonnelID, dbo.fn_GetDate_Local(), NULL, ''),
			(@ClientID, 8, 'Greenlight CaseID', 'Greenlight CaseID', 8, 0, 0, NULL, @RLandTLeadTypeID, 1, @RLandTPageID, 7, 1, '', NULL, 1, NULL, 1, 1, '', '', '', '', NULL, NULL, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, @NewClientPersonnelID, dbo.fn_GetDate_Local(), @NewClientPersonnelID, dbo.fn_GetDate_Local(), NULL, ''),
			(@ClientID, 8, 'Greenlight MatterID', 'Greenlight MatterID', 8, 0, 0, NULL, @RLandTLeadTypeID, 1, @RLandTPageID, 8, 1, '', NULL, 1, NULL, 1, 1, '', '', '', '', NULL, NULL, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, @NewClientPersonnelID, dbo.fn_GetDate_Local(), @NewClientPersonnelID, dbo.fn_GetDate_Local(), NULL, ''),
			(@ClientID, 8, 'AutomatedEventQueueID', 'AutomatedEventQueueID', 1, 0, 0, NULL, @RLandTLeadTypeID, 1, @RLandTPageID, 9, 1, '', NULL, 1, NULL, 1, 1, '', '', '', '', NULL, NULL, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, @NewClientPersonnelID, dbo.fn_GetDate_Local(), @NewClientPersonnelID, dbo.fn_GetDate_Local(), NULL, ''),
			(@ClientID, 8, 'Greenlight System Message', 'Greenlight System Message', 1, 0, 0, NULL, @RLandTLeadTypeID, 1, @RLandTPageID, 10, 1, '', NULL, 1, NULL, 1, 1, '', '', '', '', NULL, NULL, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, @NewClientPersonnelID, dbo.fn_GetDate_Local(), @NewClientPersonnelID, dbo.fn_GetDate_Local(), NULL, ''),
			(@ClientID, 8, 'Call Type', 'Call Type', 1, 0, 0, NULL, @RLandTLeadTypeID, 1, @RLandTPageID, 6, 1, '', NULL, 1, NULL, 1, 1, '', '', '', '', NULL, NULL, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, @NewClientPersonnelID, dbo.fn_GetDate_Local(), @NewClientPersonnelID, dbo.fn_GetDate_Local(), NULL, ''),
			(@ClientID, 10, 'Greenlight Call History', 'Greenlight Call History', 19, 0, 0, NULL, 0, 1, @NewDetailFieldPageID, 1, 0, '', NULL, NULL, NULL, 1, 1, '', '', '', '', NULL, @RLandTPageID, '', '', 1, 0, 0, 0, 0, 0, 0, 0, 1, NULL, NULL, @NewClientPersonnelID, dbo.fn_GetDate_Local(), @NewClientPersonnelID, dbo.fn_GetDate_Local(), NULL, '')

	SELECT 		@FolderID				[FolderID],
				@NewClientPersonnelID	[NewClientPersonnelID],
				@GreenlightEmail		[GreenlightEmail],
				@ClientOfficeID			[ClientOfficeID],
				@SqlQueryID				[SqlQueryID],
				@CustomersTableID		[CustomersTableID],
				@LeadTableID			[LeadTableID],
				@CasesTableID			[CasesTableID],
				@LeadEventTableID		[LeadEventTableID],
				@TestColID				[TestColID],
				@WhenCreatedColID		[WhenCreatedColID],
				@MatterTableID			[MatterTableID],
				@NewDetailFieldPageID	[NewDetailFieldPageID],
				@RLandTLeadTypeID		[RLandTLeadTypeID],
				@RLandTPageID			[RLandTPageID],
				@EventTypeTableID		[EventTypeTableID],
				@LeadStatusTableID		[LeadStatusTableID]

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DeployGreenlight] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_DeployGreenlight] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DeployGreenlight] TO [sp_executeall]
GO
