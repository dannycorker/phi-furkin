SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Set commands for Automated Task Schedulers. Stop all by default.
-- =============================================
CREATE PROCEDURE [dbo].[SchedulerCommand__GetCommand] 
	@SchedulerIdentifier varchar(50) = 'ALL'
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT * 
	FROM dbo.SchedulerCommand 
	/*WHERE SchedulerIdentifier = @SchedulerIdentifier*/
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[SchedulerCommand__GetCommand] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SchedulerCommand__GetCommand] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SchedulerCommand__GetCommand] TO [sp_executeall]
GO
