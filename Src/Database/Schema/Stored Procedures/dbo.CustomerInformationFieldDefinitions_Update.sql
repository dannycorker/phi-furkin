SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the CustomerInformationFieldDefinitions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerInformationFieldDefinitions_Update]
(

	@CustomerInformationFieldDefinitionID int   ,

	@ClientQuestionnaireID int   ,

	@FieldAtBeginningOfQuestionnaire bit   ,

	@OrdinalPosition int   ,

	@FieldName varchar (255)  ,

	@FieldLabel varchar (900)  ,

	@Mandatory bit   ,

	@VerifyPhoneNumber bit   ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[CustomerInformationFieldDefinitions]
				SET
					[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[FieldAtBeginningOfQuestionnaire] = @FieldAtBeginningOfQuestionnaire
					,[OrdinalPosition] = @OrdinalPosition
					,[FieldName] = @FieldName
					,[FieldLabel] = @FieldLabel
					,[Mandatory] = @Mandatory
					,[VerifyPhoneNumber] = @VerifyPhoneNumber
					,[ClientID] = @ClientID
				WHERE
[CustomerInformationFieldDefinitionID] = @CustomerInformationFieldDefinitionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerInformationFieldDefinitions_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerInformationFieldDefinitions_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerInformationFieldDefinitions_Update] TO [sp_executeall]
GO
