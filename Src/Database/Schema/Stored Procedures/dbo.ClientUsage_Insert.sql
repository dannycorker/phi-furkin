SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientUsage table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientUsage_Insert]
(

	@ClientID int   ,

	@Validity varchar (100)  ,

	@ValidityHash varchar (250)  ,

	@Usage varchar (100)  ,

	@UsageHash varchar (250)  
)
AS


				
				INSERT INTO [dbo].[ClientUsage]
					(
					[ClientID]
					,[Validity]
					,[ValidityHash]
					,[Usage]
					,[UsageHash]
					)
				VALUES
					(
					@ClientID
					,@Validity
					,@ValidityHash
					,@Usage
					,@UsageHash
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientUsage_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientUsage_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientUsage_Insert] TO [sp_executeall]
GO
