SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryTables table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryTables_GetBySqlQueryID]
(

	@SqlQueryID int   
)
AS


				SELECT
					[SqlQueryTableID],
					[ClientID],
					[SqlQueryID],
					[SqlQueryTableName],
					[TableAlias],
					[TableDisplayOrder],
					[JoinType],
					[JoinText],
					[JoinTableID],
					[JoinRTRID],
					[TempTableID],
					[TempJoinTableID],
					[SourceID]
				FROM
					[dbo].[SqlQueryTables] WITH (NOLOCK) 
				WHERE
										[SqlQueryID] = @SqlQueryID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTables_GetBySqlQueryID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTables_GetBySqlQueryID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTables_GetBySqlQueryID] TO [sp_executeall]
GO
