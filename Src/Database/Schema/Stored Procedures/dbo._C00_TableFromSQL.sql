SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-09-05
-- Description:	Generate an Aquarium table from some common SQL
-- =============================================
CREATE PROCEDURE [dbo].[_C00_TableFromSQL]
	@CustomerID INT = NULL,
	@LeadID INT, 
	@CaseID INT = NULL,
	@MatterID INT, 
	@TableDetailFieldID INT, 
	@ClientIDForDV INT = NULL, 
	@ClientPersonnelIDForDV INT = NULL, 
	@ContactIDForDV INT = NULL,
	@PrintSQL BIT = 0
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @newword varchar(100),
			@body VARCHAR(MAX),
			@Columns VARCHAR(2000),
			@SQL VARCHAR(MAX),
			@ColWidth INT,
			@NumberOfColumns INT,
			@Loop INT = 0,
			@SQLQueryID INT,
			@ClientID INT

	/*Det the detail required from the custom sql table*/
	SELECT @Columns = c.HeaderColumns, @SQL = c.QueryText, @SQLQueryID = c.SQLQueryID, @ClientID = c.ClientID
	FROM CustomTableSQL c
	WHERE c.DetailFieldID = @TableDetailFieldID
	
	IF @SQLQueryID > 0 
	BEGIN
	
		SELECT @SQL = sq.QueryText
		FROM SqlQuery sq WITH (NOLOCK) 
		WHERE sq.QueryID = @SQLQueryID
		AND sq.ClientID = @ClientID
	
	END

	SELECT @SQL = REPLACE(@SQL, '@CustomerID', CONVERT(VARCHAR,ISNULL(@CustomerID,'')))
	SELECT @SQL = REPLACE(@SQL, '@LeadID', CONVERT(VARCHAR,ISNULL(@LeadID,'')))
	SELECT @SQL = REPLACE(@SQL, '@CaseID', CONVERT(VARCHAR,ISNULL(@CaseID,'')))
	SELECT @SQL = REPLACE(@SQL, '@MatterID', CONVERT(VARCHAR,ISNULL(@MatterID,'')))
	SELECT @SQL = REPLACE(@SQL, '@ClientID', CONVERT(VARCHAR,ISNULL(@ClientID,'')))
	SELECT @SQL = REPLACE(@SQL, '@ClientPersonnelIDForDV', CONVERT(VARCHAR,ISNULL(@ClientPersonnelIDForDV,'')))
	SELECT @SQL = REPLACE(@SQL, '@ContactIDForDV', CONVERT(VARCHAR,ISNULL(@ContactIDForDV,'')))
	SELECT @SQL = REPLACE(@SQL, '@Today', CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),121)) /*Added by CS 2015-05-22*/

	WHILE @SQL LIKE '%cell_replace%' AND @Loop < 50
	BEGIN

		SELECT TOP 1 @newword = CONVERT(VARCHAR(100),RTRIM(LTRIM(Item)))
		FROM dbo.fnSplitString (@Columns, ',') f
		WHERE f.ItemIndex = @Loop
		ORDER by f.ItemIndex

		select  @SQL = stuff(@SQL, charindex('cell_replace', @SQL), len('cell_replace'), @newword)

		SELECT @Loop = @Loop + 1

	END

	SELECT @SQL = REPLACE(@SQL, 'SELECT SUM', 'SELECT_SUM')

	IF @Sql NOT LIKE '%SELECT%TableRowID%FROM%' /*CS changed from just %TableRowID% as this needs to apply if TableRowID is referenced but not in the select statement*/
	BEGIN

		SELECT @SQL = REPLACE(@SQL, 'SELECT ', 'SELECT 0 AS [TableRowID], ')
		
	END

	SELECT @SQL = REPLACE(@SQL, 'SELECT_SUM', 'SELECT SUM')

	IF @SQL NOT LIKE '%DenyEdit%'
	BEGIN

		SELECT @SQL = REPLACE(@SQL, 'FROM ', ', ''true'' AS DenyEdit, ''true'' AS DenyDelete 
		FROM ')

	END

	IF @PrintSQL = 1
	BEGIN

		SELECT @SQL
		PRINT  @SQL
	
	END

	/*Replace out @ClientID values*/
	SELECT @SQL = REPLACE(@SQL,'@ClientID',CONVERT(VARCHAR,@ClientID))

	EXEC (@SQL)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_TableFromSQL] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_TableFromSQL] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_TableFromSQL] TO [sp_executeall]
GO
