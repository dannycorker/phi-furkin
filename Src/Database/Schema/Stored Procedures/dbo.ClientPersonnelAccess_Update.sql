SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientPersonnelAccess table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelAccess_Update]
(

	@ClientPersonnelAccessID int   ,

	@ClientID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@ClientPersonnelID int   ,

	@LeadID int   ,

	@CaseID int   ,

	@AccessRuleID int   ,

	@SourceLeadEventID int   ,

	@SourceID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@AccessLevel tinyint   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientPersonnelAccess]
				SET
					[ClientID] = @ClientID
					,[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
					,[ClientPersonnelID] = @ClientPersonnelID
					,[LeadID] = @LeadID
					,[CaseID] = @CaseID
					,[AccessRuleID] = @AccessRuleID
					,[SourceLeadEventID] = @SourceLeadEventID
					,[SourceID] = @SourceID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[AccessLevel] = @AccessLevel
				WHERE
[ClientPersonnelAccessID] = @ClientPersonnelAccessID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelAccess_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess_Update] TO [sp_executeall]
GO
