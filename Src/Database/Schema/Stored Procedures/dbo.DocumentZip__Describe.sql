SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2009-07-06
-- Description:	Helpful details about DocumentZipping
-- =============================================
CREATE PROCEDURE [dbo].[DocumentZip__Describe] 
	@DocumentZipInformationID int = null
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @DocumentZipInformationID > 0
	BEGIN
	
		-- Show all values
		SELECT 'All Fields' as [All Fields], dzi.*  
		FROM dbo.DocumentZipInformation dzi WITH (NOLOCK) 
		WHERE dzi.DocumentZipInformationID = @DocumentZipInformationID 
		
		-- Intermediate grouping files
		SELECT * 
		FROM dbo.DocumentZipFile dzf WITH (NOLOCK) 
		WHERE dzf.DocumentZipInformationID = @DocumentZipInformationID
		
		-- Zip Info
		SELECT 'Zip Records' as [Zip Records], dz.StatusID, count(*) 
		FROM dbo.DocumentZip dz WITH (NOLOCK) 
		WHERE dz.DocumentZipInformationID = @DocumentZipInformationID 
		GROUP BY dz.StatusID
		ORDER BY dz.StatusID
		
		/* If the dzi record has a status of complete(31) but any child records do not, show details now */
		/*CS 2013-02-28 if there are ANY incomplete child records, show them*/
		IF 
			--(31 = (
			--		SELECT dzi.StatusID 
			--		FROM dbo.DocumentZipInformation dzi WITH (NOLOCK) 
			--		WHERE dzi.DocumentZipInformationID = @DocumentZipInformationID
			--	) 
			--	AND 
				(
					NOT EXISTS
					(
						/* No child records at all */
						SELECT * 
						FROM dbo.DocumentZip dz WITH (NOLOCK) 
						WHERE dz.DocumentZipInformationID = @DocumentZipInformationID 
					)
					OR EXISTS
					(
						/* Any incomplete child records */
						SELECT * 
						FROM dbo.DocumentZip dz WITH (NOLOCK) 
						WHERE dz.DocumentZipInformationID = @DocumentZipInformationID 
						AND dz.StatusID <> 31
					)
				)
			
		BEGIN
			SELECT 'Details' as [Details], 
			dzi.DocumentZipInformationID, 
			dq.CaseID,
			le.LeadEventID,
			le.EventDeleted, 
			ISNULL(le.DocumentQueueID,dq.DocumentQueueID) [DocumentQueueID], 
			et.Enabled as [Event Enabled], 
			dt.Enabled as [Document Enabled], 
			et.EventTypeID,
			et.EventTypeName,
			et.EventSubtypeID, 
			dq.DocumentTypeID, 
			dt.DocumentTypeID, 
			dt.DocumentTypeName, 
			dt.InputFormat, 
			dt.OutputFormat, 
			DATALENGTH(dt.Template) as [Document length], 
			DATALENGTH(dt.EmailBodyText) [Email length]
			FROM dbo.DocumentZipInformation dzi WITH (NOLOCK) 
			INNER JOIN dbo.DocumentZip dz WITH (NOLOCK) ON dz.DocumentZipInformationID = dzi.DocumentZipInformationID 
			LEFT JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.DocumentQueueID = dz.DocumentQueueID 
			LEFT JOIN dbo.DocumentQueue dq WITH (NOLOCK) ON dz.DocumentQueueID = dq.DocumentQueueID  /*CS 2015-01-21 so that we can see these even after the doc has been produced*/
			LEFT JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID = le.EventTypeID  
			LEFT JOIN dbo.DocumentType dt WITH (NOLOCK) ON dt.DocumentTypeID = dq.DocumentTypeID	 /*CS 2015-01-21 More reliable, because of predicate rules*/
			WHERE dzi.DocumentZipInformationID = @DocumentZipInformationID
			
			SELECT 'See Messages for hints' AS [Info]
			
			PRINT 'Check any records in the list of incomplete zips, looking for disabled or empty documents, and also check for mismatches between the DocumentTypeID in the queue compared to the one that is now on the EventType record' 
			
			PRINT 'Use AQ 4, 10, ' + CAST(@DocumentZipInformationID AS VARCHAR) + '-- to auto-fix the zip (calls DocumentZip__KillAndCure)'
			
			PRINT 'Use AQ 4, 11, ' + CAST(@DocumentZipInformationID AS VARCHAR) + '-- to split fails zips into a new header (calls DocumentZipInformation__SplitOutFailedZips)'
			
			PRINT 'Use AQ 4, 12, ' + CAST(@DocumentZipInformationID AS VARCHAR) + '-- to annihilate this header completely (calls DocumentZipInformation__Annihilate)'
		END
		
	END
	
	ELSE
	
	BEGIN
	
		-- Show all values for live files
		SELECT 'All Fields' as [All Fields], dzi.*  
		FROM dbo.DocumentZipInformation dzi WITH (NOLOCK) 
		WHERE dzi.StatusID IN (35, 37, 38) 
		ORDER BY dzi.DocumentZipInformationID
	
		-- Zip Info
		SELECT 'Zip Records' as [Zip Records], dz.DocumentZipInformationID, dz.StatusID, count(*) 
		FROM dbo.DocumentZipInformation dzi WITH (NOLOCK) 
		INNER JOIN dbo.DocumentZip dz WITH (NOLOCK) ON dz.DocumentZipInformationID = dzi.DocumentZipInformationID
		WHERE dzi.StatusID IN (35, 37, 38) 
		GROUP BY dz.DocumentZipInformationID, dz.StatusID
		ORDER BY dz.DocumentZipInformationID, dz.StatusID
		
	END
	
	SELECT 'Rerun the Zip' [Useful SQL], 'exec DocumentZip__KillAndCure ' + CONVERT(varchar,@DocumentZipInformationID) 
	UNION
	SELECT 'Annihilate' [Useful SQL], 'exec DocumentZipInformation__Annihilate ' + CONVERT(varchar,@DocumentZipInformationID) 
	
	SELECT ao.AquariumOptionID [StatusID], ao.AquariumOptionName, ao.AquariumOptionDescription /*Added by CS 2012-01-04*/
	FROM dbo.AquariumOption ao WITH (NOLOCK) 
	WHERE ao.AquariumOptionTypeID = 4
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZip__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZip__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZip__Describe] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[DocumentZip__Describe] TO [sp_executehelper]
GO
