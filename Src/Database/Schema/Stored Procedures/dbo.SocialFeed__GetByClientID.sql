SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 09-11-2015
-- Description:	Gets a list of social feeds for the given client
-- =============================================
CREATE PROCEDURE [dbo].[SocialFeed__GetByClientID]
	@ClientID INT
AS
BEGIN
		
	SET NOCOUNT ON;

	SELECT * FROM SocialFeed WITH (NOLOCK) 
	WHERE ClientID=@ClientID
	ORDER BY SocialTypeID, WhenCreated ASC
	

END

GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SocialFeed__GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__GetByClientID] TO [sp_executeall]
GO
