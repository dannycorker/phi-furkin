SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2011-07-13
-- Description:	Switch off a client and kill all user sessions
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ClientDeactivate]
	@ClientID int
AS
BEGIN
	SET NOCOUNT ON;
	
	/* Check to see if we have already mangled the Validity column. Don't do it twice in case we need to reactivate them later. */
	IF 'zzz' = (
		SELECT LEFT(cu.Validity, 3) 
		FROM dbo.ClientUsage cu WITH (NOLOCK) 
		WHERE cu.ClientID = @ClientID
	)
	BEGIN
		SELECT 'The usage code appears to have been deactivated already?'
	END
	ELSE
	BEGIN
		/* Mangle the validity code so that no-one can log on again */
		UPDATE dbo.ClientUsage 
		SET Validity = 'zzz' + Validity 
		WHERE ClientID = @ClientID 
		
		/* Now kill all existing user sessions */
		DELETE dbo.ActiveSession 
		FROM dbo.ActiveSession ac 
		INNER JOIN dbo.ClientPersonnel cp ON cp.EmailAddress = ac.EmailAddress 
		WHERE cp.ClientID = @ClientID
		
		/* 
			Disable all batch jobs 
			Use SourceID to show which we have switched off, so they can be switched on again if needed.
		*/
		UPDATE dbo.AutomatedTask
		SET Enabled = 0, SourceID = TaskID
		WHERE ClientID = @ClientID
		AND Enabled = 1
		
	END
	
	/* Show the results */
	SELECT * 
	FROM dbo.ClientUsage cu WITH (NOLOCK) 
	WHERE cu.ClientID = @ClientID
	
	SELECT 'All sessions have been deleted' AS [Active Session Delete]
	
	SELECT 'All batch jobs have been disabled' AS [Automated Task Update]
END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ClientDeactivate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ClientDeactivate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ClientDeactivate] TO [sp_executeall]
GO
