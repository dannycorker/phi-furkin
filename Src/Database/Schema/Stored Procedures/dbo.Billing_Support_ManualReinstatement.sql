SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Livingstone
-- Create date: 17 May 2019
-- Description:	Changes payment status ID to New, resets and rebuilds the Payment Schedules
-- =============================================
CREATE PROCEDURE [dbo].[Billing_Support_ManualReinstatement]
(
	@CancellationDATE DATE,
	@PurchasedProductID INT,
	@ExistingMandate BIT,
	@DebugMode BIT = 1
)
AS
BEGIN
	SET NOCOUNT ON;


	DECLARE
		@AccountID INT,
		@PaymentDate DATE = CONVERT(DATE,dbo.fn_GetDate_Local(), 120),  /*JEL good diligence but not required, dbo.fn_GetDate_Local() into a date variable will stay in SQL format*/ 
		@CollectionDate DATE = dbo.fnAddWorkingDays(CONVERT(DATE,dbo.fn_GetDate_Local(), 120), 3)

	
	/*JEL Let's get the account from the Purchased Product, fewer Joins*/ 

	SELECT @AccountID = p.AccountID FROM PurchasedProduct p with (NOLOCK) 
	where p.PurchasedProductID = @PurchasedProductID
	
/*	SELECT TOP 1 @AccountID = pp.AccountID
	FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
	INNER JOIN Customers cu WITH (NOLOCK) ON cu.CustomerID = ppps.CustomerID AND cu.Test = 0
	INNER JOIN Lead l WITH (NOLOCK) ON cu.CustomerID = l.CustomerID AND l.LeadTypeID = 1492
	INNER JOIN Cases c WITH (NOLOCK) ON c.LeadID = l.LeadID
	INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID = c.CaseID
	INNER JOIN MatterDetailValues mdvStatus WITH (NOLOCK) ON mdvStatus.MatterID = m.MatterID AND mdvStatus.DetailFieldID = 170038 /*Policy Status*/
	INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON pp.PurchasedProductID = ppps.PurchasedProductID
	WHERE ppps.PurchasedProductID = @PurchasedProductID
	AND ppps.PaymentStatusID = 3
	AND mdvStatus.ValueInt = 43002 /*Live*/
	AND ppps.ActualCollectionDate >= @CancellationDATE   */ 

	/*If we have an existing mandate, then we only need to add the bacs collection wait to the payment dates, if no mandate, we need to add in the 10 working day wait*/ 
	IF @ExistingMandate = 0
	BEGIN

		SELECT @PaymentDate = dbo.fnAddWorkingDays(CONVERT(DATE,dbo.fn_GetDate_Local(), 120), 10)
		SELECT @CollectionDate = dbo.fnAddWorkingDays(@PaymentDate, 3)

	END
	
	IF @DebugMode = 0
	BEGIN
		
		
		UPDATE ppps
		SET ppps.PaymentDate = @PaymentDate, ppps.ActualCollectionDate = @CollectionDate
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		INNER JOIN Customers cu WITH (NOLOCK) ON cu.CustomerID = ppps.CustomerID AND cu.Test = 0
		INNER JOIN Lead l WITH (NOLOCK) ON cu.CustomerID = l.CustomerID AND l.LeadTypeID = 1492
		INNER JOIN Cases c WITH (NOLOCK) ON c.LeadID = l.LeadID
		INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID = c.CaseID
		INNER JOIN MatterDetailValues mdvStatus WITH (NOLOCK) ON mdvStatus.MatterID = m.MatterID AND mdvStatus.DetailFieldID = 170038 /*Policy Status*/
		WHERE ppps.PurchasedProductID = @PurchasedProductID
		AND ppps.PaymentStatusID = 3
		AND mdvStatus.ValueInt = 43002 /*Live*/
		AND ppps.PaymentDate <= @PaymentDate
		

		UPDATE ppps
		SET ppps.PaymentStatusID = 1
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		WHERE ppps.PurchasedProductID = @PurchasedProductID
		AND ppps.PaymentStatusID = 3
		AND ppps.PaymentDate >= @CancellationDATE

		EXEC [dbo].[Billing__RebuildCustomerPaymentSchedule] @AccountID, @CancellationDATE, 58552 
	END

	IF @DebugMode = 1
	BEGIN
		SELECT ppps.*
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		INNER JOIN Customers cu WITH (NOLOCK) ON cu.CustomerID = ppps.CustomerID AND cu.Test = 0
		INNER JOIN Lead l WITH (NOLOCK) ON cu.CustomerID = l.CustomerID AND l.LeadTypeID = 1492
		INNER JOIN Cases c WITH (NOLOCK) ON c.LeadID = l.LeadID
		INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID = c.CaseID
		INNER JOIN MatterDetailValues mdvStatus WITH (NOLOCK) ON mdvStatus.MatterID = m.MatterID AND mdvStatus.DetailFieldID = 170038 /*Policy Status*/
		WHERE ppps.PurchasedProductID = @PurchasedProductID
		AND ppps.PaymentStatusID = 3
		AND mdvStatus.ValueInt = 43002 /*Live*/
		AND ppps.PaymentDate <= @PaymentDate
		
		/*Removed joins as they caused duplication for MultiPet Policies.*/ 
		SELECT ppps.*
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		WHERE ppps.PurchasedProductID = @PurchasedProductID
		AND ppps.PaymentStatusID = 3
		AND ppps.PaymentDate >= @CancellationDATE
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing_Support_ManualReinstatement] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing_Support_ManualReinstatement] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing_Support_ManualReinstatement] TO [sp_executeall]
GO
