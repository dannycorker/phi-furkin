SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ValidationCriteriaFieldTypes table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ValidationCriteriaFieldTypes_GetByValidationCriteriaFieldTypeID]
(

	@ValidationCriteriaFieldTypeID int   
)
AS


				SELECT
					[ValidationCriteriaFieldTypeID],
					[Allowable]
				FROM
					[dbo].[ValidationCriteriaFieldTypes] WITH (NOLOCK) 
				WHERE
										[ValidationCriteriaFieldTypeID] = @ValidationCriteriaFieldTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriteriaFieldTypes_GetByValidationCriteriaFieldTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ValidationCriteriaFieldTypes_GetByValidationCriteriaFieldTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriteriaFieldTypes_GetByValidationCriteriaFieldTypeID] TO [sp_executeall]
GO
