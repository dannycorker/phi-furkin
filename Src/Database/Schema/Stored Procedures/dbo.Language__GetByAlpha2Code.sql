SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Paul Richardson
-- Purpose: Select records from the Language table through an Alpha2Code
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Language__GetByAlpha2Code]
(
	@Alpha2Code char(2)   
)
AS


				SELECT
					[LanguageID],
					[LanguageName],
					[Alpha2Code],
					[Alpha3Code],
					[NativeName],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[Language]
				WHERE
					[Alpha2Code] = @Alpha2Code
				SELECT @@ROWCOUNT
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[Language__GetByAlpha2Code] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Language__GetByAlpha2Code] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Language__GetByAlpha2Code] TO [sp_executeall]
GO
