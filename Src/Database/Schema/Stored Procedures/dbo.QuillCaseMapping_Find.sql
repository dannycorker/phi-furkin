SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the QuillCaseMapping table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillCaseMapping_Find]
(

	@SearchUsingOR bit   = null ,

	@QuillCaseMappingID int   = null ,

	@ClientID int   = null ,

	@CaseID int   = null ,

	@QuillCaseCode varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [QuillCaseMappingID]
	, [ClientID]
	, [CaseID]
	, [QuillCaseCode]
    FROM
	[dbo].[QuillCaseMapping] WITH (NOLOCK) 
    WHERE 
	 ([QuillCaseMappingID] = @QuillCaseMappingID OR @QuillCaseMappingID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([QuillCaseCode] = @QuillCaseCode OR @QuillCaseCode IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [QuillCaseMappingID]
	, [ClientID]
	, [CaseID]
	, [QuillCaseCode]
    FROM
	[dbo].[QuillCaseMapping] WITH (NOLOCK) 
    WHERE 
	 ([QuillCaseMappingID] = @QuillCaseMappingID AND @QuillCaseMappingID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([QuillCaseCode] = @QuillCaseCode AND @QuillCaseCode is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCaseMapping_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillCaseMapping_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCaseMapping_Find] TO [sp_executeall]
GO
