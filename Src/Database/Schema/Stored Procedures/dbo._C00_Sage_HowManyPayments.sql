SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 31-05-2012
-- Description:	Counts how many payments have the given sage unique reference
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Sage_HowManyPayments]
	@ClientID INT,
	@SageUniqueRef VARCHAR(MAX),
	@LeadID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @PaymentSageAccountEntryKeyFieldTP INT = 183,
			@LeadTypeID INT,
			@PaymentSageAccountEntryKeyColumnField INT
	
	SELECT @LeadTypeID = l.LeadTypeID 
	FROM dbo.Lead l WITH (NOLOCK) 
	WHERE l.LeadID = @LeadID 

	SELECT @PaymentSageAccountEntryKeyColumnField = t.ColumnFieldID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	WHERE t.ThirdPartyFieldID = @PaymentSageAccountEntryKeyFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID 
	
	SELECT COUNT(tdv.TableRowID) FROM TableDetailValues tdv WITH (NOLOCK) WHERE DetailFieldID = @PaymentSageAccountEntryKeyColumnField AND DetailValue = @SageUniqueRef AND LeadID=@LeadID AND ClientID=@ClientID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Sage_HowManyPayments] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Sage_HowManyPayments] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Sage_HowManyPayments] TO [sp_executeall]
GO
