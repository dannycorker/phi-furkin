SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the LeadType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadType_Get_List]

AS


				
				SELECT
					[LeadTypeID],
					[ClientID],
					[LeadTypeName],
					[LeadTypeDescription],
					[Enabled],
					[Live],
					[MatterDisplayName],
					[LastReferenceInteger],
					[ReferenceValueFormatID],
					[IsBusiness],
					[LeadDisplayName],
					[CaseDisplayName],
					[CustomerDisplayName],
					[LeadDetailsTabImageFileName],
					[CustomerTabImageFileName],
					[UseEventCosts],
					[UseEventUOEs],
					[UseEventDisbursements],
					[UseEventComments],
					[AllowMatterPageChoices],
					[AllowLeadPageChoices],
					[IsRPIEnabled],
					[FollowupWorkingDaysOnly],
					[MatterLastReferenceInteger],
					[MatterReferenceValueFormatID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[CalculateAllMatters],
					[SmsGatewayID]
				FROM
					[dbo].[LeadType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType_Get_List] TO [sp_executeall]
GO
