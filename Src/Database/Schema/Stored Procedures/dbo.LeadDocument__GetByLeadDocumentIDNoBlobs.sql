SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2010-09-06
-- Description:	Gets a LeadDocument without the blobs, so we can query all the other properties quickly.
-- JWG 2012-07-12 Use LeadDocument again now that it has all the details back again.
-- =============================================
CREATE PROCEDURE [dbo].[LeadDocument__GetByLeadDocumentIDNoBlobs]
(
	@LeadDocumentID int   
)
AS
BEGIN

	SELECT
		[LeadDocumentID],
		[ClientID],
		[LeadID],
		[DocumentTypeID],
		[LeadDocumentTitle],
		[UploadDateTime],
		[WhoUploaded],
		CAST('' as varbinary) AS [DocumentBLOB],
		[FileName],
		CAST('' as varbinary) AS [EmailBLOB],
		[DocumentFormat],
		[EmailFrom],
		[EmailTo],
		[CcList],
		[BccList],
		[ElectronicSignatureDocumentKey],
		[Encoding],
		[ContentFormat],
		[ZipFormat],
		[DocumentBlobSize], 
		[EmailBlobSize],
		[DocumentDatabaseID],
		[WhenArchived],
		[DocumentTypeVersionID]
	FROM
		dbo.LeadDocument ld WITH (NOLOCK) 
	WHERE
		[LeadDocumentID] = @LeadDocumentID
	
	SELECT @@ROWCOUNT
					
END	




GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument__GetByLeadDocumentIDNoBlobs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocument__GetByLeadDocumentIDNoBlobs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument__GetByLeadDocumentIDNoBlobs] TO [sp_executeall]
GO
