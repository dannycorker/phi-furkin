SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PortalCaseListDisplay table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalCaseListDisplay_Update]
(

	@PortalCaseListDisplayID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@Field1Caption varchar (50)  ,

	@Field1TypeID int   ,

	@Field1DetailFieldID int   ,

	@Field1ColumnDetailFieldID int   ,

	@Field2Caption varchar (50)  ,

	@Field2TypeID int   ,

	@Field2DetailFieldID int   ,

	@Field2ColumnDetailFieldID int   ,

	@Field3Caption varchar (50)  ,

	@Field3TypeID int   ,

	@Field3DetailFieldID int   ,

	@Field3ColumnDetailFieldID int   ,

	@Field4Caption varchar (50)  ,

	@Field4TypeID int   ,

	@Field4DetailFieldID int   ,

	@Field4ColumnDetailFieldID int   ,

	@Field5Caption varchar (50)  ,

	@Field5TypeID int   ,

	@Field5DetailFieldID int   ,

	@Field5ColumnDetailFieldID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@SqlQueryText varchar (MAX)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PortalCaseListDisplay]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[Field1Caption] = @Field1Caption
					,[Field1TypeID] = @Field1TypeID
					,[Field1DetailFieldID] = @Field1DetailFieldID
					,[Field1ColumnDetailFieldID] = @Field1ColumnDetailFieldID
					,[Field2Caption] = @Field2Caption
					,[Field2TypeID] = @Field2TypeID
					,[Field2DetailFieldID] = @Field2DetailFieldID
					,[Field2ColumnDetailFieldID] = @Field2ColumnDetailFieldID
					,[Field3Caption] = @Field3Caption
					,[Field3TypeID] = @Field3TypeID
					,[Field3DetailFieldID] = @Field3DetailFieldID
					,[Field3ColumnDetailFieldID] = @Field3ColumnDetailFieldID
					,[Field4Caption] = @Field4Caption
					,[Field4TypeID] = @Field4TypeID
					,[Field4DetailFieldID] = @Field4DetailFieldID
					,[Field4ColumnDetailFieldID] = @Field4ColumnDetailFieldID
					,[Field5Caption] = @Field5Caption
					,[Field5TypeID] = @Field5TypeID
					,[Field5DetailFieldID] = @Field5DetailFieldID
					,[Field5ColumnDetailFieldID] = @Field5ColumnDetailFieldID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[SqlQueryText] = @SqlQueryText
				WHERE
[PortalCaseListDisplayID] = @PortalCaseListDisplayID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalCaseListDisplay_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalCaseListDisplay_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalCaseListDisplay_Update] TO [sp_executeall]
GO
