SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








-- =============================================
-- Author:		Jim Green
-- Create date: 2008-01-31
-- Description:	Get a WorkflowTask for this user
-- =============================================
CREATE PROCEDURE [dbo].[WorkflowTask_GetTaskDetails]

	@WorkflowTaskID int 

AS
BEGIN
	
	-- Finally, select the values for the application to display
	SELECT	wt.WorkflowTaskID, 
			wt.ClientID,
			wt.LeadID, 
			wt.CaseID, 
			wt.EventTypeID, 
			wt.FollowUp,
			wt.AssignedDate,
			wt.AssignedTo,
			wt.Important,
			et.EventTypeName, 
			wt.Important,
			l.CustomerID, 
			c.FullName AS 'CustomerName',
			ca.CaseRef,
			wg.WorkflowGroupID,
			wg.[Name],
			et.InProcess
			
	FROM WorkflowTask wt

	INNER JOIN Lead l ON l.LeadID = wt.LeadID 
	INNER JOIN EventType et ON wt.EventTypeID = et.EventTypeID 
	INNER JOIN Customers c ON c.CustomerID = l.CustomerID
	INNER JOIN Cases ca ON ca.CaseID = wt.CaseID
	INNER JOIN WorkflowGroup wg ON wg.WorkflowGroupID = wt.WorkflowGroupID
	WHERE WorkflowTaskID = @WorkflowTaskID 
	
END









GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_GetTaskDetails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowTask_GetTaskDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_GetTaskDetails] TO [sp_executeall]
GO
