SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuestionnaireFonts table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireFonts_GetByPartNameID]
(

	@PartNameID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[QuestionnaireFontID],
					[ClientQuestionnaireID],
					[PartNameID],
					[FontFamily],
					[FontSize],
					[FontColour],
					[FontWeight],
					[FontAlignment],
					[ClientID]
				FROM
					[dbo].[QuestionnaireFonts] WITH (NOLOCK) 
				WHERE
					[PartNameID] = @PartNameID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFonts_GetByPartNameID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireFonts_GetByPartNameID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFonts_GetByPartNameID] TO [sp_executeall]
GO
