SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Panels table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Panels_Update]
(

	@PanelID int   ,

	@ClientID int   ,

	@ClientPersonnelID int   ,

	@PanelName varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Panels]
				SET
					[ClientID] = @ClientID
					,[ClientPersonnelID] = @ClientPersonnelID
					,[PanelName] = @PanelName
				WHERE
[PanelID] = @PanelID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Panels_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Panels_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Panels_Update] TO [sp_executeall]
GO
