SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2011-08-31
-- Description:	Describe/create/copy NoteTypeSql as required
-- =============================================
CREATE PROCEDURE [dbo].[nts]
(
	@ClientID int = null, 
	@NoteTypeID int = null, 
	@CreateDefaultSql bit = null, 
	@UserID int = null,
	@CopyFromNoteTypeID int = null, 
	@PostUpdateSql varchar (MAX) = null,
	@IsInsert int = 1 
)
AS
BEGIN

	/* List all nts by default */
	IF @CreateDefaultSql IS NULL AND @CopyFromNoteTypeID IS NULL AND @PostUpdateSql IS NULL 
	BEGIN
	
		/* List all nts by default */
		SELECT * 
		FROM dbo.NoteTypeSql nts WITH (NOLOCK) 
		WHERE (nts.ClientID = @ClientID OR @ClientID IS NULL)
		AND (nts.NoteTypeID = @NoteTypeID OR @NoteTypeID IS NULL) 
		ORDER BY nts.NoteTypeID
	
	END
	ELSE
	BEGIN
		/* Copy one nts record from another */
		IF @NoteTypeID IS NOT NULL AND @CopyFromNoteTypeID IS NOT NULL 
		BEGIN
		
			/* Upsert nts */
			DELETE dbo.NoteTypeSql 
			WHERE NoteTypeID = @NoteTypeID and InsertExisting = @IsInsert
			
			INSERT INTO dbo.NoteTypeSql (ClientID, NoteTypeID, PostUpdateSql, WhoCreated, WhenCreated, WhoChanged, WhenChanged,InsertExisting) 
			SELECT nts.ClientID, @NoteTypeID, nts.PostUpdateSql, @UserID, dbo.fn_GetDate_Local(), @UserID, dbo.fn_GetDate_Local(), InsertExisting 
			FROM dbo.NoteTypeSql nts WITH (NOLOCK) 
			WHERE nts.NoteTypeID = @CopyFromNoteTypeID 
			
		END
		ELSE
		BEGIN
			/* Needs a user id to create records */
			IF @UserID IS NULL
			BEGIN
				SELECT 'You must pass in your user id within this client to create new records'
				SELECT 'Use cp nnn to find ClientPersonnel records within this client nnn'
			END
			ELSE
			BEGIN
				/* Create a default nts record */
				IF @NoteTypeID IS NOT NULL AND @CreateDefaultSql = 1
				BEGIN
					
					/* Upsert nts */
					DELETE dbo.NoteTypeSql 
					WHERE NoteTypeID = @NoteTypeID and InsertExisting = @IsInsert
					
					INSERT INTO dbo.NoteTypeSql (ClientID, NoteTypeID, PostUpdateSql, WhoCreated, WhenCreated, WhoChanged, WhenChanged,InsertExisting) 
					SELECT et.ClientID, @NoteTypeID, 'EXEC dbo._C' + CASE WHEN et.ClientID BETWEEN 1 AND 10 THEN '0' ELSE '' END + CAST(et.ClientID as varchar) + '_SAN @LeadEventID', @UserID, dbo.fn_GetDate_Local(), @UserID, dbo.fn_GetDate_Local(), @IsInsert 
					FROM dbo.NoteType et WITH (NOLOCK) 
					WHERE et.NoteTypeID = @NoteTypeID 
				
				END
				ELSE
				BEGIN
					/* Create nts record from value passed in */
					IF @NoteTypeID IS NOT NULL AND @PostUpdateSql IS NOT NULL
					BEGIN
					
						/* Upsert nts */
						DELETE dbo.NoteTypeSql 
						WHERE NoteTypeID = @NoteTypeID and InsertExisting = @IsInsert
						
						INSERT INTO dbo.NoteTypeSql (ClientID, NoteTypeID, PostUpdateSql, WhoCreated, WhenCreated, WhoChanged, WhenChanged,InsertExisting) 
						SELECT et.ClientID, @NoteTypeID, @PostUpdateSql, @UserID, dbo.fn_GetDate_Local(), @UserID, dbo.fn_GetDate_Local(), @IsInsert 
						FROM dbo.NoteType et WITH (NOLOCK) 
						WHERE et.NoteTypeID = @NoteTypeID 
					
					END
				END
			END
		END
		
		/* Show the results of any changes made */	
		EXEC dbo.nts @ClientID, @NoteTypeID
		
	END
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[nts] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[nts] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[nts] TO [sp_executeall]
GO
