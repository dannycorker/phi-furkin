SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 01/02/2016
-- Description:	Find a resource list detail field based upon a partial field name
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__FindResourceListByPartialFieldName]
	@ClientID INT,
	@FieldName VARCHAR(50),
	@LeadTypeID INT	
AS
BEGIN

	SET NOCOUNT ON;

	SELECT * FROM DetailFields WITH (NOLOCK) 
	WHERE 
		ClientID=@ClientID AND 
		(LeadTypeID=@LeadTypeID OR LeadTypeID=0) AND
		QuestionTypeID = 14 AND -- only return resource list fields
		FieldName LIKE '%' + @FieldName + '%'
		  
END

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__FindResourceListByPartialFieldName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__FindResourceListByPartialFieldName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__FindResourceListByPartialFieldName] TO [sp_executeall]
GO
