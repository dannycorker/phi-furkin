SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-03-19
-- Description:	Find and replace on Detail Fields
-- =============================================
CREATE PROCEDURE [dbo].[_C00_DFFindAndReplace]
	@ClientID INT,
	@LeadTypeID INT,
	@FindText VARCHAR(50),
	@ReplaceText VARCHAR(50)
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE DetailFields
	SET FieldCaption = REPLACE(FieldCaption, @FindText, @ReplaceText), FieldName = REPLACE(FieldName, @FindText, @ReplaceText)
	FROM DetailFields
	WHERE LeadTypeID = @LeadTypeID
	AND ClientID = @ClientID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DFFindAndReplace] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_DFFindAndReplace] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DFFindAndReplace] TO [sp_executeall]
GO
