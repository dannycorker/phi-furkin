SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2008-05-13
-- Description:	Update statistics for the SqlQuery that has just been run
-- =============================================
CREATE PROCEDURE [dbo].[SqlQuery__UpdateStatistics] 
	
	@SqlQueryID int, 
	@LastRundateStart datetime,
	@LastRundateEnd datetime,
	@LastRowcount int

AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @RunCount int,
	@MaxRuntime int, -- "seconds" is the time unit for all runtime values
	@MaxRowcount int,
	@AvgRuntime int,
	@AvgRowcount int, 
	@TotalRuntime decimal(18,4),
	@TotalRowcount decimal(18,4), 
	@LastRuntime int

	-- Calculate runtime in seconds from the last start/end times
	SELECT @LastRuntime = datediff(ss, @LastRundateStart, @LastRundateEnd)
	IF @LastRuntime = 0
	BEGIN 
		SELECT @LastRuntime = 1
	END

	SELECT @RunCount = RunCount + 1,
	@MaxRuntime = CASE WHEN MaxRuntime > @LastRuntime THEN MaxRuntime ELSE @LastRuntime END,
	@MaxRowcount = CASE WHEN MaxRowcount > @LastRowcount THEN MaxRowcount ELSE @LastRowcount END,
	@TotalRuntime = (AvgRuntime * RunCount) + @LastRuntime,
	@TotalRowcount = (AvgRowcount * RunCount) + @LastRowcount
	FROM SqlQuery
	WHERE QueryID = @SqlQueryID 

	SELECT @AvgRuntime = ceiling(@TotalRuntime / @RunCount),
	@AvgRowcount = ceiling(@TotalRowcount / @RunCount)

	UPDATE SqlQuery 
	SET RunCount = @RunCount,
	LastRundate = @LastRundateStart,
	LastRuntime = @LastRuntime,
	LastRowcount = @LastRowcount,
	MaxRuntime = @MaxRuntime,
	MaxRowcount = @MaxRowcount,
	AvgRuntime = @AvgRuntime,
	AvgRowcount = @AvgRowcount 
	WHERE QueryID = @SqlQueryID 

END





GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__UpdateStatistics] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery__UpdateStatistics] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__UpdateStatistics] TO [sp_executeall]
GO
