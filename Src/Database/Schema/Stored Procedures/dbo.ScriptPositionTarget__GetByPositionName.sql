SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 13-03-2015
-- Description:	Gets a script position target by the position name
-- =============================================
CREATE PROCEDURE [dbo].[ScriptPositionTarget__GetByPositionName]
	@ScriptSectionID INT,
	@PositionName VARCHAR(250)	
AS
BEGIN
	
	
	SET NOCOUNT ON;
	
	SELECT * FROM ScriptPositionTarget WITH (NOLOCK) 
	WHERE SectionID=@ScriptSectionID AND PositionName=@PositionName
    
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget__GetByPositionName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptPositionTarget__GetByPositionName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget__GetByPositionName] TO [sp_executeall]
GO
