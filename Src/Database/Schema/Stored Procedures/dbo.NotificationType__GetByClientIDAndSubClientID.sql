SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 14-03-2012
-- Description:	Gets Notification Type By ClientID and SubClientID
-- =============================================
CREATE PROCEDURE [dbo].[NotificationType__GetByClientIDAndSubClientID]

	@ClientID INT,
	@SubClientID INT = NULL

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @ClientID=188
	BEGIN
		SELECT * FROM NotificationType WITH (NOLOCK) 
		WHERE ClientID=@ClientID	
	END
	ELSE
	BEGIN
		SELECT * FROM NotificationType WITH (NOLOCK) 
		WHERE ClientID=@ClientID
		AND SubClientID = @SubClientID
	END

END





GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationType__GetByClientIDAndSubClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationType__GetByClientIDAndSubClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationType__GetByClientIDAndSubClientID] TO [sp_executeall]
GO
