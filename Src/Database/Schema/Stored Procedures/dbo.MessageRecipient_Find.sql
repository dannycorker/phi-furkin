SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the MessageRecipient table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MessageRecipient_Find]
(

	@SearchUsingOR bit   = null ,

	@MessageRecipientID int   = null ,

	@MessageID int   = null ,

	@ClientPersonnelID int   = null ,

	@PortalUserID int   = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [MessageRecipientID]
	, [MessageID]
	, [ClientPersonnelID]
	, [PortalUserID]
	, [ClientID]
    FROM
	[dbo].[MessageRecipient] WITH (NOLOCK) 
    WHERE 
	 ([MessageRecipientID] = @MessageRecipientID OR @MessageRecipientID IS NULL)
	AND ([MessageID] = @MessageID OR @MessageID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([PortalUserID] = @PortalUserID OR @PortalUserID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [MessageRecipientID]
	, [MessageID]
	, [ClientPersonnelID]
	, [PortalUserID]
	, [ClientID]
    FROM
	[dbo].[MessageRecipient] WITH (NOLOCK) 
    WHERE 
	 ([MessageRecipientID] = @MessageRecipientID AND @MessageRecipientID is not null)
	OR ([MessageID] = @MessageID AND @MessageID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([PortalUserID] = @PortalUserID AND @PortalUserID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[MessageRecipient_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MessageRecipient_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MessageRecipient_Find] TO [sp_executeall]
GO
