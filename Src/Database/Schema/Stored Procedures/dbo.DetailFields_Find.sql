SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DetailFields table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFields_Find]
(

	@SearchUsingOR bit   = null ,

	@DetailFieldID int   = null ,

	@ClientID int   = null ,

	@LeadOrMatter tinyint   = null ,

	@FieldName varchar (50)  = null ,

	@FieldCaption varchar (100)  = null ,

	@QuestionTypeID int   = null ,

	@Required bit   = null ,

	@Lookup bit   = null ,

	@LookupListID int   = null ,

	@LeadTypeID int   = null ,

	@Enabled bit   = null ,

	@DetailFieldPageID int   = null ,

	@FieldOrder int   = null ,

	@MaintainHistory bit   = null ,

	@EquationText varchar (2000)  = null ,

	@MasterQuestionID int   = null ,

	@FieldSize int   = null ,

	@LinkedDetailFieldID int   = null ,

	@ValidationCriteriaFieldTypeID int   = null ,

	@ValidationCriteriaID int   = null ,

	@MinimumValue varchar (50)  = null ,

	@MaximumValue varchar (50)  = null ,

	@RegEx varchar (2000)  = null ,

	@ErrorMessage varchar (250)  = null ,

	@ResourceListDetailFieldPageID int   = null ,

	@TableDetailFieldPageID int   = null ,

	@DefaultFilter varchar (250)  = null ,

	@ColumnEquationText varchar (2000)  = null ,

	@Editable bit   = null ,

	@Hidden bit   = null ,

	@LastReferenceInteger int   = null ,

	@ReferenceValueFormatID int   = null ,

	@Encrypt bit   = null ,

	@ShowCharacters int   = null ,

	@NumberOfCharactersToShow int   = null ,

	@TableEditMode int   = null ,

	@DisplayInTableView bit   = null ,

	@ObjectTypeID int   = null ,

	@SourceID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@DetailFieldStyleID int   = null ,

	@Hyperlink nvarchar (2000)  = null ,

	@IsShared bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DetailFieldID]
	, [ClientID]
	, [LeadOrMatter]
	, [FieldName]
	, [FieldCaption]
	, [QuestionTypeID]
	, [Required]
	, [Lookup]
	, [LookupListID]
	, [LeadTypeID]
	, [Enabled]
	, [DetailFieldPageID]
	, [FieldOrder]
	, [MaintainHistory]
	, [EquationText]
	, [MasterQuestionID]
	, [FieldSize]
	, [LinkedDetailFieldID]
	, [ValidationCriteriaFieldTypeID]
	, [ValidationCriteriaID]
	, [MinimumValue]
	, [MaximumValue]
	, [RegEx]
	, [ErrorMessage]
	, [ResourceListDetailFieldPageID]
	, [TableDetailFieldPageID]
	, [DefaultFilter]
	, [ColumnEquationText]
	, [Editable]
	, [Hidden]
	, [LastReferenceInteger]
	, [ReferenceValueFormatID]
	, [Encrypt]
	, [ShowCharacters]
	, [NumberOfCharactersToShow]
	, [TableEditMode]
	, [DisplayInTableView]
	, [ObjectTypeID]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [DetailFieldStyleID]
	, [Hyperlink]
	, [IsShared]
    FROM
	dbo.fnDetailFieldsShared(@ClientID)
    WHERE 
	 ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadOrMatter] = @LeadOrMatter OR @LeadOrMatter IS NULL)
	AND ([FieldName] = @FieldName OR @FieldName IS NULL)
	AND ([FieldCaption] = @FieldCaption OR @FieldCaption IS NULL)
	AND ([QuestionTypeID] = @QuestionTypeID OR @QuestionTypeID IS NULL)
	AND ([Required] = @Required OR @Required IS NULL)
	AND ([Lookup] = @Lookup OR @Lookup IS NULL)
	AND ([LookupListID] = @LookupListID OR @LookupListID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([Enabled] = @Enabled OR @Enabled IS NULL)
	AND ([DetailFieldPageID] = @DetailFieldPageID OR @DetailFieldPageID IS NULL)
	AND ([FieldOrder] = @FieldOrder OR @FieldOrder IS NULL)
	AND ([MaintainHistory] = @MaintainHistory OR @MaintainHistory IS NULL)
	AND ([EquationText] = @EquationText OR @EquationText IS NULL)
	AND ([MasterQuestionID] = @MasterQuestionID OR @MasterQuestionID IS NULL)
	AND ([FieldSize] = @FieldSize OR @FieldSize IS NULL)
	AND ([LinkedDetailFieldID] = @LinkedDetailFieldID OR @LinkedDetailFieldID IS NULL)
	AND ([ValidationCriteriaFieldTypeID] = @ValidationCriteriaFieldTypeID OR @ValidationCriteriaFieldTypeID IS NULL)
	AND ([ValidationCriteriaID] = @ValidationCriteriaID OR @ValidationCriteriaID IS NULL)
	AND ([MinimumValue] = @MinimumValue OR @MinimumValue IS NULL)
	AND ([MaximumValue] = @MaximumValue OR @MaximumValue IS NULL)
	AND ([RegEx] = @RegEx OR @RegEx IS NULL)
	AND ([ErrorMessage] = @ErrorMessage OR @ErrorMessage IS NULL)
	AND ([ResourceListDetailFieldPageID] = @ResourceListDetailFieldPageID OR @ResourceListDetailFieldPageID IS NULL)
	AND ([TableDetailFieldPageID] = @TableDetailFieldPageID OR @TableDetailFieldPageID IS NULL)
	AND ([DefaultFilter] = @DefaultFilter OR @DefaultFilter IS NULL)
	AND ([ColumnEquationText] = @ColumnEquationText OR @ColumnEquationText IS NULL)
	AND ([Editable] = @Editable OR @Editable IS NULL)
	AND ([Hidden] = @Hidden OR @Hidden IS NULL)
	AND ([LastReferenceInteger] = @LastReferenceInteger OR @LastReferenceInteger IS NULL)
	AND ([ReferenceValueFormatID] = @ReferenceValueFormatID OR @ReferenceValueFormatID IS NULL)
	AND ([Encrypt] = @Encrypt OR @Encrypt IS NULL)
	AND ([ShowCharacters] = @ShowCharacters OR @ShowCharacters IS NULL)
	AND ([NumberOfCharactersToShow] = @NumberOfCharactersToShow OR @NumberOfCharactersToShow IS NULL)
	AND ([TableEditMode] = @TableEditMode OR @TableEditMode IS NULL)
	AND ([DisplayInTableView] = @DisplayInTableView OR @DisplayInTableView IS NULL)
	AND ([ObjectTypeID] = @ObjectTypeID OR @ObjectTypeID IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([DetailFieldStyleID] = @DetailFieldStyleID OR @DetailFieldStyleID IS NULL)
	AND ([Hyperlink] = @Hyperlink OR @Hyperlink IS NULL)
	AND ([IsShared] = @IsShared OR @IsShared IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DetailFieldID]
	, [ClientID]
	, [LeadOrMatter]
	, [FieldName]
	, [FieldCaption]
	, [QuestionTypeID]
	, [Required]
	, [Lookup]
	, [LookupListID]
	, [LeadTypeID]
	, [Enabled]
	, [DetailFieldPageID]
	, [FieldOrder]
	, [MaintainHistory]
	, [EquationText]
	, [MasterQuestionID]
	, [FieldSize]
	, [LinkedDetailFieldID]
	, [ValidationCriteriaFieldTypeID]
	, [ValidationCriteriaID]
	, [MinimumValue]
	, [MaximumValue]
	, [RegEx]
	, [ErrorMessage]
	, [ResourceListDetailFieldPageID]
	, [TableDetailFieldPageID]
	, [DefaultFilter]
	, [ColumnEquationText]
	, [Editable]
	, [Hidden]
	, [LastReferenceInteger]
	, [ReferenceValueFormatID]
	, [Encrypt]
	, [ShowCharacters]
	, [NumberOfCharactersToShow]
	, [TableEditMode]
	, [DisplayInTableView]
	, [ObjectTypeID]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [DetailFieldStyleID]
	, [Hyperlink]
	, [IsShared]
    FROM
	dbo.fnDetailFieldsShared(@ClientID) 
    WHERE 
	 ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadOrMatter] = @LeadOrMatter AND @LeadOrMatter is not null)
	OR ([FieldName] = @FieldName AND @FieldName is not null)
	OR ([FieldCaption] = @FieldCaption AND @FieldCaption is not null)
	OR ([QuestionTypeID] = @QuestionTypeID AND @QuestionTypeID is not null)
	OR ([Required] = @Required AND @Required is not null)
	OR ([Lookup] = @Lookup AND @Lookup is not null)
	OR ([LookupListID] = @LookupListID AND @LookupListID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([Enabled] = @Enabled AND @Enabled is not null)
	OR ([DetailFieldPageID] = @DetailFieldPageID AND @DetailFieldPageID is not null)
	OR ([FieldOrder] = @FieldOrder AND @FieldOrder is not null)
	OR ([MaintainHistory] = @MaintainHistory AND @MaintainHistory is not null)
	OR ([EquationText] = @EquationText AND @EquationText is not null)
	OR ([MasterQuestionID] = @MasterQuestionID AND @MasterQuestionID is not null)
	OR ([FieldSize] = @FieldSize AND @FieldSize is not null)
	OR ([LinkedDetailFieldID] = @LinkedDetailFieldID AND @LinkedDetailFieldID is not null)
	OR ([ValidationCriteriaFieldTypeID] = @ValidationCriteriaFieldTypeID AND @ValidationCriteriaFieldTypeID is not null)
	OR ([ValidationCriteriaID] = @ValidationCriteriaID AND @ValidationCriteriaID is not null)
	OR ([MinimumValue] = @MinimumValue AND @MinimumValue is not null)
	OR ([MaximumValue] = @MaximumValue AND @MaximumValue is not null)
	OR ([RegEx] = @RegEx AND @RegEx is not null)
	OR ([ErrorMessage] = @ErrorMessage AND @ErrorMessage is not null)
	OR ([ResourceListDetailFieldPageID] = @ResourceListDetailFieldPageID AND @ResourceListDetailFieldPageID is not null)
	OR ([TableDetailFieldPageID] = @TableDetailFieldPageID AND @TableDetailFieldPageID is not null)
	OR ([DefaultFilter] = @DefaultFilter AND @DefaultFilter is not null)
	OR ([ColumnEquationText] = @ColumnEquationText AND @ColumnEquationText is not null)
	OR ([Editable] = @Editable AND @Editable is not null)
	OR ([Hidden] = @Hidden AND @Hidden is not null)
	OR ([LastReferenceInteger] = @LastReferenceInteger AND @LastReferenceInteger is not null)
	OR ([ReferenceValueFormatID] = @ReferenceValueFormatID AND @ReferenceValueFormatID is not null)
	OR ([Encrypt] = @Encrypt AND @Encrypt is not null)
	OR ([ShowCharacters] = @ShowCharacters AND @ShowCharacters is not null)
	OR ([NumberOfCharactersToShow] = @NumberOfCharactersToShow AND @NumberOfCharactersToShow is not null)
	OR ([TableEditMode] = @TableEditMode AND @TableEditMode is not null)
	OR ([DisplayInTableView] = @DisplayInTableView AND @DisplayInTableView is not null)
	OR ([ObjectTypeID] = @ObjectTypeID AND @ObjectTypeID is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([DetailFieldStyleID] = @DetailFieldStyleID AND @DetailFieldStyleID is not null)
	OR ([Hyperlink] = @Hyperlink AND @Hyperlink is not null)
	OR ([IsShared] = @IsShared AND @IsShared is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields_Find] TO [sp_executeall]
GO
