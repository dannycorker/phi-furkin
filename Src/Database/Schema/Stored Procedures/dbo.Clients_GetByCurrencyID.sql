SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Clients table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Clients_GetByCurrencyID]
(

	@CurrencyID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientID],
					[CompanyName],
					[WebAddress],
					[IPAddress],
					[DefaultEmailAddress],
					[ClientTypeID],
					[AllowSMS],
					[SecurityCode],
					[LeadsBelongToOffices],
					[UseEventCosts],
					[UseEventUOEs],
					[UseEventDisbursements],
					[UseEventComments],
					[VerifyAddress],
					[UseTapi],
					[UseRPI],
					[UseGBAddress],
					[UsePinpoint],
					[UseSage],
					[UseSAS],
					[UseCreditCalculation],
					[FollowupWorkingDaysOnly],
					[AddLeadByQuestionnaire],
					[UseIncendia],
					[LanguageID],
					[CountryID],
					[UseMobileInterface],
					[UseGBValidation],
					[CurrencyID],
					[AllowSmsCommandProcessing]
				FROM
					[dbo].[Clients] WITH (NOLOCK) 
				WHERE
					[CurrencyID] = @CurrencyID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Clients_GetByCurrencyID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Clients_GetByCurrencyID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Clients_GetByCurrencyID] TO [sp_executeall]
GO
