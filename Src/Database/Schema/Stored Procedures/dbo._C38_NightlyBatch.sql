SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-07-18
-- Description:	Nightly Batch for C38 Aquarium Software
-- JWG 2014-06-09 Added ISNULL check for TitleID, which is mandatory
-- =============================================
CREATE PROCEDURE [dbo].[_C38_NightlyBatch]
AS
BEGIN
	SET NOCOUNT ON;

	EXEC dbo._C38_DailyUserCount

	DECLARE @ContactsAdded INT

	INSERT INTO contact(clientid, customerid, titleid, firstname, lastname, emailaddresswork, emailaddressother, directdial, mobilephonework, address1, address2, town, county, postcode, OfficeID, DepartmentID, JobTitle, Notes)
	SELECT l.ClientID, l.CustomerID, ISNULL(cp.TitleID, 0), cp.FirstName, cp.LastName, cp.EmailAddress, '', cp.OfficeTelephone, cp.MobileTelePhone, co.Address1, co.Address2, co.Town, co.County,co.PostCode, NULL, NULL, cp.JobTitle, ''
	FROM clientpersonnel cp
	INNER JOIN ClientOffices co ON co.ClientOfficeID = cp.ClientOfficeID
	INNER JOIN lead l ON l.leadref = cp.clientid AND l.clientid = 38
	WHERE NOT EXISTS (
		SELECT *
		FROM contact
		WHERE emailaddresswork = cp.emailaddress
		AND contact.clientid = 38
	)
	AND emailaddress NOT LIKE '%aquarium%'
	AND cp.accountdisabled = 0
	
	SELECT @ContactsAdded = @@ROWCOUNT

	-- Return anything at all as a result set for the scheduler...
	SELECT @ContactsAdded AS [COUNT], 'Contacts Addded' [OF]
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_NightlyBatch] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_NightlyBatch] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_NightlyBatch] TO [sp_executeall]
GO
