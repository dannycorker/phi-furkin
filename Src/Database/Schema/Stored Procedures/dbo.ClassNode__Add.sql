SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-30
-- Description:	Add ClassNode
-- =============================================
CREATE PROCEDURE [dbo].[ClassNode__Add] 
	@ClientPersonnelID INT = NULL,
	@ClientID INT = NULL,
	@ParentClassNodeID INT = NULL,
	@NodeType INT = NULL,
	@Name VARCHAR(255) = NULL,
	@ClassDescription VARCHAR(255) = NULL,
	@NodeOrder INT = NULL
	-- @WhoCreated = ClientPersonnelID
	-- @WhenCreated = Current DATETIME
	-- @WhoModified = ClientPersonnelID
	-- @WhenModified = Current DATETIME
	
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [dbo].[ClassNode] (
		[ClientID],
		[ParentClassNodeID],
		[NodeType],
		[Name],
		[ClassDescription],
		[NodeOrder],
		[WhoCreated],
		[WhenCreated],
		[WhoModified],
		[WhenModified]
	)
	VALUES (
		@ClientID,
		@ParentClassNodeID,
		@NodeType,
		@Name,
		@ClassDescription,
		@NodeOrder,
		@ClientPersonnelID,
		dbo.fn_GetDate_Local(),
		@ClientPersonnelID,
		dbo.fn_GetDate_Local()
	)
	
	SELECT TOP 1 *
	FROM [dbo].[ClassNode]
	ORDER BY [ClassNodeID] DESC
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode__Add] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNode__Add] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode__Add] TO [sp_executeall]
GO
