SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2009-11-12
-- Description:	Format huge strings as a table of smaller strings
--				AE Aded History 2010-03-08
-- =============================================
CREATE PROCEDURE [dbo].[SplitString]  
	@InputStr VARCHAR(8000),
	@InputType VARCHAR(10) = 'ERROR',
	@Delimiter VARCHAR(2000) = ' at',
	@InfinityTest INT = 30,
	@History INT = 0,
	@UseOldVersion BIT = 0
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE	@LeftStr VARCHAR(8000), 
			@AtPos INT,
			@LenDelim INT,
			@LeftLen INT,
			@RightLen INT,
			@LoopCount INT = 0,
			@ClientID INT,
			@OutputStr VARCHAR(8000)

	DECLARE @TLine TABLE 
		(
			LineNum INT IDENTITY(1, 1), 
			TextLine VARCHAR(8000)
		)

	-- Check to see if a SqlQueryID has been passed in:
	
	IF ISNUMERIC(@InputStr) = 1
	BEGIN
	
		IF @History = 0
		BEGIN
	
			SELECT TOP 1 @InputStr = LTRIM(RTRIM(sq.QueryText)) , @ClientID = sq.ClientID
			FROM dbo.SqlQuery sq (NOLOCK) 
			WHERE sq.QueryID = @InputStr 
			
		END
		ELSE
		BEGIN
		
			SELECT TOP (@History) @InputStr = LTRIM(RTRIM(sq.QueryText)) , @ClientID = sq.ClientID
			FROM dbo.SqlQueryHistory sq (NOLOCK) 
			WHERE sq.QueryID = @InputStr 
			ORDER BY sq.SqlQueryHistoryID

		END		
		
		SELECT @InputType = 'SQL'
		
		/* 2010-02-24 Added by AE as Sometimes 
		Splitstring doesnt split correctly 
		e.g. exec SplitString 4965 */
		
		/*CS 2012-08-07 because having to replace the @SessionParameters when debugging queries is a pain*/
		SELECT @OutputStr = REPLACE(REPLACE(REPLACE(REPLACE(@InputStr,'@ClientID', CONVERT(VARCHAR,@ClientID) + ' /*@ClientID in QueryText*/'),'@Yesterday''', CONVERT(VARCHAR(10),dbo.fn_GetDate_Local()-1,121) + ''' /*@Yesterday in QueryText*/ '),'@Today''', CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),121) + ''' /*@Today in QueryText*/ '),'@Tomorrow''', CONVERT(VARCHAR(10),dbo.fn_GetDate_Local()+1,121) + ''' /*@Tomorrow in QueryText*/ ')
		PRINT @OutputStr
		
	END
	
	IF @InputType = 'SQL' AND @UseOldVersion = 0
	BEGIN
	
		DECLARE @F AS TABLE (LineNum INT IDENTITY(1, 1),string VARCHAR(MAX))
		DECLARE @QueryText NVARCHAR(MAX)

		SELECT @QueryText = @InputStr

		WHILE LEN(@QueryText) > CHARINDEX(CHAR(13),@QueryText) AND CHARINDEX(CHAR(13),@QueryText) > 0
		BEGIN
			INSERT @F (string)
			SELECT LEFT(@QueryText,CHARINDEX(CHAR(13),@QueryText))

			SELECT @QueryText = RIGHT(@QueryText,LEN(@QueryText) - CHARINDEX(CHAR(13),@QueryText)-1)
		END
		
		SELECT * FROM @F f ORDER BY f.LineNum
	
	END
	
	IF @UseOldVersion = 1
	BEGIN
	-- SQL has lots of keywords to split on	
		IF @InputType = 'SQL'
		BEGIN
			
			DECLARE @SelectPos INT = 0,
					@FromPos INT = 0,
					@JoinPos INT = 0,
					@WherePos INT = 0,
					@AndPos INT = 0,
					@GroupByPos INT = 0,
					@HavingPos INT = 0,
					@OrderByPos INT = 0
			
			DECLARE @SelectKeyword VARCHAR(10) = 'SELECT ',
					@FromKeyword VARCHAR(10) = 'FROM ',
					@JoinKeyword VARCHAR(10) = 'INNER JOIN ', --LEFT/RIGHT/FULL/INNER/OUTER/CROSS/JOIN
					@WhereKeyword VARCHAR(10) = 'WHERE ',
					@AndKeyword VARCHAR(10) = 'AND ',
					@GroupByKeyword VARCHAR(10) = 'GROUP BY ',
					@HavingKeyword VARCHAR(10) = 'HAVING ',
					@OrderByKeyword VARCHAR(10) = 'ORDER BY' 
			
			SELECT @InputStr = RTRIM(@InputStr)
					
			SELECT @SelectPos = CHARINDEX(@SelectKeyword, @InputStr, 1)
			

			IF @SelectPos > 0
			BEGIN

			
				/* FROM */			
				SELECT @FromPos = CHARINDEX(@FromKeyword, @InputStr, @SelectPos)

				IF @FromPos > 0
				BEGIN
					
					-- Output anything before the FROM clause as one line.
					-- If the sql starts "WITH ToughQuery AS (" etc then this section could do
					-- with more sophistication really
					SELECT @Leftlen = @FromPos - 1
					
					-- Grab this text.
					SELECT @LeftStr = LEFT(@InputStr, @Leftlen)
					
					-- Insert it into the output table.
					INSERT @TLine (TextLine) VALUES (@LeftStr)
					
					-- Then work out how much to keep from the rest of the string.
					SELECT @RightLen = LEN(@InputStr) - (@FromPos - 1)
					
					-- Finally, keep only the remaining text, then loop round and do it all again.
					SELECT @InputStr = LTRIM(RTRIM(RIGHT(@InputStr, @RightLen)))

					
					-- Repeat [loop] for JOIN clauses 
					-- Don't start at column 1, cos that is where the current JOIN statement will be!!
					SELECT @JoinPos = CHARINDEX(@JoinKeyword, @InputStr, 2)
					IF @JoinPos = 0
					BEGIN
						SELECT @JoinPos = CHARINDEX('LEFT JOIN ', @InputStr, 2)
					END
				
					WHILE @JoinPos > 0 AND @LoopCount < @InfinityTest
					BEGIN
						
						SELECT @LoopCount += 1
					
						SELECT @Leftlen = @JoinPos - 1
						
						-- Grab this text.
						SELECT @LeftStr = LEFT(@InputStr, @Leftlen)
						
						-- Insert it into the output table.
						INSERT @TLine (TextLine) VALUES (@LeftStr)
						
						-- Then work out how much to keep from the rest of the string.
						SELECT @RightLen = LEN(@InputStr) - (@JoinPos - 1)
						
						-- Finally, keep only the remaining text, then loop round and do it all again.
						SELECT @InputStr = LTRIM(RTRIM(RIGHT(@InputStr, @RightLen)))

						-- Loop round again looking for more JOIN clauses
						-- Don't start at column 1, cos that is where the current JOIN statement will be!!
						SELECT @JoinPos = 0
						
						SELECT @JoinPos = CHARINDEX(@JoinKeyword, @InputStr, 2)
						IF @JoinPos = 0
						BEGIN
							SELECT @JoinPos = CHARINDEX('LEFT JOIN ', @InputStr, 2)
						END
						
					END		

					/* WHERE */			
					-- Output anything before the WHERE clause as one line.
					SELECT @WherePos = CHARINDEX(@WhereKeyword, @InputStr, 1)

					SELECT @Leftlen = @WherePos - 1
					
					-- Grab this text.
					SELECT @LeftStr = LEFT(@InputStr, @Leftlen)
					
					-- Insert it into the output table.
					INSERT @TLine (TextLine) VALUES (@LeftStr)
					
					-- Then work out how much to keep from the rest of the string.
					SELECT @RightLen = LEN(@InputStr) - (@WherePos - 1)
					
					-- Finally, keep only the remaining text, then loop round and do it all again.
					SELECT @InputStr = LTRIM(RTRIM(RIGHT(@InputStr, @RightLen)))
					
					
					-- Repeat [loop] for AND clauses 
					-- Don't start at column 1, cos that is where the current JOIN statement will be!!
					SELECT @AndPos = CHARINDEX(@AndKeyword, @InputStr, 2)
				
					WHILE @AndPos > 0 AND @LoopCount < @InfinityTest
					BEGIN
						
						SELECT @LoopCount += 1
					
						SELECT @Leftlen = @AndPos - 1
						
						-- Grab this text.
						SELECT @LeftStr = LEFT(@InputStr, @Leftlen)
						
						-- Insert it into the output table.
						INSERT @TLine (TextLine) VALUES (@LeftStr)
						
						-- Then work out how much to keep from the rest of the string.
						SELECT @RightLen = LEN(@InputStr) - (@AndPos - 1)
						
						-- Finally, keep only the remaining text, then loop round and do it all again.
						SELECT @InputStr = LTRIM(RTRIM(RIGHT(@InputStr, @RightLen)))
						
						-- Loop round again looking for more AND clauses
						SELECT @AndPos = 0
						
						-- Don't start at column 1, cos that is where the current JOIN statement will be!!
						SELECT @AndPos = CHARINDEX(@AndKeyword, @InputStr, 2)
						
					END			
				

					/* ORDER BY */			
					-- ORDER BY is always dead last.
					SELECT @OrderByPos = CHARINDEX(@OrderByKeyword, @InputStr, @HavingPos)

					IF @OrderByPos > 0
					BEGIN
					
						SELECT @Leftlen = @OrderByPos - 1
						
						-- Grab this text.
						SELECT @LeftStr = LEFT(@InputStr, @Leftlen)
						
						-- Insert it into the output table.
						INSERT @TLine (TextLine) VALUES (@LeftStr)
						
						-- Then work out how much to keep from the rest of the string.
						SELECT @RightLen = LEN(@InputStr) - (@OrderByPos - 1)
						
						-- Finally, keep only the remaining text, then loop round and do it all again.
						SELECT @InputStr = LTRIM(RTRIM(RIGHT(@InputStr, @RightLen)))
						
						-- Stop here!
						INSERT @TLine (TextLine) VALUES (@InputStr)
					
					END
					ELSE
					BEGIN
						-- Don't forget to output the final if there was no ORDER BY at all.
						INSERT @TLine (TextLine) VALUES (@InputStr)
					END				
				END
				
			END
			ELSE
			BEGIN
				-- Too weird for this helper proc (eg "BEGIN" or "DECLARE @SomeVariables" or "EXEC _SomethingOrOther")
				-- Just output the whole thing and forget it.
				INSERT @TLine (TextLine) VALUES (@InputStr)
			END
			
		END
		ELSE
		BEGIN
		
			-- Everything else just split at the delimiter passed in 
			SELECT @LenDelim = LEN(@Delimiter)
			
			WHILE LEN(@InputStr) > 0 AND @LoopCount < @InfinityTest
			BEGIN

				SELECT @LoopCount += 1
				
				SELECT @AtPos = CHARINDEX(@Delimiter, @InputStr, 1)
				
				-- Delimiter found?
				IF @AtPos > 0
				BEGIN

					-- Delimiter found. Work out how much text to grab.
					SELECT @Leftlen = @AtPos + (@LenDelim - 1)
					
					-- Grab this text.
					SELECT @LeftStr = LEFT(@InputStr, @Leftlen)
					
					-- Insert it into the output table.
					INSERT @TLine (TextLine) VALUES (@LeftStr)
					
					-- Then work out how much to keep from the rest of the string.
					SELECT @RightLen = LEN(@InputStr) - (@AtPos + @LenDelim - 1)
					
					-- Finally, keep only the remaining text, then loop round and do it all again.
					SELECT @InputStr = RIGHT(@InputStr, @RightLen)
					
				END
				ELSE
				BEGIN

					-- We have run out of delimiters. Show the final chunk of text.
					INSERT @TLine (TextLine) VALUES (@InputStr)
					
					SELECT @InputStr = ''
					
				END

			END

		END

		-- Output the results
		SELECT *
		FROM @tline 
		ORDER BY linenum
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[SplitString] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SplitString] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SplitString] TO [sp_executeall]
GO
