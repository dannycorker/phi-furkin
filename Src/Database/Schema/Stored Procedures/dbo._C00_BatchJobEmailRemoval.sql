SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Louis Bromilow (via CS)
-- Create date: 2012-07-19
-- Description:	Remove a specific email address from batch job parameters and repair as necessary
-- =============================================
CREATE PROCEDURE [dbo].[_C00_BatchJobEmailRemoval]
	@Email AS VARCHAR(200),
	@ClientID as VarChar (10) = null,
	@ReadOnly as bit = 1
AS
BEGIN
	SET NOCOUNT ON;

	Declare @myERROR int,
			@NoOfNonWorkingDays int

	Select @ClientID = ISNULL(@ClientID,-1)

	Select @Email [@Email], @ClientID [@ClientID], @ReadOnly [@ReadOnly]

	IF @ReadOnly = 1
	BEGIN
		Select atp.ClientID, atp.TaskID, atp.ParamValue, REPLACE(atp.ParamValue,' ','') [Set To] from AutomatedTask a WITH (NOLOCK) 
		Inner join AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = a.TaskID
		and @ClientID in (a.ClientID, -1)
		and (atp.ParamName like 'EMAIL_TO' or atp.ParamName like 'EMAIL_%CC' or atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		and atp.ParamValue like '%'+@Email+'%'

		Select atp.ClientID, atp.TaskID, atp.ParamValue, REPLACE(atp.ParamValue,@Email + ',','') [Set To] from AutomatedTask a WITH (NOLOCK) 
		Inner join AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = a.TaskID
		and @ClientID in (a.ClientID, -1)
		and (atp.ParamName like 'EMAIL_TO' or atp.ParamName like 'EMAIL_%CC' or atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		and atp.ParamValue like @Email + ',%'

		Select atp.ClientID, atp.TaskID, atp.ParamValue, REPLACE(atp.ParamValue,','+@Email+',',',') [Set To] from AutomatedTask a WITH (NOLOCK) 
		Inner join AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = a.TaskID
		and @ClientID in (a.ClientID, -1)
		and (atp.ParamName like 'EMAIL_TO' or atp.ParamName like 'EMAIL_%CC' or atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		and atp.ParamValue like '%,'+@Email+',%'

		Select atp.ClientID, atp.TaskID, atp.ParamValue, REPLACE(atp.ParamValue,','+@Email,'') [Set To] from AutomatedTask a WITH (NOLOCK) 
		Inner join AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = a.TaskID
		and @ClientID in (a.ClientID, -1)
		and (atp.ParamName like 'EMAIL_TO' or atp.ParamName like 'EMAIL_%CC' or atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		and atp.ParamValue like '%,'+ @Email

		Select atp.ClientID, atp.TaskID, atp.ParamValue, REPLACE(atp.ParamValue,@Email,'') [Set To] from AutomatedTask a WITH (NOLOCK) 
		Inner join AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = a.TaskID
		and @ClientID in (a.ClientID, -1)
		and (atp.ParamName like 'EMAIL_TO' or atp.ParamName like 'EMAIL_%CC' or atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		and atp.ParamValue = @Email

		Select atp.ClientID, atp.TaskID, atp.ParamValue, REPLACE(atp.ParamValue,',,',',') [Set To] from AutomatedTask a WITH (NOLOCK) 
		Inner join AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = a.TaskID
		and @ClientID in (a.ClientID, -1)
		and (atp.ParamName like 'EMAIL_TO' or atp.ParamName like 'EMAIL_%CC' or atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		and atp.ParamValue like '%,,%'
		
		Select atp.ParamValue [Email TO],atp2.ParamValue [Email CC/BCC],atp3.ParamValue [SEND_RESULTS],'Set to NEVER' [Update]
		from AutomatedTask a WITH (NOLOCK) 
		Inner join AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = a.TaskID 
		Inner join AutomatedTaskParam atp2 WITH (NOLOCK) on atp.TaskID = a.TaskID and atp2.TaskID = atp.TaskID
		Inner join AutomatedTaskParam atp3 WITH (NOLOCK) on atp.TaskID = a.TaskID and atp3.TaskID = atp.TaskID
		WHERE atp.ParamName like 'EMAIL_TO'
		and atp2.ParamName like 'EMAIL_%CC'
		and atp3.ParamName like 'SEND_RESULTS'
		and atp3.ParamValue not like 'No' and atp3.ParamValue not like 'Never'
		and (atp.ParamValue = @Email or atp.ParamValue = '') 
		and (atp2.ParamValue = '') 
		and @ClientID in(a.ClientID, -1)

	END
	ELSE
	BEGIN
		Update AutomatedTaskParam
		Set ParamValue = REPLACE(atp.ParamValue,' ','') from AutomatedTask a WITH (NOLOCK) 
		Inner join AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = a.TaskID
		and @ClientID in (a.ClientID, -1)
		and (atp.ParamName like 'EMAIL_TO' or atp.ParamName like 'EMAIL_%CC' or atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		and atp.ParamValue like '%'+@Email+'%'
		
		SELECT @@RowCount [Remove Spaces]

		Update AutomatedTaskParam
		Set ParamValue = REPLACE(atp.ParamValue,@Email + ',','') from AutomatedTask a WITH (NOLOCK) 
		Inner join AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = a.TaskID
		and @ClientID in (a.ClientID, -1)
		and (atp.ParamName like 'EMAIL_TO' or atp.ParamName like 'EMAIL_%CC' or atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		and atp.ParamValue like @Email + ',%'

		SELECT @@RowCount [Replace Including Comma]

		Update AutomatedTaskParam
		Set ParamValue = REPLACE(atp.ParamValue,','+@Email+',',',') from AutomatedTask a WITH (NOLOCK) 
		Inner join AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = a.TaskID
		and @ClientID in (a.ClientID, -1)
		and (atp.ParamName like 'EMAIL_TO' or atp.ParamName like 'EMAIL_%CC' or atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		and atp.ParamValue like '%,'+@Email+',%'
		
		SELECT @@RowCount [Replace Between Commas]

		Update AutomatedTaskParam
		Set ParamValue = REPLACE(atp.ParamValue,','+@Email,'') from AutomatedTask a WITH (NOLOCK) 
		Inner join AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = a.TaskID
		and @ClientID in (a.ClientID, -1)
		and (atp.ParamName like 'EMAIL_TO' or atp.ParamName like 'EMAIL_%CC' or atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		and atp.ParamValue like '%,'+ @Email
		
		SELECT @@RowCount [Replace as Final Email]

		Update AutomatedTaskParam
		Set ParamValue = REPLACE(atp.ParamValue,@Email,'') from AutomatedTask a WITH (NOLOCK) 
		Inner join AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = a.TaskID
		and @ClientID in (a.ClientID, -1)
		and (atp.ParamName like 'EMAIL_TO' or atp.ParamName like 'EMAIL_%CC' or atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		and atp.ParamValue = @Email
		
		SELECT @@RowCount [Replace Single Email]

		Update AutomatedTaskParam
		Set ParamValue = REPLACE(atp.ParamValue,',,',',') from AutomatedTask a WITH (NOLOCK) 
		Inner join AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = a.TaskID
		and @ClientID in (a.ClientID, -1)
		and (atp.ParamName like 'EMAIL_TO' or atp.ParamName like 'EMAIL_%CC' or atp.ParamName = 'ALERT_ON_ERROR_EMAIL')
		and atp.ParamValue like '%,,%'
		
		SELECT @@RowCount [Fix Double Commas]
		
		UPDATE atp3
		SET ParamValue = 'Never'
		from AutomatedTask a WITH (NOLOCK) 
		Inner join AutomatedTaskParam atp WITH (NOLOCK) on atp.TaskID = a.TaskID 
		Inner join AutomatedTaskParam atp2 WITH (NOLOCK) on atp.TaskID = a.TaskID and atp2.TaskID = atp.TaskID
		Inner join AutomatedTaskParam atp3 WITH (NOLOCK) on atp.TaskID = a.TaskID and atp3.TaskID = atp.TaskID
		WHERE atp.ParamName like 'EMAIL_TO'
		and atp2.ParamName like 'EMAIL_%CC'
		and atp3.ParamName like 'SEND_RESULTS'
		and atp3.ParamValue not like 'No' and atp3.ParamValue not like 'Never'
		and (atp.ParamValue = @Email or atp.ParamValue = '') 
		and (atp2.ParamValue = '') 
		and @ClientID in(a.ClientID, -1)
		
		SELECT @@RowCount [Turn off unnecessary emails]
		
	END

END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_BatchJobEmailRemoval] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_BatchJobEmailRemoval] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_BatchJobEmailRemoval] TO [sp_executeall]
GO
