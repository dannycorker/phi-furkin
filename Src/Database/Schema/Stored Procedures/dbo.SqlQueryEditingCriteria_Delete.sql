SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SqlQueryEditingCriteria table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteria_Delete]
(

	@SqlQueryEditingCriteriaID int   
)
AS


				DELETE FROM [dbo].[SqlQueryEditingCriteria] WITH (ROWLOCK) 
				WHERE
					[SqlQueryEditingCriteriaID] = @SqlQueryEditingCriteriaID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteria_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_Delete] TO [sp_executeall]
GO
