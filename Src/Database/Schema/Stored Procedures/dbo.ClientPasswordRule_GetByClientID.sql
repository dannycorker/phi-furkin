SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPasswordRule table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPasswordRule_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientPasswordRuleID],
					[ClientID],
					[MinLength],
					[MinLowerCase],
					[MinUpperCase],
					[MinNumber],
					[MinSymbol],
					[ExpiryDays],
					[ResetForcesChange],
					[PasswordHistoryCount]
				FROM
					[dbo].[ClientPasswordRule] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPasswordRule_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPasswordRule_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPasswordRule_GetByClientID] TO [sp_executeall]
GO
