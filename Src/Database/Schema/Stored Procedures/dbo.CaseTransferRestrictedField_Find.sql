SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the CaseTransferRestrictedField table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferRestrictedField_Find]
(

	@SearchUsingOR bit   = null ,

	@CaseTransferRestrictedFieldID int   = null ,

	@ClientRelationshipID int   = null ,

	@DetailFieldID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CaseTransferRestrictedFieldID]
	, [ClientRelationshipID]
	, [DetailFieldID]
    FROM
	[dbo].[CaseTransferRestrictedField] WITH (NOLOCK) 
    WHERE 
	 ([CaseTransferRestrictedFieldID] = @CaseTransferRestrictedFieldID OR @CaseTransferRestrictedFieldID IS NULL)
	AND ([ClientRelationshipID] = @ClientRelationshipID OR @ClientRelationshipID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CaseTransferRestrictedFieldID]
	, [ClientRelationshipID]
	, [DetailFieldID]
    FROM
	[dbo].[CaseTransferRestrictedField] WITH (NOLOCK) 
    WHERE 
	 ([CaseTransferRestrictedFieldID] = @CaseTransferRestrictedFieldID AND @CaseTransferRestrictedFieldID is not null)
	OR ([ClientRelationshipID] = @ClientRelationshipID AND @ClientRelationshipID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferRestrictedField_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferRestrictedField_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferRestrictedField_Find] TO [sp_executeall]
GO
