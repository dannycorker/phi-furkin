SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetAvatarTextToSpeak] @ClientQuestionnaireID int, @PageNumber int

AS

Select TextToSpeakID, ClientQuestionnaireID, ClientID, PageNumber, SpeakText, IsShown
From TextToSpeak
Where ( (ClientQuestionnaireID = @ClientQuestionnaireID) and (PageNumber = @PageNumber) )



GO
GRANT VIEW DEFINITION ON  [dbo].[GetAvatarTextToSpeak] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAvatarTextToSpeak] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAvatarTextToSpeak] TO [sp_executeall]
GO
