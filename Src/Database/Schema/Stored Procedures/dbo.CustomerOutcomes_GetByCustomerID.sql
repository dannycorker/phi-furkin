SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CustomerOutcomes table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerOutcomes_GetByCustomerID]
(

	@CustomerID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[CustomerOutcomeID],
					[ClientQuestionnaireID],
					[CustomerID],
					[OutcomeID],
					[ClientID]
				FROM
					[dbo].[CustomerOutcomes] WITH (NOLOCK) 
				WHERE
					[CustomerID] = @CustomerID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOutcomes_GetByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerOutcomes_GetByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOutcomes_GetByCustomerID] TO [sp_executeall]
GO
