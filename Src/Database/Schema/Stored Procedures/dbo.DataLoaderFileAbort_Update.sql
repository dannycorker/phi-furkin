SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DataLoaderFileAbort table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileAbort_Update]
(

	@DataLoaderFileAbortID int   ,

	@ClientID int   ,

	@DateTimeFileAborted datetime   ,

	@ReasonForAbort varchar (1000)  ,

	@DataLoaderFileID int   ,

	@FileAbortedOn datetime   ,

	@LastRowImported int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DataLoaderFileAbort]
				SET
					[ClientID] = @ClientID
					,[DateTimeFileAborted] = @DateTimeFileAborted
					,[ReasonForAbort] = @ReasonForAbort
					,[DataLoaderFileID] = @DataLoaderFileID
					,[FileAbortedOn] = @FileAbortedOn
					,[LastRowImported] = @LastRowImported
				WHERE
[DataLoaderFileAbortID] = @DataLoaderFileAbortID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileAbort_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileAbort_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileAbort_Update] TO [sp_executeall]
GO
