SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-12-02
-- Description:	Set Next Payment Dates for Finance Systems
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SetNextPayments]
@LeadTypeID int,
@ClientID int,
@NextPaymentDueDateFieldID int,
@DateOfMonthForPaymentFieldID int,
@PaymentFrequencyFieldID int
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @Nextdate int

	DECLARE @RequiredFields TABLE 
	(
		DetailFieldID int
	)

	INSERT @RequiredFields 
	SELECT @NextPaymentDueDateFieldID

	/*UNION SELECT nnnm */

	DECLARE @LeadsAndMatters TABLE 
	(
		LeadID int, 
		MatterID int 
	);

	INSERT INTO @LeadsAndMatters (LeadID, MatterID)
	SELECT DISTINCT m.LeadID, m.MatterID 
	FROM Matter m with (nolock) 
	INNER JOIN Lead l with (nolock)  ON l.LeadID = m.LeadID AND l.LeadTypeID = @LeadTypeID 
	Inner Join Customers c with (nolock) on c.CustomerID = l.CustomerID and (c.Test IS NULL or c.Test = 0)
	WHERE m.ClientID = @ClientID

	/* Create all necessary MatterDetailValues records that don't already exist */
	INSERT INTO MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
	SELECT @ClientID,lam.LeadID,lam.MatterID,rf.DetailFieldID,'' 
	FROM @LeadsAndMatters lam 
	CROSS JOIN @RequiredFields rf 
	WHERE NOT EXISTS (SELECT * FROM MatterDetailValues mdv WHERE mdv.MatterID = lam.MatterID AND mdv.DetailFieldID = rf.DetailFieldID) 
	
	IF @PaymentFrequencyFieldID IS NOT NULL
	BEGIN
	
		/*	Update Next Payment Date uing Working Days - Monthly*/
		update matterdetailvalues 
		set detailvalue = convert(char(10), dbo.fnGetNextPaymentDate(dbo.fn_GetDate_Local(), convert(int,luli.itemvalue), 11), 126)
		from matterdetailvalues 
		inner join matterdetailvalues mdv2 on mdv2.matterid = matterdetailvalues.matterid and mdv2.detailfieldid = @DateOfMonthForPaymentFieldID /*Date of Month for Payment*/
		inner join matterdetailvalues mdv on mdv.matterid = matterdetailvalues.matterid and mdv.detailfieldid = @PaymentFrequencyFieldID and mdv.detailvalue IN (5672,13443,13443) -- Monthly Payments
		inner join lookuplistitems luli on luli.lookuplistitemid = mdv2.valueint 
		where matterdetailvalues.detailfieldid = @NextPaymentDueDateFieldID /*Next Payment Due Date*/
		and (datediff(dd, isnull(matterdetailvalues.ValueDate, dbo.fn_GetDate_Local()-1), dbo.fn_GetDate_Local()) > 0 or datediff(dd, isnull(matterdetailvalues.ValueDate, dbo.fn_GetDate_Local()-1), dbo.fn_GetDate_Local()) > 11)
		/*2011-05-18 ACE Changed from
		and matterdetailvalues.ValueDate is not null and (datediff(dd, isnull(matterdetailvalues.ValueDate, dbo.fn_GetDate_Local()+1), dbo.fn_GetDate_Local()) > 0 or datediff(dd, isnull(matterdetailvalues.ValueDate, dbo.fn_GetDate_Local()+1), dbo.fn_GetDate_Local()) > 11)*/
	
	END
	ELSE
	BEGIN
	
		/*	Update Next Payment Date uing Working Days - Monthly*/
		update matterdetailvalues 
		set detailvalue = convert(char(10), dbo.fnGetNextPaymentDate(dbo.fn_GetDate_Local(), convert(int,luli.itemvalue), 11), 126)
		from matterdetailvalues 
		inner join matterdetailvalues mdv2 on mdv2.matterid = matterdetailvalues.matterid and mdv2.detailfieldid = @DateOfMonthForPaymentFieldID /*Date of Month for Payment*/
		inner join lookuplistitems luli on luli.lookuplistitemid = mdv2.valueint 
		where matterdetailvalues.detailfieldid = @NextPaymentDueDateFieldID /*Next Payment Due Date*/
		and (datediff(dd, isnull(matterdetailvalues.ValueDate, dbo.fn_GetDate_Local()-1), dbo.fn_GetDate_Local()) > 0 or datediff(dd, isnull(matterdetailvalues.ValueDate, dbo.fn_GetDate_Local()-1), dbo.fn_GetDate_Local()) > 11)

	END

	/*	Update Next Payment Date uing Working Days - Quarterly*/
	update matterdetailvalues 
	set detailvalue = convert(char(10), dbo.fnGetNextPaymentDate(dateadd(mm,2,dbo.fn_GetDate_Local()), convert(int,luli.itemvalue), 11), 126)
	from matterdetailvalues 
	inner join matterdetailvalues mdv2 on mdv2.matterid = matterdetailvalues.matterid and mdv2.detailfieldid = @DateOfMonthForPaymentFieldID /*Date of Month for Payment*/
	inner join matterdetailvalues mdv on mdv.matterid = matterdetailvalues.matterid and mdv.detailfieldid = @PaymentFrequencyFieldID and mdv.detailvalue IN (5715,13445,13445) -- Quarterly Payments
	inner join lookuplistitems luli on luli.lookuplistitemid = mdv2.valueint 
	where matterdetailvalues.detailfieldid = @NextPaymentDueDateFieldID /*Next Payment Due Date*/
	and (datediff(dd, isnull(matterdetailvalues.ValueDate, dbo.fn_GetDate_Local()-1), dbo.fn_GetDate_Local()) > 0 or datediff(dd, isnull(matterdetailvalues.ValueDate, dbo.fn_GetDate_Local()-1), dbo.fn_GetDate_Local()) > 11)

	If datepart(dd,dbo.fn_GetDate_Local()) between 1 and 6 Select @Nextdate = 4
	If datepart(dd,dbo.fn_GetDate_Local()) between 7 and 13 Select @Nextdate = 11
	If datepart(dd,dbo.fn_GetDate_Local()) between 14 and 20 Select @Nextdate = 18
	If datepart(dd,dbo.fn_GetDate_Local()) between 21 and 27 Select @Nextdate = 25
	If @Nextdate is NULL Select @Nextdate = 4

	/*	Update Next Payment Date uing Working Days - Weekly*/
	update matterdetailvalues 
	set detailvalue = convert(char(10), dbo.fnGetNextPaymentDate(dbo.fn_GetDate_Local(), @Nextdate, 0), 126)
	from matterdetailvalues 
	inner join matterdetailvalues mdv2 on mdv2.matterid = matterdetailvalues.matterid and mdv2.detailfieldid = @DateOfMonthForPaymentFieldID /*Date of Month for Payment*/
	inner join matterdetailvalues mdv on mdv.matterid = matterdetailvalues.matterid and mdv.detailfieldid = @PaymentFrequencyFieldID and mdv.detailvalue IN (5671,13442,13442) -- Weekly Payments
	inner join lookuplistitems luli on luli.lookuplistitemid = mdv2.valueint 
	where matterdetailvalues.detailfieldid = @NextPaymentDueDateFieldID /*Next Payment Due Date*/
	and (datediff(dd, isnull(matterdetailvalues.ValueDate, dbo.fn_GetDate_Local()-1), dbo.fn_GetDate_Local()) > 0 )
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SetNextPayments] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SetNextPayments] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SetNextPayments] TO [sp_executeall]
GO
