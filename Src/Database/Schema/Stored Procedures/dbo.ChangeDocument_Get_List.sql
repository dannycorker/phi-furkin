SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ChangeDocument table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeDocument_Get_List]

AS


				
				SELECT
					[ChangeDocumentID],
					[DocumentData],
					[DocumentExtension],
					[WhenCreated],
					[WhoCreated]
				FROM
					[dbo].[ChangeDocument] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDocument_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeDocument_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDocument_Get_List] TO [sp_executeall]
GO
