SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Panels table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Panels_GetByClientPersonnelID]
(

	@ClientPersonnelID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[PanelID],
					[ClientID],
					[ClientPersonnelID],
					[PanelName]
				FROM
					[dbo].[Panels] WITH (NOLOCK) 
				WHERE
					[ClientPersonnelID] = @ClientPersonnelID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Panels_GetByClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Panels_GetByClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Panels_GetByClientPersonnelID] TO [sp_executeall]
GO
