SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- ==============================================================
-- Author:		Jan Wilson
-- Create date: 2012-04-04
-- Description:	Updates a document batch detail record  
--				with a lock indicating who the document
--				needs to be escalated to.

-- Parameters:  @DocumentBatchDetailID
--              The document batch entry to update
--				@ClientPersonnelId
--              The user to escalate to.
-- ==============================================================
CREATE PROCEDURE [dbo].[DocumentBatchDetail_Escalate_2]
	@DocumentBatchDetailID INT,
	@ClientPersonnelID INT,
	@Reason VARCHAR(2000),
	@EscalatedByID INT,
	@Escalated BIT
AS
BEGIN
	IF (@DocumentBatchDetailID IS NOT NULL AND @ClientPersonnelID IS NOT NULL)
		BEGIN
			IF @Escalated=1
			BEGIN
				UPDATE DocumentBatchDetail
					SET LockedBy = @ClientPersonnelID, LockDate = dbo.fn_GetDate_Local()
				WHERE	
					@DocumentBatchDetailID = DocumentBatchDetailID
					
			END
			ELSE
			BEGIN
				UPDATE DocumentBatchDetail
					SET LockedBy = NULL, LockDate = NULL
				WHERE	
					@DocumentBatchDetailID = DocumentBatchDetailID													
			END							
			
			INSERT INTO DocumentBatchDetailEscalateReason (DocumentBatchDetailID, Reason, UserIDTo, EscalatedOn, Escalated, UserIDBy)
			VALUES (@DocumentBatchDetailID, @Reason, @ClientPersonnelID, dbo.fn_GetDate_Local(), @Escalated, @EscalatedByID)
			
		END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatchDetail_Escalate_2] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentBatchDetail_Escalate_2] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatchDetail_Escalate_2] TO [sp_executeall]
GO
