SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetQuestionnaireAnalysisCutdown] @ClientID int 

AS

SELECT  TOP 20   dbo.ClientQuestionnaires.ClientID, dbo.ClientQuestionnaires.ClientQuestionnaireID, dbo.ClientQuestionnaires.QuestionnaireTitle, 
                      COUNT(dbo.CustomerQuestionnaires.CustomerID) AS LeadCount
FROM         dbo.CustomerQuestionnaires INNER JOIN
                      dbo.ClientQuestionnaires ON dbo.CustomerQuestionnaires.ClientQuestionnaireID = dbo.ClientQuestionnaires.ClientQuestionnaireID
GROUP BY dbo.ClientQuestionnaires.ClientID, dbo.ClientQuestionnaires.ClientQuestionnaireID, dbo.ClientQuestionnaires.QuestionnaireTitle
HAVING      (dbo.ClientQuestionnaires.ClientID = @ClientID) 
ORDER BY ClientQuestionnaires.ClientQuestionnaireID DESC



GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionnaireAnalysisCutdown] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetQuestionnaireAnalysisCutdown] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionnaireAnalysisCutdown] TO [sp_executeall]
GO
