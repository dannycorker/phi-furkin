SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the WebServiceExclusionList table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WebServiceExclusionList_Insert]
(

	@WebServiceExclusionListID int    OUTPUT,

	@ClientPersonnelID int   
)
AS


				
				INSERT INTO [dbo].[WebServiceExclusionList]
					(
					[ClientPersonnelID]
					)
				VALUES
					(
					@ClientPersonnelID
					)
				-- Get the identity value
				SET @WebServiceExclusionListID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WebServiceExclusionList_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WebServiceExclusionList_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WebServiceExclusionList_Insert] TO [sp_executeall]
GO
