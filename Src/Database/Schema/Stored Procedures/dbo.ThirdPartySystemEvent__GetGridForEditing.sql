SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-09-23
-- Description:	ThirdPartySystemEvent__GetGridForEditing
-- =============================================
CREATE PROCEDURE [dbo].[ThirdPartySystemEvent__GetGridForEditing]
	@ClientID INT,
	@ClientPersonnelID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT tpe.ThirdPartySystemEventID, tps.SystemName, tpe.Name, tpe.Description, et.EventTypeName, et.EventTypeID, lt.LeadTypeName, tpe.ThirdPartySystemKey, tpe.MessageName
	FROM ThirdPartySystemEvent tpe WITH (NOLOCK)
	INNER JOIN ThirdPartySystem tps WITH (NOLOCK) ON tps.ThirdPartySystemId = tpe.ThirdPartySystemID
	LEFT JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = tpe.EventTypeID
	LEFT JOIN LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = tpe.LeadTypeID
	WHERE tpe.ClientID = @ClientID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent__GetGridForEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartySystemEvent__GetGridForEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartySystemEvent__GetGridForEditing] TO [sp_executeall]
GO
