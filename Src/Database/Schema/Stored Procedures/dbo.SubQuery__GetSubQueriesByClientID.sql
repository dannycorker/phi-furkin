SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-11-17
-- Description:	Get By ClientID OR Client 0 (for custom sub queries)
-- =============================================

CREATE PROCEDURE [dbo].[SubQuery__GetSubQueriesByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SubQueryID],
					[ClientID],
					[SubQueryName],
					[SubQueryDesc],
					[SubQueryText]
				FROM [dbo].[SubQuery] WITH (NOLOCK) 
				WHERE (ClientID = @ClientID OR ClientID = 0)
				ORDER BY [SubQueryName] /*CS 2011-08-15*/
				
				SELECT @@ROWCOUNT
GO
GRANT VIEW DEFINITION ON  [dbo].[SubQuery__GetSubQueriesByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubQuery__GetSubQueriesByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubQuery__GetSubQueriesByClientID] TO [sp_executeall]
GO
