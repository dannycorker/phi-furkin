SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 01/03/2016
-- Description:	Inserts a script rule
-- =============================================
CREATE PROCEDURE [dbo].[ScriptRule__Insert]
	
	@ScriptID INT,
	@ClientID INT
	
AS
BEGIN
	
	SET NOCOUNT ON;

	INSERT INTO ScriptRule(ScriptID, ClientID)
	VALUES (@ScriptID, @ClientID)
	
	SELECT SCOPE_IDENTITY() ScriptRuleID
    	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptRule__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptRule__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptRule__Insert] TO [sp_executeall]
GO
