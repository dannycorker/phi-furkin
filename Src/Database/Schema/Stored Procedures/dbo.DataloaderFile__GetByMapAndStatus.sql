SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2016-06-08
-- Description:	Get files to be imported
-- =============================================
CREATE PROCEDURE [dbo].[DataloaderFile__GetByMapAndStatus]
	@MapID INT,
	@Status INT
AS
BEGIN

	SET NOCOUNT ON;


	SELECT *
	FROM DataLoaderFile d WITH (NOLOCK)
	WHERE d.DataLoaderMapID = @MapID
	AND d.FileStatusID = @Status

END
GO
GRANT VIEW DEFINITION ON  [dbo].[DataloaderFile__GetByMapAndStatus] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataloaderFile__GetByMapAndStatus] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataloaderFile__GetByMapAndStatus] TO [sp_executeall]
GO
