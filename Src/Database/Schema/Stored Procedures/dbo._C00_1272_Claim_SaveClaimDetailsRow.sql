SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-03-05
-- Description:	Saves a single claim details row and the deductions as long as it has not been paid
-- Used by:		Popup called from DF 147723
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_SaveClaimDetailsRow]
(
	@MatterID INT,
	@TableRowID INT,
	@Claim VARCHAR(2000),
	@Deductions dbo.tvpIntVarcharVarchar READONLY,
	@Treatments dbo.tvpIntVarcharVarchar READONLY
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE  @CustomerID INT
			,@LeadID	 INT
			,@CaseID	 INT
			,@ClientID	 INT
			,@LogEntry	 VARCHAR(2000) = ''
	
	SELECT @LogEntry = ' @MatterID = ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL')
					 + ',@TableRowID = ' + ISNULL(CONVERT(VARCHAR,@TableRowID),'NULL')
					 + ',@Claim = ' + ISNULL(CONVERT(VARCHAR,@Claim),'NULL')

	EXEC _C00_LogIt 'Info', '_C00_1272_Claim_SaveClaimDetailsRow', 'Input Parameters', @LogEntry, 0
			
	SELECT	@CustomerID = CustomerID, 
			@LeadID = LeadID, 
			@CaseID = CaseID,
			@ClientID = ClientID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	IF dbo.fn_C00_1272_Claim_AllowedTo('edit', @CaseID) = 1
	BEGIN
	
		DECLARE @Fields TABLE
		(
			FieldID INT
		)
		INSERT @Fields(FieldID) VALUES 
		(144353),  
		(146179), 
		(145678),
		(147001)
		
		-- Sum the user deductions and save to the parent row
		DECLARE @UserDeductions MONEY, 
				@Settle MONEY
		SELECT @UserDeductions = SUM(CONVERT(MONEY, AnyValue1))
		FROM @Deductions
		
		SELECT @Settle = CAST(@Claim AS MONEY) + ISNULL(@UserDeductions, 0)
		
		IF @Settle < 0 BEGIN
			SELECT @Settle = 0		
		END
		
		-- Check to see if this is a no cover row so we can zero out the claim amount with a no cover deduction
		DECLARE @ClaimRowType INT
		SELECT @ClaimRowType = ValueInt
		FROM dbo.TableDetailValues WITH (NOLOCK) 
		WHERE TableRowID = @TableRowID
		AND DetailFieldID = 149778 /*Claim Row Type*/
		
		DECLARE @NoCover MONEY = NULL
		IF @ClaimRowType = 50539 -- No Cover
		BEGIN
			SELECT @NoCover = @Settle * -1, @Settle = 0
		END
	
		-- Update values that exists
		UPDATE tdv
		SET DetailValue =
			CASE 
				WHEN tdv.DetailFieldID = 144353 THEN @Claim
				WHEN tdv.DetailFieldID = 146179 THEN CAST(@UserDeductions AS VARCHAR)
				WHEN tdv.DetailFieldID = 145678 THEN CAST(@Settle AS VARCHAR)
				WHEN tdv.DetailFieldID = 147001 THEN CAST(@NoCover AS VARCHAR)
			END
		FROM dbo.TableDetailValues tdv
		WHERE tdv.ClientID = @ClientID
		AND tdv.DetailFieldID IN (SELECT * FROM @Fields)
		AND tdv.TableRowID = @TableRowID
				
		-- And insert those that don't
		INSERT dbo.TableDetailValues (ClientID, DetailFieldID, CustomerID, LeadID, CaseID, MatterID, TableRowID, DetailValue)
		SELECT @ClientID, f.FieldID, @CustomerID, @LeadID, @CaseID, @MatterID, @TableRowID, 
			CASE
				WHEN f.FieldID = 144353 THEN @Claim
				WHEN f.FieldID = 146179 THEN CAST(@UserDeductions AS VARCHAR)
				WHEN f.FieldID = 145678 THEN CAST(@Settle AS VARCHAR)
				WHEN f.FieldID = 147001 THEN CAST(@NoCover AS VARCHAR)
			END AS Value	
		FROM @Fields f 
		WHERE NOT EXISTS (
			SELECT *
			FROM dbo.TableDetailValues tdv
			WHERE tdv.TableRowID = @TableRowID AND tdv.MatterID = @MatterID AND tdv.DetailFieldID = f.FieldID
		)
		
		-- Now deductions...
		
		-- Delete existing deduction rows
		DELETE r
		FROM dbo.TableRows r
		INNER JOIN dbo.TableDetailValues tdvTableRowID WITH (NOLOCK) ON r.TableRowID = tdvTableRowID.TableRowID AND tdvTableRowID.DetailFieldID = 147299
		WHERE r.MatterID = @MatterID
		AND r.DetailFieldID = 147302
		AND r.DetailFieldPageID = 16157
		AND r.ClientID = @ClientID
		AND tdvTableRowID.ValueInt = @TableRowID
		
		DECLARE @ToInsert TABLE
		(
			ID INT IDENTITY,
			ReasonID INT,
			Amount VARCHAR(2000),
			ItemNotCovered VARCHAR(2000)
		)
		INSERT @ToInsert (ReasonID, Amount, ItemNotCovered)
		SELECT AnyID, AnyValue1, AnyValue2
		FROM @Deductions
		
		DECLARE @Inserted TABLE
		(
			ID INT,
			TableRowID INT
		)
		
		INSERT TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID, SourceID)
		OUTPUT inserted.SourceID, inserted.TableRowID INTO @Inserted
		SELECT @ClientID, @MatterID, 147302, 16157, ID /*Claim Detail Deductions*/
		FROM @ToInsert
		
		DELETE @Fields
		INSERT @Fields(FieldID) VALUES
		(147299), 
		(147300), 
		(147301),
		(148432)

		-- Insert the new ones
		INSERT dbo.TableDetailValues (ClientID, DetailFieldID, CustomerID, LeadID, CaseID, MatterID, TableRowID, DetailValue)
		SELECT @ClientID, f.FieldID, @CustomerID, @LeadID, @CaseID, @MatterID, r.TableRowID, 
			CASE
				WHEN f.FieldID = 147299 THEN CAST(@TableRowID AS VARCHAR)
				WHEN f.FieldID = 147300 THEN CAST(v.ReasonID AS VARCHAR)
				WHEN f.FieldID = 147301 THEN CAST(v.Amount AS VARCHAR)
				WHEN f.FieldID = 148432 THEN CAST(v.ItemNotCovered AS VARCHAR)
			END AS Value	
		FROM @ToInsert v
		INNER JOIN @Inserted r ON v.ID = r.ID 
		CROSS JOIN @Fields f 
		
		
		-- Now treatments...
		
		-- Delete existing treatment rows
		DELETE r
		FROM dbo.TableRows r
		INNER JOIN dbo.TableDetailValues tdvTableRowID WITH (NOLOCK) ON r.TableRowID = tdvTableRowID.TableRowID AND tdvTableRowID.DetailFieldID = 162700
		WHERE r.MatterID = @MatterID
		AND r.DetailFieldID = 162699
		AND r.DetailFieldPageID = 16188
		AND r.ClientID = @ClientID
		AND tdvTableRowID.ValueInt = @TableRowID
		
		DECLARE @TreatmentInsert TABLE
		(
			ID INT IDENTITY,
			TreatmentID INT,
			Quatity VARCHAR(2000),
			TotalCost VARCHAR(2000)
		)
		INSERT @TreatmentInsert (TreatmentID, Quatity, TotalCost)
		SELECT AnyID, AnyValue1, AnyValue2
		FROM @Treatments
		
		DECLARE @TreatmentInserted TABLE
		(
			ID INT,
			TableRowID INT
		)
		
		INSERT TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID, SourceID)
		OUTPUT inserted.SourceID, inserted.TableRowID INTO @TreatmentInserted
		SELECT @ClientID, @MatterID, 162699, 16188, ID /*Treatment costs*/
		FROM @TreatmentInsert
		
		DELETE @Fields
		INSERT @Fields(FieldID) VALUES
		(162700), 
		(162625), 
		(162626),
		(162627)

		-- Insert the new ones
		INSERT dbo.TableDetailValues (ClientID, DetailFieldID, CustomerID, LeadID, CaseID, MatterID, TableRowID, DetailValue)
		SELECT @ClientID, f.FieldID, @CustomerID, @LeadID, @CaseID, @MatterID, r.TableRowID, 
			CASE
				WHEN f.FieldID = 162700 THEN CAST(@TableRowID AS VARCHAR)	/*ClaimRowID*/
				WHEN f.FieldID = 162625 THEN CAST(v.TreatmentID AS VARCHAR)	/*Treatment item*/
				WHEN f.FieldID = 162626 THEN CAST(v.Quatity AS VARCHAR)		/*Treatment quantity*/
				WHEN f.FieldID = 162627 THEN CAST(v.TotalCost AS VARCHAR)	/*Treatment total cost*/
			END AS Value	
		FROM @TreatmentInsert v
		INNER JOIN @TreatmentInserted r ON v.ID = r.ID 
		CROSS JOIN @Fields f 
		
		
		-- Get the claimed and approved totals into a MDF for the matter list
		EXEC dbo._C00_1272_Claim_SumClaimedAndApproved @MatterID, @LeadID
	
	END
	

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_SaveClaimDetailsRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_SaveClaimDetailsRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_SaveClaimDetailsRow] TO [sp_executeall]
GO
