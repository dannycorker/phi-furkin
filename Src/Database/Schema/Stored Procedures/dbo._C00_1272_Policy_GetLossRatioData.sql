SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-05-22
-- Description:	Gets the loss ratio data for a single policy
-- Used by:		DF 145688
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Policy_GetLossRatioData] 
(
	@LeadID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID
	
	DECLARE @TotalPremium MONEY,
			@TotalClaimed MONEY
			
	DECLARE @MatterID INT
	SELECT TOP 1 @MatterID = MatterID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE LeadID = @LeadID
			
	SELECT @TotalPremium = TotalPremiumPaid
	FROM dbo.fn_C00_1272_Policy_GetPolicyDetails(@MatterID)
	
	SELECT @TotalClaimed = SUM(tdvTotal.ValueMoney)
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvTotal WITH (NOLOCK) ON r.TableRowID = tdvTotal.TableRowID AND tdvTotal.DetailFieldID = 144352
	INNER JOIN dbo.TableDetailValues tdvPaid WITH (NOLOCK) ON r.TableRowID = tdvPaid.TableRowID AND tdvPaid.DetailFieldID = 144362
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON r.MatterID = m.MatterID 
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON m.LeadID = l.LeadID
	INNER JOIN dbo.Cases c WITH (NOLOCK) ON m.CaseID = c.CaseID
	WHERE r.DetailFieldID = 144355
	AND r.DetailFieldPageID = 16157
	AND r.ClientID = @ClientID
	AND l.LeadID = @LeadID
	AND (c.ClientStatusID IS NULL OR c.ClientStatusID NOT IN (3819)) -- Exclude rejected claims
	AND tdvTotal.ValueMoney > 0
	AND tdvPaid.ValueDate IS NOT NULL
			
	SELECT @TotalPremium AS TotalPremium, @TotalClaimed AS TotalClaimed

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetLossRatioData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Policy_GetLossRatioData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetLossRatioData] TO [sp_executeall]
GO
