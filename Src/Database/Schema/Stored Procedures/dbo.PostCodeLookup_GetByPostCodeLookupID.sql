SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PostCodeLookup table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PostCodeLookup_GetByPostCodeLookupID]
(

	@PostCodeLookupID int   
)
AS


				SELECT
					[PostCodeLookupID],
					[PostCode]
				FROM
					[dbo].[PostCodeLookup] WITH (NOLOCK) 
				WHERE
										[PostCodeLookupID] = @PostCodeLookupID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PostCodeLookup_GetByPostCodeLookupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PostCodeLookup_GetByPostCodeLookupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PostCodeLookup_GetByPostCodeLookupID] TO [sp_executeall]
GO
