SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentType__GetByClientIDAndLeadTypeIDOrNull]
(

	@ClientID int,
	@LeadTypeID int,
	@ShowBlobs bit = 1      
)
AS

				SELECT	DocumentTypeID, ClientID, LeadTypeID, DocumentTypeName, DocumentTypeDescription, Header, CASE @ShowBlobs WHEN 1 THEN Template ELSE NULL END as [Template], Footer, CanBeAutoSent, EmailSubject, CASE @ShowBlobs WHEN 1 THEN EmailBodyText ELSE NULL END as [EmailBodyText], InputFormat, OutputFormat, Enabled, RecipientsTo, RecipientsCC, RecipientsBCC, ReadOnlyTo, ReadOnlyCC, ReadOnlyBCC, SendToMultipleRecipients, MultipleRecipientDataSourceType, MultipleRecipientDataSourceID, SendToAllByDefault, ExcelTemplatePath, FromDetails, ReadOnlyFrom,
						SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified, FolderID,IsThunderheadTemplate,ThunderheadUniqueTemplateID,ThunderheadDocumentFormat, DocumentTitleTemplate, CreateNewVersionWhenSaved
				FROM
					[dbo].[DocumentType]
				WHERE
					[ClientID] = @ClientID AND
					([LeadTypeID] = @LeadTypeID OR [LeadTypeID] is NULL)
					
			Select @@ROWCOUNT

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetByClientIDAndLeadTypeIDOrNull] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType__GetByClientIDAndLeadTypeIDOrNull] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetByClientIDAndLeadTypeIDOrNull] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[DocumentType__GetByClientIDAndLeadTypeIDOrNull] TO [sp_executehelper]
GO
