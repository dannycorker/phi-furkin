SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SearchableObjectType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchableObjectType_Update]
(

	@SearchableObjectTypeID int   ,

	@OriginalSearchableObjectTypeID int   ,

	@SearchableObject varchar (250)  ,

	@PKColumnName varchar (250)  ,

	@JoinTable varchar (250)  ,

	@JoinColumn varchar (250)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SearchableObjectType]
				SET
					[SearchableObjectTypeID] = @SearchableObjectTypeID
					,[SearchableObject] = @SearchableObject
					,[PKColumnName] = @PKColumnName
					,[JoinTable] = @JoinTable
					,[JoinColumn] = @JoinColumn
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
				WHERE
[SearchableObjectTypeID] = @OriginalSearchableObjectTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchableObjectType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchableObjectType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchableObjectType_Update] TO [sp_executeall]
GO
