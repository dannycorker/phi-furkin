SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-06-23
-- Description:	Inserts or updates a mapping
-- =============================================
CREATE PROCEDURE [dbo].[KeyIdentifierMappings__SaveMapping]
(
	@KeyVal VARCHAR(50), 
	@ClientID INT,
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@LeadTypeID INT
)


AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS(	SELECT *
				FROM dbo.KeyIdentifierMappings WITH (NOLOCK) 
				WHERE KeyVal = @KeyVal
				AND ClientID = @ClientID )
	BEGIN
	
		UPDATE dbo.KeyIdentifierMappings
		SET CustomerID = @CustomerID,
			LeadID = @LeadID,
			CaseID = @CaseID,
			MatterID = @MatterID,
			LeadTypeID = @LeadTypeID
		WHERE KeyVal = @KeyVal
		AND ClientID = @ClientID
	
	END
	ELSE
	BEGIN
	
		INSERT INTO dbo.KeyIdentifierMappings (KeyVal, ClientID, CustomerID, LeadID, CaseID, MatterID, LeadTypeID)
		VALUES (@KeyVal, @ClientID, @CustomerID, @LeadID, @CaseID, @MatterID, @LeadTypeID)
	
	END
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[KeyIdentifierMappings__SaveMapping] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[KeyIdentifierMappings__SaveMapping] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[KeyIdentifierMappings__SaveMapping] TO [sp_executeall]
GO
