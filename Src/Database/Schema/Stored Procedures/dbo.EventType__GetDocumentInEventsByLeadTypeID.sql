SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------
-- Date Created: 01-04-2008

-- Created By:  Ben
-- Purpose:		Get EventTypeName and EventTypeID for binding to a DropDownList.
-- SB  2014-07-10 Updated to use view which includes shared event types
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[EventType__GetDocumentInEventsByLeadTypeID] 
(
	@LeadTypeID int,
	@ClientID INT = NULL
)
AS
BEGIN
	SET ANSI_NULLS OFF

	SELECT
		[EventTypeID],
		[EventTypeName]
	FROM
		dbo.fnEventTypeShared(@ClientID) et
	WHERE
		LeadTypeID = @LeadTypeID
		AND EventSubTypeID in (3,5) 
	ORDER BY EventTypeName

	SET ANSI_NULLS ON
END






GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetDocumentInEventsByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetDocumentInEventsByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetDocumentInEventsByLeadTypeID] TO [sp_executeall]
GO
