SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-10-06
-- Description:	Proc to Run SAS for Client 600
--	2015-11-12 DCM Turn off tablerow editting
--	2016-01-26 DCM Add ReportFilename to report batch job creation
-- 2017-11-23 Updated to Client 600 - SA
-- 2018-06-04 GPR added SAS for Rules Engine Mandatory Checkpoints
-- 2018-07-20 GPR brought tracking code SAS over from C433 for CR0011: @TrackingCode, @WebPageURL, @TrackingURL
-- 2018-07-25 GPR update to use ?cid= in place of ?token= for C600 EQB Tracking URL
-- 2018-08-20 AHOD Update to remove ?cid=
-- 2018-10-17 GPR added SAS for Claim Benefit Reserve Table and Reserve History Table
-- 2018-12-01 GPR updated SAS for Claim Benefit Reserve Table and Reserve History Table (inc errors and clearing out)
-- 2019-06-25 CR updated the Report selection for the two new report (Buddies Dialer OMF Report and Cancellation Report)
-- 2019-09-05 GPR updated the Report selection for CR84 / BAULAG-167,BAULAG-178 - now includes 'ASDA Marketing Permissions' report
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- 2021-03-15 NG for FURKIN-419 amend run report now in iAdmin functionality
-- 2021-04-22 AAD for FURKIN-531 Additions to iAdmin report running
-- =============================================
CREATE PROCEDURE [dbo].[_C600_SAS]
 	 @ClientPersonnelID INT
	,@CustomerID INT
	,@LeadID INT
	,@CaseID INT = NULL
	,@MatterID INT = NULL
	,@TableRowID INT = NULL
	,@ResourceListID INT = NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE
		@AqAutomation			INT
		,@AccountID				INT 
		,@TableRowCount			INT	
		,@BATCopyLine			VARCHAR(500)
		,@BATName				VARCHAR(100)
		,@ClientID				INT = dbo.fnGetPrimaryClientID()
		,@ControlID				INT
		,@FolderToUse			VARCHAR(100)
		,@LeadTypeID			INT
		,@PolicyNumber			VARCHAR (2000)
		,@LinkID				INT
		,@ObjectName			VARCHAR(500)
		,@ObjectID				INT
		,@ObjectTypeID			INT
		,@ReportFileExt			VARCHAR(10)
		,@ReportFilename		VARCHAR(100)
		,@TableDFID				INT 
		,@Type					VARCHAR(50)
		,@VarcharDate			VARCHAR (10)
		,@VarcharDateTime		VARCHAR (19)
		,@LogEntry				VARCHAR (1000)
		,@VetName				VARCHAR (2000)
		,@BankName				VARCHAR (2000)
		,@AccountName			VARCHAR (2000)
		,@AccountSortCode		VARCHAR (2000)
		,@AccountNumber			VARCHAR (2000)
		,@NoteDate				DATE
		,@NoteCount				INT
		,@UnderFraudCheck		INT
		,@ValueMoney			DECIMAL(18,2)
		,@SanctionScreeening	VARCHAR (2000)
		,@Channel				VARCHAR (2000) 
		,@ValidFrom				VARCHAR (2000)
		,@ValidTo				VARCHAR (2000)
		,@UserName				Varchar (2000) 
		,@WhenChanged			DATE = dbo.fn_GetDate_Local()
		,@RemoveRow				VARCHAR(2000)
		,@Notes					VARCHAR (2000)
		,@Name					VARCHAR (2000)
		,@ValueINT				INT
		,@ErrorMessage			VARCHAR(2000)
		,@WhoCreated			INT = 58552
		,@TrackingCode			VARCHAR(2000)
		,@WebPageURL			VARCHAR(2000)
		,@TrackingURL			VARCHAR(2000)
		,@ReportID				INT
		,@From					DATE
		,@To					DATE
		,@RunReport				VARCHAR(10)
		,@ValueDate				DATE
		,@ReserveType			VARCHAR(50)
		,@PaidAmount			MONEY
		,@OutstandingAmount		MONEY
		,@ValueDate2			DATE
		,@AffinityLuli			INT
		,@Section				INT
		,@SubSection			INT


	DECLARE @SASType Int = 0
			-- 1 = Lead
			-- 2 = Matter
			-- 6 = Table
			-- 10 = Customer
			-- 11 = Case

	/*Use this method because then we can use @LeadID, @MatterID etc through the proc*/
	SELECT @SASType = CASE	WHEN ISNULL(@TableRowID,0) <> 0 THEN 6 
							WHEN ISNULL(@MatterID,0) <> 0 THEN 2
							WHEN ISNULL(@CaseID,0) <> 0 THEN 11
							WHEN ISNULL(@LeadID,0) <> 0 THEN 1
							WHEN ISNULL(@CustomerID,0) <> 0 THEN 10
							WHEN ISNULL(@ResourceListID,0) <> 0		THEN 4
							ELSE 12 
							END 
	
	SELECT @AqAutomation=dbo.fnGetKeyValueAsIntFromThirdPartyIDs (@ClientID,53,'CFG|AqAutomationCPID',0)					

	/* Get LeadTypeID for use Later On */
	SELECT @LeadTypeID = l.LeadTypeID
		 , @CaseID = c.CaseID
	FROM Lead l WITH (NOLOCK)
	INNER JOIN Cases c WITH (NOLOCK) ON c.LeadID = l.LeadID
	WHERE l.LeadID = @LeadID

	IF @SASType = 1 /* Lead */
	BEGIN
	
		IF @LeadTypeID IN (1492,1490) -- Policy Admin
		BEGIN
			
			DECLARE @PolicyAdminLeadID INT, @PolicyAdminCaseID INT, @ClaimLeadID INT, @ClaimCaseID INT, @DateOfLoss VARCHAR(10) = ''
			
			IF @LeadTypeID=1492  -- policy admin leadID
			BEGIN
			
				SELECT TOP 1 @PolicyAdminLeadID=@LeadID, @PolicyAdminCaseID=ca.CaseID  
				FROM Cases ca WITH (NOLOCK) WHERE ca.LeadID=@LeadID
				SELECT TOP 1 @ClaimLeadID=ltr.ToLeadID, @ClaimCaseID=ca.CaseID 
				FROM LeadTypeRelationship ltr WITH (NOLOCK)
				INNER JOIN Cases ca WITH (NOLOCK) ON ltr.ToLeadID=ca.LeadID 
				WHERE ltr.FromLeadID=@LeadID AND ltr.ToLeadTypeID=1490
			
			END
			ELSE -- claim LeadID
			BEGIN
			
				SELECT @ClaimLeadID=@LeadID
				SELECT TOP 1 @PolicyAdminLeadID=ltr.FromLeadID, @ClaimCaseID=ca.CaseID 
				FROM LeadTypeRelationship ltr WITH (NOLOCK)
				INNER JOIN Cases ca WITH (NOLOCK) ON ltr.ToLeadID=ca.LeadID 
				WHERE ltr.ToLeadID=@LeadID AND ltr.FromLeadTypeID=1492
				SELECT TOP 1 @PolicyAdminCaseID=ca.CaseID  
				FROM Cases ca WITH (NOLOCK) WHERE ca.LeadID=@PolicyAdminLeadID
				
			
			END  
			
			IF @ClaimLeadID IS NOT NULL
			BEGIN
				
				EXEC dbo._C00_1272_Policy_SetDeadPetText @ClaimLeadID, @ClaimCaseID, @ClientPersonnelID
				
				-- set short dead pet text
				DECLARE @DeadPetMatter tvpIntInt
				
				SELECT @DateOfLoss=dbo.fnGetDv (144271,@PolicyAdminCaseID)
				
				INSERT INTO @DeadPetMatter
				SELECT m.MatterID,0
				FROM MatterDetailValues dol WITH (NOLOCK) /* SLACKY-PERF-20190705 */
				INNER JOIN Matter m WITH (NOLOCK) ON dol.MatterID=m.MatterID AND m.LeadID=@ClaimLeadID
				WHERE dol.DetailFieldID=170263
				
				WHILE EXISTS ( SELECT * FROM @DeadPetMatter WHERE ID2=0 )
				BEGIN
				
					SELECT @MatterID=ID1 FROM @DeadPetMatter WHERE ID2=0
					IF @DateOfLoss<>''
						EXEC _C600_UpdateLetterTextField 170263, 'DEADPETSHORT', @MatterID
					ELSE
						EXEC _C00_SimpleValueIntoField 170263, '', @MatterID, @AqAutomation
					UPDATE @DeadPetMatter SET ID2=1 WHERE ID1=@MatterID
				
				END
			
			END
			
			EXEC dbo._C00_1272_Policy_SetVetHistory @PolicyAdminLeadID
	
			-- set the pet type for page list display
			SELECT @Type=breedli.ItemValue FROM LeadDetailValues breed WITH (NOLOCK) /* SLACKY-PERF-20190705 */
			INNER JOIN ResourceListDetailValues breedrl WITH (NOLOCK) ON breed.ValueInt=breedrl.ResourceListID and breedrl.DetailFieldID=144269
			INNER JOIN LookupListItems breedli WITH (NOLOCK) ON breedrl.ValueInt=breedli.LookupListItemID
			WHERE breed.DetailFieldID=144272 AND breed.LeadID=@PolicyAdminLeadID
			EXEC dbo._C00_SimpleValueIntoField 175273, @Type, @PolicyAdminLeadID, @ClientPersonnelID
			
		END 

	END

	IF @SASType = 2 /* Matter */
	BEGIN
		SELECT @LeadTypeID = l.LeadTypeID
		FROM Matter m WITH ( NOLOCK ) 
		INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = m.LeadID 
		WHERE m.MatterID = @MatterID
	
		IF @LeadTypeID = 1491 /*Schemes*/
		BEGIN
			SELECT @CaseID = m.CaseID
			FROM Matter m WITH ( NOLOCK ) 
			WHERE m.MatterID = @MatterID
		
			/*Update Ref Letters for matters in this case to sort by MatterRef and StartDate*/
			;WITH NumberedMatters AS
			(
			SELECT m.MatterID, dbo.fnRefLetterFromCaseNum(ROW_NUMBER() OVER (PARTITION BY m.CaseID ORDER BY m.MatterRef, mdvSpec.ValueInt, mdv.ValueDate)) [RefLetter]
			FROM Matter m WITH ( NOLOCK ) 
			LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID AND mdv.DetailFieldID = 145690 /*Scheme Start*/
			LEFT JOIN MatterDetailValues mdvSpec WITH ( NOLOCK ) on mdvSpec.MatterID = m.MatterID AND mdvSpec.DetailFieldID = 177283 /*Scheme Species*/
			WHERE m.CaseID = @CaseID
			)
			UPDATE m
			SET RefLetter = n.RefLetter
			FROM NumberedMatters n 
			INNER JOIN Matter m WITH (NOLOCK) on m.MatterID = n.MatterID
		END
		ELSE
		IF @LeadTypeID = 1492 /*Policy Admin*/
		BEGIN
		
			SELECT @CaseID = CaseID
			FROM Matter WITH (NOLOCK)
			WHERE MatterID = @MatterID
			
			EXEC dbo._C00_1272_Claim_KeepRelatedClaimsInSync @MatterID
			
			-- Update ClaimedAndApproved totals for Claim
			EXEC dbo._C00_1272_Claim_SumClaimedAndApproved @MatterID, @LeadID		
			
			
			EXEC [dbo].[_C00_SimpleValueIntoField] 177902, '5145', @MatterID /*GPR 2018-07-12 Vet Check Required, No*/
		
		END
	
	END	
	
	IF @SASType = 6 /* Table */
	BEGIN
	
		SELECT @TableDFID=DetailFieldID FROM TableRows WITH (NOLOCK) WHERE TableRowID=@TableRowID
		
		
		/*GPR 2018-10-17 PM62, C600*/
		IF @TableDFID = 180191 /*Claim Benefit Table*/
		BEGIN
		
			/*GPR 2018-12-01 Modified for Defect #1196*/
			DECLARE @Limit MONEY
				SELECT @Limit = ValueMoney
				FROM ClientPersonnelDetailValues WITH (NOLOCK) 
				WHERE DetailFieldID= 180202 /*Approval limit*/
				AND ClientPersonnelID = @ClientPersonnelID
		
			SELECT @ValueDate = dbo.fnGetSimpleDvAsDate(180194,@TableRowID)/*Date*/
			SELECT @ReserveType = dbo.fnGetSimpleDvLuli(180196,@TableRowID)/*ReserveType*/
			SELECT @ValueMoney = dbo.fnGetSimpleDvAsMoney (180197,@TableRowID)/*ReserveFigure*/
			SELECT @PaidAmount = dbo.fnGetSimpleDvAsMoney (180198,@TableRowID)/*PaidAmount*/
			SELECT @ValueDate2 = dbo.fnGetSimpleDvAsDate(180200,@TableRowID)/*WhenModified*/
			SELECT @UserName = dbo.fnGetSimpleDv(180201,@TableRowID)/*WhoModified*/
			
			/*GPR 2018-12-01 Modified for Defect #1196*/
			IF (@PaidAmount > @Limit)
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 180198,'<font color="red">ERROR</font>',@TableRowID /*IF not approved, blank out value*/
				EXEC dbo._C00_SimpleValueIntoField 180197,'<font color="red">ERROR</font>',@TableRowID /*IF not approved, blank out value*/
			END
			
			IF (@ValueMoney > @Limit)
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 180198,'<font color="red">ERROR</font>',@TableRowID /*IF not approved, blank out value*/
				EXEC dbo._C00_SimpleValueIntoField 180197,'<font color="red">ERROR</font>',@TableRowID /*IF not approved, blank out value*/
			END
		
			SELECT @UserName = cp.UserName
			FROM ClientPersonnel cp WITH ( NOLOCK ) 
			WHERE cp.ClientPersonnelID = @ClientPersonnelID
		
			SELECT @WhenChanged = dbo.fn_GetDate_Local()
			
			/*Now work out the Outstanding Amount*/
			SELECT @OutstandingAmount = @ValueMoney - @PaidAmount
			
			EXEC dbo._C00_SimpleValueIntoField 180199, @OutstandingAmount, @TableRowID, @ClientPersonnelID /*Outstanding Amount*/
			EXEC dbo._C00_SimpleValueIntoField 180200, @WhenChanged, @TableRowID, @ClientPersonnelID
			EXEC dbo._C00_SimpleValueIntoField 180201, @UserName, @TableRowID, @ClientPersonnelID
						
			SELECT @OutstandingAmount = dbo.fnGetSimpleDvAsMoney (180199,@TableRowID)/*OutstandingAmount*/
			
			INSERT TableRows ( ClientID, MatterID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit )
			VALUES (@ClientID, @MatterID, 180193, 19211, 1, 1 ) /*Claim Benefit Table History*/
		
			SELECT @TableRowID = SCOPE_IDENTITY()
			
			EXEC dbo._C00_SimpleValueIntoField 180203,@ValueDate,@TableRowID
			EXEC dbo._C00_SimpleValueIntoFieldLuli 180204,@ReserveType,@TableRowID
			EXEC dbo._C00_SimpleValueIntoField 180205,@ValueMoney,@TableRowID
			EXEC dbo._C00_SimpleValueIntoField 180206,@PaidAmount,@TableRowID
			EXEC dbo._C00_SimpleValueIntoField 180207,@OutstandingAmount,@TableRowID
			EXEC dbo._C00_SimpleValueIntoField 180208,@ValueDate2,@TableRowID
			EXEC dbo._C00_SimpleValueIntoField 180209,@UserName,@TableRowID	
			
			/*GPR 2018-12-01 Modified for Defect #1196*/
			IF (@PaidAmount > @Limit)
			BEGIN
				EXEC dbo.TableRows_Delete @TableRowID /*delete the history row*/
			END
			IF (@ValueMoney > @Limit)
			BEGIN
				EXEC dbo.TableRows_Delete @TableRowID /*delete the history row*/
			END
			
			
			
			
		END
		
		
		/*2018-06-04 GPR moved from 433 to 600*/
		IF @TableDFID = 180043 /*Mandatory Checkpoints*/ /*CPS 2017-10-31 for TON130*/
		BEGIN
			/*Validate and save a new mandatory checkpoints row.*/
			SELECT	 @ObjectID	= dbo.fnGetSimpleDvAsInt(180038,@TableRowID) /*RuleSetID*/
					,@Name		= dbo.fnGetSimpleDv(180037,@TableRowID) /*Checkpoint Name*/
			
			SELECT @ObjectName = rs.Name
			FROM RulesEngine_RuleSets rs WITH ( NOLOCK )
			WHERE rs.RuleSetID = @ObjectID
			AND rs.RuleSetTypeID = 3 /*Premium Calculation Rule*/

			IF @ObjectName is NULL	/*Make sure the rule set exists*/
			BEGIN
				SELECT @ObjectName = 'Error:  <font color="red">No RuleSet found for ID ' + ISNULL(CONVERT(VARCHAR,@ObjectID),'NULL') + '</font>'
			END
			ELSE					/*Make sure the checkpoint exists*/
			BEGIN
				SELECT   @ObjectName = 'Error:  <font color="red">No matching checkpoint found for "' + ISNULL(@Name,'NULL') + '" within RuleSetID ' + ISNULL(CONVERT(VARCHAR,@ObjectID),'NULL') + '</font>'
				WHERE not exists ( SELECT * 
				                   FROM RulesEngine_Rules r WITH ( NOLOCK )
				                   WHERE r.RuleSetID = @ObjectID
				                   AND r.RuleCheckpoint = @Name )
			END
			
			EXEC dbo._C00_SimpleValueIntoField 180039, @ObjectName, @TableRowID, @ClientPersonnelID	/*RuleSet Name*/
			SELECT @ObjectName = NULL
			
			/*Pick up the current username and time details*/
			SELECT	 @ObjectName		= cp.UserName
					,@VarcharDateTime	= CONVERT(VARCHAR,CURRENT_TIMESTAMP,121)
			FROM ClientPersonnel cp WITH ( NOLOCK )
			WHERE cp.ClientPersonnelID = @ClientPersonnelID
			
			EXEC dbo._C00_SimpleValueIntoField 180041, @ObjectName		, @TableRowID, @ClientPersonnelID /*WhoAdded*/
			EXEC dbo._C00_SimpleValueIntoField 180040, @VarcharDateTime , @TableRowID, @ClientPersonnelID /*WhenAdded*/
			
			/*Complete the table row*/
			EXEC _C00_CompleteTableRow @TableRowID, NULL, 0, 0
		END
		
		/*SA - 2017-11-23 - Channel Audit History*/
		IF @TableDFID = 179690 /*Current Channels*/
		BEGIN
			SELECT @Channel = dbo.fnGetSimpleDvLuli				(179693,@TableRowID)
			SELECT	@ValidFrom = dbo.fnGetSimpleDv					(179694,@TableRowID)
			SELECT 	@ValidTo = dbo.fnGetSimpleDv						(179695,@TableRowID)
				
			SELECT @MatterID = tr.MatterID
			FROM TableRows tr WITH (NOLOCK)
			WHERE tr.TableRowID = @TableRowID

			SELECT	@UserName = cp.UserName
			FROM		ClientPersonnel cp WITH ( NOLOCK ) 
			WHERE	cp.ClientPersonnelID = @ClientPersonnelID

			UPDATE TableRows SET DenyDelete=1 WHERE TableRowID=@TableRowID -- we are barring update for the time being so ignore the update section below

			SELECT @RemoveRow = dbo.fnGetSimpleDv (179702, @TableRowID)

			IF @RemoveRow = 'true'
			BEGIN	
				SELECT @Notes = 'Row Deleted'
				EXEC dbo.TableRows_Delete @TableRowID
			END
			ELSE
			BEGIN
				SELECT @Notes = 'Row Updated'
			END

			INSERT TableRows ( ClientID, MatterID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit )
			VALUES (@ClientID, @MatterID, 179689, 19171, 1, 1 ) /*Channel audit history*/
		
			SELECT @TableRowID = SCOPE_IDENTITY()
		
			/*Post Values into table*/
			EXEC dbo._C00_SimpleValueIntoFieldLuLi	179683,	@Channel,@TableRowID, @ClientPersonnelID,''									/*Channel*/
			EXEC dbo._C00_SimpleValueIntoField			179684,	@ValidFrom,@TableRowID, @ClientPersonnelID									/*Dates from*/
			EXEC dbo._C00_SimpleValueIntoField			179685,	@ValidTo,@TableRowID, @ClientPersonnelID										/*Dates to*/
			EXEC dbo._C00_SimpleValueIntoField			179686,	@WhenChanged,@TableRowID, @ClientPersonnelID							/*When changed*/
			EXEC dbo._C00_SimpleValueIntoField			179687,	@UserName,@TableRowID, @ClientPersonnelID								/*Who changed*/
			EXEC dbo._C00_SimpleValueIntoField			179688,	@ClientPersonnelID,@TableRowID, @ClientPersonnelID					/*Client Personnel ID*/
			EXEC dbo._C00_SimpleValueIntoField			179703,	@Notes,@TableRowID, @ClientPersonnelID										/*Notes*/
			--SELECT @LogEntry = 'SAS being called for _C600_ - test__'+@Channel+@ValidFrom+@ValidTo
		
			--EXEC _C00_LogIt 'Debug', '_C600_LOG', 'C600_SAS', @LogEntry, @ClientPersonnelID
				
			--SET @LogEntry = ''				
			
		END
		
		IF @TableDFID = 144355 /*Claim Details Data*/
		BEGIN
			SELECT @ObjectName = cp.UserName
			FROM ClientPersonnel cp WITH ( NOLOCK ) 
			WHERE cp.ClientPersonnelID = @ClientPersonnelID
			
			SELECT @ValueMoney = SUM(tdv.ValueMoney)
			FROM TableDetailValues tdv WITH ( NOLOCK ) 
			WHERE tdv.TableRowID = @TableRowID
			AND tdv.DetailFieldID IN (	 144353 /*Claim*/
										,146179 /*User Deductions*/
										,147001 /*No Cover*/
										,146406 /*Excess*/
										,146407 /*Co Ins*/
										,146408 /*Limit*/
									 )
										
			
			SELECT @VarcharDateTime = CONVERT(VARCHAR,CURRENT_TIMESTAMP,121)
			
			EXEC dbo._C00_SimpleValueIntoField 178444, @ObjectName		, @TableRowID, @ClientPersonnelID /*User Name*/
			EXEC dbo._C00_SimpleValueIntoField 178445, @VarcharDateTime	, @TableRowID, @ClientPersonnelID /*When Changed*/
			EXEC dbo._C00_SimpleValueIntoField 144352, @ValueMoney		, @TableRowID, @ClientPersonnelID /*Total*/
		END
		IF @TableDFID = 177552 /*Queued Event Message Config*/
		BEGIN
			SELECT @ObjectName = '<font color="red">Error:  No EventType Matched</font>'
			SELECT @ObjectName = et.EventTypeName
			FROM EventType et WITH ( NOLOCK )
			WHERE et.EventTypeID = dbo.fnGetSimpleDvAsInt(177548,@TableRowID) /*EventTypeID*/
			
			EXEC dbo._C00_SimpleValueIntoField 177549, @ObjectName, @TableRowID, @ClientPersonnelID /*EventTypeName*/
		END
		ELSE
		IF @TableDFID IN (
						  177558 /*Queued Event Message Query Config*/
						 ,177566 /*Upcoming Milestone Query Config*/
						 )
		BEGIN
			SELECT @ObjectName = '<font color="red">Error:  No Query Matched</font>'
			SELECT @ObjectName = CASE 
									WHEN sq.QueryText like '%ORDER BY%'		THEN '<font color="red">Error:  ORDER BY is not valid in queries mapped here</font>'
									WHEN sq.QueryText not like '%@LeadID%'	THEN '<font color="red">Error:  Query does not refer to @LeadID</font>'
									WHEN sq.QueryText like '%EXEC %'		THEN '<font color="red">Error:  Query includes an EXEC</font>'
									ELSE sq.QueryTitle 
								 END 
			FROM SqlQuery sq WITH ( NOLOCK )
			WHERE sq.QueryID = dbo.fnGetSimpleDvAsInt(177554,@TableRowID) /*SqlQueryID*/
			
			EXEC dbo._C00_SimpleValueIntoField 177555, @ObjectName, @TableRowID, @ClientPersonnelID /*QueryTitle*/
		END		
		
		/*GPR 2019-03-11*/
		IF @TableDFID = 180240  /*External Quote and Buy Control*/
		BEGIN
		
		SELECT @AffinityLuli = dbo.fnGetSimpleDvAsInt(180242, @TableRowID)

			SELECT @ObjectID = CASE @AffinityLuli
								WHEN 76185 THEN 153389 /*L&G*/
								WHEN 76186 THEN 153391 /*Buddies*/
								WHEN 76510 THEN 157876 /*Asda Money*/
								ELSE 'Unknown'
								END
								
			EXEC dbo._C00_SimpleValueIntoField 180251, @ObjectID, @TableRowID, @ClientPersonnelID /*AffinityID*/
			
		SELECT @Section = dbo.fnGetSimpleDvAsInt(180243, @TableRowID)
		SELECT @SubSection = dbo.fnGetSimpleDvAsInt(180244, @TableRowID)
		
		SELECT @ObjectName = rldv.ResourceListID
		FROM ResourceListDetailValues rldv WITH (NOLOCK)
		INNER JOIN ResourceListDetailValues rldv2 WITH ( NOLOCK ) ON rldv2.ResourceListID = rldv.ResourceListID
		WHERE rldv.DetailFieldID = 146189
		AND rldv2.DetailFieldID = 146190
		AND rldv.DetailValue = @Section
		AND rldv2.DetailValue = @SubSection
		
		EXEC dbo._C00_SimpleValueIntoField 180252, @ObjectName, @TableRowID, @ClientPersonnelID /*PolicySectionID*/
			
		END
		
		
		IF @TableDFID = 177428 /*Specific Existing Conditions*/
		BEGIN
	
			SELECT	 @ObjectName	= cp.UserName
					,@VarcharDate	= CONVERT(VARCHAR,dbo.fn_GetDate_Local(),121)
			FROM ClientPersonnel cp WITH ( NOLOCK ) 
			WHERE cp.ClientPersonnelID = @ClientPersonnelID

			EXEC _C00_CompleteTableRow @TableRowID, NULL, 0, 0 /*Deny Editing and create any missing fields*/

			EXEC dbo._C00_SimpleValueIntoField 177427, @VarcharDate		 , @TableRowID, @ClientPersonnelID /*Date Added*/
			EXEC dbo._C00_SimpleValueIntoField 177515, @ClientPersonnelID, @TableRowID, @ClientPersonnelID /*Condition Added by ClientPersonnelID*/
			EXEC dbo._C00_SimpleValueIntoField 177516, @ObjectName		 , @TableRowID, @ClientPersonnelID /*Added By*/
		
			/*find the lead ID*/
			SELECT @LeadID = tr.LeadID
			FROM TableRows tr WITH ( NOLOCK ) 
			WHERE tr.TableRowID = @TableRowID
		
			/*get the count value*/
			SELECT @TableRowCount = COUNT (Tr.TableRowID)
			FROM Lead l WITH (NOLOCK)
			INNER JOIN TableRows TR WITH (NOLOCK) ON Tr.LeadID = l.LeadID
			WHERE Tr.DetailFieldID = 177428
			AND tr.ClientID = @ClientID
			AND tr.LeadID = @LeadID
			
			--/*insert the value*/
			--EXEC dbo._C00_SimpleValueIntoField 178145,@TableRowCount, @LeadID, @ClientPersonnelID /*Total number of exclusions*/

			/*insert the value*/
			UPDATE LeadDetailValues
			SET	  DetailValue = @TableRowCount 
			FROM LeadDetailValues ldv WITH (NOLOCK)
			WHERE ldv.DetailFieldID = 178145
			AND ldv.ClientID = @ClientID
			AND ldv.LeadID = @LeadID
	
			SELECT @NoteCount = COUNT (C.CaseID)
			FROM Cases C WITH (NOLOCK) 
			INNER JOIN LeadEvent Le WITH (NOLOCK) ON Le.CaseID = c.CaseID
			WHERE C.ClientID = @ClientID
			AND le.NoteTypeID = 928 
			AND c.CaseID = @CaseID
			AND le.EventDeleted = 0	
				
				/*GPR 2019-11-04 replaced by HUD Config*/
				----IF @NoteCount <=0
				----BEGIN
				
				----	/*Add exclusions apply note */
				----	EXEC _C00_AddANote @CaseID,928,'Exclusions Apply - please see Pet Details for further information',1,44412,0,0,@NoteDate 
				
				----END
		END
		
		-- Document Autozip Table Configuration
		IF @TableDFID=170149
		BEGIN
					
			SELECT @ObjectTypeID=dtid.ValueInt,@ObjectName=dn.DetailValue,@FolderToUse=folli.ItemValue
			FROM TableDetailValues dtid WITH (NOLOCK) 
			INNER JOIN TableDetailValues dn WITH (NOLOCK) ON dtid.TableRowID=dn.TableRowID and dn.DetailFieldID=170147
			INNER JOIN TableDetailValues fol WITH (NOLOCK) ON dtid.TableRowID=fol.TableRowID and fol.DetailFieldID=170148
			INNER JOIN LookupListItems folli WITH (NOLOCK) ON folli.LookupListItemID=fol.ValueInt
			WHERE dtid.TableRowID=@TableRowID AND dtid.DetailFieldID=170146 

			-- update
			SELECT @LinkID=dzcl.DocumentZipFtpControlLinkID
			FROM DocumentZipFtpControlLink dzcl WITH (NOLOCK) 
			WHERE dzcl.LinkNotes LIKE CONVERT(VARCHAR(30),@TableRowID) + ' %'
			
			SELECT @ControlID=dzc.DocumentZipFtpControlID FROM DocumentZipFtpControl dzc WITH (NOLOCK) 
			WHERE dzc.JobName = 'Zip to ' + @FolderToUse and JobNotes = 'SAS Config'
			
			IF @ControlID IS NULL
			BEGIN
			
				-- create new control file
				SELECT @BATName=LEFT(@FolderToUse,CASE CHARINDEX('\',@foldertouse) WHEN 0 THEN LEN(@FolderToUse) ELSE CHARINDEX('\',@FolderToUse)-1 END)
								+ REVERSE(LEFT(REVERSE(@FolderToUse),CASE CHARINDEX('\',@foldertouse) WHEN 0 THEN LEN(@FolderToUse) ELSE CHARINDEX('\',REVERSE(@FolderToUse))-1 END)) 
								+ '.bat'
				INSERT INTO DocumentZipFtpControl (ClientID,JobName,JobNotes,FiringOrder,SuppressAllLaterJobs,IsConfigFileRequired,ConfigFileName,IsBatFileRequired,BatFileName,Enabled,MaxWaitTimeMS,SpecificDocumentTypesOnly) VALUES
				(@ClientID,'Zip to '+@FolderToUse,'SAS Config',101,0,0,'',1,@BATName,1,1000,1)
							
				SELECT @ControlID = SCOPE_IDENTITY()

				INSERT INTO DocumentZipFtpCommand (ClientID,DocumentZipFtpControlID,LineType,LineText,LineNotes,LineOrder,Enabled) VALUES 
				(@ClientID,@ControlID,'bat','copy "[@ZIPFILENAME]" "\\468000-File1\AqShared\SharedFtp\Client_' + CONVERT(VARCHAR,@ClientID) + '\' + @FolderToUse + '\"','Copy file to Shared FTP folder',1,1),
				(@ClientID,@ControlID,'bat','exit','Exit DOS',2,1)
			
			END
			
			select @ControlID, @linkID
			
			IF @LinkID IS NOT NULL  -- just update the document link
			BEGIN
			
				UPDATE DocumentZipFtpControlLink 
				SET DocumentTypeID=@ObjectTypeID,
					LinkNotes=CONVERT(VARCHAR(30),@TableRowID) + ' ' + @ObjectName,
					DocumentZipFtpControlID=@ControlID 
				WHERE ClientID=@ClientID AND DocumentZipFtpControlLinkID=@LinkID
			
			END 
			ELSE
			BEGIN -- add new

				INSERT INTO DocumentZipFtpControlLink (ClientID,DocumentZipFtpControlID,DocumentTypeID,LinkNotes,Enabled) VALUES 
				(@ClientID,@ControlID,@ObjectTypeID,CONVERT(VARCHAR(30),@TableRowID) + ' ' + @ObjectName,1)

			END
			
		END
		
		-- Report Autozip Table Configuration
		IF @TableDFID=170167
		BEGIN
		
			UPDATE TableRows SET DenyEdit=1 WHERE TableRowID=@TableRowID -- we are barring update for the time being so ignore the update section below

			SELECT @ObjectTypeID=bjid.ValueInt,@ObjectName=bjn.DetailValue,@FolderToUse=folli.ItemValue,
					@ReportFileExt=rfe.DetailValue,@ReportFilename=rfn.DetailValue
			FROM TableDetailValues bjid WITH (NOLOCK) 
			INNER JOIN TableDetailValues bjn WITH (NOLOCK) ON bjid.TableRowID=bjn.TableRowID and bjn.DetailFieldID=170164
			INNER JOIN TableDetailValues fol WITH (NOLOCK) ON bjid.TableRowID=fol.TableRowID and fol.DetailFieldID=170165
			INNER JOIN TableDetailValues rfn WITH (NOLOCK) ON bjid.TableRowID=rfn.TableRowID and rfn.DetailFieldID=176590
			INNER JOIN TableDetailValues rfe WITH (NOLOCK) ON bjid.TableRowID=rfe.TableRowID and rfe.DetailFieldID=170166
			INNER JOIN LookupListItems folli WITH (NOLOCK) ON folli.LookupListItemID=fol.ValueInt
			WHERE bjid.TableRowID=@TableRowID AND bjid.DetailFieldID=170163 

			-- update
			SELECT @LinkID=atcc.AutomatedTaskCommandControlID
			FROM AutomatedTaskCommandControl atcc WITH (NOLOCK) 
			WHERE atcc.JobNotes LIKE CONVERT(VARCHAR(30),@TableRowID) + ' %'
			
			select @linkID
			
			IF @LinkID IS NOT NULL  -- update
			BEGIN
			
				UPDATE AutomatedTaskCommandControl 
				SET JobName=@ObjectName,
					JobNotes=CONVERT(VARCHAR(30),@TableRowID) + ' ' + @ObjectName,
					BatFileName=LEFT(@FolderToUse,CASE CHARINDEX('\',@foldertouse) WHEN 0 THEN LEN(@FolderToUse) ELSE CHARINDEX('\',@FolderToUse)-1 END)
								+ REVERSE(LEFT(REVERSE(@FolderToUse),CASE CHARINDEX('\',@foldertouse) WHEN 0 THEN LEN(@FolderToUse) ELSE CHARINDEX('\',REVERSE(@FolderToUse))-1 END)) 
								+ CONVERT(VARCHAR(30),@ObjectTypeID)		
								+ '.bat',
					TaskID=@ObjectTypeID,
					OutputExtension=@ReportFileExt 
				WHERE ClientID=@ClientID AND AutomatedTaskCommandControlID=@LinkID
				
				Update AutomatedTaskCommandLine
				set LineText='copy "[@ZIPFILENAME]" "\\468000-File1\AqShared\SharedFtp\Client_' + CONVERT(VARCHAR,@ClientID) + '\' + @FolderToUse + '\"'
				where  ClientID=@ClientID AND AutomatedTaskCommandControlID=@LinkID AND LineOrder=5
			
			END 
			ELSE
			BEGIN -- add new

				SELECT @BATName=LEFT(@FolderToUse,CASE CHARINDEX('\',@foldertouse) WHEN 0 THEN LEN(@FolderToUse) ELSE CHARINDEX('\',@FolderToUse)-1 END)
								+ REVERSE(LEFT(REVERSE(@FolderToUse),CASE CHARINDEX('\',@foldertouse) WHEN 0 THEN LEN(@FolderToUse) ELSE CHARINDEX('\',REVERSE(@FolderToUse))-1 END)) 
								+ CONVERT(VARCHAR(30),@ObjectTypeID)		
								+ '.bat'
				INSERT INTO AutomatedTaskCommandControl (ClientID,JobName,JobNotes,FiringOrder,SuppressAllLaterJobs,IsConfigFileRequired,ConfigFileName,IsBatFileRequired,BatFileName,Enabled,MaxWaitTimeMS,TaskID,OutputExtension) VALUES
				(@ClientID,@ObjectName,CONVERT(VARCHAR(30),@TableRowID) + ' ' + @ObjectName,101,0,0,'',1,@BATName,1,1000,@ObjectTypeID,@ReportFileExt)
							
				SELECT @ControlID = SCOPE_IDENTITY()

				-- copy string.  For text files we need to remove the header row always added by Aquarium processing
				SELECT @BATCopyLine = CASE WHEN @ReportFileExt='txt' THEN
									  'more +3 "[@ZIPFILENAME]" > "' + @ReportFilename + '.' + @ReportFileExt + '" & copy "' + @ReportFilename + '.' + @ReportFileExt + '" \\468000-File1\AqShared\SharedFtp\Client_' + CONVERT(VARCHAR,@ClientID) + '\' + @FolderToUse
									  ELSE
									  'copy "[@ZIPFILENAME]" "\\468000-File1\AqShared\SharedFtp\Client_' + CONVERT(VARCHAR,@ClientID) + '\' + @FolderToUse + '\' + @ReportFilename + '.' + @ReportFileExt + '"'
									  END
				
				INSERT INTO AutomatedTaskCommandLine (ClientID,AutomatedTaskCommandControlID,LineType,LineText,LineNotes,LineOrder,Enabled) VALUES 
				(@ClientID,@ControlID,'bat','type nul>"empty' + CONVERT(VARCHAR(30),@ObjectTypeID) + '.txt"','Create an empty file using the DOS nul command',0,1),
				(@ClientID,@ControlID,'bat','fc "[@ZIPFILENAME]" "empty' + CONVERT(VARCHAR(30),@ObjectTypeID) + '.txt"','Check for a valid file (ie not empty)',1,1),
				(@ClientID,@ControlID,'bat','if errorlevel 1 goto TRANSFERFILE','Send the file if not empty',2,1),
				(@ClientID,@ControlID,'bat','if errorlevel 0 goto END','Skip to end if file is empty',3,1),
				(@ClientID,@ControlID,'bat',':TRANSFERFILE','Label for start of file transfer section',4,1),
				--(@ClientID,@ControlID,'bat','for /F "tokens=1-4 delims=/ " %%i in (''date /t'') do set yyyymmdd=%%k%%j%%i','Date part for new filename',5,1),
				(@ClientID,@ControlID,'bat','for /F "tokens=1-4 delims=/ " %%i in (''date /t'') do set yyyy=%%k','year part for new filename',5,1),
				(@ClientID,@ControlID,'bat','for /F "tokens=1-4 delims=/ " %%i in (''date /t'') do set mn=%%j','month part for new filename',5,1),
				(@ClientID,@ControlID,'bat','for /F "tokens=1-4 delims=/ " %%i in (''date /t'') do set dd=%%i','Date part for new filename',5,1),
				--(@ClientID,@ControlID,'bat','for /F "tokens=1-2 delims=: " %%l in (''time /t'') do set hhmm=%%l%%m','Time part for new filename',6,1),
				(@ClientID,@ControlID,'bat','for /F "tokens=1-3 delims=:. " %%l in (''echo %time%'') do set hh=%%l','hour part for new filename',6,1),
				(@ClientID,@ControlID,'bat','for /F "tokens=1-3 delims=:. " %%l in (''echo %time%'') do set mm=%%m','minute part for new filename',6,1),
				(@ClientID,@ControlID,'bat','for /F "tokens=1-3 delims=:. " %%l in (''echo %time%'') do set ss=%%n','second part for new filename',6,1),
				(@ClientID,@ControlID,'bat',@BATCopyLine,'Copy file to Shared FTP folder',7,1),
				--(@ClientID,@ControlID,'bat','copy "[@ZIPFILENAME]" "\\468000-File1\AqShared\SharedFtp\Client_600\' + @FolderToUse + '\' + @ObjectName + '_%yyyymmdd%_%hhmm%.' + @ReportFileExt + '"','Copy file to Shared FTP folder',7,1),
				(@ClientID,@ControlID,'bat',':END','Label for end of processing',8,1),
				(@ClientID,@ControlID,'bat','exit','Exit DOS',9,1)

--				In resource list would have for example 'AQ_Claims_for_Cheques_%yyyymmdd%_%hhmm%' & for the risk bordereau '%yyyymm%_Risk'
			END
			
		END
		
		IF  @TableDFID=170224  -- BACS Import Lead Type; Unreconciled Imports -- Manual Reconciliation attempt
		BEGIN
		
			-- move the table row back into the new imports table and the attempt full reconciliation again
			UPDATE tr
			SET DetailFieldID = 170221
				, DenyDelete  = 0
				, DenyEdit    = 0
			FROM TableRows tr WITH (NOLOCK) 
			WHERE tr.TableRowID = @TableRowID
			
			EXEC _C600_BACS_ReconcileImport @TableRowID, 2, @ClientPersonnelID
		
		END
		
		-- Turn off editting of PA tablerows that had been temporarily granted it
		-- find a MatterID for the table row
		SELECT @ObjectID=CASE df.LeadOrMatter 
							WHEN 1 THEN tr.LeadID
							WHEN 11 THEN tr.CaseID
							WHEN 2 THEN tr.MatterID
							END,
				@ObjectTypeID=df.LeadOrMatter 
		FROM TableRows tr WITH (NOLOCK)
		INNER JOIN DetailFields df WITH (NOLOCK) ON tr.DetailFieldID=df.DetailFieldID 
		WHERE tr.TableRowID=@TableRowID

		IF EXISTS ( SELECT * FROM Lead l WITH (NOLOCK)
					INNER JOIN Matter m WITH (NOLOCK) ON ((m.MatterID=@ObjectID AND @ObjectTypeID=2)
														OR (m.CaseID=@ObjectID AND @ObjectTypeID=11)
														OR (m.LeadID=@ObjectID AND @ObjectTypeID=1))
														AND l.LeadID=m.LeadID
					INNER JOIN CustomerDetailValues aff WITH (NOLOCK) ON l.CustomerID=aff.CustomerID AND aff.DetailFieldID=170144
					INNER JOIN ResourceListDetailValues doespa WITH (NOLOCK) ON aff.ValueInt=doespa.ResourceListID AND doespa.DetailFieldID=175277
					WHERE l.LeadTypeID=1492 
						AND doespa.ValueInt=5145 )
		BEGIN
		
			UPDATE TableRows SET DenyEdit=1 WHERE TableRowID=@TableRowID
		
		END	

		/*2018-06-13 ACE - Call seperate table row proc as this one is getting large.*/
		EXEC _C600_SAS_TableRow @ClientPersonnelID, @CustomerID, @LeadID, @CaseID, @MatterID, @TableRowID, @TableDFID, @ResourceListID

	END

	IF @SASType = 10 /* Customer */
	BEGIN
	
		DECLARE @DocsBy INT, @CCEmail INT, @CCSMS INT
		
		SELECT TOP 1 @CaseID=m.CaseID FROM Matter m WITH (NOLOCK) WHERE m.CustomerID=@CustomerID
		
		SELECT @DocsBy=dbo.fnGetDvAsInt ( 170257, @CaseID )
		SELECT @CCEmail=dbo.fnGetDvAsInt ( 170259, @CaseID )
		SELECT @CCSMS=dbo.fnGetDvAsInt ( 170260, @CaseID )

		-- copy contact preferences to the claims lead

		UPDATE ldv
		SET DetailValue=CASE ldv.DetailfieldID
				WHEN 157206 THEN @DocsBy
				WHEN 157205 THEN @DocsBy
				WHEN 150397 THEN @CCEmail
				WHEN 152654 THEN @CCSMS
				END
		FROM Lead l 
		INNER JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.LeadID=l.LeadID AND ldv.DetailFieldID IN (157206,157205,150397,152654)
		WHERE l.LeadTypeID=1492 AND l.CustomerID=@CustomerID

		-- check preferences have valid contact details
		--EXEC _C600_CheckCommunicationPreferences @CustomerID /*Removed by GPR 2019-11-01*/
			
		
			/*Get the affinity value of the Camp Code we have entered*/ 
		SELECT @ValueINT = CASE rldv.ValueInt WHEN 76185 THEN 76183  -- L&G
											  WHEN 76186 THEN 74539 ELSE 0 END  -- Buddies
		FROM CustomerDetailValues cdv WITH ( NOLOCK ) 
		INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) on rldv.ResourceListID = cdv.ValueInt 
		WHere cdv.DetailFieldID = 179912
		AND rldv.DetailFieldID = 179709
		AND cdv.CustomerID = @CustomerID 

		/*If we have entered a camp code, raise an error if it's for the wrong affinity*/ 
		IF ISNULL(@ValueINT,0) <> 0 AND  EXISTS ( SELECT * 
		FROM CustomerDetailValues cdv WITH ( NOLOCK ) 
		INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) on rldv.ResourceListID = cdv.ValueInt 
		WHERE cdv.DetailFieldID = 170144
		AND rldv.DetailFieldID = 170127
		AND rldv.ValueInt <> @ValueINT
		AND cdv.CustomerID = @CustomerID) 
		BEGIN 
			
			EXEC dbo._C00_SimpleValueIntoField 179912, 0, @CustomerID, @AqAutomation
		
		END 
			
			
			
		
	END
	
	/*JEL report runner */
	IF @SASType = 12 
	BEGIN 

		SELECT @RunReport = dbo.fnGetSimpleDv(180181,@ClientID) /*Check Box*/

		IF ISNULL(@RunReport,'false') <> 'false'  /*only  run if tickbox ticked*/
		BEGIN 
			IF EXISTS (	SELECT *
			           	FROM AutomatedTask at WITH (NOLOCK) 
						WHERE at.TaskID = 22889 /*MI Report Variable*/
						AND (at.AlreadyRunning = 1 or at.NextRunDateTime <= dbo.fn_GetDate_Local())
					  )
			BEGIN
				SELECT @ErrorMessage = '<br><br><font color="red">Error: Report ' + sq.QueryTitle + ' is already queued to run at ' + ISNULL(CONVERT(VARCHAR,at.NextRunDateTime,8),'NULL') + '</font>'
				FROM AutomatedTaskParam atp WITH (NOLOCK) 
				INNER JOIN dbo.SqlQuery sq WITH (NOLOCK) on sq.QueryID = atp.ParamValue
				INNER JOIN dbo.AutomatedTask at WITH (NOLOCK) on at.TaskID = atp.TaskID
				WHERE atp.TaskID = 22889 /*MI Report Variable*/
				AND atp.ParamName = 'SQL_QUERY'

				RAISERROR( @ErrorMessage, 16, 1 )
				RETURN
				 
			END
			ELSE
			BEGIN
				SELECT  @To		  = dbo.fnGetSimpleDv(180180,@ClientID) /*To*/
					   ,@From	  = dbo.fnGetSimpleDv(180179,@ClientID) /*From*/
					   ,@ValueINT = dbo.fnGetSimpleDv(180178,@ClientID) /*Reports LuL*/ /*5*/
				   
				SELECT @ReportID =	CASE @ValueINT /*updated NG 2021-03-15 for FURKIN-419*/
										WHEN '193359' THEN '38873' -- Quotes and Sales FURKIN
										WHEN '193362' THEN '39072' -- Quotes and Sales PHI
										WHEN '193358' THEN '38878' -- Active Policy Register FURKIN
										WHEN '193361' THEN '39059' -- Active Policy Register PHI
										WHEN '193357' THEN '38867' -- Premium Bordereau FURKIN
										WHEN '193360' THEN '39075' -- Premium Bordereau PHI
										WHEN '193303' THEN '38955' -- Collections CC FURKIN
										WHEN '193304' THEN '38960' -- Refunds CC FURKIN
										WHEN '193284' THEN '38879' -- Missed Payments
										WHEN '193363' THEN '39063' -- Lapsed Policy Register FURKIN
										WHEN '193364' THEN '39064' -- Lapsed Policy Register PHI
										WHEN '193280' THEN '38868' -- Refunds DD
										WHEN '193281' THEN '38870' -- Collections DD
										WHEN '193285' THEN '38874' -- Policy Services Workflow - MTA requests
										WHEN '193286' THEN '38875' -- Policy Services Workflow - Cancellation Requests
										WHEN '193287' THEN '38876' -- Policy Services Workflow - Reinstatement Requests
										WHEN '193288' THEN '38877' -- Policy Services Workflow - Renewal Calls
										WHEN '193367' THEN '39088' -- Cancelled Policy Register - FURKIN	
										WHEN '193368' THEN '39089' -- Cancelled Policy Register - PHI DIRECT	
										WHEN '193369' THEN '39090' -- Reinstated Policy Register - FURKIN
										WHEN '193370' THEN '39093' -- Reinstated Policy Register - PHI DIRECT
										WHEN '193371' THEN '39077' -- Claim Payments FURKIN - 2021-04-22 AAD for FURKIN-531
										WHEN '193372' THEN '39096' -- Claim Payments PHI DIRECT - 2021-04-22 AAD for FURKIN-531
										WHEN '193373' THEN '39100' -- Collections CC PHI DIRECT - 2021-04-22 AAD for FURKIN-531
										WHEN '193374' THEN '39106' -- Refunds CC PHI DIRECT - 2021-04-22 AAD for FURKIN-531
										WHEN '193375' THEN '39110' -- Policy Services Workflow - Cancel after Pet Deceased Claim - 2021-05-11 NG for FURKIN-601
										ELSE 0 
									END 

				IF @ValueINT <> 0 
				BEGIN 	
					Update s 
					SET QueryText = REPLACE(REPLACE(ss.QueryText, '@FromDate',@From),'@ToDate',@To)  
					FROM SqlQuery s 
					INNER JOIN dbo.SqlQuery ss WITH (NOLOCK) on ss.QueryID = @ReportID 
					where s.QueryID = 38882 /*SYS Date Editable Template*/
				
					/* GR 2017-06-13*/
					/*CPS 2019-07-16 for CR083 - include the current user as a CC when updating the batch*/
					UPDATE atp
					SET ParamValue =CASE atp.ParamName 
										WHEN 'FILE_PREFIX'			THEN s.QueryTitle
										WHEN 'EMAIL_SALUTATION'		THEN s.QueryTitle + ' report for records between ' + ISNULL(CONVERT(VARCHAR,@From,103),'NULL') + ' and ' + ISNULL(CONVERT(VARCHAR,@To,103),'NULL')  + ISNULL(' from ' + DB_NAME(),'')
										WHEN 'MESSAGE_HEADER'		THEN s.QueryTitle + ' report triggered by ' + cp.UserName
										WHEN 'EMAIL_CC'				THEN REPLACE(cp.EmailAddress,@ClientID,'')
										WHEN 'ALERT_ON_ERROR_EMAIL' THEN REPLACE(cp.EmailAddress,@ClientID,'')
									END
					FROM AutomatedTaskParam atp
					INNER JOIN dbo.AutomatedTask at WITH (NOLOCK) ON at.TaskID = atp.TaskID
					INNER JOIN dbo.SqlQuery s WITH (NOLOCK) ON s.QueryID = @ReportID 
					INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = @ClientPersonnelID
					WHERE atp.TaskID = 22889 /*MI Report Variable*/
					AND atp.ParamName IN ('FILE_PREFIX','EMAIL_CC','EMAIL_SALUTATION','MESSAGE_HEADER') 
					AND atp.ClientID = @ClientID

					EXEC AutomatedTask__RunNow   22889,0,NULL,0 /*MI Report Variable*/
				END 
			
				/*Clear out tickbox*/ 
				EXEC _C00_SimpleValueIntoField  180181,'false',@ClientID 
			END
		END
	END 
	
	
	IF @SASType = 4 -- Resource List - VET - Record changes to Account Details
	BEGIN
		
		--	/*Get the affinity value of the Camp Code we have entered*/ 
		SELECT @ValueINT = CASE rldv.ValueInt WHEN 76185 THEN 76183  -- L&G
											  WHEN 76186 THEN 74539 ELSE 0 END  -- Buddies
		FROM CustomerDetailValues cdv WITH ( NOLOCK ) 
		INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) on rldv.ResourceListID = cdv.ValueInt 
		WHere cdv.DetailFieldID = 179912
		AND rldv.DetailFieldID = 179709
		AND cdv.CustomerID = @CustomerID 

		/*If we have entered a camp code, raise an error if it's for the wrong affinity*/ 
		IF ISNULL(@ValueINT,0) <> 0 AND  EXISTS ( SELECT * 
		FROM CustomerDetailValues cdv WITH ( NOLOCK ) 
		INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) on rldv.ResourceListID = cdv.ValueInt 
		WHERE cdv.DetailFieldID = 170144
		AND rldv.DetailFieldID = 170127
		AND rldv.ValueInt <> @ValueINT
		AND cdv.CustomerID = @CustomerID) 
		BEGIN 
			
			EXEC dbo._C00_SimpleValueIntoField 179912, 0, @CustomerID, @AqAutomation

		END 

		/*GPR 2018-07-20 brought over from C433 FOR CR0011
		Originally AH 2017-10-31 / and CPS 2017-11-23*/
		IF EXISTS ( SELECT * 
					FROM ResourceListDetailValues rdv WITH (NOLOCK) 
					WHERE rdv.ResourceListID = @ResourceListID
					AND rdv.DetailFieldID = 180101 /*Tracking Code Brand RLDF*/
				  )
		BEGIN
			SELECT TOP 1 @TrackingCode = CONVERT(VARCHAR(32), HashBytes('MD5', CONVERT(VARCHAR,@ResourceListID)), 2), @WebPageURL = dbo.fnGetSimpleDv (180108,@ResourceListID)

			--SELECT @TrackingURL = @WebPageURL + '?token=' + @TrackingCode 
			/*GPR 2018-07-25 use ?cid= in place of ?token= for C600 EQB*/
			/*AHOD 2018-08-20 Removed ?cid= */
			SELECT @TrackingURL = @WebPageURL + @TrackingCode 
													
			EXEC _C00_SimpleValueIntoField 180109,@TrackingCode,@ResourceListID,@ClientPersonnelID /*URL Field within Tracking Code RL*/
			EXEC _C00_SimpleValueIntoField 180114,@TrackingURL,@ResourceListID,@ClientPersonnelID /*AH 2017-10-31: URL Field within Tracking URL RL*/
		END
		
		IF EXISTS ( SELECT * 
		            FROM ResourceListDetailValues rdv WITH ( NOLOCK ) 
		            WHERE rdv.ResourceListID = @ResourceListID
		            AND rdv.DetailFieldID = 144473 /*VET Business Name*/
		          )
		BEGIN
		
			SELECT @LogEntry = 'ResourceList ' + convert(varchar,rdv.ResourceListID) + ' (' + rdv.DetailValue + ') edited by user '+ cp.FirstName + ' ' + cp.LastName
			FROM ResourceListDetailValues rdv WITH (NOLOCK) 
			INNER JOIN ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = @ClientPersonnelID
			WHERE rdv.ResourceListID = @ResourceListID 
			and rdv.DetailFieldID IN (175588,175587,175589,175590,178033) /*Bank name*/ /*Account Name*/ /*Account Sort Code*/ /*Account Number*/
		
			IF @LogEntry > ''
			BEGIN
			
				EXEC _C00_LogIt 'Debug', '_C600_LOG', 'C600_SAS', @LogEntry, @ClientPersonnelID
				
				SET @LogEntry = ''				
			
			END
			
			SELECT  @VetName =  rl.DetailValue,
					@BankName = rl1.DetailValue,  
					@AccountName =  rl2.DetailValue,
					@AccountSortCode =  rl3.DetailValue, 
					@AccountNumber =  rl4.DetailValue,
					@SanctionScreeening  = lli.ItemValue
			FROM ResourceList r WITH (NOLOCK) 
			INNER JOIN dbo.ResourceListDetailValues rl WITH (NOLOCK) on rl.ResourceListID = r.ResourceListID
			LEFT JOIN dbo.ResourceListDetailValues rl1 WITH (NOLOCK) on rl1.ResourceListID = rl.ResourceListID and rl1.detailfieldID = 175588 /*Bank Name*/
			LEFT JOIN dbo.ResourceListDetailValues rl2 WITH (NOLOCK) on rl2.ResourceListID = rl.ResourceListID and rl2.detailfieldID = 175587 /*Account Name*/
			LEFT JOIN dbo.ResourceListDetailValues rl3 WITH (NOLOCK) on rl3.ResourceListID = rl.ResourceListID and rl3.detailfieldID = 175589 /*Account Sort-Code*/
			LEFT JOIN dbo.ResourceListDetailValues rl4 WITH (NOLOCK) on rl4.ResourceListID = rl.ResourceListID and rl4.detailfieldID = 175590 /*Account Number*/
			LEFT JOIN dbo.ResourceListDetailValues rl5 WITH (NOLOCK) on rl5.ResourceListID = rl.ResourceListID and rl5.detailfieldID = 178033 /*screening status*/	 
			LEFT JOIN dbo.LookupListItems lli WITH (NOLOCK) ON lli.LookupListItemID = rl5.ValueInt
			where r.ResourceListID = @ResourceListID
			and rl.DetailFieldID = 144473 /*VET Business Name*/

			SELECT	 @ObjectName	= cp.UserName
					,@VarcharDate	= CONVERT(VARCHAR,dbo.fn_GetDate_Local(),121)
			FROM	ClientPersonnel cp WITH ( NOLOCK ) 
			WHERE	cp.ClientPersonnelID = @ClientPersonnelID

			EXEC _C00_LogIt   'RL SAS','C600 SAS','RL Update',@LogEntry,@ClientPersonnelID  
			
			INSERT TableRows ( ClientID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit )
			VALUES ( @ClientID, 177529, 19056, 1, 1 ) /*VET Bank Details Update History*/
			
			SELECT @TableRowID = SCOPE_IDENTITY()
			
			/*Post Values into table*/
			EXEC dbo._C00_SimpleValueIntoField 177528,		@VetName,@TableRowID, @ClientPersonnelID  /*Update made*/
			EXEC dbo._C00_SimpleValueIntoField 177530,		@BankName,@TableRowID, @ClientPersonnelID  /*Update made*/
			EXEC dbo._C00_SimpleValueIntoField 177531,		@AccountName,@TableRowID, @ClientPersonnelID  /*Update made*/
			EXEC dbo._C00_SimpleValueIntoField 177532,		@AccountSortCode,@TableRowID, @ClientPersonnelID  /*Update made*/
			EXEC dbo._C00_SimpleValueIntoField 177533,		@AccountNumber,@TableRowID, @ClientPersonnelID  /*Update made*/
			EXEC dbo._C00_SimpleValueIntoField 177534,		@ObjectName,@TableRowID, @ClientPersonnelID  /*Update made*/
			EXEC dbo._C00_SimpleValueIntoField 177535,		@VarcharDate,@TableRowID, @ClientPersonnelID  /*Update made*/
			EXEC dbo._C00_SimpleValueIntoField 177536,		@ClientPersonnelID,@TableRowID, @ClientPersonnelID  /*Update made*/

			INSERT TableRows ( ClientID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit )
			VALUES ( @ClientID, 179439, 19056, 1, 1 ) /*SS*/
	
			SELECT @TableRowID = SCOPE_IDENTITY()	

			EXEC dbo._C00_SimpleValueIntoField 179435,		@SanctionScreeening,@TableRowID, @ClientPersonnelID  /*Update made*/
			EXEC dbo._C00_SimpleValueIntoField 179434,		@VetName,@TableRowID, @ClientPersonnelID  /*Update made*/
			EXEC dbo._C00_SimpleValueIntoField 179438,		@ClientPersonnelID,@TableRowID, @ClientPersonnelID  /*Update made*/
			EXEC dbo._C00_SimpleValueIntoField 179437,		@ObjectName,@TableRowID, @ClientPersonnelID  /*Update made*/
			EXEC dbo._C00_SimpleValueIntoField 179436,		@VarcharDate,@TableRowID, @ClientPersonnelID  /*Update made*/

			/*Sanctioned screening error message*/
			SELECT @UnderFraudCheck = dbo.fnGetSimpleDv (178033,@ResourceListID)
		
			IF @UnderFraudCheck = 75243
			BEGIN
				EXEC dbo._C00_SimpleValueIntoField 178034,'<font color="red">Vet currently under compliance review</font>',@ResourceListID, @ClientPersonnelID  /*Update made*/
			END
		
			IF @UnderFraudCheck = ''
			BEGIN
				EXEC dbo._C00_SimpleValueIntoFieldLuli 178033,'Pending',@ResourceListID,NULL,''
			END
			
			/*BACs payment*/ 
			IF ISNULL(dbo.fnGetSimpleDvAsInt(179989,@ResourcelistID),0)  = 0 
			BEGIN 	
				
				SELECT @CustomerID = m.CustomerID, @ClientID = m.ClientID FROM MatterDetailValues mdv WITH ( NOLOCK ) 
				INNER JOIN Matter m WITH ( NOLOCK ) on m.MatterID = mdv.MatterID 
				WHERE mdv.DetailFieldID = 179990
				AND mdv.ValueINT = @ResourceListID 
			
				IF ISNULL(@CustomerID,0) = 0 
				BEGIN
					
					/*Add just a customer here so we can link the AccountID to it, one day we shall have a vets lead type*/ 	
					EXEC @CustomerID = _C00_CreateNewCustomerFull   @ClientID, 1506, @ClientPersonnelID,0,0,@VetName,'','','','',0,'',0,'',0,'',0,'',0,'','','','','','',0,'',2,NULL,0,@VetName,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,0,232,NULL,NULL,@ClientPersonnelID,NULL,NULL,NULL, NULL, NULL
					
					SELECT @MatterID = MatterID 
					FROM Matter m WITH ( NOLOCK ) 
					WHERE m.CustomerID = @CustomerID

					/*Add the vet to the MAtter*/ 
					EXEC _C00_SimpleValueIntoField  179990,@ResourceListID,@MatterID,@ClientPersonnelID  
				
				END	

				EXEC @AccountID = dbo.Billing__CreateClaimsAccountRecord @ClientID,@ResourceListID,4,@AccountNumber,@AccountSortCode,@AccountName,NULL,@CustomerID,@ClientPersonnelID,1
				/*Write the AccountID to the RL*/ 
				EXEC _C00_SimpleValueIntoField 179989, @AccountID, @ResourceListID, @ClientPersonnelID 
				
			END
			ELSE 
			BEGIN 
				
				/*If we have already created the account, then just update it*/ 
				SELECT @AccountID = dbo.fnGetSimpleDvAsInt(179989,@ResourcelistID) 
				
				UPDATE Account 
				SET AccountHolderName = @AccountName, AccountNumber = @AccountNumber, Sortcode = @AccountSortCode,WhoModified = @ClientPersonnelID, WhenModified = dbo.fn_GetDate_Local()
				WHERE AccountID = @AccountID 
				
			
			END 
			/*Cheque Payment*/ 
			IF ISNULL(dbo.fnGetSimpleDvAsInt(179999,@ResourcelistID),0)  = 0 
			BEGIN 	
				
				SELECT @CustomerID = m.CustomerID, @ClientID = m.ClientID FROM MatterDetailValues mdv WITH ( NOLOCK ) 
				INNER JOIN Matter m WITH ( NOLOCK ) on m.MatterID = mdv.MatterID 
				WHERE mdv.DetailFieldID = 179990
				AND mdv.ValueINT = @ResourceListID 
			
				IF ISNULL(@CustomerID,0) = 0 
				BEGIN
					
					/*Add just a customer here so we can link the AccountID to it, one day we shall have a vets lead type*/ 	
					EXEC @CustomerID = _C00_CreateNewCustomerFull  @ClientID, 1506, @ClientPersonnelID,0,0,@VetName,'','','','',0,'',0,'',0,'',0,'',0,'','','','','','',0,'',2,NULL,0,@VetName,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,0,232,NULL,NULL,@ClientPersonnelID,NULL,NULL,NULL, NULL, NULL
					SELECT @MatterID = MatterID FROM Matter m WITH ( NOLOCK ) where m.CustomerID = @CustomerID
					/*Add the vet to the MAtter*/ 
					EXEC _C00_SimpleValueIntoField  179990,@ResourceListID,@MatterID,@ClientPersonnelID  
				
				END	

				EXEC @AccountID = dbo.Billing__CreateClaimsAccountRecord @ClientID,@ResourceListID,4,NULL,NULL,@AccountName,NULL,@CustomerID,@ClientPersonnelID,3
				/*Write the AccountID to the RL*/ 
				EXEC _C00_SimpleValueIntoField 179999, @AccountID, @ResourceListID, @ClientPersonnelID 
				
			END
			ELSE 
			BEGIN 
				
				/*If we have already created the account, then just update it*/ 
				SELECT @AccountID = dbo.fnGetSimpleDvAsInt(179999,@ResourcelistID) 
				
				UPDATE Account 
				SET AccountHolderName = @AccountName, WhoModified = @ClientPersonnelID, WhenModified = dbo.fn_GetDate_Local()
				WHERE AccountID = @AccountID 
				
			
			END 
		END
		

		/*set all NEW VETS to SS status of pending*/
		IF EXISTS ( SELECT * 
		FROM ResourceListDetailValues rdv WITH (NOLOCK)
		INNER JOIN ResourceList rl WITH (NOLOCK) on rl.ResourceListID = rdv.ResourceListID
		WHERE rdv.DetailFieldID = 144473 /*VET Business Name*/
		AND rdv.ResourceListID = @ResourceListID
		AND CAST(rl.WhenCreated as DATE) = CAST(dbo.fn_GetDate_Local() AS DATE))
		BEGIN
			/*Set SS to pending*/
			EXEC dbo._C00_SimpleValueIntoFieldLuli 178033,'Pending',@ResourceListID, NULL,''  /*SS Status*/
			EXEC dbo._C00_SimpleValueIntoField 178034,'',@ResourceListID, @ClientPersonnelID  /*Remove red error msg*/
		END
		
		/*If RL is updated, has not been created today and the user has not confirmed update then default status to Pending*/ 
		IF EXISTS ( SELECT * 
		FROM ResourceListDetailValues rdv WITH (NOLOCK)
		INNER JOIN ResourceList rl WITH (NOLOCK) on rl.ResourceListID = rdv.ResourceListID
		INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) on rldv.ResourceListID = rl.ResourceListID 
		WHERE rdv.DetailFieldID = 144473 /*VET Business Name*/
		AND rdv.ResourceListID = @ResourceListID
		AND rldv.DetailFieldID = 179565
		AND rldv.ValueInt <> 5144 /*Yes*/
		AND CAST(rl.WhenModified as DATE) = CAST(dbo.fn_GetDate_Local() AS DATE))
		BEGIN
			/*Set SS to pending*/
			EXEC dbo._C00_SimpleValueIntoFieldLuli 178033,'Pending',@ResourceListID, NULL,''  /*SS Status*/
			EXEC dbo._C00_SimpleValueIntoField 178034,'',@ResourceListID, @ClientPersonnelID  /*Remove red error msg*/
		END
		
	END
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SAS] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_SAS] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SAS] TO [sp_executeall]
GO
