SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DocumentType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentType_Delete]
(

	@DocumentTypeID int   
)
AS


				DELETE FROM [dbo].[DocumentType] WITH (ROWLOCK) 
				WHERE
					[DocumentTypeID] = @DocumentTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType_Delete] TO [sp_executeall]
GO
