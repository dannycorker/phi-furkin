SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the RPIField table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIField_GetByQuestionTypeID]
(

	@QuestionTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[RPIFieldID],
					[RPIFieldName],
					[RPIFieldDescription],
					[IsEnabled],
					[QuestionTypeID],
					[DetailFieldSubtypeID],
					[LookupListID],
					[FieldOrder],
					[FieldSize],
					[WhenCreated],
					[WhenModified],
					[RPIFieldGroupID]
				FROM
					[dbo].[RPIField] WITH (NOLOCK) 
				WHERE
					[QuestionTypeID] = @QuestionTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIField_GetByQuestionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIField_GetByQuestionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIField_GetByQuestionTypeID] TO [sp_executeall]
GO
