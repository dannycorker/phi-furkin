SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the GroupRightsDynamic table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupRightsDynamic_Update]
(

	@GroupRightsDynamicID int   ,

	@ClientPersonnelAdminGroupID int   ,

	@FunctionTypeID int   ,

	@LeadTypeID int   ,

	@ObjectID int   ,

	@RightID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[GroupRightsDynamic]
				SET
					[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
					,[FunctionTypeID] = @FunctionTypeID
					,[LeadTypeID] = @LeadTypeID
					,[ObjectID] = @ObjectID
					,[RightID] = @RightID
				WHERE
[GroupRightsDynamicID] = @GroupRightsDynamicID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamic_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupRightsDynamic_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupRightsDynamic_Update] TO [sp_executeall]
GO
