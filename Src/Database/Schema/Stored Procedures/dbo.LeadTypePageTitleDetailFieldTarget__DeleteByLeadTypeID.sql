SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadTypePageTitleDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleDetailFieldTarget__DeleteByLeadTypeID]
(

	@LeadTypeID int   
)
AS


				DELETE FROM [dbo].[LeadTypePageTitleDetailFieldTarget] WITH (ROWLOCK) 
				WHERE
					[LeadTypeID] = @LeadTypeID
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleDetailFieldTarget__DeleteByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleDetailFieldTarget__DeleteByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleDetailFieldTarget__DeleteByLeadTypeID] TO [sp_executeall]
GO
