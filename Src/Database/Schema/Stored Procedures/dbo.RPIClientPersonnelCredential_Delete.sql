SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the RPIClientPersonnelCredential table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIClientPersonnelCredential_Delete]
(

	@RPIClientPersonnelCredentialID int   
)
AS


				DELETE FROM [dbo].[RPIClientPersonnelCredential] WITH (ROWLOCK) 
				WHERE
					[RPIClientPersonnelCredentialID] = @RPIClientPersonnelCredentialID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientPersonnelCredential_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIClientPersonnelCredential_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIClientPersonnelCredential_Delete] TO [sp_executeall]
GO
