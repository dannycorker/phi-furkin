SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptNoteTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptNoteTarget_GetByNoteTypeID]
(

	@NoteTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ScriptNoteTargetID],
					[ScriptID],
					[ClientID],
					[NoteTypeID],
					[PriorityID],
					[ApplyToAllCases]
				FROM
					[dbo].[ScriptNoteTarget] WITH (NOLOCK) 
				WHERE
					[NoteTypeID] = @NoteTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptNoteTarget_GetByNoteTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptNoteTarget_GetByNoteTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptNoteTarget_GetByNoteTypeID] TO [sp_executeall]
GO
