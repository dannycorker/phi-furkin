SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SubClient table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubClient_Find]
(

	@SearchUsingOR bit   = null ,

	@SubClientID int   = null ,

	@ClientID int   = null ,

	@CompanyName varchar (100)  = null ,

	@ClientTypeID int   = null ,

	@LanguageID int   = null ,

	@CountryID int   = null ,

	@WhenCreated datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SubClientID]
	, [ClientID]
	, [CompanyName]
	, [ClientTypeID]
	, [LanguageID]
	, [CountryID]
	, [WhenCreated]
    FROM
	[dbo].[SubClient] WITH (NOLOCK) 
    WHERE 
	 ([SubClientID] = @SubClientID OR @SubClientID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([CompanyName] = @CompanyName OR @CompanyName IS NULL)
	AND ([ClientTypeID] = @ClientTypeID OR @ClientTypeID IS NULL)
	AND ([LanguageID] = @LanguageID OR @LanguageID IS NULL)
	AND ([CountryID] = @CountryID OR @CountryID IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SubClientID]
	, [ClientID]
	, [CompanyName]
	, [ClientTypeID]
	, [LanguageID]
	, [CountryID]
	, [WhenCreated]
    FROM
	[dbo].[SubClient] WITH (NOLOCK) 
    WHERE 
	 ([SubClientID] = @SubClientID AND @SubClientID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([CompanyName] = @CompanyName AND @CompanyName is not null)
	OR ([ClientTypeID] = @ClientTypeID AND @ClientTypeID is not null)
	OR ([LanguageID] = @LanguageID AND @LanguageID is not null)
	OR ([CountryID] = @CountryID AND @CountryID is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SubClient_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubClient_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubClient_Find] TO [sp_executeall]
GO
