SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the YellowPagesAreaCodes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[YellowPagesAreaCodes_Delete]
(

	@YellowPagesAreaCodeID int   
)
AS


				DELETE FROM [dbo].[YellowPagesAreaCodes] WITH (ROWLOCK) 
				WHERE
					[YellowPagesAreaCodeID] = @YellowPagesAreaCodeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[YellowPagesAreaCodes_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[YellowPagesAreaCodes_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[YellowPagesAreaCodes_Delete] TO [sp_executeall]
GO
