SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LookupList table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupList_GetBySortOptionID]
(

	@SortOptionID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[LookupListID],
					[LookupListName],
					[LookupListDescription],
					[ClientID],
					[Enabled],
					[SortOptionID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[LeadTypeID],
					[IsShared]
				FROM
					[dbo].[LookupList] WITH (NOLOCK) 
				WHERE
					[SortOptionID] = @SortOptionID
				
				SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList_GetBySortOptionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupList_GetBySortOptionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList_GetBySortOptionID] TO [sp_executeall]
GO
