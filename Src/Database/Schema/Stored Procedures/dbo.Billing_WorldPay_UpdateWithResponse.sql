SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:           Paul Richardson
-- Create date: 26/03/2018
-- Description:      Updates the card transaction and account with the response results
--                         If the transaction id is empty the transaction has failed
-- 2018-03-27 ACE - Update Payment, CPS, PPPS and add required event
-- 2018-04-24 JEL - changed matching params to work with the information we have available 
-- 2019-03-15 JEL - Added filters to only update customerledger in the CPS and PPPS if the ID is currently NULL 
-- 2019-10-21 CPS for JIRA LPC-28	| Removed C600-specific renewal rules
-- 2019-10-21 CPS for JIRA LPC-28	| Include useful comments when applying the event
-- 2019-12-06 CPS for JIRA LPC-202	| Stop blocking CardTransaction update when the ErrorCode is NULL
-- 2019-12-09 CPS for JIRA LPC-181	| Trigger the card payer process once a positive notification comes back
-- 2019-12-09 CPS for JIRA LPC-181	| CustomerLedgerID is NULL filter removed following discussion with JEL
-- 2019-12-09 CPS for JIRA LPC-205	| Include LeadTypeID so we can filter CardReturnProcessing by LeadType
-- 2020-01-13 CPS for JIRA LPC-356	| Replace hard-coded ClientID with function
-- 2020-01-14 CPS for JIRA SDLPC-94 | Changed from c.OrderID to c.ParentID in refund parsing to line up with CardTransactionMap changes in Stable branch
-- 2020-01-30 JEL Removed update to card exipry as we now shred and store this in the script (now storing Token Expiy) 
-- =============================================
CREATE PROCEDURE [dbo].[Billing_WorldPay_UpdateWithResponse]
	 @CardTransactionID			INT
	,@ErrorCode					VARCHAR (100)
	,@ErrorMessage				VARCHAR (500)
	,@TransactionID				VARCHAR (50)
	,@AuthCode					VARCHAR (250)
	,@StatusMsg					VARCHAR (250)
	,@ClientPaymentGatewayID	INT
	,@MerchantCode				VARCHAR (200) 
	,@JournalType				VARCHAR (100) 
	,@MaskedCardNumber			VARCHAR (200)
	,@ExpiryDate				DATE
	,@ExpiryMonth				VARCHAR (2)
	,@ExpiryYear				VARCHAR (4)
	,@Amount					DECIMAL (18,2) 
AS
BEGIN

    SET NOCOUNT ON;
       
    DECLARE	 @AccountID							INT
			,@PaymentID							INT
			,@CustomerPaymentScheduleID			INT
			,@CustomerLedgerID					INT
			,@RunAsUserID						INT = dbo.fn_C600_GetAqAutomationUser()
			,@EventToAdd						INT
			,@ClientID							INT = dbo.fnGetPrimaryClientID()
			,@CaseID							INT
			,@LeadEventID						INT
			,@EventTypeID						INT
			,@LeadID							INT
			,@DateForEvent						DATETIME = dbo.fn_GetDate_Local()
			,@CustomerID						INT
			,@MatterID							INT
			,@PolicyMatterID					INT
			,@Description						VARCHAR(200)
			,@LogEntry							VARCHAR (MAX)
			,@AmountNet							DECIMAL(18,2)
			,@AmountVat							DECIMAL (18,2)
			,@PurchasedProductID				INT
			,@PurchasedProductPaymentScheduleID INT 
			,@EventComments						VARCHAR(2000) = ''
			,@LogID								INT
			,@RowCount							INT
			,@LeadTypeID						INT

	DECLARE @FailedPPPS	TABLE (PPPSID INT, PurchasedProductID INT)
	DECLARE @PetMatters	TABLE (MatterID INT, RetryPrepared BIT DEFAULT (0) )
                     
    DECLARE @InsertedCPS TABLE (CustomerPaymentScheduleID INT, PurchasedProductPaymentScheduleID INT)
                     
    SELECT @LogEntry =  ' CardTransactionID '		+ CAST(ISNULL(@CardTransactionID		,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' Error Code '				+ CAST(ISNULL(@ErrorCode				,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' Error Message '			+ CAST(ISNULL(@ErrorMessage				,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' TransactionID '			+ CAST(ISNULL(@TransactionID			,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' AuthCode '				+ CAST(ISNULL(@AuthCode					,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' StatusMsg '				+ CAST(ISNULL(@StatusMsg				,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' ClientPaymentGateWayID '	+ CAST(ISNULL(@ClientPaymentGatewayID	,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' MerchantCode '			+ CAST(ISNULL(@MerchantCode				,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' JournalType '				+ CAST(ISNULL(@JournalType				,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' MaskedCardNumber '		+ CAST(ISNULL(@MaskedCardNumber			,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' ExpiryDate '				+ CAST(ISNULL(@ExpiryDate				,'' ) as VARCHAR) + CHAR(13)+CHAR(10) +
						' ExpiryMonth '				+ CAST(ISNULL(@ExpiryMonth				,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' ExpiryYear '				+ CAST(ISNULL(@ExpiryYear				,'0') as VARCHAR) + CHAR(13)+CHAR(10) +
						' Amount  '					+ CAST(ISNULL(@Amount					,'0') as VARCHAR)
						  
	EXEC @LogID = _C00_LogIt  'PROC', 'CardTransaction_UpdateWithResponse', 'CardTransaction_UpdateWithResponse Values',@LogEntry,58552 /*Aquarium Automation*/
		  
	SELECT @EventComments += 'LogID ' + ISNULL(CONVERT(VARCHAR,@LogID),'NULL') + CHAR(13)+CHAR(10) + @LogEntry

		   
	IF @JournalType IN ('REFUNDED','SENT_FOR_REFUND','REFUND_FAILED','REFUNDED_BY_MERCHANT')
	BEGIN 
			 
		/*If it's a refund then the card transaction it comes in on will be in the parentID of the actual record
		Select out the actual CTID to use for the rest of it*/ 
		SELECT TOP 1 @CardTransactionID = c.CardTransactionID,  @CustomerID = c.CustomerID, @Amount = c.Amount, @CustomerPaymentScheduleID = c.CustomerPaymentScheduleID, @MatterID = c.ObjectID ,@PurchasedProductID = c.ParentID , @AccountID = c.AccountID
		FROM CardTransaction c WITH ( NOLOCK ) 
		WHERE c.ParentID = @CardTransactionID -- 2020-01-14 CPS for JIRA SDLPC-94 | Changed from c.OrderID to c.ParentID to line up with CardTransactionMap changes in Stable branch.
		ORDER BY c.CardTransactionID DESC 
				
	END
	ELSE
	BEGIN
		
		SELECT @CustomerID = c.CustomerID, @Amount = c.Amount, @CustomerPaymentScheduleID = c.CustomerPaymentScheduleID, @MatterID = c.ObjectID , @PurchasedProductID = c.ParentID, @AccountID = c.AccountID
		FROM CardTransaction c WITH ( NOLOCK ) 
		WHERE c.CardTransactionID = @CardTransactionID		
		
	END

				
	/*If possible, we want to match on the CustomerPaymentScheduleID, if this is NULL on the CT record, then pick up from the customer ID and Amount*/ 
	IF ISNULL(@CustomerPaymentScheduleID,0) <> 0 
	BEGIN
	    
		SELECT	 @CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
				,@AccountID					= cps.AccountID
				,@CaseID					= m.CaseID
				,@LeadID					= m.LeadID
				,@ClientID					= m.ClientID
				,@AmountNet					= cps.PaymentNet
				,@AmountVat					= cps.PaymentVAT
				,@LeadTypeID				= l.LeadTypeID -- 2019-12-09 CPS for JIRA LPC-205 | Include LeadTypeID so we can filter CardReturnProcessing by LeadType
		FROM CustomerPaymentSchedule cps WITH ( NOLOCK ) 
		INNER JOIN Account a WITH ( NOLOCK ) on a.AccountID = cps.AccountID
		INNER JOIN Matter m WITH ( NOLOCK ) on m.MatterID = a.ObjectID
		INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = m.LeadID
		WHERE cps.CustomerID = @CustomerID 
		AND cps.PaymentGross = @Amount
		AND cps.PaymentStatusID NOT IN (4,6) /*Not failed or Paid*/ 			
		AND cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 
	    
	END
	ELSE
	BEGIN

		SELECT	 @CustomerPaymentScheduleID = cps.CustomerPaymentScheduleID
				,@AccountID					= cps.AccountID
				,@CaseID					= m.CaseID
				,@LeadID					= m.LeadID
				,@ClientID					= m.ClientID
				,@AmountNet					= cps.PaymentNet
				,@AmountVat					= cps.PaymentVAT
				,@LeadTypeID				= l.LeadTypeID -- 2019-12-09 CPS for JIRA LPC-205 | Include LeadTypeID so we can filter CardReturnProcessing by LeadType
		FROM CustomerPaymentSchedule cps WITH ( NOLOCK ) 
		INNER JOIN Account a WITH ( NOLOCK ) on a.AccountID = cps.AccountID
		INNER JOIN Matter m WITH ( NOLOCK ) on m.MatterID = a.ObjectID
		INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = m.LeadID
		WHERE cps.CustomerID = @CustomerID 
		AND cps.PaymentGross = @Amount
		AND cps.PaymentStatusID NOT IN (4,6) /*Not failed or Paid*/ 
	    
	END
	
	PRINT '@CustomerPaymentScheduleID = ' + ISNULL(CONVERT(VARCHAR,@CustomerPaymentScheduleID),'NULL')
		
	/*We will get several notifications, we only want to process the final ones*/ 
	IF @JournalType NOT IN ('AUTHORISED','SENT_FOR_AUTHORISATION') 
	BEGIN
		PRINT '@JournalType NOT IN (''AUTHORISED'',''SENT_FOR_AUTHORISATION'') '
		-- 2019-10-21 CPS for JIRA LPC-28 | Removed C600-specific renewal rules
	   
		UPDATE CardTransaction
		SET ErrorCode = @JournalType,
				ErrorMessage = @ErrorMessage,
				TransactionID = @TransactionID,
				AuthCode = @AuthCode,
				StatusMsg = @StatusMsg,
				CustomerPaymentScheduleID = @CustomerPaymentScheduleID,
				MerchantCode = @MerchantCode              
		WHERE CardTransactionID=@CardTransactionID          
	       
		SELECT @PaymentID = p.PaymentID
		FROM Payment p 
		WHERE p.CardTransactionID = @CardTransactionID
	       
		UPDATE Payment 
		SET CustomerPaymentScheduleID = @CustomerPaymentScheduleID 
		WHERE PaymentID = @PaymentID
		   
		SELECT @CustomerLedgerID = cl.CustomerLedgerID 
		FROM CustomerLedger cl WITH ( NOLOCK ) 
		WHERE cl.PaymentID = @PaymentID
	          
		/*A success will have no error code and a type of Captured*/ 
		IF  @JournalType IN ('CAPTURED','SENT_FOR_REFUND')
		BEGIN
			PRINT '@JournalType IN (''CAPTURED'',''SENT_FOR_REFUND'')'
			-- has a transaction id and an account id so update card token with transaction id
			-- We're pretty sure the captured would not have a token, but the information so far has been poor so belt and braces
			IF @TransactionID IS NOT NULL AND LEN(@TransactionID)>0 AND @AccountID>0
			BEGIN 
				
				UPDATE Account
				SET CardToken = @TransactionID
				WHERE AccountID=@AccountID
					
				UPDATE CardTransaction
				SET AccountID = @AccountID
				Where CardTransactionID = @CardTransactionID
				
			END
				
			SELECT	 @EventToAdd = c.EventToApply
					,@RunAsUserID = c.AddEventAs
			FROM CardReturnProcessing c WITH (NOLOCK) 
			INNER JOIN EventType et WITH (NOLOCK) on et.EventTypeID = c.EventToApply
			WHERE c.FailureCode IN ('-1', @JournalType) /*-1 is a fallback event...*/
			AND c.ClientID IN (0, @ClientID)
			AND c.ReasonCodeType = @ClientPaymentGatewayID
			AND et.LeadTypeID = @LeadTypeID -- 2019-12-09 CPS for JIRA LPC-205 | Include LeadTypeID so we can filter CardReturnProcessing by LeadType
			ORDER BY c.ClientID DESC, c.FailureCode DESC
						
			UPDATE PurchasedProductPaymentSchedule 
			Set PaymentStatusID = 6,CustomerLedgerID = @CustomerLedgerID, ReconciledDate = dbo.fn_GetDate_Local()
			From PurchasedProductPaymentSchedule 
			WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID
			-- 2019-12-09 CPS for JIRA LPC-181 | CustomerLedgerID is NULL filter removed following discussion with JEL
		
			UPDATE CustomerPaymentSchedule  
			Set PaymentStatusID = 6, CustomerLedgerID = @CustomerLedgerID, ReconciledDate = dbo.fn_GetDate_Local()
			From CustomerPaymentSchedule 
			WHERE CustomerPaymentScheduleID = @CustomerPaymentScheduleID 
			-- 2019-12-09 CPS for JIRA LPC-181 | CustomerLedgerID is NULL filter removed following discussion with JEL
				
			SELECT @Description += LEFT(p.ProductDescription,CHARINDEX('(',p.ProductDescription,1)-1) + ' ' + pt.PurchasedProductPaymentScheduleTypeName + ' '
			FROM PurchasedProductPaymentSchedule pp WITH ( NOLOCK ) 
			INNER JOIN PurchasedProduct p WITH ( NOLOCK ) on p.PurchasedProductID = pp.PurchasedProductID 
			INNER JOIN PurchasedProductPaymentScheduleType pt WITH ( NOLOCK ) on pt.PurchasedProductPaymentScheduleTypeID = pp.PurchasedProductPaymentScheduleTypeID
			WHERE pp.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 
												
			UPDATE CustomerLedger 
			SET TransactionDescription = @Description
			WHERE CustomerLedgerID = @CustomerLedgerID 
			
			UPDATE p 
			SET p.PaymentStatusID = 6,
					p.FailureReason = @ErrorMessage, 
					p.WhenModified = dbo.fn_GetDate_Local(), 
					p.WhoModified = @RunAsUserID
			FROM Payment p 
			WHERE p.PaymentID = @PaymentID

			/*2018-06-11 ACE - Set next payment date and amount as required..*/
			EXEC dbo.Account__SetDateAndAmountOfNextPayment @AccountID
	       
		END

		IF @JournalType IN ('REFUSED','ERROR','CANCELLED','EXPIRED','REFUND_FAILED') 
		BEGIN			  
			PRINT '@JournalType IN (''REFUSED'',''ERROR'',''CANCELLED'',''EXPIRED'',''REFUND_FAILED'') '
			SELECT @EventToAdd = c.EventToApply, @RunAsUserID = c.AddEventAs
			FROM CardReturnProcessing c 
			INNER JOIN EventType et WITH ( NOLOCK ) on et.EventTypeID = c.EventToApply
			WHERE c.FailureCode IN ('-1', @JournalType) /*-1 is a fallback event...*/
			AND c.ClientID IN (0, @ClientID)
			AND c.ReasonCodeType = @ClientPaymentGatewayID
			AND et.LeadTypeID = @LeadTypeID -- 2019-12-09 CPS for JIRA LPC-205 | Include LeadTypeID so we can filter CardReturnProcessing by LeadType
			ORDER BY c.ClientID DESC, c.FailureCode DESC
				
			/*So not ok, this means we have a failure. Time to do the fail thing...*/
			UPDATE p 
			SET p.PaymentStatusID = CASE p.PaymentStatusID WHEN 5 THEN 9 ELSE 4 END, /*JEL if it's a retry, new status is retry failed, otherwise just failed*/
					p.FailureReason = @ErrorMessage, 
					p.WhenModified = dbo.fn_GetDate_Local(), 
					p.WhoModified = @RunAsUserID
			FROM Payment p 
			WHERE p.PaymentID = @PaymentID

			INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
			SELECT p.ClientID, p.CustomerID, PaymentDateTime, p.FailureCode, p.FailureReason, CAST(dbo.fn_GetDate_Local() AS DATE), p.CardTransactionID, 'Failure notification ' + p.FailureReason, -p.PaymentNet, -p.PaymentVAT, -p.PaymentGross, NULL, NULL, NULL, @PaymentID, NULL, 58552 /*@AqAutomation*/, dbo.fn_GetDate_Local() 
			FROM Payment p WITH (NOLOCK) 
			WHERE p.PaymentID=@PaymentID
	            
			SELECT @CustomerLedgerID = SCOPE_IDENTITY()

			-- reverse customerpaymentschedule, reverse payment status
			UPDATE cps  
			SET PaymentStatusID=4, ReconciledDate=dbo.fn_GetDate_Local()
			FROM CustomerPaymentSchedule cps
			INNER JOIN Payment p WITH (NOLOCK) ON cps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
			WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 

			-- purchasedproductpaymentschedule, reverse payment status
			UPDATE ppps  
			SET PaymentStatusID=4, ReconciledDate=dbo.fn_GetDate_Local(), ContraCustomerLedgerID = @CustomerLedgerID
				output inserted.PurchasedProductPaymentScheduleID, inserted.PurchasedProductID
				into @FailedPPPS (PPPSID, PurchasedProductID)
			FROM PurchasedProductPaymentSchedule ppps
			INNER JOIN Payment p WITH (NOLOCK) ON ppps.CustomerPaymentScheduleID=p.CustomerPaymentScheduleID
			WHERE ppps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID 

			-- 2019-10-21 CPS for JIRA LPC-28 | Occasionally we will see a failure response at first attempt then a subsequent success.  In this scenario, the success needs to counter the reversal row created by the opening failure.
			INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
			SELECT TOP 1 ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet * -1, TransactionVAT * -1, TransactionGross * -1, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, @RunAsUserID, dbo.fn_GetDate_Local()
			FROM CustomerLedger cl WITH (NOLOCK) 
			WHERE cl.PaymentID = @PaymentID
			AND cl.FailureCode <> ''
			AND cl.CustomerLedgerID <> @CustomerLedgerID
			AND cl.TransactionGross = @Amount * -1
			ORDER BY cl.CustomerLedgerID DESC
			/*!! NO SQL HERE !!*/
			IF @@RowCount > 0
			BEGIN
				SELECT @EventComments += ' New CustomerLedgerID = ' + ISNULL(CONVERT(VARCHAR,SCOPE_IDENTITY()),'NULL') + ' created to undo existing Reversal row.' + CHAR(13)+CHAR(10)
			END

			EXEC @RowCount = CardTransaction__PrepareThirdPartyFieldsFromCardTransaction @CardTransactionID, @RunAsUserID

			SELECT @EventComments += ISNULL(CONVERT(VARCHAR,@RowCount),'NULL') + ' Card Transaction retry fields updated.' + CHAR(13)+CHAR(10)

		END

		/*Add required event*/
		IF @EventToAdd > 0 /*And make sure we havent got one in the last hour..*/
		BEGIN

			/*No Instruction. Do the mandate thing...*/
			SELECT TOP 1 @LeadEventID = c.LatestNonNoteLeadEventID, @EventTypeID = le.EventTypeID
			FROM Cases c WITH (NOLOCK)
			INNER JOIN LeadEvent le WITH (NOLOCK) ON le.LeadEventID = c.LatestNonNoteLeadEventID
			WHERE c.CaseID = @CaseID

			IF NOT EXISTS (
				SELECT *
				FROM LeadEvent le WITH (NOLOCK)
				WHERE le.CaseID = @CaseID 
				AND le.EventTypeID = @EventToAdd
				AND le.WhenCreated > DATEADD(HH, -1, dbo.fn_GetDate_Local())
			)
			BEGIN
	                        
				PRINT 'LE'
				PRINT @LeadEventID
				PRINT 'Event'
				PRINT @EventToAdd
				PRINT 'USER'
				PRINT @RunAsUserID 
				PRINT 'Case'
				PRINT @CaseID 
	                        
				-- 2019-10-21 CPS for JIRA LPC-28 | Include useful comments when applying the event
				PRINT 'Queueing EventTypeID ' + ISNULL(CONVERT(VARCHAR,@EventToAdd),'NULL') + ' to CaseID ' + ISNULL(CONVERT(VARCHAR,@CaseID),'NULL')
				EXEC dbo.EventTypeAutomatedEvent__CreateAutomatedEventQueueFull @ClientID, @LeadID, @CaseID, @LeadEventID, @EventTypeID, @EventToAdd, -1, @DateForEvent, @RunAsUserID, @EventComments

			END
	                  
		END    
	END
	
	/*If this is the Authorisation then we want to captuire the CTID, Token etc. We will pick this up in the next hit for the captured*/ 
	IF @JournalType IN ('AUTHORISED','SENT_FOR_AUTHORISATION') 
	BEGIN 
		PRINT '@JournalType IN (''AUTHORISED'',''SENT_FOR_AUTHORISATION'') '
		UPDATE CardTransaction
		SET ErrorCode = @JournalType,
		ErrorMessage = @ErrorMessage,
		TransactionID = @TransactionID,
		AuthCode = @AuthCode,
		StatusMsg = @StatusMsg,
		CustomerPaymentScheduleID = @CustomerPaymentScheduleID,
		MerchantCode = @MerchantCode              
		WHERE CardTransactionID=@CardTransactionID   
		AND (
			  ErrorCode NOT IN ( 'CAPTURED','SETTLED_BY_MERCHANT','SENT_FOR_REFUND','REFUSED','ERROR','CANCELLED','EXPIRED','REFUND_FAILED','SETTLED')
			OR
			  ErrorCode is NULL -- 2019-12-06 CPS for JIRA LPC-202 | Stop blocking CardTransaction update when the ErrorCode is NULL
			)
		
		PRINT @@RowCount

		IF ISNULL(@TransactionID,'') <> ''
		BEGIN  
		
			UPDATE Account
			SET CardToken = @TransactionID
			WHERE AccountID=@AccountID
			
		END 
		
		IF ISNULL(@MaskedCardNumber,'') <> ''
		BEGIN  
		
			UPDATE Account
			SET MaskedCardNumber = @MaskedCardNumber
			WHERE AccountID=@AccountID
			
		END
		
		 
		/*JEL 2020-01-30 removed as we now capture the token expiry in the notification parser script and update directly*/ 
		--IF ISNULL(@ExpiryDate,'') <> ''
		--BEGIN  
		
		--	UPDATE Account
		--	SET ExpiryDate = @ExpiryDate, 
		--		ExpiryMonth = @ExpiryMonth, 
		--		ExpiryYear = @ExpiryYear 
		--	WHERE AccountID=@AccountID
			
		--END 
	
		-- 2019-12-09 CPS for JIRA LPC-181 | Trigger the card payer process once a positive notification comes back
		DECLARE @NextRunDateTime DATETIME = DATEADD(MINUTE,1,dbo.fn_GetDate_Local())
		EXEC AutomatedTask__RunNow 19716, 0, @NextRunDateTime, 0 /*CC Payment*/
		
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing_WorldPay_UpdateWithResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing_WorldPay_UpdateWithResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing_WorldPay_UpdateWithResponse] TO [sp_executeall]
GO
