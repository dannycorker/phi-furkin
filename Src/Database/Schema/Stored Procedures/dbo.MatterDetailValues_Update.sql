SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the MatterDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterDetailValues_Update]
(

	@MatterDetailValueID int   ,

	@ClientID int   ,

	@LeadID int   ,

	@MatterID int   ,

	@DetailFieldID int   ,

	@DetailValue varchar (2000)  ,

	@ErrorMsg varchar (1000)  ,

	@OriginalDetailValueID int   ,

	@OriginalLeadID int   ,

	@EncryptedValue varchar (3000)  ,

	@ValueInt int   ,

	@ValueMoney money   ,

	@ValueDate date   ,

	@ValueDateTime datetime2   ,

	@SourceID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[MatterDetailValues]
				SET
					[ClientID] = @ClientID
					,[LeadID] = @LeadID
					,[MatterID] = @MatterID
					,[DetailFieldID] = @DetailFieldID
					,[DetailValue] = @DetailValue
					,[ErrorMsg] = @ErrorMsg
					,[OriginalDetailValueID] = @OriginalDetailValueID
					,[OriginalLeadID] = @OriginalLeadID
					,[EncryptedValue] = @EncryptedValue
					,[ValueInt] = @ValueInt
					,[ValueMoney] = @ValueMoney
					,[ValueDate] = @ValueDate
					,[ValueDateTime] = @ValueDateTime
					,[SourceID] = @SourceID
				WHERE
[MatterDetailValueID] = @MatterDetailValueID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterDetailValues_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues_Update] TO [sp_executeall]
GO
