SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LookupList table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupList_GetByLookupListIDClientID]
(

	@LookupListID int   ,

	@ClientID int   
)
AS


				SELECT
					[LookupListID],
					[LookupListName],
					[LookupListDescription],
					[ClientID],
					[Enabled],
					[SortOptionID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[LeadTypeID],
					[IsShared]
				FROM
					dbo.fnLookupListShared(@ClientID)
				WHERE
										[LookupListID] = @LookupListID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList_GetByLookupListIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupList_GetByLookupListIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList_GetByLookupListIDClientID] TO [sp_executeall]
GO
