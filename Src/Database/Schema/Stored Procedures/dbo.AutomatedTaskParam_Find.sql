SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the AutomatedTaskParam table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTaskParam_Find]
(

	@SearchUsingOR bit   = null ,

	@AutomatedTaskParamID int   = null ,

	@TaskID int   = null ,

	@ClientID int   = null ,

	@ParamName varchar (50)  = null ,

	@ParamValue varchar (2000)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AutomatedTaskParamID]
	, [TaskID]
	, [ClientID]
	, [ParamName]
	, [ParamValue]
    FROM
	[dbo].[AutomatedTaskParam] WITH (NOLOCK) 
    WHERE 
	 ([AutomatedTaskParamID] = @AutomatedTaskParamID OR @AutomatedTaskParamID IS NULL)
	AND ([TaskID] = @TaskID OR @TaskID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ParamName] = @ParamName OR @ParamName IS NULL)
	AND ([ParamValue] = @ParamValue OR @ParamValue IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AutomatedTaskParamID]
	, [TaskID]
	, [ClientID]
	, [ParamName]
	, [ParamValue]
    FROM
	[dbo].[AutomatedTaskParam] WITH (NOLOCK) 
    WHERE 
	 ([AutomatedTaskParamID] = @AutomatedTaskParamID AND @AutomatedTaskParamID is not null)
	OR ([TaskID] = @TaskID AND @TaskID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ParamName] = @ParamName AND @ParamName is not null)
	OR ([ParamValue] = @ParamValue AND @ParamValue is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskParam_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_Find] TO [sp_executeall]
GO
