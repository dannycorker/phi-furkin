SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the YellowPagesAreaCodes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[YellowPagesAreaCodes_Insert]
(

	@YellowPagesAreaCodeID int    OUTPUT,

	@YellowPagesAreaCode varchar (10)  ,

	@PostCode varchar (10)  
)
AS


				
				INSERT INTO [dbo].[YellowPagesAreaCodes]
					(
					[YellowPagesAreaCode]
					,[PostCode]
					)
				VALUES
					(
					@YellowPagesAreaCode
					,@PostCode
					)
				-- Get the identity value
				SET @YellowPagesAreaCodeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[YellowPagesAreaCodes_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[YellowPagesAreaCodes_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[YellowPagesAreaCodes_Insert] TO [sp_executeall]
GO
