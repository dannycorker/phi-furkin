SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Lead table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Lead_GetByLeadRef]
(

	@LeadRef varchar (100)  
)
AS


				SELECT
					[LeadID],
					[ClientID],
					[LeadRef],
					[CustomerID],
					[LeadTypeID],
					[AquariumStatusID],
					[ClientStatusID],
					[BrandNew],
					[Assigned],
					[AssignedTo],
					[AssignedBy],
					[AssignedDate],
					[RecalculateEquations],
					[Password],
					[Salt],
					[WhenCreated]
				FROM
					[dbo].[Lead] WITH (NOLOCK) 
				WHERE
										[LeadRef] = @LeadRef
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_GetByLeadRef] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead_GetByLeadRef] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_GetByLeadRef] TO [sp_executeall]
GO
