SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 05-06-2013
-- Description:	Gets a list of event type equations by event type
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeEquation__GetByEventTypeIDIncludeFieldName]

	@EventTypeID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT df.FieldName, ete.* FROM EventTypeEquation ete WITH (NOLOCK) 
	INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = ete.DetailFieldID
	WHERE EventTypeID=@EventTypeID


END



GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeEquation__GetByEventTypeIDIncludeFieldName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeEquation__GetByEventTypeIDIncludeFieldName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeEquation__GetByEventTypeIDIncludeFieldName] TO [sp_executeall]
GO
