SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the TemplateType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TemplateType_Get_List]

AS


				
				SELECT
					[TemplateTypeID],
					[TemplateTypeName]
				FROM
					[dbo].[TemplateType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TemplateType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TemplateType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TemplateType_Get_List] TO [sp_executeall]
GO
