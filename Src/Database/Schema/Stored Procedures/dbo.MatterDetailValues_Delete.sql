SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the MatterDetailValues table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterDetailValues_Delete]
(

	@MatterDetailValueID int   
)
AS


				DELETE FROM [dbo].[MatterDetailValues] WITH (ROWLOCK) 
				WHERE
					[MatterDetailValueID] = @MatterDetailValueID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterDetailValues_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues_Delete] TO [sp_executeall]
GO
