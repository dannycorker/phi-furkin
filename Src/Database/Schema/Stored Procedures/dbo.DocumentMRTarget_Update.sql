SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DocumentMRTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentMRTarget_Update]
(

	@DocumentMRTargetID int   ,

	@ClientID int   ,

	@DocumentTypeID int   ,

	@Target varchar (250)  ,

	@DetailFieldID int   ,

	@ColumnField int   ,

	@TemplateTypeID int   ,

	@DetailFieldAlias varchar (500)  ,

	@ColumnFieldAlias varchar (500)  ,

	@ObjectName varchar (250)  ,

	@PropertyName varchar (250)  ,

	@Notes varchar (250)  ,

	@DocumentTypeVersionID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DocumentMRTarget]
				SET
					[ClientID] = @ClientID
					,[DocumentTypeID] = @DocumentTypeID
					,[Target] = @Target
					,[DetailFieldID] = @DetailFieldID
					,[ColumnField] = @ColumnField
					,[TemplateTypeID] = @TemplateTypeID
					,[DetailFieldAlias] = @DetailFieldAlias
					,[ColumnFieldAlias] = @ColumnFieldAlias
					,[ObjectName] = @ObjectName
					,[PropertyName] = @PropertyName
					,[Notes] = @Notes
					,[DocumentTypeVersionID] = @DocumentTypeVersionID
				WHERE
[DocumentMRTargetID] = @DocumentMRTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentMRTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentMRTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentMRTarget_Update] TO [sp_executeall]
GO
