SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Location table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Location_Get_List]

AS


				
				SELECT
					[LocationID],
					[ClientID],
					[LocationName],
					[Note],
					[Enabled],
					[Deleted],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[Location] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Location_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Location_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Location_Get_List] TO [sp_executeall]
GO
