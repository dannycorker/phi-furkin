SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Cases table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Cases_GetByLeadIDProcessStartLeadEventID]
(

	@LeadID int   ,

	@ProcessStartLeadEventID int   
)
AS


				SELECT
					[CaseID],
					[LeadID],
					[ClientID],
					[CaseNum],
					[CaseRef],
					[ClientStatusID],
					[AquariumStatusID],
					[DefaultContactID],
					[LatestLeadEventID],
					[LatestInProcessLeadEventID],
					[LatestOutOfProcessLeadEventID],
					[LatestNonNoteLeadEventID],
					[LatestNoteLeadEventID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[ProcessStartLeadEventID]
				FROM
					[dbo].[Cases] WITH (NOLOCK) 
				WHERE
										[LeadID] = @LeadID
					AND [ProcessStartLeadEventID] = @ProcessStartLeadEventID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Cases_GetByLeadIDProcessStartLeadEventID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Cases_GetByLeadIDProcessStartLeadEventID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Cases_GetByLeadIDProcessStartLeadEventID] TO [sp_executeall]
GO
