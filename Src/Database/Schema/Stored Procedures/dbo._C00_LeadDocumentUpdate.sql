SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-12-18
-- Description:	Replace a LeadDocument with a file on the A1 file system
-- =============================================
CREATE PROCEDURE [dbo].[_C00_LeadDocumentUpdate] 
	@LeadEventID INT,
	@WhoUploaded INT = NULL, 
	@FileName VARCHAR(255) = 'C:\Shares\Files\YourFileName.jpg' 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--RAISERROR( 'Do not use until Jim gives the ok!', 16, 1 )
	DECLARE @MyDynamicSQL VARCHAR(2000)

	SELECT @MyDynamicSQL = 
	'DECLARE 
	@LeadDocumentTitle VARCHAR(1000),
	@LeadDocumentID INT  ,
	@ClientID INT ,
	@LeadID INT   ,
	@DocumentTypeID INT = NULL ,
	@UploadDateTime DATETIME = dbo.fn_GetDate_Local()  ,
	@DocumentBLOB VARBINARY(MAX)   ,
	@EmailBLOB VARBINARY(MAX) = NULL  ,
	@DocumentFormat VARCHAR (24),
	@EmailFrom VARCHAR (512) = NULL  ,
	@EmailTo VARCHAR (MAX) = NULL  ,
	@CcList VARCHAR (MAX) = NULL  ,
	@BccList VARCHAR (MAX) = NULL  ,
	@ElectronicSignatureDocumentKey VARCHAR (50) = NULL  ,
	@Encoding VARCHAR (50) = NULL  ,
	@ContentFormat VARCHAR (50),
	@ZipFormat VARCHAR (50) = NULL ,
	
	@LeadEventID INT,
	@WhoUploaded INT, 
	@FileName VARCHAR(255)
	
	SELECT	@LeadDocumentTitle = fs.LeadDocumentTitle, 
			@WhoUploaded = ISNULL(' + convert(varchar,@WhoUploaded) + ',fs.WhoUploaded), 
			@LeadID = le.LeadID, 
			@ClientID = le.ClientID,
			@DocumentFormat = fs.DocumentFormat,
			@ContentFormat = fs.ContentFormat
	FROM LeadEvent le WITH (NOLOCK) 
	INNER JOIN LeadDocumentFS fs WITH (NOLOCK) on fs.LeadDocumentID = le.LeadDocumentID
	WHERE le.LeadEventID = ' + convert(varchar,@LeadEventID) + '
	
	
		SELECT @DocumentBLOB = CAST(Document.bulkcolumn AS VARBINARY(MAX))
		FROM OPENROWSET(
			BULK
			''' + @FileName + ''',
			SINGLE_BLOB ) AS DOCUMENT

		EXECUTE dbo.LeadDocument_Insert 
		   @LeadDocumentID OUTPUT
		  ,@ClientID
		  ,@LeadID
		  ,@DocumentTypeID
		  ,@LeadDocumentTitle
		  ,@UploadDateTime
		  ,@WhoUploaded
		  ,@DocumentBLOB
		  ,@FileName
		  ,@EmailBLOB
		  ,@DocumentFormat
		  ,@EmailFrom
		  ,@EmailTo
		  ,@CcList
		  ,@BccList
		  ,@ElectronicSignatureDocumentKey
		  ,@Encoding
		  ,@ContentFormat
		  ,@ZipFormat
		  
		UPDATE le
		SET LeadDocumentID = @LeadDocumentID, Comments = left(Comments + '' document updated from LDID '' + convert(varchar,isnull(le.LeadDocumentID,-1)),2000)
		FROM LeadEvent le 
		WHERE le.LeadEventID = ' + convert(varchar,@LeadEventID) + ''
	
	EXEC(@MyDynamicSQL)
	--print @MyDynamicSQL
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LeadDocumentUpdate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_LeadDocumentUpdate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LeadDocumentUpdate] TO [sp_executeall]
GO
