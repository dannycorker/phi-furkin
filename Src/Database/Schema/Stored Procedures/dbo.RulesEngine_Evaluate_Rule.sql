SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-05-31
-- Description:	Evaluates a rule based on a set of inputs
-- UPDATES:		2014-10-06	SB	Fixed bug with 'Any Value'.  Added TDV params.
--				2014-11-25	SB	Fixed bug with the returned ruleset evaluation XML
--				2014-11-26	SB	Added doesn't start with, end with and contains
--				2014-12-01	SB	Fixed XPATH to get the last rule
--				2014-12-01	SB	Fixed invalid cast on empty strings... set to null first
--				2015-03-23	SB	Added cap and collar from VPI / DLG 
--              2015-09-14  ROH Added checkpoints and deltas
--				2016-02-08  PR	Fixed operator evaluation comparison Zen Desk Ticket Number:36634
--				2016-06-15	DCM Fixed empty string comparison (empty EvaluatedValue is now not set to NULL for datatype string) 
--				2016-06-20	DCM Added operators 16 and 17 (Is Blank & Is Not Blank)
--				2016-06-20	DCM Added parameter type 7 ( Rule Input )
--				2016-07-21	DCM Added operator 18 (Is contained in)
-- IS	2016-08-04	Backup
-- SB	2016-08-30	NB... removed SMS survey changes where string is checked with fn_ismoney
-- GPR	2018-09-20	Replaced select block with optimised block from 433 to remove unnecessary joins to Customer, Lead, and Case
-- 2020-01-13 CPS for JIRA LPC-356 | Removed old XML logging
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Evaluate_Rule]
(
	@ID INT,
	@Input VARCHAR(2000) = NULL,
	@CustomerID INT = NULL,
	@LeadID INT = NULL,
	@CaseID INT = NULL,
	@MatterID INT = NULL,
	@Debug BIT = 0,
	@Verbose BIT = 0,
	@Overrides dbo.tvpIntVarcharVarchar READONLY,
	@Output XML = NULL OUTPUT
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @XmlLog XML
	SELECT @XmlLog = 
	(
		SELECT * 
		FROM @Overrides o 
		FOR XML AUTO
	)
	
	IF @ID is NULL
	BEGIN
		SELECT @ID = -1
	END

	DECLARE @TempLogEntry VARCHAR(2000) = ''


	/*GPR 2018-09-20 add block from 433 to replace block commented out below - LIVE issue noticed when matching top1 on stub customers that were without Affinity*/
	IF (@CustomerID IS NULL OR @LeadID IS NULL OR @CaseID IS NULL OR @MatterID IS NULL) AND (@CustomerID > 0 OR @LeadID > 0 OR @CaseID > 0 OR @MatterID > 0)
	BEGIN
		/*CPS 2017-11-13 removed unnecessary joins to Customer, Lead, and Case*/
		SELECT	TOP 1
				@CustomerID = m.CustomerID,
				@LeadID = m.LeadID,
				@CaseID = m.CaseID,
				@MatterID = m.MatterID
		FROM dbo.Matter m WITH (NOLOCK) 
		WHERE (@CustomerID IS NULL OR m.CustomerID = @CustomerID)
		AND (@LeadID IS NULL OR m.LeadID = @LeadID)
		AND (@CaseID IS NULL OR m.CaseID = @CaseID)
		AND (@MatterID IS NULL OR m.MatterID = @MatterID)
		
	END
	
	/*GPR 2018-09-20 commented out - replaced with block above*/
	--SELECT	TOP 1
	--		@CustomerID = c.CustomerID,
	--		@LeadID = l.LeadID,
	--		@CaseID = ca.CaseID,
	--		@MatterID = m.MatterID
	--FROM dbo.Customers c WITH (NOLOCK) 
	--INNER JOIN dbo.Lead l WITH (NOLOCK) ON c.CustomerID = l.CustomerID
	--INNER JOIN dbo.Cases ca WITH (NOLOCK) ON l.LeadID = ca.LeadID
	--INNER JOIN dbo.Matter m WITH (NOLOCK) ON ca.CaseID = m.CaseID
	--WHERE (@CustomerID IS NULL OR c.CustomerID = @CustomerID)
	--AND (@LeadID IS NULL OR l.LeadID = @LeadID)
	--AND (@CaseID IS NULL OR ca.CaseID = @CaseID)
	--AND (@MatterID IS NULL OR m.MatterID = @MatterID)

	SELECT @TempLogEntry = ' @CaseID = ' + ISNULL(CONVERT(VARCHAR,@CaseID),'NULL') 
						 + ',@CustomerID = ' + ISNULL(CONVERT(VARCHAR,@CustomerID),'NULL')
						 + ',@LeadID = ' + ISNULL(CONVERT(VARCHAR,@LeadID),'NULL')
						 + ',@MatterID = ' + ISNULL(CONVERT(VARCHAR,@MatterID),'NULL')
	EXEC _C00_LogIt 'Info', 'RulesEngine_Evaluate_Rule','CustomerDetails', @TempLogEntry, 0

	/* 
	##########
	Collect all the parameters and evaluate
	##########
	*/
	DECLARE @Parameters TABLE
	(
		RuleParameterID INT,
		Name VARCHAR(200),
		ParameterTypeID INT,
		ParameterType VARCHAR(200),
		Value VARCHAR(50),
		DataTypeID INT,
		DataType VARCHAR(50),
		EvaluatedValue VARCHAR(2000),
		Parent VARCHAR(50),
		XmlOutput XML		
	)
	INSERT @Parameters (RuleParameterID, Name, ParameterTypeID, ParameterType, Value, DataTypeID, DataType, Parent)
	SELECT p.RuleParameterID, p.Name, p.ParameterTypeID, pt.Name, p.Value, p.DataTypeID, dt.Name, p.Parent
	FROM dbo.RulesEngine_RuleParameters p WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_ParameterTypes pt WITH (NOLOCK) ON p.ParameterTypeID = pt.ParameterTypeID
	INNER JOIN dbo.RulesEngine_DataTypes dt WITH (NOLOCK) ON p.DataTypeID = dt.TypeID
	WHERE RuleID = @ID

	IF @Debug = 1 AND @Verbose = 1
	BEGIN

		SELECT 'Parameters' AS Data, *
		FROM @Parameters

	END

	-- 1 - Detail Fields
	-- Fix for empty string comparison (all empty EvaluatedValues except for string datatypes are now set to NULL, see below around line 320)
	UPDATE p
	SET EvaluatedValue = COALESCE(cdv.DetailValue, ldv.DetailValue, adv.DetailValue, mdv.DetailValue, NULL)
	FROM @Parameters p
	INNER JOIN dbo.DetailFields f WITH (NOLOCK) ON p.Value = f.DetailFieldID
	LEFT JOIN dbo.CustomerDetailValues cdv WITH (NOLOCK) ON f.DetailFieldID = cdv.DetailFieldID AND f.LeadOrMatter = 10 AND cdv.CustomerID = @CustomerID
	LEFT JOIN dbo.LeadDetailValues ldv WITH (NOLOCK) ON f.DetailFieldID = ldv.DetailFieldID AND f.LeadOrMatter = 1 AND ldv.LeadID = @LeadID
	LEFT JOIN dbo.CaseDetailValues adv WITH (NOLOCK) ON f.DetailFieldID = adv.DetailFieldID AND f.LeadOrMatter = 11 AND adv.CaseID = @CaseID
	LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON f.DetailFieldID = mdv.DetailFieldID AND f.LeadOrMatter = 2 AND mdv.MatterID = @MatterID
	WHERE p.ParameterTypeID = 1

	-- 2 - Customer fields
	UPDATE p
	SET EvaluatedValue = c.RealValue
	FROM @Parameters p
	INNER JOIN dbo.fnUnpivotCustomers(@CustomerID) c ON p.Value = c.RawColumnName
	WHERE p.ParameterTypeID = 2	

	-- 3 - Another rule set
	DECLARE @RuleSets TABLE
	(
		ID INT IDENTITY,
		RuleParameterID INT,
		RuleSetID INT
	)
	
	--	2016-08-04	IS Added Caching to speed up evaluation
	IF OBJECT_ID('tempdb..#TmpEvalRuleSet') IS NULL
	BEGIN
		CREATE TABLE #TmpEvalRuleSet (HashKey VARCHAR(250), XmlOutput XML)  
	END 

	INSERT @RuleSets (RuleParameterID, RuleSetID)
	SELECT p.RuleParameterID, p.Value
	FROM @Parameters p
	WHERE p.ParameterTypeID = 3

	DECLARE @RuleSetCount INT,
			@RuleSetIndex INT = 0

	SELECT @RuleSetCount = COUNT(*) 
	FROM @RuleSets


	WHILE @RuleSetIndex < @RuleSetCount
	BEGIN
		SELECT @RuleSetIndex += 1

		DECLARE @RuleSetID INT
		SELECT @RuleSetID = RuleSetID
		FROM @RuleSets
		WHERE ID = @RuleSetIndex
		
		--	2016-08-04	IS Added Caching to speed up evaluation
		DECLARE @HashKey VARCHAR(250) = (SELECT AnyValue2 FROM @Overrides WHERE AnyValue1='GUID') + '_' +
										ISNULL(CAST(@RuleSetID AS VARCHAR),'') 
		IF @HashKey IS NULL SELECT @HashKey = 
			ISNULL(CAST(@RuleSetID AS VARCHAR),'') +'_'+
			ISNULL(CAST(@CustomerID AS VARCHAR),'') +'_'+
			ISNULL(CAST(@LeadID AS VARCHAR),'') +'_'+
			ISNULL(CAST(@CaseID AS VARCHAR),'') +'_'+
			ISNULL(CAST(@MatterID AS VARCHAR),'')

		DECLARE @Xml XML = NULL
		
		--	2016-08-04	IS Added Caching to speed up evaluation
		IF NOT EXISTS (SELECT HashKey FROM #TmpEvalRuleSet WHERE HashKey = @HashKey)
		BEGIN		
			EXEC dbo.RulesEngine_Evaluate_RuleSet @RuleSetID, @Input, @CustomerID, @LeadID, @CaseID, @MatterID, @Overrides = @Overrides, @Output = @Xml OUTPUT
			
			INSERT INTO #TmpEvalRuleSet VALUES (@HashKey, @Xml)
		END
		
		--	2016-08-04	IS Added Caching to speed up evaluation
		SELECT	@Xml = XmlOutput
		FROM	#TmpEvalRuleSet
		WHERE	HashKey = @HashKey
		
		UPDATE p
		SET EvaluatedValue = (
				SELECT r.c.value('@Output', 'VARCHAR(2000)')
				FROM @Xml.nodes('//Rule[last()]') AS r(c)
			),
			XmlOutput = @Xml
		FROM @Parameters p
		INNER JOIN @RuleSets rs ON p.RuleParameterID = rs.RuleParameterID
		WHERE rs.ID = @RuleSetIndex
		
		--exec _C00_LogItXML  @RuleSetID, @ID, 'RulesEngine_Evaluate_RuleSet_Xml', @Xml 

	END



	-- 4 - A function	
	DECLARE @Functions TABLE
	(
		ID INT IDENTITY,
		RuleParameterID INT,
		DataTypeID INT,
		FunctionText VARCHAR(2000)
	)
	INSERT @Functions (RuleParameterID, DataTypeID, FunctionText)
	SELECT p.RuleParameterID, p.DataTypeID, 
		   CASE p.ParameterTypeID
				WHEN 4 THEN p.Value
				ELSE p.Parent
		   END

	FROM @Parameters p
	WHERE p.ParameterTypeID IN (4, 6)

	DECLARE @FunctionCount INT,
			@FunctionIndex INT = 0

	SELECT @FunctionCount = COUNT(*) 
	FROM @Functions

	WHILE @FunctionIndex < @FunctionCount
	BEGIN

		SELECT @FunctionIndex += 1

		DECLARE @DataTypeID INT,
				@FunctionText VARCHAR(2000)
		SELECT	@DataTypeID = DataTypeID,
				@FunctionText = FunctionText
		FROM @Functions
		WHERE ID = @FunctionIndex

		DECLARE @Sql NVARCHAR(2000),
				@Params NVARCHAR(MAX),
				@Value VARCHAR(2000)

		SELECT	@Sql = 'SELECT @ValueOUT = dbo.' + @FunctionText + '(@CustomerID, @LeadID, @CaseID, @MatterID, @Overrides)',
				@Params = N'@ValueOUT VARCHAR(2000) OUTPUT, @CustomerID INT, @LeadID INT, @CaseID INT, @MatterID INT, @Overrides dbo.tvpIntVarcharVarchar READONLY'	


		IF @DataTypeID = 5 -- DateTime
		BEGIN

			SELECT @Sql = 'SELECT @ValueOUT = CONVERT(VARCHAR, ' + @FunctionText + ', 120)'

		END

		EXEC sp_executesql @Sql, @Params, @ValueOUT = @Value OUTPUT, @CustomerID = @CustomerID, @LeadID = @LeadID, @CaseID = @CaseID, @MatterID = @MatterID, @Overrides = @Overrides
		
		PRINT @FunctionText
		PRINT @Value
		
		IF @FunctionText like '%Sensitivity%'
		BEGIN
			EXEC _C00_LogIt  'Info', 'RulesEngine_Evaluate_Rule Price Sensitivity',@CaseID, @Value, 0
		END

		UPDATE p
		SET EvaluatedValue = @Value
		FROM @Parameters p
		INNER JOIN @Functions f ON p.RuleParameterID = f.RuleParameterID
		WHERE f.ID = @FunctionIndex

	END


	-- 5 - Resource list field
	-- First we get the RLID
	UPDATE p
	SET EvaluatedValue = COALESCE(cdv.DetailValue, ldv.DetailValue, adv.DetailValue, mdv.DetailValue, '')
	FROM @Parameters p
	INNER JOIN dbo.DetailFields f WITH (NOLOCK) ON p.Parent = f.DetailFieldID
	LEFT JOIN dbo.CustomerDetailValues cdv WITH (NOLOCK) ON f.DetailFieldID = cdv.DetailFieldID AND f.LeadOrMatter = 10 AND cdv.CustomerID = @CustomerID
	LEFT JOIN dbo.LeadDetailValues ldv WITH (NOLOCK) ON f.DetailFieldID = ldv.DetailFieldID AND f.LeadOrMatter = 1 AND ldv.LeadID = @LeadID
	LEFT JOIN dbo.CaseDetailValues adv WITH (NOLOCK) ON f.DetailFieldID = adv.DetailFieldID AND f.LeadOrMatter = 11 AND adv.CaseID = @CaseID
	LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON f.DetailFieldID = mdv.DetailFieldID AND f.LeadOrMatter = 2 AND mdv.MatterID = @MatterID
	WHERE p.ParameterTypeID = 5
	-- The RL ID will now be in the evaluated value
	UPDATE p
	SET EvaluatedValue = ISNULL(rdv.DetailValue, '')
	FROM @Parameters p
	LEFT JOIN dbo.ResourceListDetailValues rdv WITH (NOLOCK) ON p.EvaluatedValue = rdv.ResourceListID AND rdv.DetailFieldID = p.Value
	WHERE p.ParameterTypeID = 5


	-- 6 - Table detail value
	-- The TRID will have been populated by the function evaluation above
	-- The TRID will now be in the evaluated value
	UPDATE p
	SET EvaluatedValue = ISNULL(tdv.DetailValue, '')
	FROM @Parameters p
	LEFT JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON p.EvaluatedValue = tdv.TableRowID AND tdv.DetailFieldID = p.Value
	WHERE p.ParameterTypeID = 6

	-- 7 - Rule Input
	UPDATE p
	SET EvaluatedValue = @Input
	FROM @Parameters p
	WHERE p.ParameterTypeID = 7

	-- Overrides
	UPDATE p
	SET EvaluatedValue = o.AnyValue2
	FROM @Parameters p
	INNER JOIN @Overrides o ON p.ParameterTypeID = o.AnyID AND p.Value = o.AnyValue1



	-- Get rid of duplicate matches???

	IF @Debug = 1
	BEGIN

		SELECT 'With Values' AS Data, *
		FROM @Parameters

	END


	/* 
	##########
	Collect all the options
	##########
	*/
	DECLARE @Options TABLE
	(
		ParameterOptionID INT,
		RuleParameterID INT,
		OperatorID INT,
		OperatorText VARCHAR(2000),
		Val1 VARCHAR(50),
		Val2 VARCHAR(50),
		Matched VARCHAR(2000)
	)
	
	
	-- Moved this here as it used to be between the insert and update that is now done as one hit
	-- Empty string params will not cast so set to null for all types other than string
	UPDATE @Parameters
	SET EvaluatedValue = NULL
	WHERE DataTypeID != 4
	AND EvaluatedValue = ''
	

	;WITH LookupList AS 
	(
		SELECT DISTINCT ll.LookupListItemID, ll.ItemValue
		FROM dbo.DetailFields df WITH (NOLOCK) 
		INNER JOIN dbo.LookupListItems ll WITH (NOLOCK) ON df.LookupListID = ll.LookupListID
		INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON df.DetailFieldID = p.Value
		WHERE p.RuleID = @ID
		--AND df.ClientID = @ClientID
		AND p.DataTypeID = 6
	)
	
	
	/* NEW - Insert just the matches */
	
	INSERT INTO @Options (ParameterOptionID, RuleParameterID, OperatorID, Val1, Val2, OperatorText, Matched)
	SELECT	o.ParameterOptionID, o.RuleParameterID, o.OperatorID, o.Val1, o.Val2,
			CASE
				WHEN ll.LookupListItemID IS NOT NULL THEN
						REPLACE(op.OperatorText, '{0}', ll.ItemValue)
				ELSE	REPLACE(REPLACE(op.OperatorText, '{0}', o.Val1), '{1}', ISNULL(o.Val2, ''))
			END AS OperatorText,
			CASE 
				WHEN o.OperatorID = 10 THEN ''
				WHEN o.OperatorID IN (9, 12) THEN ISNULL(o.Val1, '') -- Contains and Sounds Like
				WHEN o.OperatorID = 11 THEN ( -- Contains Any
											SELECT TOP 1 RTRIM(LTRIM(m.AnyValue))
											FROM dbo.fnTableOfValues(p.EvaluatedValue, ' ') v
											INNER JOIN dbo.fnTableOfValuesFromCSV(o.Val1) m ON RTRIM(LTRIM(v.AnyValue)) = RTRIM(LTRIM(m.AnyValue))
											)
				ELSE p.EvaluatedValue
			END AS Matched
	FROM dbo.RulesEngine_ParameterOptions o WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_RuleParameters rp WITH (NOLOCK) ON o.RuleParameterID = rp.RuleParameterID
	INNER JOIN dbo.RulesEngine_Operators op WITH (NOLOCK) ON o.OperatorID = op.OperatorID
	LEFT JOIN LookupList ll ON ll.LookupListItemID =	CASE 
															WHEN rp.DataTypeID = 6 THEN o.Val1
															ELSE NULL
														END
	INNER JOIN @Parameters p ON o.RuleParameterID = p.RuleParameterID
	WHERE
	CASE
		WHEN p.DataTypeID IN (1, 2, 3, 6) THEN	-- decimal, int, bool
			CASE
				WHEN o.OperatorID = '1' AND CAST(p.EvaluatedValue AS DECIMAL(18, 8)) = CAST(o.Val1 AS DECIMAL(18, 8)) THEN 1
				WHEN o.OperatorID = '2' AND CAST(p.EvaluatedValue AS DECIMAL(18, 8)) < CAST(o.Val1 AS DECIMAL(18, 8)) THEN 1
				WHEN o.OperatorID = '3' AND CAST(p.EvaluatedValue AS DECIMAL(18, 8)) <= CAST(o.Val1 AS DECIMAL(18, 8)) THEN 1
				WHEN o.OperatorID = '4' AND CAST(p.EvaluatedValue AS DECIMAL(18, 8)) > CAST(o.Val1 AS DECIMAL(18, 8)) THEN 1
				WHEN o.OperatorID = '5' AND CAST(p.EvaluatedValue AS DECIMAL(18, 8)) >= CAST(o.Val1 AS DECIMAL(18, 8)) THEN 1
				WHEN o.OperatorID = '6' AND CAST(p.EvaluatedValue AS DECIMAL(18, 8)) BETWEEN CAST(o.Val1 AS DECIMAL(18, 8)) AND CAST(o.Val2 AS DECIMAL(18, 8)) THEN 1
				WHEN o.OperatorID = '10' THEN 1
				ELSE 0
			END
		WHEN p.DataTypeID = 4 THEN -- string
			CASE
				WHEN o.OperatorID = '1' AND p.EvaluatedValue = o.Val1 THEN 1
				WHEN o.OperatorID = '2' AND p.EvaluatedValue < o.Val1 THEN 1
				WHEN o.OperatorID = '3' AND p.EvaluatedValue <= o.Val1 + 'z' THEN 1
				WHEN o.OperatorID = '4' AND p.EvaluatedValue > o.Val1 + 'z' THEN 1
				WHEN o.OperatorID = '5' AND p.EvaluatedValue >= o.Val1 THEN 1
				WHEN o.OperatorID = '6' AND p.EvaluatedValue BETWEEN o.Val1 AND o.Val2 + 'z' THEN 1
				WHEN o.OperatorID = '7' AND p.EvaluatedValue LIKE o.Val1 + '%' THEN 1
				WHEN o.OperatorID = '8' AND p.EvaluatedValue LIKE '%' + o.Val1 THEN 1
				WHEN o.OperatorID = '9' AND p.EvaluatedValue LIKE '%' + o.Val1 + '%' THEN 1
				WHEN o.OperatorID = '13' AND p.EvaluatedValue NOT LIKE o.Val1 + '%' THEN 1
				WHEN o.OperatorID = '14' AND p.EvaluatedValue NOT LIKE '%' + o.Val1 THEN 1
				WHEN o.OperatorID = '15' AND p.EvaluatedValue NOT LIKE '%' + o.Val1 + '%' THEN 1
				WHEN o.OperatorID = '11' AND 
					(
					SELECT TOP 1 'match'
					FROM dbo.fnTableOfValues(p.EvaluatedValue, ' ') v
					INNER JOIN dbo.fnTableOfValuesFromCSV(o.Val1) m ON RTRIM(LTRIM(v.AnyValue)) = RTRIM(LTRIM(m.AnyValue))
					) = 'match'
					THEN 1
				WHEN o.OperatorID = '12' AND DIFFERENCE(p.EvaluatedValue, o.Val1) >= CAST(o.Val2 AS INT) THEN 1
				WHEN o.OperatorID = '10' THEN 1
				WHEN o.OperatorID = '16' AND p.EvaluatedValue = '' THEN 1
				WHEN o.OperatorID = '17' AND p.EvaluatedValue <> '' THEN 1
				WHEN o.OperatorID = '18' AND CHARINDEX(p.EvaluatedValue,o.Val1) > 0 THEN 1
				ELSE 0
			END
		WHEN p.DataTypeID = 5 THEN -- date time	
			CASE
				WHEN o.OperatorID = '1' AND CAST(p.EvaluatedValue AS DATETIME) = CAST(o.Val1 AS DATETIME) THEN 1
				WHEN o.OperatorID = '2' AND CAST(p.EvaluatedValue AS DATETIME) < CAST(o.Val1 AS DATETIME) THEN 1
				WHEN o.OperatorID = '3' AND CAST(p.EvaluatedValue AS DATETIME) <= CAST(o.Val1 AS DATETIME) THEN 1
				WHEN o.OperatorID = '4' AND CAST(p.EvaluatedValue AS DATETIME) > CAST(o.Val1 AS DATETIME) THEN 1
				WHEN o.OperatorID = '5' AND CAST(p.EvaluatedValue AS DATETIME) >= CAST(o.Val1 AS DATETIME) THEN 1
				WHEN o.OperatorID = '6' AND CAST(p.EvaluatedValue AS DATETIME) BETWEEN CAST(o.Val1 AS DATETIME) AND CAST(o.Val2 AS DATETIME) THEN 1
				WHEN o.OperatorID = '10' THEN 1
				ELSE 0
			END
		ELSE 0
	END = 1
	AND rp.RuleID = @ID
	
	
	-- Delete all options that do not match
	DELETE @Options
	WHERE Matched IS NULL
	
	
	/* OLD - Insert then update options */

	--INSERT INTO @Options (ParameterOptionID, RuleParameterID, OperatorID, Val1, Val2, OperatorText)
	--SELECT	o.ParameterOptionID, o.RuleParameterID, o.OperatorID, o.Val1, o.Val2,
	--		CASE
	--			WHEN ll.LookupListItemID IS NOT NULL THEN
	--					REPLACE(op.OperatorText, '{0}', ll.ItemValue)
	--			ELSE	REPLACE(REPLACE(op.OperatorText, '{0}', o.Val1), '{1}', ISNULL(o.Val2, ''))
	--		END AS OperatorText
	--FROM dbo.RulesEngine_ParameterOptions o WITH (NOLOCK) 
	--INNER JOIN dbo.RulesEngine_RuleParameters p WITH (NOLOCK) ON o.RuleParameterID = p.RuleParameterID
	--INNER JOIN dbo.RulesEngine_Operators op WITH (NOLOCK) ON o.OperatorID = op.OperatorID
	--LEFT JOIN LookupList ll ON ll.LookupListItemID =	CASE 
	--														WHEN p.DataTypeID = 6 THEN o.Val1
	--														ELSE NULL
	--													END
	--WHERE p.RuleID = @ID

	--IF @Debug = 1 AND @Verbose = 1
	--BEGIN

	--	SELECT 'Options and Params' AS Data, *
	--	FROM @Options o
	--	INNER JOIN @Parameters p ON o.RuleParameterID = p.RuleParameterID

	--END


	---- Empty string params will not cast so set to null for all types other than string
	--UPDATE @Parameters
	--SET EvaluatedValue = NULL
	--WHERE DataTypeID != 4
	--AND EvaluatedValue = ''


	---- We now want to see which options match and for the case of Contains Any we want to find the one that we matched
	--UPDATE o
	--SET o.Matched = CASE 
	--					WHEN o.OperatorID = 10 THEN ''
	--					WHEN o.OperatorID IN (9, 12) THEN ISNULL(o.Val1, '') -- Contains and Sounds Like
	--					WHEN o.OperatorID = 11 THEN ( -- Contains Any
	--												SELECT TOP 1 RTRIM(LTRIM(m.AnyValue))
	--												FROM dbo.fnTableOfValues(p.EvaluatedValue, ' ') v
	--												INNER JOIN dbo.fnTableOfValuesFromCSV(o.Val1) m ON RTRIM(LTRIM(v.AnyValue)) = RTRIM(LTRIM(m.AnyValue))
	--												)
	--					ELSE p.EvaluatedValue
	--				END
	--FROM @Options o
	--INNER JOIN @Parameters p ON o.RuleParameterID = p.RuleParameterID
	--WHERE
	--CASE
	--	WHEN p.DataTypeID IN (1, 2, 3, 6) THEN	-- decimal, int, bool
	--		CASE
	--			WHEN o.OperatorID = '1' AND CAST(p.EvaluatedValue AS DECIMAL(18, 8)) = CAST(o.Val1 AS DECIMAL(18, 8)) THEN 1
	--			WHEN o.OperatorID = '2' AND CAST(p.EvaluatedValue AS DECIMAL(18, 8)) < CAST(o.Val1 AS DECIMAL(18, 8)) THEN 1
	--			WHEN o.OperatorID = '3' AND CAST(p.EvaluatedValue AS DECIMAL(18, 8)) <= CAST(o.Val1 AS DECIMAL(18, 8)) THEN 1
	--			WHEN o.OperatorID = '4' AND CAST(p.EvaluatedValue AS DECIMAL(18, 8)) > CAST(o.Val1 AS DECIMAL(18, 8)) THEN 1
	--			WHEN o.OperatorID = '5' AND CAST(p.EvaluatedValue AS DECIMAL(18, 8)) >= CAST(o.Val1 AS DECIMAL(18, 8)) THEN 1
	--			WHEN o.OperatorID = '6' AND CAST(p.EvaluatedValue AS DECIMAL(18, 8)) BETWEEN CAST(o.Val1 AS DECIMAL(18, 8)) AND CAST(o.Val2 AS DECIMAL(18, 8)) THEN 1
	--			WHEN o.OperatorID = '10' THEN 1
	--			ELSE 0
	--		END
	--	WHEN p.DataTypeID = 4 THEN -- string
	--		CASE
	--			WHEN o.OperatorID = '1' AND p.EvaluatedValue = o.Val1 THEN 1
	--			WHEN o.OperatorID = '2' THEN --AND p.EvaluatedValue < o.Val1 THEN 1
	--				CASE
	--					WHEN dbo.fnIsMoney(p.EvaluatedValue) = 1 AND dbo.fnIsMoney(o.Val1) = 1 THEN
	--						CASE
	--							WHEN CAST(p.EvaluatedValue AS MONEY) < CAST(o.Val1 AS MONEY) THEN 1
	--							ELSE 0
	--						END
	--					ELSE
	--						CASE
	--							WHEN p.EvaluatedValue < o.Val1 THEN 1
	--							ELSE 0
	--						END
	--				END
	--			WHEN o.OperatorID = '3' THEN --AND p.EvaluatedValue <= o.Val1 + 'z' THEN 1
	--				CASE
	--					WHEN dbo.fnIsMoney(p.EvaluatedValue) = 1 AND dbo.fnIsMoney(o.Val1) = 1 THEN
	--						CASE
	--							WHEN CAST(p.EvaluatedValue AS MONEY) <= CAST(o.Val1 AS MONEY) THEN 1
	--							ELSE 0
	--						END
	--					ELSE
	--						CASE
	--							WHEN p.EvaluatedValue <= o.Val1 + 'z' THEN 1
	--							ELSE 0
	--						END
	--				END
	--			WHEN o.OperatorID = '4' THEN --AND p.EvaluatedValue > o.Val1 + 'z' THEN 1
	--				CASE
	--					WHEN dbo.fnIsMoney(p.EvaluatedValue) = 1 AND dbo.fnIsMoney(o.Val1) = 1 THEN
	--						CASE
	--							WHEN CAST(p.EvaluatedValue AS MONEY) > CAST(o.Val1 AS MONEY) THEN 1
	--							ELSE 0
	--						END
	--					ELSE
	--						CASE
	--							WHEN p.EvaluatedValue > o.Val1 THEN 1
	--							ELSE 0
	--						END
	--				END
	--			WHEN o.OperatorID = '5' THEN --AND p.EvaluatedValue >= o.Val1 THEN 1
	--				CASE
	--					WHEN dbo.fnIsMoney(p.EvaluatedValue) = 1 AND dbo.fnIsMoney(o.Val1) = 1 THEN
	--						CASE
	--							WHEN CAST(p.EvaluatedValue AS MONEY) >= CAST(o.Val1 AS MONEY) THEN 1
	--							ELSE 0
	--						END
	--					ELSE
	--						CASE
	--							WHEN p.EvaluatedValue >= o.Val1 + 'z' THEN 1
	--							ELSE 0
	--						END
	--				END
	--			WHEN o.OperatorID = '6' THEN --AND p.EvaluatedValue BETWEEN o.Val1 AND o.Val2 + 'z' THEN 1
	--				CASE
	--					WHEN dbo.fnIsMoney(p.EvaluatedValue) = 1 AND dbo.fnIsMoney(o.Val1) = 1 AND dbo.fnIsMoney(o.Val2) = 1 THEN
	--						CASE
	--							WHEN CAST(p.EvaluatedValue AS MONEY) BETWEEN CAST(o.Val1 AS MONEY) AND CAST(o.Val2 AS MONEY) THEN 1
	--							ELSE 0
	--						END
	--					ELSE
	--						CASE
	--							WHEN p.EvaluatedValue BETWEEN o.Val1 AND o.Val2 + 'z' THEN 1
	--							ELSE 0
	--						END
	--				END
	--			WHEN o.OperatorID = '7' AND p.EvaluatedValue LIKE o.Val1 + '%' THEN 1
	--			WHEN o.OperatorID = '8' AND p.EvaluatedValue LIKE '%' + o.Val1 THEN 1
	--			WHEN o.OperatorID = '9' AND p.EvaluatedValue LIKE '%' + o.Val1 + '%' THEN 1
	--			WHEN o.OperatorID = '13' AND p.EvaluatedValue NOT LIKE o.Val1 + '%' THEN 1
	--			WHEN o.OperatorID = '14' AND p.EvaluatedValue NOT LIKE '%' + o.Val1 THEN 1
	--			WHEN o.OperatorID = '15' AND p.EvaluatedValue NOT LIKE '%' + o.Val1 + '%' THEN 1
	--			WHEN o.OperatorID = '11' AND 
	--				(
	--				SELECT TOP 1 'match'
	--				FROM dbo.fnTableOfValues(p.EvaluatedValue, ' ') v
	--				INNER JOIN dbo.fnTableOfValuesFromCSV(o.Val1) m ON RTRIM(LTRIM(v.AnyValue)) = RTRIM(LTRIM(m.AnyValue))
	--				) = 'match'
	--				THEN 1
	--			WHEN o.OperatorID = '12' AND DIFFERENCE(p.EvaluatedValue, o.Val1) >= CAST(o.Val2 AS INT) THEN 1
	--			WHEN o.OperatorID = '10' THEN 1
	--			WHEN o.OperatorID = '16' AND p.EvaluatedValue = '' THEN 1
	--			WHEN o.OperatorID = '17' AND p.EvaluatedValue <> '' THEN 1
	--			WHEN o.OperatorID = '18' AND CHARINDEX(p.EvaluatedValue,o.Val1) > 0 THEN 1
	--			ELSE 0
	--		END
	--	WHEN p.DataTypeID = 5 THEN -- date time	
	--		CASE
	--			WHEN o.OperatorID = '1' AND CAST(p.EvaluatedValue AS DATETIME) = CAST(o.Val1 AS DATETIME) THEN 1
	--			WHEN o.OperatorID = '2' AND CAST(p.EvaluatedValue AS DATETIME) < CAST(o.Val1 AS DATETIME) THEN 1
	--			WHEN o.OperatorID = '3' AND CAST(p.EvaluatedValue AS DATETIME) <= CAST(o.Val1 AS DATETIME) THEN 1
	--			WHEN o.OperatorID = '4' AND CAST(p.EvaluatedValue AS DATETIME) > CAST(o.Val1 AS DATETIME) THEN 1
	--			WHEN o.OperatorID = '5' AND CAST(p.EvaluatedValue AS DATETIME) >= CAST(o.Val1 AS DATETIME) THEN 1
	--			WHEN o.OperatorID = '6' AND CAST(p.EvaluatedValue AS DATETIME) BETWEEN CAST(o.Val1 AS DATETIME) AND CAST(o.Val2 AS DATETIME) THEN 1
	--			WHEN o.OperatorID = '10' THEN 1
	--			ELSE 0
	--		END
	--	ELSE 0
	--END = 1

	---- Delete all options that do not match
	--DELETE @Options
	--WHERE Matched IS NULL

	--IF @Debug = 1 AND @Verbose = 1
	--BEGIN

	--	SELECT 'Matching Options' AS Data, *
	--	FROM @Options o
	--	INNER JOIN @Parameters p ON o.RuleParameterID = p.RuleParameterID

	--END


	-- Get rid of any duplicate matches.  
	-- We are going to order by 'any' DESC so this gets eliminated first and then by the length of the value DESC to hopefully select the most specific rule first

	;WITH PrioritisedOptions AS 
	(
		SELECT *, 
		ROW_NUMBER() OVER(PARTITION BY o.RuleParameterID ORDER BY	CASE o.OperatorID
																		WHEN 10 THEN 1
																		ELSE 0
																	END, LEN(o.Val1) DESC) as rn 
		FROM @Options o
	)	

	DELETE @Options
	WHERE ParameterOptionID IN
	(
		SELECT ParameterOptionID 
		FROM PrioritisedOptions
		WHERE rn != 1
	)

	IF @Debug = 1
	BEGIN

		SELECT 'Prioritised Options' AS Data, *
		FROM @Options o
		INNER JOIN @Parameters p ON o.RuleParameterID = p.RuleParameterID

	END

	/* 
	##########
	Get the values
	##########
	*/

	DECLARE @Values TABLE
	(
		RuleOutputID INT,
		Value VARCHAR(2000),
		TransformID INT,
		Transform VARCHAR(50),
		Output VARCHAR(2000),
		Delta VARCHAR(2000)
	)

	-- Find output values where the options exactly match the coordinates
	
	
	/*
	INSERT @Values (RuleOutputID, Value, TransformID, Transform)
	SELECT	ro.RuleOutputID, ro.Value, ro.TransformID, t.Name
	FROM	RulesEngine_RuleOutputs ro WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_Transforms t WITH (NOLOCK) ON ro.TransformID = t.TransformID
	INNER JOIN RulesEngine_OutputCoordinates oc WITH (NOLOCK) ON ro.RuleOutputID = oc.RuleOutputID 	
	WHERE	oc.ParameterOptionID 
	IN (
		SELECT	ParameterOptionID FROM	@Options
	)
	GROUP BY ro.RuleOutputID, ro.Value, ro.TransformID, t.Name
	*/
	

	INSERT @Values (RuleOutputID, Value, TransformID, Transform)
	SELECT r.RuleOutputID, r.Value, r.TransformID, t.Name
	FROM dbo.RulesEngine_RuleOutputs r WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_Transforms t WITH (NOLOCK) ON r.TransformID = t.TransformID
	WHERE r.RuleOutputID IN
	(
		SELECT c.RuleOutputID 
		FROM dbo.RulesEngine_OutputCoordinates c WITH (NOLOCK) 
		LEFT JOIN @Options o ON c.ParameterOptionID = o.ParameterOptionID
		
		-- Performance improvement
		WHERE c.ParameterOptionID IN (
			SELECT po.ParameterOptionID
			FROM @Parameters rp
			INNER JOIN RulesEngine_ParameterOptions po WITH (NOLOCK) ON po.RuleParameterID = rp.RuleParameterID 
		)
		
		GROUP BY c.RuleOutputID
		HAVING COUNT(o.ParameterOptionID) = COUNT(*) 
	)

	IF @Debug = 1 AND @Verbose = 1
	BEGIN

		SELECT 'Matched Output' AS Data, *
		FROM @Values

		SELECT 'Matched Output Options' AS Data, v.RuleOutputID, o.*
		FROM @Values v
		INNER JOIN dbo.RulesEngine_OutputCoordinates c WITH (NOLOCK) ON v.RuleOutputID = c.RuleOutputID
		INNER JOIN @Options o ON c.ParameterOptionID = o.ParameterOptionID

	END

	/*
	##########
	Handle data types and transforms
	##########
	*/
	DECLARE @InputTypeID INT,
			@InputType VARCHAR(50),

			@RuleCheckpoint VARCHAR(50)

	SELECT @InputTypeID = rs.InputTypeID, @InputType = dt.Name, @RuleCheckpoint = r.RuleCheckpoint
	FROM dbo.RulesEngine_RuleSets rs WITH (NOLOCK) 	
	INNER JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON r.RuleSetID = rs.RuleSetID
	INNER JOIN dbo.RulesEngine_DataTypes dt WITH (NOLOCK) ON rs.InputTypeID = dt.TypeID
	WHERE r.RuleID = @ID

	SELECT @Input =	CASE
						WHEN @Input IS NOT NULL THEN @Input
						ELSE
							CASE
								WHEN @InputTypeID IN (1, 2, 3) THEN	'0' -- decimal, int, bool
								WHEN @InputTypeID = 4 THEN '' -- string
								WHEN @InputTypeID = 5 THEN CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)  -- date time	
								ELSE ''
							END
					END

	-- Handle special syntax! 
	-- 1. [!P:123] == takes the value of a parameter... users when calling another ruleset
	UPDATE v
	SET v.Value = p.EvaluatedValue
	FROM dbo.RulesEngine_RuleOutputs r
	INNER JOIN dbo.RulesEngine_OutputCoordinates c WITH (NOLOCK) ON r.RuleOutputID = c.RuleOutputID
	INNER JOIN @Parameters p ON c.OutputCoordinateID = c.OutputCoordinateID
	INNER JOIN @Values v ON r.RuleOutputID = v.RuleOutputID
	WHERE v.Value LIKE '\[!P:%]' ESCAPE '\'
	AND p.RuleParameterID = REPLACE(REPLACE(v.Value, '[!P:', ''), ']', '')


	UPDATE v
	SET v.Output =	CASE
						WHEN @InputTypeID IN (1, 2, 3, 6) THEN -- decimal, int, bool, lookup list
							CASE
								WHEN v.TransformID = 6 AND @InputTypeID = 1 THEN CAST(ROUND(CAST(@Input AS DECIMAL(18, 8)), CAST(v.Value AS INT)) AS VARCHAR)
								WHEN v.TransformID = 12 AND @InputTypeID = 1 THEN CAST(CEILING(CAST(@Input AS DECIMAL(18, 8))) AS VARCHAR) /*GPR 2020-12-08 for SDPRU-184*/
								WHEN v.TransformID = 7 AND @InputTypeID = 1 THEN CAST(ROUND(CAST(@Input AS DECIMAL(18, 8)), CAST(v.Value AS INT), 1) AS VARCHAR)
								WHEN v.TransformID = 8 AND @InputTypeID IN (1, 2) THEN CAST(CAST(POWER(CAST(@Input AS DECIMAL(18, 8)), CAST(v.Value AS DECIMAL(18, 8))) AS DECIMAL(18, 8)) AS VARCHAR)
								WHEN v.TransformID = 9 AND @InputTypeID IN (1, 2) THEN   
									CASE  
										WHEN CAST(@Input AS DECIMAL(18, 8)) > CAST(v.Value AS DECIMAL(18, 8)) THEN v.Value  
										ELSE @Input  
									END  
								WHEN v.TransformID = 10 AND @InputTypeID IN (1, 2) THEN  
									CASE  
										WHEN CAST(@Input AS DECIMAL(18, 8)) < CAST(v.Value AS DECIMAL(18, 8)) THEN v.Value  
										ELSE @Input 
									END 
								WHEN v.TransformID = 1 THEN CAST(CAST(@Input AS DECIMAL(18, 8)) + CAST(v.Value AS DECIMAL(18, 8)) AS VARCHAR)
								WHEN v.TransformID = 2 THEN CAST(CAST(@Input AS DECIMAL(18, 8)) - CAST(v.Value AS DECIMAL(18, 8)) AS VARCHAR)
								WHEN v.TransformID = 3 THEN CAST(CAST(CAST(@Input AS DECIMAL(18, 8)) * CAST(v.Value AS DECIMAL(18, 8)) AS DECIMAL(18, 8)) AS VARCHAR)
								WHEN v.TransformID = 4 THEN CAST(CAST(CAST(@Input AS DECIMAL(18, 8)) / CAST(v.Value AS DECIMAL(18, 8)) AS DECIMAL(18, 8)) AS VARCHAR)
								ELSE v.Value
							END
						WHEN @InputTypeID = 4 THEN -- string
							CASE
								WHEN v.TransformID = 1 THEN @Input + v.Value
								ELSE v.Value
							END
						WHEN @InputTypeID = 5 THEN -- date time	
							CASE
								WHEN v.TransformID = 1 THEN CONVERT(VARCHAR, CAST(@Input AS DATETIME) + CAST(v.Value AS DATETIME), 120)
								WHEN v.TransformID = 2 THEN CONVERT(VARCHAR, CAST(@Input AS DATETIME) - CAST(v.Value AS DATETIME), 120)
								ELSE v.Value
							END
						ELSE '' 	
					END
	FROM @Values v


	-- Determine Delta (the change from Input to Output)
	UPDATE v
	SET Delta = CASE
		WHEN @InputTypeID IN (1,2,3,6) THEN CONVERT(VARCHAR, CONVERT(DECIMAL(18,8), v.Output) - CONVERT(DECIMAL(18,8), @Input))
		ELSE 'n/a'
		END
	FROM @Values v


	IF @Debug = 1
	BEGIN

		SELECT 'Output With Values' AS Data, @InputTypeID AS InputTypeID, @InputType AS InputType, @Input AS Input, @RuleCheckpoint AS RuleCheckpoint, *
		FROM @Values

	END

	/* 
	##########
	Output the value!
	##########
	*/
	IF @Debug = 1
	BEGIN

		SELECT TOP 1 @Input AS Input, @RuleCheckpoint AS RuleCheckpoint, v.Transform, v.Value, v.Output, v.Delta
		FROM @Values v

	END	

	SELECT @Output = 
	(
		SELECT TOP 1 @ID AS RuleID, @Input AS Input, @RuleCheckpoint AS RuleCheckpoint, v.Transform, v.Value, v.Output, v.Delta,
			(
				SELECT o.RuleParameterID AS '@RuleParameterID', p.Name AS '@ParameterName', o.OperatorText AS '@OperatorText', o.Matched AS '@Matched'
					--,p.XmlOutput
				FROM @Values v
				INNER JOIN dbo.RulesEngine_OutputCoordinates c WITH (NOLOCK) ON v.RuleOutputID = c.RuleOutputID
				INNER JOIN @Options o ON c.ParameterOptionID = o.ParameterOptionID
				INNER JOIN @Parameters p ON o.RuleParameterID = p.RuleParameterID
				FOR XML PATH('Parameter'), type
			)
		FROM @Values v
		FOR XML RAW('Rule')	
	)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Evaluate_Rule] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Evaluate_Rule] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Evaluate_Rule] TO [sp_executeall]
GO
