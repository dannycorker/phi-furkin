SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SMSQuestion table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSQuestion_Insert]
(

	@SMSQuestionID int    OUTPUT,

	@ClientID int   ,

	@SMSQuestionnaireID int   ,

	@Question varchar (960)  ,

	@QuestionOrder int   ,

	@DocumentTypeID int   ,

	@Enabled bit   ,

	@Timeout int   ,

	@Repeat int   ,

	@RepeatPrefix varchar (2000)  ,

	@DetailFieldToSave int   ,

	@EvaluateResponse bit   ,

	@VoiceAction int   ,

	@RedirectTo varchar (50)  ,

	@GatherNumDigits int   ,

	@RecordingMaxLength int   
)
AS


				
				INSERT INTO [dbo].[SMSQuestion]
					(
					[ClientID]
					,[SMSQuestionnaireID]
					,[Question]
					,[QuestionOrder]
					,[DocumentTypeID]
					,[Enabled]
					,[Timeout]
					,[Repeat]
					,[RepeatPrefix]
					,[DetailFieldToSave]
					,[EvaluateResponse]
					,[VoiceAction]
					,[RedirectTo]
					,[GatherNumDigits]
					,[RecordingMaxLength]
					)
				VALUES
					(
					@ClientID
					,@SMSQuestionnaireID
					,@Question
					,@QuestionOrder
					,@DocumentTypeID
					,@Enabled
					,@Timeout
					,@Repeat
					,@RepeatPrefix
					,@DetailFieldToSave
					,@EvaluateResponse
					,@VoiceAction
					,@RedirectTo
					,@GatherNumDigits
					,@RecordingMaxLength
					)
				-- Get the identity value
				SET @SMSQuestionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestion_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion_Insert] TO [sp_executeall]
GO
