SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the PanelItems table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItems_Insert]
(

	@PanelItemID int    OUTPUT,

	@ClientID int   ,

	@PanelItemName varchar (50)  ,

	@PanelID int   ,

	@PanelItemTypeID int   ,

	@CreatedBy int   ,

	@QueryID int   ,

	@IsGlobal bit   ,

	@XAxisColumn varchar (50)  ,

	@Orientation varchar (50)  ,

	@Margin int   
)
AS


				
				INSERT INTO [dbo].[PanelItems]
					(
					[ClientID]
					,[PanelItemName]
					,[PanelID]
					,[PanelItemTypeID]
					,[CreatedBy]
					,[QueryID]
					,[IsGlobal]
					,[XAxisColumn]
					,[Orientation]
					,[Margin]
					)
				VALUES
					(
					@ClientID
					,@PanelItemName
					,@PanelID
					,@PanelItemTypeID
					,@CreatedBy
					,@QueryID
					,@IsGlobal
					,@XAxisColumn
					,@Orientation
					,@Margin
					)
				-- Get the identity value
				SET @PanelItemID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItems_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItems_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItems_Insert] TO [sp_executeall]
GO
