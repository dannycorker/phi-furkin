SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the FAS_CheckerData table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_CheckerData_GetPaged]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				CREATE TABLE #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [FasCheckerDataID] int 
				)
				
				-- Insert into the temp table
				DECLARE @SQL AS nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex ([FasCheckerDataID])'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [FasCheckerDataID]'
				SET @SQL = @SQL + ' FROM [dbo].[FAS_CheckerData] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				EXEC sp_executesql @SQL

				-- Return paged results
				SELECT O.[FasCheckerDataID], O.[FasUniqueReferenceNumber], O.[TitleOfAgreement], O.[IsDebtorsNameOrAddressMissing], O.[IsCreditorsNameOrAddressMissing], O.[AgreementDebtorSignatureDate], O.[NameOfCreditor], O.[AmountOfCredit], O.[TotalCashPrice], O.[DurationOfAgreementInMonths], O.[AnnualPercentageRate], O.[RateOfInterest], O.[FixedOrVariableRateOfInterest], O.[TotalInterest], O.[TotalChargeFees], O.[TotalChargeForCredit], O.[FirstPayment], O.[ConstantRegularOrSubsequentPayment], O.[LastPayment], O.[DeferredOrBallonPayment], O.[TotalPayments], O.[AdvancePayment], O.[TotalAmountPayable], O.[Category]
				FROM
				    [dbo].[FAS_CheckerData] o WITH (NOLOCK),
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexId > @PageLowerBound
					AND O.[FasCheckerDataID] = PageIndex.[FasCheckerDataID]
				ORDER BY
				    PageIndex.IndexId
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[FAS_CheckerData] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_CheckerData_GetPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_CheckerData_GetPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_CheckerData_GetPaged] TO [sp_executeall]
GO
