SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the HyperlinkDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkDetailFieldTarget_Update]
(

	@HyperlinkDetailFieldTargetID int   ,

	@ClientID int   ,

	@HyperlinkDetailFieldID int   ,

	@Target varchar (250)  ,

	@DetailFieldID int   ,

	@TemplateTypeID int   ,

	@DetailFieldAlias varchar (500)  ,

	@Notes varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[HyperlinkDetailFieldTarget]
				SET
					[ClientID] = @ClientID
					,[HyperlinkDetailFieldID] = @HyperlinkDetailFieldID
					,[Target] = @Target
					,[DetailFieldID] = @DetailFieldID
					,[TemplateTypeID] = @TemplateTypeID
					,[DetailFieldAlias] = @DetailFieldAlias
					,[Notes] = @Notes
				WHERE
[HyperlinkDetailFieldTargetID] = @HyperlinkDetailFieldTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkDetailFieldTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkDetailFieldTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkDetailFieldTarget_Update] TO [sp_executeall]
GO
