SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-05-10
-- Description:	SP to Return Nothing
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ReturnNothing]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ReturnNothing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ReturnNothing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ReturnNothing] TO [sp_executeall]
GO
