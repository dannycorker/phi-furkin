SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ReportTableRelationships table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReportTableRelationships_GetByReportTableRelationshipID]
(

	@ReportTableRelationshipID int   
)
AS


				SELECT
					[ReportTableRelationshipID],
					[TableFrom],
					[TableTo],
					[TableJoin],
					[Description],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ReportTableRelationships] WITH (NOLOCK) 
				WHERE
										[ReportTableRelationshipID] = @ReportTableRelationshipID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTableRelationships_GetByReportTableRelationshipID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReportTableRelationships_GetByReportTableRelationshipID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTableRelationships_GetByReportTableRelationshipID] TO [sp_executeall]
GO
