SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:           Richard Doyle
-- Create date: 2017-07-6
-- Description:      Get all the marital status'
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Search_MaritalStatus]
(
       @ClientId INT,
       @ClientPersonnelId INT
)
AS
BEGIN

       SELECT LookupListItemID Value, ItemValue Text
       FROM   LookupListItems WITH (NOLOCK) 
       WHERE  LookupListID = 1186

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_MaritalStatus] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Search_MaritalStatus] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_MaritalStatus] TO [sp_executeall]
GO
