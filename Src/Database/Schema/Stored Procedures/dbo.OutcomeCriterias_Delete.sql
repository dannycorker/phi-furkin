SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the OutcomeCriterias table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[OutcomeCriterias_Delete]
(

	@OutcomeCriteriaID int   
)
AS


				DELETE FROM [dbo].[OutcomeCriterias] WITH (ROWLOCK) 
				WHERE
					[OutcomeCriteriaID] = @OutcomeCriteriaID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeCriterias_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutcomeCriterias_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeCriterias_Delete] TO [sp_executeall]
GO
