SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ian Slack
-- Create date: 2013-10-29
-- Description:	Detail Field Lock Get
-- @LeadOrMatter: Lead 1, Matter 2, Customer 10, Case 11, Client 12
-- =============================================
CREATE PROCEDURE [dbo].[DetailFieldLock__Get]
	@LeadOrMatter INT,
	@ClientID INT, 
	@CustomerID INT, 
	@LeadID  INT,
	@CaseID  INT,
	@MatterID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE	@LeadOrMatterID INT
	SELECT @LeadOrMatterID =CASE 
					WHEN @LeadOrMatter = 2 AND @MatterID > 0 THEN @MatterID
					WHEN @LeadOrMatter = 11 AND @CaseID > 0 THEN @CaseID
					WHEN @LeadOrMatter = 1 AND @LeadID > 0 THEN @LeadID
					WHEN @LeadOrMatter = 10 AND @CustomerID > 0 THEN @CustomerID
					WHEN @LeadOrMatter = 12 AND @ClientID > 0 THEN @ClientID
					ELSE NULL END
					
	SELECT	DetailFieldID, DetailFieldPageID
	FROM	DetailFieldLock WITH (NOLOCK) 
	WHERE	ClientID = @ClientID
	AND		LeadOrMatterID = @LeadOrMatterID
	AND		LeadOrMatter = @LeadOrMatter
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLock__Get] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldLock__Get] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldLock__Get] TO [sp_executeall]
GO
