SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Jim Green
-- Create date: 2007-10-04
-- Description:	Get the rights that this User has to view this Lead
-- =============================================
CREATE PROCEDURE [dbo].[GetUserLeadAccess]
	-- Add the parameters for the stored procedure here
	@UserID int,
	@LeadID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT dbo.fnUserLeadAccess(@UserID, @LeadID)

END







GO
GRANT VIEW DEFINITION ON  [dbo].[GetUserLeadAccess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetUserLeadAccess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetUserLeadAccess] TO [sp_executeall]
GO
