SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Chris Townsend
-- Create date: 05/09/2008
-- Description:	Deletes all reminders for a specific appointment
-- =============================================

CREATE PROCEDURE [dbo].[DiaryReminder__DeleteByDiaryAppointmentID] 

	@DiaryAppointmentID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM DiaryReminder
	WHERE DiaryAppointmentID = @DiaryAppointmentID
END



GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder__DeleteByDiaryAppointmentID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryReminder__DeleteByDiaryAppointmentID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder__DeleteByDiaryAppointmentID] TO [sp_executeall]
GO
