SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.GetQuestionnaireFont    Script Date: 08/09/2006 12:22:41 ******/
CREATE PROCEDURE [dbo].[GetQuestionnaireFont] @ClientQuestionnaireID int, @PartNameID int

 AS

Select QuestionnaireFontID, ClientQuestionnaireID, PartNameID, FontFamily, FontSize, FontColour, FontWeight, FontAlignment, ClientID 
From QuestionnaireFonts
Where ((ClientQuestionnaireID = @ClientQuestionnaireID) and (PartNameID = @PartNameID))



GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionnaireFont] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetQuestionnaireFont] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionnaireFont] TO [sp_executeall]
GO
