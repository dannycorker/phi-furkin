SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ClientQuestionnaires table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnaires_Get_List]

AS


				
				SELECT
					[ClientQuestionnaireID],
					[ClientID],
					[QuestionnaireTitle],
					[QuestionnaireDescription],
					[QuestionsPerPage],
					[QuestionnaireLogo],
					[QuestionnaireLogoFileName],
					[QuestionnaireFlowDiagram],
					[QuestionnaireIntroductionText],
					[HideIntro],
					[QuestionnaireFooterText],
					[HideFooter],
					[QuestionnaireAddressLookup],
					[Published],
					[CustomerInformationAtStart],
					[LinkedQuestionnaireClientQuestionnaireID],
					[QuestionnaireFooterIframe],
					[QuestionnaireHeaderIframe],
					[QuestionnaireFooterInternal],
					[QuestionnaireHeaderInternal],
					[QuestionnaireFooterIframeHeight],
					[QuestionnaireHeaderIframeHeight],
					[QuestionnaireFooterIframeWidth],
					[QuestionnaireHeaderIframeWidth],
					[QuestionnaireBackGroundImage],
					[QuestionnaireBackGroundImageFileName],
					[DefaultEmailAddress],
					[MailingListType],
					[FrameMode],
					[LayoutCss],
					[LayoutCssFileName],
					[ImportDirectlyIntoLeadManager],
					[RunAsClientPersonnelID],
					[RememberAnswers],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[ClientQuestionnaires] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaires_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnaires_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaires_Get_List] TO [sp_executeall]
GO
