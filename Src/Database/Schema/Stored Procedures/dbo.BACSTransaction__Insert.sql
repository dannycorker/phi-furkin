SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 05/08/2016
-- Description:	Inserts a BACS Transaction record
-- =============================================
CREATE PROCEDURE [dbo].[BACSTransaction__Insert]
	
	@BACSFileID INT, 
	@ClientID INT, 
	@CustomerID INT, 
	@ObjectID INT, 
	@ObjectTypeID INT, 
	@PurchasedProductPaymentScheduleID INT, 
	@CustomerPaymentScheduleID INT, 
	@PaymentTypeID INT,
	@DebitAccountName VARCHAR(105), 
	@DebitSortCode VARCHAR(6), 
	@DebitAccountNumber VARCHAR(8),
	@CurrencyID INT, 
	@PaymentAmount NUMERIC(18,2), 
	@PaymentReference VARCHAR(16), 
	@BACSProcessingDate DATETIME, 
	@BeneficiaryAccountNumber VARCHAR(8), 
	@BeneficiarySortCode VARCHAR(6), 
	@BeneficiaryName VARCHAR(35), 
	@BeneficiaryAddress VARCHAR(105), 
	@BeneficiaryBankName VARCHAR(35), 
	@BeneficiaryBankAddress VARCHAR(105), 
	@SUN VARCHAR(50), 
	@FailureCode VARCHAR(16), 
	@FailureDate DATETIME, 
	@DeemedTransactionDate DATETIME, 
	@DeemedTransactedOn DATETIME, 
	@RowNumberInFile INT, 
	@BACSTransactionCodeID INT, 
	@LeadEventID INT,
	@WhoCreated INT, 
	@WhenCreated DATETIME
	
AS
BEGIN

	SET NOCOUNT ON;
	
	INSERT INTO BACSTransaction (BACSFileID, ClientID, CustomerID, ObjectID, ObjectTypeID, PurchasedProductPaymentScheduleID, CustomerPaymentScheduleID, PaymentTypeID, DebitAccountName, DebitSortCode, DebitAccountNumber, CurrencyID, PaymentAmount, PaymentReference, BACSProcessingDate, BeneficiaryAccountNumber, BeneficiarySortCode, BeneficiaryName, BeneficiaryAddress, BeneficiaryBankName, BeneficiaryBankAddress, SUN, FailureCode, FailureDate, DeemedTransactionDate, DeemedTransactedOn, RowNumberInFile, BACSTransactionCodeID, LeadEventID, WhoCreated, WhenCreated)
	VALUES (@BACSFileID, @ClientID, @CustomerID, @ObjectID, @ObjectTypeID, @PurchasedProductPaymentScheduleID, @CustomerPaymentScheduleID, @PaymentTypeID, @DebitAccountName, @DebitSortCode, @DebitAccountNumber, @CurrencyID, @PaymentAmount, @PaymentReference, @BACSProcessingDate, @BeneficiaryAccountNumber, @BeneficiarySortCode, @BeneficiaryName, @BeneficiaryAddress, @BeneficiaryBankName, @BeneficiaryBankAddress, @SUN, @FailureCode, @FailureDate, @DeemedTransactionDate, @DeemedTransactedOn, @RowNumberInFile, @BACSTransactionCodeID, @LeadEventID, @WhoCreated, @WhenCreated)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[BACSTransaction__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BACSTransaction__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BACSTransaction__Insert] TO [sp_executeall]
GO
