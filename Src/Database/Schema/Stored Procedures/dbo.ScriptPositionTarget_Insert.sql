SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ScriptPositionTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptPositionTarget_Insert]
(

	@ScriptPositionTargetID int    OUTPUT,

	@ScriptID int   ,

	@SectionID int   ,

	@ClientID int   ,

	@PositionName varchar (250)  ,

	@LinkText varchar (250)  
)
AS


				
				INSERT INTO [dbo].[ScriptPositionTarget]
					(
					[ScriptID]
					,[SectionID]
					,[ClientID]
					,[PositionName]
					,[LinkText]
					)
				VALUES
					(
					@ScriptID
					,@SectionID
					,@ClientID
					,@PositionName
					,@LinkText
					)
				-- Get the identity value
				SET @ScriptPositionTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptPositionTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget_Insert] TO [sp_executeall]
GO
