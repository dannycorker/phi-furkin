SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2016-04-01
-- Description:	Get other leads with assignee details for a lead
-- 2016-04-12 ACE Updated to include customer id as getting to customer detail field view crashes if its not there
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetLeadsForLead]
	@ClientID INT,
	@ClientPersonnelID INT,
	@CustomerID INT,
	@LeadID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT '/CustomersLeadDetails2.aspx?cid=' + CONVERT(VARCHAR,l.CustomerID) + '&lid=' + CONVERT(VARCHAR,l.LeadID) AS [Link], lt.LeadTypeName AS [Text], ISNULL(cp.UserName, 'Not Assigned') AS [Assignee]
	FROM Lead l WITH (NOLOCK)
	INNER JOIN LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID
	LEFT JOIN ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = l.AssignedTo
	CROSS APPLY dbo.fnLeadTypeSecure(@ClientPersonnelID) lts
	WHERE l.CustomerID = @CustomerID
	AND l.LeadID <> @LeadID
	AND l.ClientID = @ClientID
	AND lt.LeadTypeID = lts.LeadTypeID
	AND lts.RightID > 0

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetLeadsForLead] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetLeadsForLead] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetLeadsForLead] TO [sp_executeall]
GO
