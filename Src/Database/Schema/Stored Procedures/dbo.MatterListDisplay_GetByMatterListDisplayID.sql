SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the MatterListDisplay table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterListDisplay_GetByMatterListDisplayID]
(

	@MatterListDisplayID int   
)
AS


				SELECT
					[MatterListDisplayID],
					[ClientID],
					[LeadTypeID],
					[MatterRef],
					[MatterStatus],
					[MatterID],
					[Field1],
					[Field2],
					[Field3],
					[Field4],
					[Field5],
					[Field1ColumnDetailFieldID],
					[Field2ColumnDetailFieldID],
					[Field3ColumnDetailFieldID],
					[Field4ColumnDetailFieldID],
					[Field5ColumnDetailFieldID],
					[SqlQueryText]
				FROM
					[dbo].[MatterListDisplay] WITH (NOLOCK) 
				WHERE
										[MatterListDisplayID] = @MatterListDisplayID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterListDisplay_GetByMatterListDisplayID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterListDisplay_GetByMatterListDisplayID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterListDisplay_GetByMatterListDisplayID] TO [sp_executeall]
GO
