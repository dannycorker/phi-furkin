SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventTypePaymentGateway table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypePaymentGateway_Delete]
(

	@EventTypePaymentGatewayID int   
)
AS


				DELETE FROM [dbo].[EventTypePaymentGateway] WITH (ROWLOCK) 
				WHERE
					[EventTypePaymentGatewayID] = @EventTypePaymentGatewayID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypePaymentGateway_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypePaymentGateway_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypePaymentGateway_Delete] TO [sp_executeall]
GO
