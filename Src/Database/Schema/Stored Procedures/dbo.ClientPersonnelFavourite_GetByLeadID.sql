SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnelFavourite table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelFavourite_GetByLeadID]
(

	@LeadID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientPersonnelFavouriteID],
					[ClientID],
					[ClientPersonnelID],
					[LeadID]
				FROM
					[dbo].[ClientPersonnelFavourite] WITH (NOLOCK) 
				WHERE
					[LeadID] = @LeadID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelFavourite_GetByLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelFavourite_GetByLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelFavourite_GetByLeadID] TO [sp_executeall]
GO
