SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the CaseTransferStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferStatus_Delete]
(

	@CaseTransferStatusID int   
)
AS


				DELETE FROM [dbo].[CaseTransferStatus] WITH (ROWLOCK) 
				WHERE
					[CaseTransferStatusID] = @CaseTransferStatusID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferStatus_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferStatus_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferStatus_Delete] TO [sp_executeall]
GO
