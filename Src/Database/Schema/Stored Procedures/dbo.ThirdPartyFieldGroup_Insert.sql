SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ThirdPartyFieldGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldGroup_Insert]
(

	@ThirdPartyFieldGroupID int    OUTPUT,

	@ThirdPartySystemID int   ,

	@GroupName varchar (50)  ,

	@GroupDescription varchar (250)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[ThirdPartyFieldGroup]
					(
					[ThirdPartySystemID]
					,[GroupName]
					,[GroupDescription]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@ThirdPartySystemID
					,@GroupName
					,@GroupDescription
					,@WhenCreated
					,@WhenModified
					)
				-- Get the identity value
				SET @ThirdPartyFieldGroupID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldGroup_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldGroup_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldGroup_Insert] TO [sp_executeall]
GO
