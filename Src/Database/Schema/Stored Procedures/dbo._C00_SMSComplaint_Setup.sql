SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2016-03-10
-- Description:	Setup SMS Complaint
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SMSComplaint_Setup] 
(
	@ClientID			INT  
	,@SMSLeadTypeID		INT 
	,@SMSEventTypeID	INT
)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @PageName					VARCHAR(250)	= 'Complaint Screen Configuration'
	,@URL								VARCHAR(2000)	= 'https://apps.aquarium-software.com/smscomplaint/Logon.aspx'
	,@LeadTypeID						INT				= 1 
	,@DetailFieldSubTypeID				INT				= 12 /*Client Detail Field*/
	,@FieldName							VARCHAR(50)     = 'SMS Complaint Login'
	,@QuestionTypeID					INT				= 26
	,@ListItems tvpVarchar				INSERT INTO  @ListItems VALUES ('') /*Stop the @ListItems variable from complaining*/
	DECLARE	@Required					BIT				= 0
	,@ValidationCriteriaFieldTypeID		INT				= 1
	,@ValidationCriteriaID				INT				= 1
	,@MinimumValue						varchar(50)		= ''
	,@MaximumValue						VARCHAR(50)		= ''
	,@RegEx								VARCHAR(2000)	= ''
	,@ErrorMessage						VARCHAR(250)	= ''
	,@HTTPFieldID						INT 
	,@PageID							INT	
	,@UserID							INT	= (SELECT TOP 1 ClientPersonnelID FROM ClientPersonnel cp WITH (NOLOCK) WHERE cp.FirstName like '%aquarium%' AND cp.ClientID=@ClientID)
	,@TableColumnLeadTypeIdentity		INT
	,@TableColumnSMSEventTypeID			INT
	,@TableColumnDocumentTypeIdentity	INT
	,@DocumentTypeIdentity				INT = (SELECT TOP 1 et.DocumentTypeID FROM  EventType et WITH (NOLOCK) WHERE et.EventTypeID = @SMSeventtypeID )
	,@TableColumnGroupName				INT
	,@TableColumnUserName				INT
	,@TableColumnMobileTelephone		INT

	/*Make the Page On ClientDetailValues and add a DetailField*/
	EXEC @HTTPFieldID = _C00_DetailFields_CreatePageAndField @ClientID,@PageName,@LeadTypeID,@DetailFieldSubTypeID,@FieldName,@QuestionTypeID,@ListItems,@Required,@ValidationCriteriaFieldTypeID,@ValidationCriteriaID,@MinimumValue,@MaximumValue,@RegEx,@ErrorMessage

	/*Update the URL*/
	EXEC dbo._C00_DetailField_Update @HTTPFieldID,'Hyperlink',@URL

	SELECT @FieldName = 'PolicyNumberDetailFieldID' DECLARE @DetailFieldPageID int	= (SELECT TOP 1 DF.DetailFieldPageID FROM DetailFields DF WITH (NOLOCK) WHERE DF.DetailFieldID = @HTTPFieldID) SELECT @QuestionTypeID = 1 DECLARE @DetailFieldID	int DECLARE @LeadOrMatter	tinyint = (SELECT TOP 1 dp.LeadOrMatter FROM DetailFieldPages dp WITH (NOLOCK) WHERE dp.DetailFieldPageID = @DetailFieldPageID) DECLARE @FieldCaption	varchar(100) = @FieldName  DECLARE @Lookup			bit = 0  DECLARE @LookupListID	int DECLARE @Enabled		bit = 1 DECLARE @FieldOrder		int = 0 DECLARE @MaintainHistory bit = 0 DECLARE @EquationText	varchar(2000) = '' DECLARE @MasterQuestionID int DECLARE @FieldSize		int = 1DECLARE @LinkedDetailFieldID int DECLARE @ResourceListDetailFieldPageID int DECLARE @TableDetailFieldPageID int DECLARE @DefaultFilter varchar(250)  = '' DECLARE @ColumnEquationText varchar(2000)  = '' DECLARE @Editable bit = 1 DECLARE @Hidden bit = 0 DECLARE @LastReferenceInteger int = 0 DECLARE @ReferenceValueFormatID int = 0 DECLARE @Encrypt bit = 0 DECLARE @ShowCharacters int = 0DECLARE @NumberOfCharactersToShow int = 0DECLARE @TableEditMode int = 0DECLARE @DisplayInTableView bit = 1DECLARE @ObjectTypeID int DECLARE @SourceID int DECLARE @WhoCreated int = (select top 1 clientpersonnelid from ClientPersonnel cp WITH (NOLOCK) WHERE cp.ClientID=@ClientID and cp.AccountDisabled=0 and cp.EmailAddress like '%thomas.doyle%')DECLARE @WhenCreated datetime = dbo.fn_GetDate_Local() DECLARE @WhoModified int =(select top 1 clientpersonnelid from ClientPersonnel cp WITH (NOLOCK) WHERE cp.ClientID=@ClientID and cp.AccountDisabled=0 and cp.EmailAddress like '%thomas.doyle%') DECLARE @WhenModified datetime = dbo.fn_GetDate_Local() DECLARE @DetailFieldStyleID int DECLARE @Hyperlink nvarchar(2000) ='' DECLARE @IsShared bit
	
	/*Create PolicyNumberDetailFieldID*/
	EXECUTE DetailFields_Insert @DetailFieldID OUTPUT,@ClientID,@LeadOrMatter,@FieldName,@FieldCaption,@QuestionTypeID,@Required,@Lookup,@LookupListID,@LeadTypeID,@Enabled,@DetailFieldPageID,@FieldOrder,@MaintainHistory,@EquationText,@MasterQuestionID,@FieldSize,@LinkedDetailFieldID,@ValidationCriteriaFieldTypeID,@ValidationCriteriaID,@MinimumValue,@MaximumValue,@RegEx,@ErrorMessage,@ResourceListDetailFieldPageID,@TableDetailFieldPageID,@DefaultFilter,@ColumnEquationText,@Editable,@Hidden,@LastReferenceInteger,@ReferenceValueFormatID,@Encrypt,@ShowCharacters,@NumberOfCharactersToShow,@TableEditMode,@DisplayInTableView,@ObjectTypeID,@SourceID,@WhoCreated,@WhenCreated,@WhoModified,@WhenModified,@DetailFieldStyleID,@Hyperlink,@IsShared

	/*---Create Table: Complaint Screen Mapping-------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	DECLARE @TableDetailFieldIDComplaintScreenMapping INT 
	,@TableName Varchar(250)	= 'Complaint Screen Mapping'
	,@TableColumns tvpVarchar10	-- ID => question typeID, VAL1 => column name, Val2 => Field Order, Val3 => list of lookup list items

	INSERT INTO  @TableColumns 
	VALUES (1,'LeadTypeIdentity','1','','','','','','','','')
	,(1,'SMSEventTypeID','2','','','','','','','','')
		
	/*Add A Basic Table to the Client Detail Values Page*/
	EXEC @TableDetailFieldIDComplaintScreenMapping = dbo._C00_CreateBasicTable @ClientID,@PageName,@LeadTypeID,@DetailFieldSubTypeID,@TableName,@TableColumns,@UserID

	/*Clear the TableColumns*/
	DELETE FROM @TableColumns
	
	/*---Create Table: SMS Document Types-------------------------------------------------------------------------------------------------------------------------------------------------*/

	SELECT @TableName = 'SMS Document Types'
	
	INSERT INTO  @TableColumns 
	VALUES (1,'DocumentTypeIdentity','1','','','','','','','','')
	
	DECLARE @TableDetailFieldIDSMSDocumentTypes INT 
	/*Add A Basic Table "SMS Document Types" to the Client Detail Values Page*/
	EXEC @TableDetailFieldIDSMSDocumentTypes = dbo._C00_CreateBasicTable @ClientID,@PageName,@LeadTypeID,@DetailFieldSubTypeID,@TableName,@TableColumns,@UserID
	
	/*Clear the TableColumns*/
	DELETE FROM @TableColumns
	
	/*---Create Table: SMS Groups--------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	SELECT @TableName = 'SMS Groups'

	INSERT INTO  @TableColumns 
	VALUES (1,'GroupName','1','','','','','','','','')
	,(1,'User Name','2','','','','','','','','')
	,(1,'MobileTelephone','3','','','','','','','','')
	
	DECLARE @TableDetailFieldIDSMSGroups INT 
	/*Add A Basic Table "SMS Document Types" to the Client Detail Values Page*/
	EXEC @TableDetailFieldIDSMSGroups = dbo._C00_CreateBasicTable @ClientID,@PageName,@LeadTypeID,@DetailFieldSubTypeID,@TableName,@TableColumns,@UserID
	
	/*----------------------------------------------------------------------------------------------------------------------------------------------------*/
	
	SELECT TOP 1 @TableDetailFieldIDComplaintScreenMapping	= df.detailfieldid FROM DetailFields df WITH (NOLOCK) WHERE df.DetailFieldPageID = @DetailFieldPageID AND df.QuestionTypeID = 19 AND df.Enabled=1 AND df.FieldName ='Complaint Screen Mapping'	ORDER BY DF.DetailFieldID DESC
	SELECT TOP 1 @TableDetailFieldIDSMSDocumentTypes		= df.detailfieldid FROM DetailFields df WITH (NOLOCK) WHERE df.DetailFieldPageID = @DetailFieldPageID AND df.QuestionTypeID = 19 AND df.Enabled=1 AND df.FieldName ='SMS Document Types'		ORDER BY DF.DetailFieldID DESC
	SELECT TOP 1 @TableDetailFieldIDSMSGroups				= df.detailfieldid FROM DetailFields df WITH (NOLOCK) WHERE df.DetailFieldPageID = @DetailFieldPageID AND df.QuestionTypeID = 19 AND df.Enabled=1 AND df.FieldName ='SMS Groups'				ORDER BY DF.DetailFieldID DESC
	
	SELECT TOP 1 @TableColumnLeadTypeIdentity		= df.DetailFieldID FROM DetailFields DF WITH (NOLOCK) WHERE DF.ClientID = @ClientID AND DF.LeadOrMatter= 8 AND df.Enabled=1 AND DF.FieldName = 'LeadTypeIdentity'	ORDER BY DF.DetailFieldID DESC
	SELECT TOP 1 @TableColumnSMSEventTypeID			= df.DetailFieldID FROM DetailFields DF WITH (NOLOCK) WHERE DF.ClientID = @ClientID AND DF.LeadOrMatter= 8 AND df.Enabled=1 AND DF.FieldName = 'SMSEventTypeID'		ORDER BY DF.DetailFieldID DESC
	SELECT TOP 1 @TableColumnDocumentTypeIdentity	= df.DetailFieldID FROM DetailFields DF WITH (NOLOCK) WHERE DF.ClientID = @ClientID AND DF.LeadOrMatter= 8 AND df.Enabled=1 AND DF.FieldName = 'DocumentTypeIdentity'	ORDER BY DF.DetailFieldID DESC
	SELECT TOP 1 @TableColumnGroupName				= df.DetailFieldID FROM DetailFields DF WITH (NOLOCK) WHERE DF.ClientID = @ClientID AND DF.LeadOrMatter= 8 AND df.Enabled=1 AND DF.FieldName = 'GroupName'			ORDER BY DF.DetailFieldID DESC
	SELECT TOP 1 @TableColumnUserName				= df.DetailFieldID FROM DetailFields DF WITH (NOLOCK) WHERE DF.ClientID = @ClientID AND DF.LeadOrMatter= 8 AND df.Enabled=1 AND DF.FieldName = 'User Name'			ORDER BY DF.DetailFieldID DESC
	SELECT TOP 1 @TableColumnMobileTelephone		= df.DetailFieldID FROM DetailFields DF WITH (NOLOCK) WHERE DF.ClientID = @ClientID AND DF.LeadOrMatter= 8 AND df.Enabled=1 AND DF.FieldName = 'MobileTelephone'	ORDER BY DF.DetailFieldID DESC

	/*Put URL At top of page*/
	EXEC dbo._C00_DetailField_Update @HTTPFieldID,'FieldOrder','1'

	/*Makes the policynumber field as a customerdetailfield*/
	DECLARE @PolicynumberFieldID INT
	
	SELECT @PageName = 'PolicyInformation',@DetailFieldSubTypeID=10	,@LeadTypeID=0,@FieldName='PolicyNumber',@QuestionTypeID=1
	
	EXEC @PolicynumberFieldID = _C00_DetailFields_CreatePageAndField @ClientID,@PageName,@LeadTypeID,@DetailFieldSubTypeID,@FieldName,@QuestionTypeID,@ListItems,@Required,@ValidationCriteriaFieldTypeID,@ValidationCriteriaID,@MinimumValue,@MaximumValue,@RegEx,@ErrorMessage
	
	/*set the PolicyNumberDetailFieldID value to be the CDV @PolicynumberFieldID*/
	EXEC _C00_SimpleValueIntoField @DetailFieldID,@PolicynumberFieldID,@ClientID,NULL
	
	/*Insert into Complaint Screen mapping table*/
	DECLARE @TablerowID int
	
	INSERT INTO TableRows (ClientID,DetailFieldID,DetailFieldPageID)
	VALUES (@ClientID,@TableDetailFieldIDComplaintScreenMapping,@TableDetailFieldPageID)

	SELECT @TablerowID =  SCOPE_IDENTITY()	
	
	INSERT INTO TableDetailValues (TableRowID,DetailFieldID,DetailValue,ClientID)
	VALUES (@TablerowID,@TableColumnLeadTypeIdentity,@SMSLeadTypeID,@ClientID),
			(@TablerowID,@TableColumnSMSEventTypeID,@SMSeventtypeID,@ClientID)

	/*Insert into SMS Document Types table*/
	INSERT INTO TableRows (ClientID,DetailFieldID,DetailFieldPageID)
	VALUES (@ClientID,@TableDetailFieldIDSMSDocumentTypes,@TableDetailFieldPageID)

	SELECT @TablerowID = SCOPE_IDENTITY()
	
	INSERT INTO TableDetailValues (TableRowID,DetailFieldID,DetailValue,ClientID)
	VALUES (@TablerowID,@TableColumnDocumentTypeIdentity,@DocumentTypeIdentity,@ClientID)	
		
	/*Setup ThirdPartyFieldMapping*/
	INSERT INTO ThirdPartyFieldMapping (ClientID,LeadTypeID,ThirdPartyFieldID,DetailFieldID,ColumnFieldID,ThirdPartyFieldGroupID,IsEnabled,WhenCreated,WhenModified)
	VALUES (@ClientID,1,834,@DetailFieldID,null,65,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
	(@ClientID,1,835,@TableDetailFieldIDComplaintScreenMapping,@TableColumnLeadTypeIdentity,65,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
	(@ClientID,1,836,@TableDetailFieldIDComplaintScreenMapping,@TableColumnSMSEventTypeID,65,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
	(@ClientID,1,837,@TableDetailFieldIDSMSDocumentTypes,@TableColumnDocumentTypeIdentity,65,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
	(@ClientID,1,866,@TableDetailFieldIDSMSGroups,@TableColumnGroupName,65,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local()),
	(@ClientID,1,867,@TableDetailFieldIDSMSGroups,@TableColumnMobileTelephone,65,1,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local())
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SMSComplaint_Setup] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SMSComplaint_Setup] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SMSComplaint_Setup] TO [sp_executeall]
GO
