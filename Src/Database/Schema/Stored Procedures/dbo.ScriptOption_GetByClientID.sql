SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptOption table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptOption_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ScriptOptionID],
					[ClientID],
					[ClientPersonnelID],
					[ScriptEditorTheme],
					[ShowLineNumbers],
					[CollapseTopPanel],
					[CollapseLeftPanel],
					[CollapseRightPanel]
				FROM
					[dbo].[ScriptOption] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptOption_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptOption_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptOption_GetByClientID] TO [sp_executeall]
GO
