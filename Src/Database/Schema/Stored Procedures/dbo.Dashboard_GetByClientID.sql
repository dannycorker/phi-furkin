SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Dashboard table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Dashboard_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DashboardID],
					[DashboardName],
					[DashboardDescription],
					[OwnerID],
					[DashboardLayoutID],
					[ClientID]
				FROM
					[dbo].[Dashboard] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Dashboard_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Dashboard_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Dashboard_GetByClientID] TO [sp_executeall]
GO
