SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlQuery table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQuery_Update]
(

	@QueryID int   ,

	@ClientID int   ,

	@QueryText varchar (MAX)  ,

	@QueryTitle varchar (250)  ,

	@AutorunOnline tinyint   ,

	@OnlineLimit int   ,

	@BatchLimit int   ,

	@SqlQueryTypeID int   ,

	@FolderID int   ,

	@IsEditable bit   ,

	@IsTemplate bit   ,

	@IsDeleted bit   ,

	@WhenCreated datetime   ,

	@CreatedBy int   ,

	@OwnedBy int   ,

	@RunCount int   ,

	@LastRundate datetime   ,

	@LastRuntime int   ,

	@LastRowcount int   ,

	@MaxRuntime int   ,

	@MaxRowcount int   ,

	@AvgRuntime int   ,

	@AvgRowcount int   ,

	@Comments varchar (2000)  ,

	@WhenModified datetime   ,

	@ModifiedBy int   ,

	@LeadTypeID int   ,

	@ParentQueryID int   ,

	@IsParent bit   ,

	@SqlQueryTemplateID int   ,

	@OutputFormat varchar (20)  ,

	@ShowInCustomSearch bit   ,

	@OutputFileExtension varchar (50)  ,

	@OutputSeparatorCharmapID tinyint   ,

	@OutputEncapsulatorCharmapID tinyint   ,

	@SuppressHeaderRow bit   ,

	@LockAllTables bit   ,

	@SourceID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlQuery]
				SET
					[ClientID] = @ClientID
					,[QueryText] = @QueryText
					,[QueryTitle] = @QueryTitle
					,[AutorunOnline] = @AutorunOnline
					,[OnlineLimit] = @OnlineLimit
					,[BatchLimit] = @BatchLimit
					,[SqlQueryTypeID] = @SqlQueryTypeID
					,[FolderID] = @FolderID
					,[IsEditable] = @IsEditable
					,[IsTemplate] = @IsTemplate
					,[IsDeleted] = @IsDeleted
					,[WhenCreated] = @WhenCreated
					,[CreatedBy] = @CreatedBy
					,[OwnedBy] = @OwnedBy
					,[RunCount] = @RunCount
					,[LastRundate] = @LastRundate
					,[LastRuntime] = @LastRuntime
					,[LastRowcount] = @LastRowcount
					,[MaxRuntime] = @MaxRuntime
					,[MaxRowcount] = @MaxRowcount
					,[AvgRuntime] = @AvgRuntime
					,[AvgRowcount] = @AvgRowcount
					,[Comments] = @Comments
					,[WhenModified] = @WhenModified
					,[ModifiedBy] = @ModifiedBy
					,[LeadTypeID] = @LeadTypeID
					,[ParentQueryID] = @ParentQueryID
					,[IsParent] = @IsParent
					,[SqlQueryTemplateID] = @SqlQueryTemplateID
					,[OutputFormat] = @OutputFormat
					,[ShowInCustomSearch] = @ShowInCustomSearch
					,[OutputFileExtension] = @OutputFileExtension
					,[OutputSeparatorCharmapID] = @OutputSeparatorCharmapID
					,[OutputEncapsulatorCharmapID] = @OutputEncapsulatorCharmapID
					,[SuppressHeaderRow] = @SuppressHeaderRow
					,[LockAllTables] = @LockAllTables
					,[SourceID] = @SourceID
				WHERE
[QueryID] = @QueryID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery_Update] TO [sp_executeall]
GO
