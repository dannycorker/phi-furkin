SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.DeleteTracking    Script Date: 08/09/2006 12:22:41 ******/

CREATE PROCEDURE [dbo].[DeleteTracking] @TrackingID int
AS
Delete From Tracking
Where TrackingID = @TrackingID




GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteTracking] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteTracking] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteTracking] TO [sp_executeall]
GO
