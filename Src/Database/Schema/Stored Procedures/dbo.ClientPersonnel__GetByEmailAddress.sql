SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnel table through based on email address
-- and default third party system id
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnel__GetByEmailAddress]
(
	@EmailAddress varchar (255)  
)
AS
BEGIN

	RAISERROR('This procedure [ClientPersonnel__GetByEmailAddress] is no longer active. Please contact support!', 16, 1)
	RETURN
	
/*
	SELECT
		[ClientPersonnelID],
		[ClientID],
		[ClientOfficeID],
		[TitleID],
		[FirstName],
		[MiddleName],
		[LastName],
		[JobTitle],
		[Password],
		[ClientPersonnelAdminGroupID],
		[MobileTelephone],
		[HomeTelephone],
		[OfficeTelephone],
		[OfficeTelephoneExtension],
		[EmailAddress],
		[ChargeOutRate],
		[UserName],
		[Salt],
		[AttemptedLogins],
		[AccountDisabled],
		[ManagerID],
		[Initials],
		[LanguageID],
		[SubClientID],
		[ForcePasswordChangeOn],
		[ThirdPartySystemId],
		[CustomerID]
	FROM
		[dbo].[ClientPersonnel] WITH (NOLOCK)
	WHERE
		[EmailAddress] = @EmailAddress
	SELECT @@ROWCOUNT
					
			
*/
END




GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetByEmailAddress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel__GetByEmailAddress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel__GetByEmailAddress] TO [sp_executeall]
GO
