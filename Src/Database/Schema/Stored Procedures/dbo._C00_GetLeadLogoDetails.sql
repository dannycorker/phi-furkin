SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-05-28
-- Description:	Get Lead Logo etc Details For Part 4 Client 356
-- CS 2015-11-17	Impose LeadType restriction on SqlQuery to allow varying config by lead type
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetLeadLogoDetails]
	@ClientID			INT,
	@ClientPersonnelID	INT,
	@LeadID				INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE  @LeadTypeID			INT
			,@DetailFieldID			INT
			,@DetailFieldSubtypeID	INT
			,@ImageURL				VARCHAR(2000)
			,@ObjectID				INT
			,@MatterID				INT
			,@SQLQuery				VARCHAR(MAX)

	SELECT @LeadTypeID = l.LeadTypeID
	FROM Lead l WITH ( NOLOCK ) 
	WHERE l.LeadID = @LeadID

	SELECT	 @DetailFieldID			= ISNULL(fm.ColumnFieldID,fm.DetailFieldID)
			,@DetailFieldSubtypeID	= df.LeadOrMatter
	FROM ThirdPartyFieldMapping fm WITH ( NOLOCK )
	INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = fm.DetailFieldID
	WHERE fm.ThirdPartyFieldID = 4211 /*LeadInfoImageUrl*/
	AND fm.LeadTypeID = @LeadTypeID
	AND fm.ClientID = @ClientID 
	AND fm.ThirdPartyFieldGroupID = 97 /*LeadInfoLogo*/

	/*Pick up the first Matter.  From the matter record we can get to customer, lead, case, client etc*/
	SELECT TOP(1) @ObjectID =	CASE @DetailFieldSubtypeID
									WHEN 1  THEN m.LeadID
									WHEN 2  THEN m.MatterID
									WHEN 10 THEN m.CustomerID
									WHEN 11 THEN m.CaseID
									WHEN 12 THEN m.ClientID
									WHEN 13 THEN @ClientPersonnelID
								END 
				,@MatterID	=	m.MatterID
	FROM Matter m WITH ( NOLOCK ) 
	WHERE m.LeadID = @LeadID
	ORDER BY m.MatterID

	/*Pick the Image URL from the mapped field*/
	SELECT @ImageURL = dbo.fnGetSimpleDv(@DetailFieldID, @ObjectID)

	/*Output the Image URL as its own dataset*/
	SELECT @ImageURL [ImageUrl], NULL [MaxWidth], NULL [MaxHeight] /*App Defaults to 200x200 if NULL is presented*/
	
	SELECT @SQLQuery = sq.QueryText
	FROM SqlQuery sq WITH (NOLOCK)
	INNER JOIN ThirdPartyMappingQuery t WITH (NOLOCK) ON t.QueryID = sq.QueryID
	WHERE t.ClientID = @ClientID
	AND t.ThirdPartyFieldGroupID = 86
	AND (sq.LeadTypeID = @LeadTypeID or sq.LeadTypeID is NULL) /*CS 2015-11-17*/
	
	IF @SQLQuery IS NOT NULL
	BEGIN
	
		SELECT @SQLQuery = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@SQLQuery, '@CustomerID', m.CustomerID), '@LeadID', m.LeadID), '@CaseID', m.CaseID), '@MatterID', m.MatterID), '@ClientID', m.ClientID), '@ClientPersonnelID', @ClientPersonnelID)
		FROM Matter m WITH (NOLOCK) 
		WHERE m.MatterID = @MatterID
	
		EXEC (@SQLQuery)
	
	END
	ELSE
	BEGIN

		/*Output any additional information fields.  Use the first matter for this lead as default*/
		;WITH AdditionalFields AS
		(
			SELECT	 df.FieldCaption
					,CASE df.LeadOrMatter 
						WHEN 1  THEN dbo.fnGetSimpleDvLuliOrValue(df.DetailFieldID,m.LeadID)
						WHEN 2  THEN dbo.fnGetSimpleDvLuliOrValue(df.DetailFieldID,m.MatterID)
						WHEN 10 THEN dbo.fnGetSimpleDvLuliOrValue(df.DetailFieldID,m.CustomerID)
						WHEN 11 THEN dbo.fnGetSimpleDvLuliOrValue(df.DetailFieldID,m.CaseID)
						WHEN 12 THEN dbo.fnGetSimpleDvLuliOrValue(df.DetailFieldID,m.ClientID)
						WHEN 13 THEN dbo.fnGetSimpleDvLuliOrValue(df.DetailFieldID,@ClientPersonnelID)
						ELSE '' 
					 END [FieldValue]
					,df.FieldOrder
					,df.QuestionTypeID
			FROM ThirdPartyFieldMapping fm WITH ( NOLOCK ) 
			INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = fm.DetailFieldID
			INNER JOIN Matter m WITH ( NOLOCK ) on m.MatterID = @MatterID
			WHERE fm.LeadTypeID = @LeadTypeID
			AND fm.ClientID = @ClientID 
			AND fm.ThirdPartyFieldID = 4212 /*LeadInfoImageAdditionalInfoField*/
			AND fm.ThirdPartyFieldGroupID = 97 /*LeadInfoLogo*/
		)
		SELECT	 af.FieldCaption
				,CASE 
				 WHEN af.QuestionTypeID = 5 AND ISDATE(af.FieldValue)=1 THEN CONVERT(VARCHAR,CAST(af.FieldValue as DATE),103) 
				 ELSE af.FieldValue 
				 END [FieldValue]
		FROM AdditionalFields af
		ORDER BY af.FieldOrder

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetLeadLogoDetails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetLeadLogoDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetLeadLogoDetails] TO [sp_executeall]
GO
