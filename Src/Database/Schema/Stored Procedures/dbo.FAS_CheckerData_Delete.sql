SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the FAS_CheckerData table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_CheckerData_Delete]
(

	@FasCheckerDataID int   
)
AS


				DELETE FROM [dbo].[FAS_CheckerData] WITH (ROWLOCK) 
				WHERE
					[FasCheckerDataID] = @FasCheckerDataID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_CheckerData_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_CheckerData_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_CheckerData_Delete] TO [sp_executeall]
GO
