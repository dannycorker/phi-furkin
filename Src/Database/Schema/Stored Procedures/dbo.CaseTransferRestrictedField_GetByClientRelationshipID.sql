SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CaseTransferRestrictedField table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferRestrictedField_GetByClientRelationshipID]
(

	@ClientRelationshipID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[CaseTransferRestrictedFieldID],
					[ClientRelationshipID],
					[DetailFieldID]
				FROM
					[dbo].[CaseTransferRestrictedField] WITH (NOLOCK) 
				WHERE
					[ClientRelationshipID] = @ClientRelationshipID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferRestrictedField_GetByClientRelationshipID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferRestrictedField_GetByClientRelationshipID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferRestrictedField_GetByClientRelationshipID] TO [sp_executeall]
GO
