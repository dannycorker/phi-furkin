SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the WorkingDays table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkingDays_Get_List]

AS


				
				SELECT
					[WorkingDayID],
					[Year],
					[Month],
					[Day],
					[DayNumber],
					[IsWorkDay],
					[IsWeekDay],
					[IsBankHoliday],
					[Date],
					[WeekNumber],
					[DayNumberOfWeek],
					[CharDate]
				FROM
					[dbo].[WorkingDays] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkingDays_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkingDays_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkingDays_Get_List] TO [sp_executeall]
GO
