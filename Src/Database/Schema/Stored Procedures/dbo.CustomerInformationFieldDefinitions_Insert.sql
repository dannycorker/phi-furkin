SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the CustomerInformationFieldDefinitions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerInformationFieldDefinitions_Insert]
(

	@CustomerInformationFieldDefinitionID int    OUTPUT,

	@ClientQuestionnaireID int   ,

	@FieldAtBeginningOfQuestionnaire bit   ,

	@OrdinalPosition int   ,

	@FieldName varchar (255)  ,

	@FieldLabel varchar (900)  ,

	@Mandatory bit   ,

	@VerifyPhoneNumber bit   ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[CustomerInformationFieldDefinitions]
					(
					[ClientQuestionnaireID]
					,[FieldAtBeginningOfQuestionnaire]
					,[OrdinalPosition]
					,[FieldName]
					,[FieldLabel]
					,[Mandatory]
					,[VerifyPhoneNumber]
					,[ClientID]
					)
				VALUES
					(
					@ClientQuestionnaireID
					,@FieldAtBeginningOfQuestionnaire
					,@OrdinalPosition
					,@FieldName
					,@FieldLabel
					,@Mandatory
					,@VerifyPhoneNumber
					,@ClientID
					)
				-- Get the identity value
				SET @CustomerInformationFieldDefinitionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerInformationFieldDefinitions_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerInformationFieldDefinitions_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerInformationFieldDefinitions_Insert] TO [sp_executeall]
GO
