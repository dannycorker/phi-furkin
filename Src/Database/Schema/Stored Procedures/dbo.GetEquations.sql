SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetEquations] @ClientQuestionnaireID int
AS

SELECT     EquationID, EquationName, Equation, ClientQuestionnaireID, ClientID 
FROM         Equations
WHERE ClientQuestionnaireID = @ClientQuestionnaireID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetEquations] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEquations] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEquations] TO [sp_executeall]
GO
