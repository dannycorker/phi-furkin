SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DataLoaderFileStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileStatus_Insert]
(

	@DataLoaderFileStatusID int   ,

	@FileStatusName varchar (250)  ,

	@FileStatusDescription varchar (2000)  
)
AS


				
				INSERT INTO [dbo].[DataLoaderFileStatus]
					(
					[DataLoaderFileStatusID]
					,[FileStatusName]
					,[FileStatusDescription]
					)
				VALUES
					(
					@DataLoaderFileStatusID
					,@FileStatusName
					,@FileStatusDescription
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileStatus_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileStatus_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileStatus_Insert] TO [sp_executeall]
GO
