SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Paul Richardson
-- Create date: 2006-08-09
-- Description:	Add MasterQuestion from eCatcher
-- PR  2013-10-23 Added QuestionPossibleAnswerSortOrder
-- JWG 2014-01-23 #25390 Set WhoModified etc
-- =============================================
CREATE PROCEDURE [dbo].[AddMasterQuestion]

	@ClientQuestionnaireID INT,
	@QuestionTypeID INT,
	@QuestionText TEXT,
	@TextboxHeight INT,
	@QuestionOrder INT,
	@DefaultAnswerID INT,
	@Mandatory BIT,
	@QuestionToolTip NVARCHAR(255),
	@Active BIT,
	@MasterQuestionStatus INT, 
	@ClientID INT,
	@AnswerPosition INT,
	@DisplayAnswerAs INT,
	@NumberOfAnswersPerRow INT,
	@QuestionPossibleAnswerSortOrder INT, 
	@WhoEdited INT 

AS
BEGIN

	INSERT INTO MasterQuestions (ClientQuestionnaireID, QuestionTypeID, TextboxHeight, QuestionText, QuestionOrder, DefaultAnswerID, Mandatory,QuestionToolTip, Active, MasterQuestionStatus, ClientID, AnswerPosition, DisplayAnswerAs, NumberOfAnswersPerRow, QuestionPossibleAnswerSortOrder, WhoCreated, WhenCreated, WhoModified, WhenModified)
	VALUES (@ClientQuestionnaireID, @QuestionTypeID, @TextboxHeight, @QuestionText, @QuestionOrder, @DefaultAnswerID, @Mandatory, @QuestionToolTip, @Active, @MasterQuestionStatus, @ClientID, @AnswerPosition, @DisplayAnswerAs, @NumberOfAnswersPerRow, @QuestionPossibleAnswerSortOrder, @WhoEdited, dbo.fn_GetDate_Local(), @WhoEdited, dbo.fn_GetDate_Local())


	DECLARE @MasterQuestionID INT

	SET @MasterQuestionID = SCOPE_IDENTITY()

	SELECT MasterQuestionID, ClientQuestionnaireID, QuestionTypeID, TextboxHeight, QuestionText, QuestionOrder, DefaultAnswerID, Mandatory,QuestionToolTip, Active, MasterQuestionStatus, ClientID, AnswerPosition, DisplayAnswerAs, NumberOfAnswersPerRow
	FROM MasterQuestions
	WHERE MasterQuestionID = @MasterQuestionID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[AddMasterQuestion] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddMasterQuestion] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddMasterQuestion] TO [sp_executeall]
GO
