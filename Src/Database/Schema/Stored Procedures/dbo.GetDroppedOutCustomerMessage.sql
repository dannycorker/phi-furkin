SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetDroppedOutCustomerMessage] @ClientQuestionnaireID int

as

Select DroppedOutCustomerMessageID, ClientID, ClientQuestionnaireID, EmailFromAddress, EmailSubject, Email, SmsMessage
From DroppedOutCustomerMessages
Where ClientQuestionnaireID = @ClientQuestionnaireID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetDroppedOutCustomerMessage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetDroppedOutCustomerMessage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetDroppedOutCustomerMessage] TO [sp_executeall]
GO
