SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[DeleteCustomerOutcomes] @CustomerID int, @ClientQuestionnaireID int

AS

Delete From CustomerOutcomes
Where ((CustomerID = @CustomerID) And (ClientQuestionnaireID = @ClientQuestionnaireID))



GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteCustomerOutcomes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteCustomerOutcomes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteCustomerOutcomes] TO [sp_executeall]
GO
