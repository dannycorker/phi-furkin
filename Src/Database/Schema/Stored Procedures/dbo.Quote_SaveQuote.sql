SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2018-06-01
-- Description:	Save a Quote record and return the ID
-- CPS 2018-06-15 added TrackingCode for ticket #49975
-- CPS 2018-06-27 added SignupMethodID
-- CPS 2018-07-23 added TrackingCodeResourceListID
-- GPR 2019-10-10 created for Aquarius603Dev / LPC-36
-- =============================================
CREATE PROCEDURE [dbo].[Quote_SaveQuote]
(
	 @QuoteSessionID	INT
	,@MatterID			INT
	,@PetCount			INT
	,@QuoteXml			XML
	,@ResponseXml		XML
	,@WhoCreated		INT
	,@SourceID			INT
	,@ChangeNotes		VARCHAR(2000)
	,@TrackingCode		VARCHAR(2000) = NULL
	,@SignUpMethodID	INT = NULL
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE  @QuoteID					INT
			,@WhenCreated				DATETIME = dbo.fn_GetDate_Local()
			,@TrackingResourceListID	INT

	--CPS 2018-07-23 - Quote and Buy can only pass the tracking code, but that's horrible to report against, so decode it to the ResourceListID here
	--SELECT TOP (1) @TrackingResourceListID = tc.ResourceListID
	--FROM v_C433_TrackingCodes tc WITH ( NOLOCK ) 
	--WHERE tc.TokenCode = @TrackingCode
	--AND @TrackingCode <> ''

	/*GPR 2019-10-10*/
	SELECT @TrackingResourceListID = fn.ResourceListID
	FROM dbo.fn_C600_GetDetailsFromTrackingCode(@TrackingCode) fn		
	
	INSERT Quote ( [QuoteSessionID],[MatterID],[PetCount],[QuoteXml],[ResponseXml],[QuoteStartTime],[WhoCreated],[SourceID],[ChangeNotes],[TrackingCode],[TrackingResourceListID],[SignUpMethodID] )
	VALUES		 ( @QuoteSessionID,@MatterID,@PetCount,/*@QuoteXml*/ NULL,/*@ResponseXml*/ NULL,@WhenCreated,@WhoCreated,@SourceID,@ChangeNotes,@TrackingCode,@TrackingResourceListID,@SignUpMethodID ) -- CPS 2018-08-08 NULL out the response XML due to DB size limitations

	SELECT @QuoteID = SCOPE_IDENTITY()

	RETURN @QuoteID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_SaveQuote] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Quote_SaveQuote] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_SaveQuote] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_SaveQuote] TO [sp_executehelper]
GO
