SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 16-06-2016
-- Description:	Creates a history for the PurchasedProductPaymentSchedule and clears the PurchasedProductPaymentSchedule
-- Mods:
-- 2016-09-16 DCM Added PurchasedProductPaymentScheduleParentID & PurchasedProductPaymentScheduleTypeID
-- 2016-12-04 CS  Don't delete processed payment records
-- 2020-08-18 ACE PPET-73 Handle Transaction Fee
-- =============================================
CREATE PROCEDURE [dbo].[PurchasedProductPaymentScheduleHistory__Backup]
	@PurchasedProductID INT,	-- the product schedule to backup
	@UserID INT,				-- backup created by
	@Delete BIT					-- when true deletes the PurchasedProductPaymentSchedule
AS
BEGIN
		
	SET NOCOUNT ON;
	
	DECLARE @WhenCreated VARCHAR(10) = CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) -- current date time for when created
	DECLARE @ProductPaymentScheduleCount INT
	
	SELECT @ProductPaymentScheduleCount = COUNT(PurchasedProductPaymentScheduleID) 
	FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
	WHERE PurchasedProductID=@PurchasedProductID

	IF(@ProductPaymentScheduleCount>0)-- a schedule of payments already exists for this product, so perform backup
	BEGIN

		INSERT INTO PurchasedProductPaymentScheduleHistory (ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, OriginalPurchasedProductPaymentScheduleID, OriginalWhoCreated, OriginalWhenCreated, WhoCreated, WhenCreated, PurchasedProductPaymentScheduleParentID, PurchasedProductPaymentScheduleTypeID, TransactionFee)
		SELECT ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID,PurchasedProductPaymentScheduleID, WhoCreated, WhenCreated, @UserID, @WhenCreated, PurchasedProductPaymentScheduleParentID, PurchasedProductPaymentScheduleTypeID, TransactionFee
		FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
		WHERE PurchasedProductID = @PurchasedProductID   

	END
	
	IF(@Delete=1) 
	BEGIN

		DELETE FROM PurchasedProductPaymentSchedule
		WHERE PurchasedProductID = @PurchasedProductID   
		AND PaymentStatusID = 1 /*Don't delete processed payments*/

	END
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProductPaymentScheduleHistory__Backup] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PurchasedProductPaymentScheduleHistory__Backup] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProductPaymentScheduleHistory__Backup] TO [sp_executeall]
GO
