SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlQueryCriteriaSetMember table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteriaSetMember_Update]
(

	@SqlQueryCriteriaSetMemberID int   ,

	@ClientID int   ,

	@SqlQueryID int   ,

	@SqlQueryCriteriaSetID int   ,

	@SqlQueryColumnID int   ,

	@Criteria1 varchar (250)  ,

	@Criteria2 varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlQueryCriteriaSetMember]
				SET
					[ClientID] = @ClientID
					,[SqlQueryID] = @SqlQueryID
					,[SqlQueryCriteriaSetID] = @SqlQueryCriteriaSetID
					,[SqlQueryColumnID] = @SqlQueryColumnID
					,[Criteria1] = @Criteria1
					,[Criteria2] = @Criteria2
				WHERE
[SqlQueryCriteriaSetMemberID] = @SqlQueryCriteriaSetMemberID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSetMember_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteriaSetMember_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteriaSetMember_Update] TO [sp_executeall]
GO
