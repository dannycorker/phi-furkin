SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientQuestionnairesVisitedButNotComplete table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnairesVisitedButNotComplete_Update]
(

	@ClientQuestionnairesVisitedButNotCompleteID int   ,

	@ClientQuestionnaireID int   ,

	@VisitedDate datetime   ,

	@IPAddress varchar (50)  ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientQuestionnairesVisitedButNotComplete]
				SET
					[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[VisitedDate] = @VisitedDate
					,[IPAddress] = @IPAddress
					,[ClientID] = @ClientID
				WHERE
[ClientQuestionnairesVisitedButNotCompleteID] = @ClientQuestionnairesVisitedButNotCompleteID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedButNotComplete_Update] TO [sp_executeall]
GO
