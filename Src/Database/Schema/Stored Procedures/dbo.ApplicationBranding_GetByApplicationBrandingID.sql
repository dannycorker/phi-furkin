SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ApplicationBranding table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ApplicationBranding_GetByApplicationBrandingID]
(

	@ApplicationBrandingID int   
)
AS


				SELECT
					[ApplicationBrandingID],
					[ClientID],
					[FileName],
					[VerticalSize],
					[HorizontalSize],
					[LeadManagerAlias],
					[LeadAlias],
					[LeadDetailsTabImageFileName]
				FROM
					[dbo].[ApplicationBranding] WITH (NOLOCK) 
				WHERE
										[ApplicationBrandingID] = @ApplicationBrandingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ApplicationBranding_GetByApplicationBrandingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ApplicationBranding_GetByApplicationBrandingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ApplicationBranding_GetByApplicationBrandingID] TO [sp_executeall]
GO
