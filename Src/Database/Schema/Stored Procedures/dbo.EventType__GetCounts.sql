SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------
-- Date Created: 29 May 2007

-- Created By:  Chris, Jim
-- Purpose: Return
-- Select all in-process events for the current Lead Type, as long as the user has access to them
-- but this time, only return event types they can EDIT. Exclude Hold events
-- SB  2014-07-10 Updated to use view which includes shared event types
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[EventType__GetCounts] 
(
	@LeadTypeID int,
	@UserID int
)
AS

	SET ANSI_NULLS OFF
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.ClientPersonnel WITH (NOLOCK) 
	WHERE ClientPersonnelID = @UserID

	SELECT 'InProcessEvents' as EventCategory ,COUNT(*) as [Count]
	FROM   dbo.fnEventTypeShared(@ClientID) et
	INNER JOIN fnEventTypeSecure(@UserID, @LeadTypeID) ON et.EventTypeID = fnEventTypeSecure.objectid
	WHERE  [Enabled]    =1
	   AND EventTypeID <> 9
	   AND LeadTypeid   = @LeadTypeID
	   AND InProcess    = 1
	
	UNION ALL

	SELECT 'OutOfProcessEvents' as EventCategory ,COUNT(*) as [Count]
	FROM   dbo.fnEventTypeShared(@ClientID) et
	INNER JOIN fnEventTypeSecure(@UserID, @LeadTypeID) ON et.EventTypeID = fnEventTypeSecure.objectid
	WHERE  [Enabled]    =1
	   AND EventTypeID <> 9
	   AND LeadTypeid   = @LeadTypeID
	   AND InProcess    = 0
	
	UNION ALL

	SELECT 'HoldEvents' as EventCategory,COUNT(*) as [Count]
	FROM   dbo.fnEventTypeShared(@ClientID) et
	INNER JOIN fnEventTypeSecure(@UserID, @LeadTypeID) ON et.EventTypeID = fnEventTypeSecure.objectid
	WHERE  [Enabled]   =1
	   AND EventTypeID = 9
	   AND LeadTypeID  = @LeadTypeID

	UNION ALL

	SELECT 'Notes' as EventCategory,COUNT(*) as [Count]
	FROM   fnNoteTypeShared(@ClientID) n
	WHERE  n.ClientID = @ClientID
	AND (n.LeadTypeID IS NULL OR n.LeadTypeID = @LeadTypeID)

	SELECT @@ROWCOUNT
	SET ANSI_NULLS ON












GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetCounts] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetCounts] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetCounts] TO [sp_executeall]
GO
