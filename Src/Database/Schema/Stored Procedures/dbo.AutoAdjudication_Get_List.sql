SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the AutoAdjudication table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudication_Get_List]

AS


				
				SELECT
					[AutoAdjudicationID],
					[EventChoiceID],
					[AutoAdjudicationTypeID],
					[ObjectID],
					[AutoAdjudicationFieldTypeID],
					[Comparison],
					[Value1],
					[Value2],
					[AppliesTo],
					[Count],
					[ValueTypeID]
				FROM
					[dbo].[AutoAdjudication] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudication_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudication_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudication_Get_List] TO [sp_executeall]
GO
