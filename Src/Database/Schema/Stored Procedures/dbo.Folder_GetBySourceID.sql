SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Folder table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Folder_GetBySourceID]
(

	@SourceID int   
)
AS


				SELECT
					[FolderID],
					[ClientID],
					[FolderTypeID],
					[FolderParentID],
					[FolderName],
					[FolderDescription],
					[WhenCreated],
					[WhoCreated],
					[Personal],
					[SourceID]
				FROM
					[dbo].[Folder] WITH (NOLOCK) 
				WHERE
										[SourceID] = @SourceID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Folder_GetBySourceID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Folder_GetBySourceID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Folder_GetBySourceID] TO [sp_executeall]
GO
