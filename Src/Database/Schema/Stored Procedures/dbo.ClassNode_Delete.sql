SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClassNode table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClassNode_Delete]
(

	@ClassNodeID int   
)
AS


				DELETE FROM [dbo].[ClassNode] WITH (ROWLOCK) 
				WHERE
					[ClassNodeID] = @ClassNodeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNode_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode_Delete] TO [sp_executeall]
GO
