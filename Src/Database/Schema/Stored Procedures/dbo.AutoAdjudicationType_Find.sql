SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the AutoAdjudicationType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudicationType_Find]
(

	@SearchUsingOR bit   = null ,

	@AutoAdjudicationTypeID int   = null ,

	@AutoAdjudicationTypeName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AutoAdjudicationTypeID]
	, [AutoAdjudicationTypeName]
    FROM
	[dbo].[AutoAdjudicationType] WITH (NOLOCK) 
    WHERE 
	 ([AutoAdjudicationTypeID] = @AutoAdjudicationTypeID OR @AutoAdjudicationTypeID IS NULL)
	AND ([AutoAdjudicationTypeName] = @AutoAdjudicationTypeName OR @AutoAdjudicationTypeName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AutoAdjudicationTypeID]
	, [AutoAdjudicationTypeName]
    FROM
	[dbo].[AutoAdjudicationType] WITH (NOLOCK) 
    WHERE 
	 ([AutoAdjudicationTypeID] = @AutoAdjudicationTypeID AND @AutoAdjudicationTypeID is not null)
	OR ([AutoAdjudicationTypeName] = @AutoAdjudicationTypeName AND @AutoAdjudicationTypeName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudicationType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationType_Find] TO [sp_executeall]
GO
