SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 2006-08-09
-- Description:	Update a MasterQuestion from eCatcher
-- PR  2013-10-23 Added QuestionPossibleAnswerSortOrder
-- JWG 2014-01-23 #25390 Set WhoModified etc
-- =============================================
CREATE PROCEDURE [dbo].[UpdateMasterQuestion]

	@MasterQuestionID INT,
	@ClientQuestionnaireID INT,
	@QuestionTypeID INT,
	@TextboxHeight INT,
	@QuestionText TEXT,
	@QuestionOrder INT,
	@DefaultAnswerID INT,
	@Mandatory BIT,
	@QuestionToolTip NVARCHAR(255),
	@Active BIT,
	@MasterQuestionStatus INT,
	@ClientID INT,
	@AnswerPosition INT,
	@DisplayAnswerAs INT,
	@NumberOfAnswersPerRow INT,
	@QuestionPossibleAnswerSortOrder INT, 
	@WhoEdited INT

AS
BEGIN

	UPDATE MasterQuestions
	SET ClientQuestionnaireID = @ClientQuestionnaireID,
		QuestionTypeID = @QuestionTypeID,
		TextboxHeight = @TextboxHeight,
		QuestionText = @QuestionText,
		QuestionOrder = @QuestionOrder,
		DefaultAnswerID = @DefaultAnswerID,
		Mandatory = @Mandatory,
		QuestionToolTip = @QuestionToolTip,
		Active = @Active,
		MasterQuestionStatus = @MasterQuestionStatus,
		AnswerPosition = @AnswerPosition,
		DisplayAnswerAs = @DisplayAnswerAs,
		NumberOfAnswersPerRow = @NumberOfAnswersPerRow,
		ClientID = @ClientID,
		QuestionPossibleAnswerSortOrder = @QuestionPossibleAnswerSortOrder, 
		WhoModified = @WhoEdited, 
		WhenModified = dbo.fn_GetDate_Local()
	WHERE MasterQuestionID = @MasterQuestionID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateMasterQuestion] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateMasterQuestion] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateMasterQuestion] TO [sp_executeall]
GO
