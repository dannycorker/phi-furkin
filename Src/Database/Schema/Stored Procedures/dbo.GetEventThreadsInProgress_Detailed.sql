SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2014-03-03
-- Description:	Gets a list of InProcess events for the given CaseID, including EventSubtype detail
-- =============================================
CREATE PROCEDURE [dbo].[GetEventThreadsInProgress_Detailed]
	@ClientID int,
	@CaseID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
	le.LeadEventID AS [leadeventid], 
	et.eventtypeid, 
	et.eventtypename, 
	ec.threadnumber AS [threadnumber], 
	est.EventSubtypeID,
	est.EventSubtypeName,
	ec.Description, 
	ISNULL(le.IsOnHold, 0) AS IsOnHold, 
	le.HoldLeadEventID 
	FROM dbo.Cases c WITH (NOLOCK) 
	INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON c.caseid = le.caseid 
	INNER JOIN dbo.EventType et WITH (NOLOCK) ON le.eventtypeid = et.eventtypeid 
	INNER JOIN dbo.EventSubtype est WITH (NOLOCK) ON et.EventSubtypeID = est.EventSubtypeID
	INNER JOIN dbo.EventChoice ec WITH (NOLOCK) ON ec.EventTypeID = et.EventTypeID AND ec.EscalationEvent = 0
	INNER JOIN dbo.EventType net WITH (NOLOCK) ON net.EventTypeID = ec.NextEventTypeID 
	WHERE le.WhenFollowedUp IS NULL
	AND c.ClientID = @ClientID
	AND le.EventDeleted = 0 
	AND et.InProcess = 1 
	AND et.Enabled = 1 
	AND net.Enabled = 1
	AND c.CaseID = @caseid
	AND NOT EXISTS (
		/* 
			Partially followed-up event threads must not show event choices that have already happened
		*/
		SELECT * 
		FROM dbo.LeadEventThreadCompletion letc 
		WHERE letc.CaseID = @caseid
		AND letc.FromEventTypeID = le.EventTypeID
		AND letc.FromLeadEventID = le.LeadEventID
		AND letc.ThreadNumberRequired = ec.ThreadNumber
		AND letc.ToLeadEventID IS NOT NULL 
	)
	
	UNION
	
	/*
		Special case where there are no events for this case so far.
		Show all possible process-start events for this lead type.
	*/
	SELECT 
		-6 AS [leadeventid], 
		-6 AS [eventtypeid], 
		'Start' AS [eventtypename], 
		1 AS [threadnumber], 
		NULL AS [eventsubtypeid],
		NULL AS [eventsubtypename],
		'Start' AS [description], 
		0 AS IsOnHold, 
		NULL AS HoldLeadEventID 
	FROM dbo.EventType fet WITH (NOLOCK)
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadTypeID = fet.LeadTypeID 
	INNER JOIN dbo.Cases c WITH (NOLOCK) ON c.LeadID = l.LeadID 
	WHERE c.CaseID = @CaseID 
	AND c.ClientID = @ClientID
	AND fet.EventSubtypeID = 10 
	AND fet.AvailableManually = 1
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.LeadEvent le WITH (NOLOCK) 
		WHERE le.CaseID = @CaseID 
		AND le.EventDeleted = 0 
		AND le.EventTypeID IS NOT NULL
	)
	
	ORDER BY [leadeventid], [threadnumber]
END

GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventThreadsInProgress_Detailed] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEventThreadsInProgress_Detailed] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventThreadsInProgress_Detailed] TO [sp_executeall]
GO
