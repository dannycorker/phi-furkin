SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 07-11-2012
-- Description:	Deletes all the event type automated events that have the given event type id 
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeAutomatedEvent__DeleteByEventTypeID]

	@EventTypeID INT,
	@ClientID INT

AS
BEGIN

	DELETE FROM EventTypeAutomatedEvent
	WHERE EventTypeID=@EventTypeID AND ClientID=@ClientID	

END




GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedEvent__DeleteByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedEvent__DeleteByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedEvent__DeleteByEventTypeID] TO [sp_executeall]
GO
