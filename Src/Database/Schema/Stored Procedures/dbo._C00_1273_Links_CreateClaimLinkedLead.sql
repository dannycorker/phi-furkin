SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-09-29
-- Description:	Creates a linked claim lead from the policy side
-- 2017-06-12 CPS Add logging for ticket #43734
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Links_CreateClaimLinkedLead] 
(
	@PolicyLeadID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @LeadTypeRelationshipID INT
	
	DECLARE @ClientID INT,
			@CustomerID INT,
			@PolicyLeadTypeID INT

	SELECT @ClientID = ClientID, @CustomerID = CustomerID, @PolicyLeadTypeID = LeadTypeID
	FROM dbo.Lead WITH (NOLOCK)
	WHERE LeadID = @PolicyLeadID
	
	DECLARE @ClaimLeadTypeID INT
	SELECT @ClaimLeadTypeID = SharedTo
	FROM dbo.LeadTypeShare s WITH (NOLOCK)  
	WHERE ClientID = @ClientID
	AND s.SharedFrom = 1272
	
	DECLARE @AquariumAutomationID INT
	SELECT @AquariumAutomationID = dbo.fn_C00_GetAutomationUser(@ClientID)
	
	DECLARE @ClaimLeadID INT	
	EXEC @ClaimLeadID = dbo._C00_CreateNewLeadForCust @CustomerID, @ClaimLeadTypeID, @ClientID, @AquariumAutomationID, 1
	
	INSERT INTO dbo.LeadTypeRelationship (FromLeadTypeID, ToLeadTypeID, FromLeadID, ToLeadID, ClientID)
	VALUES (@PolicyLeadTypeID, @ClaimLeadTypeID, @PolicyLeadID, @ClaimLeadID, @ClientID)
	
	SELECT @LeadTypeRelationshipID = SCOPE_IDENTITY()
	EXEC _C00_LogIt 'Info', '_C600_LeadTypeRelationships', '_C00_1273_Links_CreateClaimLinkedLead', @LeadTypeRelationshipID, 58540 /*Cathal Sherry*/
	
	RETURN @ClaimLeadID

END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Links_CreateClaimLinkedLead] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Links_CreateClaimLinkedLead] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Links_CreateClaimLinkedLead] TO [sp_executeall]
GO
