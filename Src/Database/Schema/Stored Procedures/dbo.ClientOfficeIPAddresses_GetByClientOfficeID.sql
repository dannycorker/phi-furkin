SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientOfficeIPAddresses table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOfficeIPAddresses_GetByClientOfficeID]
(

	@ClientOfficeID int   
)
AS


				SELECT
					[ClientOfficeIpAddressID],
					[ClientOfficeID],
					[ClientOfficeIPAddress],
					[ClientID],
					[Notes]
				FROM
					[dbo].[ClientOfficeIPAddresses] WITH (NOLOCK) 
				WHERE
										[ClientOfficeID] = @ClientOfficeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeIPAddresses_GetByClientOfficeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOfficeIPAddresses_GetByClientOfficeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeIPAddresses_GetByClientOfficeID] TO [sp_executeall]
GO
