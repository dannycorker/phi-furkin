SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ReferenceValueSectionTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueSectionTypes_Insert]
(

	@ReferenceValueSectionTypeID int    OUTPUT,

	@SectionTypeName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[ReferenceValueSectionTypes]
					(
					[SectionTypeName]
					)
				VALUES
					(
					@SectionTypeName
					)
				-- Get the identity value
				SET @ReferenceValueSectionTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueSectionTypes_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueSectionTypes_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueSectionTypes_Insert] TO [sp_executeall]
GO
