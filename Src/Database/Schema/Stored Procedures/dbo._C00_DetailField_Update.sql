SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-07-13
-- Description:	Update Detail Fields, Specify a column name from the table DetailFields.
-- Updates:		
-- =============================================
CREATE PROCEDURE [dbo].[_C00_DetailField_Update]
(
	@DetailFieldID INT
	,@ColumnName VARCHAR(2000)
	,@ColumnValue VARCHAR(2000)
)
	
AS
BEGIN

	SET NOCOUNT ON	
	
DECLARE @ClientID int
,@LeadOrMatter tinyint
,@FieldName varchar(50)
,@FieldCaption varchar(100)
,@QuestionTypeID int
,@Required bit
,@Lookup bit
,@LookupListID int
,@LeadTypeID int
,@Enabled bit
,@DetailFieldPageID int
,@FieldOrder int
,@MaintainHistory bit
,@EquationText varchar(2000)
,@MasterQuestionID int
,@FieldSize int
,@LinkedDetailFieldID int
,@ValidationCriteriaFieldTypeID int
,@ValidationCriteriaID int
,@MinimumValue varchar(50)
,@MaximumValue varchar(50)
,@RegEx varchar(2000)
,@ErrorMessage varchar(250)
,@ResourceListDetailFieldPageID int
,@TableDetailFieldPageID int
,@DefaultFilter varchar(250)
,@ColumnEquationText varchar(2000)
,@Editable bit
,@Hidden bit
,@LastReferenceInteger int
,@ReferenceValueFormatID int
,@Encrypt bit
,@ShowCharacters int
,@NumberOfCharactersToShow int
,@TableEditMode int
,@DisplayInTableView bit
,@ObjectTypeID int
,@SourceID int
,@WhoCreated int
,@WhenCreated datetime
,@WhoModified int
,@WhenModified datetime
,@DetailFieldStyleID int
,@Hyperlink nvarchar(2000)
,@IsShared bit

/*Set everything to the value the Detailfield currently has*/
SELECT TOP 1
@ClientID = CASE WHEN @ColumnName = 'ClientID' 
	THEN @ColumnValue 
	ELSE df.ClientID END
,@LeadOrMatter = CASE WHEN @ColumnName = 'LeadOrMatter' 
	THEN @ColumnValue 
	ELSE df.LeadOrMatter END
,@FieldName = CASE WHEN @ColumnName = 'FieldName' 
	THEN @ColumnValue 
	ELSE df.FieldName END
,@FieldCaption = CASE WHEN @ColumnName = 'FieldCaption' 
	THEN @ColumnValue 
	ELSE  df.FieldCaption END
,@QuestionTypeID = CASE WHEN @ColumnName = 'QuestionTypeID' 
	THEN @ColumnValue 
	ELSE  df.QuestionTypeID END
,@Required  = CASE WHEN @ColumnName = 'Required' 
	THEN @ColumnValue 
	ELSE  df.Required END
,@Lookup  = CASE WHEN @ColumnName = 'Lookup' 
	THEN @ColumnValue 
	ELSE df.Lookup END
,@LookupListID  = CASE WHEN @ColumnName = 'LookupListID' 
	THEN @ColumnValue 
	ELSE df.LookupListID END
,@LeadTypeID  = CASE WHEN @ColumnName = 'LeadTypeID' 
	THEN @ColumnValue 
	ELSE df.LeadTypeID END
,@Enabled  = CASE WHEN @ColumnName = 'Enabled' 
	THEN @ColumnValue 
	ELSE df.Enabled END
,@DetailFieldPageID  = CASE WHEN @ColumnName = 'DetailFieldPageID' 
	THEN @ColumnValue 
	ELSE df.DetailFieldPageID END
,@FieldOrder  = CASE WHEN @ColumnName = 'FieldOrder' 
	THEN @ColumnValue 
	ELSE  df.FieldOrder END
,@MaintainHistory = CASE WHEN @ColumnName = 'MaintainHistory' 
	THEN @ColumnValue 
	ELSE df.MaintainHistory END
,@EquationText  = CASE WHEN @ColumnName = 'EquationText' 
	THEN @ColumnValue 
	ELSE df.EquationText END
,@MasterQuestionID = CASE WHEN @ColumnName = 'MasterQuestionID' 
	THEN @ColumnValue 
	ELSE df.MasterQuestionID END
,@FieldSize  = CASE WHEN @ColumnName = 'FieldSize' 
	THEN @ColumnValue 
	ELSE df.FieldSize END
,@LinkedDetailFieldID  = CASE WHEN @ColumnName = 'LinkedDetailFieldID' 
	THEN @ColumnValue 
	ELSE df.LinkedDetailFieldID END
,@ValidationCriteriaFieldTypeID  = CASE WHEN @ColumnName = 'ValidationCriteriaFieldTypeID' 
	THEN @ColumnValue 
	ELSE df.ValidationCriteriaFieldTypeID END
,@ValidationCriteriaID  =CASE WHEN @ColumnName = 'ValidationCriteriaID' 
	THEN @ColumnValue 
	ELSE  df.ValidationCriteriaID END
,@MinimumValue  = CASE WHEN @ColumnName = 'MinimumValue' 
	THEN @ColumnValue 
	ELSE df.MinimumValue END
,@MaximumValue  =CASE WHEN @ColumnName = 'MaximumValue' 
	THEN @ColumnValue 
	ELSE  df.MaximumValue END
,@RegEx  =CASE WHEN @ColumnName = 'RegEx' 
	THEN @ColumnValue 
	ELSE  df.RegEx END
,@ErrorMessage  =CASE WHEN @ColumnName = 'ErrorMessage' 
	THEN @ColumnValue 
	ELSE  df.ErrorMessage END
,@ResourceListDetailFieldPageID  =CASE WHEN @ColumnName = 'ResourceListDetailFieldPageID' 
	THEN @ColumnValue 
	ELSE  df.ResourceListDetailFieldPageID END
,@TableDetailFieldPageID  =CASE WHEN @ColumnName = 'TableDetailFieldPageID' 
	THEN @ColumnValue 
	ELSE  df.TableDetailFieldPageID END
,@DefaultFilter  =CASE WHEN @ColumnName = 'DefaultFilter' 
	THEN @ColumnValue 
	ELSE  df.DefaultFilter END
,@ColumnEquationText  =CASE WHEN @ColumnName = 'ColumnEquationText' 
	THEN @ColumnValue 
	ELSE  df.ColumnEquationText END
,@Editable  =CASE WHEN @ColumnName = 'Editable' 
	THEN @ColumnValue 
	ELSE  df.Editable END
,@Hidden  =CASE WHEN @ColumnName = 'Hidden' 
	THEN @ColumnValue 
	ELSE  df.Hidden END
,@LastReferenceInteger  =CASE WHEN @ColumnName = 'LastReferenceInteger' 
	THEN @ColumnValue 
	ELSE  df.LastReferenceInteger END
,@ReferenceValueFormatID  =CASE WHEN @ColumnName = 'ReferenceValueFormatID' 
	THEN @ColumnValue 
	ELSE  df.ReferenceValueFormatID END
,@Encrypt  =CASE WHEN @ColumnName = 'Encrypt' 
	THEN @ColumnValue 
	ELSE  df.Encrypt END
,@ShowCharacters  =CASE WHEN @ColumnName = 'ShowCharacters' 
	THEN @ColumnValue 
	ELSE  df.ShowCharacters END
,@NumberOfCharactersToShow  =CASE WHEN @ColumnName = 'NumberOfCharactersToShow' 
	THEN @ColumnValue 
	ELSE  df.NumberOfCharactersToShow END
,@TableEditMode  =CASE WHEN @ColumnName = 'TableEditMode' 
	THEN @ColumnValue 
	ELSE  df.TableEditMode END
,@DisplayInTableView  =CASE WHEN @ColumnName = 'DisplayInTableView' 
	THEN @ColumnValue 
	ELSE  df.DisplayInTableView END
,@ObjectTypeID  =CASE WHEN @ColumnName = 'ObjectTypeID' 
	THEN @ColumnValue 
	ELSE  df.ObjectTypeID END
,@SourceID  =CASE WHEN @ColumnName = 'SourceID' 
	THEN @ColumnValue 
	ELSE  df.SourceID END
,@WhoCreated  =CASE WHEN @ColumnName = 'WhoCreated' 
	THEN @ColumnValue 
	ELSE  df.WhoCreated END
,@WhenCreated  =CASE WHEN @ColumnName = 'WhenCreated' 
	THEN @ColumnValue 
	ELSE  df.WhenCreated END
,@WhoModified  =CASE WHEN @ColumnName = 'WhoModified' 
	THEN @ColumnValue 
	ELSE  df.WhoModified END
,@WhenModified  =CASE WHEN @ColumnName = 'WhenModified' 
	THEN @ColumnValue 
	ELSE  df.WhenModified END
,@DetailFieldStyleID  =CASE WHEN @ColumnName = 'DetailFieldStyleID' 
	THEN @ColumnValue 
	ELSE  df.DetailFieldStyleID END
,@Hyperlink = CASE WHEN @ColumnName = 'Hyperlink' 
	THEN @ColumnValue 
	ELSE  df.Hyperlink END
,@IsShared =CASE WHEN @ColumnName = 'IsShared' 
	THEN @ColumnValue 
	ELSE  df.IsShared END
FROM DetailFields df WITH (NOLOCK) WHERE df.DetailFieldID = @DetailFieldID

/* */
--DECLARE @sql nvarchar(max)
--SET @sql = ' DECLARE @' +  CAST(@columnName AS VARCHAR) + ' VARCHAR(2000)' -- + ' DECLARE @' +  CAST(@ColumnValue AS VARCHAR) + ' VARCHAR(2000)'
-- +' SELECT ' + '@'+ CAST(@columnName AS VARCHAR) + '= ' + CAST(@ColumnValue AS VARCHAR)
--EXEC(@sql)

EXECUTE DetailFields_Update @DetailFieldID,@ClientID,@LeadOrMatter,@FieldName,@FieldCaption,@QuestionTypeID,@Required,@Lookup,@LookupListID,@LeadTypeID,@Enabled,@DetailFieldPageID  ,@FieldOrder  ,@MaintainHistory  ,@EquationText  ,@MasterQuestionID,@FieldSize,@LinkedDetailFieldID,@ValidationCriteriaFieldTypeID  ,@ValidationCriteriaID ,@MinimumValue  ,@MaximumValue  ,@RegEx  ,@ErrorMessage  ,@ResourceListDetailFieldPageID  ,@TableDetailFieldPageID  ,@DefaultFilter  ,@ColumnEquationText  ,@Editable  ,@Hidden  ,@LastReferenceInteger  ,@ReferenceValueFormatID  ,@Encrypt  ,@ShowCharacters  ,@NumberOfCharactersToShow  ,@TableEditMode  ,@DisplayInTableView  ,@ObjectTypeID  ,@SourceID  ,@WhoCreated  ,@WhenCreated  ,@WhoModified,@WhenModified  ,@DetailFieldStyleID  ,@Hyperlink  ,@IsShared

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DetailField_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_DetailField_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DetailField_Update] TO [sp_executeall]
GO
