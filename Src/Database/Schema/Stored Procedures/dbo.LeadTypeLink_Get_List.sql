SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the LeadTypeLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypeLink_Get_List]

AS


				
				SELECT
					[LeadTypeLinkID],
					[ClientID],
					[LeadTypeID],
					[OutcomeID]
				FROM
					[dbo].[LeadTypeLink] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypeLink_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypeLink_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypeLink_Get_List] TO [sp_executeall]
GO
