SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 18-04-2013
-- Description:	Gets the first SMSQuestionGroup by SMSQuestionnaireID
-- =============================================
CREATE PROCEDURE [dbo].[SMSQuestionGroup__GetFirstGroup]
(
	@SMSQuestionnaireID INT   
)
AS

	SET ANSI_NULLS ON
				
	SELECT TOP 1 * FROM SMSQuestionGroup g WITH (NOLOCK) 	
	WHERE g.SMSQuestionnaireID = @SMSQuestionnaireID
	ORDER BY GroupOrder ASC
			
	SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup__GetFirstGroup] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroup__GetFirstGroup] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup__GetFirstGroup] TO [sp_executeall]
GO
