SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2016-10-19
-- Description:	Gets the products for a pet
-- Updates:		2017-01-31	SB	Force API calls to stop
--				2017-07-04 CPS	Make sure at least one of start/end date is present
--				2018-05-01 GPR  Exclude One Month Free (C600 Buddies)
--				2018-11-22 UAH	In final SELECT, I added another join called mdvDisplay which displays IQB products if this field is ticked.
--				2018-11-29 AHOD	Updated Order By within select to change the order in which products are returned for Aggregators.  Added @SignUpMethod as a nullable parameter 
--				2019-01-31 GPR added Age/MinAge code for C600 54222 / 55082
-- 2020-01-13 CPS for JIRA LPC-356 | Removed old logging
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Policy_GetProducts]
(
	@PetIndex INT,
	@XmlRequest XML,
	@PetXml XML = NULL,
	@ProductID INT = NULL,
	@SignUpMethod INT = NULL
	
)
AS
BEGIN

	DECLARE @Username VARCHAR(2000)
	SELECT	@Username = @XmlRequest.value('(//Username)[1]', 'VARCHAR(2000)')

	DECLARE @ClientID INT,
			@BrandID INT,
			@RowCount INT
	SELECT	@ClientID = @XmlRequest.value('(//ClientId)[1]', 'INT'),
			@BrandID = @XmlRequest.value('(//BrandingId)[1]', 'INT')
	
	IF @PetXml IS NULL
	BEGIN
		;WITH PetData AS 
		(
			SELECT	r.c.query('.') AS Data, 
					ROW_NUMBER() OVER(ORDER BY r.c) as rn 
			FROM @XmlRequest.nodes('//PetQuote') r(c)
		)
		SELECT @PetXml = Data
		FROM PetData
		WHERE rn = @PetIndex
	END
			
	DECLARE @SpeciesID INT,
			@StartDate DATE
	SELECT	@SpeciesID = @PetXml.value('(//SpeciesId)[1]', 'INT'),
			@StartDate = @PetXml.value('(//StartDate)[1]', 'DATE')
PRINT @SpeciesID
	DECLARE @SchemeLeadTypeID INT
	SELECT @SchemeLeadTypeID = s.SharedTo
	FROM dbo.LeadTypeShare s WITH (NOLOCK) 
	WHERE s.SharedFrom = 1275
	AND s.ClientID = @ClientID
	
	
	
	/*GPR 2019-01-31 for 54222 / 55082*/
	DECLARE @BirthDate VARCHAR(200), @AgeInWeeks INT, @MinAge INT, @BreedID INT, @Response VARCHAR(20), @YearStart VARCHAR(200)

	/*GPR 2019-01-31 Get the Age of the Pet in request for 54222 / 55082*/
	SELECT	@BreedID = @XmlRequest.value('(//BreedId)[1]', 'INT') /*BreedID*/
	SELECT	@BirthDate = @XmlRequest.value('(//BirthDate)[1]', 'VARCHAR(200)') /*IQB - Pet Birth Date, don't need to consider EQB //BirthDate because EQB uses different SProc*/
	SELECT	@YearStart = @XmlRequest.value('(//StartDate)[1]', 'VARCHAR(200)')
	
	SELECT @BirthDate = CAST(@BirthDate AS DATE)
	SELECT @YearStart = CAST(@YearStart AS DATE)
	
	IF @BirthDate IS NULL SELECT @BirthDate=dbo.fn_GetDate_Local() /*Default Birth Date*/
	IF @StartDate IS NULL SELECT @StartDate=dbo.fn_GetDate_Local() /*Default Start Date*/

	SELECT @AgeInWeeks = DATEDIFF(DAY, @BirthDate, @YearStart) / 7 -- ALM 2021-04-07 FURKIN-348

	/*GPR 2019-01-31 Get the minimum accepted age by Breed for this Pet for 54222 / 55082*/
	/*ALM 2021-04-07 FURKIN-348 use Age In Weeks*/
	SELECT @MinAge = rdvBreedMinAge.ValueInt FROM dbo.ResourceList r WITH (NOLOCK)
	LEFT JOIN dbo.ResourceListDetailValues rdvBreedMinAge WITH (NOLOCK) ON r.ResourceListID = rdvBreedMinAge.ResourceListID AND rdvBreedMinAge.DetailFieldID = 315899--177293
	WHERE r.ResourceListID = @BreedID 
	AND r.ClientID = @ClientID
	AND rdvBreedMinAge.ClientID = @ClientID

	SELECT @PetIndex AS PetIndex, m.MatterID, m.LeadID, mdvScheme.ValueInt AS SchemeID, rdvProdname.DetailValue AS ProductName,	ISNULL(llType.ItemValue, '') AS SchemeType, 
	rdvPolDoc.DetailValue AS PolDoc, rdvInfDoc.DetailValue AS InfDoc, affinAffinBrand.DetailValue AS Brand, ISNULL(rdvItemName.DetailValue, '') AS ItemName
	FROM dbo.Matter m WITH (NOLOCK) 
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON m.LeadID = l.LeadID
	INNER JOIN dbo.MatterDetailValues mdvScheme WITH (NOLOCK) ON m.MatterID = mdvScheme.MatterID AND mdvScheme.DetailFieldID = 145689
	INNER JOIN MatterDetailValues mdvDisplay WITH (NOLOCK) ON mdvDisplay.MatterID = m.MatterID AND mdvdisplay.DetailFieldID = 180218
	INNER JOIN ResourceListDetailValues schemeAffinSC WITH (NOLOCK) ON mdvScheme.ValueInt=schemeAffinSC.ResourceListID AND schemeAffinSC.DetailFieldID=175269
	INNER JOIN ResourceListDetailValues affinAffinSC WITH (NOLOCK) ON schemeAffinSC.ValueInt=affinAffinSC.ValueInt AND affinAffinSC.DetailFieldID=170127
	INNER JOIN ResourceListDetailValues affinAffinBrand WITH (NOLOCK) ON affinAffinSC.ResourceListID=affinAffinBrand.ResourceListID AND affinAffinBrand.DetailFieldID=176965
	LEFT JOIN dbo.MatterDetailValues mdvStart WITH (NOLOCK) ON m.MatterID = mdvStart.MatterID AND mdvStart.DetailFieldID = 145690
	LEFT JOIN dbo.MatterDetailValues mdvEnd WITH (NOLOCK) ON m.MatterID = mdvEnd.MatterID AND mdvEnd.DetailFieldID = 145691
	INNER JOIN dbo.ResourceListDetailValues rdvType WITH (NOLOCK) ON mdvScheme.ValueInt = rdvType.ResourceListID AND rdvType.DetailFieldID = 144319
	LEFT JOIN dbo.LookupListItems llType WITH (NOLOCK) ON rdvType.ValueInt = llType.LookupListItemID
	INNER JOIN dbo.ResourceListDetailValues rdvProdname WITH (NOLOCK) ON mdvScheme.ValueInt = rdvProdname.ResourceListID AND rdvProdname.DetailFieldID = 146200
	LEFT JOIN dbo.ResourceListDetailValues rdvPolDoc WITH (NOLOCK) ON mdvScheme.ValueInt = rdvPolDoc.ResourceListID AND rdvPolDoc.DetailFieldID = 175860
	LEFT JOIN dbo.ResourceListDetailValues rdvInfDoc WITH (NOLOCK) ON mdvScheme.ValueInt = rdvInfDoc.ResourceListID AND rdvInfDoc.DetailFieldID = 175861
	LEFT JOIN dbo.ResourceListDetailValues rdvItemName WITH (NOLOCK) ON mdvScheme.ValueInt = rdvItemName.ResourceListID AND rdvItemName.DetailFieldID = 176966
	LEFT JOIN dbo.MatterDetailValues mdvPetType WITH (NOLOCK) on mdvPetType.MatterID = m.MatterID and mdvPetType.DetailFieldID = 177283
	WHERE l.LeadTypeID = @SchemeLeadTypeID
	AND m.ClientID = @ClientID
	AND affinAffinSC.ResourceListID = @BrandID
	AND (mdvStart.ValueDate IS NULL OR mdvStart.ValueDate <= @StartDate)
	AND (mdvEnd.ValueDate IS NULL OR mdvEnd.ValueDate >= @StartDate)
	AND (mdvStart.ValueDate is not NULL or mdvEnd.ValueDate is not NULL) /*CPS 2017-07-04 Make sure at least one date is present*/
	AND (mdvPetType.ValueInt IS NULL OR mdvPetType.ValueInt = @SpeciesID)
	AND (@ProductID IS NULL OR @ProductID = 0 OR mdvScheme.ValueInt = @ProductID)
	AND mdvScheme.ValueInt <> 151577 /*GPR 2018-05-01 Exclude One Month Free (C600 Buddies)*/
	AND mdvDisplay.ValueInt = 1
	AND @AgeInWeeks >= @MinAge /*GPR 2019-01-31 for this Pet for 54222 / 55082*/
 	ORDER BY CASE WHEN @SignUpMethod = 74564 AND rdvProdname.ResourceListID IN (151566,157872) THEN 1 ELSE 0 END, rdvProdname.DetailValue /*2018-11-29 AHOD If Aggregator or Desc*/
	/*ORDER BY rdvProdname.DetailValue*/ /*CPS 2017-05-31 PoC in advance of allowing CP to sort themselves*/ /*2018-11-*/
	SELECT @RowCount = @@ROWCOUNT
	
	declare @prodIdXml nvarchar(max) = (select CONCAT('<productid>' , @ProductID, '</productid>'))

	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_GetProducts] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_GetProducts] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_GetProducts] TO [sp_executeall]
GO
