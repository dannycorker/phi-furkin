SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 27/01/2014
-- Description:	Adds a client billing detail record for sentiment analysis
-- =============================================
CREATE PROCEDURE [dbo].[BillClientForSentimentAnalysis] 
	@ClientID INT,
	@ClientPersonnelID INT,
	@Message NVARCHAR(MAX),
	@MobileTelephone VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @ClientID = 2
	BEGIN
		INSERT INTO ClientBillingDetail(ClientID, SourceAquariumOptionID, ModuleID, WhenCreated, WhoCreated, ChargeInPence, Notes, MobileTelephone)
		VALUES (@ClientID, 62, 1,dbo.fn_GetDate_Local() ,@ClientPersonnelID, 0.0, @Message, @MobileTelephone)
	END
	ELSE
	BEGIN
		INSERT INTO ClientBillingDetail(ClientID, SourceAquariumOptionID, ModuleID, WhenCreated, WhoCreated, ChargeInPence, Notes, MobileTelephone)
		VALUES (@ClientID, 62, 1,dbo.fn_GetDate_Local() ,@ClientPersonnelID, 4.0, @Message, @MobileTelephone)	
	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[BillClientForSentimentAnalysis] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillClientForSentimentAnalysis] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillClientForSentimentAnalysis] TO [sp_executeall]
GO
