SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






----------------------------------------------------------------------------------------------------
-- Date Created: 28-Feb-2008

-- Created By:  Ben Crinion
-- Purpose: Copy all DocumentTypes to a NewLeadType and return the new DocumentTypeIDs
----------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[DocumentType__CopyDocumentTypesForLeadType]
(
	@ExistingLeadTypeID int,
	@NewLeadTypeID int,
	@NewClientID int
)
AS
        
	SET NOCOUNT ON
	
	-- Declare the mapping table for returning old to new document id mappings
	DECLARE @Mapping table
	(
		OldDocumentTypeID int,
		NewDocumentTypeID int
	)

	DECLARE
        @LoopDocumentTypeID		INT , -- 
		@rowID					INT , -- 
        @iLoopControl			INT
        -- Initialize variables!

    SELECT
        @iLoopControl = 1 
	
	SELECT
        @LoopDocumentTypeID = MIN(DocumentTypeID) FROM DocumentType where LeadTypeID = @ExistingLeadTypeID
	
	SELECT @rowID = @LoopDocumentTypeID

    -- Make sure the table has data.
    IF ISNULL(@LoopDocumentTypeID,0) = 0
    BEGIN
        --SELECT 'No DocumentTypes exist for this LeadType' 
		RETURN
    END

	-- copy 1 document at a time.
	WHILE( @iLoopControl = 1 )
	BEGIN

		INSERT INTO [DBO].[DocumentType]
			(
					[ClientID]                ,
					[LeadTypeID]              ,
					[DocumentTypeName]        ,
					[DocumentTypeDescription] ,
					[Header]                  ,
					[Template]                ,
					[Footer]                  ,
					[CanBeAutoSent]           ,
					[EmailSubject]            ,
					[EmailBodyText]           ,
					[InputFormat]             ,
					[OutputFormat]            ,
					[Enabled]                 ,
					[RecipientsTo]            ,
					[RecipientsCC]            ,
					[RecipientsBCC]           ,
					[ReadOnlyTo]              ,
					[ReadOnlyCC]              ,
					[ReadOnlyBCC],
					[SendToMultipleRecipients],
					[MultipleRecipientDataSourceType],
					[MultipleRecipientDataSourceID],
					[SendToAllByDefault],
					[ExcelTemplatePath],
					[DocumentTitleTemplate],
					[CreateNewVersionWhenSaved]
			)
		SELECT  @NewClientID              ,
				@NewLeadTypeID            ,
				[DocumentTypeName]        ,
				[DocumentTypeDescription] ,
				[Header]                  ,
				[Template]                ,
				[Footer]                  ,
				[CanBeAutoSent]           ,
				[EmailSubject]            ,
				[EmailBodyText]           ,
				[InputFormat]             ,
				[OutputFormat]            ,
				[Enabled]                 ,
				[RecipientsTo]            ,
				[RecipientsCC]            ,
				[RecipientsBCC]           ,
				[ReadOnlyTo]              ,
				[ReadOnlyCC]              ,
				[ReadOnlyBCC],
				[SendToMultipleRecipients],
				[MultipleRecipientDataSourceType],
				[MultipleRecipientDataSourceID],
				[SendToAllByDefault]	,
				[ExcelTemplatePath],
				[DocumentTitleTemplate],
				[CreateNewVersionWhenSaved]
				
		FROM    [DBO].[DocumentType] 
		WHERE	[DocumentTypeID] = @LoopDocumentTypeID
		
		-- insert the old and new IDs to return
		INSERT INTO @Mapping VALUES (@LoopDocumentTypeID, SCOPE_IDENTITY())

		-- Reset looping variables.
		SELECT @LoopDocumentTypeID = NULL
		
		-- get the next document type to copy
		SELECT  @LoopDocumentTypeID = MIN(DocumentTypeID)
		FROM    DocumentType
		WHERE   LeadTypeID = @ExistingLeadTypeID
				AND
				DocumentTypeID > @rowID

		SELECT @rowID = @LoopDocumentTypeID

		IF ISNULL(@LoopDocumentTypeID,0) = 0 -- if there are no more documents to copy
		BEGIN 
			SELECT @iLoopControl = 0
			BREAK
		END
	END -- end while loop
	SELECT * FROM @Mapping
RETURN




GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__CopyDocumentTypesForLeadType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType__CopyDocumentTypesForLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__CopyDocumentTypesForLeadType] TO [sp_executeall]
GO
