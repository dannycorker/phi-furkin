SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-09-22
-- Description:	Copy the temporary event group interface from one client to another
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CS_GiveEventGroupInterface]
		@DestinationClientID			INT,
		@DestinationRandTLeadTypeID		INT,
		@OriginatingClientPageID		INT = 17667,
		@OriginatingEventGroupPageID	INT = 17668,
		@OriginatingEventMemberPageID	INT = 17669
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets FROM
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--BEGIN TRAN
			
		DECLARE	@NewClientPageID int,
				@NewEventGroupPageID int,
				@NewEventMemberPageID int
				
		EXEC @NewClientPageID		= dbo._C00_CopyPage @OriginatingClientPageID, 1, @DestinationClientID
		EXEC @NewEventGroupPageID	= dbo._C00_CopyPage @OriginatingEventGroupPageID, @DestinationRandTLeadTypeID, @DestinationClientID
		EXEC @NewEventMemberPageID	= dbo._C00_CopyPage @OriginatingEventMemberPageID, @DestinationRandTLeadTypeID, @DestinationClientID

		DECLARE	@EventGroupName							varchar(50),
				@EventGroupID							int,
				@EventTypeName							varchar(50),
				@EventTypeID							int,
				@ClientID								int,
				@EventGroupMemberID						int,
				@AddorDeleteLULI						int,
				@EventGroupListTFID						int,
				@EventGroupListEventGroupNameDFID		int,
				@EventGroupListEventGroupDFID			int,
				@EventGroupListSAEDFID					int,
				@EventGroupListSAE						varchar(50),
				@EventTypeDFID							int,					
				@EventGroupDFID							int,					
				@AddOrDeleteDFID						int,
				@FeedbackDFID							int,
				@DeleteLULI								int,
				@AddLULI								int,
				@EventGroupMemberTFID					int,
				@EventGroupMemberDFPID					int,
				@EventGroupMemberEventGroupMemberDFID	int,
				@EventGroupMemberEventGroupDFID			int,
				@EventGroupMemberEventTypeDFID			int,
				@EventGroupMemberEventTypeNameDFID		varchar(50),
				@LookupListID							int

		SELECT @ClientID = @DestinationClientID

		SELECT	@EventTypeDFID			= [EventTypeID],
				@EventGroupDFID			= [EventGroupID],
				@AddOrDeleteDFID		= [Add or Delete],
				@FeedbackDFID			= [Event GroupFeedback],
				@EventGroupListTFID		= [Event Group List],
				@EventGroupMemberTFID	= [Event Group Members]
		FROM		
			(	
			SELECT df.FieldName, df.DetailFieldID
			FROM DetailFields df WITH (NOLOCK) 
			WHERE df.DetailFieldPageID = @NewClientPageID
			)
			AS ToPivot		
		PIVOT		
		(		
			Max(DetailFieldID)		
		FOR		
		[FieldName]
			IN ( [EventTypeID],[EventGroupID],[Add or Delete],[Event GroupFeedback],[Event Group List],[Event Group Members] )
		) AS Pivoted

		UPDATE df SET TableDetailFieldPageID = @NewEventGroupPageID
		FROM DetailFields df WITH (NOLOCK) WHERE df.DetailFieldID = @EventGroupListTFID

		SELECT @EventGroupListEventGroupDFID = df.DetailFieldID FROM DetailFields df WITH (NOLOCK)  
		WHERE df.DetailFieldPageID = @NewEventGroupPageID and df.FieldName = 'EventGroupID'

		SELECT @EventGroupListEventGroupNameDFID = df.DetailFieldID FROM DetailFields df WITH (NOLOCK) 
		WHERE df.DetailFieldPageID = @NewEventGroupPageID and df.FieldName = 'EventGroupName'

		SELECT @EventGroupListSAEDFID = df.DetailFieldID FROM DetailFields df WITH (NOLOCK) 
		WHERE df.DetailFieldPageID = @NewEventGroupPageID and df.FieldName = 'Post Update SQL'

		INSERT LookupList (LookupListName,LookupListDescription,ClientID,Enabled,SortOptionID)
		VALUES ('Add or Delete','Add or Delete',@ClientID,1,21)
		SELECT @LookupListID = SCOPE_IDENTITY()

		INSERT LookupListItems (LookupListID,ItemValue,ClientID,Enabled,SortOrder)
		VALUES (@LookupListID,'Add',@ClientID,1,0)
		SELECT @AddLULI = SCOPE_IDENTITY()

		INSERT LookupListItems (LookupListID,ItemValue,ClientID,Enabled,SortOrder)
		VALUES (@LookupListID,'Delete',@ClientID,1,0)
		SELECT @DeleteLULI = SCOPE_IDENTITY()

		UPDATE df SET LookupListID = @LookupListID FROM DetailFields df WITH (NOLOCK)  WHERE df.DetailFieldID = @AddOrDeleteDFID

		SELECT @EventGroupMemberDFPID = @NewEventMemberPageID

		UPDATE df SET TableDetailFieldPageID = @NewEventMemberPageID FROM DetailFields df  WHERE df.DetailFieldID = @EventGroupMemberTFID

		SELECT	@EventGroupMemberEventGroupMemberDFID = [EventGroupMemberID],
				@EventGroupMemberEventGroupDFID = [EventGroupID],
				@EventGroupMemberEventTypeDFID = [EventTypeID],
				@EventGroupMemberEventTypeNameDFID = [EventTypeName]
		FROM		
			(	
			SELECT df.FieldName, df.DetailFieldID
			FROM DetailFields df WITH (NOLOCK) 
			WHERE df.DetailFieldPageID = @NewEventMemberPageID
			)
			AS ToPivot		
		PIVOT		
		(		
			Max(DetailFieldID)		
		FOR		
		[FieldName]
			IN ( [EventGroupMemberID],[EventGroupID],[EventTypeID],[EventTypeName] )
		) AS Pivoted
		
		SELECT 'SAS appears on the messages tab...'
		
		SELECT eg.EventGroupID, eg.EventGroupName, em.EventGroupMemberID, em.EventTypeID
		FROM EventGroup eg WITH (NOLOCK) 
		INNER JOIN EventGroupMember em WITH (NOLOCK) on em.EventGroupID = eg.EventGroupID
		WHERE eg.ClientID = @ClientID
		
		SELECT 'PRINT ('' ELSE IF @AddorDeleteLULI = @DeleteLULI
						BEGIN
							IF @EventGroupMemberID > 0
							BEGIN
								DELETE em
								FROM EventGroupMember em 
								WHERE em.EventGroupMemberID = @EventGroupMemberID
								AND em.ClientID = @ClientID
								
								DELETE tr
								FROM TableRows tr WITH (NOLOCK) 
								WHERE tr.SourceID = @EventGroupMemberID
								and tr.DetailFieldID = @EventGroupMemberTFID
								SELECT @LogEntry = ''Deleted EventID '' + convert(varchar,@EventTypeID) + '' from GroupID '' + convert(varchar,@EventGroupID)
							END
							ELSE
							BEGIN
								SELECT @LogEntry = ''ERROR: Event '' + convert(varchar,@EventTypeID) + '' is not a member of Group'' + convert(varchar,@EventGroupID)
							END
						END
						ELSE
						BEGIN
							SELECT @LogEntry = ''ERROR: invalid selection.''
						END
						
					END
				END
				ELSE
				BEGIN
					SELECT @LogEntry = ''All details not filled in.  No update.'' + CHAR(10) + ''EventTypeID = '' + CONVERT(VARCHAR,@EventTypeID) + CHAR(10) + ''EventGroupID = '' + CONVERT(VARCHAR,@EventGroupID) + CHAR(10) + ''Lookuplist Selection = '' + CONVERT(VARCHAR,@AddorDeleteLULI) + ''@ClientID = '' + convert(varchar,@ClientID)
				END
				EXEC _C00_LogIt ''Info'', ''EventGroupManager'', ''Client-LevelSAS'', @LogEntry, @ClientPersonnelID
				EXEC dbo._C00_SimpleValueIntoField @FeedbackDFID, @LogEntry, @ClientID, @ClientPersonnelID
				EXEC dbo._C00_SimpleValueIntoField @AddOrDeleteDFID, '''', @ClientID, @ClientPersonnelID /*Clear out the lookup list selection*/
			END'  [I think some SAS gets trimmed]

		PRINT('DECLARE				@EventGroupName varchar(100),
				@EventGroupID int,
				@EventTypeName varchar(100),
				@EventTypeID int,
				@ClientID int = '								+ convert(varchar,isnull(@ClientID,'')) + ',
				@EventGroupMemberID int = '						+ convert(varchar,isnull(@EventGroupMemberID,'')) + ',
				@AddorDeleteLULI int = '						+ convert(varchar,isnull(@AddorDeleteLULI,'')) + ',
				@EventGroupListTFID int = '						+ convert(varchar,isnull(@EventGroupListTFID,'')) + ',
				@EventGroupListEventGroupNameDFID int = '		+ convert(varchar,isnull(@EventGroupListEventGroupNameDFID,'')) + ',
				@EventGroupListEventGroupDFID int = '			+ convert(varchar,isnull(@EventGroupListEventGroupDFID,'')) + ',
				@EventGroupListSAEDFID int = '					+ convert(varchar,isnull(@EventGroupListSAEDFID,'')) + ',
				@EventGroupListSAE varchar(100),
				@EventTypeDFID int = '							+ convert(varchar,isnull(@EventTypeDFID,'')) + ',
				@EventGroupDFID int = '							+ convert(varchar,isnull(@EventGroupDFID,'')) + ',
				@AddOrDeleteDFID int = '						+ convert(varchar,isnull(@AddOrDeleteDFID,'')) + ',
				@FeedbackDFID int = '							+ convert(varchar,isnull(@FeedbackDFID,'')) + ',
				@DeleteLULI int = '								+ convert(varchar,isnull(@DeleteLULI,'')) + ',
				@AddLULI int = '								+ convert(varchar,isnull(@AddLULI,'')) + ',
				@EventGroupMemberTFID int = '					+ convert(varchar,isnull(@EventGroupMemberTFID,'')) + ',
				@EventGroupMemberDFPID int = '					+ convert(varchar,isnull(@EventGroupMemberDFPID,'')) + ',
				@EventGroupMemberEventGroupMemberDFID int = '	+ convert(varchar,isnull(@EventGroupMemberEventGroupMemberDFID,'')) + ',
				@EventGroupMemberEventGroupDFID int = '			+ convert(varchar,isnull(@EventGroupMemberEventGroupDFID,'')) + ',
				@EventGroupMemberEventTypeDFID int = '			+ convert(varchar,isnull(@EventGroupMemberEventTypeDFID,'')) + ',
				@EventGroupMemberEventTypeNameDFID int = '		+ convert(varchar,isnull(@EventGroupMemberEventTypeNameDFID,'')) + '
			IF @TableRowID > 0
			BEGIN
					SELECT @DetailFieldID = tr.DetailFieldID
					FROM TableRows tr WITH (NOLOCK) 
					WHERE tr.TableRowID = @TableRowID	
				IF @DetailFieldID = @EventGroupListTFID
				BEGIN
						SELECT @EventGroupName = tdv.DetailValue, @EventGroupID = tdv_id.ValueInt
						FROM TableDetailValues tdv WITH (NOLOCK) 
						LEFT JOIN dbo.TableDetailValues tdv_id WITH (NOLOCK) ON tdv_id.TableRowID = tdv.TableRowID AND tdv_id.DetailFieldID = @EventGroupListEventGroupDFID
						WHERE tdv.TableRowID = @TableRowID
						AND tdv.DetailFieldID = @EventGroupListEventGroupNameDFID
					IF @EventGroupID > 0
					BEGIN
						UPDATE eg
						SET EventGroupName = @EventGroupName
						FROM EventGroup eg WITH (NOLOCK) 
						WHERE eg.EventGroupID = @EventGroupID
					END
					ELSE
					BEGIN
						INSERT EventGroup (ClientID,EventGroupName,EventGroupDescription,WhoCreated,WhenCreated,WhoModified,WhenModified,ChangeNotes)
						VALUES (@ClientID,@EventGroupName,@EventGroupName,@ClientPersonnelID,CONVERT(VARCHAR,dbo.fn_GetDate_Local(),121),@ClientPersonnelID,CONVERT(VARCHAR,dbo.fn_GetDate_Local(),121),''Created by SAS'')
						SELECT @EventGroupID = SCOPE_IDENTITY()				
					END
						EXEC dbo._C00_SimpleValueIntoField @EventGroupListEventGroupDFID,     @EventGroupID,   @TableRowID, @ClientPersonnelID
						EXEC dbo._C00_SimpleValueIntoField @EventGroupListEventGroupNameDFID, @EventGroupName, @TableRowID, @ClientPersonnelID
					UPDATE tr
					SET DenyDelete = 1, DenyEdit = 1
					FROM TableRows tr WITH (NOLOCK) 
					WHERE tr.TableRowID = @TableRowID
				END
			END
			IF		(@MatterID = 0 OR @MatterID IS NULL)
				AND (@CustomerID = 0 OR @CustomerID IS NULL)
				AND (@LeadID = 0 OR @LeadID IS NULL)
				AND (@TableRowID = 0 OR @TableRowID IS NULL)
				AND (@ResourceListID = 0 OR @ResourceListID IS NULL)
			BEGIN		
				SELECT	@EventTypeID = ISNULL([EventTypeID],0),
						@EventGroupID = ISNULL([EventGroupID],0),
						@AddorDeleteLULI = ISNULL([ADD OR DELETE],0)
				FROM		
					(	
					SELECT df.FieldName, cdv.DetailValue 
					FROM DetailFields df WITH (NOLOCK) 
					LEFT JOIN dbo.ClientDetailValues cdv WITH (NOLOCK) ON cdv.ClientID = @ClientID AND cdv.DetailFieldID = df.DetailFieldID
					WHERE df.DetailFieldID IN(@EventTypeDFID,@EventGroupDFID,@AddOrDeleteDFID,@FeedbackDFID) 
					)
					AS ToPivot		
				PIVOT		
				(		
					MAX([DetailValue])		
				FOR		
				[FieldName]
					IN ( [EventTypeID],[EventGroupID],[ADD OR DELETE],[EVENT GroupFeedback] )
				) AS Pivoted

				IF @EventGroupID > 0 AND @EventTypeID > 0 AND @AddorDeleteLULI > 0
				BEGIN
					IF NOT EXISTS ( SELECT * FROM EventType et WITH (NOLOCK) WHERE et.ClientID = @ClientID AND et.EventTypeID = @EventTypeID )
					BEGIN
						SELECT @LogEntry = ''ERROR: '' + convert(varchar,@EventTypeID) + '' is not an EventTypeID''
					END
					ELSE 
					IF NOT EXISTS ( SELECT * FROM EventGroup eg WITH (NOLOCK) WHERE eg.ClientID = @ClientID AND eg.EventGroupID = @EventGroupID )
					BEGIN
						SELECT @LogEntry = ''ERROR: '' + convert(varchar,@EventGroupID) + '' is not an EventGroupID''
					END
					ELSE
					BEGIN
						SELECT @EventTypeName = et.EventTypeName
						FROM EventType et WITH (NOLOCK) 
						WHERE et.EventTypeID = @EventTypeID
					
						SELECT @EventGroupMemberID = em.EventGroupMemberID
						FROM EventGroupMember em WITH (NOLOCK) 
						WHERE em.EventGroupID = @EventGroupID 
						AND em.EventTypeID = @EventTypeID
					
						IF @AddorDeleteLULI = @AddLuli /*Add*/
						BEGIN
							IF @EventGroupMemberID > 0
							BEGIN
								SELECT @LogEntry = ''ERROR: Event is already a member of this group.  EventGroupMemberID '' + CONVERT(VARCHAR,@EventGroupMemberID)
							END
							ELSE
							BEGIN
								INSERT EventGroupMember (ClientID,EventGroupID,EventTypeID,EventGroupMemberDescription,WhoCreated,WhenCreated,WhoModified,WhenModified,ChangeNotes)
								VALUES (@ClientID,@EventGroupID,@EventTypeID,@EventTypeName,@ClientPersonnelID,CONVERT(VARCHAR,dbo.fn_GetDate_Local(),121),@ClientPersonnelID,CONVERT(VARCHAR,dbo.fn_GetDate_Local(),121),''Added by client-level SAS'')
									SELECT @EventGroupMemberID = SCOPE_IDENTITY()
								INSERT TableRows(ClientID,DetailFieldID,DetailFieldPageID,DenyDelete,DenyEdit,SourceID)
								SELECT em.ClientID,@EventGroupMemberTFID,@EventGroupMemberDFPID,1,1,em.EventGroupMemberID 
								FROM EventGroupMember em 
								WHERE em.EventGroupMemberID = @EventGroupMemberID
									SELECT @NewTableRowID = SCOPE_IDENTITY()
								INSERT TableDetailValues (TableRowID,DetailFieldID,DetailValue,ClientID)
								VALUES	(@NewTableRowID, @EventGroupMemberEventGroupMemberDFID, convert(varchar,@EventGroupMemberID),@ClientID),
										(@NewTableRowID, @EventGroupMemberEventGroupDFID, convert(varchar,@EventGroupID)			,@ClientID),
										(@NewTableRowID, @EventGroupMemberEventTypeDFID, convert(varchar,@EventTypeID)				,@ClientID),
										(@NewTableRowID, @EventGroupMemberEventTypeNameDFID, @EventTypeName							,@ClientID)
								SELECT @LogEntry = ''Added EventTypeID '' + convert(varchar,@EventTypeID) + '' to GroupID '' + convert(varchar,@EventGroupID) + '' ("'' + eg.EventGroupName + ''")''
								FROM EventGroup eg WITH (NOLOCK) 
								WHERE eg.EventGroupID = @EventGroupID
									SELECT @EventGroupListSAE = tdv_sae.DetailValue
									FROM TableDetailValues tdv_sae WITH (NOLOCK) 
									INNER JOIN TableDetailValues tdv_group WITH (NOLOCK) on tdv_group.TableRowID = tdv_sae.TableRowID
									WHERE tdv_group.ValueInt = @EventGroupID
									and tdv_group.DetailFieldID = @EventGroupListEventGroupDFID
									and tdv_sae.DetailFieldID = @EventGroupListSAEDFID
								IF @EventGroupListSAE > ''''
								BEGIN
									exec ets @ClientID, @EventTypeID, 1, null, @EventGroupListSAE
									SELECT @LogEntry = @LogEntry + '', with SAE''
								END
							END
						END
						ELSE IF @AddorDeleteLULI = @DeleteLULI
						BEGIN
							IF @EventGroupMemberID > 0
							BEGIN
								DELETE em
								FROM EventGroupMember em 
								WHERE em.EventGroupMemberID = @EventGroupMemberID
								AND em.ClientID = @ClientID
								
								DELETE tr
								FROM TableRows tr WITH (NOLOCK) 
								WHERE tr.SourceID = @EventGroupMemberID
								and tr.DetailFieldID = @EventGroupMemberTFID
								SELECT @LogEntry = ''Deleted EventID '' + convert(varchar,@EventTypeID) + '' from GroupID '' + convert(varchar,@EventGroupID)
							END
							ELSE
							BEGIN
								SELECT @LogEntry = ''ERROR: Event '' + convert(varchar,@EventTypeID) + '' is not a member of Group'' + convert(varchar,@EventGroupID)
							END
						END
						ELSE
						BEGIN
							SELECT @LogEntry = ''ERROR: invalid selection.''
						END
						
					END
				END
				ELSE
				BEGIN
					SELECT @LogEntry = ''All details not filled in.  No update.'' + CHAR(10) + ''EventTypeID = '' + CONVERT(VARCHAR,@EventTypeID) + CHAR(10) + ''EventGroupID = '' + CONVERT(VARCHAR,@EventGroupID) + CHAR(10) + ''Lookuplist Selection = '' + CONVERT(VARCHAR,@AddorDeleteLULI) + ''@ClientID = '' + convert(varchar,@ClientID)
				END
				EXEC _C00_LogIt ''Info'', ''EventGroupManager'', ''Client-LevelSAS'', @LogEntry, @ClientPersonnelID
				EXEC dbo._C00_SimpleValueIntoField @FeedbackDFID, @LogEntry, @ClientID, @ClientPersonnelID
				EXEC dbo._C00_SimpleValueIntoField @AddOrDeleteDFID, '''', @ClientID, @ClientPersonnelID /*Clear out the lookup list selection*/
			END')
		
--		COMMIT
	END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CS_GiveEventGroupInterface] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CS_GiveEventGroupInterface] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CS_GiveEventGroupInterface] TO [sp_executeall]
GO
