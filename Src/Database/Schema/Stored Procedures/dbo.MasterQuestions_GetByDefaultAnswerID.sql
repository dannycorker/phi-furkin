SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the MasterQuestions table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MasterQuestions_GetByDefaultAnswerID]
(

	@DefaultAnswerID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[MasterQuestionID],
					[ClientQuestionnaireID],
					[QuestionTypeID],
					[QuestionText],
					[QuestionOrder],
					[DefaultAnswerID],
					[Mandatory],
					[QuestionToolTip],
					[LinkedQuestionnaireMasterQuestionID],
					[Active],
					[MasterQuestionStatus],
					[TextboxHeight],
					[ClientID],
					[AnswerPosition],
					[DisplayAnswerAs],
					[NumberOfAnswersPerRow]
				FROM
					[dbo].[MasterQuestions] WITH (NOLOCK) 
				WHERE
					[DefaultAnswerID] = @DefaultAnswerID
				
				SELECT @@ROWCOUNT
				
			





GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions_GetByDefaultAnswerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MasterQuestions_GetByDefaultAnswerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestions_GetByDefaultAnswerID] TO [sp_executeall]
GO
