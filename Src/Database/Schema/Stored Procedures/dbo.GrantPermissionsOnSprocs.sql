SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2007-01-01
-- Description:	Grant limited permissions on tables, procs and functions to certain users & roles
-- JWG 2012-08-09 Include functions (scalar and table-valued)
-- CPS 2020-04-02 Remove specific grant of permissions to AquariumNet for tables that are no longer in scope
-- =============================================
CREATE PROCEDURE [dbo].[GrantPermissionsOnSprocs]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @stmnt varchar(500)

	DECLARE GrantCursor CURSOR DYNAMIC READ_ONLY FOR
		SELECT 'GRANT EXECUTE ON [' + ROUTINE_NAME + '] TO sp_executeall '
		FROM INFORMATION_SCHEMA.ROUTINES 
		WHERE ROUTINE_TYPE='PROCEDURE' 
		AND ROUTINE_SCHEMA = 'dbo'
		AND ROUTINE_NAME NOT IN ('Lead_Delete','Cases_Delete','Matter_Delete','Customers_Delete','am','ClientReportTableBuilder__ExecuteInstructions')
		ORDER BY 1

	OPEN GrantCursor

	FETCH RELATIVE 1 FROM GrantCursor INTO @stmnt

	WHILE @@FETCH_STATUS = 0
	BEGIN

		EXEC(@stmnt)

		FETCH NEXT FROM GrantCursor INTO @stmnt
		
	END

	CLOSE GrantCursor
	DEALLOCATE GrantCursor

	DECLARE GrantCursor CURSOR DYNAMIC READ_ONLY FOR
		SELECT 'GRANT ' + CASE WHEN DATA_TYPE = 'TABLE' THEN 'SELECT' ELSE 'EXECUTE' END  + ' ON [' + ROUTINE_NAME + '] TO sp_executeall '
		FROM INFORMATION_SCHEMA.ROUTINES 
		WHERE ROUTINE_TYPE='FUNCTION'
		AND ROUTINE_SCHEMA = 'dbo'
		ORDER BY 1

	OPEN GrantCursor

	FETCH RELATIVE 1 FROM GrantCursor INTO @stmnt

	WHILE @@FETCH_STATUS = 0
	BEGIN

		EXEC(@stmnt)

		FETCH NEXT FROM GrantCursor INTO @stmnt
		
	END

	CLOSE GrantCursor
	DEALLOCATE GrantCursor

	/* Scalar-valued functions */
	/* Select from all Aquarium views */
	DECLARE GrantCursor CURSOR DYNAMIC READ_ONLY FOR
		SELECT 'GRANT SELECT ON [' + TABLE_NAME + '] TO sp_executeall, [ReadOnly] '
		FROM INFORMATION_SCHEMA.TABLES  
		WHERE TABLE_TYPE = 'VIEW' 
		AND TABLE_SCHEMA = 'dbo'
		AND TABLE_NAME NOT LIKE 'vw_aspnet%'

	OPEN GrantCursor

	FETCH RELATIVE 1 FROM GrantCursor INTO @stmnt

	WHILE @@FETCH_STATUS = 0
	BEGIN

		EXEC(@stmnt)

		FETCH NEXT FROM GrantCursor INTO @stmnt
		
	END

	CLOSE GrantCursor
	DEALLOCATE GrantCursor


	/* Allow the new ReadOnly user access to view procs and functions */
	DECLARE GrantCursor CURSOR DYNAMIC READ_ONLY FOR
		SELECT 'GRANT VIEW DEFINITION ON [' + ROUTINE_NAME + '] TO sp_executeall, [ReadOnly] '
		FROM INFORMATION_SCHEMA.ROUTINES 
		WHERE ROUTINE_SCHEMA = 'dbo'

	OPEN GrantCursor

	FETCH RELATIVE 1 FROM GrantCursor INTO @stmnt

	WHILE @@FETCH_STATUS = 0
	BEGIN

		EXEC(@stmnt)

		FETCH NEXT FROM GrantCursor INTO @stmnt
		
	END

	CLOSE GrantCursor
	DEALLOCATE GrantCursor


	/* Allow everyone to see and use our own User-defined data typs */
	DECLARE GrantCursor CURSOR DYNAMIC READ_ONLY FOR
		SELECT 'GRANT EXEC, VIEW DEFINITION ON TYPE::' + st.name + ' TO sp_executeall, [ReadOnly] ' 
		FROM sys.types st 
		INNER JOIN sys.schemas ss ON ss.schema_id = st.schema_id 
		WHERE st.is_user_defined = 1 
		AND ss.name IN ('dbo', 'sys') 

	OPEN GrantCursor

	FETCH RELATIVE 1 FROM GrantCursor INTO @stmnt

	WHILE @@FETCH_STATUS = 0
	BEGIN

		EXEC(@stmnt)

		FETCH NEXT FROM GrantCursor INTO @stmnt
		
	END

	CLOSE GrantCursor
	DEALLOCATE GrantCursor

	/* Special cases eg where procs contain dynamic sql */
	GRANT EXECUTE ON dbo.fnIsDuplicate TO sp_executeall 
	
	/* Special cases where AquariumNet need to set IDENTITY_INSERT on for some tables */
	GRANT CONTROL ON dbo.ClientPersonnel to sp_executeall
	GRANT CONTROL ON dbo.ClientQuestionnaires to sp_executeall
	GRANT CONTROL ON dbo.LeadDocumentFS to sp_executeall
	GRANT CONTROL ON dbo.PortalUser to sp_executeall
	
	/* More special cases - This is not needed now as we are granting to AquariumNet below
	This is because we changed the scheduled tasks to run as a named user rather than using integrated
	
	BEGIN TRY
		GRANT INSERT ON dbo._C00_BankLoads TO [SchedulerUser]
	END TRY
	BEGIN CATCH
		PRINT 'GRANT INSERT ON dbo._C00_BankLoads TO [SchedulerUser] : No SchedulerUser found'
	END CATCH
	
	*/
	
	--BEGIN TRY
	--	GRANT INSERT ON dbo.CurrencyRateImport TO [AquariumNet]
	--	GRANT INSERT ON dbo._SchedulerLog TO [AquariumNet]
	--	GRANT INSERT ON dbo._C00_BankLoads TO [AquariumNet]
	--END TRY
	--BEGIN CATCH
	--	PRINT 'GRANT INSERT ON specific tables to AquariumNet failed.'
	--END CATCH
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[GrantPermissionsOnSprocs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GrantPermissionsOnSprocs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GrantPermissionsOnSprocs] TO [sp_executeall]
GO
