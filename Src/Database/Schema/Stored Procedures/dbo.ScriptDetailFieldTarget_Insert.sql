SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ScriptDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptDetailFieldTarget_Insert]
(

	@ScriptDetailFieldTargetID int    OUTPUT,

	@ScriptID int   ,

	@ClientID int   ,

	@Target varchar (250)  ,

	@DetailFieldID int   ,

	@ColumnFieldID int   ,

	@Format varchar (250)  ,

	@SectionID int   
)
AS


				
				INSERT INTO [dbo].[ScriptDetailFieldTarget]
					(
					[ScriptID]
					,[ClientID]
					,[Target]
					,[DetailFieldID]
					,[ColumnFieldID]
					,[Format]
					,[SectionID]
					)
				VALUES
					(
					@ScriptID
					,@ClientID
					,@Target
					,@DetailFieldID
					,@ColumnFieldID
					,@Format
					,@SectionID
					)
				-- Get the identity value
				SET @ScriptDetailFieldTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDetailFieldTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget_Insert] TO [sp_executeall]
GO
