SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the UserRightsDynamicEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserRightsDynamicEditing_Get_List]

AS


				
				SELECT
					[UserRightsDynamicEditingID],
					[ClientPersonnelID],
					[FunctionTypeID],
					[LeadTypeID],
					[ObjectID],
					[RightID]
				FROM
					[dbo].[UserRightsDynamicEditing] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamicEditing_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserRightsDynamicEditing_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserRightsDynamicEditing_Get_List] TO [sp_executeall]
GO
