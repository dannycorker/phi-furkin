SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Functions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Functions_Update]
(

	@FunctionID int   ,

	@OriginalFunctionID int   ,

	@FunctionName varchar (50)  ,

	@FunctionDescription varchar (2000)  ,

	@FunctionTypeID int   ,

	@ModuleID int   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Functions]
				SET
					[FunctionID] = @FunctionID
					,[FunctionName] = @FunctionName
					,[FunctionDescription] = @FunctionDescription
					,[FunctionTypeID] = @FunctionTypeID
					,[ModuleID] = @ModuleID
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
				WHERE
[FunctionID] = @OriginalFunctionID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Functions_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Functions_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Functions_Update] TO [sp_executeall]
GO
