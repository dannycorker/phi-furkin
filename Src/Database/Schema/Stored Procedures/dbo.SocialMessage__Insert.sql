SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-11-2015
-- Description:	Inserts a Social Message
-- =============================================
CREATE PROCEDURE [dbo].[SocialMessage__Insert]
	
	@ClientID INT, 
	@SocialTypeID INT, 
	@MessageID VARCHAR(250), 
	@Recipient VARCHAR(250), 
	@RecipientID VARCHAR(250), 
	@RecipientScreenName VARCHAR(250), 
	@SenderSocialCustomerID INT, 
	@SenderCustomerID INT, 
	@Sender VARCHAR(250), 
	@SenderID VARCHAR(250), 
	@SenderScreenName VARCHAR(250), 
	@MessageText  VARCHAR(2000), 
	@CreatedDate DATETIME,
	@LeadEventID INT, 
	@LeadDocumentID INT
	
AS
BEGIN
	
	SET NOCOUNT ON;

    INSERT INTO SocialMessage (ClientID, SocialTypeID, MessageID, Recipient, RecipientID, RecipientScreenName, SenderSocialCustomerID, SenderCustomerID, Sender, SenderID, SenderScreenName, MessageText, CreatedDate, LeadEventID, LeadDocumentID)
    VALUES (@ClientID, @SocialTypeID, @MessageID, @Recipient, @RecipientID, @RecipientScreenName, @SenderSocialCustomerID, @SenderCustomerID, @Sender, @SenderID, @SenderScreenName, @MessageText, @CreatedDate, @LeadEventID, @LeadDocumentID)
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[SocialMessage__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SocialMessage__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialMessage__Insert] TO [sp_executeall]
GO
