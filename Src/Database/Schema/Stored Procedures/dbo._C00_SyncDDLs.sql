SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex ELger
-- Create date: 2008-06-07
-- Description:	Syncs DDL's
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SyncDDLs]
(
	@MasterQuestionID int,
	@LookupListID int = null
)
	
AS
BEGIN

	SET NOCOUNT ON

	Declare	@myERROR int, 
			@ClientID int

	DECLARE @Answers TABLE 
	(
		MasterQuestionID int,
		ClientQuestionnaireID int,
		AnswerText varchar(1000),
		Branch int
		 
	);

	Select top 1 @ClientID = ClientID
	From QuestionPossibleAnswers
	Where MasterQuestionID IN (@MasterQuestionID)

	/* Insert into Temp table alphabetically */
	Insert Into @Answers (ClientQuestionnaireID, MasterQuestionID, AnswerText, Branch)
	select ClientQuestionnaireID, MasterQuestionID, AnswerText, Branch
	From QuestionPossibleAnswers
	Where MasterQuestionID IN (@MasterQuestionID)
	Order By AnswerText

	/* Delete Answers from Questionnaire */
	Delete From QuestionPossibleAnswers
	Where MasterQuestionID IN (@MasterQuestionID)

	/* Insert Answers Back into Questionnaire ordered by Answer Text */
	Insert Into QuestionPossibleAnswers (ClientQuestionnaireID, MasterQuestionID, AnswerText, Branch, LinkedQuestionnaireQuestionPossibleAnswerID,ClientID)
	Select ClientQuestionnaireID, MasterQuestionID, AnswerText, Branch, NULL, @ClientID
	From @Answers
	Order By AnswerText

	If @LookupListID IS NOT NULL
	Begin

		/* Add Missing Luli Values to DDL*/
		Insert into LookupListItems (LookupListID, ItemValue, ClientID, Enabled)
		Select @LookupListID, AnswerText, @ClientID, 1
		from QuestionPossibleAnswers
		Where MasterQuestionID = @MasterQuestionID
		And Not Exists(
		Select *
		From LookupListItems
		Where LookuplistID = @LookupListID
		and ItemValue = QuestionPossibleAnswers.AnswerText collate SQL_Latin1_General_CP1_CI_AS
		)

	End

	SELECT 'Success' as [Outcome]
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SyncDDLs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SyncDDLs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SyncDDLs] TO [sp_executeall]
GO
