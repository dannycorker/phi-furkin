SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











-- =============================================
-- Author:		Alex ELger
-- Create date: 2008-06-20
-- Description:	Insert Fields from Event comments etc into MDV or LDV
-- =============================================
CREATE PROCEDURE [dbo].[_C00_InsertCommentsAndDateToFields]
(
	@LeadEventID int,
	@CommentsField int,
	@DateField int,
	@WhoAuthorisedField int
)
	
AS
BEGIN

	SET NOCOUNT ON

	Declare	@myERROR int,
	@LeadID int,
	@LeadTypeID int,
	@ClientID int,
	@MatterID int,
	@LeadOrMatterFieldComments varchar(100),
	@LeadOrMatterFieldDate varchar(100),
	@LeadOrMatterFieldWhoAuthorised int,
	@Comments varchar(2000),
	@WhenCreated datetime,
	@WhoAuthorised varchar(1000)

	DECLARE @RequiredFields TABLE 
	(
		DetailFieldID int,
		LeadOrMatter int
	)

	INSERT @RequiredFields SELECT @CommentsField, ''
	UNION SELECT @DateField, ''
	UNION SELECT @WhoAuthorisedField, ''

	Select
	@LeadID = Lead.LeadID,
	@LeadTypeID = Lead.LeadTypeID,
	@ClientID = Lead.ClientID,
	@MatterID = Matter.MatterID,
	@Comments = LeadEvent.Comments,
	@WhenCreated = Leadevent.WhenCreated,
	@WhoAuthorised = ClientPersonnel.UserName
	from customers
	inner join lead on lead.customerid = customers.customerid
	inner join leadevent on lead.leadid = leadevent.leadid
	inner join Matter on Matter.LeadID = Lead.LeadID
	inner join ClientPersonnel on ClientPersonnel.ClientPersonnelID = LeadEvent.WhoCreated
	where leadevent.leadeventid = @LeadEventID 

	Select 
	@LeadOrMatterFieldComments = LeadOrMatter
	From DetailFields 
	Where DetailFieldID = @CommentsField

	Update @RequiredFields
	Set LeadOrMatter = @LeadOrMatterFieldComments
	Where DetailFieldID = @CommentsField

	Select 
	@LeadOrMatterFieldDate = LeadOrMatter
	From DetailFields 
	Where DetailFieldID = @DateField

	Update @RequiredFields
	Set LeadOrMatter = @LeadOrMatterFieldDate
	Where DetailFieldID = @DateField

	Select 
	@LeadOrMatterFieldWhoAuthorised = LeadOrMatter
	From DetailFields 
	Where DetailFieldID = @WhoAuthorisedField

	Update @RequiredFields
	Set LeadOrMatter = @LeadOrMatterFieldWhoAuthorised
	Where DetailFieldID = @WhoAuthorisedField

	if @LeadOrMatterFieldComments = 1
	Begin

		INSERT INTO LeadDetailValues (ClientID,LeadID,DetailFieldID,DetailValue)
		SELECT @ClientID,@LeadID,rf.DetailFieldID,'' 
		From @RequiredFields rf 
		WHERE NOT EXISTS (SELECT * FROM LeadDetailValues ldv WHERE ldv.LeadID = @LeadID AND ldv.DetailFieldID = rf.DetailFieldID) 
		And rf.LeadOrMatter = 1

		/* Insert Comments into Lead Feild*/
		Update LeadDetailValues 
		Set DetailValue = @Comments
		Where LeadID = @LeadID
		And DetailFieldID = @CommentsField

	end

	if @LeadOrMatterFieldComments = 2
	Begin

		INSERT INTO MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
		SELECT @ClientID,@LeadID,@MatterID,rf.DetailFieldID,'' 
		From @RequiredFields rf 
		WHERE NOT EXISTS (SELECT * FROM MatterDetailValues mdv WHERE mdv.MatterID = @MatterID AND mdv.DetailFieldID = rf.DetailFieldID) 
		And rf.LeadOrMatter = 2

		/* Insert Comments into Matter Feild*/
		Update MatterDetailValues 
		Set DetailValue = @Comments
		Where MatterID = @MatterID
		And DetailFieldID = @CommentsField

	END
	

	if @LeadOrMatterFieldDate = 1
	Begin

		INSERT INTO LeadDetailValues (ClientID,LeadID,DetailFieldID,DetailValue)
		SELECT @ClientID,@LeadID,rf.DetailFieldID,'' 
		From @RequiredFields rf 
		WHERE NOT EXISTS (SELECT * FROM LeadDetailValues ldv WHERE ldv.LeadID = @LeadID AND ldv.DetailFieldID = rf.DetailFieldID) 
		And rf.LeadOrMatter = 1

		/* Insert date into Lead Feild*/
		Update LeadDetailValues 
		Set DetailValue = @WhenCreated
		Where LeadID = @LeadID
		And DetailFieldID = @DateField

	end

	if @LeadOrMatterFieldDate = 2
	Begin

		INSERT INTO MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
		SELECT @ClientID,@LeadID,@MatterID,rf.DetailFieldID,'' 
		From @RequiredFields rf 
		WHERE NOT EXISTS (SELECT * FROM MatterDetailValues mdv WHERE mdv.MatterID = @MatterID AND mdv.DetailFieldID = rf.DetailFieldID) 
		And rf.LeadOrMatter = 2

		/* Insert Date into Matter Feild*/
		Update MatterDetailValues 
		Set DetailValue = @WhenCreated
		Where MatterID = @MatterID
		And DetailFieldID = @DateField

	END

	if @LeadOrMatterFieldWhoAuthorised = 1
	Begin

		INSERT INTO LeadDetailValues (ClientID,LeadID,DetailFieldID,DetailValue)
		SELECT @ClientID,@LeadID,rf.DetailFieldID,'' 
		From @RequiredFields rf 
		WHERE NOT EXISTS (SELECT * FROM LeadDetailValues ldv WHERE ldv.LeadID = @LeadID AND ldv.DetailFieldID = rf.DetailFieldID) 
		And rf.LeadOrMatter = 1

		/* Insert date into Lead Feild*/
		Update LeadDetailValues 
		Set DetailValue = @WhoAuthorised
		Where LeadID = @LeadID
		And DetailFieldID = @WhoAuthorisedField

	end

	if @LeadOrMatterFieldWhoAuthorised = 2
	Begin

		INSERT INTO MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
		SELECT @ClientID,@LeadID,@MatterID,rf.DetailFieldID,'' 
		From @RequiredFields rf 
		WHERE NOT EXISTS (SELECT * FROM MatterDetailValues mdv WHERE mdv.MatterID = @MatterID AND mdv.DetailFieldID = rf.DetailFieldID) 
		And rf.LeadOrMatter = 2

		/* Insert Date into Matter Feild*/
		Update MatterDetailValues 
		Set DetailValue = @WhoAuthorised
		Where MatterID = @MatterID
		And DetailFieldID = @WhoAuthorisedField

	END

END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InsertCommentsAndDateToFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_InsertCommentsAndDateToFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_InsertCommentsAndDateToFields] TO [sp_executeall]
GO
