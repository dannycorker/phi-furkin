SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2021-05-11
-- Description:	Sql After Event - TESTING: Braintree
-- 2021-05-11 CPS for JIRA FURKIN-479 | Find the next retry for this policy and update the PaymentGross, Net, and Tax with the values supplied
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_Testing_160536]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	PRINT OBJECT_NAME(@@ProcID)

	DECLARE /* Standard */
			 @EventTypeID					INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@EventComments					VARCHAR(2000) = ''
			,@ErrorMessage					VARCHAR(2000) = ''
			,@PaymentGross					DECIMAL(18,2) = 0.00
			,@PaymentTax					DECIMAL(18,2) = 0.00
			,@NextCustomerPaymentScheduleID	INT

/* ===================================================================================================================== */

	/*Select and assign Aquarium IDs using the LeadEventID*/
	SELECT TOP (1) 
		 @EventTypeID	= le.EventTypeID
		,@WhoCreated	= le.WhoCreated
		,@MatterID		= m.MatterID
		,@WhenCreated	= le.WhenCreated
	FROM dbo.LeadEvent le WITH (NOLOCK)
	INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
	INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
	WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */

	IF @EventTypeID = 160536 /*TESTING - Prepare Braintree Retry*/
	BEGIN
		SELECT	 @PaymentGross	= dbo.fnGetSimpleDvAsMoney(315930,@MatterID) /*Gross Value*/
				,@PaymentTax	= dbo.fnGetSimpleDvAsMoney(315931,@MatterID) /*Tax Value*/

		SELECT TOP (1)
				 @NextCustomerPaymentScheduleID = ppps.CustomerPaymentScheduleID
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
		INNER JOIN dbo.PurchasedProduct pp WITH (NOLOCK) on pp.PurchasedProductID = ppps.PurchasedProductID
		WHERE pp.ObjectID = @MatterID
		AND ppps.ActualCollectionDate > @WhenCreated
		AND ppps.PaymentStatusID = 5 /*Retry*/

		IF @@RowCount = 0 
		BEGIN
			SELECT @ErrorMessage = '<br><br><font color="red">Error: no "retry" PPPS records found for MatterID ' + CONVERT(VARCHAR,@MatterID) + ' with a collection date after ' + ISNULL(CONVERT(VARCHAR(10),@WhenCreated,121),'NULL') + '</font>'
			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN
		END
		ELSE
		BEGIN
			UPDATE cps
			SET  PaymentGross = @PaymentGross
				,PaymentVat	  = @PaymentTax
				,PaymentNet   = @PaymentGross - @PaymentTax
				,WhenModified = @WhenCreated
				,WhoModified  = @WhoCreated
			FROM CustomerPaymentSchedule cps WITH (NOLOCK) 
			WHERE cps.CustomerPaymentScheduleID = @NextCustomerPaymentScheduleID

			UPDATE ppps
			SET  PaymentGross = @PaymentGross
				,PaymentVat	  = @PaymentTax
				,PaymentNet   = @PaymentGross - @PaymentTax
				,WhenModified = @WhenCreated
				,WhoModified  = @WhoCreated
			FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
			WHERE ppps.CustomerPaymentScheduleID = @NextCustomerPaymentScheduleID

			SELECT @EventComments += CONVERT(VARCHAR,@@RowCount) + ' PPPS records set to $' + ISNULL(CONVERT(VARCHAR,@PaymentGross),'NULL') + ' for CustomerPaymentScheduleID ' + ISNULL(CONVERT(VARCHAR,@NextCustomerPaymentScheduleID),'NULL')

			EXEC _C00_SimpleValueIntoField 315930, '', @MatterID, @WhoCreated /*Gross Value*/
			EXEC _C00_SimpleValueIntoField 315931, '', @MatterID, @WhoCreated /*Tax Value*/

			EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1
		END
	END

	PRINT OBJECT_NAME(@@ProcID) + ' END'

END


GO
GRANT EXECUTE ON  [dbo].[_SAE_Testing_160536] TO [sp_executeall]
GO
