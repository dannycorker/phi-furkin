SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ValidationCriterias table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ValidationCriterias_Find]
(

	@SearchUsingOR bit   = null ,

	@ValidationCriteriaID int   = null ,

	@Criteria nvarchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ValidationCriteriaID]
	, [Criteria]
    FROM
	[dbo].[ValidationCriterias] WITH (NOLOCK) 
    WHERE 
	 ([ValidationCriteriaID] = @ValidationCriteriaID OR @ValidationCriteriaID IS NULL)
	AND ([Criteria] = @Criteria OR @Criteria IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ValidationCriteriaID]
	, [Criteria]
    FROM
	[dbo].[ValidationCriterias] WITH (NOLOCK) 
    WHERE 
	 ([ValidationCriteriaID] = @ValidationCriteriaID AND @ValidationCriteriaID is not null)
	OR ([Criteria] = @Criteria AND @Criteria is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriterias_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ValidationCriterias_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriterias_Find] TO [sp_executeall]
GO
