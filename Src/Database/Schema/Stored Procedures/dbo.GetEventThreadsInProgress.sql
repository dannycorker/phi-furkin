SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2008-02-07
-- Description:	Get all event threads in progress for a case
-- JWG 2010-03-12 Offer process start event even if a note event already exists.
-- SB  2014-07-25 Changed to use function for shared lead types
-- SLACKY-PERF-20190621- added (nolock) where missing
-- =============================================
CREATE PROCEDURE [dbo].[GetEventThreadsInProgress] 
	@CaseID INT 
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Cases WITH (NOLOCK) 
	WHERE CaseID = @CaseID
	
	
	/*
		Relevant list of in-process events.
		Excludes redundant events from expired threads
	*/
	SELECT le.LeadEventID AS [leadeventid], et.eventtypeid, et.eventtypename, ec.threadnumber AS [threadnumber], ec.Description, ISNULL(le.IsOnHold, 0) AS IsOnHold, le.HoldLeadEventID 
	FROM dbo.Cases c WITH (NOLOCK) 
	INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON c.caseid = le.caseid 
	INNER JOIN dbo.fnEventTypeShared(@ClientID) et ON le.eventtypeid = et.eventtypeid 
	INNER JOIN dbo.EventChoice ec WITH (NOLOCK) ON ec.EventTypeID = et.EventTypeID AND ec.EscalationEvent = 0
	INNER JOIN dbo.EventType net WITH (NOLOCK) ON net.EventTypeID = ec.NextEventTypeID 
	WHERE le.WhenFollowedUp IS NULL
	AND le.EventDeleted = 0 
	AND et.InProcess = 1 
	AND et.Enabled = 1 
	AND net.Enabled = 1
	AND c.CaseID = @caseid
	AND ec.ClientID = @ClientID
	AND NOT EXISTS (
		/* 
			Partially followed-up event threads must not show event choices that have already happened
		*/
		-- SLACKY-PERF-20190621- added (nolock) where missing
		SELECT * 
		FROM dbo.LeadEventThreadCompletion letc (NOLOCK)
		WHERE letc.CaseID = @caseid
		AND letc.FromEventTypeID = le.EventTypeID
		AND letc.FromLeadEventID = le.LeadEventID
		AND letc.ThreadNumberRequired = ec.ThreadNumber
		AND letc.ToLeadEventID IS NOT NULL 
	)
	
	UNION
	
	/*
		Special case where there are no events for this case so far.
		Show all possible process-start events for this lead type.
	*/
	SELECT -6 AS [leadeventid], -6 AS [eventtypeid], 'Start' AS [eventtypename], 1 AS [threadnumber], 'Start' AS [description], 0 AS IsOnHold, NULL AS HoldLeadEventID 
	FROM dbo.fnEventTypeShared(@ClientID) fet 
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadTypeID = fet.LeadTypeID 
	INNER JOIN dbo.Cases c WITH (NOLOCK) ON c.LeadID = l.LeadID 
	WHERE c.CaseID = @CaseID 
	AND fet.EventSubtypeID = 10 
	AND fet.AvailableManually = 1
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.LeadEvent le WITH (NOLOCK) 
		WHERE le.CaseID = @CaseID 
		AND le.EventDeleted = 0 
		AND le.EventTypeID IS NOT NULL
	)
	
	ORDER BY [leadeventid], [threadnumber]
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventThreadsInProgress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEventThreadsInProgress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventThreadsInProgress] TO [sp_executeall]
GO
