SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 09-01-2014
-- Description:	Gets a pending account record by the clientpersonnelID
-- =============================================
CREATE PROCEDURE [dbo].[PendingAccount__GetByGuid]

	@Guid VARCHAR(36)

AS
BEGIN
	
	
	SET NOCOUNT ON;

	SELECT * FROM PendingAccount pa WITH (NOLOCK) WHERE pa.[GUID] = @Guid

END


GO
GRANT VIEW DEFINITION ON  [dbo].[PendingAccount__GetByGuid] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PendingAccount__GetByGuid] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PendingAccount__GetByGuid] TO [sp_executeall]
GO
