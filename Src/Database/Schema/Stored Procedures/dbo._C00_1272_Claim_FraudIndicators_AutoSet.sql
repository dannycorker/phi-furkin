SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Robin Hall
-- Create date: 2014-02-14
-- Description:	Automatic setting of fraud indicators
-- ROH 2015-06-15 Correction to UserID for adding note 
-- JL 2016-09-10 Removed 60 Days from inception field at PHI request  
-- SA 2017-07-11 added @WhoSaved
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_FraudIndicators_AutoSet] 
(
 	 @MatterID INT
	,@WhoSaved INT = NULL
)

AS
BEGIN

	DECLARE @PolicyLeadID INT
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@MatterID)

	DECLARE @LeadID INT, @CaseID INT, @ClientID INT
	DECLARE @FraudIndicators TABLE (ID INT IDENTITY, DetaildFieldID INT)

	-- Retrieve LeadID
	SELECT @LeadID = LeadID, @CaseID = CaseID, @ClientID = ClientID 
	FROM Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID

	-----------------------------------------------------
	/* Add Fraud Indicators to temp table as identifed */
	-----------------------------------------------------
	
	DECLARE @InceptionDate DATE
	SELECT @InceptionDate = PolicyInception 
	FROM dbo.fn_C00_1272_Policy_GetPolicyDetails(@MatterID)
	
	
	/* DF 144486 Within 60 days of inception */
	INSERT @FraudIndicators (DetaildFieldID) SELECT 144486
	FROM Matter m WITH (NOLOCK)
	INNER JOIN dbo.MatterDetailValues mdvDateOfLoss WITH (NOLOCK) ON mdvDateOfLoss.MatterID = m.MatterID AND mdvDateOfLoss.DetailFieldID = 	144892
	WHERE m.MatterID = @MatterID
	AND DATEDIFF(DAY, @InceptionDate, mdvDateOfLoss.ValueDate) <= 60
	
	/* DF 144487 More than 2 conditions in 12 months */
	;WITH Conditions (ParentMatterID) AS (
		SELECT DISTINCT ISNULL(r.ParentID,r.ClaimID)	-- List of unique condition MatterIDs
		FROM dbo.fn_C00_1272_GetClaimRelationships(@LeadID) r	-- All claims on this lead
		)
	INSERT @FraudIndicators (DetaildFieldID) SELECT 144487
	FROM Conditions c
	INNER JOIN dbo.MatterDetailValues mdvDoL WITH (NOLOCK) ON mdvDoL.MatterID = c.ParentMatterID AND mdvDoL.DetailFieldID = 144892
	INNER JOIN dbo.MatterDetailValues mdvThisDoL WITH (NOLOCK) ON mdvThisDoL.MatterID = @MatterID AND mdvThisDoL.DetailFieldID = 144892
	WHERE DATEDIFF(DAY, mdvDoL.ValueDate, mdvThisDoL.ValueDate) <= 365 -- Conditions with DatesOfLoss within a year of this one.
	HAVING COUNT(*) > 2 -- More than 2 conditions 
	
	/* DF 144488 Pet much older (6 years, or 8 for a cat) than insurance policy */
	INSERT @FraudIndicators (DetaildFieldID) SELECT 144488
	FROM Lead l WITH (NOLOCK)
	INNER JOIN dbo.LeadDetailValues ldvPetDoB WITH (NOLOCK) ON ldvPetDoB.LeadID = l.LeadID AND ldvPetDoB.DetailFieldID = 144274
	INNER JOIN dbo.LeadDetailValues ldvPetType WITH (NOLOCK) ON ldvPetType.LeadID = l.LeadID AND ldvPetType.DetailFieldID = 144272
	WHERE l.LeadID = @PolicyLeadID
	AND DATEADD(YEAR, CASE ldvPetType.ValueInt WHEN 42990 THEN 8 ELSE 6 END, ldvPetDoB.ValueDate) < @InceptionDate
	
	/* DF 144499 Same date of claim, same amount */
	;WITH Claims (LeadID, MatterID, TreatmentStart, ClaimAmount) AS (
		SELECT m.LeadID, m.MatterID, mdvTreatmentStart.ValueDate TreatmentStart, mdvClaimAmount.ValueMoney ClaimAmount 
		FROM Matter m WITH (NOLOCK)
		INNER JOIN dbo.MatterDetailValues mdvTreatmentStart WITH (NOLOCK) ON mdvTreatmentStart.MatterID = m.MatterID AND mdvTreatmentStart.DetailFieldID = 144366
		INNER JOIN dbo.MatterDetailValues mdvClaimAmount WITH (NOLOCK) ON mdvClaimAmount.MatterID = m.MatterID AND mdvClaimAmount.DetailFieldID = 149850
		WHERE m.LeadID = @LeadID
		)
	INSERT @FraudIndicators (DetaildFieldID) SELECT TOP 1 144499
	FROM Claims ThisOne
	INNER JOIN Claims OtherOne ON OtherOne.LeadID = ThisOne.LeadID AND OtherOne.MatterID <> ThisOne.MatterID
	WHERE ThisOne.TreatmentStart = OtherOne.TreatmentStart
	AND ThisOne.ClaimAmount = OtherOne.ClaimAmount
	
	/* DF 144492 Claim Treatment Dates overlap other Claims */
	;WITH Claims (CustomerID, LeadID, MatterID, TreatmentStart, TreatmentEnd) AS (
		SELECT m.CustomerID, m.LeadID, m.MatterID, mdvTStart.ValueDate, mdvTEnd.ValueDate
		FROM Matter m WITH (NOLOCK)
		INNER JOIN dbo.Customers cu WITH (NOLOCK) ON cu.CustomerID = m.CustomerID AND cu.Test = 0
		INNER JOIN dbo.MatterDetailValues mdvTStart WITH (NOLOCK) ON mdvTStart.MatterID = m.MatterID AND mdvTStart.DetailFieldID = 144366
		INNER JOIN dbo.MatterDetailValues mdvTEnd WITH (NOLOCK) ON mdvTEnd.MatterID = m.MatterID AND mdvTEnd.DetailFieldID = 145674
		)
	INSERT @FraudIndicators (DetaildFieldID) SELECT TOP 1 144492
	FROM Claims c1
	INNER JOIN Claims c2 ON c2.LeadID = c1.LeadID
	WHERE c2.MatterID <> c1.MatterID
	AND (c2.TreatmentStart <= c1.TreatmentEnd AND c2.TreatmentEnd >= c1.TreatmentStart) -- Treatment date ranges overlap
	AND c1.MatterID = @MatterID
	
	/* DF 175384 Claim notified over 6 months after treatment end */
	INSERT @FraudIndicators (DetaildFieldID) SELECT 175384
	FROM Matter m WITH (NOLOCK)
	INNER JOIN dbo.MatterDetailValues mdvTreatmentEnd WITH (NOLOCK) ON mdvTreatmentEnd.MatterID = m.MatterID AND mdvTreatmentEnd.DetailFieldID = 145674
	INNER JOIN dbo.MatterDetailValues mdvCFsent WITH (NOLOCK) ON mdvCFsent.MatterID = m.MatterID AND mdvCFsent.DetailFieldID = 144519
	INNER JOIN dbo.MatterDetailValues mdvCFrecd WITH (NOLOCK) ON mdvCFrecd.MatterID = m.MatterID AND mdvCFrecd.DetailFieldID = 144520
	WHERE m.MatterID = @MatterID
	AND DATEADD(MONTH, 6, mdvTreatmentEnd.ValueDate) < ISNULL(mdvCFsent.ValueDate, mdvCFrecd.ValueDate)	
	-----------------------------------------------
	/* SET ANY FRAUD INDICATORS IDENTIFIED ABOVE */
	-----------------------------------------------
	
	DECLARE @AutomationUser INT
	SELECT @AutomationUser = dbo.fn_C00_GetAutomationUser(@ClientID)
	
	IF (SELECT COUNT(*) FROM @FraudIndicators) > 0
	BEGIN
	
		INSERT MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
		SELECT @ClientID, @LeadID, @MatterID, fi.DetaildFieldID, 'true'
		FROM @FraudIndicators fi
		
		INSERT DetailValueHistory (ClientID, LeadID, MatterID, LeadOrMatter, DetailFieldID, FieldValue, ClientPersonnelID, WhenSaved)
		SELECT @ClientID, @LeadID, @MatterID, 2, fi.DetaildFieldID, 'true', @WhoSaved, dbo.fn_GetDate_Local()
		FROM @FraudIndicators fi
		
		-- Write a note				
		DECLARE @Note VARCHAR(2000) = ''
		
		SELECT @Note += (df.FieldName + ', ')
		FROM @FraudIndicators fi
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = fi.DetaildFieldID
		
		SELECT @Note = LEFT(@note, LEN(@note)-1)
		
	
		EXEC _C00_AddANote @CaseID, 862, @Note, 0, @WhoSaved, 0
		
	END	
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_FraudIndicators_AutoSet] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_FraudIndicators_AutoSet] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_FraudIndicators_AutoSet] TO [sp_executeall]
GO
