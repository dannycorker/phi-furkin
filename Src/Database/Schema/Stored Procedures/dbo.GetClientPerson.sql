SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetClientPerson] @ClientPersonnelID int
AS

SELECT 

ClientPersonnelID,
ClientID,
ClientOfficeID,
TitleID,
FirstName,
MiddleName,
LastName,
JobTitle,
ClientPersonnel.Password,
MobileTelephone,
HomeTelephone,
OfficeTelephone,
OfficeTelephoneExtension,
EmailAddress,
Salt,
AttemptedLogins,
AccountDisabled

FROM dbo.ClientPersonnel WHERE ClientPersonnelID = @ClientPersonnelID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientPerson] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetClientPerson] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientPerson] TO [sp_executeall]
GO
