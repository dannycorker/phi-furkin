SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2010-10-26
-- Description:	SQL Before Dataload for all clients
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SBD]
	@DataLoaderFileID int,
	@ClientPersonnelID int,
	@ClientID int
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SubClientID int
	
	SELECT @SubClientID = cp.SubClientID 
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.ClientPersonnelID = @ClientPersonnelID 
	
	/*
	IF @ClientID = 183 
	BEGIN

		EXEC dbo._C183_SBD @DataLoaderFileID, @ClientPersonnelID, @ClientID, @SubClientID
		
	END
	*/
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SBD] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SBD] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SBD] TO [sp_executeall]
GO
