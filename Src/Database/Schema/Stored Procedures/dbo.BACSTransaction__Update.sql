SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 05/08/2016
-- Description:	Updates a BACSTransaction record
-- =============================================
CREATE PROCEDURE [dbo].[BACSTransaction__Update] 
	
	@BACSTransactionID INT, 
	@BACSFileID INT, 
	@ClientID INT, 
	@CustomerID INT, 
	@ObjectID INT, 
	@ObjectTypeID INT, 
	@PurchasedProductPaymentScheduleID INT, 
	@CustomerPaymentScheduleID INT, 
	@PaymentTypeID INT,
	@DebitAccountName VARCHAR(105), 
	@DebitSortCode VARCHAR(6), 
	@DebitAccountNumber VARCHAR(8),
	@CurrencyID INT, 
	@PaymentAmount NUMERIC(18,2), 
	@PaymentReference VARCHAR(16), 
	@BACSProcessingDate DATETIME, 
	@BeneficiaryAccountNumber VARCHAR(8), 
	@BeneficiarySortCode VARCHAR(6), 
	@BeneficiaryName VARCHAR(35), 
	@BeneficiaryAddress VARCHAR(105), 
	@BeneficiaryBankName VARCHAR(35), 
	@BeneficiaryBankAddress VARCHAR(105), 
	@SUN VARCHAR(50), 
	@FailureCode VARCHAR(16), 
	@FailureDate DATETIME, 
	@DeemedTransactionDate DATETIME, 
	@DeemedTransactedOn DATETIME, 
	@RowNumberInFile INT, 
	@BACSTransactionCodeID INT, 
	@LeadEventID INT, 
	@WhoCreated INT, 
	@WhenCreated DATETIME
	
AS
BEGIN
	
	
	SET NOCOUNT ON;

	UPDATE BACSTransaction
	SET BACSFileID = @BACSFileID, 
		ClientID = @ClientID, 
		CustomerID = @CustomerID, 
		ObjectID = @ObjectID, 
		ObjectTypeID = @ObjectTypeID, 
		PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID, 
		CustomerPaymentScheduleID = @CustomerPaymentScheduleID, 
		PaymentTypeID = @PaymentTypeID,
		DebitAccountName = @DebitAccountName, 
		DebitSortCode = @DebitSortCode, 
		DebitAccountNumber = @DebitAccountNumber,
		CurrencyID = @CurrencyID, 
		PaymentAmount = @PaymentAmount, 
		PaymentReference = @PaymentReference, 
		BACSProcessingDate = @BACSProcessingDate, 
		BeneficiaryAccountNumber = @BeneficiaryAccountNumber, 
		BeneficiarySortCode = @BeneficiarySortCode, 
		BeneficiaryName = @BeneficiaryName, 
		BeneficiaryAddress = @BeneficiaryAddress, 
		BeneficiaryBankName = @BeneficiaryName, 
		BeneficiaryBankAddress = @BeneficiaryBankAddress, 
		SUN = @SUN, 
		FailureCode = @FailureCode, 
		FailureDate = @FailureDate, 
		DeemedTransactionDate = @DeemedTransactionDate, 
		DeemedTransactedOn = @DeemedTransactedOn, 
		RowNumberInFile = @RowNumberInFile, 
		BACSTransactionCodeID = @BACSTransactionCodeID, 
		LeadEventID = @LeadEventID,
		WhoCreated = @WhoCreated, 
		WhenCreated = @WhenCreated
	WHERE BACSTransactionID=@BACSTransactionID
    	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[BACSTransaction__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BACSTransaction__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BACSTransaction__Update] TO [sp_executeall]
GO
