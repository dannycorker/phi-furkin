SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROC [dbo].[GetRandomNumber] (@Length int, @Seed int = null, @output varchar(10) OUTPUT)
AS  
BEGIN 

	declare @rnd decimal(18, 10)

	-- Random numbers are generated as a decimal number between 0 and 1
	-- Example: 0.7132641215236415
	-- The function returns an integer version of this, to the length
	-- specified by @Length: eg @Length=6 would return "713264".
	IF @Length > 10
	BEGIN
		SELECT @Length = 10
	END
	ELSE
	BEGIN
		IF @Length < 1
		BEGIN
			SELECT @Length = 1
		END
	END
	
	-- The user can specify a seed value if they wish.  For a given seed
	-- value (eg 3) the same number will always be returned, so this is
	-- not as random as you might expect!  Leave @Seed = NULL to get a 
	-- proper random number every time.
	IF @Seed IS NULL
	BEGIN
		SELECT @rnd = rand()
	END
	ELSE
	BEGIN
		SELECT @rnd = rand(@Seed)
	END

	-- Take the decimal, multiply it by 10, 100, 1000, 10000 or whatever
	-- to make it a useable value in the required range.  Floor the decimal
	-- to turn it into a integer, then turn it into a varchar ready for
	-- the final output.
	SELECT @output = convert(varchar, floor(@rnd * power(10, @Length)))

END






GO
GRANT VIEW DEFINITION ON  [dbo].[GetRandomNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetRandomNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetRandomNumber] TO [sp_executeall]
GO
