SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2019-11-20
-- Description:	Create required CustomerLedger and Payment records for a Premium Funding once we receive a processed response
-- 2020-01-21 GPR | Added update to set the CustomerLedgerID and ReconciledDate on the CPS/PPPS - this is used in Cancellation Adjustments and for completeness with AskBill
-- =============================================
CREATE PROCEDURE [dbo].[Billing__CreatePaymentForPremiumFunding]
	@CustomerPaymentScheduleID INT,
	@PaymentTypeID INT,
	@PaymentDescription VARCHAR(2000),
	@LeadEventID INT = NULL,
	@WhoCreated INT
AS
BEGIN

	SET NOCOUNT ON;


	DECLARE @CustomerLedgerID INT


		UPDATE cps
		SET PaymentStatusID = 6
		FROM CustomerPaymentSchedule cps 
		WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID

		UPDATE ppps
		SET PaymentStatusID = 6
		FROM PurchasedProductPaymentSchedule ppps 
		WHERE ppps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID


	CREATE TABLE #InsertedPayments (CustomerPaymentScheduleID INT, PaymentID INT)
	CREATE TABLE #InsertedCustomerLedger (CustomerLedgerID INT, PaymentID INT)

	INSERT INTO Payment (ClientID, CustomerID, PaymentDateTime, PaymentTypeID, DateReceived, PaymentDescription, PaymentReference, PaymentNet, PaymentVAT, PaymentGross, PaymentCurrency, RelatedOrderRef, RelatedRequestDescription, PayeeFullName, MaskedAccountNumber, AccountTypeDescription, PaymentStatusID, DateReconciled, ReconciliationOutcome, ObjectID, ObjectTypeID, Comments, PaymentFileName, FailureCode, FailureReason, CustomerPaymentScheduleID, WhoCreated, WhenCreated, WhoModified, WhenModified)
	OUTPUT inserted.CustomerPaymentScheduleID, inserted.PaymentID INTO #InsertedPayments (CustomerPaymentScheduleID, PaymentID)
	SELECT cps.ClientID, 
			cps.CustomerID, 
			dbo.fn_GetDate_Local(),
			10 AS [PaymentTypeID],  /*10 = Premium Funding*/
			NULL AS [DateReceived], 
			'' AS [PaymentDescription], 
			ISNULL(a.Reference,''), 
			cps.PaymentNet, 
			cps.PaymentVAT, 
			cps.PaymentGross, 
			NULL AS [PaymentCurrency], 
			NULL AS [RelatedOrderRef], 
			NULL AS [RelatedRequestDescription], 
			NULL AS [PayeeFullName], 
			NULL AS [MaskedAccountNumber], 
			NULL AS [AccountTypeDescription], 
			6 AS [PaymentStatusID], /*Paid*/
			NULL AS [DateReconciled], 
			NULL AS [ReconciliationOutcome], 
			cps.RelatedObjectID, 
			cps.RelatedObjectTypeID, 
			'' AS [Comments], 
			NULL AS [PaymentFileName], 
			NULL AS [FailureCode], 
			NULL AS [FailureReason], 
			cps.CustomerPaymentScheduleID, 
			@WhoCreated AS [WhoCreated], 
			dbo.fn_GetDate_Local() AS [WhenCreated], 
			@WhoCreated AS [WhoModified], 
			dbo.fn_GetDate_Local() AS [WhenModified]
	FROM CustomerPaymentSchedule cps 
	INNER JOIN Account a WITH (NOLOCK) ON cps.AccountID = a.AccountID
	WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID

	INSERT INTO CustomerLedger (ClientID, CustomerID, EffectivePaymentDate, FailureCode, FailureReason, TransactionDate, TransactionReference, TransactionDescription, TransactionNet, TransactionVAT, TransactionGross, LeadEventID, ObjectID, ObjectTypeID, PaymentID, OutgoingPaymentID, WhoCreated, WhenCreated)
	OUTPUT inserted.CustomerLedgerID, inserted.PaymentID INTO #InsertedCustomerLedger (CustomerLedgerID, PaymentID)
	SELECT p.ClientID, p.CustomerID, 
		dbo.fn_GetDate_Local(), 
		NULL AS [FailureCode], 
		NULL AS [FailureReason], 
		dbo.fn_GetDate_Local() AS [TransactionDate], 
		p.PaymentReference, 
		@PaymentDescription, 
		p.PaymentNet,
		p.PaymentVAT, 
		p.PaymentGross, 
		@LeadEventID AS [LeadEventID], 
		p.ObjectID AS [ObjectID], 
		p.ObjectTypeID, 
		p.PaymentID, 
		NULL AS [OutgoingPaymentID], 
		@WhoCreated, 
		dbo.fn_GetDate_Local() AS [WhenCreated]
	FROM #InsertedPayments ip 
	INNER JOIN Payment p WITH (NOLOCK) ON p.PaymentID = ip.PaymentID

	
	SELECT @CustomerLedgerID = SCOPE_IDENTITY()


	/*GPR 2020-01-21 SDLPC-107 - Set the CustomerLedgerID on our CPS and PPPS records along with the reconciled date of now*/
	UPDATE cps
	SET CustomerLedgerID = @CustomerLedgerID, ReconciledDate = dbo.fn_GetDate_Local()
	FROM CustomerPaymentSchedule cps 
	WHERE cps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID

	UPDATE ppps
	SET CustomerLedgerID = @CustomerLedgerID, ReconciledDate = dbo.fn_GetDate_Local()
	FROM PurchasedProductPaymentSchedule ppps 
	WHERE ppps.CustomerPaymentScheduleID = @CustomerPaymentScheduleID


END
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__CreatePaymentForPremiumFunding] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing__CreatePaymentForPremiumFunding] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing__CreatePaymentForPremiumFunding] TO [sp_executeall]
GO
