SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2013-01-24
-- Description:	Create an editable SqlQuery record from a sql statement or an existing SqlQuery record
-- =============================================
CREATE PROCEDURE [dbo].[_C00_MakeQueryEditable]  
	@ExistingQueryID INT = NULL,					/* Pass in the ID of an existing query if there is one */
	@NonEditableQueryText VARCHAR(MAX) = NULL,		/* If you want to make a report from a sql statement, put the statement here */
	@MakeACopy INT = 1,								/* 0 = Update the original query in place, 1 = Make a copy of the original (set this to 1 if using a sql statement above) */
	@QueryIDToCopyCreationDetailsFrom INT = NULL,	/* We need details like FolderID, CreatedBy etc, which will all come from the ID specified here if MakeACopy = 1 or @NonEditableQueryText is a sql string rather than an ID */
	@ReallyDoTheInsertAndUpdates BIT = 0,			/* Actully do the updates at the end */
	@ShowWorking BIT = 1							/* Show the calculations and decisions made along the way */
AS
BEGIN


	/*
		Example call to make an editable query out of some well-written SQL:
		
		DECLARE @SqlString VARCHAR(MAX) = 'SELECT c.FullName, l.LeadID, l.LeadRef, ca.CaseNum, le.LeadEventID, le.WhenCreated 
		FROM dbo.Customers c WITH (NOLOCK) 
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.CustomerID = c.CustomerID 
		INNER JOIN dbo.Cases ca WITH (NOLOCK) ON ca.LeadID = l.LeadID 
		INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.CaseID = ca.CaseID 
		WHERE c.ClientID = 4 
		AND c.Test = 0 
		AND l.LeadTypeID = 503 
		AND le.WhenCreated > dbo.fn_GetDate_Local() - 1'
		
		EXEC dbo._C00_MakeQueryEditable NULL, @SqlString, 1, 12345, 1, 1
	*/
	
	SET NOCOUNT ON;
	
	DECLARE	
	@LeftStr VARCHAR(MAX), 
	@AtPos INT,
	@LenDelim INT,
	@LeftLen INT,
	@RightLen INT,
	@LoopRow INT = 0,
	@LoopCount INT = 0,
	@ClientID INT,
	@OutputStr VARCHAR(MAX),
	@Delimiter VARCHAR(2000) = ',',
	@InfinityTest INT = 100,
	@JoinTypeText VARCHAR(10) = 'INNER JOIN',
	@WhereOrAnd VARCHAR(5) = 'WHERE',
	
	@ColumnToTestText VARCHAR(250),
	@ColumnPrefixText VARCHAR(250),
	@ColumnNameText VARCHAR(250),
	@ColumnSuffixText VARCHAR(250),
	@TempStr VARCHAR(MAX),
	@RawText VARCHAR(250) = '',
	@TableText VARCHAR(250) = '',
	@HintText VARCHAR(250) = '',
	@AliasText VARCHAR(250) = '',
	@JoinText VARCHAR(250) = '',	
	@JoinFromText VARCHAR(250) = '',
	@JoinFromTableText VARCHAR(250) = '',
	@JoinFromAliasText VARCHAR(250) = '',
	@JoinToText VARCHAR(250) = '',
	@JoinToTableText VARCHAR(250) = '',
	@JoinToAliasText VARCHAR(250) = '', 
	
	@SqlQueryID INT = 0	
	
	DECLARE @TLine TABLE 
	(
		LineNum INT IDENTITY(1, 1), 
		TextLine VARCHAR(MAX),
		LineType VARCHAR(30) /* Select, From, Join, Where */
	)
	
	DECLARE @Chars TABLE(
		POSITION INT, 
		CharValue CHAR, 
		CharName VARCHAR(255), 
		AsciiValue TINYINT, 
		Opening SMALLINT, 
		OpenCount TINYINT
	)
	
	DECLARE @BigString VARCHAR(MAX) = ''
	DECLARE @SelectPos INT = 0,
			@FromPos INT = 0,
			@JoinPos INT = 0,
			@WherePos INT = 0,
			@AndPos INT = 0,
			@GroupByPos INT = 0,
			@HavingPos INT = 0,
			@OrderByPos INT = 0
	
	DECLARE @SelectKeyword VARCHAR(10) = 'SELECT ',
			@FromKeyword VARCHAR(10) = 'FROM ',
			@JoinKeyword VARCHAR(10) = 'INNER JOIN ', --LEFT{OUTER} JOIN will be checked too
			@WhereKeyword VARCHAR(10) = 'WHERE ',
			@AndKeyword VARCHAR(10) = 'AND ',
			@GroupByKeyword VARCHAR(10) = 'GROUP BY ',
			@HavingKeyword VARCHAR(10) = 'HAVING ',
			@OrderByKeyword VARCHAR(10) = 'ORDER BY' 
	
	DECLARE @TableList TABLE 
	(
		AnyID INT IDENTITY(1, 1) NOT NULL,
		RawText VARCHAR(250) NOT NULL,
		JoinType VARCHAR(10) NULL,
		TableText VARCHAR(250) NULL,
		AliasText VARCHAR(250) NULL,
		JoinText VARCHAR(250) NULL
	)
	
	DECLARE @ColumnList TABLE 
	(
		AnyID INT IDENTITY(1, 1) NOT NULL,
		RawText VARCHAR(250) NOT NULL,
		ColumnText VARCHAR(250) NULL,
		TableText VARCHAR(250) NULL,
		AliasText VARCHAR(250) NULL,
		TableAnyID INT NULL, 
		SortOrder INT NULL, 
		SortDesc BIT NULL,
		DataTypeName VARCHAR(250) NULL,
		IsColumnVisible BIT NULL
	)

	DECLARE @CriteriaList TABLE 
	(
		AnyID INT IDENTITY(1, 1) NOT NULL,
		CriteriaText VARCHAR(1000) NOT NULL,
		Criteria1 VARCHAR(250) NULL,
		Criteria2 VARCHAR(250) NULL,
		CriteriaName VARCHAR(250) NULL,
		ColumnText VARCHAR(250) NULL,
		TableText VARCHAR(250) NULL,
		AliasText VARCHAR(250) NULL,
		ColumnAnyID INT NULL,
		TableAnyID INT NULL, 
		IsSecurityClause BIT NULL,
		Comparison VARCHAR(50) NULL
	)

	DECLARE @OrderByColumnList TABLE 
	(
		AnyID INT IDENTITY(1, 1) NOT NULL,
		RawText VARCHAR(250) NOT NULL,
		ColumnText VARCHAR(250) NULL,
		TableText VARCHAR(250) NULL,
		AliasText VARCHAR(250) NULL,
		SortDesc BIT NOT NULL,
		ColumnAnyID INT NULL
	)


	/**************************************************************************************************
	*
	* Start of work...
	*
	**************************************************************************************************/
	
	/* Existing query or CopyCreationDetailsFrom query must be supplied */
	IF @ExistingQueryID IS NULL AND @QueryIDToCopyCreationDetailsFrom IS NULL
	BEGIN
		PRINT 'Epic fail!  @ExistingQueryID or @QueryIDToCopyCreationDetailsFrom must be supplied'
		RETURN -1
	END
	ELSE
	BEGIN
		/* ClientID comes from the "Copy details from this query" record if supplied, otherwise from the existing query */
		/* */
		SELECT TOP 1 @ClientID = sq.ClientID, @NonEditableQueryText = LTRIM(RTRIM(CASE WHEN @NonEditableQueryText IS NULL THEN sq.QueryText ELSE @NonEditableQueryText END))
		FROM dbo.SqlQuery sq (NOLOCK) 
		WHERE sq.QueryID = CASE WHEN @QueryIDToCopyCreationDetailsFrom IS NULL THEN @ExistingQueryID ELSE @QueryIDToCopyCreationDetailsFrom END 
		
	END
	
	/* Only make the copy if we are doing this for real */
	IF @MakeACopy = 1 AND @ReallyDoTheInsertAndUpdates = 1
	BEGIN
		/*
			Several options here:
			
			1) With a string of sql as the input, take details from other query
			2) With an existing query, make a copy using its details
			3) With an existing query, make a copy using details from the other query
			4) FAIL
		*/
		
		/* Make the basic SqlQuery record here.  All the sub-tables will be populated by the rest of this procedure. */
		INSERT INTO dbo.SqlQuery (ClientID, QueryText, QueryTitle, AutorunOnline, OnlineLimit, BatchLimit, SqlQueryTypeID, FolderID, IsEditable, IsTemplate, IsDeleted, WhenCreated, CreatedBy, OwnedBy, RunCount, LastRundate, LastRuntime, LastRowcount, MaxRuntime, MaxRowcount, AvgRuntime, AvgRowcount, Comments, WhenModified, ModifiedBy, LeadTypeID, ParentQueryID, IsParent, SqlQueryTemplateID, OutputFormat, ShowInCustomSearch, OutputFileExtension, OutputSeparatorCharmapID, OutputEncapsulatorCharmapID, SuppressHeaderRow, LockAllTables, SourceID)
		SELECT @ClientID, 
		CASE WHEN @ExistingQueryID IS NULL THEN @NonEditableQueryText ELSE sq.QueryText END, 
		sq.QueryTitle + '_Copy_' + REPLACE(CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120), ':', '_'), 
		sq.AutorunOnline, 
		sq.OnlineLimit, 
		sq.BatchLimit, 
		sq.SqlQueryTypeID, 
		sq.FolderID, 
		0 AS IsEditable, 
		sq.IsTemplate, 
		sq.IsDeleted, 
		dbo.fn_GetDate_Local() AS WhenCreated, 
		sq.CreatedBy, 
		sq.OwnedBy, 
		0 AS RunCount, 
		NULL AS LastRundate, 
		0 AS LastRuntime, 
		0 AS LastRowcount, 
		0 AS MaxRuntime, 
		0 AS MaxRowcount, 
		0 AS AvgRuntime, 
		0 AS AvgRowcount, 
		sq.Comments, 
		dbo.fn_GetDate_Local() AS WhenModified, 
		sq.ModifiedBy, 
		sq.LeadTypeID, 
		sq.ParentQueryID, 
		sq.IsParent, 
		sq.SqlQueryTemplateID, 
		sq.OutputFormat, 
		sq.ShowInCustomSearch, 
		sq.OutputFileExtension, 
		sq.OutputSeparatorCharmapID, 
		sq.OutputEncapsulatorCharmapID, 
		sq.SuppressHeaderRow, 
		sq.LockAllTables, 
		CASE WHEN @QueryIDToCopyCreationDetailsFrom IS NULL THEN @ExistingQueryID ELSE @QueryIDToCopyCreationDetailsFrom END AS SourceID
		FROM dbo.SqlQuery sq WITH (NOLOCK) 
		WHERE sq.QueryID = CASE WHEN @QueryIDToCopyCreationDetailsFrom IS NULL THEN @ExistingQueryID ELSE @QueryIDToCopyCreationDetailsFrom END 
		
		SELECT @SqlQueryID = SCOPE_IDENTITY()

		IF @SqlQueryID IS NULL
		BEGIN
			PRINT 'Epic fail!  Query failed to create'
			RETURN -1
		END
		
	END
	ELSE
	BEGIN
		/* Check to see if an existing SqlQueryID has been passed in */
		IF @ExistingQueryID > 0
		BEGIN
			
			SELECT @SqlQueryID = @ExistingQueryID
			
		END
		ELSE
		BEGIN
			PRINT 'Epic fail!  @ExistingQueryID is required if you are not making a copy'
			RETURN -1
		END
	END
			
	IF @ShowWorking = 1
	BEGIN
		PRINT @NonEditableQueryText
		
		SELECT CASE WHEN @MakeACopy = 0 THEN 'Existing Query' ELSE 'New Query' END AS 'SqlQuery', sq.* 
		FROM dbo.SqlQuery sq WITH (NOLOCK) 
		WHERE sq.QueryID = @SqlQueryID
	END
	

	
	SELECT @NonEditableQueryText = LTRIM(RTRIM(@NonEditableQueryText))
			
	SELECT @SelectPos = CHARINDEX(@SelectKeyword, @NonEditableQueryText, 1)
	
	
	/*********************************************************************************************************************
	*
	* Start by looking for the SELECT keyword and the FROM keyword to get the column list
	*
	*********************************************************************************************************************/
	IF @SelectPos > 0
	BEGIN
	
		/* FROM */			
		SELECT @FromPos = CHARINDEX(@FromKeyword, @NonEditableQueryText, @SelectPos)

		IF @FromPos > 0
		BEGIN
			
			/*
				Output anything before the FROM clause as the column list
			*/
			SELECT @Leftlen = @FromPos - 1
			
			SELECT @LeftStr = RTRIM(LEFT(@NonEditableQueryText, @Leftlen)) /* Trailing spaces play havoc with fnGetCharsBeforeOrAfterSeparator below */
			
			/* Remove the SELECT keyword first */
			INSERT @TLine (TextLine, LineType) VALUES (@LeftStr, @SelectKeyword)
			
			/* Get everything after the SELECT keyword as the column list */
			SET @TempStr = dbo.fnGetCharsBeforeOrAfterSeparator(@LeftStr, 'SELECT', 1, 0)
			
			/* We need to replace misleading commas with pipes to preserve them for a while */
			INSERT @Chars (POSITION, CharValue, CharName, AsciiValue, Opening, OpenCount)
			SELECT f.POSITION, f.CharValue, f.CharName, f.AsciiValue, 0, 0
			FROM dbo.fnCharMap(@TempStr) f 
			ORDER BY f.POSITION 
			
			/*
				Now we have a table variable filled with each character of the SELECT line.
				It will look like this:
				L 
				e 
				a 
				d 
				I 
				D 
				etc
			*/
			/* Locate any chars that could mask a comma that we don't want to split on */
			UPDATE @Chars 
			SET Opening = 1
			WHERE CharValue IN ('(', '[')
			
			/* Locate the chars that close the above openers */
			UPDATE @Chars 
			SET Opening = -1
			WHERE CharValue IN (')', ']')
			
			/* Keep track of how many opening/closing things we have encountered by each point in the string */
			UPDATE c1
			SET OpenCount = (SELECT SUM(c2.Opening) 
			FROM @Chars c2 
			WHERE c2.POSITION <= c1.POSITION)
			FROM @Chars c1 
			
			/* Any commas found within brackets etc need to be preserved for a while, so replace them with pipes */
			UPDATE @Chars 
			SET CharValue = '|'
			WHERE CharValue IN (',') 
			AND OpenCount > 0
			
			/* Rebuild the SELECT line, with the pipes instead of the unwanted commas */
			SELECT @BigString += CharValue FROM @Chars
			
			/* Split on relevant commas to get individual fields */
			INSERT INTO @ColumnList (RawText, IsColumnVisible)
			SELECT f.AnyValue, 1
			FROM dbo.fnTableOfValuesRelevantOnly(@BigString, ',') f

			/*
				Then work out how much to keep from the rest of the string.
			*/
			SELECT @RightLen = LEN(@NonEditableQueryText) - (@FromPos - 1)
			
			SELECT @NonEditableQueryText = LTRIM(RTRIM(RIGHT(@NonEditableQueryText, @RightLen)))

			
			/*
				Repeatedly loop round for JOIN clauses
				Don't start at charindex 1 because that is where the current JOIN statement will be!!
			*/
			SELECT @JoinPos = CHARINDEX(@JoinKeyword, @NonEditableQueryText, 2)
			IF @JoinPos = 0
			BEGIN
				SELECT @JoinPos = CHARINDEX('LEFT JOIN ', @NonEditableQueryText, 2)
			END
			
			/* Reset the loop counter */
			SET @LoopCount = 0
			
			WHILE @JoinPos > 0 AND @LoopCount < @InfinityTest
			BEGIN
				
				SELECT @LoopCount += 1
			
				/*
					Output the FROM/JOIN clause into the work table
				*/
				SELECT @Leftlen = @JoinPos - 1
				
				SELECT @LeftStr = LEFT(@NonEditableQueryText, @Leftlen)
				
				INSERT @TLine (TextLine, LineType) VALUES (@LeftStr, CASE @LoopCount WHEN 1 THEN @FromKeyword ELSE @JoinTypeText END )
				
				/*
					Then work out how much to keep from the rest of the string.
				*/
				SELECT @RightLen = LEN(@NonEditableQueryText) - (@JoinPos - 1)
				
				SELECT @NonEditableQueryText = LTRIM(RTRIM(RIGHT(@NonEditableQueryText, @RightLen)))

				/*
					Loop round for the next JOIN clause
					Don't start at charindex 1 because that is where the current JOIN statement will be!!
				*/
				SELECT @JoinPos = 0
				
				SELECT @JoinPos = CHARINDEX(@JoinKeyword, @NonEditableQueryText, 2), @JoinTypeText = 'INNER JOIN'
				IF @JoinPos = 0
				BEGIN
					SELECT @JoinPos = CHARINDEX('LEFT JOIN ', @NonEditableQueryText, 2)
					IF @JoinPos > 0
					BEGIN
						SELECT @JoinTypeText = 'LEFT JOIN'
					END
				END
				
			END		

			/* WHERE */			
			/* Output anything before the WHERE clause as one line. */
			SELECT @WherePos = CHARINDEX(@WhereKeyword, @NonEditableQueryText, 1)

			SELECT @Leftlen = @WherePos - 1
			
			-- Grab this text.
			SELECT @LeftStr = LEFT(@NonEditableQueryText, @Leftlen)
			
			-- Insert it into the output table.
			INSERT @TLine (TextLine, LineType) VALUES (@LeftStr, CASE @LoopCount WHEN 1 THEN @FromKeyword ELSE @JoinTypeText END)
			
			-- Then work out how much to keep from the rest of the string.
			SELECT @RightLen = LEN(@NonEditableQueryText) - (@WherePos - 1)
			
			-- Finally, keep only the remaining text, then loop round and do it all again.
			SELECT @NonEditableQueryText = LTRIM(RTRIM(RIGHT(@NonEditableQueryText, @RightLen)))
			
			
			-- Repeat [loop] for AND clauses 
			-- Don't start at column 1, cos that is where the current JOIN statement will be!!
			SELECT @AndPos = CHARINDEX(@AndKeyword, @NonEditableQueryText, 2)
		
			WHILE @AndPos > 0 AND @LoopCount < @InfinityTest
			BEGIN
				
				SELECT @LoopCount += 1
			
				SELECT @Leftlen = @AndPos - 1
				
				-- Grab this text.
				SELECT @LeftStr = LEFT(@NonEditableQueryText, @Leftlen)
				
				-- Insert it into the output table.
				INSERT @TLine (TextLine, LineType) VALUES (@LeftStr, @WhereOrAnd)
				
				/* All subsequent entries are AND clauses */
				SELECT @WhereOrAnd = 'AND'
				
				-- Then work out how much to keep from the rest of the string.
				SELECT @RightLen = LEN(@NonEditableQueryText) - (@AndPos - 1)
				
				-- Finally, keep only the remaining text, then loop round and do it all again.
				SELECT @NonEditableQueryText = LTRIM(RTRIM(RIGHT(@NonEditableQueryText, @RightLen)))
				
				-- Loop round again looking for more AND clauses
				SELECT @AndPos = 0
				
				-- Don't start at column 1, cos that is where the current JOIN statement will be!!
				SELECT @AndPos = CHARINDEX(@AndKeyword, @NonEditableQueryText, 2)
				
			END			
		

			/* ORDER BY */			
			/* ORDER BY is always dead last */
			SELECT @OrderByPos = CHARINDEX(@OrderByKeyword, @NonEditableQueryText, @HavingPos)

			IF @OrderByPos > 0
			BEGIN
			
				SELECT @Leftlen = @OrderByPos - 1
				
				-- Grab this text.
				SELECT @LeftStr = LEFT(@NonEditableQueryText, @Leftlen)
				
				-- Insert it into the output table.
				INSERT @TLine (TextLine, LineType) VALUES (@LeftStr, @AndKeyword)
				
				-- Then work out how much to keep from the rest of the string.
				SELECT @RightLen = LEN(@NonEditableQueryText) - (@OrderByPos - 1)
				
				-- Finally, keep only the remaining text, then loop round and do it all again.
				SELECT @NonEditableQueryText = LTRIM(RTRIM(RIGHT(@NonEditableQueryText, @RightLen)))
				
				-- Stop here
				INSERT @TLine (TextLine, LineType) VALUES (@NonEditableQueryText, @OrderByKeyword)
				
				/* Now split the ORDER BY statement into individual columns as we need the info for SqlQueryColumns later */
				
				-- Remove irrelevant chars first of all
				SELECT @NonEditableQueryText = REPLACE(REPLACE(@NonEditableQueryText, 'ORDER BY ', ''), ' ASC', '')
				
				-- Get them into a work table, noting any that are in descending order
				INSERT @OrderByColumnList (RawText, SortDesc) 
				SELECT f.AnyValue, CASE WHEN f.AnyValue LIKE '% DESC' THEN 1 ELSE 0 END  
				FROM dbo.fnTableOfValuesFromCSV(@NonEditableQueryText) f 

				/* Loop through all rows in this table, cleaning up the column details */
				SET @LoopRow = 0
				
				SELECT @LoopCount = COUNT(*) 
				FROM @OrderByColumnList c
				
				WHILE @LoopRow < @LoopCount
				BEGIN
					SELECT @LoopRow += 1
					
					SELECT @ColumnToTestText = c.RawText 
					FROM @OrderByColumnList c
					WHERE c.AnyID = @LoopRow
					
					/*
						For something like 'l.LeadID AS [SourceID]' we need to get:
						Prefix as the text before the first '.'
						Suffix as the text after the first space
						Name as the bit in between
					*/
					SELECT @ColumnPrefixText = '',
					@ColumnSuffixText = ''
					
					IF @ColumnToTestText LIKE '%.%'
					BEGIN
						SELECT @ColumnPrefixText = dbo.fnGetCharsBeforeOrAfterSeparator(@ColumnToTestText, '.', 1, 1)
						SELECT @ColumnToTestText = RIGHT(@ColumnToTestText, LEN(@ColumnToTestText) - LEN(@ColumnPrefixText) - 1) /* Allow for the dot */
						
						/* Extra complexity for values like "convert(varchar(10)|LeadEvent.WhenCreated|120) AS [Cancellation Date]" */
						SELECT @ColumnPrefixText = dbo.fnGetCharsBeforeOrAfterSeparator(@ColumnPrefixText, '|', 1, 0)
					END
					
					IF @ColumnToTestText LIKE '% AS %'
					BEGIN
						SELECT @ColumnSuffixText = dbo.fnGetCharsBeforeOrAfterSeparator(@ColumnToTestText, ' AS ', 0, 0)
						SELECT @ColumnToTestText = LEFT(@ColumnToTestText, LEN(@ColumnToTestText) - LEN(@ColumnSuffixText) - 4) /* Allow for the ' AS ' */
					END
					/* Extra complexity for values like "convert(varchar(10)|LeadEvent.WhenCreated|120) AS [Cancellation Date]" */
					/* Cannot do this check until now, or we lose the alias */
					SELECT @ColumnToTestText = dbo.fnGetCharsBeforeOrAfterSeparator(@ColumnToTestText, '|', 0, 1)
					
					SELECT @ColumnNameText = @ColumnToTestText
					
					UPDATE @OrderByColumnList
					SET ColumnText = @ColumnNameText,
					TableText = @ColumnPrefixText,
					AliasText = @ColumnSuffixText
					WHERE AnyID = @LoopRow
					
				END
							
				/* Put the "irrelevant" commas back in instead of the pipe "|" placeholders */
				UPDATE @OrderByColumnList 
				SET RawText = REPLACE(RawText, '|', ',')
				
				
			END
			ELSE
			BEGIN
				-- Don't forget to output the final if there was no ORDER BY at all.
				INSERT @TLine (TextLine, LineType) VALUES (@NonEditableQueryText, @AndKeyword)
			END				
		END
		
		
		/*********************************************************************************************************************
		*
		* Create the individual tables
		*
		*********************************************************************************************************************/
		
		/* Loop through all rows in this table, cleaning up the column details */
		SET @LoopRow = 0
		
		SELECT @LoopCount = COUNT(*) 
		FROM @TLine t
		WHERE t.LineType IN ('FROM', 'INNER JOIN', 'LEFT JOIN')
		
		WHILE @LoopRow < @LoopCount
		BEGIN
			SELECT @LoopRow += 1
			
			SELECT TOP 1 @RawText = t.TextLine, 
			@JoinTypeText = t.LineType
			FROM @TLine t
			WHERE t.LineNum > @LoopRow 
			ORDER BY t.LineNum 
		
			/* FROM / JOIN */
			IF @JoinTypeText = 'FROM'
			BEGIN
				SELECT @RawText = dbo.fnGetCharsBeforeOrAfterSeparator(@RawText, 'FROM ', 1, 0)
			END
			ELSE
			BEGIN
				IF @JoinTypeText = 'INNER JOIN'
				BEGIN
					SELECT @RawText = dbo.fnGetCharsBeforeOrAfterSeparator(@RawText, 'INNER JOIN ', 1, 0)
				END
				ELSE
				BEGIN
					IF @JoinTypeText = 'LEFT JOIN'
					BEGIN
						SELECT @RawText = dbo.fnGetCharsBeforeOrAfterSeparator(@RawText, 'INNER JOIN ', 1, 0)
					END
					ELSE
					BEGIN
						SELECT 'INAPPROPRIATE JOIN' AS [Type]
						SELECT @RawText = ''
					END
				END
			END

			/* Remve 'dbo.',  'AS', WITH (NOLOCK) */
			SELECT @RawText = REPLACE(@RawText, 'dbo.', '')
			SELECT @RawText = REPLACE(@RawText, 'AS ', ' ')
			SELECT @RawText = REPLACE(@RawText, 'WITH', '')
			SELECT @RawText = REPLACE(@RawText, '(NOLOCK)', '')

			/* Split out the table and the ON clause */
			IF @RawText LIKE '% ON %'
			BEGIN
				SELECT @TableText = LTRIM(RTRIM(dbo.fnGetCharsBeforeOrAfterSeparator(@RawText, ' ON ', 1, 1)))
				SELECT @JoinText = LTRIM(RTRIM(dbo.fnGetCharsBeforeOrAfterSeparator(@RawText, ' ON ', 1, 0)))
			END
			ELSE
			BEGIN
				SELECT @TableText = LTRIM(RTRIM(@RawText))
			END

			/* Split out the alias (if one exists) from the ON clause */
			IF @TableText LIKE '% %'
			BEGIN
				SELECT @AliasText = LTRIM(RTRIM(dbo.fnGetCharsBeforeOrAfterSeparator(@TableText, ' ', 1, 0)))
				SELECT @TableText = LTRIM(RTRIM(dbo.fnGetCharsBeforeOrAfterSeparator(@TableText, ' ', 1, 1)))
			END

			IF @JoinText LIKE '%=%'
			BEGIN
				SELECT @JoinFromText = LTRIM(RTRIM(dbo.fnGetCharsBeforeOrAfterSeparator(@JoinText, '=', 1, 1)))
				SELECT @JoinToText = LTRIM(RTRIM(dbo.fnGetCharsBeforeOrAfterSeparator(@JoinText, '=', 1, 0)))
				
				IF @JoinFromText LIKE '%.%' 
				BEGIN
					SELECT @JoinFromAliasText = dbo.fnGetCharsBeforeOrAfterSeparator(@JoinFromText, '.', 1, 1)
					SELECT @JoinFromTableText = dbo.fnGetCharsBeforeOrAfterSeparator(@JoinFromText, '.', 1, 0)
				END	
				
				IF @JoinToText LIKE '%.%' 
				BEGIN
					SELECT @JoinToAliasText = dbo.fnGetCharsBeforeOrAfterSeparator(@JoinToText, '.', 1, 1)
					SELECT @JoinToTableText = dbo.fnGetCharsBeforeOrAfterSeparator(@JoinToText, '.', 1, 0)
				END	

			END

			IF @JoinTypeText = 'FROM'
			BEGIN
				INSERT INTO @TableList (RawText, TableText, AliasText) 
				VALUES (@RawText, @TableText, @AliasText)
			END
			ELSE
			BEGIN
				INSERT INTO @TableList (RawText, TableText, AliasText, JoinType, JoinText) 
				VALUES (@RawText, @TableText, @AliasText, @JoinTypeText, @JoinText)
			END

			SELECT @TableText = '',
			@AliasText = '',
			@JoinTypeText = '',
			@JoinText = '',
			@JoinFromText = '',
			@JoinToText = '',
			@JoinFromAliasText = '',
			@JoinFromTableText = '',
			@JoinToAliasText = '',
			@JoinToTableText = ''
			
			/*
			SqlQueryTableName = TableText
			TableAlias = AliasText
			TableDisplayOrder = (AnyID - 1)
			JoinType = JoinType
			JoinText = JoinText
			*/		
		END
		
		/* Put the "irrelevant" commas back in instead of the pipe "|" placeholders */
		UPDATE @TableList 
		SET RawText = REPLACE(RawText, '|', ',')
		
		IF @ShowWorking = 1
		BEGIN
			SELECT '@TableList' AS TempTable, t.* FROM @TableList t ORDER BY t.AnyID
		END
		
		
		/*********************************************************************************************************************
		*
		* Create the individual columns
		*
		*********************************************************************************************************************/
		
		/* Loop through all rows in this table, cleaning up the column details */
		SET @LoopRow = 0
		
		SELECT @LoopCount = COUNT(*) 
		FROM @ColumnList c
		
		WHILE @LoopRow < @LoopCount
		BEGIN
			SELECT @LoopRow += 1
			
			SELECT @ColumnToTestText = c.RawText 
			FROM @ColumnList c
			WHERE c.AnyID = @LoopRow
			
			/*
				For something like 'l.LeadID AS [SourceID]' we need to get:
				Prefix as the text before the first '.'
				Suffix as the text after the first space
				Name as the bit in between
			*/
			SELECT @ColumnPrefixText = '',
			@ColumnSuffixText = ''
			
			IF @ColumnToTestText LIKE '%.%'
			BEGIN
				SELECT @ColumnPrefixText = dbo.fnGetCharsBeforeOrAfterSeparator(@ColumnToTestText, '.', 1, 1)
				SELECT @ColumnToTestText = RIGHT(@ColumnToTestText, LEN(@ColumnToTestText) - LEN(@ColumnPrefixText) - 1) /* Allow for the dot */
				
				/* Extra complexity for values like "convert(varchar(10)|LeadEvent.WhenCreated|120) AS [Cancellation Date]" */
				SELECT @ColumnPrefixText = dbo.fnGetCharsBeforeOrAfterSeparator(@ColumnPrefixText, '|', 1, 0)
			END
			
			IF @ColumnToTestText LIKE '% AS %'
			BEGIN
				SELECT @ColumnSuffixText = dbo.fnGetCharsBeforeOrAfterSeparator(@ColumnToTestText, ' AS ', 0, 0)
				SELECT @ColumnToTestText = LEFT(@ColumnToTestText, LEN(@ColumnToTestText) - LEN(@ColumnSuffixText) - 4) /* Allow for the ' AS ' */
			END
			/* Extra complexity for values like "convert(varchar(10)|LeadEvent.WhenCreated|120) AS [Cancellation Date]" */
			/* Cannot do this check until now, or we lose the alias */
			SELECT @ColumnToTestText = dbo.fnGetCharsBeforeOrAfterSeparator(@ColumnToTestText, '|', 0, 1)
			
			SELECT @ColumnNameText = @ColumnToTestText
			
			UPDATE @ColumnList
			SET ColumnText = @ColumnNameText,
			TableText = @ColumnPrefixText,
			AliasText = @ColumnSuffixText
			WHERE AnyID = @LoopRow
			
		END
		
		/* Remove aliases from column names, because iReporting adds them automatically */
		UPDATE @ColumnList 
		SET RawText = dbo.fnGetCharsBeforeOrAfterSeparator(RawText, ' AS ', 0, 1), 
		AliasText = REPLACE(REPLACE(AliasText, '[', ''), ']', '') 
		WHERE AliasText > ''
		
		/* Put the "irrelevant" commas back in instead of the pipe "|" placeholders */
		UPDATE @ColumnList 
		SET RawText = REPLACE(RawText, '|', ',')
		
		/* Look up which columns in the ORDER BY are also in the SELECT statement and record the IDs */
		UPDATE @OrderByColumnList 
		SET ColumnAnyID = c.AnyID 
		FROM @OrderByColumnList o 
		INNER JOIN @ColumnList c ON c.TableText = o.TableText AND c.ColumnText = o.ColumnText 
		
		UPDATE @ColumnList
		SET SortOrder = o.AnyID,	/* The identity column on the @OrderByColumnList table is the order to sort by */
		SortDesc = o.SortDesc /* This is the ASC/DESC option for each column in the ORDER BY */
		FROM @ColumnList c 
		INNER JOIN @OrderByColumnList o ON o.ColumnAnyID = c.AnyID
		
		/* Create columns where they exist in the Order By but are not in the SELECT statement */
		INSERT @ColumnList(RawText, TableText, ColumnText, SortOrder, SortDesc)
		SELECT o.RawText, o.TableText, o.ColumnText, o.AnyID, o.SortDesc
		FROM @OrderByColumnList o 
		WHERE o.ColumnAnyID IS NULL
		
		
		/*********************************************************************************************************************
		*
		* Create the individual criteria
		*
		*********************************************************************************************************************/
		/*
			Loop through all rows in the WHERE clause and make criteria out of them 
			
			Example data:
			
			WHERE Customers.ClientID = 51   
			AND (Customers.Test = 0 OR Customers.Test IS NULL)
			AND (Lead.ClientID = 51) 
			AND LeadDetailValues.ValueInt NOT BETWEEN 3 AND 7 
			AND LookupListItems.LookupListItemID = 89525 
			AND DateDiff(day, LeadEvent.WhenCreated, dbo.fn_GetDate_Local()) >= 1 
			AND (MatterDetailValues.DetailFieldID = 94212) 
			AND LookupListItems.ClientID IN (0, 51) 
			
			Actually we don't need to worry about all the variations of "ID <> 7 AND Name LIKE '%Fred%' AND Date BETWEEN x AND y" because
			these breakdowns are not stored in SqlQueryCriteria ultimately.  We store the whole clause and the IsSecurityClause and everything else is 
			to do with parameter substitution, so let's assume we are not going to cover that at all here.
			
			But we do need to know which table & column we are dealing with.
		*/
		SELECT TOP 1 @LoopRow = t.LineNum
		FROM @TLine t
		WHERE t.LineType = 'WHERE'
		ORDER BY t.LineNum ASC
		
		SELECT TOP 1 @LoopCount = t.LineNum
		FROM @TLine t
		WHERE t.LineType IN ('WHERE', 'AND', 'OR')
		ORDER BY t.LineNum DESC
		
		WHILE @LoopRow <= @LoopCount
		BEGIN
			SELECT @RawText = t.TextLine, 
			@JoinTypeText = t.LineType
			FROM @TLine t
			WHERE t.LineNum = @LoopRow 
			
			/* Get the relevant text without the keyword ("WHERE" etc) */
			SELECT @RawText = dbo.fnGetCharsBeforeOrAfterSeparator(LTRIM(RTRIM(@RawText)), @JoinTypeText, 1, 0)
			
			/*
				For something like 'AND (Customers.Test = 0 OR Customers.Test IS NULL)' we need to get:
				TableName as the text before the first '.'
				ColumnName as the bit after the '.' and before the next space
			*/
			SELECT @ColumnPrefixText = '',
			@ColumnSuffixText = ''
			
			IF @RawText LIKE '%.%'
			BEGIN
				/* In a normal case, we will have a clause like "(LeadDetailValues.ValueInt = 13)" */
				SELECT @ColumnPrefixText = dbo.fnGetCharsBeforeOrAfterSeparator(@RawText, '.', 1, 1) /* Gets "(LeadDetailValues" */
				SELECT @ColumnPrefixText = dbo.fnGetCharsBeforeOrAfterSeparator(@ColumnPrefixText, '(', 0, 0) /* Gets "LeadDetailValues" */
				
				/* Extra complexity for values like "convert(varchar(10),LeadEvent.WhenCreated,120) > dbo.fn_GetDate_Local()" */
				/* This extra SELECT gets "LeadEvent" from "convert(varchar(10),LeadEvent" */
				SELECT @ColumnPrefixText = dbo.fnGetCharsBeforeOrAfterSeparator(@ColumnPrefixText, ',', 1, 0)
			END
			SELECT @ColumnToTestText = dbo.fnGetCharsBeforeOrAfterSeparator(@RawText, '.', 1, 0) /* Gets "ValueInt = 13)" */
			SELECT @ColumnToTestText = dbo.fnGetCharsBeforeOrAfterSeparator(@ColumnToTestText, ' ', 1, 1) /* Gets "ValueInt" */
			
			/* Extra complexity for values like "convert(varchar(10)|LeadEvent.WhenCreated|120) > dbo.fn_GetDate_Local()" */
			/* Cannot do this check until now, or we lose the alias */
			/* This extra SELECT gets "WhenCreated" from "LeadEvent.WhenCreated,120) > dbo.fn_GetDate_Local()" */
			SELECT @ColumnToTestText = dbo.fnGetCharsBeforeOrAfterSeparator(@ColumnToTestText, ',', 0, 1)
			
			SELECT @ColumnNameText = @ColumnToTestText
						
			/* Bracket everything that is not already bracketed */
			/* Remove line breaks first and add them in again afterwards for readability */
			SELECT @RawText = LTRIM(RTRIM(REPLACE(REPLACE(@RawText, CHAR(10), ''), CHAR(13), '')))
			
			IF LEFT(@RawText, 1) <> '('
			BEGIN
				SELECT @RawText = '(' + @RawText + ') ' + CHAR(13) + CHAR(10)
			END
			
			INSERT INTO @CriteriaList (CriteriaText, TableText, ColumnText, IsSecurityClause) 
			SELECT @RawText, LTRIM(RTRIM(@ColumnPrefixText)), LTRIM(RTRIM(@ColumnNameText)), CASE WHEN @RawText LIKE '%.ClientID%' THEN 1 ELSE 0 END AS IsSecurityClause
			
			SELECT @LoopRow += 1
			
		END
		
		/* Look up which columns in the ORDER BY are also in the SELECT statement and record the IDs */
		UPDATE @CriteriaList 
		SET ColumnAnyID = c.AnyID 
		FROM @CriteriaList o 
		INNER JOIN @ColumnList c ON c.TableText = o.TableText AND c.ColumnText = o.ColumnText 
		
		/* Populate the Table temp IDs for easier joining later on */
		UPDATE @CriteriaList 
		SET TableAnyID = t.AnyID 
		FROM @CriteriaList c 
		INNER JOIN @TableList t ON t.TableText = c.TableText
		
		/* Create columns where we have criteria for them (not security clauses) but they are not in the SELECT statement */
		INSERT @ColumnList(RawText, TableText, ColumnText, TableAnyID, IsColumnVisible)
		SELECT o.TableText + '.' + o.ColumnText, o.TableText, o.ColumnText, o.TableAnyID, 0
		FROM @CriteriaList o 
		WHERE o.ColumnAnyID IS NULL 
		AND o.IsSecurityClause = 0
		
		/* Join back to the columns we just created */
		UPDATE @CriteriaList 
		SET ColumnAnyID = c.AnyID, TableAnyID = c.TableAnyID 
		FROM @CriteriaList o 
		INNER JOIN @ColumnList c ON c.TableText = o.TableText AND c.ColumnText = o.ColumnText 
		WHERE o.ColumnAnyID IS NULL
		
		/* Then add any security criteria for tables that still need it (INFORMATION_SCHEMA.COLUMNS has a ClientID column for that table) */
		INSERT INTO @CriteriaList (CriteriaText, TableAnyID, IsSecurityClause)
		SELECT '(' + t.TableText + '.ClientID = ' + CAST(@ClientID AS VARCHAR) + ')', t.AnyID, 1
		FROM @TableList t 
		WHERE EXISTS (
			SELECT * 
			FROM INFORMATION_SCHEMA.COLUMNS isc WITH (NOLOCK) 
			WHERE isc.TABLE_NAME = t.TableText 
			AND isc.COLUMN_NAME = 'ClientID'
		)
		AND NOT EXISTS (
			SELECT * 
			FROM @CriteriaList c 
			WHERE c.TableAnyID = t.AnyID 
			AND c.IsSecurityClause = 1
		)
		
		/* Populate the Table temp IDs for easier joining later on */
		UPDATE @ColumnList 
		SET TableAnyID = t.AnyID 
		FROM @ColumnList c 
		INNER JOIN @TableList t ON t.TableText = c.TableText

		UPDATE @ColumnList 
		SET DataTypeName = ISNULL(sf.DataTypeName, 'Text') 
		FROM @ColumnList c 
		INNER JOIN @TableList t ON t.AnyID = c.TableAnyID 
		LEFT JOIN INFORMATION_SCHEMA.COLUMNS isc WITH (NOLOCK) ON isc.TABLE_NAME = c.TableText AND isc.COLUMN_NAME = c.ColumnText 
		LEFT JOIN dbo.SqlDataType sd WITH (NOLOCK) ON sd.DataTypeName = isc.DATA_TYPE 
		LEFT JOIN dbo.SqlFunctionDataType sf WITH (NOLOCK) ON sf.SqlFunctionDataTypeID = sd.SqlFunctionDataTypeID 
		
		/* Show the temp data in its current state */
		IF @ShowWorking = 1
		BEGIN
			SELECT '@OrderByColumnList' AS TempTable, t.* FROM @OrderByColumnList t ORDER BY t.AnyID
			
			SELECT '@ColumnList' AS TempTable, t.* FROM @ColumnList t ORDER BY t.AnyID
			
			SELECT '@CriteriaList' AS TempTable, t.* FROM @CriteriaList t ORDER BY t.AnyID
		END
		
	END
	ELSE
	BEGIN
		-- Too weird for this helper proc (eg "BEGIN" or "DECLARE @SomeVariables" or "EXEC _SomethingOrOther")
		-- Just output the whole thing and forget it.
		INSERT @TLine (TextLine, LineType) VALUES (@NonEditableQueryText, 'UNKNOWN')
		
		PRINT 'Too complex for auto-building'
	END
	

	/* Output the results */
	IF @ShowWorking = 1
	BEGIN
		SELECT *
		FROM @tline 
		ORDER BY linenum
	END
	
	/*********************************************************************************/
	/*                                                                               */
	/*                        Now to actually build the query                        */
	/*                                                                               */
	/*********************************************************************************/
	IF @ReallyDoTheInsertAndUpdates = 1 AND @ClientID IS NOT NULL
	BEGIN
		IF 0 = (SELECT sq.IsEditable FROM dbo.SqlQuery sq WHERE sq.QueryID = @SqlQueryID)
		BEGIN
			DELETE dbo.SqlQueryCriteria WHERE SqlQueryID = @SqlQueryID
			DELETE dbo.SqlQueryColumns WHERE SqlQueryID = @SqlQueryID
			DELETE dbo.SqlQueryTables WHERE SqlQueryID = @SqlQueryID
			
			/* Needs JoinTableID */
			INSERT INTO dbo.SqlQueryTables (SqlQueryID, ClientID, SqlQueryTableName, SourceID, JoinType, JoinText, TableAlias, TableDisplayOrder)
			SELECT @SqlQueryID, @ClientID, t.TableText, t.AnyID, t.JoinType, t.JoinText, t.AliasText, t.AnyID
			FROM @TableList t 
			
			/* Needs ManipulatedDataType ideally, but not at all sure how to achieve that */
			/* Same sort of problem for aggregates (MAX, MIN, AVERAGE etc) */
			INSERT INTO dbo.SqlQueryColumns (SqlQueryID, ClientID, SqlQueryTableID, CompleteOutputText, ColumnNaturalName, ColumnAlias, DisplayOrder, SortOrder, SortType, ColumnDataType, ShowColumn, IsAggregate)
			SELECT @SqlQueryID, @ClientID, sqt.SqlQueryTableID, c.RawText, c.ColumnText, c.AliasText, c.AnyID AS DisplayOrder, c.SortOrder, CASE WHEN c.SortOrder > 0 THEN CASE WHEN c.SortDesc = 1 THEN 'DESC' ELSE 'ASC' END  ELSE '' END AS SortType, c.DataTypeName, c.IsColumnVisible, 0 AS IsAggregate
			FROM @ColumnList c 
			INNER JOIN dbo.SqlQueryTables sqt WITH (NOLOCK) ON sqt.SourceID = c.TableAnyID 
			WHERE sqt.SqlQueryID = @SqlQueryID

			/* Add the criteria (column is NULL for security clauses, so that users cannot meddle with them) */
			INSERT INTO dbo.SqlQueryCriteria (SqlQueryID, ClientID, SqlQueryTableID, SqlQueryColumnID, CriteriaText, IsSecurityClause)
			SELECT @SqlQueryID, @ClientID, sqt.SqlQueryTableID, CASE IsSecurityClause WHEN 1 THEN NULL ELSE sqc.SqlQueryColumnID END AS ColumnAnyID, c.CriteriaText, c.IsSecurityClause
			FROM @CriteriaList c 
			INNER JOIN dbo.SqlQueryTables sqt WITH (NOLOCK) ON sqt.SourceID = c.TableAnyID 
			LEFT JOIN dbo.SqlQueryColumns sqc WITH (NOLOCK) ON sqc.SourceID = c.ColumnAnyID
			WHERE sqt.SqlQueryID = @SqlQueryID
			
			/* Now make it editable at last */
			UPDATE dbo.SqlQuery 
			SET IsEditable = 1 
			WHERE QueryID = @SqlQueryID
			
		END
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MakeQueryEditable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_MakeQueryEditable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MakeQueryEditable] TO [sp_executeall]
GO
