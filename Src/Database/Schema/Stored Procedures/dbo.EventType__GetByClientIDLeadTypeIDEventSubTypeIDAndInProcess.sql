SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO












/*
----------------------------------------------------------------------------------------------------
-- Date Created: 01-04-2008

-- Created By:  Ben
-- Purpose: 
-- SB  2014-07-10 Updated to use view which includes shared event types
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[EventType__GetByClientIDLeadTypeIDEventSubTypeIDAndInProcess] 
(
	@ClientID int,
	@LeadTypeID int,
	@EventSubTypeID int,
	@InProcess bit
)
AS

	SET ANSI_NULLS OFF

	SELECT et.*
	FROM
		dbo.fnEventTypeShared(@ClientID) et
	WHERE
		ClientID = @ClientID
		AND LeadTypeID = @LeadTypeID
		AND EventSubTypeID = @EventSubTypeID
		AND InProcess = @InProcess 
		AND (Enabled = 1) 
		AND (AvailableManually = 1) 
	ORDER BY EventTypeName

	SELECT @@ROWCOUNT
	SET ANSI_NULLS ON














GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetByClientIDLeadTypeIDEventSubTypeIDAndInProcess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetByClientIDLeadTypeIDEventSubTypeIDAndInProcess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetByClientIDLeadTypeIDEventSubTypeIDAndInProcess] TO [sp_executeall]
GO
