SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-08-26
-- Description:	Gets the policy summary for a pet
-- Modified:	SB	2014-10-09	Update to make the policy summary data be claim specific
--				SB	2015-12-17  As we could now get two excess amounts out for vet fees due to policy age, select the highest
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Policy_GetPolicySummary] 
(
	@CaseID INT 
	
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @LeadID INT,
			@MatterID INT
	SELECT	@LeadID = LeadID,
			@MatterID = MatterID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE CaseID = @CaseID

	DECLARE @CurrentPolicyMatterID INT
	SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)
	
	DECLARE @PolicyAdminMatterID	INT
	SELECT @PolicyAdminMatterID = ltr.FromMatterID
	FROM LeadTypeRelationship ltr WITH ( NOLOCK ) 
	WHERE ltr.ToLeadTypeID = 1490 /*Claims*/
	AND ltr.FromLeadTypeID = 1492 /*Policy Admin*/
	AND ltr.ToMatterID = @MatterID
	
	DECLARE @SchemeID INT
	SELECT @SchemeID = ValueInt
	FROM dbo.MatterDetailValues WITH (NOLOCK) 
	WHERE MatterID = @CurrentPolicyMatterID
	AND DetailFieldID = 145689
	
	DECLARE @Excess MONEY,
			@ExcessPercentage MONEY,
			@VolExcess MONEY

	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID
	
	DECLARE @VetFeesID INT /*GPR 2018-12-11 for C600 #1175*/
	--SELECT @VetFeesID = dbo.fn_C384_GetVetFeesResourceList(@ClientID,@CurrentPolicyMatterID)
	SELECT @VetFeesID = dbo.fn_C600_GetVetFeesResourceList(@ClientID,@CurrentPolicyMatterID)

	PRINT '@CurrentPolicyMatterID'
	PRINT @CurrentPolicyMatterID
	PRINT '@MatterID'
	PRINT @MatterID

	SELECT TOP 1 @Excess = Excess, @ExcessPercentage = ExcessPercentage, @VolExcess = VoluntaryExcess
	FROM dbo.fn_C00_1272_Policy_GetPolicyExcess(@CurrentPolicyMatterID, @MatterID)
	WHERE ResourceListID = @VetFeesID -- Limit to vet fees
	ORDER BY Excess DESC, ExcessPercentage DESC
	
	DECLARE @CoInsPercentage MONEY,
			@DateCutOff DATE, 
			@CoInsLinkedTo INT
	
	PRINT '@LeadID'
	PRINT @LeadID

	SELECT @CoInsPercentage = ExcessPercentage, @DateCutOff = DateCutOff, @CoInsLinkedTo = CoInsLinkedTo
	FROM dbo.fn_C00_1272_Policy_GetPolicyCoInsurance(@LeadID, @CurrentPolicyMatterID)
	
	PRINT '@CoInsPercentage'
	PRINT @CoInsPercentage

	DECLARE @Inception DATE , 
			@StartDate DATE ,
			@EndDate DATE 
	SELECT @Inception = MIN(tdvIncept.ValueDate), @StartDate = MIN(tdvStart.ValueDate), @EndDate = MAX(tdvEnd.ValueDate)
	FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@MatterID) r
	INNER JOIN dbo.TableDetailValues tdvIncept WITH (NOLOCK) ON r.TableRowID = tdvIncept.TableRowID AND tdvIncept.DetailFieldID = 145662
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664
	INNER JOIN dbo.TableDetailValues tdvScheme WITH (NOLOCK) ON r.TableRowID = tdvScheme.TableRowID AND tdvScheme.DetailFieldID = 145665
	WHERE tdvScheme.ResourceListID = @SchemeID
	
	SELECT	@Inception AS Inception, @StartDate AS StartDate, @EndDate AS EndDate,
			rdvAffinity.DetailValue AS Affinity, rdvProduct.DetailValue AS Product, llType.ItemValue AS PolicyType,
			(SELECT ll.ItemValue
			FROM dbo.MatterDetailValues mdv WITH (NOLOCK) 
			INNER JOIN dbo.LookupListItems ll WITH (NOLOCK) ON mdv.ValueInt = ll.LookupListItemID
			WHERE mdv.MatterID = @CurrentPolicyMatterID 
			AND mdv.DetailFieldID = 162633) AS ExcessType,
			--@Excess AS Excess,
			--ISNULL(dbo.fnGetSimpleDvAsMoney(175503,@PolicyAdminMatterID),@Excess)  AS Excess, /*Compulsory excess*/   /*GPR 2018-12-11 added ISNULL for C600 #1175*/
			@Excess AS Excess,
			@ExcessPercentage AS ExcessPercentage,
			@VolExcess AS VoluntaryExcess,
			dbo.fnGetSimpleDvAsMoney(175504,@PolicyAdminMatterID) AS CoInsurance, /*Co-Insurance*/
			@DateCutOff AS DateCutIn,
			(SELECT ItemValue FROM LookupListItems WHERE LookupListItemID = @CoInsLinkedTo) AS CoInsLinkedTo
	FROM dbo.ResourceList rl WITH (NOLOCK)
	LEFT JOIN dbo.ResourceListDetailValues rdvAffinity WITH (NOLOCK) ON rl.ResourceListID = rdvAffinity.ResourceListID AND rdvAffinity.DetailFieldID = 144314
	LEFT JOIN dbo.ResourceListDetailValues rdvProduct WITH (NOLOCK) ON rl.ResourceListID = rdvProduct.ResourceListID AND rdvProduct.DetailFieldID = 146200
	LEFT JOIN dbo.ResourceListDetailValues rdvType WITH (NOLOCK) ON rl.ResourceListID = rdvType.ResourceListID AND rdvType.DetailFieldID = 144319
	LEFT JOIN dbo.LookupListItems llType WITH (NOLOCK) ON rdvType.ValueInt = llType.LookupListItemID
	WHERE rl.ResourceListID = @SchemeID
	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetPolicySummary] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Policy_GetPolicySummary] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Policy_GetPolicySummary] TO [sp_executeall]
GO
