SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2010-10-06
-- Description:	Stop a batch job from running again.
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedTask__DoNotRunAgain] 
	@TaskID int,
	@ClientID int 
AS
BEGIN
	
	SET NOCOUNT ON;
	
	/* Kiil the task for good */
	UPDATE dbo.AutomatedTask 
	SET AlreadyRunning = 0,
	[Enabled] = 0,
	NextRunDateTime = NULL,
	RepeatTimeQuantity = NULL,
	RepeatTimeUnitsID = NULL,
	RunAtHour = NULL,
	RunAtMinute = NULL
	WHERE TaskID = @TaskID 
	AND ClientID = @ClientID /* Safety net */
	
	/* Clear any AutomatedTaskInfo just in case this task crashed on its last run */
	UPDATE dbo.AutomatedTaskInfo 
	SET LockDateTime = NULL 
	WHERE TaskID = @TaskID 
	AND ClientID = @ClientID /* Safety net */
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__DoNotRunAgain] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask__DoNotRunAgain] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__DoNotRunAgain] TO [sp_executeall]
GO
