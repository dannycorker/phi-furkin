SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







----------------------------------------------------------------------------------------------------
-- Date Created: 28-Feb-2008

-- Created By:  Ben Crinion
-- Purpose: Disables the DocumentType identified by the supplied DocumentTypeID
----------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[DocumentType__DisableDocumentType]
(
	@DocumentTypeID int 
)
AS
			
	UPDATE 
		[dbo].[DocumentType]
	SET [Enabled] = 0
	WHERE
		[DocumentTypeID] = @DocumentTypeID
					














GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__DisableDocumentType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType__DisableDocumentType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__DisableDocumentType] TO [sp_executeall]
GO
