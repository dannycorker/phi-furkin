SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DataLoaderDecodeType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderDecodeType_Update]
(

	@DataLoaderDecodeTypeID int   ,

	@OriginalDataLoaderDecodeTypeID int   ,

	@DecodeTypeName varchar (250)  ,

	@DecodeTypeDescription varchar (2000)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DataLoaderDecodeType]
				SET
					[DataLoaderDecodeTypeID] = @DataLoaderDecodeTypeID
					,[DecodeTypeName] = @DecodeTypeName
					,[DecodeTypeDescription] = @DecodeTypeDescription
				WHERE
[DataLoaderDecodeTypeID] = @OriginalDataLoaderDecodeTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDecodeType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderDecodeType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderDecodeType_Update] TO [sp_executeall]
GO
