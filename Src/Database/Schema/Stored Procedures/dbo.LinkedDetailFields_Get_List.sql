SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the LinkedDetailFields table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LinkedDetailFields_Get_List]

AS


				
				SELECT
					[LinkedDetailFieldID],
					[LeadTypeIDTo],
					[LeadTypeIDFrom],
					[DetailFieldIDTo],
					[DetailFieldIDFrom],
					[DisplayOnPageID],
					[FieldOrder],
					[Enabled],
					[History],
					[LeadOrMatter],
					[FieldName],
					[LeadLinkedTo],
					[LeadLinkedFrom],
					[IncludeLinkedField],
					[ClientID]
				FROM
					[dbo].[LinkedDetailFields] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LinkedDetailFields_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LinkedDetailFields_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LinkedDetailFields_Get_List] TO [sp_executeall]
GO
