SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2010-03-08
-- Description:	List all ClientPersonnel Options for a client/clientpersonnel record
-- =============================================
CREATE PROCEDURE [dbo].[cpo] 
	@ClientID int = NULL, 
	@ClientPersonnelID int = NULL 
AS
BEGIN
	SET NOCOUNT ON;

	IF @ClientID IS NULL AND @ClientPersonnelID IS NULL
	BEGIN
		SELECT 'Please enter @ClientID or @ClientPersonnelID'
		RETURN 
	END
	
	IF @ClientPersonnelID IS NOT NULL
	BEGIN
		SELECT cp.ClientPersonnelID, cp.UserName, cpo.* 
		FROM dbo.ClientPersonnel cp (nolock) 
		LEFT JOIN dbo.ClientPersonnelOptions cpo (nolock) ON cpo.ClientPersonnelID = cp.ClientPersonnelID 
		LEFT JOIN dbo.ClientPersonnelOptionTypes cpot (nolock) ON cpot.ClientPersonnelOptionTypeID = cpo.ClientPersonnelOptionTypeID 
		WHERE cp.ClientPersonnelID = @ClientPersonnelID 
		ORDER BY cpo.ClientPersonnelOptionID 
	END
	ELSE
	BEGIN
		SELECT cp.ClientPersonnelID, cp.UserName, cpo.* 
		FROM dbo.ClientPersonnel cp (nolock) 
		LEFT JOIN dbo.ClientPersonnelOptions cpo (nolock) ON cpo.ClientPersonnelID = cp.ClientPersonnelID 
		LEFT JOIN dbo.ClientPersonnelOptionTypes cpot (nolock) ON cpot.ClientPersonnelOptionTypeID = cpo.ClientPersonnelOptionTypeID 
		WHERE cp.ClientID = @ClientID 
		ORDER BY cp.UserName, cpo.ClientPersonnelOptionID ASC 
	END
	
	SELECT 'All possible options' as [All Possible Options], cpot.* 
	FROM dbo.ClientPersonnelOptionTypes cpot WITH (NOLOCK) 
	ORDER BY cpot.ClientPersonnelOptionTypeID
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[cpo] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[cpo] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[cpo] TO [sp_executeall]
GO
