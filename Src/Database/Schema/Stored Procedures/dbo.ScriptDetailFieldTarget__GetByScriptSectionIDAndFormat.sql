SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 06-03-2015
-- Description:	Gets the script detail field targets for the given section and type
-- =============================================
CREATE PROCEDURE [dbo].[ScriptDetailFieldTarget__GetByScriptSectionIDAndFormat]
	@SectionID int, 
	@Format varchar(250)
AS
BEGIN
	
	
	SET NOCOUNT ON;
	SELECT * FROM ScriptDetailFieldTarget WITH (NOLOCK) WHERE SectionID=@SectionID and Format=@Format
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget__GetByScriptSectionIDAndFormat] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDetailFieldTarget__GetByScriptSectionIDAndFormat] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget__GetByScriptSectionIDAndFormat] TO [sp_executeall]
GO
