SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2013-11-19
-- Description:	Get data for slider controls
--				Needs the number of fields to divide against, whether each is locked, the starting value, and the amount to divide
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SliderControlUpdateValues]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@SliderDetailFieldID INT,
	@Values dbo.tvpIntMoney READONLY,
	@ClientPersonnelID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @SliderData TABLE ( SliderNumber INT, Locked BIT, Value MONEY )
	
	DECLARE @DetailValueData dbo.tvpDetailValueData,
			@ClientID INT

	SELECT @ClientID = cp.ClientID
	FROM ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.ClientPersonnelID = @ClientPersonnelID

	DECLARE @Year1FieldID			INT,
			@Year2FieldID			INT,
			@Year3FieldID			INT,
			@Year4FieldID			INT,
			@Year5FieldID			INT,
			@Year6FieldID			INT,
			@Year7FieldID			INT,
			@YearFieldID			INT,
			@RoundingErrorFieldID	INT,
			@RoundingErrorValue		MONEY
			
	SELECT	@Year1FieldID	 = [725],
			@Year2FieldID	 = [726],
			@Year3FieldID	 = [727],
			@Year4FieldID	 = [728],
			@Year5FieldID	 = [729],
			@Year6FieldID	 = [730],
			@Year7FieldID	 = [825],
			@YearFieldID	 = [731]
	FROM
		(	
		SELECT fm.ThirdPartyFieldID, fm.DetailFieldID
		FROM ThirdPartyFieldMapping fm WITH (NOLOCK) 
		WHERE fm.ThirdPartyFieldGroupID = 62
		and fm.ClientID = @ClientID
		)
		AS ToPivot		
	PIVOT		
	(		
		Max([DetailFieldID])
	FOR		
	[ThirdPartyFieldID]
		IN ( [731],[730],[729],[728],[727],[726],[725],[825] )
	) AS Pivoted

	IF @SliderDetailFieldID = 181923 /*Fee Slider*/
	BEGIN
		/*Populate a tvp with the new data, then run a bulk save*/
		INSERT @DetailValueData (DetailValueID, ClientID, DetailFieldID, DetailValue, ObjectID, DetailFieldSubType)
		SELECT  cdv.CustomerDetailValueID, cdv.ClientID, cdv.DetailFieldID, ISNULL(v.Value,0.00), cdv.CustomerID, 10
		FROM @Values v 
		INNER JOIN CustomerDetailValues cdv WITH (NOLOCK) 
			on cdv.DetailFieldID = 
					CASE v.ID 
					WHEN 1 THEN @Year1FieldID 
					WHEN 2 THEN @Year2FieldID 
					WHEN 3 THEN @Year3FieldID 
					WHEN 4 THEN @Year4FieldID 
					WHEN 5 THEN @Year5FieldID 
					WHEN 6 THEN @Year6FieldID 
					WHEN 7 THEN @Year7FieldID
					END 
		WHERE cdv.CustomerID = @CustomerID
		
		/*Adjust for rounding errors coming from the control*/
		SELECT TOP(1) @RoundingErrorFieldID = dvd.DetailFieldID
		FROM @DetailValueData dvd 
		ORDER BY dvd.DetailValue DESC
		
		SELECT @RoundingErrorValue = 100.00 - SUM( CAST(dvd.DetailValue AS MONEY) )
		FROM @DetailValueData dvd
		
		IF @RoundingErrorValue <> 0.00
		BEGIN
			UPDATE dvd
			SET DetailValue = dvd.DetailValue + @RoundingErrorValue
			FROM @DetailValueData dvd
			WHERE dvd.DetailFieldID = @RoundingErrorFieldID
		END

		/*Save the values*/
		EXEC DetailValues_BulkSave @DetailValueData, @ClientID, @ClientPersonnelID
	END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SliderControlUpdateValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SliderControlUpdateValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SliderControlUpdateValues] TO [sp_executeall]
GO
