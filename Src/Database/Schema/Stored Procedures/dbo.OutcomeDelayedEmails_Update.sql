SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the OutcomeDelayedEmails table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[OutcomeDelayedEmails_Update]
(

	@OutcomeDelayedEmailID int   ,

	@CustomerID int   ,

	@ClientQuestionnaireID int   ,

	@DateOutcomeEmailSent datetime   ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[OutcomeDelayedEmails]
				SET
					[CustomerID] = @CustomerID
					,[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[DateOutcomeEmailSent] = @DateOutcomeEmailSent
					,[ClientID] = @ClientID
				WHERE
[OutcomeDelayedEmailID] = @OutcomeDelayedEmailID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeDelayedEmails_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutcomeDelayedEmails_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeDelayedEmails_Update] TO [sp_executeall]
GO
