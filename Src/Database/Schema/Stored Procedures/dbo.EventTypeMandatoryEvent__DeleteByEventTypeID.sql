SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventTypeMandatoryEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryEvent__DeleteByEventTypeID]
(

	@EventTypeID int   
)
AS


	DELETE FROM [dbo].[EventTypeMandatoryEvent] WITH (ROWLOCK) 
	WHERE
		[ToEventTypeID] = @EventTypeID
		
			





GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent__DeleteByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryEvent__DeleteByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryEvent__DeleteByEventTypeID] TO [sp_executeall]
GO
