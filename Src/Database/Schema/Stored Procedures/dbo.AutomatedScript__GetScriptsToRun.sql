SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =================================================
-- Author:		Simon Brushett
-- ALTER date: 2012-04-26
-- Description:	Gets a list of automated scripts that should be run now - Calls into AquariusMaster
-- =================================================
CREATE PROCEDURE [dbo].[AutomatedScript__GetScriptsToRun] 

AS
BEGIN

	EXEC AquariusMaster.dbo.AutomatedScript__GetScriptsToRun
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedScript__GetScriptsToRun] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedScript__GetScriptsToRun] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedScript__GetScriptsToRun] TO [sp_executeall]
GO
