SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Alex Elger
-- Create date: 23-Nov-2009
-- Description:	Proc to Run SAE for Client 144 Cheshire East
-- UPDATE:		Simon Brushett 2011-06-07
--				Changed to use _C00_BankLoads and handle 183 contributions into the same account
--				ACE 2012-01-24 Copied for LBH and Made Generic
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ReconcileBank]
(
@ClientID int,
@CustomerRefFieldID int, /*AKA Paris Number Field*/
@PaymentImportLeadID int,
@BankRefTableFieldID int,
@LoadIf2CardsFieldID int,
@NextPaymentDueFieldID int,
@ClientContributionTableFieldID int,
@ClientContributionHistoryDateFieldID int,
@ClientContributionHistoryValueFieldID int,
@ClientContributionHistoryNextPaymentFieldID int,
@UnreconcliedTableDetailFieldID int,
@PaymentImportDateLoadedFieldID int,
@PaymentImportValueFieldID int,
@PaymentImportReferenceFieldID int,
@PaymentImportPNumberFieldID int,
@PaymentImportSysMsgFieldID int,
@PaymentImportPaymentTypeFieldID int,
@PaymentImportNotesFieldID int,
@PaymentImportWhenRecFieldID int,
@PaymentImportWhoRecFieldID int,
@PaymentImportNewRoadIDFieldID int,
@PaymentImportMatterValue int
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TestID INT = 0, 
			@ClientContributionTableFieldPageID int,
			@UnreconcliedTableDetailFieldPageID int,
			@CEPPID INT, 
			@Text VARCHAR(255), 
			@LeadID INT,
			@MatterID INT,
			@TableRowID INT,
			@NextPaymentDueDate DATETIME, 
			@ParisNo VARCHAR(100),
			@ContributionValue MONEY,
			@TransRef VARCHAR(10),
			@MultipleCards bit = 0,
			@ParisNumberRowCount int,
			@NationalInsuranceNo BIT

	SELECT TOP 1 @CEPPID = cep.CEPPID, @Text = TEXT
	FROM _C00_BankLoads cep WITH (NOLOCK)
	WHERE Processed = 0 AND ISDuplicate = 0
	AND ClientID = @ClientID
	ORDER BY cep.CEPPID

	SELECT @ClientContributionTableFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @ClientContributionTableFieldID

	SELECT @UnreconcliedTableDetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @UnreconcliedTableDetailFieldID

	WHILE @CEPPID > @TestID
	BEGIN

		-- Initialise NI match
		SELECT @NationalInsuranceNo = 0

		IF (LEFT(@TEXT,3) NOT IN ('VOL','HDR','EOF','UTL','UHL'))
		BEGIN
		
			SELECT @ParisNo = NULL, @ContributionValue = NULL, @MatterID = NULL, @LeadID = NULL, @NextPaymentDueDate = NULL, @MultipleCards = 0
		
			SELECT @ParisNo = rtrim(ltrim(Reference)), @ContributionValue = cep.Value, @TransRef = cep.TransactionRef /*Trim added by CS for support ticket #8035*/
			FROM _C00_BankLoads cep WITH (NOLOCK) 
			WHERE cep.CEPPID = @CEPPID
			AND ClientID = @ClientID
			
			-- Check to see if the reference is a national insurance number
			SELECT @NationalInsuranceNo = dbo.RegexMatch(@ParisNo, '[ABCEGHJKLMNOPRSTWXYZ][ABCEGHJKLMNPRSTWXYZ][0-9]{6}[A-D ]')
			
			IF @NationalInsuranceNo = 0 -- Not matched on NI number so not BM program - carry on processing as normal
			BEGIN
			
				SELECT @MatterID = MatterID, @LeadID = LeadID
				FROM MatterDetailValues mdv WITH (NOLOCK)
				WHERE DetailFieldID = @CustomerRefFieldID
				AND DetailValue = @ParisNo
				
				SELECT @ParisNumberRowCount = @@ROWCOUNT
				
				IF @ParisNumberRowCount = 0
				BEGIN
				
					/*Paris Number could not be matched at all. We should therefor check the bank ref to paris number table*/
					SELECT @ParisNo = tdv_pno.DetailValue
					FROM TableDetailValues tdv_bankref WITH (NOLOCK) 
					INNER JOIN TableDetailValues tdv_pno WITH (NOLOCK) ON tdv_pno.TableRowID = tdv_bankref.TableRowID and tdv_pno.DetailFieldID = 139575
					WHERE tdv_bankref.LeadID = @PaymentImportLeadID 
					AND tdv_bankref.DetailFieldID = @BankRefTableFieldID
					and tdv_bankref.DetailValue = @ParisNo
				
					SELECT @MatterID = mdv.MatterID, @LeadID = mdv.LeadID
					FROM MatterDetailValues mdv WITH (NOLOCK)
					INNER JOIN Matter m with (NOLOCK) on m.MatterID = mdv.MatterID 
					inner join Cases c with (nolock) on c.CaseID = m.CaseID and c.AquariumStatusID = 2
					WHERE DetailFieldID = @CustomerRefFieldID
					AND DetailValue = @ParisNo
				
					SELECT @ParisNumberRowCount = @@ROWCOUNT

				END
				
				IF @ParisNumberRowCount > 1
				BEGIN
				
					SELECT @MatterID = mdv.MatterID, @LeadID = mdv.LeadID
					FROM MatterDetailValues mdv WITH (NOLOCK)
					INNER JOIN MatterDetailValues mdv_loadif2cards WITH (NOLOCK) ON mdv_loadif2cards.MatterID = mdv.MatterID and mdv_loadif2cards.DetailFieldID = @LoadIf2CardsFieldID and mdv_loadif2cards.ValueInt = 1
					INNER JOIN Matter m with (NOLOCK) on m.MatterID = mdv.MatterID 
					inner join Cases c with (nolock) on c.CaseID = m.CaseID and c.AquariumStatusID = 2
					WHERE mdv.DetailFieldID = @CustomerRefFieldID
					AND mdv.DetailValue = @ParisNo
				
					IF @@ROWCOUNT <> 1
					BEGIN
					
						SELECT @MultipleCards = 1
					
					END
				
				END
				ELSE 
				IF @MatterID IS NULL AND @MultipleCards = 0
				BEGIN

					SELECT @MatterID = MatterID, @LeadID = LeadID
					FROM MatterDetailValues mdv WITH (NOLOCK)
					WHERE DetailFieldID = @CustomerRefFieldID
					AND DetailValue IN (SELECT * FROM dbo.fnTableOfValues (@ParisNo,' '))			

					IF @@ROWCOUNT > 0
					BEGIN
					
						SELECT @MatterID = mdv.MatterID, @LeadID = mdv.LeadID
						FROM MatterDetailValues mdv WITH (NOLOCK)
						INNER JOIN MatterDetailValues mdv_loadif2cards WITH (NOLOCK) ON mdv_loadif2cards.MatterID = mdv.MatterID and mdv_loadif2cards.DetailFieldID = @LoadIf2CardsFieldID and mdv_loadif2cards.ValueInt = 1
						WHERE mdv.DetailFieldID = @CustomerRefFieldID
						AND mdv.DetailValue IN (SELECT * FROM dbo.fnTableOfValues (@ParisNo,' '))	
					
						IF @@ROWCOUNT <> 1
						BEGIN
						
							SELECT @MultipleCards = 1
						
						END
					
					END

				END
			
			END
			
			SELECT TOP 1 @NextPaymentDueDate = tdv.ValueDate
			FROM TableDetailValues tdv WITH (NOLOCK)
			WHERE tdv.MatterID = @MatterID
			AND tdv.DetailFieldID = @NextPaymentDueFieldID
			AND tdv.ValueDate > dbo.fn_GetDate_Local()
			ORDER BY tdv.ValueDate
		
			/*SELECT @ParisNo, @LeadID, @MatterID, @NextPaymentDueDate, @ContributionValue, @TransRef*/

			IF @MatterID IS NOT NULL AND @ParisNo IS NOT NULL AND @ContributionValue IS NOT NULL AND @NextPaymentDueDate IS NOT NULL AND @TransRef <> 24 AND @TransRef IS NOT NULL AND @MultipleCards = 0
			BEGIN
		
				IF @TestID = 0 
				BEGIN
				
					SELECT 'LeadID, MatterID, TableRowID, Contribution Value, NextPaymentDueDate'
				
				END
				
				/*Add Client Contribution Row to Client Matter*/
				INSERT INTO TableRows (ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, DenyDelete)
				SELECT @ClientID, @LeadID, @MatterID, @ClientContributionTableFieldID, @ClientContributionTableFieldPageID, 1
		
				SELECT @TableRowID = SCOPE_IDENTITY()

				INSERT INTO TableDetailValues (ClientID, LeadID, MatterID, TableRowID, ResourceListID, DetailFieldID, DetailValue)
				VALUES (@ClientID, @LeadID, @MatterID, @TableRowID, NULL, @ClientContributionHistoryDateFieldID, CONVERT(VARCHAR(10), dbo.fn_GetDate_Local(), 120)),
						(@ClientID, @LeadID, @MatterID, @TableRowID, NULL, @ClientContributionHistoryValueFieldID, CONVERT(VARCHAR(10),@ContributionValue)),
						(@ClientID, @LeadID, @MatterID, @TableRowID, NULL, @ClientContributionHistoryNextPaymentFieldID, CONVERT(VARCHAR(10), @NextPaymentDueDate, 120))

				SELECT @LeadID, @MatterID, @TableRowID, @ContributionValue, @NextPaymentDueDate

				UPDATE _C00_BankLoads 
				SET Processed = 1, MatterID = @MatterID, TableRowID = @TableRowID
				WHERE CEPPID = @CEPPID	
				AND ClientID = @ClientID	

			END
			ELSE 
			IF @NationalInsuranceNo = 0
			BEGIN
			
				SELECT TOP 1 @MatterID = m.MatterID
				FROM Matter m WITH (NOLOCK)
				WHERE m.LeadID = @PaymentImportLeadID 
				ORDER BY MatterID DESC
		
				/* If the Matter IS NULL or ParisNo IS NULL, or Value IS NULL or 
				Next Payment Due Date is NULL or @MatterID is null then add a 
				record into the unreconciled payments table */
				INSERT INTO TableRows (ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete)
				SELECT @ClientID, NULL, @MatterID, @UnreconcliedTableDetailFieldID, @UnreconcliedTableDetailFieldPageID, 0, 1
		
				SELECT @TableRowID = SCOPE_IDENTITY()

				INSERT INTO TableDetailValues (ClientID, LeadID, MatterID, TableRowID, ResourceListID, DetailFieldID, DetailValue)
				VALUES (@ClientID, NULL, @MatterID, @TableRowID, NULL, @PaymentImportDateLoadedFieldID, CONVERT(VARCHAR(10), dbo.fn_GetDate_Local(), 120)),
					(@ClientID, NULL, @MatterID, @TableRowID, NULL, @PaymentImportValueFieldID, CONVERT(VARCHAR(10),@ContributionValue)),
					(@ClientID, NULL, @MatterID, @TableRowID, NULL, @PaymentImportReferenceFieldID, CONVERT(VARCHAR(100), @ParisNo)),
					(@ClientID, NULL, @MatterID, @TableRowID, NULL, @PaymentImportPNumberFieldID, ''),
					(@ClientID, NULL, @MatterID, @TableRowID, NULL, @PaymentImportSysMsgFieldID, ''),
					(@ClientID, NULL, @MatterID, @TableRowID, NULL, @PaymentImportPaymentTypeFieldID, ''),
					(@ClientID, NULL, @MatterID, @TableRowID, NULL, @PaymentImportNotesFieldID, ''),
					(@ClientID, NULL, @MatterID, @TableRowID, NULL, @PaymentImportWhenRecFieldID, ''),
					(@ClientID, NULL, @MatterID, @TableRowID, NULL, @PaymentImportWhoRecFieldID, ''),
					(@ClientID, NULL, @MatterID, @TableRowID, NULL, @PaymentImportNewRoadIDFieldID, '')
					
				UPDATE MatterDetailValues
				SET DetailValue = ISNULL((
					SELECT SUM(ValueMoney)
					FROM TableDetailValues tdv WITH (NOLOCK)
					INNER JOIN TableRows tr WITH (NOLOCK) ON tr.TableRowID = tdv.TableRowID and tr.DetailFieldID = @UnreconcliedTableDetailFieldID
					WHERE tdv.MatterID = @MatterID
					and tdv.DetailFieldID = @PaymentImportValueFieldID
				),0.00)
				FROM MatterDetailValues 
				WHERE MatterDetailValues.MatterID = @MatterID
				and MatterDetailValues.DetailFieldID = @PaymentImportMatterValue

			END
		
		END	
		
		IF @NationalInsuranceNo = 1
		BEGIN
		
			-- We have a match for NI number so this is a client in the BM program
			-- Update to correct Client and SubClient
			UPDATE _C00_BankLoads
			SET ClientID = 183, SubClientID = 15
			WHERE CEPPID = @CEPPID
			AND ClientID = @ClientID
			
		END
		ELSE 
		BEGIN

			UPDATE _C00_BankLoads
			SET Processed = 1, DateLoaded = dbo.fn_GetDate_Local()
			WHERE CEPPID = @CEPPID
			AND ClientID = @ClientID
		
		END
		

		SELECT @TestID = @CEPPID

		SELECT TOP 1 @CEPPID = cep.CEPPID, @Text = TEXT
		FROM _C00_BankLoads cep WITH (NOLOCK)
		WHERE Processed = 0 AND ISDuplicate = 0
		AND CEPPID > @CEPPID
		AND ClientID = @ClientID

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ReconcileBank] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ReconcileBank] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ReconcileBank] TO [sp_executeall]
GO
