SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Substitutions table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Substitutions_GetBySubstitutionsID]
(

	@SubstitutionsID int   
)
AS


				SELECT
					[SubstitutionsID],
					[SubstitutionsVariable],
					[SubstitutionsDisplayName],
					[SubstitutionsDescription]
				FROM
					[dbo].[Substitutions] WITH (NOLOCK) 
				WHERE
										[SubstitutionsID] = @SubstitutionsID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Substitutions_GetBySubstitutionsID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Substitutions_GetBySubstitutionsID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Substitutions_GetBySubstitutionsID] TO [sp_executeall]
GO
