SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ClientAreaMailingLists table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientAreaMailingLists_Get_List]

AS


				
				SELECT
					[ClientAreaMailingListID],
					[Name],
					[Email],
					[PostCode],
					[OutcomeID],
					[YellowPagesAreaCode],
					[ClientID],
					[ClientQuestionnaireID],
					[OnHold]
				FROM
					[dbo].[ClientAreaMailingLists] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientAreaMailingLists_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientAreaMailingLists_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientAreaMailingLists_Get_List] TO [sp_executeall]
GO
