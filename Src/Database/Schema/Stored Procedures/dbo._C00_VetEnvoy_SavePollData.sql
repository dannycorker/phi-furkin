SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-05-21
-- Description:	Saves the poll data from VetEnvoy for processing
-- =============================================
CREATE PROCEDURE [dbo].[_C00_VetEnvoy_SavePollData]
(
	@ClientID INT,
	@Xml XML
)
AS
BEGIN
	SET NOCOUNT ON;
	
	-- We need to wrap this in a tran and only commit if all succeeds	
	
	-- Get all the raw data
	DECLARE @Conversations TABLE
	(
		ID INT IDENTITY,
		Service VARCHAR(50),
		Status VARCHAR(50),
		ConversationID VARCHAR(50)
	)
	INSERT @Conversations (Service, Status, ConversationID)
	SELECT	T.N.value('@t', 'VARCHAR(50)') AS Service,
			T.N.value('@s', 'VARCHAR(50)') AS Status,
			T.N.value('@id', 'VARCHAR(50)') AS ConversationID
	FROM @Xml.nodes('//c') AS T(N)
	
	-- Save the table rows
	DECLARE @TableRowIDs TABLE
	(
		ID INT IDENTITY,
		TableRowID INT
	)
	INSERT INTO dbo.TableRows (ClientID, DetailFieldID, DetailFieldPageID)
	OUTPUT inserted.TableRowID INTO @TableRowIDs
	VALUES(224, 162657, 18185)
	
	-- And now the fields
	DECLARE @FieldIDs TABLE (FieldID INT)
	INSERT @FieldIDs (FieldID) VALUES
	(162658), -- Conversation ID
	(162659), -- Received
	(162660), -- Service
	(162661)  -- Status
	
	INSERT INTO dbo.TableDetailValues  (TableRowID, DetailFieldID, ClientID, DetailValue)
	SELECT	t.TableRowID, f.FieldID, 224, 
			CASE f.FieldID
				WHEN 162658 THEN c.ConversationID
				WHEN 162659 THEN CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
				WHEN 162660 THEN c.Service
				WHEN 162661 THEN c.Status
			END
	FROM @Conversations c
	INNER JOIN @TableRowIDs t ON c.ID = t.ID
	CROSS JOIN @FieldIDs f
	
	-- Save the updated poll ref
	DECLARE @PollRef INT
	SELECT @PollRef = @Xml.value('/node()[1]/@ref', 'INT')
	EXEC dbo._C00_SimpleValueIntoField 162656, @PollRef, 224

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_VetEnvoy_SavePollData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_VetEnvoy_SavePollData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_VetEnvoy_SavePollData] TO [sp_executeall]
GO
