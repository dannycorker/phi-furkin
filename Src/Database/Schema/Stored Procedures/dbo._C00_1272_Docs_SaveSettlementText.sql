SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-04-03
-- Description:	Saves the edited text for the settlement 
-- Used by:		df_157745
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Docs_SaveSettlementText] 
(
	@MatterID INT,
	@DeadPetID INT,
	@DeadPetValue VARCHAR(2000),
	@SummaryID INT,
	@SummaryValue VARCHAR(2000),
	@MultiPagee dbo.tvpIntIntVarchar READONLY,
	@Deductions dbo.tvpIntIntVarchar READONLY
)

AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	IF @DeadPetID > 0
	BEGIN
		UPDATE dbo.MatterDetailValues
		SET DetailValue = @DeadPetValue
		WHERE MatterID = @MatterID
		AND DetailFieldID = 156628
		AND ClientID = @ClientID
	END
	ELSE
	BEGIN
		INSERT dbo.MatterDetailValues (ClientID, MatterID, LeadID, DetailFieldID, DetailValue)
		SELECT @ClientID, MatterID, LeadID, 156628, @DeadPetValue
		FROM dbo.Matter WITH (NOLOCK) 
		WHERE MatterID = @MatterID
	END
	
	IF @SummaryID > 0
	BEGIN
		UPDATE dbo.MatterDetailValues
		SET DetailValue = @SummaryValue
		WHERE MatterID = @MatterID
		AND DetailFieldID = 152764
		AND ClientID = @ClientID
	END
	ELSE
	BEGIN
		INSERT dbo.MatterDetailValues (ClientID, MatterID, LeadID, DetailFieldID, DetailValue)
		SELECT @ClientID, MatterID, LeadID, 152764, @SummaryValue
		FROM dbo.Matter WITH (NOLOCK) 
		WHERE MatterID = @MatterID
	END
	
	
	-- ID > 0 AND Delete = 1 - Delete table row
	-- Multi payee
	DELETE dbo.TableRows 
	WHERE ClientID = @ClientID
	AND DetailFieldID = 152765
	AND TableRowID IN
	(
		SELECT m.AnyID1
		FROM @MultiPagee m
		WHERE m.AnyID1 > 0
		AND m.AnyID2 = 1
	)
	
	---- Deductions
	--DELETE dbo.TableRows 
	--WHERE ClientID = @ClientID
	--AND DetailFieldID = aq 1,2,148349
	--AND TableRowID IN
	--(
	--	SELECT d.AnyID1
	--	FROM @Deductions d
	--	WHERE d.AnyID1 > 0
	--	AND d.AnyID2 = 1
	--)
	
	-- ID > 0 AND Delete = 0 - Add on CHAR(13) + CHAR(10) if required and update
	-- Multi payee
	UPDATE tdv
	SET tdv.DetailValue =	CASE
								WHEN m.AnyValue LIKE '%' + CHAR(13) + CHAR(10) THEN m.AnyValue
								ELSE m.AnyValue + CHAR(13) + CHAR(10)
							END
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON r.TableRowID = tdv.TableRowID AND tdv.DetailFieldID = 148348
	INNER JOIN @MultiPagee m ON m.AnyID1 = r.TableRowID
	WHERE r.ClientID = @ClientID
	AND r.DetailFieldID = 152765
	AND m.AnyID1 > 0
	AND m.AnyID2 = 0
	
	-- Deductions
	UPDATE tdv
	SET tdv.DetailValue =	CASE
								WHEN d.AnyValue LIKE '%' + CHAR(13) + CHAR(10) THEN d.AnyValue
								ELSE d.AnyValue + CHAR(13) + CHAR(10)
							END
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON r.TableRowID = tdv.TableRowID AND tdv.DetailFieldID = 148348
	INNER JOIN @Deductions d ON d.AnyID1 = r.TableRowID
	WHERE r.ClientID = @ClientID
	AND r.DetailFieldID = 148349
	AND d.AnyID1 > 0
	AND d.AnyID2 = 0
	

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Docs_SaveSettlementText] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Docs_SaveSettlementText] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Docs_SaveSettlementText] TO [sp_executeall]
GO
