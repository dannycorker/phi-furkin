SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventTypeMandatoryField table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeMandatoryField_GetByEventTypeIDDetailFieldID]
(

	@EventTypeID int   ,

	@DetailFieldID int   
)
AS


				SELECT
					[EventTypeMandatoryFieldID],
					[EventTypeID],
					[DetailFieldID],
					[LeadTypeID],
					[ClientID],
					[ForceEditableOverride]
				FROM
					[dbo].[EventTypeMandatoryField] WITH (NOLOCK) 
				WHERE
										[EventTypeID] = @EventTypeID
					AND [DetailFieldID] = @DetailFieldID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryField_GetByEventTypeIDDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeMandatoryField_GetByEventTypeIDDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeMandatoryField_GetByEventTypeIDDetailFieldID] TO [sp_executeall]
GO
