SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ScriptStandardTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptStandardTarget_Delete]
(

	@ScriptStandardTargetID int   
)
AS


				DELETE FROM [dbo].[ScriptStandardTarget] WITH (ROWLOCK) 
				WHERE
					[ScriptStandardTargetID] = @ScriptStandardTargetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptStandardTarget_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget_Delete] TO [sp_executeall]
GO
