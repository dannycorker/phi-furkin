SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientPersonnel table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnel_GetByClientPersonnelID]
(

	@ClientPersonnelID int   
)
AS


				SELECT
					[ClientPersonnelID],
					[ClientID],
					[ClientOfficeID],
					[TitleID],
					[FirstName],
					[MiddleName],
					[LastName],
					[JobTitle],
					[Password],
					[ClientPersonnelAdminGroupID],
					[MobileTelephone],
					[HomeTelephone],
					[OfficeTelephone],
					[OfficeTelephoneExtension],
					[EmailAddress],
					[ChargeOutRate],
					[UserName],
					[Salt],
					[AttemptedLogins],
					[AccountDisabled],
					[ManagerID],
					[Initials],
					[LanguageID],
					[SubClientID],
					[ForcePasswordChangeOn],
					[ThirdPartySystemId],
					[CustomerID],
					[IsAquarium],
					[AllowSmsCommandProcessing],
					[MemorableWord],
					[MemorableWordSalt],
					[MemorableWordAttempts],
					[PendingActivation]
				FROM
					[dbo].[ClientPersonnel] WITH (NOLOCK) 
				WHERE
										[ClientPersonnelID] = @ClientPersonnelID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_GetByClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel_GetByClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_GetByClientPersonnelID] TO [sp_executeall]
GO
