SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetClientAreaMailingListForOutcome] @ClientQuestionnaireID int, @OutcomeID int
AS

Select ClientAreaMailingLists.Name, Email, PostCode, OutcomeID, YellowPagesAreaCode, ClientID, ClientQuestionnaireID, ClientAreaMailingListID, OnHold
From ClientAreaMailingLists
Where ( (ClientQuestionnaireID = @ClientQuestionnaireID) and (OutcomeID = @OutcomeID) )



GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientAreaMailingListForOutcome] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetClientAreaMailingListForOutcome] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientAreaMailingListForOutcome] TO [sp_executeall]
GO
