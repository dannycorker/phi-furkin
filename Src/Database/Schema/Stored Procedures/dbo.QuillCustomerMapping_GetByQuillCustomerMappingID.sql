SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuillCustomerMapping table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillCustomerMapping_GetByQuillCustomerMappingID]
(

	@QuillCustomerMappingID int   
)
AS


				SELECT
					[QuillCustomerMappingID],
					[ClientID],
					[CustomerID],
					[QuillClientCode]
				FROM
					[dbo].[QuillCustomerMapping] WITH (NOLOCK) 
				WHERE
										[QuillCustomerMappingID] = @QuillCustomerMappingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCustomerMapping_GetByQuillCustomerMappingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillCustomerMapping_GetByQuillCustomerMappingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCustomerMapping_GetByQuillCustomerMappingID] TO [sp_executeall]
GO
