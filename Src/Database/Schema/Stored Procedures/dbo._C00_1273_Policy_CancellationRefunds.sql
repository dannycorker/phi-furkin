SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


 
-- =============================================
-- Author:		Robin Hall
-- Create date: 2013-11-07
-- Description:	Invoked 10 days after a cancellation that might require refunds
-- DCM 15/09/2014 Copied from 235
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Policy_CancellationRefunds] 
(
	@CaseID INT
)
 
AS
BEGIN
 
	DECLARE 
		@LeadID INT
		,@MatterID INT
		,@CancellationReason INT
		,@CancellationDate DATE
		,@PolicyStartDate DATE
		,@PolicyType INT
		,@ClientID INT
		
	-- Get details	
	SELECT TOP 1
	@LeadID = m.LeadID
	,@MatterID = m.MatterID
	,@CancellationReason = mdvCancelReason.DetailValue
	,@CancellationDate = mdvCancelDate.ValueDate
	,@PolicyType = rldvPolType.ValueInt
	,@PolicyStartDate = mdvPolStart.ValueDate
	,@ClientID=m.ClientID
	FROM Matter m WITH (NOLOCK)
	INNER JOIN dbo.MatterDetailValues mdvCancelReason WITH (NOLOCK) ON mdvCancelReason.MatterID = m.MatterID AND mdvCancelReason.DetailFieldID = 170054
	INNER JOIN dbo.MatterDetailValues mdvCancelDate WITH (NOLOCK) ON mdvCancelDate.MatterID = m.MatterID AND mdvCancelDate.DetailFieldID = 170055
	INNER JOIN dbo.MatterDetailValues mdvProduct WITH (NOLOCK) ON mdvProduct.MatterID = m.MatterID AND mdvProduct.DetailFieldID = 170034
	INNER JOIN dbo.ResourceListDetailValues rldvPolType WITH (NOLOCK) ON rldvPolType.ResourceListID = mdvProduct.ValueInt AND rldvPolType.DetailFieldID = 144319
	INNER JOIN dbo.MatterDetailValues mdvPolStart WITH (NOLOCK) ON mdvPolStart.MatterID = m.MatterID AND mdvPolStart.DetailFieldID = 170036
	WHERE m.CaseID = @CaseID
	
	
	/* Financials */
	
	DECLARE @TotalRefundTotal DECIMAL(18,2)
	DECLARE @TotalRefundSubTotal DECIMAL(18,2)
	DECLARE @TotalRefundTax DECIMAL(18,2)
	
	DECLARE @PremiumRefunds TABLE (ID INT IDENTITY, PremiumRowID INT, DateFrom DATE, DateTo DATE, PaidTotal MONEY, RefundTotal MONEY, RefundSubTotal MONEY)
	
	-- All premium rows with PaidTo after Cancellation and Status of Paid
	INSERT @PremiumRefunds (PremiumRowID,DateFrom,DateTo,PaidTotal,RefundTotal,RefundSubTotal)
	SELECT r.TableRowID, tdvPaidFromDate.ValueDate, tdvPaidToDate.ValueDate, tdvPaidTotal.ValueMoney,
		CASE 
			WHEN tdvPaidFromDate.ValueDate >= @CancellationDate 
			THEN tdvPaidTotal.ValueMoney
			ELSE (tdvPaidTotal.ValueMoney *(DATEDIFF(dd,@CancellationDate,tdvPaidToDate.ValueDate))
					/(DATEDIFF(dd,tdvPaidFromDate.ValueDate,tdvPaidToDate.ValueDate)+1))
		END,
		CASE 
			WHEN tdvPaidFromDate.ValueDate >= @CancellationDate 
			THEN tdvPaidSubTotal.ValueMoney
			ELSE (tdvPaidSubTotal.ValueMoney *(DATEDIFF(dd,@CancellationDate,tdvPaidToDate.ValueDate))
					/(DATEDIFF(dd,tdvPaidFromDate.ValueDate,tdvPaidToDate.ValueDate)+1))
		END
	FROM Matter m WITH (NOLOCK)
	INNER JOIN dbo.TableRows r WITH (NOLOCK) ON r.MatterID = m.MatterID AND r.DetailFieldID = 170088 AND r.DetailFieldPageID = 19011
	INNER JOIN dbo.TableDetailValues tdvPaidFromDate WITH (NOLOCK) ON tdvPaidFromDate.TableRowID = r.TableRowID AND tdvPaidFromDate.DetailFieldID = 170076
	INNER JOIN dbo.TableDetailValues tdvPaidToDate WITH (NOLOCK) ON tdvPaidToDate.TableRowID = r.TableRowID AND tdvPaidToDate.DetailFieldID = 170077
	INNER JOIN dbo.TableDetailValues tdvPaidStatus WITH (NOLOCK) ON tdvPaidStatus.TableRowID = r.TableRowID AND tdvPaidStatus.DetailFieldID = 170078
	INNER JOIN dbo.TableDetailValues tdvPaidPayRowID WITH (NOLOCK) ON tdvPaidPayRowID.TableRowID = r.TableRowID AND tdvPaidPayRowID.DetailFieldID = 170073
	INNER JOIN dbo.TableDetailValues tdvPaidTotal WITH (NOLOCK) ON tdvPaidTotal.TableRowID = r.TableRowID AND tdvPaidTotal.DetailFieldID = 170072
	INNER JOIN dbo.TableDetailValues tdvPaidSubTotal WITH (NOLOCK) ON tdvPaidSubTotal.TableRowID = r.TableRowID AND tdvPaidSubTotal.DetailFieldID = 170070
	INNER JOIN dbo.TableDetailValues tdvPaidTax WITH (NOLOCK) ON tdvPaidTax.TableRowID = r.TableRowID AND tdvPaidTax.DetailFieldID = 170071
	WHERE m.MatterID = @MatterID AND m.ClientID = @ClientID
	AND tdvPaidToDate.ValueDate > @CancellationDate
	AND tdvPaidStatus.ValueInt = 69898 -- "Paid"
	AND tdvPaidPayRowID.ValueInt > 0 -- AND has generated a payment row
 
	IF (SELECT SUM(RefundTotal) FROM @PremiumRefunds) > 0
	BEGIN
		
		-- Create Premium Refund Row
		SELECT @TotalRefundTotal = SUM(RefundTotal) FROM @PremiumRefunds
		SELECT @TotalRefundSubTotal = SUM(RefundSubTotal) FROM @PremiumRefunds
		SELECT @TotalRefundTax = @TotalRefundTotal - @TotalRefundSubTotal
		
		EXEC dbo.[_C00_1273_Payments_CreatePremiumRefundRow] @CaseID, @TotalRefundTotal, @TotalRefundSubTotal, @TotalRefundTax
		
		--Write General Ledger
		DECLARE @tvpPremiumRows dbo.tvpIntMoney
		INSERT @tvpPremiumRows (ID,Value)
		SELECT PremiumRowID, RefundSubTotal from @PremiumRefunds
		EXEC dbo._C00_1273_Premium_WriteGeneralLedger @tvpPremiumRows, 0 --0=Credit
		
		-- Update premium rows to refunded
		UPDATE tdvPremiumStatus SET detailvalue = 
			CASE 
				WHEN p.PaidTotal = p.RefundTotal THEN 69900 -- Refunded
				WHEN p.PaidTotal > p.RefundTotal THEN 69902 -- Partially refunded
			END
		FROM TableDetailValues tdvPremiumStatus WITH (NOLOCK)
		INNER JOIN @PremiumRefunds p ON p.PremiumRowID = tdvPremiumStatus.TableRowID
		WHERE tdvPremiumStatus.ClientID = 0 and tdvPremiumStatus.DetailFieldID = 170078
		
		-- Update PaidTo on partially refunded rows to the Cancellation Date Zen#27399
		UPDATE tdvPremiumPaidTo SET DetailValue = CONVERT(VARCHAR,@CancellationDate,120)
		FROM TableDetailValues tdvPremiumPaidTo WITH (NOLOCK)
		INNER JOIN @PremiumRefunds p ON p.PremiumRowID = tdvPremiumPaidTo.TableRowID
		WHERE tdvPremiumPaidTo.ClientID = 0 and tdvPremiumPaidTo.DetailFieldID = 170077
		and p.PaidTotal > p.RefundTotal -- Partial refund
			
		
	END	
	
END
 


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_CancellationRefunds] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Policy_CancellationRefunds] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_CancellationRefunds] TO [sp_executeall]
GO
