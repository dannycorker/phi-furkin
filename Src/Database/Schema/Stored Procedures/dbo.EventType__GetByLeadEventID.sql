SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------
-- Date Created: 23 February 2007

-- Created By:  Ben Crinion
-- Purpose: Select the EventType of a specific LeadEvent
-- SB  2014-07-10 Updated to use view which includes shared event types
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[EventType__GetByLeadEventID]
(
	@LeadEventID int
)
AS
	
		DECLARE @ClientID INT
		SELECT @ClientID = ClientID
		FROM dbo.LeadEvent WITH (NOLOCK) 
		WHERE LeadEventID = @LeadEventID

		SELECT et.*
		FROM 
			dbo.[LeadEvent] le (nolock) 
		INNER JOIN 
			dbo.fnEventTypeShared(@ClientID) et on le.EventTypeID = et.EventTypeID 
		WHERE
			le.[LeadEventID] = @LeadEventID 

		Select @@ROWCOUNT



GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetByLeadEventID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetByLeadEventID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetByLeadEventID] TO [sp_executeall]
GO
