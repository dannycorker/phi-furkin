SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*
----------------------------------------------------------------------------------------------------

-- Created By: Aaran Gravestock
-- Date: 07/09/09
-- Purpose: Select only NOTE records from the LeadEvent table for a given Case
----------------------------------------------------------------------------------------------------

-- Change: CT: 091109: Added ORDER BY WhenCreated DESC

*/


CREATE PROCEDURE [dbo].[LeadEvent__GetNotesByCaseID]
(
	@CaseID int   
)
AS


				SELECT TOP 5
					[LeadEventID],
					[ClientID],
					[LeadID],
					[WhenCreated],
					[WhoCreated],
					[Cost],
					[Comments],
					[EventTypeID],
					[NoteTypeID],
					[FollowupDateTime],
					[WhenFollowedUp],
					[AquariumEventType],
					[NextEventID],
					[CaseID],
					[LeadDocumentID],
					[NotePriority],
					[DocumentQueueID],
					[EventDeleted],
					[WhoDeleted],
					[DeletionComments],
					[ContactID],
					[BaseCost],
					[DisbursementCost],
					[DisbursementDescription],
					[ChargeOutRate],
					[UnitsOfEffort],
					[CostEnteredManually],
					[IsOnHold],
					[HoldLeadEventID]
				FROM
					[dbo].[LeadEvent]
				WHERE
					[CaseID] = @CaseID
				AND
					[NoteTypeID] is not null 
				ORDER BY WhenCreated DESC
				SELECT @@ROWCOUNT
					
			







GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent__GetNotesByCaseID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEvent__GetNotesByCaseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent__GetNotesByCaseID] TO [sp_executeall]
GO
