SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientPersonnelOptions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelOptions_Insert]
(

	@ClientPersonnelOptionID int    OUTPUT,

	@ClientPersonnelID int   ,

	@ClientPersonnelOptionTypeID int   ,

	@OptionValue varchar (255)  ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[ClientPersonnelOptions]
					(
					[ClientPersonnelID]
					,[ClientPersonnelOptionTypeID]
					,[OptionValue]
					,[ClientID]
					)
				VALUES
					(
					@ClientPersonnelID
					,@ClientPersonnelOptionTypeID
					,@OptionValue
					,@ClientID
					)
				-- Get the identity value
				SET @ClientPersonnelOptionID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptions_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelOptions_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelOptions_Insert] TO [sp_executeall]
GO
