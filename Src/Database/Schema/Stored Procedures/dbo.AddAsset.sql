SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-01
-- Description:	Add Asset to Asset table
-- =============================================
CREATE PROCEDURE [dbo].[AddAsset] 
	@ClientPersonnelID INT = NULL,
	@AssetID INT = NULL,
	@ClientID INT = NULL,
	@AssetTypeID INT = NULL,
	@AssetSubTypeID INT = NULL,
	@LocationID INT = NULL,
	@AssetName VARCHAR(255) = NULL,
	@Note VARCHAR(255) = NULL,
	@SecureNote VARCHAR(255) = NULL,
	@SecureUserName VARCHAR(255) = NULL,
	@SecurePassword VARCHAR(255) = NULL,
	@SecureOther VARCHAR(255) = NULL,
	@Version VARCHAR(255) = NULL,
	@ValidFrom DATETIME = NULL,
	@ValidTo DATETIME = NULL,
	@Enabled BIT = NULL,
	@Deleted BIT = NULL
	-- @WhoCreated = ClientPersonnelID
	-- @WhenCreated = Current DATETIME
	-- @WhoModified = NULL
	-- @WhenModified = NULL
	
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO [dbo].[Asset] (
		[AssetID], 
		[ClientID], 
		[AssetTypeID], 
		[AssetSubTypeID], 
		[LocationID], 
		[AssetName], 
		[Note], 
		[SecureNote], 
		[SecureUserName], 
		[SecurePassword],
		[SecureOther], 
		[Version], 
		[ValidFrom], 
		[ValidTo], 
		[Enabled], 
		[Deleted],
		[WhoCreated],
		[WhenCreated]
	)
	VALUES (
		@AssetID,
		@ClientID,
		@AssetTypeID,
		@AssetSubTypeID,
		@LocationID,
		@AssetName,
		@Note,
		@SecureNote,
		@SecureUserName,
		@SecurePassword,
		@SecureOther,
		@Version,
		@ValidFrom,
		@ValidTo,
		@Enabled,
		@Deleted,
		@ClientPersonnelID,
		dbo.fn_GetDate_Local()
	)
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AddAsset] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddAsset] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddAsset] TO [sp_executeall]
GO
