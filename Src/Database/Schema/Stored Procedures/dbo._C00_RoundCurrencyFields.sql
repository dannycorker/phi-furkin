SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Set currency fields to be rounded to 2 decimal places
-- JWG 2012-09-03 Only round numbers which have already been cast as ValueMoney by our triggers (eg not credit card numbers)
-- =============================================
CREATE PROCEDURE [dbo].[_C00_RoundCurrencyFields]
(
	@LeadEventID int
)


AS
BEGIN
	SET NOCOUNT ON;

	Declare	@myERROR int,
			@LeadID int,
			@ClientID int,
			@CaseID int

	Select
	@LeadID = LeadEvent.LeadID,
	@ClientID = LeadEvent.ClientID,
	@CaseID = LeadEvent.CaseID
	from leadevent
	where leadevent.leadeventid = @LeadEventID 

	/*
		Convert values to money rather than numeric.
		This still gives a precision of 2 decimal places and it allows SqlServer to cope 
		with values like "£750" that return 1 from isnumeric(detailvalue) but crash when
		you attempt to convert(numeric, detailvalue).
	*/
	/*
	Update MatterdetailValues
	Set DetailValue = case isnumeric(mdv.detailvalue) when 1 then convert(varchar(2000), convert(money, mdv.detailvalue)) else mdv.detailvalue end
	From MatterdetailValues mdv
	Inner Join DetailFields df on df.DetailFieldID = mdv.DetailFieldID and df.QuestionTypeID IN (7,10)
	Inner Join matter on matter.matterid = mdv.matterid and matter.caseid = @CaseID
	WHERE isnumeric(mdv.detailvalue) = 1 

	Update LeadDetailValues
	Set DetailValue = case isnumeric(ldv.detailvalue) when 1 then convert(varchar(2000), convert(money, ldv.detailvalue)) else ldv.detailvalue end
	From LeadDetailValues ldv
	Inner Join DetailFields df on df.DetailFieldID = ldv.DetailFieldID and df.QuestionTypeID IN (7,10)
	Where LeadID = @LeadID
	AND isnumeric(ldv.detailvalue) = 1 
	*/
	UPDATE MatterdetailValues
	SET DetailValue = CAST(mdv.ValueMoney AS VARCHAR(2000)) /* This rounds to 2 decimal places */
	FROM dbo.MatterdetailValues mdv
	INNER JOIN dbo.DetailFields df ON df.DetailFieldID = mdv.DetailFieldID AND df.QuestionTypeID IN (7,10)
	INNER JOIN dbo.Matter m ON m.MatterID = mdv.MatterID AND m.CaseID = @CaseID
	WHERE mdv.ValueMoney IS NOT NULL
	AND mdv.DetailValue > ''

	UPDATE LeadDetailValues
	SET DetailValue = CAST(ldv.ValueMoney AS VARCHAR(2000)) /* This rounds to 2 decimal places */
	FROM dbo.LeadDetailValues ldv
	INNER JOIN dbo.DetailFields df ON df.DetailFieldID = ldv.DetailFieldID AND df.QuestionTypeID IN (7,10)
	WHERE ldv.LeadID = @LeadID
	AND ldv.ValueMoney IS NOT NULL
	AND ldv.DetailValue > ''

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RoundCurrencyFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_RoundCurrencyFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RoundCurrencyFields] TO [sp_executeall]
GO
