SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Alex ELger
-- Create date: 2008-06-20
-- Description:	Deletes Fields MDV or LDV so they can be re-used as mandatory fields
-- Update: ACE - 2010-10-15 - Added inserts into history to show field clear date & nolocked SP
-- =============================================--
CREATE PROCEDURE [dbo].[_C00_DeleteFieldForReUse]
(
@LeadEventID int,
@DetailFieldID int,
@WhoAuthorised int
)
	
AS
BEGIN

	SET NOCOUNT ON

	Declare @LeadID int,
			@ClientID int,
			@CaseID int,
			@CustomerID int,
			@LeadOrMatter int,
			@MaintainHistory bit 

	Select
		@LeadID = le.LeadID,
		@ClientID = le.ClientID,
		@CaseID = le.CaseID,
		@CustomerID=c.CustomerID
	from LeadEvent le WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON le.LeadID=l.LeadID
	INNER JOIN Customers c WITH (NOLOCK) ON l.CustomerID=c.CustomerID
	where le.leadeventid = @LeadEventID 

	Select 
		@LeadOrMatter = LeadOrMatter, 
		@MaintainHistory = MaintainHistory 
	From DetailFields WITH (NOLOCK) 
	Where DetailFieldID = @DetailFieldID

	if @LeadOrMatter = 1
	Begin

		IF @MaintainHistory = 0
		BEGIN

			/* Insert old value and notes into detailvalue history for accountability */
			Insert into detailvaluehistory (ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			Select ClientID, DetailFieldID, 1, LeadID, NULL, DetailValue, dbo.fn_GetDate_Local(), @WhoAuthorised
			From LeadDetailValues WITH (NOLOCK) 
			Where LeadID = @LeadID
			And DetailFieldID = @DetailFieldID

		END
		ELSE 
		BEGIN
		
			/* Insert blank into detailvalue history to show field has been cleared */
			Insert into detailvaluehistory (ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			Select ClientID, DetailFieldID, 1, LeadID, NULL, '', dbo.fn_GetDate_Local(), @WhoAuthorised
			From LeadDetailValues WITH (NOLOCK) 
			Where LeadID = @LeadID
			And DetailFieldID = @DetailFieldID

		END
	
		/* Delete Lead Detail Feild */
		update LeadDetailValues
		set DetailValue = ''
		Where LeadID = @LeadID
		And DetailFieldID = @DetailFieldID

	end

	if @LeadOrMatter = 2
	Begin

		IF @MaintainHistory = 0
		BEGIN

			/* Insert old value and notes into detailvalue history for accountability */
			Insert into detailvaluehistory (ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			Select MatterDetailValues.ClientID, MatterDetailValues.DetailFieldID, 2, MatterDetailValues.LeadID, Matter.MatterID, MatterDetailValues.DetailValue, dbo.fn_GetDate_Local(), @WhoAuthorised
			From MatterDetailValues WITH (NOLOCK) 
			Inner Join Matter WITH (NOLOCK) on Matter.MatterID = MatterDetailValues.MatterID and Matter.CaseID = @CaseID
			Where MatterDetailValues.DetailFieldID = @DetailFieldID

		END
		ELSE
		BEGIN

			/* Insert blank into detailvalue history to show field has been cleared */
			Insert into detailvaluehistory (ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID)
			Select MatterDetailValues.ClientID, MatterDetailValues.DetailFieldID, 2, MatterDetailValues.LeadID, Matter.MatterID, '', dbo.fn_GetDate_Local(), @WhoAuthorised
			From MatterDetailValues WITH (NOLOCK) 
			Inner Join Matter WITH (NOLOCK) on Matter.MatterID = MatterDetailValues.MatterID and Matter.CaseID = @CaseID
			Where MatterDetailValues.DetailFieldID = @DetailFieldID

		END

		/* Delete Matter Detail Feild */
		Update MatterDetailValues
		Set DetailValue = ''
		From MatterDetailValues
		Inner Join Matter on Matter.MatterID = MatterDetailValues.MatterID and Matter.CaseID = @CaseID
		Where MatterDetailValues.DetailFieldID = @DetailFieldID

	END

	-- Customers DFs (added dcm 12/9/12)
	if @LeadOrMatter = 10
	Begin

		IF @MaintainHistory = 0
		BEGIN

			/* Insert old value and notes into detailvalue history for accountability */
			Insert into detailvaluehistory (ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID,CustomerID)
			Select ClientID, DetailFieldID, 10, NULL, NULL, DetailValue, dbo.fn_GetDate_Local(), @WhoAuthorised, CustomerID
			From CustomerDetailValues WITH (NOLOCK) 
			Where DetailFieldID = @DetailFieldID and CustomerID=@CustomerID

		END
		ELSE
		BEGIN

			/* Insert blank into detailvalue history to show field has been cleared */
			Insert into detailvaluehistory (ClientID, DetailFieldID, LeadOrMatter, LeadID, MatterID, FieldValue, WhenSaved, ClientPersonnelID,CustomerID)
			Select ClientID, DetailFieldID, 10, NULL, NULL, '', dbo.fn_GetDate_Local(), @WhoAuthorised, CustomerID
			From CustomerDetailValues WITH (NOLOCK) 
			Where DetailFieldID = @DetailFieldID and CustomerID=@CustomerID

		END

		/* Delete Customer Detail Field */
		Update CustomerDetailValues
		Set DetailValue = ''
		Where DetailFieldID = @DetailFieldID and CustomerID=@CustomerID

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DeleteFieldForReUse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_DeleteFieldForReUse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DeleteFieldForReUse] TO [sp_executeall]
GO
