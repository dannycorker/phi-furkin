SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 15-04-2014
-- Description:	Handles the email read from exchange by PB
-- =============================================
CREATE PROCEDURE [dbo].[_C00_HandleEmail]

	@From VARCHAR(MAX),
	@To VARCHAR(MAX),
	@Cc VARCHAR(MAX),	
	@Subject VARCHAR(MAX),
	@Body VARCHAR(MAX),
	@Attachments VARBINARY(MAX) = NULL

AS
BEGIN

	SET NOCOUNT ON;

	exec _C00_LogIt 'info','PB','HandleEmail','what the',4
	
    
END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_HandleEmail] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_HandleEmail] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_HandleEmail] TO [sp_executeall]
GO
