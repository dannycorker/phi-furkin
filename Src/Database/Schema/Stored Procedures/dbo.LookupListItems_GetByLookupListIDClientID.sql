SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LookupListItems table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupListItems_GetByLookupListIDClientID]
(

	@LookupListID int   ,

	@ClientID int   
)
AS


				SELECT
					[LookupListItemID],
					[LookupListID],
					[ItemValue],
					[ClientID],
					[Enabled],
					[SortOrder],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID],
					[IsShared]
				FROM
					dbo.fnLookupListItemsShared(@ClientID)
				WHERE
										[LookupListID] = @LookupListID
AND [ClientID] IN (0, @ClientID)
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_GetByLookupListIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupListItems_GetByLookupListIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_GetByLookupListIDClientID] TO [sp_executeall]
GO
