SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EventTypeFieldCompletion table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeFieldCompletion_Find]
(

	@SearchUsingOR bit   = null ,

	@EventTypeFieldCompletionID int   = null ,

	@ClientID int   = null ,

	@EventTypeID int   = null ,

	@DetailFieldID int   = null ,

	@InsertOptionID int   = null ,

	@DeleteOptionID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EventTypeFieldCompletionID]
	, [ClientID]
	, [EventTypeID]
	, [DetailFieldID]
	, [InsertOptionID]
	, [DeleteOptionID]
    FROM
	[dbo].[EventTypeFieldCompletion] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeFieldCompletionID] = @EventTypeFieldCompletionID OR @EventTypeFieldCompletionID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([InsertOptionID] = @InsertOptionID OR @InsertOptionID IS NULL)
	AND ([DeleteOptionID] = @DeleteOptionID OR @DeleteOptionID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EventTypeFieldCompletionID]
	, [ClientID]
	, [EventTypeID]
	, [DetailFieldID]
	, [InsertOptionID]
	, [DeleteOptionID]
    FROM
	[dbo].[EventTypeFieldCompletion] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeFieldCompletionID] = @EventTypeFieldCompletionID AND @EventTypeFieldCompletionID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([InsertOptionID] = @InsertOptionID AND @InsertOptionID is not null)
	OR ([DeleteOptionID] = @DeleteOptionID AND @DeleteOptionID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletion_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeFieldCompletion_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeFieldCompletion_Find] TO [sp_executeall]
GO
