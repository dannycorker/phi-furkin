SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richaredson
-- Create date: 09-11-2015
-- Description:	Inserts a SocialFeed record
-- =============================================
CREATE PROCEDURE [dbo].[SocialFeed__Insert]

	@ClientID INT, 
	@SocialTypeID INT,
	@Token VARCHAR(1000), 
	@Secret VARCHAR(1000), 
	@ScreenName VARCHAR(250), 
	@SocialUserID VARCHAR(50), 
	@WhenCreated DATETIME, 
	@WhoCreated INT, 
	@WhenModified DATETIME, 
	@WhoModified INT
	
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO SocialFeed (ClientID, SocialTypeID, Token, [Secret], ScreenName, SocialUserID, WhenCreated, WhoCreated, WhenModified, WhoModified)
	VALUES (@ClientID, @SocialTypeID, @Token, @Secret, @ScreenName, @SocialUserID, @WhenCreated, @WhoCreated, @WhenModified, @WhoModified)
	
	DECLARE @SocialFeedID INT
	SELECT @SocialFeedID = SCOPE_IDENTITY()
	
	SELECT @SocialFeedID SocialFeedID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SocialFeed__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__Insert] TO [sp_executeall]
GO
