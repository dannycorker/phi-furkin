SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PageControlText table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageControlText_Delete]
(

	@PageControlTextID int   
)
AS


				DELETE FROM [dbo].[PageControlText] WITH (ROWLOCK) 
				WHERE
					[PageControlTextID] = @PageControlTextID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageControlText_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageControlText_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageControlText_Delete] TO [sp_executeall]
GO
