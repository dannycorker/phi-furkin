SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetNewLeads]
(
@clientID int
)
AS
SELECT     Customers.CustomerID, Customers.Fullname, Customers.EmailAddress, Customers.DayTimeTelephoneNumber, Customers.Town, Customers.County, 
                   CustomerQuestionnaires.CustomerQuestionnaireID, CustomerQuestionnaires.ClientQuestionnaireID, ClientQuestionnaires.QuestionnaireTitle, 
                   Customers.AquariumStatusID, Customers.ClientID
FROM         Customers INNER JOIN
                   CustomerQuestionnaires ON Customers.CustomerID = CustomerQuestionnaires.CustomerID INNER JOIN
                   ClientQuestionnaires ON CustomerQuestionnaires.ClientQuestionnaireID = ClientQuestionnaires.ClientQuestionnaireID
WHERE     (Customers.AquariumStatusID = 1) AND (Customers.ClientID = @clientID)
ORDER BY Customers.Fullname



GO
GRANT VIEW DEFINITION ON  [dbo].[GetNewLeads] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetNewLeads] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetNewLeads] TO [sp_executeall]
GO
