SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ScriptSectionNavigationCriteria table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptSectionNavigationCriteria_Delete]
(

	@ScriptSectionNavigationCriteriaID int   
)
AS


				DELETE FROM [dbo].[ScriptSectionNavigationCriteria] WITH (ROWLOCK) 
				WHERE
					[ScriptSectionNavigationCriteriaID] = @ScriptSectionNavigationCriteriaID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSectionNavigationCriteria_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria_Delete] TO [sp_executeall]
GO
