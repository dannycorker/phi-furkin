SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-08-12
-- Description:	Refreshes collections list of active policies and pet name (for documentation)
-- Mods
--  2015-10-19 DCM Replace code with fn_C00_ReplaceLastCommaWithAnd function
--	2015-12-14 DCM Added total current payment amount - first & recurring
--	2016-11-07 DCM Updated to get regular monthly payment total for account from4 new nilling system
--	2017-05-24 CPS added DISTINCT to @PolicyMatters insert
--	2018-12-11 UAH added Subquery filter on line 78 to cater for outstanding balance being stamped correctly upon mandate failure
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Collections_RefreshDocumentationFields] 
(
	@MatterID INT
)

AS
BEGIN

	SET NOCOUNT ON;

--declare	
--	@MatterID INT = 50025752
	
	DECLARE 
			@AqAutomation INT,
			@DetailValue VARCHAR(1000),
			@PolicyMatters tvpINT		,
			@OutstandingBal DECIMAL (18,2) 	 
	
	INSERT INTO @PolicyMatters
	SELECT DISTINCT pstat.MatterID
	FROM LeadTypeRelationship ltr WITH (NOLOCK) 
	INNER JOIN MatterDetailValues pstat WITH (NOLOCK) ON ltr.FromMatterID=pstat.MatterID 
		AND pstat.DetailFieldID=170038 AND pstat.ValueInt=43002 -- live policies only
	WHERE ltr.ToMatterID=@MatterID AND ltr.FromLeadTypeID=1492
	
	-- policy number
	SELECT @DetailValue = COALESCE(@DetailValue + ', ', '') + mdv.DetailValue,
			 @AqAutomation=CASE WHEN @AqAutomation IS NULL THEN dbo.fnGetKeyValueAsIntFromThirdPartyIDs (mdv.ClientID,53,'CFG|AqAutomationCPID',0) ELSE @AqAutomation END
	FROM MatterDetailValues mdv WITH (NOLOCK) 
	INNER JOIN @PolicyMatters pm ON pm.AnyID=mdv.MatterID
	WHERE mdv.DetailFieldID=170050 --Policy Number
	
	IF @DetailValue = '' OR @DetailValue IS NULL
	BEGIN
	EXEC dbo._C00_SimpleValueIntoField 175606, @DetailValue, @MatterID, @AqAutomation
	END
	-- pet name
	SELECT @DetailValue = NULL
	SELECT @DetailValue = COALESCE(@DetailValue + ', ', '') + ldv.DetailValue 
	FROM LeadDetailValues ldv WITH (NOLOCK)
	INNER JOIN Matter m WITH (NOLOCK) ON ldv.LeadID=m.LeadID 
	INNER JOIN @PolicyMatters pm ON pm.AnyID=m.MatterID
	WHERE ldv.DetailFieldID=144268 -- Pet Name
	
	SELECT @DetailValue = dbo.fn_C00_ReplaceLastCommaWithAnd(@DetailValue)
		
	EXEC dbo._C00_SimpleValueIntoField 175604, @DetailValue, @MatterID, @AqAutomation
	
	SELECT @OutstandingBal =  SUM(p.PaymentGross)  FROM Lead l WITH (NOLOCK)
	INNER JOIN Cases cs WITH (NOLOCK) ON cs.LeadID = l.LeadID 
	INNER JOIN Matter m WITH ( NOLOCK ) ON m.CaseID = cs.CaseID
	INNER JOIN LeadTypeRelationship ltr WITH ( NOLOCK ) on ltr.ToMatterID = m.MatterID 
	INNER JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = ltr.FromMatterID and mdv.DetailFieldID = 177074
	INNER JOIN PurchasedProductPaymentSchedule p WITH ( NOLOCK ) on p.PurchasedProductID = mdv.ValueInt
	INNER JOIN PurchasedProductPaymentScheduleType pt WITH ( NOLOCK )  on pt.PurchasedProductPaymentScheduleTypeID = p.PurchasedProductPaymentScheduleTypeID 
	AND NOT EXISTS (SELECT * FROM PurchasedProductPaymentSchedule pp WITH ( NOLOCK ) where pp.PurchasedProductPaymentScheduleParentID = p.PurchasedProductPaymentScheduleID
					AND pp.PurchasedProductPaymentScheduleID <> p.PurchasedProductPaymentScheduleID) 
	Where  p.PaymentStatusID NOT IN (1,6,3,8,7,2)
	AND m.MatterID = @MatterID
	AND p.PaymentGross > 0.00
	
	EXEC _C00_SimpleValueIntoField  177324,@OutstandingBal,@MatterID, @AqAutomation
		
	-- current monthly - recurring month (excluding initial or trailing odd month)
	-- sum of max value of new or processed scheduled payments for purchased products using this account
	SELECT @DetailValue = CAST(SUM(ISNULL(s.PaymentItem,0)) AS VARCHAR) 
	FROM 
		(SELECT ppps.PurchasedProductID, MAX(ppps.PaymentGross) [PaymentItem] 
		FROM MatterDetailValues accid WITH (NOLOCK) 
		INNER JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) ON ppps.AccountID = accid.ValueInt
		WHERE accid.MatterID=@MatterID 
			AND accid.DetailFieldID=176973
			AND ppps.PaymentStatusID IN (1,2) -- new, processed
			AND ppps.PurchasedProductPaymentScheduleTypeID IN (1) -- scheduled
		GROUP BY ppps.PurchasedProductID)
		as s

	EXEC dbo._C00_SimpleValueIntoField 176174, @DetailValue, @MatterID, @AqAutomation /*Current monthly repayment - other (all policies)*/

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_RefreshDocumentationFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Collections_RefreshDocumentationFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_RefreshDocumentationFields] TO [sp_executeall]
GO
