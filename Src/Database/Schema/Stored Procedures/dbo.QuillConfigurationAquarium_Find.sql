SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the QuillConfigurationAquarium table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillConfigurationAquarium_Find]
(

	@SearchUsingOR bit   = null ,

	@QuillConfigurationAquariumID int   = null ,

	@Vendor varchar (50)  = null ,

	@APIKey uniqueidentifier   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [QuillConfigurationAquariumID]
	, [Vendor]
	, [APIKey]
    FROM
	[dbo].[QuillConfigurationAquarium] WITH (NOLOCK) 
    WHERE 
	 ([QuillConfigurationAquariumID] = @QuillConfigurationAquariumID OR @QuillConfigurationAquariumID IS NULL)
	AND ([Vendor] = @Vendor OR @Vendor IS NULL)
	AND ([APIKey] = @APIKey OR @APIKey IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [QuillConfigurationAquariumID]
	, [Vendor]
	, [APIKey]
    FROM
	[dbo].[QuillConfigurationAquarium] WITH (NOLOCK) 
    WHERE 
	 ([QuillConfigurationAquariumID] = @QuillConfigurationAquariumID AND @QuillConfigurationAquariumID is not null)
	OR ([Vendor] = @Vendor AND @Vendor is not null)
	OR ([APIKey] = @APIKey AND @APIKey is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationAquarium_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillConfigurationAquarium_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillConfigurationAquarium_Find] TO [sp_executeall]
GO
