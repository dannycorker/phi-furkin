SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-05-30
-- Description:	Returns all operators for the parameter options
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_Operators_GetAll]

AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT * 
	FROM dbo.RulesEngine_Operators WITH (NOLOCK) 
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Operators_GetAll] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Operators_GetAll] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Operators_GetAll] TO [sp_executeall]
GO
