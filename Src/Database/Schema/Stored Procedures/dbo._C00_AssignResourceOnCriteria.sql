SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 21-OCT-2009
-- Description:	Proc to associate resource by critera
--              JWG 2010-01-25 Bugfix NULL LeadID
-- =============================================
CREATE PROCEDURE [dbo].[_C00_AssignResourceOnCriteria]
(
	@CaseID int,
	@CriteriaDetailFieldID int,
	@DetailFieldID int,
	@RlDetailField int,
	@SuppressSpaces bit = 0,
	@FirstXChars int = 2000,
	@ExactMatch bit = 0
)

AS
BEGIN
	SET NOCOUNT ON;

	Declare @LeadOrMatter int,
			@LeadID int,
			@RLID int,
			@MatterID int,
			@TestID int,
			@DetailValue varchar(2000)

	Select @LeadOrMatter = LeadOrMatter
	From dbo.DetailFields (nolock)
	Where DetailFieldID = @DetailFieldID

	/* Get LeadID to pass to dbo._C00_CreateDetailFields */
	SELECT @LeadID = ca.LeadID 
	FROM dbo.Cases ca (nolock) 
	WHERE ca.CaseID = @CaseID

	Select @DetailValue = dbo.fnGetDv(@CriteriaDetailFieldID,@CaseID)

	If @DetailValue IS NOT NULL and @DetailValue <> ''
	BEGIN

		exec dbo._C00_CreateDetailFields @DetailFieldID, @LeadID

		If @LeadOrMatter = 1 
		BEGIN
		
			If @ExactMatch = 1
			BEGIN
			
				Select top 1 @RLID = ResourceListDetailValues.ResourceListID	
				From ResourceListDetailValues
				Where DetailFieldID = @RlDetailField
				and DetailValue = left(CASE @SuppressSpaces WHEN 1 THEN replace(@DetailValue, ' ', '') else @DetailValue END, @FirstXChars)
				Order By ResourceListDetailValueID

			END

			If @ExactMatch <> 1
			BEGIN
			
				Select top 1 @RLID = ResourceListDetailValues.ResourceListID	
				From ResourceListDetailValues
				Where DetailFieldID = @RlDetailField
				and DetailValue LIKE '%' + left(CASE @SuppressSpaces WHEN 1 THEN replace(@DetailValue, ' ', '') else @DetailValue END, @FirstXChars) + '%'
				Order By ResourceListDetailValueID

			END

			If @RLID IS NOT NULL
			BEGIN

				Select @LeadID = LeadID
				From Cases
				Where CaseID = @CaseID

				Update LeadDetailValues
				Set DetailValue = @RLID
				Where LeadID = @LeadID
				and DetailFieldID = @DetailFieldID
					
			END

		END


		If @LeadOrMatter = 2 
		BEGIN
		
			Select @MatterID = MatterID, @TestID = 0
			From Matter
			Where CaseID = @CaseID
			Order By MatterID
			
			While @MatterID > @TestID
			BEGIN
			
				Select @DetailValue = mdv.DetailValue 
				From MatterDetailValues mdv with (nolock) 
				Where mdv.MatterID = @MatterID
				and mdv.DetailFieldID = @CriteriaDetailFieldID

				If @ExactMatch = 1
				BEGIN
				
					Select top 1 @RLID = ResourceListDetailValues.ResourceListID	
					From ResourceListDetailValues
					Where DetailFieldID = @RlDetailField
					and DetailValue = left(CASE @SuppressSpaces WHEN 1 THEN replace(@DetailValue, ' ', '') else @DetailValue END, @FirstXChars)
					Order By ResourceListDetailValueID

				END

				If @ExactMatch <> 1
				BEGIN
				
					Select top 1 @RLID = ResourceListDetailValues.ResourceListID	
					From ResourceListDetailValues
					Where DetailFieldID = @RlDetailField
					and DetailValue LIKE '%' + left(CASE @SuppressSpaces WHEN 1 THEN replace(@DetailValue, ' ', '') else @DetailValue END, @FirstXChars) + '%'
					Order By ResourceListDetailValueID

				END

				If @RLID IS NOT NULL
				BEGIN

					Update MatterDetailValues
					Set DetailValue = @RLID
					From MatterDetailValues
					Where DetailFieldID = @DetailFieldID
					And MatterDetailValues.MatterID = @MatterID

				END

				Select @TestID = @MatterID 

				Select @MatterID = MatterID
				From Matter
				Where CaseID = @CaseID
				And MatterID > @MatterID
				Order By MatterID

			END
				
		END

	END

	RETURN @RLID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AssignResourceOnCriteria] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_AssignResourceOnCriteria] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AssignResourceOnCriteria] TO [sp_executeall]
GO
