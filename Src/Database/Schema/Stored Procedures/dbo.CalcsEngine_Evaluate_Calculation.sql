SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-08-14
-- Description:	Evaluates a calculation based on a set of inputs
-- =============================================
CREATE PROCEDURE [dbo].[CalcsEngine_Evaluate_Calculation]
(
	@ID INT,
	@CustomerID INT = NULL,
	@LeadID INT = NULL,
	@CaseID INT = NULL,
	@MatterID INT = NULL,
	@Debug BIT = 0
)
AS
BEGIN
	
	SET NOCOUNT ON;

	-- These are the parameters we are going to use in this calculation
	DECLARE @Parameters TABLE
	(
		ID INT IDENTITY,
		ParameterID INT,
		Name VARCHAR(150),
		ParamTypeID INT,
		ParamType VARCHAR(50),
		Param VARCHAR(150),
		Value VARCHAR(2000),
		ValueXml XML
	)
	INSERT @Parameters (ParameterID, Name, ParamTypeID, ParamType, Param)
	SELECT p.CalculationParameterID, p.Name, t.ParameterTypeID, t.Name, p.Value
	FROM dbo.CalcsEngine_CalculationParameters p WITH (NOLOCK) 
	INNER JOIN dbo.CalcsEngine_ParameterTypes t WITH (NOLOCK) ON p.ParameterTypeID = t.ParameterTypeID
	WHERE p.CalculationID = @ID

	-- Loop through calling the procs to get the xml as an output
	DECLARE @LoopCount INT = 0,
			@LoopIndex INT = 0,
			@Xpath VARCHAR(1000),
			@Xml XML,
			@OutputXml XML,
			@OutputVal VARCHAR(2000),
			@ProcXmlTemplate NVARCHAR(2000) = 'EXEC dbo.{0} @CustomerID, @LeadID, @CaseID, @MatterID, @OutputXml OUTPUT',
			@ProcXml NVARCHAR(2000) = '',
			@ProcXmlParams AS NVARCHAR(2000) = '@CustomerID INT,
												@LeadID INT,
												@CaseID INT,
												@MatterID INT,
												@OutputXml XML OUTPUT'
			
	SELECT @LoopCount = COUNT(*) 
	FROM @Parameters

	WHILE @LoopIndex < @LoopCount
	BEGIN

		SELECT @LoopIndex += 1

		DECLARE @ParamTypeID INT = NULL,
				@Param VARCHAR(150) = NULL
		
		SELECT @ParamTypeID = ParamTypeID, @Param = Param
		FROM @Parameters
		WHERE ID = @LoopIndex
		
		-- We have an XML 'table' type so exec the proc with the standard signature to get the data we need
		IF @ParamTypeID = 1 -- PROC_XML
		BEGIN
		
			SELECT @ProcXml = REPLACE(@ProcXmlTemplate, '{0}', @Param)
			EXEC sp_executesql @ProcXml, @ProcXmlParams, @CustomerID, @LeadID, @CaseID, @MatterID, @OutputXml OUTPUT
			
			UPDATE @Parameters
			SET ValueXml = @OutputXml
			WHERE ID = @LoopIndex
		
		END

	END		

	-- Clear the loop params
	SELECT	@LoopCount = 0,
			@LoopIndex = 0
			

	-- These are the actions we are going to apply in order
	DECLARE @Actions TABLE
	(
		ID INT IDENTITY,
		ActionID INT,
		ParentID INT,
		Name VARCHAR(150),
		ActionTypeID INT,
		ActionType VARCHAR(50),
		Context VARCHAR(150),
		Parameter VARCHAR(150),
		Value VARCHAR(2000),
		ValueXml XML
	)

	/*
	TYPES - LOOP, VARIABLE_SCALAR, VARIABLE_PARAMETER, VARIABLE_XML, CONDITION, TRUE, FALSE, JUMP
	*/

	INSERT @Actions (ActionID, ParentID, Name, ActionTypeID, ActionType, Context, Parameter)
	SELECT CalculationActionID, ParentID, a.Name, t.ActionTypeID, t.Name, Context, Parameter
	FROM dbo.CalcsEngine_CalculationActions a WITH (NOLOCK) 
	INNER JOIN dbo.CalcsEngine_ActionTypes t WITH (NOLOCK) ON a.ActionTypeID = t.ActionTypeID
	WHERE a.CalculationID = @ID

	SELECT @LoopCount = COUNT(*) 
	FROM @Actions

	-- We use this table to keep track of nested loops
	DECLARE @LoopNestings TABLE
	(
		ID INT IDENTITY,
		ActionID INT,
		LoopStart INT,
		LoopEnd INT,
		LoopCount INT,
		LoopIndex INT
	)

	-- Loop through actions
	WHILE @LoopIndex < @LoopCount
	BEGIN

		SELECT @LoopIndex += 1
		
		--SELECT @LoopIndex
		
		-- Get all the action details
		DECLARE @ParentID INT = NULL,
				@ActionTypeID INT = NULL,
				@Context VARCHAR(150) = NULL,
				@Parameter VARCHAR(150) = NULL
		
		SELECT @ParentID = ParentID, @ActionTypeID = ActionTypeID, @Context = Context, @Parameter = Parameter
		FROM @Actions
		WHERE ID = @LoopIndex
		
		-- And the data we need for loops
		DECLARE @NestedLoopStart INT = NULL,
				@NestedLoopEnd INT = NULL,
				@NestedLoopCount INT = NULL,
				@NestedLoopIndex INT = NULL
		
		SELECT	@NestedLoopStart = LoopStart,
				@NestedLoopEnd = LoopEnd,
				@NestedLoopCount = LoopCount,
				@NestedLoopIndex = LoopIndex
		FROM @LoopNestings
		WHERE ActionID = @ParentID
		
		-- Update the parameter with previous calcs
		SELECT @Parameter = REPLACE(@Parameter, '{' + CAST(ID AS VARCHAR) + '}', ISNULL(Value, '{' + CAST(ID AS VARCHAR) + '}'))
		FROM @Actions
		
		-- Get the appropriate context
		IF @Context = '.'
		BEGIN
			
			SELECT @Xpath = '//Row[' + CAST(@NestedLoopIndex AS VARCHAR) + ']'
			SELECT @Xml = p.ValueXml
			FROM @Parameters p
			INNER JOIN @Actions parent ON p.ID = parent.Context
			INNER JOIN @Actions child ON parent.ID = child.ParentID
			WHERE child.ID = @LoopIndex
			
			EXEC dbo.XML_SelectNodeSet @Xml, @Xpath, @OutputXml OUTPUT	
			SELECT @Xml = @OutputXml
			
		END
		ELSE IF @Context IS NOT NULL
		BEGIN
			
			SELECT @Xml = ValueXml
			FROM @Parameters
			WHERE ParameterID = @Context
			
		END
		
		
		
		-- We are about to loop!
		IF @ActionTypeID = 1 -- LOOP
		BEGIN
		
			DECLARE @InnerCount INT = 0
			
			DECLARE @InnerXml XML
			SELECT @InnerXml = ValueXml
			FROM @Parameters
			WHERE ParameterID = @Context

			SELECT @Xpath = '//Row'		

			-- Check how many iterations we will have
			EXEC @InnerCount = dbo.XML_NodeCount @InnerXml, @Xpath
			
			-- Save this data into the nesting table so we can keep track
			INSERT @LoopNestings (ActionID, LoopStart, LoopEnd, LoopCount, LoopIndex)
			SELECT @LoopIndex, @LoopIndex + 1, MAX(ID), @InnerCount, 1 -- We are starting at 1 here as we are incrementing the counter at the end rather than the start
			FROM @Actions
			WHERE ParentID = @LoopIndex
		
		END
		ELSE IF @ActionTypeID = 2 --CONDITION
		BEGIN
			-- Evaluate the statement
			SELECT @Xpath = @Parameter
			EXEC dbo.XML_SelectSingleNode @Xml, @Xpath, @OutputVal OUTPUT
			
			UPDATE @Actions
			SET Value = @OutputVal
			WHERE ID = @LoopIndex
		
		END
		ELSE IF @ActionTypeID IN (3, 4) -- TRUE or FALSE
		BEGIN
			
			DECLARE @Condition VARCHAR(2000)
			SELECT @Condition = p.Value
			FROM @Actions p
			INNER JOIN @Actions c ON c.Context = p.ID
			WHERE c.ID = @LoopIndex
			
			IF (@Condition = 'true' AND @ActionTypeID = 3) OR (@Condition = 'false' AND @ActionTypeID = 4)
			BEGIN
				
				SELECT @LoopIndex = CAST(@Parameter AS INT) - 1
							
			END

		END
		ELSE IF @ActionTypeID = 7 -- JUMP
		BEGIN
		
			SELECT @LoopIndex = CAST(@Parameter AS INT) - 1
		
		END
		ELSE IF @ActionTypeID = 5 -- VARIABLE_SCALAR
		BEGIN
			
			SELECT @Xpath = @Parameter
			EXEC dbo.XML_SelectSingleNode @Xml, @Xpath, @OutputVal OUTPUT
			
			UPDATE @Actions
			SET Value = @OutputVal
			WHERE ID = @LoopIndex
		
		END
		ELSE IF @ActionTypeID = 6 -- UPDATE
		BEGIN
		
			EXEC dbo.XML_Modify @Xml OUTPUT, @Parameter
		
			-- Save the context back again
			IF @Context = '.'
			BEGIN
						
				DECLARE @ContextXml XML = NULL	
					
				SELECT @ContextXml = p.ValueXml
				FROM @Parameters p
				INNER JOIN @Actions parent ON p.ID = parent.Context
				INNER JOIN @Actions child ON parent.ID = child.ParentID
				WHERE child.ID = @LoopIndex

				SELECT @Xpath = 'insert sql:variable("@XmlParam") after (//Row[' + CAST(@NestedLoopIndex AS VARCHAR) + '])[1]'
				EXEC dbo.XML_Modify @ContextXml OUTPUT, @Xpath, @Xml

				SELECT @Xpath = 'delete //Row[' + CAST(@NestedLoopIndex AS VARCHAR) + ']'
				EXEC dbo.XML_Modify @ContextXml OUTPUT, @Xpath
				
				UPDATE p		
				SET p.ValueXml = @ContextXml
				FROM @Parameters p
				INNER JOIN @Actions parent ON p.ID =	CASE parent.Context
															WHEN '.' THEN '0'
															ELSE parent.Context
														END
				INNER JOIN @Actions child ON parent.ID = child.ParentID
				WHERE child.ID = @LoopIndex
				
				
			END
			ELSE IF @Context IS NOT NULL
			BEGIN
				
				UPDATE @Parameters
				SET ValueXml = @Xml
				WHERE ParameterID = @Context
				
			END
			
		END

		
		-- If we are at the end of a loop cycle (or past if we are jumping)
		IF @LoopIndex >= @NestedLoopEnd
		BEGIN
			
			-- Increment the counter
			UPDATE @LoopNestings
			SET LoopIndex += 1
			WHERE ActionID = @ParentID
			
			-- If we are not yet at the end of the inner loop then start again
			IF @NestedLoopIndex < @NestedLoopCount
			BEGIN
				SELECT @LoopIndex = @NestedLoopStart - 1
			END
		END

	END		

	SELECT	@LoopCount = 0,
			@LoopIndex = 0

	SELECT *
	FROM @Actions

	SELECT * 
	FROM @Parameters
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_Evaluate_Calculation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CalcsEngine_Evaluate_Calculation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_Evaluate_Calculation] TO [sp_executeall]
GO
