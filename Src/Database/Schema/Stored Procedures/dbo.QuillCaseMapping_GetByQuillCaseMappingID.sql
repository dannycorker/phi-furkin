SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuillCaseMapping table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillCaseMapping_GetByQuillCaseMappingID]
(

	@QuillCaseMappingID int   
)
AS


				SELECT
					[QuillCaseMappingID],
					[ClientID],
					[CaseID],
					[QuillCaseCode]
				FROM
					[dbo].[QuillCaseMapping] WITH (NOLOCK) 
				WHERE
										[QuillCaseMappingID] = @QuillCaseMappingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCaseMapping_GetByQuillCaseMappingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillCaseMapping_GetByQuillCaseMappingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCaseMapping_GetByQuillCaseMappingID] TO [sp_executeall]
GO
