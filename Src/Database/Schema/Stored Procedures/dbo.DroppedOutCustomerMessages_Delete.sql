SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DroppedOutCustomerMessages table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomerMessages_Delete]
(

	@DroppedOutCustomerMessageID int   
)
AS


				DELETE FROM [dbo].[DroppedOutCustomerMessages] WITH (ROWLOCK) 
				WHERE
					[DroppedOutCustomerMessageID] = @DroppedOutCustomerMessageID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerMessages_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomerMessages_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerMessages_Delete] TO [sp_executeall]
GO
