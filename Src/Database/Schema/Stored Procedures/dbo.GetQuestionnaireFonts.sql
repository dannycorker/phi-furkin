SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/****** Object:  Stored Procedure dbo.GetQuestionnaireFonts    Script Date: 08/09/2006 12:22:37 ******/
CREATE PROCEDURE [dbo].[GetQuestionnaireFonts] @ClientQuestionnaireID int

 AS

Select QuestionnaireFontID, ClientQuestionnaireID, PartNameID, FontFamily, FontSize, FontColour, FontWeight, FontAlignment, ClientID 
From QuestionnaireFonts
Where ClientQuestionnaireID = @ClientQuestionnaireID



GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionnaireFonts] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetQuestionnaireFonts] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetQuestionnaireFonts] TO [sp_executeall]
GO
