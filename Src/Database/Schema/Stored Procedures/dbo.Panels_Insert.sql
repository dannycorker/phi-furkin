SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Panels table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Panels_Insert]
(

	@PanelID int    OUTPUT,

	@ClientID int   ,

	@ClientPersonnelID int   ,

	@PanelName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[Panels]
					(
					[ClientID]
					,[ClientPersonnelID]
					,[PanelName]
					)
				VALUES
					(
					@ClientID
					,@ClientPersonnelID
					,@PanelName
					)
				-- Get the identity value
				SET @PanelID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Panels_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Panels_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Panels_Insert] TO [sp_executeall]
GO
