SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Alex Elger
-- Create date: 2008-06-15
-- Description:	Add payments to accounts when not rejected
-- 12-07-2012 JWG  Use LeadDocument again now that it knows about DocumentBlobSize.
-- =============================================
CREATE PROCEDURE [dbo].[_C38_MonthlyReconciliation]
	
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @myERROR INT

	DECLARE @LeadsAndMatters TABLE 
	(
		LeadID INT
	);

	DECLARE @TableRows TABLE (
		TableRowID INT
	)

	BEGIN TRAN

	DECLARE @LastMonthCutoffStart DATETIME

	/* 
	Start by setting out the dates we are interested in 
	We want from the 1st to the last day of the previous month, 
	eg if today is June 26th 2008 then we want May 1st to May 31st 2008
	*/
	SELECT @LastMonthCutoffStart = DATEADD(mm, -1, dbo.fn_GetDate_Local())

	SELECT @LastMonthCutoffStart = w.[Date]
	FROM WorkingDays w 
	WHERE w.[Day] = 1 
	AND w.[Month] = DATEPART(mm, @LastMonthCutoffStart)
	AND w.[Year] = DATEPART(yy, @LastMonthCutoffStart)

	/* Insert Resource Table row */	 
	INSERT INTO @LeadsAndMatters(LeadID)
	SELECT DISTINCT LeadID
	FROM Lead
	WHERE Lead.ClientID = 38 

	INSERT INTO TableRows(ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID) 
	OUTPUT inserted.TableRowID INTO @TableRows
	SELECT 38, lam.LeadID, NULL, 12515, 1598
	FROM @LeadsAndMatters lam  
	WHERE NOT EXISTS(
		SELECT *
		FROM TableDetailValues
		WHERE DetailFieldID = 26060
		AND DetailValue = CONVERT(VARCHAR(11) , @LastMonthCutoffStart, 120)
		AND TableDetailValues.LeadID = lam.LeadID
)

	INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID)
	SELECT tr_real.TableRowID, NULL, 26060, CONVERT(VARCHAR(11) , @LastMonthCutoffStart, 120), tr_real.LeadID, 0, 38
	FROM @TableRows tr 
	INNER JOIN TableRows tr_real ON tr_real.TableRowID = tr.TableRowID

	/* Number of SMS's Sent */
	INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID)
	SELECT tr_real.TableRowID, NULL, 26061, SUM(FLOOR(ld.emailblobSize/160)+1), tr_real.LeadID, 0, 38
	FROM @TableRows tr 
	INNER JOIN TableRows tr_real ON tr_real.TableRowID = tr.TableRowID
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = tr_Real.LeadID AND ldv.DetailFieldID = 6453
	INNER JOIN Clients  ON Clients.ClientID = ldv.DetailValue
	INNER JOIN LeadEvent  ON LeadEvent.ClientID = Clients.ClientID AND DATEDIFF(mm, LeadEvent.WhenCreated, dbo.fn_GetDate_Local()) = 1 
	INNER JOIN EventType  ON LeadEvent.EventTypeID = EventType.EventTypeID AND EventType.EventSubTypeID = 13
	INNER JOIN dbo.LeadDocument ld ON ld.LeadDocumentID = LeadEvent.LeadDocumentID
	GROUP BY tr_real.TableRowID, Clients.ClientID, tr_real.LeadID

	SELECT @myERROR = @@ERROR
	IF @myERROR != 0 GOTO HANDLE_ERROR

	/* Add no if over limit */
	INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID)
	SELECT tr_real.TableRowID, NULL, 27348, 3273, tr_real.LeadID, 0, 38
	FROM @TableRows tr 
	INNER JOIN TableRows tr_real ON tr_real.TableRowID = tr.TableRowID
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = tr_Real.LeadID AND ldv.DetailFieldID = 6453
	INNER JOIN LeadDetailValues ldv2 ON ldv2.LeadID = tr_Real.LeadID AND ldv2.DetailFieldID = 12509
	INNER JOIN resourcelistdetailvalues rldv ON rldv.resourcelistid = ldv2.detailvalue AND rldv.detailfieldid = 27346
	INNER JOIN tabledetailvalues tdv ON tdv.tablerowid = tr_real.tablerowid AND tdv.detailfieldid = 26061
	INNER JOIN Clients  ON Clients.ClientID = ldv.DetailValue
	WHERE CONVERT(NUMERIC,tdv.detailvalue) > CONVERT(NUMERIC,rldv.detailvalue)

	SELECT @myERROR = @@ERROR
	IF @myERROR != 0 GOTO HANDLE_ERROR

	/* Add yes if within limit */
	INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID)
	SELECT tr_real.TableRowID, NULL, 27348, 3272, tr_real.LeadID, 0, 38
	FROM @TableRows tr 
	INNER JOIN TableRows tr_real ON tr_real.TableRowID = tr.TableRowID
	INNER JOIN LeadDetailValues ldv ON ldv.LeadID = tr_Real.LeadID AND ldv.DetailFieldID = 6453
	INNER JOIN LeadDetailValues ldv2 ON ldv2.LeadID = tr_Real.LeadID AND ldv2.DetailFieldID = 12509
	INNER JOIN resourcelistdetailvalues rldv ON rldv.resourcelistid = ldv2.detailvalue AND rldv.detailfieldid = 27346
	INNER JOIN tabledetailvalues tdv ON tdv.tablerowid = tr_real.tablerowid AND tdv.detailfieldid = 26061
	INNER JOIN Clients  ON Clients.ClientID = ldv.DetailValue
	WHERE CONVERT(NUMERIC,tdv.detailvalue) < CONVERT(NUMERIC,rldv.detailvalue)

	SELECT @myERROR = @@ERROR
	IF @myERROR != 0 GOTO HANDLE_ERROR

	UPDATE leaddetailvalues
	SET detailvalue = SMSCount.Counter
	FROM leaddetailvalues
	INNER JOIN (SELECT SUM(CONVERT(NUMERIC,detailvalue)) AS Counter, leadid FROM TableDetailValues WHERE tabledetailvalues.detailfieldid = 26061 GROUP BY leadid) AS SMSCount ON SMSCount.LeadID = leaddetailvalues.leadid
	WHERE leaddetailvalues.detailfieldid = 12514

	SET @myERROR = 1
	
	COMMIT TRAN -- No Errors, so commit all work

	GOTO END_NOW

	HANDLE_ERROR:
		ROLLBACK TRAN
		SELECT 'Failure' AS [Outcome]

	END_NOW:
		SELECT Clients.CompanyName, Clients.ClientID, SUM(FLOOR(ld.EmailBLOBSize/160)+1), tr_real.LeadID AS [Aquarium Billing (Client 38) LeadID]
		FROM @TableRows tr 
		INNER JOIN TableRows tr_real ON tr_real.TableRowID = tr.TableRowID
		INNER JOIN LeadDetailValues ldv ON ldv.LeadID = tr_Real.LeadID AND ldv.DetailFieldID = 6453
		INNER JOIN Clients  ON Clients.ClientID = ldv.DetailValue
		INNER JOIN LeadEvent  ON LeadEvent.ClientID = Clients.ClientID AND DATEDIFF(mm, LeadEvent.WhenCreated, dbo.fn_GetDate_Local()) = 1 
		INNER JOIN EventType  ON LeadEvent.EventTypeID = EventType.EventTypeID AND EventType.EventSubTypeID = 13
		INNER JOIN dbo.LeadDocument ld ON ld.LeadDocumentID = LeadEvent.LeadDocumentID
		GROUP BY tr_real.TableRowID, Clients.ClientID, tr_real.LeadID, Clients.CompanyName
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_MonthlyReconciliation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_MonthlyReconciliation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_MonthlyReconciliation] TO [sp_executeall]
GO
