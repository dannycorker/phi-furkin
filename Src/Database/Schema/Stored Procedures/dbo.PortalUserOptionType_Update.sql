SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PortalUserOptionType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserOptionType_Update]
(

	@PortalUserOptionTypeID int   ,

	@OptionTypeName nchar (100)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PortalUserOptionType]
				SET
					[OptionTypeName] = @OptionTypeName
				WHERE
[PortalUserOptionTypeID] = @PortalUserOptionTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOptionType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserOptionType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOptionType_Update] TO [sp_executeall]
GO
