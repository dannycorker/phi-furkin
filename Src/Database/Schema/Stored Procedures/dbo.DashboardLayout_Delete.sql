SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DashboardLayout table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardLayout_Delete]
(

	@DashboardLayoutID int   
)
AS


				DELETE FROM [dbo].[DashboardLayout] WITH (ROWLOCK) 
				WHERE
					[DashboardLayoutID] = @DashboardLayoutID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardLayout_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardLayout_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardLayout_Delete] TO [sp_executeall]
GO
