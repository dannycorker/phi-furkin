SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LookupList table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupList_GetByClientID]
(

	@ClientID int   
)
AS


				SELECT
					[LookupListID],
					[LookupListName],
					[LookupListDescription],
					[ClientID],
					[Enabled],
					[SortOptionID],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[LeadTypeID],
					[IsShared]
				FROM
					dbo.fnLookupListShared(@ClientID)
				WHERE
										[ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupList_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupList_GetByClientID] TO [sp_executeall]
GO
