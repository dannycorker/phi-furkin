SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-18
-- Description:	Work out which Tasks this scheduler can do, and
--              assign them so that they don't get picked up twice.
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedTask__ClearLockedTasks] 
	@QueueID int = null, 
	@SchedulerID int = null, 
	@ServerName varchar(50) = null, 
	@TaskID int = null 
AS
BEGIN
	
	SET NOCOUNT ON;

	UPDATE dbo.AutomatedTaskInfo 
	SET LockDateTime = NULL             -- Frees the task up, ready for the next time it needs to be run.
	WHERE (@QueueID IS NULL OR QueueID = @QueueID) 
	AND (@SchedulerID IS NULL OR SchedulerID = @SchedulerID) 
	AND (@ServerName IS NULL OR ServerName = @ServerName) 
	AND (@TaskID IS NULL OR TaskID = @TaskID) 

	IF (@TaskID > 0)
	BEGIN
		UPDATE dbo.AutomatedTask 
		SET AlreadyRunning = 0 
		WHERE TaskID = @TaskID
	END
	IF (@QueueID IS NULL AND @SchedulerID IS NULL AND @ServerName IS NULL AND @TaskID IS NULL)
	BEGIN
		UPDATE dbo.AutomatedTask 
		SET AlreadyRunning = 0 
		WHERE AlreadyRunning = 1
	END
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__ClearLockedTasks] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask__ClearLockedTasks] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__ClearLockedTasks] TO [sp_executeall]
GO
