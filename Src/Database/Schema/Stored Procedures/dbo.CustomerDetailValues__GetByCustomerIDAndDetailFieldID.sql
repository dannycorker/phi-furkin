SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CustomerDetailValues table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerDetailValues__GetByCustomerIDAndDetailFieldID]
(

	@CustomerID int,
	@DetailFieldID int,
	@ClientID int   
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[CustomerDetailValueID],
					[ClientID],
					[CustomerID],
					[DetailFieldID],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID]
				FROM
					[dbo].[CustomerDetailValues]
				WITH (NOLOCK) 					
				WHERE
					[CustomerID] = @CustomerID AND
					[DetailFieldID] = @DetailFieldID AND
					[ClientID] = @ClientID
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerDetailValues__GetByCustomerIDAndDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerDetailValues__GetByCustomerIDAndDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerDetailValues__GetByCustomerIDAndDetailFieldID] TO [sp_executeall]
GO
