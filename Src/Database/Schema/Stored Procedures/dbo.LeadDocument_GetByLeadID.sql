SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LeadDocument table through a foreign key
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[LeadDocument_GetByLeadID]
(

	@LeadID int   
)
AS
BEGIN

	/* This should never be used! */
	RETURN -1

END
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_GetByLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocument_GetByLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_GetByLeadID] TO [sp_executeall]
GO
