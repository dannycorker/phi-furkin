SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ==========================================================
-- Author:		Jan Wilson
-- Create date: 16-12-2014
-- Description:	Retrieves the windows username associated
--              with the account.
-- ===========================================================
CREATE PROCEDURE [dbo].[LDAP_GetWindowsUsername]
(
	@ClientID							INT,
	@ClientPersonnelID					INT
	
)
AS
	DECLARE @WindowsUsername	VARCHAR(50)
	
	SELECT @WindowsUserName = TPMUC.UserName
	FROM 
		dbo.ThirdPartyMappingUserCredential TPMUC WITH (NOLOCK) 
	WHERE
		TPMUC.UserID = @ClientPersonnelID
		AND TPMUC.ClientID = @ClientID

	SELECT ISNULL(@WindowsUsername, '') WindowsUsername
	

GO
GRANT VIEW DEFINITION ON  [dbo].[LDAP_GetWindowsUsername] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LDAP_GetWindowsUsername] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LDAP_GetWindowsUsername] TO [sp_executeall]
GO
