SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 2014-08-01
-- Description:	Attempts to find what categorys the response falls into
-- =============================================
CREATE PROCEDURE [dbo].[SMSCategoryResult__CategoriseMessageResponse]

	@TableRowID INT, -- table row id of the response	
	@LeadTypeID INT  

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @MessageResponseDetailFieldID INT
	DECLARE @ClientID INT
	
	SELECT @MessageResponseDetailFieldID=ColumnFieldID, @ClientID=ClientID 
	FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE LeadTypeID=@LeadTypeID AND ThirdPartyFieldID=376

	IF @MessageResponseDetailFieldID IS NOT NULL AND @MessageResponseDetailFieldID>0
	BEGIN
		DECLARE @Words TABLE 
		(
			WordOrder INT IDENTITY(1, 1), 
			WordText VARCHAR(50),
			IsSentenceBreak BIT  /* True for full stop etc, false for space */
		)
		
		DECLARE @CursorPos INT = 1, 
			@NextBreakPos INT = 0, 
			@IsSentenceBreak BIT = 0,
			@SpecialPhraseStart INT = 0, 
			@SpecialPhrase VARCHAR(100),
			@SpecialPhraseMax INT = 0, 
			@SentenceLength INT = 0,
			@CurrentWordLength INT = 0,
			@CurrentWord VARCHAR(50), 
			@Test INT = 0, 
			@ProfanityScore INT = 0,
			@WordNumber INT = 0, 
			@WordCount INT = 0, 
			@WordOrder INT = 0,
			@CurrentIsBad BIT, 
			@CurrentIsGood BIT, 
			@CurrentIsReverse BIT, 
			@CurrentScoreMultiplier DECIMAL(5, 2), 
			@CurrentScore DECIMAL(3, 1), 
			@Phrase VARCHAR(MAX) = '', 
			@PhraseReverse BIT = 0, 
			@PhraseScore DECIMAL (18, 2) = 0, 
			@PhraseScoreMultiplier DECIMAL (18, 2) = 0, 
			@OverallScore DECIMAL (18, 2) = 0, 
			@OverallPositiveCount INT = 0,
			@OverallPositiveScore DECIMAL (18, 2) = 0,
			@OverallNegativeCount INT = 0,
			@OverallNegativeScore DECIMAL (18, 2) = 0,
			@Sentence VARCHAR(2000)

		print @MessageResponseDetailFieldID
	
		SELECT @Sentence=v.DetailValue FROM TableDetailValues v WITH (NOLOCK) 
		WHERE v.DetailFieldID=@MessageResponseDetailFieldID AND v.ClientID=@ClientID AND v.TableRowID=@TableRowID
	
		/* Expand n't into NOT first of all */ 
		SELECT @Sentence = REPLACE(@Sentence, 'n''t', ' NOT') 
			/* Note the sentence length */ 
		SELECT @SentenceLength = LEN(@Sentence)
		
		SELECT @SpecialPhraseMax = MAX(w.SMSCategoryWordID) 
		FROM dbo.SMSCategoryWord w 
		WHERE w.WordOrPhrase LIKE '% %' 

		
		WHILE @SpecialPhraseStart < @SpecialPhraseMax
		BEGIN
			/* Get next special phrase in order */
			SELECT TOP (1) @SpecialPhraseStart = w.SMSCategoryWordID, 
			@SpecialPhrase = w.WordOrPhrase 
			FROM dbo.SMSCategoryWord w 
			WHERE w.WordOrPhrase LIKE '% %'  
			AND w.SMSCategoryWordID > @SpecialPhraseStart 
			ORDER BY w.SMSCategoryWordID
			
			/* Replace spaces with underscores within the special phrase */
			/* "in no way" becomes "in_no_way" to keep it all together */
			SELECT @Sentence = REPLACE(@Sentence, @SpecialPhrase, REPLACE(@SpecialPhrase, ' ', '_'))
			
		END
		
		WHILE @CursorPos > 0 AND @Test < 100
		BEGIN
			
			SELECT TOP (1) 
			@NextBreakPos = f.pos, 
			@IsSentenceBreak = f.isBreak 
			FROM dbo.fnNextWordChangeIsBreak (@Sentence, @CursorPos) f 
			
			--SELECT @nextbreakpos, @IsSentenceBreak
			
			IF @@ROWCOUNT = 0
			BEGIN
				SELECT @NextBreakPos = 0, @IsSentenceBreak = 0
			END			
			
			IF @NextBreakPos > 0
			BEGIN
				/* Extract this word or phrase */
				SELECT @CurrentWordLength = @NextBreakPos - @CursorPos
				
				/* Ignore garbage strings 50 chars or more */
				IF @CurrentWordLength < 51
				BEGIN
					SELECT @CurrentWord = SUBSTRING(@Sentence, @CursorPos, @CurrentWordLength)
				END
				ELSE
				BEGIN
					SELECT @CurrentWord = '(too long)'
				END
				
				/* Loop round again */
				SELECT @CursorPos = @NextBreakPos + 1

			END
			ELSE
			BEGIN
				/* Take everything up to the end of the sentence */
				SELECT @CurrentWordLength = (@SentenceLength - @CursorPos) + 1
				
				/* Ignore garbage strings 50 chars or more */
				IF @CurrentWordLength < 51
				BEGIN
					IF @CurrentWordLength < 1
					BEGIN
						SELECT @CurrentWord = ''
					END
					ELSE
					BEGIN
						SELECT @CurrentWord = SUBSTRING(@Sentence, @CursorPos, @CurrentWordLength)
					END
				END
				ELSE
				BEGIN
					SELECT @CurrentWord = '(too long)'
				END
				
				/* Definitely end of sentence */
				SET @IsSentenceBreak = 1
				
				/* Finish the loop */
				SELECT @CursorPos = -1
			END
			
			/* Turn phrases back into their constituent words where applicable */
			SELECT @CurrentWord = REPLACE(@currentword, '_', ' ')
			
			/* Store the word itself and the presence of a full stop etc if found */
			INSERT @Words (WordText, IsSentenceBreak) 
			VALUES (@CurrentWord, @IsSentenceBreak)				
			
			SELECT @test += 1
		END		
			
		SELECT v.TableRowID, v.MatterID, v.DetailValue, w.*, c.Category FROM SMSCategoryWord w WITH (NOLOCK) 
		INNER JOIN SMSCategory c WITH (NOLOCK) ON c.SMSCategoryID=w.SMSCategoryID
		INNER JOIN TableDetailValues v WITH (NOLOCK) ON  v.DetailValue LIKE '%' +  w.WordOrPhrase + '%' AND v.DetailFieldID=@MessageResponseDetailFieldID AND v.ClientID=@ClientID AND v.TableRowID=@TableRowID
		INNER JOIN @Words mw ON mw.WordText = w.WordOrPhrase
		WHERE c.ClientID=@ClientID
		ORDER BY c.Category
	
	END
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSCategoryResult__CategoriseMessageResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSCategoryResult__CategoriseMessageResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSCategoryResult__CategoriseMessageResponse] TO [sp_executeall]
GO
