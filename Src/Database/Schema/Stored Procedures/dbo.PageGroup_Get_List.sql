SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the PageGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageGroup_Get_List]

AS


				
				SELECT
					[PageGroupID],
					[ClientID],
					[LeadTypeID],
					[PageGroupName],
					[PageGroupDescription],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[PageGroup] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroup_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageGroup_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroup_Get_List] TO [sp_executeall]
GO
