SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ResourceListDetailValues table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResourceListDetailValues_GetByDetailFieldIDValueIntResourceListIDClientID]
(

	@DetailFieldID int   ,

	@ValueInt int   ,

	@ResourceListID int   ,

	@ClientID int   
)
AS


				SELECT
					[ResourceListDetailValueID],
					[ResourceListID],
					[ClientID],
					[DetailFieldID],
					[LeadOrMatter],
					[DetailValue],
					[ErrorMsg],
					[EncryptedValue],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime]
				FROM
					[dbo].[ResourceListDetailValues] WITH (NOLOCK) 
				WHERE
										[DetailFieldID] = @DetailFieldID
					AND [ValueInt] = @ValueInt
					AND [ResourceListID] = @ResourceListID
					AND [ClientID] = @ClientID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues_GetByDetailFieldIDValueIntResourceListIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceListDetailValues_GetByDetailFieldIDValueIntResourceListIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValues_GetByDetailFieldIDValueIntResourceListIDClientID] TO [sp_executeall]
GO
