SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PageGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PageGroup_Delete]
(

	@PageGroupID int   
)
AS


				DELETE FROM [dbo].[PageGroup] WITH (ROWLOCK) 
				WHERE
					[PageGroupID] = @PageGroupID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroup_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PageGroup_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PageGroup_Delete] TO [sp_executeall]
GO
