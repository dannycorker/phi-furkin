SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the IncomingPostEventValue table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncomingPostEventValue_Insert]
(

	@IncomingPostEventValueID int    OUTPUT,

	@IncomingPostEventID int   ,

	@PostKey varchar (256)  ,

	@PostValue varchar (512)  ,

	@ClientID int   ,

	@CustomerID int   ,

	@LeadID int   ,

	@CaseID int   ,

	@MatterID int   
)
AS


				
				INSERT INTO [dbo].[IncomingPostEventValue]
					(
					[IncomingPostEventID]
					,[PostKey]
					,[PostValue]
					,[ClientID]
					,[CustomerID]
					,[LeadID]
					,[CaseID]
					,[MatterID]
					)
				VALUES
					(
					@IncomingPostEventID
					,@PostKey
					,@PostValue
					,@ClientID
					,@CustomerID
					,@LeadID
					,@CaseID
					,@MatterID
					)
				-- Get the identity value
				SET @IncomingPostEventValueID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEventValue_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncomingPostEventValue_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEventValue_Insert] TO [sp_executeall]
GO
