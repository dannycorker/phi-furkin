SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Substitutions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Substitutions_Insert]
(

	@SubstitutionsID int    OUTPUT,

	@SubstitutionsVariable varchar (50)  ,

	@SubstitutionsDisplayName varchar (50)  ,

	@SubstitutionsDescription varchar (250)  
)
AS


				
				INSERT INTO [dbo].[Substitutions]
					(
					[SubstitutionsVariable]
					,[SubstitutionsDisplayName]
					,[SubstitutionsDescription]
					)
				VALUES
					(
					@SubstitutionsVariable
					,@SubstitutionsDisplayName
					,@SubstitutionsDescription
					)
				-- Get the identity value
				SET @SubstitutionsID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Substitutions_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Substitutions_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Substitutions_Insert] TO [sp_executeall]
GO
