SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClientStyle table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientStyle_Find]
(

	@SearchUsingOR bit   = null ,

	@ClientStyleID int   = null ,

	@ClientID int   = null ,

	@StyleRuleID int   = null ,

	@StyleValue varchar (100)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClientStyleID]
	, [ClientID]
	, [StyleRuleID]
	, [StyleValue]
    FROM
	[dbo].[ClientStyle] WITH (NOLOCK) 
    WHERE 
	 ([ClientStyleID] = @ClientStyleID OR @ClientStyleID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([StyleRuleID] = @StyleRuleID OR @StyleRuleID IS NULL)
	AND ([StyleValue] = @StyleValue OR @StyleValue IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClientStyleID]
	, [ClientID]
	, [StyleRuleID]
	, [StyleValue]
    FROM
	[dbo].[ClientStyle] WITH (NOLOCK) 
    WHERE 
	 ([ClientStyleID] = @ClientStyleID AND @ClientStyleID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([StyleRuleID] = @StyleRuleID AND @StyleRuleID is not null)
	OR ([StyleValue] = @StyleValue AND @StyleValue is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientStyle_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientStyle_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientStyle_Find] TO [sp_executeall]
GO
