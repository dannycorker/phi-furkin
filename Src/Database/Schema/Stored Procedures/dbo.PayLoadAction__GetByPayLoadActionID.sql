SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 11-11-2015
-- Description:	Get A Pay Load Action
-- =============================================
CREATE PROCEDURE [dbo].[PayLoadAction__GetByPayLoadActionID]
	@PayLoadActionID INT
AS
BEGIN	
	
	SET NOCOUNT ON;

	SELECT * FROM PayLoadAction WITH (NOLOCK) 
	WHERE PayLoadActionID=@PayLoadActionID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[PayLoadAction__GetByPayLoadActionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PayLoadAction__GetByPayLoadActionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PayLoadAction__GetByPayLoadActionID] TO [sp_executeall]
GO
