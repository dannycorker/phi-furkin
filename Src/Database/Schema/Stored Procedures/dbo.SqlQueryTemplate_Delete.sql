SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SqlQueryTemplate table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryTemplate_Delete]
(

	@SqlQueryTemplateID int   
)
AS


				DELETE FROM [dbo].[SqlQueryTemplate] WITH (ROWLOCK) 
				WHERE
					[SqlQueryTemplateID] = @SqlQueryTemplateID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTemplate_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTemplate_Delete] TO [sp_executeall]
GO
