SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Dave Morgan
-- Create date: 2013-01-11
-- Description:	Get the next reference value int from a Resource List
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetNextRefValueIntFromRL] 
	@RLRefIntDFID int,
	@ResourceListID int
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ValueInt int
	
	BEGIN TRAN
	
		/* 
			Increment the last used value by 1 first. 
			Do this within a transaction so that nothing else can read it
			until we have finished.
		*/
		UPDATE dbo.ResourceListDetailValues 
		SET DetailValue = COALESCE(ValueInt, 0) + 1 
		WHERE DetailFieldID = @RLRefIntDFID and ResourceListID=@ResourceListID 
		
		/*
			Now read the new value, commit the tran, and return the new value.
		*/
		SELECT @ValueInt = ValueInt 
		FROM dbo.ResourceListDetailValues 
		WHERE DetailFieldID = @RLRefIntDFID AND ResourceListID=@ResourceListID 
		
	COMMIT
	
	RETURN ISNULL(@ValueInt,0) 
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetNextRefValueIntFromRL] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetNextRefValueIntFromRL] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetNextRefValueIntFromRL] TO [sp_executeall]
GO
