SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-10-30
-- Description:	Saves the data back from custom control for use in the sae
-- Used by:		DF 159267
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1272_Claim_SaveLimitUpdateData] 
(
	@LeadID INT,
	--@Claimed MONEY,
	--@Paid MONEY,
	@PolicySection INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
	--EXEC dbo._C00_SimpleValueIntoField 159271, @Claimed, @LeadID
	--EXEC dbo._C00_SimpleValueIntoField 159272, @Paid, @LeadID
	EXEC dbo._C00_SimpleValueIntoField 159273, @PolicySection, @LeadID

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_SaveLimitUpdateData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Claim_SaveLimitUpdateData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Claim_SaveLimitUpdateData] TO [sp_executeall]
GO
