SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 2016-06-14
-- Description:	Updates the given payment
-- =============================================
CREATE PROCEDURE [dbo].[Payment__Update]
	@PaymentID INT,
	@ClientID INT,
	@CustomerID INT,
	@PaymentDateTime DATETIME,
	@PaymentTypeID INT,
	@DateReceived DATETIME = NULL,
	@PaymentDescription VARCHAR(2000),
	@PaymentReference VARCHAR(2000),
	@PaymentNet NUMERIC(18,2) = NULL,
	@PaymentVAT NUMERIC(18,2) = NULL,
	@PaymentGross NUMERIC(18,2),
	@PaymentCurrency INT = NULL,
	@RelatedOrderRef VARCHAR(2000) = NULL,
	@RelatedRequestDescription VARCHAR(2000) = NULL,
	@PayeeFullName VARCHAR(2000) = NULL,
	@MaskedAccountNumber VARCHAR(2000) = NULL,
	@AccountTypeDescription VARCHAR(100) = NULL,
	@PaymentStatusID INT = NULL,
	@DateReconciled DATE = NULL,
	@ReconciliationOutcome VARCHAR(2000) = NULL,
	@ObjectTypeID INT = NULL,
	@ObjectID INT = NULL,
	@Comments VARCHAR(2000) = NULL,
	@PaymentFileName VARCHAR(200) = NULL,
	@FailureCode VARCHAR(50) = NULL,
	@FailureReason VARCHAR(500) = NULL,
	@WhoCreated INT = NULL,
	@WhenCreated DATETIME = NULL, 	
	@WhoModified INT,
	@WhenModified DATETIME = NULL
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @ErrorMessage VARCHAR(200)

	BEGIN TRAN
	BEGIN TRY

		UPDATE Payment 
		SET		
				DateReceived = @DateReceived, 
				FailureCode = @FailureCode, 
				FailureReason =@FailureReason, 
				PaymentStatusID = @PaymentStatusID,
				PaymentDateTime = @PaymentDateTime,
				PaymentTypeID = @PaymentTypeID,
				PaymentDescription = @PaymentDescription,
				PaymentReference = @PaymentReference,
				PaymentNet = @PaymentNet,
				PaymentVAT = @PaymentVAT,
				PaymentGross = @PaymentGross,
				PaymentCurrency = @PaymentCurrency,
				RelatedOrderRef = @RelatedOrderRef,
				RelatedRequestDescription = @RelatedRequestDescription,
				PayeeFullName = @PayeeFullName,
				MaskedAccountNumber = @MaskedAccountNumber,
				AccountTypeDescription = @AccountTypeDescription,
				PaymentFileName = @PaymentFileName,
				DateReconciled = @DateReconciled,
				ReconciliationOutcome = @ReconciliationOutcome,
				ObjectTypeID = @ObjectTypeID,
				ObjectID = @ObjectID,
				WhenModified = dbo.fn_GetDate_Local(),
				WhoModified = @WhoModified,
				Comments = @Comments
		WHERE PaymentID = @PaymentID

		-- write customer ledger entry if a successful payment
		-- update CustomerPaymentSchedule entry with customer ledger id
		-- update PurchasedProductPaymentSchedule entry with customer ledger id and CustomerPaymentScheduleID

		COMMIT TRANSACTION
		
	END TRY
	
	BEGIN CATCH

		SELECT @ErrorMessage = 'Error while saving values: ' + CAST(@@ERROR AS VARCHAR) + ': ' + ERROR_MESSAGE() 
		ROLLBACK
		RAISERROR( @ErrorMessage, 16, 1 )

	END CATCH

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Payment__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Payment__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Payment__Update] TO [sp_executeall]
GO
