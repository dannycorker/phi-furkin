SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- ==============================================================
-- Author:		Jan Wilson
-- Create date: 2013-03-01
-- Description:	Return the total number of documents
--              in the queue.

-- Parameters:  @ClientId
--              The client that the documents relate to
--              @Uploaded
--              The total to return 
--                  - 0 for unprocessed documents
--                  - 1 for processed documents
-- ==============================================================
CREATE PROCEDURE [dbo].[DocumentBatch_GetTotalDocuments]
	@ClientID INT,
	@Uploaded BIT
AS
BEGIN
	SELECT
		DBD.EventTypeId,
		COUNT(*) TotalDocuments
	FROM
		DocumentBatchDetail DBD WITH (NOLOCK)
		INNER JOIN DocumentBatch DB WITH (NOLOCK)
		ON DBD.DocumentBatchID = DB.DocumentBatchID
	WHERE
		DBD.Uploaded = 0
		AND DB.Uploaded = 1
		AND DB.Processed = 0
		AND DB.ClientID = @ClientId
	GROUP BY
		DBD.EventTypeId
END



GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatch_GetTotalDocuments] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentBatch_GetTotalDocuments] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentBatch_GetTotalDocuments] TO [sp_executeall]
GO
