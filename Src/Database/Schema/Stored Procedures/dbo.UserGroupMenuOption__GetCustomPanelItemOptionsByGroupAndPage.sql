SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-12
-- Description:	Gets a list of custom panel requirements for a particular UserGroup 
-- =============================================
CREATE PROCEDURE [dbo].[UserGroupMenuOption__GetCustomPanelItemOptionsByGroupAndPage]
(
	@ClientID int,
	@ClientPersonnelAdminGroupID int = NULL,
	@MasterPageName VARCHAR(20)
)
AS
BEGIN

	SET NOCOUNT ON

	IF @ClientPersonnelAdminGroupID = -1 
	BEGIN
	
		SELECT @ClientPersonnelAdminGroupID = NULL

	END

	SELECT *
	FROM UserGroupMenuOption u WITH (NOLOCK)
	WHERE u.ClientID = @ClientID
	AND ((u.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID AND @ClientPersonnelAdminGroupID > 0)  OR (@ClientPersonnelAdminGroupID IS NULL AND u.ClientPersonnelAdminGroupID IS NULL))
	AND u.MasterPageName = @MasterPageName

END


GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption__GetCustomPanelItemOptionsByGroupAndPage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuOption__GetCustomPanelItemOptionsByGroupAndPage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption__GetCustomPanelItemOptionsByGroupAndPage] TO [sp_executeall]
GO
