SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Functions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Functions_Insert]
(

	@FunctionID int   ,

	@FunctionName varchar (50)  ,

	@FunctionDescription varchar (2000)  ,

	@FunctionTypeID int   ,

	@ModuleID int   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[Functions]
					(
					[FunctionID]
					,[FunctionName]
					,[FunctionDescription]
					,[FunctionTypeID]
					,[ModuleID]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@FunctionID
					,@FunctionName
					,@FunctionDescription
					,@FunctionTypeID
					,@ModuleID
					,@WhenCreated
					,@WhenModified
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Functions_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Functions_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Functions_Insert] TO [sp_executeall]
GO
