SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 14-03-2012
-- Description:	Inserts a notification type record and returns the notification type id
-- =============================================
CREATE PROCEDURE [dbo].[NotificationType__Insert]
	
	@NotificationTypeID INT OUTPUT, 
	@ClientID INT,
	@SubClientID INT,
	@Name VARCHAR(50),
	@BackgroundColour VARCHAR(20),
	@ForeColour VARCHAR(20)

AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [NotificationType]
           ([ClientID]
           ,[SubClientID]
           ,[Name]
           ,[BackgroundColour]
           ,[ForeColour])
     VALUES
           (@ClientID, 
            @SubClientID, 
            @Name, 
            @BackgroundColour, 
            @ForeColour)

	-- Get the identity value
	SELECT SCOPE_IDENTITY() NotificationTypeID         
            
            
END





GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationType__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NotificationType__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NotificationType__Insert] TO [sp_executeall]
GO
