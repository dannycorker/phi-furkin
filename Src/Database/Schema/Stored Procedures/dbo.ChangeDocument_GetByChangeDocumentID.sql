SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChangeDocument table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeDocument_GetByChangeDocumentID]
(

	@ChangeDocumentID int   
)
AS


				SELECT
					[ChangeDocumentID],
					[DocumentData],
					[DocumentExtension],
					[WhenCreated],
					[WhoCreated]
				FROM
					[dbo].[ChangeDocument] WITH (NOLOCK) 
				WHERE
										[ChangeDocumentID] = @ChangeDocumentID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDocument_GetByChangeDocumentID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeDocument_GetByChangeDocumentID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDocument_GetByChangeDocumentID] TO [sp_executeall]
GO
