SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the MatterListDisplay table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MatterListDisplay_Get_List]

AS


				
				SELECT
					[MatterListDisplayID],
					[ClientID],
					[LeadTypeID],
					[MatterRef],
					[MatterStatus],
					[MatterID],
					[Field1],
					[Field2],
					[Field3],
					[Field4],
					[Field5],
					[Field1ColumnDetailFieldID],
					[Field2ColumnDetailFieldID],
					[Field3ColumnDetailFieldID],
					[Field4ColumnDetailFieldID],
					[Field5ColumnDetailFieldID],
					[SqlQueryText]
				FROM
					[dbo].[MatterListDisplay] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MatterListDisplay_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterListDisplay_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterListDisplay_Get_List] TO [sp_executeall]
GO
