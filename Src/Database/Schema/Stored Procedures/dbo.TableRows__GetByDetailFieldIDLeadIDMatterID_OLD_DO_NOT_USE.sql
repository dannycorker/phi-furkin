SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Alex Elger
-- Create date: 18-06-2009
-- Description:	Get Table Rows and Pivot
-- =============================================
CREATE PROCEDURE [dbo].[TableRows__GetByDetailFieldIDLeadIDMatterID_OLD_DO_NOT_USE] 
	-- Add the parameters for the stored procedure here
@DetailFieldID int, 
@LeadID int, 
@MatterID int = 0,
@MatterIDList VARCHAR(MAX) = null,
@PrintOnly bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	Declare @cols VARCHAR(MAX),
			@fieldIdents VARCHAR(MAX),
			@rlcols VARCHAR(MAX),
			@rlfieldIdents VARCHAR(MAX),
			@query VARCHAR(MAX),
			@rlQuery VARCHAR(MAX),
			@TableDetailFieldPageID int,
			@basicQuery VARCHAR(MAX),
			@ResourceTable int,
			@ResourceListDetailFieldPageID int,
			@ResourceListDetailFieldID int,
			@pvtStatement varchar(max),
			@queryheader varchar(max),
			@matterClause varchar(MAX),
			@matterRLClause varchar(MAX)

	IF(@MatterID<>0)
	BEGIN
		SET @MatterIDList = convert(varchar(100), @MatterID)
	END
	
	IF(@MatterIDList is null)
	BEGIN
		SET @matterClause = ''
		SET @matterRLClause = ''
	END
	ELSE
	BEGIN	
		SET @matterClause = ' and dbo.TableDetailValues.MatterID in (' + @MatterIDList + ') '
		SET @matterRLClause = ' AND tdv.MatterID in (' + @MatterIDList + ') '
	END

	Select @TableDetailFieldPageID = TableDetailFieldPageID
	From DetailFields
	Where DetailFieldID = @DetailFieldID

	select @cols = COALESCE(@cols + ',[' + FieldCaption + ']', '[' + FieldCaption + ']'),
	       @fieldIdents = COALESCE(@fieldIdents + ',' + CAST(DetailFieldID as varchar(50)), CAST(DetailFieldID as varchar(50)))
	from detailfields with (nolock) 
	where detailfieldpageid = @TableDetailFieldPageID 
	and enabled = 1
	and questiontypeid <> 14
	order by FieldOrder

	IF (select count(*) from detailfields where detailfieldpageid = @TableDetailFieldPageID and questiontypeid = 14) > 0 
	BEGIN
		Set @ResourceTable = 1
	END
	    
	SET @queryheader = 'select * From ('
	
	SET @basicQuery = 'select dbo.TableDetailValues.TableRowID as [TableRowID], dbo.DetailFields.FieldCaption, isnull(dbo.TableDetailValues.DetailValue, '''') as DetailValue--, dbo.TableDetailValues.LeadID as [LeadID], dbo.TableDetailValues.MatterID as [MatterID]
		from dbo.TableDetailValues with (nolock)
		inner join dbo.DetailFields with (nolock) ON dbo.TableDetailValues.DetailFieldID = dbo.DetailFields.DetailFieldID and dbo.DetailFields.QuestionTypeID <> 4 AND (dbo.DetailFields.DetailFieldID IN (' + @fieldIdents + '))
		inner join TableRows tr with (nolock) on tr.TableRowID = dbo.TableDetailValues.TableRowID and  tr.DetailFieldID = ' + convert(varchar(100), @DetailFieldID) + '
		where dbo.TableDetailValues.LeadID = ' + convert(varchar(100), @LeadID) + @matterClause + ' 
		
		
		union

		select dbo.TableDetailValues.TableRowID as [TableRowID], dbo.DetailFields.FieldCaption, isnull(dbo.TableDetailValues.DetailValue, '''') as DetailValue--, dbo.TableDetailValues.LeadID as [LeadID], dbo.TableDetailValues.MatterID as [MatterID]
		from dbo.TableDetailValues with (nolock)
		inner join dbo.DetailFields with (nolock) ON dbo.TableDetailValues.DetailFieldID = dbo.DetailFields.DetailFieldID and dbo.DetailFields.QuestionTypeID = 4 AND (dbo.DetailFields.DetailFieldID IN (' + @fieldIdents + '))
		left join dbo.LookupListItems luli with (nolock) on luli.LookUpListItemID = TableDetailValues.ValueInt
		inner join TableRows tr with (nolock) on tr.TableRowID = dbo.TableDetailValues.TableRowID and  tr.DetailFieldID = ' + convert(varchar(100), @DetailFieldID) + '
		where dbo.TableDetailValues.LeadID = ' + convert(varchar(100), @LeadID) + @matterClause
		

	Select @pvtStatement = ') as src pivot (MAX(DetailValue) for FieldCaption in (' + @cols + '))	as pvt'
	

	IF @ResourceTable = 1
	BEGIN

		select @ResourceListDetailFieldPageID = ResourceListDetailFieldPageID, @ResourceListDetailFieldID = DetailFieldID
		from detailfields with (nolock)
		where detailfieldpageid = @TableDetailFieldPageID 
		and questiontypeid = 14

		select @rlcols = COALESCE(@rlcols + ',[' + FieldCaption + ']', '[' + FieldCaption + ']'),
			   @rlfieldIdents = COALESCE(@rlfieldIdents + ',' + CAST(DetailFieldID as varchar(50)), CAST(DetailFieldID as varchar(50)))
		from detailfields with (nolock) 
		where detailfieldpageid = @ResourceListDetailFieldPageID 
		and enabled = 1
		order by FieldOrder

		SET @rlQuery = 'select tdv.TableRowID as [TableRowID], dbo.DetailFields.FieldCaption, dbo.ResourceListDetailValues.DetailValue as DetailValue--, tdv.LeadID as [LeadID], tdv.MatterID as [MatterID]
		from dbo.ResourceListDetailValues with (nolock)
		inner join dbo.DetailFields with (nolock) ON dbo.ResourceListDetailValues.DetailFieldID = dbo.DetailFields.DetailFieldID and dbo.DetailFields.QuestionTypeID <> 4 And (dbo.DetailFields.DetailFieldID IN (' + @rlfieldIdents + '))
		inner join dbo.TableDetailValues tdv with (nolock) on tdv.DetailFieldID IN (' + @rlfieldIdents + ',' + convert(varchar(100), @ResourceListDetailFieldID) + ') and tdv.ResourceListID = ResourceListDetailValues.ResourceListID and tdv.LeadID = ' + convert(varchar(100), @LeadID) + @matterRLClause + '
		inner join TableRows tr with (nolock) on tr.TableRowID = tdv.TableRowID and  tr.DetailFieldID = ' + convert(varchar(100), @DetailFieldID) + '
			
		union

		select tdv.TableRowID as [TableRowID], dbo.DetailFields.FieldCaption, dbo.ResourceListDetailValues.DetailValue as DetailValue--, tdv.LeadID as [LeadID], tdv.MatterID as [MatterID]
		from dbo.ResourceListDetailValues with (nolock)
		inner join dbo.DetailFields with (nolock) ON dbo.ResourceListDetailValues.DetailFieldID = dbo.DetailFields.DetailFieldID and dbo.DetailFields.QuestionTypeID = 4 And (dbo.DetailFields.DetailFieldID IN (' + @rlfieldIdents + '))
		inner join dbo.TableDetailValues tdv with (nolock) on tdv.DetailFieldID IN (' + @rlfieldIdents + ',' + convert(varchar(100), @ResourceListDetailFieldID) + ') and tdv.ResourceListID = ResourceListDetailValues.ResourceListID and tdv.LeadID = ' + convert(varchar(100), @LeadID) + @matterRLClause + '
		left join dbo.LookupListItems luli with (nolock) on luli.LookUpListItemID = dbo.ResourceListDetailValues.ValueInt		
		inner join TableRows tr with (nolock) on tr.TableRowID = tdv.TableRowID and  tr.DetailFieldID = ' + convert(varchar(100), @DetailFieldID)

	END 
	
	IF (@BasicQuery is not null) and (@rlQuery is not null)
	BEGIN
		select @query = @rlQuery + ' union ' + @BasicQuery			
	END
	ELSE
	BEGIN
		IF(@rlQuery is null)
		BEGIN
			select @query = @basicQuery
		END
		ELSE
		BEGIN
			IF(@basicQuery is null)
			BEGIN
				select @query = @rlQuery
			END		
		END
	END
	

	IF(@cols is not null) and (@rlcols is not null)
	BEGIN
		select @PvtStatement = ') as src pivot (MAX(DetailValue) for FieldCaption in (' + @rlcols + ',' + @cols + '))	as pvt'		
	END
	ELSE
	BEGIN			
		if(@cols is null)
		BEGIN
			select @PvtStatement = ') as src pivot (MAX(DetailValue) for FieldCaption in (' + @rlcols + '))	as pvt'
		END
		ELSE
		BEGIN
			if(@rlcols is null)
			BEGIN
				select @PvtStatement = ') as src pivot (MAX(DetailValue) for FieldCaption in (' + @cols + '))	as pvt'
			END		
		END
	END

	

	select @query = @queryheader + @query + @PvtStatement

	IF @PrintOnly = 1
	BEGIN
		PRINT @query
	END
	ELSE
	BEGIN
	
		EXECUTE(@query)	 
		
	END

END




GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetByDetailFieldIDLeadIDMatterID_OLD_DO_NOT_USE] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows__GetByDetailFieldIDLeadIDMatterID_OLD_DO_NOT_USE] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows__GetByDetailFieldIDLeadIDMatterID_OLD_DO_NOT_USE] TO [sp_executeall]
GO
