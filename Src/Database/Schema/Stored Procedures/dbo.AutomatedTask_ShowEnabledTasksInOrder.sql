SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-18
-- Description:	Show Automated Tasks that are waiting to run
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedTask_ShowEnabledTasksInOrder] 
AS
BEGIN
	
	SELECT * 
	FROM [dbo].[AutomatedTask] at (nolock) 
	LEFT JOIN dbo.AutomatedTaskInfo ati (nolock) ON ati.TaskID = at.TaskID 
	LEFT JOIN dbo.AutomatedTaskQueue atq (nolock) ON atq.AutomatedTaskQueueID = ati.QueueID 
	WHERE at.[Enabled] = 1 
	AND at.[NextRunDateTime] < dbo.fn_GetDate_Local()
	ORDER BY at.[NextRunDateTime]
		
			
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask_ShowEnabledTasksInOrder] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask_ShowEnabledTasksInOrder] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask_ShowEnabledTasksInOrder] TO [sp_executeall]
GO
