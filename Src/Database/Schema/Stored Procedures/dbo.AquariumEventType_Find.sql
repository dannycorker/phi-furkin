SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the AquariumEventType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumEventType_Find]
(

	@SearchUsingOR bit   = null ,

	@AquariumEventTypeID int   = null ,

	@AquariumEventTypeName varchar (50)  = null ,

	@AquariumEventTypeDescription varchar (250)  = null ,

	@AquariumStatusAfterEvent int   = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AquariumEventTypeID]
	, [AquariumEventTypeName]
	, [AquariumEventTypeDescription]
	, [AquariumStatusAfterEvent]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[AquariumEventType] WITH (NOLOCK) 
    WHERE 
	 ([AquariumEventTypeID] = @AquariumEventTypeID OR @AquariumEventTypeID IS NULL)
	AND ([AquariumEventTypeName] = @AquariumEventTypeName OR @AquariumEventTypeName IS NULL)
	AND ([AquariumEventTypeDescription] = @AquariumEventTypeDescription OR @AquariumEventTypeDescription IS NULL)
	AND ([AquariumStatusAfterEvent] = @AquariumStatusAfterEvent OR @AquariumStatusAfterEvent IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AquariumEventTypeID]
	, [AquariumEventTypeName]
	, [AquariumEventTypeDescription]
	, [AquariumStatusAfterEvent]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[AquariumEventType] WITH (NOLOCK) 
    WHERE 
	 ([AquariumEventTypeID] = @AquariumEventTypeID AND @AquariumEventTypeID is not null)
	OR ([AquariumEventTypeName] = @AquariumEventTypeName AND @AquariumEventTypeName is not null)
	OR ([AquariumEventTypeDescription] = @AquariumEventTypeDescription AND @AquariumEventTypeDescription is not null)
	OR ([AquariumStatusAfterEvent] = @AquariumStatusAfterEvent AND @AquariumStatusAfterEvent is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumEventType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventType_Find] TO [sp_executeall]
GO
