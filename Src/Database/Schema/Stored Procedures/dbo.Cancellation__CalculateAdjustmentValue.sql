SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgaan
-- Create date: 2016-10-05
-- Description:	Gets the cancellation adjustment value.
--				Adapted from MidTermAdjustment__CreateFromThirdPartyFields but only for method 3
-- Mods
--	2016-12-12 DCM Set new payment date
--	2016-12-16 DCM Added @calculationdate to _C00_Cancellation__AdjustmentOverride args
--	2017-02-21 DCM Calc newpaymentdate for all flavours of adjustment override
--  2017-07-30 JEL Removed admin fee from calc
--	2017-08-23 ARH Updated to use fn_C00_CalculateActualCollectionDate to determine refund date
--  2017-09-21 JEL 45662 Added Param for Wait Type to allow us to decide if we need to add the wait period or not (Y for collection N for Refund) 
--	2019-04-11 PL  Removed the check for any schedule items cover from date to be <= Cancellation date. #55299
--  2019-05-20 JEL Added rule to cooling off period cancellation IF the customer has claimed in this policy term, treat value of cover as total cost of policy Client 600 filter added so it's stepped over for other client 
--  2020-01-13 CPS for JIRA LPC-356 | Removed the 600 filter from above.  If we want to change the behaviour for T-Pet we'll do it properly
--  2020-09-10 ACE PPET-73			| Add Transaction Fee
--	2021-04-14 ACE SDFURPHI-13		| Use @DateFormatID (101)
-- =============================================
CREATE PROCEDURE [dbo].[Cancellation__CalculateAdjustmentValue]
	@ClientID INT,
	@CaseID INT,
	@LeadEventID INT,
	@WhoCreated INT = NULL
AS
BEGIN

--declare
--	@ClientID INT = 384,
--	@CaseID INT = 1321,
--	@LeadEventID INT = 20669

	SET NOCOUNT ON;
	
	DECLARE @EventComments VARCHAR(2000) = 'Calculating Adjustment Value.' + CHAR(13)+CHAR(10)

	DECLARE @CustomerID INT, 
			@LeadID INT, 
			@MatterID INT, 
			@LeadTypeID INT,
			@DateFormatID INT = dbo.fnGetDbDateFormat(@ClientID)
	
	-- there can only be one matter per case
	SELECT TOP 1 @MatterID = m.MatterID, @LeadID = m.LeadID
	FROM Matter m WITH (NOLOCK) 
	WHERE m.CaseID = @CaseID
	
	SELECT @CustomerID = l.CustomerID, @LeadTypeID = l.LeadTypeID 
	FROM Lead l WITH (NOLOCK) 
	WHERE l.LeadID = @LeadID

	/*WhoCreated is now passed in by Cancellation__CreateFromThirdPartyFields but not _C00_1273_Policy_CalculateAdjustments*/
	IF @WhoCreated IS NULL
	BEGIN

		SELECT @WhoCreated = le.WhoCreated 
		FROM LeadEvent le WITH (NOLOCK) 
		WHERE le.LeadEventID = @LeadEventID

	END
	
	--4397	Description
	DECLARE @Description_DV VARCHAR(2000)
	SELECT @Description_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4397)		
	
	--4401	CancellationDate
	DECLARE @CancellationDate_DV VARCHAR(2000), @IsDateTime_AdjustmentDate BIT, @CancellationDate DATETIME
	SELECT @CancellationDate_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4401)		
	SELECT @IsDateTime_AdjustmentDate = dbo.fnIsDateTime(@CancellationDate_DV)		
	IF(@IsDateTime_AdjustmentDate=1)
	BEGIN
		SELECT @CancellationDate = CAST(@CancellationDate_DV AS DATETIME)
	END

	--4410	MidTermAdjustmentCalculationMethodID
	DECLARE @MidTermAdjustmentCalculationMethodID_DV VARCHAR(2000), @IsInt_MidTermAdjustmentCalculationMethodID BIT, @MidTermAdjustmentCalculationMethodID INT, @ItemID_MidTermAdjustmentCalculationMethodID INT, @MidTermAdjustmentCalculationMethodID_ItemValue VARCHAR(500)
	SELECT @MidTermAdjustmentCalculationMethodID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4410)	
	SELECT @IsInt_MidTermAdjustmentCalculationMethodID = dbo.fnIsInt(@MidTermAdjustmentCalculationMethodID_DV)		
	IF(@IsInt_MidTermAdjustmentCalculationMethodID=1)
	BEGIN
		SELECT @ItemID_MidTermAdjustmentCalculationMethodID = CAST(@MidTermAdjustmentCalculationMethodID_DV AS INT) -- this is the lookuplist item id that has to be referenced back to an actual mid term adjustment calculation method
		SELECT @MidTermAdjustmentCalculationMethodID_ItemValue = llItem.ItemValue FROM LookupListItems llItem WITH (NOLOCK) WHERE LookupListItemID = @ItemID_MidTermAdjustmentCalculationMethodID
		SELECT @MidTermAdjustmentCalculationMethodID = mtam.MidTermAdjustmentCalculationMethodID FROM MidTermAdjustmentCalculationMethod mtam WITH (NOLOCK) WHERE mtam.Method = @MidTermAdjustmentCalculationMethodID_ItemValue
	END		
	
	--4413	PurchasedProductID
	DECLARE @AccountID INT
	DECLARE @PurchasedProductID_DV VARCHAR(2000), @IsInt_PurchasedProductID BIT, @PurchasedProductID INT
	SELECT @PurchasedProductID_DV = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4413)	
	SELECT @IsInt_PurchasedProductID = dbo.fnIsInt(@PurchasedProductID_DV)		
	IF(@IsInt_PurchasedProductID=1)
	BEGIN
		SELECT @PurchasedProductID = CAST(@PurchasedProductID_DV AS INT)	
		SELECT @AccountID=AccountID 
		FROM PurchasedProduct WITH (NOLOCK) 
		WHERE PurchasedProductID=@PurchasedProductID
	END	
	
	DECLARE @CoverFrom DATETIME,
			@CoverTo DATETIME,
			@PurchasedProductPaymentScheduleID INT,
			@OneOffPaymentGross NUMERIC(18,2),
			@OneOffPaymentNet NUMERIC(18,2),
			@OneOffPaymentIPT NUMERIC(18,4),
			@NewCoverTo DATETIME,
			@NewCoverFrom DATETIME,
			@NewPaymentDate DATETIME,
			@ReturnString VARCHAR(1000),
			@WaitPeriodParam INT,
			@NewPaymentDateOnly DATE,
			@TransactionFee NUMERIC(18,2)
						
	DECLARE @NewSchedule TABLE
	(
		ID INT,	
		CoverFrom DATETIME, 
		CoverTo DATETIME, 
		AccountID INT, 
		PaymentNet NUMERIC(18,2),
		PaymentVAT NUMERIC(18,2),
		PaymentGross NUMERIC(18,2),
		TransactionFee NUMERIC(18,2)
	)
	
	DECLARE @Adjustment TABLE 
	( 
		AdjustmentOverride INT, 
		AdjustmentComment VARCHAR(500), 
		AdjustmentGross MONEY, 
		AdjustmentTax MONEY, 
		AdjustmentNet MONEY,
		TransactionFee NUMERIC(18,2),
		WriteOffNet NUMERIC(18,2),
		WriteOffTax NUMERIC(18,2),
		WriteOffGross NUMERIC(18,2)
							
	)

	/* Procedure 
	
		1. Calculate refund
		2. Set all existing non-paid rows to Ignored
		3. Add refund payment into the schedule
		
		or in more detail

		For payments already taken, pro-rata the cancellation adjustment for the period since cancellation (but only for the current live policy)  
		and make a single refund for that adjustment value, 
		and set all uncollected premiums to ignored
		and adjust the amount in the customer payment schedule records (if this has become zero, set this payment status to ignored)
		This logic should work for all increments (including single increments, i.e. annual payers)
			
	*/

	IF(@MidTermAdjustmentCalculationMethodID=3) --Collect/Refund in single payment
	BEGIN
	
		-- this is the cancellation date
		IF (dbo.fnIsDateTime(@CancellationDate_DV)=1) 		
		BEGIN
		
			EXEC _C00_Cancellation__AdjustmentOverride @PurchasedProductID, @CancellationDate
			SELECT @ReturnString = dbo.fnGetSimpleDvByThirdPartyField(@ClientID,@CustomerID,@LeadID,@CaseID,@MatterID,@LeadTypeID,4521)

			INSERT INTO @Adjustment (AdjustmentOverride, AdjustmentComment, AdjustmentGross, AdjustmentTax, AdjustmentNet)
			SELECT [1],[2],[3],[4],[5] 
			FROM
			(SELECT AnyID,Data FROM [dbo].[fnTableOfValuesFromCSVCTE] (@ReturnString)) AS s 
			PIVOT
			(MAX(Data)
			FOR AnyID IN ([1],[2],[3],[4],[5])
			) AS p
			
			IF 1 = ( SELECT AdjustmentOverride FROM @Adjustment ) -- adjustment overrides ( no refund )
			BEGIN

				SELECT @OneOffPaymentGross = 0
				SELECT @OneOffPaymentIPT = 0
				SELECT @OneOffPaymentNET = 0
				SELECT @TransactionFee = 0
			
			END
			ELSE IF 2 = ( SELECT AdjustmentOverride FROM @Adjustment ) -- adjustment overrides ( full refund )
			BEGIN
				
				/*JEL L&G have a rule where a claim on a policy means the full premium is due on cancellation.*/ 
				SELECT @CoverFrom = pp.ValidFrom , @CoverTo = pp.ValidTo 
				FROM PurchasedProduct pp with (NOLOCK) 
				WHERE pp.PurchasedProductID = @PurchasedProductID 

				/*IF the customer has claimed in this policy term, treat value of cover as total cost of policy */ 
				IF EXISTS (	
					SELECT * 
					FROM LeadTypeRelationship ltr WITH (NOLOCK) 
					INNER JOIN Matter m WITH (NOLOCK) on m.MatterID = ltr.ToMatterID AND ltr.ToLeadTypeID = 1490
					INNER JOIN Cases c WITH (NOLOCK) on c.CaseID = m.CaseID
					INNER JOIN MatterDetailValues clstart WITH (NOLOCK) on clstart.MatterID = m.MatterID AND clstart.DetailFieldID = 144366
					INNER JOIN MatterDetailValues clend WITH (NOLOCK) on clend.MatterID = m.MatterID AND clend.DetailFieldID = 145674
					INNER JOIN MatterDetailValues mdv_ailment WITH (NOLOCK) ON mdv_ailment.MatterID = m.MatterID and mdv_ailment.DetailFieldID =  144504
					WHERE ltr.FromMatterId = @MatterID
					AND m.ClientID = @ClientID
					AND c.ClientStatusID IN (4029/*05 Payment submitted*/,4470 /*06 Closed - paid*/) 
					AND mdv_ailment.ValueInt NOT IN (147037 /*Death of Pet or Loss*/)
					AND (
							clstart.ValueDate BETWEEN @CoverFrom AND @CoverTo 
							OR 
							clend.ValueDate BETWEEN @CoverFrom AND @CoverTo
						)
					)
				BEGIN 
		
					SELECT  @NewCoverFrom = d.CoverFrom,
							@NewCoverTo = d.CoverTo,
							@OneOffPaymentGross = d.AdjustmentGross,
							@OneOffPaymentNet = d.AdjustmentNet,
							@OneOffPaymentIPT = D.AdjustmentVAT,
							@TransactionFee = d.TransactionFee
					FROM dbo.fn_C00_CancellationAdjustmentByDailyPremium(@MatterID,@CancellationDate) d
	
				END
				ELSE
				BEGIN 
				
					-- find all the payments that have been made
					INSERT INTO @NewSchedule (ID, PaymentNet,PaymentVAT, PaymentGross, CoverFrom, CoverTo, AccountID, TransactionFee)
					SELECT pp.PurchasedProductPaymentScheduleID, 
						   pp.PaymentGross - PaymentVAT, 
						   pp.PaymentVAT, 
						   pp.PaymentGross,
						   pp.CoverFrom, 
						   pp.CoverTo, 
						   pp.AccountID,
						   pp.TransactionFee
					FROM PurchasedProductPaymentSchedule pp WITH (NOLOCK) 
					WHERE /*pp.CoverFrom<=@CancellationDate -- 2019-04-11 - PL - Removed the check for any schedule items cover from date to be <= Cancellation date. #55299
					AND*/ pp.PurchasedProductID=@PurchasedProductID  AND pp.ReconciledDate IS NOT NULL
					AND pp.PaymentStatusID IN (2,6)
					AND pp.PurchasedProductPaymentScheduleTypeID NOT IN (5)
					AND NOT EXISTS (
						SELECT * 
						FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
						WHERE p.PurchasedProductPaymentScheduleParentID = pp.PurchasedProductPaymentScheduleParentID
						AND p.PaymentStatusID = 4
						AND p.WhenCreated > pp.WhenCreated
					)

					SELECT @OneOffPaymentGross = -SUM(n.PaymentGross),
							@OneOffPaymentNET = -SUM(n.PaymentNet),
							@OneOffPaymentIPT = -SUM(n.PaymentVAT),
							@TransactionFee = -SUM(n.TransactionFee)
					FROM @NewSchedule n

					-- The one-off adjustment is shown as covering the period from the date of cancellation to the end of 
					-- the last period of cover being refunded 
					SELECT @NewCoverFrom = MIN(CoverFrom) FROM @NewSchedule
					SELECT @NewCoverTo = MAX(CoverTo) FROM @NewSchedule

				END

			END
			ELSE IF 3 = ( 
				SELECT AdjustmentOverride 
				FROM @Adjustment 
			) -- adjustment overrides ( custom refund )
			BEGIN

				SELECT @OneOffPaymentGross = AdjustmentGross,
						@OneOffPaymentIPT = AdjustmentTax,
						@OneOffPaymentNET = AdjustmentNet,
						@TransactionFee = 0.00 --a.TransactionFee
				FROM @Adjustment a
				
				SELECT @NewCoverFrom = @CancellationDate
				SELECT @NewCoverTo=MAX(CoverTo)
				FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
				WHERE CoverFrom<=@CancellationDate 
				AND PurchasedProductID=@PurchasedProductID AND ReconciledDate IS NOT NULL
				AND PaymentStatusID IN (2,6)

				IF @NewCoverTo IS NULL
					SELECT @NewCoverTo=@CancellationDate
			
			END
			ELSE  -- adjustment overrides ( no refund )
			BEGIN
				
				
				SELECT @NewCoverFrom = d.CoverFrom,
					   @NewCoverTo = d.CoverTo,
					   @OneOffPaymentGross = d.AdjustmentGross,
					   @OneOffPaymentNet = d.AdjustmentNet,
					   @OneOffPaymentIPT = d.AdjustmentVAT,
					   @TransactionFee = d.TransactionFee
				FROM dbo.fn_C00_CancellationAdjustmentByDailyPremium(@MatterID,@CancellationDate) d

				---- get payment schedule data for cancellation date
				--SELECT	@CoverFrom=CoverFrom, 
				--		@CoverTo=CoverTo, 
				--		@CancellationCoverPeriodGross=PaymentGross, 						
				--		@CancellationCoverPeriodVAT=PaymentVAT
				--FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
				--WHERE CoverFrom<=@CancellationDate AND CoverTo>=@CancellationDate AND PurchasedProductID=@PurchasedProductID
				--AND PaymentStatusID IN (2,6,7)
								
				--IF @CoverFrom IS NULL -- cancellation in a period for which payment has yet to be made
				--BEGIN

				--	SELECT	@CoverFrom=CoverFrom, 
				--			@CoverTo=CoverTo, 
				--			@CancellationCoverPeriodGross=PaymentGross, 						
				--			@CancellationCoverPeriodVAT=PaymentVAT
				--	FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
				--	WHERE CoverFrom<=@CancellationDate AND CoverTo>=@CancellationDate AND PurchasedProductID=@PurchasedProductID
				--	AND PaymentStatusID IN (1,5,7)

				--	SELECT @NumberOfDaysInPeriod = DATEDIFF(DAY,@CoverFrom,@CoverTo) + 1				
				--	SELECT @NumberOfDaysLeftInPeriod = DATEDIFF(DAY,@CancellationDate,@CoverTo) + 1
				--	SELECT @OneOffPaymentForCancellationPeriod = (@CancellationCoverPeriodGross / @NumberOfDaysInPeriod) * (@NumberOfDaysInPeriod-@NumberOfDaysLeftInPeriod)
				--	SELECT @OneOffPaymentForCancellationPeriodVAT = (@CancellationCoverPeriodVAT / @NumberOfDaysInPeriod) * (@NumberOfDaysInPeriod-@NumberOfDaysLeftInPeriod)

				--	SELECT @OneOffPaymentGross = -(@OneOffPaymentForCancellationPeriod)
				--	SELECT @OneOffPaymentVAT = -(@OneOffPaymentForCancellationPeriodVAT)
				--	SELECT @OneOffPaymentNET = @OneOffPaymentGross - @OneOffPaymentVAT

				--	-- The one-off adjustment is shown as covering the period from the date of cancellation to the end of 
				--	-- the last period of cover being refunded 
				--	SELECT @NewCoverFrom = @CoverFrom
				--	SELECT @NewCoverTo = @CancellationDate		

				--END
				--ELSE
				--BEGIN

				--	SELECT @NumberOfDaysInPeriod = DATEDIFF(DAY,@CoverFrom,@CoverTo) + 1				
				--	SELECT @NumberOfDaysLeftInPeriod = DATEDIFF(DAY,@CancellationDate,@CoverTo) + 1
				--	SELECT @OneOffPaymentForCancellationPeriod = (@CancellationCoverPeriodGross / @NumberOfDaysInPeriod) * @NumberOfDaysLeftInPeriod
				--	SELECT @OneOffPaymentForCancellationPeriodVAT = (@CancellationCoverPeriodVAT / @NumberOfDaysInPeriod) * @NumberOfDaysLeftInPeriod

				--	-- Calculate remainder up until the first payment that has not been reconciled
				--	INSERT INTO @NewSchedule (ID, PaymentNet,PaymentVAT, PaymentGross, CoverFrom, CoverTo, AccountID)
				--	SELECT pp.PurchasedProductPaymentScheduleID, 
				--		   pp.PaymentGross - pp.PaymentVAT, 
				--		   pp.PaymentVAT, 
				--		   pp.PaymentGross,
				--		   pp.CoverFrom, 
				--		   pp.CoverTo, 
				--		   pp.AccountID
				--	FROM PurchasedProductPaymentSchedule pp WITH (NOLOCK) 
				--	WHERE pp.CoverFrom>@CoverTo--PaymentDate>@CancellationDate 
				--	AND pp.PurchasedProductID=@PurchasedProductID AND ReconciledDate IS NOT NULL
				--	AND pp.PaymentStatusID IN (2,6)
				--	AND Not EXISTS (SELECT * FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
				--					where p.PurchasedProductPaymentScheduleParentID = pp.PurchasedProductPaymentScheduleID
				--					AND p.PaymentStatusID = 4
				--					and p.WhenCreated > pp.WhenCreated)

				--	SELECT @PaymentForRemainderOfSchedule = ISNULL(SUM(PaymentGross),0) FROM @NewSchedule
				--	SELECT @PaymentNetForRemainderOfSchedule = ISNULL(SUM(PaymentNet),0) FROM @NewSchedule
				--	SELECT @PaymentVATForRemainderOfSchedule = ISNULL(SUM(PaymentVAT),0) FROM @NewSchedule
					
				--	SELECT @OneOffPaymentGross = @OneOffPaymentForCancellationPeriod + @PaymentForRemainderOfSchedule
				--	SELECT @OneOffPaymentVAT = @OneOffPaymentForCancellationPeriodVAT + @PaymentVATForRemainderOfSchedule
				--	SELECT @OneOffPaymentNET = @OneOffPaymentGross - @OneOffPaymentVAT

				--	-- The one-off adjustment is shown as covering the period from the date of cancellation to the end of 
				--	-- the last period of cover being refunded 
				--	SELECT @NewCoverFrom = @CancellationDate
				--	SELECT @NewCoverTo = CASE WHEN @CoverTo > ISNULL(MAX(CoverTo),'1900-01-01') THEN @CoverTo ELSE MAX(CoverTo) END FROM @NewSchedule

				--END

			END
			
			--SELECT @NewPaymentDate = CASE WHEN @OneOffPaymentGross < 0 
			--							THEN CONVERT(DATE,DATEADD(DAY,bf.OneOffAdjustmentWait,dbo.fn_GetDate_Local())) 
			--							ELSE CONVERT(DATE,dbo.fn_GetDate_Local()) END
			--FROM BillingConfiguration bf WITH (NOLOCK) WHERE bf.ClientID=@ClientID
		
			
			/*
			SELECT top 1 @NewPaymentDate = w.[Date]  FROM WorkingDays w WITH (NOLOCK) 
			INNER JOIN BillingConfiguration b on b.ClientID = @ClientID 
			Where w.Date >= CONVERT(DATE,DATEADD(DAY,b.OneOffAdjustmentWait,dbo.fn_GetDate_Local())) 
			and w.Day IN (1, 8, 15, 22, 28)
			*/
			
			/*JEL 45662  If the one off payment is a refund, then don't add a wait period, if it's a collection then add the normal rules*/
			/*NOTE at this stage a refund is positive and a collection is negative*/ 
			IF @OneOffPaymentGross > 0 
			BEGIN 
				
				SELECT @WaitPeriodParam = 3
				
			END
			ELSE
			BEGIN 
						
				SELECT @WaitPeriodParam = 0
					
			END
				
			SELECT @AccountID=AccountID 
			FROM PurchasedProduct WITH (NOLOCK) 
			WHERE PurchasedProductID=@PurchasedProductID
			
			IF EXISTS (
				SELECT * 
				FROM Account a WITH (NOLOCK) 
				WHERE a.AccountID = @AccountID 
				AND a.AccountTypeID = 2
			) 
			BEGIN 
			
				SELECT @NewPaymentDate = dbo.fn_GetDate_Local() 
				SELECT @NewPaymentDateOnly = CAST(@NewPaymentDate as DATE)
			
			END
			ELSE 
			BEGIN 
			
				SELECT @NewPaymentDate = dbo.fn_C00_CalculateActualCollectionDate (@AccountID, 1, DATEADD(DAY,1,dbo.fn_GetDate_Local()), @WaitPeriodParam, -@OneOffPaymentGross) /*2017-08-23 ARH Updated to use fn_C00_CalculateActualCollectionDate*/
				
				SELECT @NewPaymentDateOnly = CAST(@NewPaymentDate as DATE)
			
			END

			/*GPR 2020-11-27 add Net and Tax values for Cancellation Adjustment*/
			DECLARE @TaxLevel1 DECIMAL(18,4) = NULL
			DECLARE @TaxLevel2 DECIMAL(18,4) = NULL
			DECLARE @ValueDate DATE = dbo.fn_GetDate_Local()
			DECLARE @CountryID INT
			DECLARE @StateID INT
			DECLARE @ZipCode VARCHAR(20) /*GPR 2021-01-26*/

			SELECT @ZipCode = c.PostCode 
			FROM Customers c WITH (NOLOCK) 
			WHERE c.CustomerID = @CustomerID

			SELECT @StateID = StateID 
			FROM StateByZip sz WITH (NOLOCK) 
			WHERE sz.Zip = @ZipCode

			IF @ClientID = 605
			BEGIN
				SET @CountryID = 233
			END

			IF @StateID IN (18, 47)
			BEGIN

				SELECT TOP 1 @TaxLevel1=i.TaxProportion 
				FROM USTaxByState i WITH (NOLOCK) 
				WHERE i.CountryID = @CountryID /*2020-10-06 ALM Changed to @CountryID*/
				AND (i.[From] IS NULL OR @ValueDate  >= i.[From])
				AND (i.[To] IS NULL OR @ValueDate <= i.[To])
				AND i.StateID = @StateID
				AND i.TaxName <> 'Kentucky Municipal Tax'
				
				SELECT TOP 1 @TaxLevel2=tax.TaxProportion 
				FROM USTaxByState tax WITH (NOLOCK) 
				WHERE tax.CountryID= @CountryID 
				AND (tax.[From] IS NULL OR @ValueDate >= tax.[From])
				AND (tax.[To] IS NULL OR @ValueDate <= tax.[To])
				AND tax.StateID IS NOT NULL
				AND tax.TaxName  <> 'Kentucky Municipal Tax'
				AND tax.StateID = @StateID

			END

			/*GPR/ALM 2021-02-25 for FURKIN-268*/
			IF @ClientID = 607
			BEGIN
				
				SET @CountryID = 39

				/*Lookup the State from the ZipCode*/
				SELECT @StateID = StateID
				FROM ProvinceByPostalCode pbp WITH (NOLOCK) 
				WHERE pbp.PostalCode = LEFT(@ZipCode,3)

				/*Return the valid GrossMultiplier for the Surcharge Tax by State and Date*/
				SELECT TOP 1 @TaxLevel1=tax.GrossMultiplier
				FROM USTaxByState tax WITH (NOLOCK) 
				WHERE tax.CountryID = @CountryID 
				AND (tax.[From] IS NULL OR @ValueDate >= tax.[From])
				AND (tax.[To] IS NULL OR @ValueDate <= tax.[To])
				AND tax.StateID = @StateID

			END

			IF @ClientID <> 607
			BEGIN
				SET @OneOffPaymentNet = @OneOffPaymentGross - @TransactionFee / ((ISNULL(@TaxLevel1,1.00) + ISNULL(@TaxLevel2,0.00)) + 1) /*ALM 2021-03-10 FURKIN Review ISNULL to 1.00 not 0.00*/
			END
				
			IF @ClientID = 607
			BEGIN
				SET @OneOffPaymentNet = @OneOffPaymentGross / (ISNULL(@TaxLevel1,1.00) + ISNULL(@TaxLevel2,0.00)) /*ALM 2021-03-10 FURKIN Review ISNULL to 1.00 not 0.00*/
			END

			SET @OneOffPaymentIPT = @OneOffPaymentGross - @OneOffPaymentNet /*GPR/ALM 2021-02-25 for FURKIN-268*/

			EXEC _C00_SimpleValueIntoField 178437,@NewPaymentDateOnly,@MatterID,58552

			SELECT @EventComments += ' One Off Payment: ' + ISNULL(CONVERT(VARCHAR,@OneOffPaymentGross),'NULL') + CHAR(13)+CHAR(10)
									+' Cancellation Date: ' + ISNULL(CONVERT(VARCHAR,@CancellationDate,@DateFormatID),'NULL')
									+' New Payment Date: ' + ISNULL(CONVERT(VARCHAR,@NewPaymentDate,@DateFormatID),'NULL')

			EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1

			SELECT @AccountID [AccountID], 
					@PurchasedProductID [PurchasedProductID], 
					@NewCoverFrom [NewCoverFrom], 
					@NewCoverTo [NewCoverTo], 
					@NewPaymentDate [NewPaymentDate], 
					@OneOffPaymentNET [OneOffPaymentNET], 
					@OneOffPaymentIPT [OneOffPaymentVAT], 
					@OneOffPaymentGross [OneOffPaymentGross], 
					0.00 AS [TransactionFee], /*GPR 2020-12-09 removed TransactionFee*/
					/*GPR 2021-04-08 to avoid Column name or number of supplied values does not match table definition. error*/
					0.00, -- WriteOffNet
					0.00, -- WriteOffTax
					0.00  -- WriteOffGross

		END		
	
	END 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Cancellation__CalculateAdjustmentValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Cancellation__CalculateAdjustmentValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Cancellation__CalculateAdjustmentValue] TO [sp_executeall]
GO
