SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Chris Townsend
-- Create date: 2009-12-15
-- Description:	List all Test Customer records for a client (or all clients)
-- =============================================
CREATE PROCEDURE [dbo].[tc] 
	@ClientID int = NULL, 
	@LeadTypeID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT c.ClientID, c.LastName, c.FirstName, c.CustomerID, c.EmailAddress, l.LeadID, l.LeadRef, l.LeadTypeID, lt.LeadTypeName 
	FROM dbo.Customers c (nolock) 
	LEFT JOIN dbo.Lead l (nolock) on l.CustomerID = c.CustomerID 
	LEFT JOIN dbo.LeadType lt (nolock) on lt.LeadTypeID = l.LeadTypeID 
	WHERE c.Test = 1 
	AND (c.ClientID = @ClientID OR @ClientID IS NULL) 
	AND (lt.LeadTypeID = @LeadTypeID OR @LeadTypeID IS NULL)
	ORDER BY c.ClientID, c.LastName, c.FirstName, c.CustomerID 
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[tc] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[tc] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[tc] TO [sp_executeall]
GO
