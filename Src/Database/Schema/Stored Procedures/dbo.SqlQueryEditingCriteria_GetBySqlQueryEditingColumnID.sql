SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditingCriteria table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteria_GetBySqlQueryEditingColumnID]
(

	@SqlQueryEditingColumnID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SqlQueryEditingCriteriaID],
					[ClientID],
					[SqlQueryEditingID],
					[SqlQueryEditingTableID],
					[SqlQueryEditingColumnID],
					[CriteriaText],
					[Criteria1],
					[Criteria2],
					[CriteriaName],
					[SubQueryID],
					[SubQueryLinkType],
					[ParamValue],
					[IsSecurityClause],
					[CriteriaSubstitutions],
					[IsParameterizable],
					[Comparison],
					[IsJoinClause]
				FROM
					[dbo].[SqlQueryEditingCriteria] WITH (NOLOCK) 
				WHERE
					[SqlQueryEditingColumnID] = @SqlQueryEditingColumnID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_GetBySqlQueryEditingColumnID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteria_GetBySqlQueryEditingColumnID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteria_GetBySqlQueryEditingColumnID] TO [sp_executeall]
GO
