SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the NoteType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NoteType_Delete]
(

	@NoteTypeID int   ,

	@ClientID int   
)
AS


				DELETE FROM [dbo].[NoteType] WITH (ROWLOCK) 
				WHERE
					[NoteTypeID] = @NoteTypeID
					AND [ClientID] = @ClientID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NoteType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType_Delete] TO [sp_executeall]
GO
