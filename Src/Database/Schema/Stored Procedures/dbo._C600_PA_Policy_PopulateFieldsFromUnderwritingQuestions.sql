SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry via SA
-- Create date: 2017-06-16
-- Description:	Look through the underwriter table and populate matter or lead level fields according to resource list config.
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Policy_PopulateFieldsFromUnderwritingQuestions]
	 @LeadID		INT
	,@RunAsUserID	INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE  @MatterID	INT
			,@ClientID	INT

	SELECT	 @MatterID = m.MatterID 
			,@ClientID = m.ClientID
	FROM Matter m WITH ( NOLOCK ) 
	WHERE m.LeadID = @LeadID 
	
	DECLARE @BlackHole TABLE ( PassedID INT, OtherID INT )

	DECLARE @FieldsAndAnswers	TABLE ( ResourceListID INT, DetailFieldID INT, Answer VARCHAR(2000), DetailFieldSubTypeID INT, LookupListID INT )            
	DECLARE @DetailValueData	dbo.tvpDetailValueData


	/*Find the fields and answers*/
	INSERT @FieldsAndAnswers ( ResourceListID, DetailFieldID, Answer, DetailFieldSubTypeID, LookupListID )
	SELECT tdvUwQuestion.ResourceListID, df.DetailFieldID, COALESCE(li.ItemValue,tdvCuAnswer.DetailValue,''), df.LeadOrMatter, df.LookupListID
	FROM TableRows tr WITH ( NOLOCK ) 
	INNER JOIN TableDetailValues tdvUwQuestion WITH ( NOLOCK ) on tdvUwQuestion.TableRowID = tr.TableRowID AND tdvUwQuestion.DetailFieldID = 177126 /*Existing Condition*/
	INNER JOIN TableDetailValues tdvCuAnswer WITH ( NOLOCK ) on tdvCuAnswer.TableRowID = tr.TableRowID AND tdvCuAnswer.DetailFieldID IN ( 177306 /*customer response*/, 177305 /*Customer Comment*/ )
	INNER JOIN DetailFields dfAnswer WITH (NOLOCK) on dfAnswer.DetailFieldID = tdvCuAnswer.DetailFieldID
	INNER JOIN ResourceListDetailValues rdvDetailFieldID WITH ( NOLOCK ) on rdvDetailFieldID.ResourceListID = tdvUwQuestion.ResourceListID 
																		AND rdvDetailFieldID.DetailFieldID = CASE WHEN tdvCuAnswer.DetailFieldID = 177306 THEN 177499 /*Detail Field To use for Yes/No*/
																												  WHEN tdvCuAnswer.DetailFieldID = 177305 THEN 177500 /*DetailFieldID to stamp free text answer into*/
																											 END
	INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = rdvDetailFieldID.ValueInt
	LEFT JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListID = dfAnswer.LookupListID AND li.LookupListItemID = tdvCuAnswer.ValueInt
	WHERE tr.DetailFieldID = 177128 /*Existing Conditions*/
	AND tr.LeadID = @LeadID
	AND tdvCuAnswer.DetailValue <> ''

	/*Prepare LeadDetailValues*/
	INSERT @DetailValueData ( ClientID, DetailFieldSubType, DetailFieldID, ObjectID, DetailValueID, DetailValue )
	SELECT @ClientID, faa.DetailFieldSubTypeID, faa.DetailFieldID, @LeadID, ISNULL(ldv.LeadDetailValueID,-1), ISNULL(faa.Answer,'')
	FROM @FieldsAndAnswers faa
	LEFT JOIN LeadDetailValues ldv WITH ( NOLOCK ) on ldv.LeadID = @LeadID and ldv.DetailFieldID = faa.DetailFieldID
	WHERE faa.DetailFieldSubTypeID = 1

	/*Prepare MatterDetailValues*/
	INSERT @DetailValueData ( ClientID, DetailFieldSubType, DetailFieldID, ObjectID, DetailValueID, DetailValue )
	SELECT @ClientID, faa.DetailFieldSubTypeID, faa.DetailFieldID, @LeadID, ISNULL(mdv.MatterDetailValueID,-1), ISNULL(faa.Answer,'')
	FROM @FieldsAndAnswers faa
	LEFT JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.LeadID = @LeadID and mdv.DetailFieldID = faa.DetailFieldID
	WHERE faa.DetailFieldSubTypeID = 2

	/*Handle LookupListItems*/
	UPDATE dvd
	SET DetailValue = li.LookupListItemID
	FROM @FieldsAndAnswers faa 
	INNER JOIN @DetailValueData dvd on dvd.DetailFieldID = faa.DetailFieldID
	INNER JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListID = faa.LookupListID AND li.ItemValue = faa.Answer
	WHERE faa.LookupListID is not NULL


	/*Perform the Save*/
	INSERT @BlackHole ( PassedID, OtherID )
	EXEC DetailValues_BulkSave @DetailValueData, @ClientID, @RunAsUserID 

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_PopulateFieldsFromUnderwritingQuestions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Policy_PopulateFieldsFromUnderwritingQuestions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Policy_PopulateFieldsFromUnderwritingQuestions] TO [sp_executeall]
GO
