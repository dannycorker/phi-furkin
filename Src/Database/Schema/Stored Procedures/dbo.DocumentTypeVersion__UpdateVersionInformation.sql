SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 11/07/2017
-- Description:	Updated the document type version information
-- =============================================
CREATE PROCEDURE [dbo].[DocumentTypeVersion__UpdateVersionInformation]
	@DocumentTypeVersionID INT,
	@ActiveFromDate DATETIME = NULL, 	
	@Archived BIT = NULL
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE DocumentTypeVersion
	SET ActiveFromDate=@ActiveFromDate, Archived=@Archived
	WHERE DocumentTypeVersionID=@DocumentTypeVersionID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeVersion__UpdateVersionInformation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTypeVersion__UpdateVersionInformation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeVersion__UpdateVersionInformation] TO [sp_executeall]
GO
