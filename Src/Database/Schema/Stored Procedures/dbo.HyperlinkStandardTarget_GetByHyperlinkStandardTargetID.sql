SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the HyperlinkStandardTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkStandardTarget_GetByHyperlinkStandardTargetID]
(

	@HyperlinkStandardTargetID int   
)
AS


				SELECT
					[HyperlinkStandardTargetID],
					[ClientID],
					[HyperlinkDetailFieldID],
					[Target],
					[ObjectName],
					[PropertyName],
					[TemplateTypeID],
					[Notes],
					[IsSpecial]
				FROM
					[dbo].[HyperlinkStandardTarget] WITH (NOLOCK) 
				WHERE
										[HyperlinkStandardTargetID] = @HyperlinkStandardTargetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkStandardTarget_GetByHyperlinkStandardTargetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkStandardTarget_GetByHyperlinkStandardTargetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkStandardTarget_GetByHyperlinkStandardTargetID] TO [sp_executeall]
GO
