SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create a new client and 
-- UPDATES:		Simon Brushett, 2011-12-08, Changed to call into the CP_Insert_Helper proc as direct insert into CP no longer allowed
-- =============================================
CREATE PROCEDURE [dbo].[_C38_CreateUserInClient]
(
@LeadEventID int
)


AS
BEGIN
	SET NOCOUNT ON;


Declare	@CompanyName nvarchar(150),
	@TitleID int,
	@FirstName nvarchar(100),
	@MiddleName nvarchar(100),
	@LastName nvarchar(100),
	@JobTitle nvarchar(100),
	@Password nvarchar(100),
	@MobilePhone nvarchar(100),
	@OfficeTelephoneCP nvarchar(50),
	@OfficeTelephoneExtension nvarchar(100),
	@Salt nvarchar(100),
	@OfficeTelephone nvarchar(50),
	@Address1 nvarchar(200),
	@Address2 nvarchar(200),
	@Town nvarchar(200),
	@County nvarchar(50),
	@Country nvarchar(200),
	@PostCode nvarchar(10),
	@ChargeOutRate money,
	@MobileTelephone nvarchar(50),
	@HomeTelephone nvarchar(50),
	@ClientID int,
	@OfficeID int,
	@myERROR int,
	@LeadID int

	Select
	@Address1 = Customers.Address1,
	@Address2 = Customers.Address2,
	@Town = Customers.Town,
	@County = Customers.County,
	@PostCode = Customers.PostCode,
	@CompanyName = Customers.CompanyName,
	@OfficeTelephone = Customers.DayTimeTelephoneNumber,
	@TitleID = ClientPersonnel.TitleID,
	@FirstName = ClientPersonnel.FirstName,
	@LastName = ClientPersonnel.LastName,
	@MiddleName = ClientPersonnel.MiddleName,
	@JobTitle = ClientPersonnel.JobTitle,
	@Password = ClientPersonnel.Password,
	@MobilePhone = ClientPersonnel.MobileTelephone,
	@HomeTelephone = ClientPersonnel.HomeTelephone,
	@OfficeTelephoneCP = ClientPersonnel.OfficeTelephone,
	@OfficeTelephoneExtension = ClientPersonnel.OfficeTelephoneExtension,
	@ChargeOutRate = ClientPersonnel.ChargeOutRate,
	@Salt = ClientPersonnel.Salt,
	@LeadID = Lead.LeadID,
	@ClientID = LeadDetailValues.DetailValue
	from customers
	inner join lead on lead.customerid = customers.customerid
	inner join leadevent on lead.leadid = leadevent.leadid
	inner join ClientPersonnel on ClientPersonnel.ClientPersonnelID = LeadEvent.WhoCreated
	inner join leaddetailvalues on leaddetailvalues.leadid = lead.leadid and leaddetailvalues.detailfieldid = 6453
	where leadevent.leadeventid = @LeadEventID 

	Select top 1 @OfficeID = ClientOfficeID
	From ClientOffices
	Where ClientID =  @ClientID

	/* Create all necessary LeadDetailValues records that don't already exist */
	DECLARE @UserName VARCHAR(200) = @FirstName + '.' + @LastName + convert(varchar,@ClientID) + '@aquarium-software.com'
	
	--EXEC dbo.ClientPersonnel_Insert_Helper @ClientID, @FirstName, @LastName, @UserName, @Password, @Salt, @OfficeID, @TitleID, @MiddleName, @JobTitle, 1, @MobileTelephone, @HomeTelephone, @OfficeTelephoneCP, @OfficeTelephoneExtension, @ChargeOutRate
	EXEC dbo.ClientPersonnel_Insert_Helper	@ClientID, @FirstName, @LastName, @UserName, @Password, @Salt, @OfficeID, @TitleID, @MiddleName, @JobTitle, 1, 
												@MobileTelephone, @HomeTelephone, @OfficeTelephone, @OfficeTelephoneExtension, @ChargeOutRate, 0, 0, NULL, 1, NULL,
												NULL, 0, NULL, 1 /* IsAquarium*/ 

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_CreateUserInClient] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_CreateUserInClient] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_CreateUserInClient] TO [sp_executeall]
GO
