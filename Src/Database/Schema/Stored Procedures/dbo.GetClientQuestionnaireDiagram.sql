SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.GetClientQuestionnaireDiagram    Script Date: 08/09/2006 12:22:37 ******/

CREATE PROCEDURE [dbo].[GetClientQuestionnaireDiagram] @ClientQuestionnaireID int

AS

Select QuestionnaireFlowDiagram
From ClientQuestionnaires
Where ClientQuestionnaireID = @ClientQuestionnaireID




GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientQuestionnaireDiagram] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetClientQuestionnaireDiagram] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientQuestionnaireDiagram] TO [sp_executeall]
GO
