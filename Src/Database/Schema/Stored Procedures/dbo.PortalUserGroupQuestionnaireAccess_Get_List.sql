SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the PortalUserGroupQuestionnaireAccess table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroupQuestionnaireAccess_Get_List]

AS


				
				SELECT
					[PortalUserGroupQuestionnaireAccessID],
					[ClientID],
					[PortalUserGroupID],
					[ClientQuestionnaireID],
					[HasAccess]
				FROM
					[dbo].[PortalUserGroupQuestionnaireAccess] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupQuestionnaireAccess_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess_Get_List] TO [sp_executeall]
GO
