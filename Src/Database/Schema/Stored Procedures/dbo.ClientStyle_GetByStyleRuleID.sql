SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientStyle table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientStyle_GetByStyleRuleID]
(

	@StyleRuleID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientStyleID],
					[ClientID],
					[StyleRuleID],
					[StyleValue]
				FROM
					[dbo].[ClientStyle] WITH (NOLOCK) 
				WHERE
					[StyleRuleID] = @StyleRuleID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientStyle_GetByStyleRuleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientStyle_GetByStyleRuleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientStyle_GetByStyleRuleID] TO [sp_executeall]
GO
