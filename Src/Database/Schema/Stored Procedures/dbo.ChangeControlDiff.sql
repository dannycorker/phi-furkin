SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2017-03-06
-- Description:	outputs the difference between two objects
-- =============================================
CREATE PROCEDURE [dbo].[ChangeControlDiff]
	-- Add the parameters for the stored procedure here
@ChangeControlIDA	int = null
,@ChangeControlIDB	int = null
,@Method			int = null -- 1 = Show additions, 0 = show subtractions , 2= debug
,@inputObja dbo.tvpVarcharInt  READONLY --for getting around the error: An INSERT EXEC statement cannot be nested.
,@inputObjb dbo.tvpVarcharInt  READONLY --for getting around the error: An INSERT EXEC statement cannot be nested.
AS
BEGIN

	SET NOCOUNT ON;
	
	declare @objA varchar(max) 
	     	,@objb varchar(max) 

	select top 1 @objA = cc.objectreference from ChangeControl cc with(nolock) WHERE cc.ChangeControlID = @ChangeControlIDA   -- 18207
	select top 1 @objb = cc.objectreference from ChangeControl cc with(nolock) WHERE cc.ChangeControlID = @ChangeControlIDB

	declare @DiffCollateA table(textA nvarchar(max),lineIDA int)
	declare @DiffCollateB table(textB nvarchar(max),lineIDB int)

	if @ChangeControlIDA is null and @ChangeControlIDB is null-- and ((SELECT * FROM @inputObja) is not null) and ((SELECT * FROM @inputObjb) is not null)
	begin
		insert into @DiffCollateA (textA,lineIDA)
		SELECT * FROM @inputObja
		
		insert into @DiffCollateB (textB,lineIDB)
		SELECT * FROM @inputObjB	
	END
	ELSE
	BEGIN
		insert into @DiffCollateA (textA,lineIDA)
		exec [dbo].[_C00_helptext] @objA

		insert into @DiffCollateB (textB,lineIDB)
		exec [dbo].[_C00_helptext] @objb
	END

	if @Method = 2
	begin

		SELECT * FROM @DiffCollateA

		SELECT * FROM @DiffCollateb

	end

	Declare @Diff table (Texta  nvarchar(max),LineIDA int, SameLine int, Textb nvarchar(max), LineIDB int)

	if ((select COUNT(*) from @DiffCollateA) >= (select COUNT(*) from @DiffCollateb))
	begin
		INSERT INTO @Diff
		SELECT 
			A.textA,A.lineIDA
			,CASE A.textA  WHEN  B.textB THEN 1 ELSE 0 END [SameLine]
			,B.textB,B.lineIDB 
		FROM @DiffCollatea a
		LEFT JOIN @DiffCollateB B ON B.lineIDB = A.lineIDA
	end
	else 
	begin
		INSERT INTO @Diff
		SELECT 
			A.textA,A.lineIDA		
			,CASE A.textA  WHEN  B.textB THEN 1 ELSE 0 END [SameLine]
			,B.textB,B.lineIDB 
		FROM @DiffCollateB B
		LEFT JOIN @DiffCollateA A ON A.lineIDA = B.lineIDB
	end 

	Declare @collateA table (line int,EditType varchar(20),Text nvarchar(max))
	Declare @collateb table (line int,EditType varchar(20),Text nvarchar(max))

	insert into @collateA
		SELECT 
		case when diff.textb is not null  
					then diff.LineIDB 
					else diff.LineIDA 			end [Line] 
		,case	when diff.textb is not null 
					then '+ ' 
					else '- ' 					end [EditType] 
		,case	when diff.textb is not null
				then rtrim(ltrim(Diff.Textb)) 
				else rtrim(ltrim(Diff.Texta)) 	end [Text] 
		FROM @Diff Diff 
		WHERE Diff.SameLine = 0 
		union 
		SELECT 
		case	when diff.textb is not null 
				then diff.LineIDB 
				else diff.LineIDA				end [Line] 
		,case	when diff.textb is not null 
				then '- ' 
				else '+ '						end [EditType] 
		,case when diff.textb is not null 
				then rtrim(ltrim(Diff.Texta))
				else rtrim(ltrim(Diff.Texta))	end [Text] 		
		FROM @Diff Diff 
		WHERE Diff.SameLine = 0 
		
	insert into @collateb 
		select * From @collatea
	
	DECLARE @OUTPUT TABLE (Line	INT, EditType VARCHAR(1),Text NVARCHAR(MAX))

	insert into @OUTPUT
		--Get additions 
		SELECT * 
		FROM @collateA a
		WHERE Not exists (
			SELECT * 
			FROM @collateb b 
			WHERE b.EditType = '+' and b.Text = a.Text
			and a.EditType = '-'
		)
		and not exists (
			SELECT * 
			FROM @Diff d 
			WHERE d.Texta = a.Text
		)
		and a.Text is not null
		UNION
		--Get subtractions
		SELECT * 
		FROM @collateb b
		WHERE b.EditType = '-'
		and not exists (
			SELECT * 
			from @collateA a 
			WHERE a.EditType ='+' 
			and a.Text = b.Text
		)
		and b.Text is not null
		ORDER BY line ASC,EditType ASC

	SELECT * 
	FROM @OUTPUT o 
	WHERE (@Method = 1 and o.EditType = '+')
	or (@Method = 0 and o.EditType = '-')
	or (@Method is null or @Method not in (1,0))

END
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeControlDiff] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeControlDiff] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeControlDiff] TO [sp_executeall]
GO
