SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Angel Petrov
-- Create date: 29/08/2014
-- Description:	LDAP based description
-- JW  2014-12-16 Return the domain name specified at client level (LDAPConfig) table if ThirdPartyUserMappingCredentials domain is empty.
-- JWG 2015-01-21 #30480 TPMUC domain is usually blank, not null, so domain was always blank. Treat blanks as nulls with NULLIF.
-- =============================================
CREATE PROCEDURE [dbo].[GetLDAPInformation]
(
	@EmailAddress varchar(2000)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT
		LDAP.QueryString,
		LDAP.QueryAccountName,
		LDAP.QueryAccountPassword,
		TPMUC.UserName,
		ISNULL(NULLIF(TPMUC.Domain, ''), LDAP.Domain) AS Domain, /* JWG #30480 */
		CP.ClientPersonnelID
   FROM
		dbo.ClientPersonnel AS CP WITH (NOLOCK) 
		INNER JOIN dbo.ThirdPartyMappingUserCredential TPMUC WITH (NOLOCK) 
		ON CP.ClientPersonnelID = TPMUC.UserID
		
		INNER JOIN dbo.LDAPConfig AS LDAP WITH (NOLOCK) 
		ON LDAP.LDAPConfigID = TPMUC.LDAPConfigID
	WHERE
		CP.EmailAddress = @EmailAddress AND LDAP.Enabled = 1;
END
GO
GRANT VIEW DEFINITION ON  [dbo].[GetLDAPInformation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetLDAPInformation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetLDAPInformation] TO [sp_executeall]
GO
