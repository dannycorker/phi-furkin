SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2018-06-05
-- Description:	Updated a saved quote to store the response and time
-- GPR 2019-10-10 created for Aquarius603Dev / LPC-36
-- =============================================
CREATE PROCEDURE [dbo].[Quote_CompleteQuote]
(
	 @QuoteID			INT
	,@XmlResponse		XML = NULL
	,@EndDateTime		DATETIME = NULL
	,@QuoteSessionID	INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE q 
	SET  /*ResponseXml	= ISNULL(@XmlResponse,q.ResponseXml) -- CPS 2018-08-08 remove due to DB size issues
		,*/QuoteEndTime	= ISNULL(@EndDateTime,dbo.fn_GetDate_Local())
		,QuoteSessionID = ISNULL(@QuoteSessionID,q.QuoteSessionID)
	FROM Quote q WITH (NOLOCK) 
	WHERE q.QuoteID = @QuoteID

	RETURN @QuoteID
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_CompleteQuote] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Quote_CompleteQuote] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_CompleteQuote] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Quote_CompleteQuote] TO [sp_executehelper]
GO
