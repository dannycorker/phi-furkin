SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-01-06
-- Description:	Get Detail Fields for Clients Customers etc
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetAllByIDAndEvent]
@EventTypeID int,
@CustomerID int, 
@LeadID int, 
@CaseID int, 
@MatterID int, 
@ClientID int, 
@ClientPersonnelID int, 
@ContactID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	WITH Fields AS (
	
		/*Customer Fields*/
		select df.*, 1 as [OutputOrder]
		from fnDetailFieldsShared(@ClientID) df
		INNER JOIN Customers c WITH (NOLOCK) ON c.ClientID = df.ClientID and c.CustomerID = @CustomerID
		where df.QuestionTypeID = 10
		AND df.LeadOrMatter = 10 AND df.Enabled = 1

		UNION ALL

		/*Lead Fields*/
		select df.*, 2 as [OutputOrder]
		from fnDetailFieldsShared(@ClientID) df
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadTypeID = df.LeadTypeID and l.LeadID = @LeadID
		where QuestionTypeID = 10
		AND df.LeadOrMatter = 1 AND df.Enabled = 1

		UNION ALL

		/*Case Fields*/
		select df.*, 3 as [OutputOrder]
		from fnDetailFieldsShared(@ClientID) df
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadTypeID = df.LeadTypeID 
		INNER JOIN Cases c WITH (NOLOCK) on c.LeadID = l.LeadID and c.CaseID = @CaseID
		where QuestionTypeID = 10
		AND df.LeadOrMatter = 11 AND df.Enabled = 1

		UNION ALL

		/*Matter Fields (From Case)*/
		select df.*, 4 as [OutputOrder]
		from fnDetailFieldsShared(@ClientID) df
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadTypeID = df.LeadTypeID 
		INNER JOIN Cases c WITH (NOLOCK) on c.LeadID = l.LeadID and c.CaseID = @CaseID AND ISNULL(@MatterID,0) = 0
		where QuestionTypeID = 10
		AND df.LeadOrMatter = 2 AND df.Enabled = 1

		UNION ALL

		/*Matter Fields*/
		select df.*, 4 as [OutputOrder]
		from fnDetailFieldsShared(@ClientID) df
		INNER JOIN Lead l WITH (NOLOCK) ON l.LeadTypeID = df.LeadTypeID 
		INNER JOIN Matter m WITH (NOLOCK) on m.LeadID = l.LeadID and m.MatterID = @MatterID
		where QuestionTypeID = 10
		AND df.LeadOrMatter = 2 AND df.Enabled = 1

		UNION ALL

		/*Client*/
		select df.*, 5 as [OutputOrder]
		from fnDetailFieldsShared(@ClientID) df
		where df.ClientID = @ClientID
		AND QuestionTypeID = 10
		AND df.LeadOrMatter = 12 AND df.Enabled = 1

		UNION ALL

		/*Client Personnel*/
		select df.*, 6 as [OutputOrder]
		from fnDetailFieldsShared(@ClientID) df
		INNER JOIN ClientPersonnel cp WITH (NOLOCK) ON cp.ClientID = df.ClientID and cp.ClientPersonnelID = @ClientPersonnelID
		where QuestionTypeID = 10
		AND df.LeadOrMatter = 13 AND df.Enabled = 1

		UNION ALL

		/*Contacts*/
		select df.*, 7 as [OutputOrder]
		from fnDetailFieldsShared(@ClientID) df
		INNER JOIN Contact ct WITH (NOLOCK) ON ct.ClientID = df.ClientID and ct.ContactID = @ContactID
		where QuestionTypeID = 10
		AND df.LeadOrMatter = 14 AND df.Enabled = 1

	)

	SELECT f.DetailFieldID, f.ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, 
                      FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, 
                      MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, 
                      LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView
	FROM Fields f
	INNER JOIN EventTypeEquation e WITH (NOLOCK) ON e.DetailFieldID = f.DetailFieldID
	WHERE e.EventTypeID = @EventTypeID
	ORDER BY f.OutputOrder, f.FieldOrder

END



GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetAllByIDAndEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetAllByIDAndEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetAllByIDAndEvent] TO [sp_executeall]
GO
