SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DiaryAppointment table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DiaryAppointment_Get_List]

AS


				
				SELECT
					[DiaryAppointmentID],
					[ClientID],
					[DiaryAppointmentTitle],
					[DiaryAppointmentText],
					[CreatedBy],
					[DueDate],
					[EndDate],
					[AllDayEvent],
					[Completed],
					[LeadID],
					[CaseID],
					[CustomerID],
					[Version],
					[ExportVersion],
					[RecurrenceInfo],
					[DiaryAppointmentEventType],
					[ResourceInfo],
					[TempReminderTimeshiftID],
					[StatusID],
					[LabelID]
				FROM
					[dbo].[DiaryAppointment] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryAppointment_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment_Get_List] TO [sp_executeall]
GO
