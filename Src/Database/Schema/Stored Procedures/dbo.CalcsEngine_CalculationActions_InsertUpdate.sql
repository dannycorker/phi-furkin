SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-09-06
-- Description:	Inserts or updates a calculation action
-- =============================================
CREATE PROCEDURE [dbo].[CalcsEngine_CalculationActions_InsertUpdate]
(
	@CalculationActionID INT,
	@Name VARCHAR(200),
	@CalculationID INT,
	@ActionTypeID INT,
	@ParentID INT,
	@Context VARCHAR(50),
	@Parameter VARCHAR(MAX),
	@ClientID INT,
	@UserID INT,
	@Description VARCHAR(MAX)
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	IF @CalculationActionID > 0
	BEGIN
	
		UPDATE dbo.CalcsEngine_CalculationActions
		SET Name = @Name,
			WhenModified = dbo.fn_GetDate_Local(), 
			WhoModified = @UserID,
			ActionTypeID = @ActionTypeID,
			ParentID = @ParentID,
			Context = @Context,
			Parameter = @Parameter,
			Description = @Description
		WHERE CalculationActionID = @CalculationActionID
	
	END
	ELSE
	BEGIN
	
		INSERT INTO dbo.CalcsEngine_CalculationActions (Name, ClientID, WhenCreated, WhoCreated, WhenModified, WhoModified, Description, ActionTypeID, ParentID, Context, Parameter, CalculationID)
		VALUES (@Name, @ClientID, dbo.fn_GetDate_Local(), @UserID, dbo.fn_GetDate_Local(), @UserID, @Description, @ActionTypeID, @ParentID, @Context, @Parameter, @CalculationID)
		
		SELECT @CalculationActionID = SCOPE_IDENTITY()
	
	END

	EXEC dbo.CalcsEngine_CalculationActions_Get @ClientID, @CalculationID, @CalculationActionID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_CalculationActions_InsertUpdate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CalcsEngine_CalculationActions_InsertUpdate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CalcsEngine_CalculationActions_InsertUpdate] TO [sp_executeall]
GO
