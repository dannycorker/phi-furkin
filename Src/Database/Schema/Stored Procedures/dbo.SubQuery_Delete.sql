SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SubQuery table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubQuery_Delete]
(

	@SubQueryID int   
)
AS


				DELETE FROM [dbo].[SubQuery] WITH (ROWLOCK) 
				WHERE
					[SubQueryID] = @SubQueryID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SubQuery_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubQuery_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubQuery_Delete] TO [sp_executeall]
GO
