SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[__tvpTest] 
	-- Add the parameters for the stored procedure here
	@tvpIDs tvpInt READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT df.* 
	FROM @tvpIDs t 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.AnyID  

END




GO
GRANT VIEW DEFINITION ON  [dbo].[__tvpTest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__tvpTest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__tvpTest] TO [sp_executeall]
GO
