SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2010-10-13
-- Description:	Populate any number of MatterDetailValues at once 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_PopulateMDVWithValues]
(
	@LeadID int,
	@MatterID int,
	@ClientPersonnelID int,
	@tvpIDValue tvpIDValue READONLY
)
AS
BEGIN
	SET NOCOUNT ON;
	
	/* Maintain history if enabled. Only do this for data that has changed, so do it first or it won't look like it has changed. */
	INSERT INTO DetailValueHistory(ClientID, LeadID, MatterID, DetailFieldID, LeadOrMatter, FieldValue, WhenSaved, ClientPersonnelID, EncryptedValue) 
	SELECT df.ClientID, @LeadID, MatterID, t.AnyID, 1, t.AnyValue, dbo.fn_GetDate_Local(), @ClientPersonnelID, NULL 
	FROM @tvpIDValue t 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.AnyID AND df.LeadOrMatter = 2 AND df.MaintainHistory = 1 
	LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON mdv.DetailFieldID = t.AnyID AND mdv.LeadID = @LeadID 
	WHERE t.AnyValue IS NOT NULL
	AND ((mdv.MatterDetailValueID IS NULL) OR (mdv.DetailValue <> t.AnyValue)) 
	
	/* Update any values that already exist but have changed */
	UPDATE dbo.MatterDetailValues 
	SET DetailValue = t.AnyValue
	FROM @tvpIDValue t 
	INNER JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON mdv.DetailFieldID = t.AnyID AND mdv.LeadID = @LeadID AND mdv.MatterID = @MatterID
	WHERE t.AnyValue IS NOT NULL
	AND mdv.DetailValue <> t.AnyValue
	
	/* Insert any values that don't exist already */
	INSERT INTO dbo.MatterDetailValues (ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
	SELECT df.ClientID, @LeadID, @MatterID, t.AnyID, t.AnyValue
	FROM @tvpIDValue t 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.AnyID AND df.LeadOrMatter = 2 
	WHERE t.AnyValue IS NOT NULL
	AND NOT EXISTS (
		SELECT * 
		FROM dbo.MatterDetailValues mdv WITH (NOLOCK) 
		WHERE mdv.LeadID = @LeadID 
		AND mdv.MatterID = @MatterID
		AND mdv.DetailFieldID = t.AnyID 
	)
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateMDVWithValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_PopulateMDVWithValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_PopulateMDVWithValues] TO [sp_executeall]
GO
