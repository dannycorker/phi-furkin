SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SubQueryLinkage table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubQueryLinkage_Find]
(

	@SearchUsingOR bit   = null ,

	@SubQueryLinkageID int   = null ,

	@ClientID int   = null ,

	@SubQueryID int   = null ,

	@LinksToTableName varchar (50)  = null ,

	@LinksToColumnName varchar (50)  = null ,

	@LinkageDataType varchar (50)  = null ,

	@ParamTableName varchar (50)  = null ,

	@ParamColumnName varchar (50)  = null ,

	@ParamColumnDataType varchar (50)  = null ,

	@ParamColumnHelperSql varchar (2000)  = null ,

	@LeadTypeFilterSql varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SubQueryLinkageID]
	, [ClientID]
	, [SubQueryID]
	, [LinksToTableName]
	, [LinksToColumnName]
	, [LinkageDataType]
	, [ParamTableName]
	, [ParamColumnName]
	, [ParamColumnDataType]
	, [ParamColumnHelperSql]
	, [LeadTypeFilterSql]
    FROM
	[dbo].[SubQueryLinkage] WITH (NOLOCK) 
    WHERE 
	 ([SubQueryLinkageID] = @SubQueryLinkageID OR @SubQueryLinkageID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SubQueryID] = @SubQueryID OR @SubQueryID IS NULL)
	AND ([LinksToTableName] = @LinksToTableName OR @LinksToTableName IS NULL)
	AND ([LinksToColumnName] = @LinksToColumnName OR @LinksToColumnName IS NULL)
	AND ([LinkageDataType] = @LinkageDataType OR @LinkageDataType IS NULL)
	AND ([ParamTableName] = @ParamTableName OR @ParamTableName IS NULL)
	AND ([ParamColumnName] = @ParamColumnName OR @ParamColumnName IS NULL)
	AND ([ParamColumnDataType] = @ParamColumnDataType OR @ParamColumnDataType IS NULL)
	AND ([ParamColumnHelperSql] = @ParamColumnHelperSql OR @ParamColumnHelperSql IS NULL)
	AND ([LeadTypeFilterSql] = @LeadTypeFilterSql OR @LeadTypeFilterSql IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SubQueryLinkageID]
	, [ClientID]
	, [SubQueryID]
	, [LinksToTableName]
	, [LinksToColumnName]
	, [LinkageDataType]
	, [ParamTableName]
	, [ParamColumnName]
	, [ParamColumnDataType]
	, [ParamColumnHelperSql]
	, [LeadTypeFilterSql]
    FROM
	[dbo].[SubQueryLinkage] WITH (NOLOCK) 
    WHERE 
	 ([SubQueryLinkageID] = @SubQueryLinkageID AND @SubQueryLinkageID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SubQueryID] = @SubQueryID AND @SubQueryID is not null)
	OR ([LinksToTableName] = @LinksToTableName AND @LinksToTableName is not null)
	OR ([LinksToColumnName] = @LinksToColumnName AND @LinksToColumnName is not null)
	OR ([LinkageDataType] = @LinkageDataType AND @LinkageDataType is not null)
	OR ([ParamTableName] = @ParamTableName AND @ParamTableName is not null)
	OR ([ParamColumnName] = @ParamColumnName AND @ParamColumnName is not null)
	OR ([ParamColumnDataType] = @ParamColumnDataType AND @ParamColumnDataType is not null)
	OR ([ParamColumnHelperSql] = @ParamColumnHelperSql AND @ParamColumnHelperSql is not null)
	OR ([LeadTypeFilterSql] = @LeadTypeFilterSql AND @LeadTypeFilterSql is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SubQueryLinkage_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubQueryLinkage_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubQueryLinkage_Find] TO [sp_executeall]
GO
