SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
----------------------------------------------------------------------------------------------------
-- Date Created: 2012-10-31
-- Created By: Cathal Sherry
-- Purpose: Show the events and batches that are triggered by a given event type
-- SB  2014-07-10 Updated to use view which includes shared event types
-- CS  2014-10-02 Added @EventTypeID > 0 filter.  Otherwise, when creating a new eventtype (ID 0), the screen shows ALL batch jobs / event types
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[EventType__GetTriggerEventsAndBatchesByEventTypeID] 
(
	@EventTypeID int,
	@ClientID INT = NULL
)
AS

		
	;With TriggeredBy as 
	(
	SELECT 'EventType' [Type],et.EventTypeID [ID], et.EventTypeName [Name], et.Enabled
	, '/EventTypeEdit.aspx?EventTypeID=' + convert(varchar,et.EventTypeID) [EditURL]
	FROM EventTypeAutomatedEvent ae WITH (NOLOCK) 
	INNER JOIN dbo.fnEventTypeShared(@ClientID) et on et.EventTypeID = ae.EventTypeID
	WHERE ae.AutomatedEventTypeID = @EventTypeID
	AND ae.ClientID = @ClientID
	AND @EventTypeID > 0
	UNION
	SELECT 'BatchJob' [Type],at.TaskID [ID], at.Taskname [Name], at.Enabled
	, '/BatchJobEdit.aspx?TaskID=' + convert(varchar,at.TaskID)
	FROM AutomatedTaskParam atp WITH (NOLOCK) 
	INNER JOIN dbo.AutomatedTask at WITH (NOLOCK) on at.TaskID = atp.TaskID 
	WHERE atp.ParamName = 'LEADEVENT_TO_ADD'
	AND atp.ParamValue = @EventTypeID
	AND atp.ClientID = @ClientID
	AND @EventTypeID > 0
	)
	SELECT 'Things that Trigger me' [Things that Trigger me],*
	FROM TriggeredBy
	ORDER BY [Type]

	;With ToTrigger as
	(
	SELECT 'EventType' [Type],et.EventTypeID [ID], et.EventTypeName [Name], et.Enabled
	, '/EventTypeEdit.aspx?EventTypeID=' + convert(varchar,et.EventTypeID) [EditURL]
	FROM EventTypeAutomatedEvent ae WITH (NOLOCK) 
	INNER JOIN dbo.fnEventTypeShared(@ClientID) et on et.EventTypeID = ae.AutomatedEventTypeID
	WHERE ae.EventTypeID = @EventTypeID
	and ae.ClientID = @ClientID
	AND @EventTypeID > 0
	UNION
	SELECT 'BatchJob' [Type], at.TaskID [ID], at.Taskname [Name], at.Enabled
	, '/BatchJobEdit.aspx?TaskID=' + convert(varchar,at.TaskID) [EditURL]
	FROM EventTypeAutomatedTask eat WITH (NOLOCK) 
	INNER JOIN dbo.AutomatedTask at WITH (NOLOCK) on eat.AutomatedTaskID = at.TaskID 
	WHERE eat.EventTypeID = @EventTypeID
	and eat.ClientID = @ClientID
	AND @EventTypeID > 0
	)
	SELECT 'Things I Trigger' [Things I Trigger],* 
	FROM ToTrigger



GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetTriggerEventsAndBatchesByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__GetTriggerEventsAndBatchesByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__GetTriggerEventsAndBatchesByEventTypeID] TO [sp_executeall]
GO
