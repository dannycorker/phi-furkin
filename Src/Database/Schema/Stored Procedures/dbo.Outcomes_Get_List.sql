SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Outcomes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Outcomes_Get_List]

AS


				
				SELECT
					[OutcomeID],
					[ClientQuestionnaireID],
					[OutcomeName],
					[OutcomeDescription],
					[SmsWhenFound],
					[mobileNumber],
					[EmailCustomer],
					[CustomersEmail],
					[ThankYouPage],
					[EmailFromAddress],
					[SmsToCustomer],
					[CustomersEmailSubject],
					[ClientID],
					[EmailClient],
					[ClientEmail],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[Outcomes] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Outcomes_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Outcomes_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Outcomes_Get_List] TO [sp_executeall]
GO
