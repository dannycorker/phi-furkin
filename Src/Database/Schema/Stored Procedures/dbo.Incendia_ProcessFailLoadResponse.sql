SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2010-10-28
-- Description:	Updated DFs based on the FAIL batch load response from Incendia
-- =============================================
CREATE PROCEDURE [dbo].[Incendia_ProcessFailLoadResponse] 
	@ClientID INT,
	@LeadEventID INT,
	@ErrorCode VARCHAR(50),
	@ErrorDescription VARCHAR(500)
AS
BEGIN

	DECLARE @CaseID INT
	DECLARE @LeadID INT
	
	SELECT @CaseID = CaseID, @LeadID = LeadID
	FROM dbo.LeadEvent WITH (NOLOCK) 
	WHERE LeadEventID = @LeadEventID
	
	DECLARE @MatterID INT
	
	SELECT TOP 1 @MatterID = MatterID
	FROM dbo.Matter WITH (NOLOCK) 
	INNER JOIN dbo.Cases WITH (NOLOCK) ON Matter.CaseID = Cases.CaseID
	WHERE Cases.CaseID = @CaseID
	ORDER BY Matter.MatterID

	DECLARE @TableRowID INT
	
	SELECT @TableRowID = TableRowID
	FROM dbo.TableDetailValues WITH (NOLOCK) 
	WHERE ClientID = @ClientID
	AND LeadID = @LeadID
	AND MatterID = @MatterID
	AND ValueInt = @LeadEventID
	
	
	-- Date/Time Confirmed
	INSERT INTO dbo.TableDetailValues (ClientID, LeadID, MatterID, TableRowID, DetailFieldID, DetailValue) 
	VALUES (@ClientID, @LeadID, @MatterID, @TableRowID, 117538, convert(varchar, dbo.fn_GetDate_Local(), 120)) /* Format 120 looks like this: "2010-08-31 12:39:24" */
	
	-- Error Code & Description
	INSERT INTO dbo.TableDetailValues (ClientID, LeadID, MatterID, TableRowID, DetailFieldID, DetailValue) 
	VALUES (@ClientID, @LeadID, @MatterID, @TableRowID, 117539, @ErrorCode),
	       (@ClientID, @LeadID, @MatterID, @TableRowID, 117540, @ErrorDescription)	
	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Incendia_ProcessFailLoadResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Incendia_ProcessFailLoadResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Incendia_ProcessFailLoadResponse] TO [sp_executeall]
GO
