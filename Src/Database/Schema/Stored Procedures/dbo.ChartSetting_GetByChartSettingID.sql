SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ChartSetting table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSetting_GetByChartSettingID]
(

	@ChartSettingID int   
)
AS


				SELECT
					[ChartSettingID],
					[ChartID],
					[SettingName],
					[SettingValue],
					[ClientID]
				FROM
					[dbo].[ChartSetting] WITH (NOLOCK) 
				WHERE
										[ChartSettingID] = @ChartSettingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSetting_GetByChartSettingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSetting_GetByChartSettingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSetting_GetByChartSettingID] TO [sp_executeall]
GO
