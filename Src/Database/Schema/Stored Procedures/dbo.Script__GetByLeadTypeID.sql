SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 6-11-2014
-- Description:	Gets a list of scripts by Lead Type
-- =============================================
CREATE PROCEDURE [dbo].[Script__GetByLeadTypeID]
	@LeadTypeID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM Script WITH (NOLOCK) WHERE LeadTypeID=@LeadTypeID    
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[Script__GetByLeadTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script__GetByLeadTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__GetByLeadTypeID] TO [sp_executeall]
GO
