SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ReferenceValueDateFormat table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueDateFormat_Insert]
(

	@ReferenceValueDateFormatID int    OUTPUT,

	@DateFormat varchar (50)  
)
AS


				
				INSERT INTO [dbo].[ReferenceValueDateFormat]
					(
					[DateFormat]
					)
				VALUES
					(
					@DateFormat
					)
				-- Get the identity value
				SET @ReferenceValueDateFormatID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDateFormat_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueDateFormat_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueDateFormat_Insert] TO [sp_executeall]
GO
