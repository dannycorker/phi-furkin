SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PortalUserGroup table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroup_Update]
(

	@PortalUserGroupID int   ,

	@ClientID int   ,

	@GroupName varchar (50)  ,

	@GroupDescription varchar (200)  ,

	@ShowGroupMessage bit   ,

	@MessageTitle varchar (MAX)  ,

	@MessageBody varchar (MAX)  ,

	@HideOwnCustomer bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PortalUserGroup]
				SET
					[ClientID] = @ClientID
					,[GroupName] = @GroupName
					,[GroupDescription] = @GroupDescription
					,[ShowGroupMessage] = @ShowGroupMessage
					,[MessageTitle] = @MessageTitle
					,[MessageBody] = @MessageBody
					,[HideOwnCustomer] = @HideOwnCustomer
				WHERE
[PortalUserGroupID] = @PortalUserGroupID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroup_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroup_Update] TO [sp_executeall]
GO
