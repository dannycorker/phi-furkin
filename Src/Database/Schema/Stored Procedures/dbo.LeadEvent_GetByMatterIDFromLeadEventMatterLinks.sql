SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records through a junction table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEvent_GetByMatterIDFromLeadEventMatterLinks]
(

	@MatterID int   
)
AS


SELECT dbo.[LeadEvent].[LeadEventID]
       ,dbo.[LeadEvent].[ClientID]
       ,dbo.[LeadEvent].[LeadID]
       ,dbo.[LeadEvent].[WhenCreated]
       ,dbo.[LeadEvent].[WhoCreated]
       ,dbo.[LeadEvent].[Cost]
       ,dbo.[LeadEvent].[Comments]
       ,dbo.[LeadEvent].[EventTypeID]
       ,dbo.[LeadEvent].[NoteTypeID]
       ,dbo.[LeadEvent].[FollowupDateTime]
       ,dbo.[LeadEvent].[WhenFollowedUp]
       ,dbo.[LeadEvent].[AquariumEventType]
       ,dbo.[LeadEvent].[NextEventID]
       ,dbo.[LeadEvent].[CaseID]
       ,dbo.[LeadEvent].[LeadDocumentID]
       ,dbo.[LeadEvent].[NotePriority]
       ,dbo.[LeadEvent].[DocumentQueueID]
       ,dbo.[LeadEvent].[EventDeleted]
       ,dbo.[LeadEvent].[WhoDeleted]
       ,dbo.[LeadEvent].[DeletionComments]
       ,dbo.[LeadEvent].[ContactID]
       ,dbo.[LeadEvent].[BaseCost]
       ,dbo.[LeadEvent].[DisbursementCost]
       ,dbo.[LeadEvent].[DisbursementDescription]
       ,dbo.[LeadEvent].[ChargeOutRate]
       ,dbo.[LeadEvent].[UnitsOfEffort]
       ,dbo.[LeadEvent].[CostEnteredManually]
       ,dbo.[LeadEvent].[IsOnHold]
       ,dbo.[LeadEvent].[HoldLeadEventID]
  FROM dbo.[LeadEvent] WITH (NOLOCK)
 WHERE EXISTS (SELECT 1
                 FROM dbo.[LeadEventMatterLinks] WITH (NOLOCK)
                WHERE dbo.[LeadEventMatterLinks].[MatterID] = @MatterID
                  AND dbo.[LeadEventMatterLinks].[LeadEventID] = dbo.[LeadEvent].[LeadEventID]
                  )
				SELECT @@ROWCOUNT			
				





GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent_GetByMatterIDFromLeadEventMatterLinks] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEvent_GetByMatterIDFromLeadEventMatterLinks] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent_GetByMatterIDFromLeadEventMatterLinks] TO [sp_executeall]
GO
