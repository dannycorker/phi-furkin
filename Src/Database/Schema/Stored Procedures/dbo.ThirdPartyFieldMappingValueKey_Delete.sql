SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ThirdPartyFieldMappingValueKey table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyFieldMappingValueKey_Delete]
(

	@ThirdPartyFieldMappingValueKeyID int   
)
AS


				DELETE FROM [dbo].[ThirdPartyFieldMappingValueKey] WITH (ROWLOCK) 
				WHERE
					[ThirdPartyFieldMappingValueKeyID] = @ThirdPartyFieldMappingValueKeyID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMappingValueKey_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMappingValueKey_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMappingValueKey_Delete] TO [sp_executeall]
GO
