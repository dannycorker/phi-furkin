SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 2014-08-01
-- Description:	Adds a new word to the word or phrase column and counts the words
-- =============================================
CREATE PROCEDURE [dbo].[SMSCategoryResult__AddWordAndUpdateWordCount]
	@TableRowID INT,
	@WordOrPhrase VARCHAR(250)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @LeadTypeID INT
	DECLARE @LeadID INT
	DECLARE @MatterID INT
	DECLARE @ClientID INT
	
	SELECT @MatterID=MatterID FROM TableRows WITH (NOLOCK) WHERE TableRowID=@TableRowID
	SELECT @LeadID=LeadID, @ClientID=ClientID FROM Matter WITH (NOLOCK)  WHERE MatterID=@MatterID
	SELECT @LeadTypeID=LeadTypeID FROM Lead WITH (NOLOCK) WHERE LeadID=@LeadID

	DECLARE @WordOrPhraseColumnDetailFieldID INT	
	SELECT @WordOrPhraseColumnDetailFieldID=ColumnFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ThirdPartyFieldID=901 AND LeadTypeID=@LeadTypeID AND ClientID=@ClientID

	DECLARE @WordCountColumnDetailFieldID INT	
	SELECT @WordCountColumnDetailFieldID=ColumnFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ThirdPartyFieldID=902 AND LeadTypeID=@LeadTypeID AND ClientID=@ClientID


	DECLARE @Count INT
	IF @WordOrPhraseColumnDetailFieldID IS NOT NULL AND @WordOrPhraseColumnDetailFieldID<>0
	BEGIN
	
		UPDATE TableDetailValues SET DetailValue=DetailValue + ', ' + @WordOrPhrase 
		WHERE TableRowID=@TableRowID AND DetailFieldID = @WordOrPhraseColumnDetailFieldID
		
		DECLARE @WordCount INT
		SELECT  @WordCount=LEN(DetailValue) - LEN(REPLACE(DetailValue, ',', '')) + 1  
		FROM TableDetailValues WITH (NOLOCK) 
		WHERE TableRowID=@TableRowID AND DetailFieldID=@WordOrPhraseColumnDetailFieldID
		
		UPDATE TableDetailValues SET DetailValue=@WordCount 
		WHERE TableRowID=@TableRowID and DetailFieldID = @WordCountColumnDetailFieldID
	
	END
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSCategoryResult__AddWordAndUpdateWordCount] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSCategoryResult__AddWordAndUpdateWordCount] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSCategoryResult__AddWordAndUpdateWordCount] TO [sp_executeall]
GO
