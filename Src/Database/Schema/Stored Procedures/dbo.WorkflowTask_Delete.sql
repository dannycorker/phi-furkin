SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the WorkflowTask table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkflowTask_Delete]
(

	@WorkflowTaskID int   
)
AS


				DELETE FROM [dbo].[WorkflowTask] WITH (ROWLOCK) 
				WHERE
					[WorkflowTaskID] = @WorkflowTaskID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkflowTask_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkflowTask_Delete] TO [sp_executeall]
GO
