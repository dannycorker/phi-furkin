SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ScriptDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptDetailFieldTarget_Update]
(

	@ScriptDetailFieldTargetID int   ,

	@ScriptID int   ,

	@ClientID int   ,

	@Target varchar (250)  ,

	@DetailFieldID int   ,

	@ColumnFieldID int   ,

	@Format varchar (250)  ,

	@SectionID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ScriptDetailFieldTarget]
				SET
					[ScriptID] = @ScriptID
					,[ClientID] = @ClientID
					,[Target] = @Target
					,[DetailFieldID] = @DetailFieldID
					,[ColumnFieldID] = @ColumnFieldID
					,[Format] = @Format
					,[SectionID] = @SectionID
				WHERE
[ScriptDetailFieldTargetID] = @ScriptDetailFieldTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDetailFieldTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget_Update] TO [sp_executeall]
GO
