SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.UpdateCustomersDownloadStatus    Script Date: 08/09/2006 12:22:37 ******/
CREATE PROCEDURE [dbo].[UpdateCustomersDownloadStatus] @CustomerID int, @HasDownloaded bit, @DownloadedOn DateTime
AS
Update Customers
Set HasDownloaded = @HasDownloaded,
DownloadedOn = @DownloadedOn
Where CustomerID = @CustomerID




GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateCustomersDownloadStatus] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateCustomersDownloadStatus] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateCustomersDownloadStatus] TO [sp_executeall]
GO
