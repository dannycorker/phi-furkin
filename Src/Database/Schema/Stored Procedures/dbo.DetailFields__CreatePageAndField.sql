SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 22/10/2014
-- Description:	Adds a simple field to a page.
--				If the page does not exist it creates it
-- Modified by: PR 08-04-2015 updated the lookup list matching
-- Modified by: PR 08-04-2015 trimmed the list items and field names 
-- Modified by: PR 02-07-2015 Added validation properties
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__CreatePageAndField]

	@ClientID INT,
	@PageName VARCHAR(250),
	@LeadTypeID INT,
	@DetailFieldSubTypeID INT,
	@FieldName VARCHAR(50),
	@QuestionTypeID INT,
	@ListItems tvpVarchar READONLY,
	@Required BIT,
	@ValidationCriteriaFieldTypeID INT, 
	@ValidationCriteriaID INT,
	@MinimumValue VARCHAR(50),
	@MaximumValue VARCHAR(50),
	@RegEx VARCHAR(2000),
	@ErrorMessage VARCHAR(250)

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @DetailFieldPageID INT

	--Lookup the detail field page id for the given script section page name and script lead type at the correct level(detail field sub type ID)
	SELECT @DetailFieldPageID=DetailFieldPageID FROM DetailFieldPages WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND PageName = @PageName AND LeadTypeID=@LeadTypeID AND LeadOrMatter=@DetailFieldSubTypeID

	IF ((@DetailFieldPageID IS NULL) OR (@DetailFieldPageID=0)) -- page not found so create it
	BEGIN
		-- Create the detail field page
		INSERT INTO DetailFieldPages (ClientID, LeadOrMatter, LeadTypeID, PageName, PageCaption, PageOrder, Enabled)
		VALUES (@ClientID, @DetailFieldSubTypeID, @LeadTypeID, @PageName, @PageName,1,1)

		SET @DetailFieldPageID = SCOPE_IDENTITY()
	END
	
	DECLARE @DetailFieldID INT
	DECLARE @ListIDs TABLE (Unid INT IDENTITY(1,1), AnyID INT)
	
	IF ((@QuestionTypeID = 2) OR (@QuestionTypeID = 4)) -- the detail field is a lookup list so find a matching one or create it
	BEGIN
	
		DECLARE @LookupListID INT
		
		--check there is not a client zero version of this lookup list based upon @ListItems		
		
		
		DECLARE @NumberOfListItems INT 
		SELECT @NumberOfListItems=COUNT(*) FROM @ListItems -- list item count must match 

		DELETE FROM @ListIDs -- re-initialise list items
		INSERT INTO @ListIDs(AnyID) -- place potential lookup list ids into a list
		SELECT DISTINCT LookupListID FROM LookupListItems lli WITH (NOLOCK) 
		INNER JOIN @ListItems ON RTRIM(LTRIM(AnyValue)) = RTRIM(LTRIM(lli.ItemValue)) AND lli.ClientID IN (0, @ClientID)
		GROUP BY LookupListID
		HAVING COUNT(*)= @NumberOfListItems
		
		DECLARE @ID INT

		DECLARE @TestID INT = 0, @Unid INT 

		SELECT TOP 1 @Unid = l.Unid, @ID = l.AnyID
		FROM @ListIDs l 
		ORDER BY l.Unid

		WHILE @Unid > @TestID -- iterate over those lookup list items and check that they exactly match our target list items
		BEGIN
		
			DECLARE @LookupListItemCount INT	
			DECLARE @MatchingRowCount INT
			SELECT @LookupListItemCount=COUNT(*) FROM LookupListItems WITH (NOLOCK) WHERE LookupListID=@ID
			IF(@LookupListItemCount=@NumberOfListItems) -- its the correct length
			BEGIN
				SELECT @MatchingRowCount= COUNT(lli.LookupListItemID) FROM LookupListItems lli WITH (NOLOCK) 
				INNER JOIN @ListItems ON RTRIM(LTRIM(AnyValue)) = RTRIM(LTRIM(lli.ItemValue)) AND lli.ClientID IN (0, @ClientID) AND lli.LookupListID = @ID
				IF(@MatchingRowCount=@NumberOfListItems) -- its the correct length with the inner join
				BEGIN
					SET @LookupListID=@ID							
				END
			END
		
			SELECT @TestID = @Unid
			
			SELECT TOP 1 @Unid = l.Unid, @ID = l.AnyID
			FROM @ListIDs l 
			WHERE l.Unid > @Unid
			ORDER BY l.Unid
		
		END
		
		IF(@LookupListID IS NULL) OR (@LookupListID=0)
		BEGIN
			--create a lookup list with the field name + _list
			INSERT INTO LookupList (LookupListName, LookupListDescription, ClientID, Enabled, SortOptionID)
			VALUES (RTRIM(LTRIM(@FieldName)) + '_List', RTRIM(LTRIM(@FieldName)) + '_List', @ClientID, 1, 23) -- specific sorting						
			
			SET @LookupListID = SCOPE_IDENTITY()
			
			--create the lookup list items from the tvp of strings	
			INSERT INTO LookupListItems (LookupListID, ItemValue, ClientID, Enabled, SortOrder)			
			SELECT @LookupListID, RTRIM(LTRIM(li.AnyValue)), @ClientID, 1, 1 FROM @ListItems li		
			
		END
		--add detail field
		INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView)
		VALUES (@ClientID, @DetailFieldSubTypeID, @FieldName,@FieldName,@QuestionTypeID,@Required,1,@LookupListID, @LeadTypeID,1,@DetailFieldPageID,1,0,'',NULL,1,NULL,@ValidationCriteriaFieldTypeID,@ValidationCriteriaID,@MinimumValue,@MaximumValue,@RegEx,@ErrorMessage,NULL,NULL,'','',1,0,0,0,0,0,0,0,1)
		
		SELECT @DetailFieldID = SCOPE_IDENTITY()
		
	END
	ELSE
	BEGIN
		-- Create a simple detail field
		INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView)
		VALUES (@ClientID, @DetailFieldSubTypeID, @FieldName,@FieldName,@QuestionTypeID,@Required,0,NULL,@LeadTypeID,1,@DetailFieldPageID,1,0,'',NULL,1,NULL,@ValidationCriteriaFieldTypeID,@ValidationCriteriaID,@MinimumValue,@MaximumValue,@RegEx,@ErrorMessage,NULL,NULL,'','',1,0,0,0,0,0,0,0,1)
		
		SELECT @DetailFieldID = SCOPE_IDENTITY()
	END
	
	SELECT @DetailFieldID DetailFieldID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__CreatePageAndField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__CreatePageAndField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__CreatePageAndField] TO [sp_executeall]
GO
