SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientOfficeLead table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOfficeLead_Update]
(

	@ClientOfficeLeadID int   ,

	@ClientID int   ,

	@ClientOfficeID int   ,

	@LeadID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientOfficeLead]
				SET
					[ClientID] = @ClientID
					,[ClientOfficeID] = @ClientOfficeID
					,[LeadID] = @LeadID
				WHERE
[ClientOfficeLeadID] = @ClientOfficeLeadID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeLead_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOfficeLead_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeLead_Update] TO [sp_executeall]
GO
