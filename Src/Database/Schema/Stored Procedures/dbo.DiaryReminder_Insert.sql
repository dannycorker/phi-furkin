SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DiaryReminder table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DiaryReminder_Insert]
(

	@DiaryReminderID int    OUTPUT,

	@ClientID int   ,

	@DiaryAppointmentID int   ,

	@ClientPersonnelID int   ,

	@ReminderTimeshiftID int   ,

	@ReminderDate datetime   
)
AS


				
				INSERT INTO [dbo].[DiaryReminder]
					(
					[ClientID]
					,[DiaryAppointmentID]
					,[ClientPersonnelID]
					,[ReminderTimeshiftID]
					,[ReminderDate]
					)
				VALUES
					(
					@ClientID
					,@DiaryAppointmentID
					,@ClientPersonnelID
					,@ReminderTimeshiftID
					,@ReminderDate
					)
				-- Get the identity value
				SET @DiaryReminderID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryReminder_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder_Insert] TO [sp_executeall]
GO
