SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the AquariumEventType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AquariumEventType_Insert]
(

	@AquariumEventTypeID int   ,

	@AquariumEventTypeName varchar (50)  ,

	@AquariumEventTypeDescription varchar (250)  ,

	@AquariumStatusAfterEvent int   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[AquariumEventType]
					(
					[AquariumEventTypeID]
					,[AquariumEventTypeName]
					,[AquariumEventTypeDescription]
					,[AquariumStatusAfterEvent]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@AquariumEventTypeID
					,@AquariumEventTypeName
					,@AquariumEventTypeDescription
					,@AquariumStatusAfterEvent
					,@WhenCreated
					,@WhenModified
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumEventType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumEventType_Insert] TO [sp_executeall]
GO
