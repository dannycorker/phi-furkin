SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CheckMateFieldGroup table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateFieldGroup_GetByCheckMateFieldGroupID]
(

	@CheckMateFieldGroupID int   
)
AS


				SELECT
					[CheckMateFieldGroupID],
					[GroupName],
					[GroupDescription]
				FROM
					[dbo].[CheckMateFieldGroup] WITH (NOLOCK) 
				WHERE
										[CheckMateFieldGroupID] = @CheckMateFieldGroupID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldGroup_GetByCheckMateFieldGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateFieldGroup_GetByCheckMateFieldGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldGroup_GetByCheckMateFieldGroupID] TO [sp_executeall]
GO
