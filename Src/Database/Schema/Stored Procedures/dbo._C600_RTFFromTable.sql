SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Robin Hall
-- Create date: 2014-11-25
-- Description:	Specialised RTFFromTable 
-- Modified:	2015-06-16	AH	Zen 32961 - Fix for £ issue
-- =============================================

CREATE PROCEDURE [dbo].[_C600_RTFFromTable]
(	
	@TableDetailFieldID INT,
	@WhereClause VARCHAR(MAX),
	@LeadID INT,
	@CaseID INT,
	@MatterID INT
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @RTF VARCHAR(MAX) = '', 
			@Items dbo.tvpVarchar,
			@ClaimCase INT
			
	/* Field-specific RTF Generation */
	
	-- Deduction text for settlement letter
	IF @TableDetailFieldID = 148349 
	BEGIN
	
		SELECT @RTF += 
			CASE WHEN @RTF <> '' THEN '\par ' ELSE '' END 
			+ REPLACE( tdvText.DetailValue , '£', '\''a3') /*AH Zendesk #32961*/
			+ '\par '
		FROM TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvText WITH (NOLOCK) on tdvText.TableRowID = r.TableRowID AND tdvText.DetailFieldID = 148348
		WHERE r.DetailFieldID = 148349 -- Deductions
		AND tdvText.MatterID = @MatterID
		ORDER by tdvText.TableRowID
		
		SELECT @RTF += CASE WHEN @RTF <> '' THEN '\par ' ELSE '' END 
	
	END

	/* Return the RTF */			
	
	SELECT @RTF AS MyHtml
	
	/* And print it for debuggers or nosey people */
	
	PRINT @RTF

END
                

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_RTFFromTable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_RTFFromTable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_RTFFromTable] TO [sp_executeall]
GO
