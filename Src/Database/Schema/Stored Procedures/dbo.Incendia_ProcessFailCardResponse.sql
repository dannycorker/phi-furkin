SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2010-10-28
-- Description:	Updated DFs based on the FAIL response from Incendia
-- =============================================
CREATE PROCEDURE [dbo].[Incendia_ProcessFailCardResponse]
	@ClientID INT, 
	@LeadEventID INT,
	@ErrorCode VARCHAR(50),
	@ErrorDescription VARCHAR(500)
AS
BEGIN
	
	-- GET Lead, Case and Matter
	DECLARE @CaseID INT
	DECLARE @LeadID INT
	
	SELECT @CaseID = CaseID, @LeadID = LeadID
	FROM dbo.LeadEvent WITH (NOLOCK) 
	WHERE LeadEventID = @LeadEventID
	
	DECLARE @MatterID INT
	
	SELECT TOP 1 @MatterID = MatterID
	FROM dbo.Matter WITH (NOLOCK) 
	INNER JOIN dbo.Cases WITH (NOLOCK) ON Matter.CaseID = Cases.CaseID
	WHERE Cases.CaseID = @CaseID
	ORDER BY Matter.MatterID
	


	DECLARE @DetailFieldID INT
		
	-- Error Code
	SELECT @DetailFieldID = 121312
	IF EXISTS (SELECT * FROM dbo.MatterDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND MatterID = @MatterID)
	BEGIN
		UPDATE MatterDetailValues
		SET DetailValue = @ErrorCode
		WHERE DetailFieldID = @DetailFieldID AND MatterID = @MatterID
	END
	ELSE
	BEGIN
		INSERT INTO MatterDetailValues
		(ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
		VALUES (@ClientID, @LeadID, @MatterID, @DetailFieldID, @ErrorCode)
	END
	
	-- Error Description
	SELECT @DetailFieldID = 121313
	IF EXISTS (SELECT * FROM dbo.MatterDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND MatterID = @MatterID)
	BEGIN
		UPDATE MatterDetailValues
		SET DetailValue = @ErrorDescription
		WHERE DetailFieldID = @DetailFieldID AND MatterID = @MatterID
	END
	ELSE
	BEGIN
		INSERT INTO MatterDetailValues
		(ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
		VALUES (@ClientID, @LeadID, @MatterID, @DetailFieldID, @ErrorDescription)
	END
		
	
	-- Card Order Result
	SELECT @DetailFieldID = 121310
	IF EXISTS (SELECT * FROM dbo.MatterDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND MatterID = @MatterID)
	BEGIN
		UPDATE MatterDetailValues
		SET DetailValue = 0
		WHERE DetailFieldID = @DetailFieldID AND MatterID = @MatterID
	END
	ELSE
	BEGIN
		INSERT INTO MatterDetailValues
		(ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
		VALUES (@ClientID, @LeadID, @MatterID, @DetailFieldID, 0)
	END
	
	-- Response Date
	SELECT @DetailFieldID = 121311
	IF EXISTS (SELECT * FROM dbo.MatterDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND MatterID = @MatterID)
	BEGIN
		UPDATE MatterDetailValues
		SET DetailValue = convert(varchar, dbo.fn_GetDate_Local(), 120) /* Format 120 looks like this: "2010-08-31 12:39:24" */
		WHERE DetailFieldID = @DetailFieldID AND MatterID = @MatterID
	END
	ELSE
	BEGIN
		INSERT INTO MatterDetailValues
		(ClientID, LeadID, MatterID, DetailFieldID, DetailValue)
		VALUES (@ClientID, @LeadID, @MatterID, @DetailFieldID, convert(varchar, dbo.fn_GetDate_Local(), 120)) /* Format 120 looks like this: "2010-08-31 12:39:24" */
	END
	
	-- Now update the status to order success
	SELECT @DetailFieldID = 117802
	UPDATE MatterDetailValues
	SET DetailValue = '25222'
	WHERE DetailFieldID = @DetailFieldID AND MatterID = @MatterID
	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Incendia_ProcessFailCardResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Incendia_ProcessFailCardResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Incendia_ProcessFailCardResponse] TO [sp_executeall]
GO
