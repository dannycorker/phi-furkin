SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green, Paul Richardson
-- Create date: 22 March 2010
-- Description:	Counts the targets in the given table detail field.
--				eg.,
--				ListOfNames = 'Plumber, Manchester Plumber'
--				Results:-
--
--				Plumber				, 10
--				Manchester Plumber	, 22
-- =============================================
CREATE PROCEDURE [dbo].[TableDetailValues__CountTargets]

@ListOfNames nvarchar(2000),
@DetailFieldID int

AS
BEGIN
	IF(@ListOfNames is not null)
	BEGIN
		SELECT tdv.DetailValue, COUNT(tdv.DetailValue) as CountOfKeyword
		FROM [dbo].[fnTableOfValuesFromCSV] (@ListOfNames) f  
		INNER JOIN dbo.TableDetailValues tdv WITH (nolock) on tdv.DetailFieldID = @DetailFieldID AND tdv.DetailValue = f.AnyValue 
		GROUP BY tdv.DetailValue 
	END
END




GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__CountTargets] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues__CountTargets] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__CountTargets] TO [sp_executeall]
GO
