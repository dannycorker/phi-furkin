SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ObjectLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectLink_Delete]
(

	@ObjectLinkID int   
)
AS


				DELETE FROM [dbo].[ObjectLink] WITH (ROWLOCK) 
				WHERE
					[ObjectLinkID] = @ObjectLinkID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectLink_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectLink_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectLink_Delete] TO [sp_executeall]
GO
