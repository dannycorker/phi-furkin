SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the CustomerInformationFieldDefinitions table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerInformationFieldDefinitions_Delete]
(

	@CustomerInformationFieldDefinitionID int   
)
AS


				DELETE FROM [dbo].[CustomerInformationFieldDefinitions] WITH (ROWLOCK) 
				WHERE
					[CustomerInformationFieldDefinitionID] = @CustomerInformationFieldDefinitionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerInformationFieldDefinitions_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerInformationFieldDefinitions_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerInformationFieldDefinitions_Delete] TO [sp_executeall]
GO
