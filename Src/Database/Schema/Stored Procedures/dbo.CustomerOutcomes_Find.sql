SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the CustomerOutcomes table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerOutcomes_Find]
(

	@SearchUsingOR bit   = null ,

	@CustomerOutcomeID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@CustomerID int   = null ,

	@OutcomeID int   = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CustomerOutcomeID]
	, [ClientQuestionnaireID]
	, [CustomerID]
	, [OutcomeID]
	, [ClientID]
    FROM
	[dbo].[CustomerOutcomes] WITH (NOLOCK) 
    WHERE 
	 ([CustomerOutcomeID] = @CustomerOutcomeID OR @CustomerOutcomeID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([OutcomeID] = @OutcomeID OR @OutcomeID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CustomerOutcomeID]
	, [ClientQuestionnaireID]
	, [CustomerID]
	, [OutcomeID]
	, [ClientID]
    FROM
	[dbo].[CustomerOutcomes] WITH (NOLOCK) 
    WHERE 
	 ([CustomerOutcomeID] = @CustomerOutcomeID AND @CustomerOutcomeID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([OutcomeID] = @OutcomeID AND @OutcomeID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOutcomes_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerOutcomes_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOutcomes_Find] TO [sp_executeall]
GO
