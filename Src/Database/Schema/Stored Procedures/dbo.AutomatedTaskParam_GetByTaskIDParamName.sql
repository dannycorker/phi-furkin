SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AutomatedTaskParam table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTaskParam_GetByTaskIDParamName]
(

	@TaskID int   ,

	@ParamName varchar (50)  
)
AS


				SELECT
					[AutomatedTaskParamID],
					[TaskID],
					[ClientID],
					[ParamName],
					[ParamValue]
				FROM
					[dbo].[AutomatedTaskParam] WITH (NOLOCK) 
				WHERE
										[TaskID] = @TaskID
					AND [ParamName] = @ParamName
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_GetByTaskIDParamName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskParam_GetByTaskIDParamName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_GetByTaskIDParamName] TO [sp_executeall]
GO
