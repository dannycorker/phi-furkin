SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 03-03-2016
-- Description:	Delete all script rule groups by script rule ID
-- =============================================
CREATE PROCEDURE [dbo].[ScriptRuleGroup__DeleteByScriptRuleID]

	@ScriptRuleID INT, 
	@ClientID INT

AS
BEGIN

	DELETE FROM ScriptRuleGroup
	WHERE ScriptRuleID=@ScriptRuleID AND ClientID=@ClientID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptRuleGroup__DeleteByScriptRuleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptRuleGroup__DeleteByScriptRuleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptRuleGroup__DeleteByScriptRuleID] TO [sp_executeall]
GO
