SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[AddOutcomeDelayedEmail]

@CustomerID int,
@ClientQuestionnaireID int,
@DateOutcomeEmailSent datetime, 
@ClientID int

as

insert into OutcomeDelayedEmails (CustomerID, ClientQuestionnaireID, DateOutcomeEmailSent, ClientID)
values (@CustomerID, @ClientQuestionnaireID, @DateOutcomeEmailSent, @ClientID)

declare @OutcomeDelayedEmailID int

set @OutcomeDelayedEmailID = SCOPE_IDENTITY()

Select OutcomeDelayedEmailID from OutcomeDelayedEmails where OutcomeDelayedEmailID = @OutcomeDelayedEmailID



GO
GRANT VIEW DEFINITION ON  [dbo].[AddOutcomeDelayedEmail] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddOutcomeDelayedEmail] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddOutcomeDelayedEmail] TO [sp_executeall]
GO
