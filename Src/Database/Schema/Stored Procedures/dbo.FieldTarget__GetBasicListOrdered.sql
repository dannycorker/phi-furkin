SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------
-- Date Created: 02 Novemeber 2011

-- Created By:  (Paul Richardson)
-- Purpose: Gets an order list of field targets
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[FieldTarget__GetBasicListOrdered]

AS

	SELECT * FROM [dbo].[FieldTarget] WITH (NOLOCK) ORDER BY ObjectName, PropertyName					

SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[FieldTarget__GetBasicListOrdered] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FieldTarget__GetBasicListOrdered] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FieldTarget__GetBasicListOrdered] TO [sp_executeall]
GO
