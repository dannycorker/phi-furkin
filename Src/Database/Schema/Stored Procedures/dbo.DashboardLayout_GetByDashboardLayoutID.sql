SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DashboardLayout table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DashboardLayout_GetByDashboardLayoutID]
(

	@DashboardLayoutID int   
)
AS


				SELECT
					[DashboardLayoutID],
					[DashboardLayoutName],
					[TopLeftDimensions],
					[TopMiddleDimensions],
					[TopRightDimensions],
					[BottomLeftDimensions],
					[BottomMiddleDimensions],
					[BottomRightDimensions]
				FROM
					[dbo].[DashboardLayout] WITH (NOLOCK) 
				WHERE
										[DashboardLayoutID] = @DashboardLayoutID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardLayout_GetByDashboardLayoutID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DashboardLayout_GetByDashboardLayoutID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DashboardLayout_GetByDashboardLayoutID] TO [sp_executeall]
GO
