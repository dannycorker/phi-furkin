SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		James Lewis 
-- Create date: 17 May 2019
-- Description:	Changes payment interval regardless of buisness rules
-- GPR 2020-05-22 updated for JIRA AAG-743
-- =============================================
CREATE PROCEDURE [dbo].[Billing_Support_ChangePaymentInterval]
(
		 @MatterID INT 
		,@PurchasedProductID INT
		,@Method INT
		,@AccountID INT
		,@Interval INT
)
AS
BEGIN
	SET NOCOUNT ON;

		DECLARE @PaymentDay INT 
		,@NumberofInstallments INT

		/*GPR 2020-05-22 for JIRA AAG-743 allow Interval to be passed in, use this to stamp the Interval and dictate the Number of Installments for the schedule*/
		IF @Interval = 69943 /*Monthly*/
		BEGIN
			
			EXEC _C00_SimpleValueIntoField 170176,69943,@MatterID /*Monthly*/
			SELECT @NumberofInstallments = 12

		END
		ELSE /*Annually*/
		BEGIN

			EXEC _C00_SimpleValueIntoField 170176,69943,@MatterID /*Annually*/
			SELECT @NumberofInstallments = 1

		END

		/*BACS*/ 
		IF @Method = 69930 
		BEGIN 

			EXEC _C00_SimpleValueIntoField 170114,69930,@MatterID
			/*GPR 2020-05-22 removed for AAG-743*/--EXEC _C00_SimpleValueIntoField 170176,69943,@MatterID
			SELECT @PaymentDay = dbo.fnGetSimpleDv(@MatterID, 170168)

			/*Payment Day, default to first of the month*/ 
			IF ISNULL(@PaymentDay,0) = 0 
			BEGIN 
				
				EXEC _C00_SimpleValueIntoField 170168,1,@MatterID
				SELECT @PaymentDay = 1

			END

			IF EXISTS (SELECT * FROM Account a with (NOLOCK) 
					   WHERE a.AccountTypeID = 2 
					   and a.AccountNumber > 1
					   and a.MaskedCardNumber IS NULL
					   and a.AccountID =@AccountID)
			BEGIN 

				Update Account 
				SET AccountTypeID = 1
				WHERE AccountID = @AccountID 

			END 

			UPDATE PurchasedProduct 
			SET AccountID = @AccountID, PaymentFrequencyID = 4, NumberOfInstallments = @NumberofInstallments /*GPR 2020-05-22 added variable AAG-743*/, PreferredPaymentDay = @PaymentDay, FirstPaymentDate = dbo.fnAddWorkingDays(dbo.fn_GetDate_Local(),10) 
			WHERE PurchasedProductID = @PurchasedProductID

			DELETE FROM PurchasedProductPaymentSchedule where PurchasedProductID = @PurchasedProductID 

			EXEC PurchasedProduct__CreatePaymentSchedule @PurchasedProductID , 0 

			Update p
			SET PaymentDate =  dbo.fnAddWorkingDays(dbo.fn_GetDate_Local(),10) , ActualCollectionDate =  dbo.fnAddWorkingDays(dbo.fn_GetDate_Local(),13) 
			FROM PurchasedProductPaymentSchedule p
			where PurchasedProductID = @PurchasedProductID 
			AND PaymentDate < dbo.fnAddWorkingDays(dbo.fn_GetDate_Local(),10) 


		END
		ELSE
		BEGIN 
			
			/*Payment Method to BACs*/ 
			EXEC _C00_SimpleValueIntoField 170114,69931,@MatterID

			/*Payment Interval to Monthly*/ 
			/*GPR 2020-05-22 removed for AAG-743*/ --EXEC _C00_SimpleValueIntoField 170176,69944,@MatterID

			UPDATE PurchasedProduct 
			SET PaymentFrequencyID = 5, NumberOfInstallments = @NumberofInstallments /*GPR 2020-05-22 added variable for AAG-743*/ , PreferredPaymentDay = 0, AccountID = @AccountID
			WHERE PurchasedProductID = @PurchasedProductID

			DELETE FROM PurchasedProductPaymentSchedule where PurchasedProductID = @PurchasedProductID 

			EXEC PurchasedProduct__CreatePaymentSchedule @PurchasedProductID , 0 


		END 
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[Billing_Support_ChangePaymentInterval] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Billing_Support_ChangePaymentInterval] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Billing_Support_ChangePaymentInterval] TO [sp_executeall]
GO
