SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DetailFieldStyle table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldStyle_Update]
(

	@DetailFieldStyleID int   ,

	@ClientID int   ,

	@StyleName nvarchar (50)  ,

	@BackgroundColor nvarchar (50)  ,

	@ForegroundColor nvarchar (50)  ,

	@FontSize nvarchar (50)  ,

	@FontWeight nvarchar (50)  ,

	@Align nvarchar (50)  ,

	@Padding nvarchar (50)  ,

	@PaddingHorizontal nvarchar (5)  ,

	@TextDecoration nvarchar (50)  ,

	@FontStyle nvarchar (50)  ,

	@PaddingVertical nvarchar (5)  ,

	@Enabled bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DetailFieldStyle]
				SET
					[ClientID] = @ClientID
					,[StyleName] = @StyleName
					,[BackgroundColor] = @BackgroundColor
					,[ForegroundColor] = @ForegroundColor
					,[FontSize] = @FontSize
					,[FontWeight] = @FontWeight
					,[Align] = @Align
					,[Padding] = @Padding
					,[PaddingHorizontal] = @PaddingHorizontal
					,[TextDecoration] = @TextDecoration
					,[FontStyle] = @FontStyle
					,[PaddingVertical] = @PaddingVertical
					,[Enabled] = @Enabled
				WHERE
[DetailFieldStyleID] = @DetailFieldStyleID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldStyle_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldStyle_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldStyle_Update] TO [sp_executeall]
GO
