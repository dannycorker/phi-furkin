SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Owen Ashcroft
-- Create date: 17/08/2018
-- Description:	Gets aggregator data
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GetAggregatorData]
	@Affinity int
AS
BEGIN
  select * from (select ResourceListId as id, FieldCaption as fc, COALESCE(ItemValue, DetailValue) as Value from (
    select rldv.ResourceListID, df.DetailFieldID, DetailValue, df.FieldCaption, li.LookupListItemID, li.ItemValue
    from ResourceListDetailValues rldv
    inner join DetailFields df on rldv.DetailFieldID = df.DetailFieldID
    left join LookupListItems li on rldv.ValueInt = li.LookupListItemID
    where ResourceListID in (select ResourceListID from ResourceListDetailValues where ValueInt = 76444)) as tble) as output

    PIVOT (
    min(value)
        for fc in([Brand],[Source],[Description],[Token Code],[Creation/Modified Date],[Tracking URL],[Campaign Code],[Call Centre Phone Number],[Aggregator Custom Policy Confirmation Message],[Suppress Saved Quote Email],[Logo Image Path],[Web Page URL],[Quote API Call Source],[Active or Inactive],[Channel])
    )p
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetAggregatorData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_GetAggregatorData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetAggregatorData] TO [sp_executeall]
GO
