SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PaymentCardType table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PaymentCardType_Find]
(

	@SearchUsingOR bit   = null ,

	@PaymentCardTypeID int   = null ,

	@PaymentCardType varchar (50)  = null ,

	@DisplayOrder int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PaymentCardTypeID]
	, [PaymentCardType]
	, [DisplayOrder]
    FROM
	[dbo].[PaymentCardType] WITH (NOLOCK) 
    WHERE 
	 ([PaymentCardTypeID] = @PaymentCardTypeID OR @PaymentCardTypeID IS NULL)
	AND ([PaymentCardType] = @PaymentCardType OR @PaymentCardType IS NULL)
	AND ([DisplayOrder] = @DisplayOrder OR @DisplayOrder IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PaymentCardTypeID]
	, [PaymentCardType]
	, [DisplayOrder]
    FROM
	[dbo].[PaymentCardType] WITH (NOLOCK) 
    WHERE 
	 ([PaymentCardTypeID] = @PaymentCardTypeID AND @PaymentCardTypeID is not null)
	OR ([PaymentCardType] = @PaymentCardType AND @PaymentCardType is not null)
	OR ([DisplayOrder] = @DisplayOrder AND @DisplayOrder is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentCardType_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentCardType_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentCardType_Find] TO [sp_executeall]
GO
