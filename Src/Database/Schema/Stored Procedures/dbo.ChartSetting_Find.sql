SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ChartSetting table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSetting_Find]
(

	@SearchUsingOR bit   = null ,

	@ChartSettingID int   = null ,

	@ChartID int   = null ,

	@SettingName varchar (100)  = null ,

	@SettingValue varchar (100)  = null ,

	@ClientID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ChartSettingID]
	, [ChartID]
	, [SettingName]
	, [SettingValue]
	, [ClientID]
    FROM
	[dbo].[ChartSetting] WITH (NOLOCK) 
    WHERE 
	 ([ChartSettingID] = @ChartSettingID OR @ChartSettingID IS NULL)
	AND ([ChartID] = @ChartID OR @ChartID IS NULL)
	AND ([SettingName] = @SettingName OR @SettingName IS NULL)
	AND ([SettingValue] = @SettingValue OR @SettingValue IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ChartSettingID]
	, [ChartID]
	, [SettingName]
	, [SettingValue]
	, [ClientID]
    FROM
	[dbo].[ChartSetting] WITH (NOLOCK) 
    WHERE 
	 ([ChartSettingID] = @ChartSettingID AND @ChartSettingID is not null)
	OR ([ChartID] = @ChartID AND @ChartID is not null)
	OR ([SettingName] = @SettingName AND @SettingName is not null)
	OR ([SettingValue] = @SettingValue AND @SettingValue is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSetting_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSetting_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSetting_Find] TO [sp_executeall]
GO
