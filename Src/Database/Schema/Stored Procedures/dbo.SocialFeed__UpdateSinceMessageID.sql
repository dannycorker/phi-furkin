SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 2015-11-17
-- Description:	Updates the social feeds since id
-- =============================================
CREATE PROCEDURE [dbo].[SocialFeed__UpdateSinceMessageID]
	@SocialFeedID INT,
	@SinceMessageID VARCHAR(250)
AS
BEGIN
		
	SET NOCOUNT ON;

	UPDATE SocialFeed
	SET SinceMessageID=@SinceMessageID
	WHERE SocialFeedID=@SocialFeedID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__UpdateSinceMessageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SocialFeed__UpdateSinceMessageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__UpdateSinceMessageID] TO [sp_executeall]
GO
