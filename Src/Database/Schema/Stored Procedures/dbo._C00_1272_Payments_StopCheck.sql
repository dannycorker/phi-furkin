SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- ============================================================
-- Author:		Simon Brushett
-- ALTER date: 2012-11-07
-- Description:	Marks the payment as stopped and creates a refund row for SAP
-- Used:		SAE
-- Modified:	2013-05-20 ROH Pass PaymentType to Approval Proc
--	2016-11-14 DCM Add reversal row to OutgoingPayment table
-- ============================================================

CREATE PROCEDURE [dbo].[_C00_1272_Payments_StopCheck]
(
	@MatterID INT
)

AS
BEGIN
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	
	DECLARE @TableRowID INT = 0
	SELECT @TableRowID = ValueInt
	FROM dbo.MatterDetailValues WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	AND DetailFieldID = 159559
	
	IF @TableRowID = 0 OR @TableRowID IS NULL
	BEGIN
		RAISERROR('No payment has been selected for stopping.',16,1)
		RETURN
	END

	-- Set the stopped date
	INSERT dbo.TableDetailValues (TableRowID, MatterID, ClientID, DetailFieldID, DetailValue)
	VALUES (@TableRowID, @MatterID, @ClientID, 159289, CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120))
	
	-- add a reversal row into the OutgoingPayment table
	INSERT INTO OutgoingPayment (ClientID, CustomerID, LeadEventID, ClaimObjectTypeID, ClaimObjectID, PolicyObjectTypeID, PolicyObjectID, PayeeTitle, PayeeFirstName, PayeeLastName, PayeeOrganisation, PayeeAddress1, PayeeAddress2, PayeeAddress3, PayeeAddress4, PayeeAddress5, PayeePostcode, PayeeCountryID, PayeeReference, PolicyholderReference, PaymentTypeID, PaymentAmount, PaymentDate, PaymentRunDate, DateCreated, OriginalPaymentReferrence, ChequeNumber, AccountNumber, Sortcode, MaskedCardNumber, PaymentSourceTypeID, PaymentStatusID, PaymentFileName, FailureCode, FailureReason, ParentOutgoingPaymentID)
	SELECT ClientID, CustomerID, LeadEventID, ClaimObjectTypeID, ClaimObjectID, PolicyObjectTypeID, PolicyObjectID, PayeeTitle, PayeeFirstName, PayeeLastName, PayeeOrganisation, PayeeAddress1, PayeeAddress2, PayeeAddress3, PayeeAddress4, PayeeAddress5, PayeePostcode, PayeeCountryID, PayeeReference, PolicyholderReference, PaymentTypeID, PaymentAmount, PaymentDate, PaymentRunDate, DateCreated, OriginalPaymentReferrence, ChequeNumber, AccountNumber, Sortcode, MaskedCardNumber, PaymentSourceTypeID, 4, '', FailureCode, 'Payment stopped', ParentOutgoingPaymentID 
	FROM OutgoingPayment op WITH (NOLOCK) 
	WHERE op.ClaimObjectID=@TableRowID
	
	DECLARE @Payments dbo.tvpIntMoney
	
	-- ALTER the cancellation row
	INSERT @Payments (ID, VALUE)
	SELECT 
	ISNULL(tdvPayTo.ValueInt, 44231) -- Default to PH (payments that were imported from SAP Claims)
	,tdvAmount.ValueMoney * -1
	FROM dbo.TableRows r WITH (NOLOCK) 
	LEFT JOIN dbo.TableDetailValues tdvPayTo WITH (NOLOCK) ON r.TableRowID = tdvPayTo.TableRowID AND tdvPayTo.DetailFieldID = 159408
	INNER JOIN dbo.TableDetailValues tdvAmount WITH (NOLOCK) ON r.TableRowID = tdvAmount.TableRowID AND tdvAmount.DetailFieldID = 154518
	WHERE r.TableRowID = @TableRowID
	AND r.MatterID = @MatterID
	
	EXEC _C00_1272_Payments_SaveSapPaymentData_ForApproval @MatterID, @Payments, '2' 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Payments_StopCheck] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1272_Payments_StopCheck] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1272_Payments_StopCheck] TO [sp_executeall]
GO
