SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DroppedOutCustomerMessages table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomerMessages_Find]
(

	@SearchUsingOR bit   = null ,

	@DroppedOutCustomerMessageID int   = null ,

	@ClientID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@EmailFromAddress varchar (255)  = null ,

	@EmailSubject varchar (500)  = null ,

	@Email varchar (MAX)  = null ,

	@SmsMessage varchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DroppedOutCustomerMessageID]
	, [ClientID]
	, [ClientQuestionnaireID]
	, [EmailFromAddress]
	, [EmailSubject]
	, [Email]
	, [SmsMessage]
    FROM
	[dbo].[DroppedOutCustomerMessages] WITH (NOLOCK) 
    WHERE 
	 ([DroppedOutCustomerMessageID] = @DroppedOutCustomerMessageID OR @DroppedOutCustomerMessageID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([EmailFromAddress] = @EmailFromAddress OR @EmailFromAddress IS NULL)
	AND ([EmailSubject] = @EmailSubject OR @EmailSubject IS NULL)
	AND ([Email] = @Email OR @Email IS NULL)
	AND ([SmsMessage] = @SmsMessage OR @SmsMessage IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DroppedOutCustomerMessageID]
	, [ClientID]
	, [ClientQuestionnaireID]
	, [EmailFromAddress]
	, [EmailSubject]
	, [Email]
	, [SmsMessage]
    FROM
	[dbo].[DroppedOutCustomerMessages] WITH (NOLOCK) 
    WHERE 
	 ([DroppedOutCustomerMessageID] = @DroppedOutCustomerMessageID AND @DroppedOutCustomerMessageID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([EmailFromAddress] = @EmailFromAddress AND @EmailFromAddress is not null)
	OR ([EmailSubject] = @EmailSubject AND @EmailSubject is not null)
	OR ([Email] = @Email AND @Email is not null)
	OR ([SmsMessage] = @SmsMessage AND @SmsMessage is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerMessages_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomerMessages_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomerMessages_Find] TO [sp_executeall]
GO
