SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the OutcomeDelayedEmails table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[OutcomeDelayedEmails_Delete]
(

	@OutcomeDelayedEmailID int   
)
AS


				DELETE FROM [dbo].[OutcomeDelayedEmails] WITH (ROWLOCK) 
				WHERE
					[OutcomeDelayedEmailID] = @OutcomeDelayedEmailID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeDelayedEmails_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutcomeDelayedEmails_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeDelayedEmails_Delete] TO [sp_executeall]
GO
