SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientAreaMailingLists table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientAreaMailingLists_GetByClientQuestionnaireID]
(

	@ClientQuestionnaireID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ClientAreaMailingListID],
					[Name],
					[Email],
					[PostCode],
					[OutcomeID],
					[YellowPagesAreaCode],
					[ClientID],
					[ClientQuestionnaireID],
					[OnHold]
				FROM
					[dbo].[ClientAreaMailingLists] WITH (NOLOCK) 
				WHERE
					[ClientQuestionnaireID] = @ClientQuestionnaireID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientAreaMailingLists_GetByClientQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientAreaMailingLists_GetByClientQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientAreaMailingLists_GetByClientQuestionnaireID] TO [sp_executeall]
GO
