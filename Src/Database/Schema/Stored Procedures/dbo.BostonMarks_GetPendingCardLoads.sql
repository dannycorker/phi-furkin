SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2010-11-10
-- Description:	Returns all pending card loads and any unacknowledged failures for boston marks clients
-- =============================================
CREATE PROCEDURE [dbo].[BostonMarks_GetPendingCardLoads] 
	@ClientID INT,
	@SubClientID INT
AS
BEGIN

	-- select table detail values in a format that can be processed by GetDetailFieldHelper.GetDetailFieldValues
	
	-- first select the field list
	SELECT f.DetailFieldID, f.QuestionTypeID
	FROM dbo.DetailFields f WITH (NOLOCK) 
	WHERE f.DetailFieldID = 117542
	
	-- now return empty record sets for simple fields, resource lists and table
	SELECT 1
	WHERE 1 = 2
	
	SELECT 1
	WHERE 1 = 2
	
	SELECT 1
	WHERE 1 = 2
	

	-- now we get our basic table
	SELECT f.DetailFieldID, t.DetailFieldID AS TableDetailFieldID, t.TableDetailValueID, t.DetailValue, t.TableRowID, 
			COALESCE(f1.Encrypt, 0) AS Encrypt, t.EncryptedValue, li.ItemValue AS LookupListValue,
			-- unlike the main __GetExpanded proc we also need the lead and matter ids
			t.LeadID, t.MatterID
	FROM
		dbo.DetailFields f 
		INNER JOIN dbo.TableRows r WITH (NOLOCK) ON r.DetailFieldID = f.DetailFieldID
		INNER JOIN dbo.TableDetailValues t WITH (NOLOCK) ON t.TableRowID = r.TableRowID
		INNER JOIN dbo.Matter m WITH (NOLOCK) ON t.MatterID = m.MatterID
		INNER JOIN dbo.Customers c WITH (NOLOCK) ON c.CustomerID = m.CustomerID  
		INNER JOIN dbo.DetailFields f1 WITH (NOLOCK) ON t.DetailFieldID = f1.DetailFieldID
		LEFT JOIN dbo.LookupList ll  WITH (NOLOCK) ON f1.LookupListID = ll.LookupListID
		LEFT JOIN dbo.LookupListItems li WITH (NOLOCK) ON ll.LookupListID = li.LookupListID AND t.ValueInt = li.LookupListItemID
	WHERE
		-- The load history field on the page
		f.DetailFieldID = 117542
		AND f.ClientID = @ClientID
		AND c.SubClientID = @SubClientID
		AND (c.Test = 0 OR c.Test IS NULL)
		AND f.Enabled = 1
		-- Limit to table rows that have not had a response or that have come back with a failure (but not acknowledged)
		AND r.TableRowID IN (
			SELECT r.TableRowID 
			FROM dbo.TableRows r WITH (NOLOCK) 
			INNER JOIN dbo.Matter m WITH (NOLOCK) ON r.MatterID = m.MatterID
			INNER JOIN dbo.Customers c WITH (NOLOCK) ON c.CustomerID = m.CustomerID  
			LEFT OUTER JOIN dbo.TableDetailValues t1 WITH (NOLOCK) ON t1.TableRowID = r.TableRowID AND t1.DetailFieldID = 117538 -- Date confirmed
			LEFT OUTER JOIN dbo.TableDetailValues t2 WITH (NOLOCK) ON t2.TableRowID = r.TableRowID AND t2.DetailFieldID = 117539 -- Error ID
			LEFT OUTER JOIN dbo.TableDetailValues t3 WITH (NOLOCK) ON t3.TableRowID = r.TableRowID AND t3.DetailFieldID = 117540 -- Error Description
			LEFT OUTER JOIN dbo.TableDetailValues t4 WITH (NOLOCK) ON t4.TableRowID = r.TableRowID AND t4.DetailFieldID = 122013 -- Failure acknowledged
			WHERE r.ClientID = @ClientID
			AND c.SubClientID = @SubClientID
			AND (c.Test = 0 OR c.Test IS NULL)
			AND(ISNULL(t1.DetailValue, '') = '' OR
				(ISNULL(t1.DetailValue, '') != '' AND 
					(ISNULL(t2.DetailValue, '') != '' OR ISNULL(t3.DetailValue, '') != '') AND ISNULL(t4.DetailValue, '') = ''))
		)
		
	ORDER BY f.DetailFieldID, f.FieldOrder, t.TableRowID, f1.FieldOrder
END




GO
GRANT VIEW DEFINITION ON  [dbo].[BostonMarks_GetPendingCardLoads] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BostonMarks_GetPendingCardLoads] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BostonMarks_GetPendingCardLoads] TO [sp_executeall]
GO
