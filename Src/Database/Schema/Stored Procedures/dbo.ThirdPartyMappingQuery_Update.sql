SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ThirdPartyMappingQuery table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyMappingQuery_Update]
(

	@ThirdPartyMappingQueryID int   ,

	@ClientID int   ,

	@ThirdPartySystemID int   ,

	@ThirdPartyFieldGroupID int   ,

	@QueryID int   ,

	@Method int   ,

	@ActionURI varchar (2000)  ,

	@SchemaName varchar (250)  ,

	@BaseIndex varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ThirdPartyMappingQuery]
				SET
					[ClientID] = @ClientID
					,[ThirdPartySystemID] = @ThirdPartySystemID
					,[ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID
					,[QueryID] = @QueryID
					,[Method] = @Method
					,[ActionURI] = @ActionURI
					,[SchemaName] = @SchemaName
					,[BaseIndex] = @BaseIndex
				WHERE
[ThirdPartyMappingQueryID] = @ThirdPartyMappingQueryID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingQuery_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyMappingQuery_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyMappingQuery_Update] TO [sp_executeall]
GO
