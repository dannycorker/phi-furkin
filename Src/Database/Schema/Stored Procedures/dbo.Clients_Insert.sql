SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Clients table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Clients_Insert]
(

	@ClientID int    OUTPUT,

	@CompanyName varchar (100)  ,

	@WebAddress varchar (255)  ,

	@IPAddress varchar (50)  ,

	@DefaultEmailAddress varchar (255)  ,

	@ClientTypeID int   ,

	@AllowSMS bit   ,

	@SecurityCode varchar (36)  ,

	@LeadsBelongToOffices bit   ,

	@UseEventCosts bit   ,

	@UseEventUOEs bit   ,

	@UseEventDisbursements bit   ,

	@UseEventComments bit   ,

	@VerifyAddress bit   ,

	@UseTapi bit   ,

	@UseRPI bit   ,

	@UseGBAddress bit   ,

	@UsePinpoint bit   ,

	@UseSage bit   ,

	@UseSAS bit   ,

	@UseCreditCalculation bit   ,

	@FollowupWorkingDaysOnly bit   ,

	@AddLeadByQuestionnaire bit   ,

	@UseIncendia bit   ,

	@LanguageID int   ,

	@CountryID int   ,

	@UseMobileInterface bit   ,

	@UseGBValidation bit   ,

	@CurrencyID int   ,

	@AllowSmsCommandProcessing bit   
)
AS


				
				INSERT INTO [dbo].[Clients]
					(
					[CompanyName]
					,[WebAddress]
					,[IPAddress]
					,[DefaultEmailAddress]
					,[ClientTypeID]
					,[AllowSMS]
					,[SecurityCode]
					,[LeadsBelongToOffices]
					,[UseEventCosts]
					,[UseEventUOEs]
					,[UseEventDisbursements]
					,[UseEventComments]
					,[VerifyAddress]
					,[UseTapi]
					,[UseRPI]
					,[UseGBAddress]
					,[UsePinpoint]
					,[UseSage]
					,[UseSAS]
					,[UseCreditCalculation]
					,[FollowupWorkingDaysOnly]
					,[AddLeadByQuestionnaire]
					,[UseIncendia]
					,[LanguageID]
					,[CountryID]
					,[UseMobileInterface]
					,[UseGBValidation]
					,[CurrencyID]
					,[AllowSmsCommandProcessing]
					)
				VALUES
					(
					@CompanyName
					,@WebAddress
					,@IPAddress
					,@DefaultEmailAddress
					,@ClientTypeID
					,@AllowSMS
					,@SecurityCode
					,@LeadsBelongToOffices
					,@UseEventCosts
					,@UseEventUOEs
					,@UseEventDisbursements
					,@UseEventComments
					,@VerifyAddress
					,@UseTapi
					,@UseRPI
					,@UseGBAddress
					,@UsePinpoint
					,@UseSage
					,@UseSAS
					,@UseCreditCalculation
					,@FollowupWorkingDaysOnly
					,@AddLeadByQuestionnaire
					,@UseIncendia
					,@LanguageID
					,@CountryID
					,@UseMobileInterface
					,@UseGBValidation
					,@CurrencyID
					,@AllowSmsCommandProcessing
					)
				-- Get the identity value
				SET @ClientID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Clients_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Clients_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Clients_Insert] TO [sp_executeall]
GO
