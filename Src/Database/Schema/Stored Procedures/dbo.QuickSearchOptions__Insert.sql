SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-07-01
-- Description:	Insert into QuickSearchOptions until the DAL has been built..
-- =============================================
CREATE PROCEDURE [dbo].[QuickSearchOptions__Insert]
	@ClientID INT, 
	@ClientPersonnelAdminGroupID INT, 
	@DropDownCaption VARCHAR(50), 
	@DropDownCode VARCHAR(5), 
	@DetailFieldID INT = NULL, 
	@SortOrder INT
AS
BEGIN

	SET NOCOUNT ON;

	/*
		SP Used to populate the QuickSearchOptions table
		Standard DDL options in the app are (Caption/Code):
		"Last name", "ln"
		"Business name", "bn"
		helper.PostcodeText, "pc"
		"Phone number", "pn"
		customerCaption + " ID", "cu"
		caseCaption + " ID", "aid"
		matterCaption + " ID", "mid"
		globalLeadAlias + " Ref", "lr"
		matterCaption + " Ref", "mr"
		"Email address", "ea"
		"LeadID", "lid"
	*/

	INSERT INTO QuickSearchOptions (ClientID ,ClientPersonnelAdminGroupID ,DropDownCaption ,DropDownCode ,DetailFieldID ,SortOrder)
	VALUES (@ClientID, @ClientPersonnelAdminGroupID, @DropDownCaption, @DropDownCode, @DetailFieldID, @SortOrder)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[QuickSearchOptions__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuickSearchOptions__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuickSearchOptions__Insert] TO [sp_executeall]
GO
