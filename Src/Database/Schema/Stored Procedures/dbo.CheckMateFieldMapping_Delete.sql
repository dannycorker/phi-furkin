SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the CheckMateFieldMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateFieldMapping_Delete]
(

	@CheckMateFieldMappingID int   
)
AS


				DELETE FROM [dbo].[CheckMateFieldMapping] WITH (ROWLOCK) 
				WHERE
					[CheckMateFieldMappingID] = @CheckMateFieldMappingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldMapping_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateFieldMapping_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldMapping_Delete] TO [sp_executeall]
GO
