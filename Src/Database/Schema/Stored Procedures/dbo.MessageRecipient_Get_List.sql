SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the MessageRecipient table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MessageRecipient_Get_List]

AS


				
				SELECT
					[MessageRecipientID],
					[MessageID],
					[ClientPersonnelID],
					[PortalUserID],
					[ClientID]
				FROM
					[dbo].[MessageRecipient] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MessageRecipient_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MessageRecipient_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MessageRecipient_Get_List] TO [sp_executeall]
GO
