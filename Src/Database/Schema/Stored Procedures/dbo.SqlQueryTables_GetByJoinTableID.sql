SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryTables table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryTables_GetByJoinTableID]
(

	@JoinTableID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[SqlQueryTableID],
					[ClientID],
					[SqlQueryID],
					[SqlQueryTableName],
					[TableAlias],
					[TableDisplayOrder],
					[JoinType],
					[JoinText],
					[JoinTableID],
					[JoinRTRID],
					[TempTableID],
					[TempJoinTableID],
					[SourceID]
				FROM
					[dbo].[SqlQueryTables] WITH (NOLOCK) 
				WHERE
					[JoinTableID] = @JoinTableID
				
				SELECT @@ROWCOUNT
			




GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTables_GetByJoinTableID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTables_GetByJoinTableID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTables_GetByJoinTableID] TO [sp_executeall]
GO
