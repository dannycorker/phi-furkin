SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-20
-- Description:	Get Equation tasks for the scheduler
-- =============================================
CREATE PROCEDURE [dbo].[EquationTasks__UpdateEquationTask]
	@EquationTaskID INT,
	@Outcome VARCHAR(50)
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE TOP (1) EquationTasks
	SET TimeToComplete = DATEDIFF(SS, e.LockDateTime, dbo.fn_GetDate_Local()), LockDateTime = NULL, Outcome = @Outcome, WhenCompleted = dbo.fn_GetDate_Local()
	FROM EquationTasks e WITH (HOLDLOCK)
	WHERE e.EquationTaskID = @EquationTaskID

	UPDATE Lead 
	SET RecalculateEquations = 0
	FROM Lead 
	INNER JOIN EquationTasks e ON e.LeadID = Lead.LeadID
	WHERE e.EquationTaskID = @EquationTaskID
	AND NOT EXISTS (
		SELECT *
		FROM EquationTasks et
		WHERE et.LeadID = e.LeadID
		AND et.WhenCompleted IS NULL
	)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTasks__UpdateEquationTask] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EquationTasks__UpdateEquationTask] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EquationTasks__UpdateEquationTask] TO [sp_executeall]
GO
