SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SMSOutgoingPhoneNumber table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SMSOutgoingPhoneNumber_Get_List]

AS


				
				SELECT
					[SMSOutgoingPhoneNumberID],
					[ClientID],
					[SMSQuestionnaireID],
					[LeadTypeID],
					[OutgoingPhoneNumber],
					[SMSSurveyStartEventTypeID],
					[MMSLetterInEvent],
					[SmsGatewayID],
					[HasIntroductionText],
					[IntroductionTextFrom],
					[IntroductionText],
					[DelayInSeconds],
					[IntroductionTextEventTypeID],
					[IntroductionTextDocumentTypeID],
					[NoCLIRejectText]
				FROM
					[dbo].[SMSOutgoingPhoneNumber] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSOutgoingPhoneNumber_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber_Get_List] TO [sp_executeall]
GO
