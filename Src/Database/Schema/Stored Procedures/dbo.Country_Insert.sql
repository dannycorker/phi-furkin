SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Country table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Country_Insert]
(

	@CountryID int   ,

	@CountryName varchar (255)  ,

	@CountryNamePlain varchar (255)  ,

	@Alpha2Code char (2)  ,

	@Alpha3Code char (3)  ,

	@NumericCode smallint   ,

	@ISO3166Dash2Code char (13)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   ,

	@UsePostcode bit   ,

	@PostcodeRegex varchar (500)  ,

	@DiallingCode varchar (4)  
)
AS


				
				INSERT INTO [dbo].[Country]
					(
					[CountryID]
					,[CountryName]
					,[CountryNamePlain]
					,[Alpha2Code]
					,[Alpha3Code]
					,[NumericCode]
					,[ISO3166Dash2Code]
					,[WhenCreated]
					,[WhenModified]
					,[UsePostcode]
					,[PostcodeRegex]
					,[DiallingCode]
					)
				VALUES
					(
					@CountryID
					,@CountryName
					,@CountryNamePlain
					,@Alpha2Code
					,@Alpha3Code
					,@NumericCode
					,@ISO3166Dash2Code
					,@WhenCreated
					,@WhenModified
					,@UsePostcode
					,@PostcodeRegex
					,@DiallingCode
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Country_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Country_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Country_Insert] TO [sp_executeall]
GO
