SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the SubQueryLinkage table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SubQueryLinkage_Delete]
(

	@SubQueryLinkageID int   
)
AS


				DELETE FROM [dbo].[SubQueryLinkage] WITH (ROWLOCK) 
				WHERE
					[SubQueryLinkageID] = @SubQueryLinkageID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SubQueryLinkage_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SubQueryLinkage_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SubQueryLinkage_Delete] TO [sp_executeall]
GO
