SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the AutoAdjudication table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudication_GetByAutoAdjudicationID]
(

	@AutoAdjudicationID int   
)
AS


				SELECT
					[AutoAdjudicationID],
					[EventChoiceID],
					[AutoAdjudicationTypeID],
					[ObjectID],
					[AutoAdjudicationFieldTypeID],
					[Comparison],
					[Value1],
					[Value2],
					[AppliesTo],
					[Count],
					[ValueTypeID]
				FROM
					[dbo].[AutoAdjudication] WITH (NOLOCK) 
				WHERE
										[AutoAdjudicationID] = @AutoAdjudicationID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudication_GetByAutoAdjudicationID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudication_GetByAutoAdjudicationID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudication_GetByAutoAdjudicationID] TO [sp_executeall]
GO
