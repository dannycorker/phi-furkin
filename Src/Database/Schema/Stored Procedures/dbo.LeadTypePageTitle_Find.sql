SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LeadTypePageTitle table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitle_Find]
(

	@SearchUsingOR bit   = null ,

	@LeadTypeID int   = null ,

	@PageTitle varchar (100)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LeadTypeID]
	, [PageTitle]
    FROM
	[dbo].[LeadTypePageTitle] WITH (NOLOCK) 
    WHERE 
	 ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([PageTitle] = @PageTitle OR @PageTitle IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LeadTypeID]
	, [PageTitle]
    FROM
	[dbo].[LeadTypePageTitle] WITH (NOLOCK) 
    WHERE 
	 ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([PageTitle] = @PageTitle AND @PageTitle is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitle_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitle_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitle_Find] TO [sp_executeall]
GO
