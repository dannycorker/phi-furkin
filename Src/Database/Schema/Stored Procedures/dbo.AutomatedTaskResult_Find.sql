SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the AutomatedTaskResult table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutomatedTaskResult_Find]
(

	@SearchUsingOR bit   = null ,

	@AutomatedTaskResultID int   = null ,

	@TaskID int   = null ,

	@ClientID int   = null ,

	@RunDate datetime   = null ,

	@Description varchar (2500)  = null ,

	@RecordCount int   = null ,

	@Complete bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [AutomatedTaskResultID]
	, [TaskID]
	, [ClientID]
	, [RunDate]
	, [Description]
	, [RecordCount]
	, [Complete]
    FROM
	[dbo].[AutomatedTaskResult] WITH (NOLOCK) 
    WHERE 
	 ([AutomatedTaskResultID] = @AutomatedTaskResultID OR @AutomatedTaskResultID IS NULL)
	AND ([TaskID] = @TaskID OR @TaskID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([RunDate] = @RunDate OR @RunDate IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
	AND ([RecordCount] = @RecordCount OR @RecordCount IS NULL)
	AND ([Complete] = @Complete OR @Complete IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [AutomatedTaskResultID]
	, [TaskID]
	, [ClientID]
	, [RunDate]
	, [Description]
	, [RecordCount]
	, [Complete]
    FROM
	[dbo].[AutomatedTaskResult] WITH (NOLOCK) 
    WHERE 
	 ([AutomatedTaskResultID] = @AutomatedTaskResultID AND @AutomatedTaskResultID is not null)
	OR ([TaskID] = @TaskID AND @TaskID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([RunDate] = @RunDate AND @RunDate is not null)
	OR ([Description] = @Description AND @Description is not null)
	OR ([RecordCount] = @RecordCount AND @RecordCount is not null)
	OR ([Complete] = @Complete AND @Complete is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskResult_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskResult_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskResult_Find] TO [sp_executeall]
GO
