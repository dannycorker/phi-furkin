SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the QuestionnaireParts table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireParts_Get_List]

AS


				
				SELECT
					[PartNameID],
					[PartName]
				FROM
					[dbo].[QuestionnaireParts] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireParts_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireParts_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireParts_Get_List] TO [sp_executeall]
GO
