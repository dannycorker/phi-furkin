SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Country table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Country_GetByAlpha2Code]
(

	@Alpha2Code char (2)  
)
AS


				SELECT
					[CountryID],
					[CountryName],
					[CountryNamePlain],
					[Alpha2Code],
					[Alpha3Code],
					[NumericCode],
					[ISO3166Dash2Code],
					[WhenCreated],
					[WhenModified],
					[UsePostcode],
					[PostcodeRegex],
					[DiallingCode]
				FROM
					[dbo].[Country] WITH (NOLOCK) 
				WHERE
										[Alpha2Code] = @Alpha2Code
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Country_GetByAlpha2Code] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Country_GetByAlpha2Code] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Country_GetByAlpha2Code] TO [sp_executeall]
GO
