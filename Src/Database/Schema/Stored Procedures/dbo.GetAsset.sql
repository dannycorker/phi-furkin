SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Austin Davies
-- Create date: 2013-08-01
-- Description:	Asset
-- =============================================
CREATE PROCEDURE [dbo].[GetAsset] 
	@ClientPersonnelID INT = NULL,
	@AssetID INT = NULL
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		a.[AssetID],
		a.[ClientID],
		a.[AssetTypeID],
		a.[AssetSubTypeID],
		a.[LocationID],
		a.[AssetName],
		a.[Note],
		a.[SecureNote],
		a.[SecureUserName],
		a.[SecurePassword],
		a.[SecureOther],
		a.[Version],
		a.[ValidFrom],
		a.[ValidTo],
		a.[Enabled],
		a.[Deleted],
		a.[WhoCreated],
		a.[WhenCreated],
		a.[WhoModified],
		a.[WhenCreated]
	FROM 
		[dbo].[Asset] a WITH (NOLOCK)
		INNER JOIN [dbo].[ClientPersonnel] cp WITH (NOLOCK) ON cp.[ClientID] = a.[ClientID]
	WHERE 
		@ClientPersonnelID = cp.[ClientPersonnelID]
		AND @AssetID = a.[AssetID]
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[GetAsset] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetAsset] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetAsset] TO [sp_executeall]
GO
