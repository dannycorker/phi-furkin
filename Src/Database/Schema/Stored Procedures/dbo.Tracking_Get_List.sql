SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Tracking table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Tracking_Get_List]

AS


				
				SELECT
					[TrackingID],
					[TrackingName],
					[TrackingDescription],
					[StartDate],
					[EndDate],
					[ClientQuestionnaireID],
					[ClientID]
				FROM
					[dbo].[Tracking] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Tracking_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Tracking_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Tracking_Get_List] TO [sp_executeall]
GO
