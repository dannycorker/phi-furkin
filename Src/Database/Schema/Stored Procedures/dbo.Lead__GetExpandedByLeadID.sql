SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2016-02-16
-- Description:	Get a single lead with lookups expanded
-- =============================================
CREATE PROCEDURE [dbo].[Lead__GetExpandedByLeadID]
(
	@LeadID INT
)
AS
BEGIN

	SELECT l.*, a.AquariumStatusName, s.StatusName, lt.LeadTypeName, cpTo.UserName AS AssignedToName, cpBy.UserName AS AssignedByName
	FROM dbo.Lead l WITH (NOLOCK) 
	INNER JOIN dbo.AquariumStatus a WITH (NOLOCK) ON l.AquariumStatusID = a.AquariumStatusID
	LEFT JOIN dbo.LeadStatus s WITH (NOLOCK) ON l.ClientStatusID = s.StatusID
	INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON l.LeadTypeID = lt.LeadTypeID
	LEFT JOIN dbo.ClientPersonnel cpTo WITH (NOLOCK) ON l.AssignedTo = cpTo.ClientPersonnelID
	LEFT JOIN dbo.ClientPersonnel cpBy WITH (NOLOCK) ON l.AssignedBy = cpBy.ClientPersonnelID
	WHERE l.LeadID = @LeadID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead__GetExpandedByLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead__GetExpandedByLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead__GetExpandedByLeadID] TO [sp_executeall]
GO
