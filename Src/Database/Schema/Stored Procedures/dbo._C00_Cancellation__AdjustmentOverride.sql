SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-09-22
-- Description:	Determines whether there should be any business override on a cancellation adjustment
--				Override Type 0 = no override, 1 = no refund, 2 = full refund, 3 = Custom Refund
-- Mods
--	2016-12-16 DCM Added @adjustmentdate for cooling off period (instead of dbo.fn_GetDate_Local())
--	2017-01-12 DCM removed special treatment for pet relocated & pet deceased
--	2020-08-20 ALM | Updated for US Cancellation Logic for PPET-110
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Cancellation__AdjustmentOverride]
(
	@PurchasedProductID INT,
	@AdjustmentDate DATE
)
AS
BEGIN

--declare @PurchasedProductID INT=2137


	SET NOCOUNT ON;

	DECLARE	@AdjustmentOverride INT=0,
			@AdjustmentComment VARCHAR(500),
			@AdjustmentGross MONEY,
			@AdjustmentTax MONEY,
			@AdjustmentNet MONEY,
			@CaseID INT,
			@ClientID INT,
			@CustomerID INT,
			@MatterID INT,
			@ObjectID INT,
			@ReturnDFID INT,
			@ReturnString VARCHAR(1000),
			@Overrides dbo.tvpIntVarcharVarchar,
			@CancellationReason INT,
			@StartDate DATE,
			@State VARCHAR(10),
			@CancellationType INT,
			@DaysIntoPolicyYear INT,
			@IsRenewal BIT,
			@CoolingOffPeriod INT,
			@CountryID INT
			
	
	SELECT @ClientID=pp.ClientID, @ObjectID=pp.ObjectID, @CountryID = cu.CountryID
	FROM PurchasedProduct pp WITH (NOLOCK) 
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = pp.ObjectID
	INNER JOIN Customers cu WITH (NOLOCK) ON cu.CustomerID = m.CustomerID
	WHERE pp.PurchasedProductID=@PurchasedProductID

	SELECT @ReturnDFID=dbo.fnGetDetailFieldIDFromThirdPartyIDs (@ClientID, 1492, 105, 4521)

	/*2020-08-20 ALM PPET-110*/
	IF @CountryID = 233 /*United States*/
	BEGIN

		SELECT @CancellationReason = dbo.fnGetSimpleDvAsInt(170054,@MatterID),
				@StartDate = dbo.fnGetSimpleDvAsDate(170036,@MatterID)

		SELECT @State = cu.County 
		FROM Customers cu WITH (NOLOCK) 
		WHERE cu.CustomerID = @CustomerID 
	
		SELECT @CancellationType = 
		CASE WHEN @CancellationReason IN (69907,69911,72031,72032,72033,72035,72037,72038,74421,74519,74521,74523,74525,75135,75136,75141) THEN 1 
		ELSE 2 END
	
		SELECT @DaysIntoPolicyYear = DATEDIFF(DD,@StartDate,dbo.fn_GetDate_Local())
	
		SELECT @IsRenewal = 
		CASE 
			WHEN COUNT (*) > 1 THEN 1 
			ELSE 0 
		END 
		FROM TableRows tr WITH (NOLOCK) 
		WHERE tr.DetailFieldID = 170033
		AND tr.MatterID = @MatterID
		
		SELECT @CoolingOffPeriod = [dbo].[fn_C00_USCancellationCoolingOffPeriod] (@State, @CancellationType, @IsRenewal, @DaysIntoPolicyYear)

		SELECT @AdjustmentOverride= CASE WHEN @CoolingOffPeriod IS NOT NULL THEN
									CASE WHEN DATEADD(DAY,@CoolingOffPeriod,pp.ProductPurchasedOnDate) >= @AdjustmentDate THEN 2 ELSE 0 END
									ELSE 0 END
		FROM PurchasedProduct pp WITH (NOLOCK) 
		LEFT JOIN BillingConfiguration bc WITH (NOLOCK) ON pp.ClientID=bc.ClientID
		WHERE PurchasedProductID=@PurchasedProductID

	END
	ELSE
	BEGIN

	-- in cooling off period?
		SELECT @AdjustmentOverride= CASE WHEN bc.CoolingOffPeriod IS NOT NULL THEN
									CASE WHEN DATEADD(DAY,bc.CoolingOffPeriod,pp.ProductPurchasedOnDate) >= @AdjustmentDate THEN 2 ELSE 0 END
									ELSE 0 END
		FROM PurchasedProduct pp WITH (NOLOCK) 
		LEFT JOIN BillingConfiguration bc WITH (NOLOCK) ON pp.ClientID=bc.ClientID
		WHERE PurchasedProductID=@PurchasedProductID

	END
	
	IF @AdjustmentOverride=2 
		SELECT @AdjustmentComment='In cooling off period'
	
	-- additional client specific rules
	IF @AdjustmentOverride=0
	BEGIN
	
		IF @ClientID=384
		BEGIN

			SELECT @CustomerID=m.CustomerID,@CaseID=m.CaseID, @MatterID=m.MatterID 
			FROM PurchasedProduct pp
			INNER JOIN Matter m WITH (NOLOCK) ON pp.ObjectID=m.MatterID
			WHERE pp.PurchasedProductID=@PurchasedProductID
			
			-- Custom adjustment?
			SELECT @AdjustmentGross=ISNULL(dbo.fnGetDvAsMoney(175451,@CaseID),0)
			IF @AdjustmentGross<>0
			BEGIN
			
				SELECT @AdjustmentComment=ISNULL(dbo.fnGetDv(177274,@CaseID),''),
						@AdjustmentOverride=3,
						@AdjustmentNet=ROUND(@AdjustmentGross/dbo.fn_C00_1273_RulesEngine_GetIPT(@CustomerID,NULL,NULL,NULL,@Overrides),2)
				SELECT @AdjustmentTax=@AdjustmentGross-@AdjustmentNet		
			
			END
						
			IF @AdjustmentOverride=0
			BEGIN
			
				-- special treatment for Pet deceased and pet relocated has been removed at PP's request
				SELECT @AdjustmentOverride=0
				/* -- is cancellation reason deceased pet or relocated pet
				SELECT @AdjustmentOverride=CASE WHEN reason.ValueInt IN (69911,74421) THEN 1 ELSE 0 END,
						@AdjustmentComment=CASE reason.ValueInt
							WHEN 69911 THEN 'Pet deceased'
							WHEN 74421 THEN 'Pet relocated'
							ELSE '' END
				FROM PurchasedProduct pp WITH (NOLOCK)
				INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID=pp.ObjectID
				INNER JOIN MatterDetailValues reason WITH (NOLOCK) ON m.MatterID=reason.MatterID AND reason.DetailFieldID=170054
				WHERE pp.PurchasedProductID=@PurchasedProductID	
				*/
			END

			---- has there been a new claim paid out in this this policy year
			--SELECT @AdjustmentOverride= CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END
			--FROM PurchasedProduct pp 
			--INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID=pp.ObjectID
			--INNER JOIN MatterDetailValues sd WITH (NOLOCK) ON m.MatterID=sd.MatterID AND sd.DetailFieldID=170036
			--INNER JOIN MatterDetailValues td WITH (NOLOCK) ON m.MatterID=td.MatterID AND td.DetailFieldID=170037
			--INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.FromLeadID=m.LeadID
			--INNER JOIN Cases ca WITH (NOLOCK) ON ltr.ToLeadID=ca.LeadID AND ca.ClientStatusID=4470
			--INNER JOIN LeadEvent le WITH (NOLOCK) ON ca.CaseID=le.CaseID AND le.EventDeleted=0
			--INNER JOIN EventType et WITH (NOLOCK) ON le.EventTypeID=et.EventTypeID AND et.StatusAfterEvent=ca.ClientStatusID 
			---- only claims of type 'new'
			--INNER JOIN Matter m2 WITH (NOLOCK) ON ca.CaseID=m2.CaseID
			--INNER JOIN MatterDetailValues ctype WITH (NOLOCK) ON m2.MatterID=ctype.MatterID AND ctype.DetailFieldID=144483
			--WHERE ltr.ToLeadTypeID=1490
			--AND le.WhenCreated BETWEEN sd.ValueDate AND td.ValueDate 
			--AND ctype.ValueInt=44356 -- new
			
			IF @AdjustmentOverride=0
			BEGIN
			
				-- a new claim has been started in this policy year
				SELECT @AdjustmentOverride= CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END,
						@AdjustmentComment= CASE WHEN COUNT(*) > 0 THEN 'Claim has been made' ELSE '' END
				FROM PurchasedProduct pp 
				INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID=pp.ObjectID
				INNER JOIN MatterDetailValues sd WITH (NOLOCK) ON m.MatterID=sd.MatterID AND sd.DetailFieldID=170036
				INNER JOIN MatterDetailValues td WITH (NOLOCK) ON m.MatterID=td.MatterID AND td.DetailFieldID=170037
				INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.FromLeadID=m.LeadID
				INNER JOIN Cases ca WITH (NOLOCK) ON ltr.ToLeadID=ca.LeadID
				INNER JOIN LeadEvent le WITH (NOLOCK) ON ca.CaseID=le.CaseID AND le.EventDeleted=0 AND le.EventTypeID=150086
				-- only claims of type 'new'
				INNER JOIN Matter m2 WITH (NOLOCK) ON ca.CaseID=m2.CaseID
				INNER JOIN MatterDetailValues ctype WITH (NOLOCK) ON m2.MatterID=ctype.MatterID AND ctype.DetailFieldID=144483
				WHERE ltr.ToLeadTypeID=1490
				AND le.WhenCreated BETWEEN sd.ValueDate AND td.ValueDate 
				AND ctype.ValueInt=44356 -- new	
				AND pp.PurchasedProductID=@PurchasedProductID		
				
			END
		
			EXEC _C00_SimpleValueIntoField 175448, @AdjustmentComment, @MatterID 
		
		END
	
	END

	SELECT @ReturnString = CONVERT(VARCHAR(2),@AdjustmentOverride) + ',' +
			ISNULL(CONVERT(VARCHAR(500),@AdjustmentComment),' ') + ',' +
			ISNULL(CONVERT(VARCHAR(20),@AdjustmentGross),0) + ',' +
			ISNULL(CONVERT(VARCHAR(20),@AdjustmentTax),0) + ',' +
			ISNULL(CONVERT(VARCHAR(20),@AdjustmentNet),0)

	EXEC _C00_SimpleValueIntoField @ReturnDFID, @ReturnString, @ObjectID 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Cancellation__AdjustmentOverride] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Cancellation__AdjustmentOverride] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Cancellation__AdjustmentOverride] TO [sp_executeall]
GO
