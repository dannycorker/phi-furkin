SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 29-06-2015
-- Description:	Gets the script section navigation criteria for the given from section id in order
-- =============================================
CREATE PROCEDURE [dbo].[ScriptSectionNavigationCriteria__GetByFromStepSectionIDOrdered]

@ClientID INT,
@FromStepSectionID INT

AS
BEGIN

	SET NOCOUNT ON;

	SELECT df.QuestionTypeID, df.FieldName, nc.* FROM ScriptSectionNavigationCriteria nc WITH (NOLOCK) 
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = nc.DetailFieldID
	INNER JOIN ScriptSection fss WITH (NOLOCK) ON fss.ScriptSectionID = nc.FromStepSectionID 
	INNER JOIN ScriptSection tss WITH (NOLOCK) ON tss.ScriptSectionID = nc.ToStepSectionID 
	WHERE nc.ClientID=@ClientID AND nc.FromStepSectionID = @FromStepSectionID 
	ORDER BY nc.ToStepSectionID ASC


END

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria__GetByFromStepSectionIDOrdered] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSectionNavigationCriteria__GetByFromStepSectionIDOrdered] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSectionNavigationCriteria__GetByFromStepSectionIDOrdered] TO [sp_executeall]
GO
