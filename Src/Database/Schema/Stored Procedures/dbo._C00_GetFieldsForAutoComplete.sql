SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 18-04-2013
-- Description:	Basic Field Finder Looks at the standard fields 
--				and the alias fields for the given target
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetFieldsForAutoComplete]
	
	@Target VARCHAR(250),
	@ClientID INT
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT ObjectName + ' ' + PropertyName Field, Target 
	FROM FieldTarget WITH (NOLOCK) WHERE PropertyName like '%' + @Target + '%'
	UNION
	SELECT CONVERT(VARCHAR,DetailFieldID) + ':' + DetailFieldAlias Field, '[!A:' + DetailFieldAlias + ']' Target 
	FROM DetailFieldAlias WITH (NOLOCK) WHERE DetailFieldAlias.DetailFieldAlias like '%' + @Target + '%' and ClientID=@ClientID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetFieldsForAutoComplete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetFieldsForAutoComplete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetFieldsForAutoComplete] TO [sp_executeall]
GO
