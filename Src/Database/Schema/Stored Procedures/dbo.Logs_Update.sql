SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Logs table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Logs_Update]
(

	@LogID int   ,

	@LogDateTime datetime   ,

	@TypeOfLogEntry varchar (6)  ,

	@ClassName varchar (512)  ,

	@MethodName varchar (512)  ,

	@LogEntry varchar (MAX)  ,

	@ClientPersonnelID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Logs]
				SET
					[LogDateTime] = @LogDateTime
					,[TypeOfLogEntry] = @TypeOfLogEntry
					,[ClassName] = @ClassName
					,[MethodName] = @MethodName
					,[LogEntry] = @LogEntry
					,[ClientPersonnelID] = @ClientPersonnelID
				WHERE
[LogID] = @LogID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Logs_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Logs_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Logs_Update] TO [sp_executeall]
GO
