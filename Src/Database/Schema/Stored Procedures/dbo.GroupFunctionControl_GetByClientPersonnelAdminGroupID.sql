SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the GroupFunctionControl table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GroupFunctionControl_GetByClientPersonnelAdminGroupID]
(

	@ClientPersonnelAdminGroupID int   
)
AS


				SELECT
					[GroupFunctionControlID],
					[ClientPersonnelAdminGroupID],
					[ModuleID],
					[FunctionTypeID],
					[HasDescendants],
					[RightID],
					[LeadTypeID]
				FROM
					[dbo].[GroupFunctionControl] WITH (NOLOCK) 
				WHERE
										[ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControl_GetByClientPersonnelAdminGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupFunctionControl_GetByClientPersonnelAdminGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupFunctionControl_GetByClientPersonnelAdminGroupID] TO [sp_executeall]
GO
