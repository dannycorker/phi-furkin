SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 26-03-2013
-- Description:	Adds an SMS Survey Start Event Type
-- =============================================
CREATE PROCEDURE [dbo].[_C00_AddSMSSurveyStartEvent]
	@ClientID INT,
	@UserID INT,
	@LeadTypeID INT,
	@SMSQuestionnaireID INT
	
AS
BEGIN
	
	DECLARE @QuestionnaireName VARCHAR(250)
	
	SELECT @QuestionnaireName=Title FROM SMSQuestionnaire WITH (NOLOCK) WHERE SMSQuestionnaireID=@SMSQuestionnaireID

	-- Add an sms survey start event 
	INSERT INTO EventType (ClientID, EventTypeName, EventTypeDescription, Enabled, UnitsOfEffort, FollowupTimeUnitsID, FollowupQuantity, AvailableManually, StatusAfterEvent, AquariumEventAfterEvent, EventSubtypeID, DocumentTypeID, LeadTypeID, AllowCustomTimeUnits, InProcess, KeyEvent, UseEventCosts, UseEventUOEs, UseEventDisbursements, UseEventComments, SignatureRequired, SignatureOverride, VisioX, VisioY, AquariumEventSubtypeID, WhoCreated, WhenCreated, WhoModified, WhenModified, SourceID)
	VALUES (@ClientID, @QuestionnaireName, @QuestionnaireName,1,0,NULL,NULL,1,NULL,NULL,24,null,@LeadTypeID,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,@UserID,dbo.fn_GetDate_Local(),@UserID,dbo.fn_GetDate_Local(), @SMSQuestionnaireID)
	
	SELECT SCOPE_IDENTITY() EventTypeID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AddSMSSurveyStartEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_AddSMSSurveyStartEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AddSMSSurveyStartEvent] TO [sp_executeall]
GO
