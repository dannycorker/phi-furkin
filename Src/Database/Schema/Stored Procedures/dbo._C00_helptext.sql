SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[_C00_helptext]
@Content nvarchar(max)
as
begin

	set nocount on

	declare @dbname sysname
	,@objid	int
	,@BlankSpaceAdded   int
	,@BasePos       int
	,@CurrentPos    int
	,@TextLength    int
	,@LineId        int
	,@AddOnLen      int
	,@LFCR          int --lengths of line feed carriage return
	,@DefinedLength int

	/* NOTE: Length of @SyscomText is 4000 to replace the length of
	** text column in syscomments.
	** lengths on @Line, #CommentText Text column and
	** value for @DefinedLength are all 255. These need to all have
	** the same values. 255 was selected in order for the max length
	** display using down level clients
	*/
	,@SyscomText	nvarchar(4000)
	,@Line          nvarchar(255)

	select @DefinedLength = 255
	select @BlankSpaceAdded = 0 /*Keeps track of blank spaces at end of lines. Note Len function ignores
								 trailing blank spaces*/
	CREATE TABLE #CommentText
	(LineId	int
	 ,Text  nvarchar(255) collate database_default)

		declare ms_crs_syscom  CURSOR LOCAL
		FOR select @Content
		FOR READ ONLY


	/*
	**  else get the text.
	*/
	select @LFCR = 2
	select @LineId = 1

	OPEN ms_crs_syscom

	FETCH NEXT from ms_crs_syscom into @SyscomText

	WHILE @@fetch_status >= 0
	begin

		select  @BasePos    = 1
		select  @CurrentPos = 1
		select  @TextLength = LEN(@SyscomText)

		WHILE @CurrentPos  != 0
		begin
			--Looking for end of line followed by carriage return
			select @CurrentPos = CHARINDEX(char(13)+char(10), @SyscomText, @BasePos)

			--If carriage return found
			IF @CurrentPos != 0
			begin
				/*If new value for @Lines length will be > then the
				**set length then insert current contents of @line
				**and proceed.
				*/
				while (isnull(LEN(@Line),0) + @BlankSpaceAdded + @CurrentPos-@BasePos + @LFCR) > @DefinedLength
				begin
					select @AddOnLen = @DefinedLength-(isnull(LEN(@Line),0) + @BlankSpaceAdded)
					INSERT #CommentText VALUES
					( @LineId,
					  isnull(@Line, N'') + isnull(SUBSTRING(@SyscomText, @BasePos, @AddOnLen), N''))
					select @Line = NULL, @LineId = @LineId + 1,
						   @BasePos = @BasePos + @AddOnLen, @BlankSpaceAdded = 0
				end
				select @Line    = isnull(@Line, N'') + isnull(SUBSTRING(@SyscomText, @BasePos, @CurrentPos-@BasePos + @LFCR), N'')
				select @BasePos = @CurrentPos+2
				INSERT #CommentText VALUES( @LineId, @Line )
				select @LineId = @LineId + 1
				select @Line = NULL
			end
			else
			--else carriage return not found
			begin
				IF @BasePos <= @TextLength
				begin
					/*If new value for @Lines length will be > then the
					**defined length
					*/
					while (isnull(LEN(@Line),0) + @BlankSpaceAdded + @TextLength-@BasePos+1 ) > @DefinedLength
					begin
						select @AddOnLen = @DefinedLength - (isnull(LEN(@Line),0) + @BlankSpaceAdded)
						INSERT #CommentText VALUES
						( @LineId,
						  isnull(@Line, N'') + isnull(SUBSTRING(@SyscomText, @BasePos, @AddOnLen), N''))
						select @Line = NULL, @LineId = @LineId + 1,
							@BasePos = @BasePos + @AddOnLen, @BlankSpaceAdded = 0
					end
					select @Line = isnull(@Line, N'') + isnull(SUBSTRING(@SyscomText, @BasePos, @TextLength-@BasePos+1 ), N'')
					if LEN(@Line) < @DefinedLength and charindex(' ', @SyscomText, @TextLength+1 ) > 0
					begin
						select @Line = @Line + ' ', @BlankSpaceAdded = 1
					end
				end
			end
			
		end

		FETCH NEXT from ms_crs_syscom into @SyscomText
	end

	IF @Line is NOT NULL
		INSERT #CommentText VALUES( @LineId, @Line )

	select Text, lineID
	 from #CommentText order by LineId
	 --c c
	CLOSE  ms_crs_syscom
	DEALLOCATE 	ms_crs_syscom

	DROP TABLE 	#CommentText

end


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_helptext] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_helptext] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_helptext] TO [sp_executeall]
GO
