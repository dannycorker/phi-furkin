SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 08-09-2014
-- Description:	Adds a document type and an event type for an sms survey introduction text
--				It then updates the sms outgoing phone number with the event type id of the sms event
-- =============================================
CREATE PROCEDURE [dbo].[SMSOutgoingPhoneNumber__UpdateIntroductionEventType]
	@ClientID INT,
	@LeadTypeID INT,
	@SMSOutgoingPhoneNumberID INT,
	@IntroductionFrom VARCHAR(250),
	@IntroductionMessage VARCHAR(2000),
	@DelayInSeconds INT,
	@UserID INT,
	@SmsGatewayID INT,
	@DocumentTypeID INT = NULL,
	@EventTypeID INT = NULL	
AS
BEGIN

	SET NOCOUNT ON;

	IF @DocumentTypeID IS NOT NULL
	BEGIN 
		-- update document type and event type for introduction test message
		UPDATE DocumentType
		SET EmailBodyText=@IntroductionMessage, WhoModified = @UserID, WhenModified = dbo.fn_GetDate_Local(), FromDetails=@IntroductionFrom, RecipientsTo='[!Mobile]'
		WHERE DocumentTypeID=@DocumentTypeID
		

		UPDATE EventType
		SET SmsGatewayID=@SmsGatewayID, WhoModified = @UserID, WhenModified = dbo.fn_GetDate_Local()
		WHERE EventTypeID=@EventTypeID
		
		DECLARE @SMSStartEventTypeID INT
		DECLARE @Delay INT
		SELECT @SMSStartEventTypeID=SMSSurveyStartEventTypeID, @Delay = DelayInSeconds
		FROM SMSOutgoingPhoneNumber WITH (NOLOCK) WHERE SMSOutgoingPhoneNumberID=@SMSOutgoingPhoneNumberID
		
		DECLARE @EventTypeAutomatedEventTypeID INT
		
		SELECT @EventTypeAutomatedEventTypeID=EventTypeAutomatedEventID FROM EventTypeAutomatedEvent WITH (NOLOCK) 
		WHERE EventTypeID=@EventTypeID AND AutomatedEventTypeID=@SMSStartEventTypeID
		
		IF @EventTypeAutomatedEventTypeID IS NULL -- does not exist so create it
		BEGIN

			INSERT INTO EventTypeAutomatedEvent(ClientID, EventTypeID, AutomatedEventTypeID, RunAsUserID, ThreadToFollowUp, InternalPriority, DelaySeconds)
			VALUES (@ClientID, @EventTypeID, @SMSStartEventTypeID, @UserID, 1, 1, @Delay)			
		
		END
		ELSE
		BEGIN
	
			UPDATE EventTypeAutomatedEvent
			SET DelaySeconds=@Delay
			WHERE EventTypeAutomatedEventID=@EventTypeAutomatedEventTypeID
	
		END
	END
	ELSE
	BEGIN
		-- no document type so create one
		EXEC SMSOutgoingPhoneNumber__AddIntroductionEventType @ClientID, @LeadTypeID, @SMSOutgoingPhoneNumberID, @IntroductionFrom, @IntroductionMessage, @DelayInSeconds, @UserID, @SmsGatewayID
		
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber__UpdateIntroductionEventType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSOutgoingPhoneNumber__UpdateIntroductionEventType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber__UpdateIntroductionEventType] TO [sp_executeall]
GO
