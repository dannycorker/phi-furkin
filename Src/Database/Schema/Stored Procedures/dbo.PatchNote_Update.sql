SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the PatchNote table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PatchNote_Update]
(

	@PatchNoteID int   ,

	@PatchID int   ,

	@Location varchar (50)  ,

	@PatchNoteText varchar (200)  ,

	@AdminOnly bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[PatchNote]
				SET
					[PatchID] = @PatchID
					,[Location] = @Location
					,[PatchNoteText] = @PatchNoteText
					,[AdminOnly] = @AdminOnly
				WHERE
[PatchNoteID] = @PatchNoteID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PatchNote_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PatchNote_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PatchNote_Update] TO [sp_executeall]
GO
