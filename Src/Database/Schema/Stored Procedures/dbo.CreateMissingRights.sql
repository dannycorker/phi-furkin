SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2009-01-29
-- Description:	Create all missing group rights and user rights for everyone. 
--              This includes a debug flag to show what will happen, without doing it.
--	JWG 2011-07-21 Include the client zero lead types for CustomerDetailValues etc
-- =============================================
CREATE PROCEDURE [dbo].[CreateMissingRights] 

	@Debug BIT = 0
	
AS
BEGIN
	SET NOCOUNT ON;

	IF @Debug = 1
	BEGIN
		/*
			1) Make sure that no-one is editing the group (or any users within it)
			The easiest way to do this is to ensure that the GroupAndUserEditingControl
			table is empty. Otherwise, you need to be sure that:
				i) no groups belonging to this client are being edited,
				AND that 
				ii) none of the Aquarium groups that any users within this client belong 
				to are being edited.
		*/
		SELECT * FROM dbo.GroupAndUserEditingControl

		/*
			Identify parent functions of interest.
		*/
		;WITH ParentFunctions AS 
		(
			SELECT ft.* 
			FROM dbo.FunctionType ft WITH (NOLOCK) 
			WHERE ft.IsVirtual = 0 
			AND ft.ParentFunctionTypeID IS NULL
			UNION ALL
			SELECT ft.* 
			FROM dbo.FunctionType ft WITH (NOLOCK) 
			INNER JOIN ParentFunctions cte ON cte.FunctionTypeID = ft.ParentFunctionTypeID
			WHERE ft.IsVirtual = 0 
		)
		SELECT ft.* 
		FROM ParentFunctions pf 
		INNER JOIN dbo.FunctionType ft WITH (NOLOCK) ON ft.FunctionTypeID = pf.FunctionTypeID
		WHERE pf.ParentFunctionTypeID IS NOT NULL 
		ORDER BY 1

		/* 
			Group records that were missed by Aquarium developers 
			Eg, added a new button under LeadManager -> Other (FunctionTypeID = 10, New FunctionID = 999)
			but forgot to call SetSecurityForNewSystemFunction for it.
		*/
		SELECT gfcParent.ClientPersonnelAdminGroupID, gfcParent.FunctionTypeID, NULL, f.FunctionID, gfcParent.RightID 
		FROM dbo.Functions f WITH (NOLOCK) 
		INNER JOIN dbo.GroupFunctionControl gfcParent WITH (NOLOCK) ON gfcParent.FunctionTypeID = f.FunctionTypeID AND gfcParent.HasDescendants = 1  
		LEFT JOIN dbo.GroupRightsDynamic g WITH (NOLOCK) ON f.FunctionID = g.ObjectID 
			AND g.ClientPersonnelAdminGroupID = gfcParent.ClientPersonnelAdminGroupID 
			AND g.FunctionTypeID = gfcParent.FunctionTypeID 
		WHERE f.FunctionTypeID IN (6,7,8,9,10,14,15,16,17,18,19,20,21,24) 
		AND g.GroupRightsDynamicID IS NULL

		/* 
			Ditto for UserRightsDynamic 
		*/
		SELECT gfcParent.ClientPersonnelID, gfcParent.FunctionTypeID, NULL, f.FunctionID, gfcParent.RightID 
		FROM dbo.Functions f WITH (NOLOCK) 
		INNER JOIN dbo.UserFunctionControl gfcParent WITH (NOLOCK) ON gfcParent.FunctionTypeID = f.FunctionTypeID AND gfcParent.HasDescendants = 1  
		LEFT JOIN dbo.UserRightsDynamic g WITH (NOLOCK) ON f.FunctionID = g.ObjectID 
			AND g.ClientPersonnelID = gfcParent.ClientPersonnelID 
			AND g.FunctionTypeID = gfcParent.FunctionTypeID 
		WHERE f.FunctionTypeID IN (6,7,8,9,10,14,15,16,17,18,19,20,21,24) 
		AND g.UserRightsDynamicID IS NULL
		
		/*
			Check for lead type rights that should exist but don't.
			Wherever the parent level (gfc 7) says it has descendants, each lead 
			type for the client should be represented at the child level (gfc 23).
			Add in each lead type that doesn't have a gfc 23 entry.
			JWG 2011-07-21 Include the client zero lead types for CustomerDetailValues etc
		*/
		SELECT * 
		FROM GroupFunctionControl gfc7 
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc7.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		INNER JOIN LeadType lt ON lt.ClientID IN (cpg.ClientID, 0) 
		WHERE gfc7.FunctionTypeID = 7 
		AND gfc7.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * FROM GroupFunctionControl gfc23 
			WHERE gfc23.ClientPersonnelAdminGroupID = gfc7.ClientPersonnelAdminGroupID 
			AND gfc23.FunctionTypeID = 23 
			AND gfc23.LeadTypeID = lt.LeadTypeID 
		)
		ORDER BY gfc7.ClientPersonnelAdminGroupID, lt.LeadTypeID 

		/* 
			Show all gfc for groups where the Individual Lead Types entry is set, 
			and HasDescendants is set to true, but no 12, 13 or 25 descendant control records exist.
			There should be none.
		*/
		SELECT * 
		FROM dbo.groupfunctioncontrol gfc_23 (NOLOCK) 
		LEFT JOIN dbo.groupfunctioncontrol gfc_12_13_25 (NOLOCK) ON gfc_12_13_25.ClientPersonnelAdminGroupID = gfc_23.ClientPersonnelAdminGroupID 
			AND gfc_12_13_25.functiontypeid IN (12, 13, 25) 
			AND gfc_12_13_25.leadtypeid = gfc_23.leadtypeid
		WHERE gfc_23.functiontypeid = 23 
		AND gfc_23.hasdescendants = 1 
		AND gfc_12_13_25.groupfunctioncontrolid IS NULL

		/* 
			Test the reverse.
			Show all gfc for groups where the Individual Lead Types entry is not set, 
			and HasDescendants is set to false, but any 12, 13 or 25 descendant control records do exist.
			There should be none.
		*/
		SELECT * 
		FROM dbo.groupfunctioncontrol gfc_23 (NOLOCK) 
		LEFT JOIN dbo.groupfunctioncontrol gfc_12_13_25 (NOLOCK) ON gfc_12_13_25.ClientPersonnelAdminGroupID = gfc_23.ClientPersonnelAdminGroupID 
			AND gfc_12_13_25.functiontypeid IN (12, 13, 25) 
			AND gfc_12_13_25.leadtypeid = gfc_23.leadtypeid
		WHERE gfc_23.functiontypeid = 23 
		AND gfc_23.hasdescendants = 0 
		AND gfc_12_13_25.groupfunctioncontrolid IS NOT NULL


		/*
			Check for function control records that claim to have no descendants,
			whereas some child records do exist.
		*/
		SELECT * 
		FROM dbo.groupfunctioncontrol gfc (NOLOCK) 
		WHERE gfc.functiontypeid IN (12, 13, 25)
		AND gfc.hasdescendants = 0 
		AND EXISTS (
			SELECT * 
			FROM dbo.grouprightsdynamic grd (NOLOCK) 
			WHERE grd.ClientPersonnelAdminGroupID = gfc.ClientPersonnelAdminGroupID 
			AND grd.functiontypeid = gfc.functiontypeid 
			AND grd.leadtypeid = gfc.leadtypeid 
		) 
		

		/*
			Look for exploded Page control records (gfc_12),
			joined to all pages for that lead type, 
			where some grd records are missing.
		*/
		SELECT * 
		FROM dbo.groupfunctioncontrol gfc (NOLOCK) 
		INNER JOIN dbo.detailfieldpages dfp (NOLOCK) ON dfp.leadtypeid = gfc.leadtypeid 
		LEFT JOIN dbo.grouprightsdynamic grd (NOLOCK) ON grd.ClientPersonnelAdminGroupID = gfc.ClientPersonnelAdminGroupID 
			AND grd.functiontypeid = 12 
			AND grd.leadtypeid = grd.leadtypeid 
			AND grd.objectid = dfp.detailfieldpageid 
		WHERE gfc.functiontypeid = 12 
		AND gfc.hasdescendants = 1 
		AND grd.grouprightsdynamicid IS NULL 

		/*
			Look for exploded Event Type control records (gfc_13),
			joined to all event types for that lead type, 
			where some grd records are missing.
		*/
		SELECT * 
		FROM dbo.groupfunctioncontrol gfc (NOLOCK) 
		INNER JOIN dbo.eventtype et (NOLOCK) ON et.leadtypeid = gfc.leadtypeid 
		LEFT JOIN dbo.grouprightsdynamic grd (NOLOCK) ON grd.ClientPersonnelAdminGroupID = gfc.ClientPersonnelAdminGroupID 
			AND grd.functiontypeid = 13
			AND grd.leadtypeid = grd.leadtypeid 
			AND grd.objectid = et.eventtypeid 
		WHERE gfc.functiontypeid = 13 
		AND gfc.hasdescendants = 1 
		AND grd.grouprightsdynamicid IS NULL 

		/*
			Look for exploded Encrypted Field control records (gfc_25),
			joined to all encrypted fields for that lead type, 
			where some grd records are missing.
		*/
		SELECT * 
		FROM dbo.groupfunctioncontrol gfc (NOLOCK) 
		INNER JOIN dbo.detailfields df (NOLOCK) ON df.leadtypeid = gfc.leadtypeid AND df.encrypt = 1
		LEFT JOIN dbo.grouprightsdynamic grd (NOLOCK) ON grd.ClientPersonnelAdminGroupID = gfc.ClientPersonnelAdminGroupID 
			AND grd.functiontypeid = 25
			AND grd.leadtypeid = grd.leadtypeid 
			AND grd.objectid = df.detailfieldid 
		WHERE gfc.functiontypeid = 25 
		AND gfc.hasdescendants = 1 
		AND grd.grouprightsdynamicid IS NULL 


		/*
			Check for office rights that should exist but don't.
			Wherever the parent level (gfc 11) says it has descendants, each office
			for that leadtype should be represented at the child level (grd 11).
			Add in each office that doesn't have a grd 11 entry.
		*/
		SELECT * 
		FROM GroupFunctionControl gfc11 
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc11.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		INNER JOIN LeadType lt ON lt.ClientID = cpg.ClientID 
		INNER JOIN ClientOffices co ON co.ClientID = cpg.ClientID 
		WHERE gfc11.FunctionTypeID = 11 
		AND gfc11.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM GroupRightsDynamic r 
			WHERE r.ClientPersonnelAdminGroupID = gfc11.ClientPersonnelAdminGroupID 
			AND r.FunctionTypeID = 11 -- Offices 
			AND r.LeadTypeID = lt.LeadTypeID 
			AND r.ObjectID = co.ClientOfficeID 
		)
		ORDER BY gfc11.ClientPersonnelAdminGroupID, lt.LeadTypeID 

		/*
			Check for page rights that should exist but don't.
			Wherever the parent level (gfc 12) says it has descendants, each page
			for that leadtype should be represented at the child level (grd 12).
			Add in each page that doesn't have a grd 12 entry.
		*/
		SELECT * 
		FROM GroupFunctionControl gfc12 
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc12.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		INNER JOIN LeadType lt ON lt.ClientID = cpg.ClientID AND gfc12.LeadTypeID = lt.LeadTypeID 
		INNER JOIN DetailFieldPages realDFP ON realDFP.LeadTypeID = lt.LeadTypeID 
		WHERE gfc12.FunctionTypeID = 12 
		AND gfc12.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM GroupRightsDynamic r 
			WHERE r.ClientPersonnelAdminGroupID = gfc12.ClientPersonnelAdminGroupID 
			AND r.FunctionTypeID = 12 -- Pages 
			AND r.LeadTypeID = lt.LeadTypeID 
			AND r.ObjectID = realDFP.DetailFieldPageID 
		)
		ORDER BY gfc12.ClientPersonnelAdminGroupID, lt.LeadTypeID 

		/*
			Check for EventType rights that should exist but don't.
			Wherever the parent level (gfc 13) says it has descendants, each EventType
			for that leadtype should be represented at the child level (grd 13).
			Add in each EventType that doesn't have a grd 13 entry.
		*/
		SELECT * 
		FROM GroupFunctionControl gfc13 
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc13.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		INNER JOIN LeadType lt ON lt.ClientID = cpg.ClientID AND gfc13.LeadTypeID = lt.LeadTypeID
		INNER JOIN EventType realEvent ON realEvent.LeadTypeID = lt.LeadTypeID 
		WHERE gfc13.FunctionTypeID = 13 
		AND gfc13.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM GroupRightsDynamic r 
			WHERE r.ClientPersonnelAdminGroupID = gfc13.ClientPersonnelAdminGroupID 
			AND r.FunctionTypeID = 13 -- EventTypes 
			AND r.LeadTypeID = lt.LeadTypeID 
			AND r.ObjectID = realEvent.EventTypeID 
		)
		ORDER BY gfc13.ClientPersonnelAdminGroupID, lt.LeadTypeID 

		/*
			Check for Encrypted Field rights that should exist but don't.
			Wherever the parent level (gfc 25) says it has descendants, each Encrypted Field
			for that leadtype should be represented at the child level (grd 25).
			Add in each Encrypted Field that doesn't have a grd 25 entry.
		*/
		SELECT * 
		FROM GroupFunctionControl gfc25 
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc25.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		INNER JOIN LeadType lt ON lt.ClientID = cpg.ClientID AND gfc25.LeadTypeID = lt.LeadTypeID
		INNER JOIN DetailFields realDF ON realDF.LeadTypeID = lt.LeadTypeID AND realDF.Encrypt = 1 
		WHERE gfc25.FunctionTypeID = 25 
		AND gfc25.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM GroupRightsDynamic r 
			WHERE r.ClientPersonnelAdminGroupID = gfc25.ClientPersonnelAdminGroupID 
			AND r.FunctionTypeID = 25 -- Encrypted Fields 
			AND r.LeadTypeID = lt.LeadTypeID 
			AND r.ObjectID = realDF.DetailFieldID 
		)
		ORDER BY gfc25.ClientPersonnelAdminGroupID, lt.LeadTypeID 

		/*
			Check for lead type rights that should exist but don't.
			Wherever the parent level (gfc 7) says it has descendants, each lead 
			type for the client should be represented at the child level (gfc 23).
			Add in each lead type that doesn't have a gfc 23 entry.
			JWG 2011-07-21 Include the client zero lead types for CustomerDetailValues etc
		*/
		SELECT * 
		FROM UserFunctionControl ufc7 
		INNER JOIN ClientPersonnel cpg ON ufc7.ClientPersonnelID = cpg.ClientPersonnelID 
		INNER JOIN LeadType lt ON lt.ClientID IN (cpg.ClientID, 0) 
		WHERE ufc7.FunctionTypeID = 7 
		AND ufc7.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * FROM UserFunctionControl ufc23 
			WHERE ufc23.ClientPersonnelID = ufc7.ClientPersonnelID 
			AND ufc23.FunctionTypeID = 23 
			AND ufc23.LeadTypeID = lt.LeadTypeID 
		)
		ORDER BY ufc7.ClientPersonnelID, lt.LeadTypeID 

		/* 
			Show all gfc for groups where the Individual Lead Types entry is set, 
			and HasDescendants is set to true, but no 12, 13 or 25 descendant control records exist.
			There should be none.
		*/
		SELECT * 
		FROM dbo.userfunctioncontrol gfc_23 (NOLOCK) 
		LEFT JOIN dbo.userfunctioncontrol gfc_12_13_25 (NOLOCK) ON gfc_12_13_25.ClientPersonnelID = gfc_23.ClientPersonnelID 
			AND gfc_12_13_25.functiontypeid IN (12, 13, 25) 
			AND gfc_12_13_25.leadtypeid = gfc_23.leadtypeid
		WHERE gfc_23.functiontypeid = 23 
		AND gfc_23.hasdescendants = 1 
		AND gfc_12_13_25.userfunctioncontrolid IS NULL

		/* 
			Test the reverse.
			Show all gfc for groups where the Individual Lead Types entry is not set, 
			and HasDescendants is set to false, but any 12, 13 or 25 descendant control records do exist.
			There should be none.
		*/
		SELECT * 
		FROM dbo.userfunctioncontrol gfc_23 (NOLOCK) 
		LEFT JOIN dbo.userfunctioncontrol gfc_12_13_25 (NOLOCK) ON gfc_12_13_25.ClientPersonnelID = gfc_23.ClientPersonnelID 
			AND gfc_12_13_25.functiontypeid IN (12, 13, 25) 
			AND gfc_12_13_25.leadtypeid = gfc_23.leadtypeid
		WHERE gfc_23.functiontypeid = 23 
		AND gfc_23.hasdescendants = 0 
		AND gfc_12_13_25.userfunctioncontrolid IS NOT NULL


		/*
			Check for function control records that claim to have no descendants,
			whereas some child records do exist.
		*/
		SELECT * 
		FROM dbo.userfunctioncontrol ufc (NOLOCK) 
		WHERE ufc.functiontypeid IN (12, 13, 25)
		AND ufc.hasdescendants = 0 
		AND EXISTS (
			SELECT * 
			FROM dbo.userrightsdynamic urd (NOLOCK) 
			WHERE urd.ClientPersonnelID = ufc.ClientPersonnelID 
			AND urd.functiontypeid = ufc.functiontypeid 
			AND urd.leadtypeid = ufc.leadtypeid 
		) 

		/*
			Look for exploded Page control records (gfc_12),
			joined to all pages for that lead type, 
			where some grd records are missing.
		*/
		SELECT * 
		FROM dbo.userfunctioncontrol gfc (NOLOCK) 
		INNER JOIN dbo.detailfieldpages dfp (NOLOCK) ON dfp.leadtypeid = gfc.leadtypeid 
		LEFT JOIN dbo.userrightsdynamic grd (NOLOCK) ON grd.ClientPersonnelID = gfc.ClientPersonnelID 
			AND grd.functiontypeid = 12 
			AND grd.leadtypeid = grd.leadtypeid 
			AND grd.objectid = dfp.detailfieldpageid 
		WHERE gfc.functiontypeid = 12 
		AND gfc.hasdescendants = 1 
		AND grd.userrightsdynamicid IS NULL 

		/*
			Look for exploded Event Type control records (gfc_13),
			joined to all event types for that lead type, 
			where some grd records are missing.
		*/
		SELECT * 
		FROM dbo.userfunctioncontrol gfc (NOLOCK) 
		INNER JOIN dbo.eventtype et (NOLOCK) ON et.leadtypeid = gfc.leadtypeid 
		LEFT JOIN dbo.userrightsdynamic grd (NOLOCK) ON grd.ClientPersonnelID = gfc.ClientPersonnelID 
			AND grd.functiontypeid = 13
			AND grd.leadtypeid = grd.leadtypeid 
			AND grd.objectid = et.eventtypeid 
		WHERE gfc.functiontypeid = 13 
		AND gfc.hasdescendants = 1 
		AND grd.userrightsdynamicid IS NULL 

		/*
			Look for exploded Encrypted Field control records (gfc_25),
			joined to all encrypted fields for that lead type, 
			where some grd records are missing.
		*/
		SELECT * 
		FROM dbo.userfunctioncontrol gfc (NOLOCK) 
		INNER JOIN dbo.detailfields df (NOLOCK) ON df.leadtypeid = gfc.leadtypeid AND df.encrypt = 1
		LEFT JOIN dbo.userrightsdynamic grd (NOLOCK) ON grd.ClientPersonnelID = gfc.ClientPersonnelID 
			AND grd.functiontypeid = 25
			AND grd.leadtypeid = grd.leadtypeid 
			AND grd.objectid = df.detailfieldid 
		WHERE gfc.functiontypeid = 25 
		AND gfc.hasdescendants = 1 
		AND grd.userrightsdynamicid IS NULL 


		/*
			Check for office rights that should exist but don't.
			Wherever the parent level (gfc 11) says it has descendants, each office
			for that leadtype should be represented at the child level (grd 11).
			Add in each office that doesn't have a grd 11 entry.
		*/
		SELECT * 
		FROM UserFunctionControl ufc11 
		INNER JOIN ClientPersonnel cpg ON ufc11.ClientPersonnelID = cpg.ClientPersonnelID 
		INNER JOIN LeadType lt ON lt.ClientID = cpg.ClientID 
		INNER JOIN ClientOffices co ON co.ClientID = cpg.ClientID 
		WHERE ufc11.FunctionTypeID = 11 
		AND ufc11.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM UserRightsDynamic r 
			WHERE r.ClientPersonnelID = ufc11.ClientPersonnelID 
			AND r.FunctionTypeID = 11 -- Offices 
			AND r.LeadTypeID = lt.LeadTypeID 
			AND r.ObjectID = co.ClientOfficeID 
		)
		ORDER BY ufc11.ClientPersonnelID, lt.LeadTypeID 

		/*
			Check for page rights that should exist but don't.
			Wherever the parent level (ufc 12) says it has descendants, each page
			for that leadtype should be represented at the child level (urd 12).
			Add in each page that doesn't have a urd 12 entry.
		*/
		SELECT * 
		FROM UserFunctionControl ufc12 
		INNER JOIN ClientPersonnel cpg ON ufc12.ClientPersonnelID = cpg.ClientPersonnelID 
		INNER JOIN LeadType lt ON lt.ClientID = cpg.ClientID AND ufc12.LeadTypeID = lt.LeadTypeID  
		INNER JOIN DetailFieldPages realDFP ON realDFP.LeadTypeID = lt.LeadTypeID 
		WHERE ufc12.FunctionTypeID = 12 
		AND ufc12.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM UserRightsDynamic r 
			WHERE r.ClientPersonnelID = ufc12.ClientPersonnelID 
			AND r.FunctionTypeID = 12 -- Pages 
			AND r.LeadTypeID = lt.LeadTypeID 
			AND r.ObjectID = realDFP.DetailFieldPageID 
		)
		ORDER BY ufc12.ClientPersonnelID, lt.LeadTypeID 

		/*
			Check for EventType rights that should exist but don't.
			Wherever the parent level (ufc 13) says it has descendants, each EventType
			for that leadtype should be represented at the child level (urd 13).
			Add in each EventType that doesn't have a urd 13 entry.
		*/
		SELECT * 
		FROM UserFunctionControl ufc13 
		INNER JOIN ClientPersonnel cpg ON ufc13.ClientPersonnelID = cpg.ClientPersonnelID 
		INNER JOIN LeadType lt ON lt.ClientID = cpg.ClientID AND ufc13.LeadTypeID = lt.LeadTypeID
		INNER JOIN EventType realEvent ON realEvent.LeadTypeID = lt.LeadTypeID 
		WHERE ufc13.FunctionTypeID = 13 
		AND ufc13.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM UserRightsDynamic r 
			WHERE r.ClientPersonnelID = ufc13.ClientPersonnelID 
			AND r.FunctionTypeID = 13 -- EventTypes 
			AND r.LeadTypeID = lt.LeadTypeID 
			AND r.ObjectID = realEvent.EventTypeID 
		)
		ORDER BY ufc13.ClientPersonnelID, lt.LeadTypeID 

		/*
			Check for Encrypted Field rights that should exist but don't.
			Wherever the parent level (ufc 25) says it has descendants, each Encrypted Field
			for that leadtype should be represented at the child level (urd 25).
			Add in each Encrypted Field that doesn't have a urd 25 entry.
		*/
		SELECT * 
		FROM UserFunctionControl ufc25 
		INNER JOIN ClientPersonnel cpg ON ufc25.ClientPersonnelID = cpg.ClientPersonnelID 
		INNER JOIN LeadType lt ON lt.ClientID = cpg.ClientID AND ufc25.LeadTypeID = lt.LeadTypeID
		INNER JOIN DetailFields realDF ON realDF.LeadTypeID = lt.LeadTypeID AND realDF.Encrypt = 1 
		WHERE ufc25.FunctionTypeID = 25 
		AND ufc25.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM UserRightsDynamic r 
			WHERE r.ClientPersonnelID = ufc25.ClientPersonnelID 
			AND r.FunctionTypeID = 25 -- Encrypted Fields 
			AND r.LeadTypeID = lt.LeadTypeID 
			AND r.ObjectID = realDF.DetailFieldID 
		)
		ORDER BY ufc25.ClientPersonnelID, lt.LeadTypeID 

		/*
			Check for Outcomes that should exist but don't.
			Wherever the parent level (gfc 9) says it has descendants, each Outcome
			for that Client should be represented at the child level (grd 9).
			Add in each Outcome that doesn't have a grd 9 entry.
		*/
		SELECT * 
		FROM GroupFunctionControl gfc9 
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc9.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		INNER JOIN Outcomes co ON co.ClientID = cpg.ClientID 
		WHERE gfc9.FunctionTypeID = 9 
		AND gfc9.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM GroupRightsDynamic r 
			WHERE r.ClientPersonnelAdminGroupID = gfc9.ClientPersonnelAdminGroupID 
			AND r.FunctionTypeID = 9 -- Outcomes 
			AND r.LeadTypeID IS NULL 
			AND r.ObjectID = co.OutcomeID 
		)
		ORDER BY gfc9.ClientPersonnelAdminGroupID


	END
	ELSE
	BEGIN
	
		/* 
			Group records that were missed by Aquarium developers 
			Eg, added a new button under LeadManager -> Other (FunctionTypeID = 10, New FunctionID = 999)
			but forgot to call SetSecurityForNewSystemFunction for it.
		*/
		INSERT INTO dbo.GroupRightsDynamic(ClientPersonnelAdminGroupID, FunctionTypeID, LeadTypeID, ObjectID, RightID)
		SELECT gfcParent.ClientPersonnelAdminGroupID, gfcParent.FunctionTypeID, NULL, f.FunctionID, gfcParent.RightID 
		FROM dbo.Functions f WITH (NOLOCK) 
		INNER JOIN dbo.GroupFunctionControl gfcParent WITH (NOLOCK) ON gfcParent.FunctionTypeID = f.FunctionTypeID AND gfcParent.HasDescendants = 1  
		LEFT JOIN dbo.GroupRightsDynamic g WITH (NOLOCK) ON f.FunctionID = g.ObjectID 
			AND g.ClientPersonnelAdminGroupID = gfcParent.ClientPersonnelAdminGroupID 
			AND g.FunctionTypeID = gfcParent.FunctionTypeID 
		WHERE f.FunctionTypeID IN (6,7,8,9,10,14,15,16,17,18,19,20,21,24) 
		AND g.GroupRightsDynamicID IS NULL

		/* 
			Ditto for UserRightsDynamic 
		*/
		INSERT INTO dbo.UserRightsDynamic(ClientPersonnelID, FunctionTypeID, LeadTypeID, ObjectID, RightID)
		SELECT gfcParent.ClientPersonnelID, gfcParent.FunctionTypeID, NULL, f.FunctionID, gfcParent.RightID 
		FROM dbo.Functions f WITH (NOLOCK) 
		INNER JOIN dbo.UserFunctionControl gfcParent WITH (NOLOCK) ON gfcParent.FunctionTypeID = f.FunctionTypeID AND gfcParent.HasDescendants = 1  
		LEFT JOIN dbo.UserRightsDynamic g WITH (NOLOCK) ON f.FunctionID = g.ObjectID 
			AND g.ClientPersonnelID = gfcParent.ClientPersonnelID 
			AND g.FunctionTypeID = gfcParent.FunctionTypeID 
		WHERE f.FunctionTypeID IN (6,7,8,9,10,14,15,16,17,18,19,20,21,24) 
		AND g.UserRightsDynamicID IS NULL


		/*
			Insert lead type rights that should exist but don't.
			Wherever the parent level (gfc 7) says it has descendants, each lead 
			type for the client should be represented at the child level (gfc 23).
			Add in each lead type that doesn't have a gfc 23 entry.
			JWG 2011-07-21 Include the client zero lead types for CustomerDetailValues etc
		*/
		INSERT INTO GroupFunctionControl(ClientPersonnelAdminGroupID, ModuleID, FunctionTypeID, HasDescendants, RightID, LeadTypeID)  
		SELECT gfc7.ClientPersonnelAdminGroupID, 1, 23, 0, 0, lt.LeadTypeID
		FROM GroupFunctionControl gfc7 
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc7.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		INNER JOIN LeadType lt ON lt.ClientID IN (cpg.ClientID, 0)
		WHERE gfc7.FunctionTypeID = 7 
		AND gfc7.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * FROM GroupFunctionControl gfc23 
			WHERE gfc23.ClientPersonnelAdminGroupID = gfc7.ClientPersonnelAdminGroupID 
			AND gfc23.FunctionTypeID = 23 
			AND gfc23.LeadTypeID = lt.LeadTypeID 
		)

		/*
			Create missing Pages control records.
		*/
		INSERT groupfunctioncontrol (ClientPersonnelAdminGroupID, moduleid, functiontypeid, hasdescendants, rightid, leadtypeid)
		SELECT gfc_23.ClientPersonnelAdminGroupID, gfc_23.moduleid, 12, 1, 0, gfc_23.leadtypeid
		FROM dbo.groupfunctioncontrol gfc_23  
		LEFT JOIN dbo.groupfunctioncontrol gfc_12_13_25 ON gfc_12_13_25.ClientPersonnelAdminGroupID = gfc_23.ClientPersonnelAdminGroupID 
			AND gfc_12_13_25.functiontypeid = 12 
			AND gfc_12_13_25.leadtypeid = gfc_23.leadtypeid
		WHERE gfc_23.functiontypeid = 23 
		AND gfc_23.hasdescendants = 1 
		AND gfc_12_13_25.groupfunctioncontrolid IS NULL

		/*
			Create missing Events control records.
		*/
		INSERT groupfunctioncontrol (ClientPersonnelAdminGroupID, moduleid, functiontypeid, hasdescendants, rightid, leadtypeid)
		SELECT gfc_23.ClientPersonnelAdminGroupID, gfc_23.moduleid, 13, 1, 0, gfc_23.leadtypeid
		FROM dbo.groupfunctioncontrol gfc_23  
		LEFT JOIN dbo.groupfunctioncontrol gfc_12_13_25 ON gfc_12_13_25.ClientPersonnelAdminGroupID = gfc_23.ClientPersonnelAdminGroupID 
			AND gfc_12_13_25.functiontypeid = 13 
			AND gfc_12_13_25.leadtypeid = gfc_23.leadtypeid
		WHERE gfc_23.functiontypeid = 23 
		AND gfc_23.hasdescendants = 1 
		AND gfc_12_13_25.groupfunctioncontrolid IS NULL

		/*
			Create missing Encrypted Fields control records.
		*/
		INSERT groupfunctioncontrol (ClientPersonnelAdminGroupID, moduleid, functiontypeid, hasdescendants, rightid, leadtypeid)
		SELECT gfc_23.ClientPersonnelAdminGroupID, gfc_23.moduleid, 25, 1, 0, gfc_23.leadtypeid
		FROM dbo.groupfunctioncontrol gfc_23  
		LEFT JOIN dbo.groupfunctioncontrol gfc_12_13_25 ON gfc_12_13_25.ClientPersonnelAdminGroupID = gfc_23.ClientPersonnelAdminGroupID 
			AND gfc_12_13_25.functiontypeid = 25 
			AND gfc_12_13_25.leadtypeid = gfc_23.leadtypeid
		WHERE gfc_23.functiontypeid = 23 
		AND gfc_23.hasdescendants = 1 
		AND gfc_12_13_25.groupfunctioncontrolid IS NULL


		/*
			Check for function control records that claim to have no descendants,
			whereas some child records do exist.
		*/
		UPDATE dbo.groupfunctioncontrol 
		SET HasDescendants = 1 
		FROM dbo.groupfunctioncontrol gfc
		WHERE gfc.functiontypeid IN (12, 13, 25)
		AND gfc.hasdescendants = 0 
		AND EXISTS (
			SELECT * 
			FROM dbo.grouprightsdynamic grd (NOLOCK) 
			WHERE grd.ClientPersonnelAdminGroupID = gfc.ClientPersonnelAdminGroupID 
			AND grd.functiontypeid = gfc.functiontypeid 
			AND grd.leadtypeid = gfc.leadtypeid 
		) 



		/*
			Insert office rights that should exist but don't.
			Wherever the parent level (gfc 11) says it has descendants, each office
			for that leadtype should be represented at the child level (grd 11).
			Add in each office that doesn't have a grd 11 entry.
		*/
		INSERT INTO GroupRightsDynamic(ClientPersonnelAdminGroupID, FunctionTypeID, LeadTypeID, ObjectID, RightID) 
		SELECT gfc11.ClientPersonnelAdminGroupID, gfc11.FunctionTypeID, lt.LeadTypeID, co.ClientOfficeID, 0
		FROM GroupFunctionControl gfc11 
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc11.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		INNER JOIN LeadType lt ON lt.ClientID = cpg.ClientID 
		INNER JOIN ClientOffices co ON co.ClientID = cpg.ClientID 
		WHERE gfc11.FunctionTypeID = 11 
		AND gfc11.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM GroupRightsDynamic r 
			WHERE r.ClientPersonnelAdminGroupID = gfc11.ClientPersonnelAdminGroupID 
			AND r.FunctionTypeID = 11 -- Offices 
			AND r.LeadTypeID = lt.LeadTypeID 
			AND r.ObjectID = co.ClientOfficeID 
		)

		/*
			Insert page rights that should exist but don't.
			Wherever the parent level (gfc 12) says it has descendants, each page
			for that leadtype should be represented at the child level (grd 12).
			Add in each page that doesn't have a grd 12 entry.
		*/
		INSERT INTO GroupRightsDynamic(ClientPersonnelAdminGroupID, FunctionTypeID, LeadTypeID, ObjectID, RightID) 
		SELECT gfc12.ClientPersonnelAdminGroupID, gfc12.FunctionTypeID, gfc12.LeadTypeID, realDFP.DetailFieldPageID, 0
		FROM GroupFunctionControl gfc12 
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc12.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		INNER JOIN LeadType lt ON lt.ClientID = cpg.ClientID AND gfc12.LeadTypeID = lt.LeadTypeID  
		INNER JOIN DetailFieldPages realDFP ON realDFP.LeadTypeID = lt.LeadTypeID 
		WHERE gfc12.FunctionTypeID = 12 
		AND gfc12.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM GroupRightsDynamic r 
			WHERE r.ClientPersonnelAdminGroupID = gfc12.ClientPersonnelAdminGroupID 
			AND r.FunctionTypeID = 12 -- Pages 
			AND r.LeadTypeID = lt.LeadTypeID 
			AND r.ObjectID = realDFP.DetailFieldPageID 
		)


		/*
			Insert EventType rights that should exist but don't.
			Wherever the parent level (gfc 13) says it has descendants, each EventType
			for that leadtype should be represented at the child level (grd 13).
			Add in each EventType that doesn't have a grd 13 entry.
		*/
		INSERT INTO GroupRightsDynamic(ClientPersonnelAdminGroupID, FunctionTypeID, LeadTypeID, ObjectID, RightID) 
		SELECT gfc13.ClientPersonnelAdminGroupID, gfc13.FunctionTypeID, gfc13.LeadTypeID, realEvent.EventTypeID, 0
		FROM GroupFunctionControl gfc13 
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc13.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		INNER JOIN LeadType lt ON lt.ClientID = cpg.ClientID AND gfc13.LeadTypeID = lt.LeadTypeID
		INNER JOIN EventType realEvent ON realEvent.LeadTypeID = lt.LeadTypeID 
		WHERE gfc13.FunctionTypeID = 13 
		AND gfc13.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM GroupRightsDynamic r 
			WHERE r.ClientPersonnelAdminGroupID = gfc13.ClientPersonnelAdminGroupID 
			AND r.FunctionTypeID = 13 -- EventTypes 
			AND r.LeadTypeID = lt.LeadTypeID 
			AND r.ObjectID = realEvent.EventTypeID 
		)


		/*
			Insert Encrypted Field rights that should exist but don't.
			Wherever the parent level (gfc 25) says it has descendants, each Encrypted Field
			for that leadtype should be represented at the child level (grd 25).
			Add in each Encrypted Field that doesn't have a grd 25 entry.
		*/
		INSERT INTO GroupRightsDynamic(ClientPersonnelAdminGroupID, FunctionTypeID, LeadTypeID, ObjectID, RightID) 
		SELECT gfc25.ClientPersonnelAdminGroupID, gfc25.FunctionTypeID, gfc25.LeadTypeID, realDF.DetailFieldID, 0
		FROM GroupFunctionControl gfc25 
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc25.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		INNER JOIN LeadType lt ON lt.ClientID = cpg.ClientID AND gfc25.LeadTypeID = lt.LeadTypeID
		INNER JOIN DetailFields realDF ON realDF.LeadTypeID = lt.LeadTypeID AND realDF.Encrypt = 1 
		WHERE gfc25.FunctionTypeID = 25 
		AND gfc25.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM GroupRightsDynamic r 
			WHERE r.ClientPersonnelAdminGroupID = gfc25.ClientPersonnelAdminGroupID 
			AND r.FunctionTypeID = 25 -- Encrypted Fields 
			AND r.LeadTypeID = lt.LeadTypeID 
			AND r.ObjectID = realDF.DetailFieldID 
		)


		/*
			Insert lead type rights that should exist but don't.
			Wherever the parent level (gfc 7) says it has descendants, each lead 
			type for the client should be represented at the child level (gfc 23).
			Add in each lead type that doesn't have a gfc 23 entry.
			JWG 2011-07-21 Include the client zero lead types for CustomerDetailValues etc
		*/
		INSERT INTO UserFunctionControl(ClientPersonnelID, ModuleID, FunctionTypeID, HasDescendants, RightID, LeadTypeID)  
		SELECT ufc7.ClientPersonnelID, 1, 23, 0, 0, lt.LeadTypeID
		FROM UserFunctionControl ufc7 
		INNER JOIN ClientPersonnel cpg ON ufc7.ClientPersonnelID = cpg.ClientPersonnelID 
		INNER JOIN LeadType lt ON lt.ClientID IN (cpg.ClientID, 0) 
		WHERE ufc7.FunctionTypeID = 7 
		AND ufc7.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * FROM UserFunctionControl ufc23 
			WHERE ufc23.ClientPersonnelID = ufc7.ClientPersonnelID 
			AND ufc23.FunctionTypeID = 23 
			AND ufc23.LeadTypeID = lt.LeadTypeID 
		)

		/*
			Create missing Pages control records.
		*/
		INSERT userfunctioncontrol (ClientPersonnelID, moduleid, functiontypeid, hasdescendants, rightid, leadtypeid)
		SELECT ufc_23.ClientPersonnelID, ufc_23.moduleid, 12, 1, 0, ufc_23.leadtypeid
		FROM dbo.userfunctioncontrol ufc_23  
		LEFT JOIN dbo.userfunctioncontrol ufc_12_13_25 ON ufc_12_13_25.ClientPersonnelID = ufc_23.ClientPersonnelID 
			AND ufc_12_13_25.functiontypeid = 12 
			AND ufc_12_13_25.leadtypeid = ufc_23.leadtypeid
		WHERE ufc_23.functiontypeid = 23 
		AND ufc_23.hasdescendants = 1 
		AND ufc_12_13_25.userfunctioncontrolid IS NULL

		/*
			Create missing Events control records.
		*/
		INSERT userfunctioncontrol (ClientPersonnelID, moduleid, functiontypeid, hasdescendants, rightid, leadtypeid)
		SELECT ufc_23.ClientPersonnelID, ufc_23.moduleid, 13, 1, 0, ufc_23.leadtypeid
		FROM dbo.userfunctioncontrol ufc_23  
		LEFT JOIN dbo.userfunctioncontrol ufc_12_13_25 ON ufc_12_13_25.ClientPersonnelID = ufc_23.ClientPersonnelID 
			AND ufc_12_13_25.functiontypeid = 13 
			AND ufc_12_13_25.leadtypeid = ufc_23.leadtypeid
		WHERE ufc_23.functiontypeid = 23 
		AND ufc_23.hasdescendants = 1 
		AND ufc_12_13_25.userfunctioncontrolid IS NULL

		/*
			Create missing Encrypted Fields control records.
		*/
		INSERT userfunctioncontrol (ClientPersonnelID, moduleid, functiontypeid, hasdescendants, rightid, leadtypeid)
		SELECT ufc_23.ClientPersonnelID, ufc_23.moduleid, 25, 1, 0, ufc_23.leadtypeid
		FROM dbo.userfunctioncontrol ufc_23  
		LEFT JOIN dbo.userfunctioncontrol ufc_12_13_25 ON ufc_12_13_25.ClientPersonnelID = ufc_23.ClientPersonnelID 
			AND ufc_12_13_25.functiontypeid = 25 
			AND ufc_12_13_25.leadtypeid = ufc_23.leadtypeid
		WHERE ufc_23.functiontypeid = 23 
		AND ufc_23.hasdescendants = 1 
		AND ufc_12_13_25.userfunctioncontrolid IS NULL


		/*
			Check for function control records that claim to have no descendants,
			whereas some child records do exist.
		*/
		UPDATE dbo.userfunctioncontrol 
		SET HasDescendants = 1 
		FROM dbo.userfunctioncontrol ufc
		WHERE ufc.functiontypeid IN (12, 13, 25)
		AND ufc.hasdescendants = 0 
		AND EXISTS (
			SELECT * 
			FROM dbo.userrightsdynamic urd (NOLOCK) 
			WHERE urd.ClientPersonnelID = ufc.ClientPersonnelID 
			AND urd.functiontypeid = ufc.functiontypeid 
			AND urd.leadtypeid = ufc.leadtypeid 
		) 



		/*
			Insert office rights that should exist but don't.
			Wherever the parent level (gfc 11) says it has descendants, each office
			for that leadtype should be represented at the child level (grd 11).
			Add in each office that doesn't have a grd 11 entry.
		*/
		INSERT INTO UserRightsDynamic(ClientPersonnelID, FunctionTypeID, LeadTypeID, ObjectID, RightID) 
		SELECT ufc11.ClientPersonnelID, ufc11.FunctionTypeID, lt.LeadTypeID, co.ClientOfficeID, 0
		FROM UserFunctionControl ufc11 
		INNER JOIN ClientPersonnel cpg ON ufc11.ClientPersonnelID = cpg.ClientPersonnelID 
		INNER JOIN LeadType lt ON lt.ClientID = cpg.ClientID 
		INNER JOIN ClientOffices co ON co.ClientID = cpg.ClientID 
		WHERE ufc11.FunctionTypeID = 11 
		AND ufc11.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM UserRightsDynamic r 
			WHERE r.ClientPersonnelID = ufc11.ClientPersonnelID 
			AND r.FunctionTypeID = 11 -- Offices 
			AND r.LeadTypeID = lt.LeadTypeID 
			AND r.ObjectID = co.ClientOfficeID 
		)

		/*
			Insert page rights that should exist but don't.
			Wherever the parent level (ufc 12) says it has descendants, each page
			for that leadtype should be represented at the child level (urd 12).
			Add in each page that doesn't have a urd 12 entry.
		*/
		INSERT INTO UserRightsDynamic(ClientPersonnelID, FunctionTypeID, LeadTypeID, ObjectID, RightID) 
		SELECT ufc12.ClientPersonnelID, ufc12.FunctionTypeID, ufc12.LeadTypeID, realDFP.DetailFieldPageID, 0
		FROM UserFunctionControl ufc12 
		INNER JOIN ClientPersonnel cpg ON ufc12.ClientPersonnelID = cpg.ClientPersonnelID 
		INNER JOIN LeadType lt ON lt.ClientID = cpg.ClientID AND ufc12.LeadTypeID = lt.LeadTypeID 
		INNER JOIN DetailFieldPages realDFP ON realDFP.LeadTypeID = lt.LeadTypeID 
		WHERE ufc12.FunctionTypeID = 12 
		AND ufc12.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM UserRightsDynamic r 
			WHERE r.ClientPersonnelID = ufc12.ClientPersonnelID 
			AND r.FunctionTypeID = 12 -- Pages 
			AND r.LeadTypeID = lt.LeadTypeID 
			AND r.ObjectID = realDFP.DetailFieldPageID 
		)


		/*
			Insert EventType rights that should exist but don't.
			Wherever the parent level (ufc 13) says it has descendants, each EventType
			for that leadtype should be represented at the child level (urd 13).
			Add in each EventType that doesn't have a urd 13 entry.
		*/
		INSERT INTO UserRightsDynamic(ClientPersonnelID, FunctionTypeID, LeadTypeID, ObjectID, RightID) 
		SELECT ufc13.ClientPersonnelID, ufc13.FunctionTypeID, ufc13.LeadTypeID, realEvent.EventTypeID, 0
		FROM UserFunctionControl ufc13 
		INNER JOIN ClientPersonnel cpg ON ufc13.ClientPersonnelID = cpg.ClientPersonnelID 
		INNER JOIN LeadType lt ON lt.ClientID = cpg.ClientID AND ufc13.LeadTypeID = lt.LeadTypeID
		INNER JOIN EventType realEvent ON realEvent.LeadTypeID = lt.LeadTypeID 
		WHERE ufc13.FunctionTypeID = 13 
		AND ufc13.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM UserRightsDynamic r 
			WHERE r.ClientPersonnelID = ufc13.ClientPersonnelID 
			AND r.FunctionTypeID = 13 -- EventTypes 
			AND r.LeadTypeID = lt.LeadTypeID 
			AND r.ObjectID = realEvent.EventTypeID 
		)


		/*
			Insert Encrypted Field rights that should exist but don't.
			Wherever the parent level (ufc 25) says it has descendants, each Encrypted Field
			for that leadtype should be represented at the child level (urd 25).
			Add in each Encrypted Field that doesn't have a urd 25 entry.
		*/
		INSERT INTO UserRightsDynamic(ClientPersonnelID, FunctionTypeID, LeadTypeID, ObjectID, RightID) 
		SELECT ufc25.ClientPersonnelID, ufc25.FunctionTypeID, ufc25.LeadTypeID, realDF.DetailFieldID, 0
		FROM UserFunctionControl ufc25 
		INNER JOIN ClientPersonnel cpg ON ufc25.ClientPersonnelID = cpg.ClientPersonnelID 
		INNER JOIN LeadType lt ON lt.ClientID = cpg.ClientID AND ufc25.LeadTypeID = lt.LeadTypeID
		INNER JOIN DetailFields realDF ON realDF.LeadTypeID = lt.LeadTypeID AND realDF.Encrypt = 1 
		WHERE ufc25.FunctionTypeID = 25 
		AND ufc25.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM UserRightsDynamic r 
			WHERE r.ClientPersonnelID = ufc25.ClientPersonnelID 
			AND r.FunctionTypeID = 25 -- Encrypted Fields 
			AND r.LeadTypeID = lt.LeadTypeID 
			AND r.ObjectID = realDF.DetailFieldID 
		)

		/*
			Insert Outcomes that should exist but don't.
			Wherever the parent level (gfc 9) says it has descendants, each Outcome
			for that Client should be represented at the child level (grd 9).
			Add in each Outcome that doesn't have a grd 9 entry.
		*/
		INSERT INTO GroupRightsDynamic(ClientPersonnelAdminGroupID, FunctionTypeID, LeadTypeID, ObjectID, RightID) 
		SELECT gfc9.ClientPersonnelAdminGroupID, gfc9.FunctionTypeID, NULL, co.OutcomeID, gfc9.RightID
		FROM GroupFunctionControl gfc9 
		INNER JOIN ClientPersonnelAdminGroups cpg ON gfc9.ClientPersonnelAdminGroupID = cpg.ClientPersonnelAdminGroupID 
		INNER JOIN Outcomes co ON co.ClientID = cpg.ClientID 
		WHERE gfc9.FunctionTypeID = 9 
		AND gfc9.HasDescendants = 1 
		AND NOT EXISTS (
			SELECT * 
			FROM GroupRightsDynamic r 
			WHERE r.ClientPersonnelAdminGroupID = gfc9.ClientPersonnelAdminGroupID 
			AND r.FunctionTypeID = 9 -- Outcomes 
			AND r.LeadTypeID IS NULL 
			AND r.ObjectID = co.OutcomeID 
		)


	END
END






GO
GRANT VIEW DEFINITION ON  [dbo].[CreateMissingRights] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CreateMissingRights] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CreateMissingRights] TO [sp_executeall]
GO
