SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Cathal Sherry
-- Create date:	2017-08-17
-- Description:	Look for all functions required by the rules engine, for which there are no overrides then calculate each.
-- 2018-07-04 CPS updated to allow @RuleSetID to be NULL.  This stored procedure will be called as part of the fast rater "Prepare" step
-- 2019-10-13 GPR created to 603 for LPC-35
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C00_RulesEngine_GetAllFunctionsForOverrides]
(
	 @RuleSetID		INT
	,@CustomerID	INT
	,@LeadID		INT
	,@CaseID		INT
	,@MatterID		INT
	,@Overrides		dbo.tvpIntVarcharVarchar READONLY
	,@XmlResponse	XML	OUTPUT -- because you can't nest an insert...exec statement :(
)
AS
BEGIN

	PRINT CHAR(13)+CHAR(10) + OBJECT_NAME(@@ProcID) + ' ' + ISNULL(CONVERT(VARCHAR,@RuleSetID),'NULL')

	SET NOCOUNT ON;

	DECLARE  @FunctionsToCall	TABLE ( FunctionName VARCHAR(2000) NOT NULL, Result VARCHAR(2000) NULL, Complete BIT NOT NULL, DataTypeID INT NOT NULL )
	DECLARE  @RowCount			INT
			,@ClientID			INT = dbo.fnGetPrimaryClientID()

	/*Recursive function to pick up all functions for this rule set*/
	/*If @RuleSetID is passed in as NULL, just get all functions*/
	IF @RuleSetID IS NULL
	BEGIN
		INSERT @FunctionsToCall ( FunctionName, Complete, DataTypeID )
		SELECT ps.Value, 0, ps.DataTypeID
		FROM dbo.RulesEngine_RuleParameters_PreSet ps WITH ( NOLOCK ) 
		WHERE ps.ParameterTypeID = 4 /*Function*/
	END
	ELSE
	BEGIN
		;WITH AllParams as
		(
		SELECT rp.ParameterTypeID, CASE WHEN rp.ParameterTypeID = 3 /*RuleSet*/ THEN CAST(rp.Value as INT) ELSE r.RuleSetID END [RuleSetID], rp.RuleID, rp.RuleParameterID, rp.Name, rp.Value, rp.DataTypeID
		FROM dbo.RulesEngine_Rules r WITH ( NOLOCK )
		INNER JOIN dbo.RulesEngine_RuleParameters rp WITH ( NOLOCK ) on rp.RuleID = r.RuleID
		WHERE r.RuleSetID = @RuleSetID
		AND rp.ParameterTypeID IN ( 3,4 ) /*RuleSet , Function*/
			UNION ALL
		SELECT rp.ParameterTypeID, CASE WHEN rp.ParameterTypeID = 3 /*RuleSet*/ THEN CAST(rp.Value as INT) ELSE r.RuleSetID END [RuleSetID], rp.RuleID, rp.RuleParameterID, rp.Name, rp.Value, rp.DataTypeID
		FROM AllParams ap 
		INNER JOIN dbo.RulesEngine_Rules r WITH ( NOLOCK ) on r.RuleSetID = ap.RuleSetID
		INNER JOIN dbo.RulesEngine_RuleParameters rp WITH ( NOLOCK ) on rp.RuleID = r.RuleID
		WHERE rp.ParameterTypeID IN ( 3,4 ) /*RuleSet , Function*/
		AND ap.ParameterTypeID = 3 /*Rule Set*/
		)
		INSERT @FunctionsToCall ( FunctionName, Complete, DataTypeID )
		SELECT DISTINCT ap.Value, 0, ap.DataTypeID
		FROM AllParams ap 
		WHERE ap.ParameterTypeID = 4 /*Function*/
		and not exists ( SELECT * FROM @Overrides o WHERE o.AnyValue1 = ap.Value )
	END
    
	/*Loop through each function, execute it, and store the result*/
	WHILE EXISTS ( SELECT * FROM @FunctionsToCall ftc WHERE ftc.Complete = 0 AND ftc.FunctionName <> '' )
	BEGIN
		DECLARE  @FunctionName	VARCHAR(2000)
				,@Sql			NVARCHAR(2000)
				,@Params		NVARCHAR(MAX)
				,@ValueOut		VARCHAR(2000)
				,@DataTypeID	INT


		SELECT TOP (1)   @FunctionName  = ftc.FunctionName
						,@DataTypeID	= ftc.DataTypeID
		FROM @FunctionsToCall ftc
		WHERE ftc.Complete = 0
		ORDER BY ftc.FunctionName
	
		SELECT	@Sql = 'SELECT @ValueOUT = dbo.' + ISNULL(@FunctionName,'') + '(@CustomerID, @LeadID, @CaseID, @MatterID, @Overrides)',
				@Params = N'@ValueOUT VARCHAR(2000) OUTPUT, @CustomerID INT, @LeadID INT, @CaseID INT, @MatterID INT, @Overrides dbo.tvpIntVarcharVarchar READONLY'	

		IF @DataTypeID = 5 -- DateTime
		BEGIN

			SELECT @Sql = 'SELECT @ValueOUT = CONVERT(VARCHAR, ' + ISNULL(@FunctionName,'') + ', 120)'

		END

		PRINT @SQL
		EXEC sp_executesql @Sql, @Params, @ValueOUT = @ValueOut OUTPUT, @CustomerID = @CustomerID, @LeadID = @LeadID, @CaseID = @CaseID, @MatterID = @MatterID, @Overrides = @Overrides
			
		UPDATE ftc
		SET Complete = 1, Result = @ValueOut
		FROM @FunctionsToCall ftc
		WHERE ftc.FunctionName = @FunctionName
		
		SELECT @FunctionName = NULL
	END

	SELECT @XmlResponse = 
	(
	SELECT 4 [ParameterTypeID], ftc.FunctionName [AnyValue1], ftc.Result [AnyValue2]
	FROM @FunctionsToCall ftc
	FOR Xml Auto
	)
	
	IF @XmlResponse is not NULL
	BEGIN
		SELECT @RuleSetID = ISNULL(@RuleSetID,-1)
		EXEC dbo._C00_LogItXML @ClientID, @RuleSetID, '_C00_RulesEngine_GetAllFunctionsForOverrides', @XmlResponse
	END

	SELECT @RowCount = @@ROWCOUNT

	RETURN @RowCount

END















GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RulesEngine_GetAllFunctionsForOverrides] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_RulesEngine_GetAllFunctionsForOverrides] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RulesEngine_GetAllFunctionsForOverrides] TO [sp_executeall]
GO
