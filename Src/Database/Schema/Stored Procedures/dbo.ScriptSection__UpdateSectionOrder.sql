SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 13-03-2015
-- Description:	Updates the section order for the given section
-- =============================================
CREATE PROCEDURE [dbo].[ScriptSection__UpdateSectionOrder]
	@ScriptSectionID INT,
	@SectionOrder INT
AS
BEGIN
		
	SET NOCOUNT ON;

	UPDATE ScriptSection
	SET SectionOrder=@SectionOrder
	WHERE ScriptSectionID=@ScriptSectionID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection__UpdateSectionOrder] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSection__UpdateSectionOrder] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection__UpdateSectionOrder] TO [sp_executeall]
GO
