SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-08-05
-- Description:	SP to output tables INTO html
-- JWG 2011-01-11 Added @Client/ClientPersonnel/ContactIDForDV AND reformatted sql.
-- ACE 2014-09-09 Added _C00_HTMLFromSQL
-- =============================================
CREATE PROCEDURE [dbo].[_C00_HTMLFromTable]
	@CustomerID INT = NULL,
	@LeadID INT, 
	@CaseID INT = NULL,
	@MatterID INT, 
	@TableDetailFieldID INT, 
	@ColList VARCHAR(1000),
	@TableProperties VARCHAR(250) = 'border="0" cellpadding="3" width="90%" align="center"',
	@Header BIT = 1,
	@WhereClause VARCHAR(MAX) = NULL, 
	@ClientIDForDV INT = NULL, 
	@ClientPersonnelIDForDV INT = NULL, 
	@ContactIDForDV INT = NULL,
	@PrintSQL BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS (
		SELECT *
		FROM CustomTableSQL c
		WHERE c.DetailFieldID = @TableDetailFieldID
	)
	BEGIN
	
		EXEC dbo._C00_HTMLFromSQL @CustomerID, @LeadID, @CaseID, @MatterID, @TableDetailFieldID, @ClientIDForDV, @ClientPersonnelIDForDV, @ContactIDForDV, @PrintSQL

		RETURN

	END
	
	
	IF @TableDetailFieldID IN (133732)
	BEGIN
	
		RETURN
	
	END

	/* Construct HTML Table as system doesnt provide functionality to do so. */
	DECLARE @FieldstoKeep TABLE(
		SortedOrder INT IDENTITY (1,1),
		DetailFieldID INT
	)

	DECLARE @TableRowsKeep TABLE(
		TableRowID INT
	)


	DECLARE @Html VARCHAR(MAX), 
			@TableRowID INT, 
			@TestID INT, 
			@ResourceListID INT, 
			@TableDetailFieldPageID INT, 
			@ResourceListDetailFieldPageID INT,
			@ResourceListFieldOrder INT = 0,
			@RLFirstIndex INT = 0,
			@RLLastIndex INT = 0,
			@ColWidth INT,
			@SQLQuery VARCHAR(MAX)

	SELECT @TableDetailFieldPageID = df.TableDetailFieldPageID
	FROM dbo.DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @TableDetailFieldID

	SELECT @ResourceListDetailFieldPageID = df.ResourceListDetailFieldPageID,
			@ResourceListFieldOrder=df.FieldOrder
	FROM dbo.DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldPageID = @TableDetailFieldPageID
	AND df.QuestionTypeID = 14

	INSERT INTO @FieldstoKeep (DetailFieldID)
	SELECT df.DetailFieldID
	FROM dbo.DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldPageID = @TableDetailFieldPageID 
	AND df.[Enabled] = 1
	AND df.QuestionTypeID <> 14
	AND df.FieldOrder < @ResourceListFieldOrder
	ORDER BY df.FieldOrder

	INSERT INTO @FieldstoKeep (DetailFieldID)
	SELECT df.DetailFieldID
	FROM dbo.DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldPageID = @ResourceListDetailFieldPageID 
	AND df.[Enabled] = 1
	ORDER BY df.FieldOrder

	INSERT INTO @FieldstoKeep (DetailFieldID)
	SELECT df.DetailFieldID
	FROM dbo.DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldPageID = @TableDetailFieldPageID 
	AND df.[Enabled] = 1
	AND df.QuestionTypeID <> 14
	AND df.FieldOrder > @ResourceListFieldOrder
	ORDER BY df.FieldOrder

	DELETE ftk
	FROM @FieldstoKeep ftk
	WHERE NOT EXISTS  (
		SELECT *
		FROM dbo.fnTableOfIDsFromCSV (@ColList) tid 
		WHERE tid.AnyID = ftk.SortedOrder
	)

	SELECT @RLLastIndex=ISNULL(MAX(ftk.SortedOrder),0),@RLFirstIndex=ISNULL(MIN(ftk.SortedOrder),0)
	FROM @FieldstoKeep ftk
	INNER JOIN dbo.ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.DetailFieldID = ftk.DetailFieldID
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = ftk.DetailFieldID AND df.DetailFieldPageID=@ResourceListDetailFieldPageID 

	SELECT @ColWidth = 100/(SELECT COUNT(*) FROM @FieldstoKeep)

	IF @WhereClause IS NULL OR @WhereClause = ''
	BEGIN

		/* Matter Table*/
		IF @MatterID > 0
		BEGIN

			INSERT INTO @TableRowsKeep (TableRowID)
			SELECT tr.TableRowID 
			FROM dbo.TableRows tr WITH (NOLOCK)
			WHERE tr.MatterID = @MatterID 
			AND tr.DetailFieldID = @TableDetailFieldID
			ORDER BY tr.TableRowID
		
		END
		ELSE
		/* Case Table*/
		IF @CaseID > 0
		BEGIN

			INSERT INTO @TableRowsKeep (TableRowID)
			SELECT tr.TableRowID 
			FROM dbo.TableRows tr WITH (NOLOCK)
			WHERE tr.CaseID = @CaseID 
			AND tr.DetailFieldID = @TableDetailFieldID
			ORDER BY tr.TableRowID
		
		END
		ELSE
		/* Lead Table*/
		IF @LeadID > 0
		BEGIN

			INSERT INTO @TableRowsKeep (TableRowID)
			SELECT tr.TableRowID 
			FROM dbo.TableRows tr WITH (NOLOCK)
			WHERE tr.LeadID = @LeadID 
			AND tr.DetailFieldID = @TableDetailFieldID
			ORDER BY tr.TableRowID
		
		END
		ELSE
		/* Customer Table*/
		IF @CustomerID > 0
		BEGIN

			INSERT INTO @TableRowsKeep (TableRowID)
			SELECT tr.TableRowID 
			FROM dbo.TableRows tr WITH (NOLOCK)
			WHERE tr.CustomerID = @CustomerID 
			AND tr.DetailFieldID = @TableDetailFieldID
			ORDER BY tr.TableRowID
		
		END
		/* Client Table*/
		IF @ClientIDForDV > 0
		BEGIN

			INSERT INTO @TableRowsKeep (TableRowID)
			SELECT tr.TableRowID 
			FROM dbo.TableRows tr WITH (NOLOCK)
			WHERE tr.ClientID = @ClientIDForDV
			AND tr.DetailFieldID = @TableDetailFieldID
			ORDER BY tr.TableRowID
		
		END
		ELSE
		/* ClientPersonnel Table*/
		IF @ClientPersonnelIDForDV > 0
		BEGIN

			INSERT INTO @TableRowsKeep (TableRowID)
			SELECT tr.TableRowID 
			FROM dbo.TableRows tr WITH (NOLOCK)
			WHERE tr.ClientPersonnelID = @ClientPersonnelIDForDV
			AND tr.DetailFieldID = @TableDetailFieldID
			ORDER BY tr.TableRowID
		
		END
		ELSE
		/* Contact Table*/
		IF @ContactIDForDV > 0
		BEGIN

			INSERT INTO @TableRowsKeep (TableRowID)
			SELECT tr.TableRowID 
			FROM dbo.TableRows tr WITH (NOLOCK)
			WHERE tr.ContactID = @ContactIDForDV
			AND tr.DetailFieldID = @TableDetailFieldID
			ORDER BY tr.TableRowID
		
		END
		
	END
	ELSE
	BEGIN
		
		SELECT @WhereClause = CASE @WhereClause 
		WHEN 'ECT1' /* Only output items WHERE the "PRINT Item" has been ticked */
			THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 136570 AND tdv.ValueInt = 1'
		WHEN 'Q1' /* Only output items WHERE the "PRINT Item" has been ticked */
			THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 111435 AND tdv.ValueInt = 1'
		WHEN 'Q2' 
			THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 114684 
			INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tr.TableRowID AND tdv1.DetailFieldID = 114693 AND tdv1.ValueDate = dbo.fnDateOnlyChar (dbo.fn_GetDate_Local()) 
			INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = tdv.MatterID AND mdv.DetailFieldID = 113507
			INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = mdv.ValueInt AND rldv.detailfieldid = 113500 AND tdv.ValueMoney > rldv.ValueMoney
			'
		WHEN 'Q3' 
			THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 114684 
			INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tr.TableRowID AND tdv1.DetailFieldID = 114693 AND tdv1.ValueDate < dbo.fnDateOnlyChar (dbo.fn_GetDate_Local()) 
			INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = tdv.MatterID AND mdv.DetailFieldID = 113507
			INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = mdv.ValueInt AND rldv.detailfieldid = 113500 AND tdv.ValueMoney > rldv.ValueMoney
			'
		WHEN 'Q4' /* Invoicing Table only show resords for today */
			THEN 'INNER JOIN TableDetailValues tdv_date WITH (NOLOCK) on tdv_date.TableRowID = tr.TableRowID AND tdv_date.DetailFieldID = 116281 AND tdv_date.ValueDate = convert(date,dbo.fn_GetDate_Local())'
		WHEN 'Q5' /* Invoicing Table only show resords for today */
			THEN 'INNER JOIN TableDetailValues tdv_date WITH (NOLOCK) on tdv_date.TableRowID = tr.TableRowID AND tdv_date.DetailFieldID = 114693 AND tdv_date.ValueDate = convert(date,dbo.fn_GetDate_Local()-1)'
		WHEN 'Q6' /* Invoicing Table only show resords for today */
			THEN 'INNER JOIN TableDetailValues tdv_date WITH (NOLOCK) on tdv_date.TableRowID = tr.TableRowID AND tdv_date.DetailFieldID = 116751 AND tdv_date.ValueDate = convert(date,dbo.fn_GetDate_Local()-1)'
		WHEN 'A1' /* Last Record */
			THEN 'INNER JOIN TableRows tr2 WITH (NOLOCK) on tr2.TableRowID = tr.TableRowID AND NOT EXISTS  (SELECT * FROM TableRows tr3 WHERE tr3.TableRowID > tr.TableRowID AND tr3.DetailFieldID = tr.DetailFieldID AND tr3.LeadID = tr.LeadID)'
		WHEN 'C185-1' /* Include invoice line if due and type referral */
			THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 119853 and tdv.ValueDate is not null
			INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tr.TableRowID and tdv1.DetailFieldID = 119857 and tdv1.ValueDate is null
			INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = tr.TableRowID and tdv2.DetailFieldID = 119851 and tdv2.ValueInt = 24871
			'
		WHEN 'C185-2' /* Include invoice line if due and type settlement */
			THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 119853 and tdv.ValueDate is not null 
			INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tr.TableRowID and tdv1.DetailFieldID = 119857 and tdv1.ValueDate is null
			INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = tr.TableRowID and tdv2.DetailFieldID = 119851 and tdv2.ValueInt = 24872
			'
		WHEN 'C184-1' /* Include invoice line if due & referral or settlement LEGAL */
			THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 118959 and (tdv.ValueDate <= convert(varchar(10),dbo.fn_GetDate_Local(),120) and tdv.ValueDate > DATEADD(DAY,-7,dbo.fn_GetDate_Local()))
			INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = tr.TableRowID and tdv2.DetailFieldID = 118995 and tdv2.ValueInt in (24577,24578)
			INNER JOIN TableDetailValues tdv3 WITH (NOLOCK) ON tdv3.TableRowID = tr.TableRowID and tdv3.DetailFieldID = 122986 and tdv3.ValueInt = 24473
			'
		WHEN 'C184-2' /* Include invoice line if due and type service */
			THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 118959 and (tdv.ValueDate <= convert(varchar(10),dbo.fn_GetDate_Local(),120) and tdv.ValueDate > DATEADD(DAY,-7,dbo.fn_GetDate_Local()))
			INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = tr.TableRowID and tdv2.DetailFieldID = 118995 and tdv2.ValueInt = 25674
			'
		WHEN 'C184-3' /* Include invoice line if due & referral or settlement RECLAIM */
			THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 118959  and (tdv.ValueDate <= convert(varchar(10),dbo.fn_GetDate_Local(),120) and tdv.ValueDate > DATEADD(DAY,-7,dbo.fn_GetDate_Local()))
			INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = tr.TableRowID and tdv2.DetailFieldID = 118995 and tdv2.ValueInt in (24577,24578)
			INNER JOIN TableDetailValues tdv3 WITH (NOLOCK) ON tdv3.TableRowID = tr.TableRowID and tdv3.DetailFieldID = 122986 and tdv3.ValueInt = 24472
			'
		WHEN 'VHM1' /* Include in statement of facts if selected (CMU Line Items) */
			THEN 'INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 120454 and tdv.ValueDate IS NOT NULL
			INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tr.TableRowID and tdv1.DetailFieldID = 120455 and tdv1.ValueDate IS NULL'
		WHEN 'C126-1' /* Show payments made last month */
			THEN 'INNER JOIN TableDetailValues tdv_date WITH (NOLOCK) on tdv_date.TableRowID = tr.TableRowID and tdv_date.DetailFieldID = 112791 and DATEDIFF(mm,tdv_date.ValueDate,dbo.fn_GetDate_Local()) = 1'
		WHEN 'A1' /* Aims - Only records from this invoice */
			THEN 'INNER JOIN TableDetailValues tdv_invno WITH (NOLOCK) on tdv_invno.TableRowID = tr.TableRowID and tdv_invno.DetailFieldID = 129546
			INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = tdv_invno.MatterID and mdv.DetailFieldID = 129894 and mdv.ValueInt = tdv_invno.ValueInt'
		WHEN 'C210-1' /* CDC CDC invoice lines for current invoice number */
			THEN 'INNER JOIN TableDetailValues tdv_invno WITH (NOLOCK) on tdv_invno.TableRowID = tr.TableRowID and tdv_invno.DetailFieldID = 135826
			INNER JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.LeadID = tdv_invno.LeadID and ldv.DetailFieldID = 135857 and ldv.ValueInt = tdv_invno.ValueInt'
		WHEN 'C210-2' /* CDC SC invoice lines for current invoice number */
			THEN 'INNER JOIN TableDetailValues tdv_invno WITH (NOLOCK) on tdv_invno.TableRowID = tr.TableRowID and tdv_invno.DetailFieldID = 135422
			INNER JOIN LeadDetailValues ldv WITH (NOLOCK) ON ldv.LeadID = tdv_invno.LeadID and ldv.DetailFieldID = 135857 and ldv.ValueInt = tdv_invno.ValueInt'
		WHEN 'C1' /* Only return Passengers */
			THEN 'INNER JOIN TableDetailValues tdv_pass WITH (NOLOCK) on tdv_pass.TableRowID = tr.TableRowID and tdv_pass.DetailFieldID = 140051 and tdv_pass.detailvalue = ''Passenger'''
		WHEN 'C1' /* Only return Witnesses */
			THEN 'INNER JOIN TableDetailValues tdv_pass WITH (NOLOCK) on tdv_pass.TableRowID = tr.TableRowID and tdv_pass.DetailFieldID = 140050 and tdv_pass.ValueInt = 39936'
		WHEN 'C211-1' /*Ecclesiastical show only invoices with "print on statment" ticked*/
			THEN 'INNER JOIN TableDetailValues tdv_print WITH (NOLOCK) ON tdv_print.TableRowID = tr.TableRowID and tdv_print.DetailFieldID = 146410 and tdv_print.ValueInt = 1'
		WHEN 'C211-2' /*Ecclesiastical only pull rows from the most recent invoice*/
			THEN 'INNER JOIN TableDetailValues tdv_invoice WITH (NOLOCK) on tdv_invoice.TableRowID = tr.TableRowID and tdv_invoice.DetailFieldID = 136822
			INNER JOIN MatterDetailValues mdv_active WITH (NOLOCK) on mdv_active.MatterID = tr.MatterID and mdv_active.DetailFieldID = 147020 and tdv_invoice.ValueInt = mdv_active.ValueInt'
		WHEN 'Q211-2' /*Ecclesiastical only pull rows from the most recent invoice - ACE 2012-03-05 Julie got the wrong code*/
			THEN 'INNER JOIN TableDetailValues tdv_invoice WITH (NOLOCK) on tdv_invoice.TableRowID = tr.TableRowID and tdv_invoice.DetailFieldID = 136822
			INNER JOIN MatterDetailValues mdv_active WITH (NOLOCK) on mdv_active.MatterID = tr.MatterID and mdv_active.DetailFieldID = 147020 and tdv_invoice.ValueInt = mdv_active.ValueInt'
		WHEN 'C173-1' /*Ecclesiastical show only invoices with "print on statment" ticked*/
			THEN 'INNER JOIN TableDetailValues tdv_print WITH (NOLOCK) ON tdv_print.TableRowID = tr.TableRowID and tdv_print.DetailFieldID = 152700 and tdv_print.ValueInt = 1'
		WHEN 'C173-2' /*Ecclesiastical only pull rows from the most recent invoice*/
			THEN 'INNER JOIN TableDetailValues tdv_invoice WITH (NOLOCK) on tdv_invoice.TableRowID = tr.TableRowID and tdv_invoice.DetailFieldID = 152645
			INNER JOIN MatterDetailValues mdv_active WITH (NOLOCK) on mdv_active.MatterID = tr.MatterID and mdv_active.DetailFieldID = 152664 and tdv_invoice.ValueInt = mdv_active.ValueInt'
		WHEN 'Q173-2' /*Ecclesiastical only pull rows from the most recent invoice - ACE 2012-03-05 Julie got the wrong code*/
			THEN 'INNER JOIN TableDetailValues tdv_invoice WITH (NOLOCK) on tdv_invoice.TableRowID = tr.TableRowID and tdv_invoice.DetailFieldID = 152645
			INNER JOIN MatterDetailValues mdv_active WITH (NOLOCK) on mdv_active.MatterID = tr.MatterID and mdv_active.DetailFieldID = 152664 and tdv_invoice.ValueInt = mdv_active.ValueInt'
		ELSE ''
		END
	
		SELECT @SQLQuery = 'SELECT tr.TableRowID 
		FROM tablerows tr WITH (NOLOCK)
		' + @WhereClause + CASE 
								WHEN @MatterID > 0 THEN ' WHERE tr.MatterID = ' + CONVERT(VARCHAR,@MatterID)   
								WHEN @CaseID > 0 THEN ' WHERE tr.CaseID = ' + CONVERT(VARCHAR,@CaseID) 
								WHEN @LeadID > 0 THEN ' WHERE tr.LeadID = ' + CONVERT(VARCHAR,@LeadID) 
								WHEN @CustomerID > 0 THEN ' WHERE tr.CustomerID = ' + CONVERT(VARCHAR,@CustomerID) 
								WHEN @ClientIDForDV > 0 THEN ' WHERE tr.ClientID = ' + CONVERT(VARCHAR, @ClientIDForDV) 
								WHEN @ClientPersonnelIDForDV > 0 THEN ' WHERE tr.ClientPersonnelID = ' + CONVERT(VARCHAR, @ClientPersonnelIDForDV) 
								WHEN @ContactIDForDV > 0 THEN ' WHERE tr.ContactID = ' + CONVERT(VARCHAR, @ContactIDForDV) 
							END  
		+ ' 
		AND tr.DetailFieldID = ' + CONVERT(VARCHAR,@TableDetailFieldID) + '
		ORDER BY tr.TableRowID'

		INSERT INTO @TableRowsKeep (TableRowID)
		EXEC (@SQLQuery)
		
	END

	IF @TableDetailFieldID = 140375
	BEGIN
		
		SELECT @TableProperties = 'border="0" cellpadding="1" width="90%" align="center" style="font-family:Courier New;"'
	
	END
	
	IF @TableDetailFieldID IN (152765, 148349)
	BEGIN
		
		SELECT @TableProperties = 'border="0" cellpadding="0" width="100%" align="center" style="font-family:Calibri;"'
	
	END

	SELECT @Html = '<table ' + @TableProperties + '>'

	IF @Header = 1
	BEGIN

		SELECT @Html = @Html + '<tr>'

		SELECT @Html = @Html + '<td width="' +  CONVERT(VARCHAR(100),@ColWidth) + '%"><b>' + df.FieldCaption + '</b></td>'
		FROM @FieldstoKeep ftk
		INNER JOIN DetailFields df ON df.DetailFieldID = ftk.DetailFieldID
	
		SELECT @Html = @Html + '</tr>'

	END

	SELECT TOP 1 @TableRowID = TableRowID, @TestID = 0
	FROM @TableRowsKeep tr
	ORDER BY TableRowID

	WHILE (@TableRowID IS NOT NULL AND @TableRowID > @TestID)
	BEGIN

		SELECT @Html = @Html + '<tr>'

		SELECT TOP 1 @ResourceListID = tdv.ResourceListID 
		FROM dbo.TableDetailValues tdv WITH (NOLOCK) 
		WHERE tdv.TableRowID = @TableRowID
		AND tdv.ResourceListID IS NOT NULL

		SELECT @Html = @Html + '<td width="' +  CONVERT(VARCHAR(100), @ColWidth) + '%">' + CASE WHEN df.QuestionTypeID IN (2,4) THEN ISNULL(luli.ItemValue, '') ELSE ISNULL(tdv.DetailValue, '') END + '</td>'
		FROM @FieldstoKeep ftk
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = ftk.DetailFieldID AND df.LeadOrMatter IN (6,8)
		LEFT JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = ftk.DetailFieldID AND tdv.tablerowid = @TableRowID
		LEFT JOIN dbo.LookupListItems luli WITH (NOLOCK) ON CONVERT(VARCHAR(10),luli.LookupListItemID) = tdv.ValueInt AND luli.LookupListID = df.LookupListID
		WHERE ftk.SortedOrder < @RLFirstIndex
		ORDER BY ftk.SortedOrder

		SELECT @Html = @Html + '<td width="' +  CONVERT(VARCHAR(100), @ColWidth) + '%">' + CASE WHEN df.QuestionTypeID IN (2,4) THEN ISNULL(luli.ItemValue, '') ELSE ISNULL(rldv.DetailValue, '') END + '</td>'
		FROM @FieldstoKeep ftk
		INNER JOIN dbo.ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.DetailFieldID = ftk.DetailFieldID AND rldv.ResourceListID = @ResourceListID
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = ftk.DetailFieldID 
		LEFT JOIN dbo.LookupListItems luli WITH (NOLOCK) ON CONVERT(VARCHAR(10),luli.LookupListItemID) = rldv.ValueInt AND luli.LookupListID = df.LookupListID

		SELECT @Html = @Html + '<td width="' +  CONVERT(VARCHAR(100), @ColWidth) + '%">' + CASE WHEN df.QuestionTypeID IN (2,4) THEN ISNULL(luli.ItemValue, '') ELSE ISNULL(tdv.DetailValue, '') END + '</td>'
		FROM @FieldstoKeep ftk
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = ftk.DetailFieldID AND df.LeadOrMatter IN (6,8)
		LEFT JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.DetailFieldID = ftk.DetailFieldID AND tdv.tablerowid = @TableRowID
		LEFT JOIN dbo.LookupListItems luli WITH (NOLOCK) ON CONVERT(VARCHAR(10),luli.LookupListItemID) = tdv.ValueInt AND luli.LookupListID = df.LookupListID
		WHERE ftk.SortedOrder > @RLFirstIndex
		ORDER BY ftk.SortedOrder

		SELECT @TestID = @TableRowID

		SELECT TOP 1 @TableRowID = TableRowID, @TestID = 0
		FROM @TableRowsKeep tr
		WHERE tr.TableRowID > @TableRowID
		ORDER BY tr.TableRowID

		SELECT @Html = @Html + '</tr>'

	END		

	SELECT @Html = @Html + '</table>'

	--IF @TableDetailFieldID = 114689
	--BEGIN

	--	SELECT @Html = @Html + ' '+ @WhereClause + 'TEST' 
	
	--END

	--IF @TableDetailFieldID=180164
	--BEGIN

	--	SELECT @Html = '';

	--	SELECT @Html = @Html + main.Product + main.MonthlyPrice FROM (
	--		SELECT tdvProduct.DetailValue Product, tdvMonthlyPrice.DetailValue MonthlyPrice
	--		FROM TableDetailValues tdvProduct WITH (NOLOCK) 				
	--		INNER JOIN dbo.TableDetailValues tdvMonthlyPrice WITH (NOLOCK) on tdvMonthlyPrice.DetailFieldID=180173 and tdvMonthlyPrice.tableRowID=tdvProduct.TableRowID 		
	--		WHERE tdvProduct.TableRowID IN (SELECT TableRowID FROM TableRows WITH (NOLOCK) WHERE DetailFieldID=180164 and leadID=@LeadID) AND tdvProduct.DetailFieldID=180172		
	--	) main

	--END


	SELECT @Html AS MyHtml

	IF @PrintSql = 1
	BEGIN
	
		PRINT @SQLQuery
	
	END

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_HTMLFromTable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_HTMLFromTable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_HTMLFromTable] TO [sp_executeall]
GO
