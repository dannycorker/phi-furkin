SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex ELger
-- Create date: 2010-06-07
-- Description:	Copy DetailFieldPage and Fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__CreateCopy]
(
@DetailFieldPageID int,
@UniqueIdent varchar(10)
)
	
AS
BEGIN

	SET NOCOUNT ON

	Declare @ClientID int, 
			@LeadOrMatter int, 
			@LeadTypeID int,
			@PageName varchar(50), 
			@PageCaption varchar(50),
			@PageOrder int,
			@Enabled bit,
			@ResourceList bit,
			@NewDetailFieldPageID int
	
	Select @ClientID = ClientID, @LeadOrMatter = LeadOrMatter, @LeadTypeID = LeadTypeID, @PageName = PageName, @PageCaption = PageCaption, @PageOrder = PageOrder, @Enabled = Enabled, @ResourceList = ResourceList
	From DetailFieldPages dfp WITH (NOLOCK)
	Where dfp.DetailFieldPageID = @DetailFieldPageID

	INSERT INTO dbo.DetailFieldPages (ClientID,LeadOrMatter,LeadTypeID,PageName,PageCaption,PageOrder,Enabled,ResourceList)
	Select @ClientID, @LeadOrMatter, @LeadTypeID, CONVERT(Varchar(50),@PageName + @UniqueIdent), CONVERT(Varchar(50),@PageCaption + @UniqueIdent), @PageOrder, @Enabled, @ResourceList

	Select @NewDetailFieldPageID = SCOPE_IDENTITY()

	Insert Into DetailFields (ClientID,LeadOrMatter,FieldName,FieldCaption,QuestionTypeID,Required,Lookup,LookupListID,LeadTypeID,Enabled,DetailFieldPageID,FieldOrder,MaintainHistory,EquationText,MasterQuestionID,FieldSize,LinkedDetailFieldID,ValidationCriteriaFieldTypeID,ValidationCriteriaID,MinimumValue,MaximumValue,RegEx,ErrorMessage,ResourceListDetailFieldPageID,TableDetailFieldPageID,DefaultFilter,ColumnEquationText,Editable,Hidden,LastReferenceInteger,ReferenceValueFormatID,Encrypt,ShowCharacters,NumberOfCharactersToShow,TableEditMode,DisplayInTableView)
	Select ClientID,LeadOrMatter,CONVERT(Varchar(50),FieldName + @UniqueIdent),CONVERT(Varchar(100),FieldCaption + @UniqueIdent),QuestionTypeID,Required,Lookup,LookupListID,LeadTypeID,Enabled,@NewDetailFieldPageID,FieldOrder,MaintainHistory,EquationText,MasterQuestionID,FieldSize,LinkedDetailFieldID,ValidationCriteriaFieldTypeID,ValidationCriteriaID,MinimumValue,MaximumValue,RegEx,ErrorMessage,ResourceListDetailFieldPageID,TableDetailFieldPageID,DefaultFilter,ColumnEquationText,Editable,Hidden,LastReferenceInteger,ReferenceValueFormatID,Encrypt,ShowCharacters,NumberOfCharactersToShow,TableEditMode,DisplayInTableView
	From DetailFields df WITH (NOLOCK)
	Where df.DetailFieldPageID = @DetailFieldPageID
	
	Select @NewDetailFieldPageID as [NewDetailFieldPageID]

END





GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__CreateCopy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__CreateCopy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__CreateCopy] TO [sp_executeall]
GO
