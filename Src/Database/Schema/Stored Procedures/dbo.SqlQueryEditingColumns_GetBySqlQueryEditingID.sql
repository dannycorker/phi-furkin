SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditingColumns table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingColumns_GetBySqlQueryEditingID]
(

	@SqlQueryEditingID int   
)
AS


				SELECT
					[SqlQueryEditingColumnID],
					[ClientID],
					[SqlQueryEditingID],
					[SqlQueryEditingTableID],
					[CompleteOutputText],
					[ColumnAlias],
					[ShowColumn],
					[DisplayOrder],
					[SortOrder],
					[SortType],
					[IsAggregate],
					[ColumnDataType],
					[ManipulatedDataType],
					[ColumnNaturalName],
					[RealSqlQueryColumnID],
					[ComplexValue]
				FROM
					[dbo].[SqlQueryEditingColumns] WITH (NOLOCK) 
				WHERE
										[SqlQueryEditingID] = @SqlQueryEditingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns_GetBySqlQueryEditingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingColumns_GetBySqlQueryEditingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingColumns_GetBySqlQueryEditingID] TO [sp_executeall]
GO
