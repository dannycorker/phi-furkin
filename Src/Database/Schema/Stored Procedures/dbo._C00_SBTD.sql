SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-12-22
-- Description:	SQL Before table row delete
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SBTD]
@TableRowID int,
@ClientPersonnelID int
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @DetailFieldID int

	SELECT @DetailFieldID = tr.DetailFieldID
	FROM TableRows tr WITH (NOLOCK)
	WHERE tr.TableRowID = @TableRowID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SBTD] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SBTD] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SBTD] TO [sp_executeall]
GO
