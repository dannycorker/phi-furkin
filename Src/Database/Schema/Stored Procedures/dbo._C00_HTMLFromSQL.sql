SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-09-05
-- Description:	Create html from SQL
-- 2016-06-07 ACE Added RawText
-- =============================================
CREATE PROCEDURE [dbo].[_C00_HTMLFromSQL]
	@CustomerID INT = NULL,
	@LeadID INT, 
	@CaseID INT = NULL,
	@MatterID INT, 
	@TableDetailFieldID INT, 
	@ClientIDForDV INT = NULL, 
	@ClientPersonnelIDForDV INT = NULL, 
	@ContactIDForDV INT = NULL,
	@PrintSQL BIT = 0
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @body VARCHAR(MAX),
			@Columns VARCHAR(2000),
			@SQL VARCHAR(MAX),
			@HTMLTableProperties VARCHAR(2000),
			@ColWidth INT,
			@NumberOfColumns INT,
			@AutoResizeColumns BIT,
			@SQLQueryID INT,
			@ClientID INT

	DECLARE @Table TABLE (XMLOutput VARCHAR(MAX))

	/*Det the detail required from the custom sql table*/
	SELECT @Columns = c.HeaderColumns, @SQL = c.QueryText, @HTMLTableProperties = c.HTMLTableProperties, @AutoResizeColumns = c.AutoSizeColumns, @SQLQueryID = c.SQLQueryID, @ClientID = c.ClientID
	FROM CustomTableSQL c
	WHERE c.DetailFieldID = @TableDetailFieldID
	
	IF @SQLQueryID > 0 
	BEGIN
	
		SELECT @SQL = sq.QueryText
		FROM SqlQuery sq WITH (NOLOCK) 
		WHERE sq.QueryID = @SQLQueryID
		AND sq.ClientID = @ClientID
	
	END

	SELECT @SQL = REPLACE(@SQL, '@CustomerID', CONVERT(VARCHAR,ISNULL(@CustomerID,'')))
	SELECT @SQL = REPLACE(@SQL, '@LeadID', CONVERT(VARCHAR,ISNULL(@LeadID,'')))
	SELECT @SQL = REPLACE(@SQL, '@CaseID', CONVERT(VARCHAR,ISNULL(@CaseID,'')))
	SELECT @SQL = REPLACE(@SQL, '@MatterID', CONVERT(VARCHAR,ISNULL(@MatterID,'')))
	SELECT @SQL = REPLACE(@SQL, '@ClientID', CONVERT(VARCHAR,ISNULL(@ClientIDForDV,'')))
	SELECT @SQL = REPLACE(@SQL, '@ClientPersonnelIDForDV', CONVERT(VARCHAR,ISNULL(@ClientPersonnelIDForDV,'')))
	SELECT @SQL = REPLACE(@SQL, '@ContactIDForDV', CONVERT(VARCHAR,ISNULL(@ContactIDForDV,'')))

	IF @HTMLTableProperties = 'RawText' /*2016-06-07 ACE Copied from [_C00_RTFFromSQL] CS 2015-07-14 Proof of Concept*/
	BEGIN
		INSERT @Table (XMLOutput)
		EXEC (@SQL)
		
		SELECT TOP(1) t.XMLOutput [MyHtml]
		FROM @Table t
		RETURN
	END

	SELECT @NumberOfColumns = COUNT(*)
	FROM dbo.fnSplitString (@Columns, ',')

	SELECT @ColWidth = 100/@NumberOfColumns
	
	SELECT @body = '<table ' + ISNULL(@HTMLTableProperties,'') + '>' + '<tr>'

	/*Add each of the headings into the rtf string*/
	SELECT @body = @body + '<td ' + CASE WHEN @AutoResizeColumns = 1 THEN 'width="' +  CONVERT(VARCHAR(100),@ColWidth) + '%"' ELSE '' END + '><b>' + CONVERT(VARCHAR(100),RTRIM(LTRIM(Item))) + '</b></td>' 
	FROM dbo.fnSplitString (@Columns, ',') f
	ORDER by f.ItemIndex

	SELECT @body = @body + '</tr>'

	/*Add the outer select and inner select together and format as XML*/
	SELECT @SQL = 'SELECT CAST((' + REPLACE(@SQL, 'cell_replace', 'td') + ' FOR XML RAW(''tr''), ELEMENTS
		) AS VARCHAR(MAX))'


	/*Swap out @ClientID filters*/
	SELECT @SQL = REPLACE(@SQL,'@ClientID',CONVERT(VARCHAR,@ClientID))
	
	/*Execute the sql stuffing the XML into a table variable so we can select it back out into a variable*/
	INSERT INTO @Table (XMLOutput)
	EXEC (@SQL)

	/*Get the body out of the table variable*/
	SELECT @body = @body + t.XMLOutput + + '</table>'
	FROM @Table t

	/*Wipe out errant DenyEdit / DenyDelete flags.  CS 2014-11-04*/
	SELECT @body = REPLACE(REPLACE(@body,'<DenyDelete>true</DenyDelete>',''),'<DenyEdit>true</DenyEdit>','')

	/*Wipe out errant TableRowID flags.  CS 2014-11-04*/
	SELECT @body = REPLACE(@body,'<TableRowID>0</TableRowID>','')

	select @body as [MyHtml]

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_HTMLFromSQL] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_HTMLFromSQL] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_HTMLFromSQL] TO [sp_executeall]
GO
