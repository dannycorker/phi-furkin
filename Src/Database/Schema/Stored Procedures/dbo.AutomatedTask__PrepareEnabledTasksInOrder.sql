SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- ALTER date: 2009-08-18
-- Description:	Work out which Tasks this scheduler can do, and
--              assign them so that they don't get picked up twice.
-- JWG 2010-03-09 Avoid tasks due either side of either side of midnight (23:55 to 00:05) and Scheduler restart time (02:55) as well
-- SB  2012-02-20 Don't return events of a specific subtype if they are disabled
-- ACE 2012-11-30 Added exclusion groups with help from Jim
-- JWG 2014-06-11 #27045 New proc AquariusMaster.dbo.GetIsMaintenanceWindow replaces the local function, to make it accessible from everywhere
--                Just add a record to the AquariusMaster.dbo.SpecialMaintenanceWindow table now if you need a long outage for any reason.
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedTask__PrepareEnabledTasksInOrder] 
	@QueueID int, 
	@SchedulerID int, 
	@ServerName varchar(50) 
AS
BEGIN
	
	/* Avoid running any tasks during Housekeeping (23:55 to 00:25) or around Scheduler restart time (02:55) */
	DECLARE @IsMaint BIT

	EXEC @IsMaint = [AquariusMaster].dbo.GetIsMaintenanceWindow
	
	/*
		JWG 2014-06-11 #27045 Use the common proc on AquariusMaster instead of each local DB having its own version.
		IF dbo.fnIsMaintenanceWindow(dbo.fn_GetDate_Local()) = 1
	*/
	IF @IsMaint = 1	
	/*IF (
			dbo.fnIsMaintenanceWindow(dbo.fn_GetDate_Local()) = 1	
		)*/
	BEGIN
		RETURN
	END

	DECLARE @MaxTaskCount int = 1, 
	@MaxRowcount int = 1, 
	@QueueEnabled bit = 1, 
	@LockResult int, 
	@RunDateTime datetime, 
	@RunDate date, 
	@RunTime time(0), 
	@DoW tinyint, 
	@BH bit 

	DECLARE @Tasks TABLE (
		TaskID int, 
		ClientID int,
		IsWorkflow bit
	)
	
	/* 
		For Queue 1 allow up to nnn tasks to be assigned at once.
		For Queue 2 only hand out one task at a time.
	*/
	
	/* Old scheduler code, that was pulling the task count from automated task queue
	
	SELECT @MaxTaskCount = atq.MaxRecords, 
	@QueueEnabled = atq.[Enabled] 
	FROM dbo.AutomatedTaskQueue atq (nolock) 
	WHERE atq.AutomatedTaskQueueID = @QueueID 

	IF @SchedulerID > 999 -- Force the new scheduler to just deal with a single task at a time
	BEGIN
	
		SELECT @MaxTaskCount = 1
	
	END
	*/
	
	/* Quit now if this queue has been disabled */
	IF @QueueEnabled = 0
	BEGIN
		RETURN
	END
	ELSE
	BEGIN
	
		/* Start a transaction for the benefit of sp_getapplock */
		BEGIN TRAN

		-- Use system proc sp_getapplock to limit this to one request at a time:
		-- @Resource: the unique name for this lock
		-- @LockMode: 'Shared', 'Update', 'IntentShared', 'IntentExclusive', 'Exclusive'
		-- @LockOwner: Scope of the lock: 'Transaction' or 'Session'
		-- @LockTimeout: Timeout in miliseconds
		-- @DbPrincipal: The DB Principal for access permisions ('dbo', 'public', 'guest')
		EXEC @LockResult = sp_getapplock 
			@Resource = 'AutomatedTask__PrepareEnabledTasksInOrder',
			@LockMode = 'Exclusive',
			@LockOwner = 'Transaction',
			@LockTimeout = 5000,
			@DbPrincipal = 'public'

		-- 0 and 1 are the valid return values from sp_getapplock
		IF @LockResult NOT IN (0, 1)
		BEGIN
			RAISERROR ( 'Unable to acquire Lock', 16, 1 )
			ROLLBACK
			RETURN
		END 
		ELSE
		BEGIN
			
			SELECT 
			@RunDateTime = dbo.fn_GetDate_Local(), 
			@RunDate = dbo.fn_GetDate_Local(), 
			@RunTime = dbo.fn_GetDate_Local(), 
			@DoW = wd.DayNumberOfWeek, 
			@BH = wd.IsBankHoliday 
			FROM dbo.WorkingDays wd WITH (NOLOCK) 
			WHERE wd.[Date] = dbo.fnDateOnly(dbo.fn_GetDate_Local()) 

			/* 
				Get a list of tasks to do, for this queue, which are not already earmarked for a scheduler to do.
				
				Tasks that have never seen this code before will default to QueueID 1 ("Small Tasks").
				
				Schedulers running on QueueID 2 ("Large Tasks") or above will find that they have no work to do if
				there is no queue info at present.
				
				Only get tasks for clients who don't have any tasks running on any other scheduler already.
				This preserves the NextRunDateTime order so tasks follow each other in a logical order.
			*/
			INSERT @Tasks (TaskID, ClientID, IsWorkflow) 
			SELECT TOP (@MaxTaskCount) at.TaskID, at.ClientID, at.WorkflowTask 
			FROM dbo.AutomatedTask at (nolock) 
			LEFT JOIN dbo.AutomatedTaskInfo ati (nolock) ON ati.TaskID = at.TaskID 
			LEFT JOIN dbo.AutomatedTaskParam atp WITH (NOLOCK) ON atp.TaskID = at.TaskID and atp.ParamName = 'LEADEVENT_TO_ADD'
			LEFT JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID = atp.ParamValue 
			LEFT JOIN dbo.EventSubtype est WITH (NOLOCK) ON et.EventSubtypeID = est.EventSubtypeID 
			WHERE at.[Enabled] = 1 
			
			AND at.ClientID NOT IN (224)--, 239)
			
			AND at.NextRunDateTime < dbo.fn_GetDate_Local() 
			AND IsNull(ati.QueueID, CASE at.WorkflowTask WHEN 1 THEN 3 ELSE 1 END) = @QueueID       -- Only tasks for this queue (defaults to 1 if no info yet) 
			AND ati.LockDateTime IS NULL                -- Only tasks that are not earmarked for a scheduler already 
			AND (est.Available IS NULL OR est.Available = 1) -- Only tasks that have an event sub type that is allowed
			--AND (at.AlreadyRunning = 0 OR at.AlreadyRunning IS NULL)
			--AND NOT EXISTS (
			--	SELECT * 
			--	FROM dbo.AutomatedTaskInfo ati_existing (nolock) 
			--	WHERE ati_existing.ClientID = at.ClientID        -- Only get tasks for clients who don't have any tasks 
			--	AND ati_existing.QueueID = ati.QueueID           -- running on any other scheduler already (within the same queue).
			--	AND ati_existing.LockDateTime IS NOT NULL 
			--)
			AND NOT EXISTS (
				SELECT * 
				FROM dbo.AutomatedTaskGroup atg WITH (NOLOCK) 
				INNER JOIN dbo.ExclusionGroup eg WITH (NOLOCK) on eg.ExclusionGroupID = atg.ExclusionGroupID AND eg.IsEnabled = 1 
				INNER JOIN dbo.ExclusionGroupMember egm WITH (NOLOCK) on egm.ExclusionGroupID = eg.ExclusionGroupID AND egm.IsEnabled = 1 
				INNER JOIN dbo.Exclusion e WITH (NOLOCK) on e.ExclusionID = egm.ExclusionID AND e.IsEnabled = 1 
				WHERE (atg.AutomatedTaskGroupID = at.AutomatedTaskGroupID) 
				AND (
					(e.ExclusionTypeID = 1 AND (
						e.ExclusionAllDay = 1 
						OR 
						@RunTime BETWEEN e.ExclusionTimeFrom AND e.ExclusionTimeTo)
					)

					OR
					(e.ExclusionTypeID = 2 AND e.ExclusionDayOfWeek = @DoW AND (
						e.ExclusionAllDay = 1 
						OR 
						@RunTime BETWEEN e.ExclusionTimeFrom AND e.ExclusionTimeTo)
					)
					OR
					(e.ExclusionTypeID = 3 AND @BH = 1 AND (
						e.ExclusionAllDay = 1 
						OR 
						@RunTime BETWEEN e.ExclusionTimeFrom AND e.ExclusionTimeTo)
					)
					OR
					(e.ExclusionTypeID = 4 AND @RunDate = e.ExclustionDate AND (
						e.ExclusionAllDay = 1 
						OR 
						@RunTime BETWEEN e.ExclusionTimeFrom AND e.ExclusionTimeTo)
					)
				)
			)
			ORDER BY at.NextRunDateTime 


			/*
				UPDATE Info records (where they exist) to show which
				scheduler will be dealing with them.
			*/
			UPDATE dbo.AutomatedTaskInfo 
			SET SchedulerID = @SchedulerID, 
			ServerName = @ServerName, 
			LockDateTime = dbo.fn_GetDate_Local() 
			FROM dbo.AutomatedTaskInfo ati 
			INNER JOIN @Tasks t ON t.TaskID = ati.TaskID

			
			/*
				ALTER new Info records where they don't exist at present. 
			*/
			INSERT INTO dbo.AutomatedTaskInfo (
				TaskID, 
				ClientID, 
				QueueID, 
				SchedulerID, 
				ServerName, 
				LockDateTime, 
				RunCount,
				LastRundate,
				LastRuntime,
				LastRowcount,
				MaxRuntime,
				MaxRowcount,
				AvgRuntime,
				AvgRowcount,
				Comments
			) 
			SELECT 
				t.TaskID, 
				t.ClientID, 
				CASE t.IsWorkflow WHEN 1 THEN 3 ELSE 1 END, /* Workflow goes to Queue 3, everything else goes to Queue 1 by default. We move tasks to Queue 2 manually if they prove to be large. */
				@SchedulerID, 
				@ServerName, 
				dbo.fn_GetDate_Local(),  -- LockDateTime 
				0,          -- Runcount 
				NULL,       -- LastRundate 
				0,          -- LastRuntime 
				0,          -- LastRowcount 
				0,          -- MaxRuntime 
				0,          -- MaxRowcount 
				0,          -- AvgRuntime 
				0,          -- AvgRowcount 
				'New'       -- Comments
			FROM @Tasks t 
			WHERE NOT EXISTS (
				SELECT * FROM dbo.AutomatedTaskInfo WHERE TaskID = t.TaskID 
			)
					
			EXEC @LockResult = sp_releaseapplock 
							@Resource = 'AutomatedTask__PrepareEnabledTasksInOrder',
							@DbPrincipal = 'public',
							@LockOwner = 'Transaction'
		END

		COMMIT

	END
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__PrepareEnabledTasksInOrder] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask__PrepareEnabledTasksInOrder] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__PrepareEnabledTasksInOrder] TO [sp_executeall]
GO
