SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2020-01-11
-- Description:	Dump out all rule outputs for a change set in order
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleOutputs_ExportForChangeSetID]
(
	 @ClientID INT
	,@ChangeSetID INT
)


AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @RulesToOutput TABLE ( RuleSetID INT, RuleSetName VARCHAR(2000), RuleID INT, RuleOrder INT, Processed BIT, Name VARCHAR(2000) )
	
	INSERT @RulesToOutput ( RuleSetID, RuleSetName, RuleID, RuleOrder, Processed, Name )
	SELECT rs.RuleSetID, rs.Name, r.RuleID, ROW_NUMBER() OVER (ORDER BY rs.Name, r.RuleOrder) [RowNum], 0, r.Name
	FROM RulesEngine_Rules r WITH ( NOLOCK ) 
	INNER JOIN RulesEngine_RuleSets rs WITH ( NOLOCK ) on rs.RuleSetID = r.RuleSetID
	WHERE rs.ChangeSetID = @ChangeSetID
	ORDER BY r.RuleOrder
	
	DECLARE @ThisRuleID INT
	
	SELECT rto.RuleOrder + 1 [Sheet], rto.RuleSetID, rto.RuleSetName, rto.RuleID, rto.Name
	FROM @RulesToOutput rto
	ORDER BY rto.RuleSetName, rto.RuleOrder
	
	WHILE EXISTS ( SELECT * 
	               FROM @RulesToOutput ro 
	               WHERE ro.Processed = 0 )
	BEGIN
		SELECT TOP 1 
			@ThisRuleID = rto.RuleID
		FROM @RulesToOutput rto 
		WHERE rto.Processed = 0
		ORDER BY rto.RuleOrder
		
		UPDATE rto
		SET Processed = 1
		FROM @RulesToOutput rto 
		WHERE rto.RuleID = @ThisRuleID
		
		EXEC RulesEngine_RuleOutputs_ExportForRuleID @ClientID, @ThisRuleID
	
		SELECT @ThisRuleID = NULL
	END
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleOutputs_ExportForChangeSetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleOutputs_ExportForChangeSetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleOutputs_ExportForChangeSetID] TO [sp_executeall]
GO
