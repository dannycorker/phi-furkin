SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO











/*
----------------------------------------------------------------------------------------------------
-- Date Created: 17 July 2007

-- Created By:  ()
-- Purpose: Select records from the Folder table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Folder__GetTopLevelFoldersByClientAndClientPersonnelID]
(
	@FolderID int = 1,
	@ClientID int,
	@ClientPersonnelID int
)
AS

	SET ANSI_NULLS OFF
	
	SELECT
		[FolderID],
		[ClientID],
		[FolderTypeID],
		[FolderParentID],
		[FolderName],
		[FolderDescription],
		[WhenCreated],
		[WhoCreated],
		[Personal]
	FROM
		dbo.[Folder]
	WHERE
		FolderTypeID  = @FolderID
		AND (([ClientID] = @ClientID AND [Personal]= 0)
		OR  ([ClientID] = @ClientID AND [WhoCreated] =  @ClientPersonnelID)
		OR  [ClientID] = 0)
		AND (FolderID > 0 OR @ClientID = 0)
	ORDER BY [FolderName]
	
	SET ANSI_NULLS ON
			











GO
GRANT VIEW DEFINITION ON  [dbo].[Folder__GetTopLevelFoldersByClientAndClientPersonnelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Folder__GetTopLevelFoldersByClientAndClientPersonnelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Folder__GetTopLevelFoldersByClientAndClientPersonnelID] TO [sp_executeall]
GO
