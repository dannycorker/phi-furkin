SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUserOption table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserOption_GetByPortalUserOptionID]
(

	@PortalUserOptionID int   
)
AS


				SELECT
					[PortalUserOptionID],
					[ClientID],
					[PortalUserID],
					[PortalUserOptionTypeID],
					[OptionValue]
				FROM
					[dbo].[PortalUserOption] WITH (NOLOCK) 
				WHERE
										[PortalUserOptionID] = @PortalUserOptionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOption_GetByPortalUserOptionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserOption_GetByPortalUserOptionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOption_GetByPortalUserOptionID] TO [sp_executeall]
GO
