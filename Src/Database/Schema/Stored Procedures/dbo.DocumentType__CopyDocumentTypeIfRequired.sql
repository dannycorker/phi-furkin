SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



----------------------------------------------------------------------------------------------------
-- Date Created: 28-Feb-2008

-- Created By:  Ben Crinion
-- Purpose: Copy a DocumentType to a NewLeadType and return the new DocumentTypeID
-- This only copys the document type if the destination client is 
-- different than the source or if the document type is NOT 
-- available to All lead types for the client.
----------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[DocumentType__CopyDocumentTypeIfRequired]
(
	@ExistingDocumentTypeID int,
	@NewLeadTypeID int,
	@ExistingLeadTypeID int,
	@NewClientID int
)
AS
	
DECLARE @InsertedDocumentTypeID int

INSERT
INTO    [DBO].[DocumentType]
        (
                [ClientID]                ,
                [LeadTypeID]              ,
                [DocumentTypeName]        ,
                [DocumentTypeDescription] ,
                [Header]                  ,
                [Template]                ,
                [Footer]                  ,
                [CanBeAutoSent]           ,
                [EmailSubject]            ,
                [EmailBodyText]           ,
                [InputFormat]             ,
                [OutputFormat]            ,
                [Enabled]                 ,
                [RecipientsTo]            ,
                [RecipientsCC]            ,
                [RecipientsBCC]           ,
                [ReadOnlyTo]              ,
                [ReadOnlyCC]              ,
                [ReadOnlyBCC],
				[SendToMultipleRecipients],
				[MultipleRecipientDataSourceType],
				[MultipleRecipientDataSourceID],
				[SendToAllByDefault],
				[ExcelTemplatePath],
				[DocumentTitleTemplate],
				[CreateNewVersionWhenSaved]
        )
SELECT  @NewClientID                 ,
        @NewLeadTypeID            ,
        [DocumentTypeName]        ,
        [DocumentTypeDescription] ,
        [Header]                  ,
        [Template]                ,
        [Footer]                  ,
        [CanBeAutoSent]           ,
        [EmailSubject]            ,
        [EmailBodyText]           ,
        [InputFormat]             ,
        [OutputFormat]            ,
        [Enabled]                 ,
        [RecipientsTo]            ,
        [RecipientsCC]            ,
        [RecipientsBCC]           ,
        [ReadOnlyTo]              ,
        [ReadOnlyCC]              ,
        [ReadOnlyBCC],
		[SendToMultipleRecipients],
		[MultipleRecipientDataSourceType],
		[MultipleRecipientDataSourceID],
		[SendToAllByDefault],
		[ExcelTemplatePath],
		[DocumentTitleTemplate],
		[CreateNewVersionWhenSaved]
FROM    [DBO].[DocumentType]
WHERE	[DocumentTypeID] = @ExistingDocumentTypeID
AND		([ClientID] <> @NewClientID 
		OR 
		(LeadTypeID IS NOT NULL AND [LeadTypeID] <> @ExistingLeadTypeID) )

SELECT @InsertedDocumentTypeID = SCOPE_IDENTITY()

IF(@InsertedDocumentTypeID is null)
BEGIN
	select @ExistingDocumentTypeID
END
ELSE
BEGIN
	SELECT @InsertedDocumentTypeID
END





GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__CopyDocumentTypeIfRequired] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType__CopyDocumentTypeIfRequired] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__CopyDocumentTypeIfRequired] TO [sp_executeall]
GO
