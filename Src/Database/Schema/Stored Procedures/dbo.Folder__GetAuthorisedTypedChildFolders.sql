SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------
-- Date Created: 18 August 2011

-- Created By:  (Ian Slack)
-- Purpose: Select child folders of a specific type and only if the user has access to them
----------------------------------------------------------------------------------------------------
*/

CREATE PROCEDURE [dbo].[Folder__GetAuthorisedTypedChildFolders]
(
	@ClientID int,
	@FolderTypeID int,
	@FolderParentID int,
	@WhoCreated int
)
AS

SET ANSI_NULLS OFF

	IF @ClientID = 276 AND @FolderTypeID <> 3 /* 2014-01-24 ACE - Cheshire East temp security */
	BEGIN
	
		/*
			Due to the current lack of security and Cheshire East requiring folder
			lock down we will have to run the following.
			
			This will give corporate or Aquarium users all folders
			and if you are in highways or transport you will only see your own folder.
		*/
	
		DECLARE @FoldersICanSee TABLE (FolderID INT)
		
		DECLARE @ClientPersonnelAdminGroupID INT
		
		SELECT @ClientPersonnelAdminGroupID = cp.ClientPersonnelAdminGroupID
		FROM ClientPersonnel cp WITH (NOLOCK)
		WHERE cp.ClientPersonnelID = @WhoCreated
		
		INSERT INTO @FoldersICanSee (FolderID)
		VALUES (-2)

		IF @ClientPersonnelAdminGroupID IN (1,288,307)
		BEGIN

			INSERT INTO @FoldersICanSee (FolderID)
			SELECT f.FolderID
			FROM Folder f WITH (NOLOCK)
			WHERE FolderTypeID  = @FolderTypeID
			AND @ClientPersonnelAdminGroupID IN (1,288,307)
			AND f.ClientID IN (276,0)
			AND (f.FolderParentID = @FolderParentID
				OR (@FolderParentID = -2 AND FolderParentID IS NULL)
			)
			AND f.FolderID <> -1
			

		END
		ELSE
		BEGIN
			IF @ClientPersonnelAdminGroupID = 286 AND @FolderParentID NOT IN (6474,6522) /*Highways*/
			BEGIN
			
				INSERT INTO @FoldersICanSee (FolderID)
				VALUES (6474), /*Highways Folder*/
						(6522) /*ACE 2014-05-08 Added All Depts*/
				
			END
			ELSE
			BEGIN
				IF @ClientPersonnelAdminGroupID = 287 AND @FolderParentID NOT IN (6473,6522) /*Transport*/
				BEGIN
				
					INSERT INTO @FoldersICanSee (FolderID)
					VALUES (6473), /*Transport Folder*/
						(6522) /*ACE 2014-05-08 Added All Depts*/
					
				END
			END
		END
		SELECT f.*
		FROM dbo.[Folder] f WITH (NOLOCK) 
		INNER JOIN @FoldersICanSee fi ON fi.FolderID = f.FolderID
		ORDER BY f.FolderName
				
	END
	ELSE
	BEGIN
		SELECT	*
		FROM	dbo.[Folder]
		WHERE
			FolderTypeID  = @FolderTypeID
			AND 
			(
				([ClientID] = @ClientID AND [Personal]= 0)
				OR  
				([ClientID] = @ClientID AND [WhoCreated] =  @WhoCreated)
				OR  
				[ClientID] = 0
			)
			AND (FolderID > 0 OR @ClientID = 0)
			AND 
			(
				(@FolderParentID = -2 AND FolderParentID IS NULL) /* -1 is ued for deleted folders, using -2 for null */
				OR
				(FolderParentID = @FolderParentID)
			)
		ORDER BY [FolderName]
	END
	
SET ANSI_NULLS ON



GO
GRANT VIEW DEFINITION ON  [dbo].[Folder__GetAuthorisedTypedChildFolders] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Folder__GetAuthorisedTypedChildFolders] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Folder__GetAuthorisedTypedChildFolders] TO [sp_executeall]
GO
