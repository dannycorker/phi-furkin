SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-10-14
-- Description:	Delete all the subtables that hang off an EventType, 
--              usually in readiness to add revised versions from scratch. 
-- PR 2013-05-01 Added [EventTypeStandardField]
-- =============================================
CREATE PROCEDURE [dbo].[EventType__DeleteAllSubtables]
(
	@EventTypeID int   
)
AS
BEGIN

	-- EventTypeMandatoryEvent
	DELETE FROM [dbo].[EventTypeMandatoryEvent] 
	WHERE [ToEventTypeID] = @EventTypeID

	-- EventTypeMandatoryField
	DELETE FROM [dbo].[EventTypeMandatoryField]  
	WHERE [EventTypeID] = @EventTypeID
	
	-- EventTypeHelperField
	DELETE FROM [dbo].[EventTypeHelperField]  
	WHERE [EventTypeID] = @EventTypeID

	-- EventTypeAttachment
	DELETE FROM [dbo].[EventTypeAttachment] 
	WHERE [EventTypeID] = @EventTypeID

	-- EventTypeAutomatedTask
	DELETE FROM [dbo].[EventTypeAutomatedTask]  
	WHERE [EventTypeID] = @EventTypeID

	-- EventTypeAutomatedTask
	DELETE FROM [dbo].[EventTypeFieldCompletion]  
	WHERE [EventTypeID] = @EventTypeID

	-- EventTypeAccessRule
	DELETE FROM [dbo].[EventTypeAccessRule]  
	WHERE [EventTypeID] = @EventTypeID
	
	-- EventTypeStandardField
	DELETE FROM [dbo].[EventTypeStandardField]
	WHERE [EventTypeID] = @EventTypeID
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__DeleteAllSubtables] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType__DeleteAllSubtables] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType__DeleteAllSubtables] TO [sp_executeall]
GO
