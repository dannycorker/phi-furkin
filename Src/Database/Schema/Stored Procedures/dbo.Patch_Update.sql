SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Patch table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Patch_Update]
(

	@PatchID int   ,

	@PatchDate datetime   ,

	@PatchVersion varchar (50)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Patch]
				SET
					[PatchDate] = @PatchDate
					,[PatchVersion] = @PatchVersion
				WHERE
[PatchID] = @PatchID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Patch_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Patch_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Patch_Update] TO [sp_executeall]
GO
