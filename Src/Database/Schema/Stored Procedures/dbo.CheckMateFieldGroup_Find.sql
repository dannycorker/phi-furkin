SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the CheckMateFieldGroup table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateFieldGroup_Find]
(

	@SearchUsingOR bit   = null ,

	@CheckMateFieldGroupID int   = null ,

	@GroupName varchar (50)  = null ,

	@GroupDescription varchar (250)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CheckMateFieldGroupID]
	, [GroupName]
	, [GroupDescription]
    FROM
	[dbo].[CheckMateFieldGroup] WITH (NOLOCK) 
    WHERE 
	 ([CheckMateFieldGroupID] = @CheckMateFieldGroupID OR @CheckMateFieldGroupID IS NULL)
	AND ([GroupName] = @GroupName OR @GroupName IS NULL)
	AND ([GroupDescription] = @GroupDescription OR @GroupDescription IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CheckMateFieldGroupID]
	, [GroupName]
	, [GroupDescription]
    FROM
	[dbo].[CheckMateFieldGroup] WITH (NOLOCK) 
    WHERE 
	 ([CheckMateFieldGroupID] = @CheckMateFieldGroupID AND @CheckMateFieldGroupID is not null)
	OR ([GroupName] = @GroupName AND @GroupName is not null)
	OR ([GroupDescription] = @GroupDescription AND @GroupDescription is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldGroup_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateFieldGroup_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateFieldGroup_Find] TO [sp_executeall]
GO
