SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-03-01
-- Description:	Get latest rate only by currency code (eg 'EUR')
-- =============================================
CREATE PROCEDURE [dbo].[CurrencyRate__GetLatestByCurrencyCode] 
	@CurrencyCode char (3)  
AS
BEGIN
	SET NOCOUNT ON;

	/* Keep this list in step with the DAL */
	SELECT  [CurrencyRateID],
			[CurrencyId],
			[CurrencyCode],
			[FromGBPRate],
			[ToGBPRate],
			[ConversionDate],
			[IsLatest]
	FROM [dbo].[CurrencyRate]
	WHERE [CurrencyCode] = @CurrencyCode 
	AND [IsLatest] = 1

	SELECT @@ROWCOUNT
END




GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate__GetLatestByCurrencyCode] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CurrencyRate__GetLatestByCurrencyCode] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate__GetLatestByCurrencyCode] TO [sp_executeall]
GO
