SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-27
-- Description:	Housekeeping proc to delete unwanted Lead Types overnight, including any related records
--			that cannot be included in the cascaded delete (eg DetailFieldAlias).
-- =============================================
CREATE PROCEDURE [dbo].[HskDeleteLeadType] 
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @LeadTypeID int 
	
	SELECT TOP 1 @LeadTypeID = h.LeadTypeID 
	FROM dbo.HskLeadTypeToDelete h 
	WHERE h.FailedToDelete = 0
	ORDER BY h.LeadTypeID
	
	WHILE @LeadTypeID > 0
	BEGIN
	
		-- Check no leads exist first
		IF NOT EXISTS (SELECT * FROM dbo.Lead (nolock) WHERE LeadTypeID = @LeadTypeID)
		BEGIN TRY
			
			/* 
				First clear out any DetailFieldAlias records, and any other 
				associated tables.  Some cannot be included in the cascade 
				delete for LeadType, so we must delete them manually in a sensible order.
			*/
			DELETE dbo.DetailFieldAlias 
			WHERE LeadTypeID = @LeadTypeID 

			DELETE dbo.DetailFieldLink 
			WHERE LeadTypeID = @LeadTypeID 

			DELETE dbo.LeadTypePageTitleTargetControl 
			WHERE LeadTypeID = @LeadTypeID 

			DELETE dbo.GroupRightsDynamic
			WHERE LeadTypeID = @LeadTypeID 

			DELETE dbo.DetailFields 
			WHERE LeadTypeID = @LeadTypeID 

			DELETE dbo.DetailFieldPages 
			WHERE LeadTypeID = @LeadTypeID 
			
			/*
				There are dozens of other tables that the cascades should take care of,
				including the HskLeadTypeToDelete table itself.
				
				These tables can all be found with this sql statement if needed:
				
				select * 
				from information_schema.columns 
				where column_name = 'LeadTypeID' 
				order by table_name, column_name 
			*/
			
			/* 
				Finally delete the LeadType Record
			*/
			
			DELETE dbo.LeadType 
			WHERE LeadTypeID = @LeadTypeID 
			
		END TRY
		BEGIN CATCH

			DECLARE @err varchar(1900) = convert(varchar(1900), ERROR_MESSAGE())
			
			EXEC dbo._C00_LogIt 'Error', 'AquariumHousekeeping', 'dbo.HskDeleteLeadType', @err, 324

			/* If the delete failed, log the error and don't try this record again until fixed. */		
			UPDATE dbo.HskLeadTypeToDelete 
			SET FailedToDelete = 1, 
			Comments = 'Failed to delete at line ' + convert(varchar, error_line()) + ' on ' + convert(varchar, dateadd(ms,1,dbo.fn_GetDate_Local()), 126) + '  Error message is: ' + @err
			WHERE LeadTypeID = @LeadTypeID 
			
		END CATCH
		ELSE
		BEGIN
		
			UPDATE dbo.HskLeadTypeToDelete 
			SET FailedToDelete = 1, 
			Comments = 'Failed to delete this leadtype as there are leads associated AT: ' + convert(varchar, dateadd(ms,1,dbo.fn_GetDate_Local()), 126)
			WHERE LeadTypeID = @LeadTypeID 

		END

		/* Loop round for the next lead type */	
		SELECT TOP 1 @LeadTypeID = h.LeadTypeID 
		FROM dbo.HskLeadTypeToDelete h 
		WHERE h.FailedToDelete = 0
		AND h.LeadTypeID > @LeadTypeID
		ORDER BY h.LeadTypeID
	
		IF @@rowcount < 1
		BEGIN
			SELECT @LeadTypeID = -1
		END
		
	END	

	/* Change execution context to 'notriggers' to bypass trgd_Customers */
	/*EXECUTE AS LOGIN = 'notriggers'
	
	DELETE dbo.[Customers] WHERE [ClientID] = 161
	DELETE dbo.[ClientPersonnel] WHERE [ClientID] = 161
	DELETE dbo.[Clients] WHERE [ClientID] = 161
	
	REVERT*/
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[HskDeleteLeadType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HskDeleteLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HskDeleteLeadType] TO [sp_executeall]
GO
