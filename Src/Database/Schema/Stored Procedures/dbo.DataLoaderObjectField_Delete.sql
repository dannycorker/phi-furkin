SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DataLoaderObjectField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderObjectField_Delete]
(

	@DataLoaderObjectFieldID int   
)
AS


				DELETE FROM [dbo].[DataLoaderObjectField] WITH (ROWLOCK) 
				WHERE
					[DataLoaderObjectFieldID] = @DataLoaderObjectFieldID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectField_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderObjectField_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderObjectField_Delete] TO [sp_executeall]
GO
