SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the NoteType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[NoteType_Insert]
(

	@NoteTypeID int    OUTPUT,

	@ClientID int   ,

	@NoteTypeName varchar (50)  ,

	@NoteTypeDescription varchar (250)  ,

	@DefaultPriority smallint   ,

	@AlertColour nchar (7)  ,

	@NormalColour nchar (7)  ,

	@Enabled bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@SourceID int   ,

	@LeadTypeID int   ,

	@IsShared bit   
)
AS


				
				INSERT INTO [dbo].[NoteType]
					(
					[ClientID]
					,[NoteTypeName]
					,[NoteTypeDescription]
					,[DefaultPriority]
					,[AlertColour]
					,[NormalColour]
					,[Enabled]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					,[SourceID]
					,[LeadTypeID]
					,[IsShared]
					)
				VALUES
					(
					@ClientID
					,@NoteTypeName
					,@NoteTypeDescription
					,@DefaultPriority
					,@AlertColour
					,@NormalColour
					,@Enabled
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					,@SourceID
					,@LeadTypeID
					,@IsShared
					)
				-- Get the identity value
				SET @NoteTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NoteType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType_Insert] TO [sp_executeall]
GO
