SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SearchableObjectType table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SearchableObjectType_GetBySearchableObjectTypeID]
(

	@SearchableObjectTypeID int   
)
AS


				SELECT
					[SearchableObjectTypeID],
					[SearchableObject],
					[PKColumnName],
					[JoinTable],
					[JoinColumn],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[SearchableObjectType] WITH (NOLOCK) 
				WHERE
										[SearchableObjectTypeID] = @SearchableObjectTypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SearchableObjectType_GetBySearchableObjectTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SearchableObjectType_GetBySearchableObjectTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SearchableObjectType_GetBySearchableObjectTypeID] TO [sp_executeall]
GO
