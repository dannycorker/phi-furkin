SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LogLevel table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LogLevel_GetByLogLevelID]
(

	@LogLevelID int   
)
AS


				SELECT
					[LogLevelID],
					[LogLevelName],
					[LogLevelDescription]
				FROM
					[dbo].[LogLevel] WITH (NOLOCK) 
				WHERE
										[LogLevelID] = @LogLevelID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LogLevel_GetByLogLevelID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LogLevel_GetByLogLevelID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LogLevel_GetByLogLevelID] TO [sp_executeall]
GO
