SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the TableDetailValues table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TableDetailValues_Find]
(

	@SearchUsingOR bit   = null ,

	@TableDetailValueID int   = null ,

	@TableRowID int   = null ,

	@ResourceListID int   = null ,

	@DetailFieldID int   = null ,

	@DetailValue varchar (2000)  = null ,

	@LeadID int   = null ,

	@MatterID int   = null ,

	@ClientID int   = null ,

	@EncryptedValue varchar (3000)  = null ,

	@ErrorMsg varchar (1000)  = null ,

	@ValueInt int   = null ,

	@ValueMoney money   = null ,

	@ValueDate date   = null ,

	@ValueDateTime datetime2   = null ,

	@CustomerID int   = null ,

	@CaseID int   = null ,

	@ClientPersonnelID int   = null ,

	@ContactID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [TableDetailValueID]
	, [TableRowID]
	, [ResourceListID]
	, [DetailFieldID]
	, [DetailValue]
	, [LeadID]
	, [MatterID]
	, [ClientID]
	, [EncryptedValue]
	, [ErrorMsg]
	, [ValueInt]
	, [ValueMoney]
	, [ValueDate]
	, [ValueDateTime]
	, [CustomerID]
	, [CaseID]
	, [ClientPersonnelID]
	, [ContactID]
    FROM
	[dbo].[TableDetailValues] WITH (NOLOCK) 
    WHERE 
	 ([TableDetailValueID] = @TableDetailValueID OR @TableDetailValueID IS NULL)
	AND ([TableRowID] = @TableRowID OR @TableRowID IS NULL)
	AND ([ResourceListID] = @ResourceListID OR @ResourceListID IS NULL)
	AND ([DetailFieldID] = @DetailFieldID OR @DetailFieldID IS NULL)
	AND ([DetailValue] = @DetailValue OR @DetailValue IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([MatterID] = @MatterID OR @MatterID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([EncryptedValue] = @EncryptedValue OR @EncryptedValue IS NULL)
	AND ([ErrorMsg] = @ErrorMsg OR @ErrorMsg IS NULL)
	AND ([ValueInt] = @ValueInt OR @ValueInt IS NULL)
	AND ([ValueMoney] = @ValueMoney OR @ValueMoney IS NULL)
	AND ([ValueDate] = @ValueDate OR @ValueDate IS NULL)
	AND ([ValueDateTime] = @ValueDateTime OR @ValueDateTime IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([ContactID] = @ContactID OR @ContactID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [TableDetailValueID]
	, [TableRowID]
	, [ResourceListID]
	, [DetailFieldID]
	, [DetailValue]
	, [LeadID]
	, [MatterID]
	, [ClientID]
	, [EncryptedValue]
	, [ErrorMsg]
	, [ValueInt]
	, [ValueMoney]
	, [ValueDate]
	, [ValueDateTime]
	, [CustomerID]
	, [CaseID]
	, [ClientPersonnelID]
	, [ContactID]
    FROM
	[dbo].[TableDetailValues] WITH (NOLOCK) 
    WHERE 
	 ([TableDetailValueID] = @TableDetailValueID AND @TableDetailValueID is not null)
	OR ([TableRowID] = @TableRowID AND @TableRowID is not null)
	OR ([ResourceListID] = @ResourceListID AND @ResourceListID is not null)
	OR ([DetailFieldID] = @DetailFieldID AND @DetailFieldID is not null)
	OR ([DetailValue] = @DetailValue AND @DetailValue is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([MatterID] = @MatterID AND @MatterID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([EncryptedValue] = @EncryptedValue AND @EncryptedValue is not null)
	OR ([ErrorMsg] = @ErrorMsg AND @ErrorMsg is not null)
	OR ([ValueInt] = @ValueInt AND @ValueInt is not null)
	OR ([ValueMoney] = @ValueMoney AND @ValueMoney is not null)
	OR ([ValueDate] = @ValueDate AND @ValueDate is not null)
	OR ([ValueDateTime] = @ValueDateTime AND @ValueDateTime is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([ContactID] = @ContactID AND @ContactID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues_Find] TO [sp_executeall]
GO
