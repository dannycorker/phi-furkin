SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[DocumentType__GetCutDownDocumentTypeByClientID] 
@ClientID int
AS
SELECT
	[DocumentTypeID],
	[LeadTypeName],
	[DocumentTypeName],
	[DocumentTypeDescription]
FROM
	dbo.[DocumentType] as DocType
	LEFT JOIN LeadType on DocType.LeadTypeID = LeadType.LeadTypeID
WHERE
	DocType.[ClientID] = @ClientID
ORDER BY 
	[DocumentTypeName]





GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetCutDownDocumentTypeByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType__GetCutDownDocumentTypeByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType__GetCutDownDocumentTypeByClientID] TO [sp_executeall]
GO
