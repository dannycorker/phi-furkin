SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By:  Jim Green
-- Purpose: Select Lead Type records, as long as the user has access to them
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[LeadType__GetByClientIDSecure2]
(
	@ClientID int,
	@UserID int,
	@IncludeDisabledLeadTypes bit = NULL
)
AS
	
	SELECT @IncludeDisabledLeadTypes = ISNULL(@IncludeDisabledLeadTypes, 1)

	SET ANSI_NULLS OFF
	
	SELECT lt.*				
	FROM [dbo].[LeadType] lt 
	INNER JOIN fnLeadTypeSecure(@UserID) f ON lt.LeadTypeID = f.LeadTypeID
	WHERE [ClientID] = @ClientID
	AND (@IncludeDisabledLeadTypes = 1 OR (@IncludeDisabledLeadTypes = 0 AND lt.Enabled = 1))
	ORDER BY [LeadTypeName]
	
	Select @@ROWCOUNT
	SET ANSI_NULLS ON
			


GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType__GetByClientIDSecure2] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadType__GetByClientIDSecure2] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadType__GetByClientIDSecure2] TO [sp_executeall]
GO
