SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the DocumentType table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentType_GetPaged]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				CREATE TABLE #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [DocumentTypeID] int 
				)
				
				-- Insert into the temp table
				DECLARE @SQL AS nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex ([DocumentTypeID])'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [DocumentTypeID]'
				SET @SQL = @SQL + ' FROM [dbo].[DocumentType] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				EXEC sp_executesql @SQL

				-- Return paged results
				SELECT O.[DocumentTypeID], O.[ClientID], O.[LeadTypeID], O.[DocumentTypeName], O.[DocumentTypeDescription], O.[Header], O.[Template], O.[Footer], O.[CanBeAutoSent], O.[EmailSubject], O.[EmailBodyText], O.[InputFormat], O.[OutputFormat], O.[Enabled], O.[RecipientsTo], O.[RecipientsCC], O.[RecipientsBCC], O.[ReadOnlyTo], O.[ReadOnlyCC], O.[ReadOnlyBCC], O.[SendToMultipleRecipients], O.[MultipleRecipientDataSourceType], O.[MultipleRecipientDataSourceID], O.[SendToAllByDefault], O.[ExcelTemplatePath], O.[FromDetails], O.[ReadOnlyFrom], O.[SourceID], O.[WhoCreated], O.[WhenCreated], O.[WhoModified], O.[WhenModified], O.[FolderID], O.[IsThunderheadTemplate], O.[ThunderheadUniqueTemplateID], O.[ThunderheadDocumentFormat], O.[DocumentTitleTemplate], O.[CreateNewVersionWhenSaved]
				FROM
				    [dbo].[DocumentType] o WITH (NOLOCK),
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexId > @PageLowerBound
					AND O.[DocumentTypeID] = PageIndex.[DocumentTypeID]
				ORDER BY
				    PageIndex.IndexId
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[DocumentType] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType_GetPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType_GetPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType_GetPaged] TO [sp_executeall]
GO
