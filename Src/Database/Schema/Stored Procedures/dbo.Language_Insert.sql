SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Language table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Language_Insert]
(

	@LanguageID int   ,

	@LanguageName varchar (50)  ,

	@Alpha2Code char (2)  ,

	@Alpha3Code char (3)  ,

	@NativeName nvarchar (50)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[Language]
					(
					[LanguageID]
					,[LanguageName]
					,[Alpha2Code]
					,[Alpha3Code]
					,[NativeName]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@LanguageID
					,@LanguageName
					,@Alpha2Code
					,@Alpha3Code
					,@NativeName
					,@WhenCreated
					,@WhenModified
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Language_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Language_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Language_Insert] TO [sp_executeall]
GO
