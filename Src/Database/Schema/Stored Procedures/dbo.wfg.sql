SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-03-15
-- Description:	Show Workflow Groups for a client
-- =============================================
CREATE PROCEDURE [dbo].[wfg] 
@ClientID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT *
	FROM WorkflowGroup wf WITH (NOLOCK)
	WHERE wf.ClientID = @ClientID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[wfg] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[wfg] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[wfg] TO [sp_executeall]
GO
