SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =======================================================================================================================================
-- Accepts a packet of XML data from Vision, shreds values, and inserts into the Claim table and CPS.
--
--				1.	Receive payload of data (@ClaimDataXML)
--				2.	Shred the values and store into a _C600_ImportedClaim table
--				3.	Attempt to locate a V1 Aquarium AccountID
--					a. If we have one lift the value as the AccountID
--					b. If we don’t create a new claim account
--				4.	Update the record in the database table to hold the matched or new AccountID for the Claim
--				5.	Insert into the CustomerPaymentSchedule
--	MODS:
--	2020-07-31	GPR - Altered procedure to remove parameter of @ClaimXML, this will be sourced from the MediateInbound table. Introduction of dbo.MediateInboundClaimAudit.
--  2020-09-16	ACE - Handle Multiple Claim Payment Rows
--				ACE - Use common DateTime var instead of multiple calls to fn_GetDate_Local
--	2020-09-25	ALM added a fix to stop claims from appearing on incorrect policies SDAAG-170
--	2021-04-21  ALM only insert into payment tables where amount is greater than 0 FURKIN-539
--  2021-04-26  ALM updated OneVisionClaim with CPS and HP TableRowID FURKIN-549
-- =======================================================================================================================================
CREATE PROCEDURE [dbo].[_C600_ClaimDataImport]

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ClaimXML XML, @MediateInboundRequest INT

	DECLARE @EmptyTableDoNotUse TABLE (RowsProcessed INT)

	DECLARE @ClientID INT = dbo.fngetprimaryclientid()
			,@Now DATETIME = dbo.fn_GetDate_Local()
			,@CustomerID INT
			,@AccountID INT
			,@AccountUseID INT = 2 /*Claims*/
			,@Payee VARCHAR(100)
			,@ClaimID INT
			,@Net MONEY
			,@Tax MONEY
			,@Gross MONEY
			,@VisionClaimID INT
			,@PetName VARCHAR(100)
			,@PolicyID INT
			,@PolicyInceptionDate DATE
			,@PolicyStartDate DATE
			,@PolicyStatus VARCHAR(10)
			,@PolicyStatusID INT
			,@ClaimFNOL DATE
			,@ClaimTreatmentStart DATE
			,@ClaimTreatmentEnd DATE
			,@ClaimAmount MONEY
			,@ClaimSettlementAmount MONEY
			,@ClaimStatus VARCHAR(50)
			,@ClaimStatusID INT
			,@AilmentFirstCause VARCHAR(250)
			,@AilmentSecondCause VARCHAR(250)
			,@AilmentVeNomCode INT
			,@PaymentID INT
			,@PaymentGross MONEY
			,@PaymentAccountName VARCHAR(100)
			,@PaymentAccountNumber VARCHAR(10)
			,@PaymentAccountSortcode VARCHAR(10)
			,@PaymentMethod VARCHAR(15)
			,@PaymentMethodID INT
			,@WhoCreated INT
			,@WhenCreated DATE
			,@RunAsUserID INT = 58552 /*Aquarium Automation*/
			,@CustomerPaymentScheduleID INT
			,@LeadID INT
			,@MatterID INT
			,@HistoricalPolicyTableRow INT
			,@OneVisionID INT
			,@DateOfDeath DATE
			,@ClassicClaimStatusID INT
			,@RecordCount INT
			,@LogitID INT
			,@UNID INT
			,@TestID INT = 0
			,@OneVisionClaimID INT
			,@ClientAccountID INT
			,@VisionPolicyTermID INT

	/*Logging: Info: Procedure Started*/
	EXEC @LogitID = dbo._C00_LogIt 'Info', '_C600_ClaimDataImport', 'Procedure Started', '', @RunAsUserID

	DECLARE @Payments TABLE (UNID INT IDENTITY(1,1), 
							VisionPaymentID INT, 
							Gross NUMERIC(18,2), 
							AccountName VARCHAR(250), 
							AccountNumber VARCHAR(250), 
							SortCode VARCHAR(250), 
							PaymentMethod VARCHAR(250), 
							PaymentMethodID INT
							)

	WHILE (
		SELECT COUNT(*) 
		FROM MediateInbound mi WITH (NOLOCK)
		WHERE NOT EXISTS (
			SELECT * 
			FROM MediateInboundClaimAudit mica WITH (NOLOCK)
			WHERE mica.MediateInboundRequest = mi.MediateInboundRequest
		)
	) > 0
	BEGIN

		SELECT TOP(1) @MediateInboundRequest = mi.MediateInboundRequest, @ClaimXML = mi.ResponseBody
		FROM MediateInbound mi WITH (NOLOCK)
		WHERE NOT EXISTS 
		(
			SELECT * 
			FROM MediateInboundClaimAudit mica WITH (NOLOCK)
			WHERE mica.MediateInboundRequest = mi.MediateInboundRequest
		)
		ORDER BY mi.MediateInboundRequest DESC /*ALM 2020-12-08*/

		EXEC dbo._C00_LogIt 'Info', '_C600_ClaimDataImport', 'Processing MediateInboundRequest', @MediateInboundRequest, @RunAsUserID

		/*
			Receive claim data from vision
			Shred values from XML
		*/
		SELECT @VisionClaimID = @ClaimXML.value('(event/claim/id)[1]', 'int')
				,@PetName = @ClaimXML.value('(event/pet/name)[1]', 'varchar(2000)')
				,@PolicyID = @ClaimXML.value('(event/policy/id)[1]', 'varchar(2000)')
				,@PolicyInceptionDate = @ClaimXML.value('(event/policy/inceptionDate)[1]', 'date')
				--,@PolicyStartDate = @ClaimXML.value('(VisionClaimResponse/PolicyData/termsDate)[1]', 'date')
				,@PolicyStatus = @ClaimXML.value('(event/policy/status)[1]', 'varchar(2000)')
				,@PolicyStatusID = @ClaimXML.value('(event/policy/statusId)[1]', 'varchar(2000)')
				,@ClaimFNOL = @ClaimXML.value('(event/claim/dateOfLoss)[1]', 'date')
				,@ClaimTreatmentStart = @ClaimXML.value('(event/claim/treatmentStartDate)[1]', 'date')
				,@ClaimTreatmentEnd = @ClaimXML.value('(event/claim/treatmentEndDate)[1]', 'date')
				,@DateOfDeath = @ClaimXML.value('(event/claim/dateOfDeath)[1]', 'date')
				,@ClaimAmount = @ClaimXML.value('(event/claim/amount)[1]', 'varchar(2000)')
				,@ClaimStatus = @ClaimXML.value('(event/claim/status)[1]', 'varchar(2000)')
				,@ClaimStatusID = @ClaimXML.value('(event/claim/statusId)[1]', 'varchar(2000)')
				,@AilmentFirstCause = @ClaimXML.value('(event/claim/ailment/firstCause)[1]', 'varchar(2000)')
				,@AilmentSecondCause = @ClaimXML.value('(event/claim/ailment/secondCause)[1]', 'varchar(2000)')
				,@AilmentVeNomCode = @ClaimXML.value('(event/claim/ailment/veNomCode)[1]', 'varchar(2000)')
				,@AccountID = NULL
		
				,@PaymentID = @ClaimXML.value('(event/payments/id)[1]', 'INT')
				,@PaymentGross = @ClaimXML.value('(event/payments/gross)[1]', 'varchar(2000)')
				/*,@ClaimSettlementAmount = @ClaimXML.value('(event/payments/gross)[1]', 'varchar(2000)')*/
				,@PaymentAccountName = @ClaimXML.value('(event/payments/accountName)[1]', 'varchar(2000)')
				,@PaymentAccountNumber = @ClaimXML.value('(event/payments/accountNumber)[1]', 'varchar(2000)')
				,@PaymentAccountSortcode = @ClaimXML.value('(event/payments/sortCode)[1]', 'varchar(2000)')
				,@PaymentMethod = @ClaimXML.value('(event/payments/paymentMethod)[1]', 'varchar(2000)')
				,@PaymentMethodID = @ClaimXML.value('(event/payments/paymentMethodId)[1]', 'varchar(2000)')
		
		,@WhoCreated = 58550 --@ClaimXML.value('(event/claim/createdById)[1]', 'varchar(2000)')
		,@WhenCreated = @ClaimXML.value('(event/claim/createdDate)[1]', 'date')


		/*GPR 2021-05-04 remove hyphen from PaymentAccountSortCode*/
		IF @PaymentAccountSortcode LIKE '%-%'
		BEGIN
			SELECT @PaymentAccountSortcode = REPLACE(@PaymentAccountSortcode, '-', '')
		END

		/*Clear payments in case we are on the nth loop*/
		DELETE p
		FROM @Payments p

		/*Populate the payments table ready for looping over*/
		INSERT INTO @Payments (VisionPaymentID, Gross, AccountName, AccountNumber, SortCode, PaymentMethod, PaymentMethodID)
		SELECT n.c.value('(id)[1]' , 'INT') AS [VisionPaymentID],
				n.c.value('(gross)[1]' , 'NUMERIC(18,2)') AS [Gross],
				n.c.value('(accountName)[1]' , 'VARCHAR(250)') AS [AccountName],
				n.c.value('(accountNumber)[1]' , 'VARCHAR(250)') AS [AccountNumber],
				n.c.value('(sortCode)[1]' , 'VARCHAR(250)') AS [SortCode],
				n.c.value('(paymentMethod)[1]' , 'VARCHAR(250)') AS [PaymentMethod],
				n.c.value('(paymentMethodId)[1]' , 'INT') AS [PaymentMethodID]
		FROM @ClaimXML.nodes('event/payments') n(c)
		OPTION (OPTIMIZE FOR ( @ClaimXML = NULL ))

		SELECT @ClaimSettlementAmount = SUM(p.Gross)
		FROM @Payments p

		/*Lookup data from OneVision table*/
		SELECT @CustomerID = ISNULL(CustomerID,0), 
				@LeadID = ISNULL(aqv.PALeadID,0), 
				@MatterID = ISNULL(aqv.PAMatterID,0), 
				@OneVisionID = OneVisionID,
				@VisionPolicyTermID = aqv.VisionPolicyTermID /*ALM 2021-04-26 FURKIN-549*/
		FROM OneVision aqv WITH (NOLOCK)
		WHERE aqv.VisionPolicyID = @PolicyID

		/*GPR 2020-07-16 ClaimStatus Mapping*/
		SELECT @ClassicClaimStatusID =	CASE @ClaimStatusID /*ClaimStatus from Vision*/
										WHEN 0 /*New*/ THEN 3818 /*00 New claim*/
										WHEN 1 /*Draft*/ THEN 4468 /*02 Putting on*/
										WHEN 2 /*Rejected by Waiting Period*/ THEN 3819 /*08 Rejected*/
										WHEN 3 /*Rejected by Limit Validation*/ THEN 3819 /*08 Rejected*/
										WHEN 4 /*Rejected by Condition Limit*/ THEN	3819 /*08 Rejected*/
										WHEN 5 /*Rejected by Multiple Conditions*/ THEN 3819 /*08 Rejected*/
										WHEN 6 /*Closed - Paid*/ THEN 4470 /*06 Closed - paid*/
										WHEN 7 /*Settled*/ THEN 4470 /*06 Closed - paid*/
										WHEN 8 /*Rejection Pending*/ THEN 4469 /*04 Assessment*/	
										WHEN 9 /*Fraud Check*/ THEN 4689 /*30 Validation pending*/
										WHEN 10 /*Missing Info*/ THEN 4028 /*03 Missing info*/
										WHEN 11 /*Rejected Manually*/ THEN 3819 /*08 Rejected*/
										WHEN 12 /*Fraud Check OK*/ THEN	4469 /*04 Assessment*/
										WHEN 13 /*Fraud Check Failed*/ THEN	4690 /*31 Investigation required*/
										WHEN 14 /*Reassessment Pending*/ THEN 4469 /*04 Assessment*/
										WHEN 15 /*Rejected by pre existing condition*/ THEN 3819 /*08 Rejected*/
										WHEN 16 /*Rejection by no active cover period*/ THEN 3819 /*08 Rejected*/
										END

		/*GPR 2020-08-04*/
		IF @OneVisionID IS NULL
		BEGIN

			EXEC dbo._C00_LogIt 'Info', '_C600_ClaimDataImport', 'Required IDs are NULL. Import Failed MediateInboundRequest: ', @MediateInboundRequest, @RunAsUserID

			INSERT INTO [dbo].[MediateInboundClaimAudit] (MediateInboundRequest, ProcessedDateTime)
			VALUES (@MediateInboundRequest, dbo.fn_GetDate_Local())

			/*Return empty table to keep the scheduler happy*/
			SELECT *
			FROM @EmptyTableDoNotUse t

			RETURN;

		END

		/*ALM 2021-04-21 FURKIN-539*/
		IF @ClaimSettlementAmount IS NOT NULL
		BEGIN
			/*Store the data in a table for audit history and renewal ratings*/
			INSERT INTO [dbo].[_C600_ClaimData] ([ClaimSystemClaimID], [ClaimSystemID], [ClientID], [CustomerID], [PetName], [PolicyID], [PolicyInceptionDate], [PolicyStartDate], [PolicyStatus], [PolicyStatusID], [ClaimFNOL], [ClaimTreatmentStart], [ClaimTreatmentEnd], [DateOfDeath], [ClaimAmount], [ClaimSettlementAmount], [ClaimStatus], [ClaimStatusID], [AilmentFirstCause], [AilmentSecondCause], [AilmentVeNomCode], [PaymentGross], [PaymentAccountID], [PaymentAccountName], [PaymentAccountNumber], [PaymentAccountSortcode], [PaymentMethod], [PaymentMethodID], [WhoCreated], [WhenCreated])
			VALUES (@VisionClaimID,  1 /*Vision*/,  @ClientID,  @CustomerID,  @PetName,  @LeadID,  @PolicyInceptionDate,  @PolicyStartDate,  @PolicyStatus,  @PolicyStatusID,  @ClaimFNOL,  @ClaimTreatmentStart,  @ClaimTreatmentEnd, @DateOfDeath, @ClaimAmount,  @ClaimSettlementAmount,  @ClaimStatus,  @ClassicClaimStatusID,  @AilmentFirstCause,  @AilmentSecondCause,  @AilmentVeNomCode,  @PaymentGross,  NULL /*PaymentAccountID*/,  @PaymentAccountName,  @PaymentAccountNumber,  @PaymentAccountSortcode,  @PaymentMethod,  @PaymentMethodID,  @WhoCreated,  @WhenCreated)

			SELECT @ClaimID = SCOPE_IDENTITY()

			/*Logging: Created ClaimID*/
			EXEC dbo._C00_LogIt 'Info', '_C600_ClaimDataImport', 'ClaimID', @ClaimID, @RunAsUserID

			/*Insert into #OneVision Claim*/
			INSERT INTO [dbo].[OneVisionClaim] ([VisionClaimID], [VisionPolicyTermID], [HistoricalPolicyTableRow], [PALeadID], [PAMatterID], [ClaimAmount], [AccountID], [AccountNumber], [SortCode], [CustomerPaymentScheduleID], [CustomerLedgerID], [TreatmentStartDate], [TreatmentEndDate])
			VALUES (@VisionClaimID, 0 /*VisionPolicyTermID*/, ISNULL(@HistoricalPolicyTableRow,0) /*HistoricalPolicyTableRow*/, @LeadID, @MatterID, @ClaimAmount, @AccountID, @PaymentAccountNumber, @PaymentAccountSortcode, @CustomerPaymentScheduleID, NULL /*CustomerLedgerID*/, @ClaimTreatmentStart, @ClaimTreatmentEnd)

			SELECT @OneVisionClaimID = SCOPE_IDENTITY()

			EXEC dbo._C00_LogIt 'Info', '_C600_ClaimDataImport', 'OneVision Inserted', @OneVisionClaimID, @RunAsUserID

			SELECT TOP 1 @UNID = p.UNID,
						@PaymentID = p.VisionPaymentID,
						@PaymentGross = p.Gross,
						@PaymentAccountName = p.AccountName,
						@PaymentAccountNumber = p.AccountNumber,
						@PaymentAccountSortcode = @PaymentAccountSortcode, /*ALM 2021-05-05 for FURKIN-579*/
						@PaymentMethod = p.PaymentMethod,
						@PaymentMethodID = p.PaymentMethodID
			FROM @Payments p
			ORDER BY p.UNID

			/*Start payment(s)*/
			WHILE @UNID > @TestID
			BEGIN

				/*Attempt to match on an active claims account for this Customer with the same account and sort code as provided*/
				SELECT @AccountID = a.AccountID
				FROM Account a WITH (NOLOCK)
				WHERE a.AccountUseID = @AccountUseID
				--AND a.Sortcode = @PaymentAccountSortcode
				AND a.Sortcode = RIGHT(@PaymentAccountSortcode,5) /*GPR 2021-05-04 for FURKIN-579*/
				AND a.AccountNumber = @PaymentAccountNumber
				AND a.CustomerID = @CustomerID
				AND a.Active = 1
				AND a.ClientID = @ClientID

				/*ALM 2021-05-06 FURKIN-592*/
				/*Pick up the affinity specific client account ID to write to the customers account record*/ 
				SELECT @ClientAccountID = rldv.ValueInt 
				FROM CustomerDetailValues cdv WITH ( NOLOCK ) 
				INNER JOIN Matter m WITH ( NOLOCK ) on m.CustomerID = cdv.CustomerID 
				INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) on rldv.ResourceListID = cdv.ValueInt
				WHERE m.MatterID = @MatterID 
				AND cdv.DetailFieldID = 170144 /*Affinity Details*/
				AND rldv.DetailFieldID = 179950 /*Claim Payments AccountID*/

				/*IF we have not found a matching account, create a new claim account with the provided details.*/
				IF @AccountID IS NULL
				BEGIN

					/*GPR 2021-05-04 for FURKIN-579 | "BranchNumber" stored as SortCode = RIGHT 5 of SortCode from Vision*/
					/*GPR 2021-05-04 for FURKIN-579 |  "InstitutionNumber" stored as InstitutionNumber = LEFT 4 or 5 of SortCode from Vision, sortcode minus branch number value*/
					INSERT INTO Account (CustomerID, ClientID, AccountHolderName, FriendlyName, AccountNumber, /*Sortcode,*/ Reference, AccountTypeID, Active, AccountUseID, ClientAccountID, WhoCreated, WhoModified, WhenCreated, WhenModified, SortCode, InstitutionNumber)
					VALUES (@CustomerID, @ClientID, @PaymentAccountName, @PaymentAccountName, @PaymentAccountNumber, /*@PaymentAccountSortcode,*/ '_C600_ClaimDataImport', 1, 1, 2, @ClientAccountID, 58550, 58550, @Now, @Now, RIGHT(@PaymentAccountSortcode,5), LEFT(@PaymentAccountSortcode,LEN(@PaymentAccountSortcode) - 5))

					SELECT @AccountID = SCOPE_IDENTITY()

					/*Update the Account Reference to CL+AccountID*/
					UPDATE a
					SET Reference = CONCAT('CL',CAST(@AccountID AS VARCHAR))
					FROM Account a WITH (NOLOCK)
					WHERE a.AccountID = @AccountID

				END

				/*Logging: Created or matched AccountID*/
				EXEC dbo._C00_LogIt 'Info', '_C600_ClaimDataImport', 'AccountID', @AccountID, @RunAsUserID

				/* Insert a record into the Customer Payment Schedule */
				INSERT INTO CustomerPaymentSchedule (ClientID, CustomerID, AccountID, ActualCollectionDate, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, WhoCreated, WhenCreated, ClientAccountID, SourceID)
				VALUES (@ClientID, @CustomerID, @AccountID, @Now, @Now, @Net, @Tax, ABS(@PaymentGross)*-1, 1, 58552, @Now, @ClientAccountID, @PaymentID)

				SELECT @CustomerPaymentScheduleID = SCOPE_IDENTITY()
    
				/*Logging: Created CustomerPaymentScheduleID*/
				EXEC dbo._C00_LogIt 'Info', '_C600_ClaimDataImport', 'CustomerPaymentScheduleID', @CustomerPaymentScheduleID, @RunAsUserID

				UPDATE cd
				SET CustomerPaymentScheduleID = @CustomerPaymentScheduleID
				FROM _C600_ClaimData cd WITH (NOLOCK)
				WHERE cd.ClaimID = @ClaimID

				/*ALM 2021-04-26 FURKIN-549*/
				UPDATE ovc
				SET CustomerPaymentScheduleID = @CustomerPaymentScheduleID
				FROM OneVisionClaim ovc WITH (NOLOCK) 
				WHERE ovc.OneVisionClaimID = @OneVisionClaimID
				
				INSERT INTO dbo.OneVisionClaimPayment([OneVisionClaimID], [VisionPaymentID], [Gross], [AccountID], [AccountNumber], [SortCode], [CustomerPaymentScheduleID], [CustomerLedgerID], [WhenCreated], [WhoCreated], [WhenModified], [WhoModified])
				VALUES (
							@OneVisionClaimID, 
							@PaymentID, 
							@Gross,
							@AccountID, 
							@PaymentAccountNumber, 
							@PaymentAccountSortcode, 
							@CustomerPaymentScheduleID, 
							NULL /*CustomerLedgerID*/, 
							@Now /*WhenCreated*/, 
							@RunAsUserID /*WhoCreated*/, 
							@Now /*WhenModified*/, 
							@RunAsUserID /*WhoModified*/
						)

				INSERT INTO [dbo].[_C600_ClaimData_Payments] ([ClaimSystemClaimID], [C600_ClaimData_ClaimID], [PaymentGross], [AccountName], [AccountNumber], [AccountSortcode], [PaymentMethod], [PaymentMethodID], [AccountID], [CustomerPaymentScheduleID], [WhoCreated], [WhenCreated], [WhoModified], [WhenModified])
				VALUES (
							@VisionClaimID /*ClaimSystemClaimID*/, 
							@ClaimID /*C600_ClaimData_ClaimID*/, 
							@Gross /*PaymentGross*/, 
							@PaymentAccountName /*AccountName*/, 
							@PaymentAccountNumber /*AccountNumber*/, 
							@PaymentAccountSortcode /*AccountSortcode*/, 
							@PaymentMethod /*PaymentMethod*/, 
							@PaymentMethodID /*PaymentMethodID*/, 
							@AccountID /*AccountID*/, 
							@CustomerPaymentScheduleID /*CustomerPaymentScheduleID*/, 
							@RunAsUserID /*WhoCreated*/, 
							@Now /*WhenCreated*/, 
							@RunAsUserID, /*WhoModified*/
							@Now /*WhenModified*/
						)

				SELECT @TestID = @UNID

				SELECT TOP 1 @UNID = p.UNID,
							@PaymentID = p.VisionPaymentID,
							@PaymentGross = p.Gross,
							@PaymentAccountName = p.AccountName,
							@PaymentAccountNumber = p.AccountNumber,
							@PaymentAccountSortcode = p.SortCode,
							@PaymentMethod = p.PaymentMethod,
							@PaymentMethodID = p.PaymentMethodID
				FROM @Payments p
				WHERE p.UNID > @UNID
				ORDER BY p.UNID

			/*End payment(s)*/
			END
		END

		/*Get HistoricalPolicyTableRow from Treatment Start Date*/
		SELECT @HistoricalPolicyTableRow = tr.TableRowID
		FROM TableRows tr WITH (NOLOCK)
		INNER JOIN TableDetailValues tdvStart WITH (NOLOCK) ON tdvStart.TableRowID = tr.TableRowID
		INNER JOIN TableDetailValues tdvEnd WITH (NOLOCK) ON tdvEnd.TableRowID = tr.TableRowID
		WHERE tr.DetailFieldID = 170033 /*Historical Policy*/
		AND @ClaimTreatmentStart BETWEEN tdvStart.ValueDate AND tdvEnd.ValueDate
		AND tr.ClientID = @ClientID
		AND tdvStart.ClientID = @ClientID
		AND tdvEnd.ClientID = @ClientID
		AND tr.MatterID = @MatterID

		/*ALM 2021-04-26 FURKIN-549*/
		UPDATE ovc
		SET HistoricalPolicyTableRow = @HistoricalPolicyTableRow
		FROM OneVisionClaim ovc WITH (NOLOCK) 
		WHERE ovc.OneVisionClaimID = @OneVisionClaimID

		/*Insert into MediateInboundClaimAudit*/
		INSERT INTO [dbo].[MediateInboundClaimAudit] (MediateInboundRequest, ProcessedDateTime)
		VALUES (@MediateInboundRequest, @Now)

		SELECT @OneVisionID = NULL /*ALM 2020-09-25*/
	
	END

    /*Return empty table to keep the scheduler happy*/
	SELECT *
	FROM @EmptyTableDoNotUse t

	/*Logging: Info: Procedure Complete*/
	EXEC dbo._C00_LogIt 'Info', '_C600_ClaimDataImport', 'Procedure Complete - StartLogItID', @LogitID, @RunAsUserID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ClaimDataImport] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_ClaimDataImport] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ClaimDataImport] TO [sp_executeall]
GO
