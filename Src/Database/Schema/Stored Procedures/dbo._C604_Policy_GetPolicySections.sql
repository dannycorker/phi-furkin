SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- ALTER date:  2012-01-31
-- Description:	Gets the pivoted policy sections for the user to select
-- Used by:		DF 145704
-- UPDATES:		2015-11-02	SB	Pull back optional coverage policy sections
--              2016-07-29 JWG  For PHI-specific custom control demo, group into 3 levels for the front end
--				2017-06-23 CPS	Honour the "exclude from policy sections selecor" tick box on the Policy Sections resource list
-- =============================================
CREATE PROCEDURE [dbo].[_C604_Policy_GetPolicySections] 
(
	@MatterID INT,
	@LeadID INT = NULL
)
WITH RECOMPILE
AS
BEGIN
	
	
	SET NOCOUNT ON;
	
	DECLARE @PolicyLeadID INT
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@MatterID)
	
	IF @LeadID IS NOT NULL
	BEGIN
		SELECT @MatterID = MatterID
		FROM dbo.Matter
		WHERE LeadID = @LeadID
	END
	
	DECLARE @CurrentPolicyMatterID INT
	SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)
	
	DECLARE @PolicyPetType INT
	SELECT @PolicyPetType = rldvPetType.ValueInt
	FROM Lead l WITH (NOLOCK)
	INNER JOIN dbo.LeadDetailValues ldvPetType WITH (NOLOCK) on ldvPetType.LeadID = l.LeadID and ldvPetType.DetailFieldID = 144272
	INNER JOIN dbo.ResourceListDetailValues rldvPetType WITH (NOLOCK) on rldvPetType.ResourceListID = ldvPetType.ValueInt and rldvPetType.DetailFieldID = 144269
	WHERE l.LeadID = @PolicyLeadID
	
	-- Now we need to see if there are any optional coverages
	DECLARE	@DateToUse DATE  
	SELECT	@DateToUse = dbo.fn_C00_1272_GetDateToUseForCalcs(@MatterID, NULL, NULL)  

	-- Now find the matching table row and get the coverages   
	DECLARE @OptionalCoverages VARCHAR(2000)
	SELECT @OptionalCoverages = tdvCover.DetailValue
	FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@MatterID) r  
	INNER JOIN dbo.TableDetailValues tdvCover WITH (NOLOCK) ON r.TableRowID = tdvCover.TableRowID AND tdvCover.DetailFieldID = 175737   
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663  
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664  
	WHERE @DateToUse >= tdvStart.ValueDate  
	AND @DateToUse < tdvEnd.ValueDate  
	
	/*
		JWG 2016-07-29 New hierarchy for this demo.  Using "Travel Cover Vet Fees" as an example:
		Top level   = "Travel Cover Vet Fees" 
		Second levels "Travel Cover Vet Fees - Dental - Stomatitis, Ulcerative", 
					  "Travel Cover Vet Fees - Dental - Stomatitis, Proliferative" 
					   etc etc
					   
		We want new groupings as follows:
		Top level    = "Travel Cover Vet Fees" (expandable/collapsible in the front end, and selectable)
		Second level = "Dental"                (expandable/collapsible in the front end, but not selectable)
		Third level  = "Dental - Stomatitis, Ulcerative",  (selectable only)
					   "Dental - Stomatitis, Proliferative" 
					    etc etc
		
		The app also wants 3 separate tables to populate and bind the new tree control, so store results in a @Table for multiple selects
	*/
	DECLARE @Sections TABLE (ResourceListID INT, Section VARCHAR(2000), SubSection VARCHAR(2000), SubSubSection VARCHAR(2000), SortOrder TINYINT, SectionLevel TINYINT)
	
	;WITH TopSections AS
	(
		SELECT tdvRl.ResourceListID, llSection.ItemValue AS Section, dbo.fnGetCharsBeforeOrAfterSeparator (llSubSection.ItemValue, ' - ', 1, 1) AS SubSection, llSubSection.ItemValue AS SubSubSection, CASE WHEN llSection.LookupListItemID = 42878 THEN 0 ELSE 1 END AS SortOrder, CASE  WHEN llSubSection.ItemValue = '-' THEN 1 ELSE 3 END  AS SectionLevel
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvRl WITH (NOLOCK) ON r.TableRowID = tdvRl.TableRowID AND tdvRl.DetailFieldID = 144357
		INNER JOIN dbo.ResourceListDetailValues rlSection WITH (NOLOCK) ON tdvRl.ResourceListID = rlSection.ResourceListID AND rlSection.DetailFieldID = 146189
		INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rlSection.ValueInt = llSection.LookupListItemID
		INNER JOIN dbo.ResourceListDetailValues rlSubSection WITH (NOLOCK) ON tdvRl.ResourceListID = rlSubSection.ResourceListID AND rlSubSection.DetailFieldID = 146190
		INNER JOIN dbo.LookupListItems llSubSection WITH (NOLOCK) ON rlSubSection.ValueInt = llSubSection.LookupListItemID
		LEFT JOIN dbo.TableDetailValues tdvPetType WITH (NOLOCK) ON tdvRL.TableRowID = tdvPetType.TableRowID AND tdvPetType.DetailFieldID = 170013 
		LEFT JOIN ResourceListDetailValues rdvExclude WITH ( NOLOCK ) on rdvExclude.ResourceListID = tdvRl.ResourceListID AND rdvExclude.DetailFieldID = 177540 /*Remove from Policy Sections Selctor*/
		WHERE r.MatterID = @CurrentPolicyMatterID
		AND (tdvPetType.ValueInt IS NULL OR tdvPetType.ValueInt IN (0, @PolicyPetType))
		AND (rdvExclude.ValueInt = 0 or rdvExclude.ValueInt is NULL)
		
		UNION

		SELECT c.ResourceListID, c.Section, dbo.fnGetCharsBeforeOrAfterSeparator (c.SubSection, ' - ', 1, 1) AS SubSection, c.SubSection AS SubSubSection, CASE WHEN c.SectionID = 42878 THEN 0 ELSE 1 END AS SortOrder, CASE  WHEN c.Section = '-' THEN 1 ELSE 3 END  AS SectionLevel
		FROM dbo.fn_C600_Scheme_GetOptionalCoverages(@CurrentPolicyMatterID) c
		INNER JOIN dbo.fnTableOfIDsFromCSV(@OptionalCoverages) id ON c.GroupID = id.AnyID
		WHERE (c.PetTypeID IS NULL OR c.PetTypeID IN (0, @PolicyPetType))
	),
	ExtraMiddleTier AS 
	(
		SELECT t1.ResourceListID, t1.Section, t1.SubSection, '-'  AS SubSubSection, t1.SortOrder, 2 AS SectionLevel, 
		ROW_NUMBER() OVER(PARTITION BY t1.Section, t1.SubSection ORDER BY t1.SubSection) as rn
		FROM TopSections t1
		WHERE t1.SubSection <> '-'
	)
	INSERT @Sections (ResourceListID, Section, SubSection, SubSubSection, SortOrder, SectionLevel)
	SELECT t1.ResourceListID, t1.Section, t1.SubSection, t1.SubSubSection, t1.SortOrder, t1.SectionLevel
	FROM TopSections t1
	UNION ALL
	SELECT m1.ResourceListID, m1.Section, m1.SubSection, m1.SubSubSection, m1.SortOrder, m1.SectionLevel
	FROM ExtraMiddleTier m1 
	WHERE m1.rn = 1

	
	DELETE s
	FROM @Sections s
	WHERE s.ResourceListID = 148149
	AND EXISTS (SELECT * FROM LeadDetailValues l WITH (NOLOCK) 
				WHERE l.LeadID = @PolicyLeadID
				and l.DetailFieldID =  180210
				and l.ValueInt = 5144)

	DELETE s
	FROM @Sections s
	WHERE exists 
		(
		SELECT * /*CPS 2017-06-15 remove excluded sections*/
        FROM TableRows tr WITH ( NOLOCK ) 
        INNER JOIN TableDetailValues tdvSection WITH ( NOLOCK ) on tdvSection.TableRowID = tr.TableRowID AND tdvSection.DetailFieldID = 177503 /*Policy Section*/
        INNER JOIN TableDetailValues tdvFromDate WITH ( NOLOCK ) on tdvFromDate.TableRowID = tr.TableRowID AND tdvFromDate.DetailFieldID = 177513 /*Excluded From Date*/
        WHERE tr.MatterID = dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(@MatterID)
        AND tr.DetailFieldID = 177506 /*Excluded Policy Sections*/
        AND tdvSection.ResourceListID = s.ResourceListID
        AND tdvFromDate.ValueDate <= @DateToUse
		)

	SELECT * FROM @Sections s1 WHERE s1.SectionLevel = 1 ORDER BY SortOrder, Section

	SELECT * FROM @Sections s2 WHERE s2.SectionLevel = 2 ORDER BY SortOrder, Section, SubSection

	SELECT * FROM @Sections s3 WHERE s3.SectionLevel = 3 ORDER BY SortOrder, Section, SubSection, SubSubSection
	
	
	SELECT * FROM @Sections 
	ORDER BY SortOrder, Section, SubSection, SubSubSection
	
	/*SELECT * FROM @Sections sx ORDER BY SortOrder, Section, SubSection, SubSubSection
	FOR XML PATH*/
	
	-- Select the already saved resource list items
	SELECT DISTINCT tdvRLID.ResourceListID AS ResourceListID
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvRLID WITH (NOLOCK) ON r.TableRowID = tdvRLID.TableRowID AND tdvRLID.DetailFieldID = 144350 /*Policy Section*/
	WHERE r.MatterID = @MatterID

	SELECT * FROM @Sections s1 
	LEFT JOIN @Sections s2 ON s2.Section = s1.Section AND s2.SectionLevel = 2
	LEFT JOIN @Sections s3 ON s3.Section = s2.Section AND s3.SubSection = s2.SubSection AND s3.SectionLevel = 3
	WHERE s1.SectionLevel = 1 
		and not exists ( 
						SELECT * /*CPS 2017-06-15 remove excluded sections*/
		                FROM TableRows tr WITH ( NOLOCK ) 
		                INNER JOIN TableDetailValues tdvSection WITH ( NOLOCK ) on tdvSection.TableRowID = tr.TableRowID AND tdvSection.DetailFieldID = 177503 /*Policy Section*/
						INNER JOIN TableDetailValues tdvFromDate WITH ( NOLOCK ) on tdvFromDate.TableRowID = tr.TableRowID AND tdvFromDate.DetailFieldID = 177513 /*Excluded From Date*/
		                WHERE tr.MatterID = dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(@MatterID)
		                AND tr.DetailFieldID = 177506 /*Excluded Policy Sections*/
		                AND tdvSection.ResourceListID IN ( s1.ResourceListID, s2.ResourceListID, s3.ResourceListID )
						AND tdvFromDate.ValueDate <= @DateToUse
		               )
	
	
	ORDER BY s1.SortOrder, s1.Section
	FOR XML AUTO
	
	
	/*SELECT * FROM @Sections sx ORDER BY SortOrder, Section, SubSection, SubSubSection FOR XML PATH*/


END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C604_Policy_GetPolicySections] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C604_Policy_GetPolicySections] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C604_Policy_GetPolicySections] TO [sp_executeall]
GO
