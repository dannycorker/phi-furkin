SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PortalUserOption table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserOption_Delete]
(

	@PortalUserOptionID int   
)
AS


				DELETE FROM [dbo].[PortalUserOption] WITH (ROWLOCK) 
				WHERE
					[PortalUserOptionID] = @PortalUserOptionID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOption_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserOption_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOption_Delete] TO [sp_executeall]
GO
