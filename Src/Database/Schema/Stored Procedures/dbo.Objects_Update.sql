SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Objects table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Objects_Update]
(

	@ObjectID int   ,

	@ClientID int   ,

	@SubClientID int   ,

	@ObjectTypeID int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@Name varchar (250)  ,

	@Details varchar (2000)  ,

	@SourceID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Objects]
				SET
					[ClientID] = @ClientID
					,[SubClientID] = @SubClientID
					,[ObjectTypeID] = @ObjectTypeID
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[Name] = @Name
					,[Details] = @Details
					,[SourceID] = @SourceID
				WHERE
[ObjectID] = @ObjectID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Objects_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Objects_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Objects_Update] TO [sp_executeall]
GO
