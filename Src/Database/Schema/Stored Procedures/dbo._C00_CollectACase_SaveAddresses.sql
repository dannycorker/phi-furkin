SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 26-09-2012
-- Description:	Saves an appointment address
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CollectACase_SaveAddresses]
	@ClientID INT,
	@CustomerID INT,
	@Address1 VARCHAR(2000),
	@Address2 VARCHAR(2000),
	@Town VARCHAR(2000),
	@County VARCHAR(2000),
	@Postcode VARCHAR(2000)	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @FieldAddress1 INT,
		@FieldAddress2 INT,
		@FieldTown INT,
		@FieldCounty INT,
		@FieldPostcode INT,
		@PageFieldID INT,
		@PageID INT,
		@TableRowID INT
		
	SELECT @FieldAddress1=tpfm.ColumnFieldID, @PageFieldID = tpfm.DetailFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) WHERE tpfm.ClientID=@ClientID AND tpfm.ThirdPartyFieldID=339
	SELECT @PageID=d.DetailFieldPageID FROM DetailFields d WITH (NOLOCK) WHERE d.DetailFieldID = @PageFieldID		
			
	SELECT @FieldAddress2=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) WHERE tpfm.ClientID=@ClientID AND tpfm.ThirdPartyFieldID=340
	
	SELECT @FieldTown=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) WHERE tpfm.ClientID=@ClientID AND tpfm.ThirdPartyFieldID=341
	
	SELECT @FieldCounty=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) WHERE tpfm.ClientID=@ClientID AND tpfm.ThirdPartyFieldID=342
	
	SELECT @FieldPostcode=tpfm.ColumnFieldID FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) WHERE tpfm.ClientID=@ClientID AND tpfm.ThirdPartyFieldID=343
	
	INSERT INTO TableRows(ClientID, CustomerID, DetailFieldID, DetailFieldPageID)	SELECT @ClientID, @CustomerID, @PageFieldID, @PageID		
	SELECT @TableRowID = SCOPE_IDENTITY() 

	INSERT INTO TableDetailValues(TableRowID, DetailFieldID, DetailValue, CustomerID, ClientID)	VALUES 			(@TableRowID, @FieldAddress1, @Address1, @CustomerID, @ClientID),			(@TableRowID, @FieldAddress2, @Address2, @CustomerID, @ClientID),			(@TableRowID, @FieldTown, @Town, @CustomerID, @ClientID),			(@TableRowID, @FieldCounty, @County, @CustomerID, @ClientID),			(@TableRowID, @FieldPostcode, @Postcode, @CustomerID, @ClientID)			
	SELECT * FROM TableRows WITH (NOLOCK) WHERE TableRowID=@TableRowID
				
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CollectACase_SaveAddresses] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CollectACase_SaveAddresses] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CollectACase_SaveAddresses] TO [sp_executeall]
GO
