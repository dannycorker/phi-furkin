SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the SqlQueryTables table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryTables_Insert]
(

	@SqlQueryTableID int    OUTPUT,

	@ClientID int   ,

	@SqlQueryID int   ,

	@SqlQueryTableName varchar (50)  ,

	@TableAlias varchar (50)  ,

	@TableDisplayOrder int   ,

	@JoinType varchar (25)  ,

	@JoinText varchar (500)  ,

	@JoinTableID int   ,

	@JoinRTRID int   ,

	@TempTableID int   ,

	@TempJoinTableID int   ,

	@SourceID int   
)
AS


				
				INSERT INTO [dbo].[SqlQueryTables]
					(
					[ClientID]
					,[SqlQueryID]
					,[SqlQueryTableName]
					,[TableAlias]
					,[TableDisplayOrder]
					,[JoinType]
					,[JoinText]
					,[JoinTableID]
					,[JoinRTRID]
					,[TempTableID]
					,[TempJoinTableID]
					,[SourceID]
					)
				VALUES
					(
					@ClientID
					,@SqlQueryID
					,@SqlQueryTableName
					,@TableAlias
					,@TableDisplayOrder
					,@JoinType
					,@JoinText
					,@JoinTableID
					,@JoinRTRID
					,@TempTableID
					,@TempJoinTableID
					,@SourceID
					)
				-- Get the identity value
				SET @SqlQueryTableID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTables_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryTables_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryTables_Insert] TO [sp_executeall]
GO
