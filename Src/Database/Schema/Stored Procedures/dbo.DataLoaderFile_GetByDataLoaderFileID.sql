SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderFile table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFile_GetByDataLoaderFileID]
(

	@DataLoaderFileID int   
)
AS


				SELECT
					[DataLoaderFileID],
					[ClientID],
					[DataLoaderMapID],
					[FileStatusID],
					[SourceFileNameAndPath],
					[TargetFileName],
					[TargetFileLocation],
					[FileFormatID],
					[ScheduledDateTime],
					[DataLoadedDateTime],
					[UploadedBy],
					[RowsInFile]
				FROM
					[dbo].[DataLoaderFile] WITH (NOLOCK) 
				WHERE
										[DataLoaderFileID] = @DataLoaderFileID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile_GetByDataLoaderFileID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFile_GetByDataLoaderFileID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFile_GetByDataLoaderFileID] TO [sp_executeall]
GO
