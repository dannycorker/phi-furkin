SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentQueue table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentQueue_GetByDocumentQueueID]
(

	@DocumentQueueID int   
)
AS


				SELECT
					[DocumentQueueID],
					[ClientID],
					[CustomerID],
					[LeadID],
					[CaseID],
					[DocumentTypeID],
					[WhoCreated],
					[WhenStored],
					[WhenCreated],
					[RequiresApproval],
					[ParsedDocumentTitle],
					[BasedUponLeadEventID]
				FROM
					[dbo].[DocumentQueue] WITH (NOLOCK) 
				WHERE
										[DocumentQueueID] = @DocumentQueueID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue_GetByDocumentQueueID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentQueue_GetByDocumentQueueID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue_GetByDocumentQueueID] TO [sp_executeall]
GO
