SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================		
-- Author:	Ian Slack		
-- Create date: 2016-09-19		
-- Description:	Retrieves all branding for the		
-- root clientid		
-- 2019-10-11 CPS for JIRA LPC-29 | Is this even used?  It's locked to a single brand rather than returning all of them.
-- =============================================		
CREATE PROCEDURE [dbo].[_C600_PA_Search_Branding]
(		
 @ClientId			INT
,@ClientPersonnelID	INT
)
AS		
BEGIN		

	EXEC dbo._C00_LogItXML @ClientID, @ClientID, '_C600_PA_Search_Branding', NULL, 'start getting branding'
	
	DECLARE  @AffinityRlText	VARCHAR(MAX) = ''
			,@CompanyName		VARCHAR(2000) = ''
			,@ResourceListID	INT = 2000184

	SELECT @CompanyName = cl.CompanyName
	FROM Clients cl WITH (NOLOCK) 
	WHERE cl.ClientID = @ClientId
	
	SELECT	 @AffinityRlText += '		<' + REPLACE(df.FieldName,' ','') + '>' + ISNULL(li.ItemValue, rdv.DetailValue) + '</' + REPLACE(df.FieldName,' ','') + '>' + CHAR(13)+CHAR(10)
	FROM ResourceListDetailValues rdv WITH ( NOLOCK )
	INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = rdv.DetailFieldID 
	LEFT JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListID = df.LookupListID AND li.LookupListItemID = rdv.ValueInt
	WHERE rdv.ResourceListID = @ResourceListID /*Affinity = AquariumPet 600*/
	AND rdv.DetailValue <> ''
	AND df.DetailFieldID IN ( 170134,170136 )
	ORDER BY df.FieldName

	IF LEN(@AffinityRlText)>2
	BEGIN
		SELECT @AffinityRlText = LEFT(@AffinityRlText,LEN(@AffinityRlText)-2)
	END

	DECLARE @BrandingXML VARCHAR(MAX) = ''
	SELECT	@BrandingXML =	
	'<ArrayOfBrandDetails>		
	 <BrandDetails>		
		<Urls>		
		<UrlScheme>		
		<Order>0</Order>		
		<Host>localhost</Host>		
		</UrlScheme>		
		<UrlScheme>		
		<Order>0</Order>		
		<Host>demo.aquarium-software.com</Host>		
		<VirtualDirectory>' + @CompanyName + '</VirtualDirectory>		
		</UrlScheme>		
		<UrlScheme>		
		<Order>0</Order>		
		<Host>lpc.aquarium-software.com</Host>		
		</UrlScheme>		
		</Urls>		
		<BrandKey>' + CONVERT(VARCHAR,@ResourceListID) + '</BrandKey>		
		<BrandId>' + CONVERT(VARCHAR,@ResourceListID) + '</BrandId>		
		<BrandName>' + @CompanyName + '</BrandName>		
		<Css>~/Content/Brands/Aquarium/Brand2/brand.css</Css>		
		<ClientId>' + CONVERT(VARCHAR,@ClientId) + '</ClientId>		
		<FooterText>		
		<string></string>
		</FooterText>		
		<MinDogAgeWeeks>8</MinDogAgeWeeks>		
		<MinCatAgeWeeks>8</MinCatAgeWeeks>		
		<MaxDogAgeYears>7</MaxDogAgeYears>		
		<MaxCatAgeYears>9</MaxCatAgeYears>		
		<PetAgeInvalidText>From the information provided we are unable to provide you with a quote. Cover is not available for pets less than 8 weeks and more than 8 years of age for dogs or less than 8 weeks and more than 10 years of age for cats.</PetAgeInvalidText>
		<TotalPremiumNote>* Total price includes Insurance Premium Tax (where applicable). Please note that there may be other taxes or costs payable not imposed by ' + @CompanyName + '.</TotalPremiumNote>
		<CompanyName>' + @CompanyName + '</CompanyName>		
		<DirectDebitCompanyName>' + @CompanyName + '</DirectDebitCompanyName>		
		<MultiPetEnabled>true</MultiPetEnabled>		
		<DirectDebitPath></DirectDebitPath>		
		<CreditCardPath></CreditCardPath>		
		<ContactUsErrorMessage></ContactUsErrorMessage>		
		<SecretQuestion>		
		<string>Postcode¬6</string>
		</SecretQuestion>
' +
		
		@AffinityRlText
		
		+
		'
	 </BrandDetails>		
	</ArrayOfBrandDetails>' 
	
	SELECT @BrandingXML = CAST(CAST(@BrandingXML as XML) as VARCHAR(MAX))
	
	EXEC dbo._C00_LogItXML @ClientId, @ClientId, '_C600_PA_Search_Branding', @BrandingXML, ''

	SELECT @BrandingXML BrandingXML		
	
	
END	






GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_Branding] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Search_Branding] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_Branding] TO [sp_executeall]
GO
