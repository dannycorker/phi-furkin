SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClassNode table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClassNode_Update]
(

	@ClassNodeID int   ,

	@ClientID int   ,

	@ParentClassNodeID int   ,

	@NodeType int   ,

	@Name varchar (255)  ,

	@ClassDescription varchar (255)  ,

	@NodeOrder int   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   ,

	@Deleted bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClassNode]
				SET
					[ClientID] = @ClientID
					,[ParentClassNodeID] = @ParentClassNodeID
					,[NodeType] = @NodeType
					,[Name] = @Name
					,[ClassDescription] = @ClassDescription
					,[NodeOrder] = @NodeOrder
					,[WhoCreated] = @WhoCreated
					,[WhenCreated] = @WhenCreated
					,[WhoModified] = @WhoModified
					,[WhenModified] = @WhenModified
					,[Deleted] = @Deleted
				WHERE
[ClassNodeID] = @ClassNodeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNode_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNode_Update] TO [sp_executeall]
GO
