SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2020-08-09
-- Description:	JIRA PPET-153 | Detailed customer fields for data return
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GetCustomerDataByCustomer] 
	@CustomerID INT
AS
BEGIN

	SET NOCOUNT ON;

    DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()

	SELECT	 l.CustomerID					[CustomerID]
			,l.WhenCreated					[CustomerCreateDate]
			,cu.FirstName					[FirstName]
			,cu.LastName					[LastName]
			,cu.Address1					[Address1]
			,cu.Address2					[Address2]
			,cu.Town						[Town]
			,cu.County						[County]
			,cu.PostCode					[PostCode]
			,cu.DateOfBirth					[DateOfBirth]
			,lli.ItemValue					[MaritalStatus]
			,cu.HomeTelephone				[HomeTelephone]
			,cu.MobileTelephone				[MobileTelephone]
			,cu.EmailAddress				[EmailAddress]
			,tdvSecondaryName.DetailValue	[SecondaryAccountHolderName]
			,tdvSecondaryName.DetailValue	[SecondaryAccountHolderDateOfBirth]
			,tdvSecondaryName.DetailValue	[SecondaryAccountHolderPhoneNumber]
	FROM Lead l WITH (NOLOCK)
	INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = l.CustomerID
	 LEFT JOIN CustomerDetailValues cdv WITH (NOLOCK) on cdv.CustomerID = cu.CustomerID AND cdv.DetailFieldID = 177538 /*Household Marital Status*/
	 LEFT JOIN LookupListItems lli WITH (NOLOCK) on lli.LookupListItemID = cdv.ValueInt
	 LEFT JOIN TableRows tr WITH (NOLOCK) on tr.CustomerID = cu.CustomerID AND tr.DetailFieldID = 178263 /*Account Authorities*/ 
	 LEFT JOIN TableDetailValues tdvSecondaryName WITH (NOLOCK) on tdvSecondaryName.TableRowID = tr.TableRowID AND tdvSecondaryName.DetailFieldID = 178259 /*Name*/
	 LEFT JOIN TableDetailValues tdvSecondaryDoB  WITH (NOLOCK) on  tdvSecondaryDoB.TableRowID = tr.TableRowID AND  tdvSecondaryDoB.DetailFieldID = 313932 /*Date of Birth*/
	 LEFT JOIN TableDetailValues tdvSecondaryPhNo WITH (NOLOCK) on tdvSecondaryPhNo.TableRowID = tr.TableRowID AND tdvSecondaryPhNo.DetailFieldID = 313933 /*Mobile*/
	WHERE l.CustomerID = @CustomerID
	AND l.ClientID = @ClientID
	AND NOT EXISTS ( SELECT * -- First record only for authorised account holders
	                 FROM TableRows trLatest WITH (NOLOCK) 
					 WHERE trLatest.CustomerID = tr.CustomerID
					 AND trLatest.DetailFieldID = tr.DetailFieldID
					 AND trLatest.TableRowID < tr.TableRowID
				   )
	AND NOT EXISTS ( SELECT * -- Earliest lead only to establish creation date
	                 FROM Lead l_earlier WITH (NOLOCK) 
					 WHERE l_earlier.CustomerID = l.CustomerID
					 AND l_earlier.LeadID < l.LeadID )

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetCustomerDataByCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_GetCustomerDataByCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetCustomerDataByCustomer] TO [sp_executeall]
GO
