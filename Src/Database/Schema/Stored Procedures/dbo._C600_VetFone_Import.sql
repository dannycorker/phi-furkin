SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Stephen Ainsworth	
-- Create date: 2017-07-24
-- Description:	Take and process an XML input from VetFone
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_VetFone_Import]
(
	 @Xml XML
)
	
AS
BEGIN

--SELECT @XML = CAST ( '<record policynumber="967"><petname>aria</petname><petspecies>cat</petspecies><petsex>F</petsex><petbreed>dsh</petbreed><petdob>2017-04-04</petdob><signs>poisoning, </signs><advice>see vet - asap</advice><casenotes>drank from the washing up water, could not see on toxins list BUT as so young advised vet asap for further advice. ABCD normal</casenotes><ownername>brad gallager</ownername><username>Anita Boiling</username><regdate>2017-07-13</regdate><regtime>20:18:36</regtime></record>' as XML )

DECLARE @PolicyNumber		VARCHAR (2000),
		@PetName			VARCHAR (2000),
		@PetSpecies			VARCHAR (2000),
		@PetSex				VARCHAR (2000),
		@PetBreed			VARCHAR (2000),
		@PetDOB				VARCHAR (2000),
		@Signs				VARCHAR (2000),
		@Advice				VARCHAR (2000),
		@CaseNotes			VARCHAR (2000),
		@OwnerName			VARCHAR (2000),
		@LogEntry				VARCHAR (2000),
		@UserName			VARCHAR (2000),
		@RegDate			VARCHAR (2000),
		@RegTime			VARCHAR (2000),
		@LeadID				INT,
		@TableRowID			INT,
		@CustomerID			INT,
		@WhoCreated			INT,
		@LeadEventID		INT,
		@DownLoadNote		VARCHAR (2000),
		@WhenImported		DATE,
		@DocumentBLOB		VARBINARY(MAX),
		@LeadDocumentID		INT,
		@ClientID			INT = dbo.fnGetPrimaryClientID(),
		@LeadDocumentTitle	VARCHAR (2000),
		@DocumentTypeID		INT = 56999,
		@UploadDateTime		VARCHAR (2000) = CONVERT(VARCHAR,dbo.fn_GetDate_Local(),121),
		@WhoUploaded		INT = 44412
		,@LogXmlId			INT

		SELECT @PolicyNumber	=	@Xml.value('(record/@policynumber)[1]','varchar(2000)'),
			   @PetName			=	@Xml.value ('(record/petname)[1]','varchar (2000)'),
			   @PetSpecies		=   @Xml.value ('(record/petspecies)[1]','varchar (2000)'),
			   @PetSex			=   @Xml.value ('(record/petsex)[1]','varchar (2000)'),
			   @PetBreed		=   @Xml.value ('(record/petbreed)[1]','varchar (2000)'),
			   @PetDOB			=   @Xml.value ('(record/petdob)[1]','varchar (2000)'),
			   @Signs			=   @Xml.value ('(record/signs)[1]','varchar (2000)'),
			   @Advice			=	@Xml.value ('(record/advice)[1]','varchar (2000)'),
			   @CaseNotes		=   @Xml.value ('(record/casenotes)[1]','varchar (2000)'),
			   @OwnerName		=   @Xml.value ('(record/ownername)[1]','varchar (2000)'),
			   @UserName		=   @Xml.value ('(record/username)[1]','varchar (2000)'),
			   @RegDate			=   @Xml.value ('(record/regdate)[1]','varchar (2000)'), 
			   @RegTime			=   @Xml.value ('(record/regtime)[1]','varchar (2000)')
			  

		SELECT @LeadID = l.LeadID
		FROM Lead l WITH (NOLOCK)
		WHERE l.LeadRef = @PolicyNumber
		AND l.LeadTypeID = 1492

		INSERT LogXML ( ClientID, LogDateTime, ContextID, ContextVarchar, LogXMLEntry )
		SELECT @ClientID, dbo.fn_GetDate_Local(), ISNULL(@LeadID,0), '_C600_VetFone_Import', @Xml
		/*!!! NO SQL HERE !!!*/
		SELECT @LogXmlId = SCOPE_IDENTITY()

		IF @LeadID > 0
		BEGIN
			/*create a table row to insert into*/
			EXEC dbo.TableRows_Insert @TableRowID OUTPUT,@ClientID, @LeadID, NULL,178014,19145,1,1,@CustomerID,NULL,@WhoCreated, NULL, @LogXmlId

			/*convert date fields*/
			SELECT @PetDOB = CONVERT(VARCHAR,@PetDOB,121)
			SELECT @RegDate = CONVERT(VARCHAR,@RegDate,121)

			/*Add simple value into field for each variable*/
			EXEC dbo._C00_SimpleValueIntoField	 178015,@PolicyNumber,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178016,@PetName,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178017,@PetSpecies,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178018,@PetSex,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178019,@PetBreed,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178020,@PetDOB,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178021,@Signs,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178022,@Advice,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178023,@CaseNotes,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178024,@OwnerName,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178025,@UserName,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178026,@RegDate,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178027,@RegTime,@TableRowID,@WhoCreated

			/*Download note*/

			/*Create 'Download note'*/
			SELECT @DocumentBLOB = CAST(@CaseNotes as VARBINARY(MAX))

			/*Create a LeadDocument*/
			EXEC @LeadDocumentID = _C00_CreateLeadDocument @ClientID, @LeadID, @DocumentBLOB,'Download Note', @DocumentTypeID, @UploadDateTime, @WhoUploaded,'VetFone Import',NULL,'TXT',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL

			/*Create HTML string for Download Note*/
			SELECT @DownLoadNote = '<a href="' + dbo.fn_C00_GetUrlByDatabase() + 'ShowImage.aspx?LeadDocumentID=' + ISNULL(CONVERT(VARCHAR,@LeadDocumentID),'1234') + '">Download</a>'
			/*Import created download note*/
			EXEC dbo._C00_SimpleValueIntoField   178028,@DownLoadNote,@TableRowID,@WhoCreated

			/*WhenImported*/
			SELECT @WhenImported = CONVERT(VARCHAR,dbo.fn_GetDate_Local(),121)
			EXEC dbo._C00_SimpleValueIntoField 178029,@WhenImported,@TableRowID,@WhoCreated
			
			SELECT CAST('<message><status>1</status><response>OK</response></message>' as XML)
			--SELECT 1 [Status], 'OK' [Response]
		END
		ELSE 
		BEGIN
			
			/*LOAD INTO FAILED VetFone Log Table - #45216*/

			/*create a table row to insert into*/
			INSERT TableRows ( ClientID, DetailFieldID, DetailFieldPageID, DenyDelete, DenyEdit )
			VALUES ( @ClientID, 179558, 19166, 1, 1 ) /*VetFone Failed Import*/

			/*convert date fields*/
			SELECT @PetDOB = CONVERT(VARCHAR,@PetDOB,121)
			SELECT @RegDate = CONVERT(VARCHAR,@RegDate,121)

			/*Add simple value into field for each variable*/
			EXEC dbo._C00_SimpleValueIntoField	 178015,@PolicyNumber,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178016,@PetName,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178017,@PetSpecies,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178018,@PetSex,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178019,@PetBreed,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178020,@PetDOB,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178021,@Signs,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178022,@Advice,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178023,@CaseNotes,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178024,@OwnerName,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178025,@UserName,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178026,@RegDate,@TableRowID,@WhoCreated
			EXEC dbo._C00_SimpleValueIntoField   178027,@RegTime,@TableRowID,@WhoCreated
	
		END
		
	RETURN

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_VetFone_Import] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_VetFone_Import] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_VetFone_Import] TO [sp_executeall]
GO
