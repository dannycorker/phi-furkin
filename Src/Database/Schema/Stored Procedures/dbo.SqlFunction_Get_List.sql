SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SqlFunction table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlFunction_Get_List]

AS


				
				SELECT
					[SqlFunctionID],
					[SqlFunctionName],
					[FunctionAlias],
					[FunctionDescription],
					[RestrictedToDataTypeID],
					[WhenCreated],
					[WhenModified],
					[IsPreferred],
					[Example],
					[ExampleResult],
					[ResultDataTypeID],
					[ExpressionMask],
					[IsAggregate]
				FROM
					[dbo].[SqlFunction] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlFunction_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlFunction_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlFunction_Get_List] TO [sp_executeall]
GO
