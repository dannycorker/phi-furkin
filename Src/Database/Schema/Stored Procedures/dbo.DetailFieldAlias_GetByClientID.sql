SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailFieldAlias table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldAlias_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DetailFieldAliasID],
					[ClientID],
					[LeadTypeID],
					[DetailFieldID],
					[DetailFieldAlias]
				FROM
					[dbo].[DetailFieldAlias] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldAlias_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias_GetByClientID] TO [sp_executeall]
GO
