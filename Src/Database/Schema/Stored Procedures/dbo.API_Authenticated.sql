SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-10-28	
-- Description:	API_Ident_Get
-- =============================================
CREATE PROCEDURE [dbo].[API_Authenticated]
	@ClientPersonnelID INT,
	@ThirdPartySystemID INT,
	@AppKey VARCHAR(250),
	@SessionID VARCHAR(50)
AS
BEGIN
	
	SELECT	ident.IdentID
	FROM	API_Ident ident WITH (NOLOCK)
	INNER JOIN ClientPersonnel cp WITH (NOLOCK) ON  cp.ClientPersonnelID = ident.ClientPersonnelID AND cp.ThirdPartySystemId = ident.ThirdPartySystemID
	INNER JOIN API_App app WITH (NOLOCK) ON app.AppKey = ident.AppKey AND app.Enabled = 1
	WHERE	cp.AccountDisabled = 0 
	AND		cp.ClientPersonnelID = @ClientPersonnelID
	AND		ident.AppKey = @AppKey
	AND		ident.ClientPersonnelID = @ClientPersonnelID 
	AND		ident.ThirdPartySystemID = @ThirdPartySystemID 
	AND		ident.SessionID = @SessionID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Authenticated] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[API_Authenticated] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Authenticated] TO [sp_executeall]
GO
