SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2020-05-29
-- Description:	Get Server Location For EFT Downloads
-- 2020-07-07 GPR updated folder to QA/BACS/ToAquarium, and QA/BACS/ToAquarium/ARUDD
-- 2020-07-21 GPR updated with Password placeholder
-- =============================================
CREATE PROCEDURE [dbo].[_C607_GetEFTServers]
	@FileTypeID INT = 1
AS
BEGIN

	SET NOCOUNT ON;

	/*2021-02-04 ACE Private key for dev*/
	SELECT	e.Folder AS [Folder],
			e.ServerURL AS [ServerURL],
			e.UserName AS [UserName],
			e.Password AS [Password],
			e.[Key] AS [Key],
			e.PortNumber AS [Portnumber]
	FROM EFTServers e
	WHERE e.ClientID = 607
	AND e.FileTypeID = @FileTypeID

END
GO
