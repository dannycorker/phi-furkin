SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the AutoAdjudication table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudication_Insert]
(

	@AutoAdjudicationID int    OUTPUT,

	@EventChoiceID int   ,

	@AutoAdjudicationTypeID int   ,

	@ObjectID int   ,

	@AutoAdjudicationFieldTypeID int   ,

	@Comparison varchar (50)  ,

	@Value1 varchar (250)  ,

	@Value2 varchar (250)  ,

	@AppliesTo varchar (50)  ,

	@Count int   ,

	@ValueTypeID int   
)
AS


				
				INSERT INTO [dbo].[AutoAdjudication]
					(
					[EventChoiceID]
					,[AutoAdjudicationTypeID]
					,[ObjectID]
					,[AutoAdjudicationFieldTypeID]
					,[Comparison]
					,[Value1]
					,[Value2]
					,[AppliesTo]
					,[Count]
					,[ValueTypeID]
					)
				VALUES
					(
					@EventChoiceID
					,@AutoAdjudicationTypeID
					,@ObjectID
					,@AutoAdjudicationFieldTypeID
					,@Comparison
					,@Value1
					,@Value2
					,@AppliesTo
					,@Count
					,@ValueTypeID
					)
				-- Get the identity value
				SET @AutoAdjudicationID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudication_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudication_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudication_Insert] TO [sp_executeall]
GO
