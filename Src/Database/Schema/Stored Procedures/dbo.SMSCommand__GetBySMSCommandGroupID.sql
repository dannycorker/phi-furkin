SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 03-01-2014
-- Description:	Gets a list of SMSCommand(s) by the SMSCommandGroupID
-- =============================================
CREATE PROCEDURE [dbo].[SMSCommand__GetBySMSCommandGroupID]

	@SMSCommandGroupID INT

AS
BEGIN

	SET NOCOUNT ON;

	SELECT * FROM SMSCommand WITH (NOLOCK) WHERE SMSCommandGroupID=@SMSCommandGroupID


END


GO
GRANT VIEW DEFINITION ON  [dbo].[SMSCommand__GetBySMSCommandGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSCommand__GetBySMSCommandGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSCommand__GetBySMSCommandGroupID] TO [sp_executeall]
GO
