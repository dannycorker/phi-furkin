SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2016-11-17
-- Description:	Get Next Available Date for Change of BACS
--	2016-11-29 CPS obey First Acceptable Payment Date
--  2020-03-02 GPR for JIRA AAG-202 | Added check on CountryID from ClientID as the switch for selecting @FirstAcceptablePaymentDate as AUS EFT does not require an AccountMandate
-- =============================================
CREATE PROCEDURE [dbo].[_C00_BACS_GetFirstApplicableDates]
	@ClientID INT,
	@CaseID INT,
	@ClientPersonnelID INT
AS
BEGIN

	SET NOCOUNT ON;

	-- Default CurrentDate to now if it's not specified
	DECLARE @CurrentDate DATE = dbo.fn_GetDate_Local(),
			@MinNumberOfFollowUpDays INT,
			@MaxNumberOfFollowUpDays INT,
			@CountryID INT,
			@Today DATE

	SELECT	 @MinNumberOfFollowUpDays = 2--10, --dbo.fnGetSimpleDvByThirdPartyField()
			,@MaxNumberOfFollowUpDays = 20--15
	
	/*
	Account Mandates include a first available payment date.  
	The payment schedule won't generate a payment before that date, so don't offer it to the user
	*/
	DECLARE @FirstAcceptablePaymentDate DATE
	
	SELECT TOP 1 @FirstAcceptablePaymentDate = am.FirstAcceptablePaymentDate
	FROM AccountMandate am WITH ( NOLOCK )
	INNER JOIN PurchasedProduct pp WITH ( NOLOCK ) on pp.AccountID = am.AccountID
	INNER JOIN Matter m WITH (NOLOCK) on m.MatterID = pp.ObjectID
	WHERE pp.ObjectTypeID = 2 /*Matter*/
	AND m.CaseID = @CaseID	
	ORDER BY am.AccountMandateID DESC


	/*GPR 2020-03-02 for AAG-202*/
	SELECT @CountryID = cl.CountryID FROM Clients cl WITH (NOLOCK)
	WHERE cl.ClientID = @ClientID

	IF @CountryID = 14
	BEGIN
		
		SELECT @Today = dbo.fn_GetDate_Local()

		SELECT @FirstAcceptablePaymentDate =	min(w.[Date]) 
												FROM workingdays w 
												WHERE w.[Date] =  @Today 
												AND w.[IsWorkDay] = 1
	END



	/*
	Assuming we've got a value for the days, select that many records
	from the WorkingDays table and get the last one as the working-days FollowUp date
	*/
	IF @MinNumberOfFollowUpDays > -1 AND @MinNumberOfFollowUpDays IS NOT NULL
	BEGIN
		;WITH ApplicableWorkingDays AS
		(
			SELECT TOP (@MinNumberOfFollowUpDays) *
			FROM WorkingDays wd WITH (NOLOCK) 
			WHERE wd.IsWorkDay = 1
			and wd.CharDate > CONVERT(VARCHAR(10), @CurrentDate, 120) 
			AND (wd.[Date] >= @FirstAcceptablePaymentDate or @FirstAcceptablePaymentDate is NULL)
			ORDER BY wd.date
		)

		SELECT TOP 1 *
		FROM ApplicableWorkingDays WITH (NOLOCK) 
		ORDER BY ApplicableWorkingDays.Date DESC
	END	

	IF @MaxNumberOfFollowUpDays > -1 AND @MaxNumberOfFollowUpDays IS NOT NULL
	BEGIN
		;WITH ApplicableWorkingDays AS
		(
			SELECT TOP (@MaxNumberOfFollowUpDays) *
			FROM WorkingDays wd WITH (NOLOCK) 
			WHERE wd.IsWorkDay = 1
			and wd.CharDate > CONVERT(VARCHAR(10), @CurrentDate, 120) 
			AND (wd.[Date] >= @FirstAcceptablePaymentDate or @FirstAcceptablePaymentDate is NULL)
			ORDER BY wd.date
		)

		SELECT TOP 1 *
		FROM ApplicableWorkingDays WITH (NOLOCK) 
		ORDER BY ApplicableWorkingDays.Date DESC
	END	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_BACS_GetFirstApplicableDates] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_BACS_GetFirstApplicableDates] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_BACS_GetFirstApplicableDates] TO [sp_executeall]
GO
