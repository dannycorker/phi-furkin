SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-08-12
-- Description:	List all country records
-- =============================================
CREATE PROCEDURE [dbo].[cou] 
	@SortOrder tinyint = 1, 
	@AlphaCode varchar(3) = NULL  
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	FROM dbo.Country c WITH (NOLOCK) 
	WHERE (c.Alpha2Code = @AlphaCode OR c.Alpha3Code = @AlphaCode OR @AlphaCode IS NULL) 
	ORDER BY CASE @SortOrder WHEN 1 THEN c.CountryName ELSE c.Alpha3Code END ASC
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[cou] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[cou] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[cou] TO [sp_executeall]
GO
