SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[EventSubtypeDefault__GetLeadTypeDefaults]
(
	@LeadTypeID int
)
AS
				SET ANSI_NULLS OFF
				
				SELECT
					[EventSubtypeDefaultID],
					[ClientID],
					[EventSubtypeID],
					[EventTypeID],
					[LeadTypeID],
					[ClientPersonnelID]
				FROM
					[dbo].[EventSubtypeDefault]
				WHERE
					[LeadTypeID] = @LeadTypeID
					AND [ClientPersonnelID] IS NULL
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			






GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault__GetLeadTypeDefaults] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubtypeDefault__GetLeadTypeDefaults] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtypeDefault__GetLeadTypeDefaults] TO [sp_executeall]
GO
