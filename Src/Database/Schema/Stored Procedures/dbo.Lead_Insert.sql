SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the Lead table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Lead_Insert]
(

	@LeadID int    OUTPUT,

	@ClientID int   ,

	@LeadRef varchar (100)  ,

	@CustomerID int   ,

	@LeadTypeID int   ,

	@AquariumStatusID int   ,

	@ClientStatusID int   ,

	@BrandNew bit   ,

	@Assigned bit   ,

	@AssignedTo int   ,

	@AssignedBy int   ,

	@AssignedDate datetime   ,

	@RecalculateEquations bit   ,

	@Password varchar (65)  ,

	@Salt varchar (25)  ,

	@WhenCreated datetime   
)
AS


				
				INSERT INTO [dbo].[Lead]
					(
					[ClientID]
					,[LeadRef]
					,[CustomerID]
					,[LeadTypeID]
					,[AquariumStatusID]
					,[ClientStatusID]
					,[BrandNew]
					,[Assigned]
					,[AssignedTo]
					,[AssignedBy]
					,[AssignedDate]
					,[RecalculateEquations]
					,[Password]
					,[Salt]
					,[WhenCreated]
					)
				VALUES
					(
					@ClientID
					,@LeadRef
					,@CustomerID
					,@LeadTypeID
					,@AquariumStatusID
					,@ClientStatusID
					,@BrandNew
					,@Assigned
					,@AssignedTo
					,@AssignedBy
					,@AssignedDate
					,@RecalculateEquations
					,@Password
					,@Salt
					,@WhenCreated
					)
				-- Get the identity value
				SET @LeadID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_Insert] TO [sp_executeall]
GO
