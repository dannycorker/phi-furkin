SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ScriptPositionTarget table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptPositionTarget_Find]
(

	@SearchUsingOR bit   = null ,

	@ScriptPositionTargetID int   = null ,

	@ScriptID int   = null ,

	@SectionID int   = null ,

	@ClientID int   = null ,

	@PositionName varchar (250)  = null ,

	@LinkText varchar (250)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ScriptPositionTargetID]
	, [ScriptID]
	, [SectionID]
	, [ClientID]
	, [PositionName]
	, [LinkText]
    FROM
	[dbo].[ScriptPositionTarget] WITH (NOLOCK) 
    WHERE 
	 ([ScriptPositionTargetID] = @ScriptPositionTargetID OR @ScriptPositionTargetID IS NULL)
	AND ([ScriptID] = @ScriptID OR @ScriptID IS NULL)
	AND ([SectionID] = @SectionID OR @SectionID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([PositionName] = @PositionName OR @PositionName IS NULL)
	AND ([LinkText] = @LinkText OR @LinkText IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ScriptPositionTargetID]
	, [ScriptID]
	, [SectionID]
	, [ClientID]
	, [PositionName]
	, [LinkText]
    FROM
	[dbo].[ScriptPositionTarget] WITH (NOLOCK) 
    WHERE 
	 ([ScriptPositionTargetID] = @ScriptPositionTargetID AND @ScriptPositionTargetID is not null)
	OR ([ScriptID] = @ScriptID AND @ScriptID is not null)
	OR ([SectionID] = @SectionID AND @SectionID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([PositionName] = @PositionName AND @PositionName is not null)
	OR ([LinkText] = @LinkText AND @LinkText is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptPositionTarget_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptPositionTarget_Find] TO [sp_executeall]
GO
