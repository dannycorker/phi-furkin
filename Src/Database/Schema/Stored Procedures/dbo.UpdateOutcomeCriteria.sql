SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/****** Object:  Stored Procedure dbo.UpdateOutcomeCriteria    Script Date: 08/09/2006 12:22:38 ******/

CREATE PROCEDURE [dbo].[UpdateOutcomeCriteria]

@OutcomeCriteriaID int,
@OutcomeID int,
@QuestionPossibleAnswerID int,
@ClientID int = null

as

Update OutcomeCriterias 
Set OutcomeID = @OutcomeID, QuestionPossibleAnswerID = @QuestionPossibleAnswerID
Where OutcomeCriteriaID = @OutcomeCriteriaID







GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateOutcomeCriteria] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateOutcomeCriteria] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateOutcomeCriteria] TO [sp_executeall]
GO
