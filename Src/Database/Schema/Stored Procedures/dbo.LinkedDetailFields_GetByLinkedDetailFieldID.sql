SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LinkedDetailFields table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LinkedDetailFields_GetByLinkedDetailFieldID]
(

	@LinkedDetailFieldID int   
)
AS


				SELECT
					[LinkedDetailFieldID],
					[LeadTypeIDTo],
					[LeadTypeIDFrom],
					[DetailFieldIDTo],
					[DetailFieldIDFrom],
					[DisplayOnPageID],
					[FieldOrder],
					[Enabled],
					[History],
					[LeadOrMatter],
					[FieldName],
					[LeadLinkedTo],
					[LeadLinkedFrom],
					[IncludeLinkedField],
					[ClientID]
				FROM
					[dbo].[LinkedDetailFields] WITH (NOLOCK) 
				WHERE
										[LinkedDetailFieldID] = @LinkedDetailFieldID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LinkedDetailFields_GetByLinkedDetailFieldID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LinkedDetailFields_GetByLinkedDetailFieldID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LinkedDetailFields_GetByLinkedDetailFieldID] TO [sp_executeall]
GO
