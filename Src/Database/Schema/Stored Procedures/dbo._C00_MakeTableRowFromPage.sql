SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex ELger
-- Create date: 2008-06-20
-- Description:	Deletes Fields MDV or LDV so they can be re-used as mandatory fields
--              dcm updated 12/07/12 to add customer level pages/tables
-- =============================================
CREATE PROCEDURE [dbo].[_C00_MakeTableRowFromPage]
(
	@FromDetailFieldPageID int,
	@TableDetailFieldID int, --LeadOrMatter = 2 WTID = 16 or 19
	@CaseID int,
	@LeaveOriginalDetailsIntact int = 0
)
	
AS
BEGIN

	SET NOCOUNT ON

	Declare @TableDetailFieldPageID int,
			@FromLeadOrMatter int,
			@ToQuestionTypeID int,
			@ToDetailFieldPageID int,
			@LeadID int,
			@CustomerID int,
			@ClientID int,
			@TableRowID int,
			@MatterID int,
			@TestID int,
			@LeadOrMatter int

	Select @FromLeadOrMatter = LeadOrMatter
	From DetailFields WITH (NOLOCK) 
	Where DetailFieldPageID = @FromDetailFieldPageID

	Select @TableDetailFieldPageID = DetailFieldPageID, @ToQuestionTypeID = QuestionTypeID, @ClientID = ClientID, @LeadOrMatter = LeadOrMatter 
	From DetailFields WITH (NOLOCK) 
	Where DetailFieldID = @TableDetailFieldID

	Select @LeadID = LeadID, @CustomerID = CustomerID
	From Matter WITH (NOLOCK) 
	Where CaseID = @CaseID

	If @FromLeadOrMatter = 1
	Begin

		INSERT INTO TableRows(ClientID, CustomerID, LeadID, MatterID, DetailFieldID, DetailFieldPageID) 
		SELECT @ClientID, @CustomerID, @LeadID, NULL, @TableDetailFieldID, @TableDetailFieldPageID

		Select @TableRowID = SCOPE_IDENTITY()

		INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID)
		select @TableRowID, NULL, dfb.DetailFieldID, leaddetailvalues.DetailValue, leaddetailvalues.LeadID, NULL, leaddetailvalues.ClientID
		from leaddetailvalues WITH (NOLOCK) 
		inner join detailfieldalias dfa WITH (NOLOCK) on dfa.detailfieldid = leaddetailvalues.detailfieldid
		inner join detailfieldalias dfb WITH (NOLOCK) on dfb.DetailFieldAlias = dfa.DetailFieldAlias and dfb.LeadTypeID IS NULL and dfb.ClientID = @ClientID
		inner join detailfields df WITH (NOLOCK) on df.detailfieldid = leaddetailvalues.detailfieldid and df.detailfieldpageid = @FromDetailFieldPageID
		inner join detailfields df_res WITH (NOLOCK) on df_res.DetailFieldID = dfb.DetailFieldID and df_res.QuestionTypeID <> 14
		where leadid = @LeadID 


		If @ToQuestionTypeID = 16
		Begin

			INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID)
			select @TableRowID, leaddetailvalues.DetailValue, dfb.DetailFieldID, NULL, leaddetailvalues.LeadID, NULL, leaddetailvalues.ClientID
			from leaddetailvalues WITH (NOLOCK) 
			inner join detailfieldalias dfa WITH (NOLOCK) on dfa.detailfieldid = leaddetailvalues.detailfieldid
			inner join detailfieldalias dfb WITH (NOLOCK) on dfb.DetailFieldAlias = dfa.DetailFieldAlias and dfb.LeadTypeID IS NULL and dfb.ClientID = @ClientID
			inner join detailfields df WITH (NOLOCK) on df.detailfieldid = leaddetailvalues.detailfieldid and df.detailfieldpageid = @FromDetailFieldPageID
			inner join detailfields df_res WITH (NOLOCK) on df_res.DetailFieldID = dfb.DetailFieldID and df_res.QuestionTypeID = 14
			where leadid = @LeadID 

		End

		IF @LeaveOriginalDetailsIntact = 0
		BEGIN

			Delete leaddetailvalues
			From leaddetailvalues WITH (NOLOCK) 
			inner join detailfields df on df.detailfieldid = leaddetailvalues.detailfieldid and df.detailfieldpageid = @FromDetailFieldPageID
			where leadid = @LeadID 

		END

	End

	If @FromLeadOrMatter = 2
	Begin

		Select @MatterID = 0, @TestID = 0

		Select Top 1 @MatterID = MatterID
		From Matter WITH (NOLOCK) 
		Where CaseID = @CaseID
		Order By MatterID

		while (@MatterID > 0 AND @MatterID > @TestID)
		Begin

			INSERT INTO TableRows(ClientID, CustomerID, LeadID, MatterID, DetailFieldID, DetailFieldPageID) 
			SELECT @ClientID, CASE @LeadOrMatter WHEN 10 THEN @CustomerID ELSE NULL END, CASE WHEN @LeadOrMatter IN (1,2) THEN @LeadID ELSE NULL END, CASE @LeadOrMatter WHEN 2 THEN @MatterID ELSE NULL END, @TableDetailFieldID, @TableDetailFieldPageID

			Select @TableRowID = SCOPE_IDENTITY()

			INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID, CustomerID)
			select @TableRowID, NULL, dfb.DetailFieldID, matterdetailvalues.DetailValue, CASE WHEN @LeadOrMatter IN (1,2) THEN matterdetailvalues.LeadID ELSE NULL END, CASE @LeadOrMatter WHEN 2 THEN matterdetailvalues.MatterID ELSE NULL END, matterdetailvalues.ClientID, CASE @LeadOrMatter WHEN 10 THEN @CustomerID ELSE NULL END
			from matterdetailvalues WITH (NOLOCK) 
			inner join detailfieldalias dfa WITH (NOLOCK) on dfa.detailfieldid = matterdetailvalues.detailfieldid
			inner join detailfieldalias dfb WITH (NOLOCK) on dfb.DetailFieldAlias = dfa.DetailFieldAlias and dfb.LeadTypeID IS NULL and dfb.ClientID = @ClientID
			inner join detailfields df WITH (NOLOCK) on df.detailfieldid = matterdetailvalues.detailfieldid and df.detailfieldpageid = @FromDetailFieldPageID
			inner join detailfields df_res WITH (NOLOCK) on df_res.DetailFieldID = dfb.DetailFieldID and df_res.QuestionTypeID <> 14
			where matterid = @MatterID 


			If @ToQuestionTypeID = 16
			Begin

				INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID, CustomerID)
				select @TableRowID, matterdetailvalues.DetailValue, dfb.DetailFieldID, NULL, CASE WHEN @LeadOrMatter IN (1,2) THEN matterdetailvalues.LeadID ELSE NULL END, CASE @LeadOrMatter WHEN 2 THEN matterdetailvalues.MatterID ELSE NULL END, matterdetailvalues.ClientID, CASE @LeadOrMatter WHEN 10 THEN @CustomerID ELSE NULL END
				from matterdetailvalues WITH (NOLOCK) 
				inner join detailfieldalias dfa WITH (NOLOCK) on dfa.detailfieldid = matterdetailvalues.detailfieldid
				inner join detailfieldalias dfb WITH (NOLOCK) on dfb.DetailFieldAlias = dfa.DetailFieldAlias and dfb.LeadTypeID IS NULL and dfb.ClientID = @ClientID
				inner join detailfields df WITH (NOLOCK) on df.detailfieldid = matterdetailvalues.detailfieldid and df.detailfieldpageid = @FromDetailFieldPageID
				inner join detailfields df_res WITH (NOLOCK) on df_res.DetailFieldID = dfb.DetailFieldID and df_res.QuestionTypeID = 14
				where matterid = @MatterID 

			End

			IF @LeaveOriginalDetailsIntact = 0
			BEGIN

				Delete matterdetailvalues
				From MatterDetailValues 
				inner join detailfields df on df.detailfieldid = matterdetailvalues.detailfieldid and df.detailfieldpageid = @FromDetailFieldPageID
				where matterid = @MatterID 

			END

			Select @TestID = @MatterID

			Select Top 1 @MatterID = MatterID
			From Matter WITH (NOLOCK) 
			Where CaseID = @CaseID
			And MatterID > @MatterID
			Order By MatterID

		End

	End

	If @FromLeadOrMatter = 10
	Begin

		INSERT INTO TableRows(ClientID, CustomerID, LeadID, MatterID, DetailFieldID, DetailFieldPageID) 
		SELECT @ClientID, @CustomerID, NULL, NULL, @TableDetailFieldID, @TableDetailFieldPageID

		Select @TableRowID = SCOPE_IDENTITY()

		INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, CustomerID, LeadID, MatterID, ClientID)
		select @TableRowID, NULL, dfb.DetailFieldID, CustomerDetailValues.DetailValue, @CustomerID, NULL, NULL, CustomerDetailValues.ClientID
		from CustomerDetailValues WITH (NOLOCK) 
		inner join detailfieldalias dfa WITH (NOLOCK) on dfa.detailfieldid = CustomerDetailValues.detailfieldid
		inner join detailfieldalias dfb WITH (NOLOCK) on dfb.DetailFieldAlias = dfa.DetailFieldAlias and dfb.LeadTypeID IS NULL and dfb.ClientID = @ClientID
		inner join detailfields df WITH (NOLOCK) on df.detailfieldid = CustomerDetailValues.detailfieldid and df.detailfieldpageid = @FromDetailFieldPageID
		inner join detailfields df_res WITH (NOLOCK) on df_res.DetailFieldID = dfb.DetailFieldID and df_res.QuestionTypeID <> 14
		where CustomerID = @CustomerID 


		If @ToQuestionTypeID = 16
		Begin

			INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, CustomerID, LeadID, MatterID, ClientID)
			select @TableRowID, CustomerDetailValues.DetailValue, dfb.DetailFieldID, NULL, @CustomerID,NULL, NULL, CustomerDetailValues.ClientID
			from CustomerDetailValues WITH (NOLOCK) 
			inner join detailfieldalias dfa WITH (NOLOCK) on dfa.detailfieldid = CustomerDetailValues.detailfieldid
			inner join detailfieldalias dfb WITH (NOLOCK) on dfb.DetailFieldAlias = dfa.DetailFieldAlias and dfb.LeadTypeID IS NULL and dfb.ClientID = @ClientID
			inner join detailfields df WITH (NOLOCK) on df.detailfieldid = CustomerDetailValues.detailfieldid and df.detailfieldpageid = @FromDetailFieldPageID
			inner join detailfields df_res WITH (NOLOCK) on df_res.DetailFieldID = dfb.DetailFieldID and df_res.QuestionTypeID = 14
			where CustomerID = @CustomerID 

		End

		IF @LeaveOriginalDetailsIntact = 0
		BEGIN
		
			-- only delete fields with matching alias
			Delete CustomerDetailValues
			From CustomerDetailValues WITH (NOLOCK) 
			inner join detailfieldalias dfa WITH (NOLOCK) on dfa.detailfieldid = CustomerDetailValues.detailfieldid
			inner join detailfieldalias dfb WITH (NOLOCK) on dfb.DetailFieldAlias = dfa.DetailFieldAlias and dfb.LeadTypeID IS NULL and dfb.ClientID = @ClientID
			inner join detailfields df WITH (NOLOCK) on df.detailfieldid = CustomerDetailValues.detailfieldid and df.detailfieldpageid = @FromDetailFieldPageID
			where CustomerID = @CustomerID 

		END

	End

	If @FromLeadOrMatter = 11 /*CaseDetailValues*/
	Begin

		Select @TestID = 0

		Select Top 1 @MatterID = MatterID
		From Matter WITH (NOLOCK) 
		Where CaseID = @CaseID
		Order By MatterID

		while (@CaseID > 0 AND @CaseID > @TestID)
		Begin

			INSERT INTO TableRows(ClientID, CustomerID, LeadID, MatterID, DetailFieldID, DetailFieldPageID) 
			SELECT @ClientID, CASE @LeadOrMatter WHEN 10 THEN @CustomerID ELSE NULL END, CASE WHEN @LeadOrMatter IN (1,2) THEN @LeadID ELSE NULL END, CASE @LeadOrMatter WHEN 2 THEN @MatterID ELSE NULL END, @TableDetailFieldID, @TableDetailFieldPageID

			Select @TableRowID = SCOPE_IDENTITY()

			INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID, CustomerID)
			select @TableRowID, NULL, dfb.DetailFieldID, cdv.DetailValue, CASE WHEN @LeadOrMatter IN (1,2) THEN m.LeadID ELSE NULL END, CASE @LeadOrMatter WHEN 2 THEN m.MatterID ELSE NULL END, m.ClientID, CASE @LeadOrMatter WHEN 10 THEN @CustomerID ELSE NULL END
			from CaseDetailValues cdv WITH (NOLOCK) 
			INNER JOIN dbo.Matter m WITH (NOLOCK) on m.CaseID = cdv.CaseID
			inner join detailfieldalias dfa WITH (NOLOCK) on dfa.detailfieldid = cdv.detailfieldid
			inner join detailfieldalias dfb WITH (NOLOCK) on dfb.DetailFieldAlias = dfa.DetailFieldAlias and dfb.LeadTypeID IS NULL and dfb.ClientID = @ClientID
			inner join detailfields df WITH (NOLOCK) on df.detailfieldid = cdv.detailfieldid and df.detailfieldpageid = @FromDetailFieldPageID
			inner join detailfields df_res WITH (NOLOCK) on df_res.DetailFieldID = dfb.DetailFieldID and df_res.QuestionTypeID <> 14
			where m.MatterID = @MatterID


			If @ToQuestionTypeID = 16
			Begin

				INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID, CustomerID)
				select @TableRowID, cdv.DetailValue, dfb.DetailFieldID, NULL, CASE WHEN @LeadOrMatter IN (1,2) THEN m.LeadID ELSE NULL END, CASE @LeadOrMatter WHEN 2 THEN m.MatterID ELSE NULL END, m.ClientID, CASE @LeadOrMatter WHEN 10 THEN @CustomerID ELSE NULL END
				from CaseDetailValues cdv WITH (NOLOCK) 
				INNER JOIN dbo.Matter m WITH (NOLOCK) on m.CaseID = cdv.CaseID
				inner join detailfieldalias dfa WITH (NOLOCK) on dfa.detailfieldid = cdv.detailfieldid
				inner join detailfieldalias dfb WITH (NOLOCK) on dfb.DetailFieldAlias = dfa.DetailFieldAlias and dfb.LeadTypeID IS NULL and dfb.ClientID = @ClientID
				inner join detailfields df WITH (NOLOCK) on df.detailfieldid = cdv.detailfieldid and df.detailfieldpageid = @FromDetailFieldPageID
				inner join detailfields df_res WITH (NOLOCK) on df_res.DetailFieldID = dfb.DetailFieldID and df_res.QuestionTypeID = 14
				where m.MatterID = @MatterID 

			End

			IF @LeaveOriginalDetailsIntact = 0
			BEGIN

				Delete cdv
				From CaseDetailValues cdv 
				inner join detailfields df on df.detailfieldid = cdv.detailfieldid and df.detailfieldpageid = @FromDetailFieldPageID
				where cdv.CaseID = @CaseID

			END

			Select @TestID = @MatterID

			Select Top 1 @MatterID = MatterID
			From Matter WITH (NOLOCK) 
			Where CaseID = @CaseID
			And MatterID > @MatterID
			Order By MatterID

		End

	End


	return @TableRowID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MakeTableRowFromPage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_MakeTableRowFromPage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_MakeTableRowFromPage] TO [sp_executeall]
GO
