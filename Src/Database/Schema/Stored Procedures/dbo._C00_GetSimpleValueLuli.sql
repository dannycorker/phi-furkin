SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex
-- Create date: 2015-07-13
-- Description:	Wraps fnGetSimpleDvLuli for calling from custom controls
-- =============================================
Create PROCEDURE [dbo].[_C00_GetSimpleValueLuli]
(
	@DetailFieldID INT,
	@ClientID INT,
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@ClientPersonnelID INT
)
	
AS
BEGIN

	SET NOCOUNT ON
	
	DECLARE @LeadOrMatter INT,
			@ObjectID INT,
			@DetailValue VARCHAR(2000)

	SELECT	@LeadOrMatter = LeadOrmatter
	FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @DetailFieldID
	
	IF @LeadOrMatter = 1
	BEGIN
		SELECT @ObjectID = @LeadID
	END
	ELSE IF @LeadOrMatter = 2
	BEGIN
		SELECT @ObjectID = @MatterID
	END
	ELSE IF @LeadOrMatter = 10
	BEGIN
		SELECT @ObjectID = @CustomerID
	END
	ELSE IF @LeadOrMatter = 11
	BEGIN
		SELECT @ObjectID = @CaseID
	END
	ELSE IF @LeadOrMatter = 12
	BEGIN
		SELECT @ObjectID = @ClientID
	END
	ELSE IF @LeadOrMatter = 13
	BEGIN
		SELECT @ObjectID = @ClientPersonnelID
	END
	
	SELECT @DetailValue = dbo.fnGetSimpleDvLuli(@DetailFieldID, @ObjectID)
	
	SELECT @DetailValue AS DetailValue

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetSimpleValueLuli] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetSimpleValueLuli] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetSimpleValueLuli] TO [sp_executeall]
GO
