SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2021-03-09
-- Description:	Use Nice In Contact?
-- =============================================
CREATE PROCEDURE [dbo].[_C600_UseNiceInContact]
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @UseNice BIT = 0

	IF @ClientID = 607
	BEGIN

		SELECT @UseNice = 1

	END

	SELECT @UseNice AS [UseNice]

END
GO
GRANT EXECUTE ON  [dbo].[_C600_UseNiceInContact] TO [sp_executeall]
GO
