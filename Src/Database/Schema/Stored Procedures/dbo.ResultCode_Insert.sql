SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ResultCode table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ResultCode_Insert]
(

	@ResultCodeID varchar (4)  ,

	@Description varchar (200)  
)
AS


				
				INSERT INTO [dbo].[ResultCode]
					(
					[ResultCodeID]
					,[Description]
					)
				VALUES
					(
					@ResultCodeID
					,@Description
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ResultCode_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResultCode_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResultCode_Insert] TO [sp_executeall]
GO
