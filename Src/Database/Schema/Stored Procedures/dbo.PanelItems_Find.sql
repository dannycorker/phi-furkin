SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the PanelItems table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PanelItems_Find]
(

	@SearchUsingOR bit   = null ,

	@PanelItemID int   = null ,

	@ClientID int   = null ,

	@PanelItemName varchar (50)  = null ,

	@PanelID int   = null ,

	@PanelItemTypeID int   = null ,

	@CreatedBy int   = null ,

	@QueryID int   = null ,

	@IsGlobal bit   = null ,

	@XAxisColumn varchar (50)  = null ,

	@Orientation varchar (50)  = null ,

	@Margin int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [PanelItemID]
	, [ClientID]
	, [PanelItemName]
	, [PanelID]
	, [PanelItemTypeID]
	, [CreatedBy]
	, [QueryID]
	, [IsGlobal]
	, [XAxisColumn]
	, [Orientation]
	, [Margin]
    FROM
	[dbo].[PanelItems] WITH (NOLOCK) 
    WHERE 
	 ([PanelItemID] = @PanelItemID OR @PanelItemID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([PanelItemName] = @PanelItemName OR @PanelItemName IS NULL)
	AND ([PanelID] = @PanelID OR @PanelID IS NULL)
	AND ([PanelItemTypeID] = @PanelItemTypeID OR @PanelItemTypeID IS NULL)
	AND ([CreatedBy] = @CreatedBy OR @CreatedBy IS NULL)
	AND ([QueryID] = @QueryID OR @QueryID IS NULL)
	AND ([IsGlobal] = @IsGlobal OR @IsGlobal IS NULL)
	AND ([XAxisColumn] = @XAxisColumn OR @XAxisColumn IS NULL)
	AND ([Orientation] = @Orientation OR @Orientation IS NULL)
	AND ([Margin] = @Margin OR @Margin IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [PanelItemID]
	, [ClientID]
	, [PanelItemName]
	, [PanelID]
	, [PanelItemTypeID]
	, [CreatedBy]
	, [QueryID]
	, [IsGlobal]
	, [XAxisColumn]
	, [Orientation]
	, [Margin]
    FROM
	[dbo].[PanelItems] WITH (NOLOCK) 
    WHERE 
	 ([PanelItemID] = @PanelItemID AND @PanelItemID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([PanelItemName] = @PanelItemName AND @PanelItemName is not null)
	OR ([PanelID] = @PanelID AND @PanelID is not null)
	OR ([PanelItemTypeID] = @PanelItemTypeID AND @PanelItemTypeID is not null)
	OR ([CreatedBy] = @CreatedBy AND @CreatedBy is not null)
	OR ([QueryID] = @QueryID AND @QueryID is not null)
	OR ([IsGlobal] = @IsGlobal AND @IsGlobal is not null)
	OR ([XAxisColumn] = @XAxisColumn AND @XAxisColumn is not null)
	OR ([Orientation] = @Orientation AND @Orientation is not null)
	OR ([Margin] = @Margin AND @Margin is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItems_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PanelItems_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PanelItems_Find] TO [sp_executeall]
GO
