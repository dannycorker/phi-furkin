SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-06-26
-- Description:	Proc to Link a resourcelist to a lookuplist
-- =============================================
CREATE PROCEDURE [dbo].[_C00_ScriptResourceLink_CreateField] 
(
		@ClientID					INT 
		,@ResourceListFieldID		INT 
		,@ResourceListFieldColumnID	INT
)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE	@PageID				INT 
		,@NewFieldName			VARCHAR(2000)
		,@LookupNewFieldName	VARCHAR(2000)
		,@DetailFieldID			INT 
		,@LeadOrMatter			INT
		,@LeadTypeID			INT 
		,@LookupListID			INT 
		,@NewLookupListID		INT
		,@LogEntry				VARCHAR(2000)
		
	/*CS 2015-08-11 to confirm that this is the proc causing stupid numbers of lookup list items to be created*/
	SELECT @LogEntry	= '@ClientID = ' + ISNULL(CONVERT(VARCHAR,@ClientID),'NULL') 
						+ ', @ResourceListFieldID = ' + ISNULL(CONVERT(VARCHAR,@ResourceListFieldID),'NULL')
						+ ', @@ResourceListFieldColumnID = ' + ISNULL(CONVERT(VARCHAR,@ResourceListFieldColumnID),'NULL')
						
	EXEC _C00_LogIt 'Info', '_C00_LOG', '_C00_ScriptResourceLink_CreateField', @LogEntry, 3814 /*Cathal Sherry*/
		
/*Get Variables for Creating the Detail Field And lookuplist*/		
	SELECT 
	@PageID = DF.DetailFieldPageID
	,@NewFieldName = DF.FieldName
	,@LeadOrMatter = df.LeadOrMatter
	,@LeadTypeID = df.LeadTypeID
	,@LookupNewFieldName = 'ScriptLink ' +  DF.FieldName
	FROM DetailFields DF 
	WHERE df.DetailFieldID = @ResourceListFieldID 
	and DF.ClientID = @ClientID

	/*Get Variables for Creating the LookuplistItems*/
	--SELECT rldvColumn.detailvalue,* 
	--FROM ResourceListDetailValues rldvColumn 
	--WHERE rldvColumn.DetailFieldID = @ResourceListFieldColumnID

	/*Creates the lookuplist, it will be named "ScriptLink" followed by the name of the resourcelist You provided as an input*/
	EXECUTE dbo.LookupList_Insert @NewLookupListID output, @LookupNewFieldName,@LookupNewFieldName,@ClientID,1,22,@ResourceListFieldColumnID  ,NULL,NULL,NULL,NULL,NULL,NULL
	
	/*Insert LookuplistItems into the lookuplist we just made*/
	INSERT INTO LookupListItems  ([LookupListID],[ItemValue],[ClientID],[Enabled],SortOrder,[SourceID])
	SELECT @NewLookupListID,rldvColumn.detailvalue,@ClientID,1,1,@ResourceListFieldColumnID
	FROM ResourceListDetailValues rldvColumn 
	WHERE rldvColumn.DetailFieldID = @ResourceListFieldColumnID --and @NewLookupListID is not null 

	--SELECT @PageID,@NewFieldName,@LeadOrMatter,@LeadTypeID,@LookupListID
	/*Create The DetailField On the same page as the ResourceList, Put the New Field at the bottom of the page*/
	EXECUTE dbo.DetailFields_Insert @DetailFieldID  output,@ClientID  ,@LeadOrMatter  ,@LookupNewFieldName  ,@LookupNewFieldName  ,4 ,0 ,1,@NewLookupListID  ,@LeadTypeID  ,1  ,@PageID ,99  ,1  ,''  ,NULL  ,1  ,NULL  ,NULL  ,NULL  ,NULL  ,NULL  ,NULL  ,NULL,NULL,NULL,'','',1,0,NULL,NULL  ,NULL  ,NULL  ,NULL  ,NULL  ,NULL  ,NULL  ,@ResourceListFieldColumnID /*Source*/ ,NULL  ,NULL  ,NULL  ,NULL  ,NULL,NULL ,NULL	
	
	/*Select the correct Customer,lead,case,matter variable for the SAS*/
	DECLARE @CustCaseLeadOrMatter VARCHAR(90)

	SELECT @CustCaseLeadOrMatter = 
	CASE LeadOrMatter
	WHEN 1 THEN '@LeadID'
	WHEN 2 THEN '@MatterID'  
	WHEN 10 THEN '@CustomerID' END
	FROM dbo.DetailFields df WITH (NOLOCK) WHERE df.DetailFieldID= @DetailFieldID
	
	/*Output this when running the proc, to use in the SAS*/
	select 'IF '+ @CustCaseLeadOrMatter + ' > 0 ' + '
	BEGIN' + ' 
	EXEC dbo._C00_ScriptResourceLink_LinkFields ' + CAST(@ClientID AS Varchar(90)) + ', ' + @CustCaseLeadOrMatter + ', ' + CAST(@DetailFieldID AS Varchar(90)) + ', ' 
	+ CAST(@ResourceListFieldID AS Varchar(90)) + ', ' + CAST(@ResourceListFieldColumnID AS Varchar(90)) + '
	END'
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ScriptResourceLink_CreateField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_ScriptResourceLink_CreateField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_ScriptResourceLink_CreateField] TO [sp_executeall]
GO
