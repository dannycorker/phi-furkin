SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-20
-- Description:	List all ClientPersonnel for a client
-- 2019-06-19 CPS add Aquarium Automation to the section that returns details for the current DB user
-- =============================================
CREATE PROCEDURE [dbo].[cp] 
	@ClientID int = NULL ,
	@ClientPersonnelID int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	IF @ClientID IS NULL
	BEGIN
		SELECT * 
		FROM dbo.ClientPersonnel cp (nolock) 
		WHERE cp.EmailAddress LIKE '%aquarium%'
		AND (cp.ClientPersonnelID = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
		ORDER BY cp.UserName ASC 
		
		SELECT * 
		FROM dbo.ClientPersonnel cp (nolock) 
		WHERE cp.EmailAddress NOT LIKE '%aquarium%'
		AND (cp.ClientPersonnelID = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
		ORDER BY cp.UserName ASC 
	END
	ELSE
	BEGIN
		/*CS 2014-09-22.  Useful if it works.  Show the current DB user's CPID first, if present*/
		IF EXISTS ( SELECT * FROM ClientPersonnel cp WITH ( NOLOCK ) WHERE cp.ClientID = @ClientID AND dbo.fnStripCharacters(cp.UserName,'^a-z') IN (SYSTEM_USER,CURRENT_USER) )
		BEGIN
			SELECT * 
		    FROM ClientPersonnel cp WITH ( NOLOCK ) 
		    WHERE cp.ClientID = @ClientID
			AND dbo.fnStripCharacters(cp.UserName,'^a-z') IN(SYSTEM_USER,CURRENT_USER)
				UNION ALL
			SELECT * 
		    FROM ClientPersonnel cp WITH ( NOLOCK ) 
		    WHERE cp.ClientID = @ClientID
			AND cp.Username = 'Aquarium Automation'
		END

		SELECT * 
		FROM dbo.ClientPersonnel cp (nolock) 
		WHERE cp.ClientID = @ClientID 
		AND cp.EmailAddress LIKE '%aquarium%'
		AND (cp.ClientPersonnelID = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
		ORDER BY cp.UserName ASC 
		
		SELECT * 
		FROM dbo.ClientPersonnel cp (nolock) 
		WHERE cp.ClientID = @ClientID 
		AND cp.EmailAddress NOT LIKE '%aquarium%'
		AND (cp.ClientPersonnelID = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
		ORDER BY cp.UserName ASC 
	END
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[cp] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[cp] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[cp] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[cp] TO [sp_executehelper]
GO
