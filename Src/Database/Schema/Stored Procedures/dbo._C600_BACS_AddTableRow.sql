SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-10-07
-- Description:	Add row to BACS Request/History table
-- Mods
--	2015-07-13 DCM Added BACS file date argument
-- =============================================
CREATE PROCEDURE [dbo].[_C600_BACS_AddTableRow] 
(
	@ObjectID INT,
	@DetailFieldID INT,
	@EffectiveDate DATE = NULL,
	@Amount MONEY,
	@Type INT,
	@BACSCode VARCHAR(100),
	@PayRef VARCHAR(100),
	@AccName VARCHAR(100),
	@AccSortCode VARCHAR(10),
	@AccNumber VARCHAR(10),
	@PremiumTableRowID INT = NULL,
	@BACSCodeType INT = 69982, -- default to Request 
	@BACSFileDate DATE = NULL
)

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE	
			@DetailFieldPageID INT,
			@CaseID INT,
			@ClientID INT,
			@CustomerID INT,
			@FailureReason VARCHAR(100) = '',
			@LeadID INT,
			@MatterID INT,
			@ObjectSubTypeID INT,
			@TableRowID INT
	
	SELECT @DetailFieldPageID=DetailFieldPageID, @ClientID=ClientID, @ObjectSubTypeID=LeadOrMatter 
	FROM DetailFields WITH (NOLOCK) WHERE DetailFieldID=@DetailFieldID
	
	-- get failure reason if BACS code type is not request
	IF @BACSCodeType <> 69982
	BEGIN
	
		-- If an ARUDD row is being added the @BACSCode will have the code description in it, so we do
		-- a reverse lookup for the code
		IF @BACSCodeType = 69985
		BEGIN
		
			DECLARE @ScratchText VARCHAR(100) = ''
			SELECT @ScratchText=cod.DetailValue 
			FROM ResourceListDetailValues typ WITH (NOLOCK) 
			INNER JOIN ResourceListDetailValues cod WITH (NOLOCK) ON typ.ResourceListID=cod.ResourceListID AND cod.DetailFieldID=170198
			INNER JOIN ResourceListDetailValues rea WITH (NOLOCK) ON typ.ResourceListID=rea.ResourceListID
				AND rea.DetailFieldID=170199 AND rea.DetailValue=@BACSCode
			INNER JOIN LookupListItems typli WITH (NOLOCK) ON typ.ValueInt=typli.LookupListItemID
			WHERE typ.DetailFieldID=170200 AND typ.ValueInt=@BACSCodeType

			-- if @scratchtext is blank we can't find the BACS code set to our default unknown and record the text sent
			IF @ScratchText=''
			BEGIN

				SELECT @ScratchText=code.DetailValue, @BACSCode=descr.DetailValue + ' (' + @BACSCode + ')' 
				FROM ResourceListDetailValues code WITH (NOLOCK)
				INNER JOIN ResourceListDetailValues descr WITH (NOLOCK) ON code.ResourceListID=descr.ResourceListID AND descr.DetailFieldID=170199
				WHERE code.DetailFieldID=170198 AND code.ResourceListID=131831		
			
			END
			
			SELECT @FailureReason = '(ARUDD) ' + @BACSCode

			SELECT @BACSCode = CASE WHEN @ScratchText='' THEN 'UN' ELSE @ScratchText END
		
		END
		ELSE
		BEGIN
		
			SELECT @FailureReason= '(' + typli.ItemValue + ') ' + rea.DetailValue 
			FROM ResourceListDetailValues typ WITH (NOLOCK) 
			INNER JOIN ResourceListDetailValues cod WITH (NOLOCK) ON typ.ResourceListID=cod.ResourceListID 
				AND cod.DetailFieldID=170198
				AND cod.DetailValue=@BACSCode
			INNER JOIN ResourceListDetailValues rea WITH (NOLOCK) ON typ.ResourceListID=rea.ResourceListID
				AND rea.DetailFieldID=170199
			INNER JOIN LookupListItems typli WITH (NOLOCK) ON typ.ValueInt=typli.LookupListItemID
			WHERE typ.DetailFieldID=170200 AND typ.ValueInt=@BACSCodeType
		
		END
	
	END
	
	IF @ObjectSubTypeID=1
	BEGIN
					
		INSERT INTO TableRows(ClientID, LeadID, DetailFieldID, DetailFieldPageID)
		SELECT @ClientID, @ObjectID, @DetailFieldID, @DetailFieldPageID
		
		SELECT @TableRowID = SCOPE_IDENTITY()
	
		INSERT INTO TableDetailValues(ClientID, LeadID, TableRowID, DetailFieldID, DetailValue) VALUES
		(@ClientID,@ObjectID,@TableRowID,170081,ISNULL(CONVERT(varchar(10),@EffectiveDate,120),'')), 
		(@ClientID,@ObjectID,@TableRowID,170079,CONVERT(VARCHAR(30),@Amount)), 
		(@ClientID,@ObjectID,@TableRowID,170082,CONVERT(VARCHAR(30),@Type)), 
		(@ClientID,@ObjectID,@TableRowID,170162,@BACSCode), 
		(@ClientID,@ObjectID,@TableRowID,170117,@PayRef), 
		(@ClientID,@ObjectID,@TableRowID,170116,@AccName), 
		(@ClientID,@ObjectID,@TableRowID,170160,@AccSortCode), 
		(@ClientID,@ObjectID,@TableRowID,170161,@AccNumber), 
		(@ClientID,@ObjectID,@TableRowID,170084,@FailureReason), 
		(@ClientID,@ObjectID,@TableRowID,170085,ISNULL(CONVERT(VARCHAR(30),@PremiumTableRowID),'')),
		(@ClientID,@ObjectID,@TableRowID,170080,ISNULL(CONVERT(varchar(10),@BACSFileDate,120),'')) 

		EXEC _C00_CompleteTableRow @TableRowID,NULL,1,1

	END
	
	IF @ObjectSubTypeID=2
	BEGIN
					
		INSERT INTO TableRows(ClientID, MatterID, DetailFieldID, DetailFieldPageID)
		SELECT @ClientID, @ObjectID, @DetailFieldID, @DetailFieldPageID
		
		SELECT @TableRowID = SCOPE_IDENTITY()
	
		INSERT INTO TableDetailValues(ClientID, MatterID, TableRowID, DetailFieldID, DetailValue) VALUES
		(@ClientID,@ObjectID,@TableRowID,170081,ISNULL(CONVERT(varchar(10),@EffectiveDate,120),'')), 
		(@ClientID,@ObjectID,@TableRowID,170079,CONVERT(VARCHAR(30),@Amount)), 
		(@ClientID,@ObjectID,@TableRowID,170082,CONVERT(VARCHAR(30),@Type)), 
		(@ClientID,@ObjectID,@TableRowID,170162,@BACSCode), 
		(@ClientID,@ObjectID,@TableRowID,170117,@PayRef), 
		(@ClientID,@ObjectID,@TableRowID,170116,@AccName), 
		(@ClientID,@ObjectID,@TableRowID,170160,@AccSortCode), 
		(@ClientID,@ObjectID,@TableRowID,170161,@AccNumber), 
		(@ClientID,@ObjectID,@TableRowID,170084,@FailureReason), 
		(@ClientID,@ObjectID,@TableRowID,170085,ISNULL(CONVERT(VARCHAR(30),@PremiumTableRowID),'')),
		(@ClientID,@ObjectID,@TableRowID,170080,ISNULL(CONVERT(varchar(10),@BACSFileDate,120),'')) 

		EXEC _C00_CompleteTableRow @TableRowID,@ObjectID,1,1
	
	END
	
	IF @ObjectSubTypeID=10
	BEGIN
					
		INSERT INTO TableRows(ClientID, CustomerID, DetailFieldID, DetailFieldPageID)
		SELECT @ClientID, @ObjectID, @DetailFieldID, @DetailFieldPageID
		
		SELECT @TableRowID = SCOPE_IDENTITY()
	
		INSERT INTO TableDetailValues(ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue) VALUES
		(@ClientID,@ObjectID,@TableRowID,170081,ISNULL(CONVERT(varchar(10),@EffectiveDate,120),'')), 
		(@ClientID,@ObjectID,@TableRowID,170079,CONVERT(VARCHAR(30),@Amount)), 
		(@ClientID,@ObjectID,@TableRowID,170082,CONVERT(VARCHAR(30),@Type)), 
		(@ClientID,@ObjectID,@TableRowID,170162,@BACSCode), 
		(@ClientID,@ObjectID,@TableRowID,170117,@PayRef), 
		(@ClientID,@ObjectID,@TableRowID,170116,@AccName), 
		(@ClientID,@ObjectID,@TableRowID,170160,@AccSortCode), 
		(@ClientID,@ObjectID,@TableRowID,170161,@AccNumber), 
		(@ClientID,@ObjectID,@TableRowID,170084,@FailureReason), 
		(@ClientID,@ObjectID,@TableRowID,170085,ISNULL(CONVERT(VARCHAR(30),@PremiumTableRowID),'')),
		(@ClientID,@ObjectID,@TableRowID,170080,ISNULL(CONVERT(varchar(10),@BACSFileDate,120),'')) 

		EXEC _C00_CompleteTableRow @TableRowID,@ObjectID,1,1
	
	END
	
	RETURN @TableRowID
				
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BACS_AddTableRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_BACS_AddTableRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_BACS_AddTableRow] TO [sp_executeall]
GO
