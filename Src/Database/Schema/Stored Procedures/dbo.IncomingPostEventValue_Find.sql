SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the IncomingPostEventValue table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncomingPostEventValue_Find]
(

	@SearchUsingOR bit   = null ,

	@IncomingPostEventValueID int   = null ,

	@IncomingPostEventID int   = null ,

	@PostKey varchar (256)  = null ,

	@PostValue varchar (512)  = null ,

	@ClientID int   = null ,

	@CustomerID int   = null ,

	@LeadID int   = null ,

	@CaseID int   = null ,

	@MatterID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [IncomingPostEventValueID]
	, [IncomingPostEventID]
	, [PostKey]
	, [PostValue]
	, [ClientID]
	, [CustomerID]
	, [LeadID]
	, [CaseID]
	, [MatterID]
    FROM
	[dbo].[IncomingPostEventValue] WITH (NOLOCK) 
    WHERE 
	 ([IncomingPostEventValueID] = @IncomingPostEventValueID OR @IncomingPostEventValueID IS NULL)
	AND ([IncomingPostEventID] = @IncomingPostEventID OR @IncomingPostEventID IS NULL)
	AND ([PostKey] = @PostKey OR @PostKey IS NULL)
	AND ([PostValue] = @PostValue OR @PostValue IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([CustomerID] = @CustomerID OR @CustomerID IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([MatterID] = @MatterID OR @MatterID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [IncomingPostEventValueID]
	, [IncomingPostEventID]
	, [PostKey]
	, [PostValue]
	, [ClientID]
	, [CustomerID]
	, [LeadID]
	, [CaseID]
	, [MatterID]
    FROM
	[dbo].[IncomingPostEventValue] WITH (NOLOCK) 
    WHERE 
	 ([IncomingPostEventValueID] = @IncomingPostEventValueID AND @IncomingPostEventValueID is not null)
	OR ([IncomingPostEventID] = @IncomingPostEventID AND @IncomingPostEventID is not null)
	OR ([PostKey] = @PostKey AND @PostKey is not null)
	OR ([PostValue] = @PostValue AND @PostValue is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([CustomerID] = @CustomerID AND @CustomerID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([MatterID] = @MatterID AND @MatterID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEventValue_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncomingPostEventValue_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncomingPostEventValue_Find] TO [sp_executeall]
GO
