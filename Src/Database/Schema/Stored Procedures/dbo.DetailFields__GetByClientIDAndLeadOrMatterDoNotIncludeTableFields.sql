SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Paul Richardson
-- Create date: 08 December 2010
-- Description:	Gets the fields for the mandatory/helper fields
-- does not include the table fields since the mandatory field editor does not support tables
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetByClientIDAndLeadOrMatterDoNotIncludeTableFields] 

	@ClientID int, 
	@LeadTypeID int, 
	@LeadOrMatter int 

AS
BEGIN
				
				SELECT
					*
				FROM
					dbo.fnDetailFieldsShared(@ClientID)
				WHERE
					[ClientID] = @ClientID AND
					[LeadTypeID] = @LeadTypeID AND
					[LeadOrMatter] = @LeadOrMatter AND
					[QuestionTypeID] <> 16 AND
					[QuestionTypeID] <> 19 AND
					[Enabled] = 1 
				ORDER BY 
					[FieldName]

				Select @@ROWCOUNT

END







GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetByClientIDAndLeadOrMatterDoNotIncludeTableFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetByClientIDAndLeadOrMatterDoNotIncludeTableFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetByClientIDAndLeadOrMatterDoNotIncludeTableFields] TO [sp_executeall]
GO
