SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlQueryGrouping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryGrouping_Update]
(

	@SqlQueryGroupingID int   ,

	@ClientID int   ,

	@SqlQueryID int   ,

	@GroupByClause varchar (2000)  ,

	@HavingClause varchar (500)  ,

	@HavingColumnID int   ,

	@HavingCriteria1 varchar (250)  ,

	@HavingCriteria2 varchar (250)  ,

	@TempHavingColumnID int   ,

	@TempHavingID int   ,

	@TempGroupingID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlQueryGrouping]
				SET
					[ClientID] = @ClientID
					,[SqlQueryID] = @SqlQueryID
					,[GroupByClause] = @GroupByClause
					,[HavingClause] = @HavingClause
					,[HavingColumnID] = @HavingColumnID
					,[HavingCriteria1] = @HavingCriteria1
					,[HavingCriteria2] = @HavingCriteria2
					,[TempHavingColumnID] = @TempHavingColumnID
					,[TempHavingID] = @TempHavingID
					,[TempGroupingID] = @TempGroupingID
				WHERE
[SqlQueryGroupingID] = @SqlQueryGroupingID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryGrouping_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryGrouping_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryGrouping_Update] TO [sp_executeall]
GO
