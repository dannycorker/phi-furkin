SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DiaryReminder table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DiaryReminder_GetByDiaryReminderID]
(

	@DiaryReminderID int   
)
AS


				SELECT
					[DiaryReminderID],
					[ClientID],
					[DiaryAppointmentID],
					[ClientPersonnelID],
					[ReminderTimeshiftID],
					[ReminderDate]
				FROM
					[dbo].[DiaryReminder] WITH (NOLOCK) 
				WHERE
										[DiaryReminderID] = @DiaryReminderID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder_GetByDiaryReminderID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryReminder_GetByDiaryReminderID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryReminder_GetByDiaryReminderID] TO [sp_executeall]
GO
