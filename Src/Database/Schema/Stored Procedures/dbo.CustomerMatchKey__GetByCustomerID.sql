SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 20-12-2012
-- Description:	Gets a record in CustomerMatchKey by the customerID
-- =============================================
CREATE PROCEDURE [dbo].[CustomerMatchKey__GetByCustomerID]
	
	@ClientID INT,
	@CustomerID INT

AS
BEGIN

	SELECT * FROM CustomerMatchKey WITH (NOLOCK) 
	WHERE CustomerID = @CustomerID AND ClientID = @ClientID
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerMatchKey__GetByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerMatchKey__GetByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerMatchKey__GetByCustomerID] TO [sp_executeall]
GO
