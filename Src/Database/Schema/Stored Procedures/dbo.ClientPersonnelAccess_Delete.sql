SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientPersonnelAccess table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelAccess_Delete]
(

	@ClientPersonnelAccessID int   
)
AS


				DELETE FROM [dbo].[ClientPersonnelAccess] WITH (ROWLOCK) 
				WHERE
					[ClientPersonnelAccessID] = @ClientPersonnelAccessID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelAccess_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAccess_Delete] TO [sp_executeall]
GO
