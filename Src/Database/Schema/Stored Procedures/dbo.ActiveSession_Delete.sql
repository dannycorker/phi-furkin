SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ActiveSession table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ActiveSession_Delete]
(

	@EmailAddress varchar (255)  ,

	@ThirdPartySystemId int   
)
AS


				DELETE FROM [dbo].[ActiveSession] WITH (ROWLOCK) 
				WHERE
					[EmailAddress] = @EmailAddress
					AND [ThirdPartySystemId] = @ThirdPartySystemId
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ActiveSession_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_Delete] TO [sp_executeall]
GO
