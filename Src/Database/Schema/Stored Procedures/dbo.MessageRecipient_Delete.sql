SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the MessageRecipient table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MessageRecipient_Delete]
(

	@MessageRecipientID int   
)
AS


				DELETE FROM [dbo].[MessageRecipient] WITH (ROWLOCK) 
				WHERE
					[MessageRecipientID] = @MessageRecipientID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[MessageRecipient_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MessageRecipient_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MessageRecipient_Delete] TO [sp_executeall]
GO
