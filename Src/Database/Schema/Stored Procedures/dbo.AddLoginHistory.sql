SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =======================================================
-- Author:		Jan Wilson
-- Create date: 2014-09-11
-- Description:	Creates an entry in the LoginHistory table
-- =============================================
CREATE PROCEDURE [dbo].[AddLoginHistory] 
	@UserName VARCHAR(255), 
	@AppLogin BIT = 1, 
	@AppInfo VARCHAR(50) = NULL,
	@ThirdPartySystemID INT = NULL,
	@UserIPDetails VARCHAR(50) = NULL,
	@Notes VARCHAR(2000) = NULL,
	@IsSuccess BIT = 1,
	@AttemptedLogins INT = 0,
	@AccountDisabled BIT = 0
AS
BEGIN
	DECLARE @ClientID INT
	DECLARE @ClientPersonnelID INT

	SELECT
		@ClientID = ClientID, 
		@ClientPersonnelID = ClientPersonnelID
	FROM
		ClientPersonnel WITH (NOLOCK) 
	WHERE
		ClientPersonnel.EmailAddress = @UserName AND ThirdPartySystemId = 0
	
	/*
		Log the results
	*/
	INSERT INTO dbo.LoginHistory (
		UserName, 
		LoginDateTime, 
		IsSuccess, 
		ClientID, 
		ClientPersonnelID, 
		AppLogin, 
		AttemptedLogins, 
		AccountDisabled, 
		Notes,
		ThirdPartySystemID,
		HostName,
		UserIPDetails
	) 
	VALUES 
	( 
		@UserName, 
		dbo.fn_GetDate_Local(), 
		@IsSuccess, 
		@ClientID, 
		@ClientPersonnelID, 
		@AppLogin, 
		@AttemptedLogins, 
		@AccountDisabled, 
		@Notes,
		@ThirdPartySystemID,			
		HOST_NAME(),
		@UserIPDetails
	)
END
GO
GRANT VIEW DEFINITION ON  [dbo].[AddLoginHistory] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddLoginHistory] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddLoginHistory] TO [sp_executeall]
GO
