SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the HyperlinkSpecialisedDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkSpecialisedDetailFieldTarget_Get_List]

AS


				
				SELECT
					[HyperlinkSpecialisedDetailFieldTargetID],
					[ClientID],
					[HyperlinkDetailFieldID],
					[Target],
					[DetailFieldID],
					[ColumnField],
					[TemplateTypeID],
					[DetailFieldAlias],
					[ColumnFieldAlias],
					[RowField],
					[Notes]
				FROM
					[dbo].[HyperlinkSpecialisedDetailFieldTarget] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkSpecialisedDetailFieldTarget_Get_List] TO [sp_executeall]
GO
