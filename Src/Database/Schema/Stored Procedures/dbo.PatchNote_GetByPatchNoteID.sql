SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PatchNote table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PatchNote_GetByPatchNoteID]
(

	@PatchNoteID int   
)
AS


				SELECT
					[PatchNoteID],
					[PatchID],
					[Location],
					[PatchNoteText],
					[AdminOnly]
				FROM
					[dbo].[PatchNote] WITH (NOLOCK) 
				WHERE
										[PatchNoteID] = @PatchNoteID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PatchNote_GetByPatchNoteID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PatchNote_GetByPatchNoteID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PatchNote_GetByPatchNoteID] TO [sp_executeall]
GO
