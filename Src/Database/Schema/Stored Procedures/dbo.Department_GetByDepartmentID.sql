SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Department table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Department_GetByDepartmentID]
(

	@DepartmentID int   
)
AS


				SELECT
					[DepartmentID],
					[ClientID],
					[CustomerID],
					[DepartmentName],
					[DepartmentDescription]
				FROM
					[dbo].[Department] WITH (NOLOCK) 
				WHERE
										[DepartmentID] = @DepartmentID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Department_GetByDepartmentID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Department_GetByDepartmentID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Department_GetByDepartmentID] TO [sp_executeall]
GO
