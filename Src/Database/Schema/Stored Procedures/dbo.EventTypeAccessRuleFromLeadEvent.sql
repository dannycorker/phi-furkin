SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2011-07-21
-- Description:	Fire and EventTypeAccessRules for the EventType
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeAccessRuleFromLeadEvent] 
	@LeadEventID INT, 
	@EventTypeID INT = NULL,
	@CaseID INT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE 
	@LastAccessRuleID INT, 
	@NextAccessRuleID INT = 0,
	@AccessRuleID INT,
	@DetailFieldID INT,
	@DataLoaderObjectTypeID INT, /* The access level being granted (1=Lead, 3=Case etc) */
	@ClientPersonnelAdminGroupID INT,
	@ClientPersonnelID INT,
	@PortalUserID INT,
	@ValueToCompare INT,
	@ValueInt INT,
	@AccessLevel TINYINT
	
    /*
		If all IDs are known then we have slightly less work to do,
		otherwise look them up from the LeadEvent record.
	*/
	IF @EventTypeID IS NULL OR @CaseID IS NULL
	BEGIN

		SELECT @EventTypeID = le.EventTypeID,
		@CaseID = le.CaseID 
		FROM dbo.LeadEvent le WITH (NOLOCK) 
		WHERE le.LeadEventID = @LeadEventID 

	END

	IF @EventTypeID IS NOT NULL
	BEGIN
		
		/* Loop round all the rules for this EventType, applying each in turn */
		WHILE @NextAccessRuleID IS NOT NULL
		BEGIN
			
			SELECT @LastAccessRuleID = @NextAccessRuleID,
			@NextAccessRuleID = NULL
			
			SELECT TOP 1 @NextAccessRuleID = etar.AccessRuleID 
			FROM dbo.EventTypeAccessRule etar WITH (NOLOCK) 
			WHERE etar.EventTypeID = @EventTypeID 
			AND etar.AccessRuleID > @LastAccessRuleID 
			ORDER BY etar.AccessRuleID
			
			IF @NextAccessRuleID > 0
			BEGIN
				SELECT 
				@AccessRuleID = ar.AccessRuleID, 
				@DetailFieldID = ar.DetailFieldID, 
				@DataLoaderObjectTypeID = ar.DataLoaderObjectTypeID, 
				@ClientPersonnelAdminGroupID = ar.ClientPersonnelAdminGroupID, 
				@ClientPersonnelID = ar.ClientPersonnelID, 
				@PortalUserID = ar.PortalUserID,
				@ValueToCompare = ar.ValueToCompare, 
				@AccessLevel = ISNULL(ar.AccessLevel, 4)
				FROM dbo.AccessRule ar WITH (NOLOCK) 
				WHERE ar.AccessRuleID = @NextAccessRuleID 
				AND ar.AccessRuleEnabled = 1 
				
				/* Make sure that an enabled record was found */
				IF @AccessRuleID = @NextAccessRuleID
				BEGIN
					/* If a DetailFieldID is specified then get the appropriate value */
					IF @DetailFieldID > 0
					BEGIN
						SELECT @ValueInt = dbo.fnGetDvAsInt(@DetailFieldID, @CaseID)
					END
					
					/* 
						If either of DetailFieldID or ValueToCompare is NULL, then apply this rule regardless.
						If both are specified, then the value found has to match the ValueToCompare 
						before the rule gets applied.
					*/
					IF (@DetailFieldID IS NULL) OR (@ValueToCompare IS NULL) OR (@ValueToCompare = @ValueInt)
					BEGIN
						EXEC dbo.ClientPersonnelAccess__Grant @AccessRuleID, @DataLoaderObjectTypeID, @ClientPersonnelAdminGroupID, @ClientPersonnelID, @PortalUserID, @LeadEventID, @AccessLevel
					END
				END
				
			END
			
		END
		
	END

END



GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAccessRuleFromLeadEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAccessRuleFromLeadEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAccessRuleFromLeadEvent] TO [sp_executeall]
GO
