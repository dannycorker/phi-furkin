SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- ALTER date:	2017-05-24
-- Description:	Populate matter detail values for document presentation at key points in the process
-- Update: SA - added Next Collection Date for MTA - added to Document Fields tab
-- 2017-08-22 CPS Update excess and postcode group
-- 2017-09-27 CPS include Reinstatement
-- 2018-08-29 AHOD Updated Number of Remaining Payments to include New and Scheduled Payments only
-- 2020-02-05 CPS for JIRA AAG-91	| Replace TableRow and TableDetailValues references with PurchasedProductPaymentScheduleDetail
-- 2020-03-18 NG  for AAG-512    | Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- 2020-09-29 ALM for SDPRU-65 Included Municipal Tax
-- 2020-09-29 ALM for SDPRU-66 Changed @314323_Value and @314325_Value to decimal
-- 2021-03-10 AAD for FURKIN-337 Ensure Payment Day for Docs (177388) is updated (currently only updates if the PPPS has free rows)
-- 2021-04-09 CPS for JIRA FURKIN-458 | Populate declaration fields
-- =============================================
CREATE PROCEDURE [dbo].[_C600_SetDocumentFields]
(
	 @MatterID			INT
	,@RunAsUserID		INT
	,@ProcessSection	VARCHAR(2000) = 'NewBusiness'
)
AS
BEGIN

	SET NOCOUNT ON

	PRINT 'Saving Document Fields <br>' + CHAR(13)+CHAR(10)

	DECLARE  @DetailValueData			dbo.tvpDetailValueData
			,@ClientID					INT
			,@BlackHole					dbo.tvpIntInt
			,@RowCount					INT
			,@ValueMoney				MONEY
			,@ValueDate					DATE
			,@ValueInt					INT
			,@DetailValue				VARCHAR(2000)
			,@PostCode					VARCHAR(2000)
			,@CustomerID				INT
			,@PurchasedProductID		INT
			,@NextCollectionDate		DATE
			,@LeadID					INT
			,@CaseID					INT
			,@IPT						DECIMAL (18,2)
			,@NetAdjustment				DECIMAL (18,2)
			,@IPTAdjustment				DECIMAL (18,2)
			,@GrossAdjustment			DECIMAL (18,2)
			,@AbsolouteAdjustment		DECIMAL (18,2) 
			,@Net						DECIMAL (18,2)
			,@314322_Value				Money
			,@314320_Value				MONEY
			,@314319_Value				MONEY
			,@314321_Value				MONEY
			,@314323_Value				DECIMAL (18,2)
			,@314325_Value				DECIMAL (18,2)
			,@314324_Value				VARCHAR(2000)
			,@314329_Value				MONEY
			,@PremiumNet				DECIMAL (18,2)
			,@PremiumTax				DECIMAL (18,2)
			,@PremiumGross				DECIMAL (18,2)
			,@RemainingPremiumNet		DECIMAL (18,2)
			,@RemainingPremiumTax		DECIMAL (18,2)
			,@RemainingPremiumGross		DECIMAL (18,2)
			
	SELECT  @ClientID = m.ClientID, 
			@CustomerID = m.CustomerID, 
			@PurchasedProductID = dbo.fn_C600_GetPurchasedProductForMatter(@MatterID,dbo.fn_GetDate_Local()), 
			@LeadID = m.LeadID, 
			@CaseID = m.CaseID
	FROM Matter m WITH (NOLOCK)
	WHERE m.MatterID = @MatterID

	/*GPR 2020-09-19 Prudent*/
	SELECT  @314322_Value = fn.TotalCost, 
			@314320_Value = fn.PolicyNet, 
			@314319_Value = fn.TransactionFee, 
			@314321_Value = fn.StateTax, 
			@314323_Value = fn.SurchargeRate, 
			@314325_Value = fn.TotalDiscountRate, 
			@314329_Value = fn.MunicipalTax
	FROM [dbo].[fn_C600_ValuesFromWrittenPremiumForPolicy](@MatterID) fn

	EXEC _C00_SimpleValueIntoField 314322, @314322_Value, @MatterID
	EXEC _C00_SimpleValueIntoField 314320, @314320_Value, @MatterID
	EXEC _C00_SimpleValueIntoField 314319, @314319_Value, @MatterID
	EXEC _C00_SimpleValueIntoField 314321, @314321_Value, @MatterID
	EXEC _C00_SimpleValueIntoField 314323, @314323_Value, @MatterID
	EXEC _C00_SimpleValueIntoField 314325, @314325_Value, @MatterID
	EXEC _C00_SimpleValueIntoField 314329, @314329_Value, @MatterID
										  
	/*DFID: 314324*/
	--SELECT * FROM [fn_C600_DiscountValuesFromWrittenPremiumForPolicy](@MatterID) fn
	SELECT @314324_Value = REPLACE(ISNULL(dbo.fn_C605_GetDiscountsCSV(@MatterID),''),',',CHAR(13)+CHAR(10))
	EXEC _C00_SimpleValueIntoField 314324, @314324_Value, @MatterID

	PRINT 'Starting Prep Doc Fields <br>'
	EXEC _C600_PrepDocumentFields @MatterID, @RunAsUserID
	PRINT 'Prep Doc Fields Complete <br>'

	-- 2021-04-09 CPS for JIRA FURKIN-458 | Populate declaration fields
	--									  | No current instruction on which process should update these or where the docs will be used
	--									  | So populate them every time, for now.  We can revise during testing or peer review if required
	;WITH CollectionsByPurchasedProduct AS
	(
		SELECT pp.ValidFrom, pp.ValidTo, ppps.PaymentNet, ppps.PaymentVAT, ppps.PaymentGross, ROW_NUMBER() OVER ( PARTITION BY pp.PurchasedProductID ORDER BY ppps.ActualCollectionDate DESC ) [Row]
		FROM PurchasedProduct pp WITH (NOLOCK) 
		INNER JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) on ppps.PurchasedProductID = pp.PurchasedProductID AND ppps.PurchasedProductPaymentScheduleTypeID = 1 /*Scheduled*/
		WHERE pp.ObjectID = @MatterID
	)
	INSERT @DetailValueData ( ClientID, DetailFieldID, DetailFieldSubType, DetailValueID, ObjectID, DetailValue )
	SELECT df.ClientID, df.DetailFieldID, df.LeadOrMatter, ISNULL(mdv.MatterDetailValueID,-1), @MatterID
		, ISNULL(
		  CASE df.DetailFieldID 
			WHEN 314320 THEN CAST(ppThis.PaymentNet   as VARCHAR(2000)) /*Policy Cost Net*/
			WHEN 314329 THEN CAST(ppThis.PaymentVat   as VARCHAR(2000)) /*Local Tax Total*/
			WHEN 314322 THEN CAST(ppThis.PaymentGross as VARCHAR(2000)) /*Total Policy Cost*/
			WHEN 314340 THEN CAST(ppNext.PaymentNet   as VARCHAR(2000)) /*Renewal Policy Cost Net*/
			WHEN 314342 THEN CAST(ppNext.PaymentVat	  as VARCHAR(2000)) /*Renewal Local Tax Total*/
			WHEN 314343 THEN CAST(ppNext.PaymentGross as VARCHAR(2000)) /*Renewal Total Policy Cost*/
		  ELSE '' 
		  END,'')
	FROM DetailFields df WITH (NOLOCK) 
	LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = @MatterID AND mdv.DetailFieldID = df.DetailFieldID
	LEFT JOIN CollectionsByPurchasedProduct ppThis on ppThis.Row = 1 AND dbo.fnGetSimpleDv(170036,@MatterID) /*Policy Start Date*/ between ppThis.ValidFrom AND ppThis.ValidTo
	LEFT JOIN CollectionsByPurchasedProduct ppNext on ppNext.Row = 1 AND dbo.fnGetSimpleDv(170036,@MatterID) /*Policy Start Date*/ < ppNext.ValidFrom
	where df.DetailFieldID IN (  314320 /*Policy Cost Net*/
								,314329	/*Local Tax Total*/
								,314322	/*Total Policy Cost*/
								,314340	/*Renewal Policy Cost Net*/
								,314342	/*Renewal Local Tax Total*/
								,314343	/*Renewal Total Policy Cost*/
							)

	IF @ProcessSection IN ( 'NewBusiness', 'MTA' )
	BEGIN


		INSERT @DetailValueData ( ClientID, DetailFieldID, DetailFieldSubType, DetailValueID, ObjectID, DetailValue )
		SELECT df.ClientID, df.DetailFieldID, df.LeadOrMatter, ISNULL(mdv.MatterDetailValueID,-1), @MatterID, ISNULL(CASE WHEN p.Amount = '£0.00' or p.Amount is NULL THEN 'Cover not Included' ELSE p.Amount END,'')
		FROM DetailFields df WITH (NOLOCK) 
		INNER JOIN ResourceListDetailValues rdv WITH (NOLOCK) on df.DetailFieldID = rdv.ValueInt AND rdv.DetailFieldID = 177467 /*Policy Admin Document DetailFieldID*/
		LEFT JOIN dbo.fn_C600_PointOfSale_BenefitsBreakdown(@MatterID) p on p.ResourceListID = rdv.ResourceListID
		LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = @MatterID AND mdv.DetailFieldID = df.DetailFieldID
		WHERE df.LeadTypeID = 1492 /*Policy Admin*/
		AND df.LeadOrMatter = 2 /*Matter*/
		AND @ProcessSection IN ( 'NewBusiness' )
		

		;WITH CollectionDates AS
		(
			SELECT TOP 2 pps.ActualCollectionDate, SUM(pps.PaymentGross) [PaymentGross], MAX(pps.PaymentGross) [BeforeAdminFee], ROW_NUMBER() OVER (ORDER BY pps.ActualCollectionDate) [RowNum]
			FROM PurchasedProduct pp WITH (NOLOCK) 
			INNER JOIN PurchasedProductPaymentSchedule pps WITH (NOLOCK) on pps.PurchasedProductID = pp.PurchasedProductID
			WHERE pp.ObjectID = @MatterID AND pp.ObjectTypeID = 2
			AND pps.PurchasedProductPaymentScheduleTypeID <> 4 /*Discount*/
			GROUP BY pps.ActualCollectionDate
		)
		INSERT @DetailValueData ( ClientID, DetailFieldID, DetailFieldSubType, DetailValueID, ObjectID, DetailValue )
		SELECT @ClientID, df.DetailFieldID, df.LeadOrMatter, ISNULL(mdv.MatterDetailValueID,-1), @MatterID, 
														ISNULL(
															CASE df.DetailFieldID
																WHEN 177477 THEN CONVERT(VARCHAR,cd.ActualCollectionDate,121)/*First Collection Date*/
																WHEN 177479 THEN CONVERT(VARCHAR,cd.PaymentGross)			 /*First Collection Amount*/
																WHEN 177481 THEN CONVERT(VARCHAR,cd.BeforeAdminFee)			 /*First Collection Amount before Fee*/
																WHEN 177478 THEN CONVERT(VARCHAR,cd.ActualCollectionDate,121)/*Second Collection Date*/
																WHEN 177480 THEN CONVERT(VARCHAR,cd.PaymentGross)			 /*Second Collection Amount*/
																WHEN 177482 THEN CONVERT(VARCHAR,cd.BeforeAdminFee)			 /*Second Collection Amount before Fee*/
																ELSE '' 
															END 
														,'')
		FROM CollectionDates cd 
		INNER JOIN DetailFields df WITH (NOLOCK) on	(	cd.RowNum = 1 AND df.DetailFieldID IN (	 177477 /*First Collection Date*/
																									,177479 /*First Collection Amount*/
																									,177481 /*First Collection Amount before Fee*/ )
														)
														OR
														(	cd.RowNum = 2 AND df.DetailFieldID IN (	 177478 /*Second Collection Date*/
																									,177480 /*Second Collection Amount*/
																									,177482 /*Second Collection Amount before Fee*/ )
														)
		LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = @MatterID AND mdv.DetailFieldID = df.DetailFieldID
		WHERE cd.RowNum IN (1,2)
		
		SELECT @ValueInt = COUNT(*), @DetailValue = MAX(pp.PreferredPaymentDay)
		FROM PurchasedProduct pp WITH (NOLOCK) 
		INNER JOIN PurchasedProductPaymentSchedule pps WITH (NOLOCK) on pps.PurchasedProductID = pp.PurchasedProductID
		WHERE pp.ObjectID = @MatterID AND pp.ObjectTypeID = 2
		AND pps.PaymentStatusID = 7 /*Free*/
		GROUP BY pps.ActualCollectionDate

		INSERT @DetailValueData ( ClientID, DetailFieldID, DetailFieldSubType, DetailValueID, ObjectID, DetailValue )
		SELECT @ClientID, df.DetailFieldID, df.LeadOrMatter, ISNULL(mdv.MatterDetailValueID,-1), @MatterID, ISNULL(CONVERT(VARCHAR,@ValueInt),'0')
		FROM DetailFields df WITH (NOLOCK) 
		LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = @MatterID AND mdv.DetailFieldID = df.DetailFieldID
		WHERE df.DetailFieldID = 177483 /*Number of Discounted Months*/
		/* ALM 2021-03-10 FURKIN-378 */
		--UNION
		--SELECT @ClientID, df.DetailFieldID, df.LeadOrMatter, ISNULL(mdv.MatterDetailValueID,-1), @MatterID, ISNULL(CONVERT(VARCHAR,dbo.fnGetSimpleDvAsMoney(177470,@MatterID)),'0.00')
		--FROM DetailFields df WITH (NOLOCK) 
		--LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = @MatterID AND mdv.DetailFieldID = df.DetailFieldID
		--WHERE df.DetailFieldID = 177484 /*Initial Admin Fee Charged*/
		/* Now done with SVIF below, not based on number of free rows. Removed 2021-03-10 AAD for FURKIN-337 */
		--	UNION
		--SELECT @ClientID, df.DetailFieldID, df.LeadOrMatter, ISNULL(mdv.MatterDetailValueID,-1), @MatterID, ISNULL(CONVERT(VARCHAR,@DetailValue),'1')
		--FROM DetailFields df WITH (NOLOCK) 
		--LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = @MatterID AND mdv.DetailFieldID = df.DetailFieldID
		--WHERE df.DetailFieldID = 177388 /*Payment Day*/
		
		/*CW Add payment day with ordinal indicator*/
		SELECT @DetailValue = MAX(pp.PreferredPaymentDay)
		FROM PurchasedProduct pp WITH (NOLOCK) 
		INNER JOIN PurchasedProductPaymentSchedule pps WITH (NOLOCK) on pps.PurchasedProductID = pp.PurchasedProductID
		WHERE pp.ObjectID = @MatterID AND pp.ObjectTypeID = 2
		AND pps.PaymentStatusID = 1 
		GROUP BY pps.ActualCollectionDate
		
		EXEC _C00_SimpleValueIntoField 177388, @DetailValue, @MatterID /* First store it without ordinal indicator. 2021-03-10 AAD for FURKIN-337 */

		Select @DetailValue = dbo._C600_OrdinalIndicator(@DetailValue)
		Exec dbo._C00_SimpleValueIntoField 179962,@DetailValue,@MatterID
	
		/*Record the Postcode Group and Excess - CPS 2017-08-22*/
		EXEC _C600_PA_SetPostcodeGroupAndExcessForMatter @MatterID
		
		INSERT @DetailValueData ( ClientID, DetailFieldID, DetailFieldSubType, DetailValueID, ObjectID, DetailValue )
		SELECT dvd.ClientID, 177480, 2, ISNULL(mdv.MatterDetailValueID,-1), @MatterID, dvd.DetailValue
		FROM @DetailValueData dvd 
		LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.DetailFieldID = 177480 /*Second Collection Amount*/ AND mdv.MatterID = @MatterID
		WHERE dvd.DetailFieldID = 177479 /*First Collection Amount*/
		AND dbo.fnGetSimpleDvAsInt(170176,@MatterID) = 69944 /*Annual*/

		--SELECT 'LeadID',@LeadID

		/*SA - #44973 - Overrage excess update for document rule*/
		INSERT @DetailValueData ( ClientID, DetailFieldID, DetailFieldSubType, DetailValueID, ObjectID, DetailValue )
		SELECT df.ClientID,179680,df.LeadOrMatter,ISNULL(ldv1.LeadDetailValueID,-1),@LeadID,(CASE WHEN DATEDIFF(m,ldv.ValueDate,mdv.ValueDate) > dbo.fnGetRLDvAsInt (144272,177292,@CaseID) THEN 5144 ELSE 5145 END)
		FROM DetailFields df WITH (NOLOCK)
		INNER  JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.DetailFieldID = 170037 /*Policy Start Date*/					
		INNER JOIN  LeadDetailValues ldv WITH (NOLOCK) ON ldv.DetailFieldID        = 144274 /*Pet Date of Birth*/					
		LEFT JOIN    LeadDetailValues ldv1 WITH (NOLOCK) ON ldv1.DetailFieldID    = 179680 /*Pet overrage at renewal*/		AND ldv1.LeadID = @LeadID 
		WHERE	mdv.MatterID = @MatterID
		AND			ldv.LeadID = @LeadID
		AND df.DetailFieldID = 179680

	END
	
	IF @ProcessSection IN ( 'Reinstatement' )
	BEGIN
		/*Sum all PPPS records for the next payment date for this policy only*/
		SELECT @ValueMoney = SUM(pps.PaymentGross),@NextCollectionDate = MAX(pps.ActualCollectionDate) 
		FROM PurchasedProduct pp WITH (NOLOCK) 
		INNER JOIN PurchasedProductPaymentSchedule pps WITH (NOLOCK) on pps.PurchasedProductID = pp.PurchasedProductID
		WHERE pp.PurchasedProductID = @PurchasedProductID
		AND pps.PaymentStatusID = 1
		  and not exists ( SELECT * 
                   FROM PurchasedProductPaymentSchedule pps2 WITH (NOLOCK) 
                   WHERE pps2.PurchasedProductID = pp.PurchasedProductID
                   AND pps2.ActualCollectionDate < pps.ActualCollectionDate
                   AND pps2.PaymentStatusID = 1 )		

		INSERT @DetailValueData ( ClientID, DetailFieldID, DetailFieldSubType, DetailValueID, ObjectID, DetailValue )
		SELECT @ClientID, df.DetailFieldID, df.LeadOrMatter, ISNULL(mdv.MatterDetailValueID,-1), @MatterID, ISNULL (
																											CASE df.DetailFieldID  
																											WHEN 179187 THEN CONVERT(VARCHAR,@ValueMoney) /*Reinstatement Next Collection Amount*/
																											WHEN 179188 THEN CONVERT(VARCHAR(10),@NextCollectionDate,121) /*Reinstatement Next Collection Date*/
																											END ,'')
		FROM DetailFields df WITH (NOLOCK)
		LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = @MatterID AND mdv.DetailFieldID = df.DetailFieldID
		WHERE df.DetailFieldID IN (  179187 /*Reinstatement Next Collection Amount*/
									,179188 /*Reinstatement Next Collection Date*/)
                   
	END
	
	IF @ProcessSection IN ( 'MTA' )
	BEGIN
		/*Sum all PPPS records for the next payment date for this policy only*/
		SELECT @ValueMoney = SUM(pps.PaymentGross),@NextCollectionDate = MAX(pps.ActualCollectionDate) 
		FROM PurchasedProduct pp WITH (NOLOCK) 
		INNER JOIN PurchasedProductPaymentSchedule pps WITH (NOLOCK) on pps.PurchasedProductID = pp.PurchasedProductID
		WHERE pp.PurchasedProductID = @PurchasedProductID
		AND pps.PaymentStatusID = 1
		  and not exists ( SELECT * 
                   FROM PurchasedProductPaymentSchedule pps2 WITH (NOLOCK) 
                   WHERE pps2.PurchasedProductID = pp.PurchasedProductID
                   AND pps2.ActualCollectionDate < pps.ActualCollectionDate
                   AND pps2.PaymentStatusID = 1 )
		
		/*Find the regular collection amount from the payment schedule*/
		SELECT TOP 1 @DetailValue = pps.PaymentGross
		FROM PurchasedProductPaymentSchedule pps WITH (NOLOCK) 
		WHERE pps.PurchasedProductID = @PurchasedProductID
		AND pps.PurchasedProductPaymentScheduleTypeID = 1
		ORDER BY pps.PurchasedProductPaymentScheduleID DESC
		
		INSERT @DetailValueData ( ClientID, DetailFieldID, DetailFieldSubType, DetailValueID, ObjectID, DetailValue )
		SELECT @ClientID, df.DetailFieldID, df.LeadOrMatter, ISNULL(mdv.MatterDetailValueID,-1), @MatterID, ISNULL (
																											CASE df.DetailFieldID  
																											WHEN 177908 THEN @DetailValue
																											--WHEN 175339 THEN @DetailValue
																											WHEN 177909 THEN CONVERT(VARCHAR,@ValueMoney)
																											WHEN 178437 THEN CONVERT(VARCHAR(10),@NextCollectionDate,121)
																											END ,'')
		FROM DetailFields df WITH (NOLOCK)
		LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = @MatterID AND mdv.DetailFieldID = df.DetailFieldID
		WHERE df.DetailFieldID IN (  177908 /*Document MTA Regular Collection*/
									,177909 /*Document MTA Next Collection*/
									--,175339 /*Current/New Premium - Other Month*/
									,178437 /*Next actual collection date*/)

		/*Removed @ValueMoney select as the update that uses it (below) is commened out
		SELECT @ValueMoney = NULL
		SELECT @ValueMoney = SUM(pps.PaymentGross)
		FROM PurchasedProductPaymentSchedule pps WITH (NOLOCK) 
		WHERE pps.PurchasedProductID = @PurchasedProductID
		AND pps.PaymentStatusID NOT IN ( 3 /*Ignored*/,4 /*Failed*/ )
		*/

		/*GPR 2020-12-02 removed - ask me about this if this causes a problem*/
		--UPDATE pp
		--SET ProductCostGross = @ValueMoney
		--FROM PurchasedProduct pp WITH (NOLOCK) 
		--WHERE pp.PurchasedProductID = @PurchasedProductID		

		/*CW Prep MTA Fields*/
		/*Adjustment Amount Net/IPT Amount*/
		/*Work out the IPT rate*/ 
		 SELECT @PostCode = LEFT(c.PostCode,3) 
		 FROM Customers c WITH (NOLOCK) 
		 WHERE c.CustomerID = @CustomerID 
		 
		 Select @ValueDate = dbo.fnGetSimpleDv(175442,@MatterID) /*Use MTA adjustment date as date to calculate IPT*/
		
		 SELECT @IPT = i.GrossMultiplier 
		 FROM IPT i WITH (NOLOCK) 
		 WHERE i.Region like '%' + @PostCode + '%'
		 
		IF @IPT IS NULL
		BEGIN
			SELECT TOP 1 @IPT=i.GrossMultiplier 
			FROM IPT i WITH (NOLOCK) 
			WHERE i.CountryID=232
			AND (i.[From] IS NULL OR @ValueDate >= i.[From])
			AND (i.[To] IS NULL OR @ValueDate <= i.[To])
			AND (i.Region IS NULL OR i.Region = '') /*2021-04-06 ACE Fixed based on SC issue rasised (add = '' check)*/
		END

		-- 2020-02-05 CPS for JIRA AAG-91	| Replace TableRow and TableDetailValues references with WrittenPremium
		 SELECT TOP 1	 @GrossAdjustment = wp.AdjustmentValueGross
						,@AbsolouteAdjustment = ABS(wp.AdjustmentValueGross)
						,@NetAdjustment = CAST(wp.AdjustmentValueGross / @IPT AS Decimal (18,2))
						,@IPTAdjustment = @GrossAdjustment - @NetAdjustment
		 FROM WrittenPremium wp WITH (NOLOCK) 
		 WHERE wp.MatterID = @MatterID
		 ORDER BY wp.WrittenPremiumID DESC
   
	     EXEC dbo._C00_SimpleValueIntoField 179968,@GrossAdjustment,@MatterID
	     EXEC dbo._C00_SimpleValueIntoField 179963,@AbsolouteAdjustment,@MatterID
	     EXEC dbo._C00_SimpleValueIntoField 179960,@NetAdjustment,@MatterID
	     EXEC dbo._C00_SimpleValueIntoField 179961,@IPTAdjustment,@MatterID
	     
		/*Number of Remaining Payments*/
		SELECT @ValueInt = COUNT(*) - 1 /*GPR 2018-07-12 minus 1 for Defect 808 C600 */
		FROM PurchasedProductPaymentSchedule cps WITH (NOLOCK) 
		INNER JOIN PurchasedProduct p WITH (NOLOCK) on p.PurchasedProductID = cps.PurchasedProductID 
		WHERE cps.ActualCollectionDate > dbo.fn_GetDate_Local()
		AND p.ObjectID = @MatterID 
		AND cps.PurchasedProductPaymentScheduleTypeID =1 /*AHOD 2018-08-29 Added Scheduled*/
		AND cps.PaymentStatusID =1 /*AHOD 2018-08-29 Added New*/
		
		EXEC dbo._C00_SimpleValueIntoField 179959,@ValueInt,@MatterID
		
		/*2021-04-06 ACE removed backup as we are not altering the schedule!
		EXEC PurchasedProductHistory__Backup @PurchasedProductID, @RunAsUserID
		*/
		
		/*GPR 2020-12-02 removed - ask me about this if this causes a problem*/
		--;WITH ScheduleItems AS
		--(
		--SELECT	 SUM(pps.PaymentGross)	[PaymentGross]
		--		,SUM(pps.PaymentNet)	[PaymentNet]
		--		,SUM(pps.PaymentVat)	[PaymentTax]
		--		,SUM(CASE WHEN pps.PurchasedProductPaymentScheduleTypeID = 5 /*MTA*/ THEN pps.PaymentGross ELSE 0.00 END) [AdminFee]
		--FROM PurchasedProductPaymentSchedule pps WITH (NOLOCK) 
		--WHERE pps.PurchasedProductID = @PurchasedProductID
		--AND pps.PaymentStatusID NOT IN ( 3 /*Ignored*/,4 /*Failed*/ )
		--)
		--UPDATE pp
		--SET  ProductCostGross		= si.PaymentGross
		--	,ProductCostNet			= si.PaymentNet - ISNULL(si.AdminFee,0.00)
		--	,ProductCostVAT			= si.PaymentTax
		--	,ProductAdditionalFee	= si.AdminFee
		--FROM ScheduleItems si 
		--INNER JOIN PurchasedProduct pp WITH (NOLOCK) on pp.PurchasedProductID = @PurchasedProductID
		--WHERE pp.PurchasedProductID = @PurchasedProductID

		/*NG 2020-12-03 removed & replaced - ask me about this if this causes a problem*/		
/*		SELECT @Net = p.ProductCostNet, @IPT = p.ProductCostVAT FROM PurchasedProduct p WITH (NOLOCK) 
		where p.PurchasedProductID = @PurchasedProductID
		
		EXEC _C00_SimpleValueIntoField 180094, @Net, @MatterID, 58552
		EXEC _C00_SimpleValueIntoField 180096, @IPT, @MatterID, 58552
*/

		/*NG 20200-12-03 MTA Premium Breakdown Fields added for JIRA PPET-769*/

		SELECT @PremiumNet				= dbo.fn_C00_PaymentScheduleTotals (@MatterID,1)
		EXEC _C00_SimpleValueIntoField 180094, @PremiumNet, @MatterID, 58552

		SELECT @PremiumTax				= dbo.fn_C00_PaymentScheduleTotals (@MatterID,2)
		EXEC _C00_SimpleValueIntoField 180096, @PremiumTax, @MatterID, 58552

		SELECT @PremiumGross			= dbo.fn_C00_PaymentScheduleTotals (@MatterID,3)
		EXEC _C00_SimpleValueIntoField 314349, @PremiumGross, @MatterID, 58552

		SELECT @RemainingPremiumNet		= dbo.fn_C00_PaymentScheduleTotals (@MatterID,4)
		EXEC _C00_SimpleValueIntoField 314350, @RemainingPremiumNet, @MatterID, 58552

		SELECT @RemainingPremiumTax		= dbo.fn_C00_PaymentScheduleTotals (@MatterID,5)
		EXEC _C00_SimpleValueIntoField 314351, @RemainingPremiumTax, @MatterID, 58552

		SELECT @RemainingPremiumGross	= dbo.fn_C00_PaymentScheduleTotals (@MatterID,6)
		EXEC _C00_SimpleValueIntoField 314352, @RemainingPremiumGross, @MatterID, 58552


	END

	SELECT @RowCount = COUNT(*)
	FROM @DetailValueData vd 
	
	IF @RowCount > 0
	BEGIN
		PRINT 'Bulk Saving Values <br>'
		INSERT @BlackHole (ID1, ID2)
		EXEC DetailValues_BulkSave @DetailValueData, @ClientID, @RunAsUserID
		PRINT 'Bulk Save Complete <br>'
	END
	
	PRINT CONVERT(VARCHAR,@RowCount) + ' fields updated.'
	RETURN @RowCount
	
END






GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SetDocumentFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_SetDocumentFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_SetDocumentFields] TO [sp_executeall]
GO
