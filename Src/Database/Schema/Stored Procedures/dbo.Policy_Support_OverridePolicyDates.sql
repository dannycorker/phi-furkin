SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		James Lewis
-- Create date: 2018-06-19
-- Description:	James' Bill
-- =============================================
CREATE PROCEDURE [dbo].[Policy_Support_OverridePolicyDates]
(
	  @Inception DATE
	 ,@PolicyStart DATE 
	 ,@PolicyEnd DATE 
	 ,@MatterID INT  
	 ,@PremiumDetailTR INT  
	 ,@Historical INT  
	 ,@PurchasedProductID INT
)
AS
BEGIN

	DECLARE @RenewalDate DATE = DATEADD(YEAR,1,@PolicyStart) 

	 /*Inception*/ 
	UPDATE MatterDetailValues 
	SET detailvalue = @Inception --yyyy-mm-dd
	where DetailFieldID = 170035 and MatterID = @MatterID


	/*The below statements will change the values for the start date, end date and inception date within the Historical Policy Table 
	on the PA leadType on the POLICY DETAILS PAGE. You need to replace the following numbers below '2071045' with the tablerow id 
	relating to the policy date you want to change. You also want to change the date detail value to the relevant date

	Historical Policy
	 */

	EXEC dbo._C00_SimpleValueIntoField 145662, @Inception, @Historical   --inception 
	EXEC dbo._C00_SimpleValueIntoField 145663, @PolicyStart, @Historical -- start 
	EXEC dbo._C00_SimpleValueIntoField 170092, @Inception, @Historical  --terms date
	EXEC dbo._C00_SimpleValueIntoField 145664, @PolicyEnd, @Historical -- end 


	/*
	The original sale date field below is located @ Matter Level and is on the policy overview page
	*/

	UPDATE MatterDetailValues 
	SET detailvalue = @Inception --yyyy-mm-dd
	where DetailFieldID = 177418 and MatterID = @MatterID

	/*
	The start date field below is located @ Matter Level and is on the policy overview page
	*/

	UPDATE MatterDetailValues 
	SET detailvalue = @PolicyStart --yyyy-mm-dd
	where DetailFieldID = 170036 and MatterID = @MatterID

	/*
	The end date field below is located @ Matter Level and is on the policy overview page
	*/
	UPDATE MatterDetailValues 
	SET detailvalue = @PolicyEnd --yyyy-mm-dd
	where DetailFieldID = 170037 and MatterID = @MatterID


	/*
	The Covered Until/Policy Terms Date field below is located @ Matter Level and is on the policy overview page
	*/
	UPDATE MatterDetailValues 
	SET detailvalue = @PolicyEnd --yyyy-mm-dd
	where DetailFieldID = 177287 and MatterID = @MatterID

	UPDATE MatterDetailValues 
	SET detailvalue = @Inception --yyyy-mm-dd
	where DetailFieldID = 176925 and MatterID = @MatterID

	/*
	For the PPPS we will have to rebuild the entire schedule due to the fact that all the cover to and the cover from will change.
	This will directly affect the scheduled and collection dates so these will need to be changed as well 
	*/

	/*
	Below is the update statements for the PREMIUM DETAIL HISTORY table within the BILLING tab on the PA lead
	*/

	EXEC dbo._C00_SimpleValueIntoField 175346, @PolicyStart, @PremiumDetailTR --From
	EXEC dbo._C00_SimpleValueIntoField 175396, @Inception, @PremiumDetailTR -- Adjustment Date
	EXEC dbo._C00_SimpleValueIntoField 175400, @PolicyStart, @PremiumDetailTR -- Adjustment Application Date

	UPDATE MatterDetailValues 
	SET detailvalue = @RenewalDate --yyyy-mm-dd
	where DetailFieldID = 175307 and MatterID = @MatterID -- Policy Renewal Date


	Update PurchasedProduct
	SET Validfrom = @PolicyStart, ValidTo = @PolicyEnd, ProductPurchasedOnDate = @Inception
	where PurchasedProductID  = @PurchasedProductID

	UPDATE PurchasedProductPaymentSchedule
	SET CoverFrom = @PolicyStart,
	CoverTo = @PolicyEnd,
	PaymentStatusID = 6
	where purchasedproductid = @PurchasedProductID

	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Policy_Support_OverridePolicyDates] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Policy_Support_OverridePolicyDates] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Policy_Support_OverridePolicyDates] TO [sp_executeall]
GO
