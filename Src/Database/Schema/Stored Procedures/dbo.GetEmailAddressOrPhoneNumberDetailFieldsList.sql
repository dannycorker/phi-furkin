SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










/*
----------------------------------------------------------------------------------------------------

-- Created By:  Chris Townsend
-- Created On:  27 May 2008
-- Purpose: Gets all email address or phone number detail fields, both for lead/matter detail fields and
-- for detail fields that are part of a resource list
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[GetEmailAddressOrPhoneNumberDetailFieldsList]
(
	
	@QuestionTypeID INT,
	@ClientID INT,
	@LeadTypeID INT = 0
)
AS
BEGIN

	SET ANSI_NULLS OFF
	
	-- Basic fields first	
	-- Select all fields of the appropriate type (@QuestionTypeID) that are not part of a resource list
	SELECT CASE df.LeadOrMatter WHEN 1 THEN '[!Lead:' ELSE '[!Matter:' END + CONVERT(VARCHAR,df.DetailFieldID) + ']' AS FieldValue,
		   CASE df.LeadOrMatter WHEN 1 THEN 'Lead:' ELSE 'Matter:' END + CONVERT(VARCHAR,df.DetailFieldID) + ':' + df.FieldName AS FieldName,
		   'Page: ' + dfp.PageCaption AS PageCaption
	FROM DetailFields df WITH (NOLOCK) 
	INNER JOIN DetailFieldPages dfp WITH (NOLOCK) ON df.DetailFieldPageID = dfp.DetailFieldPageID
	WHERE df.QuestionTypeID = @QuestionTypeID
	AND (dfp.ResourceList <> 1 OR dfp.ResourceList IS NULL)
	AND (@LeadTypeID = 0 OR df.LeadTypeID = @LeadTypeID)
	AND df.ClientID = @ClientID

	UNION

	-- Then unaliased resource list fields
	/*
	SELECT CASE df.LeadOrMatter WHEN 1 THEN '[!Lead:' ELSE '[!Matter:' END + Convert(varchar,dfr.DetailFieldID) + ', ' + Convert(varchar,df.DetailFieldID) + ']' AS FieldValue,
		   CASE df.LeadOrMatter WHEN 1 THEN 'Lead:' ELSE 'Matter:' END + Convert(varchar,dfr.DetailFieldID) + ':' + df.FieldName AS FieldName,
		   'Page: ' + dfp.PageCaption AS PageCaption
	FROM DetailFields df
	INNER JOIN DetailFieldPages dfp ON df.DetailFieldPageID = dfp.DetailFieldPageID
	INNER JOIN DetailFields dfr ON dfr.ResourceListDetailFieldPageID = dfp.DetailFieldPageID
	WHERE df.QuestionTypeID = @QuestionTypeID
	AND dfp.ResourceList = 1
	AND (@LeadTypeID = 0 OR df.LeadTypeID = @LeadTypeID)
	AND df.ClientID = @ClientID

	UNION
	*/

	-- Then resource list fields (aliased or otherwise)
	-- Find all detail fields (df) of the appropriate type (@QuestionTypeID) for this lead type (@LeadTypeID)
	-- Find the page they reside on (dfp), which we know is a resource list because of the "AND dfp.ResourceList = 1" clause
	-- Find the resource list field itself (dfr) so users can see which list they are picking from
	-- Find the Aliases (if they exist) for the resource list (dfa1) and the individual field (dfa2)
	-- Format the results nicely like this:
	--		[!A:Aliased Emails, Home Email]
	--		[!A:Aliased Emails, Work Email]
	SELECT CASE WHEN dfa1.DetailFieldAliasID IS NOT NULL AND dfa2.DetailFieldAliasID IS NOT NULL THEN '[!A:' + dfa1.DetailFieldAlias + ', ' + dfa2.DetailFieldAlias + ']' ELSE CASE df.LeadOrMatter WHEN 1 THEN '[!Lead:' ELSE '[!Matter:' END + CONVERT(VARCHAR,dfr.DetailFieldID) + ', ' + CONVERT(VARCHAR,df.DetailFieldID) + ']' END AS FieldValue,
		   CASE WHEN dfa1.DetailFieldAliasID IS NOT NULL AND dfa2.DetailFieldAliasID IS NOT NULL THEN dfa1.DetailFieldAlias + ', ' + dfa2.DetailFieldAlias ELSE CASE df.LeadOrMatter WHEN 1 THEN 'Lead:' ELSE 'Matter:' END + CONVERT(VARCHAR,dfr.DetailFieldID) + ':' + df.FieldName END AS FieldName,
		   'Page: ' + dfp.PageCaption AS PageCaption
	FROM DetailFields df WITH (NOLOCK) 
	INNER JOIN DetailFieldPages dfp WITH (NOLOCK) ON df.DetailFieldPageID = dfp.DetailFieldPageID
	INNER JOIN DetailFields dfr WITH (NOLOCK) ON dfr.ResourceListDetailFieldPageID = dfp.DetailFieldPageID 
	LEFT JOIN DetailFieldAlias dfa1 WITH (NOLOCK) ON dfa1.DetailFieldID = dfr.DetailFieldID AND dfa1.ClientID = @ClientID
	LEFT JOIN DetailFieldAlias dfa2 WITH (NOLOCK) ON dfa2.DetailFieldID = df.DetailFieldID AND dfa2.ClientID = @ClientID
	WHERE dfr.QuestionTypeID = 14 -- @Resource List
	AND df.QuestionTypeID = @QuestionTypeID -- Phone Number, Email Address etc
	AND dfp.ResourceList = 1
	AND (@LeadTypeID = 0 OR df.LeadTypeID = @LeadTypeID)
	AND df.ClientID = @ClientID

	SELECT @@ROWCOUNT
	
	SET ANSI_NULLS ON

END





GO
GRANT VIEW DEFINITION ON  [dbo].[GetEmailAddressOrPhoneNumberDetailFieldsList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEmailAddressOrPhoneNumberDetailFieldsList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEmailAddressOrPhoneNumberDetailFieldsList] TO [sp_executeall]
GO
