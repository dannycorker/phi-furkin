SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlQueryCriteria table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryCriteria_Update]
(

	@SqlQueryCriteriaID int   ,

	@ClientID int   ,

	@SqlQueryID int   ,

	@SqlQueryTableID int   ,

	@SqlQueryColumnID int   ,

	@CriteriaText varchar (1000)  ,

	@Criteria1 varchar (250)  ,

	@Criteria2 varchar (250)  ,

	@CriteriaName varchar (250)  ,

	@SubQueryID int   ,

	@SubQueryLinkType varchar (50)  ,

	@ParamValue varchar (250)  ,

	@TempTableID int   ,

	@TempColumnID int   ,

	@TempCriteriaID int   ,

	@IsSecurityClause bit   ,

	@CriteriaSubstitutions varchar (2000)  ,

	@IsParameterizable bit   ,

	@Comparison varchar (50)  ,

	@IsJoinClause bit   ,

	@SourceID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlQueryCriteria]
				SET
					[ClientID] = @ClientID
					,[SqlQueryID] = @SqlQueryID
					,[SqlQueryTableID] = @SqlQueryTableID
					,[SqlQueryColumnID] = @SqlQueryColumnID
					,[CriteriaText] = @CriteriaText
					,[Criteria1] = @Criteria1
					,[Criteria2] = @Criteria2
					,[CriteriaName] = @CriteriaName
					,[SubQueryID] = @SubQueryID
					,[SubQueryLinkType] = @SubQueryLinkType
					,[ParamValue] = @ParamValue
					,[TempTableID] = @TempTableID
					,[TempColumnID] = @TempColumnID
					,[TempCriteriaID] = @TempCriteriaID
					,[IsSecurityClause] = @IsSecurityClause
					,[CriteriaSubstitutions] = @CriteriaSubstitutions
					,[IsParameterizable] = @IsParameterizable
					,[Comparison] = @Comparison
					,[IsJoinClause] = @IsJoinClause
					,[SourceID] = @SourceID
				WHERE
[SqlQueryCriteriaID] = @SqlQueryCriteriaID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteria_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryCriteria_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryCriteria_Update] TO [sp_executeall]
GO
