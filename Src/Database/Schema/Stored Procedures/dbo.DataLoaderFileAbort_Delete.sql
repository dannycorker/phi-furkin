SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DataLoaderFileAbort table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileAbort_Delete]
(

	@DataLoaderFileAbortID int   
)
AS


				DELETE FROM [dbo].[DataLoaderFileAbort] WITH (ROWLOCK) 
				WHERE
					[DataLoaderFileAbortID] = @DataLoaderFileAbortID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileAbort_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileAbort_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileAbort_Delete] TO [sp_executeall]
GO
