SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalOption table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalOption_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[PortalOptionID],
					[ClientID],
					[ShowHelpLink],
					[HelpLinkCaption],
					[HelpLinkURL],
					[LeadListShowAssignedTo],
					[LeadListShowAssignedToCaption],
					[LeadListShowCaseStatus],
					[LeadListShowCaseStatusCaption]
				FROM
					[dbo].[PortalOption] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalOption_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalOption_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalOption_GetByClientID] TO [sp_executeall]
GO
