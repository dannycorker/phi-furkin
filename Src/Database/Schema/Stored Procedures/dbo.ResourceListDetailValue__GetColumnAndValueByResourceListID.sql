SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 03-02-2016
-- Description:	Gets a list of ResourceListDetailValues by resource list id
-- Includes the fieldName and questiontype id 
-- =============================================
CREATE PROCEDURE [dbo].[ResourceListDetailValue__GetColumnAndValueByResourceListID]

	@ResourceListID INT

AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT rdv.DetailFieldID, df.FieldName, df.FieldCaption, ISNULL(it.ItemValue,rdv.DetailValue) AS [DetailValue], df.QuestionTypeID 
	FROM ResourceListDetailValues rdv WITH (NOLOCK) 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID=rdv.DetailFieldID
	LEFT JOIN dbo.LookupListItems it WITH (NOLOCK) on LookupListItemID=rdv.ValueINT AND it.LookupListID=df.LookupListID
	WHERE rdv.ResourceListID=@ResourceListID


END
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValue__GetColumnAndValueByResourceListID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceListDetailValue__GetColumnAndValueByResourceListID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceListDetailValue__GetColumnAndValueByResourceListID] TO [sp_executeall]
GO
