SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-09-16
-- Description:	Get Fields For Simple Uploader
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CreateNewMap]
	@ClientID INT,
	@LeadTypeID INT,
	@MapMroperties dbo.tvpVarchar10 READONLY,
	@ClientPersonnelID INT,
	@MapName VARCHAR(250)
AS
BEGIN

	SET NOCOUNT ON;
	
	/*
		tvpVarchar10 contains:
		Val1 = File Column,
		Val2 = Field ID (DLM or DFID full stop seperated list),
		Val3 = Match Field,
		Val4 = EquationText,
		Val5 = Column Name 

	*/
	
	DECLARE @DataLoaderMapID INT,
			@CustomerSectionID INT,
			@LeadSectionID INT,
			@CaseSectionID INT,
			@MatterSectionID INT,
			@CustomerDetailValuesSectionID INT,
			@LeadDetailValuesSectionID INT,
			@CaseDetailValuesSectionID INT,
			@MatterDetailValuesSectionID INT,
			@BaseSectionIDForNamedValues INT,
			@DataLoaderObjectTypeIDForNamedValues INT,
			@DataLoaderObjectActionID INT,
			@HasAquariumID INT = 0
	
	DECLARE @DataloaderObjectFields TABLE (ColumnNumber INT, DataLoaderObjectTypeID INT, DataLoaderObjectFieldID INT, ColumnName VARCHAR(100), Equationtext VARCHAR(200), IsMatchField BIT)
	
	DECLARE @DetailFields TABLE (ColumnNumber INT, DetailFieldID INT, ColumnName VARCHAR(100), DetailFieldSubTypeID INT, Equationtext VARCHAR(200), IsMatchField BIT)
	
	DECLARE @NamesValues TABLE (NamedValue VARCHAR(200), DataLoaderFieldID INT)
	
	INSERT INTO @DataloaderObjectFields (ColumnNumber, DataLoaderObjectTypeID, DataLoaderObjectFieldID, ColumnName, Equationtext, IsMatchField)
	SELECT m.Val1, a.AnyValue, a.AnyValue2, m.Val5, m.Val4, m.Val3
	FROM @MapMroperties m 
	CROSS APPLY dbo.fnTableOfValuesFromCSVPSV(m.Val2, '|', '.') a
	WHERE a.AnyValue > 0
	
	--SELECT *
	--FROM @DataloaderObjectFields
	
	INSERT INTO @DetailFields (ColumnNumber, DetailFieldID, ColumnName, DetailFieldSubTypeID, Equationtext, IsMatchField)
	SELECT m.Val1, a.AnyValue2, m.Val5, df.LeadOrMatter, m.Val4, m.Val3
	FROM @MapMroperties m 
	CROSS APPLY dbo.fnTableOfValuesFromCSVPSV(m.Val2, '|', '.') a
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = a.AnyValue2
	WHERE a.AnyValue < 0

	--SELECT *
	--FROM @DetailFields
	
	/*
		Step 1. Create New Map
		Step 2. Create Sectons as required (File Header, etc)
		Step 3. Go section by section setting fields (if top level ie customer, set others as holders)
		Return Map ID
	*/
	INSERT INTO DataLoaderMap(ClientID, MapName, MapDescription, WhenCreated, CreatedBy, LastUpdated, UpdatedBy, EnabledForUse, Deleted, LeadTypeID, UseDefaultLeadForCustomer, UseDefaultCaseForLead, UseDefaultMatterForCase)
	VALUES (@ClientID, @MapName, '', dbo.fn_GetDate_Local(), @ClientPersonnelID, dbo.fn_GetDate_Local(), @ClientPersonnelID, 1, 0, @LeadTypeID, 1, 0, 0)

	SELECT @DataLoaderMapID = SCOPE_IDENTITY()

	SELECT @DataLoaderMapID AS [MapID]

	/*Create File Header*/
	INSERT INTO DataLoaderMapSection (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderObjectActionID, DataLoaderSectionLocaterTypeID, DetailFieldSubTypeID, HasAquariumID, NumberOfFooterRowsToSkip, ImportIntoLeadManager, IsFixedLengthSection, FixedLengthNumberOfRows, IsMandatory, IsMultipleAllowedWithinFile, Notes, TableDetailFieldID, ResourceListDetailFieldID)
	VALUES (@ClientID, @DataLoaderMapID, 12, NULL, 2, NULL, 0, 0, NULL, 1, 1, 0, 0, '', NULL, NULL)

	IF EXISTS (
		SELECT *
		FROM @DataloaderObjectFields d 
		WHERE d.DataLoaderObjectTypeID = 1
	)
	BEGIN
	
		IF EXISTS (
			SELECT *
			FROM @DataloaderObjectFields d
			WHERE d.DataLoaderObjectTypeID = 1
			AND d.IsMatchField = 1
		)
		BEGIN
		
			SELECT @DataLoaderObjectActionID = 2
		
		END
		ELSE
		BEGIN

			SELECT @DataLoaderObjectActionID = 1
		
		END
	
		/*Set the HasAquariumID field to 1 if the field has a Customer, Lead, Case or MatterID in it*/
		IF EXISTS (
			SELECT *
			FROM @DataloaderObjectFields d 
			WHERE d.DataLoaderObjectTypeID = 1
			AND d.DataLoaderObjectFieldID = 48
			AND d.IsMatchField = 1
		)
		BEGIN
		
			SELECT @HasAquariumID = 1 

		END

		/*We have some customer fields, create a customer section*/
		INSERT INTO DataLoaderMapSection (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderObjectActionID, DataLoaderSectionLocaterTypeID, DetailFieldSubTypeID, HasAquariumID, NumberOfFooterRowsToSkip, ImportIntoLeadManager, IsFixedLengthSection, FixedLengthNumberOfRows, IsMandatory, IsMultipleAllowedWithinFile, Notes, TableDetailFieldID, ResourceListDetailFieldID)
		VALUES (@ClientID, @DataLoaderMapID, 1, @DataLoaderObjectActionID, 2, NULL, @HasAquariumID, 0, 1, 0, 0, 0, 0, '', NULL, NULL)
		
		SELECT @CustomerSectionID = SCOPE_IDENTITY()
		
		INSERT INTO DataLoaderFieldDefinition (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderMapSectionID, DataLoaderObjectFieldID, DetailFieldID, DetailFieldAlias, NamedValue, Keyword, DataLoaderKeywordMatchTypeID, RowRelativeToKeyword, ColRelativeToKeyword, SectionRelativeRow, SectionAbsoluteCol, ValidationRegex, Equation, IsMatchField, DecodeTypeID, DefaultLookupItemID, SourceDataLoaderFieldDefinitionID, Notes, AllowErrors)
		SELECT @ClientID, @DataLoaderMapID, DataLoaderObjectTypeID, @CustomerSectionID, DataLoaderObjectFieldID, NULL, NULL, NULL, '', NULL, NULL, NULL, 0, d.ColumnNumber, '', d.Equationtext, d.IsMatchField, NULL, 0, NULL, '', 0
		FROM @DataloaderObjectFields d 
		WHERE d.DataLoaderObjectTypeID = 1
		
		SELECT @DataLoaderObjectTypeIDForNamedValues = 1, @HasAquariumID = 0

	END

	IF EXISTS (
		SELECT *
		FROM @DataloaderObjectFields d 
		WHERE d.DataLoaderObjectTypeID = 2
	)
	BEGIN
	
		IF EXISTS (
			SELECT *
			FROM @DataloaderObjectFields d 
			WHERE d.DataLoaderObjectTypeID = 2
			AND d.DataLoaderObjectFieldID = 49
			AND d.IsMatchField = 1
		)
		BEGIN

			SELECT @HasAquariumID = 1 
	
		END

		/*We have some lead fields, create a lead section*/
		INSERT INTO DataLoaderMapSection (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderObjectActionID, DataLoaderSectionLocaterTypeID, DetailFieldSubTypeID, HasAquariumID, NumberOfFooterRowsToSkip, ImportIntoLeadManager, IsFixedLengthSection, FixedLengthNumberOfRows, IsMandatory, IsMultipleAllowedWithinFile, Notes, TableDetailFieldID, ResourceListDetailFieldID)
		VALUES (@ClientID, @DataLoaderMapID, 2, 1, 2, NULL, @HasAquariumID, 0, 1, 0, 0, 0, 0, '', NULL, NULL)
		
		SELECT @CustomerSectionID = SCOPE_IDENTITY()
		
		INSERT INTO DataLoaderFieldDefinition (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderMapSectionID, DataLoaderObjectFieldID, DetailFieldID, DetailFieldAlias, NamedValue, Keyword, DataLoaderKeywordMatchTypeID, RowRelativeToKeyword, ColRelativeToKeyword, SectionRelativeRow, SectionAbsoluteCol, ValidationRegex, Equation, IsMatchField, DecodeTypeID, DefaultLookupItemID, SourceDataLoaderFieldDefinitionID, Notes, AllowErrors)
		SELECT @ClientID, @DataLoaderMapID, DataLoaderObjectTypeID, @CustomerSectionID, DataLoaderObjectFieldID, NULL, NULL, NULL, '', NULL, NULL, NULL, 0, d.ColumnNumber, '', d.Equationtext, d.IsMatchField, NULL, 0, NULL, '', 0
		FROM @DataloaderObjectFields d 
		WHERE d.DataLoaderObjectTypeID = 2
		
		SELECT @HasAquariumID = 0
		
		IF @DataLoaderObjectTypeIDForNamedValues IS NULL
		BEGIN
			SELECT @DataLoaderObjectTypeIDForNamedValues = 2
		END

	END

	IF EXISTS (
		SELECT *
		FROM @DataloaderObjectFields d 
		WHERE d.DataLoaderObjectTypeID = 3
	)
	BEGIN
	
		IF EXISTS (
			SELECT *
			FROM @DataloaderObjectFields d 
			WHERE d.DataLoaderObjectTypeID = 3
			AND d.DataLoaderObjectFieldID = 50
			AND d.IsMatchField = 1
		)
		BEGIN

			SELECT @HasAquariumID = 1 
	
		END

		/*We have some case fields, create a case section*/
		INSERT INTO DataLoaderMapSection (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderObjectActionID, DataLoaderSectionLocaterTypeID, DetailFieldSubTypeID, HasAquariumID, NumberOfFooterRowsToSkip, ImportIntoLeadManager, IsFixedLengthSection, FixedLengthNumberOfRows, IsMandatory, IsMultipleAllowedWithinFile, Notes, TableDetailFieldID, ResourceListDetailFieldID)
		VALUES (@ClientID, @DataLoaderMapID, 3, 1, 2, NULL, @HasAquariumID, 0, 1, 0, 0, 0, 0, '', NULL, NULL)
		
		SELECT @CustomerSectionID = SCOPE_IDENTITY()
		
		INSERT INTO DataLoaderFieldDefinition (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderMapSectionID, DataLoaderObjectFieldID, DetailFieldID, DetailFieldAlias, NamedValue, Keyword, DataLoaderKeywordMatchTypeID, RowRelativeToKeyword, ColRelativeToKeyword, SectionRelativeRow, SectionAbsoluteCol, ValidationRegex, Equation, IsMatchField, DecodeTypeID, DefaultLookupItemID, SourceDataLoaderFieldDefinitionID, Notes, AllowErrors)
		SELECT @ClientID, @DataLoaderMapID, DataLoaderObjectTypeID, @CustomerSectionID, DataLoaderObjectFieldID, NULL, NULL, NULL, '', NULL, NULL, NULL, 0, d.ColumnNumber, '', d.Equationtext, d.IsMatchField, NULL, 0, NULL, '', 0
		FROM @DataloaderObjectFields d 
		WHERE d.DataLoaderObjectTypeID = 3
		
		SELECT @HasAquariumID = 0
		
		IF @DataLoaderObjectTypeIDForNamedValues IS NULL
		BEGIN
			SELECT @DataLoaderObjectTypeIDForNamedValues = 3
		END

	END

	IF EXISTS (
		SELECT *
		FROM @DataloaderObjectFields d 
		WHERE d.DataLoaderObjectTypeID = 4
	)
	BEGIN
	
		IF EXISTS (
			SELECT *
			FROM @DataloaderObjectFields d 
			WHERE d.DataLoaderObjectTypeID = 4
			AND d.DataLoaderObjectFieldID = 51
			AND d.IsMatchField = 1
		)
		BEGIN

			SELECT @HasAquariumID = 1 
	
		END

		/*We have some matter fields, create a case section*/
		INSERT INTO DataLoaderMapSection (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderObjectActionID, DataLoaderSectionLocaterTypeID, DetailFieldSubTypeID, HasAquariumID, NumberOfFooterRowsToSkip, ImportIntoLeadManager, IsFixedLengthSection, FixedLengthNumberOfRows, IsMandatory, IsMultipleAllowedWithinFile, Notes, TableDetailFieldID, ResourceListDetailFieldID)
		VALUES (@ClientID, @DataLoaderMapID, 4, 1, 2, NULL, @HasAquariumID, 0, 1, 0, 0, 0, 0, '', NULL, NULL)
		
		SELECT @CustomerSectionID = SCOPE_IDENTITY()
		
		INSERT INTO DataLoaderFieldDefinition (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderMapSectionID, DataLoaderObjectFieldID, DetailFieldID, DetailFieldAlias, NamedValue, Keyword, DataLoaderKeywordMatchTypeID, RowRelativeToKeyword, ColRelativeToKeyword, SectionRelativeRow, SectionAbsoluteCol, ValidationRegex, Equation, IsMatchField, DecodeTypeID, DefaultLookupItemID, SourceDataLoaderFieldDefinitionID, Notes, AllowErrors)
		SELECT @ClientID, @DataLoaderMapID, DataLoaderObjectTypeID, @CustomerSectionID, DataLoaderObjectFieldID, NULL, NULL, NULL, '', NULL, NULL, NULL, 0, d.ColumnNumber, '', d.Equationtext, d.IsMatchField, NULL, 0, NULL, '', 0
		FROM @DataloaderObjectFields d 
		WHERE d.DataLoaderObjectTypeID = 4
		
		SELECT @HasAquariumID = 0
		
		IF @DataLoaderObjectTypeIDForNamedValues IS NULL
		BEGIN
			SELECT @DataLoaderObjectTypeIDForNamedValues = 4
		END

	END

	IF EXISTS (
		SELECT *
		FROM @DataloaderObjectFields d 
		WHERE d.DataLoaderObjectTypeID = 10
	)
	BEGIN
	
		/*We have some case fields, create a case section*/
		INSERT INTO DataLoaderMapSection (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderObjectActionID, DataLoaderSectionLocaterTypeID, DetailFieldSubTypeID, HasAquariumID, NumberOfFooterRowsToSkip, ImportIntoLeadManager, IsFixedLengthSection, FixedLengthNumberOfRows, IsMandatory, IsMultipleAllowedWithinFile, Notes, TableDetailFieldID, ResourceListDetailFieldID)
		VALUES (@ClientID, @DataLoaderMapID, 10, 1, 2, NULL, 0, 0, 1, 0, 0, 0, 0, '', NULL, NULL)
		
		SELECT @CustomerSectionID = SCOPE_IDENTITY()
		
		INSERT INTO DataLoaderFieldDefinition (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderMapSectionID, DataLoaderObjectFieldID, DetailFieldID, DetailFieldAlias, NamedValue, Keyword, DataLoaderKeywordMatchTypeID, RowRelativeToKeyword, ColRelativeToKeyword, SectionRelativeRow, SectionAbsoluteCol, ValidationRegex, Equation, IsMatchField, DecodeTypeID, DefaultLookupItemID, SourceDataLoaderFieldDefinitionID, Notes, AllowErrors)
		SELECT @ClientID, @DataLoaderMapID, DataLoaderObjectTypeID, @CustomerSectionID, DataLoaderObjectFieldID, NULL, NULL, NULL, '', NULL, NULL, NULL, 0, d.ColumnNumber, '', d.Equationtext, d.IsMatchField, NULL, 0, NULL, '', 0
		FROM @DataloaderObjectFields d 
		WHERE d.DataLoaderObjectTypeID = 10
		
		IF @DataLoaderObjectTypeIDForNamedValues IS NULL
		BEGIN
			SELECT @DataLoaderObjectTypeIDForNamedValues = 10
		END

	END

	/*Now the sections have been created set the fields up and named valeus where required*/
	SELECT @BaseSectionIDForNamedValues = COALESCE(@CustomerSectionID, @LeadSectionID, @CaseSectionID, @MatterSectionID)

	IF @BaseSectionIDForNamedValues > 0
	BEGIN

		INSERT INTO DataLoaderFieldDefinition (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderMapSectionID, DataLoaderObjectFieldID, DetailFieldID, DetailFieldAlias, NamedValue, Keyword, DataLoaderKeywordMatchTypeID, RowRelativeToKeyword, ColRelativeToKeyword, SectionRelativeRow, SectionAbsoluteCol, ValidationRegex, Equation, IsMatchField, DecodeTypeID, DefaultLookupItemID, SourceDataLoaderFieldDefinitionID, Notes, AllowErrors)
		OUTPUT inserted.DataLoaderFieldDefinitionID, inserted.NamedValue INTO @NamesValues (DataLoaderFieldID, NamedValue)
		SELECT @ClientID, @DataLoaderMapID, @DataLoaderObjectTypeIDForNamedValues, @BaseSectionIDForNamedValues, NULL, NULL, NULL, d.ColumnName, '', NULL, NULL, NULL, 0, d.ColumnNumber, '', d.Equationtext, d.IsMatchField, NULL, NULL, NULL, '', 0
		FROM @DetailFields d 

	END
	
	IF EXISTS (
		SELECT *
		FROM @DetailFields d 
		WHERE d.DetailFieldSubTypeID = 10
	)
	BEGIN
	
		/*We have some customer detail value fields, create a customerdetailvalues section*/
		INSERT INTO DataLoaderMapSection (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderObjectActionID, DataLoaderSectionLocaterTypeID, DetailFieldSubTypeID, HasAquariumID, NumberOfFooterRowsToSkip, ImportIntoLeadManager, IsFixedLengthSection, FixedLengthNumberOfRows, IsMandatory, IsMultipleAllowedWithinFile, Notes, TableDetailFieldID, ResourceListDetailFieldID)
		VALUES (@ClientID, @DataLoaderMapID, 16, 1, 2, NULL, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL)
		
		SELECT @CustomerDetailValuesSectionID = SCOPE_IDENTITY()
		
		INSERT INTO DataLoaderFieldDefinition (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderMapSectionID, DataLoaderObjectFieldID, DetailFieldID, DetailFieldAlias, NamedValue, Keyword, DataLoaderKeywordMatchTypeID, RowRelativeToKeyword, ColRelativeToKeyword, SectionRelativeRow, SectionAbsoluteCol, ValidationRegex, Equation, IsMatchField, DecodeTypeID, DefaultLookupItemID, SourceDataLoaderFieldDefinitionID, Notes, AllowErrors)
		SELECT @ClientID, @DataLoaderMapID, 16, @CustomerDetailValuesSectionID, NULL, d.DetailFieldID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 0, CASE df.Lookup WHEN 1 THEN 3 ELSE NULL END, NULL, n.DataLoaderFieldID, '', 0
		FROM @DetailFields d 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = d.DetailFieldID
		INNER JOIN @NamesValues n ON n.NamedValue = d.ColumnName
		WHERE d.DetailFieldSubTypeID = 10
	
	END

	IF EXISTS (
		SELECT *
		FROM @DetailFields d 
		WHERE d.DetailFieldSubTypeID = 1
	)
	BEGIN
	
		/*We have some customer detail value fields, create a customerdetailvalues section*/
		INSERT INTO DataLoaderMapSection (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderObjectActionID, DataLoaderSectionLocaterTypeID, DetailFieldSubTypeID, HasAquariumID, NumberOfFooterRowsToSkip, ImportIntoLeadManager, IsFixedLengthSection, FixedLengthNumberOfRows, IsMandatory, IsMultipleAllowedWithinFile, Notes, TableDetailFieldID, ResourceListDetailFieldID)
		VALUES (@ClientID, @DataLoaderMapID, 5, 1, 2, NULL, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL)
		
		SELECT @CustomerDetailValuesSectionID = SCOPE_IDENTITY()
		
		INSERT INTO DataLoaderFieldDefinition (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderMapSectionID, DataLoaderObjectFieldID, DetailFieldID, DetailFieldAlias, NamedValue, Keyword, DataLoaderKeywordMatchTypeID, RowRelativeToKeyword, ColRelativeToKeyword, SectionRelativeRow, SectionAbsoluteCol, ValidationRegex, Equation, IsMatchField, DecodeTypeID, DefaultLookupItemID, SourceDataLoaderFieldDefinitionID, Notes, AllowErrors)
		SELECT @ClientID, @DataLoaderMapID, 5, @CustomerDetailValuesSectionID, NULL, d.DetailFieldID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 0, CASE df.Lookup WHEN 1 THEN 3 ELSE NULL END, NULL, n.DataLoaderFieldID, '', 0
		FROM @DetailFields d 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = d.DetailFieldID
		INNER JOIN @NamesValues n ON n.NamedValue = d.ColumnName
		WHERE d.DetailFieldSubTypeID = 1
	
	END

	IF EXISTS (
		SELECT *
		FROM @DetailFields d 
		WHERE d.DetailFieldSubTypeID = 2
	)
	BEGIN
	
		/*We have some customer detail value fields, create a customerdetailvalues section*/
		INSERT INTO DataLoaderMapSection (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderObjectActionID, DataLoaderSectionLocaterTypeID, DetailFieldSubTypeID, HasAquariumID, NumberOfFooterRowsToSkip, ImportIntoLeadManager, IsFixedLengthSection, FixedLengthNumberOfRows, IsMandatory, IsMultipleAllowedWithinFile, Notes, TableDetailFieldID, ResourceListDetailFieldID)
		VALUES (@ClientID, @DataLoaderMapID, 6, 1, 2, NULL, 0, 0, 0, 0, 0, 0, 0, '', NULL, NULL)
		
		SELECT @CustomerDetailValuesSectionID = SCOPE_IDENTITY()
		
		INSERT INTO DataLoaderFieldDefinition (ClientID, DataLoaderMapID, DataLoaderObjectTypeID, DataLoaderMapSectionID, DataLoaderObjectFieldID, DetailFieldID, DetailFieldAlias, NamedValue, Keyword, DataLoaderKeywordMatchTypeID, RowRelativeToKeyword, ColRelativeToKeyword, SectionRelativeRow, SectionAbsoluteCol, ValidationRegex, Equation, IsMatchField, DecodeTypeID, DefaultLookupItemID, SourceDataLoaderFieldDefinitionID, Notes, AllowErrors)
		SELECT @ClientID, @DataLoaderMapID, 6, @CustomerDetailValuesSectionID, NULL, d.DetailFieldID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 0, CASE df.Lookup WHEN 1 THEN 3 ELSE NULL END, NULL, n.DataLoaderFieldID, '', 0
		FROM @DetailFields d 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = d.DetailFieldID
		INNER JOIN @NamesValues n ON n.NamedValue = d.ColumnName
		WHERE d.DetailFieldSubTypeID = 2
	
	END

	/*Set the IsMultipleAllowedWithinFile field (AKA Moved onto next section) to be the last section*/
	UPDATE DataLoaderMapSection
	SET IsMultipleAllowedWithinFile = 1
	WHERE DataLoaderMapSectionID = COALESCE(@MatterDetailValuesSectionID, @MatterSectionID, @CaseDetailValuesSectionID, @CaseSectionID, @LeadDetailValuesSectionID, @LeadSectionID, @CustomerDetailValuesSectionID, @CustomerSectionID)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateNewMap] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CreateNewMap] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CreateNewMap] TO [sp_executeall]
GO
