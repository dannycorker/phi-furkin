SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[AddClientQuestionnairesVisitedAndCompleted]

@ClientQuestionnaireID int,
@VisitedDate datetime,
@IPAddress nvarchar(50),
@ClientID int


As

insert ClientQuestionnairesVisitedAndCompleted (ClientQuestionnaireID, VisitedDate, IPAddress, ClientID)
values (@ClientQuestionnaireID, @VisitedDate, @IPAddress, @ClientID)


declare @ClientQuestionnairesVisitedAndCompletedID int

set @ClientQuestionnairesVisitedAndCompletedID = SCOPE_IDENTITY()

select ClientQuestionnairesVisitedAndCompletedID from ClientQuestionnairesVisitedAndCompleted where ClientQuestionnairesVisitedAndCompletedID = @ClientQuestionnairesVisitedAndCompletedID



GO
GRANT VIEW DEFINITION ON  [dbo].[AddClientQuestionnairesVisitedAndCompleted] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddClientQuestionnairesVisitedAndCompleted] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddClientQuestionnairesVisitedAndCompleted] TO [sp_executeall]
GO
