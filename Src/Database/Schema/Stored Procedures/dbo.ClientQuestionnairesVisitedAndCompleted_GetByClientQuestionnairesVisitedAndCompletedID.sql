SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ClientQuestionnairesVisitedAndCompleted table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnairesVisitedAndCompleted_GetByClientQuestionnairesVisitedAndCompletedID]
(

	@ClientQuestionnairesVisitedAndCompletedID int   
)
AS


				SELECT
					[ClientQuestionnairesVisitedAndCompletedID],
					[ClientQuestionnaireID],
					[VisitedDate],
					[IPAddress],
					[ClientID]
				FROM
					[dbo].[ClientQuestionnairesVisitedAndCompleted] WITH (NOLOCK) 
				WHERE
										[ClientQuestionnairesVisitedAndCompletedID] = @ClientQuestionnairesVisitedAndCompletedID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedAndCompleted_GetByClientQuestionnairesVisitedAndCompletedID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnairesVisitedAndCompleted_GetByClientQuestionnairesVisitedAndCompletedID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisitedAndCompleted_GetByClientQuestionnairesVisitedAndCompletedID] TO [sp_executeall]
GO
