SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Robin Hall
-- Create date: 2015-04-28
-- Description:	Policy Sales/Renewals
-- 2015-08-04 ROH Earlier (uncommented and undocumented) change to exclude created-in-error was excluding all approved claims 
-- =============================================
CREATE PROCEDURE [dbo].[_C600_MI_ClaimsProcessed]

AS
BEGIN

	--DECLARE 
	--	@MonthStart DATE
	--	,@MonthEnd DATE

	--SELECT @MonthEnd = DATEADD(DAY, -DATEPART(DAY, dbo.fn_GetDate_Local()), dbo.fn_GetDate_Local())
	--SELECT @MonthStart = DATEADD(DAY, 1-DATEPART(DAY, @MonthEnd), @MonthEnd)


	;WITH AssessmentEvents (MatterID, AssesedDate, Assessor, Outcome, RejectionReasonID)
		AS (
		SELECT 
			m.MatterID, convert(date, le.WhenCreated), cp.UserName 
			,CASE le.EventTypeID WHEN 121865 THEN 'Approved' WHEN 121869 THEN 'Rejected' END
			,CASE le.EventTypeID WHEN 121869 THEN mdvRejReason.ValueInt ELSE NULL END
		
		FROM 
			Matter m WITH (NOLOCK)
			INNER JOIN dbo.LeadEvent le WITH (NOLOCK) ON le.CaseID = m.CaseID AND le.EventDeleted = 0
			INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = le.WhoCreated
			LEFT JOIN MatterDetailValues mdvRejReason WITH (NOLOCK) on mdvRejReason.MatterID = m.MatterID and mdvRejReason.DetailFieldID = 146184
		WHERE 
			le.EventTypeID IN (121865, 121869)
			AND NOT EXISTS (
				SELECT 1 
				FROM LeadEvent le2 WITH (NOLOCK)
				WHERE le2.CaseID = le.CaseID 
				AND le2.EventTypeID IN (121865, 121869) 
				AND le2.EventDeleted = 0 
				AND le2.LeadEventID < le.LeadEventID
				)
		)
		
	,Policy AS (
		SELECT 
			ltr.ToLeadID ClaimLeadID
			, ltr.FromLeadID PolicyLeadID
			, ldvPetName.DetailValue PetName
			, rdvAff.DetailValue Affinity
			, ROW_NUMBER() OVER(PARTITION BY ltr.ToLeadID ORDER BY ltr.FromMatterID ASC ) rn
		FROM LeadTypeRelationship ltr WITH (NOLOCK)
		LEFT JOIN dbo.LeadDetailValues ldvPetName WITH (NOLOCK) on ldvPetName.LeadID = ltr.FromLeadID and ldvPetName.DetailFieldID = 144268
		LEFT JOIN dbo.MatterDetailValues mdvCurPol WITH (NOLOCK) on mdvCurPol.MatterID = ltr.FromMatterID and mdvCurPol.DetailFieldID = 170034
		LEFT JOIN dbo.ResourceListDetailValues rdvAff WITH (NOLOCK) on rdvAff.ResourceListID = mdvCurPol.ValueInt and rdvAff.DetailFieldID = 144314
	)
		
	SELECT 
		cu.Fullname, p.PetName, m.MatterID, ae.Outcome, ae.Assessor, ae.AssesedDate
		, DATEADD(DAY, 1 - DATEPART(DAY, ae.AssesedDate), ae.AssesedDate) MonthOfAssessment
		, p.Affinity
	FROM 
		Matter m WITH (NOLOCK)
		INNER JOIN dbo.Customers cu WITH (NOLOCK) ON cu.CustomerID = m.CustomerID
		INNER JOIN AssessmentEvents ae on ae.MatterID = m.MatterID
		INNER JOIN Policy p on p.ClaimLeadID = m.LeadID and p.rn = 1
		--LEFT JOIN dbo.LeadTypeRelationship ltr WITH (NOLOCK) on ltr.ToLeadID = m.LeadID and ltr.FromLeadTypeID = 1492
		--LEFT JOIN dbo.LeadDetailValues ldvPetName WITH (NOLOCK) on ldvPetName.LeadID = ltr.FromLeadID and ldvPetName.DetailFieldID = 144268
	WHERE 
		cu.Test = 0
		AND (ae.RejectionReasonID IS NULL OR ae.RejectionReasonID <> 53432) -- Exclude claims created in error
	ORDER BY 
		ae.AssesedDate, cu.LastName, cu.FirstName, m.MatterID
		
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_MI_ClaimsProcessed] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_MI_ClaimsProcessed] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_MI_ClaimsProcessed] TO [sp_executeall]
GO
