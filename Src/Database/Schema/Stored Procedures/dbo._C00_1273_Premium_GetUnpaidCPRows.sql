SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


 
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-06-12
-- Description:	Gets the premium details that are unpaid for collection
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Premium_GetUnpaidCPRows] 

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT *
	FROM dbo.fn_C384_GetUnpaidCPRows()
 
 
END
 
 
 


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Premium_GetUnpaidCPRows] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Premium_GetUnpaidCPRows] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Premium_GetUnpaidCPRows] TO [sp_executeall]
GO
