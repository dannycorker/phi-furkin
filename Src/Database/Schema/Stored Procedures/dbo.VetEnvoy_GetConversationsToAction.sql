SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-03-24
-- Description:	Retrieves the conversations for actioning
-- Modified:	2015-04-28	SB	Added a type flag for filtering of test and live
-- =============================================
CREATE PROCEDURE [dbo].[VetEnvoy_GetConversationsToAction]
(
	@ClientID INT,
	@Status VARCHAR(2000),
	@Type VARCHAR(100)
)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT h.HistoryID, h.ConversationID, c.ParentConversationID, h.Status, a.AccountID, a.Name, a.Username, a.Password, a.Url
	FROM dbo.VetEnvoy_ConversationHistory h WITH (NOLOCK) 
	INNER JOIN dbo.VetEnvoy_Conversations c WITH (NOLOCK) ON h.ConversationPK = c.ConversationPK
	INNER JOIN dbo.VetEnvoy_Accounts a WITH (NOLOCK) ON c.AccountID = a.AccountID
	WHERE a.Enabled = 1
	AND a.AccountType = @Type
	AND h.ClientID = @ClientID
	AND h.Actioned IS NULL
	AND h.Status IN
	(
		SELECT AnyID
		FROM dbo.fnTableOfIDsFromCSV(@Status)
	)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_GetConversationsToAction] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VetEnvoy_GetConversationsToAction] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VetEnvoy_GetConversationsToAction] TO [sp_executeall]
GO
