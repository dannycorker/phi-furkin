SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-12-04
-- Description:	Add a note and force SAN
-- =============================================
CREATE PROCEDURE [dbo].[_C600_AddANote] 
(
	@CaseID INT, 
	@NoteTypeID INT,
	@NoteText VARCHAR(2000),
	@Status INT = 1,
	@WhoCreated INT = NULL
)

AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @NoteLeadEventID INT,
			@AllowMultiple INT = 1 

	SELECT @AllowMultiple = CASE WHEN amn.ValueInt = 5144 THEN 1 ELSE 0 END
	FROM ResourceListDetailValues ntid WITH (NOLOCK) 
	INNER JOIN ResourceListDetailValues amn WITH (NOLOCK) ON ntid.ResourceListID=amn.ResourceListID AND amn.DetailFieldID=176939
	WHERE ntid.DetailFieldID=176936 AND ntid.ValueInt=@NoteTypeID
	
	SELECT @WhoCreated=CASE WHEN @WhoCreated IS NULL THEN dbo.fnGetKeyValueAsIntFromThirdPartyIDs (nt.ClientID,53,'CFG|AqAutomationCPID',0) ELSE @WhoCreated END 
	FROM NoteType nt WITH (NOLOCK) 
	WHERE nt.NoteTypeID=@NoteTypeID

	IF @AllowMultiple = 0 
		AND 
		0 < ( SELECT COUNT(*) FROM LeadEvent oth WHERE oth.EventDeleted=0 
				AND oth.NotePriority=@Status 
				AND oth.NoteTypeID=@NoteTypeID 
				AND oth.CaseID=@CaseID ) 
	BEGIN

		RETURN
	
	END
	
	EXEC _C00_AddANote @CaseID, @NoteTypeID, @NoteText, @Status, @WhoCreated
	
	SELECT @NoteLeadEventID=LeadEventID 
	FROM LeadEvent WITH (NOLOCK) 
	WHERE CaseID=@CaseID AND NoteTypeID=@NoteTypeID 
	ORDER BY LeadEventID DESC

	EXEC _C600_SAN @NoteLeadEventID
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_AddANote] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_AddANote] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_AddANote] TO [sp_executeall]
GO
