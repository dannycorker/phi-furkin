SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ReminderTimeshift table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReminderTimeshift_Find]
(

	@SearchUsingOR bit   = null ,

	@ReminderTimeshiftID int   = null ,

	@ReminderTimeshiftName varchar (50)  = null ,

	@TimeUnitsID int   = null ,

	@TimeUnitsQuantity int   = null ,

	@DisplayOrder int   = null ,

	@TimeUnitsQuantityInMinutes int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ReminderTimeshiftID]
	, [ReminderTimeshiftName]
	, [TimeUnitsID]
	, [TimeUnitsQuantity]
	, [DisplayOrder]
	, [TimeUnitsQuantityInMinutes]
    FROM
	[dbo].[ReminderTimeshift] WITH (NOLOCK) 
    WHERE 
	 ([ReminderTimeshiftID] = @ReminderTimeshiftID OR @ReminderTimeshiftID IS NULL)
	AND ([ReminderTimeshiftName] = @ReminderTimeshiftName OR @ReminderTimeshiftName IS NULL)
	AND ([TimeUnitsID] = @TimeUnitsID OR @TimeUnitsID IS NULL)
	AND ([TimeUnitsQuantity] = @TimeUnitsQuantity OR @TimeUnitsQuantity IS NULL)
	AND ([DisplayOrder] = @DisplayOrder OR @DisplayOrder IS NULL)
	AND ([TimeUnitsQuantityInMinutes] = @TimeUnitsQuantityInMinutes OR @TimeUnitsQuantityInMinutes IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ReminderTimeshiftID]
	, [ReminderTimeshiftName]
	, [TimeUnitsID]
	, [TimeUnitsQuantity]
	, [DisplayOrder]
	, [TimeUnitsQuantityInMinutes]
    FROM
	[dbo].[ReminderTimeshift] WITH (NOLOCK) 
    WHERE 
	 ([ReminderTimeshiftID] = @ReminderTimeshiftID AND @ReminderTimeshiftID is not null)
	OR ([ReminderTimeshiftName] = @ReminderTimeshiftName AND @ReminderTimeshiftName is not null)
	OR ([TimeUnitsID] = @TimeUnitsID AND @TimeUnitsID is not null)
	OR ([TimeUnitsQuantity] = @TimeUnitsQuantity AND @TimeUnitsQuantity is not null)
	OR ([DisplayOrder] = @DisplayOrder AND @DisplayOrder is not null)
	OR ([TimeUnitsQuantityInMinutes] = @TimeUnitsQuantityInMinutes AND @TimeUnitsQuantityInMinutes is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ReminderTimeshift_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReminderTimeshift_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReminderTimeshift_Find] TO [sp_executeall]
GO
