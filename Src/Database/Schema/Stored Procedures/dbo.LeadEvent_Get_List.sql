SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the LeadEvent table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadEvent_Get_List]

AS


				
				SELECT
					[LeadEventID],
					[ClientID],
					[LeadID],
					[WhenCreated],
					[WhoCreated],
					[Cost],
					[Comments],
					[EventTypeID],
					[NoteTypeID],
					[FollowupDateTime],
					[WhenFollowedUp],
					[AquariumEventType],
					[NextEventID],
					[CaseID],
					[LeadDocumentID],
					[NotePriority],
					[DocumentQueueID],
					[EventDeleted],
					[WhoDeleted],
					[DeletionComments],
					[ContactID],
					[BaseCost],
					[DisbursementCost],
					[DisbursementDescription],
					[ChargeOutRate],
					[UnitsOfEffort],
					[CostEnteredManually],
					[IsOnHold],
					[HoldLeadEventID]
				FROM
					[dbo].[LeadEvent] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadEvent_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadEvent_Get_List] TO [sp_executeall]
GO
