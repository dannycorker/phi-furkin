SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ClientQuestionnaireLayout table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnaireLayout_Get_List]

AS


				
				SELECT
					[ClientQuestionnaireLayoutID],
					[ClientQuestionnaireID],
					[ClientID],
					[PageNumber],
					[LayoutCss],
					[LayoutCssFileName],
					[QuestionColumns]
				FROM
					[dbo].[ClientQuestionnaireLayout] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaireLayout_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnaireLayout_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnaireLayout_Get_List] TO [sp_executeall]
GO
