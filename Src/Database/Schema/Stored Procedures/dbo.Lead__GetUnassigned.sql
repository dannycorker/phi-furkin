SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 20091030
-- Description:	Gets a list of Unassigned Leads
-- =============================================
CREATE PROCEDURE [dbo].[Lead__GetUnassigned]
	@FromRow int,
	@ToRow int,
	@ClientID int,
	@LeadTypeID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    WITH results as (
	SELECT TOP 100 PERCENT
	l.LeadID,
	l.CustomerID,
	l.LeadTypeID,
	c.Fullname,
	lt.LeadTypeDescription,
	CASE WHEN cq.SubmissionDate IS NULL THEN 'Manual Lead'
	ELSE CONVERT(varchar,cq.SubmissionDate,121)
	END AS SubmissionDate,
	ROW_NUMBER() OVER (ORDER BY l.LeadID) as RN
	FROM Lead l (NOLOCK)
	INNER JOIN Customers c (NOLOCK) ON c.CustomerID = l.CustomerID
	INNER JOIN LeadType lt (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID
	LEFT JOIN CustomerQuestionnaires cq (NOLOCK) ON cq.CustomerID = l.CustomerID
	WHERE l.ClientID = @ClientID
	AND (l.Assigned = 'false' OR l.Assigned IS NULL)
	AND l.LeadTypeID = @LeadTypeID
	ORDER BY LeadID
)
SELECT * FROM results --WHERE results.RN BETWEEN @FromRow and @ToRow
END




GO
GRANT VIEW DEFINITION ON  [dbo].[Lead__GetUnassigned] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead__GetUnassigned] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead__GetUnassigned] TO [sp_executeall]
GO
