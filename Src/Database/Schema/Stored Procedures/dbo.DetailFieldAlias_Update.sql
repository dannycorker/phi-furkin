SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DetailFieldAlias table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldAlias_Update]
(

	@DetailFieldAliasID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@DetailFieldID int   ,

	@DetailFieldAlias varchar (500)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DetailFieldAlias]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[DetailFieldID] = @DetailFieldID
					,[DetailFieldAlias] = @DetailFieldAlias
				WHERE
[DetailFieldAliasID] = @DetailFieldAliasID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldAlias_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias_Update] TO [sp_executeall]
GO
