SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Partner table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Partner_Get_List]

AS


				
				SELECT
					[PartnerID],
					[CustomerID],
					[ClientID],
					[UseCustomerAddress],
					[TitleID],
					[FirstName],
					[MiddleName],
					[LastName],
					[EmailAddress],
					[DayTimeTelephoneNumber],
					[HomeTelephone],
					[MobileTelephone],
					[Address1],
					[Address2],
					[Town],
					[County],
					[PostCode],
					[Occupation],
					[Employer],
					[DateOfBirth],
					[FullName],
					[CountryID],
					[WhenModified],
					[WhoModified],
					[Longitude],
					[Latitude]
				FROM
					[dbo].[Partner] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Partner_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Partner_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Partner_Get_List] TO [sp_executeall]
GO
