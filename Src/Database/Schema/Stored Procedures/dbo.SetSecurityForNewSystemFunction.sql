SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2007-11-20
-- Description:	Create default Rights for new system objects
-- that Aquarium Software have invented (B_ADD_BLANK_CASE etc)
-- =============================================
CREATE PROCEDURE [dbo].[SetSecurityForNewSystemFunction]
	-- Add the parameters for the stored procedure here
	@FunctionID int,
	@Level int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRAN

	-- If a new button, menu option or whatever has just been added, 
	-- then look for any Groups with a GroupFunctionControl record of that type 9 where HasDescendants = 1.  
	-- For each record found, add a new GroupRightsDynamic record for this new object.
	-- Then do it all again for individual User rights.
	BEGIN

		-- Groups first
		INSERT INTO GroupRightsDynamic (
			ClientPersonnelAdminGroupID,
			FunctionTypeID,
			LeadTypeID,
			ObjectID,
			RightID
		)
		SELECT 
			gfc.ClientPersonnelAdminGroupID,
			Functions.FunctionTypeID, 
			NULL, 
			@FunctionID, 
			@Level
		FROM Functions 
		INNER JOIN GroupFunctionControl gfc ON gfc.FunctionTypeID = Functions.FunctionTypeID 
		WHERE Functions.FunctionID = @FunctionID 
		AND gfc.LeadTypeID IS NULL 
		AND gfc.HasDescendants = 1 

		-- Ditto for users
		INSERT INTO UserRightsDynamic (
			ClientPersonnelID,
			FunctionTypeID,
			LeadTypeID,
			ObjectID,
			RightID
		)
		SELECT 
			ufc.ClientPersonnelID,
			Functions.FunctionTypeID, 
			NULL,  
			@FunctionID, 
			@Level
		FROM Functions 
		INNER JOIN UserFunctionControl ufc ON ufc.FunctionTypeID = Functions.FunctionTypeID 
		WHERE Functions.FunctionID = @FunctionID 
		AND ufc.LeadTypeID IS NULL 
		AND ufc.HasDescendants = 1 

	END

	COMMIT
END











GO
GRANT VIEW DEFINITION ON  [dbo].[SetSecurityForNewSystemFunction] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SetSecurityForNewSystemFunction] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SetSecurityForNewSystemFunction] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[SetSecurityForNewSystemFunction] TO [sp_executehelper]
GO
