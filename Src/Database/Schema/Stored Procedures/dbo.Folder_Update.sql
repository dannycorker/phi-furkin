SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Folder table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Folder_Update]
(

	@FolderID int   ,

	@ClientID int   ,

	@FolderTypeID int   ,

	@FolderParentID int   ,

	@FolderName varchar (50)  ,

	@FolderDescription varchar (250)  ,

	@WhenCreated datetime   ,

	@WhoCreated int   ,

	@Personal bit   ,

	@SourceID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Folder]
				SET
					[ClientID] = @ClientID
					,[FolderTypeID] = @FolderTypeID
					,[FolderParentID] = @FolderParentID
					,[FolderName] = @FolderName
					,[FolderDescription] = @FolderDescription
					,[WhenCreated] = @WhenCreated
					,[WhoCreated] = @WhoCreated
					,[Personal] = @Personal
					,[SourceID] = @SourceID
				WHERE
[FolderID] = @FolderID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Folder_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Folder_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Folder_Update] TO [sp_executeall]
GO
