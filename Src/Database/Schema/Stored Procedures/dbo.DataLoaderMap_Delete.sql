SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DataLoaderMap table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderMap_Delete]
(

	@DataLoaderMapID int   
)
AS


				DELETE FROM [dbo].[DataLoaderMap] WITH (ROWLOCK) 
				WHERE
					[DataLoaderMapID] = @DataLoaderMapID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMap_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMap_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMap_Delete] TO [sp_executeall]
GO
