SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 08-01-2013
-- Description:	Gets a record in PartnerMatchKey by the partnerID
-- =============================================
CREATE PROCEDURE [dbo].[PartnerMatchKey__GetByPartnerID]
	
	@ClientID INT,
	@PartnerID INT

AS
BEGIN

	SELECT * FROM PartnerMatchKey WITH (NOLOCK) 
	WHERE PartnerID = @PartnerID AND ClientID = @ClientID
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[PartnerMatchKey__GetByPartnerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PartnerMatchKey__GetByPartnerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PartnerMatchKey__GetByPartnerID] TO [sp_executeall]
GO
