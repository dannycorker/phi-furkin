SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the Cases table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Cases_Find]
(

	@SearchUsingOR bit   = null ,

	@CaseID int   = null ,

	@LeadID int   = null ,

	@ClientID int   = null ,

	@CaseNum int   = null ,

	@CaseRef varchar (250)  = null ,

	@ClientStatusID int   = null ,

	@AquariumStatusID int   = null ,

	@DefaultContactID int   = null ,

	@LatestLeadEventID int   = null ,

	@LatestInProcessLeadEventID int   = null ,

	@LatestOutOfProcessLeadEventID int   = null ,

	@LatestNonNoteLeadEventID int   = null ,

	@LatestNoteLeadEventID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@ProcessStartLeadEventID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [CaseID]
	, [LeadID]
	, [ClientID]
	, [CaseNum]
	, [CaseRef]
	, [ClientStatusID]
	, [AquariumStatusID]
	, [DefaultContactID]
	, [LatestLeadEventID]
	, [LatestInProcessLeadEventID]
	, [LatestOutOfProcessLeadEventID]
	, [LatestNonNoteLeadEventID]
	, [LatestNoteLeadEventID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [ProcessStartLeadEventID]
    FROM
	[dbo].[Cases] WITH (NOLOCK) 
    WHERE 
	 ([CaseID] = @CaseID OR @CaseID IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([CaseNum] = @CaseNum OR @CaseNum IS NULL)
	AND ([CaseRef] = @CaseRef OR @CaseRef IS NULL)
	AND ([ClientStatusID] = @ClientStatusID OR @ClientStatusID IS NULL)
	AND ([AquariumStatusID] = @AquariumStatusID OR @AquariumStatusID IS NULL)
	AND ([DefaultContactID] = @DefaultContactID OR @DefaultContactID IS NULL)
	AND ([LatestLeadEventID] = @LatestLeadEventID OR @LatestLeadEventID IS NULL)
	AND ([LatestInProcessLeadEventID] = @LatestInProcessLeadEventID OR @LatestInProcessLeadEventID IS NULL)
	AND ([LatestOutOfProcessLeadEventID] = @LatestOutOfProcessLeadEventID OR @LatestOutOfProcessLeadEventID IS NULL)
	AND ([LatestNonNoteLeadEventID] = @LatestNonNoteLeadEventID OR @LatestNonNoteLeadEventID IS NULL)
	AND ([LatestNoteLeadEventID] = @LatestNoteLeadEventID OR @LatestNoteLeadEventID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([ProcessStartLeadEventID] = @ProcessStartLeadEventID OR @ProcessStartLeadEventID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [CaseID]
	, [LeadID]
	, [ClientID]
	, [CaseNum]
	, [CaseRef]
	, [ClientStatusID]
	, [AquariumStatusID]
	, [DefaultContactID]
	, [LatestLeadEventID]
	, [LatestInProcessLeadEventID]
	, [LatestOutOfProcessLeadEventID]
	, [LatestNonNoteLeadEventID]
	, [LatestNoteLeadEventID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [ProcessStartLeadEventID]
    FROM
	[dbo].[Cases] WITH (NOLOCK) 
    WHERE 
	 ([CaseID] = @CaseID AND @CaseID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([CaseNum] = @CaseNum AND @CaseNum is not null)
	OR ([CaseRef] = @CaseRef AND @CaseRef is not null)
	OR ([ClientStatusID] = @ClientStatusID AND @ClientStatusID is not null)
	OR ([AquariumStatusID] = @AquariumStatusID AND @AquariumStatusID is not null)
	OR ([DefaultContactID] = @DefaultContactID AND @DefaultContactID is not null)
	OR ([LatestLeadEventID] = @LatestLeadEventID AND @LatestLeadEventID is not null)
	OR ([LatestInProcessLeadEventID] = @LatestInProcessLeadEventID AND @LatestInProcessLeadEventID is not null)
	OR ([LatestOutOfProcessLeadEventID] = @LatestOutOfProcessLeadEventID AND @LatestOutOfProcessLeadEventID is not null)
	OR ([LatestNonNoteLeadEventID] = @LatestNonNoteLeadEventID AND @LatestNonNoteLeadEventID is not null)
	OR ([LatestNoteLeadEventID] = @LatestNoteLeadEventID AND @LatestNoteLeadEventID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([ProcessStartLeadEventID] = @ProcessStartLeadEventID AND @ProcessStartLeadEventID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[Cases_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Cases_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Cases_Find] TO [sp_executeall]
GO
