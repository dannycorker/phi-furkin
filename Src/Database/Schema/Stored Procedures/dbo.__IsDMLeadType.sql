SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2012-12-11
-- Description:	Quick fix to recalc all matters for debt management leads, but only the current record for all other types.
-- =============================================
CREATE PROCEDURE [dbo].[__IsDMLeadType]
	@LeadTypeID INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT CASE WHEN @LeadTypeID IN (657 , 700 , 734 , 833 , 956 , 979 , 1011 ,1093, 1238 , 1310 , 1313, 1323, 1402) THEN 1 ELSE 0 END AS [IsDmLeadType]
END



GO
GRANT VIEW DEFINITION ON  [dbo].[__IsDMLeadType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__IsDMLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__IsDMLeadType] TO [sp_executeall]
GO
