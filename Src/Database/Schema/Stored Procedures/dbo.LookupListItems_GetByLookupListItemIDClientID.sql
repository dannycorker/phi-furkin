SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the LookupListItems table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LookupListItems_GetByLookupListItemIDClientID]
(

	@LookupListItemID int   ,

	@ClientID int   
)
AS


				SELECT
					[LookupListItemID],
					[LookupListID],
					[ItemValue],
					[ClientID],
					[Enabled],
					[SortOrder],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[SourceID],
					[IsShared]
				FROM
					dbo.fnLookupListItemsShared(@ClientID)
				WHERE
										[LookupListItemID] = @LookupListItemID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_GetByLookupListItemIDClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LookupListItems_GetByLookupListItemIDClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LookupListItems_GetByLookupListItemIDClientID] TO [sp_executeall]
GO
