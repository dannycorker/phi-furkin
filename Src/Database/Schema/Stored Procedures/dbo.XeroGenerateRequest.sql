SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Ian Slack
-- Create date: 2011-07-27
-- Altered:		2013-10-28 : formated unitamount
-- =============================================
CREATE PROCEDURE [dbo].[XeroGenerateRequest] 
(
	@QueryID int
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE	
		@ClientID int,
		@XMLContainerName varchar(100),
		@GeneratedXML xml
		
	SELECT
		@ClientID = ClientID,
		@XMLContainerName = t.XMLContainerName
	FROM dbo.ThirdPartyMappingQuery q WITH (NOLOCK) 
	INNER JOIN dbo.ThirdPartyMappingTemplate t WITH (NOLOCK) ON q.ThirdPartySystemID = t.ThirdPartySystemID AND q.ThirdPartyFieldGroupID = t.ThirdPartyFieldGroupID
	WHERE QueryID = @QueryID

	IF @XMLContainerName = 'Invoices'
	BEGIN
		WITH Invoices AS
		(
			SELECT
				CustomerID, 
				TableRowID,
				MAX([134769]) LeadType,
				MAX([134770]) MatterID,       
				MAX([134771]) InvoiceNumber,
				MAX([134772]) ContactName,
				MAX([134773]) Date,
				MAX([134774]) DueDate,
				MAX([134775]) Description,
				MAX([134776]) AccountCode, 
				MAX([134777]) Amount,
				MAX([134778]) Status,
				MAX([134779]) AmountDue,    
				MAX([134780]) AmountPaid,
				MAX([134781]) InvoiceID,
				MAX([134782]) UpdateDateUTC,
				'ACCREC' Type -- ISNULL(MAX([135423]), 'ACCREC') Type
			FROM
			(
				SELECT	DetailFieldID, RTRIM(ISNULL(DetailValue,'')) DetailValue, CustomerID, TableRowID
				FROM	TableDetailValues --WITH (NOLOCK) /*CS and IS for ticket 15773*/
				where	ClientID = @ClientID
				and		DetailFieldID in 
				(134769,          
				134770,       
				134771,
				134772,
				134773,
				134774,
				134775,
				134776, 
				134777,
				134778,
				134779,    
				134780,
				134781,
				134782,
				135423)
			) AS SourceTable
			PIVOT
			(
				MAX(DetailValue)
				FOR DetailFieldID IN 
				(
					[134769],          
					[134770],       
					[134771],
					[134772],
					[134773],
					[134774],
					[134775],
					[134776], 
					[134777],
					[134778],
					[134779],    
					[134780],
					[134781],
					[134782],
					[135423]
				)
			) AS PivotTable
			GROUP BY CustomerID, TableRowID
			HAVING 
				MAX([134770]) <> ''
			AND
				MAX([134772]) <> ''
			AND
				MAX([134781]) = ''
		)
		SELECT @GeneratedXML = 
		(
			SELECT 
				LeadType+'_'+cast(TableRowID as varchar(50))+'_'+cast(CustomerID as varchar(50))+'_'+cast(MatterID as varchar(50)) Reference,
				InvoiceNumber,
				(SELECT ContactName Name FROM Invoices _inner where _inner.TableRowID = _outer.TableRowID FOR XML PATH('Contact'), TYPE ),
				CONVERT(VARCHAR(23), CAST(Date as Datetime), 126) Date,
				CONVERT(VARCHAR(23), CAST(Date as Datetime), 126) DueDate,
				CASE WHEN LeadType = 'PI PO' THEN 'NoTax' ELSE 'Inclusive' END LineAmountTypes,
				Type, --'ACCREC' Type,
				'DRAFT' Status,
				(SELECT 
					Description,
					1 Quantity,
					CAST(Amount AS Money) UnitAmount,
					AccountCode
				 FROM Invoices _inner where _inner.TableRowID = _outer.TableRowID 
				 FOR XML PATH('LineItem'), ROOT('LineItems'), TYPE )
			FROM Invoices _outer
			WHERE dbo.fnIsDateTime([date]) = 1
			FOR XML PATH('Invoice'), ROOT('Invoices')
		)
	END
	
	IF @XMLContainerName = 'Payments'
	BEGIN
		WITH Invoices AS
		(
			SELECT
				CustomerID, 
				TableRowID,
				MAX([134769]) LeadType,
				MAX([134770]) MatterID,       
				MAX([134771]) InvoiceNumber,
				MAX([134772]) ContactName,
				MAX([134773]) Date,
				MAX([134774]) DueDate,
				MAX([134775]) Description,
				MAX([134776]) AccountCode, 
				MAX([134777]) Amount,
				MAX([134778]) Status,
				MAX([134779]) AmountDue,    
				MAX([134780]) AmountPaid,
				MAX([134781]) InvoiceID,
				MAX([134782]) UpdateDateUTC,
				'ACCREC' Type
			FROM
			(
				SELECT	DetailFieldID, RTRIM(ISNULL(DetailValue,'')) DetailValue, CustomerID, TableRowID
				FROM	TableDetailValues WITH (NOLOCK) 
				where	ClientID = @ClientID
				and		DetailFieldID in
				(134769,          
				134770,       
				134771,
				134772,
				134773,
				134774,
				134775,
				134776, 
				134777,
				134778,
				134779,    
				134780,
				134781,
				134782,
				135423)
			) AS SourceTable
			PIVOT
			(
				MAX(DetailValue)
				FOR DetailFieldID IN 
				(
					[134769],          
					[134770],       
					[134771],
					[134772],
					[134773],
					[134774],
					[134775],
					[134776], 
					[134777],
					[134778],
					[134779],    
					[134780],
					[134781],
					[134782],
					[135423]
				)
			) AS PivotTable
			GROUP BY CustomerID, TableRowID
			HAVING MAX([134781]) <> ''
			AND  MAX([134778]) = 'AUTHORISED'
		),
		Payments AS
		(
			SELECT
				CustomerID,
				TableRowID,
				MAX([134842]) LeadType,
				MAX([134843]) MatterID,
				MAX([134845]) InvoiceNumber,
				MAX([134846]) AccountCode,
				MAX([134847]) Date,
				MAX([134848]) Amount
			FROM
			(
				SELECT	DetailFieldID, RTRIM(ISNULL(DetailValue,'')) DetailValue, CustomerID, TableRowID
				FROM	TableDetailValues WITH (NOLOCK) 
				where	ClientID = @ClientID
				and		DetailFieldID in
				(134842,
				134843,
				134844,
				134845,
				134846,
				134847,
				134848,
				134849)
			) AS SourceTable
			PIVOT
			(
				MAX(DetailValue)
				FOR DetailFieldID IN 
				(
					[134842],
					[134843],
					[134844],
					[134845],
					[134846],
					[134847],
					[134848],
					[134849]
				)
			) AS PivotTable
			GROUP BY CustomerID, TableRowID
			HAVING 
				MAX([134843]) <> ''
			AND
				isnull(MAX([134849]),'') = ''
		)
		SELECT @GeneratedXML = 
		(
			SELECT 
				pay.LeadType+'_'+cast(pay.TableRowID as varchar(50))+'_'+cast(pay.CustomerID as varchar(50))+'_'+cast(pay.MatterID as varchar(50)) Reference,
				(SELECT InvoiceNumber FROM Payments _inner where _inner.TableRowID = pay.TableRowID FOR XML PATH('Invoice'), TYPE ),
				(SELECT AccountCode Code FROM Payments _inner where _inner.TableRowID = pay.TableRowID FOR XML PATH('Account'), TYPE ),
				CONVERT(VARCHAR(23), CAST(pay.Date as Datetime), 126) Date,
				CAST(pay.Amount AS Money) Amount
			FROM Payments pay
			INNER JOIN Invoices inv 
				ON inv.InvoiceNumber = pay.InvoiceNumber
					AND inv.AmountDue <> '0.00' 
					AND inv.Status = 'AUTHORISED'
			WHERE dbo.fnIsDateTime(pay.[date]) = 1
			FOR XML PATH('Payment'), ROOT('Payments')
		)
	END
	
	IF @XMLContainerName = 'CreditNotes'
	BEGIN
		WITH CreditNotes AS
		(
			SELECT
				CustomerID, 
				TableRowID,
				MAX([134850]) LeadType,
				MAX([134851]) MatterID,       
				MAX([134852]) CreditNoteNumber,
				MAX([134853]) ContactName,
				MAX([134855]) Date,
				MAX([134857]) Description,
				MAX([134854]) AccountCode, 
				MAX([134856]) Amount,
				MAX([134858]) Status,
				MAX([134860]) CreditNoteID,
				MAX([134859]) UpdateDateUTC,
				'ACCRECCREDIT' Type --ISNULL(MAX([135425]), 'ACCRECCREDIT') Type
			FROM
			(
				SELECT	DetailFieldID, RTRIM(ISNULL(DetailValue,'')) DetailValue, CustomerID, TableRowID
				FROM	TableDetailValues --WITH (NOLOCK)  /*CS and IS for ticket 15773*/
				where	ClientID = @ClientID
				and		DetailFieldID in
				(134850,
				134851,       
				134852,
				134853,
				134855,
				134857,
				134854, 
				134856,
				134858,
				134860,
				134859,
				135425)
			) AS SourceTable
			PIVOT
			(
				MAX(DetailValue)
				FOR DetailFieldID IN 
				(
					[134850],
					[134851],       
					[134852],
					[134853],
					[134855],
					[134857],
					[134854], 
					[134856],
					[134858],
					[134860],
					[134859],
					[135425]
				)
			) AS PivotTable
			GROUP BY CustomerID, TableRowID
			HAVING 
				MAX([134851]) <> ''
			AND
				isnull(MAX([134860]),'') = ''
			AND
				MAX([134850]) IN ('PI', 'PPI', 'PI PO', 'PPI DC')
		)
		SELECT @GeneratedXML = 
		(
			SELECT 
				LeadType+'_'+cast(TableRowID as varchar(50))+'_'+cast(CustomerID as varchar(50))+'_'+cast(MatterID as varchar(50)) Reference,
				CreditNoteNumber,
				(SELECT ContactName Name FROM CreditNotes _inner where _inner.TableRowID = _outer.TableRowID FOR XML PATH('Contact'), TYPE ),
				CONVERT(VARCHAR(23), CAST(Date as Datetime), 126) Date,
				CASE WHEN LeadType = 'PI PO' THEN 'NoTax' ELSE 'Inclusive' END LineAmountTypes,
				'ACCRECCREDIT' Type, -- --'ACCRECCREDIT' Type,
				'DRAFT' Status,
				(SELECT 
					Description,
					1 Quantity,
					CAST(Amount AS Money) UnitAmount,
					AccountCode
				 FROM CreditNotes _inner where _inner.TableRowID = _outer.TableRowID 
				 FOR XML PATH('LineItem'), ROOT('LineItems'), TYPE )
			FROM CreditNotes _outer
			WHERE dbo.fnIsDateTime([date]) = 1
			FOR XML PATH('CreditNote'), ROOT('CreditNotes')
		)
	END

	exec dbo.LogXML__AddEntry @ClientID, @QueryID, 'XeroGenerateRequest', @GeneratedXML
	
	SELECT @GeneratedXML

END


GO
GRANT VIEW DEFINITION ON  [dbo].[XeroGenerateRequest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[XeroGenerateRequest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[XeroGenerateRequest] TO [sp_executeall]
GO
