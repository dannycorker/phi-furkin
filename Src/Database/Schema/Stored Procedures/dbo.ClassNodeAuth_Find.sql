SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ClassNodeAuth table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClassNodeAuth_Find]
(

	@SearchUsingOR bit   = null ,

	@ClassNodeAuthID int   = null ,

	@ClientID int   = null ,

	@ClassNodeID int   = null ,

	@ClientPersonnelAdminGroupID int   = null ,

	@RightsLevel int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null ,

	@Deleted bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ClassNodeAuthID]
	, [ClientID]
	, [ClassNodeID]
	, [ClientPersonnelAdminGroupID]
	, [RightsLevel]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [Deleted]
    FROM
	[dbo].[ClassNodeAuth] WITH (NOLOCK) 
    WHERE 
	 ([ClassNodeAuthID] = @ClassNodeAuthID OR @ClassNodeAuthID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ClassNodeID] = @ClassNodeID OR @ClassNodeID IS NULL)
	AND ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID OR @ClientPersonnelAdminGroupID IS NULL)
	AND ([RightsLevel] = @RightsLevel OR @RightsLevel IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
	AND ([Deleted] = @Deleted OR @Deleted IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ClassNodeAuthID]
	, [ClientID]
	, [ClassNodeID]
	, [ClientPersonnelAdminGroupID]
	, [RightsLevel]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
	, [Deleted]
    FROM
	[dbo].[ClassNodeAuth] WITH (NOLOCK) 
    WHERE 
	 ([ClassNodeAuthID] = @ClassNodeAuthID AND @ClassNodeAuthID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ClassNodeID] = @ClassNodeID AND @ClassNodeID is not null)
	OR ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID AND @ClientPersonnelAdminGroupID is not null)
	OR ([RightsLevel] = @RightsLevel AND @RightsLevel is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	OR ([Deleted] = @Deleted AND @Deleted is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNodeAuth_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClassNodeAuth_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClassNodeAuth_Find] TO [sp_executeall]
GO
