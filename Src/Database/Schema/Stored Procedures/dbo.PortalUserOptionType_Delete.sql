SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the PortalUserOptionType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserOptionType_Delete]
(

	@PortalUserOptionTypeID int   
)
AS


				DELETE FROM [dbo].[PortalUserOptionType] WITH (ROWLOCK) 
				WHERE
					[PortalUserOptionTypeID] = @PortalUserOptionTypeID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOptionType_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserOptionType_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOptionType_Delete] TO [sp_executeall]
GO
