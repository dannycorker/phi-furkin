SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Clients table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Clients_Delete]
(

	@ClientID int   
)
AS


				DELETE FROM [dbo].[Clients] WITH (ROWLOCK) 
				WHERE
					[ClientID] = @ClientID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Clients_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Clients_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Clients_Delete] TO [sp_executeall]
GO
