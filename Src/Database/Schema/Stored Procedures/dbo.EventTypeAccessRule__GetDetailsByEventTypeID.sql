SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-07-27
-- Description:	Get short details for the EventTypeEdit page
-- =============================================
CREATE PROCEDURE [dbo].[EventTypeAccessRule__GetDetailsByEventTypeID]
	@EventTypeID int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT etar.EventTypeAccessRuleID,
	ar.AccessRuleID,
	ar.AccessRuleEnabled,
	ar.AccessRuleName
	FROM [dbo].[EventTypeAccessRule] etar WITH (NOLOCK) 
	INNER JOIN dbo.AccessRule ar WITH (NOLOCK) ON ar.AccessRuleID = etar.AccessRuleID 
	WHERE etar.EventTypeID = @EventTypeID 
				
END




GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAccessRule__GetDetailsByEventTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAccessRule__GetDetailsByEventTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAccessRule__GetDetailsByEventTypeID] TO [sp_executeall]
GO
