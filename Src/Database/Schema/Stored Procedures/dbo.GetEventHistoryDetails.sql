SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------
-- Date Created: 2015-02-12

-- Created By:  ACE
-- Purpose: Show the event history for a lead in order of priority and date
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[GetEventHistoryDetails]
(

	@LeadID int   ,
	@CaseID int = null,
	@ShowDeleted bit = 0,
	@ClientPersonnelID int = 0,
	@MinimumRightsRequired int = 1
)
AS
BEGIN

	DECLARE @ClientID INT,
			@UseEventCosts BIT = 0,
			@UseEventDisbursements BIT = 0
	
	SELECT @ClientID = ClientID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID

	IF EXISTS (

		SELECT *
		FROM
			dbo.LeadEvent le (nolock)
			INNER JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = le.EventTypeID
			LEFT JOIN dbo.fnNoteTypeSecure(@ClientPersonnelID, @MinimumRightsRequired) snt ON snt.ObjectID = le.NoteTypeID
		WHERE	le.LeadID = @LeadID
		AND		(le.CaseID = @CaseID OR @CaseID IS NULL) 
		AND
			CASE
				WHEN @ShowDeleted = 0 AND (le.EventDeleted = 0 OR le.EventDeleted is null) THEN 1
				WHEN @ShowDeleted = 1 THEN 1
				ELSE 0 
			END = 1
		AND et.UseEventCosts = 1
	)
	BEGIN
	
		SELECT @UseEventCosts = 1
	
	END

	IF EXISTS (

		SELECT *
		FROM
			dbo.LeadEvent le (nolock)
			INNER JOIN EventType et WITH (NOLOCK) ON et.EventTypeID = le.EventTypeID
			LEFT JOIN dbo.fnNoteTypeSecure(@ClientPersonnelID, @MinimumRightsRequired) snt ON snt.ObjectID = le.NoteTypeID
		WHERE	le.LeadID = @LeadID
		AND		(le.CaseID = @CaseID OR @CaseID IS NULL) 
		AND
			CASE
				WHEN @ShowDeleted = 0 AND (le.EventDeleted = 0 OR le.EventDeleted is null) THEN 1
				WHEN @ShowDeleted = 1 THEN 1
				ELSE 0 
			END = 1
		AND et.UseEventDisbursements = 1
	)
	BEGIN
	
		SELECT @UseEventDisbursements = 1
	
	END

	SELECT @UseEventCosts AS [UseEventCosts], @UseEventDisbursements AS [UseEventDisbursements]

END


GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventHistoryDetails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetEventHistoryDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetEventHistoryDetails] TO [sp_executeall]
GO
