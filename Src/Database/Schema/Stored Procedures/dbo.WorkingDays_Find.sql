SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the WorkingDays table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkingDays_Find]
(

	@SearchUsingOR bit   = null ,

	@WorkingDayID int   = null ,

	@Year int   = null ,

	@Month int   = null ,

	@Day int   = null ,

	@DayNumber int   = null ,

	@IsWorkDay bit   = null ,

	@IsWeekDay bit   = null ,

	@IsBankHoliday bit   = null ,

	@Date datetime   = null ,

	@WeekNumber tinyint   = null ,

	@DayNumberOfWeek tinyint   = null ,

	@CharDate char (10)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [WorkingDayID]
	, [Year]
	, [Month]
	, [Day]
	, [DayNumber]
	, [IsWorkDay]
	, [IsWeekDay]
	, [IsBankHoliday]
	, [Date]
	, [WeekNumber]
	, [DayNumberOfWeek]
	, [CharDate]
    FROM
	[dbo].[WorkingDays] WITH (NOLOCK) 
    WHERE 
	 ([WorkingDayID] = @WorkingDayID OR @WorkingDayID IS NULL)
	AND ([Year] = @Year OR @Year IS NULL)
	AND ([Month] = @Month OR @Month IS NULL)
	AND ([Day] = @Day OR @Day IS NULL)
	AND ([DayNumber] = @DayNumber OR @DayNumber IS NULL)
	AND ([IsWorkDay] = @IsWorkDay OR @IsWorkDay IS NULL)
	AND ([IsWeekDay] = @IsWeekDay OR @IsWeekDay IS NULL)
	AND ([IsBankHoliday] = @IsBankHoliday OR @IsBankHoliday IS NULL)
	AND ([Date] = @Date OR @Date IS NULL)
	AND ([WeekNumber] = @WeekNumber OR @WeekNumber IS NULL)
	AND ([DayNumberOfWeek] = @DayNumberOfWeek OR @DayNumberOfWeek IS NULL)
	AND ([CharDate] = @CharDate OR @CharDate IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [WorkingDayID]
	, [Year]
	, [Month]
	, [Day]
	, [DayNumber]
	, [IsWorkDay]
	, [IsWeekDay]
	, [IsBankHoliday]
	, [Date]
	, [WeekNumber]
	, [DayNumberOfWeek]
	, [CharDate]
    FROM
	[dbo].[WorkingDays] WITH (NOLOCK) 
    WHERE 
	 ([WorkingDayID] = @WorkingDayID AND @WorkingDayID is not null)
	OR ([Year] = @Year AND @Year is not null)
	OR ([Month] = @Month AND @Month is not null)
	OR ([Day] = @Day AND @Day is not null)
	OR ([DayNumber] = @DayNumber AND @DayNumber is not null)
	OR ([IsWorkDay] = @IsWorkDay AND @IsWorkDay is not null)
	OR ([IsWeekDay] = @IsWeekDay AND @IsWeekDay is not null)
	OR ([IsBankHoliday] = @IsBankHoliday AND @IsBankHoliday is not null)
	OR ([Date] = @Date AND @Date is not null)
	OR ([WeekNumber] = @WeekNumber AND @WeekNumber is not null)
	OR ([DayNumberOfWeek] = @DayNumberOfWeek AND @DayNumberOfWeek is not null)
	OR ([CharDate] = @CharDate AND @CharDate is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkingDays_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkingDays_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkingDays_Find] TO [sp_executeall]
GO
