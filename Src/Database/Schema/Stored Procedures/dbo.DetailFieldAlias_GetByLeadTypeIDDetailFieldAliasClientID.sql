SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DetailFieldAlias table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldAlias_GetByLeadTypeIDDetailFieldAliasClientID]
(

	@LeadTypeID int   ,

	@DetailFieldAlias varchar (500)  ,

	@ClientID int   
)
AS
BEGIN

	SELECT
		[DetailFieldAliasID],
		[ClientID],
		[LeadTypeID],
		[DetailFieldID],
		[DetailFieldAlias]
	FROM
		[dbo].[DetailFieldAlias]
	WHERE
		(([LeadTypeID] = @LeadTypeID) OR ([LeadTypeID] IS NULL AND @LeadTypeID IS NULL))
		AND ltrim(rtrim([DetailFieldAlias])) = ltrim(rtrim(@DetailFieldAlias))
		AND [ClientID] = @ClientID
		
	SELECT @@ROWCOUNT
		
			
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias_GetByLeadTypeIDDetailFieldAliasClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldAlias_GetByLeadTypeIDDetailFieldAliasClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldAlias_GetByLeadTypeIDDetailFieldAliasClientID] TO [sp_executeall]
GO
