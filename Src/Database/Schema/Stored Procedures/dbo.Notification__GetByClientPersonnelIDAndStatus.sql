SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 14-03-2012
-- Description:	Gets a list of notifications by clientpersonnelid and status
-- =============================================
CREATE PROCEDURE [dbo].[Notification__GetByClientPersonnelIDAndStatus]

	@ClientPersonnelID INT,
	@NotificationStatusID INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT	
			cpf.FirstName FromFirstName, cpf.MiddleName FromMiddleName, cpf.LastName FromLastName,
			nr.ClientPersonnelID,
			nr.NotificationRecipientID,
			nr.NotificationStatusID,			
			n.[NotificationID],
			n.[ClientID],
			n.[SubClientID],
			n.[ThirdPartySystemID],
			n.[Message],
			n.[DateMessageCreated],
			n.[MessageCreatedBy],
			n.[NotificationTypeID],
			nt.Name,
			nt.ForeColour,
			nt.BackgroundColour,
			ns.NotificationStatusID,
			ns.Status
	FROM [NOTIFICATION] n WITH (NOLOCK) 
	INNER JOIN dbo.NotificationRecipient nr WITH (NOLOCK) 
	ON nr.ClientPersonnelID = @ClientPersonnelID 
	AND nr.NotificationStatusID = @NotificationStatusID 
	AND nr.NotificationID = n.NotificationID
	INNER JOIN dbo.NotificationType nt WITH (NOLOCK) ON nt.NotificationTypeID = n.NotificationTypeID
	INNER JOIN dbo.NotificationStatus ns WITH (NOLOCK) ON ns.NotificationStatusID = nr.NotificationStatusID
	INNER JOIN dbo.ClientPersonnel cpf WITH (NOLOCK) On cpf.ClientPersonnelID = n.[MessageCreatedBy]
	WHERE nr.ClientPersonnelID = @ClientPersonnelID AND nr.NotificationStatusID = @NotificationStatusID

END





GO
GRANT VIEW DEFINITION ON  [dbo].[Notification__GetByClientPersonnelIDAndStatus] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Notification__GetByClientPersonnelIDAndStatus] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Notification__GetByClientPersonnelIDAndStatus] TO [sp_executeall]
GO
