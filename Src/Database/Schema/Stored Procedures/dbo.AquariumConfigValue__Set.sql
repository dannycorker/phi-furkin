SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Ian Slack	
-- Create date: 2012-07-18
-- Description:	Aquarium Config Value Setter (Insert & Update)
-- =============================================
CREATE PROCEDURE [dbo].[AquariumConfigValue__Set] 
	@ConfigID INT, 
	@ClientID INT, 
	@SubClientID INT,
	@ConfigValueID INT, 
	@Name VARCHAR(50), 
	@Value VARCHAR(2000) = NULL, 
	@Salt VARCHAR(50) = NULL
AS
BEGIN

	DECLARE @SetIsValid BIT = CASE WHEN @ConfigID > 0 AND @ClientID > 0 THEN 1 ELSE 0 END,
			@IsInsert BIT = CASE WHEN @ConfigValueID = 0 THEN 1 ELSE 0 END
	
	IF (@SetIsValid = 1)
	BEGIN
		IF (@IsInsert = 1)
		BEGIN
			INSERT INTO AquariumConfigValue (ConfigID, ClientID, SubClientID, Name, Value, Salt)
			VALUES (@ConfigID, @ClientID, @SubClientID, @Name, @Value, @Salt)
			
			SELECT @ConfigValueID = @@IDENTITY
		END
		ELSE
		BEGIN
			UPDATE AquariumConfigValue
			SET	Name = @Name, 
				Value = @Value, 
				Salt = @Salt
			WHERE
				ConfigValueID = @ConfigValueID
		END
	END

	SELECT	
		ConfigValueID, 
		Name, 
		Value, 
		ValueInt, 
		ValueMoney, 
		ValueDate, 
		ValueDateTime, 
		Salt
	FROM	AquariumConfigValue WITH (NOLOCK)
	WHERE	ConfigValueID = @ConfigValueID
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumConfigValue__Set] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AquariumConfigValue__Set] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AquariumConfigValue__Set] TO [sp_executeall]
GO
