SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 31-05-2012
-- Description:	Counts how many credit notes have the given sage unique reference
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Sage_HowManyCreditNotes]

	@ClientID INT,
	@SageUniqueRef VARCHAR(MAX),
	@LeadID INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @LeadTypeID INT,
	@SageUniqueReferenceFieldTP INT = 185,
	@SageUniqueReferenceColumnField INT

	SELECT @LeadTypeID = l.LeadTypeID 
	FROM dbo.Lead l WITH (NOLOCK) 
	WHERE l.LeadID = @LeadID 
	
		/* SageUniqueReference */
	SELECT @SageUniqueReferenceColumnField = t.DetailFieldID 
	FROM dbo.ThirdPartyFieldMapping t WITH (NOLOCK) 
	WHERE t.ThirdPartyFieldID = @SageUniqueReferenceFieldTP AND t.ClientID = @ClientID AND t.LeadTypeID = @LeadTypeID 

	SELECT COUNT(tdv.TableRowID) FROM TableDetailValues tdv WITH (NOLOCK) WHERE DetailFieldID = @SageUniqueReferenceColumnField AND DetailValue = @SageUniqueRef AND LeadID=@LeadID AND ClientID=@ClientID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Sage_HowManyCreditNotes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Sage_HowManyCreditNotes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Sage_HowManyCreditNotes] TO [sp_executeall]
GO
