SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-10-27
-- Description:	List valid encapsulator characters for SqlQuery output
-- =============================================
CREATE PROCEDURE [dbo].[SqlQuery__GetOutputEncapsulatorList] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
	c.AsciiValue, 
	CASE c.AsciiValue 
		WHEN 0 THEN 'None' 
		WHEN 34 THEN 'Double Quotes' 
		WHEN 39 THEN 'Single Quotes' 
		ELSE c.CharDescription 
	END AS CharDescription 
	FROM dbo.CharMap c WITH (NOLOCK) 
	WHERE c.AsciiValue IN (
		0,   /* NULL */
		34,  /* Double Quotes */
		39   /* Single Quotes */
	)
END




GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__GetOutputEncapsulatorList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQuery__GetOutputEncapsulatorList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQuery__GetOutputEncapsulatorList] TO [sp_executeall]
GO
