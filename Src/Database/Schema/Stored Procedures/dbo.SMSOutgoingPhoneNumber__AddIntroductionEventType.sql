SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 08-09-2014
-- Description:	Adds a document type and an event type for an sms survey introduction text
--				It then updates the sms outgoing phone number with the event type id of the sms event
-- =============================================
CREATE PROCEDURE [dbo].[SMSOutgoingPhoneNumber__AddIntroductionEventType]
	@ClientID INT,
	@LeadTypeID INT,
	@SMSOutgoingPhoneNumberID INT,
	@IntroductionFrom VARCHAR(250),
	@IntroductionMessage VARCHAR(2000),
	@DelayInSeconds INT,
	@UserID INT,
	@SmsGatewayID INT	
AS
BEGIN

	SET NOCOUNT ON;

	-- Create document type for introduction test message
	INSERT INTO DocumentType (ClientID,LeadTypeID,DocumentTypeName,DocumentTypeDescription,Header,Template,Footer,CanBeAutoSent,EmailSubject,EmailBodyText,InputFormat,OutputFormat,Enabled,RecipientsTo,RecipientsCC,RecipientsBCC,ReadOnlyTo,ReadOnlyCC,ReadOnlyBCC,SendToMultipleRecipients, MultipleRecipientDataSourceType,MultipleRecipientDataSourceID,SendToAllByDefault,ExcelTemplatePath,FromDetails,ReadOnlyFrom)
	VALUES (@ClientID,NULL,'SMS Introduction For ' + CAST(@SMSOutgoingPhoneNumberID AS VARCHAR),'SMS Introduction For ' + CAST(@SMSOutgoingPhoneNumberID AS VARCHAR),NULL,NULL,NULL,1,'',@IntroductionMessage,'HTML','SMS',1,'[!Mobile]',NULL,NULL,0,0,0,0,NULL,NULL,0,NULL,@IntroductionFrom,0)

	DECLARE @DocumentTypeID INT
	SET @DocumentTypeID= SCOPE_IDENTITY()
	
	DECLARE @SMSQuestionnaireID INT
	DECLARE @QuestionnaireTitle VARCHAR(250)

	SELECT @SMSQuestionnaireID = SMSQuestionnaireID
	FROM SMSOutgoingPhoneNumber WITH (NOLOCK) 
	WHERE SMSOutgoingPhoneNumberID= @SMSOutgoingPhoneNumberID
	
	SELECT @QuestionnaireTitle = Title FROM SMSQuestionnaire WITH (NOLOCK) WHERE SMSQuestionnaireID=@SMSQuestionnaireID
	
	-- place document in questionnaires folder
	DECLARE @FolderID INT
	SELECT @FolderID=FolderID FROM Folder WHERE LEFT(@QuestionnaireTitle,50) = FolderName
	
	IF @FolderID IS NULL
	BEGIN
	
		INSERT INTO Folder (ClientID, FolderTypeID, FolderName, FolderDescription, WhenCreated, WhoCreated, Personal)
		VALUES (@ClientID, 3, LEFT(@QuestionnaireTitle,50),@QuestionnaireTitle,dbo.fn_GetDate_Local(),@UserID,0)
		
		SET @FolderID = SCOPE_IDENTITY()
		
	END
	
	INSERT INTO DocumentTypeFolderLink (DocumentTypeID, FolderID)
	VALUES (@DocumentTypeID, @FolderID)
	
	-- insert introduction event type

	INSERT INTO EventType (ClientID, EventTypeName, EventTypeDescription, Enabled, UnitsOfEffort, FollowupTimeUnitsID, FollowupQuantity, AvailableManually, StatusAfterEvent, AquariumEventAfterEvent, EventSubtypeID, DocumentTypeID, LeadTypeID, AllowCustomTimeUnits, InProcess, KeyEvent, UseEventCosts, UseEventUOEs, UseEventDisbursements, UseEventComments, SignatureRequired, SignatureOverride, VisioX, VisioY, AquariumEventSubtypeID, WhoCreated, WhenCreated, WhoModified, WhenModified, SmsGatewayID)	
	VALUES (@ClientID,'SMS Introduction For ' + CAST(@SMSOutgoingPhoneNumberID AS VARCHAR),'SMS Introduction For ' + CAST(@SMSOutgoingPhoneNumberID AS VARCHAR),1,0,NULL,NULL,1,NULL,NULL,13,@DocumentTypeID, @LeadTypeID,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,@UserID,dbo.fn_GetDate_Local(),@UserID,dbo.fn_GetDate_Local(), @SmsGatewayID)
	
	DECLARE @EventTypeID INT
	SET @EventTypeID = SCOPE_IDENTITY()
	
	-- Update the sms outgoing phone number with the document type id and the event type id
	UPDATE SMSOutgoingPhoneNumber
	SET IntroductionTextEventTypeID=@EventTypeID, IntroductionTextDocumentTypeID=@DocumentTypeID 
	WHERE SMSOutgoingPhoneNumberID = @SMSOutgoingPhoneNumberID
	
	DECLARE @SMSStartEventTypeID INT
	DECLARE @Delay INT
	SELECT @SMSStartEventTypeID=SMSSurveyStartEventTypeID, @Delay = DelayInSeconds 
	FROM SMSOutgoingPhoneNumber WITH (NOLOCK) WHERE SMSOutgoingPhoneNumberID=@SMSOutgoingPhoneNumberID
	

	DECLARE @EventTypeAutomatedEventTypeID INT
	
	SELECT @EventTypeAutomatedEventTypeID=EventTypeAutomatedEventID FROM EventTypeAutomatedEvent WITH (NOLOCK) 
	WHERE EventTypeID=@EventTypeID AND AutomatedEventTypeID=@SMSStartEventTypeID

	IF @EventTypeAutomatedEventTypeID IS NULL -- does not exist so create it
	BEGIN

		INSERT INTO EventTypeAutomatedEvent(ClientID, EventTypeID, AutomatedEventTypeID, RunAsUserID, ThreadToFollowUp, InternalPriority, DelaySeconds)
		VALUES (@ClientID, @EventTypeID, @SMSStartEventTypeID, @UserID, 1, 1, @Delay)			
	
	END
	ELSE
	BEGIN
	
		UPDATE EventTypeAutomatedEvent
		SET DelaySeconds=@Delay
		WHERE EventTypeAutomatedEventID=@EventTypeAutomatedEventTypeID
	
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber__AddIntroductionEventType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSOutgoingPhoneNumber__AddIntroductionEventType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber__AddIntroductionEventType] TO [sp_executeall]
GO
