SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptStandardTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptStandardTarget_GetByScriptID]
(

	@ScriptID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ScriptStandardTargetID],
					[ScriptID],
					[ClientID],
					[Target],
					[ObjectName],
					[PropertyName]
				FROM
					[dbo].[ScriptStandardTarget] WITH (NOLOCK) 
				WHERE
					[ScriptID] = @ScriptID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget_GetByScriptID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptStandardTarget_GetByScriptID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptStandardTarget_GetByScriptID] TO [sp_executeall]
GO
