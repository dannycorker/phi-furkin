SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2015-11-30	
-- Description:	API_Get
-- =============================================
CREATE PROCEDURE [dbo].[API_Get]
	@QID UNIQUEIDENTIFIER,
	@Handled VARCHAR(50)
AS
BEGIN
	
	SELECT	HandleXml
	FROM	API_Queue WITH (NOLOCK) 
	WHERE	QueueID = @QID

	IF @@ROWCOUNT > 0 AND @Handled = 'sync'
	BEGIN
		
		UPDATE	API_Queue
			SET RecieveDT = SYSDATETIME()
		WHERE	QueueID = @QID
		
	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Get] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[API_Get] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[API_Get] TO [sp_executeall]
GO
