SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the QuestionPossibleAnswers table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionPossibleAnswers_Find]
(

	@SearchUsingOR bit   = null ,

	@QuestionPossibleAnswerID int   = null ,

	@ClientQuestionnaireID int   = null ,

	@MasterQuestionID int   = null ,

	@AnswerText varchar (2000)  = null ,

	@Branch int   = null ,

	@LinkedQuestionnaireQuestionPossibleAnswerID int   = null ,

	@ClientID int   = null ,

	@SourceID int   = null ,

	@WhoCreated int   = null ,

	@WhenCreated datetime   = null ,

	@WhoModified int   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [QuestionPossibleAnswerID]
	, [ClientQuestionnaireID]
	, [MasterQuestionID]
	, [AnswerText]
	, [Branch]
	, [LinkedQuestionnaireQuestionPossibleAnswerID]
	, [ClientID]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[QuestionPossibleAnswers] WITH (NOLOCK) 
    WHERE 
	 ([QuestionPossibleAnswerID] = @QuestionPossibleAnswerID OR @QuestionPossibleAnswerID IS NULL)
	AND ([ClientQuestionnaireID] = @ClientQuestionnaireID OR @ClientQuestionnaireID IS NULL)
	AND ([MasterQuestionID] = @MasterQuestionID OR @MasterQuestionID IS NULL)
	AND ([AnswerText] = @AnswerText OR @AnswerText IS NULL)
	AND ([Branch] = @Branch OR @Branch IS NULL)
	AND ([LinkedQuestionnaireQuestionPossibleAnswerID] = @LinkedQuestionnaireQuestionPossibleAnswerID OR @LinkedQuestionnaireQuestionPossibleAnswerID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoModified] = @WhoModified OR @WhoModified IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [QuestionPossibleAnswerID]
	, [ClientQuestionnaireID]
	, [MasterQuestionID]
	, [AnswerText]
	, [Branch]
	, [LinkedQuestionnaireQuestionPossibleAnswerID]
	, [ClientID]
	, [SourceID]
	, [WhoCreated]
	, [WhenCreated]
	, [WhoModified]
	, [WhenModified]
    FROM
	[dbo].[QuestionPossibleAnswers] WITH (NOLOCK) 
    WHERE 
	 ([QuestionPossibleAnswerID] = @QuestionPossibleAnswerID AND @QuestionPossibleAnswerID is not null)
	OR ([ClientQuestionnaireID] = @ClientQuestionnaireID AND @ClientQuestionnaireID is not null)
	OR ([MasterQuestionID] = @MasterQuestionID AND @MasterQuestionID is not null)
	OR ([AnswerText] = @AnswerText AND @AnswerText is not null)
	OR ([Branch] = @Branch AND @Branch is not null)
	OR ([LinkedQuestionnaireQuestionPossibleAnswerID] = @LinkedQuestionnaireQuestionPossibleAnswerID AND @LinkedQuestionnaireQuestionPossibleAnswerID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoModified] = @WhoModified AND @WhoModified is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionPossibleAnswers_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionPossibleAnswers_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionPossibleAnswers_Find] TO [sp_executeall]
GO
