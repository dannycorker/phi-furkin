SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Paul Richardson
-- Create date: 23-02-2015
-- Description:	Gets the script detail field target by the detail field id and the scriptid 
-- =============================================
CREATE PROCEDURE [dbo].[ScriptDetailFieldTarget__GetByDetailFieldIDAndScriptID]
	@DetailFieldID INT  ,
	@ScriptID INT	
AS
BEGIN

		SET ANSI_NULLS ON
		
		SELECT *			
		FROM
			[dbo].[ScriptDetailFieldTarget] WITH (NOLOCK) 
		WHERE
			[DetailFieldID] = @DetailFieldID AND
			[ScriptID] = @ScriptID			
		
		SELECT @@ROWCOUNT
			
END


GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget__GetByDetailFieldIDAndScriptID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptDetailFieldTarget__GetByDetailFieldIDAndScriptID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptDetailFieldTarget__GetByDetailFieldIDAndScriptID] TO [sp_executeall]
GO
