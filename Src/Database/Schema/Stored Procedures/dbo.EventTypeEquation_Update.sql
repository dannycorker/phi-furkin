SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventTypeEquation table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeEquation_Update]
(

	@EventTypeEquationID int   ,

	@ClientID int   ,

	@EventTypeID int   ,

	@DetailFieldID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventTypeEquation]
				SET
					[ClientID] = @ClientID
					,[EventTypeID] = @EventTypeID
					,[DetailFieldID] = @DetailFieldID
				WHERE
[EventTypeEquationID] = @EventTypeEquationID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeEquation_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeEquation_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeEquation_Update] TO [sp_executeall]
GO
