SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DisplayPosition table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DisplayPosition_Find]
(

	@SearchUsingOR bit   = null ,

	@DisplayPositionNum smallint   = null ,

	@DisplayPositionName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DisplayPositionNum]
	, [DisplayPositionName]
    FROM
	[dbo].[DisplayPosition] WITH (NOLOCK) 
    WHERE 
	 ([DisplayPositionNum] = @DisplayPositionNum OR @DisplayPositionNum IS NULL)
	AND ([DisplayPositionName] = @DisplayPositionName OR @DisplayPositionName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DisplayPositionNum]
	, [DisplayPositionName]
    FROM
	[dbo].[DisplayPosition] WITH (NOLOCK) 
    WHERE 
	 ([DisplayPositionNum] = @DisplayPositionNum AND @DisplayPositionNum is not null)
	OR ([DisplayPositionName] = @DisplayPositionName AND @DisplayPositionName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DisplayPosition_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DisplayPosition_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DisplayPosition_Find] TO [sp_executeall]
GO
