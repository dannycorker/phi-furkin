SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-10-31
-- Description:	Get all event choices from and to and event type
-- MODIFIED	2014-07-25	SB	Limited event choice by client ID and used event type function due to shared lead types
-- =============================================
CREATE PROCEDURE [dbo].[EventChoice__GetFromAndToForEventType]
	@EventTypeID int,
	@ClientID INT = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT CASE ec.EventTypeID WHEN @EventTypeID THEN 'From Me' ELSE 'To Me' END [Direction], ec.EventChoiceID, ec.Description [EventChoiceName], et.EventTypeID [From EventTypeID],et.EventSubtypeID [From EventSubtypeID], et.EventTypeName [From EventTypeName], ec.EscalationEvent, ec.Description, et2.EventTypeID [To EventTypeID], et2.EventTypeName [To EventTypeName], et2.EventSubtypeID [To EventSubtypeID]
	, '/EventChoiceEdit.aspx?et=' + convert(varchar,et.EventTypeID) + '&lt=' + convert(varchar,ec.LeadTypeID ) [EditURL]
	, '/EventTypeEdit.aspx?EventTypeID=' + convert(varchar,ec.EventTypeID) [EventEditURL]
	, '/EventTypeEdit.aspx?EventTypeID=' + convert(varchar,ec.NextEventTypeID) [NextEventEditURL]
	FROM EventChoice ec WITH (NOLOCK) 
	INNER JOIN dbo.fnEventTypeShared(@ClientID) et on et.EventTypeID = ec.EventTypeID
	INNER JOIN dbo.fnEventTypeShared(@ClientID) et2 on et2.EventTypeID = ec.NextEventTypeID
	WHERE (ec.EventTypeID = @EventTypeID or ec.NextEventTypeID = @EventTypeID)
	AND (ec.ClientID = @ClientID OR @ClientID IS NULL)
	ORDER BY [Direction], Description, [From EventTypeName], [To EventTypeName]

END


GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice__GetFromAndToForEventType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventChoice__GetFromAndToForEventType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventChoice__GetFromAndToForEventType] TO [sp_executeall]
GO
