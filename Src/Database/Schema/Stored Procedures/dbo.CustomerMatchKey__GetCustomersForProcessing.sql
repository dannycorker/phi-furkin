SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 20-12-2012
-- Description:	Gets a record in CustomerMatchKey by the customerID
-- =============================================
CREATE PROCEDURE [dbo].[CustomerMatchKey__GetCustomersForProcessing]
	
	@ClientID INT

AS
BEGIN

	SELECT CustomerID, FirstName, LastName, Address1, PostCode AS Postcode, DateOfBirth 
	FROM Customers c WITH (NOLOCK) 	
	WHERE c.ClientID = @ClientID AND 
	c.Test<>1 AND c.CustomerID<>954225 AND
	c.CustomerID NOT IN (SELECT CustomerID FROM CustomerMatchKey WITH (NOLOCK) WHERE ClientID=@ClientID)
	ORDER BY c.CustomerID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerMatchKey__GetCustomersForProcessing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerMatchKey__GetCustomersForProcessing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerMatchKey__GetCustomersForProcessing] TO [sp_executeall]
GO
