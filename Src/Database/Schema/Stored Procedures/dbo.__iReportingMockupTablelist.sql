SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-08-19
-- Description:	Demo for iReporting Mockup
-- =============================================
CREATE PROCEDURE [dbo].[__iReportingMockupTablelist]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Tables TABLE (
		[TableName] varchar(100), 
		[FromTableName] varchar(100),
		[JoinType] varchar(5),
		[TableOrder] tinyint
	)

	INSERT @Tables VALUES 
	('vLeadEventFast','','',1),
	('vLeadFast','vLeadEventFast','INNER',2),
	('vCaseWithLatestEvent','vLeadFast','LEFT',3)

	SELECT t.[TableName], 
		t.[FromTableName],
		t.[JoinType]
	FROM @Tables t 
	ORDER BY t.[TableOrder] 
END




GO
GRANT VIEW DEFINITION ON  [dbo].[__iReportingMockupTablelist] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__iReportingMockupTablelist] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__iReportingMockupTablelist] TO [sp_executeall]
GO
