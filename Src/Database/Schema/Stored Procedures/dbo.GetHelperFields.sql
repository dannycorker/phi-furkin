SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Paul Richardson, Jim Green
-- Create date: 2007-08-17
-- Description:	List helper fields for a Lead and Case
-- Modified:	2010-12-02	PR	Added Customer and Case Detail Fields
--				2014-10-21	SB	Changes to allow helper fields to work across linkes leads Zendesk #29131
-- =============================================
CREATE PROCEDURE [dbo].[GetHelperFields]  
	-- Add the parameters for the stored procedure here
	@EventTypeID int, 
	@LeadID int, 
	@CaseID int	
AS
BEGIN

	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID

	-- Get all related linked leads / cases
	DECLARE @LinkedRecords TABLE
	(
		ID INT IDENTITY,
		LeadID INT,
		CaseID INT,
		LeadTypeID INT,
		LeadRowNum INT,
		CaseRowNum INT
	)
	
	INSERT @LinkedRecords (LeadID, CaseID, LeadTypeID)
	SELECT @LeadID, @CaseID, LeadTypeID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID
	
	INSERT @LinkedRecords (LeadID, CaseID, LeadTypeID)
	EXEC dbo.LeadTypeRelationship__GetRelated @LeadID, @CaseID
	
	-- Flag duplicates so we can just select where rn = 1 
	;WITH InnerSql AS 
	(
		SELECT ID, LeadID, CaseID, LeadTypeID, 
		ROW_NUMBER() OVER(PARTITION BY LeadID ORDER BY ID) as LeadRowNum,
		ROW_NUMBER() OVER(PARTITION BY CaseID ORDER BY ID) as CaseRowNum 
		FROM @LinkedRecords
	)
	
	UPDATE l
	SET l.LeadRowNum = i.LeadRowNum,
		l.CaseRowNum = i.CaseRowNum
	FROM @LinkedRecords l
	INNER JOIN InnerSql i ON l.ID = i.ID
	
	

	SELECT 'Client Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], '' as [MatterRefLetter], df.DetailFieldID, 0 as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], 0 AS LeadID, 0 AS CaseID
	FROM dbo.EventTypeHelperField ethf (nolock) 
	INNER JOIN dbo.DetailFields df (nolock) ON ethf.DetailFieldID = df.DetailFieldID 
	INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
	WHERE ethf.EventTypeID = @EventTypeID
	AND df.LeadOrMatter = 12
	AND df.Enabled = 1 
	UNION
	SELECT 'Client Personnel Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], '' as [MatterRefLetter], df.DetailFieldID, 0 as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], 0 AS LeadID, 0 AS CaseID
	FROM dbo.EventTypeHelperField ethf (nolock) 
	INNER JOIN dbo.DetailFields df (nolock) ON ethf.DetailFieldID = df.DetailFieldID 
	INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
	WHERE ethf.EventTypeID = @EventTypeID
	AND df.LeadOrMatter = 13
	AND df.Enabled = 1 	
	UNION
	SELECT 'Lead Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], '' as [MatterRefLetter], df.DetailFieldID, 0 as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], r.LeadID, r.CaseID
	FROM dbo.EventTypeHelperField ethf (nolock) 
	INNER JOIN dbo.fnDetailFieldsShared(@ClientID) df ON ethf.DetailFieldID = df.DetailFieldID 
	INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
	INNER JOIN @LinkedRecords r ON df.LeadTypeID = r.LeadTypeID AND r.LeadRowNum = 1
	WHERE ethf.EventTypeID = @EventTypeID
	AND df.LeadOrMatter = 1
	AND df.Enabled = 1 
--	AND NOT EXISTS(
--		SELECT * 
--		FROM dbo.LeadDetailValues ldv (nolock) 
--		WHERE ldv.LeadID = @LeadID 
--		AND ldv.DetailFieldID = df.DetailFieldID 
--		AND ((df.QuestionTypeID = 4 AND ldv.DetailValue <> '0' AND ldv.DetailValue <> '') OR (df.QuestionTypeID = 14 AND ldv.DetailValue <> '0' AND ldv.DetailValue <> '') OR (df.QuestionTypeID <> 4 AND df.QuestionTypeID <> 14 AND ldv.DetailValue <> ''))
--	)
	UNION
	SELECT 'Customer Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], '' as [MatterRefLetter], df.DetailFieldID, 0 as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], 0 AS LeadID, 0 AS CaseID
	FROM dbo.EventTypeHelperField ethf (nolock) 
	INNER JOIN dbo.DetailFields df (nolock) ON ethf.DetailFieldID = df.DetailFieldID 
	INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
	WHERE ethf.EventTypeID = @EventTypeID
	AND df.LeadOrMatter = 10
	AND df.Enabled = 1 
	UNION 
	SELECT 'Case Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], '' as [MatterRefLetter], df.DetailFieldID, 0 as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], r.LeadID, r.CaseID
	FROM dbo.EventTypeHelperField ethf (nolock) 
	INNER JOIN dbo.fnDetailFieldsShared(@ClientID) df ON ethf.DetailFieldID = df.DetailFieldID 
	INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
	INNER JOIN @LinkedRecords r ON df.LeadTypeID = r.LeadTypeID AND r.CaseRowNum = 1
	WHERE ethf.EventTypeID = @EventTypeID
	AND df.LeadOrMatter = 11
	AND df.Enabled = 1 	
	UNION
	SELECT 'Matter Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], m1.RefLetter as [MatterRefLetter], df.DetailFieldID, m1.MatterID as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], m1.LeadID, m1.CaseID
	FROM dbo.EventTypeHelperField ethf (nolock) 
	INNER JOIN dbo.fnDetailFieldsShared(@ClientID) df ON ethf.DetailFieldID = df.DetailFieldID
	INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
	CROSS JOIN dbo.Matter m1 (nolock) 
	WHERE ethf.EventTypeID = @EventTypeID
	AND df.LeadOrMatter = 2
	AND df.Enabled = 1 
	AND m1.LeadID IN (SELECT r.LeadID FROM @LinkedRecords r WHERE r.LeadTypeID = df.LeadTypeID)
	AND m1.CaseID IN (SELECT r.CaseID FROM @LinkedRecords r WHERE r.LeadTypeID = df.LeadTypeID)
	ORDER BY [MissingType], [MatterRefLetter], [PageDisplayOrder], [FieldDisplayOrder]
--	AND NOT EXISTS(
--		SELECT * 
--		FROM dbo.MatterDetailValues mdv (nolock) 
--		INNER JOIN dbo.Matter m (nolock) ON mdv.MatterID = m.MatterID
--		INNER JOIN dbo.Cases c (nolock) ON m.CaseID = c.CaseID
--		WHERE mdv.LeadID = @LeadID 
--		AND mdv.MatterID = m1.MatterID
--		AND c.CaseID = @CaseID
--		AND mdv.DetailFieldID = df.DetailFieldID 
--		AND ((df.QuestionTypeID = 4 AND mdv.DetailValue <> '0' AND mdv.DetailValue <> '') OR (df.QuestionTypeID = 14 AND mdv.DetailValue <> '0' AND mdv.DetailValue <> '') OR (df.QuestionTypeID <> 4 AND df.QuestionTypeID <> 14 AND mdv.DetailValue <> ''))
--	)
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[GetHelperFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetHelperFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetHelperFields] TO [sp_executeall]
GO
