SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the SqlQueryColumns table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryColumns_Update]
(

	@SqlQueryColumnID int   ,

	@ClientID int   ,

	@SqlQueryID int   ,

	@SqlQueryTableID int   ,

	@CompleteOutputText varchar (500)  ,

	@ColumnAlias varchar (50)  ,

	@ShowColumn bit   ,

	@DisplayOrder int   ,

	@SortOrder int   ,

	@SortType varchar (10)  ,

	@TempColumnID int   ,

	@TempTableID int   ,

	@IsAggregate bit   ,

	@ColumnDataType varchar (50)  ,

	@ManipulatedDataType varchar (50)  ,

	@ColumnNaturalName varchar (50)  ,

	@SourceID int   ,

	@ComplexValue nvarchar (MAX)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[SqlQueryColumns]
				SET
					[ClientID] = @ClientID
					,[SqlQueryID] = @SqlQueryID
					,[SqlQueryTableID] = @SqlQueryTableID
					,[CompleteOutputText] = @CompleteOutputText
					,[ColumnAlias] = @ColumnAlias
					,[ShowColumn] = @ShowColumn
					,[DisplayOrder] = @DisplayOrder
					,[SortOrder] = @SortOrder
					,[SortType] = @SortType
					,[TempColumnID] = @TempColumnID
					,[TempTableID] = @TempTableID
					,[IsAggregate] = @IsAggregate
					,[ColumnDataType] = @ColumnDataType
					,[ManipulatedDataType] = @ManipulatedDataType
					,[ColumnNaturalName] = @ColumnNaturalName
					,[SourceID] = @SourceID
					,[ComplexValue] = @ComplexValue
				WHERE
[SqlQueryColumnID] = @SqlQueryColumnID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryColumns_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryColumns_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryColumns_Update] TO [sp_executeall]
GO
