SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-01-31
-- Description:	Gets the policy limits for the selected policy sections for a set of grouped claims
-- Used by:		Calculation
-- UPDATES:		ROH	2014-07-02	Pet Type selection
--				SB	2015-10-21	Change discussed with Robin... don't pass any non financial limits in as these are handled elsewhere.  Age when creating the rows and occurance limits manually.
--								Look up the optional coverages from the policy history row and then pull them in from the appropriate policy
--				JL 2016-06-13   Added default 1 paramter for 'IsForScarfDisplayOnly' so subsections with the same insured amount as thier parent section are not displayed. Amended other proc calls for calcs to call with 0
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Policy_GetGroupedPolicyLimits] 
(
	@MatterID INT,
	@CurrentPolicyMatterID INT = NULL,
	@IsForScarfDisplayOnly BIT = 1 
)

AS
BEGIN

	SET NOCOUNT ON;
	
	

	DECLARE @PolicyLeadID INT
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@MatterID)

	IF @CurrentPolicyMatterID IS NULL
	BEGIN
		SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)
	END

	DECLARE @PolicyPetType INT
	SELECT @PolicyPetType = rldvPetType.ValueInt
	FROM Lead l WITH (NOLOCK)
	INNER JOIN dbo.LeadDetailValues ldvPetType WITH (NOLOCK) on ldvPetType.LeadID = l.LeadID and ldvPetType.DetailFieldID = 144272
	INNER JOIN dbo.ResourceListDetailValues rldvPetType WITH (NOLOCK) on rldvPetType.ResourceListID = ldvPetType.ValueInt and rldvPetType.DetailFieldID = 144269
	WHERE l.LeadID = @PolicyLeadID

	-- Now we need to see if there are any optional coverages
	DECLARE	@DateToUse DATE  
	SELECT	@DateToUse = dbo.fn_C00_1272_GetDateToUseForCalcs(@MatterID, NULL, NULL)  

	-- Now find the matching table row and get the coverages   
	DECLARE @OptionalCoverages VARCHAR(2000)
	SELECT @OptionalCoverages = tdvCover.DetailValue
	FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@MatterID) r  
	INNER JOIN dbo.TableDetailValues tdvCover WITH (NOLOCK) ON r.TableRowID = tdvCover.TableRowID AND tdvCover.DetailFieldID = 175737   
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663  
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664  
	WHERE @DateToUse >= tdvStart.ValueDate  
	AND @DateToUse <= tdvEnd.ValueDate  
	
	DECLARE @Output TABLE (ResourceListID INT, Section VARCHAR(1000),SubSection VARCHAR(1000),SumInsured DECIMAL (18,2), AllowedCount INT,LimitTypeID INT, LimitType VARCHAR(1000),Reinstatement INT, TreatmentDate Date)
	
	INSERT INTO @Output (ResourceListID,Section,SubSection,SumInsured,AllowedCount,LimitTypeID,LimitType, Reinstatement, TreatmentDate) 
	SELECT DISTINCT s.Out_ResourceListID AS ResourceListID, 
	
	CASE tdvReinstatement.ValueInt 
		WHEN 42932 THEN llSection.ItemValue 
		ELSE llSection.ItemValue + ' ' + CONVERT(VARCHAR,CAST(a.TreatmentStart as DATE),103)
	END    AS Section,
	CASE  
		WHEN llSubSection.ItemValue = '-'  THEN llSubSection.ItemValue  
		WHEN  tdvReinstatement.ValueInt  = 42932 THEN llSubSection.ItemValue 
		ELSE llSubSection.ItemValue + ' ' + CONVERT(VARCHAR,CAST(a.TreatmentStart as DATE),103)
	END  AS SubSection,
	tdvSumInsured.ValueMoney AS SumInsured,
					tdvCount.ValueInt AS AllowedCount, tdvLimitType.ValueInt AS LimitTypeID, llType.ItemValue AS LimitType, tdvReinstatement.ValueInt, a.TreatmentStart
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvRLID WITH (NOLOCK) ON r.TableRowID = tdvRLID.TableRowID AND tdvRLID.DetailFieldID = 144350
	INNER JOIN dbo.fn_C00_1272_GetPolicySectionRelationships(@CurrentPolicyMatterID) s ON tdvRLID.ResourceListID = s.ResourceListID
	INNER JOIN dbo.ResourceListDetailValues rdvSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189
	INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rdvSection.ValueInt = llSection.LookupListItemID 
	INNER JOIN dbo.ResourceListDetailValues rdvSubSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSubSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190
	INNER JOIN dbo.LookupListItems llSubSection WITH (NOLOCK) ON rdvSubSection.ValueInt = llSubSection.LookupListItemID 
	INNER JOIN dbo.TableDetailValues tdvRL WITH (NOLOCK) ON s.Out_ResourceListID = tdvRL.ResourceListID AND tdvRL.DetailFieldID = 144357
	INNER JOIN dbo.TableDetailValues tdvSumInsured WITH (NOLOCK) ON tdvRL.TableRowID = tdvSumInsured.TableRowID AND tdvSumInsured.DetailFieldID = 144358
	LEFT JOIN dbo.TableDetailValues tdvPetType WITH (NOLOCK) ON tdvRL.TableRowID = tdvPetType.TableRowID AND tdvPetType.DetailFieldID = 170013
	LEFT JOIN dbo.TableDetailValues tdvCount WITH (NOLOCK) ON tdvRL.TableRowID = tdvCount.TableRowID AND tdvCount.DetailFieldID = 144267
	LEFT JOIN dbo.TableDetailValues tdvLimitType WITH (NOLOCK) ON tdvRL.TableRowID = tdvLimitType.TableRowID AND tdvLimitType.DetailFieldID = 175388
	LEFT JOIN dbo.LookupListItems llType WITH (NOLOCK) ON tdvLimitType.ValueInt = llType.LookupListItemID
	INNER JOIN dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 1) m ON r.MatterID = m.MatterID 
	INNER JOIN dbo.fn_C600_GroupedClaimDetails(@MatterID) a on a.Section = llSection.ItemValue
	INNER JOIN dbo.TableDetailValues tdvReinstatement WITH (NOLOCK) on tdvReinstatement.TableRowID = tdvRL.TableRowID and tdvReinstatement.DetailFieldID =  176935
 	WHERE tdvRL.MatterID = @CurrentPolicyMatterID
	--AND (tdvPetType.ValueInt IS NULL OR tdvPetType.ValueInt IN (0, @PolicyPetType))
	
	UNION ALL
	
	SELECT DISTINCT s.Out_ResourceListID AS ResourceListID, llSection.ItemValue AS Section, llSubSection.ItemValue AS SubSection, c.SumInsured,
					c.AllowedCount, c.LimitTypeID, c.LimitType,'',''
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvRLID WITH (NOLOCK) ON r.TableRowID = tdvRLID.TableRowID AND tdvRLID.DetailFieldID = 144350
	INNER JOIN dbo.fn_C00_1272_GetOptionalSectionRelationships(@CurrentPolicyMatterID) s ON tdvRLID.ResourceListID = s.ResourceListID
	INNER JOIN dbo.ResourceListDetailValues rdvSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189
	INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rdvSection.ValueInt = llSection.LookupListItemID 
	INNER JOIN dbo.ResourceListDetailValues rdvSubSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSubSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190
	INNER JOIN dbo.LookupListItems llSubSection WITH (NOLOCK) ON rdvSubSection.ValueInt = llSubSection.LookupListItemID 
	INNER JOIN dbo.fn_C600_Scheme_GetOptionalCoverages(@CurrentPolicyMatterID) c ON s.Out_ResourceListID = c.ResourceListID
	INNER JOIN dbo.fnTableOfIDsFromCSV(@OptionalCoverages) id ON c.GroupID = id.AnyID
	INNER JOIN dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 1) m ON r.MatterID = m.MatterID 
	WHERE (c.PetTypeID IS NULL OR c.PetTypeID IN (0, @PolicyPetType))

	IF @IsForScarfDisplayOnly = 1 /*For PHI, don't display subsections where the limit = the section limit for scarf display only*/ 
	BEGIN
		
		DELETE o FROM @Output o 
		where o.SubSection <> '-'
		and EXISTS (SELECT * FROM @Output op 
			where op.SumInsured = o.SumInsured
			and op.SubSection = '-' 
			and op.Section = o.Section) 	
		
		DELETE o FROM @Output o 
		Where o.Reinstatement = 42932 /*Max Ben*/ 
		and exists (SELECT * FROM @Output op 
					Where op.ResourceListID = o.ResourceListID
					and op.Reinstatement = 42932 
					and op.TreatmentDate < o.TreatmentDate)
			
	END


	SELECT * FROM @Output o 
	Order by o.Section desc


END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Policy_GetGroupedPolicyLimits] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Policy_GetGroupedPolicyLimits] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Policy_GetGroupedPolicyLimits] TO [sp_executeall]
GO
