SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-05-21
-- Description:	Retrieves the conversations for actioning
-- =============================================
CREATE PROCEDURE [dbo].[_C00_VetEnvoy_GetConversationsToAction]
(
	@ClientID INT,
	@Status VARCHAR(2000)
)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT tdvConversationID.DetailValue AS ConversationID
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvConversationID WITH (NOLOCK) ON r.TableRowID = tdvConversationID.TableRowID AND tdvConversationID.DetailFieldID = 162658
	INNER JOIN dbo.TableDetailValues tdvStatus WITH (NOLOCK) ON r.TableRowID = tdvStatus.TableRowID AND tdvStatus.DetailFieldID = 162661
	LEFT JOIN dbo.TableDetailValues tdvActioned WITH (NOLOCK) ON r.TableRowID = tdvActioned.TableRowID AND tdvActioned.DetailFieldID = 162662
	WHERE r.ClientID = 224
	AND tdvActioned.ValueDate IS NULL
	AND tdvStatus.ValueInt IN
	(
		SELECT AnyID
		FROM dbo.fnTableOfIDsFromCSV(@Status)
	)

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_VetEnvoy_GetConversationsToAction] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_VetEnvoy_GetConversationsToAction] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_VetEnvoy_GetConversationsToAction] TO [sp_executeall]
GO
