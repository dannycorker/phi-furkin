SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-02-16
-- Description:	Get ready to compare static data between two Aquarium databases.
-- Perform a BINARY_CHECKSUM on all static data on this database and store the results.
-- =============================================
CREATE PROCEDURE [dbo].[MigrationChecksumSourcePopulation]
	/*
		This gets called twice:
		1) First, only compare the data in MigrationTable itself to get that in sync 
		2) Then use that data to compare all the other tables 
	*/
	@MigrationTableOnly bit = 0, 
	@StandaloneClientID int = -1
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ColumnList varchar(2000),
	@TableID int,
	@TableName varchar(2000) = '',
	@PKColumnName varchar(2000),
	@SqlStatement varchar(8000)


	/* For each table... */
	WHILE @TableName IS NOT NULL
	BEGIN
		
		IF @MigrationTableOnly = 1
		BEGIN
			SELECT TOP 1 @ColumnList = '', 
			@TableID = mt.MigrationTableID,
			@TableName = mt.TableName
			FROM dbo.MigrationTable mt WITH (NOLOCK) 
			WHERE mt.TableName = 'MigrationTable'
		END
		ELSE
		BEGIN
			SELECT TOP 1 @ColumnList = '', 
			@TableID = mt.MigrationTableID,
			@TableName = mt.TableName
			FROM dbo.MigrationTable mt WITH (NOLOCK) 
			WHERE mt.IsAquariumStaticData = 1 
			AND mt.TableName > @TableName
			AND mt.CompareStandalone = 1 /* Most static tables need to be replicated, but not WorkingDays or TallyBig etc */ 
			AND mt.TableName <> 'MigrationTable'
			ORDER BY mt.TableName 
		END
		
		IF @@ROWCOUNT = 0
		BEGIN
			SET @TableName = NULL
		END
		ELSE
		BEGIN
			
			/* 
				BINARY_CHECKSUM(*) works fine on all columns, BUT we get mismatches 
				between dev and live for NULLable varchar columns, so list them all out 
				and wrap ISNULL(col, 'NULL') around them where applicable.
			*/
			SELECT @ColumnList += CASE WHEN isc.DATA_TYPE = 'varchar' AND isc.IS_NULLABLE = 'YES' THEN 'ISNULL(' ELSE '' END +  isc.COLUMN_NAME + CASE WHEN isc.DATA_TYPE = 'varchar' AND isc.IS_NULLABLE = 'YES' THEN ', '''') ' ELSE '' END + ',' 
			FROM INFORMATION_SCHEMA.COLUMNS isc 
			WHERE isc.TABLE_NAME = @TableName 
			ORDER BY isc.ORDINAL_POSITION 
			
			IF REVERSE(@ColumnList) LIKE ',%'
			BEGIN
				SELECT @ColumnList = Left(@ColumnList, LEN(@ColumnList) - 1)
			END
			
			SELECT @PKColumnName = isc.COLUMN_NAME 
			FROM INFORMATION_SCHEMA.COLUMNS isc 
			WHERE isc.TABLE_NAME = @TableName 
			AND isc.ORDINAL_POSITION = 1
			
			SELECT @SqlStatement = 'SELECT ' + CAST(@TableID as varchar) + ' , ''' + @TableName + ''' as [TableName], ' + @PKColumnName + ' as [PKID], BINARY_CHECKSUM(' + @ColumnList + ') FROM dbo.' + @TableName + ' WITH (NOLOCK) ' 
			
			/* Only only iteration if this is the MigrationTableOnly loop */
			IF @MigrationTableOnly = 1
			BEGIN
				SET @TableName = NULL 
				
				IF @StandaloneClientID > -1
				BEGIN
					/* Only compare tables that should exist on the target database */
					SELECT @SqlStatement += ' WHERE (ClientID IS NULL OR ClientID = 0) AND AlwaysDrop = 0 '
				END
			END
						
			INSERT dbo.MigrationChecksumSource (MigrationTableID, MigrationTableName, SourcePKID, SourceChecksumValue) 
			EXEC (@SqlStatement)
			
		END
	END
END




GO
GRANT VIEW DEFINITION ON  [dbo].[MigrationChecksumSourcePopulation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MigrationChecksumSourcePopulation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MigrationChecksumSourcePopulation] TO [sp_executeall]
GO
