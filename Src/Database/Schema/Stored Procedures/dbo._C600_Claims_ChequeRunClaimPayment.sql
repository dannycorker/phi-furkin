SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		James Lewis
-- Create date: 27-Sept-2016
-- Description:	Send claim payment instruction 
-- Mods
--	2016-11-14 DCM Update outgoing payment table
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Claims_ChequeRunClaimPayment]
(
	@XmlData XML	
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()

	EXEC dbo.LogXML__AddEntry   @ClientID, 0, 'ChequeClaimPayment', @XmlData, NULL
				
	DECLARE @FileAndFolderEntryToUse INT = 69966 -- TableRowID from Fileand Folder Names table
	
	DECLARE 
			@AccountID INT,
			@Cert VARCHAR(2000),
			@EffectiveDate DATE,
			@EPSTable dbo.tvpGenericPaymentData,
			@EventToAdd INT,
			@FileFormatID INT,
			@Filename VARCHAR(500),
			@LeadEventID INT, 
			@LocalFolder VARCHAR(500),
			@MatterID INT, 
			@PayRef VARCHAR(50),
			@PW VARCHAR(100),
			@Seq INT,
			@SFTPFolder VARCHAR(500),
			@TransactionCode VARCHAR(10),
			@Username VARCHAR(100),
			@WhoCreated INT = dbo.fn_C600_GetAqAutomationUser()

	INSERT INTO @EPSTable (Reference,SettlementDate,PayeeFullname,Amount,ChequeNumber,AddresseeFullname,AddressLine1,AddressLine2,AddressLine3,AddressCounty,AddressPostcode,TableRowID,ParagraphText) 
	SELECT 	bacs.value('(Reference)[1]', 'varchar(100)'),
			bacs.value('(SettlementDate)[1]', 'DATE')  ,
			bacs.value('(PayeeFullName)[1]', 'VARCHAR(100)'),
			bacs.value('(Amount)[1]', 'DECIMAL (18,2)')  ,	
			bacs.value('(ChequeNumber)[1]', 'INT')  ,
			bacs.value('(PayeeFullName)[1]', 'VARCHAR(100)'),
			bacs.value('(Address1)[1]', 'VARCHAR(100)'),	
			bacs.value('(Address2)[1]', 'VARCHAR(100)'),	
			bacs.value('(Address3)[1]', 'VARCHAR(100)'),	
			bacs.value('(County)[1]', 'VARCHAR(100)'),	
			bacs.value('(PostCode)[1]', 'VARCHAR(100)'),	
			bacs.value('(TableRowID)[1]', 'VARCHAR(100)'),
			''		
	FROM @XMLData.nodes('/row') AS Tbl(bacs)
		
	UPDATE @EPSTable
	SET LeadID= 
			CASE WHEN AddressLine1<>'' OR AddressLine2<>'' THEN 1 ELSE 0 END +
			CASE WHEN AddressLine3<>'' OR AddressCounty<>'' THEN 3 ELSE 0 END +
			CASE WHEN AddressPostcode<>'' THEN 5 ELSE 0 END 
	FROM @EPSTable

	UPDATE @EPSTable
	SET MatterID=
			CASE WHEN LeadID IN (1,3,5) THEN 3 ELSE
			CASE WHEN LeadID IN (4,6,8) THEN 2 ELSE 1 END
			END
	FROM @EPSTable

	-- PART 1 File details  BEU filename/does file name need recording in collection tables?
	SELECT @Filename=REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(fn.DetailValue
			                  ,'|DD|',RIGHT('0' + CAST(DATEPART(DD,dbo.fn_GetDate_Local()) AS VARCHAR),2))
                              ,'|MM|',RIGHT('0' + CAST(DATEPART(MM,dbo.fn_GetDate_Local()) AS VARCHAR),2))
                              ,'|YYYY|',RIGHT('0' + CAST(DATEPART(YYYY,dbo.fn_GetDate_Local()) AS VARCHAR),4))
                              ,'|HH|',RIGHT('0' + CAST(DATEPART(HH,dbo.fn_GetDate_Local()) AS VARCHAR),2))
                              ,'|NN|',RIGHT('0' + CAST(DATEPART(MINUTE,dbo.fn_GetDate_Local()) AS VARCHAR),2))
                              ,'|SS|',RIGHT('0' + CAST(DATEPART(SS,dbo.fn_GetDate_Local()) AS VARCHAR),2)),
		   @SFTPFolder=CASE WHEN ut.ValueInt=5144 THEN tf.DetailValue ELSE lf.DetailValue END,
		   @LocalFolder=CASE WHEN ut.ValueInt=5144 THEN '\\468000-File1\AqShared\SharedFtp\Client_' + CONVERT(VARCHAR,@ClientID) + '\UAT\TempExportFiles' 
									ELSE '\\468000-File1\AqShared\SharedFtp\Client_' + CONVERT(VARCHAR,@ClientID) + '\Live\TempExportFiles' END,
		   @Username=CASE WHEN ut.ValueInt=5144 THEN 'Aquarium_support' ELSE un.DetailValue END,
		   @PW=CASE WHEN ut.ValueInt=5144 THEN 'Password4231' ELSE pw.DetailValue END,
		   @Cert=CASE WHEN ut.ValueInt=5144 THEN '' ELSE crt.DetailValue END,
		   @FileFormatID=ffid.ValueInt		   
	FROM TableRows ffn WITH (NOLOCK)
	INNER JOIN TableDetailValues fn WITH (NOLOCK) ON ffn.TableRowID=fn.TableRowID AND fn.DetailFieldID=177117 
	INNER JOIN TableDetailValues lf WITH (NOLOCK) ON ffn.TableRowID=lf.TableRowID AND lf.DetailFieldID=177118 
	INNER JOIN TableDetailValues tf WITH (NOLOCK) ON ffn.TableRowID=tf.TableRowID AND tf.DetailFieldID=177119
	INNER JOIN TableDetailValues ffid WITH (NOLOCK) ON ffn.TableRowID=ffid.TableRowID AND ffid.DetailFieldID=177122
	INNER JOIN ClientDetailValues un WITH (NOLOCK) ON ffn.ClientID=un.ClientID AND un.DetailFieldID=177163
	INNER JOIN ClientDetailValues pw WITH (NOLOCK) ON ffn.ClientID=pw.ClientID AND pw.DetailFieldID=177164
	INNER JOIN ClientDetailValues crt WITH (NOLOCK) ON ffn.ClientID=crt.ClientID AND crt.DetailFieldID=177167
	INNER JOIN ClientDetailValues ut WITH (NOLOCK) ON ffn.ClientID=ut.ClientID AND ut.DetailFieldID=177114 
	WHERE ffn.ClientID=@ClientID 
	AND ffn.DetailFieldID=177120 
	AND ffn.TableRowID= @FileAndFolderEntryToUse

	SELECT @Filename AS [ExportFileName],
			@LocalFolder AS [ExportFilePath],
			0  AS [OutputHeaderRow],
			'web1' AS [SFTPServer],
			@Username AS [SFTPUserName],
			@PW AS [SFTPPassword],
			@SFTPFolder AS [SFTPFolder],
			@Cert AS [SFTPCertificate],
			22 AS [SFTPPort]

		-- PART 2 detail

	IF EXISTS ( SELECT * FROM @EPSTable )
	BEGIN
		EXEC _C600_Collections_ChequeCreateOutput @EPSTable
	END
	ELSE
	BEGIN
	
		SELECT ''
	
	END
	
	-- update outgoing payment table
	UPDATE op
	SET PaymentStatusID=2,
		PaymentFileName=@Filename
	FROM OutgoingPayment op
	INNER JOIN @EPSTable eps ON eps.TableRowID=op.ClaimObjectID
	WHERE op.PaymentFileName IS NULL  

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Claims_ChequeRunClaimPayment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Claims_ChequeRunClaimPayment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Claims_ChequeRunClaimPayment] TO [sp_executeall]
GO
