SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 25-06-2011
-- Description:	Gets the customerid, leadID, caseID and fullname by checking the surname or postcode matches
--				The results are limited to the top 5 since they will be returned via the web service
--				to the aquarium ultra application
-- =============================================
CREATE PROCEDURE [dbo].[__FindCustomerBySurname]
	@ClientID INT,
	@Surname VARCHAR(MAX),
	@Postcode VARCHAR(MAX),
	@RowsToShow INT = 5
AS
BEGIN
		IF((LEN(@Surname)>0) AND (LEN(@Postcode)>0))
		BEGIN
			;WITH InnerSql AS 
			(
				SELECT c.Fullname, c.PostCode, c.CustomerID, l.LeadID, ca.CaseID, ca.CaseNum, lt.LeadTypeName, ma.MatterID PolicyID, ma.MatterRef, ma.RefLetter, ls.StatusName,
				ROW_NUMBER() OVER(PARTITION BY ca.LeadID ORDER BY ca.CaseNum) AS rn 
				FROM dbo.Customers c WITH (NOLOCK) 
				INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.CustomerID = c.CustomerID
				INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID
				INNER JOIN dbo.Cases ca WITH (NOLOCK) ON ca.LeadID = l.LeadID
				INNER JOIN dbo.Matter ma WITH (NOLOCK) ON ma.CaseID = ca.CaseID AND ma.LeadID = l.LeadID
				INNER JOIN dbo.LeadStatus ls WITH (NOLOCK) ON ls.StatusID = ca.ClientStatusID
				WHERE c.ClientID = @ClientID 
				AND (c.LastName = @Surname) AND (c.PostCode = @Postcode)
			)
			SELECT TOP (@RowsToShow) i.* 
			FROM InnerSql i 
		END 
		ELSE IF (LEN(@Surname)>0)
		BEGIN 
			;WITH InnerSql AS 
			(
				SELECT c.Fullname, c.PostCode, c.CustomerID, l.LeadID, ca.CaseID, ca.CaseNum, lt.LeadTypeName, ma.MatterID PolicyID, ma.MatterRef, ma.RefLetter, ls.StatusName,
				ROW_NUMBER() OVER(PARTITION BY ca.LeadID ORDER BY ca.CaseNum) AS rn 
				FROM dbo.Customers c WITH (NOLOCK) 
				INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.CustomerID = c.CustomerID
				INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID
				INNER JOIN dbo.Cases ca WITH (NOLOCK) ON ca.LeadID = l.LeadID
				INNER JOIN dbo.Matter ma WITH (NOLOCK) ON ma.CaseID = ca.CaseID AND ma.LeadID = l.LeadID
				INNER JOIN dbo.LeadStatus ls WITH (NOLOCK) ON ls.StatusID = ca.ClientStatusID
				WHERE c.ClientID = @ClientID 
				AND (c.LastName = @Surname)
			)
			SELECT TOP (@RowsToShow) i.* 
			FROM InnerSql i 		
		END
		ELSE IF (LEN(@Postcode)>0)
		BEGIN 
			;WITH InnerSql AS 
			(
				SELECT c.Fullname, c.PostCode, c.CustomerID, l.LeadID, ca.CaseID, ca.CaseNum, lt.LeadTypeName, ma.MatterID PolicyID, ma.MatterRef, ma.RefLetter, ls.StatusName,
				ROW_NUMBER() OVER(PARTITION BY ca.LeadID ORDER BY ca.CaseNum) AS rn 
				FROM dbo.Customers c WITH (NOLOCK) 
				INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.CustomerID = c.CustomerID
				INNER JOIN dbo.LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = l.LeadTypeID
				INNER JOIN dbo.Cases ca WITH (NOLOCK) ON ca.LeadID = l.LeadID
				INNER JOIN dbo.Matter ma WITH (NOLOCK) ON ma.CaseID = ca.CaseID AND ma.LeadID = l.LeadID
				INNER JOIN dbo.LeadStatus ls WITH (NOLOCK) ON ls.StatusID = ca.ClientStatusID
				WHERE c.ClientID = @ClientID 
				AND (c.PostCode = @Postcode)
			)
			SELECT TOP (@RowsToShow) i.* 
			FROM InnerSql i 		
		END
END




GO
GRANT VIEW DEFINITION ON  [dbo].[__FindCustomerBySurname] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__FindCustomerBySurname] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__FindCustomerBySurname] TO [sp_executeall]
GO
