SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-11-22
-- Description:	Temp proc to allow master db to query the client personnel table on AQ
-- =============================================
CREATE PROCEDURE [dbo].[__AQ_GetClientPersonnelDatabaseMapping]
(
	@Username VARCHAR(100)
)
AS
BEGIN

	DECLARE @ClientID INT

	SELECT @ClientID = cp.ClientID 
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.EmailAddress = @Username

	RETURN ISNULL(@ClientID, -1)
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[__AQ_GetClientPersonnelDatabaseMapping] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__AQ_GetClientPersonnelDatabaseMapping] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__AQ_GetClientPersonnelDatabaseMapping] TO [sp_executeall]
GO
