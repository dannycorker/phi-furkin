SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-02-09
-- Description:	Re-rates the policy either for display or permanently
-- Mods
-- DCM 2015-02-09 Default adjustment date to today 
-- DCM 2015-02-09 Update no refund note text
-- ACE 2018-06-11 Added print statements to match 384 and formatted to look slightly better
-- 2020-02-13 CPS for JIRA AAG-28	| Replace the call to CalculatePremiumForPeriod with the _Evaluated version
-- =============================================
CREATE PROCEDURE [dbo].[_C600_ReRatePolicy]
(
	@Leadeventid INT,
	@MatterID INT,
	@RatingType INT,
	@DisplayOnly INT = 0,
	@WhoCreated INT = NULL,
	@OutputData XML OUTPUT

)
AS
BEGIN

	SET NOCOUNT ON;
	
	PRINT 'Starting _C600_ReRatePolicy' --Merged line across from Aquariusdev384 version

--declare
--	@Leadeventid INT = 43896,
--	@MatterID INT = 4178,
--	@RatingType INT = 9,
--	@DisplayOnly INT = 0,
--	@WhoCreated INT = NULL

-- 1=monthly premium, 2=renewal, 3=MTA, 4=Quote, 5=cancellation	

	DECLARE
			@AdjustmentDate DATE,
			@CaseID INT,
			@ClientID INT,
			@EndDate DATE,
			@IsOK INT,
			@RatingDate DATE,
			@StartDate DATE,
			@TableRowID INT,
			@AdjDateText VARCHAR(10),
			@AdjComment VARCHAR(500),
			@EventComments 	VARCHAR(2000) = '',
			@Overrides dbo.tvpIntVarcharVarchar

	SELECT @CaseID=CaseID, @ClientID=ClientID 
	FROM Matter WITH (NOLOCK) 
	WHERE MatterID=@MatterID			
	
	SELECT @EventComments += CHAR(13)+CHAR(10) + OBJECT_NAME(@@ProcID) + CHAR(13)+CHAR(10)
							+ ' Rating Type ' + ISNULL(CONVERT(VARCHAR,@RatingType),'NULL')

	IF @RatingType=2
	BEGIN

		SELECT @RatingDate=dbo.fnGetDvAsDate (175307,@CaseID) -- Renewal Date

	END
	ELSE IF @RatingType=5
	BEGIN

		SELECT @RatingDate=dbo.fnGetDvAsDate(170036,@CaseID)
		-- cancellation date cannot be in the past or beyond the end of the policy
		SELECT @AdjustmentDate=ISNULL(dbo.fnGetDvAsDate (170055,@CaseID),dbo.fn_GetDate_Local())
		IF @AdjustmentDate<@RatingDate SELECT @AdjustmentDate=@RatingDate

	END
	ELSE IF @RatingType IN (3,9)
	BEGIN

		PRINT '</br>'
		PRINT 'Begin _C600_IsOverrideDateOK'
		EXEC @IsOK=_C600_IsOverrideDateOK @CaseID, @WhoCreated, @DisplayOnly
		PRINT '</br>'
		PRINT 'End _C600_IsOverrideDateOK'

		IF @IsOK = 0
		BEGIN
		
				RAISERROR('<br /><br /><font color="red">You cannot use the override date if it is less than 10 days from today and you have not confirmed that you have the customer''s consent.</font><br/><br/>',16,1)
				RETURN
		
		END
	
		PRINT '</br>'
		PRINT 'Begin Adj Date'
		-- for MTA Rating Date should be policy start date 
		-- the adjustment date cannot be earlier than this
		SELECT @RatingDate=dbo.fnGetDvAsDate(170036,@CaseID)
		SELECT @AdjustmentDate=ISNULL(dbo.fnGetDvAsDate (175442,@CaseID),DATEADD(DAY,-1,dbo.fn_GetDate_Local()))

		IF @AdjustmentDate<@RatingDate SELECT @AdjustmentDate=@RatingDate

	END
	ELSE
	BEGIN

		SELECT @RatingDate=dbo.fnGetDvAsDate (170036,@CaseID) -- Start Date

	END

	PRINT '</br>'
	PRINT 'Get Adj Date'
	SELECT @AdjDateText=CONVERT(VARCHAR(10),@AdjustmentDate,120)

	IF @DisplayOnly = 0
	BEGIN

		EXEC _C00_SimpleValueIntoField 175442,@AdjDateText,@MatterID,@WhoCreated /*Adjustment Date*/

	END
	
	EXEC @TableRowID=dbo._C00_1273_Policy_CalculatePremiumForPeriod_Evaluated @LeadEventID, 0, NULL, @Overrides, @RatingType, @DisplayOnly
	
	-- if MTA or cancellation update the adjustment/renewal fields 
	IF @RatingType IN (3,5,9) 
	BEGIN
			
		SELECT @StartDate=dbo.fnGetDvAsDate (170036,@CaseID) /*Policy Start Date*/
		SELECT @EndDate=dbo.fnGetDvAsDate (170037,@CaseID)	 /*Policy End Date*/
		
		-- calculate the value of adjustment
		EXEC dbo._C00_1273_Policy_CalculateAdjustments @MatterID,@StartDate,@EndDate,@AdjustmentDate,@RatingType,@DisplayOnly,@Leadeventid /*GPR 2019-10-30 - Removed additional param ,0, after @DisplayOnly*/
		PRINT '1'
		-- add banner note if the cancellation override text has contents
		SELECT @AdjComment=ISNULL(dbo.fnGetDv (177274, @CaseID),'') /*Override cancellation comment*/
		IF @RatingType=5 AND @AdjComment<> '' AND @DisplayOnly = 0
		BEGIN

			EXEC _C600_AddANote @CaseID, 908, @AdjComment
			EXEC dbo._C00_SimpleValueIntoField 175448, @AdjComment, @MatterID /*Adjustment Comment*/

		END

	END 
	
	PRINT 'Ending _C600_ReRatePolicy' --merged line from Aquarius384dev version
	PRINT CONVERT(VARCHAR,Current_Timestamp,120)--merged line from Aquarius384dev version

	SELECT @EventComments += ' TableRowID ' + ISNULL(CONVERT(VARCHAR,@TableRowID),'NULL')
	
	EXEC _C00_SetLeadEventComments @LeadEventID, @EventComments, 1
	
	RETURN @TableRowID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ReRatePolicy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_ReRatePolicy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_ReRatePolicy] TO [sp_executeall]
GO
