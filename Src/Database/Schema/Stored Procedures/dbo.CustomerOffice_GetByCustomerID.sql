SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CustomerOffice table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerOffice_GetByCustomerID]
(

	@CustomerID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[OfficeID],
					[ClientID],
					[CustomerID],
					[OfficeName],
					[MainPhoneNumber],
					[FaxNumber],
					[Address1],
					[Address2],
					[Town],
					[County],
					[Postcode],
					[Country],
					[Notes],
					[CountryID]
				FROM
					[dbo].[CustomerOffice] WITH (NOLOCK) 
				WHERE
					[CustomerID] = @CustomerID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice_GetByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerOffice_GetByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerOffice_GetByCustomerID] TO [sp_executeall]
GO
