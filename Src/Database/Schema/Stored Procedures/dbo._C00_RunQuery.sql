SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[_C00_RunQuery]
(
	@QueryID int,
	@ClientID int,
	@CaseID int=null,
	@MatterID int=null,
	@LeadID int=null,
	@CustomerID int=null,
	@SubClientID int=null,
	@ClientPersonnelID int =null,
	@ClientPersonnelAdminGroupID int=null,
	@PortalUserID int=null
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @isQueryOnClient BIT = 0 

	IF EXISTS (
		SELECT *
		FROM SqlQuery sq with(nolock) 
		WHERE SQ.QueryID = @QueryID 
		and sq.ClientID = @ClientID
	)
	BEGIN

		SET @isQueryOnClient = 1

	END 

	IF @isQueryOnClient = 1 /* Check Query exists on this clientID */
	BEGIN

		DECLARE @QUERY_TEXT NVARCHAR(MAX)

		SELECT @QUERY_TEXT = QueryText FROM SqlQuery WITH (NOLOCK) 
		WHERE	QueryID = @QueryID	

		SELECT @QUERY_TEXT = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
			REPLACE(@QUERY_TEXT,'@ClientID', @ClientID)
			,'@MatterID', ISNULL(@MatterID,0) )
			,'@LeadID', ISNULL(@LeadID,0))
			,'@CustomerID', ISNULL(@CustomerID,0))
			,'@CaseID', ISNULL(@CaseID,0))

			,'@SubClientID', ISNULL(@SubClientID,0))
			,'@ClientPersonnelID', ISNULL(@ClientPersonnelID,0))
			,'@ClientPersonnelAdminGroupID', ISNULL(@ClientPersonnelAdminGroupID,0))
			,'@PortalUserID', ISNULL(@PortalUserID,0))
	
		EXEC sp_executesql @QUERY_TEXT 

	END

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RunQuery] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_RunQuery] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_RunQuery] TO [sp_executeall]
GO
