SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the LeadStatus table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadStatus_Get_List]

AS


				
				SELECT
					[StatusID],
					[ClientID],
					[StatusName],
					[StatusDescription],
					[SourceID],
					[LeadTypeID],
					[IsShared]
				FROM
					[dbo].[LeadStatus] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadStatus_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadStatus_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadStatus_Get_List] TO [sp_executeall]
GO
