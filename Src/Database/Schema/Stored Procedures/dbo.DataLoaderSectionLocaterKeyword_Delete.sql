SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DataLoaderSectionLocaterKeyword table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderSectionLocaterKeyword_Delete]
(

	@DataLoaderSectionLocaterKeywordID int   
)
AS


				DELETE FROM [dbo].[DataLoaderSectionLocaterKeyword] WITH (ROWLOCK) 
				WHERE
					[DataLoaderSectionLocaterKeywordID] = @DataLoaderSectionLocaterKeywordID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderSectionLocaterKeyword_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterKeyword_Delete] TO [sp_executeall]
GO
