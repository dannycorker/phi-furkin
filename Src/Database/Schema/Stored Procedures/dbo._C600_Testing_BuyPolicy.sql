SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-01-07
-- Description:	Rollback dates by n days or 1 month (for testing)
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Testing_BuyPolicy] 
(
	@LeadEventID INT
)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()
	
	DECLARE @Xml XML = '<BuyPolicyRequest xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	  <ClientId>' + CONVERT(VARCHAR,@ClientID) + '</ClientId>
	  <ClientPersonnelId>0</ClientPersonnelId>
	  <Username>test.ppweb1@aquarium-software.com</Username>
	  <Password />
	  <BrandingId>134923</BrandingId>
	  <QuoteId>0</QuoteId>
	  <SessionId>1</SessionId>
	  <TransactionRef />
	  <CustomerInfo>
		<CustomerId>0</CustomerId>
		<TitleId>1</TitleId>
		<FirstName>|FirstName|</FirstName>
		<LastName>|LastName|</LastName>
		<HomePhone>01231231234</HomePhone>
		<MobilePhone>|MobilePhone|</MobilePhone>
		<Email>|Email|</Email>
		<Address1>1 Test Street</Address1>
		<Address2>Test Village</Address2>
		<TownCity>Test Town</TownCity>
		<Postcode>|Postcode|</Postcode>
		<SecondaryTitleId>0</SecondaryTitleId>
		<SecondaryFirstName />
		<SecondaryLastName />
		<SecondaryEmail />
		<SecondaryHomePhone />
		<SecondaryMobilePhone />
		<ExistingPolicyHolder>false</ExistingPolicyHolder>
		<DoNotSms>|sms|</DoNotSms>
		<DoNotEmail>|em|</DoNotEmail>
		<DateOfBirth>1997-07-01T00:00:00</DateOfBirth>
		<ContactMethodId>60788</ContactMethodId>
		<MembershipNumber>98099570108</MembershipNumber>
		<SignUpMethod>Online</SignUpMethod>
	  </CustomerInfo>
	  <PaymentInfo>
		<PaymentDay>25</PaymentDay>
		<PaymentMethodTypeId>69930</PaymentMethodTypeId>
		<PaymentIntervalId>|PaymentIntervalId|</PaymentIntervalId>
		<SortCode>074456</SortCode>
		<AccountNumber>12345112</AccountNumber>
		<BankName>HSBC</BankName>
		<BankAccountName>Mr Test</BankAccountName>
		<CreditCardToken />
		<CreditCardHolderName />
		<CreditCardNumberMasked />
		<CreditCardExpireDate />
	  </PaymentInfo>
	  <PetQuotes>
		<PetQuote>
		  <PetInfo>
			<PetRef />
			<PetName>|PetName|</PetName>
			<SpeciesId>|SpeciesID|</SpeciesId>
			<BreedId>|BreedID|</BreedId>
			<Gender>M</Gender>
			<BirthDate>|DoB|T00:00:00</BirthDate>
			<IsNeutered>false</IsNeutered>
			<HasMicrochip>true</HasMicrochip>
			<MicrochipNo>micro123</MicrochipNo>
			<PurchasePrice>350.00</PurchasePrice>
			<PetColour>73619</PetColour>
			<VaccinationsUpToDate>false</VaccinationsUpToDate>
			<HasExistingConditions>false</HasExistingConditions>
		  </PetInfo>
		  <PolicyValues>
			<PolicyValue>
			  <ProductId>|ProductID|</ProductId>
			  <PolicyLimit>0</PolicyLimit>
			  <Excess>0</Excess>
			  <VoluntaryExcess>0.0000</VoluntaryExcess>
			  <CoInsurance>0</CoInsurance>
			  <OptionalCoverages></OptionalCoverages>
			</PolicyValue>
		  </PolicyValues>
		  <StartDate>|StartDate|T00:00:00</StartDate>
		</PetQuote>
	  </PetQuotes>
	  <OtherPetsInsured>0</OtherPetsInsured>
	  <DiscountCodes>
		<string>PPLBrandPPC</string>
	  </DiscountCodes>
	  <KeyValues />
	</BuyPolicyRequest>'

	DECLARE 
			@Firstname VARCHAR(100)='dcm',
			@Lastname VARCHAR(100)='testnewpolicy',
			@Email VARCHAR(100)='dave.morgan@aquarium-software.com',
			@Mobile VARCHAR(100)='',
			@Postcode VARCHAR(100)='WA14 2UN',
			@PaymentInterval VARCHAR(2)='1',
			@StartDate VARCHAR(10)='2016-09-30',
			@PetName VARCHAR(100)='Bonza',
			@PolicyID VARCHAR(30),
			@CaseID INT,
			@MatterID INT,
			@em VARCHAR(30),
			@sms VARCHAR(30),
			@ProductID INT = 145279,
			@BreedID INT = 143414,
			@SpeciesID INT = 42990,
			@DoB DATE = '2014-01-01'
			
	DECLARE @Output TABLE ( XmlOutput XML )
	
	SELECT @CaseID=le.CaseID, @MatterID=m.MatterID FROM LeadEvent le WITH (NOLOCK)
	INNER JOIN Matter m WITH (NOLOCK) ON le.CaseID=m.CaseID 
	WHERE  le.LeadEventID=@LeadEventID
			
	SELECT @Email=ISNULL(dbo.fnGetDv(177146,@CaseID),'')
	IF @Email=''
		SELECT @em='true',@Email='test@aquarium-software.com'
	ELSE
		SELECT @em='false'
		
	SELECT @Mobile=ISNULL(dbo.fnGetDv(177147,@CaseID),'')
	IF @Mobile=''
		SET @sms='true'
	ELSE
		SET @sms='false'
			
	SELECT @Firstname = CASE WHEN ISNULL(dbo.fnGetDv(177144,@CaseID),'')='' THEN @Firstname ELSE dbo.fnGetDv(177144,@CaseID) END,
			@Lastname = CASE WHEN ISNULL(dbo.fnGetDv(177145,@CaseID),'')='' THEN @Lastname ELSE dbo.fnGetDv(177145,@CaseID) END,
			--@Email = CASE WHEN ISNULL(dbo.fnGetDv(177146,@CaseID),'')='' THEN @Email ELSE dbo.fnGetDv(177146,@CaseID) END,
			--@Mobile = CASE WHEN ISNULL(dbo.fnGetDv(177147,@CaseID),'')='' THEN @Mobile ELSE dbo.fnGetDv(177147,@CaseID) END,
			@Postcode = CASE WHEN ISNULL(dbo.fnGetDv(177148,@CaseID),'')='' THEN @Postcode ELSE dbo.fnGetDv(177148,@CaseID) END,
			@PaymentInterval = CASE WHEN ISNULL(dbo.fnGetDv(177149,@CaseID),'')='' THEN @PaymentInterval ELSE dbo.fnGetDv(177149,@CaseID) END,
			@StartDate = CASE WHEN ISNULL(dbo.fnGetDv(177150,@CaseID),'')='' THEN @StartDate ELSE dbo.fnGetDv(177150,@CaseID) END,
			@PetName = CASE WHEN ISNULL(dbo.fnGetDv(177151,@CaseID),'')='' THEN @PetName ELSE dbo.fnGetDv(177151,@CaseID) END,
			@ProductID = CASE WHEN ISNULL(dbo.fnGetDv(177156,@CaseID),'')='' THEN @ProductID ELSE dbo.fnGetDv(177156,@CaseID) END,
			@BreedID = CASE WHEN ISNULL(dbo.fnGetDv(177155,@CaseID),'')='' THEN @BreedID ELSE dbo.fnGetDv(177155,@CaseID) END,
			@Dob = CASE WHEN ISNULL(dbo.fnGetDv(177157,@CaseID),'')='' THEN @DoB ELSE dbo.fnGetDv(177157,@CaseID) END
			
	SELECT @SpeciesID = rldv.ValueINT FROM ResourceListDetailValues rldv 
	where rldv.ResourceListID = @BreedID 
	and rldv.DetailFieldID =  144269
	
	SELECT @XML=REPLACE(CAST(@Xml AS VARCHAR(MAX)),'|Firstname|',@Firstname)
	SELECT @XML=REPLACE(CAST(@Xml AS VARCHAR(MAX)),'|Lastname|',@Lastname)
	SELECT @XML=REPLACE(CAST(@Xml AS VARCHAR(MAX)),'|Email|',@Email)
	SELECT @XML=REPLACE(CAST(@Xml AS VARCHAR(MAX)),'|MobilePhone|',@Mobile)
	SELECT @XML=REPLACE(CAST(@Xml AS VARCHAR(MAX)),'|Postcode|',@Postcode)
	SELECT @XML=REPLACE(CAST(@Xml AS VARCHAR(MAX)),'|PaymentIntervalId|',@PaymentInterval)
	SELECT @XML=REPLACE(CAST(@Xml AS VARCHAR(MAX)),'|StartDate|',@StartDate)
	SELECT @XML=REPLACE(CAST(@Xml AS VARCHAR(MAX)),'|PetName|',@PetName)
	SELECT @XML=REPLACE(CAST(@Xml AS VARCHAR(MAX)),'|em|',@em)
	SELECT @XML=REPLACE(CAST(@Xml AS VARCHAR(MAX)),'|sms|',@sms)
	SELECT @XML=REPLACE(CAST(@Xml AS VARCHAR(MAX)),'|ProductID|',@ProductID)
	SELECT @XML=REPLACE(CAST(@Xml AS VARCHAR(MAX)),'|BreedID|',@BreedID)
	SELECT @XML=REPLACE(CAST(@Xml AS VARCHAR(MAX)),'|SpeciesID|',@SpeciesID)
	SELECT @XML=REPLACE(CAST(@Xml AS VARCHAR(MAX)),'|DoB|',@DoB)

	INSERT INTO @Output
	EXEC dbo._C00_1273_Policy_BuyPolicy @Xml

	SELECT @PolicyID=XmlOutput.value('(//BuyPolicyResponse/PolicyNumber)[1]', 'varchar(100)') FROM @output
	
	UPDATE LeadEvent
	SET Comments = 'New MatterID = ' + ISNULL(@PolicyID,'')
	WHERE LeadEventID=@LeadEventID
	
	UPDATE MatterDetailValues SET DetailValue='' WHERE DetailFieldID between 177144 AND 177151 AND MatterID=@MatterID

	DECLARE @RunTime DATETIME
	SELECT @RunTime=dbo.fn_GetDate_Local() 
	EXEC dbo.AutomatedTask__RunNow 19749, 0, @RunTime, 0
	EXEC dbo.AutomatedTask__RunNow 20075, 0, @RunTime, 0
	EXEC dbo.AutomatedTask__RunNow 20859, 0, @RunTime, 0

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Testing_BuyPolicy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Testing_BuyPolicy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Testing_BuyPolicy] TO [sp_executeall]
GO
