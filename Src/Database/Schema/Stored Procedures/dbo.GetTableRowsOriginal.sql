SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







-- =============================================
-- Author:		Jim Green
-- Create date: 2008-06-26
-- Description:	Get Table Rows for the app to pivot, filter and display
-- =============================================
CREATE PROCEDURE [dbo].[GetTableRowsOriginal]
	@ClientID int,
	@TableRowDetailFieldPageID int,
	@TableRowDetailFieldID int,
	@LeadID int = null,
	@MatterID int = null,
	@MatterIDList varchar(50) = null
AS
BEGIN
	SET NOCOUNT ON;

	/* 
		Return all table fields, with resource lists exploded into all their component fields as well. 
		There is guaranteed to be exactly 1 resource list within a table. 
		Turn all dates round into dd-mm-yyyy format ready for display.
	*/
	SELECT tr.MatterID, tdv.TableDetailValueID, tdv.TableRowID, tdv.DetailFieldID, 
	tdv.DetailValue as [tdvDetailValue], tdv.ResourceListID, tdv.EncryptedValue as [tdvEncryptedDetailValue], 
    tdv_df.FieldName, tdv_df.FieldCaption, tdv_df.QuestionTypeID, 
	tdv_df.Enabled, tdv_df.EquationText, tdv_df.Editable, 
	rldv.DetailValue as [rldvDetailValue],	rldv.EncryptedValue as [rldvEncryptedDetailValue], 
	tdv_df.Encrypt, tdv_df.LeadTypeID 
	FROM TableRows tr 
	INNER JOIN TableDetailValues tdv ON tdv.TableRowID = tr.TableRowID 
	INNER JOIN DetailFields tdv_df ON tdv_df.DetailFieldID = tdv.DetailFieldID 
	LEFT JOIN ResourceListDetailValues rldv ON rldv.ResourceListID = tdv.ResourceListID AND rldv.DetailFieldID = tdv.DetailFieldID 
	WHERE tr.ClientID = @ClientID 
	AND tr.DetailFieldPageID = @TableRowDetailFieldPageID 
	AND tr.DetailFieldID = @TableRowDetailFieldID 
	AND (@LeadID IS NULL OR tr.LeadID = @LeadID) 
	-- CT: remove this and just use the 2nd part:
	-- @MatterIDList IS NULL AND @MatterID IS NULL OR tr.MatterID = @MatterID
	-- if this is causing any problems
	AND (@MatterIDList IS NOT NULL AND tr.MatterID IN (SELECT * FROM dbo.fnTableOfIDsFromCSV (@MatterIDList)) 
	OR
	(@MatterIDList IS NULL AND @MatterID IS NULL OR tr.MatterID = @MatterID) 
	)
	ORDER BY tdv.ClientID, tdv.LeadID, tdv.MatterID, tdv.TableRowID, tdv.DetailFieldID, tdv.ResourceListID, rldv.DetailFieldID 
END







GO
GRANT VIEW DEFINITION ON  [dbo].[GetTableRowsOriginal] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetTableRowsOriginal] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetTableRowsOriginal] TO [sp_executeall]
GO
