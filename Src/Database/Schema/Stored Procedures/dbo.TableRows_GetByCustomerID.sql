SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the TableRows table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TableRows_GetByCustomerID]
(

	@CustomerID int   
)
AS


				SELECT
					[TableRowID],
					[ClientID],
					[LeadID],
					[MatterID],
					[DetailFieldID],
					[DetailFieldPageID],
					[DenyEdit],
					[DenyDelete],
					[CustomerID],
					[CaseID],
					[ClientPersonnelID],
					[ContactID],
					[SourceID]
				FROM
					[dbo].[TableRows] WITH (NOLOCK) 
				WHERE
										[CustomerID] = @CustomerID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows_GetByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRows_GetByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRows_GetByCustomerID] TO [sp_executeall]
GO
