SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 01/07/2011
-- Description:	Gets a list of rows to supress in ultra
-- 101	RowID
-- 102	TransactionID
-- 103	AgentFirstName
-- 104	AgentLastName
-- 105	UserName
-- 106	UserID
-- 107	DateTime Of Call
-- 108	Deleted From Ultra
-- 109	Uploaded to Ultra Date time
-- 110	BatchID
-- 111	SchemaName
-- 113	Error
-- =============================================
-- Modified 04/12/2012
-- By Paul Richardson
-- Added Case ID and backward compatibility
CREATE PROCEDURE [dbo].[__GetRowsToSuppressInUltra]
	@ClientID INT,
	@LeadID INT,
	@CaseID INT = NULL
AS
BEGIN


	DECLARE @TableRowID INT,			
			@DetailFieldID INT,
			@101FieldID INT,
			@108FieldID INT,
			@110FieldID INT,
			@111FieldID	INT,
			@169FieldID INT

	SELECT @101FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 101
	AND t.ClientID = @ClientID
	
	SELECT @108FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 108
	AND t.ClientID = @ClientID
	
	SELECT @110FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 110
	AND t.ClientID = @ClientID
	
	SELECT @111FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 111
	AND t.ClientID = @ClientID
	
	SELECT @169FieldID = t.ColumnFieldID
	FROM ThirdPartyFieldMapping t WITH (NOLOCK)
	WHERE t.ThirdPartyFieldID = 169
	AND t.ClientID = @ClientID

	IF @CaseID IS NULL
	BEGIN
	
		SELECT tdv.TableRowID, tdv.DetailValue RowID, tdv2.DetailValue DeleteFlag, tdv3.DetailValue BaseID, tdv4.DetailValue SchemaName
		FROM dbo.TableDetailValues tdv WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv2 ON tdv2.TableRowID = tdv.TableRowID AND tdv2.DetailFieldID = @108FieldID
		INNER JOIN TableDetailValues tdv3 ON tdv3.TableRowID = tdv.TableRowID AND tdv3.DetailFieldID = @110FieldID AND tdv3.DetailValue IS NOT NULL AND tdv3.DetailValue <> ''
		INNER JOIN TableDetailValues tdv4 ON tdv4.TableRowID = tdv.TableRowID AND tdv4.DetailFieldID = @111FieldID AND tdv4.DetailValue IS NOT NULL AND tdv4.DetailValue <> ''		
		WHERE tdv.ClientID=@ClientID AND tdv.LeadID=@LeadID AND tdv.DetailFieldID =@101FieldID 
		ORDER BY tdv.TableRowID DESC
		
	END
	ELSE -- has a case id so filter on that
	BEGIN
	
		SELECT tdv.TableRowID, tdv.DetailValue RowID, tdv2.DetailValue DeleteFlag, tdv3.DetailValue BaseID, tdv4.DetailValue SchemaName
		FROM dbo.TableDetailValues tdv WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv2 ON tdv2.TableRowID = tdv.TableRowID AND tdv2.DetailFieldID = @108FieldID
		INNER JOIN TableDetailValues tdv3 ON tdv3.TableRowID = tdv.TableRowID AND tdv3.DetailFieldID = @110FieldID AND tdv3.DetailValue IS NOT NULL AND tdv3.DetailValue <> ''
		INNER JOIN TableDetailValues tdv4 ON tdv4.TableRowID = tdv.TableRowID AND tdv4.DetailFieldID = @111FieldID AND tdv4.DetailValue IS NOT NULL AND tdv4.DetailValue <> ''
		INNER JOIN TableDetailValues tdv5 ON tdv5.TableRowID = tdv.TableRowID AND tdv5.DetailFieldID = @169FieldID AND tdv5.ValueInt=@CaseID
		WHERE tdv.ClientID=@ClientID AND tdv.LeadID=@LeadID AND tdv.DetailFieldID =@101FieldID 
		ORDER BY tdv.TableRowID DESC
		
	END
END


GO
GRANT VIEW DEFINITION ON  [dbo].[__GetRowsToSuppressInUltra] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[__GetRowsToSuppressInUltra] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[__GetRowsToSuppressInUltra] TO [sp_executeall]
GO
