SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ReferenceValueFormat table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueFormat_Find]
(

	@SearchUsingOR bit   = null ,

	@ReferenceValueFormatID int   = null ,

	@ClientID int   = null ,

	@ReferenceValueName varchar (50)  = null ,

	@ReferenceValueDescription varchar (255)  = null ,

	@IntegerSectionSize int   = null ,

	@IntegerStartPoint int   = null ,

	@DateFormat varchar (50)  = null ,

	@YearFormat varchar (50)  = null ,

	@DelimiterID int   = null ,

	@Section1TypeID int   = null ,

	@Section1DetailFieldID int   = null ,

	@Section2TypeID int   = null ,

	@Section2DetailFieldID int   = null ,

	@Section3TypeID int   = null ,

	@Section3DetailFieldID int   = null ,

	@Section4TypeID int   = null ,

	@Section4DetailFieldID int   = null ,

	@Section5TypeID int   = null ,

	@Section5DetailFieldID int   = null ,

	@LeftPadIntegerSection bit   = null ,

	@IncludeBlankSections bit   = null ,

	@EnforceUniqueness bit   = null ,

	@SourceID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ReferenceValueFormatID]
	, [ClientID]
	, [ReferenceValueName]
	, [ReferenceValueDescription]
	, [IntegerSectionSize]
	, [IntegerStartPoint]
	, [DateFormat]
	, [YearFormat]
	, [DelimiterID]
	, [Section1TypeID]
	, [Section1DetailFieldID]
	, [Section2TypeID]
	, [Section2DetailFieldID]
	, [Section3TypeID]
	, [Section3DetailFieldID]
	, [Section4TypeID]
	, [Section4DetailFieldID]
	, [Section5TypeID]
	, [Section5DetailFieldID]
	, [LeftPadIntegerSection]
	, [IncludeBlankSections]
	, [EnforceUniqueness]
	, [SourceID]
    FROM
	[dbo].[ReferenceValueFormat] WITH (NOLOCK) 
    WHERE 
	 ([ReferenceValueFormatID] = @ReferenceValueFormatID OR @ReferenceValueFormatID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ReferenceValueName] = @ReferenceValueName OR @ReferenceValueName IS NULL)
	AND ([ReferenceValueDescription] = @ReferenceValueDescription OR @ReferenceValueDescription IS NULL)
	AND ([IntegerSectionSize] = @IntegerSectionSize OR @IntegerSectionSize IS NULL)
	AND ([IntegerStartPoint] = @IntegerStartPoint OR @IntegerStartPoint IS NULL)
	AND ([DateFormat] = @DateFormat OR @DateFormat IS NULL)
	AND ([YearFormat] = @YearFormat OR @YearFormat IS NULL)
	AND ([DelimiterID] = @DelimiterID OR @DelimiterID IS NULL)
	AND ([Section1TypeID] = @Section1TypeID OR @Section1TypeID IS NULL)
	AND ([Section1DetailFieldID] = @Section1DetailFieldID OR @Section1DetailFieldID IS NULL)
	AND ([Section2TypeID] = @Section2TypeID OR @Section2TypeID IS NULL)
	AND ([Section2DetailFieldID] = @Section2DetailFieldID OR @Section2DetailFieldID IS NULL)
	AND ([Section3TypeID] = @Section3TypeID OR @Section3TypeID IS NULL)
	AND ([Section3DetailFieldID] = @Section3DetailFieldID OR @Section3DetailFieldID IS NULL)
	AND ([Section4TypeID] = @Section4TypeID OR @Section4TypeID IS NULL)
	AND ([Section4DetailFieldID] = @Section4DetailFieldID OR @Section4DetailFieldID IS NULL)
	AND ([Section5TypeID] = @Section5TypeID OR @Section5TypeID IS NULL)
	AND ([Section5DetailFieldID] = @Section5DetailFieldID OR @Section5DetailFieldID IS NULL)
	AND ([LeftPadIntegerSection] = @LeftPadIntegerSection OR @LeftPadIntegerSection IS NULL)
	AND ([IncludeBlankSections] = @IncludeBlankSections OR @IncludeBlankSections IS NULL)
	AND ([EnforceUniqueness] = @EnforceUniqueness OR @EnforceUniqueness IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ReferenceValueFormatID]
	, [ClientID]
	, [ReferenceValueName]
	, [ReferenceValueDescription]
	, [IntegerSectionSize]
	, [IntegerStartPoint]
	, [DateFormat]
	, [YearFormat]
	, [DelimiterID]
	, [Section1TypeID]
	, [Section1DetailFieldID]
	, [Section2TypeID]
	, [Section2DetailFieldID]
	, [Section3TypeID]
	, [Section3DetailFieldID]
	, [Section4TypeID]
	, [Section4DetailFieldID]
	, [Section5TypeID]
	, [Section5DetailFieldID]
	, [LeftPadIntegerSection]
	, [IncludeBlankSections]
	, [EnforceUniqueness]
	, [SourceID]
    FROM
	[dbo].[ReferenceValueFormat] WITH (NOLOCK) 
    WHERE 
	 ([ReferenceValueFormatID] = @ReferenceValueFormatID AND @ReferenceValueFormatID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ReferenceValueName] = @ReferenceValueName AND @ReferenceValueName is not null)
	OR ([ReferenceValueDescription] = @ReferenceValueDescription AND @ReferenceValueDescription is not null)
	OR ([IntegerSectionSize] = @IntegerSectionSize AND @IntegerSectionSize is not null)
	OR ([IntegerStartPoint] = @IntegerStartPoint AND @IntegerStartPoint is not null)
	OR ([DateFormat] = @DateFormat AND @DateFormat is not null)
	OR ([YearFormat] = @YearFormat AND @YearFormat is not null)
	OR ([DelimiterID] = @DelimiterID AND @DelimiterID is not null)
	OR ([Section1TypeID] = @Section1TypeID AND @Section1TypeID is not null)
	OR ([Section1DetailFieldID] = @Section1DetailFieldID AND @Section1DetailFieldID is not null)
	OR ([Section2TypeID] = @Section2TypeID AND @Section2TypeID is not null)
	OR ([Section2DetailFieldID] = @Section2DetailFieldID AND @Section2DetailFieldID is not null)
	OR ([Section3TypeID] = @Section3TypeID AND @Section3TypeID is not null)
	OR ([Section3DetailFieldID] = @Section3DetailFieldID AND @Section3DetailFieldID is not null)
	OR ([Section4TypeID] = @Section4TypeID AND @Section4TypeID is not null)
	OR ([Section4DetailFieldID] = @Section4DetailFieldID AND @Section4DetailFieldID is not null)
	OR ([Section5TypeID] = @Section5TypeID AND @Section5TypeID is not null)
	OR ([Section5DetailFieldID] = @Section5DetailFieldID AND @Section5DetailFieldID is not null)
	OR ([LeftPadIntegerSection] = @LeftPadIntegerSection AND @LeftPadIntegerSection is not null)
	OR ([IncludeBlankSections] = @IncludeBlankSections AND @IncludeBlankSections is not null)
	OR ([EnforceUniqueness] = @EnforceUniqueness AND @EnforceUniqueness is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueFormat_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueFormat_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueFormat_Find] TO [sp_executeall]
GO
