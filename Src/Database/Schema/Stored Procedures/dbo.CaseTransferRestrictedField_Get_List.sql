SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the CaseTransferRestrictedField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferRestrictedField_Get_List]

AS


				
				SELECT
					[CaseTransferRestrictedFieldID],
					[ClientRelationshipID],
					[DetailFieldID]
				FROM
					[dbo].[CaseTransferRestrictedField] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferRestrictedField_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferRestrictedField_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferRestrictedField_Get_List] TO [sp_executeall]
GO
