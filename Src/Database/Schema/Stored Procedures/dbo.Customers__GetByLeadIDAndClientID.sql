SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 13-04-2016
-- Description:	Gets a customer by leadID and customerID
-- =============================================
CREATE PROCEDURE [dbo].[Customers__GetByLeadIDAndClientID]
	@ClientID INT,
	@LeadID INT
AS
BEGIN
		
	SET NOCOUNT ON;

    DECLARE @CustomerID INT
	
	SELECT @CustomerID=CustomerID FROM Lead l WITH (NOLOCK) 
	WHERE l.LeadID=@LeadID AND l.ClientID=@ClientID

	SELECT * FROM Customers WITH (NOLOCK) 
	WHERE CustomerID=@CustomerID
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetByLeadIDAndClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Customers__GetByLeadIDAndClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Customers__GetByLeadIDAndClientID] TO [sp_executeall]
GO
