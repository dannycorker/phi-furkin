SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Chris Townsend
-- Create date: 10th March 2008
-- Description:	Gets a collection of IDs and Names
--				to put in a helper list for iReporting
--				2011-03-28 ACE Added join to DetailFieldSubType
-- =============================================
CREATE PROCEDURE [dbo].[ReportBuilder_GetHelperListItems]
	@ColumnName varchar(50),
	@LeadTypeID int = 0,
	@ClientID int
AS
BEGIN
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @ColumnName = 'EventTypeID'
		BEGIN
		
		SELECT EventTypeID AS ID, EventTypeName AS [Name]
		FROM EventType WITH (NOLOCK) 
		WHERE EventType.ClientID = @ClientID
		AND (@LeadTypeID = 0 OR EventType.LeadTypeID = @LeadTypeID)
		ORDER BY [Name]

		END

	ELSE IF @ColumnName = 'LeadTypeID'
		BEGIN

		SELECT LeadTypeID AS ID, LeadTypeName AS [Name]
		FROM LeadType WITH (NOLOCK) 
		WHERE LeadType.ClientID = @ClientID
		AND (@LeadTypeID = 0 OR LeadType.LeadTypeID = @LeadTypeID)
		ORDER BY [Name]

		END

	ELSE IF @ColumnName = 'DetailFieldID'
		BEGIN

		SELECT DetailFieldID AS ID, 
		'(' + lt.LeadTypeName + ') ' + dfst.DetailFieldSubTypeName + ': ' + DetailFields.FieldName AS [Name]
		FROM DetailFields WITH (NOLOCK) 
		INNER JOIN LeadType lt WITH (NOLOCK) ON lt.LeadTypeID = DetailFields.LeadTypeID
		INNER JOIN DetailFieldSubType dfst WITH (NOLOCK) ON dfst.DetailFieldSubTypeID = DetailFields.LeadOrMatter
		WHERE DetailFields.ClientID = @ClientID
		AND (@LeadTypeID = 0 OR DetailFields.LeadTypeID = @LeadTypeID)
		ORDER BY [Name]

		END

	ELSE IF @ColumnName = 'ClientStatusID'
		BEGIN

		SELECT StatusID AS ID, StatusName AS [Name]
		FROM LeadStatus WITH (NOLOCK) 
		WHERE LeadStatus.ClientID = @ClientID
		ORDER BY [Name]

		END

	ELSE IF @ColumnName = 'AquariumStatusID'
		BEGIN

		SELECT AquariumStatusID AS ID, AquariumStatusName AS [Name]
		FROM AquariumStatus WITH (NOLOCK) 
		ORDER BY [Name]

		END

	ELSE IF @ColumnName = 'ClientQuestionnaireID'
		BEGIN

		SELECT ClientQuestionnaireID AS ID, QuestionnaireTitle AS [Name]
		FROM ClientQuestionnaires WITH (NOLOCK) 
		WHERE ClientQuestionnaires.ClientID = @ClientID
		ORDER BY [Name]

		END

	ELSE IF @ColumnName = 'MasterQuestionID'
		BEGIN

		SELECT MasterQuestionID AS ID, QuestionText AS [Name]
		FROM MasterQuestions WITH (NOLOCK) 
		WHERE MasterQuestions.ClientID = @ClientID
		ORDER BY [Name]

		END


END








GO
GRANT VIEW DEFINITION ON  [dbo].[ReportBuilder_GetHelperListItems] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReportBuilder_GetHelperListItems] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReportBuilder_GetHelperListItems] TO [sp_executeall]
GO
