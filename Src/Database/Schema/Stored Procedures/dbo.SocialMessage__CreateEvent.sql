SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-11-2015
-- Description:	Creates a Social In event
-- =============================================
CREATE PROCEDURE [dbo].[SocialMessage__CreateEvent]
	@ClientID INT,
	@LeadID INT,
	@CaseID INT,
	@MessageBytes VARBINARY(MAX),
	@OverView VARCHAR(1000),
	@EventTypeID INT

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @LeadDocumentID INT, @Now DATETIME
    SELECT @Now = dbo.fn_GetDate_Local()
    DECLARE @UserID INT
    EXEC @UserID = dbo.fn_C00_GetAutomationUser @ClientID
    
    EXEC @LeadDocumentID = dbo._C00_CreateLeadDocument @ClientID, @LeadID, @DocumentBLOB = NULL, @EmailBLOB = @MessageBytes, @LeadDocumentTitle = @OverView, @WhoUploaded = @UserID, @UploadDateTime = @Now, @FileName=NULL   
            
    DECLARE @Event dbo.tvpLeadEvent
    INSERT @Event (ClientID, LeadID, CaseID, EventTypeID, LeadDocumentID, WhenCreated, WhoCreated, EventDeleted)
    VALUES (@ClientID, @LeadID, @CaseID, @EventTypeID, @LeadDocumentID, @Now, @UserID, 0)
            
    DECLARE @BlackHole TABLE (ID INT, LeadEventID INT)  
    INSERT @BlackHole       
    EXEC dbo._C00_ApplyLeadEvent @Event
    
    SELECT *, @LeadDocumentID LeadDocumentID FROM @BlackHole


END
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialMessage__CreateEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SocialMessage__CreateEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialMessage__CreateEvent] TO [sp_executeall]
GO
