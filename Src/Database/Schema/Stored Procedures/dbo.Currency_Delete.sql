SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the Currency table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Currency_Delete]
(

	@CurrencyID int   
)
AS


				DELETE FROM [dbo].[Currency] WITH (ROWLOCK) 
				WHERE
					[CurrencyID] = @CurrencyID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Currency_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Currency_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Currency_Delete] TO [sp_executeall]
GO
