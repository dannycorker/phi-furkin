SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
----------------------------------------------------------------------------------------------------

-- Created By: Aaran Gravestock (https://www.aquarium-software.com)
-- Purpose: Select DetailFields by DetailFieldPage and LeadOrMatter
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared fields
-- MODIFIED: 2014-09-05 SB	Added IsShared as output column
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFields__GetByDetailFieldPageIDAndLeadOrMatter]
(

	@DetailFieldPageID int,
	@LeadOrMatter int,
	@ClientID INT = NULL
)
AS


				SELECT
		df.[DetailFieldID],
		df.[ClientID],
		df.[LeadTypeID],
		df.[LeadOrMatter],
		df.[FieldName],
		df.[FieldCaption],
		qt.[Name] AS [FieldType],
		ISNULL(df.[Required],0) AS IsRequired,
		ISNULL(df.[Lookup],0) AS IsLookup,
		lul.[LookupListName],
		ISNULL(df.[Enabled],0) AS IsEnabled,
		df.[FieldOrder],
		ISNULL(df.[MaintainHistory], 0) AS MaintainHistory,
		ISNULL(df.[Encrypt], 0) AS IsEncrypted,
		df.IsShared
	FROM
		dbo.fnDetailFieldsShared(@ClientID) df
		INNER JOIN QuestionTypes qt WITH (NOLOCK) ON qt.QuestionTypeID = df.QuestionTypeID
		LEFT JOIN LookupList lul WITH (NOLOCK) ON lul.LookupListID = df.LookupListID
	WHERE
		df.[DetailFieldPageID] = @DetailFieldPageID
	AND df.[LeadOrMatter] = @LeadOrMatter
	
	ORDER BY df.[FieldOrder]
	
				SELECT @@ROWCOUNT
					
			




GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetByDetailFieldPageIDAndLeadOrMatter] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetByDetailFieldPageIDAndLeadOrMatter] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetByDetailFieldPageIDAndLeadOrMatter] TO [sp_executeall]
GO
