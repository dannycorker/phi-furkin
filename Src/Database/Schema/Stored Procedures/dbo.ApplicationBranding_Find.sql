SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ApplicationBranding table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ApplicationBranding_Find]
(

	@SearchUsingOR bit   = null ,

	@ApplicationBrandingID int   = null ,

	@ClientID int   = null ,

	@FileName varchar (50)  = null ,

	@VerticalSize int   = null ,

	@HorizontalSize int   = null ,

	@LeadManagerAlias varchar (50)  = null ,

	@LeadAlias varchar (50)  = null ,

	@LeadDetailsTabImageFileName varchar (255)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ApplicationBrandingID]
	, [ClientID]
	, [FileName]
	, [VerticalSize]
	, [HorizontalSize]
	, [LeadManagerAlias]
	, [LeadAlias]
	, [LeadDetailsTabImageFileName]
    FROM
	[dbo].[ApplicationBranding] WITH (NOLOCK) 
    WHERE 
	 ([ApplicationBrandingID] = @ApplicationBrandingID OR @ApplicationBrandingID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([FileName] = @FileName OR @FileName IS NULL)
	AND ([VerticalSize] = @VerticalSize OR @VerticalSize IS NULL)
	AND ([HorizontalSize] = @HorizontalSize OR @HorizontalSize IS NULL)
	AND ([LeadManagerAlias] = @LeadManagerAlias OR @LeadManagerAlias IS NULL)
	AND ([LeadAlias] = @LeadAlias OR @LeadAlias IS NULL)
	AND ([LeadDetailsTabImageFileName] = @LeadDetailsTabImageFileName OR @LeadDetailsTabImageFileName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ApplicationBrandingID]
	, [ClientID]
	, [FileName]
	, [VerticalSize]
	, [HorizontalSize]
	, [LeadManagerAlias]
	, [LeadAlias]
	, [LeadDetailsTabImageFileName]
    FROM
	[dbo].[ApplicationBranding] WITH (NOLOCK) 
    WHERE 
	 ([ApplicationBrandingID] = @ApplicationBrandingID AND @ApplicationBrandingID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([FileName] = @FileName AND @FileName is not null)
	OR ([VerticalSize] = @VerticalSize AND @VerticalSize is not null)
	OR ([HorizontalSize] = @HorizontalSize AND @HorizontalSize is not null)
	OR ([LeadManagerAlias] = @LeadManagerAlias AND @LeadManagerAlias is not null)
	OR ([LeadAlias] = @LeadAlias AND @LeadAlias is not null)
	OR ([LeadDetailsTabImageFileName] = @LeadDetailsTabImageFileName AND @LeadDetailsTabImageFileName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ApplicationBranding_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ApplicationBranding_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ApplicationBranding_Find] TO [sp_executeall]
GO
