SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 2013-05-20
-- Description:	Writes an authorise request to the Authorise basic table
-- =============================================
CREATE PROCEDURE [dbo].[PaymentGateway__WriteAuthoriseRequest]
	@ClientID INT,
	@CustomerID INT,
	@AuthoriseRequestVendorTxCode VARCHAR(2000),
	@AuthoriseRequestTxType VARCHAR(2000),
	@AuthoriseRequestAmount VARCHAR(2000),
	@AuthoriseRequestCurrency VARCHAR(2000),
	@AuthoriseRequestDescription VARCHAR(2000),
	@AuthoriseRequestRelatedVPSTxId VARCHAR(2000),
	@AuthoriseRequestRelatedVendorTxCode VARCHAR(2000),
	@AuthoriseRequestRelatedSecurityKey VARCHAR(2000)
	
AS BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TableDetailFieldID INT,
			@TableDetailFieldPageID INT,
			@TableRowID INT

	SELECT TOP (1) @TableDetailFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ClientID = @ClientID 
	AND tpfm.ThirdPartyFieldID = 629
	
	SELECT @TableDetailFieldPageID = df.DetailFieldPageID
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @TableDetailFieldID

	INSERT INTO TableRows (ClientID, CustomerID, DetailFieldID, DetailFieldPageID)
	VALUES (@ClientID, @CustomerID, @TableDetailFieldID, @TableDetailFieldPageID)

	SELECT @TableRowID = SCOPE_IDENTITY()
	
	INSERT INTO TableDetailValues (ClientID, CustomerID, TableRowID, DetailFieldID, DetailValue)
	SELECT @ClientID, @CustomerID, @TableRowID, tpm.ColumnFieldID, CASE tpm.ThirdPartyFieldID 
																	WHEN 629 THEN @AuthoriseRequestVendorTxCode
																	WHEN 630 THEN @AuthoriseRequestTxType
																	WHEN 631 THEN @AuthoriseRequestAmount
																	WHEN 632 THEN @AuthoriseRequestCurrency
																	WHEN 633 THEN @AuthoriseRequestDescription
																	WHEN 634 THEN @AuthoriseRequestRelatedVPSTxId
																	WHEN 635 THEN @AuthoriseRequestRelatedVendorTxCode
																	WHEN 636 THEN @AuthoriseRequestRelatedSecurityKey																																
																	ELSE ''
																	END
	FROM ThirdPartyFieldMapping tpm
	WHERE tpm.ClientID = @ClientID
	AND tpm.LeadTypeID = 0
	AND tpm.ThirdPartyFieldID IN (629,630,631,632,633,634,635,636)
	
	DECLARE @AuthoriseVendorTxIDDetailFieldID INT	
	SELECT @AuthoriseVendorTxIDDetailFieldID =DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=645
	EXEC _C00_SimpleValueIntoField @AuthoriseVendorTxIDDetailFieldID, @AuthoriseRequestVendorTxCode, @CustomerID

	DECLARE @AuthoriseAmountDetailFieldID INT	
	SELECT @AuthoriseAmountDetailFieldID =DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=646
	EXEC _C00_SimpleValueIntoField @AuthoriseAmountDetailFieldID, @AuthoriseRequestAmount, @CustomerID

	DECLARE @AuthoriseCurrencyDetailFieldID INT	
	SELECT @AuthoriseCurrencyDetailFieldID =DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=647
	EXEC _C00_SimpleValueIntoField @AuthoriseCurrencyDetailFieldID, @AuthoriseRequestCurrency, @CustomerID

	DECLARE @AuthoriseDescriptionDetailFieldID INT	
	SELECT @AuthoriseDescriptionDetailFieldID =DetailFieldID FROM ThirdPartyFieldMapping WITH (NOLOCK) 
	WHERE ClientID=@ClientID AND ThirdPartyFieldID=648
	EXEC _C00_SimpleValueIntoField @AuthoriseDescriptionDetailFieldID, @AuthoriseRequestDescription, @CustomerID

	SELECT @TableRowID TableRowID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WriteAuthoriseRequest] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PaymentGateway__WriteAuthoriseRequest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PaymentGateway__WriteAuthoriseRequest] TO [sp_executeall]
GO
