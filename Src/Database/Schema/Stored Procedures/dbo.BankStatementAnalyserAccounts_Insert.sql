SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the BankStatementAnalyserAccounts table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BankStatementAnalyserAccounts_Insert]
(

	@BankStatementAnalyserAccountID int   ,

	@ClientID int   ,

	@BankStatementPageCount int   ,

	@CostPerPage money   ,

	@LastResetDate datetime   
)
AS


				
				INSERT INTO [dbo].[BankStatementAnalyserAccounts]
					(
					[BankStatementAnalyserAccountID]
					,[ClientID]
					,[BankStatementPageCount]
					,[CostPerPage]
					,[LastResetDate]
					)
				VALUES
					(
					@BankStatementAnalyserAccountID
					,@ClientID
					,@BankStatementPageCount
					,@CostPerPage
					,@LastResetDate
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[BankStatementAnalyserAccounts_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BankStatementAnalyserAccounts_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BankStatementAnalyserAccounts_Insert] TO [sp_executeall]
GO
