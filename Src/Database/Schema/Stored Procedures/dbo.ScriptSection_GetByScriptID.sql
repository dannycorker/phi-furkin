SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ScriptSection table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptSection_GetByScriptID]
(

	@ScriptID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[ScriptSectionID],
					[ScriptID],
					[ClientID],
					[SectionTitle],
					[SectionOrder],
					[WhenCreated],
					[WhoCreated],
					[WhenModified],
					[WhoModified],
					[SourceID],
					[NextEventTypeID]
				FROM
					[dbo].[ScriptSection] WITH (NOLOCK) 
				WHERE
					[ScriptID] = @ScriptID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection_GetByScriptID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptSection_GetByScriptID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptSection_GetByScriptID] TO [sp_executeall]
GO
