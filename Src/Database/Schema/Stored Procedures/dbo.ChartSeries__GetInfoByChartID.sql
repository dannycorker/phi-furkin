SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









/*
----------------------------------------------------------------------------------------------------
-- Date Created: 28 April 2008

-- Created By:  Chris Townsend
-- Purpose: Gets full info for all chart series for a specific chart
--			Full info includes Type names etc.
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartSeries__GetInfoByChartID]
(
	@ChartID int
)
AS

SELECT	cs.ChartSeriesID, 
		cs.ChartSeriesTypeID, 
		cst.ChartSeriesTypeName,
		cs.ChartSeriesOrder,
		cs.ChartID,
		cs.ClientID,
		cs.PanelItemChartingID as [PanelItemID],
		cs.ColumnName

FROM ChartSeries cs

INNER JOIN ChartSeriesType cst ON cs.ChartSeriesTypeID = cst.ChartSeriesTypeID

WHERE cs.ChartID = @ChartID





GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeries__GetInfoByChartID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartSeries__GetInfoByChartID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartSeries__GetInfoByChartID] TO [sp_executeall]
GO
