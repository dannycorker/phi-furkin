SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the LeadTypePageTitleDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleDetailFieldTarget_Update]
(

	@LeadTypePageTitleDetailFieldTargetID int   ,

	@ClientID int   ,

	@LeadTypeID int   ,

	@Target varchar (250)  ,

	@DetailFieldID int   ,

	@TemplateTypeID int   ,

	@DetailFieldAlias varchar (500)  ,

	@Notes varchar (250)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[LeadTypePageTitleDetailFieldTarget]
				SET
					[ClientID] = @ClientID
					,[LeadTypeID] = @LeadTypeID
					,[Target] = @Target
					,[DetailFieldID] = @DetailFieldID
					,[TemplateTypeID] = @TemplateTypeID
					,[DetailFieldAlias] = @DetailFieldAlias
					,[Notes] = @Notes
				WHERE
[LeadTypePageTitleDetailFieldTargetID] = @LeadTypePageTitleDetailFieldTargetID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleDetailFieldTarget_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleDetailFieldTarget_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleDetailFieldTarget_Update] TO [sp_executeall]
GO
