SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 2016-06=15
-- Description:	Get payments for allocation
-- =============================================
CREATE PROCEDURE [dbo].[Payment__GetByPaymentID]
	@PaymentID INT,
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	SELECT p.*, c.CurrencyCode, pt.PaymentTypeName
	FROM Payment p WITH (NOLOCK)
	LEFT JOIN Currency c WITH (NOLOCK) ON c.CurrencyID = p.PaymentCurrency
	LEFT JOIN PaymentTypes pt WITH (NOLOCK) ON pt.PaymentTypeID = p.PaymentTypeID
	WHERE p.PaymentID = @PaymentID AND p.ClientID = @ClientID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[Payment__GetByPaymentID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Payment__GetByPaymentID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Payment__GetByPaymentID] TO [sp_executeall]
GO
