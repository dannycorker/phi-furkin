SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the CurrencyRate table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CurrencyRate_Delete]
(

	@CurrencyRateID int   
)
AS


				DELETE FROM [dbo].[CurrencyRate] WITH (ROWLOCK) 
				WHERE
					[CurrencyRateID] = @CurrencyRateID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CurrencyRate_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CurrencyRate_Delete] TO [sp_executeall]
GO
