SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[RemoveBranchingFromQuestion]
@ClientQuestionnaireID int, @MasterQuestionID int
AS

	Update QuestionPossibleAnswers
	Set Branch=0
	Where ((ClientQuestionnaireID = @ClientQuestionnaireID) And (Branch = MasterQuestionID))








GO
GRANT VIEW DEFINITION ON  [dbo].[RemoveBranchingFromQuestion] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RemoveBranchingFromQuestion] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RemoveBranchingFromQuestion] TO [sp_executeall]
GO
