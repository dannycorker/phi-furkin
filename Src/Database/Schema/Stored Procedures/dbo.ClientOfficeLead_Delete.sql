SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientOfficeLead table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientOfficeLead_Delete]
(

	@ClientOfficeLeadID int   
)
AS


				DELETE FROM [dbo].[ClientOfficeLead] WITH (ROWLOCK) 
				WHERE
					[ClientOfficeLeadID] = @ClientOfficeLeadID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeLead_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOfficeLead_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeLead_Delete] TO [sp_executeall]
GO
