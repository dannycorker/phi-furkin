SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ThirdPartyField table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ThirdPartyField_GetByFieldName]
(

	@FieldName varchar (250)  
)
AS


				SELECT
					[ThirdPartyFieldID],
					[ThirdPartySystemId],
					[FieldName],
					[FieldDescription],
					[IsEnabled],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[ThirdPartyField]
				WHERE
					[FieldName] = @FieldName
				SELECT @@ROWCOUNT
					
			





GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyField_GetByFieldName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyField_GetByFieldName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyField_GetByFieldName] TO [sp_executeall]
GO
