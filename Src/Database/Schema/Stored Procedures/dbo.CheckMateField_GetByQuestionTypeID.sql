SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CheckMateField table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateField_GetByQuestionTypeID]
(

	@QuestionTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[CheckMateFieldID],
					[CheckMateFieldName],
					[CheckMateFieldDescription],
					[QuestionTypeID],
					[LookupListID],
					[IsEnabled],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[CheckMateField] WITH (NOLOCK) 
				WHERE
					[QuestionTypeID] = @QuestionTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateField_GetByQuestionTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateField_GetByQuestionTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateField_GetByQuestionTypeID] TO [sp_executeall]
GO
