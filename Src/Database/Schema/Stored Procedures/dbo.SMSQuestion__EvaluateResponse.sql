SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-06-23
-- Description:	Evaluates a response to return a new piece of text for matching against the rules.  e.g. return 1 or 0 to route to different responses.
--				NB - The child procs must return the evaluated value as the first column in the first record set
-- =============================================
CREATE PROCEDURE [dbo].[SMSQuestion__EvaluateResponse]
(
	@MessageText VARCHAR(2000),
	@SMSQuestionID INT, 
	@EventTypeID INT,
	@ClientID INT,
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT
)


AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @Sql NVARCHAR(2000),
			@Params NVARCHAR(2000)
	

	SELECT @Sql = 'EXEC dbo._C' + CAST(@ClientID AS VARCHAR(50)) + '_SMSQuestion__EvaluateResponse  
					@MessageText = @MessageText, 
					@SMSQuestionID = @SMSQuestionID,
					@EventTypeID = @EventTypeID,
					@ClientID = @ClientID,
					@CustomerID = @CustomerID,
					@LeadID = @LeadID,
					@CaseID = @CaseID,
					@MatterID = @MatterID'
	
	SELECT @Params = '@MessageText VARCHAR(2000),
						@SMSQuestionID INT, 
						@EventTypeID INT,
						@ClientID INT,
						@CustomerID INT,
						@LeadID INT,
						@CaseID INT,
						@MatterID INT'
					
	EXEC sp_executesql @Sql, @Params,	@MessageText,
										@SMSQuestionID, 
										@EventTypeID,
										@ClientID,
										@CustomerID,
										@LeadID,
										@CaseID,
										@MatterID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion__EvaluateResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestion__EvaluateResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestion__EvaluateResponse] TO [sp_executeall]
GO
