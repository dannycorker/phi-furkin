SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DroppedOutCustomers table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DroppedOutCustomers_Delete]
(

	@CustomerID int   
)
AS


				DELETE FROM [dbo].[DroppedOutCustomers] WITH (ROWLOCK) 
				WHERE
					[CustomerID] = @CustomerID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomers_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DroppedOutCustomers_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DroppedOutCustomers_Delete] TO [sp_executeall]
GO
