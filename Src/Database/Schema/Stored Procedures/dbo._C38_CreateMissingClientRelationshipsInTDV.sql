SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create a new client and 
-- =============================================
CREATE PROCEDURE [dbo].[_C38_CreateMissingClientRelationshipsInTDV]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Declare	@AquariumClientID int,
	@AquariumLeadID int,
	@ClientRelationshipID int,
	@LastKnownID int,
	@TestID int,
	@TableRowID int,
	@ClientRelationshipName varchar(4000),
	@OutgoingLeadTypeID int,
	@ReceivingClientID int,
	@IncomingLeadTypeID int,
	@OutgoingEventTypeID int,
	@IncomingEventTypeID int,
	@Enabled int,
	@ClearLeadRefs int
	

	DECLARE @ClientRelationShip TABLE 
	(
		ClientRelationShipID int,
		Created int, 
		AquariumLeadID int
	);

Insert Into @ClientRelationShip(ClientRelationShipID,Created, AquariumLeadID)
Select ClientRelationshipID, 0, LeadID
from ClientRelationship
Inner Join LeadDetailValues On LeadDetailValues.DetailFieldID = 6453 and DetailValue = OutgoingClientID
Where NOT EXISTS (
Select *
from TableDetailValues
Where DetailFieldID = 36709
And DetailValue = ClientRelationship.ClientRelationshipID
)
Order by ClientRelationshipID

select @LastKnownID = 0, @TestID = 0

Select Top 1 @LastKnownID = ClientRelationshipID, @AquariumLeadID = AquariumLeadID
From @ClientRelationShip 
Where Created = 0
Order by ClientRelationshipID

while (@LastKnownID > 0 AND @LastKnownID > @TestID)
	begin

Select	@AquariumClientID = OutgoingClientID,
		@ClientRelationshipName = ClientRelationshipName,
		@ReceivingClientID = ReceivingClientID,
		@OutgoingLeadTypeID = OutgoingLeadTypeID,
		@IncomingLeadTypeID = IncomingLeadTypeID,
		@OutgoingEventTypeID = OutgoingEventTypeID,
		@IncomingEventTypeID = IncomingEventTypeID,
		@Enabled = Enabled,
		@ClearLeadRefs = ClearLeadRefs
From ClientRelationship
Where ClientRelationshipID = @LastKnownID

	INSERT INTO TableRows(ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID) 
	SELECT 38, @AquariumLeadID, NULL, 36710, 4957 

	Select @TableRowID = SCOPE_IDENTITY() 
	
	INSERT INTO TableDetailValues (TableRowID, ResourceListID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID)
	SELECT @TableRowID, NULL, 36701, @ClientRelationshipName, @AquariumLeadID, 0, 38
	UNION
	SELECT @TableRowID, NULL, 36702, convert(varchar,@ReceivingClientID), @AquariumLeadID, 0, 38
	UNION
	SELECT @TableRowID, NULL, 36703, convert(varchar,@OutgoingLeadTypeID), @AquariumLeadID, 0, 38
	UNION
	SELECT @TableRowID, NULL, 36704, convert(varchar,@IncomingLeadTypeID), @AquariumLeadID, 0, 38
	UNION
	SELECT @TableRowID, NULL, 36705, convert(varchar,@OutgoingEventTypeID), @AquariumLeadID, 0, 38
	UNION
	SELECT @TableRowID, NULL, 36706, convert(varchar,@IncomingEventTypeID), @AquariumLeadID, 0, 38
	UNION
	SELECT @TableRowID, NULL, 36707, Case @Enabled When 1 then 'true' Else 'false' END, @AquariumLeadID, 0, 38
	UNION
	SELECT @TableRowID, NULL, 36708, Case @ClearLeadRefs When 1 then 'true' Else 'false' END, @AquariumLeadID, 0, 38
	UNION
	SELECT @TableRowID, NULL, 36709, convert(varchar,@LastKnownID), @AquariumLeadID, 0, 38

	Update @ClientRelationShip 
	Set Created = 1
	Where ClientRelationshipID = @LastKnownID

	set @TestID = @LastKnownID

	Select Top 1 @LastKnownID = ClientRelationshipID, @AquariumLeadID = AquariumLeadID
	From @ClientRelationShip 
	Where Created = 0
	And ClientRelationshipID > @LastKnownID
	Order by ClientRelationshipID

	END

	exec [_C38_SyncClientRelationshipChanges]



END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_CreateMissingClientRelationshipsInTDV] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_CreateMissingClientRelationshipsInTDV] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_CreateMissingClientRelationshipsInTDV] TO [sp_executeall]
GO
