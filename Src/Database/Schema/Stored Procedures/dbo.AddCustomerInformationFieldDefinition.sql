SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[AddCustomerInformationFieldDefinition]

@ClientQuestionnaireID int,
@FieldAtBeginningOfQuestionnaire bit,
@OrdinalPosition int,
@FieldName nvarchar(255),
@FieldLabel nvarchar(900),
@Mandatory bit,
@VerifyPhoneNumber bit,
@ClientID int

AS

insert into CustomerInformationFieldDefinitions (ClientQuestionnaireID,FieldAtBeginningOfQuestionnaire,OrdinalPosition,FieldName,FieldLabel, Mandatory, VerifyPhoneNumber, ClientID)
values (@ClientQuestionnaireID,@FieldAtBeginningOfQuestionnaire,@OrdinalPosition,@FieldName,@FieldLabel, @Mandatory, @VerifyPhoneNumber, @ClientID)

declare @CustomerInformationFieldDefinitionID int

set @CustomerInformationFieldDefinitionID = SCOPE_IDENTITY()

Select CustomerInformationFieldDefinitionID  from CustomerInformationFieldDefinitions where CustomerInformationFieldDefinitionID = @CustomerInformationFieldDefinitionID



GO
GRANT VIEW DEFINITION ON  [dbo].[AddCustomerInformationFieldDefinition] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AddCustomerInformationFieldDefinition] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AddCustomerInformationFieldDefinition] TO [sp_executeall]
GO
