SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2010-08-13
-- Description:	List all DetailFieldSubType records
-- =============================================
CREATE PROCEDURE [dbo].[dfs] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dfs.* 
	FROM dbo.DetailFieldSubType dfs (nolock) 
	ORDER BY dfs.DetailFieldSubTypeID ASC 
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[dfs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[dfs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[dfs] TO [sp_executeall]
GO
