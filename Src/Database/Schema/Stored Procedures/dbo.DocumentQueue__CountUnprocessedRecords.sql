SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 17-04-2012
-- Description:	Counts the number of document queue records that 
--				have not been processed yet.
-- Modified by Alex Elger 17-05-2012 Improved speed of execution!
-- =============================================
CREATE PROCEDURE [dbo].[DocumentQueue__CountUnprocessedRecords]
	
	@ClientID INT
	
AS
BEGIN


	SET NOCOUNT ON;
	
	SELECT COUNT(*) NumberOfDocumentQueueRecords
	FROM		DocumentQueue dq (NOLOCK)
	INNER JOIN	ClientPersonnel cp (NOLOCK) ON dq.WhoCreated = cp.ClientPersonnelID 
	LEFT JOIN   DocumentZip dz (NOLOCK) ON dz.[DocumentQueueID] = dq.[DocumentQueueID] 
	WHERE dq.ClientID = @ClientID 
	AND dq.[WhenCreated] IS NULL
	AND dz.[DocumentQueueID] IS NULL 
	AND dq.[RequiresApproval] = 0
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue__CountUnprocessedRecords] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentQueue__CountUnprocessedRecords] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentQueue__CountUnprocessedRecords] TO [sp_executeall]
GO
