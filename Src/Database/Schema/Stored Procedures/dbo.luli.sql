SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-03-28
-- Description:	LookupListItems helper proc
-- =============================================
CREATE PROCEDURE [dbo].[luli] 
	@LookupListItemID int = NULL,
	@LookupListID int = NULL 
AS
BEGIN
	SET NOCOUNT ON;
	
	/* Specific item lookup */
	IF @LookupListItemID > 0
	BEGIN
		SELECT * 
		FROM dbo.LookupListItems luli WITH (NOLOCK) 
		WHERE luli.LookupListItemID = @LookupListItemID 
	END
	
	/* Items for a specific list */
	IF @LookupListID > 0
	BEGIN
		SELECT 'List ' + CAST(@LookupListID AS VARCHAR) + ' Items' AS [List Items], luli.* 
		FROM dbo.LookupListItems luli WITH (NOLOCK) 
		WHERE luli.LookupListID = @LookupListID 
		ORDER BY luli.LookupListItemID 
	END
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[luli] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[luli] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[luli] TO [sp_executeall]
GO
