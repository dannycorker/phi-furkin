SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Ian Slack
-- Create date: 2014-09-24
-- Description:	Get all the base pet species
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_Search_PetSpecies]
(
	@ClientId INT,
	@ClientPersonnelId INT
)
AS
BEGIN

	SELECT	LookupListItemID Value, ItemValue Text
	FROM	LookupListItems WITH (NOLOCK) 
	WHERE	LookupListID = 3725
	AND Enabled = 1 

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_PetSpecies] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_Search_PetSpecies] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_Search_PetSpecies] TO [sp_executeall]
GO
