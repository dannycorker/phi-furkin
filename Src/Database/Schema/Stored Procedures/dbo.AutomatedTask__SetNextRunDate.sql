SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2010-05-20
-- Description:	Set the NextRunDateTime as efficiently as possible.
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedTask__SetNextRunDate] 
	@TaskID int, 
	@NextRunDateTime datetime = null 
AS
BEGIN
	
	SET NOCOUNT ON;

	UPDATE dbo.AutomatedTask 
	SET NextRunDateTime = @NextRunDateTime 
	WHERE TaskID = @TaskID 

END




GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__SetNextRunDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTask__SetNextRunDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTask__SetNextRunDate] TO [sp_executeall]
GO
