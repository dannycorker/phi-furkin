SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ReportTables table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReportTables_Find]
(

	@SearchUsingOR bit   = null ,

	@ReportTableID int   = null ,

	@ReportTableName varchar (50)  = null ,

	@DisplayOrder int   = null ,

	@RequiresClientSecurity bit   = null ,

	@IsLookupTable bit   = null ,

	@ShowAquariumValues bit   = null ,

	@WhenCreated datetime   = null ,

	@WhenModified datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ReportTableID]
	, [ReportTableName]
	, [DisplayOrder]
	, [RequiresClientSecurity]
	, [IsLookupTable]
	, [ShowAquariumValues]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[ReportTables] WITH (NOLOCK) 
    WHERE 
	 ([ReportTableID] = @ReportTableID OR @ReportTableID IS NULL)
	AND ([ReportTableName] = @ReportTableName OR @ReportTableName IS NULL)
	AND ([DisplayOrder] = @DisplayOrder OR @DisplayOrder IS NULL)
	AND ([RequiresClientSecurity] = @RequiresClientSecurity OR @RequiresClientSecurity IS NULL)
	AND ([IsLookupTable] = @IsLookupTable OR @IsLookupTable IS NULL)
	AND ([ShowAquariumValues] = @ShowAquariumValues OR @ShowAquariumValues IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhenModified] = @WhenModified OR @WhenModified IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ReportTableID]
	, [ReportTableName]
	, [DisplayOrder]
	, [RequiresClientSecurity]
	, [IsLookupTable]
	, [ShowAquariumValues]
	, [WhenCreated]
	, [WhenModified]
    FROM
	[dbo].[ReportTables] WITH (NOLOCK) 
    WHERE 
	 ([ReportTableID] = @ReportTableID AND @ReportTableID is not null)
	OR ([ReportTableName] = @ReportTableName AND @ReportTableName is not null)
	OR ([DisplayOrder] = @DisplayOrder AND @DisplayOrder is not null)
	OR ([RequiresClientSecurity] = @RequiresClientSecurity AND @RequiresClientSecurity is not null)
	OR ([IsLookupTable] = @IsLookupTable AND @IsLookupTable is not null)
	OR ([ShowAquariumValues] = @ShowAquariumValues AND @ShowAquariumValues is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhenModified] = @WhenModified AND @WhenModified is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTables_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReportTables_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReportTables_Find] TO [sp_executeall]
GO
