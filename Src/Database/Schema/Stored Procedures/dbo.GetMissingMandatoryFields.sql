SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2007-08-17
-- Description:	List missing mandatory fields for a Lead and Case
-- Modified: 2010-12-2, Paul Richardson
-- Modified Description: Added Case and Customer Detail Values
-- Modified: 2013-05-13, Paul Richardson
-- Modified Description: Added Question Type so that basic table rows can be checked
-- Modified: 2013-07-17 tr lookup to include clientid in some instances. 
--				2014-10-21	SB	Changes to allow helper fields to work across linkes leads Zendesk #29131
-- JWG 2015-02-06 #30994 Lookup the CustomerID
-- =============================================
CREATE PROCEDURE [dbo].[GetMissingMandatoryFields]  
	@EventTypeID int, 
	@LeadID int, 
	@CaseID int, 
	@CustomerID int = null,
	@ClientID int = null, 
	@UserID int = null	
AS
BEGIN
	
	SET NOCOUNT ON;
	
	/* JWG 2015-02-06 #30994 Add CustomerID to the lookup */
	SELECT @ClientID = l.ClientID, 
	@CustomerID = l.CustomerID
	FROM dbo.Lead l WITH (NOLOCK) 
	WHERE l.LeadID = @LeadID

	-- Get all related linked leads / cases
	DECLARE @LinkedRecords TABLE
	(
		ID INT IDENTITY,
		LeadID INT,
		CaseID INT,
		LeadTypeID INT,
		LeadRowNum INT,
		CaseRowNum INT
	)
	
	INSERT @LinkedRecords (LeadID, CaseID, LeadTypeID)
	SELECT @LeadID, @CaseID, LeadTypeID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID
	
	INSERT @LinkedRecords (LeadID, CaseID, LeadTypeID)
	EXEC dbo.LeadTypeRelationship__GetRelated @LeadID, @CaseID
	
	-- Flag duplicates so we can just select where rn = 1 
	;WITH InnerSql AS 
	(
		SELECT ID, LeadID, CaseID, LeadTypeID, 
		ROW_NUMBER() OVER(PARTITION BY LeadID ORDER BY ID) as LeadRowNum,
		ROW_NUMBER() OVER(PARTITION BY CaseID ORDER BY ID) as CaseRowNum 
		FROM @LinkedRecords
	)
	
	UPDATE l
	SET l.LeadRowNum = i.LeadRowNum,
		l.CaseRowNum = i.CaseRowNum
	FROM @LinkedRecords l
	INNER JOIN InnerSql i ON l.ID = i.ID
	
	-- List all missing mandatory Lead and Matter fields
	-- Normal values must not be blank
	-- ResourceList Values must not be zero
	-- In the second part of the UNION, we need Matter for the result set, 
	-- but it's not linked to the other tables. It is filtered by the 
	-- parameters @LeadID and @CaseID in the WHERE clause below.	
	
	DECLARE @OutputTable TABLE 
	(
		MissingType VARCHAR(100), 
		FieldCaption VARCHAR(200), 
		MatterRefLetter VARCHAR(10), 
		DetailFieldID INT, 
		MatterID INT, 
		PageDisplayOrder INT, 
		FieldDisplayOrder INT, 
		FieldSize INT, 
		QuestionTypeID INT,
		LeadID INT,
		CaseID INT
	)
	
	IF EXISTS (SELECT * FROM EventTypeMandatoryField e WITH (NOLOCK) 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = e.DetailFieldID and df.LeadOrMatter = 12
		WHERE e.EventTypeID = @EventTypeID)
	BEGIN
	
		INSERT INTO @OutputTable (MissingType, FieldCaption, MatterRefLetter, DetailFieldID, MatterID, PageDisplayOrder, FieldDisplayOrder, FieldSize, QuestionTypeID, LeadID, CaseID)
		SELECT 'Client Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], '' as [MatterRefLetter], df.DetailFieldID, 0 as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], df.FieldSize as [FieldSize], df.QuestionTypeID [QuestionTypeID], 0 AS LeadID, 0 AS CaseID
		FROM dbo.EventTypeMandatoryField etmf (nolock) 
		INNER JOIN dbo.DetailFields df (nolock) ON etmf.DetailFieldID = df.DetailFieldID 
		INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
		WHERE etmf.EventTypeID = @EventTypeID
		AND df.LeadOrMatter = 12
		AND df.Enabled = 1 
		AND NOT EXISTS(
			SELECT * 
			FROM dbo.ClientDetailValues cldv (nolock) 
			WHERE cldv.ClientID = @ClientID 
			AND cldv.DetailFieldID = df.DetailFieldID 
			AND ((df.QuestionTypeID = 4 AND cldv.ValueInt > 0) OR (df.QuestionTypeID = 14 AND cldv.ValueInt > 0) OR (df.QuestionTypeID <> 4 AND df.QuestionTypeID <> 14 AND cldv.DetailValue <> ''))
		)		

	END

	IF EXISTS (SELECT * FROM EventTypeMandatoryField e WITH (NOLOCK) 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = e.DetailFieldID and df.LeadOrMatter = 13
		WHERE e.EventTypeID = @EventTypeID)
	BEGIN
	
		INSERT INTO @OutputTable (MissingType, FieldCaption, MatterRefLetter, DetailFieldID, MatterID, PageDisplayOrder, FieldDisplayOrder, FieldSize, QuestionTypeID, LeadID, CaseID)
		SELECT 'Client Personnel Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], '' as [MatterRefLetter], df.DetailFieldID, 0 as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], df.FieldSize as [FieldSize], df.QuestionTypeID [QuestionTypeID], 0 AS LeadID, 0 AS CaseID
		FROM dbo.EventTypeMandatoryField cpdmf (nolock) 
		INNER JOIN dbo.DetailFields df (nolock) ON cpdmf.DetailFieldID = df.DetailFieldID 
		INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
		WHERE cpdmf.EventTypeID = 40244
		AND df.LeadOrMatter = 13
		AND df.Enabled = 1 
		AND NOT EXISTS(
			SELECT * 
			FROM dbo.ClientPersonnelDetailValues clpdv (nolock) 
			WHERE clpdv.ClientPersonnelID = @UserID
			AND clpdv.DetailFieldID = df.DetailFieldID 
			AND (
					(df.QuestionTypeID IN (4, 14) AND clpdv.ValueInt > 0) 
					OR 
					(df.QuestionTypeID NOT IN (4, 14) AND clpdv.DetailValue <> '')
				)
		)
		AND ((df.QuestionTypeID <> 19) OR (df.QuestionTypeID = 19 AND NOT EXISTS (SELECT * FROM dbo.TableRows tr WITH (NOLOCK) WHERE tr.DetailFieldID = df.DetailFieldID AND tr.ClientPersonnelID = @UserID AND tr.ClientID = @ClientID)))

	END

	IF EXISTS (SELECT * FROM EventTypeMandatoryField e WITH (NOLOCK) 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = e.DetailFieldID and df.LeadOrMatter = 1
		WHERE e.EventTypeID = @EventTypeID)
	BEGIN
	

		INSERT INTO @OutputTable (MissingType, FieldCaption, MatterRefLetter, DetailFieldID, MatterID, PageDisplayOrder, FieldDisplayOrder, FieldSize, QuestionTypeID, LeadID, CaseID)
		SELECT 'Lead Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], '' as [MatterRefLetter], df.DetailFieldID, 0 as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], df.FieldSize as [FieldSize], df.QuestionTypeID [QuestionTypeID], r.LeadID, r.CaseID
		FROM dbo.EventTypeMandatoryField etmf (nolock) 
		INNER JOIN dbo.fnDetailFieldsShared(@ClientID) df ON etmf.DetailFieldID = df.DetailFieldID 
		INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
		INNER JOIN @LinkedRecords r ON df.LeadTypeID = r.LeadTypeID AND r.LeadRowNum = 1
		WHERE etmf.EventTypeID = @EventTypeID
		AND df.LeadOrMatter = 1
		AND df.Enabled = 1 
		AND NOT EXISTS(
			SELECT * 
			FROM dbo.LeadDetailValues ldv (nolock) 
			WHERE ldv.LeadID IN (SELECT r.LeadID FROM @LinkedRecords r WHERE r.LeadTypeID = df.LeadTypeID)
			AND ldv.DetailFieldID = df.DetailFieldID 
			AND 
			(
				(df.QuestionTypeID IN (4, 14) AND ldv.ValueInt > 0) 
				OR 
				(df.QuestionTypeID NOT IN (4, 14) AND ldv.DetailValue <> '')
			)
		)
		/* AND this is a basic table (19) without any rows */
		AND ((df.QuestionTypeID <> 19) OR (df.QuestionTypeID = 19 AND NOT EXISTS (SELECT * FROM dbo.TableRows tr WITH (NOLOCK) WHERE tr.DetailFieldID = df.DetailFieldID AND tr.LeadID IN (SELECT r.LeadID FROM @LinkedRecords r WHERE r.LeadTypeID = df.LeadTypeID)))) 

	END

	IF EXISTS (SELECT * FROM EventTypeMandatoryField e WITH (NOLOCK) 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = e.DetailFieldID and df.LeadOrMatter = 10
		WHERE e.EventTypeID = @EventTypeID)
	BEGIN
	
		INSERT INTO @OutputTable (MissingType, FieldCaption, MatterRefLetter, DetailFieldID, MatterID, PageDisplayOrder, FieldDisplayOrder, FieldSize, QuestionTypeID, LeadID, CaseID)
		SELECT 'Customer Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], '' as [MatterRefLetter], df.DetailFieldID, 0 as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], df.FieldSize as [FieldSize], df.QuestionTypeID [QuestionTypeID], 0 AS LeadID, 0 AS CaseID
		FROM dbo.EventTypeMandatoryField etmf (nolock) 
		INNER JOIN dbo.DetailFields df (nolock) ON etmf.DetailFieldID = df.DetailFieldID 
		INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
		WHERE etmf.EventTypeID = @EventTypeID
		AND df.LeadOrMatter = 10
		AND df.Enabled = 1 
		AND NOT EXISTS(
			SELECT * 
			FROM dbo.CustomerDetailValues cdv (nolock) 
			WHERE cdv.CustomerID = @CustomerID 
			AND cdv.DetailFieldID = df.DetailFieldID 
			AND ((df.QuestionTypeID = 4 AND cdv.ValueInt >0) OR (df.QuestionTypeID = 14 AND cdv.ValueInt > 0 ) OR (df.QuestionTypeID <> 4 AND df.QuestionTypeID <> 14 AND cdv.DetailValue <> ''))
		)	
		AND ((df.QuestionTypeID <> 19) OR (df.QuestionTypeID = 19 AND NOT EXISTS (SELECT * FROM dbo.TableRows tr WITH (NOLOCK) WHERE tr.DetailFieldID = df.DetailFieldID AND tr.CustomerID = @CustomerID)))

	END

	IF EXISTS (SELECT * FROM EventTypeMandatoryField e WITH (NOLOCK) 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = e.DetailFieldID and df.LeadOrMatter = 11
		WHERE e.EventTypeID = @EventTypeID)
	BEGIN
	
		INSERT INTO @OutputTable (MissingType, FieldCaption, MatterRefLetter, DetailFieldID, MatterID, PageDisplayOrder, FieldDisplayOrder, FieldSize, QuestionTypeID, LeadID, CaseID)
		SELECT 'Case Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], '' as [MatterRefLetter], df.DetailFieldID, 0 as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], df.FieldSize as [FieldSize], df.QuestionTypeID [QuestionTypeID], r.LeadID, r.CaseID
		FROM dbo.EventTypeMandatoryField etmf (nolock) 
		INNER JOIN dbo.fnDetailFieldsShared(@ClientID) df ON etmf.DetailFieldID = df.DetailFieldID 
		INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
		INNER JOIN @LinkedRecords r ON df.LeadTypeID = r.LeadTypeID AND r.CaseRowNum = 1
		WHERE etmf.EventTypeID = @EventTypeID
		AND df.LeadOrMatter = 11
		AND df.Enabled = 1 
		AND NOT EXISTS(
			SELECT * 
			FROM dbo.CaseDetailValues csdv (nolock) 
			WHERE csdv.CaseID IN (SELECT r.CaseID FROM @LinkedRecords r WHERE r.LeadTypeID = df.LeadTypeID)
			AND csdv.DetailFieldID = df.DetailFieldID 
			AND ((df.QuestionTypeID IN (4,14) AND csdv.ValueInt > 0) OR (df.QuestionTypeID NOT IN (4,14) AND csdv.DetailValue <> ''))
		)		
		AND ((df.QuestionTypeID <> 19) OR (df.QuestionTypeID = 19 AND NOT EXISTS (SELECT * FROM dbo.TableRows tr WITH (NOLOCK) WHERE tr.DetailFieldID = df.DetailFieldID AND tr.CaseID IN (SELECT r.CaseID FROM @LinkedRecords r WHERE r.LeadTypeID = df.LeadTypeID) AND tr.ClientID = @ClientID)))

	END

	IF EXISTS (SELECT * FROM EventTypeMandatoryField e WITH (NOLOCK) 
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = e.DetailFieldID and df.LeadOrMatter = 2
		WHERE e.EventTypeID = @EventTypeID)
	BEGIN
	
		INSERT INTO @OutputTable (MissingType, FieldCaption, MatterRefLetter, DetailFieldID, MatterID, PageDisplayOrder, FieldDisplayOrder, FieldSize, QuestionTypeID, LeadID, CaseID)
		SELECT 'Matter Details' as [MissingType], df.FieldCaption + ' ( ' + dfp.PageCaption + ' )' as [FieldCaption], m1.RefLetter as [MatterRefLetter], df.DetailFieldID, m1.MatterID as [MatterID], dfp.PageOrder as [PageDisplayOrder], df.FieldOrder as [FieldDisplayOrder], df.FieldSize as [FieldSize], df.QuestionTypeID [QuestionTypeID], m1.LeadID, m1.CaseID
		FROM dbo.EventTypeMandatoryField etmf (nolock) 
		INNER JOIN dbo.fnDetailFieldsShared(@ClientID) df ON etmf.DetailFieldID = df.DetailFieldID
		INNER JOIN dbo.DetailFieldPages dfp (nolock) ON dfp.DetailFieldPageID = df.DetailFieldPageID 
		INNER JOIN dbo.Matter m1 (nolock) 
			ON m1.LeadID IN (SELECT r.LeadID FROM @LinkedRecords r WHERE r.LeadTypeID = df.LeadTypeID) 
			AND m1.CaseID IN (SELECT r.CaseID FROM @LinkedRecords r WHERE r.LeadTypeID = df.LeadTypeID)
		WHERE etmf.EventTypeID = @EventTypeID
		AND df.LeadOrMatter = 2
		AND df.Enabled = 1 
		AND NOT EXISTS(
			SELECT * 
			FROM dbo.MatterDetailValues mdv (nolock) 
			INNER JOIN dbo.Matter m (nolock) ON mdv.MatterID = m.MatterID
			INNER JOIN dbo.Cases c (nolock) ON m.CaseID = c.CaseID
			WHERE mdv.LeadID IN (SELECT r.LeadID FROM @LinkedRecords r WHERE r.LeadTypeID = df.LeadTypeID) 
			AND mdv.MatterID = m1.MatterID
			AND c.CaseID IN (SELECT r.CaseID FROM @LinkedRecords r WHERE r.LeadTypeID = df.LeadTypeID)
			AND mdv.DetailFieldID = df.DetailFieldID 
			AND ((df.QuestionTypeID = 4 AND mdv.ValueInt > 0) OR (df.QuestionTypeID = 14 AND mdv.ValueInt > 0) OR (df.QuestionTypeID <> 4 AND df.QuestionTypeID <> 14 AND mdv.DetailValue <> ''))
		)
		AND ((df.QuestionTypeID <> 19) OR (df.QuestionTypeID = 19 AND NOT EXISTS (SELECT * FROM dbo.TableRows tr WITH (NOLOCK) WHERE tr.DetailFieldID = df.DetailFieldID AND tr.MatterID = m1.MatterID))) 

	END
	
	/* 
		JWG 2013-09-11 #22422 
		Only show headers if there are any other missing fields. 
		The QuestionTypeID for a header field is 25. 
	*/
	IF EXISTS(SELECT * FROM @OutputTable ot WHERE ot.QuestionTypeID NOT IN (25))
	BEGIN
		/* Show headers and fields together if any blank fields exist */
		SELECT *
		FROM @OutputTable
		ORDER BY [MissingType], [MatterRefLetter], [PageDisplayOrder], [FieldDisplayOrder]
	END
	ELSE
	BEGIN
		/* 
			Otherwise there are no fields at all to return, or only header fields.
			Keep the shape of the datatable that the app expects, but guarantee that no rows are returned 
		*/
		SELECT *
		FROM @OutputTable
		WHERE 1=2
	END
	
	/* Some callers need the count back as the return code */
	RETURN @@ROWCOUNT
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[GetMissingMandatoryFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetMissingMandatoryFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetMissingMandatoryFields] TO [sp_executeall]
GO
