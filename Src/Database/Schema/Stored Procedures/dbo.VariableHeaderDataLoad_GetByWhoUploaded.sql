SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the VariableHeaderDataLoad table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[VariableHeaderDataLoad_GetByWhoUploaded]
(

	@WhoUploaded int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[VariableHeaderDataLoadID],
					[ClientID],
					[ListID],
					[ImportID],
					[TokenKey],
					[CallbackMethod],
					[ActionToPerform],
					[DeleteListRecords],
					[EmailFailureReport],
					[EmailAddress],
					[FileName],
					[WhoUploaded],
					[DateTimeOfUpload],
					[Processed],
					[Notes],
					[DateTimeStart],
					[DateTimeEnd],
					[LeadTypeID]
				FROM
					[dbo].[VariableHeaderDataLoad] WITH (NOLOCK) 
				WHERE
					[WhoUploaded] = @WhoUploaded
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad_GetByWhoUploaded] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[VariableHeaderDataLoad_GetByWhoUploaded] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[VariableHeaderDataLoad_GetByWhoUploaded] TO [sp_executeall]
GO
