SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EventTypeAccessRule table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAccessRule_Delete]
(

	@EventTypeAccessRuleID int   
)
AS


				DELETE FROM [dbo].[EventTypeAccessRule] WITH (ROWLOCK) 
				WHERE
					[EventTypeAccessRuleID] = @EventTypeAccessRuleID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAccessRule_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAccessRule_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAccessRule_Delete] TO [sp_executeall]
GO
