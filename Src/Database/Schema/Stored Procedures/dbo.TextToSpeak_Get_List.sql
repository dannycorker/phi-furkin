SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the TextToSpeak table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[TextToSpeak_Get_List]

AS


				
				SELECT
					[TextToSpeakID],
					[ClientQuestionnaireID],
					[ClientID],
					[PageNumber],
					[SpeakText],
					[IsShown]
				FROM
					[dbo].[TextToSpeak] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[TextToSpeak_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TextToSpeak_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TextToSpeak_Get_List] TO [sp_executeall]
GO
