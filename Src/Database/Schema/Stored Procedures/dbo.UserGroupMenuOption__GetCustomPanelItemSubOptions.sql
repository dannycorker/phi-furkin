SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		 Alex Elger
-- Create date:  2015-03-24
-- Description:	 Gets a list of custom panel requirements for a particular User Group #31839
-- CS 2016-03-04 Proof of Concept using Override table.  Can be wiped out at diff
-- =============================================
CREATE PROCEDURE [dbo].[UserGroupMenuOption__GetCustomPanelItemSubOptions]
(
	@ClientPersonnelAdminGroupID int,
	@ClientPersonnelID int,
	@ClientID int,
	@UserGroupMenuOptionID INT
)
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @QueryText VARCHAR(MAX)

	/*Allow iReporting reports to override the content of a particular menu item*/
	SELECT @QueryText = REPLACE(
						REPLACE(
						REPLACE(
						REPLACE(sq.QueryText,'@ClientPersonnelAdminGroupID',CONVERT(VARCHAR,@ClientPersonnelAdminGroupID))
								,'@ClientPersonnelID',CONVERT(VARCHAR,@ClientPersonnelID))
								,'@ClientID',CONVERT(VARCHAR,@ClientID))
								,'@UserGroupMenuOptionID',CONVERT(VARCHAR,@UserGroupMenuOptionID))
	FROM Overrides ovr WITH ( NOLOCK ) 
	INNER JOIN SqlQuery sq WITH ( NOLOCK ) on sq.QueryID = ovr.QueryID
	WHERE ovr.ClientID = @ClientID
	AND ovr.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID
	AND ovr.OverrideTypeID = 5

	/*If we found a report, run that instead.  The report must have an output to match sections below*/
	IF @QueryText <> ''
	BEGIN
		PRINT @QueryText
		EXEC (@QueryText)
	END 
	ELSE
	/*If there are any for my client, page and admin group show them, otherwise try for client and page*/
	IF EXISTS (
		SELECT *
		FROM dbo.UserGroupMenuSubOption pugmo WITH (NOLOCK) 
		WHERE pugmo.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID 
		AND pugmo.ClientID = @ClientID
		AND pugmo.UserGroupMenuOptionID = @UserGroupMenuOptionID
	)
	BEGIN

		SELECT UserGroupMenuSubOptionID, UserGroupMenuOptionID, ClientID, ClientPersonnelAdminGroupID, PanelItemName, PanelItemCaption, PanelItemURL, ShowInFancyBox
		FROM dbo.UserGroupMenuSubOption pugmo WITH (NOLOCK) 
		WHERE pugmo.ClientPersonnelAdminGroupID = @ClientPersonnelAdminGroupID 
		AND pugmo.ClientID = @ClientID
		AND pugmo.UserGroupMenuOptionID = @UserGroupMenuOptionID
		ORDER BY pugmo.SubItemOrder
	
	END
	ELSE
	BEGIN
	
		SELECT UserGroupMenuSubOptionID, UserGroupMenuOptionID, ClientID, ClientPersonnelAdminGroupID, PanelItemName, PanelItemCaption, PanelItemURL, ShowInFancyBox
		FROM dbo.UserGroupMenuSubOption pugmo WITH (NOLOCK) 
		WHERE pugmo.ClientID = @ClientID
		AND pugmo.ClientPersonnelAdminGroupID IS NULL
		AND pugmo.UserGroupMenuOptionID = @UserGroupMenuOptionID
		ORDER BY pugmo.SubItemOrder

	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption__GetCustomPanelItemSubOptions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserGroupMenuOption__GetCustomPanelItemSubOptions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserGroupMenuOption__GetCustomPanelItemSubOptions] TO [sp_executeall]
GO
