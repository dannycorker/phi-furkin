SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-10-20
-- Description:	Return altered filenames for some clients (eg LeadID as prefix) 
-- =============================================
CREATE PROCEDURE [dbo].[DocumentZip__GenerateFileName]
	@FileName VARCHAR(200),
	@ClientID INT = NULL,
	@LeadID INT = NULL,
	@CaseID INT = NULL,
	@EventTypeID INT = NULL
AS
BEGIN

	DECLARE @Prefix VARCHAR(50),
			@MatterID INT,
			@DetailValue VARCHAR(2000),
			@ValueInt INT,
			@EventSubtypeID INT,
			@EventTypeName VARCHAR(200),
			@DocumentTypeID INT,
			@LogEntry varchar(2000) = ''
			,@LeadTypeID INT
	
	SET NOCOUNT ON;
	
	SELECT @LogEntry =	'@Filename = [' + @FileName + 
						'], @ClientID = [' + convert(varchar,@ClientID) + 
						'], @LeadID = [' + convert(varchar,@LeadID) + 
						'], @CaseID = [' + convert(varchar,@CaseID) +
						'], @EventTypeID = [' + convert(varchar,@EventTypeID) +  
						']' 
	
	SELECT @LogEntry = @LogEntry + ', @Filename = [' + ISNULL(@FileName,'') + ']'

	EXEC _C00_LogIt 'Info', 'DocumentZip__GenerateFileName', 'DocumentZip__GenerateFileName', @LogEntry, 3848

	/* The default for most clients is the name they passed in */
	SELECT @FileName AS [FileName]
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZip__GenerateFileName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentZip__GenerateFileName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentZip__GenerateFileName] TO [sp_executeall]
GO
