SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2008-04-01
-- Description:	Cancel all SqlQuery editing from temp/work tables
-- =============================================
CREATE PROCEDURE [dbo].[CancelAllSqlQueryForEditing]
AS
BEGIN
	SET NOCOUNT ON;

	-- Let the database cascade the deletes to all tables!
	DELETE SqlQueryEditing 

END







GO
GRANT VIEW DEFINITION ON  [dbo].[CancelAllSqlQueryForEditing] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CancelAllSqlQueryForEditing] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CancelAllSqlQueryForEditing] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[CancelAllSqlQueryForEditing] TO [sp_executehelper]
GO
