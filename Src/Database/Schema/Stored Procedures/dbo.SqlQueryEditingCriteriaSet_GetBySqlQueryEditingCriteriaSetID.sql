SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditingCriteriaSet table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingCriteriaSet_GetBySqlQueryEditingCriteriaSetID]
(

	@SqlQueryEditingCriteriaSetID int   
)
AS


				SELECT
					[SqlQueryEditingCriteriaSetID],
					[ClientID],
					[SqlQueryEditingID],
					[SetName],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified]
				FROM
					[dbo].[SqlQueryEditingCriteriaSet] WITH (NOLOCK) 
				WHERE
										[SqlQueryEditingCriteriaSetID] = @SqlQueryEditingCriteriaSetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSet_GetBySqlQueryEditingCriteriaSetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingCriteriaSet_GetBySqlQueryEditingCriteriaSetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingCriteriaSet_GetBySqlQueryEditingCriteriaSetID] TO [sp_executeall]
GO
