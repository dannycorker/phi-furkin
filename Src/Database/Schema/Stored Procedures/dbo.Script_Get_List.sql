SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Script table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Script_Get_List]

AS


				
				SELECT
					[ScriptID],
					[ClientID],
					[LeadTypeID],
					[ScriptName],
					[ScriptDescription],
					[WhenCreated],
					[WhoCreated],
					[WhenModified],
					[WhoModified],
					[FinishEventTypeID],
					[Published],
					[Disabled],
					[ScriptParentID],
					[StartEventTypeID],
					[ShowNotes],
					[NumberOfNotesToShow],
					[IntegrateWithDialler],
					[ApplyFinishEventTo],
					[Redact]
				FROM
					[dbo].[Script] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Script_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script_Get_List] TO [sp_executeall]
GO
