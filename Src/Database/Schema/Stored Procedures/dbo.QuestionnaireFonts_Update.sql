SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the QuestionnaireFonts table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionnaireFonts_Update]
(

	@QuestionnaireFontID int   ,

	@ClientQuestionnaireID int   ,

	@PartNameID int   ,

	@FontFamily varchar (50)  ,

	@FontSize varchar (50)  ,

	@FontColour varchar (50)  ,

	@FontWeight varchar (50)  ,

	@FontAlignment varchar (50)  ,

	@ClientID int   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[QuestionnaireFonts]
				SET
					[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[PartNameID] = @PartNameID
					,[FontFamily] = @FontFamily
					,[FontSize] = @FontSize
					,[FontColour] = @FontColour
					,[FontWeight] = @FontWeight
					,[FontAlignment] = @FontAlignment
					,[ClientID] = @ClientID
				WHERE
[QuestionnaireFontID] = @QuestionnaireFontID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFonts_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionnaireFonts_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionnaireFonts_Update] TO [sp_executeall]
GO
