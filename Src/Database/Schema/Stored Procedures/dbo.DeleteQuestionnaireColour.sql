SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.DeleteQuestionnaireColour    Script Date: 08/09/2006 12:22:37 ******/

CREATE PROCEDURE [dbo].[DeleteQuestionnaireColour] @QuestionnaireColourID int

AS

Delete From QuestionnaireColours
Where QuestionnaireColourID = @QuestionnaireColourID




GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteQuestionnaireColour] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteQuestionnaireColour] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteQuestionnaireColour] TO [sp_executeall]
GO
