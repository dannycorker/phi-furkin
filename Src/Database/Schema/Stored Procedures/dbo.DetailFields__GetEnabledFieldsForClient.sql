SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-10-21
-- Description:	Returns fields of a specific type for a client and lead type. Optionaly exclude tables and include linked leads
-- =============================================
CREATE PROCEDURE [dbo].[DetailFields__GetEnabledFieldsForClient] 

	@ClientID INT, 
	@LeadTypeID INT, 
	@LeadOrMatter INT,
	@IncludeResourceListTables BIT = 0,
	@IncludeLinkedLeads BIT = 1

AS
BEGIN
	
	DECLARE @LeadTypes TABLE (LeadTypeID INT)
	INSERT @LeadTypes(LeadTypeID)
	VALUES (@LeadTypeID)
	
	-- 1 = Lead, 2 = Matter, 11 = Case
	IF @LeadOrMatter IN (1, 2, 11)
	BEGIN
	
		INSERT @LeadTypes (LeadTypeID)
		SELECT	CASE
					WHEN d.FromLeadTypeID = @LeadTypeID THEN d.ToLeadTypeID
					ELSE d.FromLeadTypeID 
				END
		FROM dbo.LeadTypeRelationshipDefinition d WITH (NOLOCK)
		WHERE (d.FromLeadTypeID = @LeadTypeID OR d.ToLeadTypeID = @LeadTypeID)
		AND 
		(
			(@LeadOrMatter = 1 AND d.LeadsAreLinked = 1) OR
			(@LeadOrMatter = 2 AND d.MattersAreLinked = 1) OR
			(@LeadOrMatter = 11 AND d.CasesAreLinked = 1)
		)
	
	END
				
	SELECT *
	FROM dbo.fnDetailFieldsShared(@ClientID)
	WHERE ClientID = @ClientID 
	AND LeadTypeID IN (SELECT LeadTypeID FROM @LeadTypes)
	AND LeadOrMatter = @LeadOrMatter 
	AND Enabled = 1 
	AND (@IncludeResourceListTables = 1 OR QuestionTypeID <> 16)
	ORDER BY FieldName

	SELECT @@ROWCOUNT

END






GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetEnabledFieldsForClient] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFields__GetEnabledFieldsForClient] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFields__GetEnabledFieldsForClient] TO [sp_executeall]
GO
