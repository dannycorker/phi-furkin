SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the FAS_AmountOfCredit table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[FAS_AmountOfCredit_Get_List]

AS


				
				SELECT
					[FasAmountOfCreditID],
					[FasCheckerDataID],
					[Description],
					[CostAmount]
				FROM
					[dbo].[FAS_AmountOfCredit] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_AmountOfCredit_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[FAS_AmountOfCredit_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[FAS_AmountOfCredit_Get_List] TO [sp_executeall]
GO
