SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the DocumentDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentDetailFieldTarget_Delete]
(

	@DocumentDetailFieldTargetID int   
)
AS


				DELETE FROM [dbo].[DocumentDetailFieldTarget] WITH (ROWLOCK) 
				WHERE
					[DocumentDetailFieldTargetID] = @DocumentDetailFieldTargetID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentDetailFieldTarget_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentDetailFieldTarget_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentDetailFieldTarget_Delete] TO [sp_executeall]
GO
