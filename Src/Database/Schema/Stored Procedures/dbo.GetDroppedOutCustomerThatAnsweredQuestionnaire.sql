SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetDroppedOutCustomerThatAnsweredQuestionnaire] @CustomerID int, @ClientQuestionnaireID int

AS

SELECT     dbo.DroppedOutCustomers.CustomerID, dbo.DroppedOutCustomers.ClientID, dbo.DroppedOutCustomers.TitleID, dbo.DroppedOutCustomers.FirstName, dbo.DroppedOutCustomers.MiddleName, 
                      dbo.DroppedOutCustomers.LastName, dbo.DroppedOutCustomers.EmailAddress, dbo.DroppedOutCustomers.DayTimeTelephoneNumber, dbo.DroppedOutCustomers.HomeTelephone, 
                      dbo.DroppedOutCustomers.MobileTelephone, dbo.DroppedOutCustomers.CompanyTelephone, dbo.DroppedOutCustomers.WorksTelephone, 
                      dbo.DroppedOutCustomers.Address1, dbo.DroppedOutCustomers.Address2, dbo.DroppedOutCustomers.Town, dbo.DroppedOutCustomers.County, 
                      dbo.DroppedOutCustomers.PostCode, dbo.DroppedOutCustomers.Test, dbo.DroppedOutCustomers.CompanyName, dbo.DroppedOutCustomers.Occupation, dbo.DroppedOutCustomers.Employer, 
                      dbo.DroppedOutCustomers.ReminderEmailSentDate, dbo.DroppedOutCustomers.ReminderSmsSentDate, dbo.DroppedOutCustomerQuestionnaires.CustomerQuestionnaireID, dbo.DroppedOutCustomerQuestionnaires.ClientQuestionnaireID
FROM         dbo.DroppedOutCustomers INNER JOIN
                      dbo.DroppedOutCustomerQuestionnaires ON dbo.DroppedOutCustomers.CustomerID = dbo.DroppedOutCustomerQuestionnaires.CustomerID
WHERE     (dbo.DroppedOutCustomerQuestionnaires.ClientQuestionnaireID = @ClientQuestionnaireID) AND (dbo.DroppedOutCustomers.CustomerID = @CustomerID)



GO
GRANT VIEW DEFINITION ON  [dbo].[GetDroppedOutCustomerThatAnsweredQuestionnaire] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetDroppedOutCustomerThatAnsweredQuestionnaire] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetDroppedOutCustomerThatAnsweredQuestionnaire] TO [sp_executeall]
GO
