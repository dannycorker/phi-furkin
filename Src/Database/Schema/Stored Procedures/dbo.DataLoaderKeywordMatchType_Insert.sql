SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the DataLoaderKeywordMatchType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderKeywordMatchType_Insert]
(

	@DataLoaderKeywordMatchTypeID int    OUTPUT,

	@MatchTypeName varchar (250)  ,

	@MatchTypeDescription varchar (2000)  
)
AS


				
				INSERT INTO [dbo].[DataLoaderKeywordMatchType]
					(
					[MatchTypeName]
					,[MatchTypeDescription]
					)
				VALUES
					(
					@MatchTypeName
					,@MatchTypeDescription
					)
				-- Get the identity value
				SET @DataLoaderKeywordMatchTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderKeywordMatchType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderKeywordMatchType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderKeywordMatchType_Insert] TO [sp_executeall]
GO
