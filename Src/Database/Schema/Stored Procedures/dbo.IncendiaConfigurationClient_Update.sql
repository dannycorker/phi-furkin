SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the IncendiaConfigurationClient table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[IncendiaConfigurationClient_Update]
(

	@IncendiaConfigurationClientID int   ,

	@AquariumClientID int   ,

	@Username varchar (50)  ,

	@Password varchar (50)  ,

	@URI varchar (150)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[IncendiaConfigurationClient]
				SET
					[AquariumClientID] = @AquariumClientID
					,[Username] = @Username
					,[Password] = @Password
					,[URI] = @URI
				WHERE
[IncendiaConfigurationClientID] = @IncendiaConfigurationClientID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationClient_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[IncendiaConfigurationClient_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[IncendiaConfigurationClient_Update] TO [sp_executeall]
GO
