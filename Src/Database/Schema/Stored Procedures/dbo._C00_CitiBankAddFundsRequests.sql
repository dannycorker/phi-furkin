SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2010-01-20
-- Description:	CitiBank Add Funds Requests for Cheshire East
-- ACE/JWG 2010-03-23 Get details from TDVs. Exclude contributions <= £0.00
-- ACE Copied for LBH (226) and Made Generic
-- =============================================
CREATE PROCEDURE [dbo].[_C00_CitiBankAddFundsRequests] 
(
@ClientID int,
@CCTableDateOfMonthFieldID int,
@CCTableTransactionAmountFieldID int,
@LAContributionFieldID int,
@LATotalLoadedFieldID int,
@LATableDateOfMonthFieldID int,
@AccountRequestedEventTypeID int,
@JobNumber varchar(10), /*Batch Job Running the Report*/
@ProgramID varchar(4),
@PartnerID varchar(4),
@OutputAsXML bit = 1,
@EcountNumberFieldID int,
@DateOfMonth datetime = '2009-12-10',
@IdentifierNumber int = 0 /*ComtrOCC/ParisNumber Number Use 0 For CustomerID*/
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @XML varchar(max),
	@CreationDate char(10),
	@FileName varchar(50),
	@RunNumber varchar(10)
	
	
	/* Changed to use actual table rows to get data to pay client AE 2010-01-19
	SELECT @XML = (
		SELECT 
		'Customer:' + cast(c.customerid as varchar) as [request/@partneruserid], 
		'Lead:' + cast(le.LeadID as varchar) + ' LeadEvent:' + cast(le.LeadEventID as varchar) as [request/addfundsrequest/@passthrough], 
		100 as [request/addfundsrequest/amount], 
		0 as [request/addfundsrequest/taxableflag], 
		0 as [request/addfundsrequest/notificationcode], 
		le.LeadEventID as [request/addfundsrequest/partnerpaymentid], 
		0 as [request/addfundsrequest/directclaimflag] 
		FROM dbo.LeadEvent le WITH (NOLOCK) 
		INNER JOIN dbo.Cases ca WITH (NOLOCK) ON ca.CaseID = le.CaseID 
		INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = ca.LeadID 
		INNER JOIN dbo.Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID 
		WHERE le.EventTypeID = 70571 
		AND le.WhenFollowedUp IS NULL 
		AND le.EventDeleted = 0 
		AND le.ClientID = 144 
		/* TESTING! */ AND c.CustomerID = 876519 /* TESTING! */ 
		FOR XML PATH(''), ROOT('batch')
	)

	*/

	DECLARE @TotalstoLoad Table ( Total money, MatterID int, TableRowID int, CustomerID varchar(2000), LeadID int)

	IF @DateOfMonth = '2009-12-10'
	BEGIN

		SELECT @DateOfMonth = dbo.fnDateOnly(dbo.fn_GetDate_Local())
	
	END

	/*
		Sum up all the tdv payments the client has made by MatterID 
	*/
	;
	WITH clientcontributions as (
		SELECT sum(tdv_transamount.ValueMoney) as ClientContribution, tdv_dateofpaymentth.MatterID
		FROM TableDetailValues tdv_dateofpaymentth WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv_transamount WITH (NOLOCK) on tdv_transamount.TableRowID = tdv_dateofpaymentth.TableRowID and tdv_transamount.DetailFieldID = @CCTableTransactionAmountFieldID
		WHERE tdv_dateofpaymentth.DetailFieldID = @CCTableDateOfMonthFieldID 
		AND tdv_dateofpaymentth.ValueDate = @DateOfMonth
		--AND tdv_dateofpaymentth.ValueDate >= '2011-04-09' and tdv_dateofpaymentth.ValueDate < dbo.fn_GetDate_Local()
		GROUP BY tdv_DateOfPaymentth.MatterID
	)

	/*
		Sum together the LA contribution (93169) + actual client contributions (from CTE clientcontributions above) 
		These come from 2 separate tables (as in TableDetailValues) within Aquarium
		They need to be joined by MatterID and "Payment Date" field (94361, 93168)
	*/
	INSERT INTO @TotalstoLoad ( Total, MatterID, TableRowID, CustomerID, LeadID)
	SELECT sum(isnull(tdv_LAContribution.ValueMoney, 0)) + isnull(sum(cc.ClientContribution), 0) as [Total to Load], 
	tdv_DateOfPayment.MatterID, 
	tdv_DateOfPayment.TableRowID, 
	CASE @IdentifierNumber WHEN 0 THEN convert(varchar,m.CustomerID) ELSE mdv.DetailValue END, 
	m.LeadID
	FROM TableDetailValues tdv_DateOfPayment WITH (NOLOCK) 
	/*INNER JOIN TableDetailValues tdv_ClientContribution WITH (NOLOCK) on tdv_ClientContribution.TableRowID = tdv_DateOfPayment.TableRowID and tdv_ClientContribution.DetailFieldID = 93170*/
	INNER JOIN TableDetailValues tdv_LAContribution WITH (NOLOCK) on tdv_LAContribution.TableRowID = tdv_DateOfPayment.TableRowID and tdv_LAContribution.DetailFieldID = @LAContributionFieldID
	LEFT JOIN clientcontributions cc WITH (NOLOCK) on cc.MatterID = tdv_DateOfPayment.MatterID
	INNER JOIN Matter m WITH (NOLOCK) on m.MatterID = tdv_DateOfPayment.MatterID 
	INNER JOIN Cases c WITH (NOLOCK) ON c.CaseID = m.CaseID and (c.ClientStatusID <> 1085 or c.ClientStatusID IS NULL)
	INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = m.CustomerID and (cu.Test = 0 or cu.Test IS NULL)
	INNER JOIN TableDetailValues tdv_totalloaded WITH (NOLOCK) on tdv_totalloaded.TableRowID = tdv_DateOfPayment.TableRowID and tdv_totalloaded.DetailFieldID = @LATotalLoadedFieldID and (tdv_totalloaded.ValueMoney = 0 or tdv_totalloaded.ValueMoney IS NULL)
	LEFT JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = tdv_DateOfPayment.MatterID and mdv.DetailFieldID = @IdentifierNumber
	WHERE tdv_DateOfPayment.DetailFieldID = @LATableDateOfMonthFieldID
	AND tdv_DateOfPayment.ValueDate = @DateOfMonth
	--AND tdv_DateOfPayment.ValueDate >= '2011-04-09' and tdv_DateOfPayment.ValueDate < dbo.fn_GetDate_Local()
	--AND tdv_DateOfPayment.ValueDate BETWEEN '2011-03-16' AND '2011-03-22'
	AND EXISTS (
		Select *
		From LeadEvent le WITH (NOLOCK)
		WHERE le.CaseID = m.CaseID 
		and le.EventTypeID = @AccountRequestedEventTypeID /*New CitiBank Account Creation Requested*/
		--and le.WhenCreated < CASE WHEN @DateOfMonth = dbo.fnDateOnly(dbo.fn_GetDate_Local()) THEN dbo.fn_GetDate_Local()-2 ELSE dbo.fn_GetDate_Local() END
	)
	GROUP BY tdv_DateOfPayment.MatterID, tdv_DateOfPayment.TableRowID, CASE @IdentifierNumber WHEN 0 THEN convert(varchar,m.CustomerID) ELSE mdv.DetailValue END, m.LeadID

	/*
		Update the "Total Loaded Onto Card" tdv field with the results for each Matter above
	*/
	
	IF @DateOfMonth = dbo.fnDateOnly(dbo.fn_GetDate_Local())
	BEGIN
	
		UPDATE TableDetailValues
		SET DetailValue = Total
		FROM TableDetailValues
		INNER JOIN @TotalstoLoad tl on tl.TableRowID = TableDetailValues.TableRowID
		WHERE TableDetailValues.DetailFieldID = @LATotalLoadedFieldID
	
	END
	
	/*
		Exclude payments <= £0.00 as the CitiBank crashes if it finds any.
	*/
	
	IF @OutputAsXML = 1
	BEGIN
	
		SELECT @XML = (
			SELECT 
			CASE @IdentifierNumber WHEN 0 THEN 'Customer:' ELSE '' END + cast(CustomerID as varchar) as [request/@partneruserid], 
			--'Customer:' + cast(CustomerID as varchar) as [request/@partneruserid], 
			'Lead:' + cast(LeadID as varchar) + ' TableRow:' + cast(TableRowID as varchar) as [request/addfundsrequest/@passthrough], 
			replace(Total, '.', '') as [request/addfundsrequest/amount], 
			0 as [request/addfundsrequest/taxableflag], 
			0 as [request/addfundsrequest/notificationcode], 
			TableRowID as [request/addfundsrequest/partnerpaymentid], 
			0 as [request/addfundsrequest/directclaimflag] 
			FROM @TotalstoLoad  
			WHERE Total > 0 
			ORDER BY CustomerID
			FOR XML PATH(''), ROOT('batch')
		)

		/* Do not select anything at all if there are no records today. */
		IF @XML IS NULL
		BEGIN
			SELECT NULL as [CitiBank DataFile] 
			WHERE 0 = 1
		END
		ELSE
		BEGIN
				
			SELECT @CreationDate = convert(char(10), dbo.fn_GetDate_Local(), 126)

			/* Use the WorkingDays table to get a unique run number for the file (as long as it runs once per day). */
			SELECT @RunNumber = cast(w.WorkingDayID as varchar) 
			FROM dbo.WorkingDays w (nolock) 
			WHERE w.CharDate = @CreationDate 


			SELECT @FileName = 'CitiBank Add Funds ' + @CreationDate + ' ' + replace(convert(varchar, dbo.fn_GetDate_Local(), 108), ':', '') + '.xml'

			SELECT @XML = replace(@XML, '<batch>', '<batch programid="' + @ProgramID + '" batchdescription="Add Funds Batch" passthrough="Job Number (AT) ' + @JobNumber + '" batchpromotionid="0" settlementdate="' + @CreationDate + '">')

			SELECT @XML = '<requestfile xmlns="http://www.ecount.com/" partnerid="' + @PartnerID + '" filename="' + @FileName + '" passthrough="Run Number (ATR) ' + @RunNumber + '" creationdate="' + @CreationDate + '">' + @XML + '</requestfile>'

			SELECT cast(@XML as XML) as [CitiBank DataFile]
		END

	END
	ELSE
	BEGIN
	
		SELECT REPLACE(CONVERT(varchar,CONVERT(date,dbo.fn_GetDate_Local())),'-',''), 
				t.CustomerID, 
				mdv_ecount.DetailValue as [EcountID], 
				c.FirstName, 
				c.MiddleName, 
				c.LastName, 
				ti.Title, 
				c.DateOfBirth, 
				c.Address1, 
				c.Address2, 
				'' as [Attention Line], 
				'' AS [Company Name], 
				c.Town, 
				'' as [State],
				c.PostCode,
				c.County,
				'UK' as [Country],
				'+44' as [Phone Country Code],
				'' as [Phone Area Code],
				c.HomeTelephone as [Phone Number],
				'' as [Extension],
				'' as [Phone Type],
				c.EmailAddress,
				'' as [Location Code],
				CONVERT(int,(t.Total)*100) as [Amount],
				'' as [Notification Indicator],
				'' as [Taxable Flag],
				'' as [Direct Claim Flag],
				'' as [Online Registration Required],
				'Run Number (ATR) ' + @RunNumber as [Passthrough]
		FROM @TotalstoLoad t
		LEFT JOIN MatterDetailValues mdv_ecount ON mdv_ecount.MatterID = t.MatterID and mdv_ecount.DetailFieldID = @EcountNumberFieldID
		INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = t.CustomerID
		INNER JOIN Titles ti WITH (NOLOCK) ON ti.TitleID = c.TitleID
		WHERE Total > 0 
		ORDER BY c.CustomerID	

	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CitiBankAddFundsRequests] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_CitiBankAddFundsRequests] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_CitiBankAddFundsRequests] TO [sp_executeall]
GO
