SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadDocument table
-- JWG 2012-07-12 Note that documents are not deleted from any of the archive databases.
----------------------------------------------------------------------------------------------------
*/
CREATE PROCEDURE [dbo].[LeadDocument_Delete]
(

	@LeadDocumentID INT   
)
AS
BEGIN

	/* JWG 2012-07-12 Note that documents are not deleted from any of the archive databases. */
	DELETE FROM [dbo].[LeadDocumentFS] WITH (ROWLOCK) 
	WHERE [LeadDocumentID] = @LeadDocumentID

	/* JWG 2012-07-12 Note that documents are not deleted from any of the archive databases. */
	DELETE FROM [dbo].[LeadDocument] WITH (ROWLOCK) 
	WHERE [LeadDocumentID] = @LeadDocumentID
	AND WhenArchived IS NULL

END
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocument_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocument_Delete] TO [sp_executeall]
GO
