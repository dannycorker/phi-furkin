SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.GetClientOffices    Script Date: 08/09/2006 12:22:46 ******/

CREATE PROCEDURE [dbo].[GetClientOffices] @ClientID int
AS

SELECT     ClientOfficeID, ClientID, BuildingName, Address1, Address2, Town, County, Country, PostCode, OfficeTelephone, OfficeTelephoneExtension
FROM         dbo.ClientOffices
WHERE ClientID = @ClientID





GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientOffices] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetClientOffices] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetClientOffices] TO [sp_executeall]
GO
