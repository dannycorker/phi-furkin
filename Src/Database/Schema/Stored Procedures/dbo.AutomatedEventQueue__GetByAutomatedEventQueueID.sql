SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ian Slack
-- Create date: 2011-10-21
-- Description:	Gets a specifi automated queue event by id
-- Purpose:		Used by the scheduler to test individual event execution
-- =============================================
CREATE PROCEDURE [dbo].[AutomatedEventQueue__GetByAutomatedEventQueueID]
	@AutomatedEventQueueID INT
AS
BEGIN

	SELECT * 
	FROM dbo.AutomatedEventQueue a WITH (NOLOCK) 
	WHERE AutomatedEventQueueID = @AutomatedEventQueueID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedEventQueue__GetByAutomatedEventQueueID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedEventQueue__GetByAutomatedEventQueueID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedEventQueue__GetByAutomatedEventQueueID] TO [sp_executeall]
GO
