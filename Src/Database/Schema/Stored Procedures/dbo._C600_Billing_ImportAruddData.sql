SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-04-06
-- Description:	Import Arudd Data to Arudd Processing Table
-- 2018-06-07 ACE - Added failure file type
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Billing_ImportAruddData]
	 @XMLRaw VARCHAR(MAX),
	 @FileName VARCHAR(100) = NULL
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @XML XML = @XMLRaw,
			@ClientID INT = dbo.fnGetPrimaryClientID()

	/*NB Incoming sort code can have hyphens in them...*/
	INSERT INTO BACSPaymentFailures (ClientID, FileName, FailureFileType, RowNum, AccountNumber, SortCode, Name, PaymentAmount, BACSProcessedDate, FailureReason, FailureCode, Reference, ImportStatusID, ImportedDate, Comments)
	OUTPUT inserted.*
	SELECT  @ClientID, 
			@FileName AS [FileName],
			'ACH' AS [FailureFileType],
			ROW_NUMBER() OVER(ORDER BY (SELECT 1)) AS [RowNum],
			n.c.value('(PayerAccount/@number)[1]' , 'varchar(2000)') AS [PayerAccountNumber],
			REPLACE(n.c.value('(PayerAccount/@sortCode)[1]' , 'varchar(2000)'), '-', '') AS [PayerSortCode],
			n.c.value('(PayerAccount/@name)[1]' , 'varchar(2000)') AS [PayerName],
			n.c.value('(@valueOf)[1]' , 'varchar(2000)') AS [Amount],
			n.c.value('(@originalProcessingDate)[1]' , 'varchar(2000)') AS [OriginalProcessingDate],
			n.c.value('(@returnDescription)[1]' , 'varchar(2000)') AS [ReasonDescription],
			n.c.value('(@returnCode)[1]' , 'varchar(2000)') AS [ReasonCode],
			LTRIM(RTRIM(n.c.value('(@ref)[1]' , 'varchar(2000)'))) AS [Reference],
			1 AS [ImportStatusID],
			dbo.fn_GetDate_Local() AS [ImportedDate], 
			'' AS [Comments]
	FROM @XML.nodes('//BACSDocument/Data/ARUDD/Advice/OriginatingAccountRecords/OriginatingAccountRecord/ReturnedDebitItem') n(c)
	OPTION (OPTIMIZE FOR ( @XML = NULL ))

	EXEC Billing__ReconcilePaymentFailures @ClientID

	SELECT *
	FROM BacsPaymentFailures b
	WHERE b.FileName = @FileName

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Billing_ImportAruddData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Billing_ImportAruddData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Billing_ImportAruddData] TO [sp_executeall]
GO
