SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-01-15
-- Description:	CustomViewRouting_Insert
-- =============================================
CREATE PROCEDURE [dbo].[CustomViewRouting_Insert]
	@ClientID int,
	@DetailFieldSubTypeID int,
	@LeadTypeID int,
	@PageID int,
	@FieldID int,
	@MapsToFieldID int,
	@MapsToClientID int
AS
BEGIN

	SET NOCOUNT ON;


INSERT INTO [dbo].[CustomViewRouting]
           ([ClientID]
           ,[DetailFieldSubTypeID]
           ,[LeadTypeID]
           ,[PageID]
           ,[FieldID]
           ,[MapsToFieldID]
           ,[MapsToClientID])
     VALUES
           (@ClientID,
           @DetailFieldSubTypeID, 
           @LeadTypeID, 
           @PageID, 
           @FieldID, 
           @MapsToFieldID,
           @MapsToClientID)

END

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomViewRouting_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomViewRouting_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomViewRouting_Insert] TO [sp_executeall]
GO
