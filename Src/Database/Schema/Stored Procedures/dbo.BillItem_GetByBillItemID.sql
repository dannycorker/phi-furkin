SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the BillItem table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BillItem_GetByBillItemID]
(

	@BillItemID int   
)
AS


				SELECT
					[BillItemID],
					[ClientID],
					[BillID],
					[Description],
					[ClientPersonnelID],
					[WorkUnits],
					[Total],
					[ChargeOutRate],
					[TotalIsCustomValue]
				FROM
					[dbo].[BillItem] WITH (NOLOCK) 
				WHERE
										[BillItemID] = @BillItemID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[BillItem_GetByBillItemID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillItem_GetByBillItemID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillItem_GetByBillItemID] TO [sp_executeall]
GO
