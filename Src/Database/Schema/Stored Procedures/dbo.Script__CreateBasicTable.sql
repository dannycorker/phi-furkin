SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardsons
-- Create date: 07-04-2014
-- Description:	Creates a basic table within the resources and tables leadtype,
-- Either creates the page at the given level with the section name or looks up the existing one
-- Adds the table field to the page and returns the detail fieldID
-- Modified by: PR 08-04-2015 updated the lookup list matching
-- =============================================
CREATE PROCEDURE [dbo].[Script__CreateBasicTable]
	@ClientID INT, 
	@ScriptID INT, 
	@ScriptSectionID INT,
	@DetailFieldSubTypeID INT,
	@TableName VARCHAR(250),
	@TableColumns tvpVarchar10 READONLY, -- ID=>question typeID, VAL1=>column name, Val2=>Field Order, Val3=>list of lookup list items
	@UserID INT -- who created
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @ResourcesAndTablesLeadTypeID INT	
	DECLARE @ScriptLeadTypeID INT	
	
	--Get the script lead type ID
	SELECT @ScriptLeadTypeID=LeadTypeID
	FROM Script WITH (NOLOCK) WHERE ScriptID=@ScriptID AND ClientID=@ClientID
	
	--Get the resources and tables lead type ID
	SELECT TOP 1 @ResourcesAndTablesLeadTypeID=LeadTypeID 
	FROM LeadType WITH (NOLOCK) WHERE ClientID=@ClientID AND LeadTypeName LIKE '%Resource%'
		
	IF ((@ResourcesAndTablesLeadTypeID IS NULL) OR (@ResourcesAndTablesLeadTypeID<=0)) -- can not find resources and tables lead type so make it
	BEGIN
		INSERT INTO LeadType (ClientID, LeadTypeName, LeadTypeDescription, Enabled, Live, MatterDisplayName, LastReferenceInteger, ReferenceValueFormatID, IsBusiness, LeadDisplayName, CaseDisplayName, CustomerDisplayName, LeadDetailsTabImageFileName, CustomerTabImageFileName, UseEventCosts, UseEventUOEs, UseEventDisbursements, UseEventComments)
		SELECT @ClientID,'Resources and Tables','Resources and Tables',1,1,'',NULL,NULL,0,'','','',NULL,NULL,0,0,0,0
				
		SELECT @ResourcesAndTablesLeadTypeID = SCOPE_IDENTITY()
		
	END
		
	IF @ResourcesAndTablesLeadTypeID>0 
	BEGIN
	
		--Create the basic table page within the resources and tables lead type 
		DECLARE @TablePageID INT
		INSERT INTO DetailFieldPages (ClientID, LeadOrMatter, LeadTypeID, PageName, PageCaption, PageOrder, Enabled, WhoCreated, WhoModified, WhenCreated, WhenModified)
		VALUES (@ClientID, 8, @ResourcesAndTablesLeadTypeID, @TableName, @TableName, 0, 1, @UserID, @UserID, dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local())
				
		SELECT @TablePageID = SCOPE_IDENTITY()
		
		--Create the columns for the basic table
		INSERT INTO DetailFields (ClientID, LeadOrMatter, FieldName, FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID,            Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView, ObjectTypeID, SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified, DetailFieldStyleID, Hyperlink, IsShared)
		SELECT                    @ClientID,8,            Val1,     Val1,          ID,             0,        0,      NULL, @ResourcesAndTablesLeadTypeID, 1,       @TablePageID, CONVERT(INT, Val2), 0,            '',           NULL,             1,         NULL,                1,                             1,                    '',           '',           '',    '',           NULL,                          NULL,                   '',            '',                 1,        0,      0,                    0,                      0,       0,              0,                        0,             1,                  NULL,         NULL,     @UserID,    dbo.fn_GetDate_Local(),   @UserID,     dbo.fn_GetDate_Local(),    NULL,               '',        NULL
		FROM @TableColumns
		
		DECLARE @PageName VARCHAR(250)
		SELECT @PageName=SectionTitle FROM ScriptSection WITH (NOLOCK) 
		WHERE ScriptSectionID=@ScriptSectionID AND ClientID=@ClientID AND ScriptID=@ScriptID
		
		
		/* We are about to make a page at the Customer level, which has a shared LeadTypeID of 0 for all clients */
		IF @DetailFieldSubTypeID=10
		BEGIN
			SET @ScriptLeadTypeID=0
		END			
		
		
		DECLARE @DetailFieldPageID INT
		-- does the page already exist within the script lead type and level(detail field subtype ID)
		SELECT @DetailFieldPageID=DetailFieldPageID FROM DetailFieldPages WITH (NOLOCK) 
		WHERE ClientID=@ClientID AND LeadOrMatter = @DetailFieldSubTypeID AND PageName = @PageName AND (LeadTypeID=@ScriptLeadTypeID)
	
		IF (@DetailFieldPageID IS NULL) OR (@DetailFieldPageID<=0) -- page does not exist so create the page
		BEGIN
			
			INSERT INTO DetailFieldPages (ClientID, LeadOrMatter, LeadTypeID, PageName, PageCaption, PageOrder, Enabled, WhoCreated, WhoModified, WhenCreated, WhenModified)
			VALUES (@ClientID, @DetailFieldSubTypeID, @ScriptLeadTypeID, @PageName, @PageName,0, 1, @UserID, @UserID, dbo.fn_GetDate_Local(), dbo.fn_GetDate_Local())
			
			SELECT @DetailFieldPageID = SCOPE_IDENTITY()
		END
	
		-- add the basic table field to the page
		INSERT INTO DetailFields (ClientID,  LeadOrMatter,          FieldName,  FieldCaption, QuestionTypeID, Required, Lookup, LookupListID, LeadTypeID, Enabled, DetailFieldPageID, FieldOrder, MaintainHistory, EquationText, MasterQuestionID, FieldSize, LinkedDetailFieldID, ValidationCriteriaFieldTypeID, ValidationCriteriaID, MinimumValue, MaximumValue, RegEx, ErrorMessage, ResourceListDetailFieldPageID, TableDetailFieldPageID, DefaultFilter, ColumnEquationText, Editable, Hidden, LastReferenceInteger, ReferenceValueFormatID, Encrypt, ShowCharacters, NumberOfCharactersToShow, TableEditMode, DisplayInTableView, ObjectTypeID, SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified, DetailFieldStyleID, Hyperlink, IsShared)
		VALUES					 (@ClientID, @DetailFieldSubtypeID, @TableName, @TableName,   19,             0,        0,      NULL, @ScriptLeadTypeID,  1,     @DetailFieldPageID,  1,          0,               '',           NULL,             1,         NULL,                1,                             1,                    '',           '',           '',    '',           NULL,                          @TablePageID,           '',            '',                 1,        0,      0,                    0,                      0,       0,              0,                        0,             1,                  NULL,         NULL,     @UserID,    dbo.fn_GetDate_Local(),   @UserID,     dbo.fn_GetDate_Local(),    NULL,               '',        NULL)
		
		DECLARE @DetailFieldIDOnPage INT
		SELECT @DetailFieldIDOnPage = SCOPE_IDENTITY() -- store created detail field id for return back to the caller
		
		DECLARE @NumberOfRows INT -- this is the number of columns created
		DECLARE @Position INT = 1 -- field order position starts at 1
		
		SELECT @NumberOfRows = COUNT(*) FROM @TableColumns
		
		DECLARE @ListItems TABLE (AnyValue VARCHAR(2000))
		DECLARE @ListIDs TABLE (Unid INT IDENTITY(1,1), AnyID INT)
		DECLARE @ColumnName VARCHAR(250)
		DECLARE @CsvList VARCHAR(MAX)
		DECLARE @FieldID INT -- get the field if its a list type field
		DECLARE @NumberOfListItems INT 
		
		WHILE(@Position<=@NumberOfRows) -- iterate over all the table columns
		BEGIN 
					
			-- get the column name and the pipe separated list
			SELECT @ColumnName = Val1, @CsvList=Val3 FROM @TableColumns WHERE Val2=@Position			
			SET @FieldID=0
			SELECT @FieldID = DetailFieldID FROM DetailFields WITH (NOLOCK) 
			WHERE DetailFieldPageID=@TablePageID AND FieldName = @ColumnName AND (QuestionTypeID=2 OR QuestionTypeID=4)	
			IF(@FieldID>0) -- we have a field that is a lookup list 
			BEGIN 
			
				-- place list in tvp
				
				DELETE FROM @ListItems -- re-initialise list items
				INSERT INTO @ListItems (AnyValue) -- split the string into individual rows
				SELECT t.AnyValue 
				FROM dbo.fnTableOfValues(@CsvList, '|') t 
					
				SELECT @NumberOfListItems=COUNT(*) FROM @ListItems -- count the number of list items
	
				DELETE FROM @ListIDs -- re-initialise list items
				INSERT INTO @ListIDs(AnyID) -- place potential lookup list ids into a list
				SELECT DISTINCT LookupListID FROM LookupListItems lli WITH (NOLOCK) 
				INNER JOIN @ListItems ON RTRIM(LTRIM(AnyValue)) = RTRIM(LTRIM(lli.ItemValue)) AND lli.ClientID IN (0, @ClientID)
				GROUP BY LookupListID
				HAVING COUNT(*)= @NumberOfListItems
	
				DECLARE @LookupListID INT
				DECLARE @ID INT

				DECLARE @TestID INT = 0, @Unid INT 

				SELECT TOP 1 @Unid = l.Unid, @ID = l.AnyID
				FROM @ListIDs l 
				ORDER BY l.Unid

				WHILE @Unid > @TestID -- iterate over those lookup list items and check that they exactly match our target list items
				BEGIN
				
					DECLARE @LookupListItemCount INT	
					DECLARE @MatchingRowCount INT
					SELECT @LookupListItemCount=COUNT(*) FROM LookupListItems WITH (NOLOCK) WHERE LookupListID=@ID
					IF(@LookupListItemCount=@NumberOfListItems) -- its the correct length
					BEGIN
						SELECT @MatchingRowCount= COUNT(lli.LookupListItemID) FROM LookupListItems lli WITH (NOLOCK) 
						INNER JOIN @ListItems ON RTRIM(LTRIM(AnyValue)) = RTRIM(LTRIM(lli.ItemValue)) AND lli.ClientID IN (0, @ClientID) AND lli.LookupListID = @ID
						IF(@MatchingRowCount=@NumberOfListItems) -- its the correct length with the inner join
						BEGIN
							SET @LookupListID=@ID							
						END
					END
				
					SELECT @TestID = @Unid
					
					SELECT TOP 1 @Unid = l.Unid, @ID = l.AnyID
					FROM @ListIDs l 
					WHERE l.Unid > @Unid
					ORDER BY l.Unid
				
				END
				
				IF(@LookupListID IS NULL) OR (@LookupListID=0)
				BEGIN
					--create a lookup list with the field name + _list
					INSERT INTO LookupList (LookupListName, LookupListDescription, ClientID, Enabled, SortOptionID)
					VALUES (RTRIM(LTRIM(@ColumnName)) + '_List', RTRIM(LTRIM(@ColumnName)) + '_List', @ClientID, 1, 23) -- specific sorting						
					
					SET @LookupListID = SCOPE_IDENTITY()
					
					--create the lookup list items from the tvp of strings	
					INSERT INTO LookupListItems (LookupListID, ItemValue, ClientID, Enabled, SortOrder)			
					SELECT @LookupListID, RTRIM(LTRIM(li.AnyValue)), @ClientID, 1, 1 FROM @ListItems li		
					
				END
				
				-- update detail field with lookup list id
				UPDATE DetailFields
				SET LookupListID=@LookupListID
				WHERE DetailFieldID = @FieldID
				
				SET @LookupListID=0
				SET @ID=0;
			END 
			
			SET @Position=@Position+1;
		END		
		
		SELECT @DetailFieldIDOnPage DetailFieldID	-- return the created table on the page detail field
		
	END 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__CreateBasicTable] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Script__CreateBasicTable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Script__CreateBasicTable] TO [sp_executeall]
GO
