SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-10-07
-- Description:	Add row to itemised collections table
-- Mods
-- DCM 2015-04-30 removed date filter on @premiumrows
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Collections_AddTableRow] 
(
	@EventTypeID INT,
	@MatterID INT,
	@WhoCreated INT
)

AS
BEGIN

	SET NOCOUNT ON;

--declare 	@EventTypeID INT = 150091,
--	@MatterID INT = 50022264,
--	@WhoCreated INT = 33135


	DECLARE
			@AccName VARCHAR(100),
			@AccSortCode VARCHAR(10),
			@AccNumber VARCHAR(10),
			@AffinitySC VARCHAR(10),
			@Amount MONEY,
			@BACSCode VARCHAR(3),
			@CaseID INT,
			@DateNextPaymentDue DATE,
			@EffectiveDate DATE,
			@IsAdjustment INT,
			@IsDD INT,
			@LeadID INT,
			@NewTableRowID INT,
			@PayRef VARCHAR(100),
			@TableRowID INT


	DECLARE @PremiumRows TABLE (TableRowID INT, Amount MONEY, PayType INT, Done INT)

	SELECT @LeadID=LeadID, @CaseID=CaseID FROM Matter WITH (NOLOCK) WHERE MatterID=@MatterID
	
	SELECT @DateNextPaymentDue=dbo.fnGetDvAsDate(170185,@CaseID)
	
	-- make the payment reference
	SELECT @AffinitySC=scli.ItemValue FROM CustomerDetailValues sc WITH (NOLOCK) 
	INNER JOIN Lead cl WITH (NOLOCK) ON sc.CustomerID=cl.CustomerID
	INNER JOIN Matter cm WITH (NOLOCK) ON cm.LeadID=cl.LeadID AND cm.MatterID=@MatterID
	INNER JOIN ResourceListDetailValues scrl WITH (NOLOCK) ON sc.ValueInt=scrl.ResourceListID and scrl.DetailFieldID=170127
	INNER JOIN LookupListItems scli WITH (NOLOCK) ON scrl.ValueInt=scli.LookupListItemID
	WHERE sc.DetailFieldID=170144
	
	SELECT @PayRef= dbo.fn_C600_GetMandateReference(@MatterID)

	-- add payment row
	-- select possible payment rows
	INSERT INTO @PremiumRows (TableRowID,Amount,PayType,Done)
	SELECT pp.TableRowID,am.ValueMoney,pt.ValueInt,0 
	FROM TableRows pp WITH (NOLOCK) 
	INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON pp.MatterID=ltr.FromMatterID AND ltr.FromLeadTypeID=1492 AND ltr.ToLeadTypeID=1493
		AND ltr.ToMatterID=@MatterID 
	INNER JOIN TableDetailValues am WITH (NOLOCK) ON pp.TableRowID=am.TableRowID AND am.DetailFieldID=170072
	INNER JOIN TableDetailValues pt WITH (NOLOCK) ON pp.TableRowID=pt.TableRowID AND pt.DetailFieldID=175383	
	INNER JOIN TableDetailValues dd WITH (NOLOCK) ON pp.TableRowID=dd.TableRowID AND dd.DetailFieldID=175597	
	INNER JOIN TableDetailValues fdate WITH (NOLOCK) ON pp.TableRowID=fdate.TableRowID AND fdate.DetailFieldID=170076	
	INNER JOIN TableDetailValues tdate WITH (NOLOCK) ON pp.TableRowID=tdate.TableRowID AND tdate.DetailFieldID=170077	
	WHERE pp.DetailFieldID=170088
	AND dd.ValueDate<=@DateNextPaymentDue -- only collect payment where the due date is less than date next payment due
	--AND ( tdate.ValueDate < dbo.fn_GetDate_Local() OR dbo.fn_GetDate_Local() BETWEEN fdate.ValueDate AND tdate.ValueDate )
	AND NOT EXISTS ( SELECT * FROM TableDetailValues crid WITH (NOLOCK) WHERE pp.TableRowID=crid.TableRowID and crid.DetailFieldID=170073 and crid.DetailValue<>'')

	-- DD or CP matter?
	SELECT @IsDD=CASE WHEN dbo.fnGetDvAsInt (170115,@CaseID)=69930 THEN 1 ELSE 0 END

	WHILE EXISTS ( SELECT * FROM @PremiumRows WHERE Done=0 )
	BEGIN
	
		SELECT @TableRowID=TableRowID,@Amount=Amount,@IsAdjustment=CASE WHEN PayType=72140 THEN 1 ELSE 0 END 
		FROM @PremiumRows WHERE Done=0

		-- set bacs code accrding to whether this is a first payment (from the mandate account perspective)
		IF @IsDD=0 -- CP transactions
		BEGIN
		
			SELECT @BACSCode=CASE WHEN @Amount<0 THEN 'CPR' ELSE 'CPI' END
		
		END
		ELSE -- DD transactions
		BEGIN
		
			IF @EventTypeID = 150120 -- Retry payment collection ( use code used on the latest collection attempt )
			BEGIN
			
				SELECT TOP 1 @BACSCode=cbacs.DetailValue 
				FROM TableRows tr WITH (NOLOCK) 
				INNER JOIN TableDetailValues ref WITH (NOLOCK) ON tr.TableRowID=ref.TableRowID and ref.DetailFieldID=170117
						AND CASE WHEN LEN(ref.DetailValue)<4 THEN 0 ELSE CONVERT(INT,RIGHT(ref.DetailValue,LEN(ref.DetailValue)-3)) END = @MatterID
				INNER JOIN TableDetailValues bacs WITH (NOLOCK) ON tr.TableRowID=bacs.TableRowID and bacs.DetailFieldID=170162
						AND bacs.DetailValue IN ('01','17')			
				INNER JOIN TableDetailValues cbacs WITH (NOLOCK) ON cbacs.TableRowID=ref.TableRowID and cbacs.DetailFieldID=170162
				WHERE tr.DetailFieldID=170245
				order by cbacs.TableRowID desc
			END
			ELSE IF @Amount<0 -- its a refund
			BEGIN
			
				SELECT @BACSCode='99'
			
			END
			ELSE IF @EventTypeID IN (150090) -- First payment event
				OR EXISTS ( SELECT * FROM MatterDetailValues mct WITH (NOLOCK) WHERE mct.DetailFieldID=170231 
							AND mct.MatterID=@MatterID AND mct.ValueInt=70013 ) -- mandate has new account details
				SELECT @BACSCode='01'		
			ELSE
				SELECT @BACSCode='17'
		
		END
		
		IF @Amount<0 SELECT @Amount=@Amount*(-1)

		SELECT @AccName=an.DetailValue, @AccSortCode=acs.DetailValue, @AccNumber=anum.DetailValue
		FROM MatterDetailValues an WITH (NOLOCK) 
		INNER JOIN MatterDetailValues acs WITH (NOLOCK) ON acs.MatterID=an.MatterID and acs.DetailFieldID=170189
		INNER JOIN MatterDetailValues anum WITH (NOLOCK) ON anum.MatterID=an.MatterID and anum.DetailFieldID=170190
		WHERE an.MatterID=@MatterID and an.DetailFieldID=170188 

		EXEC @NewTableRowID=_C600_BACS_AddTableRow @LeadID, 170119, @EffectiveDate, @Amount, 69930, @BACSCode, @PayRef, @AccName, @AccSortCode, @AccNumber, @TableRowID 
					
		-- update premium table
		EXEC dbo._C00_SimpleValueIntoField 170073, @NewTableRowID, @TableRowID, @WhoCreated -- Collection Request ID
		EXEC dbo._C00_SimpleValueIntoField 170078, 72154, @TableRowID, @WhoCreated -- Status ( = ready )
		
		-- if this is a CP record copy need to copy up to customer table? -- TODO review all CP record handling
		
		UPDATE @PremiumRows SET Done=1 WHERE TableRowID=@TableRowID
		
	END

	RETURN @NewTableRowID
				
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_AddTableRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_Collections_AddTableRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_Collections_AddTableRow] TO [sp_executeall]
GO
