SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ActiveSession table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ActiveSession_Find]
(

	@SearchUsingOR bit   = null ,

	@SessionID varchar (50)  = null ,

	@EmailAddress varchar (255)  = null ,

	@ThirdPartySystemId int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SessionID]
	, [EmailAddress]
	, [ThirdPartySystemId]
    FROM
	[dbo].[ActiveSession] WITH (NOLOCK) 
    WHERE 
	 ([SessionID] = @SessionID OR @SessionID IS NULL)
	AND ([EmailAddress] = @EmailAddress OR @EmailAddress IS NULL)
	AND ([ThirdPartySystemId] = @ThirdPartySystemId OR @ThirdPartySystemId IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SessionID]
	, [EmailAddress]
	, [ThirdPartySystemId]
    FROM
	[dbo].[ActiveSession] WITH (NOLOCK) 
    WHERE 
	 ([SessionID] = @SessionID AND @SessionID is not null)
	OR ([EmailAddress] = @EmailAddress AND @EmailAddress is not null)
	OR ([ThirdPartySystemId] = @ThirdPartySystemId AND @ThirdPartySystemId is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ActiveSession_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ActiveSession_Find] TO [sp_executeall]
GO
