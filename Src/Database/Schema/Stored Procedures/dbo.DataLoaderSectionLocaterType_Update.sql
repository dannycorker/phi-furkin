SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the DataLoaderSectionLocaterType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderSectionLocaterType_Update]
(

	@DataLoaderSectionLocaterTypeID int   ,

	@OriginalDataLoaderSectionLocaterTypeID int   ,

	@LocaterTypeName varchar (250)  ,

	@LocaterTypeDescription varchar (2000)  
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[DataLoaderSectionLocaterType]
				SET
					[DataLoaderSectionLocaterTypeID] = @DataLoaderSectionLocaterTypeID
					,[LocaterTypeName] = @LocaterTypeName
					,[LocaterTypeDescription] = @LocaterTypeDescription
				WHERE
[DataLoaderSectionLocaterTypeID] = @OriginalDataLoaderSectionLocaterTypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterType_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderSectionLocaterType_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderSectionLocaterType_Update] TO [sp_executeall]
GO
