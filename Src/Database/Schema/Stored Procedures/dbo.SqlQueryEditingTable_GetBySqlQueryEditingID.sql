SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the SqlQueryEditingTable table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryEditingTable_GetBySqlQueryEditingID]
(

	@SqlQueryEditingID int   
)
AS


				SELECT
					[SqlQueryEditingTableID],
					[ClientID],
					[SqlQueryEditingID],
					[SqlQueryTableName],
					[TableAlias],
					[TableDisplayOrder],
					[JoinType],
					[JoinText],
					[JoinTableID],
					[JoinRTRID],
					[RealSqlQueryTableID]
				FROM
					[dbo].[SqlQueryEditingTable] WITH (NOLOCK) 
				WHERE
										[SqlQueryEditingID] = @SqlQueryEditingID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable_GetBySqlQueryEditingID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryEditingTable_GetBySqlQueryEditingID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryEditingTable_GetBySqlQueryEditingID] TO [sp_executeall]
GO
