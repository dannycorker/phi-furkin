SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetStatisticsForQuestionnaireResponse]
	@ClientQuestionnaireID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    with AnswerData as (
		SELECT		ca.MasterQuestionID,
					ca.QuestionPossibleAnswerID,
					ca.ClientID,
					IsNull(IsNull(qpa.AnswerText, ca.Answer), '') as [AnswerText]
		FROM		dbo.Customers c
		LEFT JOIN	dbo.CustomerQuestionnaires cuq ON cuq.CustomerID = c.CustomerID 
		LEFT JOIN	dbo.CustomerAnswers ca ON ca.CustomerQuestionnaireID = cuq.CustomerQuestionnaireID 
		LEFT JOIN	dbo.QuestionPossibleAnswers qpa ON qpa.QuestionPossibleAnswerID = ca.QuestionPossibleAnswerID
		WHERE		cuq.ClientQuestionnaireID = @ClientQuestionnaireID
	),
	AnswerCounts as (
		SELECT	ca.MasterQuestionID,
				ca.AnswerText,
				count(*) as AnswersByQuestion
		FROM AnswerData ca
		GROUP BY ca.MasterQuestionID, ca.AnswerText
	),
	TotalAnswerCounts as (
		SELECT	ca.MasterQuestionID,
				count(*) as TotalAnswersForThisQuestion
		FROM AnswerData ca
		GROUP BY ca.MasterQuestionID
	)

	select a1.MasterQuestionID, mq.QuestionText, a1.AnswerText, a1.AnswersByQuestion, ta.TotalAnswersForThisQuestion, convert(decimal(18,2), 100.00 * (convert(decimal(18,2), a1.AnswersByQuestion) / convert(decimal(18,2), ta.TotalAnswersForThisQuestion))) as PercentageOfTotal
	from AnswerCounts a1 
	inner join TotalAnswerCounts ta ON ta.MasterQuestionID = a1.MasterQuestionID
	inner join MasterQuestions mq ON mq.MasterQuestionID = a1.MasterQuestionID
	ORDER BY a1.MasterQuestionID, a1.AnswerText
END





GO
GRANT VIEW DEFINITION ON  [dbo].[GetStatisticsForQuestionnaireResponse] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetStatisticsForQuestionnaireResponse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetStatisticsForQuestionnaireResponse] TO [sp_executeall]
GO
