SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DocumentTypeVersion table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DocumentTypeVersion_GetByClientID]
(

	@ClientID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[DocumentTypeVersionID],
					[DocumentTypeID],
					[ClientID],
					[LeadTypeID],
					[DocumentTypeName],
					[DocumentTypeDescription],
					[Header],
					[Template],
					[Footer],
					[CanBeAutoSent],
					[EmailSubject],
					[EmailBodyText],
					[InputFormat],
					[OutputFormat],
					[Enabled],
					[RecipientsTo],
					[RecipientsCC],
					[RecipientsBCC],
					[ReadOnlyTo],
					[ReadOnlyCC],
					[ReadOnlyBCC],
					[SendToMultipleRecipients],
					[MultipleRecipientDataSourceType],
					[MultipleRecipientDataSourceID],
					[SendToAllByDefault],
					[ExcelTemplatePath],
					[FromDetails],
					[ReadOnlyFrom],
					[SourceID],
					[WhoCreated],
					[WhenCreated],
					[WhoModified],
					[WhenModified],
					[FolderID],
					[IsThunderheadTemplate],
					[ThunderheadUniqueTemplateID],
					[ThunderheadDocumentFormat],
					[DocumentTitleTemplate],
					[CreatedBy],
					[CreatedOn],
					[VersionNumber],
					[ActiveFromDate],
					[Archived]
				FROM
					[dbo].[DocumentTypeVersion] WITH (NOLOCK) 
				WHERE
					[ClientID] = @ClientID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeVersion_GetByClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentTypeVersion_GetByClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentTypeVersion_GetByClientID] TO [sp_executeall]
GO
