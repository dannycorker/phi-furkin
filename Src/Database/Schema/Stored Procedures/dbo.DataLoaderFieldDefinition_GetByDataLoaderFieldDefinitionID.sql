SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderFieldDefinition table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFieldDefinition_GetByDataLoaderFieldDefinitionID]
(

	@DataLoaderFieldDefinitionID int   
)
AS


				SELECT
					[DataLoaderFieldDefinitionID],
					[ClientID],
					[DataLoaderMapID],
					[DataLoaderObjectTypeID],
					[DataLoaderMapSectionID],
					[DataLoaderObjectFieldID],
					[DetailFieldID],
					[DetailFieldAlias],
					[NamedValue],
					[Keyword],
					[DataLoaderKeywordMatchTypeID],
					[RowRelativeToKeyword],
					[ColRelativeToKeyword],
					[SectionRelativeRow],
					[SectionAbsoluteCol],
					[ValidationRegex],
					[Equation],
					[IsMatchField],
					[DecodeTypeID],
					[DefaultLookupItemID],
					[SourceDataLoaderFieldDefinitionID],
					[Notes],
					[AllowErrors]
				FROM
					[dbo].[DataLoaderFieldDefinition] WITH (NOLOCK) 
				WHERE
										[DataLoaderFieldDefinitionID] = @DataLoaderFieldDefinitionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFieldDefinition_GetByDataLoaderFieldDefinitionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFieldDefinition_GetByDataLoaderFieldDefinitionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFieldDefinition_GetByDataLoaderFieldDefinitionID] TO [sp_executeall]
GO
