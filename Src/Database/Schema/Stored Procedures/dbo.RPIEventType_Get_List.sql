SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the RPIEventType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[RPIEventType_Get_List]

AS


				
				SELECT
					[RPIEventTypeID],
					[RPIEventTypeName],
					[RPIEventTypeDescription],
					[IsAquariumTriggered],
					[IsEnabled],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[RPIEventType] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RPIEventType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RPIEventType_Get_List] TO [sp_executeall]
GO
