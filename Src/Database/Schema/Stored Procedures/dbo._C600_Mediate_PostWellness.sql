SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds / Alexandra Maguire
-- Create date: 2020-09-21
-- Description: Update OneVision and Post to Vision
-- =============================================
CREATE PROCEDURE [dbo].[_C600_Mediate_PostWellness]
AS
BEGIN

	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID(), 
			@LeadEventID INT,
			@OneVisionID INT,
			@MediateQueueID INT, 
			@MatterID INT,
			@PolicyOneVisionID INT,
			@VisionPolicyID INT,
			@VisionPetID INT


	DECLARE @UnprocessedRecords TABLE (OneVisionID INT, MatterID INT, LeadEventID INT)

	DECLARE @EmptyTableDoNotUse TABLE (RowsProcessed INT)

	EXEC dbo._C00_LogIt 'Info', '_C600_Mediate_PostWellness', '', 'Proc Started', 58552

	INSERT INTO @UnprocessedRecords (OneVisionID, MatterID, LeadEventID)
	SELECT ov.OneVisionID, m.MatterID, le.LeadEventID
	FROM OneVision ov WITH (NOLOCK) 
	JOIN Matter m WITH (NOLOCK) ON m.MatterID = ov.PAMatterID
	JOIN Cases ca WITH (NOLOCK) ON ca.CaseID = m.CaseID
	JOIN LeadEvent le WITH (NOLOCK) ON le.LeadEventID = 
	(SELECT TOP 1 le1.LeadEventID 
	FROM LeadEvent le1 
	WHERE le1.CaseID = ca.CaseID 
	AND le1.EventTypeID = 156920 
	ORDER BY 1 DESC)
	AND ov.VisionPolicyID IS NULL 
	AND ov.VisionPetID IS NULL 
	AND ov.VisionCustomerID IS NOT NULL
	AND ov.PolicyTermType = 'Wellness' 
	AND m.ClientID = @ClientID 
	ORDER BY ov.OneVisionID DESC

	IF EXISTS (SELECT * FROM @UnprocessedRecords)
	BEGIN
		WHILE (SELECT COUNT(*) FROM @UnprocessedRecords) >= 1
		BEGIN
			
			SELECT TOP 1 @OneVisionID = u.OneVisionID, @LeadEventID = u.LeadEventID, @MatterID = u.MatterID
			FROM @UnprocessedRecords u
			ORDER BY OneVisionID DESC

			SELECT @PolicyOneVisionID = OneVisionID FROM OneVision WITH (NOLOCK) WHERE PAMatterID = @MatterID AND PolicyTermType = 'Policy'
			--SELECT @WellnessOneVisionID = OneVisionID FROM OneVision WITH (NOLOCK) WHERE PAMatterID = @MatterID AND PolicyTermType = 'Wellness'
			/*Get VisionPolicyID and VisionPetID from Policy Row*/
			SELECT @VisionPolicyID = VisionPolicyID, @VisionPetID = VisionPetID
			FROM OneVision WITH (NOLOCK)
			WHERE OneVisionID = @PolicyOneVisionID

			/*Update Wellness OneVision record*/
			UPDATE o
			SET o.VisionPolicyID = @VisionPolicyID, o.VisionPetID = @VisionPetID
			FROM OneVision o WITH (NOLOCK)
			WHERE o.OneVisionID = @OneVisionID

			WAITFOR DELAY '00:00:10'

			EXEC dbo._C600_Mediate_QueueForVision @RequestType = 'Wellness',
			                                      @HttpMethod = 'POST',
			                                      @LeadEventID = @LeadEventID,
												  @OneVisionID = @OneVisionID



			DELETE FROM @UnprocessedRecords WHERE OneVisionID = @OneVisionID


		END
	END

	/*Return empty table to keep the scheduler happy*/
	SELECT *
	FROM @EmptyTableDoNotUse t 

	EXEC dbo._C00_LogIt 'Info', '_C600_Mediate_PostWellness', '', 'Proc Ended', 58552
   
END
GO
GRANT EXECUTE ON  [dbo].[_C600_Mediate_PostWellness] TO [sp_executeall]
GO
