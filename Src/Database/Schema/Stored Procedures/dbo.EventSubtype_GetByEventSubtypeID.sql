SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the EventSubtype table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventSubtype_GetByEventSubtypeID]
(

	@EventSubtypeID int   
)
AS


				SELECT
					[EventSubtypeID],
					[EventSubtypeName],
					[EventSubtypeDescription],
					[ApplyEventMaxThreshold],
					[Available]
				FROM
					[dbo].[EventSubtype] WITH (NOLOCK) 
				WHERE
										[EventSubtypeID] = @EventSubtypeID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtype_GetByEventSubtypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubtype_GetByEventSubtypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtype_GetByEventSubtypeID] TO [sp_executeall]
GO
