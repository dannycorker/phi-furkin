SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- ALTER date: 21/08/2017
-- Description:	Gets the cardtransaction map record from the master database
-- 2019-12-03 CPS for JIRA LPC-174 | Carried over from Aquarius427Dev
-- =============================================
CREATE PROCEDURE [dbo].[CardTransactionMap__GetByID]
	@CardTransactionMapID INT
AS
BEGIN
		
	SET NOCOUNT ON;

    EXEC AquariusMaster.dbo.CardTransactionMap__GetByID @CardTransactionMapID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransactionMap__GetByID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CardTransactionMap__GetByID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransactionMap__GetByID] TO [sp_executeall]
GO
