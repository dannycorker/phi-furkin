SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 2006-08-09
-- Description:	Update an Equation from eCatcher
-- JWG 2014-01-23 #25390 Set WhoModified etc
-- =============================================
CREATE PROCEDURE [dbo].[UpdateEquation]
	@EquationID INT,
	@EquationName NVARCHAR(50),
	@Equation TEXT,
	@ClientQuestionnaireID INT,
	@ClientID INT, 
	@WhoEdited INT
AS
BEGIN

	UPDATE Equations
	SET EquationName = @EquationName, 
	Equation = @Equation, 
	ClientQuestionnaireID = @ClientQuestionnaireID, 
	WhoModified = @WhoEdited, 
	WhenModified = dbo.fn_GetDate_Local()
	WHERE EquationID = @EquationID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateEquation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UpdateEquation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UpdateEquation] TO [sp_executeall]
GO
