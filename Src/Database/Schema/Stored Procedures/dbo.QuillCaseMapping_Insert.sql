SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the QuillCaseMapping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillCaseMapping_Insert]
(

	@QuillCaseMappingID int    OUTPUT,

	@ClientID int   ,

	@CaseID int   ,

	@QuillCaseCode varchar (50)  
)
AS


				
				INSERT INTO [dbo].[QuillCaseMapping]
					(
					[ClientID]
					,[CaseID]
					,[QuillCaseCode]
					)
				VALUES
					(
					@ClientID
					,@CaseID
					,@QuillCaseCode
					)
				-- Get the identity value
				SET @QuillCaseMappingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCaseMapping_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillCaseMapping_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCaseMapping_Insert] TO [sp_executeall]
GO
