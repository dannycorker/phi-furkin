SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ReferenceValueFormat table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ReferenceValueFormat_Delete]
(

	@ReferenceValueFormatID int   
)
AS


				DELETE FROM [dbo].[ReferenceValueFormat] WITH (ROWLOCK) 
				WHERE
					[ReferenceValueFormatID] = @ReferenceValueFormatID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueFormat_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ReferenceValueFormat_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ReferenceValueFormat_Delete] TO [sp_executeall]
GO
