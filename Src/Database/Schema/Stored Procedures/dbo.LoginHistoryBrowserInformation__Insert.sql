SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 21-01-2016
-- Description:	Inserts browser information record
-- =============================================
CREATE PROCEDURE [dbo].[LoginHistoryBrowserInformation__Insert]

	@LoginHistoryID INT, 
	@ClientID INT, 
	@ClientPersonnelID INT, 
	@UserAgentString VARCHAR(2000), 
	@BrowserFamily VARCHAR(50), 
	@BrowserName VARCHAR(50), 
	@BrowserVersion VARCHAR(50), 
	@BrowserMajor VARCHAR(10), 
	@BrowserMinor VARCHAR(10), 
	@BrowserPatch VARCHAR(10), 
	@DeviceFamily VARCHAR(50), 
	@DeviceName VARCHAR(50), 
	@DeviceVersion VARCHAR(50), 
	@DeviceMajor VARCHAR(10), 
	@DeviceMinor VARCHAR(10), 
	@DevicePatch VARCHAR(10), 
	@DeviceType VARCHAR(50), 
	@DeviceManufacturer VARCHAR(50), 
	@OperatingSystemFamily VARCHAR(50), 
	@OperatingSystemName VARCHAR(50), 
	@OperatingSystemVersion VARCHAR(50), 
	@OperatingSystemMajor VARCHAR(10), 
	@OperatingSystemMinor VARCHAR(10), 
	@OperatingSystemPatch VARCHAR(10)

AS
BEGIN
	
	SET NOCOUNT ON;

	INSERT INTO LoginHistoryBrowserInformation (LoginHistoryID, ClientID, ClientPersonnelID, UserAgentString, BrowserFamily, BrowserName, BrowserVersion, BrowserMajor, BrowserMinor, BrowserPatch, DeviceFamily, DeviceName, DeviceVersion, DeviceMajor, DeviceMinor, DevicePatch, DeviceType, DeviceManufacturer, OperatingSystemFamily, OperatingSystemName, OperatingSystemVersion, OperatingSystemMajor, OperatingSystemMinor, OperatingSystemPatch)
	VALUES (@LoginHistoryID, @ClientID, @ClientPersonnelID, @UserAgentString, @BrowserFamily, @BrowserName, @BrowserVersion, @BrowserMajor, @BrowserMinor, @BrowserPatch, @DeviceFamily, @DeviceName, @DeviceVersion, @DeviceMajor, @DeviceMinor, @DevicePatch, @DeviceType, @DeviceManufacturer, @OperatingSystemFamily, @OperatingSystemName, @OperatingSystemVersion, @OperatingSystemMajor, @OperatingSystemMinor, @OperatingSystemPatch)
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[LoginHistoryBrowserInformation__Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LoginHistoryBrowserInformation__Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LoginHistoryBrowserInformation__Insert] TO [sp_executeall]
GO
