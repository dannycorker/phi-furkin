SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2014-09-24
-- Description:	Validate GetQuoteValues Request
-- Mods
--	2015-05-14 DCM Ticket #31733 Entire contents moved to new proc to facilitate renaming in prelive
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Policy_GetQuoteValues_Validate]
(
	@XmlRequest XML,
	@XmlResponse XML OUTPUT,
	@Result INT OUTPUT
)
AS
BEGIN

	EXEC _C600_PA_Policy_GetQuoteValues_Validate @XmlRequest, @XmlResponse OUTPUT, @Result OUTPUT
		
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_GetQuoteValues_Validate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Policy_GetQuoteValues_Validate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_GetQuoteValues_Validate] TO [sp_executeall]
GO
