SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the CustomerAnswers table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CustomerAnswers_Insert]
(

	@CustomerAnswerID int    OUTPUT,

	@CustomerQuestionnaireID int   ,

	@MasterQuestionID int   ,

	@Answer varchar (2000)  ,

	@QuestionPossibleAnswerID int   ,

	@ClientID int   
)
AS


				
				INSERT INTO [dbo].[CustomerAnswers]
					(
					[CustomerQuestionnaireID]
					,[MasterQuestionID]
					,[Answer]
					,[QuestionPossibleAnswerID]
					,[ClientID]
					)
				VALUES
					(
					@CustomerQuestionnaireID
					,@MasterQuestionID
					,@Answer
					,@QuestionPossibleAnswerID
					,@ClientID
					)
				-- Get the identity value
				SET @CustomerAnswerID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerAnswers_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerAnswers_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerAnswers_Insert] TO [sp_executeall]
GO
