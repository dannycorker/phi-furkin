SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 31/07/2017
-- Description:	Gets a card transaction by its identity
-- 2019-12-03 CPS for JIRA LPC-174 | Brought over from Aquarius427Dev
-- =============================================
CREATE PROCEDURE [dbo].[CardTransaction__GetByID]
	@CardTransactionID INT,
	@ClientID INT
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT * FROM CardTransaction WITH (NOLOCK) 
	WHERE CardTransactionID=@CardTransactionID AND ClientID=@ClientID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__GetByID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CardTransaction__GetByID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CardTransaction__GetByID] TO [sp_executeall]
GO
