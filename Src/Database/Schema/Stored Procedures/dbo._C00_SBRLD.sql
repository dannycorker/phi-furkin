SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-01-03
-- Description:	SQL Before resource record delete
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SBRLD]
@ResourceListID int,
@ClientPersonnelID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SBRLD] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SBRLD] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SBRLD] TO [sp_executeall]
GO
