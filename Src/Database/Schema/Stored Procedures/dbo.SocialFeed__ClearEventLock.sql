SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 24-11-2015
-- Description:	Clear Social Feed From Event
-- =============================================
CREATE PROCEDURE [dbo].[SocialFeed__ClearEventLock]
	@SocialFeedID INT
AS
BEGIN
		
	SET NOCOUNT ON;

    UPDATE EventType
    SET SocialFeedID=0
    WHERE EventSubtypeID=29 And SocialFeedID=@SocialFeedID    
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__ClearEventLock] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SocialFeed__ClearEventLock] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SocialFeed__ClearEventLock] TO [sp_executeall]
GO
