SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-07-11
-- Description:	Saves rule outputs and coordinates
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleOutputs_SaveRuleOutputs]
(
	@RuleID INT,
	@ClientID INT,
	@UserID INT,
	@Outputs dbo.tvpIntVarcharVarchar READONLY
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @RuleOutputs TABLE
	(
		ID INT IDENTITY,
		Value VARCHAR(2000),
		TransformID INT,
		CoordinatesCsv VARCHAR(2000)
	)
	
	INSERT @RuleOutputs(TransformID, Value, CoordinatesCsv)
	SELECT AnyID, AnyValue1, AnyValue2
	FROM @Outputs
	
	-- Clear out existing rule outputs	
	DELETE co
	FROM dbo.RulesEngine_OutputCoordinates co
	INNER JOIN dbo.RulesEngine_RuleOutputs o ON o.RuleOutputID = co.RuleOutputID
	WHERE RuleID = @RuleID

	DELETE dbo.RulesEngine_RuleOutputs
	WHERE RuleID = @RuleID
	
	DECLARE @RuleOutputIDs TABLE
	(
		ID INT IDENTITY,
		RuleOutputID INT
	)
	
	INSERT INTO dbo.RulesEngine_RuleOutputs(RuleID, Value, TransformID)
	OUTPUT inserted.RuleOutputID INTO @RuleOutputIDs
	SELECT @RuleID, Value, TransformID
	FROM @RuleOutputs
	
	INSERT INTO dbo.RulesEngine_OutputCoordinates (RuleOutputID, ParameterOptionID)
	SELECT i.RuleOutputID, csv.AnyID
	FROM @RuleOutputs o
	INNER JOIN @RuleOutputIDs i ON o.ID = i.ID
	CROSS APPLY dbo.fnTableOfIDsFromCSV(o.CoordinatesCsv) csv
	
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleOutputs_SaveRuleOutputs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleOutputs_SaveRuleOutputs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleOutputs_SaveRuleOutputs] TO [sp_executeall]
GO
