SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 22/08/2016
-- Description: Queues an MTA. We supply the new premium.  Any adjustment (credit or debit) is queued for Collections 
--				and the payment schedule is re-calculated
-- Mods
-- =============================================
CREATE PROCEDURE [dbo].[_C600_PA_QueueMidTermAdjustment]
(
	@LeadEventID INT,
	@MatterID INT
)
AS
BEGIN

--declare 
--	@LeadEventID INT = 61578,
--	@MatterID INT = 5937

	SET NOCOUNT ON;

	DECLARE 
			@AdjustmentDate DATE,
			@AnnGross MONEY,
			@AnnNet MONEY,
			@AnnTax MONEY,
			@ClientID INT,
			@CostBreakDown VARCHAR(MAX),
			@Description VARCHAR(MAX),
			@LeadTypeID INT,
			@MTACalculationMethod INT,
			@NotificationDate DATE,
			@ObjectID INT,
			@OneOffAdjustmentWait INT,
			@PremiumCalculationDetailID INT,
			@PurchasedProduct INT,
			@RegularPaymentWait INT,
			@RuleSetID INT,
			@TableRowID INT
	

	SELECT TOP 1 
			@ClientID=mPCID.ClientID,
			@TableRowID=tPCID.TableRowID,
			@Description='Changes: ' + ISNULL(madj.DetailValue,''),
			@AnnGross=anngross.ValueMoney,
			@AnnTax=anntax.ValueMoney,
			@AnnNet=anngross.ValueMoney-anntax.ValueMoney,
			@AdjustmentDate=madjust.ValueDate,
			@NotificationDate=le.WhenCreated,
			@RegularPaymentWait=rpw.ValueInt,
			@OneOffAdjustmentWait=ooaw.ValueInt,
			@MTACalculationMethod=mcm.ValueInt,
			@PurchasedProduct=pp.ValueInt,
			@RuleSetID=mPCID.ValueInt,
			--@CostBreakDown=CAST(lx.ProductCostBreakdown AS VARCHAR(MAX)),
			@PremiumCalculationDetailID=mPCID.ValueInt,
			@LeadTypeID=l.LeadTypeID
	FROM Matter m WITH (NOLOCK)
	INNER JOIN MatterDetailValues mPCID WITH (NOLOCK) ON m.MatterID=mPCID.MatterID AND mPCID.DetailFieldID=175711 /*Current Premium/New - PremiumCalculationID*/
	INNER JOIN MatterDetailValues madj WITH (NOLOCK) ON m.MatterID=madj.MatterID AND madj.DetailFieldID=177387  -- adjustment summary
	INNER JOIN TableDetailValues tPCID WITH (NOLOCK) ON mPCID.ValueInt=tPCID.ValueInt AND tPCID.DetailFieldID=175709 /*PremiumCalculationID*/ AND mPCID.MatterID=tPCID.MatterID
	INNER JOIN TableDetailValues anntax WITH (NOLOCK) ON tPCID.TableRowID=anntax.TableRowID AND anntax.DetailFieldID=175375 /*Previous Annual IPT*/
	--INNER JOIN TableDetailValues anngross WITH (NOLOCK) ON tPCID.TableRowID=anngross.TableRowID AND anngross.DetailFieldID=175349 /*Previous Annual*/ /*CPS 2017-08-16 for ticket #44964*/
	INNER JOIN TableDetailValues tdv_live WITH (NOLOCK) ON tdv_live.TableRowID = tPCID.TableRowID AND tdv_live.DetailFieldID = 175722 
	INNER JOIN TableDetailValues anngross WITH (NOLOCK) ON tPCID.TableRowID=anngross.TableRowID AND anngross.DetailFieldID=177899 /*Premium less Discount*/
	INNER JOIN MatterDetailValues madjust WITH (NOLOCK) ON m.MatterID=madjust.MatterID AND madjust.DetailFieldID=175442 /*Adjustment Date*/
	INNER JOIN LeadEvent le WITH (NOLOCK) ON m.CaseID=le.CaseID AND le.EventDeleted=0 AND le.EventTypeID IN (155247 /*begin - Change Pet Details*/
																											,155246 /*begin - Change Customer Details*/
																											,156695 /*begin - Transfer of Ownership*/
																											,156689 /*PH Notification of Death of PH*/
																											)
	INNER JOIN ClientDetailValues rpw WITH (NOLOCK) ON rpw.ClientID=m.ClientID AND rpw.DetailFieldID=170228 /*Wait period for adjustments added to regular payments*/
	INNER JOIN ClientDetailValues ooaw WITH (NOLOCK) ON ooaw.ClientID=m.ClientID AND ooaw.DetailFieldID=177037 /*Wait period for adjustments taken as one-off payments*/
	INNER JOIN ClientDetailValues mcm WITH (NOLOCK) ON mcm.ClientID=m.ClientID AND mcm.DetailFieldID=177036 /*MTA Calculation Method*/
	INNER JOIN MatterDetailValues pp WITH (NOLOCK) ON m.MatterID=pp.MatterID AND pp.DetailFieldID=177074 /*Purchased Policy ID*/
	INNER JOIN dbo.PremiumCalculationDetail lx WITH (NOLOCK) ON lx.PremiumCalculationDetailID=mPCID.ValueInt
	INNER JOIN Lead l WITH (NOLOCK) ON m.LeadID=l.LeadID
	WHERE mPCID.MatterID=@MatterID
	AND NOT EXISTS ( SELECT * FROM LeadEvent le2 WITH (NOLOCK) WHERE le2.CaseID=le.CaseID 
																	AND le2.EventDeleted=0 
																	AND le2.EventTypeID IN (155247,155246,156695,156689) 
																	AND le2.LeadEventID > le.LeadEventID )
	AND tdv_live.ValueInt = 72326 /*2018-06-14 ACE Make sure we pick the live row!*/


	UPDATE LeadEvent 
	Set Comments = Comments 
		+ CASE WHEN Comments='' THEN '' ELSE ' ' END 
		+ 'MTA table row ID: ' 
		+  CAST(@TableRowID AS VARCHAR)
	WHERE LeadEventID=@LeadEventID

	SELECT 
			@ClientID [ClientID],
			@TableRowID [TableRowID],
			@Description [Description],
			@AnnGross [AnnGross],
			@AnnTax [AnnTax],
			@AnnNet [AnnNet],
			@AdjustmentDate [AdjustmentDate],
			@NotificationDate [NotificationDate],
			@RegularPaymentWait [RegularPaymentWait],
			@OneOffAdjustmentWait [OneOffAdjustmentWait],
			@MTACalculationMethod [MTACalculationMethod],
			@PurchasedProduct [PurchasedProduct],
			@RuleSetID [RuleSetID],
			@CostBreakDown [CostBreakDown],
			@LeadTypeID [LeadTypeID]

	-- clear helper or update fields
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4397)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @Description, @MatterID -- Description
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4399)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @AnnGross, @MatterID -- Premium Amount
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4414)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @AnnNet, @MatterID -- Premium Net
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4415)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @AnnTax, @MatterID -- Premium VAT
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4401)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @AdjustmentDate, @MatterID -- Adjustment Date
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4402)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @NotificationDate, @MatterID -- Notification Date
	-- waits are taken from billing configuration
	--SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4408)
	--EXEC dbo._C00_SimpleValueIntoField @ObjectID, @RegularPaymentWait, @MatterID -- Regular Payment Wait
	--SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4409)
	--EXEC dbo._C00_SimpleValueIntoField @ObjectID, @OneOffAdjustmentWait, @MatterID -- One Off Adjustment Wait
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4410)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @MTACalculationMethod, @MatterID -- MTA Calculation Method ID
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4413)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @PurchasedProduct, @MatterID -- Purchased Product ID
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4404)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @RuleSetID, @MatterID -- RuleSetID
	--SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4406)
	--EXEC dbo._C00_SimpleValueIntoField @ObjectID, @CostBreakDown, @MatterID -- Cost Breakdown
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4507)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @PremiumCalculationDetailID, @MatterID -- PremiumCalculationDetailID
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4411)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, 8, @MatterID -- ObjectTypeID
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4412)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, @TableRowID, @MatterID -- ObjectID
	SELECT @ObjectID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @leadTypeID, 105, 4530)
	EXEC dbo._C00_SimpleValueIntoField @ObjectID, 0, @MatterID -- AdjustmentValueOnly

END





GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_QueueMidTermAdjustment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_PA_QueueMidTermAdjustment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_PA_QueueMidTermAdjustment] TO [sp_executeall]
GO
