SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the AssetType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AssetType_Insert]
(

	@AssetTypeID int    OUTPUT,

	@AssetSubTypeID int   ,

	@AssetTypeName varchar (255)  ,

	@Note varchar (255)  ,

	@Enabled bit   ,

	@Deleted bit   ,

	@WhoCreated int   ,

	@WhenCreated datetime   ,

	@WhoModified int   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[AssetType]
					(
					[AssetSubTypeID]
					,[AssetTypeName]
					,[Note]
					,[Enabled]
					,[Deleted]
					,[WhoCreated]
					,[WhenCreated]
					,[WhoModified]
					,[WhenModified]
					)
				VALUES
					(
					@AssetSubTypeID
					,@AssetTypeName
					,@Note
					,@Enabled
					,@Deleted
					,@WhoCreated
					,@WhenCreated
					,@WhoModified
					,@WhenModified
					)
				-- Get the identity value
				SET @AssetTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AssetType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AssetType_Insert] TO [sp_executeall]
GO
