SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-07-14
-- Description:	Supports report/batch job copying (see below)
-- =============================================
CREATE PROCEDURE [dbo].[_C00_LeadTypeCopy_Reports] 

	@AnalysisType INT = 1, -- 1 = search for candidate reports to copy, 2 = produce copy script, 3 = list potential changes for defined query
	@NewLeadTypeID int,
	@NewRTLeadTypeID int = -1,
	@NewClientID int,
	@OldClientID int,
	@OldLeadTypeID int,
	@RunAsUserID int,
	@AddtionalMap VARCHAR(MAX),
	@NewPrefix VARCHAR(20)='',
	@OldPrefix VARCHAR(20)='',
	@QueryID INT=0,
	@Inspect INT=1
	
AS
BEGIN

/* 

	Supports report copying. First an object identifier map is created using the SourceID columns and any additional identifiers
	passed in in @AddtionalMap.
	
	@AnalysisType = 1. Searches for reports (and batch jobs that use them) that use any of the object identifiers included in the Map
					for the old lead type. You can use this to help decide which reports/batch jobs need copying & which updating and also
					what additions are required to the report folder structure.

	@AnalysisType = 2. Creates a list of exec commands either _C00_CopyReport or _C00_CopyATandReport as appropriate, using Folder.SourceID,
	                   if set, to identify the new folder for the report.
	
	@AnalysisType = 3. Called with an indivdual queryID. Checks the query against the map looking for any remaining identifiers
					   that may need updating.  Searches SqlQueryCriteria.Criteria1 for editable reports and 
					   SqlQuery.QueryText for non-editable reports. So for reports that just need updating this should identify the OIDs
					   that need adding/adjusting.

 */
	SET NOCOUNT ON;
	
	DECLARE @IsNumberSearch INT = 0,
		 	@SQL VARCHAR(MAX),
		 	@Break int,
			@Pos int = 2,
			@Add1 varchar(10),
			@Add2 varchar(10),
			@Add3 varchar(10),
			@Temp varchar(max)
					
	DECLARE @Tokens TABLE (Seq INT IDENTITY(1,1), 
							Token VARCHAR(MAX),
							Done INT )	
						
	DECLARE @Map TABLE (Seq INT IDENTITY(1,1), 
						OldID VARCHAR(30), 
						NewID VARCHAR(30), 
						Type VARCHAR(20), 
						Dup VARCHAR(10))

	-- Create Map
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT dfnew.SourceID,dfnew.DetailFieldID,'DF','' FROM DetailFields dfnew WITH (NOLOCK) WHERE (dfnew.LeadTypeID in (@NewLeadTypeID,@NewRTLeadTypeID) or (LeadOrMatter in (10,12,13) and ClientID=@NewClientID) ) and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT etnew.SourceID,etnew.EventTypeID,'ET','' FROM EventType etnew WITH (NOLOCK) WHERE etnew.LeadTypeID in (@NewLeadTypeID,@NewRTLeadTypeID) and SourceID is not null and SourceID <> 0 
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT dtnew.SourceID,dtnew.DocumentTypeID,'DT','' FROM DocumentType dtnew WITH (NOLOCK) WHERE dtnew.LeadTypeID in (@NewLeadTypeID,@NewRTLeadTypeID) and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT dtnew.SourceID,dtnew.LookupListItemID,'LI','' FROM LookupListItems dtnew WITH (NOLOCK) WHERE dtnew.ClientID=@NewClientID and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT dtnew.SourceID,dtnew.LookupListID,'LL','' FROM LookupList dtnew WITH (NOLOCK) WHERE dtnew.ClientID=@NewClientID and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT dtnew.SourceID,dtnew.ResourceListID,'RL','' FROM ResourceList dtnew WITH (NOLOCK) WHERE dtnew.ClientID=@NewClientID and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT dtnew.SourceID,dtnew.DetailFieldPageID,'DFP','' FROM Detailfieldpages dtnew WITH (NOLOCK) WHERE dtnew.ClientID=@NewClientID and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT dtnew.SourceID,dtnew.LeadTypeID,'LT','' FROM LeadType dtnew WITH (NOLOCK) WHERE dtnew.ClientID=@NewClientID and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT wg.SourceID,wg.WorkflowGroupID,'WG','' FROM WorkflowGroup wg WITH (NOLOCK) WHERE wg.ClientID=@NewClientID and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup)
	SELECT eg.SourceID,eg.EventGroupID,'EG','' FROM EventGroup eg WITH (NOLOCK) WHERE eg.ClientID=@NewClientID and SourceID is not null and SourceID <> 0
	INSERT INTO @Map (OldID,NEWID,TYPE,Dup) VALUES											
	(@OldClientID,@NewClientID,'CL','')

	-- add any specials to the map
	SELECT @Temp=@AddtionalMap
	WHILE @AddtionalMap <> ''
	BEGIN
	
		SELECT @Add1='',@Add2='',@Add2=''
		
		SELECT @Add1=LEFT(@AddtionalMap,CASE WHEN CHARINDEX(',',@AddtionalMap)=0 THEN LEN(@AddtionalMap) ELSE CHARINDEX(',',@AddtionalMap)-1 END), @AddtionalMap=RIGHT(@AddtionalMap,CASE WHEN CHARINDEX(',',@AddtionalMap)=0 THEN 0 ELSE LEN(@AddtionalMap) - CHARINDEX(',',@AddtionalMap) END)
		SELECT @Add2=LEFT(@AddtionalMap,CASE WHEN CHARINDEX(',',@AddtionalMap)=0 THEN LEN(@AddtionalMap) ELSE CHARINDEX(',',@AddtionalMap)-1 END), @AddtionalMap=RIGHT(@AddtionalMap,CASE WHEN CHARINDEX(',',@AddtionalMap)=0 THEN 0 ELSE LEN(@AddtionalMap) - CHARINDEX(',',@AddtionalMap) END)
		SELECT @Add3=LEFT(@AddtionalMap,CASE WHEN CHARINDEX(',',@AddtionalMap)=0 THEN LEN(@AddtionalMap) ELSE CHARINDEX(',',@AddtionalMap)-1 END), @AddtionalMap=RIGHT(@AddtionalMap,CASE WHEN CHARINDEX(',',@AddtionalMap)=0 THEN 0 ELSE LEN(@AddtionalMap) - CHARINDEX(',',@AddtionalMap) END)

		IF @Add1<>'' AND @Add2<>'' AND @Add3<>''
			INSERT INTO @Map (OldID,NEWID,TYPE,Dup) VALUES
			(@Add1,@Add2,@Add3,'')
	
	END
	SELECT @AddtionalMap=@Temp
	
	-- look for duplicates
	UPDATE m1
	SET m1.Dup='do'
	FROM @Map m1 
	INNER JOIN @Map m2 ON m1.OldID=m2.OldID AND m1.Seq <> m2.Seq
	UPDATE m1
	SET m1.Dup=m1.Dup+'dn'
	FROM @Map m1 
	INNER JOIN @Map m2 ON m1.NewID=m2.NewID AND m1.Seq <> m2.Seq
		
	IF @AnalysisType in (1,2)  -- 1. find reports using Old object IDs, 2. constructs copy statements
	BEGIN
	
		DELETE FROM @Map WHERE Type='CL'
	
		DECLARE @Reports TABLE (ReportID INT, 
								TaskID INT, 
								IsEditable INT, 
								FolderID INT,
								FolderName VARCHAR(500),
								ParentID INT, 
								QueryName VARCHAR(500), 
								TaskName VARCHAR(500), 
								Candidate INT, 
								Done INT )
		
		DECLARE @ReportID INT
		
		INSERT INTO @Reports (ReportID,Candidate,Done)
		SELECT QueryID,0,0 FROM SqlQuery WITH (NOLOCK) WHERE ClientID=@OldClientID and FolderID <> -1 
													and FolderID NOT IN (5647,5770,6438) -- 261 specific

		WHILE EXISTS ( SELECT * FROM @Reports WHERE Done=0)
		BEGIN
		
			SELECT TOP 1 @ReportID=ReportID FROM @Reports WHERE Done=0
			
			SELECT @SQL = ' ' + QueryText FROM SqlQuery WITH (NOLOCK) WHERE QueryID=@ReportID

			WHILE LEN(@sql) > 0
			BEGIN

				IF @IsNumberSearch = 0
				BEGIN
				
					INSERT INTO @Tokens (Token,Done)
					SELECT LEFT(@SQL,CASE WHEN PATINDEX('%[0-9]%' ,@SQL) = 0 THEN LEN(@SQL) ELSE PATINDEX('%[0-9]%' ,@SQL)-1 END), 0
					SELECT @SQL=CASE WHEN PATINDEX('%[0-9]%' ,@SQL) = 0 THEN '' ELSE RIGHT(@SQL,LEN(@SQL)-PATINDEX('%[0-9]%',@SQL)+1) END
				
					SELECT @IsNumberSearch = 1
				END
				ELSE
				BEGIN
				
					INSERT INTO @Tokens (Token,Done)
					SELECT LEFT(@SQL,CASE WHEN PATINDEX('%[^0-9]%' ,@SQL) = 0 THEN LEN(@SQL) ELSE PATINDEX('%[^0-9]%' ,@SQL)-1 END), 0
					SELECT @SQL=CASE WHEN PATINDEX('%[^0-9]%' ,@SQL) = 0 THEN '' ELSE RIGHT(@SQL,LEN(@SQL)-PATINDEX('%[^0-9]%',@SQL)+1) END
				
					SELECT @IsNumberSearch = 0
				END

			END 

			-- update reports table if there is any match
			UPDATE r
			set Candidate=1,
				IsEditable=sq.IsEditable,
				TaskID=ISNULL(atp.TaskID,0),
				QueryName=sq.QueryTitle,
				TaskName=ISNULL(at.Taskname,''),
				FolderID=sq.FolderID,
				FolderName=f.FolderName,
				ParentID=f.FolderParentID				
			FROM @Reports r
			INNER JOIN SqlQuery sq WITH (NOLOCK) ON r.ReportID=sq.QueryID
			INNER JOIN Folder f WITH (NOLOCK) ON sq.FolderID=f.FolderID
			LEFT JOIN AutomatedTaskParam atp WITH (NOLOCK) ON sq.QueryID=atp.ParamValue and atp.ParamName='SQL_QUERY' and atp.ClientID=sq.ClientID
			LEFT JOIN AutomatedTask at WITH (NOLOCK) ON at.TaskID=atp.TaskID
			WHERE r.ReportID=@ReportID
			AND EXISTS ( SELECT * FROM @Tokens t	
							INNER JOIN @Map m ON t.Token=m.OldID )

			
			UPDATE @Reports SET Done=1 WHERE ReportID=@ReportID
			SELECT @SQL='',@IsNumberSearch = 0
			DELETE FROM @Tokens
		
		END

		IF @AnalysisType=1	
		BEGIN	
			SELECT r.ReportID,r.QueryName,r.ParentID,r.FolderID,r.FolderName,r.IsEditable,r.TaskID,r.TaskName FROM @Reports r WHERE Candidate=1 order by FolderID,QueryName
		END
		ELSE
		BEGIN
			
			SELECT r.ReportID, r.FolderID, r.QueryName,
			'exec _C00_CopyReport ' + CONVERT(VARCHAR(30),r.ReportID) + ',' + 
									CASE WHEN f.FolderID IS NOT NULL THEN CONVERT(VARCHAR(30),f.FolderID) ELSE CONVERT(VARCHAR(30),r.FolderID) END + ',' +   
									CONVERT(VARCHAR(30),@OldLeadTypeID) + ',' +
									CONVERT(VARCHAR(30),@NewLeadTypeID) + ',''' + @OldPrefix + ''',''' + @NewPrefix + ''''
			FROM @Reports r 
			LEFT JOIN Folder f WITH (NOLOCK) ON r.FolderID = f.SourceID and f.ClientID=@NewClientID
			WHERE r.Candidate=1 and r.TaskID=0
			UNION
			SELECT r.ReportID, r.FolderID, r.QueryName, 
			'exec _C00_CopyATandReport ' + CONVERT(VARCHAR(30),r.TaskID) + ',' + 
									CASE WHEN f.FolderID IS NOT NULL THEN CONVERT(VARCHAR(30),f.FolderID) ELSE CONVERT(VARCHAR(30),r.FolderID) END + ',''' +   
									@NewPrefix + ''',''' + @OldPrefix + ''','''','''',' +
									CONVERT(VARCHAR(30),@OldLeadTypeID) + ',' +
									CONVERT(VARCHAR(30),@NewLeadTypeID) + ',' +
									CONVERT(VARCHAR(30),@RunAsUserID) + ',NULL'
			FROM @Reports r 
			LEFT JOIN Folder f WITH (NOLOCK) ON r.FolderID = f.SourceID and f.ClientID=@NewClientID
			WHERE r.Candidate=1 and r.TaskID<>0 
			order by FolderID,QueryName
		
		END
		
	END	
	
	IF @AnalysisType=3 -- list possible updates
	BEGIN
	
		SELECT * FROM @Map m order by m.Type,m.OldID

		IF EXISTS ( SELECT * FROM SqlQuery WITH (NOLOCK) WHERE QueryID=@QueryID and IsEditable=1 )
		BEGIN
		
			Select sc.CriteriaText,sc.Criteria1,m.OldID,m.NewID,m.Type 
			from SqlQueryCriteria sc WITH (NOLOCK) 
			LEFT JOIN @Map m ON (sc.Criteria1=m.OldID AND ISNUMERIC(sc.Criteria1)=1) OR
			                    (sc.Criteria1 LIKE '%' + m.OldID + '%' AND ISNUMERIC(sc.Criteria1)=0) OR
			                    (sc.Criteria1 LIKE '%' + m.OldID + '%' AND (ISNUMERIC(sc.Criteria1)=1 AND sc.Criteria1 LIKE '%,%'))			                    
			WHERE sc.SqlQueryID=@QueryID
		
		END
		ELSE
		BEGIN
		
			SELECT @SQL=QueryText FROM SqlQuery WITH (NOLOCK) WHERE QueryID=@QueryID
			exec _C00_LeadTypeCopy_SwitchObjectIdentifiers @NewLeadTypeID,@NewRTLeadTypeID,@NewClientID,@OldClientID,NULL,@SQL,@AddtionalMap,@Inspect
		
		END
	
	END
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LeadTypeCopy_Reports] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_LeadTypeCopy_Reports] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_LeadTypeCopy_Reports] TO [sp_executeall]
GO
