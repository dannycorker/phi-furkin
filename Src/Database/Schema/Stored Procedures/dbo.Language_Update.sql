SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the Language table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Language_Update]
(

	@LanguageID int   ,

	@OriginalLanguageID int   ,

	@LanguageName varchar (50)  ,

	@Alpha2Code char (2)  ,

	@Alpha3Code char (3)  ,

	@NativeName nvarchar (50)  ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[Language]
				SET
					[LanguageID] = @LanguageID
					,[LanguageName] = @LanguageName
					,[Alpha2Code] = @Alpha2Code
					,[Alpha3Code] = @Alpha3Code
					,[NativeName] = @NativeName
					,[WhenCreated] = @WhenCreated
					,[WhenModified] = @WhenModified
				WHERE
[LanguageID] = @OriginalLanguageID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Language_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Language_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Language_Update] TO [sp_executeall]
GO
