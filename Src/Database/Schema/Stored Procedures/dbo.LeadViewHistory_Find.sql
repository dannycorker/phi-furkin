SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LeadViewHistory table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadViewHistory_Find]
(

	@SearchUsingOR bit   = null ,

	@LeadViewHistoryID int   = null ,

	@ClientPersonnelID int   = null ,

	@ClientID int   = null ,

	@LeadID int   = null ,

	@WhenViewed datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LeadViewHistoryID]
	, [ClientPersonnelID]
	, [ClientID]
	, [LeadID]
	, [WhenViewed]
    FROM
	[dbo].[LeadViewHistory] WITH (NOLOCK) 
    WHERE 
	 ([LeadViewHistoryID] = @LeadViewHistoryID OR @LeadViewHistoryID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadID] = @LeadID OR @LeadID IS NULL)
	AND ([WhenViewed] = @WhenViewed OR @WhenViewed IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LeadViewHistoryID]
	, [ClientPersonnelID]
	, [ClientID]
	, [LeadID]
	, [WhenViewed]
    FROM
	[dbo].[LeadViewHistory] WITH (NOLOCK) 
    WHERE 
	 ([LeadViewHistoryID] = @LeadViewHistoryID AND @LeadViewHistoryID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadID] = @LeadID AND @LeadID is not null)
	OR ([WhenViewed] = @WhenViewed AND @WhenViewed is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadViewHistory_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadViewHistory_Find] TO [sp_executeall]
GO
