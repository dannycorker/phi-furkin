SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-20
-- Description:	List all DetailFields for a client, leadtype, leadormatter and questiontypeid
-- =============================================
CREATE PROCEDURE [dbo].[df] 
	@ClientID int, 
	@LeadTypeID int = NULL, 
	@LeadOrMatter int = NULL,
	@QuestionTypeID int = NULL  
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	FROM dbo.DetailFields df (nolock) 
	WHERE (df.ClientID = @ClientID) 
	AND (df.LeadTypeID = @LeadTypeID OR @LeadTypeID IS NULL) 
	AND (df.LeadOrMatter = @LeadOrMatter OR @LeadOrMatter IS NULL) 
	AND (df.QuestionTypeID = @QuestionTypeID OR @QuestionTypeID IS NULL) 
	ORDER BY df.ClientID, df.FieldCaption, df.LeadTypeID, df.LeadOrMatter, df.QuestionTypeID  
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[df] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[df] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[df] TO [sp_executeall]
GO
