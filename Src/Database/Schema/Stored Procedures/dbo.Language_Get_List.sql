SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the Language table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Language_Get_List]

AS


				
				SELECT
					[LanguageID],
					[LanguageName],
					[Alpha2Code],
					[Alpha3Code],
					[NativeName],
					[WhenCreated],
					[WhenModified]
				FROM
					[dbo].[Language] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Language_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Language_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Language_Get_List] TO [sp_executeall]
GO
