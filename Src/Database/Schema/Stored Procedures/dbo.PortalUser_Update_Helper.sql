SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2011-12-08
-- Description:	Allows a quick update of the portal user table correctly setting the context which this table enforces
-- =============================================


CREATE PROCEDURE [dbo].[PortalUser_Update_Helper]
(
	@PortalUserID INT,
	@Username VARCHAR(50),
	@ClientID INT = NULL,
	@CustomerID INT = NULL,
	@Password VARCHAR(65) = NULL,
	@Salt VARCHAR (25) = NULL,
	@ClientPersonnelID INT = NULL,
	@Enabled BIT = NULL,
	@AttemptedLogins INT = NULL,
	@PortalUserGroupID INT = NULL
)
AS

BEGIN
			
	-- Updates the master record
	EXEC AquariusMaster.dbo.AQ_PortalUser_Update @PortalUserID, @Username
	
	-- Set context info to allow access to table	
	DECLARE @ContextInfo VARBINARY(100) = CAST('PortalUser' AS VARBINARY)
	SET CONTEXT_INFO @ContextInfo
	
	UPDATE dbo.PortalUser
	SET Username = @Username,
	ClientID = ISNULL(@ClientID, ClientID),
	CustomerID = ISNULL(@CustomerID, CustomerID),
	Password = ISNULL(@Password, Password),
	Salt = ISNULL(@Salt, Salt),
	ClientPersonnelID = ISNULL(@ClientPersonnelID, ClientPersonnelID),
	Enabled = ISNULL(@Enabled, Enabled),
	AttemptedLogins = ISNULL(@AttemptedLogins, AttemptedLogins),
	PortalUserGroupID = ISNULL(@PortalUserGroupID, PortalUserGroupID)
	WHERE PortalUserID = @PortalUserID		
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser_Update_Helper] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUser_Update_Helper] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUser_Update_Helper] TO [sp_executeall]
GO
