SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Dave Morgan
-- Create date: 2017-02-02
-- Description:	Client specific XML logging house-keeping
-- 2018-11-16 JWG #53803 Keep 62 days, delete the rest. No point keeping empty log records forever.
-- 2019-10-11 GPR add QuotepetProductCheckpoint housekeeping for LPC-36
-- =============================================
CREATE PROCEDURE [dbo].[_C00_XMLHousekeeping]
AS
BEGIN

	SET NOCOUNT ON;
	
	/* 2018-11-16 JWG #53803 New logic */
	/* Delete child values first */
	DECLARE @DateMinus62 DATE = CAST(DATEADD(DAY,-62,dbo.fn_GetDate_Local()) AS DATE)
	
	DELETE qv 
	FROM dbo._C600_SavedQuote sq
	INNER JOIN dbo._C600_QuoteValues qv ON sq.QuoteSessionID = qv.QuoteSessionID
	WHERE sq.WhenExpire < @DateMinus62
	
	/* Then the parent records */
	DELETE dbo._C600_SavedQuote 
	WHERE WhenExpire < @DateMinus62
	
	/* Weekly Tasks, rebuild the indexes releases even more disk space than the deletes alone */
	/* SUNDAY = 1 */
	IF DATEPART(dw, dbo.fn_GetDate_Local()) = 1 
	BEGIN 
		ALTER INDEX ALL ON dbo.LogXML REBUILD;
		ALTER INDEX ALL ON dbo._C600_SavedQuote REBUILD;
		ALTER INDEX ALL ON dbo._C600_QuoteValues REBUILD;
	END

	/*GPR 2019-10-11 for LPC-36*/
	/*QuotePetProductCheckpoint housekeeping*/
	EXEC _C00_QuotePetProductHousekeeping
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_XMLHousekeeping] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_XMLHousekeeping] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_XMLHousekeeping] TO [sp_executeall]
GO
