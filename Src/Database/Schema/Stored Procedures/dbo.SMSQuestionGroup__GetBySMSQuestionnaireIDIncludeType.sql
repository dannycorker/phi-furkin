SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 18-04-2013
-- Description:	Gets a list of the SMSQuestionGroups by SMSQuestionnaireID
-- =============================================
CREATE PROCEDURE [dbo].[SMSQuestionGroup__GetBySMSQuestionnaireIDIncludeType]
(
	@SMSQuestionnaireID INT   
)
AS

	SET ANSI_NULLS ON
				
	SELECT g.*, o.Description FROM SMSQuestionGroup g WITH (NOLOCK) 
	INNER JOIN dbo.SMSQuestionOrder o WITH (NOLOCK) ON o.SMSQuestionOrderID = g.SMSQuestionOrderID
	WHERE g.SMSQuestionnaireID = @SMSQuestionnaireID
	ORDER BY GroupOrder ASC
			
	SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup__GetBySMSQuestionnaireIDIncludeType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSQuestionGroup__GetBySMSQuestionnaireIDIncludeType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSQuestionGroup__GetBySMSQuestionnaireIDIncludeType] TO [sp_executeall]
GO
