SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the ClientPersonnel table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnel_Get_List]

AS


				
				SELECT
					[ClientPersonnelID],
					[ClientID],
					[ClientOfficeID],
					[TitleID],
					[FirstName],
					[MiddleName],
					[LastName],
					[JobTitle],
					[Password],
					[ClientPersonnelAdminGroupID],
					[MobileTelephone],
					[HomeTelephone],
					[OfficeTelephone],
					[OfficeTelephoneExtension],
					[EmailAddress],
					[ChargeOutRate],
					[UserName],
					[Salt],
					[AttemptedLogins],
					[AccountDisabled],
					[ManagerID],
					[Initials],
					[LanguageID],
					[SubClientID],
					[ForcePasswordChangeOn],
					[ThirdPartySystemId],
					[CustomerID],
					[IsAquarium],
					[AllowSmsCommandProcessing],
					[MemorableWord],
					[MemorableWordSalt],
					[MemorableWordAttempts],
					[PendingActivation]
				FROM
					[dbo].[ClientPersonnel] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnel_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnel_Get_List] TO [sp_executeall]
GO
