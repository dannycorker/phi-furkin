SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ScriptLock table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ScriptLock_Delete]
(

	@ScriptLockID int   
)
AS


				DELETE FROM [dbo].[ScriptLock] WITH (ROWLOCK) 
				WHERE
					[ScriptLockID] = @ScriptLockID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptLock_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ScriptLock_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ScriptLock_Delete] TO [sp_executeall]
GO
