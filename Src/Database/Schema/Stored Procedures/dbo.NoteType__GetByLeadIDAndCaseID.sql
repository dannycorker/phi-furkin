SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
Modified	2014-07-14	SB	Changed to use the new view for lead type sharing
*/

CREATE PROCEDURE [dbo].[NoteType__GetByLeadIDAndCaseID]
(
	@LeadID int,
	@CaseID int,
	@EventCount int = 50
)
AS

	SET ROWCOUNT @EventCount
	
	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID

	SELECT
		le.LeadEventID,
		le.LeadID,
		le.Comments,
		convert(char(11),le.WhenCreated,103) + LEFT(convert(char(5),LE.WhenCreated,108),5) AS 'When',
		cp.UserName,
		le.NotePriority,
		isnull(nt.AlertColour, '') as [AlertColour],
		isnull(nt.NormalColour, '') as [NormalColour]
	FROM
		LeadEvent le WITH (NOLOCK) 
		LEFT OUTER JOIN EventType et WITH (NOLOCK) ON le.EventTypeID = et.EventTypeID
		LEFT OUTER JOIN dbo.fnNoteTypeShared(@ClientID) nt ON le.NoteTypeID = nt.NoteTypeID
		INNER JOIN ClientPersonnel cp WITH (NOLOCK) ON le.WhoCreated = cp.ClientPersonnelID 
	WHERE 
		le.LeadID = @LeadID 
	AND le.CaseID = @CaseID 
	AND	(le.EventDeleted = 0 OR le.EventDeleted = 0)
	AND et.EventTypeID is null
		
ORDER BY IsNull(le.NotePriority,0) desc, le.WhenCreated desc, le.LeadEventID desc


GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType__GetByLeadIDAndCaseID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[NoteType__GetByLeadIDAndCaseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[NoteType__GetByLeadIDAndCaseID] TO [sp_executeall]
GO
