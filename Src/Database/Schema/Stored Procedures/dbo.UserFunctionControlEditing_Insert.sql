SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the UserFunctionControlEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserFunctionControlEditing_Insert]
(

	@UserFunctionControlEditingID int    OUTPUT,

	@ClientPersonnelID int   ,

	@ModuleID int   ,

	@FunctionTypeID int   ,

	@HasDescendants int   ,

	@RightID int   ,

	@LeadTypeID int   
)
AS


				
				INSERT INTO [dbo].[UserFunctionControlEditing]
					(
					[ClientPersonnelID]
					,[ModuleID]
					,[FunctionTypeID]
					,[HasDescendants]
					,[RightID]
					,[LeadTypeID]
					)
				VALUES
					(
					@ClientPersonnelID
					,@ModuleID
					,@FunctionTypeID
					,@HasDescendants
					,@RightID
					,@LeadTypeID
					)
				-- Get the identity value
				SET @UserFunctionControlEditingID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControlEditing_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserFunctionControlEditing_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControlEditing_Insert] TO [sp_executeall]
GO
