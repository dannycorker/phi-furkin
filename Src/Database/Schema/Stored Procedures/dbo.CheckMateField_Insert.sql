SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the CheckMateField table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CheckMateField_Insert]
(

	@CheckMateFieldID int    OUTPUT,

	@CheckMateFieldName varchar (50)  ,

	@CheckMateFieldDescription varchar (250)  ,

	@QuestionTypeID int   ,

	@LookupListID int   ,

	@IsEnabled bit   ,

	@WhenCreated datetime   ,

	@WhenModified datetime   
)
AS


				
				INSERT INTO [dbo].[CheckMateField]
					(
					[CheckMateFieldName]
					,[CheckMateFieldDescription]
					,[QuestionTypeID]
					,[LookupListID]
					,[IsEnabled]
					,[WhenCreated]
					,[WhenModified]
					)
				VALUES
					(
					@CheckMateFieldName
					,@CheckMateFieldDescription
					,@QuestionTypeID
					,@LookupListID
					,@IsEnabled
					,@WhenCreated
					,@WhenModified
					)
				-- Get the identity value
				SET @CheckMateFieldID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateField_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CheckMateField_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CheckMateField_Insert] TO [sp_executeall]
GO
