SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the EventSubtype table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventSubtype_Update]
(

	@EventSubtypeID int   ,

	@OriginalEventSubtypeID int   ,

	@EventSubtypeName varchar (50)  ,

	@EventSubtypeDescription varchar (250)  ,

	@ApplyEventMaxThreshold int   ,

	@Available bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[EventSubtype]
				SET
					[EventSubtypeID] = @EventSubtypeID
					,[EventSubtypeName] = @EventSubtypeName
					,[EventSubtypeDescription] = @EventSubtypeDescription
					,[ApplyEventMaxThreshold] = @ApplyEventMaxThreshold
					,[Available] = @Available
				WHERE
[EventSubtypeID] = @OriginalEventSubtypeID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtype_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventSubtype_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventSubtype_Update] TO [sp_executeall]
GO
