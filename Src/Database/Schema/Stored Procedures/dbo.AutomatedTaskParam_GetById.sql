SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.AutomatedTaskParam_GetById    Script Date: 08/09/2006 12:22:49 ******/
CREATE PROCEDURE [dbo].[AutomatedTaskParam_GetById] 
	@taskid int, @ClientID int
AS
	select paramname, paramvalue
	from    dbo.AutomatedTaskParam with(nolock)
	where TaskId = @taskid 
	and ClientID = @ClientID




GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_GetById] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutomatedTaskParam_GetById] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutomatedTaskParam_GetById] TO [sp_executeall]
GO
