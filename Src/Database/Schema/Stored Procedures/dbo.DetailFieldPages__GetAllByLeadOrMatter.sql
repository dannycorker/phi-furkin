SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By:  Alex Elger
-- Purpose: Select records from detail field pages
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared pages
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldPages__GetAllByLeadOrMatter]
(
	@LeadTypeID int,
	@LeadOrMatter int,
	@ClientID int  
)
AS


				SET ANSI_NULLS OFF
				
				SELECT  dfp.*
				FROM
					dbo.fnDetailFieldPagesShared(@ClientID) dfp
				WHERE
					[LeadTypeID] = @LeadTypeID					
					AND LeadOrMatter = @LeadOrMatter
					AND ClientID = @ClientID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			


GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages__GetAllByLeadOrMatter] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldPages__GetAllByLeadOrMatter] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages__GetAllByLeadOrMatter] TO [sp_executeall]
GO
