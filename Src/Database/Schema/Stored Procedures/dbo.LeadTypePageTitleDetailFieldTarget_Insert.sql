SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the LeadTypePageTitleDetailFieldTarget table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypePageTitleDetailFieldTarget_Insert]
(

	@LeadTypePageTitleDetailFieldTargetID int    OUTPUT,

	@ClientID int   ,

	@LeadTypeID int   ,

	@Target varchar (250)  ,

	@DetailFieldID int   ,

	@TemplateTypeID int   ,

	@DetailFieldAlias varchar (500)  ,

	@Notes varchar (250)  
)
AS


				
				INSERT INTO [dbo].[LeadTypePageTitleDetailFieldTarget]
					(
					[ClientID]
					,[LeadTypeID]
					,[Target]
					,[DetailFieldID]
					,[TemplateTypeID]
					,[DetailFieldAlias]
					,[Notes]
					)
				VALUES
					(
					@ClientID
					,@LeadTypeID
					,@Target
					,@DetailFieldID
					,@TemplateTypeID
					,@DetailFieldAlias
					,@Notes
					)
				-- Get the identity value
				SET @LeadTypePageTitleDetailFieldTargetID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleDetailFieldTarget_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypePageTitleDetailFieldTarget_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypePageTitleDetailFieldTarget_Insert] TO [sp_executeall]
GO
