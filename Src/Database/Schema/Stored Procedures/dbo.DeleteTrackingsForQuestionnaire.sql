SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/****** Object:  Stored Procedure dbo.DeleteTrackingsForQuestionnaire    Script Date: 08/09/2006 12:22:41 ******/

CREATE PROCEDURE [dbo].[DeleteTrackingsForQuestionnaire] @ClientQuestionnaireID int
AS
Delete From Tracking
Where ClientQuestionnaireID = @ClientQuestionnaireID




GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteTrackingsForQuestionnaire] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DeleteTrackingsForQuestionnaire] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DeleteTrackingsForQuestionnaire] TO [sp_executeall]
GO
