SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets records from the EventType table passing page index and page count parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventType_GetPaged]
(

	@WhereClause varchar (2000)  ,

	@OrderBy varchar (2000)  ,

	@PageIndex int   ,

	@PageSize int   
)
AS


				
				BEGIN
				DECLARE @PageLowerBound int
				DECLARE @PageUpperBound int
				
				-- Set the page bounds
				SET @PageLowerBound = @PageSize * @PageIndex
				SET @PageUpperBound = @PageLowerBound + @PageSize

				-- Create a temp table to store the select results
				CREATE TABLE #PageIndex
				(
				    [IndexId] int IDENTITY (1, 1) NOT NULL,
				    [EventTypeID] int, [ClientID] int 
				)
				
				-- Insert into the temp table
				DECLARE @SQL AS nvarchar(4000)
				SET @SQL = 'INSERT INTO #PageIndex ([EventTypeID], [ClientID])'
				SET @SQL = @SQL + ' SELECT'
				IF @PageSize > 0
				BEGIN
					SET @SQL = @SQL + ' TOP ' + CONVERT(nvarchar, @PageUpperBound)
				END
				SET @SQL = @SQL + ' [EventTypeID], [ClientID]'
				SET @SQL = @SQL + ' FROM [dbo].[EventType] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				IF LEN(@OrderBy) > 0
				BEGIN
					SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
				END
				
				-- Populate the temp table
				EXEC sp_executesql @SQL

				-- Return paged results
				SELECT O.[EventTypeID], O.[ClientID], O.[EventTypeName], O.[EventTypeDescription], O.[Enabled], O.[UnitsOfEffort], O.[FollowupTimeUnitsID], O.[FollowupQuantity], O.[AvailableManually], O.[StatusAfterEvent], O.[AquariumEventAfterEvent], O.[EventSubtypeID], O.[DocumentTypeID], O.[LeadTypeID], O.[AllowCustomTimeUnits], O.[InProcess], O.[KeyEvent], O.[UseEventCosts], O.[UseEventUOEs], O.[UseEventDisbursements], O.[UseEventComments], O.[SignatureRequired], O.[SignatureOverride], O.[VisioX], O.[VisioY], O.[AquariumEventSubtypeID], O.[WhoCreated], O.[WhenCreated], O.[WhoModified], O.[WhenModified], O.[FollowupWorkingDaysOnly], O.[CalculateTableRows], O.[SourceID], O.[SmsGatewayID], O.[IsShared], O.[SocialFeedID]
				FROM
				    [dbo].[EventType] o WITH (NOLOCK),
				    #PageIndex PageIndex
				WHERE
				    PageIndex.IndexId > @PageLowerBound
					AND O.[EventTypeID] = PageIndex.[EventTypeID]
					AND O.[ClientID] = PageIndex.[ClientID]
				ORDER BY
				    PageIndex.IndexId
				
				-- get row count
				SET @SQL = 'SELECT COUNT(*) AS TotalRowCount'
				SET @SQL = @SQL + ' FROM [dbo].[EventType] WITH (NOLOCK)'
				IF LEN(@WhereClause) > 0
				BEGIN
					SET @SQL = @SQL + ' WHERE ' + @WhereClause
				END
				EXEC sp_executesql @SQL
			
				END
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_GetPaged] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventType_GetPaged] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventType_GetPaged] TO [sp_executeall]
GO
