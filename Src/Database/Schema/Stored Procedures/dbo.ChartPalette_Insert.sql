SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ChartPalette table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChartPalette_Insert]
(

	@ChartPaletteID int   ,

	@ChartPaletteName varchar (50)  ,

	@ChartPalettePreviewImageUrl varchar (255)  
)
AS


				
				INSERT INTO [dbo].[ChartPalette]
					(
					[ChartPaletteID]
					,[ChartPaletteName]
					,[ChartPalettePreviewImageUrl]
					)
				VALUES
					(
					@ChartPaletteID
					,@ChartPaletteName
					,@ChartPalettePreviewImageUrl
					)
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChartPalette_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChartPalette_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChartPalette_Insert] TO [sp_executeall]
GO
