SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-09-26
-- Description:	Gets the policies for the custom matter list
-- Mods: 
-- 2015-02-25 DCM changed source of premium
-- 2017-06-16 CPS display Product Name
-- 2017-07-18 CPS take premium from Purchased Product
-- 2017-07-19 CPS append status to policy type
-- 2018-12-03 JEL Changed join for purchasedproduct to pick up from the matter, not the dates 
-- 2019-01-04 AHOD Change the join for ppid to Left from Inner as it was preventing the matter list from displaying for OMF policies
-- =============================================
CREATE PROCEDURE [dbo].[_C00_1273_Policy_GetMatterList] 
(
	@PolicyLeadID INT
)

AS
BEGIN
	SET NOCOUNT ON;
	
		SELECT	c.CaseID, m.MatterID, llType.ItemValue + ISNULL(' (' + liStatus.ItemValue + ')','') AS PolicyType, /*llPolicy.ItemValue*/ rdvProduct.DetailValue AS ActivePolicy, /*2018/03/28 - change from product to ActivePolicy*/ 
		dbo.fn_C600_GetCurrentWellnessProductName(m.MatterID) AS Wellness /*GPR 2020-09-19*/,
			mdvInception.ValueDate AS Inception, mdvStart.ValueDate AS PolicyStart, 
			mdvEnd.ValueDate AS PolicyEnd, pp.ProductCostGross AS Premium
	FROM dbo.Cases c WITH (NOLOCK) 
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON c.CaseID = m.CaseID
	LEFT JOIN dbo.MatterDetailValues ppid WITH (NOLOCK) on ppid.MatterID = m.MatterID and ppid.DetailFieldID = 177074 /*2019-01-04 AHOD Updated to Left Join from Inner Join*/
	LEFT JOIN dbo.MatterDetailValues mdvPolicy WITH (NOLOCK) ON m.MatterID = mdvPolicy.MatterID AND mdvPolicy.DetailFieldID = 170034
	LEFT JOIN dbo.ResourceListDetailValues rdvType WITH (NOLOCK) ON mdvPolicy.ValueInt = rdvType.ResourceListID AND rdvType.DetailFieldID = 144319
	LEFT JOIN dbo.LookupListItems llType WITH (NOLOCK) ON rdvType.ValueInt = llType.LookupListItemID
	LEFT JOIN ResourceListDetailValues rdvProduct WITH ( NOLOCK ) on rdvProduct.ResourceListID = mdvPolicy.ValueInt AND rdvProduct.DetailFieldID = 146200 /*Product*/
	--LEFT JOIN dbo.ResourceListDetailValues rdvPolicy WITH (NOLOCK) ON mdvPolicy.ValueInt = rdvPolicy.ResourceListID AND rdvPolicy.DetailFieldID = 144318 /*Scheme Code*/
	--LEFT JOIN dbo.LookupListItems llPolicy WITH (NOLOCK) ON rdvPolicy.ValueInt = llPolicy.LookupListItemID
	LEFT JOIN dbo.MatterDetailValues mdvInception WITH (NOLOCK) ON m.MatterID = mdvInception.MatterID AND mdvInception.DetailFieldID = 170035
	LEFT JOIN dbo.MatterDetailValues mdvStart WITH (NOLOCK) ON m.MatterID = mdvStart.MatterID AND mdvStart.DetailFieldID = 170036
	LEFT JOIN dbo.MatterDetailValues mdvEnd WITH (NOLOCK) ON m.MatterID = mdvEnd.MatterID AND mdvEnd.DetailFieldID = 170037
	LEFT JOIN PurchasedProduct pp WITH ( NOLOCK ) on pp.PurchasedProductID = ppid.ValueInt --dbo.fn_C600_GetPurchasedProductForMatter(m.MatterID,dbo.fn_GetDate_Local())
	--LEFT JOIN dbo.MatterDetailValues p WITH (NOLOCK) ON m.MatterID = p.MatterID AND p.DetailFieldID = 177898 /*Current/New Premium - After Discount*/
	--LEFT JOIN dbo.MatterDetailValues p WITH (NOLOCK) ON m.MatterID = p.MatterID AND p.DetailFieldID = 175337 /*Current/New Premium - Annual*/
	--LEFT JOIN Premiums p ON m.MatterID = p.MatterID AND p.rn = 1 
	--LEFT JOIN dbo.MatterDetailValues mdvQuote WITH (NOLOCK) ON m.MatterID = mdvQuote.MatterID AND mdvQuote.DetailFieldID = 171338
	LEFT JOIN MatterDetailValues mdvStatus WITH ( NOLOCK ) on mdvStatus.MatterID = m.MatterID AND mdvStatus.DetailFieldID = 170038 /*Policy Status*/
	LEFT JOIN LookupListItems liStatus WITH ( NOLOCK ) on liStatus.LookupListItemID = mdvStatus.ValueInt
	WHERE c.LeadID = @PolicyLeadID
	--AND (mdvQuote.ValueInt IS NULL OR mdvQuote.ValueInt = 0)
	ORDER BY m.MatterID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_GetMatterList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_1273_Policy_GetMatterList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_1273_Policy_GetMatterList] TO [sp_executeall]
GO
