SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ChangeStep table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeStep_Delete]
(

	@ChangeStepID int   
)
AS


				DELETE FROM [dbo].[ChangeStep] WITH (ROWLOCK) 
				WHERE
					[ChangeStepID] = @ChangeStepID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStep_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeStep_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeStep_Delete] TO [sp_executeall]
GO
