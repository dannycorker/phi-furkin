SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the QuillCustomerMapping table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuillCustomerMapping_GetByCustomerID]
(

	@CustomerID int   
)
AS


				SELECT
					[QuillCustomerMappingID],
					[ClientID],
					[CustomerID],
					[QuillClientCode]
				FROM
					[dbo].[QuillCustomerMapping] WITH (NOLOCK) 
				WHERE
										[CustomerID] = @CustomerID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCustomerMapping_GetByCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuillCustomerMapping_GetByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuillCustomerMapping_GetByCustomerID] TO [sp_executeall]
GO
