SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DiaryAppointment table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DiaryAppointment__GetByClientPersonnelIDAndDateRange]
(
	@ClientPersonnelID	int,
	@StartDate			datetime,
	@EndDate			datetime
)
AS

	SET ANSI_NULLS OFF

	SELECT
		apt.[DiaryAppointmentID],
		apt.[ClientID],
		apt.[DiaryAppointmentTitle],
		apt.[DiaryAppointmentText],
		apt.[CreatedBy],
		apt.[DueDate],
		apt.[EndDate],
		apt.[AllDayEvent],
		apt.[Completed],
		apt.[LeadID],
		apt.[CaseID],
		apt.[CustomerID],
		apt.[Version],
		apt.[ExportVersion],
		apt.[RecurrenceInfo],
		apt.[DiaryAppointmentEventType],
		apt.[ResourceInfo],
		CASE apt.[CreatedBy]
			WHEN @ClientPersonnelID THEN apt.[TempReminderTimeshiftID] 
			ELSE rem.[ReminderTimeshiftID]
		END as [TempReminderTimeshiftID],
		apt.[StatusID],
		apt.[LabelID]
	FROM
		[dbo].[DiaryAppointment] apt
	LEFT JOIN 
		[dbo].[DiaryReminder] rem ON rem.[DiaryAppointmentID] = apt.[DiaryAppointmentID] AND rem.[ClientPersonnelID] <> apt.[CreatedBy]
	WHERE
		([CreatedBy] = @ClientPersonnelID OR rem.[ClientPersonnelID] = @ClientPersonnelID)
		AND [DueDate] BETWEEN @StartDate AND @EndDAte
				
	Select @@ROWCOUNT
	SET ANSI_NULLS ON









GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment__GetByClientPersonnelIDAndDateRange] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DiaryAppointment__GetByClientPersonnelIDAndDateRange] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DiaryAppointment__GetByClientPersonnelIDAndDateRange] TO [sp_executeall]
GO
