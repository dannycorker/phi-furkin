SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Robin Hall
-- Create date: 2014-12-11
-- Description:	Automated event exception report
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE PROCEDURE [dbo].[_C600_MI_AutomatedEventExceptions] 
(
	@CheckFromDate DATE = NULL
)
AS
BEGIN

	DECLARE @ClientID		INT = dbo.fnGetPrimaryClientID()
	DECLARE @iCheckFromDate DATE
	SELECT @iCheckFromDate = ISNULL(@CheckFromDate, DATEADD(DAY, -1, dbo.fn_GetDate_Local()))

	SELECT 
		Cu.Fullname Customer, CASE Cu.Test WHEN 1 THEN 'TEST' ELSE '' END Test
		, le.LeadID, le.CaseID, m.MatterID, le.LeadEventID, le.WhenCreated EventCreated, cp.UserName UserName
		, et1.EventTypeID FromEventTypeID, et1.EventTypeName FromEventTypeName
		, et2.EventTypeID ToEventTypeID, et2.EventTypeName ToEventTypeName
		, aeq.WhenCreated EventQueueRequested, aeq.ErrorDateTime
		, ISNULL(aeq.ErrorMessage,'No AutomatedEventQueue record created') ErrorMessage
	FROM 
		LeadEvent le WITH (NOLOCK)
		INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.CaseID = le.CaseID
		INNER JOIN dbo.Customers cu WITH (NOLOCK) ON cu.CustomerID = m.CustomerID
		INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = le.WhoCreated
		INNER JOIN dbo.EventType et1 WITH (NOLOCK) ON et1.EventTypeID = le.EventTypeID
		LEFT JOIN dbo.EventTypeAutomatedEvent etat WITH (NOLOCK) ON etat.EventTypeID = le.EventTypeID
		LEFT JOIN AutomatedEventQueue aeq WITH (NOLOCK) ON aeq.FromLeadEventID = le.LeadEventID
		LEFT JOIN dbo.EventType et2 WITH (NOLOCK) ON et2.EventTypeID = ISNULL(etat.AutomatedEventTypeID, aeq.AutomatedEventTypeID)
	WHERE 
		le.ClientID = @ClientID
		AND le.EventDeleted = 0
		AND (	
				(etat.AutomatedEventTypeID IS NOT NULL AND aeq.AutomatedEventQueueID IS NULL) -- AutomatedEventType failed to raise AutomatedEventQueue
			OR
				(etat.AutomatedEventTypeID = aeq.AutomatedEventTypeID and aeq.ErrorDateTime IS NOT NULL) -- Automated EventType raised AutomatedEventQueue but event failed
			OR	
				(etat.AutomatedEventTypeID IS NULL AND aeq.ErrorDateTime IS NOT NULL) -- AutomatedEventQueue raised without AutomatedEventType (eg SAE) but event failed
			)
		AND le.WhenCreated >= @iCheckFromDate
	ORDER BY 
		le.WhenCreated

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_MI_AutomatedEventExceptions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_MI_AutomatedEventExceptions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_MI_AutomatedEventExceptions] TO [sp_executeall]
GO
