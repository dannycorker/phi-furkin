SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the EsignatureApiKey table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EsignatureApiKey_Delete]
(

	@EsignatureApiKeyID int   
)
AS


				DELETE FROM [dbo].[EsignatureApiKey] WITH (ROWLOCK) 
				WHERE
					[EsignatureApiKeyID] = @EsignatureApiKeyID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureApiKey_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EsignatureApiKey_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EsignatureApiKey_Delete] TO [sp_executeall]
GO
