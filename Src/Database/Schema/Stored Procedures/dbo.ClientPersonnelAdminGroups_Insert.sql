SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the ClientPersonnelAdminGroups table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPersonnelAdminGroups_Insert]
(

	@ClientPersonnelAdminGroupID int    OUTPUT,

	@ClientID int   ,

	@GroupName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[ClientPersonnelAdminGroups]
					(
					[ClientID]
					,[GroupName]
					)
				VALUES
					(
					@ClientID
					,@GroupName
					)
				-- Get the identity value
				SET @ClientPersonnelAdminGroupID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAdminGroups_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPersonnelAdminGroups_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPersonnelAdminGroups_Insert] TO [sp_executeall]
GO
