SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Paul Richardson
-- Create date: 28-09-2011
-- Description:	Gets enable detail fields in order by page
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetFields]
	@DetailFieldPageID int
AS
BEGIN
	
	SET NOCOUNT ON;
    
	SELECT DetailFieldID, LeadOrMatter, QuestionTypeID, FieldName, ResourceListDetailFieldPageID, TableDetailFieldPageID 
	FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldPageID = @DetailFieldPageID and Enabled=1 ORDER BY FieldOrder asc
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetFields] TO [sp_executeall]
GO
