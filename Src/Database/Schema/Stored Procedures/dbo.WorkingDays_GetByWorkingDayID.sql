SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the WorkingDays table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WorkingDays_GetByWorkingDayID]
(

	@WorkingDayID int   
)
AS


				SELECT
					[WorkingDayID],
					[Year],
					[Month],
					[Day],
					[DayNumber],
					[IsWorkDay],
					[IsWeekDay],
					[IsBankHoliday],
					[Date],
					[WeekNumber],
					[DayNumberOfWeek],
					[CharDate]
				FROM
					[dbo].[WorkingDays] WITH (NOLOCK) 
				WHERE
										[WorkingDayID] = @WorkingDayID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WorkingDays_GetByWorkingDayID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WorkingDays_GetByWorkingDayID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WorkingDays_GetByWorkingDayID] TO [sp_executeall]
GO
