SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientAreaMailingLists table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientAreaMailingLists_Delete]
(

	@ClientAreaMailingListID int   
)
AS


				DELETE FROM [dbo].[ClientAreaMailingLists] WITH (ROWLOCK) 
				WHERE
					[ClientAreaMailingListID] = @ClientAreaMailingListID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientAreaMailingLists_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientAreaMailingLists_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientAreaMailingLists_Delete] TO [sp_executeall]
GO
