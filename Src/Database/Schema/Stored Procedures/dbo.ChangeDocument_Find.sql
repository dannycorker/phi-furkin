SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ChangeDocument table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ChangeDocument_Find]
(

	@SearchUsingOR bit   = null ,

	@ChangeDocumentID int   = null ,

	@DocumentData varbinary (MAX)  = null ,

	@DocumentExtension varchar (10)  = null ,

	@WhenCreated datetime   = null ,

	@WhoCreated int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ChangeDocumentID]
	, [DocumentData]
	, [DocumentExtension]
	, [WhenCreated]
	, [WhoCreated]
    FROM
	[dbo].[ChangeDocument] WITH (NOLOCK) 
    WHERE 
	 ([ChangeDocumentID] = @ChangeDocumentID OR @ChangeDocumentID IS NULL)
	AND ([DocumentExtension] = @DocumentExtension OR @DocumentExtension IS NULL)
	AND ([WhenCreated] = @WhenCreated OR @WhenCreated IS NULL)
	AND ([WhoCreated] = @WhoCreated OR @WhoCreated IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ChangeDocumentID]
	, [DocumentData]
	, [DocumentExtension]
	, [WhenCreated]
	, [WhoCreated]
    FROM
	[dbo].[ChangeDocument] WITH (NOLOCK) 
    WHERE 
	 ([ChangeDocumentID] = @ChangeDocumentID AND @ChangeDocumentID is not null)
	OR ([DocumentExtension] = @DocumentExtension AND @DocumentExtension is not null)
	OR ([WhenCreated] = @WhenCreated AND @WhenCreated is not null)
	OR ([WhoCreated] = @WhoCreated AND @WhoCreated is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDocument_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ChangeDocument_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ChangeDocument_Find] TO [sp_executeall]
GO
