SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the CaseTransferStatus table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[CaseTransferStatus_GetByCaseTransferStatusID]
(

	@CaseTransferStatusID int   
)
AS


				SELECT
					[CaseTransferStatusID],
					[Status]
				FROM
					[dbo].[CaseTransferStatus] WITH (NOLOCK) 
				WHERE
										[CaseTransferStatusID] = @CaseTransferStatusID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferStatus_GetByCaseTransferStatusID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CaseTransferStatus_GetByCaseTransferStatusID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CaseTransferStatus_GetByCaseTransferStatusID] TO [sp_executeall]
GO
