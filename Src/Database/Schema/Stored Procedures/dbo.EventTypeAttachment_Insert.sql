SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the EventTypeAttachment table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAttachment_Insert]
(

	@AttachmentID int    OUTPUT,

	@EventTypeID int   ,

	@AttachmentEventTypeID int   ,

	@All bit   ,

	@AttachmentEventGroupID int   
)
AS


				
				INSERT INTO [dbo].[EventTypeAttachment]
					(
					[EventTypeID]
					,[AttachmentEventTypeID]
					,[All]
					,[AttachmentEventGroupID]
					)
				VALUES
					(
					@EventTypeID
					,@AttachmentEventTypeID
					,@All
					,@AttachmentEventGroupID
					)
				-- Get the identity value
				SET @AttachmentID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAttachment_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAttachment_Insert] TO [sp_executeall]
GO
