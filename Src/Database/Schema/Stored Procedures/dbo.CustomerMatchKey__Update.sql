SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Paul Richardson
-- Create date: 20-12-2012
-- Description:	Updates a record in CustomerMatchKey
-- =============================================
CREATE PROCEDURE [dbo].[CustomerMatchKey__Update]
	
	@ClientID INT,
	@CustomerID INT,
	@Firstname varchar(250),
	@Lastname varchar(250),
	@Address1 varchar(250),
	@Postcode varchar(250),
	@DateOfBirth varchar(250)

AS
BEGIN

	IF @DateOfBirth = ''
	BEGIN
	
		SET @DateOfBirth = NULL
	
	END

	UPDATE CustomerMatchKey
	SET ClientID = @ClientID, Firstname = @Firstname, Lastname = @Lastname, Address1 = @Address1, Postcode = @Postcode, DateOfBirth = @DateOfBirth
	WHERE CustomerID = @CustomerID
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerMatchKey__Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerMatchKey__Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerMatchKey__Update] TO [sp_executeall]
GO
