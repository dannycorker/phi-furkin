SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-08-17
-- Description:	Helpful details about an RuleSet
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_RuleSet__Describe] 
	@RuleSetID INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @Rules TABLE (RuleID INT, Name VARCHAR(2000))

	SELECT rs.* 
	FROM RulesEngine_RuleSets rs WITH ( NOLOCK )
	WHERE rs.RuleSetID = @RuleSetID

	SELECT	 'Decode Values' [Decode Values]
			,dtIn.Name [InputType]
			,dtOut.Name [OutputType]
			,cp.UserName [WhoCreated]
			,cpMod.UserName [WhoModified]
			,st.RuleSetType
	FROM RulesEngine_RuleSets rs 	
	LEFT JOIN RulesEngine_DataTypes dtIn WITH ( NOLOCK ) on dtIn.TypeID = rs.InputTypeID
	LEFT JOIN RulesEngine_DataTypes dtOut WITH ( NOLOCK ) on dtOut.TypeID = rs.OutputTypeID
	LEFT JOIN ClientPersonnel cp WITH ( NOLOCK ) on cp.ClientPersonnelID = rs.WhoCreated
	LEFT JOIN ClientPersonnel cpMod WITH ( NOLOCK ) on cpMod.ClientPersonnelID = rs.WhoModified
	LEFT JOIN RulesEngine_RuleSetTypeID st WITH ( NOLOCK ) on st.RulesEngine_RuleSetTypeID = rs.RuleSetTypeID
	WHERE rs.RuleSetID = @RuleSetID


	INSERT @Rules ( RuleID, Name )
	SELECT r.RuleID, r.Name
	FROM RulesEngine_Rules r WITH ( NOLOCK )
	WHERE r.RuleSetID = @RuleSetID

	SELECT	 'Rules' [Rules]
			,r.* 
			,cp.UserName [WhoCreated]
	FROM RulesEngine_Rules r WITH ( NOLOCK ) 
	INNER JOIN @Rules r2 on r.RuleID = r2.RuleID
	LEFT JOIN ClientPersonnel cp WITH ( NOLOCK ) on cp.ClientPersonnelID = r.WhoCreated
	ORDER BY r.RuleOrder

	SELECT 'Parameters' [Parameters], r.RuleID, r.Name
		,rp.RuleParameterID
		,rp.Name
		,rp.ParameterTypeID
		,pt.Name [ParameterTypeName]
		,rp.Value
		,rp.DataTypeID
		,dt.Name [DataTypeName]
		,rp.WhenCreated
		,cp.UserName + ' (' + CONVERT(VARCHAR,cp.ClientPersonnelID) + ')' [WhoCreated UserName]
		,rp.WhenModified
		,cpMod.UserName + ' (' + CONVERT(VARCHAR,cpMod.ClientPersonnelID) + ')' [WhoModified UserName]
	FROM @Rules r 
	INNER JOIN RulesEngine_RuleParameters rp WITH ( NOLOCK ) on rp.RuleID = r.RuleID
	 LEFT JOIN RulesEngine_ParameterTypes pt WITH ( NOLOCK ) on pt.ParameterTypeID = rp.ParameterTypeID
	 LEFT JOIN RulesEngine_DataTypes dt WITH ( NOLOCK ) on dt.TypeID = rp.DataTypeID
	 LEFT JOIN ClientPersonnel cp WITH ( NOLOCK ) on cp.ClientPersonnelID = rp.WhoCreated
	 LEFT JOIN ClientPersonnel cpMod WITH ( NOLOCK ) on cpMod.ClientPersonnelID = rp.WhoModified
	ORDER BY r.RuleID
	
END









GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleSet__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_RuleSet__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_RuleSet__Describe] TO [sp_executeall]
GO
