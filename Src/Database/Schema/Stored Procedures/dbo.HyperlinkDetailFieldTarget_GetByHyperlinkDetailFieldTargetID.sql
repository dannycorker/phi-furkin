SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the HyperlinkDetailFieldTarget table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkDetailFieldTarget_GetByHyperlinkDetailFieldTargetID]
(

	@HyperlinkDetailFieldTargetID int   
)
AS


				SELECT
					[HyperlinkDetailFieldTargetID],
					[ClientID],
					[HyperlinkDetailFieldID],
					[Target],
					[DetailFieldID],
					[TemplateTypeID],
					[DetailFieldAlias],
					[Notes]
				FROM
					[dbo].[HyperlinkDetailFieldTarget] WITH (NOLOCK) 
				WHERE
										[HyperlinkDetailFieldTargetID] = @HyperlinkDetailFieldTargetID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkDetailFieldTarget_GetByHyperlinkDetailFieldTargetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkDetailFieldTarget_GetByHyperlinkDetailFieldTargetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkDetailFieldTarget_GetByHyperlinkDetailFieldTargetID] TO [sp_executeall]
GO
