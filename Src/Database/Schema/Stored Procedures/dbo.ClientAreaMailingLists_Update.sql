SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Updates a record in the ClientAreaMailingLists table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientAreaMailingLists_Update]
(

	@ClientAreaMailingListID int   ,

	@Name varchar (200)  ,

	@Email varchar (255)  ,

	@PostCode varchar (10)  ,

	@OutcomeID int   ,

	@YellowPagesAreaCode varchar (10)  ,

	@ClientID int   ,

	@ClientQuestionnaireID int   ,

	@OnHold bit   
)
AS


				
				
				
				-- Modify the updatable columns
				UPDATE
					[dbo].[ClientAreaMailingLists]
				SET
					[Name] = @Name
					,[Email] = @Email
					,[PostCode] = @PostCode
					,[OutcomeID] = @OutcomeID
					,[YellowPagesAreaCode] = @YellowPagesAreaCode
					,[ClientID] = @ClientID
					,[ClientQuestionnaireID] = @ClientQuestionnaireID
					,[OnHold] = @OnHold
				WHERE
[ClientAreaMailingListID] = @ClientAreaMailingListID 
				
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientAreaMailingLists_Update] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientAreaMailingLists_Update] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientAreaMailingLists_Update] TO [sp_executeall]
GO
