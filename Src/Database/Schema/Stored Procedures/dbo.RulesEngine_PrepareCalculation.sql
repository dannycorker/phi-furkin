SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2018-07-04
-- Description:	Prepare overrides for the C# based calculation engine
-- 2018-07-05 CPS					 | allow proc to return tvp contents only, for INSERT...EXEC calls to @Overrides
-- 2019-10-13 GPR for LPC-35		 | created to 603 
-- 2019-11-21 CPS for JIRA SDLPC-11  | Date of birth isn't expclicitly referenced in any rules because we use functions to calculate age instead.  Force the DoB to be included in the risk details.
-- 2020-11-19 CPS for JIRA SDPRU-155 | Include CalculationTypeID in [fn_C600_RulesEngine_GetStandardOverrides] so we can vary the start date if this is a renewal calculation
-- =============================================
CREATE PROCEDURE [dbo].[RulesEngine_PrepareCalculation] 
	 @LeadEventID			INT
	,@ReturnTvpContentsOnly BIT = 0
	,@XmlOutput				XML OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

	PRINT CHAR(13)+CHAR(10) + OBJECT_NAME(@@ProcID)
	
	DECLARE  @CustomerID				INT
			,@LeadID					INT
			,@CaseID					INT
			,@MatterID					INT
			,@WhoCreated				INT
			,@EventComments				VARCHAR(2000) = ''
			,@Overrides					dbo.tvpIntvarcharVarchar
			,@OverridesWithFunctions	dbo.tvpIntvarcharVarchar
			,@XmlResponse				XML
			,@CalculationTypeID			INT
			,@RuleSetID					INT
			,@ClientID					INT
			,@LeadTypeID				INT
			,@BrandID					INT 

	/*Find the mapped Calculation Type*/
	SELECT	 @CalculationTypeID = se.ThirdPartySystemKey
	FROM dbo.LeadEvent le WITH ( NOLOCK )
	INNER JOIN dbo.ThirdPartySystemEvent se WITH ( NOLOCK ) ON se.EventTypeID = le.EventTypeID AND se.MessageName = 'CalculationType'
	WHERE le.LeadEventID = @LeadEventID

	SELECT TOP (1)   @CustomerID	= m.CustomerID
					,@LeadID		= m.LeadID
					,@CaseID		= m.CaseID
					,@MatterID		= m.MatterID
					,@WhoCreated	= m.WhoCreated
					,@LeadTypeID	= l.LeadTypeID
					,@ClientID		= m.ClientID
	FROM dbo.LeadEvent le WITH ( NOLOCK ) 
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.CaseID = le.CaseID
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = m.LeadID
	WHERE le.LeadEventID = @LeadEventID
	ORDER BY m.MatterID

	IF @CalculationTypeID IN ( 3,9 ) -- MTA, Pre MTA.  Swap customer and pet details in advance of calculation
	BEGIN
		EXEC dbo._C600_TempSwapPHandPetDetails @CustomerID

		EXEC [dbo].[_C00_LogIt] 'Info', 'RulesEngine_PrepareCalculation', 'LeadID', @LeadID, 58552

		EXEC dbo._C600_TempSwapPolicyDetails @LeadID /*GPR 2020-09-16 for PPET-527*/
	END

	IF @CalculationTypeID = 11 -- TestFileItem.  Take details straight from the TestFileItemOverrides table
	BEGIN
		DECLARE @TestFileItemID INT =  dbo.fnGetSimpleDvAsInt(dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, @LeadTypeID, 108, 4594) , @LeadID ) -- RulesEngine TestFileItemID

		/*CPS 2018-07-16 Use the mapped third party field to find a test file item, pick up its overrides, and decode any lookuplists*/
		INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
		SELECT rp.ParameterTypeID, rp.Value, ISNULL( CONVERT(VARCHAR,li.LookupListItemID),fio.ParameterValue)
		FROM dbo.RulesEngine_TestFileItemOverride fio WITH ( NOLOCK ) 
		INNER JOIN dbo.RulesEngine_RuleParameters rp WITH ( NOLOCK ) ON rp.RuleParameterID = fio.RuleParameterID
		LEFT JOIN dbo.DetailFields df WITH ( NOLOCK ) ON CONVERT(VARCHAR,df.DetailFieldID) = rp.[Value]
		LEFT JOIN dbo.LookupListItems li WITH ( NOLOCK ) ON li.LookupListID = df.LookupListID AND li.ItemValue = fio.ParameterValue
		WHERE fio.TestFileItemID = @TestFileItemID 
		AND fio.ParameterValue <> ''

		SELECT @RuleSetID = ISNULL(tfi.EvaluatedRuleSetID,3498)
		FROM RulesEngine_TestFileItem tfi WITH ( NOLOCK ) 
		WHERE tfi.TestFileItemID = @TestFileItemID
	END
	ELSE 
	BEGIN
		/*Detail Fields (simple)*/
		INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
		SELECT	 rps.ParameterTypeID
				,rps.Value
				,dbo.fnGetSimpleDv(df.DetailFieldID
								, CASE df.LeadOrMatter 
									WHEN 10 THEN @CustomerID
									WHEN  1 THEN @LeadID
									WHEN 11 THEN @CaseID
									WHEN  2 THEN @MatterID
									WHEN 13 THEN @WhoCreated
									ELSE  0
								  END ) 
		FROM dbo.RulesEngine_RuleParameters_PreSet rps
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = rps.Value
		WHERE rps.ParameterTypeID = 1
		AND dbo.fnIsInt(rps.Value) = 1

		-- 2019-11-21 CPS for JIRA SDLPC-11 | Date of birth isn't expclicitly referenced in any rules because we use functions to calculate age instead.  Force the DoB to be included in the risk details.
		-- 2019-11-27						| Change to a LDV select in case the date is empty
		UNION SELECT 1, '144274', DetailValue
		FROM LeadDetailValues ldv WITH (NOLOCK) 
		WHERE ldv.LeadID = @LeadID
		AND ldv.DetailFieldID = 144274 /*Pet Date of Birth*/
		AND ldv.ValueDate is not NULL

		/*Detail Fields (resource lists)*/
		INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
		SELECT	 rps.ParameterTypeID
				,rps.Value
				,dbo.fnGetSimpleRLDv( df.DetailFieldID
									, rps.Value
									, CASE df.LeadOrMatter 
										WHEN 10 THEN @CustomerID
										WHEN  1 THEN @LeadID
										WHEN 11 THEN @CaseID
										WHEN  2 THEN @MatterID
										WHEN 13 THEN @WhoCreated
										ELSE  0
									  END
									  ,0 )
		FROM dbo.RulesEngine_RuleParameters_PreSet rps
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = rps.Parent
		WHERE rps.ParameterTypeID = 5
		AND dbo.fnIsInt(rps.Value) = 1
		AND dbo.fnIsInt(rps.Parent) = 1

		/*Standard Fields - custom SQL for each*/
		INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
		SELECT 2, fn.ParameterValue, fn.EvaluatedValue
		FROM dbo.fn_C600_RulesEngine_GetStandardOverrides(@CustomerID,@LeadID,@CaseID,@MatterID,@CalculationTypeID) fn

		IF @CalculationTypeID IN ( 3,9 ) -- MTA, Pre MTA.  Swap customer and pet details back now that we have all the overrides
		BEGIN
			EXEC dbo._C600_TempSwapPHandPetDetails @CustomerID
			EXEC dbo._C600_TempSwapPolicyDetails @LeadID /*GPR 2020-09-16 for PPET-527*/
		END
	END

	-- 2020-01-12 CPS for New Pricing Rates | Make sure we correctly look up the date for calculation
	INSERT @Overrides ( AnyID, AnyValue1, AnyValue2 )
	SELECT 1, 'EvaluateByChangeSetID', CONVERT(VARCHAR,rs.ChangeSetID)
	FROM MatterDetailValues mdv WITH (NOLOCK) 
	INNER JOIN RulesEngine_RuleSets rs WITH (NOLOCK) on rs.RuleSetID = mdv.ValueInt
	WHERE mdv.DetailFieldID = 178144 /*New Business RuleSetID*/
	AND mdv.MatterID = @MatterID
		UNION 
	SELECT 1, 'EvaluateByEffectiveDate', o.AnyValue2
	FROM @Overrides o
	WHERE o.AnyValue1 = 'YearStart'

	/*ALM/GPR FURKIN-195*/
	SELECT @BrandID = dbo.fnGetSimpleDvAsInt(170144, @CustomerID)
	SELECT @RuleSetID = dbo.fnGetInitialRuleSetID(@BrandID)
	SELECT @RuleSetID = dbo.fn_RulesEngine_GetRuleSetOverride(@RuleSetID,@Overrides) 
	
	/*Functions - called last because many functions use the @Overrides table*/
	EXEC dbo._C00_RulesEngine_GetAllFunctionsForOverrides @RuleSetID	= NULL
														, @CustomerID	= @CustomerID
														, @LeadID		= @LeadID
														, @CaseID		= @CaseID
														, @MatterID		= @MatterID
														, @Overrides	= @Overrides
														, @XmlResponse  = @XmlResponse OUTPUT

	INSERT @OverridesWithFunctions ( AnyID, AnyValue1, AnyValue2 )
	SELECT	 b.value('(@ParameterTypeID)[1]','INT') [ParameterTypeID]
			,b.value('(@AnyValue1)[1]','VARCHAR(2000)') [AnyValue1]
			,b.value('(@AnyValue2)[1]','VARCHAR(2000)') [AnyValue2]
	FROM @XmlResponse.nodes('*') a(b)
	WHERE b.value('(@AnyValue2)[1]','VARCHAR(2000)') <> ''

	/*Apply overrides if provided*/
	UPDATE owf
		SET AnyValue2 = ovr.AnyValue2
	FROM @OverridesWithFunctions owf
	INNER JOIN @Overrides ovr on ovr.AnyValue1 = owf.AnyValue1
	WHERE ovr.AnyValue2 <> ''

	/*GPR 2020-11-18 MultiPet Override*/
	IF @CalculationTypeID IN ( 3,9 )
	BEGIN
		DECLARE @Multipet INT

		SELECT @Multipet = dbo.fnGetSimpleDvAsInt(179923, @MatterID)
					
		IF @Multipet = 5145 -- No and do not add.
		BEGIN
				UPDATE owf
				SET AnyValue2 = '1'
				FROM @OverridesWithFunctions owf
				WHERE owf.AnyValue1 = 'fn_C00_1273_RulesEngine_PetCount'
		END
		ELSE -- Yes   ...and do not remove
		BEGIN
				UPDATE owf
				SET AnyValue2 = '2'
				FROM @OverridesWithFunctions owf
				WHERE owf.AnyValue1 = 'fn_C00_1273_RulesEngine_PetCount'
		END
	END

	/*Take remaining values from the original tvp*/
	INSERT @OverridesWithFunctions ( AnyID, AnyValue1, AnyValue2 )
	SELECT ovr.AnyID, ovr.AnyValue1, ovr.AnyValue2
	FROM @Overrides ovr
	WHERE not exists ( SELECT * 
	                   FROM @OverridesWithFunctions	owf
	                   WHERE owf.AnyValue1 = ovr.AnyValue1 )

	IF @ReturnTvpContentsOnly = 1
	BEGIN
		SELECT owf.AnyID, owf.AnyValue1, owf.AnyValue2 [AnyValue2]
		FROM @OverridesWithFunctions owf 
		WHERE owf.AnyValue2 <> ''
		ORDER BY owf.AnyID, owf.AnyValue1
	END
	ELSE
	BEGIN
		/*Return values to the caller, including an identifier that can be used to match the results back to the request*/
		SELECT @EventComments += CONVERT(VARCHAR(3),COUNT(owf.AnyID)) + ' override values prepared.' + CHAR(13)+CHAR(10)
		FROM @OverridesWithFunctions owf

		/*CPS 2018-07-04 spit all of the comments into the event history.  Useful while testing.  Take away when we go live*/
		SELECT @EventComments += ps.Name + ': ' + ISNULL(owf.AnyValue2,'NULL') + ISNULL(' (' + lli.ItemValue + ')','') + CHAR(13)+CHAR(10)
		FROM @OverridesWithFunctions owf 
		INNER JOIN dbo.RulesEngine_RuleParameters_PreSet ps WITH ( NOLOCK ) ON ps.Value = owf.AnyValue1
		LEFT JOIN dbo.LookupListItems lli WITH ( NOLOCK ) ON CONVERT(VARCHAR(20),lli.LookupListItemID) = owf.AnyValue2 AND ps.DataTypeID = 6 -- LookupList
		ORDER BY ps.Name

		--EXEC dbo._C00_SetLeadEventComments @LeadEventID = @LeadEventID, @Comments = @EventComments, @AddOrOverwrite = 0 -- overwrite
		PRINT @EventComments
	END

	/*Populate the output variable*/
	SELECT @XmlOutput = 
	(
	SELECT	 @LeadEventID		[Key]
			,'0.00'				[Input]
			,@RuleSetID			[RuleSetID]
			,
			(
			SELECT	 owf.AnyID [ParameterTypeID]
			,owf.AnyValue1	[ParameterValue]
			,owf.AnyValue2	[Value]
			FROM @OverridesWithFunctions owf
			FOR XML PATH ('CalculatedValue'), TYPE
			) [CalculatedValues]
	FOR XML PATH(''), TYPE
	)

END

GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_PrepareCalculation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_PrepareCalculation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_PrepareCalculation] TO [sp_executeall]
GO
