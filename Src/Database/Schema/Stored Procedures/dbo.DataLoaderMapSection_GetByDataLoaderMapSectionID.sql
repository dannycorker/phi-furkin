SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the DataLoaderMapSection table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderMapSection_GetByDataLoaderMapSectionID]
(

	@DataLoaderMapSectionID int   
)
AS


				SELECT
					[DataLoaderMapSectionID],
					[ClientID],
					[DataLoaderMapID],
					[DataLoaderObjectTypeID],
					[DataLoaderObjectActionID],
					[DataLoaderSectionLocaterTypeID],
					[DetailFieldSubTypeID],
					[HasAquariumID],
					[NumberOfFooterRowsToSkip],
					[ImportIntoLeadManager],
					[IsFixedLengthSection],
					[FixedLengthNumberOfRows],
					[IsMandatory],
					[IsMultipleAllowedWithinFile],
					[Notes],
					[TableDetailFieldID],
					[ResourceListDetailFieldID],
					[CreatePaymentSchedule]
				FROM
					[dbo].[DataLoaderMapSection] WITH (NOLOCK) 
				WHERE
										[DataLoaderMapSectionID] = @DataLoaderMapSectionID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection_GetByDataLoaderMapSectionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMapSection_GetByDataLoaderMapSectionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection_GetByDataLoaderMapSectionID] TO [sp_executeall]
GO
