SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-04-28
-- Description:	List all AquariumOptionType records
-- =============================================
CREATE PROCEDURE [dbo].[aot] 
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT aot.* 
	FROM dbo.AquariumOptionType aot WITH (NOLOCK)
	ORDER BY aot.AquariumOptionTypeID

END






GO
GRANT VIEW DEFINITION ON  [dbo].[aot] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[aot] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[aot] TO [sp_executeall]
GO
