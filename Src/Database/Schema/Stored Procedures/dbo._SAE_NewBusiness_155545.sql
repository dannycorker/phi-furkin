SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-03
-- Description:	Sql After Event - New terms receivedojn all matched Policies
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_NewBusiness_155545]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */

/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */			
			
	/* New Terms received on all matched policies */
	IF @EventTypeID IN (155545)
	BEGIN
	
		-- check that the user is correct & we are not still waiting for new terms
		IF EXISTS ( SELECT * FROM Lead l WITH (NOLOCK) 
					INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
					INNER JOIN LeadEvent le WITH (NOLOCK) ON ca.LatestInProcessLeadEventID=le.LeadEventID
					WHERE l.CustomerID=@CustomerID AND l.LeadTypeID=1492 
						AND le.EventTypeID<>155221 )
		BEGIN
			
			SELECT @ErrorMessage = '<br /><br /><font color="red">New terms have not yet been received for another of this customer''s pets.</font><br/><br /><font color="#edf4fa">'
			RAISERROR(@ErrorMessage,16,1)
			RETURN
		
		END	
		
		-- also this event can only be added to the first pet (lowest PolicyID)
		IF NOT EXISTS ( SELECT * FROM fn_C00_1272_Policy_GetPreMatchPetList(@CustomerID) p 
						WHERE p.MatterID=@MatterID
							AND	p.PetNumber=1 )				
		BEGIN
			
			SELECT @ErrorMessage = '<br /><br /><font color="red">This event can only be added to the first pet (lowest PolicyID).</font><br/><br /><font color="#edf4fa">'
			RAISERROR(@ErrorMessage,16,1)
			RETURN
		
		END	
		
	END	



	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_NewBusiness_155545] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_NewBusiness_155545] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_NewBusiness_155545] TO [sp_executeall]
GO
