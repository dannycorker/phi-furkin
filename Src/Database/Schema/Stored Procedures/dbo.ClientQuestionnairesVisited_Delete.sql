SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientQuestionnairesVisited table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientQuestionnairesVisited_Delete]
(

	@ClientQuestionnairesVisitedID int   
)
AS


				DELETE FROM [dbo].[ClientQuestionnairesVisited] WITH (ROWLOCK) 
				WHERE
					[ClientQuestionnairesVisitedID] = @ClientQuestionnairesVisitedID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisited_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientQuestionnairesVisited_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientQuestionnairesVisited_Delete] TO [sp_executeall]
GO
