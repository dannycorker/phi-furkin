SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- ================================================================
-- Author:      Jan Wilson
-- Create date: 2014-06-09
-- Description: Returns all rule parameters for a specific rule set
-- ================================================================
CREATE PROCEDURE [dbo].[RulesEngine_Parameters_GetParametersByRuleSetID]
(
      @ClientID INT,
      @RuleSetID INT
)
AS
BEGIN
      
	SET NOCOUNT ON;
	SELECT
		RP.RuleParameterID ID, r.Name + ' - ' + RP.Name Name
	FROM
		dbo.RulesEngine_Rules r WITH (NOLOCK) 
		
		INNER JOIN dbo.ClientPersonnel cpCreate WITH (NOLOCK)
		ON r.WhoCreated = cpCreate.ClientPersonnelID

		INNER JOIN dbo.ClientPersonnel cpMod WITH (NOLOCK)
		ON r.WhoModified = cpMod.ClientPersonnelID

		INNER JOIN dbo.RulesEngine_RuleParameters RP
		ON r.RuleID = RP.RuleID
				
	WHERE
		r.ClientID = @ClientID
		AND r.RuleSetID = @RuleSetID
	ORDER BY
		r.RuleOrder, RP.Name
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Parameters_GetParametersByRuleSetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_Parameters_GetParametersByRuleSetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_Parameters_GetParametersByRuleSetID] TO [sp_executeall]
GO
