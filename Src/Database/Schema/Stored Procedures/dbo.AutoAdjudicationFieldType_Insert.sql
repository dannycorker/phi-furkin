SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Inserts a record into the AutoAdjudicationFieldType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[AutoAdjudicationFieldType_Insert]
(

	@AutoAdjudicationFieldTypeID int    OUTPUT,

	@AutoAdjudicationFieldTypeName varchar (50)  
)
AS


				
				INSERT INTO [dbo].[AutoAdjudicationFieldType]
					(
					[AutoAdjudicationFieldTypeName]
					)
				VALUES
					(
					@AutoAdjudicationFieldTypeName
					)
				-- Get the identity value
				SET @AutoAdjudicationFieldTypeID = SCOPE_IDENTITY()
									
							
			

GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationFieldType_Insert] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[AutoAdjudicationFieldType_Insert] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[AutoAdjudicationFieldType_Insert] TO [sp_executeall]
GO
