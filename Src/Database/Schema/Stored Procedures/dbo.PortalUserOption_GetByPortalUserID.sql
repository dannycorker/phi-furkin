SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the PortalUserOption table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserOption_GetByPortalUserID]
(

	@PortalUserID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[PortalUserOptionID],
					[ClientID],
					[PortalUserID],
					[PortalUserOptionTypeID],
					[OptionValue]
				FROM
					[dbo].[PortalUserOption] WITH (NOLOCK) 
				WHERE
					[PortalUserID] = @PortalUserID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOption_GetByPortalUserID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserOption_GetByPortalUserID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserOption_GetByPortalUserID] TO [sp_executeall]
GO
