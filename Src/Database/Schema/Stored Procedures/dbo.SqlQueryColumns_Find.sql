SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SqlQueryColumns table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryColumns_Find]
(

	@SearchUsingOR bit   = null ,

	@SqlQueryColumnID int   = null ,

	@ClientID int   = null ,

	@SqlQueryID int   = null ,

	@SqlQueryTableID int   = null ,

	@CompleteOutputText varchar (500)  = null ,

	@ColumnAlias varchar (50)  = null ,

	@ShowColumn bit   = null ,

	@DisplayOrder int   = null ,

	@SortOrder int   = null ,

	@SortType varchar (10)  = null ,

	@TempColumnID int   = null ,

	@TempTableID int   = null ,

	@IsAggregate bit   = null ,

	@ColumnDataType varchar (50)  = null ,

	@ManipulatedDataType varchar (50)  = null ,

	@ColumnNaturalName varchar (50)  = null ,

	@SourceID int   = null ,

	@ComplexValue nvarchar (MAX)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SqlQueryColumnID]
	, [ClientID]
	, [SqlQueryID]
	, [SqlQueryTableID]
	, [CompleteOutputText]
	, [ColumnAlias]
	, [ShowColumn]
	, [DisplayOrder]
	, [SortOrder]
	, [SortType]
	, [TempColumnID]
	, [TempTableID]
	, [IsAggregate]
	, [ColumnDataType]
	, [ManipulatedDataType]
	, [ColumnNaturalName]
	, [SourceID]
	, [ComplexValue]
    FROM
	[dbo].[SqlQueryColumns] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryColumnID] = @SqlQueryColumnID OR @SqlQueryColumnID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([SqlQueryID] = @SqlQueryID OR @SqlQueryID IS NULL)
	AND ([SqlQueryTableID] = @SqlQueryTableID OR @SqlQueryTableID IS NULL)
	AND ([CompleteOutputText] = @CompleteOutputText OR @CompleteOutputText IS NULL)
	AND ([ColumnAlias] = @ColumnAlias OR @ColumnAlias IS NULL)
	AND ([ShowColumn] = @ShowColumn OR @ShowColumn IS NULL)
	AND ([DisplayOrder] = @DisplayOrder OR @DisplayOrder IS NULL)
	AND ([SortOrder] = @SortOrder OR @SortOrder IS NULL)
	AND ([SortType] = @SortType OR @SortType IS NULL)
	AND ([TempColumnID] = @TempColumnID OR @TempColumnID IS NULL)
	AND ([TempTableID] = @TempTableID OR @TempTableID IS NULL)
	AND ([IsAggregate] = @IsAggregate OR @IsAggregate IS NULL)
	AND ([ColumnDataType] = @ColumnDataType OR @ColumnDataType IS NULL)
	AND ([ManipulatedDataType] = @ManipulatedDataType OR @ManipulatedDataType IS NULL)
	AND ([ColumnNaturalName] = @ColumnNaturalName OR @ColumnNaturalName IS NULL)
	AND ([SourceID] = @SourceID OR @SourceID IS NULL)
	AND ([ComplexValue] = @ComplexValue OR @ComplexValue IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SqlQueryColumnID]
	, [ClientID]
	, [SqlQueryID]
	, [SqlQueryTableID]
	, [CompleteOutputText]
	, [ColumnAlias]
	, [ShowColumn]
	, [DisplayOrder]
	, [SortOrder]
	, [SortType]
	, [TempColumnID]
	, [TempTableID]
	, [IsAggregate]
	, [ColumnDataType]
	, [ManipulatedDataType]
	, [ColumnNaturalName]
	, [SourceID]
	, [ComplexValue]
    FROM
	[dbo].[SqlQueryColumns] WITH (NOLOCK) 
    WHERE 
	 ([SqlQueryColumnID] = @SqlQueryColumnID AND @SqlQueryColumnID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([SqlQueryID] = @SqlQueryID AND @SqlQueryID is not null)
	OR ([SqlQueryTableID] = @SqlQueryTableID AND @SqlQueryTableID is not null)
	OR ([CompleteOutputText] = @CompleteOutputText AND @CompleteOutputText is not null)
	OR ([ColumnAlias] = @ColumnAlias AND @ColumnAlias is not null)
	OR ([ShowColumn] = @ShowColumn AND @ShowColumn is not null)
	OR ([DisplayOrder] = @DisplayOrder AND @DisplayOrder is not null)
	OR ([SortOrder] = @SortOrder AND @SortOrder is not null)
	OR ([SortType] = @SortType AND @SortType is not null)
	OR ([TempColumnID] = @TempColumnID AND @TempColumnID is not null)
	OR ([TempTableID] = @TempTableID AND @TempTableID is not null)
	OR ([IsAggregate] = @IsAggregate AND @IsAggregate is not null)
	OR ([ColumnDataType] = @ColumnDataType AND @ColumnDataType is not null)
	OR ([ManipulatedDataType] = @ManipulatedDataType AND @ManipulatedDataType is not null)
	OR ([ColumnNaturalName] = @ColumnNaturalName AND @ColumnNaturalName is not null)
	OR ([SourceID] = @SourceID AND @SourceID is not null)
	OR ([ComplexValue] = @ComplexValue AND @ComplexValue is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryColumns_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryColumns_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryColumns_Find] TO [sp_executeall]
GO
