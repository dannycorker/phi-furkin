SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









-- =============================================
-- Author:		Jim Green
-- Create date: 2007-10-04
-- Description:	Copy all Group Rights to a new group
-- =============================================
CREATE PROCEDURE [dbo].[CopyGroupRightsToNewGroup]
	-- Add the parameters for the stored procedure here
	@FromGroupID int,
	@ToGroupID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRAN

	INSERT GroupFunctionControl (ClientPersonnelAdminGroupID, ModuleID, FunctionTypeID, HasDescendants, RightID, LeadTypeID)
	SELECT @ToGroupID, ModuleID, FunctionTypeID, HasDescendants, RightID, LeadTypeID
	FROM GroupFunctionControl
	WHERE ClientPersonnelAdminGroupID = @FromGroupID

	INSERT GroupRightsDynamic (ClientPersonnelAdminGroupID, FunctionTypeID, LeadTypeID, ObjectID, RightID)
	SELECT @ToGroupID, FunctionTypeID, LeadTypeID, ObjectID, RightID
	FROM GroupRightsDynamic
	WHERE ClientPersonnelAdminGroupID = @FromGroupID

	COMMIT

	
END









GO
GRANT VIEW DEFINITION ON  [dbo].[CopyGroupRightsToNewGroup] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CopyGroupRightsToNewGroup] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CopyGroupRightsToNewGroup] TO [sp_executeall]
GO
