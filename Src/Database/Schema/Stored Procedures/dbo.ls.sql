SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-03-02
-- Description:	Cet Client Status's from LeadStatus by ClientID
-- =============================================
CREATE PROCEDURE [dbo].[ls]
@ClientID int = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	If @ClientID IS NOT NULL
	BEGIN

			Select *
			From LeadStatus
			Where ClientID = @ClientID
			Order By StatusName

	End
	Else
	Begin
	
			Select *
			From LeadStatus
			Order By StatusName

	End

END




GO
GRANT VIEW DEFINITION ON  [dbo].[ls] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ls] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ls] TO [sp_executeall]
GO
