SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[TableDetailValues__GetByDetailFieldIDAndLeadID]
(

	@TableRowID int,
	@ClientID int,
	@DetailFieldID int,
	@LeadID int	
)
AS


				SELECT
					[TableDetailValueID],
					[TableRowID],
					[ResourceListID],
					[DetailFieldID],
					[DetailValue],
					[LeadID],
					[MatterID],
					[ClientID],
					[EncryptedValue],
					[ErrorMsg],
					[ValueInt],
					[ValueMoney],
					[ValueDate],
					[ValueDateTime],
					[CustomerID],
					[CaseID],
					[ClientPersonnelID],
					[ContactID]								
				FROM
					[dbo].[TableDetailValues] WITH (NOLOCK) 
				WHERE
					[TableRowID] = @TableRowID and 
					[ClientID] = @ClientID and					
					[DetailFieldID] = @DetailFieldID and
					[LeadID] = @LeadID
					
					
					
			Select @@ROWCOUNT




GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetByDetailFieldIDAndLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableDetailValues__GetByDetailFieldIDAndLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableDetailValues__GetByDetailFieldIDAndLeadID] TO [sp_executeall]
GO
