SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [dbo].[GetCustomer] @CustomerID int

as

Select Customers.CustomerID, Customers.ClientID, TitleID, FirstName, MiddleName, LastName, EmailAddress, 
DayTimeTelephoneNumber, DayTimeTelephoneNumberVerifiedAndValid, 
HomeTelephone, HomeTelephoneVerifiedAndValid, 
MobileTelephone, MobileTelephoneVerifiedAndValid, 
Address1, Address2, Town,
County, PostCode, CustomerQuestionnaires.ClientQuestionnaireID, HasDownLoaded, DownLoadedOn, Test, CompanyName, 
CompanyTelephone, CompanyTelephoneVerifiedAndValid, 
WorksTelephone, WorksTelephoneVerifiedAndValid, 
Occupation, Employer, DoNotEmail, DoNotSellToThirdParty, AgreedToTermsAndConditions,
PhoneNumbersVerifiedOn, DateOfBirth, IsBusiness, DefaultContactID
From Customers INNER JOIN
                      dbo.CustomerQuestionnaires ON dbo.Customers.CustomerID = dbo.CustomerQuestionnaires.CustomerID
Where Customers.CustomerID = @CustomerID





GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetCustomer] TO [sp_executeall]
GO
