SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the ClientPasswordRule table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ClientPasswordRule_Delete]
(

	@ClientPasswordRuleID int   
)
AS


				DELETE FROM [dbo].[ClientPasswordRule] WITH (ROWLOCK) 
				WHERE
					[ClientPasswordRuleID] = @ClientPasswordRuleID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPasswordRule_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientPasswordRule_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientPasswordRule_Delete] TO [sp_executeall]
GO
