SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EventTypeLimitation table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeLimitation_Find]
(

	@SearchUsingOR bit   = null ,

	@EventTypeLimitationID int   = null ,

	@ClientID int   = null ,

	@LeadTypeID int   = null ,

	@EventTypeIDFrom int   = null ,

	@EventTypeIDTo int   = null ,

	@LimitationDays int   = null ,

	@Description varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EventTypeLimitationID]
	, [ClientID]
	, [LeadTypeID]
	, [EventTypeIDFrom]
	, [EventTypeIDTo]
	, [LimitationDays]
	, [Description]
    FROM
	[dbo].[EventTypeLimitation] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeLimitationID] = @EventTypeLimitationID OR @EventTypeLimitationID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadTypeID] = @LeadTypeID OR @LeadTypeID IS NULL)
	AND ([EventTypeIDFrom] = @EventTypeIDFrom OR @EventTypeIDFrom IS NULL)
	AND ([EventTypeIDTo] = @EventTypeIDTo OR @EventTypeIDTo IS NULL)
	AND ([LimitationDays] = @LimitationDays OR @LimitationDays IS NULL)
	AND ([Description] = @Description OR @Description IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EventTypeLimitationID]
	, [ClientID]
	, [LeadTypeID]
	, [EventTypeIDFrom]
	, [EventTypeIDTo]
	, [LimitationDays]
	, [Description]
    FROM
	[dbo].[EventTypeLimitation] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeLimitationID] = @EventTypeLimitationID AND @EventTypeLimitationID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadTypeID] = @LeadTypeID AND @LeadTypeID is not null)
	OR ([EventTypeIDFrom] = @EventTypeIDFrom AND @EventTypeIDFrom is not null)
	OR ([EventTypeIDTo] = @EventTypeIDTo AND @EventTypeIDTo is not null)
	OR ([LimitationDays] = @LimitationDays AND @LimitationDays is not null)
	OR ([Description] = @Description AND @Description is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeLimitation_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeLimitation_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeLimitation_Find] TO [sp_executeall]
GO
