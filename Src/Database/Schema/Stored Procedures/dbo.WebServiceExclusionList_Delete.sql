SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the WebServiceExclusionList table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[WebServiceExclusionList_Delete]
(

	@WebServiceExclusionListID int   
)
AS


				DELETE FROM [dbo].[WebServiceExclusionList] WITH (ROWLOCK) 
				WHERE
					[WebServiceExclusionListID] = @WebServiceExclusionListID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[WebServiceExclusionList_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[WebServiceExclusionList_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[WebServiceExclusionList_Delete] TO [sp_executeall]
GO
