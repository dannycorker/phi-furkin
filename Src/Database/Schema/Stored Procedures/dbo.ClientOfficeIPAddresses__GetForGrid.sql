SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2010-04-26
-- Description:	Gets a list of ClientOfficeIPAddresses with Office Names
-- =============================================
CREATE PROCEDURE [dbo].[ClientOfficeIPAddresses__GetForGrid] 
	@ClientID int,
	@ClientOfficeID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT coipa.ClientOfficeIpAddressID
      ,coipa.ClientOfficeID
      ,ISNULL(co.BuildingName, ISNULL(co.Address1, ISNULL(co.Town, 'Unknown'))) AS 'OfficeName'
      ,coipa.ClientOfficeIPAddress
      ,coipa.Notes
      ,coipa.ClientID
  FROM ClientOfficeIPAddresses coipa WITH (NOLOCK)
  INNER JOIN ClientOffices co WITH (NOLOCK) ON co.ClientOfficeID = coipa.ClientOfficeID
  WHERE coipa.ClientID = @ClientID
  AND coipa.ClientOfficeID = @ClientOfficeID
END




GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeIPAddresses__GetForGrid] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ClientOfficeIPAddresses__GetForGrid] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ClientOfficeIPAddresses__GetForGrid] TO [sp_executeall]
GO
