SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the EventTypeAutomatedTask table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[EventTypeAutomatedTask_Find]
(

	@SearchUsingOR bit   = null ,

	@EventTypeAutomatedTaskID int   = null ,

	@ClientID int   = null ,

	@EventTypeID int   = null ,

	@AutomatedTaskID int   = null ,

	@TimeDelayInSeconds int   = null ,

	@AddTimeDelayIfAboutToRun bit   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [EventTypeAutomatedTaskID]
	, [ClientID]
	, [EventTypeID]
	, [AutomatedTaskID]
	, [TimeDelayInSeconds]
	, [AddTimeDelayIfAboutToRun]
    FROM
	[dbo].[EventTypeAutomatedTask] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeAutomatedTaskID] = @EventTypeAutomatedTaskID OR @EventTypeAutomatedTaskID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([EventTypeID] = @EventTypeID OR @EventTypeID IS NULL)
	AND ([AutomatedTaskID] = @AutomatedTaskID OR @AutomatedTaskID IS NULL)
	AND ([TimeDelayInSeconds] = @TimeDelayInSeconds OR @TimeDelayInSeconds IS NULL)
	AND ([AddTimeDelayIfAboutToRun] = @AddTimeDelayIfAboutToRun OR @AddTimeDelayIfAboutToRun IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [EventTypeAutomatedTaskID]
	, [ClientID]
	, [EventTypeID]
	, [AutomatedTaskID]
	, [TimeDelayInSeconds]
	, [AddTimeDelayIfAboutToRun]
    FROM
	[dbo].[EventTypeAutomatedTask] WITH (NOLOCK) 
    WHERE 
	 ([EventTypeAutomatedTaskID] = @EventTypeAutomatedTaskID AND @EventTypeAutomatedTaskID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([EventTypeID] = @EventTypeID AND @EventTypeID is not null)
	OR ([AutomatedTaskID] = @AutomatedTaskID AND @AutomatedTaskID is not null)
	OR ([TimeDelayInSeconds] = @TimeDelayInSeconds AND @TimeDelayInSeconds is not null)
	OR ([AddTimeDelayIfAboutToRun] = @AddTimeDelayIfAboutToRun AND @AddTimeDelayIfAboutToRun is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedTask_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EventTypeAutomatedTask_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EventTypeAutomatedTask_Find] TO [sp_executeall]
GO
