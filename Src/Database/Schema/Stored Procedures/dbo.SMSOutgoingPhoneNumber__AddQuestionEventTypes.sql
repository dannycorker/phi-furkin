SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 26-03-2013
-- Description:	Adds new question events for the given lead type
-- Updates:		2014-05-12	PR	Added sms gateway id
--				2015-07-06	SB	Set the event subtype to be voice if the questionnaire is voice
-- =============================================
CREATE PROCEDURE [dbo].[SMSOutgoingPhoneNumber__AddQuestionEventTypes]
	@ClientID INT,
	@LeadTypeID INT,
	@SMSQuestionnaireID INT,
	@UserID INT, 
	@SmsGatewayID INT
AS
BEGIN
	
	DECLARE @EventSubType INT = 13,
			@IsVoice BIT
			
	SELECT @IsVoice = IsVoice
	FROM dbo.SMSQuestionnaire WITH (NOLOCK) 
	WHERE SMSQuestionnaireID = @SMSQuestionnaireID
	
	IF @IsVoice = 1
	BEGIN
	
		SELECT @EventSubType = 26 
	
	END
	
	DECLARE @Events TABLE (EventtypeID int, SMSQuestionID int)	

	-- Add an event for each leadtype defined in SMSOutgoingPhoneNumber 
	INSERT INTO EventType (ClientID, EventTypeName, EventTypeDescription, Enabled, UnitsOfEffort, FollowupTimeUnitsID, FollowupQuantity, AvailableManually, StatusAfterEvent, AquariumEventAfterEvent, EventSubtypeID, DocumentTypeID, LeadTypeID, AllowCustomTimeUnits, InProcess, KeyEvent, UseEventCosts, UseEventUOEs, UseEventDisbursements, UseEventComments, SignatureRequired, SignatureOverride, VisioX, VisioY, AquariumEventSubtypeID, WhoCreated, WhenCreated, WhoModified, WhenModified, SourceID, SmsGatewayID)
	OUTPUT inserted.EventTypeID, inserted.SourceID INTO @Events (EventtypeID, SMSQuestionID)
	SELECT @ClientID,'SMS Out Event For Question ' + CAST(q.SMSQuestionID AS VARCHAR),'SMS Out Event For Question ' + CAST(q.SMSQuestionID AS VARCHAR),1,0,NULL,NULL,1,NULL,NULL,@EventSubType,q.DocumentTypeID,s.LeadTypeID,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,@UserID,dbo.fn_GetDate_Local(),@UserID,dbo.fn_GetDate_Local(), q.SMSQuestionID, @SmsGatewayID
	FROM SMSOutgoingPhoneNumber s WITH (NOLOCK) 
	INNER JOIN dbo.SMSQuestion q WITH (NOLOCK) ON q.SMSQuestionnaireID=s.SMSQuestionnaireID
	WHERE s.SMSQuestionnaireID=@SMSQuestionnaireID AND LeadTypeID=@LeadTypeID

	-- Add each event to SAE
	INSERT INTO EventTypeSql (ClientID, EventTypeID, PostUpdateSql, IsNativeSql)
	SELECT @ClientID, e.EventtypeID, 'EXEC dbo._C00_PostSMSSurveyTableInsert @LeadEventID',0
	FROM @Events e	
	
	-- Map events to question
	INSERT INTO SMSQuestionEventType (ClientID, SMSQuestionID, EventTypeID)
	SELECT @ClientID, e.SMSQuestionID, e.EventTypeID
	FROM @Events e


END
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber__AddQuestionEventTypes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SMSOutgoingPhoneNumber__AddQuestionEventTypes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SMSOutgoingPhoneNumber__AddQuestionEventTypes] TO [sp_executeall]
GO
