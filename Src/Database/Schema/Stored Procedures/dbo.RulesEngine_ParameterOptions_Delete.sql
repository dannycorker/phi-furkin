SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =========================================================
-- Author:		Jan Wilson
-- Create date: 2014-06-03
-- Description: Deletes a rule parameter options including
--              its output coordinates
-- =========================================================
CREATE PROCEDURE [dbo].[RulesEngine_ParameterOptions_Delete]
(
	@ParameterOptionID INT
)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DELETE oc
	FROM dbo.RulesEngine_OutputCoordinates oc WITH (NOLOCK) 
	INNER JOIN dbo.RulesEngine_ParameterOptions po WITH (NOLOCK) ON oc.ParameterOptionID = @ParameterOptionID

	DELETE po 
	FROM dbo.RulesEngine_ParameterOptions po WITH (NOLOCK) 
	WHERE po.ParameterOptionID = @ParameterOptionID
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ParameterOptions_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RulesEngine_ParameterOptions_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RulesEngine_ParameterOptions_Delete] TO [sp_executeall]
GO
