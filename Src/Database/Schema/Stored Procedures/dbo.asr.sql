SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- ALTER date: 2012-05-03
-- Description:	List all AutomatedScriptResults for a task or client
-- =============================================
CREATE PROCEDURE [dbo].[asr] 
	@ScriptID INT = NULL, 
	@ClientID INT = NULL, 
	@MaxRows INT = 500 
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		EXECUTE AS LOGIN = 'AquariumNet'
		
		EXEC AquariusMaster.dbo.asr @ScriptID, @ClientID, @MaxRows 
		
		REVERT
	END TRY
	BEGIN CATCH
		SELECT 'Please ask a dbo for IMPERSONATE permission on the AquariumNet login' as [Fail]
	END CATCH
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[asr] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[asr] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[asr] TO [sp_executeall]
GO
