SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2010-10-11
-- Description:	Select matter pages via MatterPageChoice for a particular matter and user.
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared pages
-- =============================================
CREATE PROCEDURE [dbo].[DetailFieldPages__GetByMatterPageChoiceSecure] 
	@MatterID int, 
	@UserID int, 
	@ClientID int, 
	@LeadTypeID int 
AS
BEGIN

	SELECT  dfp.*
	FROM
		dbo.fnDetailFieldPagesShared(@ClientID) dfp 
		INNER JOIN dbo.fnDetailFieldPagesSecure(@UserID, @LeadTypeID) f ON dfp.DetailFieldPageID = f.objectid 
	WHERE
		dfp.[LeadTypeID] = @LeadTypeID
	AND dfp.[Enabled] = 1 
	AND dfp.[LeadOrMatter] = 2 
	AND (
		NOT EXISTS (
			SELECT * 
			FROM dbo.MatterPageChoice mpc WITH (NOLOCK) 
			WHERE mpc.MatterID = @MatterID 
			AND mpc.ClientID = @ClientID
		)
		OR EXISTS (
			SELECT * 
			FROM dbo.MatterPageChoice mpc WITH (NOLOCK) 
			WHERE mpc.MatterID = @MatterID 
			AND mpc.ClientID = @ClientID 
			AND mpc.DetailFieldPageID = dfp.DetailFieldPageID 
		)
	)

	SELECT @@ROWCOUNT 
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages__GetByMatterPageChoiceSecure] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldPages__GetByMatterPageChoiceSecure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages__GetByMatterPageChoiceSecure] TO [sp_executeall]
GO
