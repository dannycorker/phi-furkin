SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the SystemMessageAssignment table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SystemMessageAssignment_Find]
(

	@SearchUsingOR bit   = null ,

	@SystemMessageAssignmentID int   = null ,

	@ClientID int   = null ,

	@ClientPersonnelAdminGroupID int   = null ,

	@ClientPersonnelID int   = null ,

	@SystemMessageID int   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [SystemMessageAssignmentID]
	, [ClientID]
	, [ClientPersonnelAdminGroupID]
	, [ClientPersonnelID]
	, [SystemMessageID]
    FROM
	[dbo].[SystemMessageAssignment] WITH (NOLOCK) 
    WHERE 
	 ([SystemMessageAssignmentID] = @SystemMessageAssignmentID OR @SystemMessageAssignmentID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID OR @ClientPersonnelAdminGroupID IS NULL)
	AND ([ClientPersonnelID] = @ClientPersonnelID OR @ClientPersonnelID IS NULL)
	AND ([SystemMessageID] = @SystemMessageID OR @SystemMessageID IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [SystemMessageAssignmentID]
	, [ClientID]
	, [ClientPersonnelAdminGroupID]
	, [ClientPersonnelID]
	, [SystemMessageID]
    FROM
	[dbo].[SystemMessageAssignment] WITH (NOLOCK) 
    WHERE 
	 ([SystemMessageAssignmentID] = @SystemMessageAssignmentID AND @SystemMessageAssignmentID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([ClientPersonnelAdminGroupID] = @ClientPersonnelAdminGroupID AND @ClientPersonnelAdminGroupID is not null)
	OR ([ClientPersonnelID] = @ClientPersonnelID AND @ClientPersonnelID is not null)
	OR ([SystemMessageID] = @SystemMessageID AND @SystemMessageID is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessageAssignment_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SystemMessageAssignment_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SystemMessageAssignment_Find] TO [sp_executeall]
GO
