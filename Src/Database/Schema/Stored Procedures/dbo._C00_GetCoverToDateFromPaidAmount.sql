SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex ELger
-- Create date: 2018-07-02
-- Description:	Get cover to based on amount paid
-- 2020-02-05 CPS for JIRA AAG-91	| Replace TableRow and TableDetailValues references with PurchasedProductPaymentScheduleDetail
-- 2020-03-18 NG  for AAG-512    | Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- =============================================
CREATE PROCEDURE [dbo].[_C00_GetCoverToDateFromPaidAmount]
	@PurchasedProductID INT,
	@AmountPaid MONEY,
	@Debug INT = 0
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @CoveredToDate DATE,
			@MatterID INT,
			@ProductStart DATE,
			@ProductEnd DATE,
			@ProductDays INT,
			@UNID int = 1,
			@TestID int = 0,
			@DaysUsed INT

	DECLARE @Table TABLE (UNID INT IDENTITY(1,1), CoverFrom DATE, CoverTo DATE, PremiumGross MONEY, DaysInPeriod INT, PricePerDay NUMERIC(18,4), AmountPaidFor MONEY, DaysUsedInPeriod INT)

	/*Get the start and end date of the product*/
	SELECT @ProductStart = pp.ValidFrom, @ProductEnd = pp.ValidTo, @MatterID = pp.ObjectID
	FROM PurchasedProduct pp WITH (NOLOCK)
	WHERE pp.PurchasedProductID = @PurchasedProductID

	SELECT @ProductDays = DATEDIFF(DD, @ProductStart, @ProductEnd)

	/*Work out the number of days in total (IE if we are in a leap year or not)*/
	IF @ProductDays > 0
	BEGIN

		SELECT @ProductDays = DATEDIFF(DD, @ProductStart, @ProductEnd)+1
	
	END

	IF @Debug = 1
	BEGIN

		SELECT @ProductStart AS [ProductStart],
				@ProductEnd AS [ProductEnd],
				@MatterID AS [MatterID],
				@ProductDays AS [ProductDays]

	END

	/*Add to our table variable the various terms as there may have been an MTA*/
	INSERT INTO @Table (CoverFrom, CoverTo, PremiumGross, DaysInPeriod)

	SELECT	 wp.ValidFrom AS [CoverFrom]
			,ISNULL(wp.ValidTo,@ProductEnd) AS [CoverTo]
			,(wp.AnnualPriceForRiskGross / @ProductDays) * DATEDIFF(DAY,wp.ValidFrom,ISNULL(wp.ValidTo,@ProductEnd)) AS [PremiumGross]
			,DATEDIFF(DAY,wp.ValidFrom,ISNULL(wp.ValidTo,@ProductEnd)) AS [DaysInPeriod]
	FROM WrittenPremium wp WITH (NOLOCK) 
	WHERE wp.MatterID = @MatterID
	AND wp.ValidFrom >= @ProductStart 
	AND (wp.ValidTo <= @ProductEnd or ValidTo is NULL)
	AND wp.AdjustmentTypeID IN ( 1 /*New*/,2 /*MTA*/,3 /*Renewal*/, 6 /*Reinstatement*/ )
	AND (wp.ValidFrom <= wp.ValidTo or wp.ValidTo is NULL)
	ORDER BY wp.WrittenPremiumID DESC

	DELETE FROM @Table 
	WHERE DaysInPeriod = 0 

	IF @Debug = 1
	BEGIN

		SELECT 'Terms Pre Update', *
		FROM @Table

	END



	/*Update the table to set the price of each day*/
	UPDATE @Table
	SET PricePerDay = PremiumGross/DaysInPeriod

	IF @Debug = 1
	BEGIN

		SELECT 'Terms Post Update 1', *
		FROM @Table

	END

	/*Loop over each row one at a time */
	WHILE @UNID > @testid
	BEGIN

		DECLARE @PremiumGross MONEY

		/*Get the wole annual premium value for the period in question*/
		SELECT @PremiumGross = t.PremiumGross
		FROM @Table t
		WHERE t.UNID = @UNID

		/*If the premium is less then the amount paid for this part of the policy then we can take the whole amount off the period */
		IF @PremiumGross <= @AmountPaid
		BEGIN

			UPDATE t
			SET AmountPaidFor = @PremiumGross, DaysUsedInPeriod = DaysInPeriod
			FROM @Table t
			WHERE t.UNID = @UNID

			/*Now we need to reduce the amount paid by the premium in this period*/
			SELECT @AmountPaid = @AmountPaid - @PremiumGross

		END
		ELSE
		BEGIN

			/*
				If we dont have enough premium to cover this period then we will need to work out how much of the period the payment will cover.
			*/
			SELECT @DaysUsed = @AmountPaid / t.PricePerDay
			FROM @Table t
			WHERE t.UNID = @UNID

			/*Update the table and set the amount paid to zero as there are no funds left in the pot..*/
			UPDATE t
			SET AmountPaidFor = @AmountPaid, DaysUsedInPeriod = @DaysUsed
			FROM @Table t
			WHERE t.UNID = @UNID

			SELECT @AmountPaid = 0.00

		END

		SELECT @TestID = @UNID
	
		/*Get the next row, there is no point getting one if the mnoey left in the pot is zero, break out of the loop..*/
		SELECT TOP 1 @PremiumGross = t.PremiumGross, @UNID = t.UNID
		FROM @Table t
		WHERE t.UNID > @UNID
		AND @AmountPaid > 0.00

	END

	IF @Debug = 1
	BEGIN

		SELECT 'Terms Post Update Loop', *, DATEADD(DD, NULLIF(DaysUsedInPeriod,0)-1, t.coverfrom) AS [Period Covered To]
		FROM @Table t

		SELECT SUM(t.DaysUsedInPeriod) AS [Total Number of Days Used]
		FROM @Table t


	END

	/*Finally get the max cover to date from the table*/
	SELECT @CoveredToDate = MAX(DATEADD(DD, NULLIF(DaysUsedInPeriod,0)-1, t.coverfrom))
	FROM @Table t

	SELECT @CoveredToDate AS [CoverToDate]

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetCoverToDateFromPaidAmount] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetCoverToDateFromPaidAmount] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetCoverToDateFromPaidAmount] TO [sp_executeall]
GO
