SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2008-12-18
-- Description:	Tell us all about a DataLoaderMap
-- =============================================
CREATE PROC [dbo].[DataLoaderMap__Describe] 
	@DataLoaderMapID int = null 
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @CurrentMapID int, 
	@CurrentSectionID int,
	@CurrentSectionLocaterTypeID int


	/* Display all the headline DataLoaderMap details */	
	SELECT m.MapName as [DataLoaderMap],
	m.DataLoaderMapID,
	m.ClientID,
	cl.CompanyName,
	m.MapName,
	m.MapDescription,
	m.WhenCreated,
	m.CreatedBy,
	cp1.UserName,
	m.LastUpdated,
	m.UpdatedBy,
	cp2.UserName,
	m.EnabledForUse,
	m.Deleted,
	m.LeadTypeID,
	lt.LeadTypeName,
	m.UseDefaultLeadForCustomer,
	m.UseDefaultCaseForLead,
	m.UseDefaultMatterForCase
	FROM dbo.DataLoaderMap m (nolock) 
	INNER JOIN dbo.Clients cl (nolock) ON cl.ClientID = m.ClientID 
	INNER JOIN dbo.ClientPersonnel cp1 (nolock) ON cp1.ClientPersonnelID = m.CreatedBy
	INNER JOIN dbo.ClientPersonnel cp2 (nolock) ON cp2.ClientPersonnelID = m.UpdatedBy
	LEFT JOIN dbo.LeadType lt (nolock) ON lt.LeadTypeID = m.LeadTypeID
	WHERE (m.DataLoaderMapID = @DataLoaderMapID OR @DataLoaderMapID IS NULL)
	ORDER BY m.DataLoaderMapID

	/* If a specific map id was passed in, examine that map now */
	IF @DataLoaderMapID IS NOT NULL
	BEGIN
		/* Loop through all the sections for the map, one at a time */
		DECLARE section_cursor CURSOR FOR 
		SELECT ms.DataLoaderMapID, ms.DataLoaderMapSectionID, ms.DataLoaderSectionLocaterTypeID
		FROM dbo.DataLoaderMapSection ms (nolock) 
		WHERE (ms.DataLoaderMapID = @DataLoaderMapID OR @DataLoaderMapID IS NULL) 
		ORDER BY ms.DataLoaderMapID, ms.DataLoaderObjectTypeID

		OPEN section_cursor   
		FETCH NEXT FROM section_cursor INTO @CurrentMapID, @CurrentSectionID, @CurrentSectionLocaterTypeID   

		WHILE @@FETCH_STATUS = 0   
		BEGIN   

			/* Map sections, displayed one at a time */	
			SELECT ot.ObjectTypeName as [DataLoaderMapSection],
			ms.DataLoaderMapID,
			ms.DataLoaderMapSectionID,
			ms.ClientID,
			ms.DataLoaderMapID,
			ms.DataLoaderObjectTypeID,
			ot.ObjectTypeName,
			ms.DataLoaderObjectActionID,
			oa.DataLoaderObjectActionName,
			ms.DataLoaderSectionLocaterTypeID,
			slt.LocaterTypeName,
			ms.DetailFieldSubTypeID,
			dfs.DetailFieldSubTypeName,
			ms.HasAquariumID,
			ms.NumberOfFooterRowsToSkip,
			ms.ImportIntoLeadManager,
			ms.IsFixedLengthSection,
			ms.FixedLengthNumberOfRows,
			ms.IsMandatory,
			ms.IsMultipleAllowedWithinFile,
			ms.Notes, 
			ms.TableDetailFieldID 
			FROM dbo.DataLoaderMap m (nolock) 
			LEFT JOIN dbo.DataLoaderMapSection ms (nolock) ON ms.DataLoaderMapID = m.DataLoaderMapID 
			LEFT JOIN dbo.DataLoaderObjectType ot (nolock) ON ot.DataLoaderObjectTypeID = ms.DataLoaderObjectTypeID
			LEFT JOIN dbo.DataLoaderObjectAction oa (nolock) ON oa.DataLoaderObjectActionID = ms.DataLoaderObjectActionID
			LEFT JOIN dbo.DataLoaderSectionLocaterType slt (nolock) ON slt.DataLoaderSectionLocaterTypeID = ms.DataLoaderSectionLocaterTypeID 
			LEFT JOIN dbo.DetailFieldSubType dfs (nolock) ON dfs.DetailFieldSubTypeID = ms.DetailFieldSubTypeID 
			WHERE ms.DataLoaderMapID = @CurrentMapID 
			AND ms.DataLoaderMapSectionID = @CurrentSectionID


			/* Now within each section, show all the details for finding the section start and end */
			IF @CurrentSectionLocaterTypeID = 1
			BEGIN
				SELECT slk.Keyword as [Section Locater Keywords],
				slk.ClientID,
				slk.DataLoaderMapID,
				slk.DataLoaderObjectTypeID,
				slk.DataLoaderMapSectionID,
				slk.Keyword,
				slk.DataLoaderKeywordMatchTypeID,
				slk.KeywordNotes,
				slk.RelativeRow,
				slk.AbsoluteCol,
				slk.IsForSectionStart 
				FROM dbo.DataLoaderSectionLocaterKeyword slk (nolock) 
				WHERE slk.DataLoaderMapID = @CurrentMapID 
				AND slk.DataLoaderMapSectionID = @CurrentSectionID
			END

			/* Also show all the fields we are going to detect and use */
			IF 0 < (
					SELECT COUNT(*) 
					FROM dbo.DataLoaderFieldDefinition fd (nolock) 
					WHERE fd.DataLoaderMapID = @CurrentMapID 
					AND fd.DataLoaderMapSectionID = @CurrentSectionID
					)
			BEGIN
				SELECT fd.DataLoaderFieldDefinitionID as [Field Definition],
				fd.ClientID,
				fd.DataLoaderMapID,
				fd.DataLoaderObjectTypeID,
				fd.DataLoaderMapSectionID,
				fd.DataLoaderObjectFieldID,
				obf.FieldName,
				obf.IsMandatory,
				dt.DataTypeName,
				fd.DetailFieldID,
				df.FieldName,
				df.Enabled,
				df.LeadOrMatter,
				dfs.DetailFieldSubTypeName,
				fd.DetailFieldAlias,
				fd.NamedValue,
				fd.Keyword,
				fd.DataLoaderKeywordMatchTypeID,
				fd.RowRelativeToKeyword,
				fd.ColRelativeToKeyword,
				fd.SectionRelativeRow,
				fd.SectionAbsoluteCol,
				fd.ValidationRegex,
				fd.Equation,
				fd.IsMatchField,
				fd.DecodeTypeID,
				dct.DecodeTypeName,
				fd.SourceDataLoaderFieldDefinitionID,
				fd.Notes,
				fd.AllowErrors
				FROM dbo.DataLoaderFieldDefinition fd (nolock) 
				LEFT JOIN dbo.DataLoaderObjectField obf (nolock) ON obf.DataLoaderObjectFieldID = fd.DataLoaderObjectFieldID
				LEFT JOIN dbo.DataLoaderDataType dt (nolock) ON dt.DataLoaderDataTypeID = obf.DataTypeID
				LEFT JOIN dbo.DetailFields df (nolock) ON df.DetailFieldID = fd.DetailFieldID 
				LEFT JOIN dbo.DetailFieldSubType dfs (nolock) ON dfs.DetailFieldSubTypeID = df.LeadOrMatter 
				LEFT JOIN dbo.DataLoaderDecodeType dct (nolock) ON dct.DataLoaderDecodeTypeID = fd.DecodeTypeID
				WHERE fd.DataLoaderMapID = @CurrentMapID 
				AND fd.DataLoaderMapSectionID = @CurrentSectionID
			END
			ELSE
			BEGIN
				SELECT 'No Fields defined for this section' as [Field Definition]
			END

			/* Re-fetch the section cursor */
			FETCH NEXT FROM section_cursor INTO @CurrentMapID, @CurrentSectionID, @CurrentSectionLocaterTypeID
		END   

		CLOSE section_cursor   
		DEALLOCATE section_cursor 

		/*CS 2012-04-24*/
		SELECT top(50) f.DataLoaderFileID [Last 50 DataloaderFileIDs],f.TargetFileName,f.TargetFileLocation,cp.UserName,f.ScheduledDateTime,f.DataLoadedDateTime, dbo.fn_C00_GetUrlByDatabase() + 'DataLoader/'+ f.TargetFileLocation +'/Client_' + convert(varchar,f.ClientID) + '/' + convert(varchar,f.DataLoaderFileID) + '.' + CASE f.FileFormatID WHEN 1 THEN 'xls' WHEN 2 THEN 'csv' END [File URL]
		FROM dbo.DataLoaderFile f WITH (NOLOCK) 
		INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = f.UploadedBy
		WHERE f.DataLoaderMapID = @DataLoaderMapID
		ORDER BY f.DataLoaderFileID desc
		/*CS 2012-04-24*/

	END
	ELSE
	BEGIN
		SELECT 'Full details of a map will be displayed if you call this proc with a MapID, eg: "DataLoaderMap__Describe 5"' as [Further Information]
	END

END




GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMap__Describe] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMap__Describe] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMap__Describe] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMap__Describe] TO [sp_executehelper]
GO
