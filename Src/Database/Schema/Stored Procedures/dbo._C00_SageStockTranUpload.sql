SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SageStockTranUpload]
	@StockData VARCHAR(MAX),
	@ClientID INT
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO LogXML (ClientID, ContextID, ContextVarchar, InvalidXML, LogDateTime, LogXMLEntry)
	VALUES (@ClientID, 98, 'XML Upload', NULL, dbo.fn_GetDate_Local(), @StockData)
	
	INSERT INTO AsyncQueue (ClientID, WhenCreated, QueueTypeID, Status, Payload, Outcome, WhenCompleted)
	VALUES (@ClientID, dbo.fn_GetDate_Local(), 12, 1, @StockData, NULL, NULL)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SageStockTranUpload] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SageStockTranUpload] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SageStockTranUpload] TO [sp_executeall]
GO
