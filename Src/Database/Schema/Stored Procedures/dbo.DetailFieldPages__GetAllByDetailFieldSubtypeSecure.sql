SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By:  Paul Richardson
-- Purpose: Select records from the DetailFieldPages that this user is allowed to see
-- MODIFIED: 2014-07-09 SB	Updated to use view which includes shared pages
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DetailFieldPages__GetAllByDetailFieldSubtypeSecure]
(
	
	@LeadOrMatter int,
	@ClientID int  
	
)
AS


				SET ANSI_NULLS OFF
				
				SELECT DISTINCT 
					[DetailFieldPageID],
					[ClientID],
					[LeadOrMatter],
					[LeadTypeID],
					[PageName],
					[PageCaption],
					[PageOrder],
					[Enabled],
					[ResourceList]
				FROM
					dbo.fnDetailFieldPagesShared(@ClientID)
				WHERE					
				Enabled = 1
				AND LeadOrMatter = @LeadOrMatter
				AND ClientID = @ClientID
				
				Select @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages__GetAllByDetailFieldSubtypeSecure] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DetailFieldPages__GetAllByDetailFieldSubtypeSecure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DetailFieldPages__GetAllByDetailFieldSubtypeSecure] TO [sp_executeall]
GO
