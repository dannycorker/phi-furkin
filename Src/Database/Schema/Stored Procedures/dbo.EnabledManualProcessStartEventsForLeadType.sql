SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-09-21
-- Description:	For use in GetEventChoicesByCaseAndEvent proc
-- =============================================
CREATE PROCEDURE [dbo].[EnabledManualProcessStartEventsForLeadType] 
	
	@ClientID INT,
	@LeadTypeID INT
	
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT NULL AS [EventChoiceID], et.EventTypeID AS [NextEventTypeID], et.EventTypeName AS [EventTypeName], 1 AS [ThreadNumber], et.EventTypeName AS [Description], et.EventSubtypeID, 0
	FROM EventType et 
	WHERE et.LeadTypeID = @LeadTypeID 
	AND et.EventSubtypeID = 10 
	AND et.AvailableManually = 1
	AND et.Enabled = 1
	AND et.ClientID = @ClientID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[EnabledManualProcessStartEventsForLeadType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[EnabledManualProcessStartEventsForLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[EnabledManualProcessStartEventsForLeadType] TO [sp_executeall]
GO
