SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the LeadDocumentEsignatureStatus table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadDocumentEsignatureStatus_Find]
(

	@SearchUsingOR bit   = null ,

	@LeadDocumentEsignatureStatusID int   = null ,

	@ClientID int   = null ,

	@LeadDocumentID int   = null ,

	@ElectronicSignatureDocumentKey varchar (50)  = null ,

	@DocumentName varchar (255)  = null ,

	@SentTo varchar (255)  = null ,

	@DateSent datetime   = null ,

	@EsignatureStatusID int   = null ,

	@StatusCheckedDate datetime   = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [LeadDocumentEsignatureStatusID]
	, [ClientID]
	, [LeadDocumentID]
	, [ElectronicSignatureDocumentKey]
	, [DocumentName]
	, [SentTo]
	, [DateSent]
	, [EsignatureStatusID]
	, [StatusCheckedDate]
    FROM
	[dbo].[LeadDocumentEsignatureStatus] WITH (NOLOCK) 
    WHERE 
	 ([LeadDocumentEsignatureStatusID] = @LeadDocumentEsignatureStatusID OR @LeadDocumentEsignatureStatusID IS NULL)
	AND ([ClientID] = @ClientID OR @ClientID IS NULL)
	AND ([LeadDocumentID] = @LeadDocumentID OR @LeadDocumentID IS NULL)
	AND ([ElectronicSignatureDocumentKey] = @ElectronicSignatureDocumentKey OR @ElectronicSignatureDocumentKey IS NULL)
	AND ([DocumentName] = @DocumentName OR @DocumentName IS NULL)
	AND ([SentTo] = @SentTo OR @SentTo IS NULL)
	AND ([DateSent] = @DateSent OR @DateSent IS NULL)
	AND ([EsignatureStatusID] = @EsignatureStatusID OR @EsignatureStatusID IS NULL)
	AND ([StatusCheckedDate] = @StatusCheckedDate OR @StatusCheckedDate IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [LeadDocumentEsignatureStatusID]
	, [ClientID]
	, [LeadDocumentID]
	, [ElectronicSignatureDocumentKey]
	, [DocumentName]
	, [SentTo]
	, [DateSent]
	, [EsignatureStatusID]
	, [StatusCheckedDate]
    FROM
	[dbo].[LeadDocumentEsignatureStatus] WITH (NOLOCK) 
    WHERE 
	 ([LeadDocumentEsignatureStatusID] = @LeadDocumentEsignatureStatusID AND @LeadDocumentEsignatureStatusID is not null)
	OR ([ClientID] = @ClientID AND @ClientID is not null)
	OR ([LeadDocumentID] = @LeadDocumentID AND @LeadDocumentID is not null)
	OR ([ElectronicSignatureDocumentKey] = @ElectronicSignatureDocumentKey AND @ElectronicSignatureDocumentKey is not null)
	OR ([DocumentName] = @DocumentName AND @DocumentName is not null)
	OR ([SentTo] = @SentTo AND @SentTo is not null)
	OR ([DateSent] = @DateSent AND @DateSent is not null)
	OR ([EsignatureStatusID] = @EsignatureStatusID AND @EsignatureStatusID is not null)
	OR ([StatusCheckedDate] = @StatusCheckedDate AND @StatusCheckedDate is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadDocumentEsignatureStatus_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadDocumentEsignatureStatus_Find] TO [sp_executeall]
GO
