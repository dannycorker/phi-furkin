SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the UserFunctionControlEditing table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[UserFunctionControlEditing_Delete]
(

	@UserFunctionControlEditingID int   
)
AS


				DELETE FROM [dbo].[UserFunctionControlEditing] WITH (ROWLOCK) 
				WHERE
					[UserFunctionControlEditingID] = @UserFunctionControlEditingID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControlEditing_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[UserFunctionControlEditing_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[UserFunctionControlEditing_Delete] TO [sp_executeall]
GO
