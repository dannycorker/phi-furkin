SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[GetOutcomeCounts] @ClientQuestionnaireID int, @OutcomeID int  AS

SELECT   ClientQuestionnaireID, COUNT(CustomerID) AS CustomerCount, OutcomeID, ClientID 
FROM     dbo.CustomerOutcomes
WHERE   (ClientQuestionnaireID = @ClientQuestionnaireID) 
AND		(OutcomeID = @OutcomeID)
GROUP BY ClientQuestionnaireID, OutcomeID, ClientID 



GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeCounts] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetOutcomeCounts] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetOutcomeCounts] TO [sp_executeall]
GO
