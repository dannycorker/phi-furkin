SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-03-02
-- Description:	Updated DFs based on the FAIL response from Incendia - for partner cards
-- =============================================
CREATE PROCEDURE [dbo].[Incendia_ProcessFailCardResponsePartner]
	@ClientID INT, 
	@LeadEventID INT,
	@ErrorCode VARCHAR(50),
	@ErrorDescription VARCHAR(500)
AS
BEGIN
	
	-- GET Lead, Case and Matter
	DECLARE @CaseID INT
	DECLARE @LeadID INT
	
	SELECT @CaseID = CaseID, @LeadID = LeadID
	FROM dbo.LeadEvent WITH (NOLOCK) 
	WHERE LeadEventID = @LeadEventID
	
	DECLARE @MatterID INT
	
	SELECT TOP 1 @MatterID = MatterID
	FROM dbo.Matter WITH (NOLOCK) 
	INNER JOIN dbo.Cases WITH (NOLOCK) ON Matter.CaseID = Cases.CaseID
	WHERE Cases.CaseID = @CaseID
	ORDER BY Matter.MatterID
	
	
	DECLARE @TableRowID INT
	SELECT @TableRowID = TableRowID
	FROM TableDetailValues
	WHERE DetailFieldID = 128257
	AND ValueInt = @LeadEventID


	DECLARE @DetailFieldID INT
		
	-- Error Code
	SELECT @DetailFieldID = 128251
	IF EXISTS (SELECT * FROM dbo.TableDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND TableRowID = @TableRowID)
	BEGIN
		UPDATE TableDetailValues
		SET DetailValue = @ErrorCode
		WHERE DetailFieldID = @DetailFieldID AND TableRowID = @TableRowID
	END
	ELSE
	BEGIN
		INSERT INTO TableDetailValues
		(ClientID, LeadID, MatterID, TableRowID, DetailFieldID, DetailValue)
		VALUES (@ClientID, @LeadID, @MatterID, @TableRowID, @DetailFieldID, @ErrorCode)
	END
	
	-- Error Description
	SELECT @DetailFieldID = 128252
	IF EXISTS (SELECT * FROM dbo.TableDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND TableRowID = @TableRowID)
	BEGIN
		UPDATE TableDetailValues
		SET DetailValue = @ErrorDescription
		WHERE DetailFieldID = @DetailFieldID AND TableRowID = @TableRowID
	END
	ELSE
	BEGIN
		INSERT INTO TableDetailValues
		(ClientID, LeadID, MatterID, TableRowID, DetailFieldID, DetailValue)
		VALUES (@ClientID, @LeadID, @MatterID, @TableRowID, @DetailFieldID, @ErrorDescription)
	END
		
	
	-- Card Order Result
	SELECT @DetailFieldID = 128259
	IF EXISTS (SELECT * FROM dbo.TableDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND TableRowID = @TableRowID)
	BEGIN
		UPDATE TableDetailValues
		SET DetailValue = 0
		WHERE DetailFieldID = @DetailFieldID AND TableRowID = @TableRowID
	END
	ELSE
	BEGIN
		INSERT INTO TableDetailValues
		(ClientID, LeadID, MatterID, TableRowID, DetailFieldID, DetailValue)
		VALUES (@ClientID, @LeadID, @MatterID, @TableRowID, @DetailFieldID, 1)
	END
	
	-- Response Date
	SELECT @DetailFieldID = 128250
	IF EXISTS (SELECT * FROM dbo.TableDetailValues WITH (NOLOCK) WHERE DetailFieldID = @DetailFieldID AND TableRowID = @TableRowID)
	BEGIN
		UPDATE TableDetailValues
		SET DetailValue = convert(varchar, dbo.fn_GetDate_Local(), 120) /* Format 120 looks like this: "2010-08-31 12:39:24" */
		WHERE DetailFieldID = @DetailFieldID AND TableRowID = @TableRowID
	END
	ELSE
	BEGIN
		INSERT INTO TableDetailValues
		(ClientID, LeadID, MatterID, TableRowID, DetailFieldID, DetailValue)
		VALUES (@ClientID, @LeadID, @MatterID, @TableRowID, @DetailFieldID, convert(varchar, dbo.fn_GetDate_Local(), 120)) /* Format 120 looks like this: "2010-08-31 12:39:24" */
	END
	
	-- Now update the status to order success
	SELECT @DetailFieldID = 128253
	UPDATE TableDetailValues
	SET DetailValue = '25222'
	WHERE DetailFieldID = @DetailFieldID AND TableRowID = @TableRowID
	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[Incendia_ProcessFailCardResponsePartner] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Incendia_ProcessFailCardResponsePartner] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Incendia_ProcessFailCardResponsePartner] TO [sp_executeall]
GO
