SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the HyperlinkStandardTarget table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[HyperlinkStandardTarget_GetByTemplateTypeID]
(

	@TemplateTypeID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[HyperlinkStandardTargetID],
					[ClientID],
					[HyperlinkDetailFieldID],
					[Target],
					[ObjectName],
					[PropertyName],
					[TemplateTypeID],
					[Notes],
					[IsSpecial]
				FROM
					[dbo].[HyperlinkStandardTarget] WITH (NOLOCK) 
				WHERE
					[TemplateTypeID] = @TemplateTypeID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkStandardTarget_GetByTemplateTypeID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[HyperlinkStandardTarget_GetByTemplateTypeID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[HyperlinkStandardTarget_GetByTemplateTypeID] TO [sp_executeall]
GO
