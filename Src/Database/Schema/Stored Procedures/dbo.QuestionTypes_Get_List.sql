SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the QuestionTypes table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[QuestionTypes_Get_List]

AS


				
				SELECT
					[QuestionTypeID],
					[Name],
					[Description],
					[Multiselect],
					[EcatcherField]
				FROM
					[dbo].[QuestionTypes] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionTypes_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[QuestionTypes_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[QuestionTypes_Get_List] TO [sp_executeall]
GO
