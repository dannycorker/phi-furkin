SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the BillItem table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BillItem_Delete]
(

	@BillItemID int   
)
AS


				DELETE FROM [dbo].[BillItem] WITH (ROWLOCK) 
				WHERE
					[BillItemID] = @BillItemID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[BillItem_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BillItem_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BillItem_Delete] TO [sp_executeall]
GO
