SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Paul Richardson
-- Create date: 08/10/2013
-- Description:	Resource List Usage Check
-- =============================================
CREATE PROCEDURE [dbo].[ResourceList__UsageCheck]

	@ResourceListDetailFieldPageID INT,
	@ClientID INT,
	@ResourceListID INT

AS
BEGIN

	DECLARE @Temp TABLE 
	( 
		DetailFieldID INT
	)

	INSERT INTO @Temp (DetailFieldID)
	SELECT DetailFieldID FROM DetailFields WITH (NOLOCK) 
	WHERE ResourceListDetailFieldPageID = @ResourceListDetailFieldPageID AND ClientID=@ClientID

	DECLARE @ClientDetailValueCount INT
	DECLARE @CustomerDetailValueCount INT
	DECLARE @LeadDetailValueCount INT
	DECLARE @CaseDetailValueCount INT
	DECLARE @MatterDetailValueCount INT

	SELECT @ClientDetailValueCount = COUNT(*) 
	FROM ClientDetailValues WITH (NOLOCK) 
	WHERE DetailFieldID IN (SELECT DetailFieldID FROM @Temp) AND ValueInt = @ResourceListID
	
	SELECT @CustomerDetailValueCount = COUNT(*) 
	FROM CustomerDetailValues WITH (NOLOCK) 
	WHERE DetailFieldID IN (SELECT DetailFieldID FROM @Temp) AND ValueInt = @ResourceListID
	
	SELECT @LeadDetailValueCount = COUNT(*) 
	FROM LeadDetailValues WITH (NOLOCK) 
	WHERE DetailFieldID IN (SELECT DetailFieldID FROM @Temp) AND ValueInt = @ResourceListID
	
	SELECT @CaseDetailValueCount = COUNT(*) 
	FROM CaseDetailValues WITH (NOLOCK) 
	WHERE DetailFieldID IN (SELECT DetailFieldID FROM @Temp) AND ValueInt = @ResourceListID
	
	SELECT @MatterDetailValueCount = COUNT(*) 
	FROM MatterDetailValues WITH (NOLOCK) 
	WHERE DetailFieldID IN (SELECT DetailFieldID FROM @Temp) AND ValueInt = @ResourceListID

	SELECT @ClientDetailValueCount ClientValueCount, 
		@CustomerDetailValueCount CustomerValueCount,
		@LeadDetailValueCount LeadValueCount,
		@CaseDetailValueCount CaseValueCount,
		@MatterDetailValueCount MatterValueCount	


END



GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList__UsageCheck] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ResourceList__UsageCheck] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ResourceList__UsageCheck] TO [sp_executeall]
GO
