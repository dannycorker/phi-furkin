SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the LeadTypeLink table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[LeadTypeLink_Delete]
(

	@LeadTypeLinkID int   
)
AS


				DELETE FROM [dbo].[LeadTypeLink] WITH (ROWLOCK) 
				WHERE
					[LeadTypeLinkID] = @LeadTypeLinkID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypeLink_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[LeadTypeLink_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[LeadTypeLink_Delete] TO [sp_executeall]
GO
