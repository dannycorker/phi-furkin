SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the ValidationCriterias table through an index
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ValidationCriterias_GetByValidationCriteriaID]
(

	@ValidationCriteriaID int   
)
AS


				SELECT
					[ValidationCriteriaID],
					[Criteria]
				FROM
					[dbo].[ValidationCriterias] WITH (NOLOCK) 
				WHERE
										[ValidationCriteriaID] = @ValidationCriteriaID
				SELECT @@ROWCOUNT
					
			

GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriterias_GetByValidationCriteriaID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ValidationCriterias_GetByValidationCriteriaID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ValidationCriterias_GetByValidationCriteriaID] TO [sp_executeall]
GO
