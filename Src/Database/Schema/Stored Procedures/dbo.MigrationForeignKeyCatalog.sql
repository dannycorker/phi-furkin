SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-02-10
-- Description:	Script Foreign Keys ready for drop/recreate/escrow.
-- =============================================
CREATE PROCEDURE [dbo].[MigrationForeignKeyCatalog] 
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @fkName VARCHAR(800), 
	@tabName VARCHAR(800), 
	@refName VARCHAR(800),
	@isDel INT, 
	@isUpd INT, 
	@fkCol VARCHAR(8000), 
	@refCol VARCHAR(8000),
	@pline VARCHAR(8000), 
	@dline VARCHAR(8000) 

	/* Clear out the migration table before we start */
	DELETE [dbo].[MigrationForeignKey]
	
	/*
		Declare a cursor to loop through all keys one at a time.
		We need this to handle the update/delete cascade options individually. 
	*/
	DECLARE fkCursor CURSOR FOR
		SELECT DISTINCT OBJECT_NAME(constid), OBJECT_NAME(fkeyid), 
			OBJECT_NAME(rkeyid), 
			OBJECTPROPERTY ( constid , 'CnstIsDeleteCascade' ),
			OBJECTPROPERTY ( constid , 'CnstIsUpdateCascade' )
		FROM sysforeignkeys k 
		ORDER BY OBJECT_NAME(fkeyid)

	OPEN fkCursor

	FETCH NEXT FROM fkCursor 
		INTO @fkName, @tabName, @refName, @isDel, @isUpd

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @fkCol = NULL, @refCol = NULL
		
		SELECT @fkCol = ISNULL(@fkCol + ', ','') + '[' + COL_NAME(fkeyid, fkey) + ']'
		FROM sysforeignkeys 
		WHERE OBJECT_NAME(constid) = @fkName 
		ORDER BY keyno

		SELECT @refCol = ISNULL(@refCol + ', ','') + '[' + COL_NAME(rkeyid, rkey) + ']'
		FROM sysforeignkeys 
		WHERE OBJECT_NAME(constid) = @fkName 
		ORDER BY keyno

		SELECT @dline = 'ALTER TABLE [dbo].[' + @tabName + '] DROP CONSTRAINT ' + @fkName 
			
		SELECT @pline = 'ALTER TABLE [dbo].[' + @tabName + '] ADD CONSTRAINT [' + @fkName + ']' + ' FOREIGN KEY (' + @fkCol + ') REFERENCES [dbo].[' + @refName + '] (' + @refCol + ')' + CASE @isDel WHEN 1 THEN ' ON DELETE CASCADE' ELSE '' END + CASE @isUpd WHEN 1 THEN ' ON UPDATE CASCADE' ELSE '' END 
		
		/* Insert the definition of each FK into our migration table */
		INSERT INTO [dbo].[MigrationForeignKey]
		(
			[TableName],
			[ForeignKeyName],
			[DropText],
			[CreateText],
			[IsEnabled]
		)
		SELECT
			@tabName,
			@fkName,
			@dline,
			@pline,
			1
		
		FETCH NEXT FROM fkCursor 
			INTO @fkName, @tabName, @refName, @isDel, @isUpd
	END

	CLOSE fkCursor
	DEALLOCATE fkCursor

END			





GO
GRANT VIEW DEFINITION ON  [dbo].[MigrationForeignKeyCatalog] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MigrationForeignKeyCatalog] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MigrationForeignKeyCatalog] TO [sp_executeall]
GO
