SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
----------------------------------------------------------------------------------------------------

-- Created By: Aaran Gravestock
-- Purpose: Select records from the PortalUserGroupQuestionnaireAccess table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[PortalUserGroupQuestionnaireAccess__GetByPortalUserGroupIDAndClientQuestionnaireID]
(

	@PortalUserGroupID int,
	@ClientQuestionnaireID int
)
AS


				SET ANSI_NULLS OFF
				
				SELECT
					[PortalUserGroupQuestionnaireAccessID],
					[ClientID],
					[PortalUserGroupID],
					[ClientQuestionnaireID],
					[HasAccess]
				FROM
					[dbo].[PortalUserGroupQuestionnaireAccess]
				WHERE
					[PortalUserGroupID] = @PortalUserGroupID
				AND [ClientQuestionnaireID] = @ClientQuestionnaireID
				
				SELECT @@ROWCOUNT
				SET ANSI_NULLS ON
			





GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess__GetByPortalUserGroupIDAndClientQuestionnaireID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PortalUserGroupQuestionnaireAccess__GetByPortalUserGroupIDAndClientQuestionnaireID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PortalUserGroupQuestionnaireAccess__GetByPortalUserGroupIDAndClientQuestionnaireID] TO [sp_executeall]
GO
