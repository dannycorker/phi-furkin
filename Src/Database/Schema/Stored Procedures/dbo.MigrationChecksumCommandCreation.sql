SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2011-02-16
-- Description:	Having compared static data between two Aquarium databases,
-- create the commands to run on the target database so it matches the source.
-- =============================================
CREATE PROCEDURE [dbo].[MigrationChecksumCommandCreation]
	/*
		This proc gets called twice: first to produce a list of results to auto-run; and then again for a list of tricky tables to check manually 
	*/
	@ManualUpdateStandalone bit = 0 
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TableName varchar(1024), 
	@Action char(6),
	@IDListToUpdate dbo.tvpInt, 
	@SysTableID int,					/* From Sql Server */
	@ColumnList varchar(2000) = '', 
	@PKColumnName varchar(2000), 
	@HasIdentity bit, 
	@IdentityName sysname, 
	@ColumnNumber int,
	@ColumnName sysname,
	@ColumnDataType nvarchar(128),		/* From Sql Server */
	@IsColumnNullable varchar(3),		/* From ISC: values are NO or YES */
	@IDToUpdate int,
	@WhereClauseToGetData varchar(250),
	@WhereClauseForPrint varchar(250),
	@SqlStatement varchar(max),
	@WorkSeedID int
	
	/* 
		Work out all the combinations of tables and actions 
	*/
	DECLARE @TablesToSync TABLE (
		TableName varchar(1024), 
		ActionName char(6)
	)
	/* Create a work list in the form DELETE FROM xyz, INSERT xyz, UPDATE xyz */
	INSERT INTO @TablesToSync (TableName, ActionName) 
	SELECT DISTINCT mca.MigrationTableName, mca.ActionName 
	FROM dbo.MigrationChecksumAction mca WITH (NOLOCK) 
	INNER JOIN dbo.MigrationTable mt WITH (NOLOCK) ON mt.TableName = mca.MigrationTableName 
	/* Some tables may be relevant to the standalone, but probably aren't, eg SequenceNumber, ThirdPartyField */
	/* COALESCE is not great in a where clause but this is a non-indexed column on a tiny table */
	WHERE COALESCE(mt.ManualUpdateStandalone, 0) = @ManualUpdateStandalone 
	ORDER BY mca.ActionName, mca.MigrationTableName 

	EXEC @WorkSeedID = [dbo].[GetWorkSeedID]
	
	/* Loop through all the table/action combinations one at a time */
	WHILE EXISTS(SELECT * FROM @TablesToSync)
	BEGIN
		
		/* Pick the next table/action combination */
		SELECT TOP (1) 
		@TableName = tts.TableName, 
		@Action = tts.ActionName 
		FROM @TablesToSync tts 
		ORDER BY tts.ActionName, tts.TableName 
		
		SELECT @PKColumnName = isc.COLUMN_NAME 
		FROM INFORMATION_SCHEMA.COLUMNS isc 
		WHERE isc.TABLE_NAME = @TableName 
		AND isc.ORDINAL_POSITION = 1

		IF @Action = 'DELETE'
		BEGIN
			INSERT dbo.Work2 (WorkSeedID, Value1) 
			SELECT @WorkSeedID, 'DELETE dbo.' + @TableName + ' WHERE ' + @PKColumnName + ' = ' + CAST(mca.PKID AS VARCHAR) 
			FROM MigrationChecksumAction mca WITH (NOLOCK) 
			WHERE mca.MigrationTableName = @TableName 
			AND mca.ActionName = 'DELETE' 
		END
		ELSE
		BEGIN
			/* Pick the list of rows for this table/action combination */
			INSERT INTO @IDListToUpdate (AnyID) 
			SELECT mca.PKID 
			FROM dbo.MigrationChecksumAction mca WITH (NOLOCK) 
			WHERE mca.MigrationTableName = @TableName 
			AND mca.ActionName = @Action 
			ORDER BY mca.PKID 
			
			/* Get the integer ID that Sql Server uses to identify this table */
			SELECT @SysTableID = OBJECT_ID(@TableName), 
			@ColumnName = '',
			@ColumnList = '',
			@SqlStatement = '', 
			@IdentityName = NULL 

			/* Check if the table has an IDENTITY column */
			SELECT @IdentityName = ic.[name] 
			FROM sys.identity_columns ic 
			WHERE ic.[object_id] = @SysTableID 

			SELECT @HasIdentity = CASE WHEN @IdentityName IS NULL THEN 0 ELSE 1 END
		END
		
		IF @Action = 'INSERT'
		BEGIN

			/* Turn Identity inserts on if required */
			IF @HasIdentity = 1
			BEGIN
				INSERT dbo.Work2 (WorkSeedID, Value1) 
				VALUES (@WorkSeedID, 'SET IDENTITY_INSERT ' + @TableName + ' ON')
			END
			
			SELECT @ColumnList += isc.COLUMN_NAME + ',' 
			FROM INFORMATION_SCHEMA.COLUMNS isc 
			WHERE isc.TABLE_NAME = @TableName 
			ORDER BY isc.ORDINAL_POSITION 
			
			IF @ColumnList LIKE '%,'
			BEGIN
				SELECT @ColumnList = Left(@ColumnList, LEN(@ColumnList) - 1)
			END

			WHILE EXISTS(SELECT * FROM @IDListToUpdate)
			BEGIN
			
				/* Loop through the list of all the IDs to update */
				SELECT TOP 1 
				@IDToUpdate = i.AnyID,
				@ColumnNumber = 0,
				@ColumnName = '',
				@SqlStatement = 'SELECT ' + CAST(@WorkSeedID as varchar) + ', ''INSERT dbo.' + @TableName + ' ( ' + @ColumnList + ' ) VALUES (', 
				@WhereClauseForPrint = ' ) '' ', 
				@WhereClauseToGetData = ' FROM dbo.' + @TableName + ' t WITH (NOLOCK) WHERE ' + @PKColumnName + ' = ' + CAST(i.AnyID as varchar) 
				FROM @IDListToUpdate i 
				ORDER BY i.AnyID
				
				WHILE @ColumnName IS NOT NULL
				BEGIN
					SELECT TOP 1 
					@ColumnName = isc.COLUMN_NAME,
					@ColumnNumber = isc.ORDINAL_POSITION, 
					@ColumnDataType = isc.DATA_TYPE, 
					@IsColumnNullable = isc.IS_NULLABLE 
					FROM INFORMATION_SCHEMA.COLUMNS isc 
					WHERE isc.TABLE_NAME = @TableName 
					AND isc.ORDINAL_POSITION > @ColumnNumber 
					ORDER BY isc.ORDINAL_POSITION 
					
					IF @@ROWCOUNT < 1
					BEGIN
						SELECT @ColumnName = NULL
					END
					ELSE
					BEGIN
						--SELECT @SqlStatement += ' '' + IsNull(master.dbo.fn_varbintohexstr (cast(t.' + @ColumnName + ' as varbinary(max))), ''null'') + '','--
						--SELECT @SqlStatement += ' '' + IsNull(master.dbo.fn_varbintohexstr (cast(t.' + @ColumnName + ' as varbinary(max))), ' + CASE @IsColumnNullable WHEN 'YES' THEN '''NULL''' ELSE '''0x00''' END + ') + '','
						SELECT @SqlStatement += CASE WHEN @ColumnDataType = 'DATE' THEN ' CAST(' ELSE ' ' END + ''' + IsNull(master.dbo.fn_varbintohexstr (cast(t.' + @ColumnName + ' as varbinary(max))), ' + CASE @IsColumnNullable WHEN 'YES' THEN '''NULL''' ELSE '''0x00''' END + ') + ''' + CASE WHEN @ColumnDataType = 'DATE' THEN ' AS DATE)' ELSE '' END  + ','
					END
					
				END
				
				/* Remove the inevitable trailing comma */	
				IF @SqlStatement LIKE '%,'
				BEGIN
					SELECT @SqlStatement = Left(@SqlStatement, LEN(@SqlStatement) - 1)
				END
				
				SELECT @SqlStatement += @WhereClauseForPrint + @WhereClauseToGetData 
				
				/* EXEC this statement and store the results */
				INSERT dbo.Work2 (WorkSeedID, Value1)
				EXEC(@SqlStatement)
				
				/* Remove this entry from the list and loop round again */
				DELETE @IDListToUpdate WHERE AnyID = @IDToUpdate
				
			END
			
			/* Turn Identity inserts off again if required */
			IF @HasIdentity = 1
			BEGIN
				INSERT dbo.Work2 (WorkSeedID, Value1) 
				VALUES (@WorkSeedID, 'SET IDENTITY_INSERT ' + @TableName + ' OFF')
			END
			
		END

		IF @Action = 'UPDATE'
		BEGIN

			WHILE EXISTS(SELECT * FROM @IDListToUpdate)
			BEGIN
			
				/* Loop through the list of all the IDs to update */
				SELECT TOP 1 
				@IDToUpdate = i.AnyID,
				@ColumnNumber = 0,
				@ColumnName = '',
				@SqlStatement = 'SELECT ' + CAST(@WorkSeedID as varchar) + ', ''UPDATE dbo.' + @TableName + ' SET ', 
				@WhereClauseForPrint = ' WHERE ' + @PKColumnName + ' = ' + CAST(i.AnyID as varchar) + ' '' ', 
				@WhereClauseToGetData = ' FROM dbo.' + @TableName + ' t WITH (NOLOCK) WHERE ' + @PKColumnName + ' = ' + CAST(i.AnyID as varchar) 
				FROM @IDListToUpdate i 
				ORDER BY i.AnyID
				
				WHILE @ColumnName IS NOT NULL
				BEGIN
					SELECT TOP 1 
					@ColumnName = isc.COLUMN_NAME,
					@ColumnNumber = isc.ORDINAL_POSITION, 
					@ColumnDataType = isc.DATA_TYPE, 
					@IsColumnNullable = isc.IS_NULLABLE 
					FROM INFORMATION_SCHEMA.COLUMNS isc 
					WHERE isc.TABLE_NAME = @TableName 
					AND isc.COLUMN_NAME <> @PKColumnName
					AND isc.ORDINAL_POSITION > @ColumnNumber 
					ORDER BY isc.ORDINAL_POSITION 
					
					IF @@ROWCOUNT < 1
					BEGIN
						SELECT @ColumnName = NULL
					END
					ELSE
					BEGIN
						--SELECT @SqlStatement += @ColumnName + ' = '' + IsNull(master.dbo.fn_varbintohexstr (cast(t.' + @ColumnName + ' as varbinary(max))), ''null'') + '','
						--SELECT @SqlStatement += @ColumnName + ' = '' + IsNull(master.dbo.fn_varbintohexstr (cast(t.' + @ColumnName + ' as varbinary(max))), ' + CASE @IsColumnNullable WHEN 'YES' THEN '''NULL''' ELSE '''0x00''' END + ') + '','
						SELECT @SqlStatement += @ColumnName + ' = ' + CASE WHEN @ColumnDataType = 'DATE' THEN 'CAST(' ELSE '' END + ''' + IsNull(master.dbo.fn_varbintohexstr (cast(t.' + @ColumnName + ' as varbinary(max))), ' + CASE @IsColumnNullable WHEN 'YES' THEN '''NULL''' ELSE '''0x00''' END + ') + ''' + CASE WHEN @ColumnDataType = 'DATE' THEN ' AS DATE)' ELSE '' END  + ','

					END
					
				END
				
				/* Remove the inevitable trailing comma */	
				IF @SqlStatement LIKE '%,'
				BEGIN
					SELECT @SqlStatement = Left(@SqlStatement, LEN(@SqlStatement) - 1)
				END
				
				SELECT @SqlStatement += @WhereClauseForPrint + @WhereClauseToGetData 
							
				/* EXEC this statement and store the results */
				INSERT dbo.Work2 (WorkSeedID, Value1)
				EXEC(@SqlStatement)
				
				/* Remove this entry from the list and loop round again */
				DELETE @IDListToUpdate WHERE AnyID = @IDToUpdate
				
			END /* Update branch WHILE loop */
			
		END /* Update branch */

		/* Pick the next table/action combination */
		/* Remove this table from the list and loop round again */
		DELETE @TablesToSync 
		WHERE TableName = @TableName 
		AND ActionName = @Action 
		
	END

	/* Select out all the commands in the exact order in which they were added */
	SELECT w2.Value1 as [Command] 
	FROM dbo.Work2 w2 WITH (NOLOCK) 
	WHERE w2.WorkSeedID = @WorkSeedID 
	ORDER BY w2.Work2ID 

	/* Clear the work table (using cascade delete) */
	DELETE dbo.WorkSeed WHERE WorkSeedID = @WorkSeedID 
	
END





GO
GRANT VIEW DEFINITION ON  [dbo].[MigrationChecksumCommandCreation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MigrationChecksumCommandCreation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MigrationChecksumCommandCreation] TO [sp_executeall]
GO
