SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2014-07-15
-- Description:	Proc to Run SAE for shared Pet Claim lead type 1272
-- ROH 2014-08-27 Lock claim rows for edit/delete after claim calculation
-- JEL 2017-07-33 Readded claim payment SAE
-- CR  2019-07-26 Added an entry for the amendment process to allow assessors to approve payments if the amend the claim 
-- =============================================
CREATE PROCEDURE [dbo].[_C00_SAE_1272] 
(
	@LeadEventID INT
)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE	@LeadID INT,
			@EventTypeID INT,
			@CustomerID INT,
			@ClientID INT,
			@CaseID INT,
			@ClientAccountID INT,
			@WhoCreated INT,
			@MatterID INT, 
			@TableRowID INT,
			@SubClientName VARCHAR(100),
			@ClientTypeID INT,
			@OfficeName VARCHAR(100),
			@OfficeAddress1 VARCHAR(100),
			@OfficeAddress2 VARCHAR(100),
			@OfficeTown VARCHAR(100),
			@OfficeCounty VARCHAR(100),
			@OfficeCountry VARCHAR(100),
			@OfficePostCode VARCHAR(10),
			@OfficePhone VARCHAR(100),
			@SubClientID INT,
			@OfficeID INT,
			@Value VARCHAR(2000),
			@ValueDate VARCHAR(10),
			@ValueMoney MONEY,
			@ClaimStatus INT,
			@AutomationUser INT,
			@PolMatterID INT,
			@RenewalDate DATE,
			@WhenCreated DATE,
			@EventComments	VARCHAR(2000) = '',
			@AccountName VARCHAR (2000),
			@AccountSortCode VARCHAR (2000),
			@AccountNumber VARCHAR (2000),
			@ResourceListID INT,
			@PolicyAdminMatterID INT,
			@LeadIDToUse INT,
			@DetailValue	VARCHAR(2000),
			@FraudFlagsChecked INT = 0,
			@ImportantFlagsChecked INT = 0,
			@PALeadID INT,
			@PACaseID INT,
			@DateForEvent DATE,
			@CommentsToApply VARCHAR(1000)	,
			@RowCount INT,
			@DateOfLoss VARCHAR(2000),
			@PAMatterID INT
			
	DECLARE @ErrorMessage VARCHAR(2000)
	
	DECLARE @LeadEventTvp tvpLeadEvent

	DECLARE @CPS TABLE (CustomerPaymentScheduleID INT)

	SELECT TOP (1) 
		@LeadID = le.LeadID, 
		@EventTypeID = le.EventTypeID, 
		@CustomerID = m.CustomerID, 
		@ClientID = m.ClientID, 
		@CaseID = le.CaseID, 
		@WhoCreated = le.WhoCreated, 
		@MatterID = m.MatterID
	FROM dbo.LeadEvent le WITH (NOLOCK)
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.CaseID = le.CaseID	
	WHERE le.LeadEventID = @LeadEventID 
	
	SELECT @AutomationUser = dbo.fn_C00_GetAutomationUser(@ClientID)
	
	-- Fraud checklist reviewed
	IF @EventTypeID IN (125982) 
	BEGIN
	
		
		-- Determine how many fraud flags are checked
		-- and how many important flags (within 60 days inception, unreasonable costs, unreasonable treatments, dodgy documents)
		SELECT 
			@FraudFlagsChecked += 1
			--,@ImportantFlagsChecked += CASE WHEN mdvFraudFlag.DetailFieldID IN (144486, 144495, 144496, 144497) THEN 1 ELSE 0 END /*GPR commented out for C600 2018-03-19*/
		FROM Matter m WITH (NOLOCK)
		INNER JOIN dbo.MatterDetailValues mdvFraudFlag WITH (NOLOCK) ON mdvFraudFlag.MatterID = m.MatterID
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON mdvFraudFlag.DetailFieldID = df.DetailFieldID
		WHERE m.MatterID = @MatterID
		AND df.DetailFieldPageID = 16160 -- Fraud checklist page
		AND mdvFraudFlag.ValueInt = 1 -- Flag checked

		-- If 3 or more flags are checked, or any of the important flags, raise the "Fraud - check requested" event
		IF (@FraudFlagsChecked >= 1) --3 OR @ImportantFlagsChecked >= 1) /*2018-03-19 GPR commented out for C600, and changed flag count to >=1 to add note*/
		BEGIN
			
			-- Release date feature removed for SIG -- ROH 2015-01-14
			
			--DECLARE @ReleaseDate DATETIME
			---- Set check expiry date to N working days after claim form received
			--SELECT @ReleaseDate = dbo.fnAddWorkingDays(mdvCFRecd.ValueDate, 5)
			--FROM MatterDetailValues mdvCFRecd WITH (NOLOCK)
			----LEFT JOIN dbo.ClientDetailValues cdv WITH (NOLOCK) ON cdv.ClientID = @ClientID AND cdv.DetailFieldID = 162702 -- Fraud check days setting
			--WHERE mdvCFRecd.ClientID = @ClientID
			--AND mdvCFRecd.MatterID = @MatterID
			--AND mdvCFRecd.DetailFieldID = 144520
			
			-- Only raise event if there is sufficent time
			--IF @ReleaseDate IS NOT NULL AND @ReleaseDate >= dbo.fn_GetDate_Local()
			BEGIN

				INSERT @LeadEventTvp (ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, FollowupDateTime, CaseID, Eventdeleted, LeadEventIDToFollowUp)
				--SELECT @ClientID, @LeadID, dbo.fn_GetDate_Local(), @AutomationUser, 0.00, 'Created by _C00_SAE_1272', 142117, @ReleaseDate, @CaseID, 0, NULL
				SELECT @ClientID, @LeadID, dbo.fn_GetDate_Local(), @AutomationUser, 0.00, 'Created by _C00_SAE_1272', 142117, NULL, @CaseID, 0, NULL
				
				EXEC _C00_ApplyLeadEvent @LeadEventTvp
				
				-- Add a nice note
				EXEC _C00_AddANote @CaseID, 776, 'Held for validation check', 0, @AutomationUser, 0
				
				-- Force status because the trigger won't fire when an event raised from here
				UPDATE Cases SET ClientStatusID = 4689 WHERE CaseID = @CaseID
									
			END
			--ELSE
			--BEGIN
		
			--	-- Add a nice note
			--	EXEC _C00_AddANote @CaseID, 776, 'Validation request suppressed as SLA period already used up', 0, @AutomationUser, 0
			
			--END
			
		END

	END

	
	-- Enter Treatment Dates
	
	/*SA - Added @WhoCreated parameter following proc update*/
	IF @EventTypeID IN (138258)
	BEGIN
		
		-- Set Fraud Indicators
		EXEC [_C00_1272_Claim_FraudIndicators_AutoSet] @MatterID, @WhoCreated
		
	END
	
	
	
	-- Pet claim apply excess and other deductions
	IF @EventTypeID IN (121862,156567)
	BEGIN

	/*GPR 2019-11-04 replaced with HUD Congig ReportID 38891*/
		
		----/*CPS 2017-08-16*/
		----IF dbo.fnGetSimpleDvAsInt(178446,@MatterID) = 1 /*Refer these calculations for manager override*/
		----BEGIN
		----	EXEC _C00_AddANote @CaseID, 935, 'Manager override required', 1, @WhoCreated, 0, 1, @WhenCreated
		----	SELECT @DetailValue = dbo.fn_C00_GetUrl_CaseDetailValuesByObjectID(@CaseID,11,0)
		----	EXEC dbo._C00_SimpleValueIntoField 178652, @DetailValue, @MatterID, @WhoCreated /*Override Claims Calculations Window*/
		----END
				
		-- For any linked claims that have an event of 121862 not followed up then we need to follow it up with 124063 (calculation run on linked claim) 
		
		INSERT @LeadEventTvp (ClientID, LeadID, WhenCreated, WhoCreated, Cost, Comments, EventTypeID, FollowupDateTime, CaseID, Eventdeleted, LeadEventIDToFollowUp)
		SELECT @ClientID, ev.LeadID, dbo.fn_GetDate_Local(), @AutomationUser, 0.00, 'Created by _C00_SAE_1272', 124063, dbo.fn_GetDate_Local(), ev.CaseID, 0, ev.LeadEventID
		FROM dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 0) m
		INNER JOIN dbo.LeadEvent ev WITH (NOLOCK) ON m.CaseID = ev.CaseID AND ev.EventTypeID IN (121863, 122701, 123460, 123461, 123462) -- 5 pay x events
		WHERE m.MatterID != @MatterID
		AND ev.EventDeleted = 0
		AND ev.WhenFollowedUp IS NULL
		
		EXEC _C00_ApplyLeadEvent @LeadEventTvp
		
		-- Run the calculation	
		EXEC _C00_1272_Calculations_RunGroupedClaimCalcs @CaseID
		
		-- Lock claim rows
		UPDATE r SET DenyEdit = 1, DenyDelete = 1
		FROM TableRows r WITH (NOLOCK)
		WHERE r.MatterID = @MatterID
		AND r.DetailFieldID = 144355
	
	END
	
	--IF @EventTypeID = 156567
	--BEGIN -- OOP calculation event needs two events to run, so apply, enter details, then the event you have applied morphs into an in process event to force you to apply the calculation event 
		
	--	EXEC _C00_MorphLeadEvent @LeadEventID,	156568
		
		
	--END

	-- Update the payee and payee type field
	IF @EventTypeID IN  (123460, 123461, 121863, 123462, 157254)
	BEGIN
		
		DECLARE @PayToData dbo.tvpIntMoney
		INSERT @PayToData(ID, VALUE)
		SELECT	CASE @EventTypeID
					WHEN 123460 THEN 44231 -- Pay PH
					WHEN 123461 THEN 44233 -- Pay TP
					WHEN 121863 THEN 44232 -- Pay VT
					WHEN 123462 THEN 46135 -- Pay RV
								WHEN 157254 THEN 76367 -- Pay RV (Out of Hours) - /*SA - 2018-03-15*/
				END, SUM(ValueMoney) 
		FROM dbo.TableDetailValues WITH (NOLOCK) 
		WHERE MatterID = @MatterID
		AND DetailFieldID = 144352
		
		-- Set Potential Fraud Indicator if paying third party
		IF @EventTypeID = 123461
		BEGIN
			EXEC _C00_SimpleValueIntoField 144494, 'true', @MatterID, @AutomationUser
		END
		
		-- Handle recalcs
		DECLARE @AlreadyPaid MONEY
		EXEC @AlreadyPaid = dbo.fn_C00_1272_GetAlreadyPaid @MatterID
		
		IF @AlreadyPaid <> 0
		BEGIN
			UPDATE @PayToData
			SET VALUE = VALUE - @AlreadyPaid
		END
				
		EXEC dbo._C00_1272_Payments_SaveSapPaymentData_ForApproval @MatterID, @PayToData, '0' 
		
	END
	
	
	---- Send status update to VetEnvoy for claim being processed, settled and declined
	--IF @EventTypeID IN (121472, 121865, 121869)
	--BEGIN
	
	--	DECLARE @Status VARCHAR(2000)
	--	SELECT @Status =	CASE @EventTypeID
	--							WHEN 121472 THEN '19000' -- Processed
	--							WHEN 121865 THEN '19001' -- Settled
	--							WHEN 121869 THEN '19002' -- Declined
	--						END
	
	--	EXEC dbo._C321_VetEnvoy_UpdateStatus @MatterID, @Status
	
	--END
	
	IF @EventTypeID in(150192) /*Pay (6) under excess*/
	BEGIN
		/*
		CPS 2017-06-22
		For Below-Excess Claims, mark the row as approved automatically so that it can eat into the excess for this condition for this policy year.
		*/
		SELECT @EventComments = ''
		
		INSERT dbo.TableDetailValues (ClientID, DetailFieldID, CustomerID, LeadID, CaseID, MatterID, TableRowID, DetailValue)
		SELECT @ClientID, 144362, @CustomerID, @CaseID, @LeadID, @MatterID, TableRowID, CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120) /*Approved*/
		FROM TableRows r WITH (NOLOCK) /* SLACKY-PERF-20190705 */
		WHERE r.DetailFieldID = 144355 /*Claim Details Data*/
		AND r.DetailFieldPageID = 16157 /*Claim Details*/
		AND r.MatterID = @MatterID
		AND ClientID = @ClientID
		AND NOT EXISTS (
			SELECT *
			FROM dbo.TableDetailValues tdv WITH (NOLOCK) /* SLACKY-PERF-20190705 */
			WHERE tdv.TableRowID = r.TableRowID AND tdv.MatterID = r.MatterID AND tdv.DetailFieldID = 144362 /*Approved*/
		)
		/* !!! NO SQL HERE !!! */
		SELECT @EventComments += CONVERT(VARCHAR,@@ROWCOUNT) + ' claim detail items marked as approved.'
		
		EXEC dbo._C00_SetLeadEventComments @LeadEventID, @EventComments, 1
	END
	
	IF @EventTypeID IN (121865) -- Confirm payee
	BEGIN
		
		/*UH 2018-06-29*/
		SELECT @ValueMoney =   tdv.ValueMoney FROM TableDetailValues tdv WITH (NOLOCK) where tdv.DetailFieldID = 154518 and tdv.MatterID = @MatterID
  
		SELECT  @DetailValue = dbo.fnNumberToText(@ValueMoney, 1) --2nd parameter =  GPB currency type ID
  
		--Convert Total Settlement figure to words
		EXEC _C00_SimpleValueIntoField 180000,@DetailValue,@MatterID, @WhoCreated
		
					
		
		/* Enforce Approval Limits */
		
		/*Ensure the user who scheduled the payment does not approve the same payment.*/
		/* CR 2019-07-26 Added an additional entry for the Ammendment that will allow the assessor to authorise the payment*/ 

		IF EXISTS(SELECT TOP 1* FROM LeadEvent le WITH ( NOLOCK ) 
		INNER JOIN ClientPersonnel cp WITH ( NOLOCK ) on cp.ClientPersonnelID = le.WhoCreated
		WHERE le.EventTypeID IN (150191,150190) AND le.WhoCreated = @WhoCreated AND le.caseID = @CaseID /*and cp.IsAquarium <> 1*/ ) -- Pay by BACS/Cheque
		AND NOT EXISTS(SELECT TOP 1* FROM LeadEvent le WITH ( NOLOCK )																	/*CR 2019-07-26 added this not exists select for the amendment autorisation*/
		INNER JOIN ClientPersonnel cp WITH ( NOLOCK ) on cp.ClientPersonnelID = le.WhoCreated
		WHERE le.EventTypeID IN (/*150191,150190,*/158915) AND le.WhoCreated = @WhoCreated AND le.caseID = @CaseID /*and cp.IsAquarium <> 1*/ ) -- Pay by BACS/Cheque/Amend
		AND 0 = ( SELECT cp.IsAquarium FROM Clientpersonnel cp WITH (NOLOCK) WHERE cp.ClientPersonnelID = @WhoCreated )
		BEGIN
			SELECT @ErrorMessage = '<font color="red"></br></br>Notice: Person who scheduled the payment cannot approve that payment</br></font>'
			RAISERROR( @ErrorMessage, 16, 1 )
			RETURN 
		END
				
		DECLARE @UserApprovalLimit MONEY
		SELECT @UserApprovalLimit = COALESCE(cpdv.ValueMoney, cdv.ValueMoney, 0.00) 
		FROM ClientPersonnel cp WITH (NOLOCK) 
		LEFT JOIN ClientPersonnelDetailValues cpdv WITH (NOLOCK) ON cpdv.ClientPersonnelID = cp.ClientPersonnelID AND cpdv.DetailFieldID = 175378 
		LEFT JOIN ClientDetailValues cdv WITH (NOLOCK) on cdv.ClientID = cp.ClientID and cdv.DetailFieldID = 175447
		WHERE cp.ClientPersonnelID = @WhoCreated
		
		DECLARE @ClaimApprovalTotal MONEY
		SELECT @ClaimApprovalTotal = SUM(tdvTotal.ValueMoney)
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvTotal WITH (NOLOCK) ON r.TableRowID = tdvTotal.TableRowID AND tdvTotal.DetailFieldID = 144352
		WHERE r.MatterID = @MatterID
				
		IF @ClaimApprovalTotal > @UserApprovalLimit
		BEGIN

			SELECT @ErrorMessage = 
				'<br /><br /><font color="red" size="4">Claim amount (£' 
				+ CONVERT(VARCHAR,@ClaimApprovalTotal) 
				+ ') exceeds the approval limit (£'
				+ CONVERT(VARCHAR,@UserApprovalLimit)
				+') for your user profile.</font><br/><br/>'
				+'<font color="blue" size=3>Please pass this claim for high-value approval.</font><br/><br/>'
			RAISERROR(@ErrorMessage,16,1)
			RETURN
		
		END
		
		-- Set an approved date
	
		UPDATE dbo.TableDetailValues
		SET DetailValue = CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
		WHERE DetailFieldID = 144362
		AND MatterID = @MatterID
		AND ValueDate IS NULL
		AND ClientID = @ClientID
		
		INSERT dbo.TableDetailValues (ClientID, DetailFieldID, CustomerID, LeadID, CaseID, MatterID, TableRowID, DetailValue)
		SELECT @ClientID, 144362, @CustomerID, @CaseID, @LeadID, @MatterID, TableRowID, CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
		FROM TableRows r WITH (NOLOCK) /* SLACKY-PERF-20190705 */
		WHERE r.DetailFieldID = 144355
		AND r.DetailFieldPageID = 16157
		AND r.MatterID = @MatterID
		AND ClientID = @ClientID
		AND NOT EXISTS (
			SELECT *
			FROM dbo.TableDetailValues tdv WITH (NOLOCK) /* SLACKY-PERF-20190705 */
			WHERE tdv.TableRowID = r.TableRowID AND tdv.MatterID = r.MatterID AND tdv.DetailFieldID = 144362
		)

		-- Check to see if there is any owed premium and deduct it from this claim
		-- EXEC dbo._C321_Claim_DeductOwedPremium @MatterID, @LeadID
		
		
		-- Get the claimed and approved totals into a MDF for the matter list
		EXEC dbo._C00_1272_Claim_SumClaimedAndApproved @MatterID, @LeadID
		
		
		-- Write the deduction text to the claim table ready for the letter to be produced
		EXEC dbo._C00_1272_Docs_PrepareSettlementDoc @MatterID
		
		-- Save the SAP payment data ready for sending
		--EXEC _C321_Payments_SaveSapPaymentData @MatterID
		
		-- Now the SAP payments have already been saved so we just need to set them as approved.
		UPDATE dbo.TableDetailValues
		SET DetailValue = CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
		WHERE DetailFieldID = 159407
		AND MatterID = @MatterID
		AND ValueDate IS NULL
		AND ClientID = @ClientID
		
		INSERT dbo.TableDetailValues (ClientID, DetailFieldID, CustomerID, LeadID, CaseID, MatterID, TableRowID, DetailValue)
		SELECT @ClientID, 159407, @CustomerID, @CaseID, @LeadID, @MatterID, TableRowID, CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120)
		FROM TableRows r WITH (NOLOCK) /* SLACKY-PERF-20190705 */
		WHERE r.DetailFieldID = 154485
		AND r.DetailFieldPageID = 17324
		AND r.MatterID = @MatterID
		AND ClientID = @ClientID
		AND NOT EXISTS (
			SELECT *
			FROM dbo.TableDetailValues tdv WITH (NOLOCK) /* SLACKY-PERF-20190705 */
			WHERE tdv.TableRowID = r.TableRowID AND tdv.MatterID = r.MatterID AND tdv.DetailFieldID = 159407
		)
		
		-- Set the Condition Running Total on the Matter (for Fees reporting etc)
		DECLARE @RunningTotal MONEY 
		SET @RunningTotal = ISNULL(dbo.fn_C00_1272_GetGroupedClaimsRunningTotal(@MatterID),0)
		EXEC dbo._C00_SimpleValueIntoField 162645,@RunningTotal,@MatterID,@WhoCreated
		
		-- Clear any rejection artefacts (for claims that have been previously rejected before being reopened)
		EXEC dbo._C00_SimpleValueIntoField 146184,0,@MatterID,@WhoCreated	--Rejection reason
		EXEC dbo._C00_SimpleValueIntoField 152692,'',@MatterID,@WhoCreated	--Excluded treatment
		EXEC dbo._C00_SimpleValueIntoField 152875,'',@MatterID,@WhoCreated	--Rejection text PH
		EXEC dbo._C00_SimpleValueIntoField 152876,'',@MatterID,@WhoCreated	--Rejection text VET
		-- Except this one which is now used by below-excess approval
		--EXEC dbo._C00_SimpleValueIntoField 144521,0,@MatterID,@WhoCreated	--Rejection Payee
		
		/*Get the client account to settle claim payment from from Affinity RL*/ 
		SELECT @ClientAccountID = rldv.ValueInt 
		FROM CustomerDetailValues cdv WITH ( NOLOCK ) 
		INNER JOIN Matter m WITH ( NOLOCK ) on m.CustomerID = cdv.CustomerID 
		INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) on rldv.ResourceListID = cdv.ValueInt
		WHERE m.MatterID = @MatterID 
		AND cdv.DetailFieldID = 170144
		AND rldv.DetailFieldID = 179950

		INSERT INTO CustomerPaymentSchedule (ClientID,CustomerID,AccountID,ActualCollectionDate,PaymentDate,PaymentNet,PaymentVAT,PaymentGross,PaymentStatusID,CustomerLedgerID,ReconciledDate,WhoCreated,WhenCreated,SourceID,RelatedObjectID,RelatedObjectTypeID,WhoModified,WhenModified,ClientAccountID)
		OUTPUT inserted.CustomerPaymentScheduleID INTO @CPS
		SELECT tr.ClientID,@CustomerID,tdvAccount.ValueInt,dbo.fn_GetDate_Local(),dbo.fn_GetDate_Local(),-tdvAmount.ValueMoney,0.00,-tdvAmount.ValueMoney,1,NULL,NULL,@WhoCreated,dbo.fn_GetDate_Local(),tr.TableRowID, tr.MatterID,2,@WhoCreated, dbo.fn_GetDate_Local(), @ClientAccountID  
		FROM TableRows tr WITH ( NOLOCK ) 
		LEFT JOIN TableDetailValues tdvAccount WITH ( NOLOCK ) on tdvAccount.TableRowID = tr.tableRowID 
		INNER JOIN TableDetailValues tdvAmount WITH ( NOLOCK ) on tdvAmount.TableRowID = tr.TableRowID 
		AND not EXISTS (SELECT * FROM TableDetailValues tdvFail WITH ( NOLOCK ) where tdvFail.TableRowID = tr.TableRowID and tdvFail.DetailFieldID = 179902 and ISNULL(tdvFail.DetailValue,'') <> '') 
		AND not EXISTS (SELECT * FROM TableDetailValues tdvstop WITH ( NOLOCK ) where tdvstop.TableRowID = tr.TableRowID and tdvstop.DetailFieldID = 159289 and ISNULL(tdvstop.DetailValue,'') <> '') 
		WHERE tr.MatterID = @MatterID 
		AND tdvAccount.DetailFieldID = 179988
		AND tdvAmount.DetailFieldID = 154518
		
		SELECT @RowCount = @@RowCount

		SELECT @CommentsToApply = ISNULL(CONVERT(VARCHAR,@RowCount), 'Null') + ' rows added'

		SELECT @CommentsToApply = @CommentsToApply + ', CPS ID = ' + CONVERT(VARCHAR,c.CustomerPaymentScheduleID)
		FROM @CPS c

		EXEC dbo._C00_SetLeadEventComments @LeadEventID, @CommentsToApply, 1

	END
	
	
	IF @EventTypeID IN (121869) --Claim rejected
	BEGIN
	 
		DECLARE @NoteTypeID INT
		DECLARE @NoteText VARCHAR(2000)
		DECLARE @NotePriority INT
		DECLARE @ApplyToAll INT

		SET @NoteTypeID = 861 --Rejection
		SET @NotePriority = 0 --Default priority
		SET @ApplyToAll = 0 --Case/Matter only

		--Retrieve rejection reason text from lookup list
		SELECT @NoteText = 'Rejection reason: ' + lli.ItemValue
		FROM MatterDetailValues mdv WITH (NOLOCK)
		INNER JOIN LookupListItems lli WITH (NOLOCK) ON mdv.ValueInt = lli.LookupListItemID
		WHERE mdv.MatterID = @MatterID
		AND mdv.DetailFieldID = 146184 --Rejection reason

		EXEC dbo._C00_AddANote
				  @CaseID
				  ,@NoteTypeID
				  ,@NoteText
				  ,@NotePriority
				  ,@WhoCreated
				  ,@ApplyToAll
					  
		
		-- Get the claimed and approved totals (set to 0.00 as rejected) into a MDF for the matter list
		EXEC dbo._C00_1272_Claim_SumClaimedAndApproved @MatterID, @LeadID, @ClaimRejection = 1
		
		-- Write the rejection text to the matter level system table ready for the letter to be produced
		EXEC _C00_1272_Docs_PrepareRejectionDoc @MatterID
	 
	END
	
	
	IF @EventTypeID IN (122704) -- Setting the exclusion text so need to re-run the rejection doc proc
	BEGIN
		-- Write the rejection text to the matter level system table ready for the letter to be produced
		EXEC _C00_1272_Docs_PrepareRejectionDoc @MatterID
	END
	
	
	
	-- Pet claim - Claim ready for putting on.
	-- Link to the correct policy wording for the scheme
	IF @EventTypeID IN (121472)
	BEGIN
	
		DECLARE @CurrentPolicyMatterID INT,
				@CurrentPolicyCaseID INT
		
		SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)
		SELECT @CurrentPolicyCaseID = CaseID
		FROM dbo.Matter WITH (NOLOCK) 
		WHERE MatterID = @CurrentPolicyMatterID
	
		DECLARE @PolicyDocumentID INT
		SELECT TOP 1 @PolicyDocumentID = le.LeadDocumentID
		FROM dbo.LeadEvent le WITH (NOLOCK)
		WHERE le.EventTypeID = 121130
		AND le.CaseID = @CurrentPolicyCaseID
		ORDER BY le.LeadEventID DESC
		
		UPDATE dbo.LeadEvent
		SET LeadDocumentID = @PolicyDocumentID,
		Comments = 'Policy Wording'
		WHERE LeadEventID = @LeadEventID

		-- Set the dead pet text if required
		EXEC dbo._C00_1272_Policy_SetDeadPetText @LeadID, @CaseID, @WhoCreated
		
	END


	-- Open claim to re-issue payment --- stop check
	IF @EventTypeID IN (138585)
	BEGIN
		
		EXEC dbo._C00_1272_Payments_StopCheck @MatterID
	
	END
	
	
	-- Refunds!
	IF @EventTypeID IN (142096)
	BEGIN
	
		DECLARE @Error VARCHAR(2000) = '',
				@Amount MONEY,
				@PaymentRow INT,
				@ClaimRow INT
				
		SELECT @Amount = mdvAmount.ValueMoney, @PaymentRow = mdvPayment.ValueInt, @ClaimRow = mdvClaim.ValueInt
		FROM dbo.Matter m WITH (NOLOCK) 
		INNER JOIN dbo.MatterDetailValues mdvAmount WITH (NOLOCK) ON m.MatterID = mdvAmount.MatterID AND mdvAmount.DetailFieldID = 162654
		INNER JOIN dbo.MatterDetailValues mdvPayment WITH (NOLOCK) ON m.MatterID = mdvPayment.MatterID AND mdvPayment.DetailFieldID = 162652
		INNER JOIN dbo.MatterDetailValues mdvClaim WITH (NOLOCK) ON m.MatterID = mdvClaim.MatterID AND mdvClaim.DetailFieldID = 162653
		WHERE m.MatterID = @MatterID
		
		IF @Amount > 0 AND @PaymentRow > 0 AND @ClaimRow > 0
		BEGIN
		
			DECLARE @ClaimTotal MONEY,
					@PaymentTotal MONEY
					
			SELECT @ClaimTotal = ValueMoney
			FROM dbo.TableDetailValues WITH (NOLOCK) 
			WHERE TableRowID = @ClaimRow
			AND DetailFieldID = 144352
			
			SELECT @PaymentTotal = ValueMoney
			FROM dbo.TableDetailValues WITH (NOLOCK) 
			WHERE TableRowID = @PaymentRow
			AND DetailFieldID = 154518
					
			IF @Amount <= @ClaimTotal AND @Amount <= @PaymentTotal
			BEGIN
			
				-- We create a new user deduction of type refund for the amount specified
				INSERT INTO dbo.TableRows (ClientID, MatterID, DetailFieldID, DetailFieldPageID)
				VALUES (@ClientID, @MatterID, 147302, 16157)
				
				DECLARE @DeductionRow INT
				SELECT @DeductionRow = SCOPE_IDENTITY()
				
				INSERT INTO dbo.TableDetailValues(TableRowID, DetailFieldID, DetailValue, LeadID, MatterID, ClientID) VALUES
				(@DeductionRow, 147299, CAST(@ClaimRow AS VARCHAR), @LeadID, @MatterID, @ClientID),
				(@DeductionRow, 147300, '69824', @LeadID, @MatterID, @ClientID),
				(@DeductionRow, 147301, CAST(@Amount * -1 AS VARCHAR), @LeadID, @MatterID, @ClientID),
				(@DeductionRow, 148432, '', @LeadID, @MatterID, @ClientID)
				
				-- then we change the user deduction amount, settle value and claim row total appropriately
				UPDATE dbo.TableDetailValues
				SET DetailValue = ISNULL(ValueMoney, 0) - @Amount
				WHERE TableRowID = @ClaimRow
				AND MatterID = @MatterID
				AND ClientID = @ClientID
				AND DetailFieldID IN (146179, 145678, 144352)
				
				-- finally we create a new payment row for the refund
				DECLARE @RefundData dbo.tvpIntMoney
				INSERT @RefundData(ID, VALUE)
				SELECT ISNULL(tdvPayTo.ValueInt, 44231), @Amount * -1  -- default to pay PH
				FROM dbo.Matter m WITH (NOLOCK)  
				LEFT JOIN dbo.TableDetailValues tdvPayTo WITH (NOLOCK) ON m.MatterID = tdvPayTo.MatterID AND tdvPayTo.TableRowID = @PaymentRow AND tdvPayTo.DetailFieldID = 159408
				WHERE m.MatterID = @MatterID
				
				-- Remembering to save the original row as the reference
				EXEC dbo._C00_SimpleValueIntoField 159559, @PaymentRow, @MatterID
				
				EXEC dbo._C00_1272_Payments_SaveSapPaymentData_ForApproval @MatterID, @RefundData, '1'
			
			END
			ELSE
			BEGIN
				SELECT @Error = '<br /><br /><font color="red">The amount to refund must be less than or equal to the claim row total and less than or equal to the payment amount.</font><br/><br />'
				RAISERROR(@Error,16,1)
				RETURN
			END
		
		END
		ELSE
		BEGIN
			SELECT @Error = '<br /><br /><font color="red">Please enter a valid refund amount and choose a payment and claim row.</font><br/><br />'
			RAISERROR(@Error,16,1)
			RETURN
		END
	
	END
	
	
	
	-- Confirm payment cancellation
	IF @EventTypeID IN (142108) -- OOP Confirm payment cancellation
	BEGIN
	
		EXEC _C00_1272_Payments_ConfirmOrAbortCancellation @CaseID, 1
	
	END
	
		-- Confirm payment cancellation
	IF @EventTypeID IN (142109) -- OOP Abort unsent payment rows
	BEGIN
	
		EXEC _C00_1272_Payments_ConfirmOrAbortCancellation @CaseID, 2
	
	END
	
	
	
	IF @EventTypeID IN (121861) -- Setting the Rejection Reason to "Created in error" if "Terminate - Created in error" event is raised
	BEGIN
		
		/*
		
			2018-07-03
			Robin!!! No History Use SimpleValue into Field!
		
		INSERT MatterDetailValues (ClientID,LeadID,MatterID,DetailFieldID,DetailValue)
		VALUES(@ClientID,@LeadID,@MatterID,146184,53432)
		*/

		EXEC dbo._C00_SimpleValueIntoField 146184, '53432' /*Created in error*/, @MatterID, @WhoCreated

	END
	
	--IF @EventTypeID = 121865 /*Rather than the document writing tool we have mapped deduction reasons to a rl, this then pulls the relevant paragraph onto the letter*/
	--BEGIN
	
	--	DECLARE @Rows TABLE (Reason VARCHAR(2000),Amount DECIMAL (18,2), RowID INT, Line VARCHAR(2000)) 	
	--	INSERT INTO @Rows (Reason,Amount,Line, RowID) 
		
	--	SELECT	 llReason.ItemValue AS Reason, tdvAmount.ValueMoney AS Amount, rldv1.DetailValue + '£' + CAST(tdvAmount.ValueMoney * -1 AS VARCHAR(50)) + rldv2.DetailValue + CHAR(13) + CHAR(10), r.TableRowID
	--	FROM dbo.TableRows r WITH (NOLOCK)
	--	INNER JOIN dbo.TableDetailValues tdvRow WITH (NOLOCK) ON r.TableRowID = tdvRow.TableRowID AND tdvRow.DetailFieldID = 147299
	--	INNER JOIN dbo.TableDetailValues tdvReason WITH (NOLOCK) ON r.TableRowID = tdvReason.TableRowID AND tdvReason.DetailFieldID = 147300
	--	INNER JOIN dbo.LookupListItems llReason WITH (NOLOCK) ON tdvReason.ValueInt = llReason.LookupListItemID 
	--	INNER JOIN dbo.TableDetailValues tdvAmount WITH (NOLOCK) ON r.TableRowID = tdvAmount.TableRowID AND tdvAmount.DetailFieldID = 147301
	--	INNER JOIN dbo.ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.DetailFieldID = 176950 AND rldv.ValueInt = tdvReason.ValueInt 
	--	INNER JOIN dbo.ResourceListDetailValues rldv1 WITH (NOLOCK) ON rldv.ResourceListID = rldv1.ResourceListID AND rldv1.DetailFieldID = 176951
	--	INNER JOIN dbo.ResourceListDetailValues rldv2 WITH (NOLOCK) ON rldv2.ResourceListID = rldv.ResourceListID AND rldv2.DetailFieldID = 176952
	--	WHERE r.MatterID = @MatterID
	--	AND r.DetailFieldID = 147302
	--	AND r.DetailFieldPageID = 16157
		
	--	DECLARE @NewTableRowIDs TABLE ( TAbleRowID INT, SourceID INT )
	
	--	SELECT @PolMatterID =  dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(@MatterID)

	--	SELECT @RenewalDate = dbo.fnGetSimpleDVasDate(170037,@PolMatterID)
		
	--	UPDATE @Rows 
	--	SET Line = REPLACE(Line,'{RenewalDate}',CAST(@RenewalDate AS VARCHAR(50))) 
		

		
	--	INSERT INTO TableDetailValues (ClientID, MatterID,DetailValue,DetailFieldID,TableRowID) 
	--	SELECT 384,@MatterID,r.Line,148348, tr.TableRowID  
	--	FROM @Rows r
	--	INNER JOIN @NewTableRowIDs tr ON tr.SourceID = r.RowID 
		
	--	IF EXISTS (SELECT * FROM LeadEvent le WITH (NOLOCK) WHERE le.CaseID = @CaseID 
	--			   AND le.EventTypeID = 150191)
	--	BEGIN 
		
	--		EXEC _C00_SimpleValueIntoField   150191,'true',@MatterID, @WhoCreated 
		
	--	END
	--	ELSE
	--	BEGIN
			
	--		EXEC _C00_SimpleValueIntoField   150191,'false',@MatterID, @WhoCreated 
		
	--	END
	
	--	SELECT @NoteText = @NoteText + CHAR(13) + CHAR(10) + tdv.DetailValue   FROM TableRows tr 
	--	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 148348 
	--	WHERE tr.MatterID = @MatterID 
			
	--	IF ISNULL(@NoteText,'') <> '' 
	--	BEGIN
	--		EXEC _C00_AddANote @CaseID,921,@NoteText,0,@WhoCreated,0,0,@WhenCreated 
	--	END
	--END
	
	/*SA Added 2017-06-27 - temp workaround to transfer VET bank details to detail fields for Pay by BACS*/
	
	IF @EventTypeID = 121863 /*Pay (2) vet*/
	BEGIN	
	
		SELECT @PolicyAdminMatterID =  dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(@MatterID)
		
		SELECT @LeadIDToUse = l.LeadID
		FROM   Lead l WITH (NOLOCK)
		INNER JOIN Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID
		WHERE m.MatterID = @PolicyAdminMatterID 
		
		PRINT @LeadIDToUse
	
		SELECT @ResourceListID = dbo.fnGetSimpleDvAsInt (146215,@LeadIDToUse)
	
		PRINT ISNULL(@ResourceListID,'0')
	
		/*Obtain selected VET bank details*/
		SELECT @AccountName	     =	dbo.fnGetSimpleDv(175587, @ResourceListID)  
		SELECT @AccountSortCode  =	dbo.fnGetSimpleDv(175589, @ResourceListID)  	
		SELECT @AccountNumber	 =  dbo.fnGetSimpleDv(175590, @ResourceListID)  
	
		/*Update the VET bank details into standard account fields*/
		EXEC _C00_SimpleValueIntoField  170264, @AccountName,@MatterID,@WhoCreated 
		EXEC _C00_SimpleValueIntoField  170266, @AccountSortCode,@MatterID,@WhoCreated
		EXEC _C00_SimpleValueIntoField  170265, @AccountNumber,@MatterID,@WhoCreated
	 
		/*set use existing bank details to No*/
		EXEC dbo._C00_SimpleValueIntoFieldLuli 175297,'No',@MatterID  
	
	END

	/*(9) Benefit Reached, Approve payment*/
	IF @EventTypeID IN (158607, 121865)
	BEGIN

		/*If the claim if for death of pet or loss then cancel the policy...*/
		IF dbo.fnGetSimpleDvAsINT(144504, @MatterID) IN (
			147037 /*Death of Pet or Loss*/,
			147059 /*Loss by theft or straying*/
			)
		BEGIN

			SELECT @PALeadID = r.FromLeadID
			FROM LeadtypeRelationship r WITH (NOLOCK)
			WHERE r.ToMatterID = @MatterID

			SELECT @PACaseID = c.CaseID
			FROM Cases c WITH (NOLOCK)
			WHERE c.LeadID = @PALeadID

			SELECT @PAMatterID = m.MatterID
			FROM Matter m WITH (NOLOCK)
			WHERE m.CaseID = @PACaseID

			SELECT @DateOfLoss = dbo.fnGetSimpleDv(144892,@MatterID)

			SELECT @DateOfLoss, @PAMatterID

			EXEC dbo._C00_SimpleValueIntoField 170055, @DateOfLoss, @PAMatterID, @WhoCreated 
			EXEC dbo._C00_SimpleValueIntoField 175452, @DateOfLoss, @PAMatterID, @WhoCreated 
			EXEC dbo._C00_SimpleValueIntoField 177026, @DateOfLoss, @PAMatterID, @WhoCreated 

			SELECT @DateForEvent = dbo.fn_GetDate_Local(), 
					@CommentsToApply = 'Added from approve payment/(9) Benefit Reached on claim matter ' + CONVERT(VARCHAR(100),@MatterID)

			EXEC dbo.EventTypeAutomatedEvent__CreateAutomatedEventQueueFull @ClientID, @PALeadID, @PACaseID, @LeadEventID, @EventTypeID, 156748, -1, @DateForEvent, @WhoCreated, @CommentsToApply

			SELECT @ClientID, @PALeadID, @PACaseID, @LeadEventID, @EventTypeID, 156748 /*Policy cancelled by claims process*/, -1, @DateForEvent, @WhoCreated, @CommentsToApply

		END

	END
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SAE_1272] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_SAE_1272] TO [sp_executeall]
GO
GRANT ALTER ON  [dbo].[_C00_SAE_1272] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_SAE_1272] TO [sp_executeall]
GO
