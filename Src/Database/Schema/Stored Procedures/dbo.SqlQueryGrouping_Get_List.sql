SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SqlQueryGrouping table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlQueryGrouping_Get_List]

AS


				
				SELECT
					[SqlQueryGroupingID],
					[ClientID],
					[SqlQueryID],
					[GroupByClause],
					[HavingClause],
					[HavingColumnID],
					[HavingCriteria1],
					[HavingCriteria2],
					[TempHavingColumnID],
					[TempHavingID],
					[TempGroupingID]
				FROM
					[dbo].[SqlQueryGrouping] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryGrouping_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlQueryGrouping_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlQueryGrouping_Get_List] TO [sp_executeall]
GO
