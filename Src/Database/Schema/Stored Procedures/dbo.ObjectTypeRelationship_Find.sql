SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the ObjectTypeRelationship table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[ObjectTypeRelationship_Find]
(

	@SearchUsingOR bit   = null ,

	@ObjectTypeRelationshipID int   = null ,

	@FromObjectTypeID int   = null ,

	@ToObjectTypeID int   = null ,

	@RelationshipTypeID int   = null ,

	@RelationshipName varchar (50)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [ObjectTypeRelationshipID]
	, [FromObjectTypeID]
	, [ToObjectTypeID]
	, [RelationshipTypeID]
	, [RelationshipName]
    FROM
	[dbo].[ObjectTypeRelationship] WITH (NOLOCK) 
    WHERE 
	 ([ObjectTypeRelationshipID] = @ObjectTypeRelationshipID OR @ObjectTypeRelationshipID IS NULL)
	AND ([FromObjectTypeID] = @FromObjectTypeID OR @FromObjectTypeID IS NULL)
	AND ([ToObjectTypeID] = @ToObjectTypeID OR @ToObjectTypeID IS NULL)
	AND ([RelationshipTypeID] = @RelationshipTypeID OR @RelationshipTypeID IS NULL)
	AND ([RelationshipName] = @RelationshipName OR @RelationshipName IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [ObjectTypeRelationshipID]
	, [FromObjectTypeID]
	, [ToObjectTypeID]
	, [RelationshipTypeID]
	, [RelationshipName]
    FROM
	[dbo].[ObjectTypeRelationship] WITH (NOLOCK) 
    WHERE 
	 ([ObjectTypeRelationshipID] = @ObjectTypeRelationshipID AND @ObjectTypeRelationshipID is not null)
	OR ([FromObjectTypeID] = @FromObjectTypeID AND @FromObjectTypeID is not null)
	OR ([ToObjectTypeID] = @ToObjectTypeID AND @ToObjectTypeID is not null)
	OR ([RelationshipTypeID] = @RelationshipTypeID AND @RelationshipTypeID is not null)
	OR ([RelationshipName] = @RelationshipName AND @RelationshipName is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectTypeRelationship_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ObjectTypeRelationship_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ObjectTypeRelationship_Find] TO [sp_executeall]
GO
