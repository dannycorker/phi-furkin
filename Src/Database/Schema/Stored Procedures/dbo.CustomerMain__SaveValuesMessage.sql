SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-05-24
-- Description:	Return some nice message on clicking save all values in CustomerMain
-- =============================================
CREATE PROCEDURE [dbo].[CustomerMain__SaveValuesMessage]
@ClientID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @OutputMessage VARCHAR(2000)

	IF @ClientID = 239
	BEGIN
	
		SELECT @OutputMessage = 'Are you sure you want to save the details and send the pack out?'
	
	END
	ELSE
	BEGIN
		SELECT @OutputMessage = 'Are you sure you want to save the details?'
	END
	
	SELECT @OutputMessage

END


GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerMain__SaveValuesMessage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[CustomerMain__SaveValuesMessage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[CustomerMain__SaveValuesMessage] TO [sp_executeall]
GO
