SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date:	2019-11-26
-- Description:	Pops a request from the Liberty Mediate API queue.
--				No parameter validation is performed as this proc is only ever called from C# so parameters 
--				are assumed to be correct - so no time wasted by validation.
--
-- 2020-01-03 MAB Added header comments
-- 2020-01-06 MAB TRY..CATCH block and @ErrorCode and @ErrorMessage parameters added for error handling
-- 2020-01-07 MAB modified Errors array in JSON response to a single item and renamed to Error
-- =============================================
CREATE PROCEDURE [dbo].[_C00_Mediate_Pop]
(
     @AppKey         NVARCHAR(50)
    ,@Queue          NVARCHAR(50)
    ,@Flow           NVARCHAR(50)
    ,@Action         NVARCHAR(50)
    ,@Method         NVARCHAR(50)
	,@ObjectID		 NVARCHAR(MAX)
    ,@RequestBody    NVARCHAR(MAX)
    ,@RequestType    NVARCHAR(50)
    ,@ResponseBody   NVARCHAR(MAX)
    ,@ResponseType   NVARCHAR(50)
    ,@Exception      NVARCHAR(2000)		= ''
    ,@ExceptionCount INT				= 0
    ,@QueueID        BIGINT				= 0
	,@ParentObjectID NVARCHAR(MAX)	= NULL
) AS
BEGIN
 
	SET NOCOUNT ON

	DECLARE
		@NewObjectID INT
		,@UserID INT = 76488 /*Claims API User*/
		,@XmlRequest XML
		,@JsonResponse NVARCHAR(MAX) = N'{}'
		,@ErrorCode NVARCHAR(10)
		,@ErrorMessage VARCHAR(2000) = NULL
		,@LogEntry VARCHAR(2000) = 'QueueID = ' + ISNULL(CONVERT(VARCHAR,@QueueID),'NULL')

	EXEC _C00_Logit 'Info','_C00_Mediate_Pop',@Action,@LogEntry,@UserID

	IF @Queue = N'In' 
	BEGIN
		BEGIN TRY
			SELECT @XmlRequest = CAST(@RequestBody AS XML)

			IF @RequestType = N'CustomerRequest'
			BEGIN
				EXEC @NewObjectID = dbo._C600_API_Receive_Customer	@XmlRequest, @Method, @ObjectID, @ParentObjectID, @UserID, @JsonResponse OUTPUT
			END
			ELSE
			IF @RequestType = N'PetRequest'
			BEGIN
				EXEC @NewObjectID = dbo._C600_API_Receive_Pet		@XmlRequest, @Method, @ObjectID, @ParentObjectID, @UserID, @JsonResponse OUTPUT
			END
			ELSE
			IF @RequestType = N'PolicyRequest'
			BEGIN
				EXEC @NewObjectID = dbo._C600_API_Receive_Policy	@XmlRequest, @Method, @ObjectID, @ParentObjectID, @UserID, @JsonResponse OUTPUT
			END
			ELSE
			IF @RequestType = N'ClaimRequest'
			BEGIN
				EXEC @NewObjectID = dbo._C600_API_Receive_Claim		@XmlRequest, @Method, @ObjectID, @ParentObjectID, @UserID, @JsonResponse OUTPUT
			END
		END TRY

		BEGIN CATCH
			SELECT @ErrorCode = CONVERT(NVARCHAR(10), ERROR_NUMBER())
				,@ErrorMessage = ERROR_PROCEDURE() + ': line ' + CONVERT(NVARCHAR(10), ERROR_LINE()) + ': ' + ERROR_MESSAGE()
				,@JsonResponse = N'{"Error":{"Code":' + @ErrorCode + N',"Message":"' + @ErrorMessage + N'"}}'

			UPDATE [dbo].[_C00_Mediate_Queue]
			SET     [Action] = @Action,
					[ModifyDate] = dbo.fn_GetDate_Local(),
					[ResponseBody] = @JsonResponse,
					[ExceptionCount] += 1,
					[Exception] = @ErrorMessage
			OUTPUT inserted.*
			WHERE [QueueID] = @QueueID

			RETURN
		END CATCH

		UPDATE [dbo].[_C00_Mediate_Queue]
		SET     [Action] = @Action,
				[ModifyDate] = dbo.fn_GetDate_Local(),
				[ResponseBody] = @JsonResponse
		OUTPUT inserted.*
		WHERE [QueueID] = @QueueID
	END
	ELSE
	BEGIN
		UPDATE [dbo].[_C00_Mediate_Queue]
		SET     [Action] = @Action,
				[ModifyDate] = dbo.fn_GetDate_Local()
		OUTPUT inserted.*
		WHERE [QueueID] = @QueueID
	END

	EXEC _C00_Logit 'Info','_C00_Mediate_Pop',@Action,@LogEntry,@UserID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Mediate_Pop] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_Mediate_Pop] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_Mediate_Pop] TO [sp_executeall]
GO
