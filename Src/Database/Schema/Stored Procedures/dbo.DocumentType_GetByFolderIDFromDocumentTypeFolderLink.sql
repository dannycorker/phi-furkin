SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ben Crinion
-- Create date: 2008-09-16
-- Description:	Get a list of documents for a specific folder
--				JWG 2014-01-31 Added new fields from SourceID to ThunderheadDocumentFormat
--				NG/PR Added CreateNewVersionWhenSaved Column 2020/01/08
-- =============================================
CREATE PROCEDURE [dbo].[DocumentType_GetByFolderIDFromDocumentTypeFolderLink]
(
	@FolderID int   
)
AS
BEGIN

	SELECT dt.DocumentTypeID,
	dt.ClientID,
	dt.LeadTypeID,
	dt.DocumentTypeName,
	dt.DocumentTypeDescription,
	NULL AS Header,
	NULL AS Template,
	NULL AS Footer,
	dt.CanBeAutoSent,
	dt.EmailSubject,
	NULL AS EmailBodyText,
	dt.InputFormat,
	dt.OutputFormat,
	dt.Enabled,
	dt.RecipientsTo,
	dt.RecipientsCC,
	dt.RecipientsBCC,
	dt.ReadOnlyTo,
	dt.ReadOnlyCC,
	dt.ReadOnlyBCC,
	dt.SendToMultipleRecipients,
	dt.MultipleRecipientDataSourceType,
	dt.MultipleRecipientDataSourceID,
	dt.SendToAllByDefault,
	dt.ExcelTemplatePath,
	dt.FromDetails,
	dt.ReadOnlyFrom,
	dt.SourceID,
	dt.WhoCreated,
	dt.WhenCreated,
	dt.WhoModified,
	dt.WhenModified,
	dt.FolderID,
	dt.IsThunderheadTemplate,
	dt.ThunderheadUniqueTemplateID,
	dt.ThunderheadDocumentFormat,
	dt.DocumentTitleTemplate,
	dt.CreateNewVersionWhenSaved
	FROM dbo.DocumentType dt WITH (NOLOCK) 
	WHERE EXISTS (
		SELECT *
		FROM dbo.DocumentTypeFolderLink dtfl WITH (NOLOCK) 
		WHERE dtfl.FolderID = @FolderID
		AND dtfl.DocumentTypeID = dt.DocumentTypeID
	)

	SELECT @@ROWCOUNT			
				
END


GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType_GetByFolderIDFromDocumentTypeFolderLink] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DocumentType_GetByFolderIDFromDocumentTypeFolderLink] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DocumentType_GetByFolderIDFromDocumentTypeFolderLink] TO [sp_executeall]
GO
