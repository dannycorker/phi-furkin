SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-02-06
-- Description:	Gets the DataCash payment row
-- =============================================
CREATE PROCEDURE [dbo].[_C600_DataCash_SavePayment]
(
	@TableRowID INT,
	@CreditCardToken VARCHAR(2000)
)
AS
BEGIN

	DECLARE @AqAutomation INT
	
	SELECT @AqAutomation=dbo.fnGetKeyValueAsIntFromThirdPartyIDs (tr.ClientID,53,'CFG|AqAutomationCPID',0) 
	FROM TableRows tr WITH (NOLOCK) 
	WHERE TableRowID=@TableRowID

	EXEC _C00_SimpleValueIntoField 175466, @CreditCardToken, @TableRowID, @AqAutomation
	-- change status to collecting to trigger collection attempt
	EXEC _C00_SimpleValueIntoField 175469, 72149, @TableRowID, @AqAutomation 

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_DataCash_SavePayment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_DataCash_SavePayment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_DataCash_SavePayment] TO [sp_executeall]
GO
