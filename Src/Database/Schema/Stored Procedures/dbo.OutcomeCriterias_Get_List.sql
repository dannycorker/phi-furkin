SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the OutcomeCriterias table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[OutcomeCriterias_Get_List]

AS


				
				SELECT
					[OutcomeCriteriaID],
					[OutcomeID],
					[QuestionPossibleAnswerID],
					[ClientID]
				FROM
					[dbo].[OutcomeCriterias] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeCriterias_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[OutcomeCriterias_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[OutcomeCriterias_Get_List] TO [sp_executeall]
GO
