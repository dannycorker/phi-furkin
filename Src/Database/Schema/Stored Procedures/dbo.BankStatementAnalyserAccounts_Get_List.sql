SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the BankStatementAnalyserAccounts table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[BankStatementAnalyserAccounts_Get_List]

AS


				
				SELECT
					[BankStatementAnalyserAccountID],
					[ClientID],
					[BankStatementPageCount],
					[CostPerPage],
					[LastResetDate]
				FROM
					[dbo].[BankStatementAnalyserAccounts] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[BankStatementAnalyserAccounts_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[BankStatementAnalyserAccounts_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[BankStatementAnalyserAccounts_Get_List] TO [sp_executeall]
GO
