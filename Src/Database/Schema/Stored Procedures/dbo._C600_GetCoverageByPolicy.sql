SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2020-08-16
-- Description:	JIRA PPET-156 | Return data from the historical policy table for a matter with the option of filtering to a given date
-- =============================================
CREATE PROCEDURE [dbo].[_C600_GetCoverageByPolicy]
	 @MatterID		INT
	,@FilterToDate	DATE = NULL
AS
BEGIN

	SET NOCOUNT ON;

    DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()

	SELECT	 m.MatterID 
			,tdvSta.ValueDate		[CoverStartDate]
			,tdvEnd.ValueDate		[CoverEndDate]
			,tdvExs.ValueMoney		[Deductible]
			,tdvCop.ValueMoney		[Copay]
			,rdv.DetailValue		[Product]
			,0.00					[ExamFee]	-- Future Dev. Need to establish whether or not this will appear on the historical policy table
			,'none'					[Wellness]	-- Future Dev. Need to establish whether or not this will appear on the historical policy table
	FROM Matter m WITH (NOLOCK)
	INNER JOIN TableRows tr WITH (NOLOCK) on tr.MatterID = m.MatterID AND tr.DetailFieldID = 170033 /*Historical Policy*/
	INNER JOIN TableDetailValues tdvPrd WITH (NOLOCK) on tdvPrd.TableRowID = tr.TableRowID AND tdvPrd.DetailFieldID = 145665 /*Policy*/
	INNER JOIN ResourceListDetailValues rdv WITH (NOLOCK) on rdv.ResourceListID = tdvPrd.ResourceListID AND rdv.DetailFieldID = 146200 /*Product*/
	INNER JOIN TableDetailValues tdvSta WITH (NOLOCK) on tdvSta.TableRowID = tr.TableRowID AND tdvSta.DetailFieldID = 145663 /*Start*/
	 LEFT JOIN TableDetailValues tdvEnd WITH (NOLOCK) on tdvEnd.TableRowID = tr.TableRowID AND tdvEnd.DetailFieldID = 145664 /*End*/
	 LEFT JOIN TableDetailValues tdvCop WITH (NOLOCK) on tdvCop.TableRowID = tr.TableRowID AND tdvCop.DetailFieldID = 177415 /*Co Pay Rate*/
	 LEFT JOIN TableDetailValues tdvExs WITH (NOLOCK) on tdvExs.TableRowID = tr.TableRowID AND tdvExs.DetailFieldID = 177416 /*Excess*/
	WHERE m.MatterID = @MatterID
	AND m.ClientID = @ClientID
	AND (	
			(@FilterToDate is NULL)
		 OR  
			(@FilterToDate BETWEEN tdvSta.ValueDate AND tdvEnd.ValueDate)
		 OR
			(@FilterToDate > tdvSta.ValueDate AND tdvEnd.ValueDate is NULL)
		)
	ORDER BY tdvSta.ValueDate ASC

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetCoverageByPolicy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_GetCoverageByPolicy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetCoverageByPolicy] TO [sp_executeall]
GO
