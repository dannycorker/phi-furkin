SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		AQUARIUM
-- Create date: 2020-02-03
-- Description:	Sql After Event - PH match confirmed
-- =============================================
CREATE PROCEDURE [dbo].[_SAE_NewBusiness_155201]
	@LeadEventID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE /* Standard */
			 @LeadID						INT		
			,@EventTypeID					INT
			,@CustomerID					INT
			,@ClientID						INT
			,@CaseID						INT
			,@WhoCreated					INT
			,@MatterID						INT
			,@WhenCreated					DATE
			,@AqAutomation					INT
			,@LeadTypeID					INT
			,@ErrorMessage					VARCHAR(2000)
			/* Specific to procedure */
			,@UseExistingAccount			INT
			,@NewCustomerID					INT
			,@NewMatterID					INT
			,@NewCaseID						INT
			,@WaitingDays					INT


/* ===================================================================================================================== */

			/*Select and assign Aquarium IDs using the LeadEventID*/
			SELECT TOP (1) 
			 @LeadID		= le.LeadID
			,@EventTypeID	= le.EventTypeID
			,@CustomerID	= l.CustomerID
			,@ClientID		= l.ClientID
			,@CaseID		= le.CaseID
			,@WhoCreated	= le.WhoCreated
			,@MatterID		= m.MatterID
			,@WhenCreated	= le.WhenCreated
			,@AqAutomation	= dbo.fnGetKeyValueAsIntFromThirdPartyIDs (l.ClientID,53,'CFG|AqAutomationCPID',0)
			,@LeadTypeID	= l.LeadTypeID
			FROM dbo.LeadEvent le WITH (NOLOCK)
			INNER JOIN dbo.Lead l WITH ( NOLOCK ) ON l.LeadID = le.LeadID
			INNER JOIN dbo.Matter m WITH ( NOLOCK ) ON m.CaseID = le.CaseID	
			WHERE le.LeadEventID = @LeadEventID		
	
/* ===================================================================================================================== */


/* PH match confirmed */
	IF @EventTypeID IN (155201)
	BEGIN
		
		/* 
		
			if a match has been made the Customer ID field must have been completed & must match a 600 non-test customer
			
			if collections matter id has been completed it must match a collections matter for the customer identified in Customer ID
			
			if a collections matterID has been completed it must be for the same interval i.e. both annual or both monthly 
			& the date first or next collection must be at least wait period in advnace.
		
		*/
		
		SELECT @NewCustomerID = dbo.fnGetDvAsInt(175285,@CaseID) -- new CustomerID
		SELECT @NewMatterID = dbo.fnGetDvAsInt(175279,@CaseID) -- collection MatterID
		
		-- dcm 2015-03-16 ticket #31658
		IF EXISTS ( SELECT * FROM Customers c WITH (NOLOCK) 
					INNER JOIN Lead l WITH (NOLOCK) ON l.CustomerID=c.CustomerID AND LeadTypeID=1492
					WHERE c.CustomerID=@NewCustomerID AND c.Test=0 AND c.ClientID=@ClientID )
		BEGIN

			IF @NewMatterID <> 0 AND
				NOT EXISTS ( SELECT * FROM Matter m WITH (NOLOCK) 
							INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=m.LeadID AND LeadTypeID=1493
							WHERE m.CustomerID=@NewCustomerID AND m.MatterID=@NewMatterID )
			BEGIN

				SELECT @ErrorMessage = ISNULL(@ErrorMessage,'') + '<br /><br /><font color="red">Chosen collections matter does not belong to the customer specified.</font><br/><br/><font color="#edf4fa">'
				RAISERROR(@ErrorMessage,16,1)

			END
			ELSE
			BEGIN
			
				SELECT @NewCaseID=CaseID FROM Matter WITH (NOLOCK) WHERE MatterID=@NewMatterID
				
				SELECT @WaitingDays=ValueInt FROM ClientDetailValues WITH (NOLOCK) WHERE ClientID=@ClientID AND DetailFieldID=170228
				IF dbo.fnGetDvAsDate(170185,@NewCaseID) < ( SELECT CAST(DATEADD(DAY,@WaitingDays,dbo.fn_GetDate_Local()) AS DATE) )
				BEGIN
				
					SELECT @ErrorMessage = ISNULL(@ErrorMessage,'') + '<br /><br /><font color="red">Collection date of next payment is less than 10 days away.</font><br/><br/><font color="#edf4fa">'
					RAISERROR(@ErrorMessage,16,1)
				
				END
			
			END

		END
		ELSE
		BEGIN
		
			SELECT @ErrorMessage = '<br /><br /><font color="red">Chosen customer does not exist (invalid CustomerID) or is a test Customer.</font><br/><br /><font color="#edf4fa">'
			RAISERROR(@ErrorMessage,16,1)
		
		END
		
	END

	PRINT OBJECT_NAME(@@ProcID) + ' END'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_NewBusiness_155201] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_SAE_NewBusiness_155201] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_SAE_NewBusiness_155201] TO [sp_executeall]
GO
