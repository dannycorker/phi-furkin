SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[ThirdPartyFieldMapping__GetByLeadTypeIDAndThirdPartyFieldGroupID]
(
	@LeadTypeID INT,
	@ThirdPartyFieldGroupID INT
)
AS


	SET ANSI_NULLS ON
	
	SELECT *					
	FROM
		[dbo].[ThirdPartyFieldMapping] WITH (NOLOCK) 
	WHERE
		[LeadTypeID] = @LeadTypeID AND
		[ThirdPartyFieldGroupID] = @ThirdPartyFieldGroupID
	
	SELECT @@ROWCOUNT
			



GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping__GetByLeadTypeIDAndThirdPartyFieldGroupID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[ThirdPartyFieldMapping__GetByLeadTypeIDAndThirdPartyFieldGroupID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[ThirdPartyFieldMapping__GetByLeadTypeIDAndThirdPartyFieldGroupID] TO [sp_executeall]
GO
