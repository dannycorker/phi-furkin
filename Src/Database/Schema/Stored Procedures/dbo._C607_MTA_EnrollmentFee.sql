SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2021-01-28
-- Description:	Check and fix enrollment fee
-- =============================================
CREATE PROCEDURE [dbo].[_C607_MTA_EnrollmentFee]
	@CustomerID INT,
	@LeadID INT,
	@MatterID INT,
	@PurchasedProductID INT,
	@County VARCHAR(10),
	@AccountID INT,
	@ClientAccountID INT,
	@WhoCreated INT
AS
BEGIN

	SET NOCOUNT ON;

	/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
	DECLARE @ClientID INT = 607,
			@BrandID INT = dbo.fnGetSimpleDvAsint(170144, @CustomerID),
			@ProvinceLookupListItemID INT,
			@PetIndex INT,
			@StartDate DATE,
			@CoverTo DATE,
			@FeeTableRowID INT, 
			@EnrollmentFee NUMERIC(18,2), 
			@TaxTableRowID INT, 
			@Tax NUMERIC(18,2), 
			@Total NUMERIC(18,2),
			@LoggedEnrollmentFee NUMERIC(18,2), 
			@LoggedTax NUMERIC(18,2), 
			@LoggedTotal NUMERIC(18,2),
			@EnrollmentFeeDiff NUMERIC(18,2), 
			@EnrollmentFeeTaxDiff NUMERIC(18,2), 
			@EnrollmentFeeTotalDiff NUMERIC(18,2),
			@PurchasedProductPaymentScheduleID INT,
			@XmlLog XML

	DECLARE @Policies TABLE (LeadID INT, PolicyNumber INT)

	/*Check 1, are we in year 1?*/
	IF NOT EXISTS (
		SELECT *
		FROM PurchasedProduct pp WITH (NOLOCK)
		WHERE pp.ObjectID = @MatterID
		AND pp.PurchasedProductID < @PurchasedProductID
	)
	BEGIN

		/*We are in year 1, we only apply enrollment fee in year 1. Continue...*/

		/*Get the start and end dates*/
		SELECT @StartDate = pp.ValidFrom, @CoverTo = pp.ValidTo
		FROM PurchasedProduct pp WITH (NOLOCK)
		WHERE pp.PurchasedProductID = @PurchasedProductID

		/*Get province Luli*/
		SELECT @ProvinceLookupListItemID = CASE @County 
				WHEN 'AB' THEN 61739
				WHEN 'BC' THEN 61740
				WHEN 'MB' THEN 61741
				WHEN 'NB' THEN 61742
				WHEN 'NL' THEN 61743
				WHEN 'NS' THEN 61744
				WHEN 'NT' THEN 61745
				WHEN 'NU' THEN 61746
				WHEN 'ON' THEN 61747
				WHEN 'PE' THEN 61748
				WHEN 'QC' THEN 61749
				WHEN 'SK' THEN 61750
				WHEN 'YT' THEN 61751
		END

		/*Work out pet number*/
		INSERT INTO @Policies (LeadID, PolicyNumber)
		SELECT l.LeadID, ROW_NUMBER() OVER (PARTITION BY (SELECT 1) ORDER BY l.LeadID)
		FROM Lead l WITH (NOLOCK)
		WHERE l.CustomerID = @CustomerID
		AND l.leadTypeID = 1492

		SELECT @Petindex = p.PolicyNumber
		FROM @Policies p
		WHERE p.LeadID = @LeadID

		/*Final check. Is the amount in the PPPS for enrollment fee correct for this MTA/State/Province..?*/
		SELECT @FeeTableRowID = FeeTableRowID, 
				@EnrollmentFee = EnrollmentFee, 
				@TaxTableRowID = TaxTableRowID, 
				@Tax = Tax, 
				@Total = Total
		FROM dbo.fn_C600_GetEnrollmentFee(@BrandID, @CustomerID, @ProvinceLookupListItemID, @PetIndex, @StartDate)

		/*Get the enrollment fee(s) for whats been or going to be paid*/
		SELECT @LoggedEnrollmentFee = SUM(ppps.PaymentNet), @LoggedTax = SUM(ppps.PaymentVAT), @LoggedTotal = SUM(ppps.PaymentGross)
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
		WHERE ppps.PurchasedProductID = @PurchasedProductID
		AND ppps.PaymentStatusID IN (1, 2, 6) /*New, Processed, Paid*/
		AND ppps.PurchasedProductPaymentScheduleTypeID = 5 /*Admin Fee*/

		/*Is the amount logged the same as what we need?*/
		IF @LoggedTotal <> @Total
		BEGIN

			/*Work out the differences*/
			SELECT @EnrollmentFeeDiff = @EnrollmentFee - @LoggedEnrollmentFee,
					@EnrollmentFeeTaxDiff = @Tax - @LoggedTax,
					@EnrollmentFeeTotalDiff = @Total - @LoggedTotal

			/*Create a payment row for the differences*/
			INSERT INTO PurchasedProductPaymentSchedule(ClientID, CustomerID, AccountID, PurchasedProductID, ActualCollectionDate, CoverFrom, CoverTo, PaymentDate, PaymentNet, PaymentVAT, PaymentGross, PaymentStatusID, CustomerLedgerID, ReconciledDate, CustomerPaymentScheduleID, WhoCreated, WhenCreated, ClientAccountID, TransactionFee, PurchasedProductPaymentScheduleTypeID)
			VALUES (@ClientID,@CustomerID,@AccountID,@PurchasedProductID, dbo.fn_GetDate_Local(), @StartDate, @CoverTo, dbo.fn_GetDate_Local(), @EnrollmentFeeDiff, @EnrollmentFeeTaxDiff, @EnrollmentFeeTotalDiff, 1, NULL, NULL, NULL, @WhoCreated, dbo.fn_GetDate_Local(),@ClientAccountID, 0.00, 5)

			SELECT @PurchasedProductPaymentScheduleID = SCOPE_IDENTITY()

			/*Set the parent ID back*/
			UPDATE ppps
			SET PurchasedProductPaymentScheduleParentID = PurchasedProductPaymentScheduleID
			FROM PurchasedProductPaymentSchedule ppps
			WHERE ppps.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

			/*Save enrollment fee details to matter detail values*/
			EXEC dbo._C00_SimpleValueIntoField 315877, @FeeTableRowID, @MatterID, @WhoCreated -- FeeTableRowID
			EXEC dbo._C00_SimpleValueIntoField 315878, @EnrollmentFee, @MatterID, @WhoCreated -- EnrollmentFee
			EXEC dbo._C00_SimpleValueIntoField 315879, @TaxTableRowID, @MatterID, @WhoCreated -- TaxTableRowID
			EXEC dbo._C00_SimpleValueIntoField 315880, @Tax, @MatterID, @WhoCreated -- Taxes
			EXEC dbo._C00_SimpleValueIntoField 315881, @Total, @MatterID, @WhoCreated -- Total

		END

		SELECT @XmlLog = (
			SELECT @BrandID AS [BrandID],
					@CustomerID AS [CustomerID],
					@StartDate AS [StartDate],
					@CoverTo AS [CoverTo],
					@County AS [County],
					@ProvinceLookupListItemID AS [ProvinceLookupListItemID],
					@Petindex AS [Petindex],
					@FeeTableRowID AS [FeeTableRowID],
					@EnrollmentFee AS [EnrollmentFee],
					@TaxTableRowID AS [TaxTableRowID],
					@Tax AS [Tax],
					@Total AS [Total],
					@LoggedEnrollmentFee AS [LoggedEnrollmentFee],
					@LoggedTax AS [LoggedTax],
					@LoggedTotal AS [LoggedTotal],
					@EnrollmentFeeDiff AS [EnrollmentFeeDiff],
					@EnrollmentFeeTaxDiff AS [EnrollmentFeeTaxDiff],
					@EnrollmentFeeTotalDiff AS [EnrollmentFeeTotalDiff]
			FOR XML PATH ('_C607_MTA_EnrollmentFee')
		)

		EXEC dbo._C00_LogItXML 607, @MatterID, '_C607_MTA_EnrollmentFee', @XmlLog

	END

	/*End add enrollment fee*/

END
GO
GRANT EXECUTE ON  [dbo].[_C607_MTA_EnrollmentFee] TO [sp_executeall]
GO
