SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the MasterQuestionStatus table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[MasterQuestionStatus_Find]
(

	@SearchUsingOR bit   = null ,

	@MasterQuestionStatusID int   = null ,

	@MasterQuestionStatusName varchar (50)  = null ,

	@MasterQuestonStatusDescription varchar (250)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [MasterQuestionStatusID]
	, [MasterQuestionStatusName]
	, [MasterQuestonStatusDescription]
    FROM
	[dbo].[MasterQuestionStatus] WITH (NOLOCK) 
    WHERE 
	 ([MasterQuestionStatusID] = @MasterQuestionStatusID OR @MasterQuestionStatusID IS NULL)
	AND ([MasterQuestionStatusName] = @MasterQuestionStatusName OR @MasterQuestionStatusName IS NULL)
	AND ([MasterQuestonStatusDescription] = @MasterQuestonStatusDescription OR @MasterQuestonStatusDescription IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [MasterQuestionStatusID]
	, [MasterQuestionStatusName]
	, [MasterQuestonStatusDescription]
    FROM
	[dbo].[MasterQuestionStatus] WITH (NOLOCK) 
    WHERE 
	 ([MasterQuestionStatusID] = @MasterQuestionStatusID AND @MasterQuestionStatusID is not null)
	OR ([MasterQuestionStatusName] = @MasterQuestionStatusName AND @MasterQuestionStatusName is not null)
	OR ([MasterQuestonStatusDescription] = @MasterQuestonStatusDescription AND @MasterQuestonStatusDescription is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestionStatus_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MasterQuestionStatus_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MasterQuestionStatus_Find] TO [sp_executeall]
GO
