SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2020-01-31
-- Description:	Insert into PurchasedProductPaymentScheduleDetail helper
-- 2020-03-18 GPR for AAG-512	| Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- 2020-03-20 GPR | Updated tax related column names for AAG-507
-- =============================================
CREATE PROCEDURE [dbo].[PurchasedProductPaymentScheduleDetail__InsertHelper]

	@ClientID INT,
    @CustomerID INT,
    @LeadID INT,
    @MatterID INT,
    @PurchasedProductID INT,
    @RatingDateTime DATETIME,
    @PremiumCalculationID INT,
    @ValidFrom DATE,
    @ValidTo DATE,
    @AdjustmentTypeID INT,
    @Active BIT,
    @AdjustmentRequestDate DATE,
    @AnnualPriceForRiskGross NUMERIC(18,2),
    @AnnualPriceForRiskNET NUMERIC(18,2),
    @AnnualPriceForRiskNationalTax NUMERIC(18,2),
    @AnnualAdminFee NUMERIC(18,2),
    @AnnualDiscountCommissionSacrifice NUMERIC(18,2),
    @AnnualDiscountPremiumAffecting NUMERIC(18,2),
    @AdjustmentValueGross MONEY,
    @AdjustmentValueNET MONEY,
    @AdjustmentValueNationalTax MONEY,
    @AdjustmentAdminFee MONEY,
    @AdjustmentDiscountCommissionSacrifice MONEY,
    @AdjustmentDiscountCommissionPremium MONEY,
    @AdjustmentBrokerCommission NUMERIC(18,2),
    @AdjustmentUnderwriterCommission NUMERIC(18,2),
    @AdjustmentSourcecommission NUMERIC(18,2),
    @CancellationReason VARCHAR(100),
    @AdditionalDetailXML XML,
    @Comments VARCHAR(2000),
    @WhoCreated INT,
    @WhenCreated DATETIME,
    @SourceID int

AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO WrittenPremium
           ([ClientID],
           [CustomerID],
           [LeadID],
           [MatterID],
           [PurchasedProductID],
           [RatingDateTime],
           [PremiumCalculationID],
           [ValidFrom],
           [ValidTo],
           [AdjustmentTypeID],
           [Active],
           [AdjustmentRequestDate],
           [AnnualPriceForRiskGross],
           [AnnualPriceForRiskNET],
           [AnnualPriceForRiskNationalTax],
           [AnnualAdminFee],
           [AnnualDiscountCommissionSacrifice],
           [AnnualDiscountPremiumAffecting],
           [AdjustmentValueGross],
           [AdjustmentValueNET],
           [AdjustmentValueNationalTax],
           [AdjustmentAdminFee],
           [AdjustmentDiscountCommissionSacrifice],
           [AdjustmentDiscountCommissionPremium],
           [AdjustmentBrokerCommission],
           [AdjustmentUnderwriterCommission],
           [AdjustmentSourcecommission],
           [CancellationReason],
           [AdditionalDetailXML],
           [Comments],
           [WhoCreated],
           [WhenCreated],
           [WhoChanged],
           [WhenChanged],
           [SourceID])
     VALUES (
			@ClientID,
			@CustomerID,
			@LeadID,
			@MatterID,
			@PurchasedProductID,
			@RatingDateTime,
			@PremiumCalculationID,
			@ValidFrom,
			@ValidTo,
			@AdjustmentTypeID,
			@Active,
			@AdjustmentRequestDate,
			@AnnualPriceForRiskGross,
			@AnnualPriceForRiskNET,
			@AnnualPriceForRiskNationalTax,
			@AnnualAdminFee,
			@AnnualDiscountCommissionSacrifice,
			@AnnualDiscountPremiumAffecting,
			@AdjustmentValueGross,
			@AdjustmentValueNET,
			@AdjustmentValueNationalTax,
			@AdjustmentAdminFee,
			@AdjustmentDiscountCommissionSacrifice,
			@AdjustmentDiscountCommissionPremium,
			@AdjustmentBrokerCommission,
			@AdjustmentUnderwriterCommission,
			@AdjustmentSourcecommission,
			@CancellationReason,
			@AdditionalDetailXML,
			@Comments,
			@WhoCreated,
			@WhenCreated,
			@WhoCreated,
			@WhenCreated,
			@SourceID
	 )

END
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProductPaymentScheduleDetail__InsertHelper] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[PurchasedProductPaymentScheduleDetail__InsertHelper] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[PurchasedProductPaymentScheduleDetail__InsertHelper] TO [sp_executeall]
GO
