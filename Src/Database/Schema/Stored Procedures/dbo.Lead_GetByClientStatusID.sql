SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Select records from the Lead table through a foreign key
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[Lead_GetByClientStatusID]
(

	@ClientStatusID int   
)
AS


				SET ANSI_NULLS ON
				
				SELECT
					[LeadID],
					[ClientID],
					[LeadRef],
					[CustomerID],
					[LeadTypeID],
					[AquariumStatusID],
					[ClientStatusID],
					[BrandNew],
					[Assigned],
					[AssignedTo],
					[AssignedBy],
					[AssignedDate],
					[RecalculateEquations],
					[Password],
					[Salt],
					[WhenCreated]
				FROM
					[dbo].[Lead] WITH (NOLOCK) 
				WHERE
					[ClientStatusID] = @ClientStatusID
				
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_GetByClientStatusID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[Lead_GetByClientStatusID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[Lead_GetByClientStatusID] TO [sp_executeall]
GO
