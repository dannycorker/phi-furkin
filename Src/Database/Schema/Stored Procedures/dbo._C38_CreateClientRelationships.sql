SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Alex Elger
-- Create date: 28-May-2008
-- Description:	Proc to Create a new client and 
-- =============================================
CREATE PROCEDURE [dbo].[_C38_CreateClientRelationships]
(
@LeadEventID int
)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Declare	@AquariumClientID int,
	@myERROR int,
	@LeadID int,
	@EventTypeID int,
	@CaseID int,
	@ClientRelationshipID int,
	@LastKnownID int,
	@TestID int

	Select
	@AquariumClientID = ldv.DetailValue, 
	@EventTypeID = LeadEvent.EventTypeID, 
	@LeadID = LeadEvent.LeadID,
	@CaseID = LeadEvent.CaseID
	from customers
	inner join lead on lead.customerid = customers.customerid
	inner join leadevent on lead.leadid = leadevent.leadid
	Inner Join LeadDetailValues ldv on ldv.LeadID = leadevent.leadid and ldv.DetailFieldID = 6453
	where leadevent.leadeventid = @LeadEventID 


If @EventTypeID IN (34646,33848)
Begin

	DECLARE @Tablerows TABLE 
	(
		TableRowID int, 
		Created int 
	);

Insert Into @Tablerows(TableRowID,Created)
Select TableRowID, 0
from TableDetailValues
Where DetailFieldID = 36709
And DetailValue IS NULL
And LeadID = @LeadID

select @LastKnownID = 0, @TestID = 0

Select Top 1 @LastKnownID = TableRowID
From @Tablerows 
Where Created = 0

while (@LastKnownID > 0 AND @LastKnownID > @TestID)
	begin

	INSERT INTO ClientRelationship(ClientRelationshipName,OutgoingClientID,ReceivingClientID,OutgoingLeadTypeID,IncomingLeadTypeID,OutgoingEventTypeID,IncomingEventTypeID,Enabled,ClearLeadRefs)
	Select tdv_RelationshipName.Detailvalue,
	@AquariumClientID,
	tdv_ReceivingClientID.DetailValue,
	tdv_OutgoingLeadTypeID.DetailValue,
	tdv_IncomingLeadTypeID.DetailValue,
	tdv_OutgoingEventTypeID.DetailValue,
	tdv_IncomingEventTypeID.DetailValue,
	Case tdv_Enabled.DetailValue WHEN 'true' THEN 1 ELSE 0 END,
	Case tdv_ClearLeadRefs.DetailValue  WHEN 'true' THEN 1 ELSE 0 END
	From tabledetailvalues tdv_RelationshipName
	inner join TableDetailValues tdv_ReceivingClientID ON tdv_ReceivingClientID.TableRowID = @LastKnownID and tdv_ReceivingClientID.DetailFieldID = 36702
	inner join TableDetailValues tdv_OutgoingLeadTypeID ON tdv_OutgoingLeadTypeID.TableRowID = @LastKnownID and tdv_OutgoingLeadTypeID.DetailFieldID = 36703
	inner join TableDetailValues tdv_IncomingLeadTypeID ON tdv_IncomingLeadTypeID.TableRowID = @LastKnownID and tdv_IncomingLeadTypeID.DetailFieldID = 36704
	inner join TableDetailValues tdv_OutgoingEventTypeID ON tdv_OutgoingEventTypeID.TableRowID = @LastKnownID and tdv_OutgoingEventTypeID.DetailFieldID = 36705
	inner join TableDetailValues tdv_IncomingEventTypeID ON tdv_IncomingEventTypeID.TableRowID = @LastKnownID and tdv_IncomingEventTypeID.DetailFieldID = 36706
	inner join TableDetailValues tdv_Enabled ON tdv_Enabled.TableRowID = @LastKnownID and tdv_Enabled.DetailFieldID = 36707
	inner join TableDetailValues tdv_ClearLeadRefs ON tdv_ClearLeadRefs.TableRowID = @LastKnownID and tdv_ClearLeadRefs.DetailFieldID = 36708
	where tdv_RelationshipName.TableRowID = @LastKnownID
	and tdv_RelationshipName.DetailFieldID = 36701

	Select @ClientRelationshipID = SCOPE_IDENTITY() 

	Update TableDetailValues
	Set DetailValue = @ClientRelationshipID 
	Where TableRowID = @LastKnownID
	And DetailFieldID = 36709

	Update @Tablerows 
	Set Created = 1
	Where TableRowID = @LastKnownID

	set @TestID = @LastKnownID

	Select Top 1 @LastKnownID = TableRowID
	From @Tablerows 
	Where Created = 0
	And TableRowID > @LastKnownID

	END

	Update ClientRelationship
	Set ClientRelationshipName = tdv_RelationshipName.Detailvalue,
	OutgoingClientID = @AquariumClientID,
	ReceivingClientID = tdv_ReceivingClientID.DetailValue,
	OutgoingLeadTypeID = tdv_OutgoingLeadTypeID.DetailValue,
	IncomingLeadTypeID = tdv_IncomingLeadTypeID.DetailValue,
	OutgoingEventTypeID = tdv_OutgoingEventTypeID.DetailValue,
	IncomingEventTypeID = tdv_IncomingEventTypeID.DetailValue,
	Enabled = Case tdv_Enabled.DetailValue WHEN 'true' THEN 1 ELSE 0 END,
	ClearLeadRefs = Case tdv_ClearLeadRefs.DetailValue  WHEN 'true' THEN 1 ELSE 0 END
	From ClientRelationship
	Inner Join tabledetailvalues tdv_RelationshipID on tdv_RelationshipID.DetailFieldID = 36709 and tdv_RelationshipID.DetailValue = ClientRelationship.ClientRelationshipID
	inner join TableDetailValues tdv_RelationshipName ON tdv_RelationshipName.TableRowID = tdv_RelationshipID.TableRowID and tdv_RelationshipName.DetailFieldID = 36701
	inner join TableDetailValues tdv_ReceivingClientID ON tdv_ReceivingClientID.TableRowID = tdv_RelationshipID.TableRowID and tdv_ReceivingClientID.DetailFieldID = 36702
	inner join TableDetailValues tdv_OutgoingLeadTypeID ON tdv_OutgoingLeadTypeID.TableRowID = tdv_RelationshipID.TableRowID and tdv_OutgoingLeadTypeID.DetailFieldID = 36703
	inner join TableDetailValues tdv_IncomingLeadTypeID ON tdv_IncomingLeadTypeID.TableRowID = tdv_RelationshipID.TableRowID and tdv_IncomingLeadTypeID.DetailFieldID = 36704
	inner join TableDetailValues tdv_OutgoingEventTypeID ON tdv_OutgoingEventTypeID.TableRowID = tdv_RelationshipID.TableRowID and tdv_OutgoingEventTypeID.DetailFieldID = 36705
	inner join TableDetailValues tdv_IncomingEventTypeID ON tdv_IncomingEventTypeID.TableRowID = tdv_RelationshipID.TableRowID and tdv_IncomingEventTypeID.DetailFieldID = 36706
	inner join TableDetailValues tdv_Enabled ON tdv_Enabled.TableRowID = tdv_RelationshipID.TableRowID and tdv_Enabled.DetailFieldID = 36707
	inner join TableDetailValues tdv_ClearLeadRefs ON tdv_ClearLeadRefs.TableRowID = tdv_RelationshipID.TableRowID and tdv_ClearLeadRefs.DetailFieldID = 36708
	Where ClientRelationship.OutgoingClientID = @AquariumClientID
END


END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_CreateClientRelationships] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C38_CreateClientRelationships] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C38_CreateClientRelationships] TO [sp_executeall]
GO
