SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the SqlFunctionDataType table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[SqlFunctionDataType_Get_List]
AS
SELECT
	[SqlFunctionDataTypeID],
	[DataTypeName],
	[DataTypeDescription],
	[WhenCreated],
	[WhenModified]
FROM
	[dbo].[SqlFunctionDataType]	WITH (NOLOCK)

	




GO
GRANT VIEW DEFINITION ON  [dbo].[SqlFunctionDataType_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[SqlFunctionDataType_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[SqlFunctionDataType_Get_List] TO [sp_executeall]
GO
