SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Deletes a record in the StyleRule table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[StyleRule_Delete]
(

	@StyleRuleID int   
)
AS


				DELETE FROM [dbo].[StyleRule] WITH (ROWLOCK) 
				WHERE
					[StyleRuleID] = @StyleRuleID
			

GO
GRANT VIEW DEFINITION ON  [dbo].[StyleRule_Delete] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[StyleRule_Delete] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[StyleRule_Delete] TO [sp_executeall]
GO
