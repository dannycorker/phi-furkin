SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Gets all records from the DataLoaderMapSection table
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderMapSection_Get_List]

AS


				
				SELECT
					[DataLoaderMapSectionID],
					[ClientID],
					[DataLoaderMapID],
					[DataLoaderObjectTypeID],
					[DataLoaderObjectActionID],
					[DataLoaderSectionLocaterTypeID],
					[DetailFieldSubTypeID],
					[HasAquariumID],
					[NumberOfFooterRowsToSkip],
					[ImportIntoLeadManager],
					[IsFixedLengthSection],
					[FixedLengthNumberOfRows],
					[IsMandatory],
					[IsMultipleAllowedWithinFile],
					[Notes],
					[TableDetailFieldID],
					[ResourceListDetailFieldID],
					[CreatePaymentSchedule]
				FROM
					[dbo].[DataLoaderMapSection] WITH (NOLOCK) 
					
				SELECT @@ROWCOUNT
			

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection_Get_List] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderMapSection_Get_List] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderMapSection_Get_List] TO [sp_executeall]
GO
