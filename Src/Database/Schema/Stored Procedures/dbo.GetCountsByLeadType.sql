SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[GetCountsByLeadType]
(
@ClientID int
)

 AS

SELECT	LeadType.LeadTypeName, Pending.PendingCount, Accepted.AcceptedCount, Dudded.DuddedCount, Closed.ClosedCount, OnHold.OnHoldCount FROM LeadType

LEFT Outer JOIN (
	SELECT    LeadType.LeadTypeName, COUNT(Customers.CustomerID) AS PendingCount
	FROM      LeadType INNER JOIN LeadTypeLink ON LeadType.LeadTypeID = LeadTypeLink.LeadTypeID INNER JOIN Outcomes ON LeadTypeLink.OutcomeID = Outcomes.OutcomeID INNER JOIN Customers INNER JOIN CustomerOutcomes ON Customers.CustomerID = CustomerOutcomes.CustomerID ON Outcomes.OutcomeID = CustomerOutcomes.OutcomeID
	GROUP BY  LeadType.LeadTypeName, Customers.AquariumStatusID
	HAVING    (Customers.AquariumStatusID = 1)
	)
AS Pending ON LeadType.LeadTypeName = Pending.LeadTypeName
LEFT Outer JOIN (
	SELECT    LeadType.LeadTypeName, COUNT(Customers.CustomerID) AS AcceptedCount
	FROM      LeadType INNER JOIN LeadTypeLink ON LeadType.LeadTypeID = LeadTypeLink.LeadTypeID INNER JOIN Outcomes ON LeadTypeLink.OutcomeID = Outcomes.OutcomeID INNER JOIN Customers INNER JOIN CustomerOutcomes ON Customers.CustomerID = CustomerOutcomes.CustomerID ON Outcomes.OutcomeID = CustomerOutcomes.OutcomeID
	GROUP BY  LeadType.LeadTypeName, Customers.AquariumStatusID
	HAVING    (Customers.AquariumStatusID = 2)
	)
AS Accepted ON LeadType.LeadTypeName = Accepted.LeadTypeName
LEFT Outer JOIN (
	SELECT    LeadType.LeadTypeName, COUNT(Customers.CustomerID) AS DuddedCount
	FROM      LeadType INNER JOIN LeadTypeLink ON LeadType.LeadTypeID = LeadTypeLink.LeadTypeID INNER JOIN Outcomes ON LeadTypeLink.OutcomeID = Outcomes.OutcomeID INNER JOIN Customers INNER JOIN CustomerOutcomes ON Customers.CustomerID = CustomerOutcomes.CustomerID ON Outcomes.OutcomeID = CustomerOutcomes.OutcomeID
	GROUP BY  LeadType.LeadTypeName, Customers.AquariumStatusID
	HAVING    (Customers.AquariumStatusID = 3)
	)
AS Dudded ON LeadType.LeadTypeName = Dudded.LeadTypeName
LEFT Outer JOIN (
	SELECT    LeadType.LeadTypeName, COUNT(Customers.CustomerID) AS ClosedCount
	FROM      LeadType INNER JOIN LeadTypeLink ON LeadType.LeadTypeID = LeadTypeLink.LeadTypeID INNER JOIN Outcomes ON LeadTypeLink.OutcomeID = Outcomes.OutcomeID INNER JOIN Customers INNER JOIN CustomerOutcomes ON Customers.CustomerID = CustomerOutcomes.CustomerID ON Outcomes.OutcomeID = CustomerOutcomes.OutcomeID
	GROUP BY  LeadType.LeadTypeName, Customers.AquariumStatusID
	HAVING    (Customers.AquariumStatusID = 4)
	)
AS Closed ON LeadType.LeadTypeName = Closed.LeadTypeName
LEFT Outer JOIN (
	SELECT    LeadType.LeadTypeName, COUNT(Customers.CustomerID) AS OnHoldCount
	FROM      LeadType INNER JOIN LeadTypeLink ON LeadType.LeadTypeID = LeadTypeLink.LeadTypeID INNER JOIN Outcomes ON LeadTypeLink.OutcomeID = Outcomes.OutcomeID INNER JOIN Customers INNER JOIN CustomerOutcomes ON Customers.CustomerID = CustomerOutcomes.CustomerID ON Outcomes.OutcomeID = CustomerOutcomes.OutcomeID
	GROUP BY  LeadType.LeadTypeName, Customers.AquariumStatusID
	HAVING    (Customers.AquariumStatusID = 5)
	)

AS OnHold ON LeadType.LeadTypeName = OnHold.LeadTypeName
WHERE (LeadType.ClientID = @ClientID)
ORDER BY LeadType.LeadTypeName



GO
GRANT VIEW DEFINITION ON  [dbo].[GetCountsByLeadType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetCountsByLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetCountsByLeadType] TO [sp_executeall]
GO
