SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-29
-- Description:	Set Group/User Editing in progress mode
--				Pass 1 to start, 2 to commit, 3 to cancel
-- =============================================
CREATE PROCEDURE [dbo].[GroupEditingSetInProgress]
	@GroupID int,
	@UserID int,
	@WhoIsEditing int,
	@WhatToDo int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE 
	@ClientID INT,
	@GroupName VARCHAR(50), 
	@AccountDisabled BIT

    -- Use the @WhatToDo flag to determine which branch to follow
	IF @WhatToDo = 1
	BEGIN
		IF @UserID > 0
		BEGIN
			IF EXISTS (SELECT * FROM GroupAndUserEditingControl WHERE ClientPersonnelAdminGroupID = @GroupID AND ClientPersonnelID = @UserID)
			BEGIN
				-- We are taking over an existing User session
				UPDATE GroupAndUserEditingControl 
				SET EditStartedAt = dbo.fn_GetDate_Local() 
				WHERE ClientPersonnelAdminGroupID = @GroupID 
				AND ClientPersonnelID = @UserID 
			END
			ELSE
			BEGIN
				-- Set User EditInProgress
				INSERT GroupAndUserEditingControl (ClientPersonnelAdminGroupID, ClientPersonnelID, WhoIsEditing, EditStartedAt)
				VALUES (@GroupID, @UserID, @WhoIsEditing, dbo.fn_GetDate_Local())

				EXEC CopyUserRightsForEditing @GroupID, @UserID
			END
		END
		ELSE
		BEGIN
			IF EXISTS (SELECT * FROM GroupAndUserEditingControl WHERE ClientPersonnelAdminGroupID = @GroupID)
			BEGIN
				-- We are taking over an existing Group session
				UPDATE GroupAndUserEditingControl 
				SET EditStartedAt = dbo.fn_GetDate_Local()
				WHERE ClientPersonnelAdminGroupID = @GroupID
				AND ClientPersonnelID IS NULL 
			END
			ELSE
			BEGIN
				-- Set Group EditInProgress
				INSERT GroupAndUserEditingControl (ClientPersonnelAdminGroupID, ClientPersonnelID, WhoIsEditing, EditStartedAt)
				VALUES (@GroupID, NULL, @WhoIsEditing, dbo.fn_GetDate_Local())

				EXEC CopyGroupRightsForEditing @GroupID
			END
		END
	END
	ELSE
	BEGIN
		-- Clear flags and then commit or cancel changes as appropriate
		-- #45354 Additional audit logging
	    SELECT @ClientID = cp.ClientID, @GroupName = g.GroupName, @AccountDisabled = cpUser.AccountDisabled -- may be null, but that means this is a group not a user being edited, so we don't need it in that case
	    FROM dbo.GroupAndUserEditingControl c WITH (NOLOCK) 
	    INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON cp.ClientPersonnelID = c.WhoIsEditing 
	    INNER JOIN dbo.ClientPersonnelAdminGroups g WITH (NOLOCK) ON g.ClientPersonnelAdminGroupID = c.ClientPersonnelAdminGroupID 
		LEFT JOIN dbo.ClientPersonnel cpUser WITH (NOLOCK) ON cpUser.ClientPersonnelID = @UserID  
		
		IF @UserID > 0
		BEGIN
			DELETE GroupAndUserEditingControl 
			WHERE ClientPersonnelAdminGroupID = @GroupID 
			AND ClientPersonnelID = @UserID
		END
		ELSE
		BEGIN
			DELETE GroupAndUserEditingControl 
			WHERE ClientPersonnelAdminGroupID = @GroupID
			--AND ClientPersonnelID IS NULL : Group and User editing is mutually exclusive, so no need for this line.
		END

		IF @WhatToDo = 2
		BEGIN
		    -- Commit work in progress and clear flags
			IF @UserID > 0
			BEGIN
				-- #45354 Additional audit logging
				EXEC dbo.AuditClientPersonnel__Insert @ClientPersonnelID = @UserID, @ClientID = @ClientID, @WhoModified = @WhoIsEditing, @IsInsert = 0, @IsDisabled = @AccountDisabled, @NewClientPersonnelAdminGroupID = @GroupID, @ChangeNotes = 'GroupEditingSetInProgress proc. User rights commit'
				
				EXEC CommitUserRightsForEditing @GroupID, @UserID
			END
			ELSE
			BEGIN
				-- #45354 Additional audit logging
				EXEC dbo.AuditClientPersonnelAdminGroup__Insert @ClientPersonnelAdminGroupID = @GroupID, @ClientID = @ClientID, @WhoModified = @WhoIsEditing , @IsInsert = 0 , @GroupName = @GroupName, @ChangeNotes = 'GroupEditingSetInProgress proc. Group rights commit'
				
				EXEC CommitGroupRightsForEditing @GroupID
			END
		END
		ELSE
		BEGIN
		    -- Cancel work in progress and clear flags
			IF @UserID > 0
			BEGIN
				EXEC CancelUserRightsForEditing @UserID
			END
			ELSE
			BEGIN
				EXEC CancelGroupRightsForEditing @GroupID
			END
		END
	END

END
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupEditingSetInProgress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GroupEditingSetInProgress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GroupEditingSetInProgress] TO [sp_executeall]
GO
