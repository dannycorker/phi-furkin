SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-09-30
-- Description:	Return a pivoted table of TableDetailValues
-- =============================================
CREATE PROC [dbo].[TableRowPivot__GetTableOfValues]
(
	@LeadID int,
	@MatterID int = null,
	@DriveDetailFieldID int = null
)
AS
BEGIN

	/* If no drive-field was passed in, try to find one */
	IF @DriveDetailFieldID IS NULL
	BEGIN 

		SELECT TOP 1 DetailFieldID 
		FROM TableDetailValues 
		WHERE (@LeadID = @LeadID)
		AND (@MatterID IS NULL OR MatterID = @MatterID)

	END

	/* Must have a drive-field or the rest of these statements will not make any sense */
	IF @DriveDetailFieldID IS NULL
	BEGIN 
		SELECT @LeadID as [LeadID], @MatterID as [MatterID], 'No fields found' as [Message]

		RETURN
	END

	/* Create a GUID for our set of records */
	DECLARE @QueryID UNIQUEIDENTIFIER 

	/* Table variable for holding the list of detailfields defined within the TableRow */
	DECLARE @DFList TABLE (DFID int)

	/* Loop variables for populating the perma-temp table */
	DECLARE @ExtraFieldCount int, @FieldsToUpdate int, @FieldNumber int, @LoopDetailFieldID int, @LoopDetailFieldName nvarchar(50), 
			@FieldID varchar(15), @ValueName varchar(15), @EncryptedValueName varchar(15), 
			@UpdateStatement varchar(2000), @OutputStatement varchar(max) 


	/* Create a GUID so that this query will never clash with another one running at the same time */
	SELECT @QueryID = newid()

	/* Insert the base data into the perma-temp table, using our GUID for a unique result set */
	INSERT INTO [dbo].[TableRowPivot]
	(
		QueryID,
		TableRowID,
		ClientID,
		LeadID,
		MatterID,
		DetailFieldID1,
		DetailValue1,
		EncryptedValue1,
		DetailFieldName1 
	)
	SELECT DISTINCT 
		@QueryID, 
		tdv.TableRowID,
		tdv.ClientID,
		@LeadID,
		@MatterID,
		tdv.DetailFieldID,
		tdv.DetailValue,
		tdv.EncryptedValue, 
		df.FieldName  
	FROM TableDetailValues tdv 
	INNER JOIN DetailFields df ON df.DetailFieldID = tdv.DetailFieldID 
	WHERE (@LeadID IS NULL OR tdv.LeadID = @LeadID)
	AND (@MatterID IS NULL OR tdv.MatterID = @MatterID)
	AND tdv.DetailFieldID = @DriveDetailFieldID 

	/* Populate the list of all the remaining detail fields used by this table */
	INSERT @DFList (DFID) 
	SELECT DISTINCT(detailfieldid) 
	FROM tabledetailvalues 
	WHERE tablerowid = (SELECT TOP 1 TableRowID FROM [dbo].[TableRowPivot] WHERE QueryID = @QueryID)
	AND detailfieldid <> @DriveDetailFieldID 

	/* Tot up how many times we have to loop round the update statement */
	SELECT @ExtraFieldCount = COUNT(*) FROM @DFList

	SELECT @FieldsToUpdate = @ExtraFieldCount, @FieldNumber = 1

	IF @FieldsToUpdate > 0
	BEGIN
		SELECT TOP 1 @LoopDetailFieldID = DFID,
		@FieldNumber = @FieldNumber + 1 
		FROM @DFList
	END

	WHILE @FieldsToUpdate > 0 AND @FieldNumber < 26
	BEGIN
		SELECT @FieldID = 'DetailFieldID' + convert(varchar, @FieldNumber), 
		@ValueName = 'DetailValue' + convert(varchar, @FieldNumber),
		@EncryptedValueName = 'EncryptedValue' + convert(varchar, @FieldNumber), 
		@LoopDetailFieldName = 'DetailFieldName' + convert(varchar, @FieldNumber)

		SELECT @UpdateStatement = 'UPDATE tp SET ' 
			+ @FieldID + ' = tdv.DetailFieldID, '
			+ @ValueName + ' = tdv.DetailValue, '
			+ @EncryptedValueName + ' = tdv.EncryptedValue, '
			+ @LoopDetailFieldName + ' = df.FieldName '
			+ ' FROM dbo.TableRowPivot tp ' 
			+ ' INNER JOIN dbo.TableDetailValues tdv ON tdv.TableRowID = tp.TableRowID '
			+ ' AND tdv.DetailFieldID = ' + convert(varchar, @LoopDetailFieldID) 
			+ CASE WHEN @LeadID IS NULL THEN '' ELSE ' AND tdv.LeadID = ' + convert(varchar, @LeadID) END 
			+ CASE WHEN @MatterID IS NULL THEN '' ELSE ' AND tdv.MatterID = ' + convert(varchar, @MatterID) END 
			+ ' INNER JOIN dbo.DetailFields df ON df.DetailFieldID = tdv.DetailFieldID ' 
			+ ' AND tp.QueryID = ' + '''' + convert(varchar(36), @QueryID) + ''''

		EXEC(@UpdateStatement)

		DELETE @DFList WHERE DFID = @LoopDetailFieldID

		SELECT TOP 1 @LoopDetailFieldID = DFID
		FROM @DFList

		/* Loop control (keep this separate to avoid infinite loops) */
		SELECT @FieldNumber = @FieldNumber + 1, @FieldsToUpdate = @FieldsToUpdate - 1

	END

	/* Turn this into a dynamic statement, returning the detailfield names as column names */
	SELECT TOP 1 @OutputStatement = 'SELECT LeadID, MatterID, DetailValue1 as [' + DetailFieldName1 + '] ' 
		+ CASE WHEN @ExtraFieldCount > 0 THEN ', DetailValue2 as [' + DetailFieldName2 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 1 THEN ', DetailValue3 as [' + DetailFieldName3 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 2 THEN ', DetailValue4 as [' + DetailFieldName4 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 3 THEN ', DetailValue5 as [' + DetailFieldName5 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 4 THEN ', DetailValue6 as [' + DetailFieldName6 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 5 THEN ', DetailValue7 as [' + DetailFieldName7 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 6 THEN ', DetailValue8 as [' + DetailFieldName8 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 7 THEN ', DetailValue9 as [' + DetailFieldName9 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 8 THEN ', DetailValue10 as [' + DetailFieldName10 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 9 THEN ', DetailValue11 as [' + DetailFieldName11 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 10 THEN ', DetailValue12 as [' + DetailFieldName12 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 11 THEN ', DetailValue13 as [' + DetailFieldName13 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 12 THEN ', DetailValue14 as [' + DetailFieldName14 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 13 THEN ', DetailValue15 as [' + DetailFieldName15 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 14 THEN ', DetailValue16 as [' + DetailFieldName16 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 15 THEN ', DetailValue17 as [' + DetailFieldName17 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 16 THEN ', DetailValue18 as [' + DetailFieldName18 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 17 THEN ', DetailValue19 as [' + DetailFieldName19 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 18 THEN ', DetailValue20 as [' + DetailFieldName20 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 19 THEN ', DetailValue21 as [' + DetailFieldName21 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 20 THEN ', DetailValue22 as [' + DetailFieldName22 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 21 THEN ', DetailValue23 as [' + DetailFieldName23 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 22 THEN ', DetailValue24 as [' + DetailFieldName24 + '] ' ELSE '' END 
		+ CASE WHEN @ExtraFieldCount > 23 THEN ', DetailValue25 as [' + DetailFieldName25 + '] ' ELSE '' END 
		+ ' FROM TableRowPivot '
		+ ' WHERE QueryID = ' + '''' + convert(varchar(36), @QueryID) + '''' 
		+ ' ORDER BY TableRowID '
	FROM TableRowPivot 
	WHERE QueryID = @QueryID 

	/* This is the select statement that returns the results to the user */
	EXEC(@OutputStatement)

	/* Tidy Up */
	DELETE TableRowPivot WHERE QueryID = @QueryID 
END



GO
GRANT VIEW DEFINITION ON  [dbo].[TableRowPivot__GetTableOfValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[TableRowPivot__GetTableOfValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[TableRowPivot__GetTableOfValues] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[TableRowPivot__GetTableOfValues] TO [sp_executehelper]
GO
