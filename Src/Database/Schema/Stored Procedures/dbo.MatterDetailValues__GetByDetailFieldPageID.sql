SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[MatterDetailValues__GetByDetailFieldPageID]
(

	@PageID int, 
	@ClientID int
	
)

AS


				SELECT v.*
				FROM
					[dbo].[MatterDetailValues] v
					INNER JOIN dbo.DetailFields f ON v.DetailFieldID = f.DetailFieldID
				WHERE
					f.DetailFieldPageID = @PageID
					AND v.[ClientID] = @ClientID





GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues__GetByDetailFieldPageID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[MatterDetailValues__GetByDetailFieldPageID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[MatterDetailValues__GetByDetailFieldPageID] TO [sp_executeall]
GO
