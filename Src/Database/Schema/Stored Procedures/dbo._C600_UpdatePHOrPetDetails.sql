SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Dave Morgan
-- Create date: 20-Nov-2014
-- Description:	Updates PH Or Pet Details or just the fields containing current settings
--				Return flags whether MTA required 
-- TD 2016-08-12 Get policystart date and compare age change of pet - if policy within first 14 days and pet age change less than 3 months - no MTA/requote
-- TD 2016-08-16 Pending or apply- need to hold the details temporarily at customer level until vet confirmation of changes can be made
-- TD 2016-08-17 Write to temp lead and matterID fields for retrieval  
-- DC 2016-10-21 Updated pet update section to write the value being replaced to the internal tab at Lead level (#40310)
-- 2017-08-03 CPS populate WhenChanged, WhoChanged, ChangeNotes for the history
-- 2017-08-14 maintain TPL exclusion records
-- DJO 2018-05-17 Update purchased product description with new pet name
-- 2018-06-22 GPR modified campaign code block (previoulsy used for pettype (same field IDs) in another implementation) for C600 defect 274
-- 2018-06-28 GPR modified ccampaign code block, moved to UpdateType=2 following move to 'begin-change pet details' event - defect 269
-- =============================================
CREATE PROCEDURE [dbo].[_C600_UpdatePHOrPetDetails]
(
	@LeadEventID INT,
	@UpdateType INT, -- PH=1,Pet=2,3=Payment details
	@UpdateCurrentSettingsFieldsOnly INT = 0,
	@PendingOrApply BIT = 1 /*Default applies*/
)

AS
BEGIN

	SET NOCOUNT ON;

	DECLARE 
			@Address1 VARCHAR(200),
			@Address2 VARCHAR(200),
			@CaseID INT,
			@ColMatterID INT,
			@County VARCHAR(200),
			@CurrentDetails VARCHAR(2000),
			@CustomerID INT,
			@DFID INT,
			@EventTypeID INT,
			@ItemText VARCHAR(50),
			@LeadID INT,
			@MatterID INT,
			@OrigValue VARCHAR(500),
			@MTARequired INT = 0,
			@Packed VARCHAR(2000),
			@PostCode VARCHAR(200),
			@Seq INT,
			@Town VARCHAR(200),
			@UpdateRecord VARCHAR(2000)='',
			@Value VARCHAR(500),
			@OldValue VARCHAR (500),
			@Whocreated INT,
			@AdjustmentDate		DATE,
			@ExclusionTableRowID	INT,
			@ProductDescription VARCHAR(200)
					
	DECLARE @Unpacked TABLE
			(
				ID INT IDENTITY,
				Line VARCHAR(2000)
			)			
			
	DECLARE @Items TABLE (Seq INT, ItemText VARCHAR(50), DFID INT, VALUE VARCHAR(500), Done INT)
	
	SELECT TOP (1) 
		@LeadID = le.LeadID, 
		@EventTypeID = le.EventTypeID, 
		@CustomerID = m.CustomerID, 
		@CaseID = le.CaseID, 
		@MatterID = m.MatterID,
		@Whocreated = le.WhoCreated
	FROM dbo.LeadEvent le WITH (NOLOCK)
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.CaseID = le.CaseID	
	WHERE le.LeadEventID = @LeadEventID 
			
	SELECT @ColMatterID=dbo.fn_C600_GetCollectionsMatterID(@MatterID)

	IF @UpdateType=2 AND @UpdateCurrentSettingsFieldsOnly = 0
	BEGIN
	
	/*GPR 2018-06-27 moved to UpdateType 2*/					
	/*Campaign Code: previously this block was used for PetType as DC 2016, this is not used in C600. GPR 2018-06-22
	modified this block to suit Campaign Code (field IDs were the same)*/
		SELECT @Value = dbo.fnGetSimpleDv(179912,@CustomerID) /*GPR 2018-06-22*/
		SELECT @OldValue = dbo.fnGetSimpleDv(175488,@MatterID) /*GPR 2018-06-22*/
		
		IF @Value<>'' AND @Value<>@OldValue
		BEGIN
		
			SELECT @UpdateRecord=ISNULL(@UpdateRecord,'') + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Campaign Code from ' + ISNULL(rl.DetailValue,'') + ' (' + CONVERT(VARCHAR(30),ldv.DetailValue) + ') to ' 
			FROM MatterDetailValues ldv WITH (NOLOCK) 
			LEFT JOIN ResourceListDetailValues rl WITH (NOLOCK) ON ldv.ValueInt=rl.ResourceListID AND rl.DetailFieldID=177363
			WHERE ldv.MatterID=@MatterID AND ldv.DetailFieldID=175488

			SELECT @UpdateRecord=@UpdateRecord + DetailValue + ' (' + @Value + ')' 
			FROM ResourceListDetailValues WITH (NOLOCK) WHERE ResourceListID=@Value AND DetailFieldID=175488
			
			IF @PendingOrApply = 1
			BEGIN
			
			    /*GPR 2018-06-22 Update previous campaign code field at Lead level*/
				EXEC dbo._C00_SimpleValueIntoField 179913, @OldValue, @LeadID, @Whocreated
				/*GPR 2018-06-22 Matter: Campaign Code*/
				EXEC dbo._C00_SimpleValueIntoField 175488, @Value, @MatterID, @Whocreated
				/*GPR 2018-06-22 Lead: Campaign Code*/
			    EXEC dbo._C00_SimpleValueIntoField 179912, @Value, @CustomerID, @Whocreated
			
			END 
						
			SELECT @MTARequired = 1
		
		END
	END		
		
	IF @UpdateType=1 AND @UpdateCurrentSettingsFieldsOnly = 0
	BEGIN	
		
		-- Title
			
		SELECT @Value = dbo.fnGetDvLuli(175310,@CaseID)
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord = @UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Title from ' + ISNULL(t.Title,'') + ' to ' + @Value
			FROM Customers c WITH (NOLOCK) 
			LEFT JOIN Titles t WITH (NOLOCK) ON t.TitleID=c.TitleID
			WHERE CustomerID=@CustomerID 
			
			IF @PendingOrApply = 1
			BEGIN
			
				UPDATE Customers 
				SET TitleID=( SELECT t.TitleID FROM Titles t WITH (NOLOCK)
								INNER JOIN LookupListItems lli WITH (NOLOCK) ON t.Title=lli.ItemValue 
								AND lli.LookupListItemID=dbo.fnGetDvAsInt(175310,@CaseID) )  
					,WhenChanged = CURRENT_TIMESTAMP
					,WhoChanged = @WhoCreated
					,ChangeSource = OBJECT_NAME(@@PROCID)
				WHERE CustomerID=@CustomerID
				
				EXEC dbo._C00_SimpleValueIntoField 175310, '', @CustomerID, @Whocreated /*Title*/
			
			END
			
		END
				
		-- FirstName
			
		SELECT @Value = dbo.fnGetDv(175311,@CaseID) /*First Name*/
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'First name from ' + ISNULL(FirstName,'') + ' to ' + @Value
			FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID
			
			IF @PendingOrApply = 1
			BEGIN
			
				UPDATE Customers SET FirstName=@Value 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID) + ' @LeadEventID = ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
				WHERE CustomerID=@CustomerID
			
				EXEC dbo._C00_SimpleValueIntoField 175311, '', @CustomerID, @Whocreated /*First Name*/
			END
		
		END
				
		-- MiddleName
			
		SELECT @Value = dbo.fnGetDv(175312,@CaseID) /*Middle Name*/
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Middle name from ' + ISNULL(MiddleName,'') + ' to ' + @Value
			FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID
			
			IF @PendingOrApply = 1
			BEGIN
				
				UPDATE Customers 
				SET  MiddleName		= @Value 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID) + ' @LeadEventID = ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
				WHERE CustomerID=@CustomerID
			
				EXEC dbo._C00_SimpleValueIntoField 175312, '', @CustomerID, @Whocreated /*Middle Name*/
			
			END
		
		END
				
		-- Last Name
			
		SELECT @Value = dbo.fnGetDv(175313,@CaseID) /*Last Name*/
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Last Name from ' + ISNULL(LastName,'') + ' to ' + @Value
			FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID
			
			IF @PendingOrApply = 1
			BEGIN
			
				UPDATE Customers 
				SET	 LastName		= @Value 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID) + ' @LeadEventID = ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
				WHERE CustomerID=@CustomerID
				
				EXEC dbo._C00_SimpleValueIntoField 175313, '', @CustomerID, @Whocreated /*Last Name*/
				
			END
		
		END
				
		-- Address1
			
		SELECT @Value = dbo.fnGetDv(175314,@CaseID)
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Address1 from ' + ISNULL(Address1,'') + ' to ' + @Value
			FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID
			
			IF @PendingOrApply = 1
			BEGIN
			
				UPDATE Customers 
				SET	 Address1		= @Value 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID) + ' @LeadEventID = ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
				WHERE CustomerID=@CustomerID
			
				EXEC dbo._C00_SimpleValueIntoField 175314, '', @CustomerID, @Whocreated /*Address 1*/
				
			END
		
		END
				
		-- Address2
			
		SELECT @Value = dbo.fnGetDv(175315,@CaseID) /*Address 2*/
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Address2 from ' + ISNULL(Address2,'') + ' to ' + @Value
			FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID
			
			IF @PendingOrApply = 1
			BEGIN
			
				UPDATE Customers 
				SET  Address2		= @Value 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID) + ' @LeadEventID = ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
				WHERE CustomerID=@CustomerID
			
				EXEC dbo._C00_SimpleValueIntoField 175315, '', @CustomerID, @Whocreated /*Address 2*/
				
			END
			
		END
				
		-- Town
			
		SELECT @Value = dbo.fnGetDv(175316,@CaseID)
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Town from ' + ISNULL(Town,'') + ' to ' + @Value
			FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID
			
			IF @PendingOrApply = 1
			BEGIN
				
				UPDATE Customers 
				SET  Town			= @Value 
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID) + ' @LeadEventID = ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
				WHERE CustomerID=@CustomerID
			
				EXEC dbo._C00_SimpleValueIntoField 175316, '', @CustomerID, @Whocreated /*Town*/
				
			END
		
		END
				
		-- County
			
		SELECT @Value = dbo.fnGetDv(175317,@CaseID) /*County*/
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'County from ' + ISNULL(County,'') + ' to ' + @Value
			FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID
		
			IF @PendingOrApply = 1
			BEGIN
			
				UPDATE Customers 
				SET  County			= @Value 
					,WhenChanged = CURRENT_TIMESTAMP
					,WhoChanged = @WhoCreated
					,ChangeSource = OBJECT_NAME(@@PROCID) + ' @LeadEventID = ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
				WHERE CustomerID=@CustomerID
			
				EXEC dbo._C00_SimpleValueIntoField 175317, '', @CustomerID, @Whocreated /*County*/
				
			END
		
		END
				
		-- Postcode
			
		SELECT @Value = dbo.fnGetDv(175318,@CaseID) /*Postcode*/
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Postcode from ' + ISNULL(PostCode,'') + ' to ' + @Value
			FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID
			
			IF @PendingOrApply = 1
			BEGIN
			
				UPDATE Customers 
				SET  PostCode		= @Value 
					,WhenChanged = CURRENT_TIMESTAMP
					,WhoChanged = @WhoCreated
					,ChangeSource = OBJECT_NAME(@@PROCID) + ' @LeadEventID = ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
				WHERE CustomerID=@CustomerID
			
				EXEC dbo._C00_SimpleValueIntoField 175318, '', @CustomerID, @Whocreated /*Postcode*/
			
			END
			
			SELECT @MTARequired = 1
		
		END
		
		-- Address Picker change
				
		SELECT @Packed = DetailValue
		FROM dbo.CustomerDetailValues WITH ( NOLOCK )
		WHERE DetailFieldID = 175478 /*Change Address - Address Picker*/
		AND CustomerID = @CustomerID 

		INSERT @Unpacked (Line)
		SELECT AnyValue
		FROM dbo.fnTableOfValues(@Packed, '_')

		DECLARE @Count INT = 0
		SELECT @Count = COUNT(*) 
		FROM @Unpacked

		IF @Count = 5
		BEGIN
				
			SELECT @Address1=Line FROM @Unpacked WHERE ID = 1 
			SELECT @Address2=Line FROM @Unpacked WHERE ID = 2 
			SELECT @Town=Line FROM @Unpacked WHERE ID = 3
			SELECT @County=Line FROM @Unpacked WHERE ID = 4 
			SELECT @PostCode=Line FROM @Unpacked WHERE ID = 5 
	
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + 
					'Address from ' +  ISNULL(Address1,'') + CASE WHEN ISNULL(Address2,'')<>'' THEN ', ' ELSE '' END + ISNULL(Address2,'')
							 + CASE WHEN ISNULL(Town,'')<>'' THEN ', ' ELSE '' END + ISNULL(Town,'')
							 + CASE WHEN ISNULL(County,'')<>'' THEN ', ' ELSE '' END + ISNULL(County,'')
							 + CASE WHEN ISNULL(PostCode,'')<>'' THEN ', ' ELSE '' END + ISNULL(Postcode,'')
							 + ' to ' 
							 + ISNULL(@Address1,'') + CASE WHEN ISNULL(@Address2,'')<>'' THEN ', ' ELSE '' END + ISNULL(@Address2,'')
							 + CASE WHEN ISNULL(@Town,'')<>'' THEN ', ' ELSE '' END + ISNULL(@Town,'')
							 + CASE WHEN ISNULL(@County,'')<>'' THEN ', ' ELSE '' END + ISNULL(@County,'')
							 + CASE WHEN ISNULL(@PostCode,'')<>'' THEN ', ' ELSE '' END + ISNULL(@Postcode,'')
			FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID
			
			-- only re-rate if postcode has changed
			IF REPLACE(LTRIM(RTRIM(@PostCode)),' ','') <> 
				( SELECT REPLACE(LTRIM(RTRIM(PostCode)),' ','') FROM Customers c WITH (NOLOCK) WHERE c.CustomerID=@CustomerID )
			BEGIN		
				SELECT @MTARequired = 1		
			END
			
			UPDATE dbo.Customers
			SET Address1	= @Address1,
				Address2	= @Address2,
				Town		= @Town,
				County		= @County,
				PostCode	= @PostCode
				,WhenChanged = CURRENT_TIMESTAMP
				,WhoChanged = @WhoCreated
				,ChangeSource = OBJECT_NAME(@@PROCID) + ' @LeadEventID = ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
			WHERE CustomerID = @CustomerID
					
			EXEC dbo._C00_SimpleValueIntoField 175478, '', @CustomerID, @Whocreated /*Change Address - Address Picker*/
			
		END
				
		-- DOB
			
		SELECT @Value = dbo.fnGetDv(175319,@CaseID)
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'DOB from ' + ISNULL(CONVERT(VARCHAR(10),DateOfBirth,120),'') + ' to ' + @Value
			FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID
			
			IF @PendingOrApply = 1
			BEGIN	
			
				UPDATE Customers 
				SET  DateOfBirth	= @Value 
					,WhenChanged = CURRENT_TIMESTAMP
					,WhoChanged = @WhoCreated
					,ChangeSource = OBJECT_NAME(@@PROCID) + ' @LeadEventID = ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
				WHERE CustomerID=@CustomerID
			
				EXEC dbo._C00_SimpleValueIntoField 175319, '', @CustomerID, @Whocreated /*Date Of Birth*/
				
			END
		
		END
				
		-- Email
			
		SELECT @Value = dbo.fnGetDv(175320,@CaseID) /*E-mail*/
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Email from ' + ISNULL(EmailAddress,'') + ' to ' + @Value
			FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID
			
			IF @PendingOrApply = 1
			BEGIN
			
				UPDATE Customers 
				SET  EmailAddress	= @Value 
					,WhenChanged = CURRENT_TIMESTAMP
					,WhoChanged = @WhoCreated
					,ChangeSource = OBJECT_NAME(@@PROCID) + ' @LeadEventID = ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
				WHERE CustomerID=@CustomerID
				
				EXEC dbo._C00_SimpleValueIntoField 175320, '', @CustomerID, @Whocreated /*E-mail*/
			END
		
		END
				
		-- Home phone
			
		SELECT @Value = dbo.fnGetDv(175321,@CaseID) /*Home Phone*/
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Home phone from ' + ISNULL(HomeTelephone,'') + ' to ' + @Value
			FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID
			
			IF @PendingOrApply = 1
			BEGIN
			
				UPDATE Customers 
				SET  HomeTelephone	= @Value 
					,WhenChanged = CURRENT_TIMESTAMP
					,WhoChanged = @WhoCreated
					,ChangeSource = OBJECT_NAME(@@PROCID) + ' @LeadEventID = ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
				WHERE CustomerID=@CustomerID
			
				EXEC dbo._C00_SimpleValueIntoField 175321, '', @CustomerID, @Whocreated /*Home Phone*/
				
			END
		
		END
				
		-- Daytime phone
			
		SELECT @Value = dbo.fnGetDv(175322,@CaseID) /*Daytime Phone*/
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Daytime phone from ' + ISNULL(DayTimeTelephoneNumber,'') + ' to ' + @Value
			FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID
			
			IF @PendingOrApply = 1
			BEGIN	
			
				UPDATE Customers 
				SET  DayTimeTelephoneNumber	= @Value 
					,WhenChanged = CURRENT_TIMESTAMP
					,WhoChanged = @WhoCreated
					,ChangeSource = OBJECT_NAME(@@PROCID) + ' @LeadEventID = ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
				WHERE CustomerID=@CustomerID
			
				EXEC dbo._C00_SimpleValueIntoField 175322, '', @CustomerID, @Whocreated /*Daytime Phone*/
			
			END
		
		END
				
		-- Mobile phone
			
		SELECT @Value = dbo.fnGetDv(175323,@CaseID) /*Mobile Phone*/
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Mobile phone from ' + ISNULL(MobileTelephone,'') + ' to ' + @Value
			FROM Customers WITH (NOLOCK) WHERE CustomerID=@CustomerID
			
			IF @PendingOrApply = 1
			BEGIN
			
				UPDATE Customers 
				SET  MobileTelephone= @Value 
					,WhenChanged = CURRENT_TIMESTAMP
					,WhoChanged = @WhoCreated
					,ChangeSource = OBJECT_NAME(@@PROCID) + ' @LeadEventID = ' + ISNULL(CONVERT(VARCHAR,@LeadEventID),'NULL')
				WHERE CustomerID=@CustomerID
			
				EXEC dbo._C00_SimpleValueIntoField 175323, '', @CustomerID, @Whocreated /*Mobile Phone*/
			END
		
		END

		-- check preferences have valid contact details
		----EXEC _C600_CheckCommunicationPreferences @CustomerID /*Removed by GPR 2019-11-01*/
	
	END
	
	IF @UpdateType=2 AND @UpdateCurrentSettingsFieldsOnly = 0
	BEGIN
				
		-- all pet detail changes require vet confirmation & so should go through re-rating exercise
		SELECT @MTARequired = 1
		
		-- Pet Name
		
		/*Get pet name from customer field*/	
		SELECT @Value = dbo.fnGetDv(175325,@CaseID) /*Pet Name*/
		
		/*Get the value being overwritten at lead level - DC 2016-10-21*/
		SELECT @OldValue = dbo.fnGetDv(144268, @CaseID); /*Pet Name*/

		SELECT @ProductDescription = ProductDescription
		FROM dbo.PurchasedProduct pp
		WHERE pp.ObjectID = @MatterID

		SELECT @ProductDescription = REPLACE(@ProductDescription, @OldValue, @Value)
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Pet name from ' + ISNULL(DetailValue,'') + ' to ' + @Value
			FROM LeadDetailValues WITH (NOLOCK) WHERE LeadID=@LeadID AND DetailFieldID=144268 /*Pet Name (Lead Details)*/

			IF @PendingOrApply = 1
			BEGIN
				
				/*Write old value to the lead internal tab - DC 2016-10-21*/
				EXEC dbo._C00_SimpleValueIntoField 177294, @OldValue, @LeadID, @Whocreated /*Previous Pet Name (Lead Details)*/
				
				/*Write pet name to lead field*/
				EXEC dbo._C00_SimpleValueIntoField 144268, @Value, @LeadID, @Whocreated /*Pet Name (Lead Details)*/
				
				/*Clear pet name from customer field*/
				EXEC dbo._C00_SimpleValueIntoField 175325, '', @CustomerID, @Whocreated /*Pet Name (Customer Details)*/

				UPDATE dbo.PurchasedProduct
				SET ProductDescription = @ProductDescription
				WHERE ObjectID = @MatterID
			
			END	
	
		END
				
		-- Pet Date Of Birth
			
		SELECT @Value = dbo.fnGetDv(175326,@CaseID) /*Pet Date Of Birth*/
		
		/*Get the value being overwritten at lead level - DC 2016-10-21*/
		SELECT @OldValue = dbo.fnGetDv(144274, @CaseID);

		IF @Value<>''
		BEGIN
			
			SELECT @MTARequired = 1
			
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Pet date of birth from ' + ISNULL(DetailValue,'') + ' to ' + @Value
			FROM LeadDetailValues WITH (NOLOCK) WHERE LeadID=@LeadID AND DetailFieldID=144274 /*Pet Date of Birth*/
			
			IF @PendingOrApply = 1
			BEGIN
			
			    /*Write old value to the lead internal tab - DC 2016-10-21*/
				EXEC dbo._C00_SimpleValueIntoField 177295, @OldValue, @LeadID, @Whocreated /*Previous Pet DoB*/
				
				EXEC dbo._C00_SimpleValueIntoField 144274, @Value, @LeadID, @Whocreated /*Pet Date of Birth*/
			
				EXEC dbo._C00_SimpleValueIntoField 175326, '', @CustomerID, @Whocreated /*Pet Date Of Birth*/
			
			END

			
		END
				
		-- Pet Nickname
			
		SELECT @Value = dbo.fnGetDv(175327,@CaseID) /*Pet Nickname*/
		
		IF @Value<>''
		BEGIN
		
			--SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + 
			--		'Pet nickname from ' + ISNULL(DetailValue,'') + ' to ' + @Value
			--FROM LeadDetailValues WITH (NOLOCK) WHERE LeadID=@LeadID AND DetailFieldID=123
			
			--EXEC dbo._C00_SimpleValueIntoField 123, @Value, @LeadID
			
			IF @PendingOrApply = 1
			BEGIN
				
				EXEC dbo._C00_SimpleValueIntoField 175327, '', @CustomerID, @Whocreated /*Pet Nickname*/
				
			END
		
		END

		-- Pet Breed
			
		SELECT @Value = dbo.fnGetDv(175328,@CaseID) /*Pet Breed*/
		
		/*Get the value being overwritten at lead level - DC 2016-10-21*/
		SELECT @OldValue = dbo.fnGetDv(144272, @CaseID); /*Pet Type*/
		
		IF @Value<>'' AND @Value<>@OldValue
		BEGIN
		
			SELECT @UpdateRecord=ISNULL(@UpdateRecord,'') + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Pet breed from ' + ISNULL(rl.DetailValue,'') + ' (' + CONVERT(VARCHAR(30),ldv.DetailValue) + ') to ' 
			FROM LeadDetailValues ldv WITH (NOLOCK) 
			LEFT JOIN ResourceListDetailValues rl WITH (NOLOCK) ON ldv.ValueInt=rl.ResourceListID AND rl.DetailFieldID=144270 /*Pet Breed*/
			WHERE ldv.LeadID=@LeadID AND ldv.DetailFieldID=144272 /*Pet Type*/

			SELECT @UpdateRecord=@UpdateRecord + DetailValue + ' (' + @Value + ')' 
			FROM ResourceListDetailValues WITH (NOLOCK) WHERE ResourceListID=@Value AND DetailFieldID=144270 /*Pet Breed*/
			
			IF @PendingOrApply = 1
			BEGIN
			
			    /*Write old value to the lead internal tab - DC 2016-10-21*/
				EXEC dbo._C00_SimpleValueIntoField 177296, @OldValue, @LeadID, @Whocreated /*Previous Pet Type*/
			
				EXEC dbo._C00_SimpleValueIntoField 144272, @Value, @LeadID, @Whocreated /*Pet Type*/
			
				EXEC dbo._C00_SimpleValueIntoField 175328, '', @CustomerID, @Whocreated /*Pet Breed*/
			
			END 
			
			SELECT @MTARequired = 1
		
		END
				
		-- Pet Sex
		SELECT @Value = dbo.fnGetDvLuli(175329,@CaseID) /*Pet Sex*/
		
		/*Get the value being overwritten at lead level - DC 2016-10-21*/
		SELECT @OldValue = dbo.fnGetDv(144275, @CaseID); /*Pet Sex*/
		
		IF @Value <>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Pet sex from ' + ISNULL(dbo.fnGetDvLuli(144275,@CaseID),'') + ' to ' + @Value /*Pet Sex*/
			
			SELECT @Value = dbo.fnGetDv(175329,@CaseID) /*Pet Sex*/
			
			IF @PendingOrApply = 1
			BEGIN
			
			    /*Write old value to the lead internal tab - DC 2016-10-21*/
				EXEC dbo._C00_SimpleValueIntoField 177297, @OldValue, @LeadID, @Whocreated /*Previous Pet Sex*/
			
				EXEC dbo._C00_SimpleValueIntoField 144275, @Value, @LeadID, @Whocreated /*Pet Sex*/
			
				EXEC dbo._C00_SimpleValueIntoField 175329, '', @CustomerID, @Whocreated /*Pet Sex*/
			
			END
			
			SELECT @MTARequired = 1
		
		END
						
		-- Pet Neutered
		SELECT @Value = dbo.fnGetDvLuli(178406,@CaseID) /*Pet Neutered (customer)*/
		
		/*Get the value being overwritten at lead level - DC 2016-10-21*/
		SELECT @OldValue = dbo.fnGetDv(152783, @CaseID); /*Pet Neutered (lead)*/
		
		IF @Value <>'' AND @Value <> '0'
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Pet neutered from ' + ISNULL(dbo.fnGetDvLuli(152783,@CaseID),'') + ' to ' + @Value /*Pet Neutered (lead)*/
			
			SELECT @Value = dbo.fnGetDv(178406,@CaseID) /*Pet Neutered*/
			
			IF @PendingOrApply = 1
			BEGIN
			
			    /*Write old value to the lead internal tab - DC 2016-10-21*/
				EXEC dbo._C00_SimpleValueIntoField 177672, @OldValue, @LeadID, @Whocreated /*Previous Pet Neutered*/
			
				EXEC dbo._C00_SimpleValueIntoField 152783, @Value, @LeadID, @Whocreated /*Pet Neutered (lead)*/
			
				EXEC dbo._C00_SimpleValueIntoField 178406, '', @CustomerID, @Whocreated /*Pet Neutered (customer)*/
			
			END
			
			SELECT @MTARequired = 1
		
		END

		-- Pet Purchase Cost
			
		SELECT @Value = dbo.fnGetDv(175352,@CaseID) /*Pet Purchase Cost*/
		
		/*Get the value being overwritten at lead level - DC 2016-10-21*/
		SELECT @OldValue = dbo.fnGetDv(144339, @CaseID); /*Pet Purchase Cost*/
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Pet purchase cost from ' + ISNULL(DetailValue,'') + ' to ' + @Value
			FROM LeadDetailValues WITH (NOLOCK) WHERE LeadID=@LeadID AND DetailFieldID=144339 /*Pet Purchase Cost*/
			
			IF @PendingOrApply = 1
			BEGIN
			
			    /*Write old value to the lead internal tab - DC 2016-10-21*/
				EXEC dbo._C00_SimpleValueIntoField 177298, @OldValue, @LeadID, @Whocreated /*Previous Pet Purchase Cost*/
						
				EXEC dbo._C00_SimpleValueIntoField 144339, @Value, @LeadID, @Whocreated /*Pet Purchase Cost*/
			
				EXEC dbo._C00_SimpleValueIntoField 175352, '', @CustomerID, @Whocreated /*Pet Purchase Cost*/
			
			END
			
			SELECT @MTARequired = 1
		
		END

		-- Pet Vicious Tendencies
			
		SELECT @Value = dbo.fnGetDv(178405,@CaseID) /*Vicious Tendencies*/
		
		/*Get the value being overwritten at lead level - DC 2016-10-21*/
		SELECT @OldValue = dbo.fnGetDv(177452, @CaseID); /*Has your pet ever shown aggressive tendencies?*/
		
		IF @Value<>'' AND @Value <> '0'
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Vicious Tendencies from ' + ISNULL(liOld.ItemValue,'') + ' to ' + ISNULL(liNew.ItemValue,'')
			FROM LeadDetailValues ldv WITH (NOLOCK) 
			LEFT JOIN LookupListItems liOld WITH ( NOLOCK ) on liOld.LookupListItemID = ldv.ValueInt
			LEFT JOIN LookupListItems liNew WITH ( NOLOCK ) on CONVERT(VARCHAR,liNew.LookupListItemID) = @Value
			WHERE LeadID=@LeadID AND DetailFieldID=177452 /*Has your pet ever shown aggressive tendencies?*/
			
			IF @PendingOrApply = 1
			BEGIN
			
			    /*Write old value to the lead internal tab - DC 2016-10-21*/
				EXEC dbo._C00_SimpleValueIntoField 178032, @OldValue, @LeadID, @Whocreated /*Previous Vicious Tendencies*/
						
				EXEC dbo._C00_SimpleValueIntoField 177452, @Value, @LeadID, @Whocreated /*Has your pet ever shown aggressive tendencies?*/
			
				EXEC dbo._C00_SimpleValueIntoField 178405, '', @CustomerID, @Whocreated /*Vicious Tendencies*/
				
				SELECT @ExclusionTableRowID = NULL
				SELECT @ExclusionTableRowID = tr.TableRowID
                FROM TableRows tr WITH ( NOLOCK ) 
                INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 177503 /*Policy Section*/
                WHERE tr.MatterID = @MatterID
                AND tr.DetailFieldID = 177506 /*Excluded Policy Sections*/
                AND tdv.ResourceListID = 148149 /*Third Party Liability*/
				
				IF		@Value = '5144' /*Yes*/
					AND	@ExclusionTableRowID is NULL
				BEGIN
					--SELECT @AdjustmentDate = ISNULL(dbo.fnGetSimpleDvAsDate(175442,@MatterID),dbo.fn_GetDate_Local()) /*Adjustment Date*/
					SELECT @AdjustmentDate = dbo.fnGetSimpleDvAsDate(170036,@MatterID) /*Policy Start Date*/
					EXEC @ExclusionTableRowID = _C600_CreatePolicySectionExclusionRow @MatterID, 148149 /*TPL*/, '74574' /*Aggressive Tendencies*/, @LeadEventID, @WhoCreated, @AdjustmentDate
					
					SELECT @UpdateRecord += 'TPL Exclusion TableRowID ' + ISNULL(CONVERT(VARCHAR,@ExclusionTableRowID),'NULL') + ' (' + ISNULL(CONVERT(VARCHAR,dbo.fnGetSimpleDvAsDate(177513,@ExclusionTableRowID),103),'NULL') + ') created.' + CHAR(13)+CHAR(10)
				END 
				ELSE
				IF		@Value = '5145' /*No*/
					AND @ExclusionTableRowID <> 0
				BEGIN
					SELECT @UpdateRecord += 'TPL Exclusion TableRowID ' + ISNULL(CONVERT(VARCHAR,@ExclusionTableRowID),'NULL') + ' (' + ISNULL(CONVERT(VARCHAR,dbo.fnGetSimpleDvAsDate(177513,@ExclusionTableRowID),103),'NULL') + ') deleted.' + CHAR(13)+CHAR(10)
					EXEC TableRows_Delete @ExclusionTableRowID
				END
			
			END
			
			SELECT @MTARequired = 1
		
		END

	END
	
	-- Account Details
	IF @UpdateType=3 AND @UpdateCurrentSettingsFieldsOnly = 0
	BEGIN
								
		-- Account Name
			
		SELECT @Value = dbo.fnGetDv(170250,@CaseID) /*New Account Name*/
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10)+
					'Account Name ' + ISNULL(DetailValue,'') + ' to ' + @Value
			FROM MatterDetailValues WITH (NOLOCK) WHERE MatterID=@MatterID AND DetailFieldID=170188
			
			IF @PendingOrApply = 1
			BEGIN
			
				EXEC dbo._C00_SimpleValueIntoField 170188, @Value, @MatterID, @Whocreated /*Account Name*/
			
				EXEC dbo._C00_SimpleValueIntoField 170250, '', @CustomerID, @Whocreated /*New Account Name*/
			END
		
		END
				
		-- Account Sort Code
			
		SELECT @Value = dbo.fnGetDv(175363,@CaseID) /*New Sort Code*/
		-- if collections new value is empty, try pol admin
		IF @Value=''
			SELECT @Value = dbo.fnGetDv(170251,@CaseID) /*New Account Sort Code (DD account only)*/
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Sort Code ' + ISNULL(DetailValue,'') + ' to ' + @Value
			FROM MatterDetailValues WITH (NOLOCK) WHERE MatterID=@ColMatterID AND DetailFieldID=170189 /*Account Sort Code*/
			
			IF @PendingOrApply = 1
			BEGIN
						
				EXEC dbo._C00_SimpleValueIntoField 170189, @Value, @MatterID, @Whocreated /*Account Sort Code*/
			
				--these are cleared by sae
				--EXEC dbo._C00_SimpleValueIntoField 175363, '', @ColMatterID
				--EXEC dbo._C00_SimpleValueIntoField 170251, '', @CustomerID
			
			END
					
		END
				
		-- Account Number
			
		SELECT @Value = dbo.fnGetDv(175382,@CaseID)
		-- if collections new value is empty, try pol admin
		IF @Value=''
			SELECT @Value = dbo.fnGetDv(170252,@CaseID)
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Account Number ' + ISNULL(DetailValue,'') + ' to ' + @Value
			FROM MatterDetailValues WITH (NOLOCK) WHERE MatterID=@ColMatterID AND DetailFieldID=170190
			
			IF @PendingOrApply = 1
			BEGIN
		
				EXEC dbo._C00_SimpleValueIntoField 170190, @Value, @MatterID, @Whocreated
			
				--these are cleared by sae
				--EXEC dbo._C00_SimpleValueIntoField 175382, '', @ColMatterID
				--EXEC dbo._C00_SimpleValueIntoField 170252, '', @CustomerID
			
			end
		
		END
				
		-- Card Identifier
			
		SELECT @Value = dbo.fnGetDv(177313,@CaseID)
		-- if collections new value is empty, try pol admin
		IF @Value=''
			SELECT @Value = dbo.fnGetDv(170255,@CaseID)
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Card Identifier ' + ISNULL(DetailValue,'') + ' to ' + @Value
			FROM MatterDetailValues WITH (NOLOCK) WHERE MatterID=@ColMatterID AND DetailFieldID=170193
			
			IF @PendingOrApply = 1
			BEGIN
		
				EXEC dbo._C00_SimpleValueIntoField 170193, @Value, @MatterID, @Whocreated
			
				--these are cleared by sae
				--EXEC dbo._C00_SimpleValueIntoField 177313, '', @ColMatterID
				--EXEC dbo._C00_SimpleValueIntoField 170255, '', @CustomerID
			
			end
		
		END

		-- Day Of Payment
			
		SELECT @Value = ItemValue 
		FROM LookupListItems WITH (NOLOCK) WHERE LookupListItemID = dbo.fnGetDvAsInt(170248,@CaseID)
		
		IF @Value<>''
		BEGIN
		
			SELECT @UpdateRecord=@UpdateRecord + CASE WHEN @UpdateRecord <> '' THEN '; ' ELSE '' END + CHAR(13)+CHAR(10) +
					'Payment Day ' + ISNULL(DetailValue,'') + ' to ' + @Value
			FROM MatterDetailValues WITH (NOLOCK) WHERE MatterID=@MatterID AND DetailFieldID=170168
			
			IF @PendingOrApply = 1
			BEGIN
			
				EXEC dbo._C00_SimpleValueIntoField 170168, @Value, @MatterID, @Whocreated
			
				--EXEC dbo._C00_SimpleValueIntoField 170248, '', @CustomerID  -- this is cleared by the SAE once the change is made
			
			END
		
		END					

	END
	
	IF @UpdateRecord <> ''
	BEGIN
		UPDATE LeadEvent SET Comments = CONCAT(' Changed Details: ', @UpdateRecord) WHERE LeadEventID=@LeadEventID
		EXEC dbo._C00_SimpleValueIntoField 177387, @UpdateRecord, @MatterID, @Whocreated

		EXEC _C00_LogIt 'Info', 'MTA', 'ChangedRecord', @UpdateRecord,  58550
		EXEC _C00_LogIt 'Info', 'MTA', 'LeadEvent', @LeadEventID,  58550

	END

	-- Update Current Values DFs
	IF @UpdateType=1 -- PH
	BEGIN
	
		SELECT @CurrentDetails=
				'Title: ' + ISNULL(t.Title,'') + CHAR(13) + CHAR(10) 
				+
				'Name: ' + ISNULL(FirstName,'') + ' ' + ISNULL(MiddleName,'') + ' ' + ISNULL(LastName,'') + CHAR(13) + CHAR(10) 
				+
				'Address: ' + ISNULL(Address1,'') + CASE WHEN ISNULL(Address2,'')<>'' THEN ', ' ELSE '' END + ISNULL(Address2,'')
							 + CASE WHEN ISNULL(Town,'')<>'' THEN ', ' ELSE '' END + ISNULL(Town,'')
							 + CASE WHEN ISNULL(County,'')<>'' THEN ', ' ELSE '' END + ISNULL(County,'') + CHAR(13) + CHAR(10) +
				'Postcode: ' + ISNULL(PostCode,'') + CHAR(13) + CHAR(10) 
				+
				'DOB: ' + ISNULL(CONVERT(VARCHAR(10),DateOfBirth,120),'') + CHAR(13) + CHAR(10) 
				+
				'Email: ' + ISNULL(EmailAddress,'') + CHAR(13) + CHAR(10) 
				+
				'Phone: ' + ISNULL(HomeTelephone,'') + ' (h); ' + ISNULL(DayTimeTelephoneNumber,'') + ' (d); ' + ISNULL(MobileTelephone,'') + ' (m)'
		FROM Customers c WITH (NOLOCK) 
		LEFT JOIN Titles t WITH (NOLOCK) ON c.TitleID=t.TitleID
		WHERE CustomerID=@CustomerID
		
		EXEC dbo._C00_SimpleValueIntoField 175332, @CurrentDetails, @CustomerID, @Whocreated

	END
	
	IF @UpdateType=2  -- Pet
	BEGIN

		SELECT @CurrentDetails=
				'Name: ' + pn.DetailValue + CHAR(13) + CHAR(10) + 
				'DOB: ' + ISNULL(CONVERT(VARCHAR(10),pdob.DetailValue,120),'') + CHAR(13) + CHAR(10) + 
				'Breed: ' + ISNULL(rlpbr.DetailValue,'') + CHAR(13) + CHAR(10) + 
				'Sex: ' + ISNULL(lips.ItemValue,'') + CHAR(13) + CHAR(10) +
				'Purchase Cost: ' + ISNULL(ppc.DetailValue,'') + CHAR(13)+CHAR(10) +
				'Neutered: ' + ISNULL(liNeut.ItemValue,'') /*CPS 2017-08-09*/
		FROM LeadDetailValues pn WITH (NOLOCK) 
		LEFT JOIN LeadDetailValues pdob WITH (NOLOCK) ON pdob.LeadID = pn.LeadID AND pdob.DetailFieldID				= 144274
		LEFT JOIN LeadDetailValues pbr WITH (NOLOCK) ON pbr.LeadID = pn.LeadID AND pbr.DetailFieldID				= 144272
		LEFT JOIN ResourceListDetailValues rlpbr WITH (NOLOCK) ON pbr.ValueInt=rlpbr.ResourceListID AND rlpbr.DetailFieldID=144270 /*Pet Breed*/
		LEFT JOIN LeadDetailValues ps WITH (NOLOCK) ON ps.LeadID = pn.LeadID AND ps.DetailFieldID					= 144275 /*Pet Sex*/
		LEFT JOIN LookupListItems lips WITH (NOLOCK) ON ps.ValueInt=lips.LookupListItemID
		LEFT JOIN LeadDetailValues ppc WITH (NOLOCK) ON ppc.LeadID = pn.LeadID AND ppc.DetailFieldID				= 144339 /*Pet Purchase Cost*/
		LEFT JOIN LeadDetailValues ldvNeut WITH ( NOLOCK ) on ldvNeut.LeadID = pn.LeadID AND ldvNeut.DetailFieldID	= 152783 /*Pet Neutered*/
		LEFT JOIN LookupListItems liNeut WITH ( NOLOCK ) on liNeut.LookupListItemID = ldvNeut.ValueInt
		WHERE pn.LeadID=@LeadID AND pn.DetailFieldID=144268 /*Pet Name*/
				
			
		EXEC dbo._C00_SimpleValueIntoField 175333, @CurrentDetails, @CustomerID, @Whocreated /*Current Pet Details*/
	

	END
	
	IF @UpdateType=3  -- Payment Details
	BEGIN

		SELECT @CurrentDetails = COALESCE(@CurrentDetails + CHAR(13) + CHAR(10), '') + (
				'Policy Number: ' + polnum.DetailValue + ' (' + pname.DetailValue + '):' + CHAR(13) + CHAR(10) + 
				'    Billing System Account No: ' + bsaccno.DetailValue + CHAR(13) + CHAR(10) +
				'    Type: ' + CASE WHEN a.AccountTypeID=1 THEN 'DD' ELSE 'CP' END + CHAR(13) + CHAR(10) +
				CASE WHEN a.AccountTypeID=1 THEN
					'        Account No: ' + a.AccountNumber + CHAR(13) + CHAR(10) +
					'        Sort Code: ' + a.Sortcode + CHAR(13) + CHAR(10)
				ELSE	
					'        Card Identifier: ' + a.MaskedCardNumber + CHAR(13) + CHAR(10) +
					'        Expiry Date: ' + ISNULL(CAST(a.ExpiryMonth AS VARCHAR),'') + '/' + ISNULL(CAST(a.ExpiryYear AS VARCHAR),'') + CHAR(13) + CHAR(10) 
				END +
				'    Payment Day: ' + payday.DetailValue + CHAR(13) + CHAR(10) +
				'    Interval: ' +  payintluli.ItemValue)
		FROM Matter colm WITH (NOLOCK) 
		INNER JOIN Customers cc WITH (NOLOCK) ON colm.CustomerID=cc.CustomerID
		INNER JOIN Lead allcl WITH (NOLOCK) ON cc.CustomerID=allcl.CustomerID AND allcl.LeadTypeID=1493
		INNER JOIN Matter allcm WITH (NOLOCK) ON allcl.leadID=allcm.LeadID
		INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.ToMatterID=allcm.MatterID AND ltr.FromLeadTypeID=1492
		INNER JOIN Matter pam WITH (NOLOCK) ON ltr.FromMatterID=pam.MatterID
		INNER JOIN Lead pal WITH (NOLOCK) ON pam.LeadID=pal.LeadID
		INNER JOIN MatterDetailValues stat WITH (NOLOCK) ON stat.MatterID=pam.MatterID AND stat.DetailFieldID=170038 AND stat.ValueInt=43002
		INNER JOIN MatterDetailValues polnum WITH (NOLOCK) ON polnum.MatterID=pam.MatterID AND polnum.DetailFieldID=170050
		INNER JOIN MatterDetailValues payday WITH (NOLOCK) ON payday.MatterID=pam.MatterID AND payday.DetailFieldID=170168
		INNER JOIN MatterDetailValues payint WITH (NOLOCK) ON payint.MatterID=pam.MatterID AND payint.DetailFieldID=170176
		INNER JOIN LookupListItems payintluli WITH (NOLOCK) ON payint.ValueInt=payintluli.LookupListItemID
		INNER JOIN LeadDetailValues pname WITH (NOLOCK) ON pname.LeadID=pal.LeadID AND pname.DetailFieldID=144268
		INNER JOIN MatterDetailValues bsaccno WITH (NOLOCK) ON bsaccno.MatterID=allcm.MatterID AND bsaccno.DetailFieldID=176973
		INNER JOIN Account a WITH (NOLOCK) ON a.AccountID=bsaccno.ValueInt
		WHERE colm.MatterID=@ColMatterID AND a.Active=1

		EXEC dbo._C00_SimpleValueIntoField 175331, @CurrentDetails, @CustomerID, @Whocreated

	END
	
	/*Write to temp lead and matterID customer level fields*/
	exec _C00_SimpleValueIntoField 177038,@LeadID,@CustomerID,@Whocreated
	exec _C00_SimpleValueIntoField 177039,@MatterID,@CustomerID,@Whocreated
	
	EXEC _C00_SetLeadEventComments @LeadEventID, @UpdateRecord, 1

	RETURN @MTARequired

END

















GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_UpdatePHOrPetDetails] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_UpdatePHOrPetDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_UpdatePHOrPetDetails] TO [sp_executeall]
GO
