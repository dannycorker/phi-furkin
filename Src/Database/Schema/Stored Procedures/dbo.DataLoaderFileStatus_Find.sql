SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
----------------------------------------------------------------------------------------------------

-- Created By: Aquarium Software (https://www.aquarium-software.com)
-- Purpose: Finds records in the DataLoaderFileStatus table passing nullable parameters
----------------------------------------------------------------------------------------------------
*/


CREATE PROCEDURE [dbo].[DataLoaderFileStatus_Find]
(

	@SearchUsingOR bit   = null ,

	@DataLoaderFileStatusID int   = null ,

	@FileStatusName varchar (250)  = null ,

	@FileStatusDescription varchar (2000)  = null 
)
AS


				
  IF ISNULL(@SearchUsingOR, 0) <> 1
  BEGIN
    SELECT
	  [DataLoaderFileStatusID]
	, [FileStatusName]
	, [FileStatusDescription]
    FROM
	[dbo].[DataLoaderFileStatus] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderFileStatusID] = @DataLoaderFileStatusID OR @DataLoaderFileStatusID IS NULL)
	AND ([FileStatusName] = @FileStatusName OR @FileStatusName IS NULL)
	AND ([FileStatusDescription] = @FileStatusDescription OR @FileStatusDescription IS NULL)
						
  END
  ELSE
  BEGIN
    SELECT
	  [DataLoaderFileStatusID]
	, [FileStatusName]
	, [FileStatusDescription]
    FROM
	[dbo].[DataLoaderFileStatus] WITH (NOLOCK) 
    WHERE 
	 ([DataLoaderFileStatusID] = @DataLoaderFileStatusID AND @DataLoaderFileStatusID is not null)
	OR ([FileStatusName] = @FileStatusName AND @FileStatusName is not null)
	OR ([FileStatusDescription] = @FileStatusDescription AND @FileStatusDescription is not null)
	SELECT @@ROWCOUNT			
  END
				

GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileStatus_Find] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[DataLoaderFileStatus_Find] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[DataLoaderFileStatus_Find] TO [sp_executeall]
GO
