CREATE TABLE [dbo].[SqlFunction]
(
[SqlFunctionID] [int] NOT NULL,
[SqlFunctionName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[FunctionAlias] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[FunctionDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[RestrictedToDataTypeID] [int] NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[IsPreferred] [bit] NOT NULL,
[Example] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ExampleResult] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ResultDataTypeID] [int] NOT NULL,
[ExpressionMask] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[IsAggregate] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[SqlFunction] ADD CONSTRAINT [PK_SqlFunction] PRIMARY KEY CLUSTERED  ([SqlFunctionID])
GO
