CREATE TABLE [dbo].[LeadEventsAffected]
(
[LeadEventsAffectedID] [int] NOT NULL IDENTITY(1, 1),
[LeadEventID] [int] NULL,
[EventTypeID] [int] NULL,
[LeadID] [int] NULL,
[CaseID] [int] NULL,
[EventDeleted] [bit] NULL,
[NextEventID] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhenFollowedUp] [datetime] NULL,
[InProcess] [bit] NOT NULL,
[CurrentDateTime] [datetime] NOT NULL,
[TriggerGUID] [uniqueidentifier] NOT NULL
)
GO
ALTER TABLE [dbo].[LeadEventsAffected] ADD CONSTRAINT [PK_LeadEventsAffected] PRIMARY KEY CLUSTERED  ([LeadEventsAffectedID])
GO
