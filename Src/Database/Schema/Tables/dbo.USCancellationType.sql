CREATE TABLE [dbo].[USCancellationType]
(
[USCancellationTypeID] [int] NOT NULL IDENTITY(1, 1),
[MethodID] [int] NOT NULL,
[MethodName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[USCancellationType] ADD CONSTRAINT [PK_USCancellationTypeID] PRIMARY KEY CLUSTERED  ([USCancellationTypeID])
GO
