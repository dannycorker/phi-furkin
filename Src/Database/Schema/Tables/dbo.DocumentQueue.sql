CREATE TABLE [dbo].[DocumentQueue]
(
[DocumentQueueID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[CaseID] [int] NOT NULL,
[DocumentTypeID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenStored] [datetime] NOT NULL,
[WhenCreated] [datetime] NULL,
[RequiresApproval] [bit] NOT NULL CONSTRAINT [DF__DocumentQ__Requi__5A1A5A11] DEFAULT ((0)),
[ParsedDocumentTitle] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[BasedUponLeadEventID] [int] NULL
)
GO
ALTER TABLE [dbo].[DocumentQueue] ADD CONSTRAINT [PK_DocumentQueue] PRIMARY KEY CLUSTERED  ([DocumentQueueID])
GO
CREATE NONCLUSTERED INDEX [IX_DocumentQueue_Client] ON [dbo].[DocumentQueue] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_DocumentQueue_ClientDocTypeWhenCreated] ON [dbo].[DocumentQueue] ([ClientID], [DocumentTypeID], [WhenCreated])
GO
CREATE NONCLUSTERED INDEX [IX_DocumentQueue_] ON [dbo].[DocumentQueue] ([DocumentTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_DocumentQueue_Lead] ON [dbo].[DocumentQueue] ([LeadID])
GO
CREATE NONCLUSTERED INDEX [IX_DocumentQueue_WhenCreated] ON [dbo].[DocumentQueue] ([WhenCreated])
GO
CREATE NONCLUSTERED INDEX [IX_DocumentQueue_WhoCreated] ON [dbo].[DocumentQueue] ([WhoCreated])
GO
ALTER TABLE [dbo].[DocumentQueue] ADD CONSTRAINT [FK_DocumentQueue_Cases] FOREIGN KEY ([CaseID]) REFERENCES [dbo].[Cases] ([CaseID])
GO
ALTER TABLE [dbo].[DocumentQueue] ADD CONSTRAINT [FK_DocumentQueue_ClientPersonnel] FOREIGN KEY ([WhoCreated]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[DocumentQueue] ADD CONSTRAINT [FK_DocumentQueue_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DocumentQueue] ADD CONSTRAINT [FK_DocumentQueue_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[DocumentQueue] ADD CONSTRAINT [FK_DocumentQueue_DocumentQueue] FOREIGN KEY ([DocumentQueueID]) REFERENCES [dbo].[DocumentQueue] ([DocumentQueueID])
GO
ALTER TABLE [dbo].[DocumentQueue] ADD CONSTRAINT [FK_DocumentQueue_DocumentType] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentType] ([DocumentTypeID])
GO
ALTER TABLE [dbo].[DocumentQueue] ADD CONSTRAINT [FK_DocumentQueue_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
