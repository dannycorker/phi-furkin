CREATE TABLE [dbo].[ObjectType]
(
[ObjectTypeID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[ObjectTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ObjectTypeDescription] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Enabled] [bit] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[ObjectType] ADD CONSTRAINT [PK_ObjectType] PRIMARY KEY CLUSTERED  ([ObjectTypeID])
GO
