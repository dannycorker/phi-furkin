CREATE TABLE [dbo].[WorkWinsCasesWithWins]
(
[WorkWinsCasesWithWinsID] [int] NOT NULL IDENTITY(1, 1),
[WorkWinSeedID] [int] NOT NULL,
[CaseID] [int] NULL
)
GO
ALTER TABLE [dbo].[WorkWinsCasesWithWins] ADD CONSTRAINT [PK_WorkWinsCasesWithWins] PRIMARY KEY CLUSTERED  ([WorkWinsCasesWithWinsID])
GO
CREATE NONCLUSTERED INDEX [IX_WorkWinsCasesWithWins] ON [dbo].[WorkWinsCasesWithWins] ([WorkWinSeedID], [CaseID])
GO
ALTER TABLE [dbo].[WorkWinsCasesWithWins] ADD CONSTRAINT [FK_WorkWinsCasesWithWins_WorkWinSeed] FOREIGN KEY ([WorkWinSeedID]) REFERENCES [dbo].[WorkWinSeed] ([WorkWinSeedID]) ON DELETE CASCADE
GO
