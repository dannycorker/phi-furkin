CREATE TABLE [dbo].[_RawPatch]
(
[Filename] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[RowNumber] [int] NULL,
[PatchVer] [int] NULL,
[Location] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Note] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[AdminOnly] [int] NULL,
[AnyID] [int] NOT NULL IDENTITY(1, 1)
)
GO
ALTER TABLE [dbo].[_RawPatch] ADD CONSTRAINT [PK__RawPatch] PRIMARY KEY CLUSTERED  ([AnyID])
GO
