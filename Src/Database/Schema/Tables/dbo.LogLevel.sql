CREATE TABLE [dbo].[LogLevel]
(
[LogLevelID] [int] NOT NULL,
[LogLevelName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[LogLevelDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[LogLevel] ADD CONSTRAINT [PK_LogLevel] PRIMARY KEY CLUSTERED  ([LogLevelID])
GO
CREATE NONCLUSTERED INDEX [IX_LogLevelName] ON [dbo].[LogLevel] ([LogLevelName])
GO
