CREATE TABLE [dbo].[ClientPersonnelAccess]
(
[ClientPersonnelAccessID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientPersonnelAdminGroupID] [int] NULL,
[ClientPersonnelID] [int] NULL,
[LeadID] [int] NOT NULL,
[CaseID] [int] NULL,
[AccessRuleID] [int] NULL,
[SourceLeadEventID] [int] NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[AccessLevel] [tinyint] NULL
)
GO
ALTER TABLE [dbo].[ClientPersonnelAccess] ADD CONSTRAINT [PK_ClientPersonnelAccess] PRIMARY KEY CLUSTERED  ([ClientPersonnelAccessID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientPersonnelAccess_GroupClient] ON [dbo].[ClientPersonnelAccess] ([ClientPersonnelAdminGroupID], [ClientID], [LeadID], [CaseID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientPersonnelAccess_UserClient] ON [dbo].[ClientPersonnelAccess] ([ClientPersonnelID], [ClientID], [LeadID], [CaseID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientPersonnelAccess_SourceID] ON [dbo].[ClientPersonnelAccess] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[ClientPersonnelAccess] ADD CONSTRAINT [FK_ClientPersonnelAccess_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[ClientPersonnelAccess] ADD CONSTRAINT [FK_ClientPersonnelAccess_ClientPersonnelAdminGroups] FOREIGN KEY ([ClientPersonnelAdminGroupID]) REFERENCES [dbo].[ClientPersonnelAdminGroups] ([ClientPersonnelAdminGroupID])
GO
ALTER TABLE [dbo].[ClientPersonnelAccess] ADD CONSTRAINT [FK_ClientPersonnelAccess_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
