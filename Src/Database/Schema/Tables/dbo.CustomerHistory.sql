CREATE TABLE [dbo].[CustomerHistory]
(
[CustomerHistoryID] [int] NOT NULL IDENTITY(1, 1),
[CustomerID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[TitleID] [int] NULL,
[IsBusiness] [bit] NOT NULL,
[FirstName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[MiddleName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[EmailAddress] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[DayTimeTelephoneNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[HomeTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MobileTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CompanyTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[WorksTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Address1] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Address2] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Town] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[County] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[PostCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Website] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[AquariumStatusID] [int] NOT NULL,
[ClientStatusID] [int] NULL,
[Test] [bit] NULL,
[CompanyName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Occupation] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Employer] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[DoNotEmail] [bit] NULL,
[DoNotSellToThirdParty] [bit] NULL,
[AgreedToTermsAndConditions] [bit] NULL,
[DateOfBirth] [datetime] NULL,
[DefaultContactID] [int] NULL,
[DefaultOfficeID] [int] NULL,
[AddressVerified] [bit] NULL,
[CountryID] [int] NULL,
[SubClientID] [int] NULL,
[CustomerRef] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[WhoChanged] [int] NULL,
[WhenChanged] [datetime] NOT NULL,
[ChangeAction] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ChangeNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Comments] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[LanguageID] [int] NULL
)
GO
ALTER TABLE [dbo].[CustomerHistory] ADD CONSTRAINT [PK_CustomerHistory] PRIMARY KEY CLUSTERED  ([CustomerHistoryID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerHistory_Customer] ON [dbo].[CustomerHistory] ([CustomerID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IS_CustomerHistory_WhenChanged_SubClient] ON [dbo].[CustomerHistory] ([WhenChanged], [SubClientID])
GO
