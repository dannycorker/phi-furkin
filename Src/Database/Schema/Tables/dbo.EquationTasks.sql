CREATE TABLE [dbo].[EquationTasks]
(
[EquationTaskID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[CaseID] [int] NULL,
[MatterID] [int] NULL,
[ContactID] [int] NULL,
[LeadTypeID] [int] NULL,
[RunAsUserID] [int] NOT NULL,
[DetailFieldID] [int] NULL,
[LockDateTime] [datetime] NULL,
[WhenAdded] [datetime] NOT NULL,
[WhenCompleted] [datetime] NULL,
[TimeToComplete] [int] NULL,
[Outcome] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[EquationTasks] ADD CONSTRAINT [PK_EquationTask] PRIMARY KEY CLUSTERED  ([EquationTaskID])
GO
