CREATE TABLE [dbo].[CheckMateFieldGroup]
(
[CheckMateFieldGroupID] [int] NOT NULL IDENTITY(1, 1),
[GroupName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[GroupDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[CheckMateFieldGroup] ADD CONSTRAINT [PK_CheckMateFieldGroup] PRIMARY KEY CLUSTERED  ([CheckMateFieldGroupID])
GO
