CREATE TABLE [dbo].[CustomerMatchKey]
(
[CustomerMatchKeyID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[FirstName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Address1] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Postcode] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[DateOfBirth] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CustomerMatchKey] ADD CONSTRAINT [PK_CustomerMatchKey] PRIMARY KEY CLUSTERED  ([CustomerMatchKeyID])
GO
