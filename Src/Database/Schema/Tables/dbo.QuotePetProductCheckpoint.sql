CREATE TABLE [dbo].[QuotePetProductCheckpoint]
(
[QuotePetProductCheckpointID] [int] NOT NULL IDENTITY(1, 1),
[QuotePetProductID] [int] NULL,
[RuleSetID] [int] NULL,
[RuleID] [int] NULL,
[RuleOrder] [int] NULL,
[CheckpointName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Input] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Output] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Delta] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Transform] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Value] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ValueDecimal] [decimal] (18, 8) NULL,
[InputDecimal] [decimal] (18, 8) NULL,
[OutputDecimal] [decimal] (18, 8) NULL,
[DeltaDecimal] [decimal] (18, 8) NULL,
[EvaluatedXml] [xml] NULL,
[WhenCreated] [datetime] NULL,
[WhoCreated] [int] NULL,
[SourceID] [int] NULL,
[ChangeNotes] [varchar] (100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[QuotePetProductCheckpoint] ADD CONSTRAINT [PK_QuotePetProductCheckpoint] PRIMARY KEY CLUSTERED  ([QuotePetProductCheckpointID])
GO
CREATE NONCLUSTERED INDEX [IX_QuotePetProductCheckpoint_QuotePetProductID] ON [dbo].[QuotePetProductCheckpoint] ([QuotePetProductID], [QuotePetProductCheckpointID])
GO
ALTER TABLE [dbo].[QuotePetProductCheckpoint] ADD CONSTRAINT [FK_QuotePetProductCheckpoint_QuotePetProduct] FOREIGN KEY ([QuotePetProductID]) REFERENCES [dbo].[QuotePetProduct] ([QuotePetProductID])
GO
