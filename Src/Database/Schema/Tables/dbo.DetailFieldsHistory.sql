CREATE TABLE [dbo].[DetailFieldsHistory]
(
[DetailFieldHistoryID] [int] NOT NULL IDENTITY(1, 1),
[DetailFieldID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[LeadOrMatter] [tinyint] NOT NULL,
[FieldName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[FieldCaption] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[QuestionTypeID] [int] NOT NULL,
[Required] [bit] NOT NULL,
[Lookup] [bit] NOT NULL,
[LookupListID] [int] NULL,
[LeadTypeID] [int] NOT NULL,
[Enabled] [bit] NULL,
[DetailFieldPageID] [int] NOT NULL,
[FieldOrder] [int] NULL,
[MaintainHistory] [bit] NOT NULL,
[EquationText] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[MasterQuestionID] [int] NULL,
[FieldSize] [int] NULL,
[LinkedDetailFieldID] [int] NULL,
[ValidationCriteriaFieldTypeID] [int] NULL,
[ValidationCriteriaID] [int] NULL,
[MinimumValue] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MaximumValue] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[RegEx] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ErrorMessage] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ResourceListDetailFieldPageID] [int] NULL,
[TableDetailFieldPageID] [int] NULL,
[DefaultFilter] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ColumnEquationText] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Editable] [bit] NULL,
[Hidden] [bit] NULL,
[LastReferenceInteger] [int] NULL,
[ReferenceValueFormatID] [int] NULL,
[Encrypt] [bit] NULL,
[ShowCharacters] [int] NULL,
[NumberOfCharactersToShow] [int] NULL,
[TableEditMode] [int] NULL,
[DisplayInTableView] [bit] NULL,
[WhoChanged] [int] NULL,
[WhenChanged] [datetime] NOT NULL,
[ChangeAction] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ChangeNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ObjectTypeID] [int] NULL
)
GO
ALTER TABLE [dbo].[DetailFieldsHistory] ADD CONSTRAINT [PK_DetailFieldsHistory] PRIMARY KEY CLUSTERED  ([DetailFieldHistoryID])
GO
CREATE NONCLUSTERED INDEX [IX_DetailFieldsHistory_Clients] ON [dbo].[DetailFieldsHistory] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_DetailFieldsHistory_DetailFields] ON [dbo].[DetailFieldsHistory] ([DetailFieldID])
GO
