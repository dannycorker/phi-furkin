CREATE TABLE [dbo].[DataLoaderFile]
(
[DataLoaderFileID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DataLoaderMapID] [int] NULL,
[FileStatusID] [int] NOT NULL,
[SourceFileNameAndPath] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[TargetFileName] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[TargetFileLocation] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[FileFormatID] [int] NOT NULL,
[ScheduledDateTime] [datetime] NULL,
[DataLoadedDateTime] [datetime] NULL,
[UploadedBy] [int] NULL,
[RowsInFile] [int] NULL
)
GO
ALTER TABLE [dbo].[DataLoaderFile] ADD CONSTRAINT [PK_DataLoaderFile] PRIMARY KEY CLUSTERED  ([DataLoaderFileID])
GO
ALTER TABLE [dbo].[DataLoaderFile] ADD CONSTRAINT [FK_DataLoaderFile_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DataLoaderFile] ADD CONSTRAINT [FK_DataLoaderFile_DataLoaderFileFormat] FOREIGN KEY ([FileFormatID]) REFERENCES [dbo].[DataLoaderFileFormat] ([DataLoaderFileFormatID])
GO
ALTER TABLE [dbo].[DataLoaderFile] ADD CONSTRAINT [FK_DataLoaderFile_DataLoaderFileStatus] FOREIGN KEY ([FileStatusID]) REFERENCES [dbo].[DataLoaderFileStatus] ([DataLoaderFileStatusID])
GO
ALTER TABLE [dbo].[DataLoaderFile] ADD CONSTRAINT [FK_DataLoaderFile_DataLoaderMap] FOREIGN KEY ([DataLoaderMapID]) REFERENCES [dbo].[DataLoaderMap] ([DataLoaderMapID])
GO
