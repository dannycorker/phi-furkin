CREATE TABLE [dbo].[PremiumFunderQuoteAgreementAdjustmentRequest]
(
[PremiumFunderQuoteAgreementAdjustmentRequestID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[MatterID] [int] NULL,
[AdjustmentAmount] [decimal] (18, 2) NULL,
[AdjustmentType] [varchar] (16) COLLATE Latin1_General_CI_AS NULL,
[EffectiveDate] [datetime] NULL,
[RequestJson] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[PremiumFunderRequestID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[PremiumFunderQuoteAgreementAdjustmentRequest] ADD CONSTRAINT [PK_PremiumFunderQuoteAgreementAdjustmentRequest] PRIMARY KEY CLUSTERED  ([PremiumFunderQuoteAgreementAdjustmentRequestID])
GO
