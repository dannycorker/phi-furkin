CREATE TABLE [dbo].[FolderType]
(
[FolderTypeID] [int] NOT NULL,
[FolderTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[FolderTypeDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[FolderType] ADD CONSTRAINT [PK_FolderType] PRIMARY KEY CLUSTERED  ([FolderTypeID])
GO
