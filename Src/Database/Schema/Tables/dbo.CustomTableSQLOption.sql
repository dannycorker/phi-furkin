CREATE TABLE [dbo].[CustomTableSQLOption]
(
[CustomTableSQLOptionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[OptionType] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[OptionValue] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[CustomTableSQLOption] ADD CONSTRAINT [PK_CustomTableSQLOption] PRIMARY KEY CLUSTERED  ([CustomTableSQLOptionID])
GO
ALTER TABLE [dbo].[CustomTableSQLOption] ADD CONSTRAINT [FK_CustomTableSQLOption_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[CustomTableSQLOption] ADD CONSTRAINT [FK_CustomTableSQLOption_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
