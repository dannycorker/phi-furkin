CREATE TABLE [dbo].[PredicateRuleAssigned]
(
[PredicateRuleAssignedID] [int] NOT NULL IDENTITY(1, 1),
[LeadTypeID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[DocumentTypeID] [int] NOT NULL,
[PredicateRuleID] [int] NOT NULL,
[RuleOrder] [int] NOT NULL,
[OverrideDocumentTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[PredicateRuleAssigned] ADD CONSTRAINT [PK_PredicateRuleAssigned] PRIMARY KEY CLUSTERED  ([PredicateRuleAssignedID])
GO
