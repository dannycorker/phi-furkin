CREATE TABLE [dbo].[CaseTransferStatus]
(
[CaseTransferStatusID] [int] NOT NULL IDENTITY(1, 1),
[Status] [varchar] (60) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[CaseTransferStatus] ADD CONSTRAINT [PK_CaseTransferStatus] PRIMARY KEY CLUSTERED  ([CaseTransferStatusID])
GO
ALTER TABLE [dbo].[CaseTransferStatus] ADD CONSTRAINT [IX_CaseTransferStatus] UNIQUE NONCLUSTERED  ([Status])
GO
