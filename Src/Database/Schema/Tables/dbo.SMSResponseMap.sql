CREATE TABLE [dbo].[SMSResponseMap]
(
[SMSResponseMapID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SMSQuestionnaireID] [int] NOT NULL,
[FromSMSQuestionGroupID] [int] NULL,
[FromSMSQuestionID] [int] NULL,
[FromAnswer] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[ToSMSQuestionGroupID] [int] NULL,
[ToSMSQuestionID] [int] NULL,
[EndOfQuestionnaire] [bit] NULL,
[Score] [int] NULL,
[ConfidenceRangeStart] [int] NULL,
[ConfidenceRangeEnd] [int] NULL,
[RuleID] [int] NULL,
[ProcessingOrder] [int] NULL
)
GO
ALTER TABLE [dbo].[SMSResponseMap] ADD CONSTRAINT [PK_SMSResponseMap] PRIMARY KEY CLUSTERED  ([SMSResponseMapID])
GO
ALTER TABLE [dbo].[SMSResponseMap] ADD CONSTRAINT [FK_SMSResponseMap_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SMSResponseMap] ADD CONSTRAINT [FK_SMSResponseMap_SMSQuestionnaire] FOREIGN KEY ([SMSQuestionnaireID]) REFERENCES [dbo].[SMSQuestionnaire] ([SMSQuestionnaireID])
GO
