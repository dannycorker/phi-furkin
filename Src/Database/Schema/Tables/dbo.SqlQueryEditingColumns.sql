CREATE TABLE [dbo].[SqlQueryEditingColumns]
(
[SqlQueryEditingColumnID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SqlQueryEditingID] [int] NOT NULL,
[SqlQueryEditingTableID] [int] NULL,
[CompleteOutputText] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[ColumnAlias] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ShowColumn] [bit] NOT NULL,
[DisplayOrder] [int] NOT NULL,
[SortOrder] [int] NULL,
[SortType] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[IsAggregate] [bit] NOT NULL,
[ColumnDataType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ManipulatedDataType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ColumnNaturalName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[RealSqlQueryColumnID] [int] NULL,
[ComplexValue] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SqlQueryEditingColumns] ADD CONSTRAINT [PK_SqlQueryEditingColumn] PRIMARY KEY CLUSTERED  ([SqlQueryEditingColumnID])
GO
CREATE NONCLUSTERED INDEX [IX_SqlQueryEditingColumns_Query] ON [dbo].[SqlQueryEditingColumns] ([SqlQueryEditingID])
GO
ALTER TABLE [dbo].[SqlQueryEditingColumns] ADD CONSTRAINT [FK_SqlQueryEditingColumn_SqlQueryEditing] FOREIGN KEY ([SqlQueryEditingID]) REFERENCES [dbo].[SqlQueryEditing] ([SqlQueryEditingID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SqlQueryEditingColumns] ADD CONSTRAINT [FK_SqlQueryEditingColumn_SqlQueryEditingTable] FOREIGN KEY ([SqlQueryEditingTableID]) REFERENCES [dbo].[SqlQueryEditingTable] ([SqlQueryEditingTableID])
GO
