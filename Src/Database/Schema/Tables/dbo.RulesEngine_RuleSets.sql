CREATE TABLE [dbo].[RulesEngine_RuleSets]
(
[RuleSetID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[InputTypeID] [int] NULL,
[OutputTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[Description] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[SourceID] [int] NULL,
[ExposeForUse] [bit] NOT NULL CONSTRAINT [DF__RulesEngi__Expos__7F4BDEC0] DEFAULT ((0)),
[RuleSetTypeID] [int] NULL,
[ChangeSetID] [int] NULL,
[ParentRuleSetID] [int] NULL
)
GO
ALTER TABLE [dbo].[RulesEngine_RuleSets] ADD CONSTRAINT [PK_RulesEngine_RuleSets] PRIMARY KEY CLUSTERED  ([RuleSetID])
GO
ALTER TABLE [dbo].[RulesEngine_RuleSets] ADD CONSTRAINT [FK_RulesEngine_RuleSets_RulesEngine_DataTypes] FOREIGN KEY ([InputTypeID]) REFERENCES [dbo].[RulesEngine_DataTypes] ([TypeID])
GO
ALTER TABLE [dbo].[RulesEngine_RuleSets] ADD CONSTRAINT [FK_RulesEngine_RuleSets_RulesEngine_DataTypes1] FOREIGN KEY ([OutputTypeID]) REFERENCES [dbo].[RulesEngine_DataTypes] ([TypeID])
GO
