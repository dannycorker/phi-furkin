CREATE TABLE [dbo].[RulesEngine_DataTypes]
(
[TypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[RulesEngine_DataTypes] ADD CONSTRAINT [PK_RulesEngine_DataTypes] PRIMARY KEY CLUSTERED  ([TypeID])
GO
