CREATE TABLE [dbo].[MailChimpCampaigns]
(
[MailChimpCampaignID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CampaignID] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[SentDate] [datetime] NULL,
[Title] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[EmailSent] [int] NOT NULL,
[Unsubscribed] [int] NULL,
[Status] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[LastCompletePoll] [datetime] NULL,
[MailchimpAccount] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[MailChimpCampaigns] ADD CONSTRAINT [PK_MailChimpCampaigns] PRIMARY KEY CLUSTERED  ([MailChimpCampaignID])
GO
CREATE NONCLUSTERED INDEX [IX_CampaignID] ON [dbo].[MailChimpCampaigns] ([CampaignID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientID] ON [dbo].[MailChimpCampaigns] ([ClientID])
GO
