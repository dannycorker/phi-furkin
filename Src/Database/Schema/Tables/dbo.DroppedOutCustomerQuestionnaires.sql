CREATE TABLE [dbo].[DroppedOutCustomerQuestionnaires]
(
[CustomerQuestionnaireID] [int] NOT NULL IDENTITY(1, 1),
[ClientQuestionnaireID] [int] NULL,
[CustomerID] [int] NULL,
[SubmissionDate] [datetime] NULL,
[TrackingID] [int] NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[DroppedOutCustomerQuestionnaires] ADD CONSTRAINT [PK_DroppedOutCustomerQuestionnaires] PRIMARY KEY CLUSTERED  ([CustomerQuestionnaireID])
GO
ALTER TABLE [dbo].[DroppedOutCustomerQuestionnaires] ADD CONSTRAINT [FK_DroppedOutCustomerQuestionnaires_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
