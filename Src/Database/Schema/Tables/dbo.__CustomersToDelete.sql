CREATE TABLE [dbo].[__CustomersToDelete]
(
[CustomerID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[__CustomersToDelete] ADD CONSTRAINT [PK___CustomersToDelete] PRIMARY KEY CLUSTERED  ([CustomerID])
GO
