CREATE TABLE [dbo].[Asset]
(
[AssetID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[AssetTypeID] [int] NOT NULL,
[AssetSubTypeID] [int] NOT NULL,
[LocationID] [int] NULL,
[AssetName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Note] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[SecureNote] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[SecureUserName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[SecurePassword] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[SecureOther] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Version] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ValidFrom] [datetime] NULL,
[ValidTo] [datetime] NULL,
[ReminderDue] [datetime] NULL,
[ReminderTimeUnitID] [int] NULL,
[ReminderTimeUnitQuantity] [int] NULL,
[ReminderNotificationGroupID] [int] NULL,
[Enabled] [bit] NOT NULL,
[Deleted] [bit] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--===================================================
-- Author:		Ian Slack
-- Create date: 2013-09-11
-- Description:	Store Asset changes into AssetHistory
-- ==================================================
CREATE TRIGGER [dbo].[trgiu_Asset] 
   ON [dbo].[Asset] 
   AFTER INSERT, UPDATE 
AS 
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF SYSTEM_USER = 'notriggers'
	BEGIN
		RETURN
	END

    /* Populate deltas in the history table */
	INSERT INTO AssetHistory
	(AssetID, ClientID, AssetTypeID, AssetSubTypeID, LocationID, AssetName, Note, SecureNote, SecureUserName, SecurePassword, SecureOther, Version, ValidFrom, ValidTo, ReminderDue, ReminderTimeUnitID, ReminderTimeUnitQuantity, ReminderNotificationGroupID, Enabled, Deleted, WhoCreated, WhenCreated, WhoModified, WhenModified)
	SELECT 
	AssetID, ClientID, AssetTypeID, AssetSubTypeID, LocationID, AssetName, Note, SecureNote, SecureUserName, SecurePassword, SecureOther, Version, ValidFrom, ValidTo, ReminderDue, ReminderTimeUnitID, ReminderTimeUnitQuantity, ReminderNotificationGroupID, Enabled, Deleted, WhoCreated, WhenCreated, WhoModified, WhenModified
	FROM inserted i	
	
END

GO
ALTER TABLE [dbo].[Asset] ADD CONSTRAINT [PK__Asset__434923720308BD91] PRIMARY KEY CLUSTERED  ([AssetID])
GO
ALTER TABLE [dbo].[Asset] ADD CONSTRAINT [FK__Asset__AssetSubT__06D94E75] FOREIGN KEY ([AssetSubTypeID]) REFERENCES [dbo].[AssetSubType] ([AssetSubTypeID])
GO
ALTER TABLE [dbo].[Asset] ADD CONSTRAINT [FK__Asset__AssetType__05E52A3C] FOREIGN KEY ([AssetTypeID]) REFERENCES [dbo].[AssetType] ([AssetTypeID])
GO
ALTER TABLE [dbo].[Asset] ADD CONSTRAINT [FK__Asset__ClientID__04F10603] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[Asset] ADD CONSTRAINT [FK__Asset__LocationI__07CD72AE] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location] ([LocationID])
GO
