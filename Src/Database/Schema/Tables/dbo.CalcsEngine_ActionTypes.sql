CREATE TABLE [dbo].[CalcsEngine_ActionTypes]
(
[ActionTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[CalcsEngine_ActionTypes] ADD CONSTRAINT [PK_CalcsEngine_ActionTypes] PRIMARY KEY CLUSTERED  ([ActionTypeID])
GO
