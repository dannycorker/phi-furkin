CREATE TABLE [dbo].[CustomerLedger]
(
[CustomerLedgerID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NULL,
[CustomerID] [int] NULL,
[EffectivePaymentDate] [datetime] NULL,
[FailureCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FailureReason] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[TransactionDate] [datetime] NULL,
[TransactionReference] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[TransactionDescription] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[TransactionNet] [numeric] (18, 2) NULL,
[TransactionVAT] [numeric] (18, 2) NULL,
[TransactionGross] [numeric] (18, 2) NULL,
[LeadEventID] [int] NULL,
[ObjectID] [int] NULL,
[ObjectTypeID] [int] NULL,
[PaymentID] [int] NULL,
[OutgoingPaymentID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[SourceID] [int] NULL
)
GO
ALTER TABLE [dbo].[CustomerLedger] ADD CONSTRAINT [PK_CustomerLedger] PRIMARY KEY CLUSTERED  ([CustomerLedgerID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerLedger_Customer] ON [dbo].[CustomerLedger] ([CustomerID], [ClientID])
GO
ALTER TABLE [dbo].[CustomerLedger] ADD CONSTRAINT [FK_CustomerLedger_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[CustomerLedger] ADD CONSTRAINT [FK_CustomerLedger_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
