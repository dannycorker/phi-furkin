CREATE TABLE [dbo].[MediateInboundClaimAudit]
(
[MediateInboundClaimAudit] [int] NOT NULL IDENTITY(1, 1),
[MediateInboundRequest] [int] NULL,
[ProcessedDateTime] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[MediateInboundClaimAudit] ADD CONSTRAINT [PK_MediateInboundClaimAudit] PRIMARY KEY CLUSTERED  ([MediateInboundClaimAudit])
GO
