CREATE TABLE [dbo].[SMSCategory]
(
[SMSCategoryID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[Category] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[SMSCategory] ADD CONSTRAINT [PK_SMSCategory] PRIMARY KEY CLUSTERED  ([SMSCategoryID])
GO
