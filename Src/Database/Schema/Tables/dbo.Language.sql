CREATE TABLE [dbo].[Language]
(
[LanguageID] [int] NOT NULL,
[LanguageName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Alpha2Code] [char] (2) COLLATE Latin1_General_CI_AS NOT NULL,
[Alpha3Code] [char] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[NativeName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[Language] ADD CONSTRAINT [PK_Language] PRIMARY KEY CLUSTERED  ([LanguageID])
GO
