CREATE TABLE [dbo].[MigrationChecksumAction]
(
[MigrationChecksumActionID] [int] NOT NULL IDENTITY(1, 1),
[MigrationTableID] [int] NOT NULL,
[MigrationTableName] [varchar] (1024) COLLATE Latin1_General_CI_AS NULL,
[ActionName] [char] (6) COLLATE Latin1_General_CI_AS NOT NULL,
[PKID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[MigrationChecksumAction] ADD CONSTRAINT [PK_MigrationChecksumAction] PRIMARY KEY CLUSTERED  ([MigrationChecksumActionID])
GO
