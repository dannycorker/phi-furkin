CREATE TABLE [dbo].[PurchasedProductPaymentSchedule]
(
[PurchasedProductPaymentScheduleID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[AccountID] [int] NOT NULL,
[PurchasedProductID] [int] NULL,
[ActualCollectionDate] [datetime] NULL,
[CoverFrom] [datetime] NULL,
[CoverTo] [datetime] NULL,
[PaymentDate] [datetime] NULL,
[PaymentNet] [numeric] (18, 2) NULL,
[PaymentVAT] [numeric] (18, 2) NULL,
[PaymentGross] [numeric] (18, 2) NULL,
[PaymentStatusID] [int] NULL,
[CustomerLedgerID] [int] NULL,
[ReconciledDate] [datetime] NULL,
[CustomerPaymentScheduleID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[PurchasedProductPaymentScheduleParentID] [int] NULL,
[PurchasedProductPaymentScheduleTypeID] [int] NULL,
[PaymentGroupedIntoID] [int] NULL,
[ContraCustomerLedgerID] [int] NULL,
[ClientAccountID] [int] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[SourceID] [int] NULL,
[TransactionFee] [numeric] (18, 2) NULL
)
GO
ALTER TABLE [dbo].[PurchasedProductPaymentSchedule] ADD CONSTRAINT [PK_ProductPaymentSchedule] PRIMARY KEY CLUSTERED  ([PurchasedProductPaymentScheduleID])
GO
CREATE NONCLUSTERED INDEX [IX_PurchasedProductPaymentSchedule_Account_CL_PS] ON [dbo].[PurchasedProductPaymentSchedule] ([AccountID], [CustomerLedgerID], [PaymentStatusID])
GO
CREATE NONCLUSTERED INDEX [IX_PurchasedProductPaymentSchedule_AccountIDCustomerLedgerIDPaymentStatusID] ON [dbo].[PurchasedProductPaymentSchedule] ([AccountID], [CustomerLedgerID], [PaymentStatusID])
GO
CREATE NONCLUSTERED INDEX [IX_PurchasedProductPaymentSchedule_PaymentGross] ON [dbo].[PurchasedProductPaymentSchedule] ([PaymentGross])
GO
CREATE NONCLUSTERED INDEX [IX_PurchasedProductPaymentSchedule_PaymentStatusID] ON [dbo].[PurchasedProductPaymentSchedule] ([PaymentStatusID]) INCLUDE ([PurchasedProductID], [PurchasedProductPaymentScheduleParentID])
GO
CREATE NONCLUSTERED INDEX [IX_PurchasedProductPaymentSchedule_PurchasedProductID] ON [dbo].[PurchasedProductPaymentSchedule] ([PurchasedProductID])
GO
CREATE NONCLUSTERED INDEX [IX_PurchasedProductPaymentSchedule_PurchasedProductPaymentScheduleParentID] ON [dbo].[PurchasedProductPaymentSchedule] ([PurchasedProductPaymentScheduleParentID])
GO
ALTER TABLE [dbo].[PurchasedProductPaymentSchedule] ADD CONSTRAINT [FK_ProductPaymentSchedule_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
GO
ALTER TABLE [dbo].[PurchasedProductPaymentSchedule] ADD CONSTRAINT [FK_ProductPaymentSchedule_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[PurchasedProductPaymentSchedule] ADD CONSTRAINT [FK_ProductPaymentSchedule_CustomerLedger] FOREIGN KEY ([CustomerLedgerID]) REFERENCES [dbo].[CustomerLedger] ([CustomerLedgerID])
GO
ALTER TABLE [dbo].[PurchasedProductPaymentSchedule] ADD CONSTRAINT [FK_ProductPaymentSchedule_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[PurchasedProductPaymentSchedule] ADD CONSTRAINT [FK_ProductPaymentSchedule_PurchasedProduct] FOREIGN KEY ([PurchasedProductID]) REFERENCES [dbo].[PurchasedProduct] ([PurchasedProductID])
GO
