CREATE TABLE [dbo].[Logs]
(
[LogID] [int] NOT NULL IDENTITY(1, 1),
[LogDateTime] [datetime] NULL,
[TypeOfLogEntry] [varchar] (6) COLLATE Latin1_General_CI_AS NULL,
[ClassName] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[MethodName] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[LogEntry] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ClientPersonnelID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[trgi_Logs] 
   ON [dbo].[Logs] 
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT * FROM inserted WHERE LogEntry LIKE 'Incorrect Login Attempt from%')
	BEGIN
		UPDATE l 
		SET LogEntry = LEFT(i.LogEntry, CHARINDEX(' and password', i.LogEntry, 1)) 
		FROM dbo.Logs l 
		INNER JOIN inserted i ON i.LogID = l.LogID 
		WHERE i.LogEntry LIKE 'Incorrect Login Attempt from%' 
	END
	
END

GO
ALTER TABLE [dbo].[Logs] ADD CONSTRAINT [PK_Logs] PRIMARY KEY CLUSTERED  ([LogID])
GO
CREATE NONCLUSTERED INDEX [IX_Logs_LogDateTime] ON [dbo].[Logs] ([LogDateTime], [TypeOfLogEntry], [ClassName])
GO
CREATE NONCLUSTERED INDEX [IX_Logs_Type] ON [dbo].[Logs] ([TypeOfLogEntry])
GO
