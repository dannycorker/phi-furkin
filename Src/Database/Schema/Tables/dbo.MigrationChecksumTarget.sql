CREATE TABLE [dbo].[MigrationChecksumTarget]
(
[MigrationChecksumTargetID] [int] NOT NULL,
[MigrationTableID] [int] NOT NULL,
[MigrationTableName] [varchar] (1024) COLLATE Latin1_General_CI_AS NULL,
[TargetPKID] [int] NOT NULL,
[TargetChecksumValue] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[MigrationChecksumTarget] ADD CONSTRAINT [PK_MigrationChecksumTarget] PRIMARY KEY CLUSTERED  ([MigrationChecksumTargetID])
GO
