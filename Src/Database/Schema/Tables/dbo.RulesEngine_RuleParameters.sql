CREATE TABLE [dbo].[RulesEngine_RuleParameters]
(
[RuleParameterID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[RuleID] [int] NOT NULL,
[ParameterTypeID] [int] NOT NULL,
[Value] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DataTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[Parent] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SourceID] [int] NULL,
[RuleParameterPreSetID] [int] NULL
)
GO
ALTER TABLE [dbo].[RulesEngine_RuleParameters] ADD CONSTRAINT [PK_RulesEngine_RuleParameters] PRIMARY KEY CLUSTERED  ([RuleParameterID])
GO
CREATE NONCLUSTERED INDEX [IX_RulesEngine_RuleParameters_ParameterTypeID_RuleID] ON [dbo].[RulesEngine_RuleParameters] ([RuleID], [ParameterTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_RulesEngine_RuleParameters_Rule] ON [dbo].[RulesEngine_RuleParameters] ([RuleID], [ParameterTypeID]) INCLUDE ([DataTypeID], [Name], [Parent], [Value])
GO
CREATE NONCLUSTERED INDEX [IX_RulesEngine_RuleParameters_Value] ON [dbo].[RulesEngine_RuleParameters] ([Value])
GO
ALTER TABLE [dbo].[RulesEngine_RuleParameters] ADD CONSTRAINT [FK_RulesEngine_RuleParameters_RulesEngine_ParameterTypes] FOREIGN KEY ([ParameterTypeID]) REFERENCES [dbo].[RulesEngine_ParameterTypes] ([ParameterTypeID])
GO
ALTER TABLE [dbo].[RulesEngine_RuleParameters] ADD CONSTRAINT [FK_RulesEngine_RuleParameters_RulesEngine_Rules] FOREIGN KEY ([RuleID]) REFERENCES [dbo].[RulesEngine_Rules] ([RuleID])
GO
