CREATE TABLE [dbo].[PortalOption]
(
[PortalOptionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ShowHelpLink] [bit] NULL,
[HelpLinkCaption] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[HelpLinkURL] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[LeadListShowAssignedTo] [bit] NULL,
[LeadListShowAssignedToCaption] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LeadListShowCaseStatus] [bit] NULL,
[LeadListShowCaseStatusCaption] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PortalOption] ADD CONSTRAINT [PK_PortalOption] PRIMARY KEY CLUSTERED  ([PortalOptionID])
GO
ALTER TABLE [dbo].[PortalOption] ADD CONSTRAINT [FK_PortalOption_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
