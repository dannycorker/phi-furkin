CREATE TABLE [dbo].[Clients]
(
[ClientID] [int] NOT NULL IDENTITY(1, 1),
[CompanyName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[WebAddress] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[IPAddress] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DefaultEmailAddress] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ClientTypeID] [int] NULL,
[AllowSMS] [bit] NULL CONSTRAINT [DF__Clients__AllowSM__49E3F248] DEFAULT ((0)),
[SecurityCode] [varchar] (36) COLLATE Latin1_General_CI_AS NULL,
[LeadsBelongToOffices] [bit] NULL,
[UseEventCosts] [bit] NULL,
[UseEventUOEs] [bit] NULL,
[UseEventDisbursements] [bit] NULL,
[UseEventComments] [bit] NULL,
[VerifyAddress] [bit] NULL,
[UseTapi] [bit] NULL,
[UseRPI] [bit] NULL,
[UseGBAddress] [bit] NULL,
[UsePinpoint] [bit] NULL,
[UseSage] [bit] NULL,
[UseSAS] [bit] NULL,
[UseCreditCalculation] [bit] NULL,
[FollowupWorkingDaysOnly] [bit] NULL,
[AddLeadByQuestionnaire] [bit] NULL,
[UseIncendia] [bit] NULL,
[LanguageID] [int] NULL,
[CountryID] [int] NULL,
[UseMobileInterface] [bit] NULL,
[UseGBValidation] [bit] NULL,
[CurrencyID] [int] NULL,
[AllowSmsCommandProcessing] [bit] NULL,
[TimeZoneID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2008-07-11
-- Description:	Create a unique security code for each new Client
--              This will be used on the logon screen, along with
--              the client id, to show the custom branding for
--              that client before the user has even logged on.
-- JG 2008-08-12 This has now been recoded to use a GUID for more security.
-- JG 2012-12-14 Temporary reminder to use the AquariusAlpha from now on.  Please review/remove this code in Jan 2013.
-- =============================================
CREATE TRIGGER [dbo].[trgi_clients]
   ON [dbo].[Clients]
   AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	-- Allow special user 'notriggers' to bypass trigger code
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END

	DECLARE @ClientList TABLE (
		ClientID int
	)

	DECLARE @ClientID int, @rnd varchar(36)

	-- Look for all new records (might have inserted lots at once)
	INSERT INTO @ClientList (ClientID)
	SELECT ClientID
	FROM Inserted

	SELECT TOP 1 @ClientID = ClientID
	FROM @ClientList

	WHILE @ClientID > -1
	BEGIN

		-- Generate a GUID as a security code for each new client:
		SELECT @rnd = newid()

		-- Update the client record
		UPDATE Clients
		SET SecurityCode = @rnd
		FROM Clients c
		WHERE c.ClientID = @ClientID

		-- Remove this client from the list of inserts
		DELETE @ClientList
		WHERE ClientID = @ClientID

		-- Prevent infinite loop
		SELECT @ClientID = -1

		-- Get the next clientid, if there is one
		SELECT TOP 1 @ClientID = ClientID
		FROM @ClientList

	END

END



GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2009-04-06
-- Description:	Capture changes and store in History table
-- =============================================
CREATE TRIGGER [dbo].[trgiud_Clients]
   ON  [dbo].[Clients]
   AFTER INSERT,DELETE,UPDATE
AS
BEGIN
	SET NOCOUNT ON;

    -- Inserts appear in the inserted table,
    -- Deletes appear in the deleted table,
    -- and Updates appear in both tables at once identically.
    -- These are all 'logical' Sql Server tables.
	INSERT INTO dbo.ClientsHistory(
		ClientID,
		CompanyName,
		WebAddress,
		IPAddress,
		DefaultEmailAddress,
		ClientTypeID,
		AllowSMS,
		SecurityCode,
		LeadsBelongToOffices,
		UseEventCosts,
		UseEventUOEs,
		UseEventDisbursements,
		UseEventComments,
		WhoChanged,
		WhenChanged,
		ChangeAction,
		ChangeNotes,
		VerifyAddress,
		UseTapi,
		UseRPI,
		UseGBAddress,
		UsePinpoint,
		UseSage,
		UseSAS,
		UseCreditCalculation,
		FollowupWorkingDaysOnly,
		AddLeadByQuestionnaire,
		UseIncendia,
		LanguageID,
		CountryID,
		UseMobileInterface
	)
	SELECT
		COALESCE(i.ClientID, d.ClientID),
		COALESCE(i.CompanyName, d.CompanyName),
		COALESCE(i.WebAddress, d.WebAddress),
		COALESCE(i.IPAddress, d.IPAddress),
		COALESCE(i.DefaultEmailAddress, d.DefaultEmailAddress),
		COALESCE(i.ClientTypeID, d.ClientTypeID),
		COALESCE(i.AllowSMS, d.AllowSMS),
		COALESCE(i.SecurityCode, d.SecurityCode),
		COALESCE(i.LeadsBelongToOffices, d.LeadsBelongToOffices),
		COALESCE(i.UseEventCosts, d.UseEventCosts),
		COALESCE(i.UseEventUOEs, d.UseEventUOEs),
		COALESCE(i.UseEventDisbursements, d.UseEventDisbursements),
		COALESCE(i.UseEventComments, d.UseEventComments),
		NULL, --i.WhoChanged is not available yet
		dbo.fn_GetDate_Local(),
		CASE WHEN d.ClientID IS NULL THEN 'Insert' WHEN i.ClientID IS NULL THEN 'Delete' ELSE 'Update' END,
		dbo.fnGetUserInfo(),
		COALESCE(i.VerifyAddress, d.VerifyAddress),
		COALESCE(i.UseTapi, d.UseTapi),
		COALESCE(i.UseRPI, d.UseRPI),
		COALESCE(i.UseGBAddress, d.UseGBAddress),
		COALESCE(i.UsePinpoint, d.UsePinpoint),
		COALESCE(i.UseSage, d.UseSage),
		COALESCE(i.UseSAS, d.UseSAS),
		COALESCE(i.UseCreditCalculation, d.UseCreditCalculation),
		COALESCE(i.FollowupWorkingDaysOnly, d.FollowupWorkingDaysOnly),
		COALESCE(i.AddLeadByQuestionnaire, d.AddLeadByQuestionnaire),
		COALESCE(i.UseIncendia, d.UseIncendia),
		COALESCE(i.LanguageID, d.LanguageID),
		COALESCE(i.CountryID, d.CountryID),
		COALESCE(i.UseMobileInterface, d.UseMobileInterface)
	FROM inserted i
	FULL OUTER JOIN deleted d ON i.ClientID = d.ClientID

END


GO
ALTER TABLE [dbo].[Clients] ADD CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED  ([ClientID])
GO
ALTER TABLE [dbo].[Clients] ADD CONSTRAINT [FK__Clients__Currenc__53AEEA52] FOREIGN KEY ([CurrencyID]) REFERENCES [dbo].[Currency] ([CurrencyID])
GO
ALTER TABLE [dbo].[Clients] ADD CONSTRAINT [FK_Clients_ClientTypes] FOREIGN KEY ([ClientTypeID]) REFERENCES [dbo].[ClientTypes] ([ClientTypeID])
GO
