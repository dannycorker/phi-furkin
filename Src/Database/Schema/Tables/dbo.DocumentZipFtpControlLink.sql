CREATE TABLE [dbo].[DocumentZipFtpControlLink]
(
[DocumentZipFtpControlLinkID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DocumentZipFtpControlID] [int] NOT NULL,
[DocumentTypeID] [int] NOT NULL,
[LinkNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Enabled] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[DocumentZipFtpControlLink] ADD CONSTRAINT [PK_DocumentZipFtpControlLink] PRIMARY KEY CLUSTERED  ([DocumentZipFtpControlLinkID])
GO
ALTER TABLE [dbo].[DocumentZipFtpControlLink] ADD CONSTRAINT [FK_DocumentZipFtpControlLink_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DocumentZipFtpControlLink] ADD CONSTRAINT [FK_DocumentZipFtpControlLink_DocumentType] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentType] ([DocumentTypeID])
GO
ALTER TABLE [dbo].[DocumentZipFtpControlLink] ADD CONSTRAINT [FK_DocumentZipFtpControlLink_DocumentZipFtpControl] FOREIGN KEY ([DocumentZipFtpControlID]) REFERENCES [dbo].[DocumentZipFtpControl] ([DocumentZipFtpControlID])
GO
