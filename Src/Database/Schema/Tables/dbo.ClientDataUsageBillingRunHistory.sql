CREATE TABLE [dbo].[ClientDataUsageBillingRunHistory]
(
[ClientDataUsageBillingRunID] [int] NOT NULL,
[WhoArchived] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenArchived] [datetime2] (0) NOT NULL,
[WhoStarted] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenStarted] [datetime2] (0) NOT NULL,
[WhenFinished] [datetime2] (0) NULL,
[TimeInSeconds] [int] NULL,
[CurrentStage] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[UserNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ClientDataUsageBillingRunHistory] ADD CONSTRAINT [PK_ClientDataUsageBillingRunHistory] PRIMARY KEY CLUSTERED  ([ClientDataUsageBillingRunID])
GO
