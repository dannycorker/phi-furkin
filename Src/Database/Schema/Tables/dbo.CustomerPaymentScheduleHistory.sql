CREATE TABLE [dbo].[CustomerPaymentScheduleHistory]
(
[CustomerPaymentScheduleHistoryID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[AccountID] [int] NOT NULL,
[ActualCollectionDate] [datetime] NULL,
[PaymentDate] [datetime] NULL,
[PaymentNet] [numeric] (18, 2) NULL,
[PaymentVAT] [numeric] (18, 2) NULL,
[PaymentGross] [numeric] (18, 2) NULL,
[PaymentStatusID] [int] NULL,
[CustomerLedgerID] [int] NULL,
[ReconciledDate] [datetime] NULL,
[OriginalCustomerPaymentScheduleID] [int] NULL,
[OriginalWhoCreated] [int] NOT NULL,
[OriginalWhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[SourceID] [int] NULL,
[RelatedObjectID] [int] NULL,
[RelatedObjectTypeID] [int] NULL,
[ClientAccountID] [int] NULL
)
GO
ALTER TABLE [dbo].[CustomerPaymentScheduleHistory] ADD CONSTRAINT [PK_CustomerPaymentScheduleHistory_1] PRIMARY KEY CLUSTERED  ([CustomerPaymentScheduleHistoryID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerPaymentScheduleHistory_Customer] ON [dbo].[CustomerPaymentScheduleHistory] ([CustomerID], [ClientID])
GO
ALTER TABLE [dbo].[CustomerPaymentScheduleHistory] ADD CONSTRAINT [FK_CustomerPaymentScheduleHistory_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
GO
ALTER TABLE [dbo].[CustomerPaymentScheduleHistory] ADD CONSTRAINT [FK_CustomerPaymentScheduleHistory_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[CustomerPaymentScheduleHistory] ADD CONSTRAINT [FK_CustomerPaymentScheduleHistory_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
