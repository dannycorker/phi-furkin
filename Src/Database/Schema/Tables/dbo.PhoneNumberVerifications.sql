CREATE TABLE [dbo].[PhoneNumberVerifications]
(
[PhoneNumberVerificationID] [int] NOT NULL IDENTITY(1, 1),
[ClientQuestionnaireID] [int] NULL,
[CustomerID] [int] NULL,
[SkyNetID] [varchar] (25) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PhoneNumberVerifications] ADD CONSTRAINT [PK_PhoneNumberVerifications] PRIMARY KEY CLUSTERED  ([PhoneNumberVerificationID])
GO
