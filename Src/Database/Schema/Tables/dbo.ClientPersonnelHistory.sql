CREATE TABLE [dbo].[ClientPersonnelHistory]
(
[ClientPersonnelHistoryID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelID] [int] NOT NULL,
[PasswordChanged] [bit] NOT NULL,
[WhoChanged] [int] NULL,
[WhenChanged] [datetime] NOT NULL,
[ChangeAction] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ChangeNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ClientPersonnelHistory] ADD CONSTRAINT [PK_ClientPersonnelHistory] PRIMARY KEY CLUSTERED  ([ClientPersonnelHistoryID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientPersonnelHistory_ClientPersonnel] ON [dbo].[ClientPersonnelHistory] ([ClientPersonnelID])
GO
