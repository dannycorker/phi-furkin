CREATE TABLE [dbo].[RPIField]
(
[RPIFieldID] [int] NOT NULL,
[RPIFieldName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[RPIFieldDescription] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[IsEnabled] [bit] NOT NULL,
[QuestionTypeID] [int] NOT NULL,
[DetailFieldSubtypeID] [tinyint] NOT NULL,
[LookupListID] [int] NULL,
[FieldOrder] [int] NULL,
[FieldSize] [int] NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[RPIFieldGroupID] [int] NULL
)
GO
ALTER TABLE [dbo].[RPIField] ADD CONSTRAINT [PK_RPIField] PRIMARY KEY CLUSTERED  ([RPIFieldID])
GO
ALTER TABLE [dbo].[RPIField] ADD CONSTRAINT [FK_RPIField_DetailFieldSubType] FOREIGN KEY ([DetailFieldSubtypeID]) REFERENCES [dbo].[DetailFieldSubType] ([DetailFieldSubTypeID])
GO
ALTER TABLE [dbo].[RPIField] ADD CONSTRAINT [FK_RPIField_LookupList] FOREIGN KEY ([LookupListID]) REFERENCES [dbo].[LookupList] ([LookupListID])
GO
ALTER TABLE [dbo].[RPIField] ADD CONSTRAINT [FK_RPIField_QuestionTypes] FOREIGN KEY ([QuestionTypeID]) REFERENCES [dbo].[QuestionTypes] ([QuestionTypeID])
GO
ALTER TABLE [dbo].[RPIField] ADD CONSTRAINT [FK_RPIField_RPIFieldGroup] FOREIGN KEY ([RPIFieldGroupID]) REFERENCES [dbo].[RPIFieldGroup] ([RPIFieldGroupID])
GO
