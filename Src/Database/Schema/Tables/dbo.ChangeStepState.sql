CREATE TABLE [dbo].[ChangeStepState]
(
[ChangeStepStateID] [int] NOT NULL IDENTITY(1, 1),
[StateDescription] [varchar] (255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ChangeStepState] ADD CONSTRAINT [PK__ChangeSt__145024BB51D34EF2] PRIMARY KEY CLUSTERED  ([ChangeStepStateID])
GO
