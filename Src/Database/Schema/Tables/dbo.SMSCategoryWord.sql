CREATE TABLE [dbo].[SMSCategoryWord]
(
[SMSCategoryWordID] [int] NOT NULL,
[SMSCategoryID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[WordOrPhrase] [nvarchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[SMSCategoryWord] ADD CONSTRAINT [PK_SMSCategoryWord] PRIMARY KEY CLUSTERED  ([SMSCategoryWordID])
GO
ALTER TABLE [dbo].[SMSCategoryWord] ADD CONSTRAINT [FK_SMSCategoryWord_SMSCategory] FOREIGN KEY ([SMSCategoryID]) REFERENCES [dbo].[SMSCategory] ([SMSCategoryID])
GO
