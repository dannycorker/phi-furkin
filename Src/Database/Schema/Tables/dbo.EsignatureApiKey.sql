CREATE TABLE [dbo].[EsignatureApiKey]
(
[EsignatureApiKeyID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ApiKey] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[EsignatureApiKey] ADD CONSTRAINT [PK_EsignatureApiKey] PRIMARY KEY CLUSTERED  ([EsignatureApiKeyID])
GO
ALTER TABLE [dbo].[EsignatureApiKey] ADD CONSTRAINT [FK_EsignatureApiKey_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
