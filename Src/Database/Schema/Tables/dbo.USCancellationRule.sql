CREATE TABLE [dbo].[USCancellationRule]
(
[USCancellationRuleID] [int] NOT NULL IDENTITY(1, 1),
[State] [varchar] (6) COLLATE Latin1_General_CI_AS NOT NULL,
[UWPeriodDays] [int] NOT NULL,
[GraceDayStart] [int] NOT NULL,
[GraceDayEnd] [int] NULL,
[IsRenewal] [bit] NOT NULL,
[CustomerCancelNonPay] [int] NULL,
[CompanyCancelOther] [int] NULL,
[CancellationOptionInsured] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[CancellationOptionCompany] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[CancellationOptionEquity] [varchar] (100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[USCancellationRule] ADD CONSTRAINT [USCancellationRuleID] PRIMARY KEY CLUSTERED  ([USCancellationRuleID])
GO
