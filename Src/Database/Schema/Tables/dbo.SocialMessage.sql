CREATE TABLE [dbo].[SocialMessage]
(
[SocialMessageID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SocialTypeID] [int] NOT NULL,
[MessageID] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Recipient] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[RecipientID] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[RecipientScreenName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[SenderSocialCustomerID] [int] NOT NULL,
[SenderCustomerID] [int] NOT NULL,
[Sender] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[SenderID] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[SenderScreenName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[MessageText] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[LeadEventID] [int] NOT NULL,
[LeadDocumentID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[SocialMessage] ADD CONSTRAINT [PK_SocialMessage] PRIMARY KEY CLUSTERED  ([SocialMessageID])
GO
ALTER TABLE [dbo].[SocialMessage] ADD CONSTRAINT [FK_SocialMessage_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SocialMessage] ADD CONSTRAINT [FK_SocialMessage_Customers] FOREIGN KEY ([SenderCustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[SocialMessage] ADD CONSTRAINT [FK_SocialMessage_SocialCustomer] FOREIGN KEY ([SenderSocialCustomerID]) REFERENCES [dbo].[SocialCustomer] ([SocialCustomerID])
GO
ALTER TABLE [dbo].[SocialMessage] ADD CONSTRAINT [FK_SocialMessage_SocialType] FOREIGN KEY ([SocialTypeID]) REFERENCES [dbo].[SocialType] ([SocialTypeID])
GO
