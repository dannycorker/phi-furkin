CREATE TABLE [dbo].[_C00_ClientReportTableMember]
(
[ClientReportTableMemberID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[TableName] [varchar] (1024) COLLATE Latin1_General_CI_AS NOT NULL,
[ColumnName] [varchar] (1024) COLLATE Latin1_General_CI_AS NULL,
[DetailFieldID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime2] (0) NOT NULL
)
GO
