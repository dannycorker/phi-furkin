CREATE TABLE [dbo].[VariableHeaderDataLoad]
(
[VariableHeaderDataLoadID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ListID] [int] NOT NULL,
[ImportID] [int] NOT NULL,
[TokenKey] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[CallbackMethod] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ActionToPerform] [int] NULL,
[DeleteListRecords] [bit] NULL,
[EmailFailureReport] [bit] NULL,
[EmailAddress] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[FileName] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[WhoUploaded] [int] NOT NULL,
[DateTimeOfUpload] [datetime] NOT NULL,
[Processed] [bit] NULL,
[Notes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[DateTimeStart] [datetime] NULL,
[DateTimeEnd] [datetime] NULL,
[LeadTypeID] [int] NULL
)
GO
ALTER TABLE [dbo].[VariableHeaderDataLoad] ADD CONSTRAINT [PK_VariableHeaderDataLoad] PRIMARY KEY CLUSTERED  ([VariableHeaderDataLoadID])
GO
ALTER TABLE [dbo].[VariableHeaderDataLoad] ADD CONSTRAINT [FK_VariableHeaderDataLoad_ClientPersonnel] FOREIGN KEY ([WhoUploaded]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[VariableHeaderDataLoad] ADD CONSTRAINT [FK_VariableHeaderDataLoad_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
