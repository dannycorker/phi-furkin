CREATE TABLE [dbo].[SqlQueryEditing]
(
[SqlQueryEditingID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SqlQueryID] [int] NULL,
[WhoIsEditing] [int] NOT NULL,
[EditStartedAt] [datetime] NOT NULL,
[QueryText] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[QueryTitle] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[LeadTypeID] [int] NULL,
[FolderID] [int] NULL,
[ParentQueryID] [int] NULL,
[LockAllTables] [bit] NULL
)
GO
ALTER TABLE [dbo].[SqlQueryEditing] ADD CONSTRAINT [PK_SqlQueryEditingControl] PRIMARY KEY CLUSTERED  ([SqlQueryEditingID])
GO
ALTER TABLE [dbo].[SqlQueryEditing] ADD CONSTRAINT [FK_SqlQueryEditingControl_ClientPersonnel] FOREIGN KEY ([WhoIsEditing]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[SqlQueryEditing] ADD CONSTRAINT [FK_SqlQueryEditingControl_SqlQuery] FOREIGN KEY ([SqlQueryID]) REFERENCES [dbo].[SqlQuery] ([QueryID])
GO
