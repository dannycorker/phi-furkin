CREATE TABLE [dbo].[CallbackQueue]
(
[CallbackQueueID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NULL,
[CallSid] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[FromPhoneNumber] [varchar] (25) COLLATE Latin1_General_CI_AS NOT NULL,
[DateCreated] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[CallbackQueue] ADD CONSTRAINT [PK_CallBackQueue] PRIMARY KEY CLUSTERED  ([CallbackQueueID])
GO
