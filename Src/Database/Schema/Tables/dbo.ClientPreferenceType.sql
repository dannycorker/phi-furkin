CREATE TABLE [dbo].[ClientPreferenceType]
(
[ClientPreferenceTypeID] [int] NOT NULL IDENTITY(1, 1),
[PreferenceTypeName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ClientPreferenceType] ADD CONSTRAINT [PK_ClientPreferenceType] PRIMARY KEY CLUSTERED  ([ClientPreferenceTypeID])
GO
