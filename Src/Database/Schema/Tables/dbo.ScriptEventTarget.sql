CREATE TABLE [dbo].[ScriptEventTarget]
(
[ScriptEventTargetID] [int] NOT NULL IDENTITY(1, 1),
[ScriptID] [int] NOT NULL,
[SectionID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[Target] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[EventTypeID] [int] NOT NULL,
[EventTypeName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[EventSubtypeID] [int] NOT NULL,
[Format] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ScriptEventTarget] ADD CONSTRAINT [PK_ScriptEventTarget] PRIMARY KEY CLUSTERED  ([ScriptEventTargetID])
GO
ALTER TABLE [dbo].[ScriptEventTarget] ADD CONSTRAINT [FK_ScriptEventTarget_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ScriptEventTarget] ADD CONSTRAINT [FK_ScriptEventTarget_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[ScriptEventTarget] ADD CONSTRAINT [FK_ScriptEventTarget_Script] FOREIGN KEY ([ScriptID]) REFERENCES [dbo].[Script] ([ScriptID])
GO
ALTER TABLE [dbo].[ScriptEventTarget] ADD CONSTRAINT [FK_ScriptEventTarget_ScriptEventTarget] FOREIGN KEY ([ScriptEventTargetID]) REFERENCES [dbo].[ScriptEventTarget] ([ScriptEventTargetID])
GO
ALTER TABLE [dbo].[ScriptEventTarget] ADD CONSTRAINT [FK_ScriptEventTarget_ScriptSection] FOREIGN KEY ([SectionID]) REFERENCES [dbo].[ScriptSection] ([ScriptSectionID])
GO
