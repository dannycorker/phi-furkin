CREATE TABLE [dbo].[EventTypeMandatoryField]
(
[EventTypeMandatoryFieldID] [int] NOT NULL IDENTITY(1, 1),
[EventTypeID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[ForceEditableOverride] [bit] NULL
)
GO
ALTER TABLE [dbo].[EventTypeMandatoryField] ADD CONSTRAINT [PK_EventTypeMandatoryField] PRIMARY KEY CLUSTERED  ([EventTypeMandatoryFieldID])
GO
CREATE NONCLUSTERED INDEX [IX_EventType_DetailFieldID] ON [dbo].[EventTypeMandatoryField] ([EventTypeID], [DetailFieldID])
GO
ALTER TABLE [dbo].[EventTypeMandatoryField] ADD CONSTRAINT [FK_EventTypeMandatoryField_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EventTypeMandatoryField] ADD CONSTRAINT [FK_EventTypeMandatoryField_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EventTypeMandatoryField] ADD CONSTRAINT [FK_EventTypeMandatoryField_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[EventTypeMandatoryField] ADD CONSTRAINT [FK_EventTypeMandatoryField_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
