CREATE TABLE [dbo].[TableRows]
(
[TableRowID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadID] [int] NULL,
[MatterID] [int] NULL,
[DetailFieldID] [int] NULL,
[DetailFieldPageID] [int] NULL,
[DenyEdit] [bit] NULL,
[DenyDelete] [bit] NULL,
[CustomerID] [int] NULL,
[CaseID] [int] NULL,
[ClientPersonnelID] [int] NULL,
[ContactID] [int] NULL,
[SourceID] [int] NULL
)
GO
ALTER TABLE [dbo].[TableRows] ADD CONSTRAINT [PK_TableRows] PRIMARY KEY CLUSTERED  ([TableRowID])
GO
CREATE NONCLUSTERED INDEX [IX_TableRowsCase] ON [dbo].[TableRows] ([CaseID])
GO
CREATE NONCLUSTERED INDEX [IX_TableRowsClient] ON [dbo].[TableRows] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_TableRowsCustomer] ON [dbo].[TableRows] ([CustomerID])
GO
CREATE NONCLUSTERED INDEX [IX_TableRows_Field2017] ON [dbo].[TableRows] ([DetailFieldID], [ClientID]) INCLUDE ([CaseID], [CustomerID], [LeadID], [MatterID])
GO
CREATE NONCLUSTERED INDEX [IX_TableRowsLeadField2018] ON [dbo].[TableRows] ([LeadID], [DetailFieldID]) INCLUDE ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_TableRowsMatter] ON [dbo].[TableRows] ([MatterID])
GO
CREATE NONCLUSTERED INDEX [IX_TableRowsMatterClient2017] ON [dbo].[TableRows] ([MatterID], [ClientID], [DetailFieldID])
GO
CREATE NONCLUSTERED INDEX [IX_TableRows_SourceID] ON [dbo].[TableRows] ([SourceID]) WHERE ([SourceID]>(0))
GO
