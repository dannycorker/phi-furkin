CREATE TABLE [dbo].[SMSQuestionOrder]
(
[SMSQuestionOrderID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[SMSQuestionOrder] ADD CONSTRAINT [PK_SMSQuestionOrder] PRIMARY KEY CLUSTERED  ([SMSQuestionOrderID])
GO
