CREATE TABLE [dbo].[EventTypeOption]
(
[EventTypeOptionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[AquariumOptionID] [int] NOT NULL,
[OptionValue] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ValueInt] [int] NULL,
[ValueMoney] [money] NULL,
[ValueDate] [date] NULL,
[ValueDateTime] [datetime2] (0) NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[SourceID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Aaran Gravestock
-- Create date: 2015-06-05
-- Description:	Store EventTypeOptions as specific datatypes
-- =============================================
CREATE TRIGGER [dbo].[trgiu_EventTypeOption]
   ON [dbo].[EventTypeOption]
   AFTER INSERT, UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END


	/*
		If the DetailValue column has been updated then fire the trigger,
		otherwise we are here again because the trigger has just fired,
		in which case do nothing.
	*/
	IF UPDATE(OptionValue)
	BEGIN
		/* Populate ValueInt et al */
		UPDATE dbo.EventTypeOption
		SET ValueInt = dbo.fnValueAsIntBitsIncluded(i.OptionValue),
		ValueMoney = CASE WHEN dbo.fnIsMoney(i.OptionValue) = 1 THEN convert(money, i.OptionValue) ELSE NULL END,
		ValueDate = CASE WHEN dbo.fnIsDateTime(i.OptionValue) = 1 THEN convert(date, i.OptionValue) ELSE NULL END,
		ValueDateTime = CASE WHEN dbo.fnIsDateTime(i.OptionValue) = 1 THEN convert(datetime2, i.OptionValue) ELSE NULL END
		FROM dbo.EventTypeOption eto
		INNER JOIN inserted i ON i.EventTypeOptionID = eto.EventTypeOptionID
		
	END
	
END
GO
ALTER TABLE [dbo].[EventTypeOption] ADD CONSTRAINT [PK_EventTypeOption] PRIMARY KEY CLUSTERED  ([EventTypeOptionID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_EventTypeOption_EventTypeAquariumOption] ON [dbo].[EventTypeOption] ([EventTypeID], [AquariumOptionID])
GO
ALTER TABLE [dbo].[EventTypeOption] ADD CONSTRAINT [FK_EventTypeOption_AquariumOption] FOREIGN KEY ([AquariumOptionID]) REFERENCES [dbo].[AquariumOption] ([AquariumOptionID])
GO
ALTER TABLE [dbo].[EventTypeOption] ADD CONSTRAINT [FK_EventTypeOption_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EventTypeOption] ADD CONSTRAINT [FK_EventTypeOption_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
