CREATE TABLE [dbo].[SecureCoRequest]
(
[SecureCoRequestID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[MetaData] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[RequestJson] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[RequestType] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[SecureCoRequest] ADD CONSTRAINT [PK_SecureCoRequest] PRIMARY KEY CLUSTERED  ([SecureCoRequestID])
GO
