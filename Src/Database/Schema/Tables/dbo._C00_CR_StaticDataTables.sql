CREATE TABLE [dbo].[_C00_CR_StaticDataTables]
(
[StaticDataTableID] [int] NOT NULL IDENTITY(1, 1),
[ProcessingSequence] [int] NOT NULL,
[TableName] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[IdentityColumn] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[HintColumn] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[Hint2Column] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[AllowDelete] [bit] NOT NULL,
[Enabled] [bit] NOT NULL,
[EnabledNote] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[_C00_CR_StaticDataTables] ADD CONSTRAINT [PK__C00_StaticDataTable] PRIMARY KEY CLUSTERED  ([StaticDataTableID])
GO
ALTER TABLE [dbo].[_C00_CR_StaticDataTables] ADD CONSTRAINT [UQ___C00_CR___C52F2382187915EB] UNIQUE NONCLUSTERED  ([ProcessingSequence])
GO
