CREATE TABLE [dbo].[DisplayPosition]
(
[DisplayPositionNum] [smallint] NOT NULL,
[DisplayPositionName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[DisplayPosition] ADD CONSTRAINT [PK_DisplayPosition] PRIMARY KEY CLUSTERED  ([DisplayPositionNum])
GO
