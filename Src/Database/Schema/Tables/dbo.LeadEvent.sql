CREATE TABLE [dbo].[LeadEvent]
(
[LeadEventID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NULL,
[Cost] [money] NULL,
[Comments] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[EventTypeID] [int] NULL,
[NoteTypeID] [int] NULL,
[FollowupDateTime] [datetime] NULL,
[WhenFollowedUp] [datetime] NULL,
[AquariumEventType] [int] NULL,
[NextEventID] [int] NULL,
[CaseID] [int] NULL,
[LeadDocumentID] [int] NULL,
[NotePriority] [int] NULL,
[DocumentQueueID] [int] NULL,
[EventDeleted] [bit] NOT NULL,
[WhoDeleted] [int] NULL,
[DeletionComments] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ContactID] [int] NULL,
[BaseCost] [money] NULL,
[DisbursementCost] [money] NULL,
[DisbursementDescription] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[ChargeOutRate] [money] NULL,
[UnitsOfEffort] [int] NULL,
[CostEnteredManually] [bit] NULL,
[IsOnHold] [bit] NULL,
[HoldLeadEventID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-02-21
-- Description:	Check for new events that might affect WorkflowTask records
--              Also maintain records in the LeadEventThreadCompletion table.
--              JWG 2009-04-24 Logging added.
--              JWG 2010-02-24 LatestLeadEvents added. 
--              JWG 2010-12-02 Speed up. Tidy up. Remove debugging. 
--              JWG 2011-04-14 Exclude Redress Claims from the automatic LETC closure of multiple identical events.
--              JWG 2011-07-20 EventTypeAccessRule added
--              JWG 2011-10-25 EventTypeAutomatedEvent added
--              JWG 2012-08-08 Cases.ProcessStartLeadEventID added
--              JWG 2014-01-22 #22327 Reorder steps so that SAE completes before anything like EventTypeAutomatedEvent gets added
--				SB	2014-07-10 Added logic to handle SAE for shared lead types
--				SB	2014-07-28 Prevent multiple letc records for shared lead types
--              JWG 2016-02-11 #35288 Check that the new event is not a "deleted" LeadEvent that is being copied to a new Case, when a Matter is split from the existing Case 
-- =============================================
CREATE TRIGGER [dbo].[trgiu_LeadEvent] 
   ON [dbo].[LeadEvent] 
   AFTER INSERT, UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers' 
	BEGIN
		RETURN
	END
	
	DECLARE @ErrorMessage NVARCHAR(4000),
    @ErrorSeverity INT,
    @ErrorState INT
    
	DECLARE @InsertingNewRecords bit,
	@PostUpdateSQL nvarchar(2000),
	@AquariumOptionID int,
	@WorkflowTaskID int,
	@CompletedBy int,
	@CompletedOn datetime,
	@CompletionDescription varchar(255),
	@LeadEventID int,
	@EventTypeID int,
	@NoteTypeID int,
	@ClientID int,
	@LeadID int,
	@CaseID int,
	@EventDeleted bit,
	@NextEventID int, 
	@WhenCreated datetime,
	@WhenFollowedUp datetime,
	@InProcess bit,
	@WhoCreated int,
	@RC int = 0,
	@EventSubtypeID int,
	@NextProcessStartEvent int

    -- Create a table variable to hold all the IDs being inserted or updated.
	DECLARE @LeadEventsAffected TABLE (
		LeadEventID int, 
		EventTypeID int,
		EventSubtypeID int null,
		ClientID int,
		LeadID int,
		CaseID int,
		EventDeleted bit,
		NextEventID int,
		NoteTypeID int,
		WhenCreated datetime, 
		WhenFollowedUp datetime,
		InProcess bit, 
		WhoCreated int 
	)

	/* 
		For each record, we need to know whether it is a new insert, or an update to EventCost, EventDeleted etc 
		This is achieved by testing to see if the identity column LeadEventID is affected for any of the rows.
		If it is, then all records must be part of an insert statement, else they must all be updates.  Normally
		only 1 record will be affected at a time anyway, but this covers all DML statements too.
	*/

	IF UPDATE(LeadEventID)
	BEGIN
		SELECT @InsertingNewRecords = 1
	END
	ELSE
	BEGIN
		SELECT @InsertingNewRecords = 0
	END

	/*
		A trigger only fires once even if you update 100 records at once, in which case
		the logical table "inserted" will then contain all 100 LeadEvent records.
		Put them all into our table variable so we can work through them one at a time.
    */
	INSERT INTO @LeadEventsAffected (LeadEventID, EventTypeID, EventSubtypeID, ClientID, LeadID, CaseID, EventDeleted, NextEventID, NoteTypeID, WhenCreated, WhenFollowedUp, InProcess, WhoCreated) 
	SELECT i.LeadEventID, i.EventTypeID, et.EventSubtypeID, i.ClientID, i.LeadID, i.CaseID, i.EventDeleted, i.NextEventID, i.NoteTypeID, i.WhenCreated, i.WhenFollowedUp, COALESCE(et.InProcess, 0), i.WhoCreated 
	FROM inserted i 
	LEFT JOIN dbo.EventType et WITH (NOLOCK) ON et.EventTypeID = i.EventTypeID 

	SELECT TOP 1 @LeadEventID = LeadEventID,
	@EventTypeID = EventTypeID,
	@EventSubtypeID = EventSubtypeID,
	@NoteTypeID = NoteTypeID,
	@ClientID = ClientID,
	@LeadID = LeadID,
	@CaseID = CaseID,
	@EventDeleted = EventDeleted,
	@NextEventID = NextEventID,
	@WhenCreated = WhenCreated, 
	@WhenFollowedUp = WhenFollowedUp,
	@InProcess = InProcess, 
	@WhoCreated = WhoCreated 
	FROM @LeadEventsAffected
	
	WHILE @LeadEventID > 0 AND @RC > -1
	BEGIN
		
		IF @EventTypeID IS NULL 
		BEGIN

			/* SQL After Note */
			SELECT @PostUpdateSQL = PostUpdateSQL
			FROM dbo.NoteTypeSQL nts WITH (NOLOCK) 
			WHERE nts.NoteTypeID = @NoteTypeID
			and nts.InsertExisting = @InsertingNewRecords

			IF @@ROWCOUNT = 1 AND rtrim(@PostUpdateSQL) <> ''
			BEGIN 

				SELECT @PostUpdateSQL = replace(@PostUpdateSQL, '@LeadEventID', @LeadEventID)

				EXEC sp_executesql @PostUpdateSQL

			END

			/* Make Notes followedup at once */
			IF @InsertingNewRecords = 1 AND @WhenFollowedUp IS NULL 
			BEGIN
				
				UPDATE LeadEvent 
				SET WhenFollowedUp = WhenCreated 
				WHERE LeadEventID = @LeadEventID 
				
			END
		END
		ELSE
		BEGIN
			IF @InsertingNewRecords = 1
			BEGIN
			
				/*
					Make out-of-process events followedup at once, so that
					they don't suddenly appear in the todo list for a case
					if the EventType suddenly becomes in-process one day.
				*/
				IF @InProcess = 0 AND @WhenFollowedUp IS NULL 
				BEGIN
					
					UPDATE LeadEvent 
					SET WhenFollowedUp = WhenCreated 
					WHERE LeadEventID = @LeadEventID 
					
				END
				
				/*
					Now handle multi-threading. 
					
					There are several parts to it:
	 
					1) If this is an insert, then it might be following up an existing leadevent, so tell
					   the LeadEventThreadCompletion table that this has now been done.
					   
						@NextEventID IS NULL      /* Don't complete new threads when the app marks the OLD event as followed up. */
						@EventDeleted = 0         /* Deleted events don't affect anything. */
						@EventTypeID IS NOT NULL  /* Notes don't affect anything. */

				*/
				IF @NextEventID IS NULL AND @EventDeleted = 0 AND @EventTypeID IS NOT NULL
				BEGIN
					UPDATE dbo.LeadEventThreadCompletion 
					SET ToLeadEventID = @LeadEventID, 
					ToEventTypeID = @EventTypeID 
					FROM dbo.LeadEventThreadCompletion letc 
					INNER JOIN dbo.EventChoice ec ON ec.EventTypeID = letc.FromEventTypeID
						AND ec.ThreadNumber = letc.ThreadNumberRequired
						AND ec.NextEventTypeID = @EventTypeID
					WHERE letc.CaseID = @CaseID 
					AND letc.ToLeadEventID IS NULL			/* Don't keep updating records that are already complete. */
					AND ec.ClientID NOT IN (183)	/* SB 2011-03-24 Prevent this running for BM (183) where we have multiple paths with the same event types. JWG 2011-04-14 Ditto for Redress Claims (150, 197) */
					--AND ec.ClientID NOT IN (150, 183, 197)	/* SB 2011-03-24 Prevent this running for BM (183) where we have multiple paths with the same event types. JWG 2011-04-14 Ditto for Redress Claims (150, 197) */
				END
				
			END
			
			IF @InsertingNewRecords = 0
			BEGIN
				/*
					2) If this leadevent is being logically deleted, then UN-followup anything
					   that was relying on it, bringing that ThreadNumberRequired back into play.
				*/
				IF @EventDeleted = 1 AND @EventTypeID IS NOT NULL
				BEGIN
					UPDATE dbo.LeadEventThreadCompletion 
					SET ToLeadEventID = NULL, 
					ToEventTypeID = NULL 
					WHERE CaseID = @CaseID 
					AND ToLeadEventID = @LeadEventID
					
					/* JWG 2012-08-08 Find the next process start event, if one exists */
					IF @EventSubtypeID = 10
					BEGIN
						SELECT TOP (1) @NextProcessStartEvent = le.LeadEventID 
						FROM dbo.LeadEvent le 
						INNER JOIN dbo.EventType et ON et.EventTypeID = le.EventTypeID 
						WHERE le.CaseID = @CaseID 
						AND le.EventDeleted = 0 
						AND et.EventSubtypeID = 10 
						AND le.LeadEventID <> @LeadEventID
						ORDER BY le.LeadEventID ASC 
						
						UPDATE dbo.Cases 
						SET ProcessStartLeadEventID = @NextProcessStartEvent
						WHERE CaseID = @CaseID
					END
					
				END
				
				-- 3) If this is an update then it might have been trying to set the 
				--    followup details prematurely, so prevent this from happening.
				--    We could check for (InProcess = 1) as well here, just in case the event process has changed since
				--    the LETC record was set up and it is no longer required now.
				IF (@WhenFollowedUp IS NOT NULL OR @NextEventID IS NOT NULL) AND @EventDeleted = 0 AND @EventTypeID IS NOT NULL 
				BEGIN
					IF EXISTS (
						SELECT * FROM dbo.LeadEventThreadCompletion 
						WHERE CaseID = @CaseID 
						AND FromLeadEventID = @LeadEventID
						AND ToLeadEventID IS NULL
					)
					BEGIN
						/* JWG 2012-06-01 Log which letc record(s) blocked this update */
						INSERT INTO [dbo].[__BlockedLeadEventUpdate] ([LeadEventID], [LeadEventThreadCompletionID], [WhenBlocked])
						SELECT FromLeadEventID, LeadEventThreadCompletionID, dbo.fn_GetDate_Local()
						FROM dbo.LeadEventThreadCompletion 
						WHERE CaseID = @CaseID 
						AND FromLeadEventID = @LeadEventID
						AND ToLeadEventID IS NULL
				
						UPDATE LeadEvent 
						SET WhenFollowedUp = NULL, NextEventID = NULL
						WHERE LeadEventID = @LeadEventID 
						
					END
				END

			END

			-- 4) Where this is an event that will be multi-threaded, set up the LETC record(s) for it now
			IF @InsertingNewRecords = 1
			BEGIN
				INSERT INTO LeadEventThreadCompletion (ClientID, LeadID, CaseID, FromEventTypeID, FromLeadEventID, ThreadNumberRequired, ToEventTypeID, ToLeadEventID)
				SELECT DISTINCT ec.ClientID, @LeadID, @CaseID, ec.EventTypeID, @LeadEventID, ec.ThreadNumber, NULL, NULL  
				FROM EventChoice ec WITH (NOLOCK) 
				WHERE ec.EventTypeID = @EventTypeID 
				AND ec.ClientID = @ClientID
				AND ec.EscalationEvent = 0 -- Don't create threads for escalation event choices.  These are usually thread 1 anyway, as it happens.
				AND @NextEventID IS NULL -- Only do this for new inserts, not where we are following up an existing leadevent, and not where we are deleting a later one!
				AND NOT EXISTS (
					SELECT * 
					FROM LeadEventThreadCompletion WITH (NOLOCK) 
					WHERE CaseID = @CaseID
					AND FromLeadEventID = @LeadEventID
				)
			END
		END

		IF @InsertingNewRecords = 1
		BEGIN
					
			/* AMG 20130712 - SplitMatterIntoNewCase - do not run SAE */
			IF ISNULL(CAST(CONTEXT_INFO() AS VARCHAR),'') <> 'NoSAE'
			BEGIN
			
				/* Sql After Event */
				IF @NextEventID IS NULL
				BEGIN
					DECLARE @Params nvarchar(20), @IsNativeSql bit

					SELECT @Params = '@LeadEventID int'

					DECLARE @SqlStatement nvarchar(2000)

					-- Handle client 0 SAE and client specific SAE for shared event types
					-- We always call the client specific override if it available and this will go on to call the shared SAE if required
					;WITH ets AS 
					(
						SELECT ets.PostUpdateSql, ets.IsNativeSql, 
						ROW_NUMBER() OVER(PARTITION BY ets.EventTypeID ORDER BY ets.ClientID DESC) as rn 
						FROM dbo.EventTypeSql ets  WITH (NOLOCK) 
						WHERE ets.EventTypeID = @EventTypeID
						AND ets.ClientID IN (@ClientID, 0)
					)
					
					SELECT @SqlStatement = ets.PostUpdateSql, @IsNativeSql = ets.IsNativeSql
					FROM ets
					WHERE rn = 1

					IF @@ROWCOUNT = 1 AND rtrim(@SqlStatement) <> ''
					BEGIN 
						IF @IsNativeSql = 1
						BEGIN

							DECLARE @CustomerID int, @UserID int

							SELECT @ClientID = le.ClientID,
							@UserID = le.WhoCreated,
							@CustomerID = l.CustomerID
							FROM LeadEvent le WITH (NOLOCK) 
							INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID = le.LeadID 
							WHERE le.LeadEventID = @LeadEventID 

							SELECT @SqlStatement = replace(@SqlStatement, '@LeadEventID', @LeadEventID)

							SELECT @SqlStatement = dbo.fnSubstituteSqlParams(@SqlStatement, @UserID, @ClientID, @CustomerID, @LeadID, @CaseID, NULL, NULL)

							EXEC @RC = sp_executesql @SqlStatement
						END
						ELSE
						BEGIN
							/* 
								JWG 2014-02-13 #22327
								Ensure that any errors and deliberate RAISERRORs in the SAE procs stop the rest of this trigger code from executing.
								Otherwise the trigger code up to this point get rolled back (in the implicit trigger tran) but then all the remaining steps get executed in the outer tran.
							*/
							BEGIN TRY
							
								EXEC @RC = sp_executesql @SqlStatement, @Params, @LeadEventID  
								
							END TRY
							BEGIN CATCH
							
								SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();
								
								RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
								
								SET @RC = -1;  /* Prevent any more code from executing below */
								
							END CATCH
						END

						/* Workaround until all SAE procs are changed. Some used to set this to 1. */
						SET ROWCOUNT 0

					END
				END
			
			END
			
			/* JWG 2014-02-13 #22327 */
			IF @RC > -1
			BEGIN
				/* Keep LatestLeadEvents in step */
				IF @InProcess = 1
				BEGIN
					/* LatestLeadEventID (always updated) */
					/* In Process */
					/* Non Note */
					/* JWG 2012-08-08 Process start event might have been added for the first time */
					UPDATE dbo.Cases 
					SET LatestLeadEventID = @LeadEventID, 
					LatestInProcessLeadEventID = @LeadEventID,
					LatestNonNoteLeadEventID = @LeadEventID, 
					ProcessStartLeadEventID = CASE 
													/* Process start added for the first time */
													WHEN ProcessStartLeadEventID IS NULL AND @EventSubtypeID = 10 AND @EventDeleted = 0 THEN @LeadEventID 
													
													/* No change to the existing value */
													ELSE ProcessStartLeadEventID 
											  END 
					WHERE CaseID = @CaseID
				END
				ELSE
				BEGIN
					IF @EventTypeID IS NOT NULL
					BEGIN
						/* LatestLeadEventID (always updated) */
						/* Out Of Process */
						/* Non Note */
						UPDATE dbo.Cases 
						SET LatestLeadEventID = @LeadEventID, 
						LatestOutOfProcessLeadEventID = @LeadEventID,
						LatestNonNoteLeadEventID = @LeadEventID
						WHERE CaseID = @CaseID
					END
					ELSE
					BEGIN
						/* LatestLeadEventID (always updated) */
						/* Note */
						UPDATE dbo.Cases 
						SET LatestLeadEventID = @LeadEventID, 
						LatestNoteLeadEventID = @LeadEventID
						WHERE CaseID = @CaseID
					END
				END		
			END	
		END

		/* JWG 2014-02-13 #22327 */
		IF @RC > -1
		BEGIN
			/* JWG 2014-01-22 Moved here after the SAE section to reduce transaction times. */
			/* Now sort out the workflow tasks completed */
			IF @EventDeleted = 0
			BEGIN
				SELECT @WorkflowTaskID = w.WorkflowTaskID,
				@CompletedBy = CASE WHEN IsNull(@NextEventID, 0) = 0 THEN @WhoCreated ELSE (SELECT le_new.WhoCreated FROM dbo.LeadEvent le_new WITH (NOLOCK) WHERE le_new.LeadEventID = @NextEventID) END,
				@CompletedOn = CASE WHEN @NextEventID IS NULL THEN @WhenCreated ELSE @WhenFollowedUp END,
				@CompletionDescription = CASE WHEN @NextEventID IS NULL THEN 'Inserted by trigger' ELSE 'Updated by trigger' END
				FROM WorkflowTask w WITH (NOLOCK) 
				WHERE w.CaseID = @CaseID AND w.EventTypeID = @EventTypeID AND w.Followup = CASE WHEN @NextEventID IS NULL THEN 0 ELSE 1 END
			END
			
			/* IF this is relevant to workflow, update the record now */
			IF @@ROWCOUNT > 0 AND @WorkflowTaskID > 0
			BEGIN
				EXECUTE @RC = dbo.WorkflowTask_SetComplete @WorkflowTaskID, @CompletedBy, @CompletedOn, @CompletionDescription 
			END
			
			/* Lead/Case access rules */
			IF @InsertingNewRecords = 1 AND @EventTypeID IS NOT NULL
			BEGIN
				EXEC dbo.EventTypeAccessRuleFromLeadEvent @LeadEventID, @EventTypeID, @CaseID
			END
			
			/* Event Type Field Completion */
			SELECT @AquariumOptionID = CASE @EventDeleted WHEN 0 THEN 1 ELSE 2 END

			-- Only trigger field completion if the leadevent is being inserted or logically deleted, 
			-- not just updated for some change of Cost or other irrelevant change:
			IF @InsertingNewRecords = 1 OR @AquariumOptionID = 2
			BEGIN
				EXEC dbo.EventTypeFieldCompletionFromLeadEvent @LeadEventID, @WhenCreated, @AquariumOptionID, @EventTypeID, @LeadID, @CaseID 
			END
		
			IF @InsertingNewRecords = 1
			BEGIN
				
				/* 
					ACE 2012-08-10 EventTypeAutomatedWorkflow added 
					This creates a workflow task immediately with no need for a batch job to run.
				*/
				IF EXISTS(SELECT * FROM dbo.EventTypeAutomatedWorkflow et WITH (NOLOCK) WHERE et.EventTypeID = @EventTypeID)
				BEGIN
					EXEC dbo.EventTypeAutomatedWorkflowFromLeadEvent @LeadEventID, @EventTypeID 
				END
				
				/* #35288 Check that the new event is not a "deleted" LeadEvent that is being copied to a new Case, when a Matter is split from the existing Case */
				IF @EventDeleted = 0
				BEGIN
					
					/* Event Type Batch Job */
					IF EXISTS(SELECT * FROM dbo.EventTypeAutomatedTask et WITH (NOLOCK) WHERE et.EventTypeID = @EventTypeID)
					BEGIN
						EXEC dbo.EventTypeAutomatedTaskFromLeadEvent @LeadEventID, @EventTypeID 
					END
					
					/* 
						JWG 2011-10-25 EventTypeAutomatedEvent added 
						A new scheduler "Events Scheduler" will apply these events individually now.
					*/
					IF EXISTS(SELECT * FROM dbo.EventTypeAutomatedEvent et WITH (NOLOCK) WHERE et.EventTypeID = @EventTypeID)
					BEGIN
						EXEC dbo.EventTypeAutomatedEvent__CreateAutomatedEventQueue @ClientID, @LeadID, @CaseID, @LeadEventID, @EventTypeID, @WhenCreated, @WhoCreated 
					END
					
				END
			END
		END

		-- Now loop round for the next event. Normally there will not be one, but if
		-- someone has updated a set of events in one update statement.
		DELETE FROM @LeadEventsAffected
		WHERE LeadEventID = @LeadEventID

		SELECT TOP 1 @LeadEventID = LeadEventID,
		@EventTypeID = EventTypeID,
		@ClientID = ClientID,
		@LeadID = LeadID,
		@CaseID = CaseID,
		@EventDeleted = EventDeleted,
		@NextEventID = NextEventID,
		@WhenCreated = WhenCreated, 
		@WhenFollowedUp = WhenFollowedUp,
		@InProcess = InProcess
		FROM @LeadEventsAffected

		IF @@ROWCOUNT < 1
		BEGIN
			SELECT @LeadEventID = -1
		END

	END

END
GO
ALTER TABLE [dbo].[LeadEvent] ADD CONSTRAINT [PK_LeadEvent] PRIMARY KEY CLUSTERED  ([LeadEventID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEvent_CoveringNoEventTypeID2017] ON [dbo].[LeadEvent] ([CaseID], [EventDeleted], [ClientID], [WhenCreated]) INCLUDE ([WhenFollowedUp], [WhoCreated])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEvent_Covering2017V2] ON [dbo].[LeadEvent] ([CaseID], [EventTypeID], [EventDeleted], [ClientID], [WhenCreated]) INCLUDE ([WhenFollowedUp], [WhoCreated])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_LeadEvent_NoteFiltered] ON [dbo].[LeadEvent] ([CaseID], [NoteTypeID], [NotePriority], [EventDeleted], [ClientID]) WHERE ([NoteTypeID]>(0))
GO
CREATE NONCLUSTERED INDEX [IX_LeadEventClient] ON [dbo].[LeadEvent] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEventClientEvent2017] ON [dbo].[LeadEvent] ([ClientID], [EventTypeID], [EventDeleted]) INCLUDE ([CaseID], [WhenCreated], [WhoCreated])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEvent_DocumentQueue] ON [dbo].[LeadEvent] ([DocumentQueueID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEvent_DeletedOnly2017] ON [dbo].[LeadEvent] ([EventDeleted], [ClientID]) INCLUDE ([CaseID], [EventTypeID], [WhenCreated])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEvent_EventType2017] ON [dbo].[LeadEvent] ([EventTypeID], [EventDeleted]) INCLUDE ([CaseID], [WhenCreated])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEventLeadDocument] ON [dbo].[LeadEvent] ([LeadDocumentID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEventLead] ON [dbo].[LeadEvent] ([LeadID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEventLead2017V2] ON [dbo].[LeadEvent] ([LeadID], [ClientID]) INCLUDE ([EventDeleted], [EventTypeID], [WhenCreated])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEventNextEvent] ON [dbo].[LeadEvent] ([NextEventID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEventWhenCreated] ON [dbo].[LeadEvent] ([WhenCreated], [EventDeleted])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEventWhenFollowedUp] ON [dbo].[LeadEvent] ([WhenFollowedUp], [EventDeleted])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEventWhoCreated] ON [dbo].[LeadEvent] ([WhoCreated], [EventDeleted])
GO
ALTER TABLE [dbo].[LeadEvent] ADD CONSTRAINT [FK_LeadEvent_AquariumEventType] FOREIGN KEY ([AquariumEventType]) REFERENCES [dbo].[AquariumEventType] ([AquariumEventTypeID])
GO
ALTER TABLE [dbo].[LeadEvent] ADD CONSTRAINT [FK_LeadEvent_Cases] FOREIGN KEY ([CaseID]) REFERENCES [dbo].[Cases] ([CaseID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[LeadEvent] ADD CONSTRAINT [FK_LeadEvent_ClientPersonnel] FOREIGN KEY ([WhoDeleted]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[LeadEvent] ADD CONSTRAINT [FK_LeadEvent_Contact] FOREIGN KEY ([ContactID]) REFERENCES [dbo].[Contact] ([ContactID])
GO
ALTER TABLE [dbo].[LeadEvent] ADD CONSTRAINT [FK_LeadEvent_DocumentQueue] FOREIGN KEY ([DocumentQueueID]) REFERENCES [dbo].[DocumentQueue] ([DocumentQueueID])
GO
ALTER TABLE [dbo].[LeadEvent] ADD CONSTRAINT [FK_LeadEvent_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[LeadEvent] ADD CONSTRAINT [FK_LeadEvent_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID])
GO
ALTER TABLE [dbo].[LeadEvent] ADD CONSTRAINT [FK_LeadEvent_LeadDocument] FOREIGN KEY ([LeadDocumentID]) REFERENCES [dbo].[LeadDocument] ([LeadDocumentID])
GO
ALTER TABLE [dbo].[LeadEvent] ADD CONSTRAINT [FK_NoteTypeID_NoteType] FOREIGN KEY ([NoteTypeID]) REFERENCES [dbo].[NoteType] ([NoteTypeID])
GO
