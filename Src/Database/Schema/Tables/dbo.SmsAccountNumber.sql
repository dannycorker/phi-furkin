CREATE TABLE [dbo].[SmsAccountNumber]
(
[SmsAccountNumberID] [int] NOT NULL IDENTITY(1, 1),
[PhoneNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[AccountNumber] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Token] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[Username] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Password] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SmsAccountNumber] ADD CONSTRAINT [PK__SmsAccou__017D1B8B357F68ED] PRIMARY KEY CLUSTERED  ([SmsAccountNumberID])
GO
