CREATE TABLE [dbo].[PremiumCalculationDetail]
(
[PremiumCalculationDetailID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ProductCostBreakdown] [xml] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[LeadEventID] [int] NULL
)
GO
ALTER TABLE [dbo].[PremiumCalculationDetail] ADD CONSTRAINT [PK_PremiumCalculationDetail] PRIMARY KEY CLUSTERED  ([PremiumCalculationDetailID])
GO
