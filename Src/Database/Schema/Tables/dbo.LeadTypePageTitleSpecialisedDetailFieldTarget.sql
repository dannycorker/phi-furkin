CREATE TABLE [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget]
(
[LeadTypePageTitleSpecialisedDetailFieldTargetID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[Target] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[DetailFieldID] [int] NULL,
[ColumnField] [int] NULL,
[TemplateTypeID] [int] NOT NULL,
[DetailFieldAlias] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[ColumnFieldAlias] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[RowField] [int] NULL,
[Notes] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget] ADD CONSTRAINT [PK_LeadTypePageTitleSpecialisedDetailFieldTarget] PRIMARY KEY CLUSTERED  ([LeadTypePageTitleSpecialisedDetailFieldTargetID])
GO
ALTER TABLE [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget] ADD CONSTRAINT [FK_LeadTypePageTitleSpecialisedDetailFieldTarget_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget] ADD CONSTRAINT [FK_LeadTypePageTitleSpecialisedDetailFieldTarget_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
ALTER TABLE [dbo].[LeadTypePageTitleSpecialisedDetailFieldTarget] ADD CONSTRAINT [FK_LeadTypePageTitleSpecialisedDetailFieldTarget_TemplateType] FOREIGN KEY ([TemplateTypeID]) REFERENCES [dbo].[TemplateType] ([TemplateTypeID])
GO
