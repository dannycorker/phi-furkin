CREATE TABLE [dbo].[AutomatedTaskParam]
(
[AutomatedTaskParamID] [int] NOT NULL IDENTITY(1, 1),
[TaskID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[ParamName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ParamValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2011-07-20
-- Description:	Temp trigger to remove unwanted WEBSERVICE_NAME='None' entries
-- =============================================
CREATE TRIGGER [dbo].[trgi_AutomatedTaskParam]
   ON [dbo].[AutomatedTaskParam]
   AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	DELETE dbo.AutomatedTaskParam
	FROM dbo.AutomatedTaskParam atp
	INNER JOIN inserted i ON i.AutomatedTaskParamID = atp.AutomatedTaskParamID
	WHERE i.ParamName = 'WEBSERVICE_NAME'
	AND i.ParamValue = 'None'

END
GO
ALTER TABLE [dbo].[AutomatedTaskParam] ADD CONSTRAINT [PK_AutomatedTaskParam] PRIMARY KEY CLUSTERED  ([AutomatedTaskParamID])
GO
CREATE NONCLUSTERED INDEX [IX_AutomatedTaskParam_TaskParamName] ON [dbo].[AutomatedTaskParam] ([TaskID], [ParamName])
GO
ALTER TABLE [dbo].[AutomatedTaskParam] ADD CONSTRAINT [FK_AutomatedTaskParam_AutomatedTask] FOREIGN KEY ([TaskID]) REFERENCES [dbo].[AutomatedTask] ([TaskID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[AutomatedTaskParam] ADD CONSTRAINT [FK_AutomatedTaskParam_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
