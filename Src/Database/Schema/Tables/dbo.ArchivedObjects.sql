CREATE TABLE [dbo].[ArchivedObjects]
(
[ArchivedObjectID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NULL,
[LeadID] [int] NULL,
[CaseID] [int] NULL,
[MatterID] [int] NULL,
[NewCustomerID] [int] NULL,
[NewLeadID] [int] NULL,
[NewCaseID] [int] NULL,
[DateArchived] [datetime] NOT NULL,
[WhoArchived] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[DateForDeletion] [date] NOT NULL,
[DateDeleted] [datetime] NULL,
[Notes] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[Status] [int] NULL,
[DateRestored] [datetime] NULL,
[WhoRestored] [varchar] (100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ArchivedObjects] ADD CONSTRAINT [PK_ArchivedObjects] PRIMARY KEY CLUSTERED  ([ArchivedObjectID])
GO
