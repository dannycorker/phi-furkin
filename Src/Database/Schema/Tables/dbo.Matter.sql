CREATE TABLE [dbo].[Matter]
(
[MatterID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[MatterRef] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[CustomerID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[MatterStatus] [tinyint] NOT NULL CONSTRAINT [DF__Matter__MatterSt__74CE504D] DEFAULT ((1)),
[RefLetter] [varchar] (3) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF__Matter__RefLette__75C27486] DEFAULT ('A'),
[BrandNew] [bit] NOT NULL CONSTRAINT [DF__Matter__BrandNew__73DA2C14] DEFAULT ((1)),
[CaseID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime2] (0) NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime2] (0) NULL,
[SourceID] [int] NULL
)
GO
ALTER TABLE [dbo].[Matter] ADD CONSTRAINT [PK_Matter] PRIMARY KEY CLUSTERED  ([MatterID])
GO
CREATE NONCLUSTERED INDEX [IX_Matter_Case] ON [dbo].[Matter] ([CaseID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_MatterClient] ON [dbo].[Matter] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_MatterClient2017] ON [dbo].[Matter] ([ClientID]) INCLUDE ([CaseID], [CustomerID], [LeadID], [MatterID])
GO
CREATE NONCLUSTERED INDEX [IX_Matter_Customer2017] ON [dbo].[Matter] ([CustomerID], [ClientID]) INCLUDE ([CaseID], [LeadID])
GO
CREATE NONCLUSTERED INDEX [IX_Matter_Lead] ON [dbo].[Matter] ([LeadID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_MatterRef] ON [dbo].[Matter] ([MatterRef])
GO
ALTER TABLE [dbo].[Matter] ADD CONSTRAINT [FK_Matter_Cases] FOREIGN KEY ([CaseID]) REFERENCES [dbo].[Cases] ([CaseID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Matter] ADD CONSTRAINT [FK_Matter_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[Matter] ADD CONSTRAINT [FK_Matter_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID]) ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Matter] ADD CONSTRAINT [FK_Matter_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID])
GO
