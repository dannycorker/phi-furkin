CREATE TABLE [dbo].[FAS_CheckerData]
(
[FasCheckerDataID] [int] NOT NULL IDENTITY(1, 1),
[FasUniqueReferenceNumber] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[TitleOfAgreement] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[IsDebtorsNameOrAddressMissing] [bit] NULL,
[IsCreditorsNameOrAddressMissing] [bit] NULL,
[AgreementDebtorSignatureDate] [datetime] NULL,
[NameOfCreditor] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[AmountOfCredit] [decimal] (18, 0) NULL,
[TotalCashPrice] [decimal] (18, 0) NULL,
[DurationOfAgreementInMonths] [int] NULL,
[AnnualPercentageRate] [float] NULL,
[RateOfInterest] [float] NULL,
[FixedOrVariableRateOfInterest] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[TotalInterest] [decimal] (18, 0) NULL,
[TotalChargeFees] [decimal] (18, 0) NULL,
[TotalChargeForCredit] [decimal] (18, 0) NULL,
[FirstPayment] [decimal] (18, 0) NULL,
[ConstantRegularOrSubsequentPayment] [decimal] (18, 0) NULL,
[LastPayment] [decimal] (18, 0) NULL,
[DeferredOrBallonPayment] [decimal] (18, 0) NULL,
[TotalPayments] [decimal] (18, 0) NULL,
[AdvancePayment] [decimal] (18, 0) NULL,
[TotalAmountPayable] [decimal] (18, 0) NULL,
[Category] [char] (1) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[FAS_CheckerData] ADD CONSTRAINT [PK_FAS_CheckerData] PRIMARY KEY CLUSTERED  ([FasCheckerDataID])
GO
