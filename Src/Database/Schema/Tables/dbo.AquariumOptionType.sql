CREATE TABLE [dbo].[AquariumOptionType]
(
[AquariumOptionTypeID] [int] NOT NULL,
[AquariumOptionTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[AquariumOptionTypeDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[AquariumOptionType] ADD CONSTRAINT [PK_AquariumOptionType] PRIMARY KEY CLUSTERED  ([AquariumOptionTypeID])
GO
