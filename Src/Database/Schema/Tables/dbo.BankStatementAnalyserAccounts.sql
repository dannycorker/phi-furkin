CREATE TABLE [dbo].[BankStatementAnalyserAccounts]
(
[BankStatementAnalyserAccountID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[BankStatementPageCount] [int] NULL,
[CostPerPage] [money] NULL,
[LastResetDate] [datetime] NULL
)
GO
ALTER TABLE [dbo].[BankStatementAnalyserAccounts] ADD CONSTRAINT [PK_BankStatementAnalyserAccounts] PRIMARY KEY CLUSTERED  ([BankStatementAnalyserAccountID])
GO
