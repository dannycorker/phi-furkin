CREATE TABLE [dbo].[PerfLog]
(
[PerfLogID] [int] NOT NULL IDENTITY(1, 1),
[SPID] [int] NOT NULL,
[Origin] [varchar] (128) COLLATE Latin1_General_CI_AS NOT NULL,
[Start] [datetime] NULL,
[Finish] [datetime] NULL,
[ParamList] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ClientPersonnelID] [int] NULL,
[RunTimeSeconds] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-07-19
-- Description:	When entries are updated with long perflog times, send an email.
-- =============================================
CREATE TRIGGER [dbo].[trgiud_PerflogLong] 
   ON [dbo].[PerfLog] 
   AFTER UPDATE 
AS 
BEGIN
	SET NOCOUNT ON;
	
	/* Allow special user 'notriggers' to bypass trigger code */
	IF SYSTEM_USER = 'notriggers'
	BEGIN
		RETURN
	END
	
	IF EXISTS (SELECT * FROM inserted i WHERE i.RunTimeSeconds > 9 AND i.Origin NOT LIKE '%SAE%')
	BEGIN
	
--		DECLARE @body1 VARCHAR(4000),
--				@query1 NVARCHAR(2000)
		
--		SELECT @body1 = i.Origin + ' ' + CONVERT(VARCHAR(20),i.RunTimeSeconds), @query1 = 'Select i.PerfLogID, ''| Origin: '', i.Origin, ''| RunTimeSeconds: '', i.RunTimeSeconds from '+'[' + @@SERVERNAME + '].'+DB_NAME()+'.dbo.perflog i with (nolock) where perflogid = '+ CAST(i.PerfLogID AS VARCHAR(20))+''
--		FROM inserted i

--		EXEC msdb.dbo.sp_send_dbmail 
--			@profile_name='SC2 DBMAIL',
--			@recipients='louis.bromilow@aquarium-software.com',
--			@subject = 'Slow Perflogged Proc',
--			@body = @body1,
--			@body_format = 'HTML',
--			@query_result_header = 0,
--			@query_result_no_padding = 1,
--			@query = @query1,
--			@attach_query_result_as_file = 0,
--			@query_attachment_filename = 'SlowData.txt'
			
		/*Flagfor Recomplication*/
		INSERT INTO AutomatedExecution (SQLToRun, ExecutionSubTypeID, EarliestRunDateTime, CompletionDateTime, ErrorDateTime, ClientID)
		VALUES ('EXEC RecompileProcsAndFunctionsLegalandGeneral','1',dbo.fn_GetDate_Local(),NULL,NULL,327)

		Select 1 where 1 = 2
			
	END

END
GO
ALTER TABLE [dbo].[PerfLog] ADD CONSTRAINT [PK_PerfLog] PRIMARY KEY CLUSTERED  ([PerfLogID])
GO
CREATE NONCLUSTERED INDEX [IX_PerfLog_Origin] ON [dbo].[PerfLog] ([Origin], [ClientPersonnelID], [Finish], [RunTimeSeconds]) INCLUDE ([SPID], [Start])
GO
CREATE NONCLUSTERED INDEX [IX_PerfLog_StartRuntime] ON [dbo].[PerfLog] ([Start], [RunTimeSeconds])
GO
