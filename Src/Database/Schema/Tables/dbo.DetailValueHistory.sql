CREATE TABLE [dbo].[DetailValueHistory]
(
[DetailValueHistoryID] [int] NOT NULL IDENTITY(2, 1),
[ClientID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[LeadOrMatter] [tinyint] NOT NULL,
[LeadID] [int] NULL,
[MatterID] [int] NULL,
[FieldValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[WhenSaved] [datetime] NOT NULL,
[ClientPersonnelID] [int] NOT NULL,
[EncryptedValue] [varchar] (3000) COLLATE Latin1_General_CI_AS NULL,
[ValueInt] [int] NULL,
[ValueMoney] [money] NULL,
[ValueDate] [date] NULL,
[ValueDateTime] [datetime2] (0) NULL,
[CustomerID] [int] NULL,
[CaseID] [int] NULL,
[ContactID] [int] NULL,
[ClientPersonnelDetailValueID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-27
-- Description:	Store DetailValue as specific datatypes
-- =============================================
CREATE TRIGGER [dbo].[trgiu_DetailValueHistory]
   ON [dbo].[DetailValueHistory]
   AFTER INSERT, UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END


    /*
		If the DetailValue column has been updated then fire the trigger,
		otherwise we are here again because the trigger has just fired,
		in which case do nothing.
	*/
    IF UPDATE(FieldValue)
    BEGIN
		UPDATE dbo.DetailValueHistory
		SET ValueInt = dbo.fnValueAsIntBitsIncluded(i.fieldvalue) /*CASE WHEN dbo.fnIsInt(i.fieldvalue) = 1 THEN convert(int, i.fieldvalue) ELSE NULL END*/,
		ValueMoney = CASE WHEN dbo.fnIsMoney(i.fieldvalue) = 1 THEN convert(money, i.fieldvalue) ELSE NULL END,
		ValueDate = CASE WHEN dbo.fnIsDateTime(i.fieldvalue) = 1 THEN convert(date, i.fieldvalue) ELSE NULL END,
		ValueDateTime = CASE WHEN dbo.fnIsDateTime(i.fieldvalue) = 1 THEN convert(datetime2, i.fieldvalue) ELSE NULL END
		FROM dbo.DetailValueHistory dvh
		INNER JOIN inserted i ON i.DetailValueHistoryID = dvh.DetailValueHistoryID
	END


END

GO
ALTER TABLE [dbo].[DetailValueHistory] ADD CONSTRAINT [PK_DetailValueHistory] PRIMARY KEY CLUSTERED  ([DetailValueHistoryID])
GO
CREATE NONCLUSTERED INDEX [IX_DetailValueHistory_Client] ON [dbo].[DetailValueHistory] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_DetailValueHistory_ClientField] ON [dbo].[DetailValueHistory] ([ClientID], [DetailFieldID], [WhenSaved])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_DetailValueHistory_Customer] ON [dbo].[DetailValueHistory] ([CustomerID], [DetailFieldID], [WhenSaved]) WHERE ([CustomerID] IS NOT NULL)
GO
CREATE NONCLUSTERED INDEX [IX_DetailValueHistoryDetailField] ON [dbo].[DetailValueHistory] ([DetailFieldID])
GO
CREATE NONCLUSTERED INDEX [IX_DetailValueHistoryLead] ON [dbo].[DetailValueHistory] ([LeadID], [DetailFieldID], [WhenSaved])
GO
CREATE NONCLUSTERED INDEX [IX_DetailValueHistoryMatter] ON [dbo].[DetailValueHistory] ([MatterID], [DetailFieldID], [WhenSaved])
GO
ALTER TABLE [dbo].[DetailValueHistory] ADD CONSTRAINT [FK__DetailVal__Custo__4097839E] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[DetailValueHistory] ADD CONSTRAINT [FK_DetailValueHistory_Cases] FOREIGN KEY ([CaseID]) REFERENCES [dbo].[Cases] ([CaseID])
GO
ALTER TABLE [dbo].[DetailValueHistory] ADD CONSTRAINT [FK_DetailValueHistory_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DetailValueHistory] ADD CONSTRAINT [FK_DetailValueHistory_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[DetailValueHistory] ADD CONSTRAINT [FK_DetailValueHistory_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[DetailValueHistory] ADD CONSTRAINT [FK_DetailValueHistory_Matter] FOREIGN KEY ([MatterID]) REFERENCES [dbo].[Matter] ([MatterID])
GO
