CREATE TABLE [dbo].[_C00_Mediate_Queue]
(
[QueueID] [bigint] NOT NULL IDENTITY(1, 1),
[QueueDate] [datetime2] NOT NULL CONSTRAINT [DF___C00_Medi__Queue__471B92E6] DEFAULT (getdate()),
[ModifyDate] [datetime2] NOT NULL CONSTRAINT [DF___C00_Medi__Modif__480FB71F] DEFAULT (getdate()),
[AppKey] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Queue] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Flow] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Action] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Method] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ObjectID] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[ParentObjectID] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[RequestBody] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[RequestType] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ResponseBody] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ResponseType] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Exception] [nvarchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ExceptionCount] [int] NOT NULL CONSTRAINT [DF___C00_Medi__Excep__4903DB58] DEFAULT ((0)),
[IsProcessCompleted] [bit] NULL CONSTRAINT [DF___C00_Medi__IsPro__49F7FF91] DEFAULT ((0)),
[OneVisionID] [int] NULL
)
GO
GRANT UPDATE ON  [dbo].[_C00_Mediate_Queue] TO [AquariumNet]
GO
