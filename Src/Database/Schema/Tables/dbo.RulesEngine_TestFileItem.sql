CREATE TABLE [dbo].[RulesEngine_TestFileItem]
(
[TestFileItemID] [int] NOT NULL IDENTITY(1, 1),
[TestFileID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[Input] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[CustomerID] [int] NULL,
[LeadID] [int] NULL,
[CaseID] [int] NULL,
[MatterID] [int] NULL,
[EvaluateByEffectiveDate] [date] NULL,
[EvaluateByChangeSetID] [int] NULL,
[SourceID] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[EvaluatedRuleSetID] [int] NULL,
[Output] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[RulesEngine_TestFileItem] ADD CONSTRAINT [PK_RulesEngine_TestFileItem] PRIMARY KEY CLUSTERED  ([TestFileItemID])
GO
ALTER TABLE [dbo].[RulesEngine_TestFileItem] ADD CONSTRAINT [FK_RulesEngine_TestFileItem_RulesEngine_TestFile] FOREIGN KEY ([TestFileID]) REFERENCES [dbo].[RulesEngine_TestFile] ([TestFileID]) ON DELETE CASCADE
GO
