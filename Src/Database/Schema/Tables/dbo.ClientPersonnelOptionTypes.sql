CREATE TABLE [dbo].[ClientPersonnelOptionTypes]
(
[ClientPersonnelOptionTypeID] [int] NOT NULL IDENTITY(1, 1),
[OptionTypeName] [nchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ClientPersonnelOptionTypes] ADD CONSTRAINT [PK_UserOptionTypes] PRIMARY KEY CLUSTERED  ([ClientPersonnelOptionTypeID])
GO
