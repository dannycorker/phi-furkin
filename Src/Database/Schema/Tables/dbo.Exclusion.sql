CREATE TABLE [dbo].[Exclusion]
(
[ExclusionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[IsEnabled] [bit] NOT NULL,
[ExclusionName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[ExclusionDescription] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[ExclusionTypeID] [int] NOT NULL,
[ExclusionDayOfWeek] [tinyint] NULL,
[ExclustionDate] [date] NULL,
[ExclusionTimeFrom] [time] (0) NULL,
[ExclusionTimeTo] [time] (0) NULL,
[ExclusionAllDay] [bit] NOT NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[Exclusion] ADD CONSTRAINT [PK_Exclusion] PRIMARY KEY CLUSTERED  ([ExclusionID])
GO
CREATE NONCLUSTERED INDEX [IX_Exclusion_ClientIsEnabled] ON [dbo].[Exclusion] ([ClientID], [IsEnabled])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Exclusion] ON [dbo].[Exclusion] ([ExclusionID])
GO
CREATE NONCLUSTERED INDEX [IX_Exclusion_SourceID] ON [dbo].[Exclusion] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[Exclusion] ADD CONSTRAINT [FK_Exclusion_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[Exclusion] ADD CONSTRAINT [FK_Exclusion_ExclusionType] FOREIGN KEY ([ExclusionTypeID]) REFERENCES [dbo].[ExclusionType] ([ExclusionTypeID])
GO
