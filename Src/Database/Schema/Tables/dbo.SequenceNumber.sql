CREATE TABLE [dbo].[SequenceNumber]
(
[SequenceNumberID] [int] NOT NULL,
[SequenceNumberName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[SequenceNumberDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[LastValueInt] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[SequenceNumber] ADD CONSTRAINT [PK_SequenceNumber] PRIMARY KEY CLUSTERED  ([SequenceNumberID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SequenceNumber_Name] ON [dbo].[SequenceNumber] ([SequenceNumberName])
GO
