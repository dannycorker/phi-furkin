CREATE TABLE [dbo].[SMSStopConfigText]
(
[SMSStopConfigTextID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[OutgoingPhoneNumber] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[SMSTextToAppend] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[LeadTypeID] [int] NULL
)
GO
