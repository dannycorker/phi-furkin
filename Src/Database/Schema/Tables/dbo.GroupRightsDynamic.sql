CREATE TABLE [dbo].[GroupRightsDynamic]
(
[GroupRightsDynamicID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelAdminGroupID] [int] NOT NULL,
[FunctionTypeID] [int] NOT NULL,
[LeadTypeID] [int] NULL,
[ObjectID] [int] NOT NULL,
[RightID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[GroupRightsDynamic] ADD CONSTRAINT [PK_GroupRightsDynamic] PRIMARY KEY CLUSTERED  ([GroupRightsDynamicID])
GO
CREATE NONCLUSTERED INDEX [IX_GroupRightsDynamicGroup] ON [dbo].[GroupRightsDynamic] ([ClientPersonnelAdminGroupID])
GO
CREATE NONCLUSTERED INDEX [IX_GroupRightsDynamicGroup_Covering2017] ON [dbo].[GroupRightsDynamic] ([ClientPersonnelAdminGroupID]) INCLUDE ([FunctionTypeID], [LeadTypeID], [ObjectID], [RightID])
GO
ALTER TABLE [dbo].[GroupRightsDynamic] ADD CONSTRAINT [FK_GroupRightsDynamic_ClientPersonnelAdminGroups] FOREIGN KEY ([ClientPersonnelAdminGroupID]) REFERENCES [dbo].[ClientPersonnelAdminGroups] ([ClientPersonnelAdminGroupID])
GO
ALTER TABLE [dbo].[GroupRightsDynamic] ADD CONSTRAINT [FK_GroupRightsDynamic_FunctionType] FOREIGN KEY ([FunctionTypeID]) REFERENCES [dbo].[FunctionType] ([FunctionTypeID])
GO
ALTER TABLE [dbo].[GroupRightsDynamic] ADD CONSTRAINT [FK_GroupRightsDynamic_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
ALTER TABLE [dbo].[GroupRightsDynamic] ADD CONSTRAINT [FK_GroupRightsDynamic_Rights] FOREIGN KEY ([RightID]) REFERENCES [dbo].[Rights] ([RightID])
GO
