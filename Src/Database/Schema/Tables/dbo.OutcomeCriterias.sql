CREATE TABLE [dbo].[OutcomeCriterias]
(
[OutcomeCriteriaID] [int] NOT NULL IDENTITY(1, 1),
[OutcomeID] [int] NULL,
[QuestionPossibleAnswerID] [int] NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[OutcomeCriterias] ADD CONSTRAINT [PK_OutcomeCriterias] PRIMARY KEY CLUSTERED  ([OutcomeCriteriaID])
GO
ALTER TABLE [dbo].[OutcomeCriterias] ADD CONSTRAINT [FK_OutcomeCriterias_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[OutcomeCriterias] ADD CONSTRAINT [FK_OutcomeCriterias_Outcomes] FOREIGN KEY ([OutcomeID]) REFERENCES [dbo].[Outcomes] ([OutcomeID])
GO
