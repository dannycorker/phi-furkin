CREATE TABLE [dbo].[DataLoaderFileAbort]
(
[DataLoaderFileAbortID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DateTimeFileAborted] [datetime] NOT NULL,
[ReasonForAbort] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[DataLoaderFileID] [int] NOT NULL,
[FileAbortedOn] [datetime] NULL,
[LastRowImported] [int] NULL
)
GO
ALTER TABLE [dbo].[DataLoaderFileAbort] ADD CONSTRAINT [PK_DataLoaderFileAbort] PRIMARY KEY CLUSTERED  ([DataLoaderFileAbortID])
GO
ALTER TABLE [dbo].[DataLoaderFileAbort] ADD CONSTRAINT [FK_DataLoaderFileAbort_DataLoaderFile] FOREIGN KEY ([DataLoaderFileID]) REFERENCES [dbo].[DataLoaderFile] ([DataLoaderFileID]) ON DELETE CASCADE
GO
