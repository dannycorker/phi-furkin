CREATE TABLE [dbo].[RulesEngine_Operators]
(
[OperatorID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[OperatorText] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[IncludeVal1] [bit] NOT NULL CONSTRAINT [DF__RulesEngi__Inclu__7D63964E] DEFAULT ((0)),
[IncludeVal2] [bit] NOT NULL CONSTRAINT [DF__RulesEngi__Inclu__7E57BA87] DEFAULT ((0))
)
GO
ALTER TABLE [dbo].[RulesEngine_Operators] ADD CONSTRAINT [PK_RulesEngine_Operators] PRIMARY KEY CLUSTERED  ([OperatorID])
GO
