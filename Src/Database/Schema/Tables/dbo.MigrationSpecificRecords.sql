CREATE TABLE [dbo].[MigrationSpecificRecords]
(
[MatterID] [int] NOT NULL,
[CaseID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[MigrationSpecificRecords] ADD CONSTRAINT [PK_MigrationSpecificRecords] PRIMARY KEY CLUSTERED  ([MatterID])
GO
CREATE NONCLUSTERED INDEX [IX_MigrationSpecificRecords_Case] ON [dbo].[MigrationSpecificRecords] ([CaseID])
GO
CREATE NONCLUSTERED INDEX [IX_MigrationSpecificRecords_Customer] ON [dbo].[MigrationSpecificRecords] ([CustomerID])
GO
CREATE NONCLUSTERED INDEX [IX_MigrationSpecificRecords_Lead] ON [dbo].[MigrationSpecificRecords] ([LeadID])
GO
