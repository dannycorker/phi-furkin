CREATE TABLE [dbo].[Dashboard]
(
[DashboardID] [int] NOT NULL IDENTITY(1, 1),
[DashboardName] [nchar] (10) COLLATE Latin1_General_CI_AS NULL,
[DashboardDescription] [nchar] (10) COLLATE Latin1_General_CI_AS NULL,
[OwnerID] [nchar] (10) COLLATE Latin1_General_CI_AS NULL,
[DashboardLayoutID] [int] NOT NULL,
[ClientID] [int] NULL
)
GO
ALTER TABLE [dbo].[Dashboard] ADD CONSTRAINT [PK_Dashboard_1] PRIMARY KEY CLUSTERED  ([DashboardID])
GO
ALTER TABLE [dbo].[Dashboard] ADD CONSTRAINT [FK_Dashboard_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[Dashboard] ADD CONSTRAINT [FK_Dashboard_DashboardLayout] FOREIGN KEY ([DashboardLayoutID]) REFERENCES [dbo].[DashboardLayout] ([DashboardLayoutID])
GO
