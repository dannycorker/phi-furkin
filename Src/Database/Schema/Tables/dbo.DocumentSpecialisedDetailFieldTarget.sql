CREATE TABLE [dbo].[DocumentSpecialisedDetailFieldTarget]
(
[DocumentSpecialisedDetailFieldTargetID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DocumentTypeID] [int] NOT NULL,
[Target] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[DetailFieldID] [int] NULL,
[ColumnField] [int] NULL,
[TemplateTypeID] [int] NOT NULL,
[DetailFieldAlias] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[ColumnFieldAlias] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[RowField] [int] NULL,
[Notes] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ExcelSheetLocation] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Format] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[DocumentTypeVersionID] [int] NULL
)
GO
ALTER TABLE [dbo].[DocumentSpecialisedDetailFieldTarget] ADD CONSTRAINT [PK_DocumentSpecialisedDetailFieldTargetID] PRIMARY KEY CLUSTERED  ([DocumentSpecialisedDetailFieldTargetID])
GO
ALTER TABLE [dbo].[DocumentSpecialisedDetailFieldTarget] ADD CONSTRAINT [FK_DocumentSpecialisedDetailFieldTarget_DocumentTypeVersion] FOREIGN KEY ([DocumentTypeVersionID]) REFERENCES [dbo].[DocumentTypeVersion] ([DocumentTypeVersionID])
GO
ALTER TABLE [dbo].[DocumentSpecialisedDetailFieldTarget] ADD CONSTRAINT [FK_DocumentSpecialisedDetailFieldTarget_TemplateType] FOREIGN KEY ([TemplateTypeID]) REFERENCES [dbo].[TemplateType] ([TemplateTypeID])
GO
ALTER TABLE [dbo].[DocumentSpecialisedDetailFieldTarget] ADD CONSTRAINT [FK_DocumentSpecialisedDetailFieldTargetID_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DocumentSpecialisedDetailFieldTarget] ADD CONSTRAINT [FK_DocumentSpecialisedDetailFieldTargetID_DocumentType] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentType] ([DocumentTypeID])
GO
