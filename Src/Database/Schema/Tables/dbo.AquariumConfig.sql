CREATE TABLE [dbo].[AquariumConfig]
(
[ConfigID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubClientID] [int] NOT NULL,
[CategoryID] [int] NOT NULL,
[NameID] [int] NOT NULL,
[AccessTypeID] [int] NOT NULL,
[AccessTypeValueID] [int] NOT NULL,
[Encrypted] [bit] NOT NULL CONSTRAINT [DF__AquariumC__Encry__35DCF99B] DEFAULT ((0))
)
GO
ALTER TABLE [dbo].[AquariumConfig] ADD CONSTRAINT [PK_AquariumConfig] PRIMARY KEY CLUSTERED  ([ConfigID])
GO
ALTER TABLE [dbo].[AquariumConfig] ADD CONSTRAINT [FK_AquariumConfig_AquariumConfigAccessType] FOREIGN KEY ([AccessTypeID]) REFERENCES [dbo].[AquariumConfigAccessType] ([AccessTypeID])
GO
ALTER TABLE [dbo].[AquariumConfig] ADD CONSTRAINT [FK_AquariumConfig_AquariumConfigCategory] FOREIGN KEY ([CategoryID]) REFERENCES [dbo].[AquariumConfigCategory] ([CategoryID])
GO
ALTER TABLE [dbo].[AquariumConfig] ADD CONSTRAINT [FK_AquariumConfig_AquariumConfigName] FOREIGN KEY ([NameID]) REFERENCES [dbo].[AquariumConfigName] ([NameID])
GO
ALTER TABLE [dbo].[AquariumConfig] ADD CONSTRAINT [FK_AquariumConfig_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[AquariumConfig] ADD CONSTRAINT [FK_AquariumConfig_SubClient] FOREIGN KEY ([SubClientID]) REFERENCES [dbo].[SubClient] ([SubClientID])
GO
