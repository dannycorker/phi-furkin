CREATE TABLE [dbo].[MediateQueuePolicyAudit]
(
[MediateQueuePolicyAuditID] [int] NOT NULL IDENTITY(1, 1),
[MediateQueueID] [int] NULL,
[OneVisionID] [int] NULL,
[ProcessedDateTime] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[MediateQueuePolicyAudit] ADD CONSTRAINT [PK_MediateQueuePolicyAuditID] PRIMARY KEY CLUSTERED  ([MediateQueuePolicyAuditID])
GO
