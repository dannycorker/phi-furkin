CREATE TABLE [dbo].[DataLoaderMap]
(
[DataLoaderMapID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[MapName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[MapDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[CreatedBy] [int] NOT NULL,
[LastUpdated] [datetime] NOT NULL,
[UpdatedBy] [int] NOT NULL,
[EnabledForUse] [bit] NOT NULL,
[Deleted] [bit] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[UseDefaultLeadForCustomer] [bit] NOT NULL,
[UseDefaultCaseForLead] [bit] NOT NULL,
[UseDefaultMatterForCase] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[DataLoaderMap] ADD CONSTRAINT [PK_DataLoaderMap] PRIMARY KEY CLUSTERED  ([DataLoaderMapID])
GO
ALTER TABLE [dbo].[DataLoaderMap] ADD CONSTRAINT [FK_DataLoaderMap_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DataLoaderMap] ADD CONSTRAINT [FK_DataLoaderMap_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[DataLoaderMap] ADD CONSTRAINT [FK_DataLoaderMap_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DataLoaderMap] ADD CONSTRAINT [FK_DataLoaderMap_UpdatedBy] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
