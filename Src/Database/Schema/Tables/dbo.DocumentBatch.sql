CREATE TABLE [dbo].[DocumentBatch]
(
[DocumentBatchID] [int] NOT NULL IDENTITY(1, 1),
[BatchID] [int] NOT NULL,
[MetadataXml] [xml] NOT NULL,
[Processed] [bit] NOT NULL CONSTRAINT [DF__DocumentB__Proce__5555A4F4] DEFAULT ((0)),
[Uploaded] [bit] NOT NULL CONSTRAINT [DF__DocumentB__Uploa__5649C92D] DEFAULT ((0)),
[ClientID] [int] NOT NULL,
[BatchProductionDate] [datetime] NULL,
[UploadedDate] [datetime] NULL CONSTRAINT [DF__DocumentB__Uploa__39578374] DEFAULT ([dbo].[fn_GetDate_Local]())
)
GO
ALTER TABLE [dbo].[DocumentBatch] ADD CONSTRAINT [PK_DocumentBatch] PRIMARY KEY CLUSTERED  ([DocumentBatchID])
GO
