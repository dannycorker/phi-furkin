CREATE TABLE [dbo].[OneVision]
(
[OneVisionID] [int] NOT NULL IDENTITY(1, 1),
[VisionCustomerID] [int] NULL,
[VisionPolicyID] [int] NULL,
[VisionPetID] [int] NULL,
[VisionPolicyTermID] [int] NULL,
[CustomerID] [int] NOT NULL,
[PALeadID] [int] NOT NULL,
[PAMatterID] [int] NOT NULL,
[HistoricalPolicyTableRow] [int] NULL,
[VisionAddressID] [int] NULL,
[PolicyTermType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[OneVision] ADD CONSTRAINT [PK_OneVision] PRIMARY KEY CLUSTERED  ([OneVisionID])
GO
ALTER TABLE [dbo].[OneVision] ADD CONSTRAINT [FK_OneVision_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[OneVision] ADD CONSTRAINT [FK_OneVision_Lead] FOREIGN KEY ([PALeadID]) REFERENCES [dbo].[Lead] ([LeadID])
GO
ALTER TABLE [dbo].[OneVision] ADD CONSTRAINT [FK_OneVision_Matter] FOREIGN KEY ([PAMatterID]) REFERENCES [dbo].[Matter] ([MatterID])
GO
ALTER TABLE [dbo].[OneVision] ADD CONSTRAINT [FK_OneVision_TableRows] FOREIGN KEY ([HistoricalPolicyTableRow]) REFERENCES [dbo].[TableRows] ([TableRowID])
GO
GRANT UPDATE ON  [dbo].[OneVision] TO [AquariumNet]
GO
