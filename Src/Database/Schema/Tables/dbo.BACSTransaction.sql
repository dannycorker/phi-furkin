CREATE TABLE [dbo].[BACSTransaction]
(
[BACSTransactionID] [int] NOT NULL IDENTITY(1, 1),
[BACSFileID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[ObjectID] [int] NULL,
[ObjectTypeID] [int] NULL,
[PurchasedProductPaymentScheduleID] [int] NULL,
[CustomerPaymentScheduleID] [int] NULL,
[PaymentTypeID] [int] NULL,
[DebitAccountName] [varchar] (105) COLLATE Latin1_General_CI_AS NULL,
[DebitSortCode] [varchar] (6) COLLATE Latin1_General_CI_AS NULL,
[DebitAccountNumber] [varchar] (8) COLLATE Latin1_General_CI_AS NULL,
[CurrencyID] [int] NULL,
[PaymentAmount] [numeric] (18, 2) NULL,
[PaymentReference] [varchar] (16) COLLATE Latin1_General_CI_AS NULL,
[BACSProcessingDate] [datetime] NULL,
[BeneficiaryAccountNumber] [varchar] (8) COLLATE Latin1_General_CI_AS NULL,
[BeneficiarySortCode] [varchar] (6) COLLATE Latin1_General_CI_AS NULL,
[BeneficiaryName] [varchar] (35) COLLATE Latin1_General_CI_AS NULL,
[BeneficiaryAddress] [varchar] (105) COLLATE Latin1_General_CI_AS NULL,
[BeneficiaryBankName] [varchar] (35) COLLATE Latin1_General_CI_AS NULL,
[BeneficiaryBankAddress] [varchar] (105) COLLATE Latin1_General_CI_AS NULL,
[SUN] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FailureCode] [varchar] (16) COLLATE Latin1_General_CI_AS NULL,
[FailureDate] [datetime] NULL,
[DeemedTransactionDate] [date] NULL,
[DeemedTransactedOn] [datetime] NULL,
[RowNumberInFile] [int] NULL,
[BACSTransactionCodeID] [int] NULL,
[LeadEventID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[BACSTransaction] ADD CONSTRAINT [PK_BACsTransaction] PRIMARY KEY CLUSTERED  ([BACSTransactionID])
GO
