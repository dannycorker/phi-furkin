CREATE TABLE [dbo].[SMSQuestionnaire]
(
[SMSQuestionnaireID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[Title] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Enabled] [bit] NOT NULL,
[ToTarget] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[CreateCustomerIfNotFound] [bit] NULL,
[UseSentimentAnalysis] [bit] NULL,
[RuleSetID] [int] NULL,
[IsVoice] [bit] NULL,
[Timeout] [int] NULL,
[Repeat] [int] NULL,
[RepeatPrefix] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SMSQuestionnaire] ADD CONSTRAINT [PK_SMSQuestionnaire] PRIMARY KEY CLUSTERED  ([SMSQuestionnaireID])
GO
ALTER TABLE [dbo].[SMSQuestionnaire] ADD CONSTRAINT [FK_SMSQuestionnaire_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
