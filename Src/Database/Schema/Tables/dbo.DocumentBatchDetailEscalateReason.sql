CREATE TABLE [dbo].[DocumentBatchDetailEscalateReason]
(
[DocumentBatchDetailEscalateReasonID] [int] NOT NULL IDENTITY(1, 1),
[DocumentBatchDetailID] [int] NOT NULL,
[Reason] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[UserIDTo] [int] NOT NULL,
[EscalatedOn] [datetime] NOT NULL,
[Escalated] [bit] NOT NULL,
[UserIDBy] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[DocumentBatchDetailEscalateReason] ADD CONSTRAINT [PK_DocumentBatchEscalateReason] PRIMARY KEY CLUSTERED  ([DocumentBatchDetailEscalateReasonID])
GO
