CREATE TABLE [dbo].[WorkWinsPossibleValidMatters]
(
[WorkWinsPossibleValidMatterID] [int] NOT NULL IDENTITY(1, 1),
[WorkWinSeedID] [int] NOT NULL,
[MatterID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[WorkWinsPossibleValidMatters] ADD CONSTRAINT [PK_WorkWinsPossibleValidMatters] PRIMARY KEY CLUSTERED  ([WorkWinsPossibleValidMatterID])
GO
CREATE NONCLUSTERED INDEX [IX_WorkWinsPossibleValidMatters] ON [dbo].[WorkWinsPossibleValidMatters] ([WorkWinSeedID], [MatterID], [DetailFieldID])
GO
ALTER TABLE [dbo].[WorkWinsPossibleValidMatters] ADD CONSTRAINT [FK_WorkWinsPossibleValidMatters_WorkWinSeed] FOREIGN KEY ([WorkWinSeedID]) REFERENCES [dbo].[WorkWinSeed] ([WorkWinSeedID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[WorkWinsPossibleValidMatters] ADD CONSTRAINT [FK_WorkWinsPossibleValidMatters_WorkWinsPossibleValidMatters] FOREIGN KEY ([WorkWinsPossibleValidMatterID]) REFERENCES [dbo].[WorkWinsPossibleValidMatters] ([WorkWinsPossibleValidMatterID])
GO
