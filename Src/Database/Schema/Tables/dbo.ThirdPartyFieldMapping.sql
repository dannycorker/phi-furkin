CREATE TABLE [dbo].[ThirdPartyFieldMapping]
(
[ThirdPartyFieldMappingID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[ThirdPartyFieldID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[ColumnFieldID] [int] NULL,
[ThirdPartyFieldGroupID] [int] NOT NULL,
[IsEnabled] [bit] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[ThirdPartyFieldMapping] ADD CONSTRAINT [PK_ThirdPartyFieldMapping] PRIMARY KEY CLUSTERED  ([ThirdPartyFieldMappingID])
GO
CREATE NONCLUSTERED INDEX [IX_ThirdPartyFieldMapping] ON [dbo].[ThirdPartyFieldMapping] ([ClientID], [ThirdPartyFieldGroupID], [ThirdPartyFieldID])
GO
ALTER TABLE [dbo].[ThirdPartyFieldMapping] ADD CONSTRAINT [FK_ThirdPartyFieldMapping_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ThirdPartyFieldMapping] ADD CONSTRAINT [FK_ThirdPartyFieldMapping_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[ThirdPartyFieldMapping] ADD CONSTRAINT [FK_ThirdPartyFieldMapping_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
ALTER TABLE [dbo].[ThirdPartyFieldMapping] ADD CONSTRAINT [FK_ThirdPartyFieldMapping_ThirdPartyField] FOREIGN KEY ([ThirdPartyFieldID]) REFERENCES [dbo].[ThirdPartyField] ([ThirdPartyFieldID])
GO
ALTER TABLE [dbo].[ThirdPartyFieldMapping] ADD CONSTRAINT [FK_ThirdPartyFieldMapping_ThirdPartyFieldGroup] FOREIGN KEY ([ThirdPartyFieldGroupID]) REFERENCES [dbo].[ThirdPartyFieldGroup] ([ThirdPartyFieldGroupID])
GO
