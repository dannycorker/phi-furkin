CREATE TABLE [dbo].[OneIncRequest]
(
[OneIncRequestID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NULL,
[MatterID] [int] NULL,
[RequestJson] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[OneIncRequest] ADD CONSTRAINT [PK_OneIncRequest] PRIMARY KEY CLUSTERED  ([OneIncRequestID])
GO
