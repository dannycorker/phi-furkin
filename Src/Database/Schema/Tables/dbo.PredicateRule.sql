CREATE TABLE [dbo].[PredicateRule]
(
[PredicateRuleID] [int] NOT NULL IDENTITY(1, 1),
[RuleName] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[RuleTypeID] [int] NOT NULL,
[FieldTypeID] [int] NOT NULL,
[FieldID] [int] NOT NULL,
[FieldComparisonTypeID] [int] NOT NULL,
[FieldValueTypeID] [int] NOT NULL,
[FieldValue1] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[FieldValue2] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[EventTypeID] [int] NOT NULL,
[AppliesTo] [int] NOT NULL,
[Count] [int] NOT NULL,
[RuleSQL] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[LeadTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[PredicateRule] ADD CONSTRAINT [PK_PredicateRule] PRIMARY KEY CLUSTERED  ([PredicateRuleID])
GO
