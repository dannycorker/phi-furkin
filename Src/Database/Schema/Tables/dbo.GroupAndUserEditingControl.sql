CREATE TABLE [dbo].[GroupAndUserEditingControl]
(
[EditingID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelAdminGroupID] [int] NOT NULL,
[ClientPersonnelID] [int] NULL,
[WhoIsEditing] [int] NOT NULL,
[EditStartedAt] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[GroupAndUserEditingControl] ADD CONSTRAINT [PK_GroupAndUserEditingControl] PRIMARY KEY CLUSTERED  ([EditingID])
GO
ALTER TABLE [dbo].[GroupAndUserEditingControl] ADD CONSTRAINT [FK_GroupAndUserEditingControl_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[GroupAndUserEditingControl] ADD CONSTRAINT [FK_GroupAndUserEditingControl_ClientPersonnelAdminGroups] FOREIGN KEY ([ClientPersonnelAdminGroupID]) REFERENCES [dbo].[ClientPersonnelAdminGroups] ([ClientPersonnelAdminGroupID])
GO
ALTER TABLE [dbo].[GroupAndUserEditingControl] ADD CONSTRAINT [FK_GroupAndUserEditingControl_WhoIsEditing] FOREIGN KEY ([WhoIsEditing]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
