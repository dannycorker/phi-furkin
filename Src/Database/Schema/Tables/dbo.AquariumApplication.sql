CREATE TABLE [dbo].[AquariumApplication]
(
[AquariumApplicationID] [int] NOT NULL IDENTITY(1, 1),
[ApplicationName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ApplicationDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[CurrentVersion] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[MinimumVersion] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[CurrentVersionValidFrom] [datetime] NULL,
[CurrentVersionValidTo] [datetime] NULL,
[CurrentVersionDeployedOn] [datetime] NULL
)
GO
ALTER TABLE [dbo].[AquariumApplication] ADD CONSTRAINT [PK_AquariumApplication] PRIMARY KEY CLUSTERED  ([AquariumApplicationID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_AquariumApplication] ON [dbo].[AquariumApplication] ([ApplicationName])
GO
