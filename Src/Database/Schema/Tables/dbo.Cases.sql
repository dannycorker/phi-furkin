CREATE TABLE [dbo].[Cases]
(
[CaseID] [int] NOT NULL IDENTITY(1, 1),
[LeadID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[CaseNum] [int] NOT NULL CONSTRAINT [DF__Cases__CaseNum__3B95D2F1] DEFAULT ((1)),
[CaseRef] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ClientStatusID] [int] NULL,
[AquariumStatusID] [int] NULL,
[DefaultContactID] [int] NULL,
[LatestLeadEventID] [int] NULL,
[LatestInProcessLeadEventID] [int] NULL,
[LatestOutOfProcessLeadEventID] [int] NULL,
[LatestNonNoteLeadEventID] [int] NULL,
[LatestNoteLeadEventID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL CONSTRAINT [DF__Cases__WhenCreat__376F3B02] DEFAULT ([dbo].[fn_GetDate_Local]()),
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[ProcessStartLeadEventID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2013-10-08
-- Description:	Ensure that closed leads are re-opened when subsequent cases are added
-- =============================================
CREATE TRIGGER [dbo].[trgi_Cases]
   ON  [dbo].[Cases]
   AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	/*
		For each inserted Cases record, make sure that the matching Lead now has a status of Accepted (2) where it was Complete (4).
		There will normally only be 1 insert at a time from the app, but could be many from T-SQL.
		Only update each Lead once no matter how many Cases are being inserted, hence the InnerSql block.
	*/
	;WITH InnerSql AS
	(
		SELECT i.LeadID, ROW_NUMBER() OVER(PARTITION BY i.LeadID ORDER BY i.CaseNum DESC) as rn
		FROM inserted i
		WHERE i.CaseNum > 1
	)
	UPDATE dbo.Lead
	SET AquariumStatusID = 2
	FROM InnerSql isql
	INNER JOIN dbo.Lead l ON isql.LeadID = l.LeadID
	WHERE l.AquariumStatusID = 4
	AND isql.rn = 1

END
GO
ALTER TABLE [dbo].[Cases] ADD CONSTRAINT [PK_Cases] PRIMARY KEY CLUSTERED  ([CaseID])
GO
CREATE NONCLUSTERED INDEX [IX_CasesClient] ON [dbo].[Cases] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_Cases_Joins2017V2] ON [dbo].[Cases] ([ClientID], [ClientStatusID]) INCLUDE ([CaseNum], [LatestInProcessLeadEventID], [LatestNonNoteLeadEventID], [LatestNoteLeadEventID], [LatestOutOfProcessLeadEventID], [LeadID], [ProcessStartLeadEventID])
GO
CREATE NONCLUSTERED INDEX [IX_Cases_Covering] ON [dbo].[Cases] ([ClientID], [LeadID], [ClientStatusID], [AquariumStatusID], [CaseNum])
GO
CREATE NONCLUSTERED INDEX [IX_CasesClientStatus] ON [dbo].[Cases] ([ClientStatusID])
GO
CREATE NONCLUSTERED INDEX [IX_CasesLead] ON [dbo].[Cases] ([LeadID])
GO
CREATE NONCLUSTERED INDEX [IX_CasesLeadProcessStart] ON [dbo].[Cases] ([LeadID], [ProcessStartLeadEventID])
GO
ALTER TABLE [dbo].[Cases] ADD CONSTRAINT [FK_Cases_AquariumStatus] FOREIGN KEY ([AquariumStatusID]) REFERENCES [dbo].[AquariumStatus] ([AquariumStatusID])
GO
ALTER TABLE [dbo].[Cases] ADD CONSTRAINT [FK_Cases_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[Cases] ADD CONSTRAINT [FK_Cases_Contact] FOREIGN KEY ([DefaultContactID]) REFERENCES [dbo].[Contact] ([ContactID])
GO
ALTER TABLE [dbo].[Cases] ADD CONSTRAINT [FK_Cases_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Cases] ADD CONSTRAINT [FK_Cases_LeadStatus] FOREIGN KEY ([ClientStatusID]) REFERENCES [dbo].[LeadStatus] ([StatusID])
GO
