CREATE TABLE [dbo].[DocumentSignProviderType]
(
[DSPTypeID] [int] NOT NULL IDENTITY(1, 1),
[DSPTypeName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[DSPSignatureTemplate] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[DocumentSignProviderType] ADD CONSTRAINT [PK_DocumentSignProviderType] PRIMARY KEY CLUSTERED  ([DSPTypeID])
GO
