CREATE TABLE [dbo].[PortalStatusOrder]
(
[PortalStatusOrderID] [int] NOT NULL IDENTITY(1, 1),
[StatusID] [int] NOT NULL,
[PortalOrder] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[PortalStatusOrder] ADD CONSTRAINT [PK_PortalStatusOrder] PRIMARY KEY CLUSTERED  ([PortalStatusOrderID])
GO
