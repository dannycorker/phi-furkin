CREATE TABLE [dbo].[EventTypeAutomatedWorkflow]
(
[EventTypeAutomatedWorkflowID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[WorkflowGroupID] [int] NOT NULL,
[RunAsUserID] [int] NULL,
[FollowUp] [bit] NOT NULL,
[EventTypeToAdd] [int] NOT NULL,
[Priority] [tinyint] NOT NULL,
[Important] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[EventTypeAutomatedWorkflow] ADD CONSTRAINT [PK_EventTypeAutomatedWorkflow] PRIMARY KEY CLUSTERED  ([EventTypeAutomatedWorkflowID])
GO
ALTER TABLE [dbo].[EventTypeAutomatedWorkflow] ADD CONSTRAINT [FK_EventTypeAutomatedWorkflow_AutomatedEventType] FOREIGN KEY ([EventTypeToAdd]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[EventTypeAutomatedWorkflow] ADD CONSTRAINT [FK_EventTypeAutomatedWorkflow_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EventTypeAutomatedWorkflow] ADD CONSTRAINT [FK_EventTypeAutomatedWorkflow_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
