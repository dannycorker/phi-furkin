CREATE TABLE [dbo].[LeadTypePageTitleStandardTarget]
(
[LeadTypePageTitleStandardTargetID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[Target] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[ObjectName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[PropertyName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[TemplateTypeID] [int] NOT NULL,
[Notes] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[LeadTypePageTitleStandardTarget] ADD CONSTRAINT [PK_LeadTypePageTitleStandardTarget] PRIMARY KEY CLUSTERED  ([LeadTypePageTitleStandardTargetID])
GO
ALTER TABLE [dbo].[LeadTypePageTitleStandardTarget] ADD CONSTRAINT [FK_LeadTypePageTitleStandardTarget_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[LeadTypePageTitleStandardTarget] ADD CONSTRAINT [FK_LeadTypePageTitleStandardTarget_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
ALTER TABLE [dbo].[LeadTypePageTitleStandardTarget] ADD CONSTRAINT [FK_LeadTypePageTitleStandardTarget_TemplateType] FOREIGN KEY ([TemplateTypeID]) REFERENCES [dbo].[TemplateType] ([TemplateTypeID])
GO
