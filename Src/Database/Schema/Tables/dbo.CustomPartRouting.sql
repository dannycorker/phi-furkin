CREATE TABLE [dbo].[CustomPartRouting]
(
[CustomPartRouteID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[PartID] [int] NOT NULL,
[MapsToLeadTypeID] [int] NULL,
[MapsToClientID] [int] NULL
)
GO
ALTER TABLE [dbo].[CustomPartRouting] ADD CONSTRAINT [PK_CustomPartRouting] PRIMARY KEY CLUSTERED  ([CustomPartRouteID])
GO
