CREATE TABLE [dbo].[ValidationCriterias]
(
[ValidationCriteriaID] [int] NOT NULL IDENTITY(1, 1),
[Criteria] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ValidationCriterias] ADD CONSTRAINT [PK_ValidationCriterias] PRIMARY KEY CLUSTERED  ([ValidationCriteriaID])
GO
