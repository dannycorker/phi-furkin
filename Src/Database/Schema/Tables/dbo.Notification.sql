CREATE TABLE [dbo].[Notification]
(
[NotificationID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[ThirdPartySystemID] [int] NULL,
[Message] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[DateMessageCreated] [datetime] NOT NULL,
[MessageCreatedBy] [int] NOT NULL,
[NotificationTypeID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[Notification] ADD CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED  ([NotificationID])
GO
ALTER TABLE [dbo].[Notification] ADD CONSTRAINT [FK_Notification_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[Notification] ADD CONSTRAINT [FK_Notification_NotificationType] FOREIGN KEY ([NotificationTypeID]) REFERENCES [dbo].[NotificationType] ([NotificationTypeID])
GO
ALTER TABLE [dbo].[Notification] ADD CONSTRAINT [FK_Notification_SubClient] FOREIGN KEY ([SubClientID]) REFERENCES [dbo].[SubClient] ([SubClientID])
GO
ALTER TABLE [dbo].[Notification] ADD CONSTRAINT [FK_Notification_ThirdPartySystem] FOREIGN KEY ([ThirdPartySystemID]) REFERENCES [dbo].[ThirdPartySystem] ([ThirdPartySystemId])
GO
