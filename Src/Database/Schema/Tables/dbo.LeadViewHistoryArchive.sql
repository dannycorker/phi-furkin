CREATE TABLE [dbo].[LeadViewHistoryArchive]
(
[LeadViewHistoryID] [int] NOT NULL,
[ClientPersonnelID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[WhenViewed] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[LeadViewHistoryArchive] ADD CONSTRAINT [PK_LeadViewHistoryArchive] PRIMARY KEY CLUSTERED  ([LeadViewHistoryID])
GO
