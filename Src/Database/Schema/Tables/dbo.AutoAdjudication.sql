CREATE TABLE [dbo].[AutoAdjudication]
(
[AutoAdjudicationID] [int] NOT NULL IDENTITY(1, 1),
[EventChoiceID] [int] NOT NULL,
[AutoAdjudicationTypeID] [int] NOT NULL,
[ObjectID] [int] NOT NULL,
[AutoAdjudicationFieldTypeID] [int] NULL,
[Comparison] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Value1] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Value2] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[AppliesTo] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Count] [int] NULL,
[ValueTypeID] [int] NULL
)
GO
ALTER TABLE [dbo].[AutoAdjudication] ADD CONSTRAINT [PK_AutoAdjudication] PRIMARY KEY CLUSTERED  ([AutoAdjudicationID])
GO
ALTER TABLE [dbo].[AutoAdjudication] ADD CONSTRAINT [FK_AutoAdjudication_AquariumOption] FOREIGN KEY ([ValueTypeID]) REFERENCES [dbo].[AquariumOption] ([AquariumOptionID])
GO
ALTER TABLE [dbo].[AutoAdjudication] ADD CONSTRAINT [FK_AutoAdjudication_AutoAdjudicationField] FOREIGN KEY ([AutoAdjudicationFieldTypeID]) REFERENCES [dbo].[AutoAdjudicationFieldType] ([AutoAdjudicationFieldTypeID])
GO
ALTER TABLE [dbo].[AutoAdjudication] ADD CONSTRAINT [FK_AutoAdjudication_AutoAdjudicationType] FOREIGN KEY ([AutoAdjudicationTypeID]) REFERENCES [dbo].[AutoAdjudicationType] ([AutoAdjudicationTypeID])
GO
ALTER TABLE [dbo].[AutoAdjudication] ADD CONSTRAINT [FK_AutoAdjudication_EventChoice] FOREIGN KEY ([EventChoiceID]) REFERENCES [dbo].[EventChoice] ([EventChoiceID]) ON DELETE CASCADE
GO
