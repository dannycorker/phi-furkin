CREATE TABLE [dbo].[Results]
(
[TextToSpeakID] [int] NOT NULL,
[ClientQuestionnaireID] [int] NULL,
[ClientID] [int] NOT NULL,
[PageNumber] [int] NULL,
[SpeakText] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[IsShown] [bit] NULL,
[AnyID] [int] NOT NULL IDENTITY(1, 1)
)
GO
ALTER TABLE [dbo].[Results] ADD CONSTRAINT [PK_Results] PRIMARY KEY CLUSTERED  ([AnyID])
GO
