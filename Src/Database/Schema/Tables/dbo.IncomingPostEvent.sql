CREATE TABLE [dbo].[IncomingPostEvent]
(
[IncomingPostEventID] [int] NOT NULL IDENTITY(1, 1),
[EventDateTime] [datetime] NOT NULL,
[ClientID] [int] NULL,
[CustomerID] [int] NULL,
[LeadID] [int] NULL,
[CaseID] [int] NULL,
[MatterID] [int] NULL
)
GO
ALTER TABLE [dbo].[IncomingPostEvent] ADD CONSTRAINT [PK_IncomingPostEvent] PRIMARY KEY CLUSTERED  ([IncomingPostEventID])
GO
