CREATE TABLE [dbo].[API_Queue]
(
[QueueIdx] [bigint] NOT NULL IDENTITY(1, 1),
[QueueID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__API_Queue__Queue__31CED39A] DEFAULT (newid()),
[AppKey] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ObjectKey] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[IdentID] [uniqueidentifier] NOT NULL,
[QueueDT] [datetime2] NOT NULL CONSTRAINT [DF__API_Queue__Queue__32C2F7D3] DEFAULT (sysdatetime()),
[QueueXml] [xml] NOT NULL,
[QueueByteSize] [int] NOT NULL,
[HandleDT] [datetime2] NULL,
[HandleXml] [xml] NULL,
[RecieveDT] [datetime2] NULL,
[ParentQID] [uniqueidentifier] NULL,
[ErrorMessage] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[API_Queue] ADD CONSTRAINT [PK_API_Queue] PRIMARY KEY CLUSTERED  ([QueueID])
GO
