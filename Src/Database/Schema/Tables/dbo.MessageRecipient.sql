CREATE TABLE [dbo].[MessageRecipient]
(
[MessageRecipientID] [int] NOT NULL IDENTITY(1, 1),
[MessageID] [int] NOT NULL,
[ClientPersonnelID] [int] NULL,
[PortalUserID] [int] NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[MessageRecipient] ADD CONSTRAINT [PK_MessageRecipient] PRIMARY KEY CLUSTERED  ([MessageRecipientID])
GO
ALTER TABLE [dbo].[MessageRecipient] ADD CONSTRAINT [FK_MessageRecipient_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[MessageRecipient] ADD CONSTRAINT [FK_MessageRecipient_Messages] FOREIGN KEY ([MessageID]) REFERENCES [dbo].[Messages] ([MessageID])
GO
