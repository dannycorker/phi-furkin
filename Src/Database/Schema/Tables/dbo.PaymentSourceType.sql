CREATE TABLE [dbo].[PaymentSourceType]
(
[PaymentSourceTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[SourceType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[PaymentSourceType] ADD CONSTRAINT [PK_PaymentSourceType] PRIMARY KEY CLUSTERED  ([PaymentSourceTypeID])
GO
