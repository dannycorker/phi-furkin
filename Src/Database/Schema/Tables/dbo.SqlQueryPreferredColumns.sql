CREATE TABLE [dbo].[SqlQueryPreferredColumns]
(
[ReportPreferredColumnID] [int] NOT NULL IDENTITY(1, 1),
[ReportTableID] [int] NOT NULL,
[ReportColumnName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ShowColumn] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[SqlQueryPreferredColumns] ADD CONSTRAINT [PK_SqlQueryPreferredColumns] PRIMARY KEY CLUSTERED  ([ReportPreferredColumnID])
GO
