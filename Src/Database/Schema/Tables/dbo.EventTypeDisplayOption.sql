CREATE TABLE [dbo].[EventTypeDisplayOption]
(
[EventTypeDisplayOptionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[ShowAll] [bit] NOT NULL,
[ShowLastN] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[EventTypeDisplayOption] ADD CONSTRAINT [PK_EventTypeDisplayOption] PRIMARY KEY CLUSTERED  ([EventTypeDisplayOptionID])
GO
