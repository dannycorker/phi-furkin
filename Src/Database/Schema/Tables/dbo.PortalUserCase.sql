CREATE TABLE [dbo].[PortalUserCase]
(
[PortalUserCaseID] [int] NOT NULL IDENTITY(1, 1),
[PortalUserID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[CaseID] [int] NULL,
[ClientID] [int] NULL
)
GO
ALTER TABLE [dbo].[PortalUserCase] ADD CONSTRAINT [PK_PortalUserCase] PRIMARY KEY CLUSTERED  ([PortalUserCaseID])
GO
CREATE NONCLUSTERED INDEX [IX_PortalUserCase_Covering] ON [dbo].[PortalUserCase] ([PortalUserID], [LeadID], [CaseID], [ClientID])
GO
ALTER TABLE [dbo].[PortalUserCase] ADD CONSTRAINT [FK_PortalUserCase_Cases] FOREIGN KEY ([CaseID]) REFERENCES [dbo].[Cases] ([CaseID])
GO
ALTER TABLE [dbo].[PortalUserCase] ADD CONSTRAINT [FK_PortalUserCase_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID])
GO
ALTER TABLE [dbo].[PortalUserCase] ADD CONSTRAINT [FK_PortalUserCase_PortalUser] FOREIGN KEY ([PortalUserID]) REFERENCES [dbo].[PortalUser] ([PortalUserID]) ON DELETE CASCADE
GO
