CREATE TABLE [dbo].[ChangeControl]
(
[ChangeControlID] [int] NOT NULL IDENTITY(1, 1),
[Instance] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[DatabaseName] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Branch] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[UserName] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[ChangeDateTime] [datetime] NOT NULL,
[ObjectType] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[ObjectName] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[ObjectDescription] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[ObjectReference] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Latest] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[ChangeControl] ADD CONSTRAINT [PK_ChangeControl] PRIMARY KEY CLUSTERED  ([ChangeControlID])
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [AaranGravestock]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [AndrewHerndlhofer]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [AndrewHodkinson]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [BuildManager]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [CathalSherry]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [ChristianWilliams]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [DanielleSaccomano]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [EdShropshire]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [GavinReynolds]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [IanSlack]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [JamesLewis]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [JimGreen]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [LouisBromilow]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [MarkBeaumont]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [NeilAfflick]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [NessaGreen]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [OwenAshcroft]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [StephenAinsworth]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [StephenWiggins]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [SusanSwanton]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [ThomasDoyle]
GO
GRANT INSERT ON  [dbo].[ChangeControl] TO [UmerHamid]
GO
