CREATE TABLE [dbo].[ClientDataUsageHistory]
(
[ClientDataUsageHistoryID] [int] NOT NULL IDENTITY(1, 1),
[ClientDataUsageID] [int] NOT NULL,
[ClientDataUsageBillingRunID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[DocumentBytes] [bigint] NULL,
[ClientImagesBytes] [bigint] NULL,
[StandardTablesBytes] [bigint] NULL,
[CustomTablesBytes] [bigint] NULL,
[TotalBytes] [bigint] NULL,
[UserNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ClientDataUsageHistory] ADD CONSTRAINT [PK_ClientDataUsageHistory] PRIMARY KEY CLUSTERED  ([ClientDataUsageHistoryID])
GO
