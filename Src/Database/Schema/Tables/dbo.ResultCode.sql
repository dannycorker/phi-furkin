CREATE TABLE [dbo].[ResultCode]
(
[ResultCodeID] [varchar] (4) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ResultCode] ADD CONSTRAINT [PK_ResultCode] PRIMARY KEY CLUSTERED  ([ResultCodeID])
GO
