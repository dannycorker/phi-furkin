CREATE TABLE [dbo].[CustomerPaymentSchedule]
(
[CustomerPaymentScheduleID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[AccountID] [int] NOT NULL,
[ActualCollectionDate] [datetime] NULL,
[PaymentDate] [datetime] NULL,
[PaymentNet] [numeric] (18, 2) NULL,
[PaymentVAT] [numeric] (18, 2) NULL,
[PaymentGross] [numeric] (18, 2) NULL,
[PaymentStatusID] [int] NULL,
[CustomerLedgerID] [int] NULL,
[ReconciledDate] [datetime] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[SourceID] [int] NULL,
[RelatedObjectID] [int] NULL,
[RelatedObjectTypeID] [int] NULL,
[ClientAccountID] [int] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[LegacyRef] [int] NULL
)
GO
ALTER TABLE [dbo].[CustomerPaymentSchedule] ADD CONSTRAINT [PK_CustomerPaymentSchedule] PRIMARY KEY CLUSTERED  ([CustomerPaymentScheduleID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerPaymentSchedule_AccountID] ON [dbo].[CustomerPaymentSchedule] ([AccountID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerPaymentSchedule_ActualCollectionDate] ON [dbo].[CustomerPaymentSchedule] ([ActualCollectionDate])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerPaymentSchedule_ClientID] ON [dbo].[CustomerPaymentSchedule] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerPaymentSchedule_Customer] ON [dbo].[CustomerPaymentSchedule] ([CustomerID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerPaymentSchedule_CustomerAmountStatus] ON [dbo].[CustomerPaymentSchedule] ([CustomerID], [PaymentGross], [PaymentStatusID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerPaymentSchedule_CustomerLedgerID] ON [dbo].[CustomerPaymentSchedule] ([CustomerLedgerID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerpaymentSchedule_LegacyRef] ON [dbo].[CustomerPaymentSchedule] ([LegacyRef])
GO
ALTER TABLE [dbo].[CustomerPaymentSchedule] ADD CONSTRAINT [FK_CustomerPaymentSchedule_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
GO
ALTER TABLE [dbo].[CustomerPaymentSchedule] ADD CONSTRAINT [FK_CustomerPaymentSchedule_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[CustomerPaymentSchedule] ADD CONSTRAINT [FK_CustomerPaymentSchedule_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
