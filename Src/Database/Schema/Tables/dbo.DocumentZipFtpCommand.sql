CREATE TABLE [dbo].[DocumentZipFtpCommand]
(
[DocumentZipFtpCommandID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DocumentZipFtpControlID] [int] NOT NULL,
[LineType] [char] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[LineText] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[LineNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[LineOrder] [int] NOT NULL,
[Enabled] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[DocumentZipFtpCommand] ADD CONSTRAINT [CK__DocumentZ__LineT__1CDC41A7] CHECK (([LineType]='cfg' OR [LineType]='bat'))
GO
ALTER TABLE [dbo].[DocumentZipFtpCommand] ADD CONSTRAINT [PK_DocumentZipFtpCommand] PRIMARY KEY NONCLUSTERED  ([DocumentZipFtpCommandID])
GO
CREATE CLUSTERED INDEX [IX_DocumentZipFtpCommand_ControlLine] ON [dbo].[DocumentZipFtpCommand] ([DocumentZipFtpControlID], [LineType], [LineOrder])
GO
ALTER TABLE [dbo].[DocumentZipFtpCommand] ADD CONSTRAINT [FK_DocumentZipFtpCommand_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DocumentZipFtpCommand] ADD CONSTRAINT [FK_DocumentZipFtpCommand_DocumentZipFtpControl] FOREIGN KEY ([DocumentZipFtpControlID]) REFERENCES [dbo].[DocumentZipFtpControl] ([DocumentZipFtpControlID])
GO
