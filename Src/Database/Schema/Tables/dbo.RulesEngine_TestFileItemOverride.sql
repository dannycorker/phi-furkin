CREATE TABLE [dbo].[RulesEngine_TestFileItemOverride]
(
[TestFileItemOverrideID] [int] NOT NULL IDENTITY(1, 1),
[TestFileItemID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[RuleParameterID] [int] NULL,
[ParameterValue] [varchar] (200) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[RulesEngine_TestFileItemOverride] ADD CONSTRAINT [PK_RulesEngine_TestFileItemOverride] PRIMARY KEY CLUSTERED  ([TestFileItemOverrideID])
GO
ALTER TABLE [dbo].[RulesEngine_TestFileItemOverride] ADD CONSTRAINT [FK_RulesEngine_TestFileItemOverride_RulesEngine_TestFileItem] FOREIGN KEY ([TestFileItemID]) REFERENCES [dbo].[RulesEngine_TestFileItem] ([TestFileItemID]) ON DELETE CASCADE
GO
