CREATE TABLE [dbo].[EventTypeResourceSet]
(
[EventTypeResourceSetID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[CriteriaDetailFieldID] [int] NOT NULL,
[ResourceListDetailFieldID] [int] NOT NULL,
[ResourceListMatchFieldID] [int] NOT NULL,
[SuppressSpaces] [bit] NOT NULL,
[FirstXChars] [int] NOT NULL,
[ExactMatch] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[EventTypeResourceSet] ADD CONSTRAINT [PK_EventTypeResourceSet] PRIMARY KEY CLUSTERED  ([EventTypeResourceSetID])
GO
