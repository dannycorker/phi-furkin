CREATE TABLE [dbo].[ApplicationBrandingHistory]
(
[ApplicationBrandingHistoryID] [int] NOT NULL IDENTITY(1, 1),
[ApplicationBrandingID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[FileName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[VerticalSize] [int] NULL,
[HorizontalSize] [int] NULL,
[LeadManagerAlias] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LeadAlias] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LeadDetailsTabImageFileName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[WhoChanged] [int] NULL,
[WhenChanged] [datetime] NOT NULL,
[ChangeAction] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ChangeNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ApplicationBrandingHistory] ADD CONSTRAINT [PK_ApplicationBrandingHistory] PRIMARY KEY CLUSTERED  ([ApplicationBrandingHistoryID])
GO
CREATE NONCLUSTERED INDEX [IX_ApplicationBrandingHistory_ApplicationBranding] ON [dbo].[ApplicationBrandingHistory] ([ApplicationBrandingID])
GO
CREATE NONCLUSTERED INDEX [IX_ApplicationBrandingHistory_Clients] ON [dbo].[ApplicationBrandingHistory] ([ClientID])
GO
