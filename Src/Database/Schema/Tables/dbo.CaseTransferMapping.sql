CREATE TABLE [dbo].[CaseTransferMapping]
(
[CaseTransferMappingID] [int] NOT NULL IDENTITY(1, 1),
[ClientRelationshipID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[CaseID] [int] NOT NULL,
[MatterID] [int] NOT NULL,
[NewClientID] [int] NOT NULL,
[NewCustomerID] [int] NOT NULL,
[NewLeadID] [int] NOT NULL,
[NewCaseID] [int] NOT NULL,
[NewMatterID] [int] NOT NULL,
[CaseTransferStatusID] [int] NOT NULL,
[NewLeadTypeID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[CaseTransferMapping] ADD CONSTRAINT [PK_CaseTranferMapping] PRIMARY KEY CLUSTERED  ([CaseTransferMappingID])
GO
CREATE NONCLUSTERED INDEX [IX_CaseTransferMapping_CaseClient] ON [dbo].[CaseTransferMapping] ([CaseID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_CaseTransferMapping_Client] ON [dbo].[CaseTransferMapping] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_CaseTransferMapping_LeadClient] ON [dbo].[CaseTransferMapping] ([LeadID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_CaseTransferMapping_NewCaseClient] ON [dbo].[CaseTransferMapping] ([NewCaseID], [NewClientID])
GO
CREATE NONCLUSTERED INDEX [IX_CaseTransferMapping_NewClient] ON [dbo].[CaseTransferMapping] ([NewClientID])
GO
CREATE NONCLUSTERED INDEX [IX_CaseTransferMapping_NewLeadClient] ON [dbo].[CaseTransferMapping] ([NewLeadID], [NewClientID])
GO
ALTER TABLE [dbo].[CaseTransferMapping] ADD CONSTRAINT [FK_CaseTranferMapping_Cases] FOREIGN KEY ([NewCaseID]) REFERENCES [dbo].[Cases] ([CaseID])
GO
ALTER TABLE [dbo].[CaseTransferMapping] ADD CONSTRAINT [FK_CaseTranferMapping_Cases1] FOREIGN KEY ([CaseID]) REFERENCES [dbo].[Cases] ([CaseID])
GO
ALTER TABLE [dbo].[CaseTransferMapping] ADD CONSTRAINT [FK_CaseTranferMapping_CaseTransferStatus] FOREIGN KEY ([CaseTransferStatusID]) REFERENCES [dbo].[CaseTransferStatus] ([CaseTransferStatusID])
GO
ALTER TABLE [dbo].[CaseTransferMapping] ADD CONSTRAINT [FK_CaseTranferMapping_ClientRelationship] FOREIGN KEY ([ClientRelationshipID]) REFERENCES [dbo].[ClientRelationship] ([ClientRelationshipID])
GO
ALTER TABLE [dbo].[CaseTransferMapping] ADD CONSTRAINT [FK_CaseTranferMapping_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[CaseTransferMapping] ADD CONSTRAINT [FK_CaseTranferMapping_Clients1] FOREIGN KEY ([NewClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[CaseTransferMapping] ADD CONSTRAINT [FK_CaseTranferMapping_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[CaseTransferMapping] ADD CONSTRAINT [FK_CaseTranferMapping_Customers1] FOREIGN KEY ([NewCustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[CaseTransferMapping] ADD CONSTRAINT [FK_CaseTranferMapping_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID])
GO
ALTER TABLE [dbo].[CaseTransferMapping] ADD CONSTRAINT [FK_CaseTranferMapping_Lead1] FOREIGN KEY ([NewLeadID]) REFERENCES [dbo].[Lead] ([LeadID])
GO
ALTER TABLE [dbo].[CaseTransferMapping] ADD CONSTRAINT [FK_CaseTranferMapping_Matter] FOREIGN KEY ([MatterID]) REFERENCES [dbo].[Matter] ([MatterID])
GO
ALTER TABLE [dbo].[CaseTransferMapping] ADD CONSTRAINT [FK_CaseTranferMapping_Matter1] FOREIGN KEY ([NewMatterID]) REFERENCES [dbo].[Matter] ([MatterID]) ON DELETE CASCADE
GO
