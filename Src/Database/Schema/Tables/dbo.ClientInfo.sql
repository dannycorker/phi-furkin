CREATE TABLE [dbo].[ClientInfo]
(
[ClientID] [int] NOT NULL,
[CompanyName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[CompanyAliases] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[IsTest] [bit] NOT NULL,
[IsActive] [bit] NOT NULL,
[ClosureDate] [datetime2] (0) NULL,
[IsReadyForDeletion] [bit] NOT NULL,
[IsAquarium] [bit] NOT NULL,
[IsDeleted] [bit] NOT NULL,
[Notes] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ClientInfo] ADD CONSTRAINT [PK_ClientInfo] PRIMARY KEY CLUSTERED  ([ClientID])
GO
