CREATE TABLE [dbo].[EventTypeEquation]
(
[EventTypeEquationID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[EventTypeEquation] ADD CONSTRAINT [PK_EventTypeEquation] PRIMARY KEY CLUSTERED  ([EventTypeEquationID])
GO
ALTER TABLE [dbo].[EventTypeEquation] ADD CONSTRAINT [FK_EventTypeEquation_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EventTypeEquation] ADD CONSTRAINT [FK_EventTypeEquation_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[EventTypeEquation] ADD CONSTRAINT [FK_EventTypeEquation_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
