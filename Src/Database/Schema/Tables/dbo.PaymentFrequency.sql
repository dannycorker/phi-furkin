CREATE TABLE [dbo].[PaymentFrequency]
(
[PaymentFrequencyID] [int] NOT NULL,
[FrequencyName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DefaultInstallmentLength] [int] NULL
)
GO
ALTER TABLE [dbo].[PaymentFrequency] ADD CONSTRAINT [PK_PaymentFrequency] PRIMARY KEY CLUSTERED  ([PaymentFrequencyID])
GO
