CREATE TABLE [dbo].[_MigrationScriptsHistory]
(
[ScriptNameId] [nvarchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[ExecutionDate] [datetime2] NOT NULL,
[ScriptHash] [nvarchar] (255) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[_MigrationScriptsHistory] ADD CONSTRAINT [PK___Migrati__7C02A22F6D764BC5] PRIMARY KEY CLUSTERED  ([ScriptNameId])
GO
