CREATE TABLE [dbo].[DocumentZipFtpControl]
(
[DocumentZipFtpControlID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[JobName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[JobNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[FiringOrder] [int] NOT NULL,
[SuppressAllLaterJobs] [bit] NOT NULL,
[IsConfigFileRequired] [bit] NOT NULL,
[ConfigFileName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[IsBatFileRequired] [bit] NOT NULL,
[BatFileName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Enabled] [bit] NOT NULL,
[MaxWaitTimeMS] [int] NOT NULL,
[SpecificDocumentTypesOnly] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[DocumentZipFtpControl] ADD CONSTRAINT [PK_DocumentZipFtpControl] PRIMARY KEY CLUSTERED  ([DocumentZipFtpControlID])
GO
CREATE NONCLUSTERED INDEX [IX_DocumentZipFtpControl_Batname] ON [dbo].[DocumentZipFtpControl] ([ClientID], [BatFileName])
GO
CREATE NONCLUSTERED INDEX [IX_DocumentZipFtpControl_Configname] ON [dbo].[DocumentZipFtpControl] ([ClientID], [ConfigFileName])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DocumentZipFtpControl_Jobname] ON [dbo].[DocumentZipFtpControl] ([ClientID], [JobName])
GO
ALTER TABLE [dbo].[DocumentZipFtpControl] ADD CONSTRAINT [FK_DocumentZipFtpControl_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DocumentZipFtpControl] ADD CONSTRAINT [FK_DocumentZipFtpControl_DocumentZipFtpControl] FOREIGN KEY ([DocumentZipFtpControlID]) REFERENCES [dbo].[DocumentZipFtpControl] ([DocumentZipFtpControlID])
GO
