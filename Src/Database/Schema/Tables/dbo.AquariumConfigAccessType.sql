CREATE TABLE [dbo].[AquariumConfigAccessType]
(
[AccessTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[AquariumConfigAccessType] ADD CONSTRAINT [PK_AquariumConfigAccessType] PRIMARY KEY CLUSTERED  ([AccessTypeID])
GO
