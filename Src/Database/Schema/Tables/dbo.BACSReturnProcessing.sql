CREATE TABLE [dbo].[BACSReturnProcessing]
(
[BACSReturnProcessingID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[AccountUseID] [int] NULL,
[ReasonCodeType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[FailureCode] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[FailureDescription] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[MandateStatusID] [int] NULL,
[NewAccountRequired] [bit] NULL,
[EventToApply] [int] NULL,
[AddEventAs] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[BACSReturnProcessing] ADD CONSTRAINT [PK_BACSReturnProcessing] PRIMARY KEY CLUSTERED  ([BACSReturnProcessingID])
GO
