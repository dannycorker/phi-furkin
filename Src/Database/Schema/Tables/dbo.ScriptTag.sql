CREATE TABLE [dbo].[ScriptTag]
(
[ScriptTagID] [int] NOT NULL IDENTITY(1, 1),
[Tag] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Template] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ScriptTag] ADD CONSTRAINT [PK_ScriptTag] PRIMARY KEY CLUSTERED  ([ScriptTagID])
GO
