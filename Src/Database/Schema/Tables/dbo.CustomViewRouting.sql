CREATE TABLE [dbo].[CustomViewRouting]
(
[CustomViewRouteID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DetailFieldSubTypeID] [int] NULL,
[LeadTypeID] [int] NULL,
[PageID] [int] NULL,
[FieldID] [int] NULL,
[MapsToFieldID] [int] NULL,
[MapsToClientID] [int] NULL,
[MapsToLeadTypeID] [int] NULL
)
GO
ALTER TABLE [dbo].[CustomViewRouting] ADD CONSTRAINT [PK_CustomViewRouting] PRIMARY KEY CLUSTERED  ([CustomViewRouteID])
GO
