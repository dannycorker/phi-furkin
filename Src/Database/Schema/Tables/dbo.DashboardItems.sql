CREATE TABLE [dbo].[DashboardItems]
(
[DashboardItemID] [int] NOT NULL IDENTITY(1, 1),
[DashboardGroupID] [int] NOT NULL,
[ShowAs] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[Title] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[QueryID] [int] NOT NULL,
[TransposeRequired] [int] NOT NULL,
[ChartType] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[Enabled] [bit] NULL,
[WhenCreated] [datetime] NULL,
[WhoCreated] [int] NULL,
[WhenChanged] [datetime] NULL,
[WhoChanged] [int] NULL
)
GO
ALTER TABLE [dbo].[DashboardItems] ADD CONSTRAINT [PK_DashboardItems] PRIMARY KEY CLUSTERED  ([DashboardItemID])
GO
