CREATE TABLE [dbo].[_C600_ImportCustomerData]
(
[ImportedCustomerID] [int] NOT NULL IDENTITY(1, 1),
[ImportID] [int] NOT NULL,
[BatchID] [int] NULL,
[TitleID] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[FirstName] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[MiddleName] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[EmailAddress] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[DayTimeTelephoneNumber] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[HomeTelephone] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[MobileTelephone] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Address1] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Address2] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Town] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[County] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[PostCode] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[DateOfBirth] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[CountryID] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Senddocumentsby] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[CustomerCareEmail] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[CustomerCareSMS] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ContactMarketingCommsSMS] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ContactMarketingCommsPhone] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ContactMarketingCommsEmail] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ContactMarketingCommsPost] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[CustomerID] [int] NULL,
[ImportDate] [date] NULL,
[IsInFile] [bit] NOT NULL CONSTRAINT [DF__C600_ImportCustomerData_IsInFile] DEFAULT ((0))
)
GO
ALTER TABLE [dbo].[_C600_ImportCustomerData] ADD CONSTRAINT [PK__C600_ImportCustomerData_1] PRIMARY KEY CLUSTERED  ([ImportedCustomerID])
GO
