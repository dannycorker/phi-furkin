CREATE TABLE [dbo].[CurrencyRateImport]
(
[hvalue] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[csymbol] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[cname] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[crate] [real] NULL,
[AnyID] [int] NOT NULL IDENTITY(1, 1)
)
GO
ALTER TABLE [dbo].[CurrencyRateImport] ADD CONSTRAINT [PK_CurrencyRateImport] PRIMARY KEY CLUSTERED  ([AnyID])
GO
GRANT INSERT ON  [dbo].[CurrencyRateImport] TO [AquariumNet]
GO
