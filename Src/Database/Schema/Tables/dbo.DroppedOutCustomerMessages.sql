CREATE TABLE [dbo].[DroppedOutCustomerMessages]
(
[DroppedOutCustomerMessageID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientQuestionnaireID] [int] NULL,
[EmailFromAddress] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[EmailSubject] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[Email] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[SmsMessage] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[DroppedOutCustomerMessages] ADD CONSTRAINT [PK_DroppedOutCustomerMessages] PRIMARY KEY CLUSTERED  ([DroppedOutCustomerMessageID])
GO
