CREATE TABLE [dbo].[SqlFunctionDataType]
(
[SqlFunctionDataTypeID] [int] NOT NULL,
[DataTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DataTypeDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[SqlFunctionDataType] ADD CONSTRAINT [PK_SqlFunctionDataType] PRIMARY KEY CLUSTERED  ([SqlFunctionDataTypeID])
GO
