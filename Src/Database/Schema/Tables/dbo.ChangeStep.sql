CREATE TABLE [dbo].[ChangeStep]
(
[ChangeStepID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ChangeInstanceID] [int] NOT NULL,
[StepTypeID] [int] NOT NULL,
[ChangeDocumentID] [int] NULL,
[Comment] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ExternalUrl] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ChangeStepStateID] [int] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[ChangeStep] ADD CONSTRAINT [PK__ChangeSt__2587D218071D7014] PRIMARY KEY CLUSTERED  ([ChangeStepID])
GO
ALTER TABLE [dbo].[ChangeStep] ADD CONSTRAINT [FK__ChangeSte__Chang__09F9DCBF] FOREIGN KEY ([ChangeInstanceID]) REFERENCES [dbo].[ChangeInstance] ([ChangeInstanceID])
GO
ALTER TABLE [dbo].[ChangeStep] ADD CONSTRAINT [FK__ChangeSte__Clien__0905B886] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ChangeStep] ADD CONSTRAINT [FK__ChangeSte__StepT__0BE22531] FOREIGN KEY ([StepTypeID]) REFERENCES [dbo].[ClassNode] ([ClassNodeID])
GO
ALTER TABLE [dbo].[ChangeStep] ADD CONSTRAINT [FK__ChangeSte__WhoCr__0CD6496A] FOREIGN KEY ([WhoCreated]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[ChangeStep] ADD CONSTRAINT [FK__ChangeSte__WhoMo__0DCA6DA3] FOREIGN KEY ([WhoModified]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[ChangeStep] ADD CONSTRAINT [FK_ChangeStep_ChangeStepState] FOREIGN KEY ([ChangeStepStateID]) REFERENCES [dbo].[ChangeStepState] ([ChangeStepStateID])
GO
