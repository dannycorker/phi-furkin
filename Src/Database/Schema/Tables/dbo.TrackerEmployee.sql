CREATE TABLE [dbo].[TrackerEmployee]
(
[ClientPersonnelID] [int] NOT NULL,
[On] [bit] NOT NULL CONSTRAINT [DF__TrackerEmplo__On__4B0495B8] DEFAULT ((0)),
[Pin] [varchar] (3) COLLATE Latin1_General_CI_AS NULL CONSTRAINT [DF__TrackerEmpl__Pin__4BF8B9F1] DEFAULT ((0)),
[Icon] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF__TrackerEmp__Icon__4CECDE2A] DEFAULT ('fa-user')
)
GO
ALTER TABLE [dbo].[TrackerEmployee] ADD CONSTRAINT [FK_TrackerEmployee_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
