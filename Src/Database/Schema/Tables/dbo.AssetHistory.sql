CREATE TABLE [dbo].[AssetHistory]
(
[AssetHistoryID] [int] NOT NULL IDENTITY(1, 1),
[AssetID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[AssetTypeID] [int] NOT NULL,
[AssetSubTypeID] [int] NOT NULL,
[LocationID] [int] NULL,
[AssetName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Note] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[SecureNote] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[SecureUserName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[SecurePassword] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[SecureOther] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Version] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ValidFrom] [datetime] NULL,
[ValidTo] [datetime] NULL,
[ReminderDue] [datetime] NULL,
[ReminderTimeUnitID] [int] NULL,
[ReminderTimeUnitQuantity] [int] NULL,
[ReminderNotificationGroupID] [int] NULL,
[Enabled] [bit] NOT NULL,
[Deleted] [bit] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[AssetHistory] ADD CONSTRAINT [PK__AssetHis__5681DDED0AA9DF59] PRIMARY KEY CLUSTERED  ([AssetHistoryID])
GO
ALTER TABLE [dbo].[AssetHistory] ADD CONSTRAINT [FK__AssetHist__Asset__0D864C04] FOREIGN KEY ([AssetTypeID]) REFERENCES [dbo].[AssetType] ([AssetTypeID])
GO
ALTER TABLE [dbo].[AssetHistory] ADD CONSTRAINT [FK__AssetHist__Asset__0E7A703D] FOREIGN KEY ([AssetSubTypeID]) REFERENCES [dbo].[AssetSubType] ([AssetSubTypeID])
GO
ALTER TABLE [dbo].[AssetHistory] ADD CONSTRAINT [FK__AssetHist__Clien__0C9227CB] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[AssetHistory] ADD CONSTRAINT [FK__AssetHist__Locat__0F6E9476] FOREIGN KEY ([LocationID]) REFERENCES [dbo].[Location] ([LocationID])
GO
