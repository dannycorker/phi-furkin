CREATE TABLE [dbo].[API_Ident]
(
[IdentID] [uniqueidentifier] NOT NULL CONSTRAINT [DF__API_Ident__Ident__27514527] DEFAULT (newid()),
[IdentDT] [datetime2] NOT NULL CONSTRAINT [DF__API_Ident__Ident__28456960] DEFAULT (sysdatetime()),
[AppKey] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NULL,
[SubClientID] [int] NULL,
[ClientPersonnelID] [int] NULL,
[ThirdPartySystemID] [int] NULL,
[SessionID] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[RegID] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[PIN] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[HostAddress] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Platform] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Version] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Config] [xml] NULL,
[Enabled] [bit] NOT NULL CONSTRAINT [DF__API_Ident__Enabl__29398D99] DEFAULT ((1))
)
GO
ALTER TABLE [dbo].[API_Ident] ADD CONSTRAINT [PK_API_Ident] PRIMARY KEY CLUSTERED  ([IdentID])
GO
ALTER TABLE [dbo].[API_Ident] ADD CONSTRAINT [FK_API_Ident_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
