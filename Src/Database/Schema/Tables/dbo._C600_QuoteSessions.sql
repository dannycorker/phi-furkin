CREATE TABLE [dbo].[_C600_QuoteSessions]
(
[QuoteSessionID] [int] NOT NULL IDENTITY(1, 1),
[BrandID] [int] NOT NULL,
[FirstName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Email] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[PostCode] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[QuoteStart] [datetime] NOT NULL,
[QuoteCount] [int] NOT NULL,
[BuyDate] [datetime] NULL,
[BuyXml] [xml] NULL,
[CustomerID] [int] NULL,
[LeadIDs] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[MatterIDs] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[HomeTel] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MobileTel] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[TrackingCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LastQuoteStart] [datetime] NULL,
[Invalid] [bit] NULL CONSTRAINT [DF___C600_Quo__Inval__4EAB096A] DEFAULT ((0)),
[CardTransactionID] [int] NULL
)
GO
ALTER TABLE [dbo].[_C600_QuoteSessions] ADD CONSTRAINT [PK__C327_QuoteSessions] PRIMARY KEY CLUSTERED  ([QuoteSessionID])
GO
CREATE NONCLUSTERED INDEX [IX_C433_QuoteSessions_QuoteSession] ON [dbo].[_C600_QuoteSessions] ([QuoteSessionID])
GO
