CREATE TABLE [dbo].[RPIFieldMapping]
(
[RPIFieldMappingID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[RPIFieldID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[IsEnabled] [bit] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[ColumnDetailFieldID] [int] NULL
)
GO
ALTER TABLE [dbo].[RPIFieldMapping] ADD CONSTRAINT [PK_RPIFieldMapping] PRIMARY KEY CLUSTERED  ([RPIFieldMappingID])
GO
ALTER TABLE [dbo].[RPIFieldMapping] ADD CONSTRAINT [FK__RPIFieldM__RPIFi__4862C298] FOREIGN KEY ([RPIFieldID]) REFERENCES [dbo].[RPIField] ([RPIFieldID])
GO
ALTER TABLE [dbo].[RPIFieldMapping] ADD CONSTRAINT [FK_RPIFieldMapping_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[RPIFieldMapping] ADD CONSTRAINT [FK_RPIFieldMapping_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[RPIFieldMapping] ADD CONSTRAINT [FK_RPIFieldMapping_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
