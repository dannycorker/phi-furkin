CREATE TABLE [dbo].[SmsGateway]
(
[SmsGatewayID] [int] NOT NULL,
[Gateway] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[SmsGateway] ADD CONSTRAINT [PK__SmsGatew__4DDD189F385BD598] PRIMARY KEY CLUSTERED  ([SmsGatewayID])
GO
