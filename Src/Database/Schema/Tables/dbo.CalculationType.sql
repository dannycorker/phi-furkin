CREATE TABLE [dbo].[CalculationType]
(
[CalculationTypeID] [int] NOT NULL IDENTITY(1, 1),
[CalculationTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[CalculationTypeDescription] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Enabled] [bit] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[UseLatestLiveRow] [bit] NULL,
[CloseLatestLiveRow] [bit] NULL,
[LatestLiveRowStatusID] [int] NULL,
[UpdateCurrentAndPreviousPremium] [bit] NULL
)
GO
ALTER TABLE [dbo].[CalculationType] ADD CONSTRAINT [PK_CalculationType] PRIMARY KEY CLUSTERED  ([CalculationTypeID])
GO
