CREATE TABLE [dbo].[Rights]
(
[RightID] [int] NOT NULL,
[RightName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[Rights] ADD CONSTRAINT [PK_Rights] PRIMARY KEY CLUSTERED  ([RightID])
GO
