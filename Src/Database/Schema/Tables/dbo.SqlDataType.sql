CREATE TABLE [dbo].[SqlDataType]
(
[SqlDataTypeID] [int] NOT NULL,
[SqlFunctionDataTypeID] [int] NULL,
[DataTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DataTypeDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[FixedLengthInBytes] [int] NULL,
[LengthVariesByPrecision] [bit] NULL,
[LengthVariesByData] [bit] NULL,
[StorageOverhead] [int] NULL,
[StorageMultiplier] [int] NULL
)
GO
ALTER TABLE [dbo].[SqlDataType] ADD CONSTRAINT [PK_SqlDataType] PRIMARY KEY CLUSTERED  ([SqlDataTypeID])
GO
