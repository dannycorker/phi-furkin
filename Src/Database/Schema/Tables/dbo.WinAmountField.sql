CREATE TABLE [dbo].[WinAmountField]
(
[WinAmountFieldID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[WinAmountField1] [int] NOT NULL,
[WinAmountField2] [int] NULL,
[OfferAmountField1] [int] NULL
)
GO
ALTER TABLE [dbo].[WinAmountField] ADD CONSTRAINT [PK_WinAmountField] PRIMARY KEY CLUSTERED  ([WinAmountFieldID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_WinAmountField] ON [dbo].[WinAmountField] ([ClientID], [LeadTypeID])
GO
