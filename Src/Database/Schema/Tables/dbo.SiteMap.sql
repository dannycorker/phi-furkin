CREATE TABLE [dbo].[SiteMap]
(
[SiteMapID] [int] NOT NULL IDENTITY(1, 1),
[Url] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Title] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ParentID] [int] NULL,
[Secured] [bit] NULL
)
GO
ALTER TABLE [dbo].[SiteMap] ADD CONSTRAINT [PK_SiteMap] PRIMARY KEY CLUSTERED  ([SiteMapID])
GO
