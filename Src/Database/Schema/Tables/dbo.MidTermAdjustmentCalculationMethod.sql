CREATE TABLE [dbo].[MidTermAdjustmentCalculationMethod]
(
[MidTermAdjustmentCalculationMethodID] [int] NOT NULL,
[Method] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[MidTermAdjustmentCalculationMethod] ADD CONSTRAINT [PK_MidTermAdjustmentCalculationMethod] PRIMARY KEY CLUSTERED  ([MidTermAdjustmentCalculationMethodID])
GO
