CREATE TABLE [dbo].[EventTypeSql]
(
[EventTypeSqlID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[PostUpdateSql] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[IsNativeSql] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[EventTypeSql] ADD CONSTRAINT [PK_EventTypeSql] PRIMARY KEY CLUSTERED  ([EventTypeSqlID])
GO
CREATE NONCLUSTERED INDEX [IX_EventTypeClient] ON [dbo].[EventTypeSql] ([EventTypeID], [ClientID])
GO
ALTER TABLE [dbo].[EventTypeSql] ADD CONSTRAINT [FK_EventTypeSql_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EventTypeSql] ADD CONSTRAINT [FK_EventTypeSql_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
