CREATE TABLE [dbo].[ResourceListDetailValues]
(
[ResourceListDetailValueID] [int] NOT NULL IDENTITY(1, 1),
[ResourceListID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[LeadOrMatter] [int] NOT NULL,
[DetailValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[ErrorMsg] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[EncryptedValue] [varchar] (3000) COLLATE Latin1_General_CI_AS NULL,
[ValueInt] [int] NULL,
[ValueMoney] [money] NULL,
[ValueDate] [date] NULL,
[ValueDateTime] [datetime2] (0) NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-27
-- Description:	Store DetailValue as specific datatypes
-- 2011-07-13 JWG Remove duplicates instantly at source
-- =============================================
CREATE TRIGGER [dbo].[trgiu_ResourceListDetailValues] 
   ON [dbo].[ResourceListDetailValues] 
   AFTER INSERT, UPDATE 
AS 
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END


    /* 
		If the DetailValue column has been updated then fire the trigger, 
		otherwise we are here again because the trigger has just fired,
		in which case do nothing. 
	*/
	IF UPDATE(DetailValue)
	BEGIN

		IF EXISTS (SELECT * FROM inserted i INNER JOIN deleted d on d.ResourceListDetailValueID = i.ResourceListDetailValueID WHERE i.DetailFieldID in (217319,217320,217321,217322,217323,217324,217325,217326,217327,221282,221283,221284,223029,223030,243951,243952,243953,243954,243955,243956,243957,243958,243959,285062) AND i.DetailValue <> d.DetailValue) AND 0 = (Select ISNULL(dbo.fnGetSimpleDvAsInt(299057,322),0))
		BEGIN

			DECLARE 
			@Message VARCHAR(2000),
			@RowCount INT,
			@RLDVID INT,
			@DetailValue VARCHAR(100)

			SELECT @RowCount = COUNT(*) FROM inserted

			SELECT TOP 1 @Message = 'Resource List Update Failed Please Contact the Support Department. RLDV: '+ CAST(ResourceListDetailValueID as varchar(20))+', DetailValue: ' + CAST(DetailValue as VARCHAR(300)) + ', Row Count: ' + CAST(@RowCount as VARCHAR(20))
			FROM inserted
			
			ROLLBACK

			EXEC _C00_LogIt 'DEBUG_', 'ResourceListDetailValues', 'trgiu_ResourceListDetailValues', @Message, 32488

			RAISERROR (@Message, 16, 1)			

		END
		ELSE
		BEGIN
			/* Check for duplicates (but only on insert, not update) */
			IF NOT EXISTS (SELECT * FROM deleted)
			BEGIN
			
				/* Only issue the delete if we know that any records exist, otherwise the delete operation gets attempted every time regardless */
				IF EXISTS(SELECT * FROM dbo.ResourceListDetailValues dv WITH (NOLOCK) INNER JOIN inserted i ON i.ResourceListID = dv.ResourceListID AND i.DetailFieldID = dv.DetailFieldID AND i.ResourceListDetailValueID > dv.ResourceListDetailValueID )
				BEGIN
				
					/* Remove any older duplicates for this field */
					DELETE dbo.ResourceListDetailValues 
					OUTPUT 4, deleted.ResourceListDetailValueID, deleted.ResourceListID, deleted.DetailValue, deleted.DetailFieldID 
					INTO dbo.__DeletedValue (DetailFieldSubTypeID, DetailValueID, ParentID, DetailValue, DetailFieldID)
					FROM dbo.ResourceListDetailValues dv 
					INNER JOIN inserted i ON i.ResourceListID = dv.ResourceListID AND i.DetailFieldID = dv.DetailFieldID AND i.ResourceListDetailValueID > dv.ResourceListDetailValueID 
				
				END
			END
			
			/* Populate ValueInt et al */
			UPDATE dbo.ResourceListDetailValues 
			SET ValueInt = dbo.fnValueAsIntBitsIncluded(i.DetailValue) /*CASE WHEN dbo.fnIsInt(i.detailvalue) = 1 THEN convert(int, i.DetailValue) ELSE NULL END*/, 
			ValueMoney = CASE WHEN dbo.fnIsMoney(i.DetailValue) = 1 THEN convert(money, i.DetailValue) ELSE NULL END, 
			ValueDate = CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN convert(date, i.DetailValue) ELSE NULL END, 
			ValueDateTime = CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN convert(datetime2, i.DetailValue) ELSE NULL END 
			FROM dbo.ResourceListDetailValues dv 
			INNER JOIN inserted i ON i.ResourceListDetailValueID = dv.ResourceListDetailValueID 
			
		END
		
	END
	

END

GO
ALTER TABLE [dbo].[ResourceListDetailValues] ADD CONSTRAINT [PK_ResourceDetailValues] PRIMARY KEY CLUSTERED  ([ResourceListDetailValueID])
GO
CREATE NONCLUSTERED INDEX [IX_ResourceListDetailValues_Client] ON [dbo].[ResourceListDetailValues] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_ResourceListDetailValues_DetailFieldID] ON [dbo].[ResourceListDetailValues] ([DetailFieldID])
GO
CREATE NONCLUSTERED INDEX [IX_ResourceListDetailValues_Covering2017] ON [dbo].[ResourceListDetailValues] ([DetailFieldID], [ClientID], [ResourceListID]) INCLUDE ([DetailValue], [ValueInt])
GO
CREATE NONCLUSTERED INDEX [IX_ResourceListDetailValues_Covering] ON [dbo].[ResourceListDetailValues] ([DetailFieldID], [ValueInt], [ResourceListID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_ResourceListDetailValues_RLIDAndDetailFieldID] ON [dbo].[ResourceListDetailValues] ([ResourceListID], [DetailFieldID], [ClientID])
GO
ALTER TABLE [dbo].[ResourceListDetailValues] ADD CONSTRAINT [FK_ResourceListDetailValues_ResourceList] FOREIGN KEY ([ResourceListID]) REFERENCES [dbo].[ResourceList] ([ResourceListID]) ON DELETE CASCADE
GO
