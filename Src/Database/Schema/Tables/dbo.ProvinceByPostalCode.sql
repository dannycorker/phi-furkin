CREATE TABLE [dbo].[ProvinceByPostalCode]
(
[ProvinceByPostalCodeID] [int] NOT NULL IDENTITY(1, 1),
[CountryID] [int] NOT NULL CONSTRAINT [DF__ProvinceB__Count__062CE529] DEFAULT ((39)),
[StateID] [int] NOT NULL,
[PostalCode] [varchar] (6) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ProvinceByPostalCode] ADD CONSTRAINT [PK_ProvinceByPostalCode] PRIMARY KEY CLUSTERED  ([ProvinceByPostalCodeID])
GO
ALTER TABLE [dbo].[ProvinceByPostalCode] ADD CONSTRAINT [FK_ProvinceByPostalCode_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
ALTER TABLE [dbo].[ProvinceByPostalCode] ADD CONSTRAINT [FK_ProvinceByPostalCode_State] FOREIGN KEY ([StateID]) REFERENCES [dbo].[UnitedStates] ([StateID])
GO
