CREATE TABLE [dbo].[ClientPersonnel]
(
[ClientPersonnelID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientOfficeID] [int] NULL,
[TitleID] [int] NULL,
[FirstName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[MiddleName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[JobTitle] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Password] [varchar] (65) COLLATE Latin1_General_CI_AS NULL,
[ClientPersonnelAdminGroupID] [int] NULL,
[MobileTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[HomeTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[OfficeTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[OfficeTelephoneExtension] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[EmailAddress] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[ChargeOutRate] [money] NOT NULL CONSTRAINT [DF__ClientPer__Charg__451F3D2B] DEFAULT ((0)),
[UserName] AS (([Firstname]+' ')+[Lastname]),
[Salt] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[AttemptedLogins] [int] NULL,
[AccountDisabled] [bit] NULL CONSTRAINT [DF__ClientPer__Accou__442B18F2] DEFAULT ((0)),
[ManagerID] [int] NULL,
[Initials] AS ((case  when [FirstName] IS NULL then '' when [FirstName]='' then '' else left([FirstName],(1)) end+case  when [MiddleName] IS NULL then '' when [MiddleName]='' then '' else left([MiddleName],(1)) end)+case  when [LastName] IS NULL then '' when [LastName]='' then '' else left([LastName],(1)) end),
[LanguageID] [int] NULL,
[SubClientID] [int] NULL,
[ForcePasswordChangeOn] [datetime] NULL,
[ThirdPartySystemId] [int] NOT NULL CONSTRAINT [DF__ClientPer__Third__46136164] DEFAULT ((0)),
[CustomerID] [int] NULL,
[IsAquarium] [bit] NULL,
[AllowSmsCommandProcessing] [bit] NULL,
[MemorableWord] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[MemorableWordSalt] [varchar] (65) COLLATE Latin1_General_CI_AS NULL,
[MemorableWordAttempts] [int] NULL,
[PendingActivation] [bit] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2010-10-14
-- Description:	Log password changes
-- UPDATE:		Simon Brushett
--				2011-12-07
--				Added context info check to prevent unwanted access to table.  Also added in notriggers check.
--              2021-02-03 JG & NG For T-Pet, only Aquarium staff can belong to the 5 built-in groups that belong to Client Zero.
-- =============================================
CREATE TRIGGER [dbo].[trgiud_ClientPersonnel]
   ON [dbo].[ClientPersonnel]
   AFTER INSERT, UPDATE, DELETE
AS
BEGIN
	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END

	DECLARE @InsertCount INT,
			@DeleteCount INT

	SELECT @InsertCount = COUNT(*)
	FROM  INSERTED

	SELECT @DeleteCount = COUNT(*)
	FROM  DELETED

	IF @DeleteCount > 0 AND @InsertCount = 0
	BEGIN
		ROLLBACK -- Prevent deletions from this table
	END

	/*
	Only procs that call SET CONTEXT_INFO correctly can insert, update or delete from this table to protect the sync between AQ and AQMaster.
	We also need to enforce this for insert and when the email address is being updated
	*/
	IF UPDATE([EmailAddress]) OR UPDATE([ThirdPartySystemID])
	BEGIN
		IF CAST(CONTEXT_INFO() AS VARCHAR) IS NULL OR CAST(CONTEXT_INFO() AS VARCHAR) <> 'ClientPersonnel'
		BEGIN
			ROLLBACK
		END
    END

	/*
	Only Aquarium staff can belong to the 5 built-in groups that belong to Client Zero.
	*/
	IF UPDATE([ClientPersonnelAdminGroupID])
	BEGIN
		IF EXISTS(SELECT * FROM Inserted WHERE IsAquarium = 0 AND ClientPersonnelAdminGroupID < 6)
		BEGIN
			ROLLBACK
		END
    END

    /*
		Inserts appear in the inserted table,
		Deletes appear in the deleted table,
		and Updates appear in both tables at once identically.
		These are all 'logical' Sql Server tables.
    */

    /*
		Capture changes to passwords
    */
	IF UPDATE([Password]) OR UPDATE ([Salt])
	BEGIN
		INSERT INTO dbo.ClientPersonnelHistory
		(
			ClientPersonnelID,
			PasswordChanged,
			WhoChanged,
			WhenChanged,
			ChangeAction,
			ChangeNotes
		)
		SELECT
		COALESCE(i.ClientPersonnelID, d.ClientPersonnelID),
		1,
		NULL, --i.WhoChanged is not available yet
		dbo.fn_GetDate_Local(),
		CASE WHEN d.ClientPersonnelID IS NULL THEN 'Insert' WHEN i.ClientPersonnelID IS NULL THEN 'Delete' ELSE 'Update' END,
		dbo.fnGetUserInfo()
		FROM inserted i
		FULL OUTER JOIN deleted d ON i.ClientPersonnelID = d.ClientPersonnelID

	END

END
GO
ALTER TABLE [dbo].[ClientPersonnel] ADD CONSTRAINT [PK_ClientPersonnel] PRIMARY KEY CLUSTERED  ([ClientPersonnelID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientPersonnel_ClientUserName2017] ON [dbo].[ClientPersonnel] ([ClientID], [UserName])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ClientPersonnel_EmailAddress_ThirdPartySystemId] ON [dbo].[ClientPersonnel] ([EmailAddress], [ThirdPartySystemId])
GO
ALTER TABLE [dbo].[ClientPersonnel] ADD CONSTRAINT [FK_ClientPersonnel_ClientPersonnel1] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[ClientPersonnel] ADD CONSTRAINT [FK_ClientPersonnel_ClientPersonnelAdminGroups] FOREIGN KEY ([ClientPersonnelAdminGroupID]) REFERENCES [dbo].[ClientPersonnelAdminGroups] ([ClientPersonnelAdminGroupID])
GO
ALTER TABLE [dbo].[ClientPersonnel] ADD CONSTRAINT [FK_ClientPersonnel_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ClientPersonnel] ADD CONSTRAINT [FK_ClientPersonnel_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[ClientPersonnel] ADD CONSTRAINT [FK_ClientPersonnel_Manager] FOREIGN KEY ([ManagerID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[ClientPersonnel] ADD CONSTRAINT [FK_ClientPersonnel_ThirdPartySystem] FOREIGN KEY ([ThirdPartySystemId]) REFERENCES [dbo].[ThirdPartySystem] ([ThirdPartySystemId])
GO
GRANT CONTROL ON  [dbo].[ClientPersonnel] TO [sp_executeall]
GO
