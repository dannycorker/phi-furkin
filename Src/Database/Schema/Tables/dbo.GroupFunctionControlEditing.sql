CREATE TABLE [dbo].[GroupFunctionControlEditing]
(
[GroupFunctionControlEditingID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelAdminGroupID] [int] NOT NULL,
[ModuleID] [int] NOT NULL,
[FunctionTypeID] [int] NOT NULL,
[HasDescendants] [bit] NOT NULL,
[RightID] [int] NOT NULL,
[LeadTypeID] [int] NULL
)
GO
ALTER TABLE [dbo].[GroupFunctionControlEditing] ADD CONSTRAINT [PK_GroupFunctionControlEditing] PRIMARY KEY CLUSTERED  ([GroupFunctionControlEditingID])
GO
ALTER TABLE [dbo].[GroupFunctionControlEditing] ADD CONSTRAINT [FK_GroupFunctionControlEditing_ClientPersonnelAdminGroups] FOREIGN KEY ([ClientPersonnelAdminGroupID]) REFERENCES [dbo].[ClientPersonnelAdminGroups] ([ClientPersonnelAdminGroupID])
GO
ALTER TABLE [dbo].[GroupFunctionControlEditing] ADD CONSTRAINT [FK_GroupFunctionControlEditing_FunctionType] FOREIGN KEY ([FunctionTypeID]) REFERENCES [dbo].[FunctionType] ([FunctionTypeID])
GO
ALTER TABLE [dbo].[GroupFunctionControlEditing] ADD CONSTRAINT [FK_GroupFunctionControlEditing_Module] FOREIGN KEY ([ModuleID]) REFERENCES [dbo].[Module] ([ModuleID])
GO
ALTER TABLE [dbo].[GroupFunctionControlEditing] ADD CONSTRAINT [FK_GroupFunctionControlEditing_Rights] FOREIGN KEY ([RightID]) REFERENCES [dbo].[Rights] ([RightID])
GO
