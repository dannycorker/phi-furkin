CREATE TABLE [dbo].[ScriptRuleGroup]
(
[ScriptRuleGroupID] [int] NOT NULL IDENTITY(1, 1),
[ScriptRuleID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[ComparatorID] [int] NOT NULL,
[Value] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[OperatorID] [int] NULL,
[GroupID] [int] NOT NULL,
[GroupOperatorID] [int] NULL
)
GO
ALTER TABLE [dbo].[ScriptRuleGroup] ADD CONSTRAINT [PK_ScriptRuleGroup] PRIMARY KEY CLUSTERED  ([ScriptRuleGroupID])
GO
