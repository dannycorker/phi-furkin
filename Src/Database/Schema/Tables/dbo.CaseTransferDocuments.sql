CREATE TABLE [dbo].[CaseTransferDocuments]
(
[CaseTransferDocID] [int] NOT NULL IDENTITY(1, 1),
[ClientRelationshipID] [int] NOT NULL,
[FromEventTypeID] [int] NOT NULL,
[ToEventTypeID] [int] NOT NULL,
[ApplyEventAs] [int] NOT NULL,
[LastOnly] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[CaseTransferDocuments] ADD CONSTRAINT [PK_CaseTransferDocuments] PRIMARY KEY CLUSTERED  ([CaseTransferDocID])
GO
