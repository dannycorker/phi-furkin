CREATE TABLE [dbo].[StateByZip]
(
[StateByZipID] [int] NOT NULL IDENTITY(1, 1),
[CountryID] [int] NOT NULL CONSTRAINT [DF__StateByZi__Count__4B412985] DEFAULT ((233)),
[StateID] [int] NOT NULL,
[Zip] [varchar] (5) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[StateByZip] ADD CONSTRAINT [PK_StateByZip] PRIMARY KEY CLUSTERED  ([StateByZipID])
GO
ALTER TABLE [dbo].[StateByZip] ADD CONSTRAINT [FK_StateByZip_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
ALTER TABLE [dbo].[StateByZip] ADD CONSTRAINT [FK_StateByZip_State] FOREIGN KEY ([StateID]) REFERENCES [dbo].[UnitedStates] ([StateID])
GO
