CREATE TABLE [dbo].[DocumentMRTarget]
(
[DocumentMRTargetID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DocumentTypeID] [int] NOT NULL,
[Target] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[DetailFieldID] [int] NULL,
[ColumnField] [int] NULL,
[TemplateTypeID] [int] NOT NULL,
[DetailFieldAlias] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[ColumnFieldAlias] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[ObjectName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[PropertyName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Notes] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[DocumentTypeVersionID] [int] NULL
)
GO
ALTER TABLE [dbo].[DocumentMRTarget] ADD CONSTRAINT [PK_DocumentMRTarget] PRIMARY KEY CLUSTERED  ([DocumentMRTargetID])
GO
ALTER TABLE [dbo].[DocumentMRTarget] ADD CONSTRAINT [FK_DocumentMRTarget_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DocumentMRTarget] ADD CONSTRAINT [FK_DocumentMRTarget_DocumentType] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentType] ([DocumentTypeID])
GO
ALTER TABLE [dbo].[DocumentMRTarget] ADD CONSTRAINT [FK_DocumentMRTarget_DocumentTypeVersion] FOREIGN KEY ([DocumentTypeVersionID]) REFERENCES [dbo].[DocumentTypeVersion] ([DocumentTypeVersionID])
GO
ALTER TABLE [dbo].[DocumentMRTarget] ADD CONSTRAINT [FK_DocumentMRTarget_TemplateType] FOREIGN KEY ([TemplateTypeID]) REFERENCES [dbo].[TemplateType] ([TemplateTypeID])
GO
