CREATE TABLE [dbo].[AutoAdjudicationFieldType]
(
[AutoAdjudicationFieldTypeID] [int] NOT NULL IDENTITY(1, 1),
[AutoAdjudicationFieldTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[AutoAdjudicationFieldType] ADD CONSTRAINT [PK_AutoAdjudicationField] PRIMARY KEY CLUSTERED  ([AutoAdjudicationFieldTypeID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_AutoAdjudicationFieldType] ON [dbo].[AutoAdjudicationFieldType] ([AutoAdjudicationFieldTypeName])
GO
