CREATE TABLE [dbo].[ChartSetting]
(
[ChartSettingID] [int] NOT NULL IDENTITY(1, 1),
[ChartID] [int] NOT NULL,
[SettingName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[SettingValue] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ChartSetting] ADD CONSTRAINT [PK_ChartSetting] PRIMARY KEY CLUSTERED  ([ChartSettingID])
GO
ALTER TABLE [dbo].[ChartSetting] ADD CONSTRAINT [FK_ChartSetting_Chart] FOREIGN KEY ([ChartID]) REFERENCES [dbo].[Chart] ([ChartID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ChartSetting] ADD CONSTRAINT [FK_ChartSetting_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
