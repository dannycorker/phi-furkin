CREATE TABLE [dbo].[MatterPageChoice]
(
[MatterPageChoiceID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[MatterID] [int] NOT NULL,
[DetailFieldPageID] [int] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[MatterPageChoice] ADD CONSTRAINT [PK_MatterPageChoice] PRIMARY KEY CLUSTERED  ([MatterPageChoiceID])
GO
CREATE NONCLUSTERED INDEX [IX_MatterPageChoice_MatterPage] ON [dbo].[MatterPageChoice] ([MatterID], [DetailFieldPageID]) INCLUDE ([ClientID])
GO
ALTER TABLE [dbo].[MatterPageChoice] ADD CONSTRAINT [FK_MatterPageChoice_Matter] FOREIGN KEY ([MatterID]) REFERENCES [dbo].[Matter] ([MatterID])
GO
