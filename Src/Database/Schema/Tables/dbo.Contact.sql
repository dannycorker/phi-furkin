CREATE TABLE [dbo].[Contact]
(
[ContactID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[TitleID] [int] NOT NULL,
[Firstname] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Middlename] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Lastname] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Fullname] AS (([Firstname]+' ')+[Lastname]),
[EmailAddressWork] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[EmailAddressOther] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[DirectDial] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[MobilePhoneWork] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[MobilePhoneOther] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Address1] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Address2] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Town] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[County] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Postcode] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Country] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[OfficeID] [int] NULL,
[DepartmentID] [int] NULL,
[JobTitle] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Notes] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[CountryID] [int] NULL CONSTRAINT [DF__Contact__Country__4AD81681] DEFAULT ((232)),
[LanguageID] [int] NULL,
[WhenModified] [datetime] NULL,
[WhoModified] [int] NULL,
[Longitude] [numeric] (25, 18) NULL,
[Latitude] [numeric] (25, 18) NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-12
-- Description:	Capture changes and store in History table
-- =============================================
CREATE TRIGGER [dbo].[trgiud_Contact]
   ON  [dbo].[Contact]
   AFTER INSERT,UPDATE, DELETE
AS
BEGIN
	SET NOCOUNT ON;

	-- Allow special user 'notriggers' to bypass trigger code
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END

	/*
		Inserts and updates both appear in the inserted table, which is a 'logical' Sql Server table.
    */
	INSERT INTO dbo.ContactHistory(
		ContactID, 
		ClientID, 
		CustomerID, 
		TitleID, 
		Firstname, 
		Middlename, 
		Lastname, 
		EmailAddressWork, 
		EmailAddressOther, 
		DirectDial, 
		MobilePhoneWork, 
		MobilePhoneOther, 
		Address1, 
		Address2, 
		Town, 
		County, 
		Postcode, 
		Country, 
		OfficeID, 
		DepartmentID, 
		JobTitle, 
		Notes, 
		CountryID, 
		LanguageID, 
		WhoChanged, 
		WhenChanged, 
		ChangeAction, 
		ChangeNotes
	)
	SELECT
		CASE WHEN i.ContactID IS NULL THEN d.ContactID ELSE i.ContactID END, 
		CASE WHEN i.ContactID IS NULL THEN d.ClientID ELSE i.ClientID END, 
		CASE WHEN i.ContactID IS NULL THEN d.CustomerID ELSE i.CustomerID END, 
		CASE WHEN i.ContactID IS NULL THEN d.TitleID ELSE i.TitleID END, 
		CASE WHEN i.ContactID IS NULL THEN d.Firstname ELSE i.Firstname END, 
		CASE WHEN i.ContactID IS NULL THEN d.Middlename ELSE i.Middlename END, 
		CASE WHEN i.ContactID IS NULL THEN d.Lastname ELSE i.Lastname END, 
		CASE WHEN i.ContactID IS NULL THEN d.EmailAddressWork ELSE i.EmailAddressWork END, 
		CASE WHEN i.ContactID IS NULL THEN d.EmailAddressOther ELSE i.EmailAddressOther END, 
		CASE WHEN i.ContactID IS NULL THEN d.DirectDial ELSE i.DirectDial END, 
		CASE WHEN i.ContactID IS NULL THEN d.MobilePhoneWork ELSE i.MobilePhoneWork END, 
		CASE WHEN i.ContactID IS NULL THEN d.MobilePhoneOther ELSE i.MobilePhoneOther END, 
		CASE WHEN i.ContactID IS NULL THEN d.Address1 ELSE i.Address1 END, 
		CASE WHEN i.ContactID IS NULL THEN d.Address2 ELSE i.Address2 END, 
		CASE WHEN i.ContactID IS NULL THEN d.Town ELSE i.Town END, 
		CASE WHEN i.ContactID IS NULL THEN d.County ELSE i.County END, 
		CASE WHEN i.ContactID IS NULL THEN d.Postcode ELSE i.Postcode END, 
		CASE WHEN i.ContactID IS NULL THEN d.Country ELSE i.Country END, 
		CASE WHEN i.ContactID IS NULL THEN d.OfficeID ELSE i.OfficeID END, 
		CASE WHEN i.ContactID IS NULL THEN d.DepartmentID ELSE i.DepartmentID END, 
		CASE WHEN i.ContactID IS NULL THEN d.JobTitle ELSE i.JobTitle END, 
		CASE WHEN i.ContactID IS NULL THEN d.Notes ELSE i.Notes END, 
		CASE WHEN i.ContactID IS NULL THEN d.CountryID ELSE i.CountryID END, 
		CASE WHEN i.ContactID IS NULL THEN d.LanguageID ELSE i.LanguageID END, 
		NULL, 
		dbo.fn_GetDate_Local(), 
		CASE WHEN d.ContactID IS NULL THEN 'Insert' WHEN i.ContactID IS NULL THEN 'Delete' ELSE 'Update' END,
		dbo.fnGetUserInfo() /* Record which app/class/method was used to make the change */
	FROM inserted i
	FULL OUTER JOIN deleted d ON i.ContactID = d.ContactID

END
GO
ALTER TABLE [dbo].[Contact] ADD CONSTRAINT [PK_Contacts] PRIMARY KEY CLUSTERED  ([ContactID])
GO
CREATE NONCLUSTERED INDEX [IX_Contact_Client] ON [dbo].[Contact] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_Contact_Customer] ON [dbo].[Contact] ([CustomerID])
GO
ALTER TABLE [dbo].[Contact] ADD CONSTRAINT [FK_Contact_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
ALTER TABLE [dbo].[Contact] ADD CONSTRAINT [FK_Contact_CustomerOffice] FOREIGN KEY ([OfficeID]) REFERENCES [dbo].[CustomerOffice] ([OfficeID])
GO
ALTER TABLE [dbo].[Contact] ADD CONSTRAINT [FK_Contacts_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[Contact] ADD CONSTRAINT [FK_Contacts_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[Contact] ADD CONSTRAINT [FK_Contacts_Titles] FOREIGN KEY ([TitleID]) REFERENCES [dbo].[Titles] ([TitleID])
GO
