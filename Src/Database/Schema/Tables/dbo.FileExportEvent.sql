CREATE TABLE [dbo].[FileExportEvent]
(
[FileExportEventID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[LeadEventID] [int] NOT NULL,
[EventTypeID] [int] NULL,
[AutomatedTaskID] [int] NULL,
[Filename] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[FileID] [int] NULL,
[WhenFollowedup] [datetime] NULL,
[NextLeadEventID] [int] NULL,
[NextEventTypeID] [int] NULL,
[NextAutomatedTaskID] [int] NULL,
[SubmissionNotes] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[ResubmissionNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[FileExportEvent] ADD CONSTRAINT [PK_FileExportEvent] PRIMARY KEY CLUSTERED  ([FileExportEventID])
GO
CREATE NONCLUSTERED INDEX [IX_FileExportEvent_Client] ON [dbo].[FileExportEvent] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_FileExportEvent_LeadEvent] ON [dbo].[FileExportEvent] ([LeadEventID])
GO
ALTER TABLE [dbo].[FileExportEvent] ADD CONSTRAINT [FK_FileExportEvent_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
