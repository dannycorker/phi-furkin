CREATE TABLE [dbo].[RulesEngine_SmsFieldMap]
(
[SmsFieldMapID] [int] NOT NULL IDENTITY(1, 1),
[RuleParameterID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[RulesEngine_SmsFieldMap] ADD CONSTRAINT [PK_RulesEngine_SmsFieldMap] PRIMARY KEY CLUSTERED  ([SmsFieldMapID])
GO
ALTER TABLE [dbo].[RulesEngine_SmsFieldMap] ADD CONSTRAINT [FK_RulesEngine_SmsFieldMap_RulesEngine_RuleParameters] FOREIGN KEY ([RuleParameterID]) REFERENCES [dbo].[RulesEngine_RuleParameters] ([RuleParameterID])
GO
