CREATE TABLE [dbo].[NoteTypeSQL]
(
[NoteTypeSQLID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[NoteTypeID] [int] NOT NULL,
[PostUpdateSQL] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoChanged] [int] NOT NULL,
[WhenChanged] [datetime] NOT NULL,
[InsertExisting] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[NoteTypeSQL] ADD CONSTRAINT [PK_NoteTypeSQL] PRIMARY KEY CLUSTERED  ([NoteTypeSQLID])
GO
