CREATE TABLE [dbo].[__LoginExclusions]
(
[LogonExclusionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientPersonnelID] [int] NOT NULL,
[Description] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[__LoginExclusions] ADD CONSTRAINT [PK___LogonExclusions] PRIMARY KEY CLUSTERED  ([LogonExclusionID])
GO
ALTER TABLE [dbo].[__LoginExclusions] ADD CONSTRAINT [FK___LoginExclusions_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID]) ON DELETE CASCADE
GO
