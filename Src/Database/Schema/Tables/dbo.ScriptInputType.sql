CREATE TABLE [dbo].[ScriptInputType]
(
[ScriptInputTypeID] [int] NOT NULL IDENTITY(1, 1),
[InputType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Template] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ScriptInputType] ADD CONSTRAINT [PK_ScriptInputType] PRIMARY KEY CLUSTERED  ([ScriptInputTypeID])
GO
