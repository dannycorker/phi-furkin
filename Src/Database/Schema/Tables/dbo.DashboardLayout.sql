CREATE TABLE [dbo].[DashboardLayout]
(
[DashboardLayoutID] [int] NOT NULL IDENTITY(1, 1),
[DashboardLayoutName] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[TopLeftDimensions] [char] (3) COLLATE Latin1_General_CI_AS NULL,
[TopMiddleDimensions] [char] (3) COLLATE Latin1_General_CI_AS NULL,
[TopRightDimensions] [char] (3) COLLATE Latin1_General_CI_AS NULL,
[BottomLeftDimensions] [char] (3) COLLATE Latin1_General_CI_AS NULL,
[BottomMiddleDimensions] [char] (3) COLLATE Latin1_General_CI_AS NULL,
[BottomRightDimensions] [char] (3) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[DashboardLayout] ADD CONSTRAINT [PK_Dashboard] PRIMARY KEY CLUSTERED  ([DashboardLayoutID])
GO
