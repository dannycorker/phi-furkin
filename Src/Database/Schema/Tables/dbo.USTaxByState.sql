CREATE TABLE [dbo].[USTaxByState]
(
[USTaxByStateID] [int] NOT NULL IDENTITY(1, 1),
[TaxName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[CountryID] [int] NOT NULL CONSTRAINT [DF__USTaxBySt__Count__2679B92A] DEFAULT ((233)),
[StateID] [int] NOT NULL,
[TaxPercent] [money] NOT NULL,
[From] [date] NULL,
[To] [date] NULL,
[TaxProportion] [money] NULL,
[GrossMultiplier] [money] NULL,
[UsesUSTaxCityandCounty] [bit] NULL,
[IsMunicipal] [bit] NULL
)
GO
ALTER TABLE [dbo].[USTaxByState] ADD CONSTRAINT [PK_USTaxByState] PRIMARY KEY CLUSTERED  ([USTaxByStateID])
GO
ALTER TABLE [dbo].[USTaxByState] ADD CONSTRAINT [FK_USTaxByState_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
ALTER TABLE [dbo].[USTaxByState] ADD CONSTRAINT [FK_USTaxByState_State] FOREIGN KEY ([StateID]) REFERENCES [dbo].[UnitedStates] ([StateID])
GO
