CREATE TABLE [dbo].[RPIClientCredential]
(
[RPIClientCredentialID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[UserName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Password] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[AsUser] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[RPIClientCredential] ADD CONSTRAINT [PK_RPIClientCredential] PRIMARY KEY CLUSTERED  ([RPIClientCredentialID])
GO
ALTER TABLE [dbo].[RPIClientCredential] ADD CONSTRAINT [FK_RPIClientCredential_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
