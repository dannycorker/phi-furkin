CREATE TABLE [dbo].[LeadEventThreadCompletion]
(
[LeadEventThreadCompletionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[CaseID] [int] NOT NULL,
[FromLeadEventID] [int] NOT NULL,
[FromEventTypeID] [int] NOT NULL,
[ThreadNumberRequired] [int] NOT NULL,
[ToLeadEventID] [int] NULL,
[ToEventTypeID] [int] NULL
)
GO
ALTER TABLE [dbo].[LeadEventThreadCompletion] ADD CONSTRAINT [PK_LeadEventThreadCompletion] PRIMARY KEY CLUSTERED  ([LeadEventThreadCompletionID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEventThreadCompletionCaseAndFromEventType] ON [dbo].[LeadEventThreadCompletion] ([CaseID], [FromEventTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEventThreadCompletion_Covering] ON [dbo].[LeadEventThreadCompletion] ([CaseID], [FromLeadEventID], [FromEventTypeID], [ThreadNumberRequired], [ToLeadEventID], [ToEventTypeID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEventThreadCompletion_CaseFromLeadEventToLeadEvent] ON [dbo].[LeadEventThreadCompletion] ([CaseID], [FromLeadEventID], [ToLeadEventID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEventThreadCompletion_ClientCaseFromLEToLE] ON [dbo].[LeadEventThreadCompletion] ([ClientID], [CaseID], [FromLeadEventID], [ToLeadEventID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEventThreadCompletionFromLeadEventID] ON [dbo].[LeadEventThreadCompletion] ([FromLeadEventID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEventThreadCompletionToLeadEventID] ON [dbo].[LeadEventThreadCompletion] ([ToLeadEventID])
GO
ALTER TABLE [dbo].[LeadEventThreadCompletion] ADD CONSTRAINT [FK_LeadEventThreadCompletion_Cases] FOREIGN KEY ([CaseID]) REFERENCES [dbo].[Cases] ([CaseID])
GO
ALTER TABLE [dbo].[LeadEventThreadCompletion] ADD CONSTRAINT [FK_LeadEventThreadCompletion_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[LeadEventThreadCompletion] ADD CONSTRAINT [FK_LeadEventThreadCompletion_EventType] FOREIGN KEY ([FromEventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[LeadEventThreadCompletion] ADD CONSTRAINT [FK_LeadEventThreadCompletion_EventType1] FOREIGN KEY ([ToEventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[LeadEventThreadCompletion] ADD CONSTRAINT [FK_LeadEventThreadCompletion_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[LeadEventThreadCompletion] ADD CONSTRAINT [FK_LeadEventThreadCompletion_LeadEvent] FOREIGN KEY ([FromLeadEventID]) REFERENCES [dbo].[LeadEvent] ([LeadEventID])
GO
ALTER TABLE [dbo].[LeadEventThreadCompletion] ADD CONSTRAINT [FK_LeadEventThreadCompletion_LeadEvent1] FOREIGN KEY ([ToLeadEventID]) REFERENCES [dbo].[LeadEvent] ([LeadEventID])
GO
