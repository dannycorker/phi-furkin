CREATE TABLE [dbo].[MigrationProc]
(
[MigrationProcID] [int] NOT NULL IDENTITY(1, 1),
[ProcName] [varchar] (1024) COLLATE Latin1_General_CI_AS NOT NULL,
[DropText] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[ClientID] [int] NOT NULL,
[IsComplete] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[MigrationProc] ADD CONSTRAINT [PK_MigrationProc] PRIMARY KEY CLUSTERED  ([MigrationProcID])
GO
