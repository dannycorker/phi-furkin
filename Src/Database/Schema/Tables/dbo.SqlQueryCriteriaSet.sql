CREATE TABLE [dbo].[SqlQueryCriteriaSet]
(
[SqlQueryCriteriaSetID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SqlQueryID] [int] NOT NULL,
[SetName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[SourceID] [int] NULL
)
GO
ALTER TABLE [dbo].[SqlQueryCriteriaSet] ADD CONSTRAINT [PK_SqlQueryCriteriaSet] PRIMARY KEY CLUSTERED  ([SqlQueryCriteriaSetID])
GO
CREATE NONCLUSTERED INDEX [IX_SqlQueryCriteriaSet_Query] ON [dbo].[SqlQueryCriteriaSet] ([SqlQueryID])
GO
ALTER TABLE [dbo].[SqlQueryCriteriaSet] ADD CONSTRAINT [FK_SqlQueryCriteriaSet_SqlQuery] FOREIGN KEY ([SqlQueryID]) REFERENCES [dbo].[SqlQuery] ([QueryID]) ON DELETE CASCADE
GO
