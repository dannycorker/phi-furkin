CREATE TABLE [dbo].[LeadDocumentFSConversionList]
(
[ClientID] [int] NOT NULL,
[EstDocCount] [int] NOT NULL,
[IsDone] [bit] NOT NULL CONSTRAINT [DF__LeadDocum__IsDon__6E2152BE] DEFAULT ((0))
)
GO
ALTER TABLE [dbo].[LeadDocumentFSConversionList] ADD CONSTRAINT [PK_LeadDocumentFSConversionList] PRIMARY KEY CLUSTERED  ([ClientID])
GO
