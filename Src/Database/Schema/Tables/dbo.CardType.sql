CREATE TABLE [dbo].[CardType]
(
[CardTypeID] [int] NOT NULL IDENTITY(1, 1),
[CardTypeName] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[CardType] ADD CONSTRAINT [PK_CardType] PRIMARY KEY CLUSTERED  ([CardTypeID])
GO
