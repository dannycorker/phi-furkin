CREATE TABLE [dbo].[PurchasedProduct]
(
[PurchasedProductID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[AccountID] [int] NOT NULL,
[PaymentFrequencyID] [int] NOT NULL,
[NumberOfInstallments] [int] NULL,
[ProductPurchasedOnDate] [datetime] NULL,
[ValidFrom] [datetime] NULL,
[ValidTo] [datetime] NULL,
[PreferredPaymentDay] [int] NULL,
[FirstPaymentDate] [datetime] NULL,
[RemainderUpFront] [bit] NULL,
[ProductName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ProductDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ProductCostNet] [numeric] (18, 2) NULL,
[ProductCostVAT] [numeric] (18, 2) NULL,
[ProductCostGross] [numeric] (18, 2) NULL,
[ProductCostCalculatedWithRuleSetID] [int] NULL,
[ProductCostCalculatedOn] [datetime] NULL,
[ProductCostBreakdown] [xml] NULL,
[ObjectID] [int] NULL,
[ObjectTypeID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[PaymentScheduleSuccessfullyCreated] [bit] NULL,
[PaymentScheduleCreatedOn] [datetime] NULL,
[PaymentScheduleFailedDataLoaderLogID] [int] NULL,
[PremiumCalculationDetailID] [int] NULL,
[ProductAdditionalFee] [numeric] (18, 2) NULL,
[ClientAccountID] [int] NULL,
[LegacyRef] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CurrencyID] [int] NULL
)
GO
ALTER TABLE [dbo].[PurchasedProduct] ADD CONSTRAINT [PK_PurchasedProducts] PRIMARY KEY CLUSTERED  ([PurchasedProductID])
GO
CREATE NONCLUSTERED INDEX [IX_PurchasedProduct_Customer] ON [dbo].[PurchasedProduct] ([CustomerID], [ClientID])
GO
ALTER TABLE [dbo].[PurchasedProduct] ADD CONSTRAINT [FK_PurchasedProduct_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
GO
ALTER TABLE [dbo].[PurchasedProduct] ADD CONSTRAINT [FK_PurchasedProduct_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[PurchasedProduct] ADD CONSTRAINT [FK_PurchasedProduct_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[PurchasedProduct] ADD CONSTRAINT [FK_PurchasedProduct_PaymentFrequency] FOREIGN KEY ([PaymentFrequencyID]) REFERENCES [dbo].[PaymentFrequency] ([PaymentFrequencyID])
GO
