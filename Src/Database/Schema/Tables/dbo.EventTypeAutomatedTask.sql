CREATE TABLE [dbo].[EventTypeAutomatedTask]
(
[EventTypeAutomatedTaskID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[AutomatedTaskID] [int] NOT NULL,
[TimeDelayInSeconds] [int] NOT NULL,
[AddTimeDelayIfAboutToRun] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[EventTypeAutomatedTask] ADD CONSTRAINT [PK_EventTypeAutomatedTask] PRIMARY KEY CLUSTERED  ([EventTypeAutomatedTaskID])
GO
ALTER TABLE [dbo].[EventTypeAutomatedTask] ADD CONSTRAINT [FK_EventTypeAutomatedTask_AutomatedTask] FOREIGN KEY ([AutomatedTaskID]) REFERENCES [dbo].[AutomatedTask] ([TaskID])
GO
ALTER TABLE [dbo].[EventTypeAutomatedTask] ADD CONSTRAINT [FK_EventTypeAutomatedTask_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EventTypeAutomatedTask] ADD CONSTRAINT [FK_EventTypeAutomatedTask_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
