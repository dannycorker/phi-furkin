CREATE TABLE [dbo].[SMSOutgoingPhoneNumber]
(
[SMSOutgoingPhoneNumberID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SMSQuestionnaireID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[OutgoingPhoneNumber] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[SMSSurveyStartEventTypeID] [int] NULL,
[MMSLetterInEvent] [int] NULL,
[SmsGatewayID] [int] NULL,
[HasIntroductionText] [bit] NULL,
[IntroductionTextFrom] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[IntroductionText] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[DelayInSeconds] [int] NULL,
[IntroductionTextEventTypeID] [int] NULL,
[IntroductionTextDocumentTypeID] [int] NULL,
[NoCLIRejectText] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SMSOutgoingPhoneNumber] ADD CONSTRAINT [PK_SMSOutgoingPhoneNumber] PRIMARY KEY CLUSTERED  ([SMSOutgoingPhoneNumberID])
GO
ALTER TABLE [dbo].[SMSOutgoingPhoneNumber] ADD CONSTRAINT [FK_SMSOutgoingPhoneNumber_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SMSOutgoingPhoneNumber] ADD CONSTRAINT [FK_SMSOutgoingPhoneNumber_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
ALTER TABLE [dbo].[SMSOutgoingPhoneNumber] ADD CONSTRAINT [FK_SMSOutgoingPhoneNumber_SMSQuestionnaire] FOREIGN KEY ([SMSQuestionnaireID]) REFERENCES [dbo].[SMSQuestionnaire] ([SMSQuestionnaireID])
GO
