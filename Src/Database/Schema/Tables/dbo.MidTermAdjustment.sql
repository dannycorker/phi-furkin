CREATE TABLE [dbo].[MidTermAdjustment]
(
[MidTermAdjustmentID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NULL,
[CustomerID] [int] NULL,
[PurchasedProductID] [int] NULL,
[Description] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[PremiumNet] [numeric] (18, 2) NULL,
[PremiumVAT] [numeric] (18, 2) NULL,
[PremiumAmount] [numeric] (18, 2) NULL,
[AdjustmentDate] [datetime] NULL,
[NotificationDate] [datetime] NULL,
[OverrideNextPayment] [bit] NULL,
[RuleSetID] [int] NULL,
[CostBreakDown] [xml] NULL,
[RegularPaymentWait] [int] NULL,
[OneOffAdjustmentWait] [int] NULL,
[MidTermAdjustmentCalculationMethodID] [int] NULL,
[ObjectTypeID] [int] NULL,
[ObjectID] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoCreated] [int] NULL
)
GO
ALTER TABLE [dbo].[MidTermAdjustment] ADD CONSTRAINT [PK_MidTermAdjustment] PRIMARY KEY CLUSTERED  ([MidTermAdjustmentID])
GO
