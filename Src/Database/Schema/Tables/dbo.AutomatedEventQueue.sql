CREATE TABLE [dbo].[AutomatedEventQueue]
(
[AutomatedEventQueueID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NULL,
[LeadID] [int] NOT NULL,
[CaseID] [int] NOT NULL,
[FromLeadEventID] [int] NOT NULL,
[FromEventTypeID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL,
[AutomatedEventTypeID] [int] NOT NULL,
[RunAsUserID] [int] NOT NULL,
[ThreadToFollowUp] [int] NOT NULL,
[InternalPriority] [tinyint] NOT NULL,
[SchedulerID] [int] NULL,
[ServerName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LockDateTime] [datetime] NULL,
[BumpCount] [int] NOT NULL,
[ErrorCount] [int] NOT NULL,
[ErrorMessage] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ErrorDateTime] [datetime] NULL,
[SuccessDateTime] [datetime] NULL,
[EarliestRunDateTime] [datetime] NULL,
[CommentsToApply] [varchar] (1500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[AutomatedEventQueue] ADD CONSTRAINT [PK_AutomatedEventQueue] PRIMARY KEY CLUSTERED  ([AutomatedEventQueueID])
GO
CREATE NONCLUSTERED INDEX [IX_AutomatedEventQueue_WhenCreated] ON [dbo].[AutomatedEventQueue] ([WhenCreated])
GO
ALTER TABLE [dbo].[AutomatedEventQueue] ADD CONSTRAINT [FK_AutomatedEventQueue_AutomatedEventType] FOREIGN KEY ([AutomatedEventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[AutomatedEventQueue] ADD CONSTRAINT [FK_AutomatedEventQueue_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[AutomatedEventQueue] ADD CONSTRAINT [FK_AutomatedEventQueue_EventType] FOREIGN KEY ([FromEventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
