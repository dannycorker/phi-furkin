CREATE TABLE [dbo].[Lead]
(
[LeadID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadRef] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[CustomerID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[AquariumStatusID] [int] NOT NULL CONSTRAINT [DF__Lead__AquariumSt__6A50C1DA] DEFAULT ((2)),
[ClientStatusID] [int] NULL,
[BrandNew] [bit] NOT NULL CONSTRAINT [DF__Lead__BrandNew__6B44E613] DEFAULT ((1)),
[Assigned] [bit] NULL,
[AssignedTo] [int] NULL,
[AssignedBy] [int] NULL,
[AssignedDate] [datetime] NULL,
[RecalculateEquations] [bit] NULL CONSTRAINT [DF__Lead__Recalculat__6C390A4C] DEFAULT ((0)),
[Password] [varchar] (65) COLLATE Latin1_General_CI_AS NULL,
[Salt] [varchar] (25) COLLATE Latin1_General_CI_AS NULL,
[WhenCreated] [datetime] NULL CONSTRAINT [DF__Lead__WhenCreate__3B3FCBE6] DEFAULT ([dbo].[fn_GetDate_Local]())
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2008-07-28
-- Description:	Keep ClientOfficeLeads in step with lead assignment
-- =============================================
CREATE TRIGGER [dbo].[trgiu_Lead]
   ON [dbo].[Lead]
   AFTER INSERT,UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	-- Allow special user 'notriggers' to bypass trigger code
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END

	-- Only fire this trigger if Lead.AssignedTo column has been affected.
	-- An insert always affects every column.
	-- The logical table called "inserted" is a Sql Server object that mirrors
	-- the Lead table exactly; it represents all inserted AND updated rows at once;
	-- and it contains the new values AFTER the inserts/updates have taken place.
	IF UPDATE(AssignedTo)
	BEGIN

		DECLARE @AffectedLeads TABLE (LeadID int, NewClientPersonnelID int)

		DECLARE @RowsToProcess int, @LeadID int, @NewClientPersonnelID int

		-- Create and populate a table variable of all affected LeadIDs
		INSERT INTO @AffectedLeads (LeadID, NewClientPersonnelID)
		SELECT LeadID, AssignedTo
		FROM inserted

		-- Immediately count how many records we have to deal with.
		-- This will usually be 1, but we cannot guarantee that at all.
		SELECT @RowsToProcess = @@ROWCOUNT

		-- Loop round each affected lead, performing appropriate actions:
		WHILE @RowsToProcess > 0
		BEGIN

			SELECT TOP 1 @LeadID = al.LeadID, @NewClientPersonnelID = al.NewClientPersonnelID
			FROM @AffectedLeads al

			-- Lead assignment requires ClientOfficeLead processing now:
			EXEC dbo.Lead__HandleChangeOfAssignmentByUserID @LeadID, @NewClientPersonnelID

			-- End-of-loop handling:
			DELETE @AffectedLeads
			WHERE LeadID = @LeadID

			SELECT @RowsToProcess = @RowsToProcess - 1

		END

		/* JWG 2011-12-02 Keep a history of lead assignments */
		INSERT INTO dbo.LeadAssignment(ClientID, LeadID, AssignedTo, AssignedBy, AssignedDate, WhenCreated)
		SELECT i.ClientID, i.LeadID, i.AssignedTo, i.AssignedBy, i.AssignedDate, dbo.fn_GetDate_Local()
		FROM inserted i
		WHERE i.AssignedBy IS NOT NULL

	END

END
GO
ALTER TABLE [dbo].[Lead] ADD CONSTRAINT [PK_Lead] PRIMARY KEY CLUSTERED  ([LeadID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadAssignedTo] ON [dbo].[Lead] ([AssignedTo])
GO
CREATE NONCLUSTERED INDEX [IX_Lead_Client] ON [dbo].[Lead] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_Lead_Client2017] ON [dbo].[Lead] ([ClientID]) INCLUDE ([CustomerID], [LeadTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_Lead_Customer] ON [dbo].[Lead] ([CustomerID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_Lead_LeadRef] ON [dbo].[Lead] ([LeadRef])
GO
CREATE NONCLUSTERED INDEX [IX_Lead_LeadType] ON [dbo].[Lead] ([LeadTypeID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_Lead_LeadType2017] ON [dbo].[Lead] ([LeadTypeID], [ClientID]) INCLUDE ([CustomerID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadWhenCreated] ON [dbo].[Lead] ([WhenCreated])
GO
ALTER TABLE [dbo].[Lead] ADD CONSTRAINT [FK_Lead_AquariumStatus] FOREIGN KEY ([AquariumStatusID]) REFERENCES [dbo].[AquariumStatus] ([AquariumStatusID])
GO
ALTER TABLE [dbo].[Lead] ADD CONSTRAINT [FK_Lead_ClientPersonnel] FOREIGN KEY ([AssignedTo]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[Lead] ADD CONSTRAINT [FK_Lead_ClientPersonnel1] FOREIGN KEY ([AssignedBy]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[Lead] ADD CONSTRAINT [FK_Lead_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[Lead] ADD CONSTRAINT [FK_Lead_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Lead] ADD CONSTRAINT [FK_Lead_LeadStatus] FOREIGN KEY ([ClientStatusID]) REFERENCES [dbo].[LeadStatus] ([StatusID])
GO
ALTER TABLE [dbo].[Lead] ADD CONSTRAINT [FK_Lead_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
