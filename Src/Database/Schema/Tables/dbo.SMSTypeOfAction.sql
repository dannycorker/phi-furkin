CREATE TABLE [dbo].[SMSTypeOfAction]
(
[SMSTypeOfActionID] [int] NOT NULL IDENTITY(1, 1),
[Action] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[SMSTypeOfAction] ADD CONSTRAINT [PK_SMSTypeOfAction] PRIMARY KEY CLUSTERED  ([SMSTypeOfActionID])
GO
