CREATE TABLE [dbo].[EventSubtype]
(
[EventSubtypeID] [int] NOT NULL,
[EventSubtypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[EventSubtypeDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[ApplyEventMaxThreshold] [int] NOT NULL CONSTRAINT [DF__EventSubt__Apply__62AFA012] DEFAULT ((0)),
[Available] [bit] NULL
)
GO
ALTER TABLE [dbo].[EventSubtype] ADD CONSTRAINT [PK_AquariumEventSubtype] PRIMARY KEY CLUSTERED  ([EventSubtypeID])
GO
ALTER TABLE [dbo].[EventSubtype] ADD CONSTRAINT [IX_EventSubtype] UNIQUE NONCLUSTERED  ([EventSubtypeName])
GO
