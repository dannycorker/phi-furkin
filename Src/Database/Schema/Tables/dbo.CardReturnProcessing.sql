CREATE TABLE [dbo].[CardReturnProcessing]
(
[CardReturnProcessingID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ReasonCodeType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FailureCode] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[FailureDescription] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[NewAccountRequired] [bit] NULL,
[EventToApply] [int] NULL,
[AddEventAs] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[PaymentGatewayID] [int] NULL,
[RetryPayment] [bit] NULL
)
GO
ALTER TABLE [dbo].[CardReturnProcessing] ADD CONSTRAINT [PK_CardReturnProcessing] PRIMARY KEY CLUSTERED  ([CardReturnProcessingID])
GO
