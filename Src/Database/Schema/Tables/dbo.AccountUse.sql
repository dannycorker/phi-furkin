CREATE TABLE [dbo].[AccountUse]
(
[AccountUseID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[AccountUse] ADD CONSTRAINT [PK___AccountUse] PRIMARY KEY CLUSTERED  ([AccountUseID])
GO
