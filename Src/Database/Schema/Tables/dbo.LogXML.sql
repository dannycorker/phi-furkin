CREATE TABLE [dbo].[LogXML]
(
[LogXMLID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LogDateTime] [datetime] NOT NULL,
[ContextID] [int] NOT NULL,
[ContextVarchar] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[LogXMLEntry] [xml] NULL,
[InvalidXML] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[LogXML] ADD CONSTRAINT [PK_LogXML] PRIMARY KEY CLUSTERED  ([LogXMLID])
GO
