CREATE TABLE [dbo].[ThirdPartyMappingTemplate]
(
[ThirdPartyMappingTemplateID] [int] NOT NULL IDENTITY(1, 1),
[ThirdPartyFieldGroupID] [int] NOT NULL,
[ThirdPartySystemID] [int] NOT NULL,
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[XMLItemTemplate] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[XMLContainerName] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[XMLItemName] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[XMLItemKey] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2013-12-19
-- Description:	Only allow ThirdParty records to be created in the Aquarius database
-- =============================================
CREATE TRIGGER [dbo].[trgi_ThirdPartyMappingTemplate]
   ON [dbo].[ThirdPartyMappingTemplate]
   AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF SYSTEM_USER = 'notriggers'
	BEGIN
		RETURN
	END

	/* Also allow special user 'AquariumSqlDiff' to insert records.  And allow anyone in the Aquarius database. */
    IF DB_NAME() = 'Aquarius' OR SYSTEM_USER = 'AquariumSqlDiff'
    BEGIN
		RETURN
    END

	/* Deny the insert and raise an error back to the user */
	ROLLBACK
	RAISERROR('Stop! Changes to this table can only be made in the Aquarius database and distributed by the AquariumSqlDiff user!', 16, 1)

END
GO
ALTER TABLE [dbo].[ThirdPartyMappingTemplate] ADD CONSTRAINT [PK_ThirdPartyMappingTemplate] PRIMARY KEY CLUSTERED  ([ThirdPartyMappingTemplateID])
GO
ALTER TABLE [dbo].[ThirdPartyMappingTemplate] ADD CONSTRAINT [FK_ThirdPartyMappingTemplate_ThirdPartyFieldGroup] FOREIGN KEY ([ThirdPartyFieldGroupID]) REFERENCES [dbo].[ThirdPartyFieldGroup] ([ThirdPartyFieldGroupID])
GO
ALTER TABLE [dbo].[ThirdPartyMappingTemplate] ADD CONSTRAINT [FK_ThirdPartyMappingTemplate_ThirdPartySystem] FOREIGN KEY ([ThirdPartySystemID]) REFERENCES [dbo].[ThirdPartySystem] ([ThirdPartySystemId])
GO
