CREATE TABLE [dbo].[_C600_ClaimData]
(
[ClaimID] [int] NOT NULL IDENTITY(1, 1),
[ClaimSystemClaimID] [int] NULL,
[ClaimSystemID] [int] NULL,
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[PetName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[PolicyID] [int] NOT NULL,
[PolicyInceptionDate] [date] NULL,
[PolicyStartDate] [date] NULL,
[PolicyStatus] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[PolicyStatusID] [int] NULL,
[ClaimFNOL] [date] NOT NULL,
[ClaimTreatmentStart] [date] NOT NULL,
[ClaimTreatmentEnd] [date] NULL,
[ClaimAmount] [money] NOT NULL,
[ClaimSettlementAmount] [money] NOT NULL,
[ClaimStatus] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ClaimStatusID] [int] NOT NULL,
[AilmentFirstCause] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[AilmentSecondCause] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[AilmentVeNomCode] [int] NOT NULL,
[PaymentGross] [money] NOT NULL,
[PaymentAccountID] [int] NULL,
[PaymentAccountName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[PaymentAccountNumber] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[PaymentAccountSortcode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[PaymentMethod] [varchar] (15) COLLATE Latin1_General_CI_AS NULL,
[PaymentMethodID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [date] NULL,
[CustomerPaymentScheduleID] [int] NULL,
[DateOfDeath] [date] NULL
)
GO
ALTER TABLE [dbo].[_C600_ClaimData] ADD CONSTRAINT [PK_C600_ClaimData] PRIMARY KEY CLUSTERED  ([ClaimID])
GO
ALTER TABLE [dbo].[_C600_ClaimData] ADD CONSTRAINT [FK_C600_ClaimData_ClaimSystemID] FOREIGN KEY ([ClaimSystemID]) REFERENCES [dbo].[_C600_ClaimSystem] ([ClaimSystemID])
GO
ALTER TABLE [dbo].[_C600_ClaimData] ADD CONSTRAINT [FK_C600_ClaimData_ClientID] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[_C600_ClaimData] ADD CONSTRAINT [FK_C600_ClaimData_CustomerID] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[_C600_ClaimData] ADD CONSTRAINT [FK_C600_ClaimData_PolicyID] FOREIGN KEY ([PolicyID]) REFERENCES [dbo].[Lead] ([LeadID])
GO
