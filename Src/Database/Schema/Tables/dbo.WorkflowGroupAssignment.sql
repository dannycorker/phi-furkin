CREATE TABLE [dbo].[WorkflowGroupAssignment]
(
[WorkflowGroupAssignmentID] [int] NOT NULL IDENTITY(1, 1),
[WorkflowGroupID] [int] NOT NULL,
[ClientPersonnelID] [int] NOT NULL,
[Priority] [int] NOT NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[WorkflowGroupAssignment] ADD CONSTRAINT [PK_WorkflowGroupAssignment] PRIMARY KEY CLUSTERED  ([WorkflowGroupAssignmentID])
GO
ALTER TABLE [dbo].[WorkflowGroupAssignment] ADD CONSTRAINT [FK_WorkflowGroupAssignment_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[WorkflowGroupAssignment] ADD CONSTRAINT [FK_WorkflowGroupAssignment_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[WorkflowGroupAssignment] ADD CONSTRAINT [FK_WorkflowGroupAssignment_WorkflowGroup] FOREIGN KEY ([WorkflowGroupID]) REFERENCES [dbo].[WorkflowGroup] ([WorkflowGroupID])
GO
