CREATE TABLE [dbo].[PremiumFunderCancelAgreementRequest]
(
[PremiumFunderCancelAgreementRequestID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[MatterID] [int] NULL,
[RequestJson] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[PremiumFunderRequestID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[PremiumFunderCancelAgreementRequest] ADD CONSTRAINT [PK_PremiumFunderCancelAgreementRequest] PRIMARY KEY CLUSTERED  ([PremiumFunderCancelAgreementRequestID])
GO
