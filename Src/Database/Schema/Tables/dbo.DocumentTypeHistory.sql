CREATE TABLE [dbo].[DocumentTypeHistory]
(
[DocumentTypeHistoryID] [int] NOT NULL IDENTITY(1, 1),
[DocumentTypeID] [int] NULL,
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NULL,
[DocumentTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DocumentTypeDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[HeaderChanged] [bit] NULL,
[TemplateChanged] [bit] NULL,
[FooterChanged] [bit] NULL,
[CanBeAutoSent] [bit] NOT NULL,
[EmailSubject] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[EmailBodyTextChanged] [bit] NULL,
[InputFormat] [varchar] (24) COLLATE Latin1_General_CI_AS NULL,
[OutputFormat] [varchar] (24) COLLATE Latin1_General_CI_AS NULL,
[Enabled] [bit] NOT NULL,
[RecipientsTo] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[RecipientsCC] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[RecipientsBCC] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[ReadOnlyTo] [bit] NOT NULL,
[ReadOnlyCC] [bit] NOT NULL,
[ReadOnlyBCC] [bit] NOT NULL,
[SendToMultipleRecipients] [bit] NOT NULL,
[MultipleRecipientDataSourceType] [int] NULL,
[MultipleRecipientDataSourceID] [int] NULL,
[SendToAllByDefault] [bit] NOT NULL,
[WhoChanged] [int] NULL,
[WhenChanged] [datetime] NOT NULL,
[ChangeAction] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ChangeNotes] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[EmailBodyText] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ExcelTemplatePath] [varchar] (1024) COLLATE Latin1_General_CI_AS NULL,
[FromDetails] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[ReadOnlyFrom] [bit] NULL,
[FolderID] [int] NULL,
[IsThunderheadTemplate] [bit] NULL,
[ThunderheadUniqueTemplateID] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[ThunderheadDocumentFormat] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[DocumentTitleTemplate] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[DocumentTypeHistory] ADD CONSTRAINT [PK_DocumentTypeHistory] PRIMARY KEY CLUSTERED  ([DocumentTypeHistoryID])
GO
