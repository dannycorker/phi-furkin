CREATE TABLE [dbo].[__BlockedLeadEventUpdate]
(
[BlockedLeadEventUpdateID] [int] NOT NULL IDENTITY(1, 1),
[LeadEventID] [int] NOT NULL,
[LeadEventThreadCompletionID] [int] NOT NULL,
[WhenBlocked] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[__BlockedLeadEventUpdate] ADD CONSTRAINT [PK___BlockedLeadEventUpdate] PRIMARY KEY CLUSTERED  ([BlockedLeadEventUpdateID])
GO
