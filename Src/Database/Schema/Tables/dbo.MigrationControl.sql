CREATE TABLE [dbo].[MigrationControl]
(
[MigrationControlID] [int] NOT NULL,
[Username] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[StageID] [int] NOT NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[MigrationControl] ADD CONSTRAINT [PK_MigrationControl] PRIMARY KEY CLUSTERED  ([MigrationControlID])
GO
ALTER TABLE [dbo].[MigrationControl] ADD CONSTRAINT [FK_MigrationControl_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[MigrationControl] ADD CONSTRAINT [FK_MigrationControl_MigrationStage] FOREIGN KEY ([StageID]) REFERENCES [dbo].[MigrationStage] ([MigrationStageID])
GO
