CREATE TABLE [dbo].[ThirdPartyFieldMappingValueKey]
(
[ThirdPartyFieldMappingValueKeyID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[FieldMappingID] [int] NOT NULL,
[DetailValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[LookupListItemID] [int] NULL,
[ThirdPartyKey] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[ThirdPartyFieldMappingValueKey] ADD CONSTRAINT [PK_ThirdPartyFieldMappingValueKey] PRIMARY KEY CLUSTERED  ([ThirdPartyFieldMappingValueKeyID])
GO
CREATE NONCLUSTERED INDEX [IX_ThirdPartyFieldMappingValueKey] ON [dbo].[ThirdPartyFieldMappingValueKey] ([ClientID], [FieldMappingID])
GO
ALTER TABLE [dbo].[ThirdPartyFieldMappingValueKey] ADD CONSTRAINT [FK_ThirdPartyFieldMappingValueKey_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ThirdPartyFieldMappingValueKey] ADD CONSTRAINT [FK_ThirdPartyFieldMappingValueKey_LookupListItems] FOREIGN KEY ([LookupListItemID]) REFERENCES [dbo].[LookupListItems] ([LookupListItemID])
GO
ALTER TABLE [dbo].[ThirdPartyFieldMappingValueKey] ADD CONSTRAINT [FK_ThirdPartyFieldMappingValueKey_ThirdPartyFieldMappingValueKey] FOREIGN KEY ([FieldMappingID]) REFERENCES [dbo].[ThirdPartyFieldMapping] ([ThirdPartyFieldMappingID])
GO
