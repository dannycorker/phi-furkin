CREATE TABLE [dbo].[DeletedPartner]
(
[PartnerID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[UseCustomerAddress] [bit] NOT NULL,
[TitleID] [int] NULL,
[FirstName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[MiddleName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[EmailAddress] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[DayTimeTelephoneNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[HomeTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MobileTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Address1] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Address2] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Town] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[County] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[PostCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[Occupation] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Employer] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[DateOfBirth] [datetime] NULL,
[FullName] AS (([FirstName]+' ')+[LastName]),
[CountryID] [int] NULL,
[WhoDeleted] [int] NULL
)
GO
