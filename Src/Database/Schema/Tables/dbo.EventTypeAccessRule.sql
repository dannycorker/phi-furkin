CREATE TABLE [dbo].[EventTypeAccessRule]
(
[EventTypeAccessRuleID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[AccessRuleID] [int] NOT NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[EventTypeAccessRule] ADD CONSTRAINT [PK_EventTypeAccessRule] PRIMARY KEY CLUSTERED  ([EventTypeAccessRuleID])
GO
CREATE NONCLUSTERED INDEX [IX_EventTypeAccessRule_SourceID] ON [dbo].[EventTypeAccessRule] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[EventTypeAccessRule] ADD CONSTRAINT [FK_EventTypeAccessRule_AccessRule] FOREIGN KEY ([AccessRuleID]) REFERENCES [dbo].[AccessRule] ([AccessRuleID])
GO
ALTER TABLE [dbo].[EventTypeAccessRule] ADD CONSTRAINT [FK_EventTypeAccessRule_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EventTypeAccessRule] ADD CONSTRAINT [FK_EventTypeAccessRule_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
