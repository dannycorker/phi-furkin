CREATE TABLE [dbo].[DroppedOutCustomerAnswers]
(
[CustomerAnswerID] [int] NOT NULL IDENTITY(1, 1),
[CustomerQuestionnaireID] [int] NULL,
[MasterQuestionID] [int] NULL,
[Answer] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[QuestionPossibleAnswerID] [int] NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[DroppedOutCustomerAnswers] ADD CONSTRAINT [PK_DroppedOutCustomerAnswers] PRIMARY KEY CLUSTERED  ([CustomerAnswerID])
GO
ALTER TABLE [dbo].[DroppedOutCustomerAnswers] ADD CONSTRAINT [FK_DroppedOutCustomerAnswers_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DroppedOutCustomerAnswers] ADD CONSTRAINT [FK_DroppedOutCustomerAnswers_CustomerQuestionnaires] FOREIGN KEY ([CustomerQuestionnaireID]) REFERENCES [dbo].[DroppedOutCustomerQuestionnaires] ([CustomerQuestionnaireID])
GO
