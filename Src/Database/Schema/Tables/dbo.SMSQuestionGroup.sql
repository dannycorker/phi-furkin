CREATE TABLE [dbo].[SMSQuestionGroup]
(
[SMSQuestionGroupID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SMSQuestionnaireID] [int] NOT NULL,
[GroupName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[SMSQuestionOrderID] [int] NOT NULL,
[GroupOrder] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[SMSQuestionGroup] ADD CONSTRAINT [PK_SMSQuestionGroup] PRIMARY KEY CLUSTERED  ([SMSQuestionGroupID])
GO
ALTER TABLE [dbo].[SMSQuestionGroup] ADD CONSTRAINT [FK_SMSQuestionGroup_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SMSQuestionGroup] ADD CONSTRAINT [FK_SMSQuestionGroup_SMSQuestionnaire] FOREIGN KEY ([SMSQuestionnaireID]) REFERENCES [dbo].[SMSQuestionnaire] ([SMSQuestionnaireID])
GO
ALTER TABLE [dbo].[SMSQuestionGroup] ADD CONSTRAINT [FK_SMSQuestionGroup_SMSQuestionOrder] FOREIGN KEY ([SMSQuestionOrderID]) REFERENCES [dbo].[SMSQuestionOrder] ([SMSQuestionOrderID])
GO
