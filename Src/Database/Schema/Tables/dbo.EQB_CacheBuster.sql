CREATE TABLE [dbo].[EQB_CacheBuster]
(
[CacheBusterId] [int] NOT NULL IDENTITY(1, 1),
[Created] [datetime2] NULL CONSTRAINT [DF__EQB_Cache__Creat__3A4BA7AD] DEFAULT ([dbo].[fn_GetDate_Local]())
)
GO
ALTER TABLE [dbo].[EQB_CacheBuster] ADD CONSTRAINT [PK__EQB_Cach__7002CE7DE8E94703] PRIMARY KEY CLUSTERED  ([CacheBusterId])
GO
