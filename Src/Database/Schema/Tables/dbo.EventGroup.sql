CREATE TABLE [dbo].[EventGroup]
(
[EventGroupID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventGroupName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[EventGroupDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[ChangeNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[SourceID] [int] NULL,
[LeadTypeID] [int] NULL
)
GO
ALTER TABLE [dbo].[EventGroup] ADD CONSTRAINT [PK_EventGroup] PRIMARY KEY CLUSTERED  ([EventGroupID])
GO
CREATE NONCLUSTERED INDEX [IX_EventGroup_SourceID] ON [dbo].[EventGroup] ([SourceID]) WHERE ([SourceID]>(0))
GO
