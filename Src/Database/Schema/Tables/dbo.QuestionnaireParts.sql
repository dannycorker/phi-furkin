CREATE TABLE [dbo].[QuestionnaireParts]
(
[PartNameID] [int] NOT NULL IDENTITY(1, 1),
[PartName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[QuestionnaireParts] ADD CONSTRAINT [PK_QuestionnaireParts] PRIMARY KEY CLUSTERED  ([PartNameID])
GO
