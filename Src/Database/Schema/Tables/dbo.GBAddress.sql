CREATE TABLE [dbo].[GBAddress]
(
[GBAddressID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[WebServiceURL] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[GBAddress] ADD CONSTRAINT [PK_GBAddress] PRIMARY KEY CLUSTERED  ([GBAddressID])
GO
ALTER TABLE [dbo].[GBAddress] ADD CONSTRAINT [FK_GBAddress_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
