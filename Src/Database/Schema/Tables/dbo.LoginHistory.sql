CREATE TABLE [dbo].[LoginHistory]
(
[LoginHistoryID] [int] NOT NULL IDENTITY(1, 1),
[UserName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[LoginDateTime] [datetime] NOT NULL,
[AppLogin] [bit] NOT NULL,
[IsSuccess] [bit] NOT NULL,
[ClientID] [int] NULL,
[ClientPersonnelID] [int] NULL,
[AttemptedLogins] [int] NULL,
[AccountDisabled] [bit] NULL,
[Notes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ThirdPartySystemID] [int] NULL,
[HostName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[UserIPDetails] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[IsLogout] [bit] NULL
)
GO
ALTER TABLE [dbo].[LoginHistory] ADD CONSTRAINT [PK_LoginHistory] PRIMARY KEY CLUSTERED  ([LoginHistoryID])
GO
CREATE NONCLUSTERED INDEX [IX_LoginHistory_ClientClientpersonnel] ON [dbo].[LoginHistory] ([ClientID], [ClientPersonnelID])
GO
CREATE NONCLUSTERED INDEX [IX_LoginHistory_LoginDateTime] ON [dbo].[LoginHistory] ([LoginDateTime])
GO
