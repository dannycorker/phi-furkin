CREATE TABLE [dbo].[WorkObject1]
(
[WorkObject1ID] [int] NOT NULL IDENTITY(1, 1),
[WorkSeedID] [int] NOT NULL,
[ObjectID] [int] NULL,
[Name] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Details] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ObjectTypeID] [int] NULL,
[ObjectTypeRelationshipID] [int] NULL,
[FromObjectID] [int] NULL
)
GO
ALTER TABLE [dbo].[WorkObject1] ADD CONSTRAINT [PK_WorkObject1] PRIMARY KEY CLUSTERED  ([WorkObject1ID])
GO
ALTER TABLE [dbo].[WorkObject1] ADD CONSTRAINT [FK_WorkObject1_WorkSeed] FOREIGN KEY ([WorkSeedID]) REFERENCES [dbo].[WorkSeed] ([WorkSeedID]) ON DELETE CASCADE
GO
