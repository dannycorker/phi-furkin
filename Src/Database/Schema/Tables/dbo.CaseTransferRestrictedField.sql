CREATE TABLE [dbo].[CaseTransferRestrictedField]
(
[CaseTransferRestrictedFieldID] [int] NOT NULL IDENTITY(1, 1),
[ClientRelationshipID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[CaseTransferRestrictedField] ADD CONSTRAINT [PK_CaseTransferRestrictedField] PRIMARY KEY CLUSTERED  ([CaseTransferRestrictedFieldID])
GO
ALTER TABLE [dbo].[CaseTransferRestrictedField] ADD CONSTRAINT [FK_CaseTransferRestrictedField_ClientRelationship] FOREIGN KEY ([ClientRelationshipID]) REFERENCES [dbo].[ClientRelationship] ([ClientRelationshipID])
GO
ALTER TABLE [dbo].[CaseTransferRestrictedField] ADD CONSTRAINT [FK_CaseTransferRestrictedField_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
