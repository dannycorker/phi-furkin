CREATE TABLE [dbo].[WorkflowGroup]
(
[WorkflowGroupID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[ManagerID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[Enabled] [bit] NOT NULL CONSTRAINT [DF__WorkflowG__Enabl__116A8EFB] DEFAULT ((1)),
[SortOrder] [varchar] (4) COLLATE Latin1_General_CI_AS NULL,
[AssignedLeadsOnly] [bit] NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[WorkflowGroup] ADD CONSTRAINT [PK_WorkflowGroup] PRIMARY KEY CLUSTERED  ([WorkflowGroupID])
GO
CREATE NONCLUSTERED INDEX [IX_WorkflowGroup_SourceID] ON [dbo].[WorkflowGroup] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[WorkflowGroup] ADD CONSTRAINT [FK_WorkflowGroup_ClientPersonnel] FOREIGN KEY ([ManagerID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[WorkflowGroup] ADD CONSTRAINT [FK_WorkflowGroup_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
