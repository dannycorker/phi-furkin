CREATE TABLE [dbo].[TableRowsClientOption]
(
[TableRowsClientOptionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[SqlToUse] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[CompleteSQLOverride] [bit] NOT NULL,
[Description] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[WhoAdded] [varchar] (201) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenAdded] [datetime] NOT NULL,
[WhoChanged] [varchar] (201) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenChanged] [datetime] NOT NULL
)
GO
