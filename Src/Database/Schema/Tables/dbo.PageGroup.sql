CREATE TABLE [dbo].[PageGroup]
(
[PageGroupID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[PageGroupName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[PageGroupDescription] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[PageGroup] ADD CONSTRAINT [PK_PageGroup] PRIMARY KEY CLUSTERED  ([PageGroupID])
GO
ALTER TABLE [dbo].[PageGroup] ADD CONSTRAINT [FK_PageGroup_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[PageGroup] ADD CONSTRAINT [FK_PageGroup_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
