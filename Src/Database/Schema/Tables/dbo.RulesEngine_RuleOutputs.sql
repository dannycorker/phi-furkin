CREATE TABLE [dbo].[RulesEngine_RuleOutputs]
(
[RuleOutputID] [int] NOT NULL IDENTITY(1, 1),
[RuleID] [int] NOT NULL,
[Value] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[TransformID] [int] NOT NULL,
[SourceID] [int] NULL
)
GO
ALTER TABLE [dbo].[RulesEngine_RuleOutputs] ADD CONSTRAINT [PK_RulesEngine_RuleOutputs] PRIMARY KEY CLUSTERED  ([RuleOutputID])
GO
CREATE NONCLUSTERED INDEX [IX_RulesEngine_RuleOutputs] ON [dbo].[RulesEngine_RuleOutputs] ([RuleOutputID], [TransformID], [Value], [RuleID])
GO
CREATE NONCLUSTERED INDEX [IX_RulesEngine_RuleOutputs_TransformID_RuleOutputID] ON [dbo].[RulesEngine_RuleOutputs] ([TransformID], [RuleOutputID])
GO
ALTER TABLE [dbo].[RulesEngine_RuleOutputs] ADD CONSTRAINT [FK_RulesEngine_RuleOutputs_RulesEngine_Rules] FOREIGN KEY ([RuleID]) REFERENCES [dbo].[RulesEngine_Rules] ([RuleID])
GO
ALTER TABLE [dbo].[RulesEngine_RuleOutputs] ADD CONSTRAINT [FK_RulesEngine_RuleOutputs_RulesEngine_Transforms] FOREIGN KEY ([TransformID]) REFERENCES [dbo].[RulesEngine_Transforms] ([TransformID])
GO
