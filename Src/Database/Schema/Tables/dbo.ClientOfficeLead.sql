CREATE TABLE [dbo].[ClientOfficeLead]
(
[ClientOfficeLeadID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientOfficeID] [int] NOT NULL,
[LeadID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ClientOfficeLead] ADD CONSTRAINT [PK_OfficeLeadOwnership] PRIMARY KEY CLUSTERED  ([ClientOfficeLeadID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientOfficeLead_ClientOffice] ON [dbo].[ClientOfficeLead] ([ClientOfficeID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientOfficeLead_Lead] ON [dbo].[ClientOfficeLead] ([LeadID])
GO
ALTER TABLE [dbo].[ClientOfficeLead] ADD CONSTRAINT [FK_OfficeLeadOwnership_ClientOffices] FOREIGN KEY ([ClientOfficeID]) REFERENCES [dbo].[ClientOffices] ([ClientOfficeID])
GO
ALTER TABLE [dbo].[ClientOfficeLead] ADD CONSTRAINT [FK_OfficeLeadOwnership_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ClientOfficeLead] ADD CONSTRAINT [FK_OfficeLeadOwnership_OfficeLeadOwnership] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
