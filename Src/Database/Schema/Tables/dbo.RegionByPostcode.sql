CREATE TABLE [dbo].[RegionByPostcode]
(
[RegionByPostcodeID] [int] NOT NULL IDENTITY(1, 1),
[CountryID] [int] NOT NULL,
[Postcode] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[PostcodeInt] [int] NULL,
[Region] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[RegionByPostcode] ADD CONSTRAINT [PK_RegionByPostcode] PRIMARY KEY CLUSTERED  ([RegionByPostcodeID])
GO
