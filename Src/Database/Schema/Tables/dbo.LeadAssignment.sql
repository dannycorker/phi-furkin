CREATE TABLE [dbo].[LeadAssignment]
(
[LeadAssignmentID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[AssignedTo] [int] NULL,
[AssignedBy] [int] NOT NULL,
[AssignedDate] [datetime] NOT NULL,
[WhenCreated] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[LeadAssignment] ADD CONSTRAINT [PK_LeadAssignment] PRIMARY KEY CLUSTERED  ([LeadAssignmentID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadAssignment_Client] ON [dbo].[LeadAssignment] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadAssignment_Lead] ON [dbo].[LeadAssignment] ([LeadID], [ClientID])
GO
