CREATE TABLE [dbo].[DocumentTypeFolderLink]
(
[DocumentTypeID] [int] NOT NULL,
[FolderID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[DocumentTypeFolderLink] ADD CONSTRAINT [PK_DocumentTypeFolderLink] PRIMARY KEY CLUSTERED  ([DocumentTypeID], [FolderID])
GO
ALTER TABLE [dbo].[DocumentTypeFolderLink] ADD CONSTRAINT [FK_DocumentTypeFolderLink_DocumentType] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentType] ([DocumentTypeID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DocumentTypeFolderLink] ADD CONSTRAINT [FK_DocumentTypeFolderLink_Folder] FOREIGN KEY ([FolderID]) REFERENCES [dbo].[Folder] ([FolderID])
GO
