CREATE TABLE [dbo].[CalcsEngine_CalculationParameters]
(
[CalculationParameterID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[CalculationID] [int] NOT NULL,
[ParameterTypeID] [int] NOT NULL,
[Value] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[Description] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[CalcsEngine_CalculationParameters] ADD CONSTRAINT [PK_CalcsEngine_CalculationParameters] PRIMARY KEY CLUSTERED  ([CalculationParameterID])
GO
ALTER TABLE [dbo].[CalcsEngine_CalculationParameters] ADD CONSTRAINT [FK_CalcsEngine_CalculationParameters_CalcsEngine_Calculations] FOREIGN KEY ([CalculationID]) REFERENCES [dbo].[CalcsEngine_Calculations] ([CalculationID])
GO
ALTER TABLE [dbo].[CalcsEngine_CalculationParameters] ADD CONSTRAINT [FK_CalcsEngine_CalculationParameters_CalcsEngine_ParameterTypes] FOREIGN KEY ([ParameterTypeID]) REFERENCES [dbo].[CalcsEngine_ParameterTypes] ([ParameterTypeID])
GO
