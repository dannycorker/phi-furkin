CREATE TABLE [dbo].[ObjectLink]
(
[ObjectLinkID] [int] NOT NULL IDENTITY(1, 1),
[FromObjectID] [int] NOT NULL,
[ToObjectID] [int] NOT NULL,
[ObjectTypeRelationshipID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ObjectLink] ADD CONSTRAINT [PK_ObjectLink] PRIMARY KEY CLUSTERED  ([ObjectLinkID])
GO
ALTER TABLE [dbo].[ObjectLink] ADD CONSTRAINT [FK_ObjectLink_ObjectsFrom] FOREIGN KEY ([FromObjectID]) REFERENCES [dbo].[Objects] ([ObjectID])
GO
ALTER TABLE [dbo].[ObjectLink] ADD CONSTRAINT [FK_ObjectLink_ObjectsTo] FOREIGN KEY ([ToObjectID]) REFERENCES [dbo].[Objects] ([ObjectID])
GO
ALTER TABLE [dbo].[ObjectLink] ADD CONSTRAINT [FK_ObjectLink_ObjectTypeRelationship] FOREIGN KEY ([ObjectTypeRelationshipID]) REFERENCES [dbo].[ObjectTypeRelationship] ([ObjectTypeRelationshipID])
GO
