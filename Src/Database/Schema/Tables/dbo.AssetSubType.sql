CREATE TABLE [dbo].[AssetSubType]
(
[AssetSubTypeID] [int] NOT NULL IDENTITY(1, 1),
[AssetSubTypeName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Note] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Enabled] [bit] NOT NULL,
[Deleted] [bit] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[AssetSubType] ADD CONSTRAINT [PK__AssetSub__57CEFB10666C7EE3] PRIMARY KEY CLUSTERED  ([AssetSubTypeID])
GO
