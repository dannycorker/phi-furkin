CREATE TABLE [dbo].[RPIInsurerExistingDataRL]
(
[ResourceListID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[InsurerName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[InsurerType] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[OrganisationID] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[OrganisationPath] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[RPIInsurerExistingDataRL] ADD CONSTRAINT [PK_RPIInsurerExistingDataRL] PRIMARY KEY CLUSTERED  ([ResourceListID])
GO
