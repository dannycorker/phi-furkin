CREATE TABLE [dbo].[ClientRelationshipHistory]
(
[ClientRelationshipHistoryID] [int] NOT NULL IDENTITY(1, 1),
[ClientRelationshipID] [int] NOT NULL,
[ClientRelationshipName] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[OutgoingClientID] [int] NOT NULL,
[ReceivingClientID] [int] NOT NULL,
[OutgoingLeadTypeID] [int] NOT NULL,
[IncomingLeadTypeID] [int] NOT NULL,
[OutgoingEventTypeID] [int] NOT NULL,
[IncomingEventTypeID] [int] NOT NULL,
[Enabled] [bit] NOT NULL,
[ClearLeadRefs] [bit] NOT NULL,
[WhoChanged] [int] NULL,
[WhenChanged] [datetime] NOT NULL,
[ChangeAction] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ChangeNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ClientRelationshipHistory] ADD CONSTRAINT [PK_ClientRelationshipHistory] PRIMARY KEY CLUSTERED  ([ClientRelationshipHistoryID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientRelationshipHistory_ClientRelationship] ON [dbo].[ClientRelationshipHistory] ([ClientRelationshipID])
GO
