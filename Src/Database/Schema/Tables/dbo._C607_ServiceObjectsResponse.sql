CREATE TABLE [dbo].[_C607_ServiceObjectsResponse]
(
[ServiceObjectsResponseID] [int] NOT NULL IDENTITY(1, 1),
[MatterID] [int] NOT NULL,
[ResponseXML] [xml] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[_C607_ServiceObjectsResponse] ADD CONSTRAINT [PK_ServiceObjectsResponse] PRIMARY KEY CLUSTERED  ([ServiceObjectsResponseID])
GO
