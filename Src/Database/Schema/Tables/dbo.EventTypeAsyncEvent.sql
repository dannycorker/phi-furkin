CREATE TABLE [dbo].[EventTypeAsyncEvent]
(
[EventTypeAsyncEventID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SourceEventTypeID] [int] NOT NULL,
[AsyncEventTypeID] [int] NOT NULL,
[FollowupThreadNumber] [int] NULL,
[AquariumOptionID] [int] NOT NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[EventTypeAsyncEvent] ADD CONSTRAINT [PK_EventTypeAsyncEvent] PRIMARY KEY CLUSTERED  ([EventTypeAsyncEventID])
GO
CREATE NONCLUSTERED INDEX [IX_EventTypeAsyncEvent_SourceID] ON [dbo].[EventTypeAsyncEvent] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[EventTypeAsyncEvent] ADD CONSTRAINT [FK_EventTypeAsyncEvent_AquariumOption] FOREIGN KEY ([AquariumOptionID]) REFERENCES [dbo].[AquariumOption] ([AquariumOptionID])
GO
ALTER TABLE [dbo].[EventTypeAsyncEvent] ADD CONSTRAINT [FK_EventTypeAsyncEvent_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EventTypeAsyncEvent] ADD CONSTRAINT [FK_EventTypeAsyncEvent_EventType] FOREIGN KEY ([AsyncEventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[EventTypeAsyncEvent] ADD CONSTRAINT [FK_EventTypeAsyncEvent_EventType1] FOREIGN KEY ([SourceEventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
