CREATE TABLE [dbo].[LeadTypePageTitle]
(
[LeadTypeID] [int] NOT NULL,
[PageTitle] [varchar] (100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[LeadTypePageTitle] ADD CONSTRAINT [PK_LeadTypePageTitle] PRIMARY KEY CLUSTERED  ([LeadTypeID])
GO
ALTER TABLE [dbo].[LeadTypePageTitle] ADD CONSTRAINT [FK_LeadTypePageTitle_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID]) ON DELETE CASCADE
GO
