CREATE TABLE [dbo].[RPIClientPersonnelCredential]
(
[RPIClientPersonnelCredentialID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientPersonnelID] [int] NOT NULL,
[UserName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Password] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[AsUser] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[RPIClientPersonnelCredential] ADD CONSTRAINT [PK_RPIClientPersonnelCredential] PRIMARY KEY CLUSTERED  ([RPIClientPersonnelCredentialID])
GO
ALTER TABLE [dbo].[RPIClientPersonnelCredential] ADD CONSTRAINT [FK_RPIClientPersonnelCredential_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[RPIClientPersonnelCredential] ADD CONSTRAINT [FK_RPIClientPersonnelCredential_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
