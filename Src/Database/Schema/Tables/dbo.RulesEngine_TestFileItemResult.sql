CREATE TABLE [dbo].[RulesEngine_TestFileItemResult]
(
[TestFileItemResultID] [int] NOT NULL IDENTITY(1, 1),
[TestFileItemID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[RuleID] [int] NULL,
[RuleName] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[RuleCheckpoint] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[InputValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Transform] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Value] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[OutputValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ErrorMessage] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[WhenCreated] [datetime] NOT NULL CONSTRAINT [DF_RulesEngine_TestFileItemResult_WhenCreate] DEFAULT ([dbo].[fn_GetDate_Local]()),
[WhenProcessed] [datetime] NULL
)
GO
ALTER TABLE [dbo].[RulesEngine_TestFileItemResult] ADD CONSTRAINT [PK_RulesEngine_TestFileItemResult] PRIMARY KEY CLUSTERED  ([TestFileItemResultID])
GO
CREATE NONCLUSTERED INDEX [IX_RulesEngine_TestFileItemResult_TestFileItemID_RuleID] ON [dbo].[RulesEngine_TestFileItemResult] ([TestFileItemID], [RuleID])
GO
ALTER TABLE [dbo].[RulesEngine_TestFileItemResult] ADD CONSTRAINT [FK_RulesEngine_TestFileItemResult_RulesEngine_TestFileItem] FOREIGN KEY ([TestFileItemID]) REFERENCES [dbo].[RulesEngine_TestFileItem] ([TestFileItemID]) ON DELETE CASCADE
GO
