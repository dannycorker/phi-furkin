CREATE TABLE [dbo].[PaymentStatus]
(
[PaymentStatusID] [int] NOT NULL IDENTITY(1, 1),
[PaymentStatusName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[PaymentStatus] ADD CONSTRAINT [PK_PaymentStatus] PRIMARY KEY CLUSTERED  ([PaymentStatusID])
GO
