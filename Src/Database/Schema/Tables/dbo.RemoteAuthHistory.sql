CREATE TABLE [dbo].[RemoteAuthHistory]
(
[RemoteAuthHistoryID] [int] NOT NULL IDENTITY(1, 1),
[AuthKey] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[RequestedUsername] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[ClientPersonnelID] [int] NULL,
[ClientID] [int] NULL,
[WhenRequested] [datetime2] (0) NOT NULL,
[IPAddress] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[IsValid] [bit] NOT NULL,
[ValidUntil] [datetime2] (0) NULL,
[WhenFirstUsed] [datetime2] (0) NULL,
[WhenLastUsed] [datetime2] (0) NULL,
[UsageCount] [int] NOT NULL,
[WhenRemoved] [datetime2] (0) NULL,
[Notes] [varchar] (500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[RemoteAuthHistory] ADD CONSTRAINT [PK_RemoteAuthHistory] PRIMARY KEY CLUSTERED  ([RemoteAuthHistoryID])
GO
