CREATE TABLE [dbo].[StandaloneProperty]
(
[StandalonePropertyID] [int] NOT NULL,
[StandalonePropertyName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[StandalonePropertyDescription] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[StandalonePropertyValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[StandalonePropertyValueInt] [int] NULL,
[StandalonePropertyValueMoney] [int] NULL,
[StandalonePropertyValueDate] [date] NULL,
[StandalonePropertyValueDateTime] [datetime2] (0) NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[StandaloneProperty] ADD CONSTRAINT [PK_StandaloneProperty] PRIMARY KEY CLUSTERED  ([StandalonePropertyID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_StandaloneProperty_Name] ON [dbo].[StandaloneProperty] ([StandalonePropertyName])
GO
