CREATE TABLE [dbo].[SqlQueryTemplate]
(
[SqlQueryTemplateID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[Name] [varchar] (150) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[TemplatePath] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[DataWorksheetNumber] [int] NOT NULL,
[DataStartCell] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[TemplateFormat] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[FolderID] [int] NULL,
[CreatedBy] [int] NOT NULL,
[CreatedOn] [datetime] NULL,
[ModifiedBy] [int] NULL,
[ModifiedOn] [datetime] NULL,
[Enabled] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[SqlQueryTemplate] ADD CONSTRAINT [PK_SqlQueryTemplate] PRIMARY KEY CLUSTERED  ([SqlQueryTemplateID])
GO
CREATE NONCLUSTERED INDEX [IX_SqlQueryTemplate_ClientEnabled] ON [dbo].[SqlQueryTemplate] ([ClientID], [Enabled], [FolderID])
GO
ALTER TABLE [dbo].[SqlQueryTemplate] ADD CONSTRAINT [FK_SqlQueryTemplate_ClientPersonnel] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[SqlQueryTemplate] ADD CONSTRAINT [FK_SqlQueryTemplate_ClientPersonnel1] FOREIGN KEY ([ModifiedBy]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[SqlQueryTemplate] ADD CONSTRAINT [FK_SqlQueryTemplate_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SqlQueryTemplate] ADD CONSTRAINT [FK_SqlQueryTemplate_Folder] FOREIGN KEY ([FolderID]) REFERENCES [dbo].[Folder] ([FolderID])
GO
