CREATE TABLE [dbo].[_C00_ClientReportTableToDrop]
(
[ClientReportTableToDropID] [int] NOT NULL IDENTITY(1, 1),
[ClientReportTableBuilderID] [int] NULL,
[ClientID] [int] NOT NULL,
[TableName] [varchar] (1024) COLLATE Latin1_General_CI_AS NOT NULL,
[WhoRequested] [int] NOT NULL,
[WhenRequested] [datetime2] (0) NOT NULL,
[WhenCompleted] [datetime2] (0) NULL,
[UserNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[SSISNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
