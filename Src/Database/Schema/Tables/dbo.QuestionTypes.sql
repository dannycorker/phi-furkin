CREATE TABLE [dbo].[QuestionTypes]
(
[QuestionTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Multiselect] [bit] NOT NULL,
[EcatcherField] [bit] NULL
)
GO
ALTER TABLE [dbo].[QuestionTypes] ADD CONSTRAINT [PK_QuestionTypes] PRIMARY KEY CLUSTERED  ([QuestionTypeID])
GO
