CREATE TABLE [dbo].[KeyIdentifierMappings]
(
[KeyIdentifierMappingID] [int] NOT NULL IDENTITY(1, 1),
[KeyVal] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ClientID] [int] NOT NULL,
[CustomerID] [int] NULL,
[LeadID] [int] NULL,
[CaseID] [int] NULL,
[MatterID] [int] NULL,
[LeadTypeID] [int] NULL
)
GO
ALTER TABLE [dbo].[KeyIdentifierMappings] ADD CONSTRAINT [PK_KeyIdentifierMappings] PRIMARY KEY CLUSTERED  ([KeyIdentifierMappingID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_KeyValClientID] ON [dbo].[KeyIdentifierMappings] ([KeyVal], [ClientID])
GO
