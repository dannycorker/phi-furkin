CREATE TABLE [dbo].[GroupRightsDynamicEditing]
(
[GroupRightsDynamicEditingID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelAdminGroupID] [int] NOT NULL,
[FunctionTypeID] [int] NOT NULL,
[LeadTypeID] [int] NULL,
[ObjectID] [int] NOT NULL,
[RightID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[GroupRightsDynamicEditing] ADD CONSTRAINT [PK_GroupRightsDynamicEditing] PRIMARY KEY CLUSTERED  ([GroupRightsDynamicEditingID])
GO
ALTER TABLE [dbo].[GroupRightsDynamicEditing] ADD CONSTRAINT [FK_GroupRightsDynamicEditing_ClientPersonnelAdminGroups] FOREIGN KEY ([ClientPersonnelAdminGroupID]) REFERENCES [dbo].[ClientPersonnelAdminGroups] ([ClientPersonnelAdminGroupID])
GO
ALTER TABLE [dbo].[GroupRightsDynamicEditing] ADD CONSTRAINT [FK_GroupRightsDynamicEditing_FunctionType] FOREIGN KEY ([FunctionTypeID]) REFERENCES [dbo].[FunctionType] ([FunctionTypeID])
GO
ALTER TABLE [dbo].[GroupRightsDynamicEditing] ADD CONSTRAINT [FK_GroupRightsDynamicEditing_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
ALTER TABLE [dbo].[GroupRightsDynamicEditing] ADD CONSTRAINT [FK_GroupRightsDynamicEditing_Rights] FOREIGN KEY ([RightID]) REFERENCES [dbo].[Rights] ([RightID])
GO
