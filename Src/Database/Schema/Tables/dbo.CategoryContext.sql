CREATE TABLE [dbo].[CategoryContext]
(
[CategoryContextID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[Context] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[Category1] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Category2] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Category3] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Category4] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Category5] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CategoryContext] ADD CONSTRAINT [PK_CategoryContext] PRIMARY KEY CLUSTERED  ([CategoryContextID])
GO
ALTER TABLE [dbo].[CategoryContext] ADD CONSTRAINT [FK_CategoryContext_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
