CREATE TABLE [dbo].[EventTypeAttachment]
(
[AttachmentID] [int] NOT NULL IDENTITY(1, 1),
[EventTypeID] [int] NOT NULL,
[AttachmentEventTypeID] [int] NULL,
[All] [bit] NOT NULL,
[AttachmentEventGroupID] [int] NULL CONSTRAINT [DF_EventTypeAttachment_AttachmentEventGroupID] DEFAULT (NULL)
)
GO
ALTER TABLE [dbo].[EventTypeAttachment] ADD CONSTRAINT [PK_EventTypeAttachment] PRIMARY KEY CLUSTERED  ([AttachmentID])
GO
ALTER TABLE [dbo].[EventTypeAttachment] ADD CONSTRAINT [FK_EventTypeAttachment_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[EventTypeAttachment] ADD CONSTRAINT [FK_EventTypeAttachment_EventType1] FOREIGN KEY ([AttachmentEventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
