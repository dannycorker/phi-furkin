CREATE TABLE [dbo].[OverrideType]
(
[OverrideTypeID] [int] NOT NULL IDENTITY(1, 1),
[OverrideTypeName] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[OverrideType] ADD CONSTRAINT [PK_OverrideType] PRIMARY KEY CLUSTERED  ([OverrideTypeID])
GO
