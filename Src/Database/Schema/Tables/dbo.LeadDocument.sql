CREATE TABLE [dbo].[LeadDocument]
(
[LeadDocumentID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[DocumentTypeID] [int] NULL,
[LeadDocumentTitle] [varchar] (1000) COLLATE Latin1_General_CI_AS NOT NULL,
[UploadDateTime] [datetime] NOT NULL,
[WhoUploaded] [int] NULL,
[DocumentBLOB] [varbinary] (max) NULL,
[FileName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[EmailBLOB] [varbinary] (max) NULL,
[DocumentFormat] [varchar] (24) COLLATE Latin1_General_CI_AS NULL,
[EmailFrom] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[EmailTo] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[CcList] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[BccList] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ElectronicSignatureDocumentKey] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Encoding] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ContentFormat] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ZipFormat] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DocumentBlobSize] [int] NULL,
[EmailBlobSize] [int] NULL,
[DocumentDatabaseID] [int] NULL,
[WhenArchived] [datetime] NULL,
[DocumentTypeVersionID] [int] NULL
)
GO
ALTER TABLE [dbo].[LeadDocument] ADD CONSTRAINT [PK_LeadDocument] PRIMARY KEY CLUSTERED  ([LeadDocumentID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadDocumentClient] ON [dbo].[LeadDocument] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadDocument_DocumentType] ON [dbo].[LeadDocument] ([DocumentTypeID]) INCLUDE ([ClientID], [LeadID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadDocument_LD_DB_Client] ON [dbo].[LeadDocument] ([LeadDocumentID], [DocumentDatabaseID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadDocument_LeadDocType] ON [dbo].[LeadDocument] ([LeadID], [DocumentTypeID])
GO
ALTER TABLE [dbo].[LeadDocument] ADD CONSTRAINT [FK_LeadDocument_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[LeadDocument] ADD CONSTRAINT [FK_LeadDocument_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
