CREATE TABLE [dbo].[ScriptLock]
(
[ScriptLockID] [int] NOT NULL IDENTITY(1, 1),
[ScriptID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[WhoIsEditing] [int] NOT NULL,
[EditStartedAt] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[ScriptLock] ADD CONSTRAINT [PK_ScriptLock] PRIMARY KEY CLUSTERED  ([ScriptLockID])
GO
ALTER TABLE [dbo].[ScriptLock] ADD CONSTRAINT [FK_ScriptLock_ClientPersonnel] FOREIGN KEY ([WhoIsEditing]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[ScriptLock] ADD CONSTRAINT [FK_ScriptLock_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ScriptLock] ADD CONSTRAINT [FK_ScriptLock_Script] FOREIGN KEY ([ScriptID]) REFERENCES [dbo].[Script] ([ScriptID])
GO
