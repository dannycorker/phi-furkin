CREATE TABLE [dbo].[LeadType]
(
[LeadTypeID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LeadTypeDescription] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Enabled] [bit] NOT NULL,
[Live] [bit] NOT NULL,
[MatterDisplayName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF__LeadType__Matter__6F1576F7] DEFAULT ('Matter'),
[LastReferenceInteger] [int] NULL,
[ReferenceValueFormatID] [int] NULL,
[IsBusiness] [bit] NULL,
[LeadDisplayName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[CaseDisplayName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[CustomerDisplayName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[LeadDetailsTabImageFileName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[CustomerTabImageFileName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[UseEventCosts] [bit] NULL,
[UseEventUOEs] [bit] NULL,
[UseEventDisbursements] [bit] NULL,
[UseEventComments] [bit] NULL,
[AllowMatterPageChoices] [bit] NULL,
[AllowLeadPageChoices] [bit] NULL,
[IsRPIEnabled] [bit] NULL,
[FollowupWorkingDaysOnly] [bit] NULL,
[MatterLastReferenceInteger] [int] NULL,
[MatterReferenceValueFormatID] [int] NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[CalculateAllMatters] [bit] NULL,
[SmsGatewayID] [int] NULL
)
GO
ALTER TABLE [dbo].[LeadType] ADD CONSTRAINT [PK_MatterType] PRIMARY KEY CLUSTERED  ([LeadTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadType_SourceID] ON [dbo].[LeadType] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[LeadType] ADD CONSTRAINT [FK_LeadType_ReferenceValueFormat] FOREIGN KEY ([ReferenceValueFormatID]) REFERENCES [dbo].[ReferenceValueFormat] ([ReferenceValueFormatID])
GO
ALTER TABLE [dbo].[LeadType] ADD CONSTRAINT [FK_MatterType_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
