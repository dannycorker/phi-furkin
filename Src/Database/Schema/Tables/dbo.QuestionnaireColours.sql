CREATE TABLE [dbo].[QuestionnaireColours]
(
[QuestionnaireColourID] [int] NOT NULL IDENTITY(1, 1),
[ClientQuestionnaireID] [int] NOT NULL,
[Page] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Header] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Intro] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Body] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Footer] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[QuestionnaireColours] ADD CONSTRAINT [PK_QuestionnaireColours] PRIMARY KEY CLUSTERED  ([QuestionnaireColourID])
GO
ALTER TABLE [dbo].[QuestionnaireColours] ADD CONSTRAINT [FK_QuestionnaireColours_ClientQuestionnaires] FOREIGN KEY ([ClientQuestionnaireID]) REFERENCES [dbo].[ClientQuestionnaires] ([ClientQuestionnaireID])
GO
ALTER TABLE [dbo].[QuestionnaireColours] ADD CONSTRAINT [FK_QuestionnaireColours_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
