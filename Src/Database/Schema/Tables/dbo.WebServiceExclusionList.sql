CREATE TABLE [dbo].[WebServiceExclusionList]
(
[WebServiceExclusionListID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[WebServiceExclusionList] ADD CONSTRAINT [PK_WebServiceExclusionList] PRIMARY KEY CLUSTERED  ([WebServiceExclusionListID])
GO
