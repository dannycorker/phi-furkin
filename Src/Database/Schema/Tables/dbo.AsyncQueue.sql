CREATE TABLE [dbo].[AsyncQueue]
(
[AsyncQueueID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[QueueTypeID] [int] NOT NULL,
[Status] [int] NOT NULL,
[Payload] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[Outcome] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[WhenCompleted] [datetime] NULL
)
GO
ALTER TABLE [dbo].[AsyncQueue] ADD CONSTRAINT [PK_AsyncQueue] PRIMARY KEY CLUSTERED  ([AsyncQueueID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientID] ON [dbo].[AsyncQueue] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_QueueTypeID] ON [dbo].[AsyncQueue] ([QueueTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_Status] ON [dbo].[AsyncQueue] ([Status])
GO
