CREATE TABLE [dbo].[Module]
(
[ModuleID] [int] NOT NULL,
[ModuleName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Module] ADD CONSTRAINT [PK_Module] PRIMARY KEY CLUSTERED  ([ModuleID])
GO
