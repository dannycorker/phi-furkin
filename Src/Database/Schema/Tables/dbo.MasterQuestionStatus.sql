CREATE TABLE [dbo].[MasterQuestionStatus]
(
[MasterQuestionStatusID] [int] NOT NULL,
[MasterQuestionStatusName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[MasterQuestonStatusDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[MasterQuestionStatus] ADD CONSTRAINT [PK_MasterQuestionStatus] PRIMARY KEY CLUSTERED  ([MasterQuestionStatusID])
GO
