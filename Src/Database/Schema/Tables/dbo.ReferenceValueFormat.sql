CREATE TABLE [dbo].[ReferenceValueFormat]
(
[ReferenceValueFormatID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ReferenceValueName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ReferenceValueDescription] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[IntegerSectionSize] [int] NULL,
[IntegerStartPoint] [int] NULL,
[DateFormat] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[YearFormat] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DelimiterID] [int] NULL,
[Section1TypeID] [int] NULL,
[Section1DetailFieldID] [int] NULL,
[Section2TypeID] [int] NULL,
[Section2DetailFieldID] [int] NULL,
[Section3TypeID] [int] NULL,
[Section3DetailFieldID] [int] NULL,
[Section4TypeID] [int] NULL,
[Section4DetailFieldID] [int] NULL,
[Section5TypeID] [int] NULL,
[Section5DetailFieldID] [int] NULL,
[LeftPadIntegerSection] [bit] NULL,
[IncludeBlankSections] [bit] NULL,
[EnforceUniqueness] [bit] NULL,
[SourceID] [int] NULL
)
GO
ALTER TABLE [dbo].[ReferenceValueFormat] ADD CONSTRAINT [PK_ReferenceValueFormat] PRIMARY KEY CLUSTERED  ([ReferenceValueFormatID])
GO
ALTER TABLE [dbo].[ReferenceValueFormat] ADD CONSTRAINT [FK_ReferenceValueFormat_DetailFields] FOREIGN KEY ([Section1DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[ReferenceValueFormat] ADD CONSTRAINT [FK_ReferenceValueFormat_DetailFields2] FOREIGN KEY ([Section2DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[ReferenceValueFormat] ADD CONSTRAINT [FK_ReferenceValueFormat_DetailFields3] FOREIGN KEY ([Section3DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[ReferenceValueFormat] ADD CONSTRAINT [FK_ReferenceValueFormat_DetailFields4] FOREIGN KEY ([Section4DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[ReferenceValueFormat] ADD CONSTRAINT [FK_ReferenceValueFormat_DetailFields5] FOREIGN KEY ([Section5DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[ReferenceValueFormat] ADD CONSTRAINT [FK_ReferenceValueFormat_ReferenceValueSectionTypes] FOREIGN KEY ([Section1TypeID]) REFERENCES [dbo].[ReferenceValueSectionTypes] ([ReferenceValueSectionTypeID])
GO
ALTER TABLE [dbo].[ReferenceValueFormat] ADD CONSTRAINT [FK_ReferenceValueFormat_ReferenceValueSectionTypes2] FOREIGN KEY ([Section2TypeID]) REFERENCES [dbo].[ReferenceValueSectionTypes] ([ReferenceValueSectionTypeID])
GO
ALTER TABLE [dbo].[ReferenceValueFormat] ADD CONSTRAINT [FK_ReferenceValueFormat_ReferenceValueSectionTypes3] FOREIGN KEY ([Section3TypeID]) REFERENCES [dbo].[ReferenceValueSectionTypes] ([ReferenceValueSectionTypeID])
GO
ALTER TABLE [dbo].[ReferenceValueFormat] ADD CONSTRAINT [FK_ReferenceValueFormat_ReferenceValueSectionTypes4] FOREIGN KEY ([Section4TypeID]) REFERENCES [dbo].[ReferenceValueSectionTypes] ([ReferenceValueSectionTypeID])
GO
ALTER TABLE [dbo].[ReferenceValueFormat] ADD CONSTRAINT [FK_ReferenceValueFormat_ReferenceValueSectionTypes5] FOREIGN KEY ([Section5TypeID]) REFERENCES [dbo].[ReferenceValueSectionTypes] ([ReferenceValueSectionTypeID])
GO
