CREATE TABLE [dbo].[ActiveSession]
(
[SessionID] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[EmailAddress] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[ThirdPartySystemId] [int] NOT NULL CONSTRAINT [DF__ActiveSes__Third__34E8D562] DEFAULT ((0))
)
GO
ALTER TABLE [dbo].[ActiveSession] ADD CONSTRAINT [PK_ActiveSession] PRIMARY KEY CLUSTERED  ([EmailAddress], [ThirdPartySystemId])
GO
ALTER TABLE [dbo].[ActiveSession] ADD CONSTRAINT [FK_ActiveSession_ThirdPartySystem] FOREIGN KEY ([ThirdPartySystemId]) REFERENCES [dbo].[ThirdPartySystem] ([ThirdPartySystemId])
GO
