CREATE TABLE [dbo].[TemplateType]
(
[TemplateTypeID] [int] NOT NULL IDENTITY(1, 1),
[TemplateTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[TemplateType] ADD CONSTRAINT [PK_TemplateType] PRIMARY KEY CLUSTERED  ([TemplateTypeID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TemplateType] ON [dbo].[TemplateType] ([TemplateTypeName])
GO
