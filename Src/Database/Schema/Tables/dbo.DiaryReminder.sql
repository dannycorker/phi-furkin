CREATE TABLE [dbo].[DiaryReminder]
(
[DiaryReminderID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DiaryAppointmentID] [int] NOT NULL,
[ClientPersonnelID] [int] NOT NULL,
[ReminderTimeshiftID] [int] NOT NULL,
[ReminderDate] [datetime] NULL
)
GO
ALTER TABLE [dbo].[DiaryReminder] ADD CONSTRAINT [PK_DiaryReminder] PRIMARY KEY CLUSTERED  ([DiaryReminderID])
GO
CREATE NONCLUSTERED INDEX [IX_DiaryReminder_ClientPersonnel] ON [dbo].[DiaryReminder] ([ClientPersonnelID], [DiaryAppointmentID])
GO
ALTER TABLE [dbo].[DiaryReminder] ADD CONSTRAINT [FK_DiaryReminder_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[DiaryReminder] ADD CONSTRAINT [FK_DiaryReminder_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DiaryReminder] ADD CONSTRAINT [FK_DiaryReminder_DiaryAppointment] FOREIGN KEY ([DiaryAppointmentID]) REFERENCES [dbo].[DiaryAppointment] ([DiaryAppointmentID])
GO
ALTER TABLE [dbo].[DiaryReminder] ADD CONSTRAINT [FK_DiaryReminder_ReminderTimeshift] FOREIGN KEY ([ReminderTimeshiftID]) REFERENCES [dbo].[ReminderTimeshift] ([ReminderTimeshiftID])
GO
