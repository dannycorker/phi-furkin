CREATE TABLE [dbo].[ClientTypes]
(
[ClientTypeID] [int] NOT NULL IDENTITY(1, 1),
[ClientTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ClientTypes] ADD CONSTRAINT [PK_ClientTypes] PRIMARY KEY CLUSTERED  ([ClientTypeID])
GO
