CREATE TABLE [dbo].[LeadEventCallbacks]
(
[LeadEventCallbackID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadEventID] [int] NOT NULL,
[ProviderID] [int] NOT NULL,
[CallbackTypeID] [int] NOT NULL,
[EventDateTime] [datetime] NOT NULL,
[IpAddress] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[Url] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[SentTo] [varchar] (150) COLLATE Latin1_General_CI_AS NULL,
[WhenCreated] [datetime] NOT NULL CONSTRAINT [DF_LeadEventCallbacks_WhenCreated] DEFAULT ([dbo].[fn_GetDate_Local]())
)
GO
ALTER TABLE [dbo].[LeadEventCallbacks] ADD CONSTRAINT [PK_LeadEventCallbacks] PRIMARY KEY CLUSTERED  ([LeadEventCallbackID])
GO
