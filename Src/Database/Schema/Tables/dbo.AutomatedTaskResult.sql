CREATE TABLE [dbo].[AutomatedTaskResult]
(
[AutomatedTaskResultID] [int] NOT NULL IDENTITY(1, 1),
[TaskID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[RunDate] [datetime] NOT NULL,
[Description] [varchar] (2500) COLLATE Latin1_General_CI_AS NOT NULL,
[RecordCount] [int] NOT NULL,
[Complete] [bit] NOT NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-01-30
-- Description:	Check for new batch jobs that might affect bath jobs
-- =============================================
CREATE TRIGGER [dbo].[trgiu_AutomatedTaskResult]
   ON [dbo].[AutomatedTaskResult]
   AFTER INSERT
AS
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END

	DECLARE @InsertingNewRecords bit,
			@PostUpdateSQL nvarchar(2000),
			@ClientID int,
			@TaskID int,
			@RecordCount int,
			@Complete bit,
			@TaskIDToTrigger int,
			@ErrorMessage VARCHAR(2000),
			@Params nvarchar(200),
			@TriggerIfIncomplete bit,
			@TriggerOnZeroRecords bit


	DECLARE @AutomatedTaskResult TABLE (TaskID int, ClientID int, RecordCount int, Complete bit)

	/*
		For each record, we need to know whether it is a new insert, or an update to Normally
		only 1 record will be affected at a time anyway, but this covers all DML statements too.
	*/

	IF UPDATE(TaskID)
	BEGIN
		SELECT @InsertingNewRecords = 1
	END
	ELSE
	BEGIN
		SELECT @InsertingNewRecords = 0
	END

	INSERT INTO @AutomatedTaskResult (TaskID, ClientID, RecordCount, Complete)
	SELECT i.TaskID, i.ClientID, i.RecordCount, i.Complete
	FROM inserted i

	SELECT TOP 1 @TaskID = t.TaskID,
				@ClientID = t.ClientID,
				@RecordCount = t.RecordCount,
				@Complete = t.Complete
	FROM @AutomatedTaskResult t

	WHILE @TaskID > 0
	BEGIN

		SELECT @TaskIDToTrigger = ats.TaskIDToTrigger, @TriggerIfIncomplete = ats.TriggerIfIncomplete, @TriggerOnZeroRecords = ats.TriggerOnZeroRecords
		FROM AutomatedTaskTrigger ats WITH (NOLOCK)
		WHERE ats.AutomatedTaskID = @TaskID

		IF @@ROWCOUNT = 1 AND @TaskIDToTrigger > 0 AND ((@Complete = 1) OR (@Complete = 0 AND @TriggerIfIncomplete = 1)) AND ((@RecordCount > 0) OR (@RecordCount = 0 AND @TriggerOnZeroRecords = 1))
		BEGIN

			UPDATE AutomatedTask
			SET NextRunDateTime = dbo.fn_GetDate_Local()
			FROM AutomatedTask
			WHERE TaskID = @TaskIDToTrigger

		END

		-- Now loop round for the next task. Normally there will not be one, but if
		-- someone has updated a set of tasks in one update statement.
		DELETE FROM @AutomatedTaskResult
		WHERE TaskID = @TaskID

		SELECT TOP 1 @TaskID = t.TaskID,
				@ClientID = t.ClientID,
				@RecordCount = t.RecordCount,
				@Complete = t.Complete
		FROM @AutomatedTaskResult t

		IF @@ROWCOUNT < 1
		BEGIN
			SELECT @TaskID = -1
		END

	END

END
GO
ALTER TABLE [dbo].[AutomatedTaskResult] ADD CONSTRAINT [PK_AutomatedTaskResult] PRIMARY KEY CLUSTERED  ([AutomatedTaskResultID])
GO
CREATE NONCLUSTERED INDEX [IX_AutomatedTaskResult_RundateClientTask] ON [dbo].[AutomatedTaskResult] ([RunDate], [ClientID], [TaskID])
GO
ALTER TABLE [dbo].[AutomatedTaskResult] ADD CONSTRAINT [FK_AutomatedTaskResult_AutomatedTask] FOREIGN KEY ([TaskID]) REFERENCES [dbo].[AutomatedTask] ([TaskID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[AutomatedTaskResult] ADD CONSTRAINT [FK_AutomatedTaskResult_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
