CREATE TABLE [dbo].[MasterQuestions]
(
[MasterQuestionID] [int] NOT NULL IDENTITY(1, 1),
[ClientQuestionnaireID] [int] NULL,
[QuestionTypeID] [int] NOT NULL,
[QuestionText] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[QuestionOrder] [int] NULL,
[DefaultAnswerID] [int] NULL,
[Mandatory] [bit] NULL,
[QuestionToolTip] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[LinkedQuestionnaireMasterQuestionID] [int] NULL,
[Active] [bit] NULL,
[MasterQuestionStatus] [int] NULL,
[TextboxHeight] [int] NULL,
[ClientID] [int] NOT NULL,
[AnswerPosition] [int] NULL,
[DisplayAnswerAs] [int] NULL,
[NumberOfAnswersPerRow] [int] NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[QuestionPossibleAnswerSortOrder] [int] NULL
)
GO
ALTER TABLE [dbo].[MasterQuestions] ADD CONSTRAINT [PK_MasterQuestions] PRIMARY KEY CLUSTERED  ([MasterQuestionID])
GO
CREATE NONCLUSTERED INDEX [IX_MasterQuestionsClient] ON [dbo].[MasterQuestions] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_MasterQuestionsClientQuestionnaire] ON [dbo].[MasterQuestions] ([ClientQuestionnaireID])
GO
CREATE NONCLUSTERED INDEX [IX_MasterQuestionsQuestionType] ON [dbo].[MasterQuestions] ([QuestionTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_MasterQuestions_SourceID] ON [dbo].[MasterQuestions] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[MasterQuestions] ADD CONSTRAINT [FK_MasterQuestions_ClientQuestionares] FOREIGN KEY ([ClientQuestionnaireID]) REFERENCES [dbo].[ClientQuestionnaires] ([ClientQuestionnaireID]) ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[MasterQuestions] ADD CONSTRAINT [FK_MasterQuestions_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[MasterQuestions] ADD CONSTRAINT [FK_MasterQuestions_QuestionTypes] FOREIGN KEY ([QuestionTypeID]) REFERENCES [dbo].[QuestionTypes] ([QuestionTypeID])
GO
