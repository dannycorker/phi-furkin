CREATE TABLE [dbo].[Chart]
(
[ChartID] [int] NOT NULL IDENTITY(1, 1),
[QueryID] [int] NOT NULL,
[ClientID] [int] NULL,
[ChartTitle] [varchar] (150) COLLATE Latin1_General_CI_AS NULL,
[ChartDescription] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ChartTypeID] [int] NOT NULL,
[XAxisColumn] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[CreatedBy] [int] NULL,
[CreatedOn] [datetime] NULL,
[LastEditedBy] [int] NULL,
[LastEditedOn] [datetime] NULL
)
GO
ALTER TABLE [dbo].[Chart] ADD CONSTRAINT [PK_Chart] PRIMARY KEY CLUSTERED  ([ChartID])
GO
ALTER TABLE [dbo].[Chart] ADD CONSTRAINT [FK_Chart_ChartType] FOREIGN KEY ([ChartTypeID]) REFERENCES [dbo].[ChartType] ([ChartTypeID])
GO
ALTER TABLE [dbo].[Chart] ADD CONSTRAINT [FK_Chart_ClientPersonnel] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[Chart] ADD CONSTRAINT [FK_Chart_ClientPersonnel1] FOREIGN KEY ([LastEditedBy]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[Chart] ADD CONSTRAINT [FK_Chart_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
