CREATE TABLE [dbo].[MatterDetailValues]
(
[MatterDetailValueID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[MatterID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[DetailValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[ErrorMsg] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[OriginalDetailValueID] [int] NULL,
[OriginalLeadID] [int] NULL,
[EncryptedValue] [varchar] (3000) COLLATE Latin1_General_CI_AS NULL,
[ValueInt] [int] NULL,
[ValueMoney] [money] NULL,
[ValueDate] [date] NULL,
[ValueDateTime] [datetime2] (0) NULL,
[SourceID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-27
-- Description:	Store DetailValue as specific datatypes
-- 2011-07-13 JWG Remove duplicates instantly at source
-- =============================================
CREATE TRIGGER [dbo].[trgiu_MatterDetailValues]
   ON [dbo].[MatterDetailValues]
   AFTER INSERT, UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END


    /*
		If the DetailValue column has been updated then fire the trigger,
		otherwise we are here again because the trigger has just fired,
		in which case do nothing.
	*/
    IF UPDATE(DetailValue)
    BEGIN
		/* Check for duplicates (but only on insert, not update) */
		IF NOT EXISTS (SELECT * FROM deleted)
		BEGIN
			/* Only issue the delete if we know that any records exist, otherwise the delete operation gets attempted every time regardless */
			IF EXISTS(SELECT * FROM dbo.MatterDetailValues dv WITH (NOLOCK) INNER JOIN inserted i ON i.MatterID = dv.MatterID AND i.DetailFieldID = dv.DetailFieldID AND i.MatterDetailValueID > dv.MatterDetailValueID)
			BEGIN
				/* Remove any older duplicates for this field */
				DELETE dbo.MatterDetailValues
				OUTPUT 2, deleted.MatterDetailValueID, deleted.MatterID, deleted.DetailValue, deleted.DetailFieldID
				INTO dbo.__DeletedValue (DetailFieldSubTypeID, DetailValueID, ParentID, DetailValue, DetailFieldID)
				FROM dbo.MatterDetailValues dv
				INNER JOIN inserted i ON i.MatterID = dv.MatterID AND i.DetailFieldID = dv.DetailFieldID AND i.MatterDetailValueID > dv.MatterDetailValueID
			END
		END

		/* Populate ValueInt et al */
		UPDATE dbo.MatterDetailValues
		SET ValueInt = dbo.fnValueAsIntBitsIncluded(i.DetailValue) /*CASE WHEN dbo.fnIsInt(i.detailvalue) = 1 THEN convert(int, i.DetailValue) ELSE NULL END*/,
		ValueMoney = CASE WHEN dbo.fnIsMoney(i.DetailValue) = 1 THEN convert(money, i.DetailValue) ELSE NULL END,
		ValueDate = CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN convert(date, i.DetailValue) ELSE NULL END,
		ValueDateTime = CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN convert(datetime2, i.DetailValue) ELSE NULL END
		FROM dbo.MatterDetailValues dv
		INNER JOIN inserted i ON i.MatterDetailValueID = dv.MatterDetailValueID
	END


END

GO
ALTER TABLE [dbo].[MatterDetailValues] ADD CONSTRAINT [PK_MatterDetailValues] PRIMARY KEY CLUSTERED  ([MatterDetailValueID])
GO
CREATE NONCLUSTERED INDEX [IX_MatterDetailValues_Client] ON [dbo].[MatterDetailValues] ([ClientID]) INCLUDE ([MatterID])
GO
CREATE NONCLUSTERED INDEX [IX_MatterDetailValues_DetailField] ON [dbo].[MatterDetailValues] ([DetailFieldID]) INCLUDE ([MatterID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_MatterDetailValues_FieldValueDate] ON [dbo].[MatterDetailValues] ([DetailFieldID], [ValueDate], [LeadID], [ClientID]) INCLUDE ([MatterID]) WHERE ([ValueDate] IS NOT NULL)
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_MatterDetailValues_FieldValueInt2017] ON [dbo].[MatterDetailValues] ([DetailFieldID], [ValueInt], [ClientID]) INCLUDE ([MatterID]) WHERE ([ValueInt] IS NOT NULL)
GO
CREATE NONCLUSTERED INDEX [IX_MatterDetailValues_Lead] ON [dbo].[MatterDetailValues] ([LeadID])
GO
CREATE NONCLUSTERED INDEX [IX_MatterDetailValues_MatterAndField2017] ON [dbo].[MatterDetailValues] ([MatterID], [DetailFieldID], [LeadID], [ClientID]) INCLUDE ([ValueDate], [ValueInt], [ValueMoney])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_MatterDetailValues_MatterFieldValueInt] ON [dbo].[MatterDetailValues] ([MatterID], [DetailFieldID], [ValueInt], [LeadID], [ClientID]) WHERE ([ValueInt] IS NOT NULL)
GO
ALTER TABLE [dbo].[MatterDetailValues] ADD CONSTRAINT [FK_MatterDetailValues_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID])
GO
ALTER TABLE [dbo].[MatterDetailValues] ADD CONSTRAINT [FK_MatterDetailValues_Matter] FOREIGN KEY ([MatterID]) REFERENCES [dbo].[Matter] ([MatterID]) ON DELETE CASCADE
GO
