CREATE TABLE [dbo].[QuotePet]
(
[QuotePetID] [int] NOT NULL IDENTITY(1, 1),
[QuoteID] [int] NULL,
[QuoteSessionID] [int] NULL,
[MatterID] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoCreated] [int] NULL,
[SourceID] [int] NULL,
[ChangeNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[PetName] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ProductCount] [int] NULL,
[Risk_Postcode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[Risk_TermsDate] [date] NULL,
[Risk_StartDate] [date] NULL,
[Risk_EndDate] [date] NULL,
[Risk_AggressiveTendencies] [bit] NULL,
[Risk_Breed] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Risk_DateOfBirth] [date] NULL,
[Risk_Channel] [int] NULL,
[Risk_Sex] [int] NULL,
[Risk_IsNeutered] [bit] NULL,
[Risk_IsRescue] [bit] NULL,
[Risk_Species] [int] NULL,
[Risk_Value] [decimal] (18, 2) NULL,
[Risk_Brand] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Risk_CampaignCode] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Risk_NewBusiness] [bit] NULL,
[OverridesXml] [xml] NULL,
[LeadEventID] [int] NULL,
[BreedResourceListID] [int] NULL,
[CustomerDoB] [date] NULL,
[Risk_PreExistingConditions] [bit] NULL,
[Risk_HasMicrochip] [bit] NULL,
[Risk_ExistingCustomerID] [int] NULL
)
GO
ALTER TABLE [dbo].[QuotePet] ADD CONSTRAINT [PK_QuotePet] PRIMARY KEY CLUSTERED  ([QuotePetID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Quote_QuoteID_QuoteSessionID_QuotePetID] ON [dbo].[QuotePet] ([QuoteID], [QuoteSessionID], [QuotePetID])
GO
ALTER TABLE [dbo].[QuotePet] ADD CONSTRAINT [FK_QuotePet_Quote] FOREIGN KEY ([QuoteID]) REFERENCES [dbo].[Quote] ([QuoteID]) ON DELETE CASCADE
GO
