CREATE TABLE [dbo].[CalcsEngine_CalculationActions]
(
[CalculationActionID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[CalculationID] [int] NOT NULL,
[ActionTypeID] [int] NOT NULL,
[ParentID] [int] NULL,
[Context] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Parameter] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[WhenCreated] [datetime] NULL,
[WhoCreated] [int] NULL,
[WhenModified] [datetime] NULL,
[WhoModified] [int] NULL,
[Description] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL,
[ActionOrder] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[CalcsEngine_CalculationActions] ADD CONSTRAINT [PK_CalcsEngine_CalculationActions] PRIMARY KEY CLUSTERED  ([CalculationActionID])
GO
ALTER TABLE [dbo].[CalcsEngine_CalculationActions] ADD CONSTRAINT [FK_CalcsEngine_CalculationActions_CalcsEngine_ActionTypes] FOREIGN KEY ([ActionTypeID]) REFERENCES [dbo].[CalcsEngine_ActionTypes] ([ActionTypeID])
GO
ALTER TABLE [dbo].[CalcsEngine_CalculationActions] ADD CONSTRAINT [FK_CalcsEngine_CalculationActions_CalcsEngine_CalculationActions] FOREIGN KEY ([ParentID]) REFERENCES [dbo].[CalcsEngine_CalculationActions] ([CalculationActionID])
GO
ALTER TABLE [dbo].[CalcsEngine_CalculationActions] ADD CONSTRAINT [FK_CalcsEngine_CalculationActions_CalcsEngine_Calculations] FOREIGN KEY ([CalculationID]) REFERENCES [dbo].[CalcsEngine_Calculations] ([CalculationID])
GO
