CREATE TABLE [dbo].[NotificationType]
(
[NotificationTypeID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[BackgroundColour] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[ForeColour] [varchar] (20) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[NotificationType] ADD CONSTRAINT [PK_NotificationType] PRIMARY KEY CLUSTERED  ([NotificationTypeID])
GO
ALTER TABLE [dbo].[NotificationType] ADD CONSTRAINT [FK_NotificationType_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[NotificationType] ADD CONSTRAINT [FK_NotificationType_SubClient] FOREIGN KEY ([SubClientID]) REFERENCES [dbo].[SubClient] ([SubClientID])
GO
