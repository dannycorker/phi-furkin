CREATE TABLE [dbo].[NotificationStatus]
(
[NotificationStatusID] [int] NOT NULL IDENTITY(1, 1),
[Status] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[NotificationStatus] ADD CONSTRAINT [PK_NotificationStatus] PRIMARY KEY CLUSTERED  ([NotificationStatusID])
GO
