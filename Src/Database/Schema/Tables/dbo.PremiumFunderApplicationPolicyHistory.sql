CREATE TABLE [dbo].[PremiumFunderApplicationPolicyHistory]
(
[PremiumFunderApplicationPolicyHistoryID] [int] NOT NULL IDENTITY(1, 1),
[PremiumFunderApplicationPolicyID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[PAMatterID] [int] NULL,
[PremiumFunderRequestID] [int] NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenArchived] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[PremiumFunderApplicationPolicyHistory] ADD CONSTRAINT [PK_PremiumFunderApplicationPolicyHistory] PRIMARY KEY CLUSTERED  ([PremiumFunderApplicationPolicyHistoryID])
GO
