CREATE TABLE [dbo].[ChangeControlBranch]
(
[UserBranchID] [int] NOT NULL IDENTITY(1, 1),
[UserName] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[BranchName] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[CreatedDateTime] [datetime] NOT NULL
)
GO
