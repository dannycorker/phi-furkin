CREATE TABLE [dbo].[AquariumHelperObject]
(
[AquariumHelperObjectID] [int] NOT NULL IDENTITY(1, 1),
[MenuID] [int] NOT NULL,
[SubmenuID] [int] NOT NULL,
[ProcName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Notes] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[IsExecutable] [bit] NOT NULL,
[HasIntParam] [bit] NOT NULL,
[IntParamName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[NeedsIntParam] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[AquariumHelperObject] ADD CONSTRAINT [PK_AquariumHelperObject] PRIMARY KEY NONCLUSTERED  ([AquariumHelperObjectID])
GO
CREATE UNIQUE CLUSTERED INDEX [IX_AquariumHelperObject] ON [dbo].[AquariumHelperObject] ([MenuID], [SubmenuID])
GO
