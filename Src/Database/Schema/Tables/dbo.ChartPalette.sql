CREATE TABLE [dbo].[ChartPalette]
(
[ChartPaletteID] [int] NOT NULL,
[ChartPaletteName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ChartPalettePreviewImageUrl] [varchar] (255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ChartPalette] ADD CONSTRAINT [PK_ChartPalette] PRIMARY KEY CLUSTERED  ([ChartPaletteID])
GO
