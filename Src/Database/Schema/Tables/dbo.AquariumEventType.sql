CREATE TABLE [dbo].[AquariumEventType]
(
[AquariumEventTypeID] [int] NOT NULL,
[AquariumEventTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[AquariumEventTypeDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[AquariumStatusAfterEvent] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[AquariumEventType] ADD CONSTRAINT [PK_AquariumEventType] PRIMARY KEY CLUSTERED  ([AquariumEventTypeID])
GO
