CREATE TABLE [dbo].[CardTransactionLeadEventLink]
(
[CardTransactionLeadEventID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CardTransactionID] [int] NULL,
[LeadEventID] [int] NULL,
[WorldPayRequestID] [int] NULL,
[WorldPayResponseID] [int] NULL,
[WorldPayPaymentUrl] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CardTransactionLeadEventLink] ADD CONSTRAINT [PK_CardTransactionLeadEventLink] PRIMARY KEY CLUSTERED  ([CardTransactionLeadEventID])
GO
