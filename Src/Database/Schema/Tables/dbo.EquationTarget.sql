CREATE TABLE [dbo].[EquationTarget]
(
[EquationTargetID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EquationDetailFieldID] [int] NOT NULL,
[DetailFieldID] [int] NULL,
[Target] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ObjectName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[PropertyName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[SqlFunctionID] [int] NULL,
[IsEquation] [bit] NULL,
[IsColumnSum] [bit] NULL,
[ColumnFieldID] [int] NULL,
[DetailFieldSubTypeID] [int] NULL
)
GO
ALTER TABLE [dbo].[EquationTarget] ADD CONSTRAINT [PK_EquationTarget] PRIMARY KEY CLUSTERED  ([EquationTargetID])
GO
CREATE NONCLUSTERED INDEX [IX_EquationTarget] ON [dbo].[EquationTarget] ([DetailFieldID])
GO
CREATE NONCLUSTERED INDEX [IX_EquationDetailFieldID] ON [dbo].[EquationTarget] ([EquationDetailFieldID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_ObjectName] ON [dbo].[EquationTarget] ([ObjectName])
GO
ALTER TABLE [dbo].[EquationTarget] ADD CONSTRAINT [FK_EquationTarget_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EquationTarget] ADD CONSTRAINT [FK_EquationTarget_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
