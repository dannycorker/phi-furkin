CREATE TABLE [dbo].[_C600_QuoteValues]
(
[QuoteValueID] [int] NOT NULL IDENTITY(1, 1),
[QuoteSessionID] [int] NOT NULL,
[QuoteDate] [datetime] NOT NULL,
[FirstName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Email] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[PostCode] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[PetName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[BreedID] [int] NOT NULL,
[PetDoB] [date] NOT NULL,
[QuoteXml] [xml] NOT NULL,
[PetXml] [xml] NOT NULL,
[ResponseXml] [xml] NULL,
[HomeTel] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MobileTel] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Breed] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[SpeciesID] [int] NULL,
[Species] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Gender] [varchar] (1) COLLATE Latin1_General_CI_AS NULL,
[IsNeutered] [varchar] (1) COLLATE Latin1_General_CI_AS NULL,
[PetPrice] [money] NULL,
[VolExcess] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[QuoteEnd] [datetime] NULL
)
GO
ALTER TABLE [dbo].[_C600_QuoteValues] ADD CONSTRAINT [PK__C327_QuoteValues] PRIMARY KEY CLUSTERED  ([QuoteValueID])
GO
CREATE NONCLUSTERED INDEX [IX__C600_QuoteValues_QuoteSessionID] ON [dbo].[_C600_QuoteValues] ([QuoteSessionID])
GO
ALTER TABLE [dbo].[_C600_QuoteValues] ADD CONSTRAINT [FK__C327_QuoteValues__C327_QuoteValues] FOREIGN KEY ([QuoteSessionID]) REFERENCES [dbo].[_C600_QuoteSessions] ([QuoteSessionID])
GO
