CREATE TABLE [dbo].[WorkWinsFirstValidSave]
(
[WorkWinsFirstValidSave] [int] NOT NULL IDENTITY(1, 1),
[WorkWinSeedID] [int] NOT NULL,
[MatterID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[WhenSaved] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[WorkWinsFirstValidSave] ADD CONSTRAINT [PK_WorkWinsFirstValidSave] PRIMARY KEY CLUSTERED  ([WorkWinsFirstValidSave])
GO
CREATE NONCLUSTERED INDEX [IX_WorkWinsFirstValidSave] ON [dbo].[WorkWinsFirstValidSave] ([WorkWinSeedID], [MatterID], [DetailFieldID], [WhenSaved])
GO
ALTER TABLE [dbo].[WorkWinsFirstValidSave] ADD CONSTRAINT [FK_WorkWinsFirstValidSave_WorkWinSeed] FOREIGN KEY ([WorkWinSeedID]) REFERENCES [dbo].[WorkWinSeed] ([WorkWinSeedID]) ON DELETE CASCADE
GO
