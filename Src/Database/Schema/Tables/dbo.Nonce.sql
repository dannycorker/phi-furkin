CREATE TABLE [dbo].[Nonce]
(
[NonceValue] [tinyint] NOT NULL
)
GO
ALTER TABLE [dbo].[Nonce] ADD CONSTRAINT [PK_Nonce] PRIMARY KEY CLUSTERED  ([NonceValue])
GO
