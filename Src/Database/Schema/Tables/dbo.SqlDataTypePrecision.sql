CREATE TABLE [dbo].[SqlDataTypePrecision]
(
[SqlDataTypePrecisionID] [int] NOT NULL,
[SqlDataTypeID] [int] NOT NULL,
[PrecisionLowerBound] [int] NOT NULL,
[PrecisionUpperBound] [int] NOT NULL,
[PreciseLengthInBytes] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[SqlDataTypePrecision] ADD CONSTRAINT [PK_SqlDataTypePrecision] PRIMARY KEY CLUSTERED  ([SqlDataTypePrecisionID])
GO
