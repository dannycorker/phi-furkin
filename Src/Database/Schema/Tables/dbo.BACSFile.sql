CREATE TABLE [dbo].[BACSFile]
(
[BACSFileID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[BACSFileFormatID] [int] NULL,
[FileName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ExportLocation] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[NumberOfRowsInFile] [int] NULL,
[TotalCreditValue] [numeric] (18, 2) NULL,
[TotalDebitValue] [numeric] (18, 2) NULL,
[TotalValue] [numeric] (18, 2) NULL,
[ExportDate] [datetime] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[BACSFile] ADD CONSTRAINT [PK_BACSFile] PRIMARY KEY CLUSTERED  ([BACSFileID])
GO
