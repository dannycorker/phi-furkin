CREATE TABLE [dbo].[DataLoaderFileStatus]
(
[DataLoaderFileStatusID] [int] NOT NULL,
[FileStatusName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[FileStatusDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[DataLoaderFileStatus] ADD CONSTRAINT [PK_DataLoaderFileStatus] PRIMARY KEY CLUSTERED  ([DataLoaderFileStatusID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DataLoaderFileStatusName] ON [dbo].[DataLoaderFileStatus] ([FileStatusName])
GO
