CREATE TABLE [dbo].[DocumentType]
(
[DocumentTypeID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NULL,
[DocumentTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DocumentTypeDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Header] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Template] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Footer] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[CanBeAutoSent] [bit] NOT NULL,
[EmailSubject] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[EmailBodyText] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[InputFormat] [varchar] (24) COLLATE Latin1_General_CI_AS NULL,
[OutputFormat] [varchar] (24) COLLATE Latin1_General_CI_AS NULL,
[Enabled] [bit] NOT NULL,
[RecipientsTo] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[RecipientsCC] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[RecipientsBCC] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ReadOnlyTo] [bit] NOT NULL CONSTRAINT [DF__DocumentT__ReadO__5DEAEAF5] DEFAULT ((0)),
[ReadOnlyCC] [bit] NOT NULL CONSTRAINT [DF__DocumentT__ReadO__5C02A283] DEFAULT ((0)),
[ReadOnlyBCC] [bit] NOT NULL CONSTRAINT [DF__DocumentT__ReadO__5B0E7E4A] DEFAULT ((0)),
[SendToMultipleRecipients] [bit] NOT NULL,
[MultipleRecipientDataSourceType] [int] NULL,
[MultipleRecipientDataSourceID] [int] NULL,
[SendToAllByDefault] [bit] NOT NULL,
[ExcelTemplatePath] [varchar] (1024) COLLATE Latin1_General_CI_AS NULL,
[FromDetails] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[ReadOnlyFrom] [bit] NOT NULL CONSTRAINT [DF__DocumentT__ReadO__5CF6C6BC] DEFAULT ((1)),
[SourceID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[FolderID] [int] NULL,
[IsThunderheadTemplate] [bit] NULL,
[ThunderheadUniqueTemplateID] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[ThunderheadDocumentFormat] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[DocumentTitleTemplate] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[CreateNewVersionWhenSaved] [bit] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2010-02-18
-- Description:	Capture changes and store in History table
-- 2016-03-29 ACE Updated to include WhoModified #37422
-- =============================================
CREATE TRIGGER [dbo].[trgiud_DocumentType] 
   ON  [dbo].[DocumentType] 
   AFTER INSERT,UPDATE,DELETE 
AS 
BEGIN
	SET NOCOUNT ON;
	
	/*
		Inserts appear in the inserted table, 
		Deletes appear in the deleted table,
		and Updates appear in both tables at once identically.
		These are all 'logical' Sql Server tables.
	    
		We don't have space to store all the varchar(max) fields
		every time a change is made (to any field).  So just note
		whether or not the template etc has changed.
    */
    
	INSERT INTO dbo.DocumentTypeHistory(
		DocumentTypeID,
		ClientID,
		LeadTypeID,
		DocumentTypeName,
		DocumentTypeDescription,
		HeaderChanged,
		TemplateChanged,
		FooterChanged,
		CanBeAutoSent,
		EmailSubject,
		EmailBodyTextChanged,
		InputFormat,
		OutputFormat,
		[Enabled],
		RecipientsTo,
		RecipientsCC,
		RecipientsBCC,
		ReadOnlyTo,
		ReadOnlyCC,
		ReadOnlyBCC,
		SendToMultipleRecipients,
		MultipleRecipientDataSourceType,
		MultipleRecipientDataSourceID,
		SendToAllByDefault,
		WhoChanged,
		WhenChanged, 
		ChangeAction,
		ChangeNotes,
		EmailBodyText,
		ExcelTemplatePath,
		FromDetails,
		ReadOnlyFrom,
		FolderID,
		IsThunderheadTemplate,
		ThunderheadUniqueTemplateID,
		ThunderheadDocumentFormat,
		DocumentTitleTemplate
	)
	SELECT
		COALESCE(i.DocumentTypeID, d.DocumentTypeID),
		COALESCE(i.ClientID, d.ClientID),
		COALESCE(i.LeadTypeID, d.LeadTypeID),
		COALESCE(i.DocumentTypeName, d.DocumentTypeName),
		COALESCE(i.DocumentTypeDescription, d.DocumentTypeDescription),
		CASE WHEN UPDATE(Header) THEN 1 ELSE 0 END,
		CASE WHEN UPDATE(Template) THEN 1 ELSE 0 END,
		CASE WHEN UPDATE(Footer) THEN 1 ELSE 0 END,
		COALESCE(i.CanBeAutoSent, d.CanBeAutoSent),
		COALESCE(i.EmailSubject, d.EmailSubject),
		CASE WHEN UPDATE(EmailBodyText) THEN 1 ELSE 0 END,
		COALESCE(i.InputFormat, d.InputFormat),
		COALESCE(i.OutputFormat, d.OutputFormat),
		COALESCE(i.[Enabled], d.[Enabled]),
		LEFT(COALESCE(i.RecipientsTo, d.RecipientsTo), 500),
		LEFT(COALESCE(i.RecipientsCC, d.RecipientsCC), 500),
		LEFT(COALESCE(i.RecipientsBCC, d.RecipientsBCC), 500),
		COALESCE(i.ReadOnlyTo, d.ReadOnlyTo),
		COALESCE(i.ReadOnlyCC, d.ReadOnlyCC),
		COALESCE(i.ReadOnlyBCC, d.ReadOnlyBCC),
		COALESCE(i.SendToMultipleRecipients, d.SendToMultipleRecipients),
		COALESCE(i.MultipleRecipientDataSourceType, d.MultipleRecipientDataSourceType),
		COALESCE(i.MultipleRecipientDataSourceID, d.MultipleRecipientDataSourceID),
		COALESCE(i.SendToAllByDefault, d.SendToAllByDefault),
		COALESCE(i.WhoModified, d.WhoModified),
		dbo.fn_GetDate_Local(),
		CASE WHEN d.DocumentTypeID IS NULL THEN 'Insert' WHEN i.DocumentTypeID IS NULL THEN 'Delete' ELSE 'Update' END,
		dbo.fnGetUserInfo(),
		COALESCE(i.EmailBodyText, d.EmailBodyText),
		COALESCE(i.ExcelTemplatePath, d.ExcelTemplatePath),
		COALESCE(i.FromDetails, d.FromDetails),
		COALESCE(i.ReadOnlyFrom, d.ReadOnlyFrom),
		COALESCE(i.FolderID, d.FolderID),
		COALESCE(i.IsThunderheadTemplate, d.IsThunderheadTemplate),
		COALESCE(i.ThunderheadUniqueTemplateID, d.ThunderheadUniqueTemplateID),
		COALESCE(i.ThunderheadDocumentFormat, d.ThunderheadDocumentFormat),
		CASE WHEN UPDATE(DocumentTitleTemplate) THEN 1 ELSE 0 END
	FROM inserted i 
	FULL OUTER JOIN deleted d ON i.DocumentTypeID = d.DocumentTypeID 
	
END
GO
ALTER TABLE [dbo].[DocumentType] ADD CONSTRAINT [PK_DocumentType] PRIMARY KEY CLUSTERED  ([DocumentTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_DocumentType1] ON [dbo].[DocumentType] ([ClientID], [LeadTypeID], [Enabled], [CanBeAutoSent])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_DocumentType_SourceID] ON [dbo].[DocumentType] ([SourceID]) WHERE ([SourceID]>(0))
GO
