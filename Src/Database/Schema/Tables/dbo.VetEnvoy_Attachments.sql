CREATE TABLE [dbo].[VetEnvoy_Attachments]
(
[AttachmentID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[AccountID] [int] NOT NULL,
[ConversationID] [uniqueidentifier] NOT NULL,
[Received] [datetime] NOT NULL,
[SenderID] [uniqueidentifier] NOT NULL,
[Bytes] [varbinary] (max) NOT NULL,
[ContentType] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[LeadEventID] [int] NULL
)
GO
ALTER TABLE [dbo].[VetEnvoy_Attachments] ADD CONSTRAINT [PK_VetEnvoy_Attachments] PRIMARY KEY CLUSTERED  ([AttachmentID])
GO
CREATE NONCLUSTERED INDEX [IX_VetEnvoy_Attachments] ON [dbo].[VetEnvoy_Attachments] ([ConversationID])
GO
ALTER TABLE [dbo].[VetEnvoy_Attachments] ADD CONSTRAINT [FK_VetEnvoy_Attachments_VetEnvoy_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[VetEnvoy_Accounts] ([AccountID])
GO
