CREATE TABLE [dbo].[DataLoaderObjectAction]
(
[DataLoaderObjectActionID] [int] NOT NULL,
[DataLoaderObjectActionName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[DataLoaderObjectActionDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[DataLoaderObjectAction] ADD CONSTRAINT [PK_DataLoaderObjectAction] PRIMARY KEY CLUSTERED  ([DataLoaderObjectActionID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DataLoaderObjectActionName] ON [dbo].[DataLoaderObjectAction] ([DataLoaderObjectActionName])
GO
