CREATE TABLE [dbo].[ClientReportTableBuilder]
(
[ClientReportTableBuilderID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[TableName] [varchar] (1024) COLLATE Latin1_General_CI_AS NOT NULL,
[ColumnName] [varchar] (1024) COLLATE Latin1_General_CI_AS NULL,
[LeadTypeID] [int] NOT NULL,
[TableSubtypeID] [int] NOT NULL,
[DetailFieldID] [int] NULL,
[TableDetailFieldPageID] [int] NULL,
[ActionType] [varchar] (1024) COLLATE Latin1_General_CI_AS NOT NULL,
[WhoRequested] [int] NOT NULL,
[WhenRequested] [datetime2] (0) NOT NULL,
[WhenStarted] [datetime2] (0) NULL,
[WhenCompleted] [datetime2] (0) NULL,
[RecordsExpected] [int] NULL,
[RecordsProcessed] [int] NULL,
[RecordsAtATime] [int] NULL,
[MaxRecords] [int] NULL,
[LockType] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[UserNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[SSISNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
