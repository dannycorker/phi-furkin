CREATE TABLE [dbo].[MatterListDisplay]
(
[MatterListDisplayID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[MatterRef] [bit] NULL,
[MatterStatus] [bit] NULL,
[MatterID] [bit] NULL,
[Field1] [int] NULL,
[Field2] [int] NULL,
[Field3] [int] NULL,
[Field4] [int] NULL,
[Field5] [int] NULL,
[Field1ColumnDetailFieldID] [int] NULL,
[Field2ColumnDetailFieldID] [int] NULL,
[Field3ColumnDetailFieldID] [int] NULL,
[Field4ColumnDetailFieldID] [int] NULL,
[Field5ColumnDetailFieldID] [int] NULL,
[SqlQueryText] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[MatterListDisplay] ADD CONSTRAINT [PK_MatterListDisplay] PRIMARY KEY CLUSTERED  ([MatterListDisplayID])
GO
ALTER TABLE [dbo].[MatterListDisplay] ADD CONSTRAINT [FK_MatterListDisplay_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[MatterListDisplay] ADD CONSTRAINT [FK_MatterListDisplay_DetailFields] FOREIGN KEY ([Field1]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[MatterListDisplay] ADD CONSTRAINT [FK_MatterListDisplay_DetailFields1] FOREIGN KEY ([Field2]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[MatterListDisplay] ADD CONSTRAINT [FK_MatterListDisplay_DetailFields2] FOREIGN KEY ([Field3]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[MatterListDisplay] ADD CONSTRAINT [FK_MatterListDisplay_DetailFields3] FOREIGN KEY ([Field4]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[MatterListDisplay] ADD CONSTRAINT [FK_MatterListDisplay_DetailFields4] FOREIGN KEY ([Field5]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[MatterListDisplay] ADD CONSTRAINT [FK_MatterListDisplay_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID]) ON DELETE CASCADE
GO
