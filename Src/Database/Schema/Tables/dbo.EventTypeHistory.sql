CREATE TABLE [dbo].[EventTypeHistory]
(
[EventTypeHistoryID] [int] NOT NULL IDENTITY(1, 1),
[EventTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[EventTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[EventTypeDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Enabled] [bit] NOT NULL,
[UnitsOfEffort] [int] NOT NULL,
[FollowupTimeUnitsID] [int] NULL,
[FollowupQuantity] [int] NULL,
[AvailableManually] [bit] NOT NULL,
[StatusAfterEvent] [int] NULL,
[AquariumEventAfterEvent] [int] NULL,
[EventSubtypeID] [int] NOT NULL,
[DocumentTypeID] [int] NULL,
[LeadTypeID] [int] NOT NULL,
[AllowCustomTimeUnits] [bit] NULL,
[InProcess] [bit] NOT NULL,
[KeyEvent] [bit] NOT NULL,
[UseEventCosts] [bit] NULL,
[UseEventUOEs] [bit] NULL,
[UseEventDisbursements] [bit] NULL,
[UseEventComments] [bit] NULL,
[SignatureRequired] [bit] NULL,
[SignatureOverride] [bit] NULL,
[VisioX] [decimal] (18, 8) NULL,
[VisioY] [decimal] (18, 8) NULL,
[WhoChanged] [int] NULL,
[WhenChanged] [datetime] NOT NULL,
[ChangeAction] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ChangeNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[AquariumEventSubtypeID] [int] NULL,
[FollowupWorkingDaysOnly] [bit] NULL,
[CalculateTableRows] [bit] NULL
)
GO
ALTER TABLE [dbo].[EventTypeHistory] ADD CONSTRAINT [PK_EventTypeHistory] PRIMARY KEY CLUSTERED  ([EventTypeHistoryID])
GO
CREATE NONCLUSTERED INDEX [IX_EventTypeHistory_Clients] ON [dbo].[EventTypeHistory] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_EventTypeHistory_EventType] ON [dbo].[EventTypeHistory] ([EventTypeID])
GO
