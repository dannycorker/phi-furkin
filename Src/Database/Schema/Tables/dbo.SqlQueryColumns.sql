CREATE TABLE [dbo].[SqlQueryColumns]
(
[SqlQueryColumnID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SqlQueryID] [int] NOT NULL,
[SqlQueryTableID] [int] NULL,
[CompleteOutputText] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[ColumnAlias] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ShowColumn] [bit] NOT NULL,
[DisplayOrder] [int] NOT NULL,
[SortOrder] [int] NULL,
[SortType] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[TempColumnID] [int] NULL,
[TempTableID] [int] NULL,
[IsAggregate] [bit] NOT NULL,
[ColumnDataType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ManipulatedDataType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ColumnNaturalName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[SourceID] [int] NULL,
[ComplexValue] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SqlQueryColumns] ADD CONSTRAINT [PK_SqlQueryColumns] PRIMARY KEY CLUSTERED  ([SqlQueryColumnID])
GO
CREATE NONCLUSTERED INDEX [IX_SqlQueryColumns_SourceID] ON [dbo].[SqlQueryColumns] ([SourceID]) WHERE ([SourceID]>(0))
GO
CREATE NONCLUSTERED INDEX [IX_SqlQueryColumns_SqlQueryID] ON [dbo].[SqlQueryColumns] ([SqlQueryID])
GO
CREATE NONCLUSTERED INDEX [IX_SqlQueryColumns_QueryTable] ON [dbo].[SqlQueryColumns] ([SqlQueryID], [SqlQueryTableID])
GO
