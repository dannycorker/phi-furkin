CREATE TABLE [dbo].[_C600_ImportBatch]
(
[ImportBatchID] [int] NOT NULL IDENTITY(1, 1),
[ImportFileName] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[BatchXML] [xml] NOT NULL,
[StartDateTime] [datetime] NOT NULL,
[EndDateTime] [datetime] NULL,
[TimeTaken] [int] NULL
)
GO
ALTER TABLE [dbo].[_C600_ImportBatch] ADD CONSTRAINT [PK__C600_ImportBatch] PRIMARY KEY CLUSTERED  ([ImportBatchID])
GO
