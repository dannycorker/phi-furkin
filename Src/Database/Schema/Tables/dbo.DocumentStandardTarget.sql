CREATE TABLE [dbo].[DocumentStandardTarget]
(
[DocumentStandardTargetID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DocumentTypeID] [int] NOT NULL,
[Target] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[ObjectName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[PropertyName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[TemplateTypeID] [int] NOT NULL,
[Notes] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[IsSpecial] [bit] NULL,
[ExcelSheetLocation] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DocumentTypeVersionID] [int] NULL
)
GO
ALTER TABLE [dbo].[DocumentStandardTarget] ADD CONSTRAINT [PK_DocumentStandardTarget] PRIMARY KEY CLUSTERED  ([DocumentStandardTargetID])
GO
ALTER TABLE [dbo].[DocumentStandardTarget] ADD CONSTRAINT [FK_DocumentStandardTarget_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DocumentStandardTarget] ADD CONSTRAINT [FK_DocumentStandardTarget_DocumentType] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentType] ([DocumentTypeID])
GO
ALTER TABLE [dbo].[DocumentStandardTarget] ADD CONSTRAINT [FK_DocumentStandardTarget_DocumentTypeVersion] FOREIGN KEY ([DocumentTypeVersionID]) REFERENCES [dbo].[DocumentTypeVersion] ([DocumentTypeVersionID])
GO
ALTER TABLE [dbo].[DocumentStandardTarget] ADD CONSTRAINT [FK_DocumentStandardTarget_TemplateType] FOREIGN KEY ([TemplateTypeID]) REFERENCES [dbo].[TemplateType] ([TemplateTypeID])
GO
