CREATE TABLE [dbo].[AssetType]
(
[AssetTypeID] [int] NOT NULL IDENTITY(1, 1),
[AssetSubTypeID] [int] NOT NULL,
[AssetTypeName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Note] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Enabled] [bit] NOT NULL,
[Deleted] [bit] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[AssetType] ADD CONSTRAINT [PK__AssetTyp__FD33C2226A3D0FC7] PRIMARY KEY CLUSTERED  ([AssetTypeID])
GO
ALTER TABLE [dbo].[AssetType] ADD CONSTRAINT [FK__AssetType__Asset__6C255839] FOREIGN KEY ([AssetSubTypeID]) REFERENCES [dbo].[AssetSubType] ([AssetSubTypeID])
GO
