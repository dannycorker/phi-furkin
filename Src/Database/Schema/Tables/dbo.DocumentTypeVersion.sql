CREATE TABLE [dbo].[DocumentTypeVersion]
(
[DocumentTypeVersionID] [int] NOT NULL IDENTITY(1, 1),
[DocumentTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NULL,
[DocumentTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DocumentTypeDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Header] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Template] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Footer] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[CanBeAutoSent] [bit] NOT NULL,
[EmailSubject] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[EmailBodyText] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[InputFormat] [varchar] (24) COLLATE Latin1_General_CI_AS NULL,
[OutputFormat] [varchar] (24) COLLATE Latin1_General_CI_AS NULL,
[Enabled] [bit] NOT NULL,
[RecipientsTo] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[RecipientsCC] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[RecipientsBCC] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ReadOnlyTo] [bit] NOT NULL,
[ReadOnlyCC] [bit] NOT NULL,
[ReadOnlyBCC] [bit] NOT NULL,
[SendToMultipleRecipients] [bit] NOT NULL,
[MultipleRecipientDataSourceType] [int] NULL,
[MultipleRecipientDataSourceID] [int] NULL,
[SendToAllByDefault] [bit] NOT NULL,
[ExcelTemplatePath] [varchar] (1024) COLLATE Latin1_General_CI_AS NULL,
[FromDetails] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[ReadOnlyFrom] [bit] NOT NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[FolderID] [int] NULL,
[IsThunderheadTemplate] [bit] NULL,
[ThunderheadUniqueTemplateID] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[ThunderheadDocumentFormat] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[DocumentTitleTemplate] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[CreatedBy] [int] NULL,
[CreatedOn] [datetime] NULL,
[VersionNumber] [int] NULL,
[ActiveFromDate] [datetime] NULL,
[Archived] [bit] NULL
)
GO
ALTER TABLE [dbo].[DocumentTypeVersion] ADD CONSTRAINT [PK_DocumentTypeVersion] PRIMARY KEY CLUSTERED  ([DocumentTypeVersionID])
GO
ALTER TABLE [dbo].[DocumentTypeVersion] ADD CONSTRAINT [FK_DocumentTypeVersion_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DocumentTypeVersion] ADD CONSTRAINT [FK_DocumentTypeVersion_DocumentType] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentType] ([DocumentTypeID])
GO
ALTER TABLE [dbo].[DocumentTypeVersion] ADD CONSTRAINT [FK_DocumentTypeVersion_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
