CREATE TABLE [dbo].[BillingConfiguration]
(
[BillingConfigurationID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[RemainderUpFront] [bit] NULL,
[RegularPaymentWait] [int] NULL,
[OneOffAdjustmentWait] [int] NULL,
[MandateWait] [int] NULL,
[CoolingOffPeriod] [int] NULL,
[MinimumAdjustmentValue] [money] NULL,
[AdjustmentDayIsFirstDayOfNewPremium] [int] NULL,
[DDCollectionProcessingInterval] [int] NULL
)
GO
ALTER TABLE [dbo].[BillingConfiguration] ADD CONSTRAINT [PK_BillingConfiguration] PRIMARY KEY CLUSTERED  ([BillingConfigurationID])
GO
