CREATE TABLE [dbo].[VetEnvoy_ConversationHistory]
(
[HistoryID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ConversationPK] [int] NOT NULL,
[ConversationID] [uniqueidentifier] NOT NULL,
[Received] [datetime] NOT NULL,
[Service] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Status] [int] NOT NULL,
[Actioned] [datetime] NULL
)
GO
ALTER TABLE [dbo].[VetEnvoy_ConversationHistory] ADD CONSTRAINT [PK_VetEnvoy_ConversationHistory] PRIMARY KEY CLUSTERED  ([HistoryID])
GO
ALTER TABLE [dbo].[VetEnvoy_ConversationHistory] ADD CONSTRAINT [FK_VetEnvoy_ConversationHistory_VetEnvoy_Conversations] FOREIGN KEY ([ConversationPK]) REFERENCES [dbo].[VetEnvoy_Conversations] ([ConversationPK])
GO
