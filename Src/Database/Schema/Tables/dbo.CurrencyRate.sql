CREATE TABLE [dbo].[CurrencyRate]
(
[CurrencyRateID] [int] NOT NULL IDENTITY(1, 1),
[CurrencyId] [int] NOT NULL,
[CurrencyCode] [char] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[FromGBPRate] [decimal] (18, 10) NOT NULL,
[ToGBPRate] [decimal] (18, 10) NOT NULL,
[ConversionDate] [datetime] NOT NULL,
[IsLatest] [bit] NOT NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2011-01-07
-- Description:	Set the IsLatest flag to false for all older records when new ones get added
-- =============================================
CREATE TRIGGER [dbo].[trgi_CurrencyRate]
   ON [dbo].[CurrencyRate]
   AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	/*
		If we have just inserted records for one or more currencies,
		make sure that the earlier records are no longer flagged as IsLatest.

		This trigger only fires for inserts, not updates, so there is no
		problem with causing an infinite loop here.
	*/
	UPDATE dbo.CurrencyRate
	SET IsLatest = 0
	FROM dbo.CurrencyRate cr
	INNER JOIN inserted i ON i.CurrencyID = cr.CurrencyID AND cr.IsLatest = 1
	WHERE cr.CurrencyRateID < i.CurrencyRateID

END
GO
ALTER TABLE [dbo].[CurrencyRate] ADD CONSTRAINT [PK_CurrencyRate] PRIMARY KEY CLUSTERED  ([CurrencyRateID])
GO
ALTER TABLE [dbo].[CurrencyRate] ADD CONSTRAINT [FK_CurrencyRate_Currency] FOREIGN KEY ([CurrencyCode]) REFERENCES [dbo].[Currency] ([CurrencyCode])
GO
