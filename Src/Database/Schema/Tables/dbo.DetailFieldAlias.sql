CREATE TABLE [dbo].[DetailFieldAlias]
(
[DetailFieldAliasID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NULL,
[DetailFieldID] [int] NOT NULL,
[DetailFieldAlias] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[DetailFieldAlias] ADD CONSTRAINT [PK_DetailFieldAlias] PRIMARY KEY CLUSTERED  ([DetailFieldAliasID])
GO
CREATE NONCLUSTERED INDEX [IX_DetailFieldAlias_Covering] ON [dbo].[DetailFieldAlias] ([DetailFieldAlias], [LeadTypeID], [DetailFieldID], [ClientID])
GO
ALTER TABLE [dbo].[DetailFieldAlias] ADD CONSTRAINT [IX_DetailFieldAlias_DetailFieldID] UNIQUE NONCLUSTERED  ([DetailFieldID], [ClientID])
GO
ALTER TABLE [dbo].[DetailFieldAlias] ADD CONSTRAINT [IX_DetailFieldAlias] UNIQUE NONCLUSTERED  ([LeadTypeID], [DetailFieldAlias], [ClientID])
GO
ALTER TABLE [dbo].[DetailFieldAlias] ADD CONSTRAINT [FK_DetailFieldAlias_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DetailFieldAlias] ADD CONSTRAINT [FK_DetailFieldAlias_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
