CREATE TABLE [dbo].[RPIFieldGroup]
(
[RPIFieldGroupID] [int] NOT NULL IDENTITY(1, 1),
[GroupName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[RPIFieldGroup] ADD CONSTRAINT [PK_RPIFieldGroup] PRIMARY KEY CLUSTERED  ([RPIFieldGroupID])
GO
