CREATE TABLE [dbo].[AccountMandate]
(
[AccountMandateID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[AccountID] [int] NOT NULL,
[MandateStatusID] [int] NOT NULL,
[DateRequested] [datetime] NULL,
[FirstAcceptablePaymentDate] [datetime] NULL,
[DirectDebitInstructionID] [int] NULL,
[Reference] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[FailureCode] [varchar] (16) COLLATE Latin1_General_CI_AS NULL,
[FailureDate] [datetime] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[Comments] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[AccountMandate] ADD CONSTRAINT [PK_AccountMandate] PRIMARY KEY CLUSTERED  ([AccountMandateID])
GO
ALTER TABLE [dbo].[AccountMandate] ADD CONSTRAINT [FK_AccountMandate_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
GO
ALTER TABLE [dbo].[AccountMandate] ADD CONSTRAINT [FK_AccountMandate_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[AccountMandate] ADD CONSTRAINT [FK_AccountMandate_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[AccountMandate] ADD CONSTRAINT [FK_AccountMandate_MandateStatus] FOREIGN KEY ([MandateStatusID]) REFERENCES [dbo].[MandateStatus] ([MandateStatusID])
GO
