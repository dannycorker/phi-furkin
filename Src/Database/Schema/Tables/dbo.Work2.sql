CREATE TABLE [dbo].[Work2]
(
[Work2ID] [int] NOT NULL IDENTITY(1, 1),
[WorkSeedID] [int] NOT NULL,
[Value1] [varchar] (4000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Work2] ADD CONSTRAINT [PK_Work2] PRIMARY KEY CLUSTERED  ([Work2ID])
GO
ALTER TABLE [dbo].[Work2] ADD CONSTRAINT [FK_Work2_WorkSeed] FOREIGN KEY ([WorkSeedID]) REFERENCES [dbo].[WorkSeed] ([WorkSeedID]) ON DELETE CASCADE
GO
