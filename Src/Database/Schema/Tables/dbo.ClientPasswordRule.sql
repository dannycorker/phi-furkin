CREATE TABLE [dbo].[ClientPasswordRule]
(
[ClientPasswordRuleID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[MinLength] [int] NOT NULL,
[MinLowerCase] [int] NOT NULL,
[MinUpperCase] [int] NOT NULL,
[MinNumber] [int] NOT NULL,
[MinSymbol] [int] NOT NULL,
[ExpiryDays] [int] NOT NULL,
[ResetForcesChange] [bit] NOT NULL,
[PasswordHistoryCount] [int] NULL
)
GO
ALTER TABLE [dbo].[ClientPasswordRule] ADD CONSTRAINT [PK_ClientPasswordRule] PRIMARY KEY CLUSTERED  ([ClientPasswordRuleID])
GO
ALTER TABLE [dbo].[ClientPasswordRule] ADD CONSTRAINT [FK_ClientPasswordRule_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
