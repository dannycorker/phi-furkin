CREATE TABLE [dbo].[SqlQueryEditingCriteriaSetMember]
(
[SqlQueryEditingCriteriaSetMemberID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SqlQueryEditingID] [int] NOT NULL,
[SqlQueryEditingCriteriaSetID] [int] NOT NULL,
[SqlQueryEditingColumnID] [int] NOT NULL,
[Criteria1] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Criteria2] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SqlQueryEditingCriteriaSetMember] ADD CONSTRAINT [PK_SqlQueryEditingCriteriaSetMember] PRIMARY KEY CLUSTERED  ([SqlQueryEditingCriteriaSetMemberID])
GO
CREATE NONCLUSTERED INDEX [IX_SqlQueryEditingCriteriaSetMember_Query_Criteria] ON [dbo].[SqlQueryEditingCriteriaSetMember] ([SqlQueryEditingID], [SqlQueryEditingCriteriaSetID])
GO
ALTER TABLE [dbo].[SqlQueryEditingCriteriaSetMember] ADD CONSTRAINT [FK_SqlQueryEditingCriteriaSetMember_SqlQueryEditing] FOREIGN KEY ([SqlQueryEditingID]) REFERENCES [dbo].[SqlQueryEditing] ([SqlQueryEditingID]) ON DELETE CASCADE
GO
