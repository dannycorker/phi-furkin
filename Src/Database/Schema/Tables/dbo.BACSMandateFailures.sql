CREATE TABLE [dbo].[BACSMandateFailures]
(
[BACSMandateFailureID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[FileType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FileName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[RowNo] [int] NOT NULL,
[AccountNumber] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[SortCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[NewAccountNumber] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[NewSortCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[NewName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[BACSProcessedDate] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FailureReason] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[FailureCode] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Reference] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[AccountID] [int] NULL,
[AccountMandateID] [int] NULL,
[ImportStatusID] [int] NULL,
[ImportedDate] [date] NULL,
[BACSReturnProcessingID] [int] NULL,
[EventToApply] [int] NULL,
[CaseID] [int] NULL,
[Comments] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[BACSMandateFailures] ADD CONSTRAINT [PK_BACSMandateFailures] PRIMARY KEY CLUSTERED  ([BACSMandateFailureID])
GO
