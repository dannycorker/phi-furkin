CREATE TABLE [dbo].[BrainTreePaymentMethodCreate]
(
[BrainTreePaymentMethodCreateID] [int] NOT NULL IDENTITY(1, 1),
[MatterID] [int] NOT NULL,
[RequestXML] [xml] NOT NULL,
[ResponseXML] [xml] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[BrainTreePaymentMethodCreate] ADD CONSTRAINT [PK_BrainTreePaymentMethodCreate] PRIMARY KEY CLUSTERED  ([BrainTreePaymentMethodCreateID])
GO
