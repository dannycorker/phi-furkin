CREATE TABLE [dbo].[__RestartService]
(
[RestartID] [int] NOT NULL IDENTITY(1, 1),
[Service] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Server] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[DateTimeRestarted] [datetime] NULL
)
GO
ALTER TABLE [dbo].[__RestartService] ADD CONSTRAINT [PK__C00_RestartService] PRIMARY KEY CLUSTERED  ([RestartID])
GO
