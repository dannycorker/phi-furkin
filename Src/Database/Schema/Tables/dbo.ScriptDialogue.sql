CREATE TABLE [dbo].[ScriptDialogue]
(
[ScriptDialogueID] [int] NOT NULL IDENTITY(1, 1),
[ScriptSectionID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[Dialogue] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[HtmlDialogue] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[WhenCreated] [datetime] NULL,
[WhoCreated] [int] NULL,
[WhenModified] [datetime] NULL,
[WhoModified] [int] NULL
)
GO
ALTER TABLE [dbo].[ScriptDialogue] ADD CONSTRAINT [PK_ScriptDialogue] PRIMARY KEY CLUSTERED  ([ScriptDialogueID])
GO
ALTER TABLE [dbo].[ScriptDialogue] ADD CONSTRAINT [FK_ScriptDialogue_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ScriptDialogue] ADD CONSTRAINT [FK_ScriptDialogue_ScriptSection] FOREIGN KEY ([ScriptSectionID]) REFERENCES [dbo].[ScriptSection] ([ScriptSectionID])
GO
