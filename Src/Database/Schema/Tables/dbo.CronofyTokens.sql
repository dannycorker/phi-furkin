CREATE TABLE [dbo].[CronofyTokens]
(
[CronofyTokenID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientPersonnelID] [int] NOT NULL,
[AccessToken] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[TokenType] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[ExpiresIn] [int] NOT NULL,
[RefreshToken] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Scope] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[DateTimeUpdated] [datetime] NULL
)
GO
ALTER TABLE [dbo].[CronofyTokens] ADD CONSTRAINT [PK_CronofyTokens] PRIMARY KEY CLUSTERED  ([CronofyTokenID])
GO
