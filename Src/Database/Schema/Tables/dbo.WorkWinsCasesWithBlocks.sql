CREATE TABLE [dbo].[WorkWinsCasesWithBlocks]
(
[WorkWinsCasesWithBlocksID] [int] NOT NULL IDENTITY(1, 1),
[WorkWinSeedID] [int] NOT NULL,
[CaseID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[WorkWinsCasesWithBlocks] ADD CONSTRAINT [PK_WorkWinsCasesWithBlocks] PRIMARY KEY CLUSTERED  ([WorkWinsCasesWithBlocksID])
GO
CREATE NONCLUSTERED INDEX [IX_WorkWinsCasesWithBlocks] ON [dbo].[WorkWinsCasesWithBlocks] ([WorkWinSeedID], [CaseID])
GO
ALTER TABLE [dbo].[WorkWinsCasesWithBlocks] ADD CONSTRAINT [FK_WorkWinsCasesWithBlocks_WorkWinSeed] FOREIGN KEY ([WorkWinSeedID]) REFERENCES [dbo].[WorkWinSeed] ([WorkWinSeedID]) ON DELETE CASCADE
GO
