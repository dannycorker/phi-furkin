CREATE TABLE [dbo].[ClientOffices]
(
[ClientOfficeID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[OfficeName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[BuildingName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Address1] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Address2] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Town] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[County] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Country] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[PostCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[OfficeTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[OfficeTelephoneExtension] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[OfficeFax] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CountryID] [int] NULL CONSTRAINT [DF__ClientOff__Count__4242D080] DEFAULT ((232)),
[LanguageID] [int] NULL,
[SubClientID] [int] NULL,
[AdminClientPersonnelID] [int] NULL,
[CurrencyID] [int] NULL
)
GO
ALTER TABLE [dbo].[ClientOffices] ADD CONSTRAINT [PK_ClientOffices] PRIMARY KEY CLUSTERED  ([ClientOfficeID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientOffices_Client] ON [dbo].[ClientOffices] ([ClientID])
GO
ALTER TABLE [dbo].[ClientOffices] ADD CONSTRAINT [FK_ClientOffices_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ClientOffices] ADD CONSTRAINT [FK_ClientOffices_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
ALTER TABLE [dbo].[ClientOffices] ADD CONSTRAINT [FK_ClientOffices_Currency] FOREIGN KEY ([CurrencyID]) REFERENCES [dbo].[Currency] ([CurrencyID])
GO
