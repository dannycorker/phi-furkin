CREATE TABLE [dbo].[DocumentBatchDetail]
(
[DocumentBatchDetailID] [int] NOT NULL IDENTITY(1, 1),
[DocumentBatchID] [int] NOT NULL,
[EventTypeId] [int] NOT NULL CONSTRAINT [DF__DocumentB__Event__5832119F] DEFAULT ((0)),
[LeadDocumentID] [int] NOT NULL,
[DocumentMetaData] [xml] NULL,
[Uploaded] [bit] NOT NULL CONSTRAINT [DF__DocumentB__Uploa__592635D8] DEFAULT ((0)),
[LockedBy] [int] NULL,
[LockDate] [datetime] NULL,
[ClientID] [int] NOT NULL,
[MatterID] [int] NULL,
[VendorResourceListID] [int] NULL,
[Notes] [varchar] (500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[DocumentBatchDetail] ADD CONSTRAINT [PK_DocumentBatchDetail] PRIMARY KEY CLUSTERED  ([DocumentBatchDetailID])
GO
