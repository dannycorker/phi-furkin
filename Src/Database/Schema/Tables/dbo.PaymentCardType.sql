CREATE TABLE [dbo].[PaymentCardType]
(
[PaymentCardTypeID] [int] NOT NULL IDENTITY(1, 1),
[PaymentCardType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DisplayOrder] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[PaymentCardType] ADD CONSTRAINT [PK_PaymentCardType] PRIMARY KEY CLUSTERED  ([PaymentCardTypeID])
GO
