CREATE TABLE [dbo].[Quote]
(
[QuoteID] [int] NOT NULL IDENTITY(1, 1),
[QuoteSessionID] [int] NULL,
[MatterID] [int] NULL,
[PetCount] [int] NULL,
[QuoteXml] [xml] NULL,
[ResponseXml] [xml] NULL,
[QuoteStartTime] [datetime] NULL,
[QuoteEndTime] [datetime] NULL,
[WhoCreated] [int] NULL,
[SourceID] [int] NULL,
[ChangeNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[TrackingCode] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[SignUpMethodID] [int] NULL,
[TrackingResourceListID] [int] NULL
)
GO
ALTER TABLE [dbo].[Quote] ADD CONSTRAINT [PK_Quote] PRIMARY KEY CLUSTERED  ([QuoteID])
GO
CREATE NONCLUSTERED INDEX [IX_Quote_QuoteSessionID] ON [dbo].[Quote] ([QuoteSessionID], [QuoteID])
GO
CREATE NONCLUSTERED INDEX [IX_Quote_QuoteSessionID_TrackingResourceListID] ON [dbo].[Quote] ([QuoteSessionID], [QuoteID], [TrackingResourceListID])
GO
