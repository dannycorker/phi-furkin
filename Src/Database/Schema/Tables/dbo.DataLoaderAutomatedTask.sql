CREATE TABLE [dbo].[DataLoaderAutomatedTask]
(
[DataLoaderAutomatedTaskID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DataloaderMapID] [int] NOT NULL,
[AutomatedTaskID] [int] NOT NULL,
[DelaySeconds] [int] NULL
)
GO
ALTER TABLE [dbo].[DataLoaderAutomatedTask] ADD CONSTRAINT [PK_DataLoaderAutomatedTask] PRIMARY KEY CLUSTERED  ([DataLoaderAutomatedTaskID])
GO
