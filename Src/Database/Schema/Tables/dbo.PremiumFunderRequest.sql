CREATE TABLE [dbo].[PremiumFunderRequest]
(
[PremiumFunderRequestID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NULL,
[MatterID] [int] NULL,
[RequestJson] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[externalQuoteReference] [nvarchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[PremiumFunderRequest] ADD CONSTRAINT [PK_PremiumFunderRequest] PRIMARY KEY CLUSTERED  ([PremiumFunderRequestID])
GO
