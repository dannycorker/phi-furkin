CREATE TABLE [dbo].[DataLoaderObjectType]
(
[DataLoaderObjectTypeID] [int] NOT NULL,
[ObjectTypeName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[ObjectTypeDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[ProcessingOrder] [int] NULL
)
GO
ALTER TABLE [dbo].[DataLoaderObjectType] ADD CONSTRAINT [PK_DataLoaderObjectType] PRIMARY KEY CLUSTERED  ([DataLoaderObjectTypeID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DataLoaderObjectTypeName] ON [dbo].[DataLoaderObjectType] ([ObjectTypeName])
GO
