CREATE TABLE [dbo].[ChartSeriesType]
(
[ChartSeriesTypeID] [int] NOT NULL,
[ChartSeriesTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ChartSeriesType] ADD CONSTRAINT [PK_ChartSeriesType] PRIMARY KEY CLUSTERED  ([ChartSeriesTypeID])
GO
