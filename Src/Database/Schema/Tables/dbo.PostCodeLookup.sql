CREATE TABLE [dbo].[PostCodeLookup]
(
[PostCodeLookupID] [int] NOT NULL IDENTITY(1, 1),
[PostCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PostCodeLookup] ADD CONSTRAINT [PK_PostCodeLookup] PRIMARY KEY CLUSTERED  ([PostCodeLookupID])
GO
