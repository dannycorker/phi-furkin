CREATE TABLE [dbo].[PremiumFunderApplicationPolicy]
(
[PremiumFunderApplicationPolicyID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[PAMatterID] [int] NULL,
[PremiumFunderRequestID] [int] NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds (GPR)
-- Create date: 2019-12-03
-- Description:	Add a record to the PremiumFunderApplicationPolicy History table on Insert, Update, Delete to PremiumFunderApplicationPolicy
-- =============================================
CREATE TRIGGER [dbo].[trgiud_PremiumFunderApplicationPolicy]
   ON  [dbo].[PremiumFunderApplicationPolicy] 
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN

	SET NOCOUNT ON;

    INSERT INTO dbo.PremiumFunderApplicationPolicyHistory([PremiumFunderApplicationPolicyID], [ClientID], [PAMatterID], [PremiumFunderRequestID], [WhenCreated], [WhoCreated], [WhenArchived])
	SELECT
		COALESCE(i.PremiumFunderApplicationPolicyID, d.PremiumFunderApplicationPolicyID),
		COALESCE(i.ClientID, d.ClientID),
		COALESCE(i.PAMatterID, d.PAMatterID),
		COALESCE(i.PremiumFunderRequestID, d.PremiumFunderRequestID),
		COALESCE(i.WhenCreated, d.WhenCreated),
		COALESCE(i.WhoCreated, d.WhoCreated),
		dbo.fn_GetDate_Local()
	FROM inserted i
	FULL OUTER JOIN deleted d ON i.PremiumFunderApplicationPolicyID = d.PremiumFunderApplicationPolicyID

END
GO
ALTER TABLE [dbo].[PremiumFunderApplicationPolicy] ADD CONSTRAINT [PK_PremiumFunderApplicationPolicy] PRIMARY KEY CLUSTERED  ([PremiumFunderApplicationPolicyID])
GO
