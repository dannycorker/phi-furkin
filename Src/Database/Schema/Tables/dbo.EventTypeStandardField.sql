CREATE TABLE [dbo].[EventTypeStandardField]
(
[EventTypeStandardFieldID] [int] NOT NULL IDENTITY(1, 1),
[EventTypeID] [int] NOT NULL,
[DataLoaderObjectFieldID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[Mandatory] [bit] NULL
)
GO
ALTER TABLE [dbo].[EventTypeStandardField] ADD CONSTRAINT [PK_EventTypeStandardField] PRIMARY KEY CLUSTERED  ([EventTypeStandardFieldID])
GO
ALTER TABLE [dbo].[EventTypeStandardField] ADD CONSTRAINT [FK_EventTypeStandardField_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EventTypeStandardField] ADD CONSTRAINT [FK_EventTypeStandardField_DataLoaderObjectField] FOREIGN KEY ([DataLoaderObjectFieldID]) REFERENCES [dbo].[DataLoaderObjectField] ([DataLoaderObjectFieldID])
GO
ALTER TABLE [dbo].[EventTypeStandardField] ADD CONSTRAINT [FK_EventTypeStandardField_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
