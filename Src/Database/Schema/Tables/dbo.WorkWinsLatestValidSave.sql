CREATE TABLE [dbo].[WorkWinsLatestValidSave]
(
[WorkWinsLatestValidSave] [int] NOT NULL IDENTITY(1, 1),
[WorkWinSeedID] [int] NOT NULL,
[MatterID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[WhenSaved] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[WorkWinsLatestValidSave] ADD CONSTRAINT [PK_WorkWinsLatestValidSave] PRIMARY KEY CLUSTERED  ([WorkWinsLatestValidSave])
GO
CREATE NONCLUSTERED INDEX [IX_WorkWinsLatestValidSave] ON [dbo].[WorkWinsLatestValidSave] ([WorkWinSeedID], [MatterID], [DetailFieldID], [WhenSaved])
GO
ALTER TABLE [dbo].[WorkWinsLatestValidSave] ADD CONSTRAINT [FK_WorkWinsLatestValidSave_WorkWinSeed] FOREIGN KEY ([WorkWinSeedID]) REFERENCES [dbo].[WorkWinSeed] ([WorkWinSeedID]) ON DELETE CASCADE
GO
