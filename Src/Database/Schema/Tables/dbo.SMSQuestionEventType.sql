CREATE TABLE [dbo].[SMSQuestionEventType]
(
[SMSQuestionEventTypeID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SMSQuestionID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[SMSQuestionEventType] ADD CONSTRAINT [PK_SMSQuestionEventType] PRIMARY KEY CLUSTERED  ([SMSQuestionEventTypeID])
GO
ALTER TABLE [dbo].[SMSQuestionEventType] ADD CONSTRAINT [FK_SMSQuestionEventType_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SMSQuestionEventType] ADD CONSTRAINT [FK_SMSQuestionEventType_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[SMSQuestionEventType] ADD CONSTRAINT [FK_SMSQuestionEventType_SMSQuestion] FOREIGN KEY ([SMSQuestionID]) REFERENCES [dbo].[SMSQuestion] ([SMSQuestionID])
GO
