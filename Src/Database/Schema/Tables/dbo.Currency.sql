CREATE TABLE [dbo].[Currency]
(
[CurrencyID] [int] NOT NULL,
[CurrencyCode] [char] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[CurrencyName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[MajorNamePlural] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MajorNameSingular] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MinorNamePlural] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MinorNameSingular] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CurrencySymbol] [nvarchar] (2) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Currency] ADD CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED  ([CurrencyID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Currency] ON [dbo].[Currency] ([CurrencyCode])
GO
