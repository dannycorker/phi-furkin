CREATE TABLE [dbo].[EsignatureStatus]
(
[EsignatureStatusID] [int] NOT NULL IDENTITY(1, 1),
[EsignatureStatusDescription] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[EsignatureStatus] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[EsignatureStatus] ADD CONSTRAINT [PK_EsignatureStatus] PRIMARY KEY CLUSTERED  ([EsignatureStatusID])
GO
