CREATE TABLE [dbo].[DetailFieldLock]
(
[ClientID] [int] NOT NULL,
[LeadOrMatter] [int] NOT NULL,
[LeadOrMatterID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL CONSTRAINT [DF__DetailFie__Detai__51851410] DEFAULT ((0)),
[DetailFieldPageID] [int] NOT NULL CONSTRAINT [DF__DetailFie__Detai__52793849] DEFAULT ((0)),
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[DetailFieldLock] ADD CONSTRAINT [PK_DetailFieldLock] PRIMARY KEY CLUSTERED  ([ClientID], [LeadOrMatter], [LeadOrMatterID], [DetailFieldID], [DetailFieldPageID])
GO
