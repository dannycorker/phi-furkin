CREATE TABLE [dbo].[AutomatedTaskHistory]
(
[TaskHistoryID] [int] NOT NULL IDENTITY(1, 1),
[TaskID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[Taskname] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Enabled] [bit] NOT NULL,
[RunAtHour] [int] NULL,
[RunAtMinute] [int] NULL,
[RepeatTimeUnitsID] [int] NULL,
[RepeatTimeQuantity] [int] NULL,
[NextRunDateTime] [datetime] NULL,
[WorkflowTask] [bit] NOT NULL,
[AlreadyRunning] [bit] NULL,
[AutomatedTaskGroupID] [int] NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[MaximumAllowableErrors] [int] NULL,
[EventSubTypeThresholding] [bit] NOT NULL,
[ChangeAction] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[ChangeNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[AutomatedTaskHistory] ADD CONSTRAINT [PK_AutomatedTaskHistory] PRIMARY KEY CLUSTERED  ([TaskHistoryID])
GO
