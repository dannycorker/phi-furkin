CREATE TABLE [dbo].[SearchField]
(
[SearchFieldID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[SearchTypeID] [int] NOT NULL,
[SearchFieldName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DetailFieldID] [int] NOT NULL,
[LookupDetailFieldID] [int] NULL,
[EditClientPersonnelAdminGroupID] [int] NULL
)
GO
ALTER TABLE [dbo].[SearchField] ADD CONSTRAINT [PK_SearchTypeField] PRIMARY KEY CLUSTERED  ([SearchFieldID])
GO
ALTER TABLE [dbo].[SearchField] ADD CONSTRAINT [FK_SearchField_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SearchField] ADD CONSTRAINT [FK_SearchField_SearchType] FOREIGN KEY ([SearchTypeID]) REFERENCES [dbo].[SearchType] ([SearchTypeID])
GO
ALTER TABLE [dbo].[SearchField] ADD CONSTRAINT [FK_SearchField_SubClient] FOREIGN KEY ([SubClientID]) REFERENCES [dbo].[SubClient] ([SubClientID])
GO
