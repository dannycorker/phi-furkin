CREATE TABLE [dbo].[AutomatedTaskCommandLine]
(
[AutomatedTaskCommandLineID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[AutomatedTaskCommandControlID] [int] NOT NULL,
[LineType] [char] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[LineText] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[LineNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[LineOrder] [int] NOT NULL,
[Enabled] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[AutomatedTaskCommandLine] ADD CONSTRAINT [CK__Automated__LineT__1AF3F935] CHECK (([LineType]='cfg' OR [LineType]='bat'))
GO
ALTER TABLE [dbo].[AutomatedTaskCommandLine] ADD CONSTRAINT [PK_AutomatedTaskCommandLine] PRIMARY KEY NONCLUSTERED  ([AutomatedTaskCommandLineID])
GO
CREATE CLUSTERED INDEX [IX_AutomatedTaskCommandLine_ControlLine] ON [dbo].[AutomatedTaskCommandLine] ([AutomatedTaskCommandControlID], [LineType], [LineOrder])
GO
ALTER TABLE [dbo].[AutomatedTaskCommandLine] ADD CONSTRAINT [FK_AutomatedTaskCommandLine_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[AutomatedTaskCommandLine] ADD CONSTRAINT [FK_AutomatedTaskCommandLine_CommandControl] FOREIGN KEY ([AutomatedTaskCommandControlID]) REFERENCES [dbo].[AutomatedTaskCommandControl] ([AutomatedTaskCommandControlID])
GO
