CREATE TABLE [dbo].[ThirdPartyFieldGroup]
(
[ThirdPartyFieldGroupID] [int] NOT NULL IDENTITY(1, 1),
[ThirdPartySystemID] [int] NOT NULL,
[GroupName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[GroupDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2013-12-19
-- Description:	Only allow ThirdParty records to be created in the Aquarius database
-- =============================================
CREATE TRIGGER [dbo].[trgi_ThirdPartyFieldGroup]
   ON [dbo].[ThirdPartyFieldGroup]
   AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF SYSTEM_USER = 'notriggers'
	BEGIN
		RETURN
	END

	/* Also allow special user 'AquariumSqlDiff' to insert records.  And allow anyone in the Aquarius database. */
    IF DB_NAME() = 'Aquarius' OR SYSTEM_USER = 'AquariumSqlDiff'
    BEGIN
		RETURN
    END

	/* Deny the insert and raise an error back to the user */
	ROLLBACK
	RAISERROR('Stop! Changes to this table can only be made in the Aquarius database and distributed by the AquariumSqlDiff user!', 16, 1)

END
GO
ALTER TABLE [dbo].[ThirdPartyFieldGroup] ADD CONSTRAINT [PK_ThirdPartyFieldGroup] PRIMARY KEY CLUSTERED  ([ThirdPartyFieldGroupID])
GO
ALTER TABLE [dbo].[ThirdPartyFieldGroup] ADD CONSTRAINT [FK_ThirdPartyFieldGroup_ThirdPartySystem] FOREIGN KEY ([ThirdPartySystemID]) REFERENCES [dbo].[ThirdPartySystem] ([ThirdPartySystemId])
GO
