CREATE TABLE [dbo].[Panels]
(
[PanelID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientPersonnelID] [int] NOT NULL,
[PanelName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Panels] ADD CONSTRAINT [PK_Panels] PRIMARY KEY CLUSTERED  ([PanelID])
GO
ALTER TABLE [dbo].[Panels] ADD CONSTRAINT [FK_Panels_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[Panels] ADD CONSTRAINT [FK_Panels_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
