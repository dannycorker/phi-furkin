CREATE TABLE [dbo].[SharedEventTypePosition]
(
[SharedEventTypePositionID] [int] NOT NULL IDENTITY(1, 1),
[EventTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[VisioX] [decimal] (18, 8) NULL,
[VisioY] [decimal] (18, 8) NULL
)
GO
ALTER TABLE [dbo].[SharedEventTypePosition] ADD CONSTRAINT [PK_SharedEventTypePosition] PRIMARY KEY CLUSTERED  ([SharedEventTypePositionID])
GO
ALTER TABLE [dbo].[SharedEventTypePosition] ADD CONSTRAINT [FK_SharedEventTypePosition_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SharedEventTypePosition] ADD CONSTRAINT [FK_SharedEventTypePosition_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[SharedEventTypePosition] ADD CONSTRAINT [FK_SharedEventTypePosition_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
