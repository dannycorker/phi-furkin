CREATE TABLE [dbo].[ScriptSectionNavigationCriteria]
(
[ScriptSectionNavigationCriteriaID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[FromStepSectionID] [int] NOT NULL,
[ToStepSectionID] [int] NOT NULL,
[WizardButton] [varchar] (12) COLLATE Latin1_General_CI_AS NOT NULL,
[DetailFieldID] [int] NOT NULL,
[CriteriaOperator] [varchar] (12) COLLATE Latin1_General_CI_AS NOT NULL,
[Value] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ScriptSectionNavigationCriteria] ADD CONSTRAINT [PK_ScriptSectionNavigationCriteria] PRIMARY KEY CLUSTERED  ([ScriptSectionNavigationCriteriaID])
GO
ALTER TABLE [dbo].[ScriptSectionNavigationCriteria] ADD CONSTRAINT [FK_ScriptSectionNavigationCriteria_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ScriptSectionNavigationCriteria] ADD CONSTRAINT [FK_ScriptSectionNavigationCriteria_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[ScriptSectionNavigationCriteria] ADD CONSTRAINT [FK_ScriptSectionNavigationCriteria_ScriptSection] FOREIGN KEY ([FromStepSectionID]) REFERENCES [dbo].[ScriptSection] ([ScriptSectionID])
GO
ALTER TABLE [dbo].[ScriptSectionNavigationCriteria] ADD CONSTRAINT [FK_ScriptSectionNavigationCriteria_ScriptSection1] FOREIGN KEY ([ToStepSectionID]) REFERENCES [dbo].[ScriptSection] ([ScriptSectionID])
GO
