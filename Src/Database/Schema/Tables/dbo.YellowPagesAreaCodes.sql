CREATE TABLE [dbo].[YellowPagesAreaCodes]
(
[YellowPagesAreaCodeID] [int] NOT NULL IDENTITY(1, 1),
[YellowPagesAreaCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[PostCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[YellowPagesAreaCodes] ADD CONSTRAINT [PK_YellowPagesAreaCodes] PRIMARY KEY CLUSTERED  ([YellowPagesAreaCodeID])
GO
