CREATE TABLE [dbo].[BillingConfigurationItem]
(
[BillingConfigurationItemID] [int] NOT NULL IDENTITY(1, 1),
[BillingConfigurationItemName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[BillingConfigurationItem] ADD CONSTRAINT [PK_BillingConfigurationItem] PRIMARY KEY CLUSTERED  ([BillingConfigurationItemID])
GO
