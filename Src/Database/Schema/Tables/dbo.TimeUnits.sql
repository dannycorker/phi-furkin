CREATE TABLE [dbo].[TimeUnits]
(
[TimeUnitsID] [int] NOT NULL IDENTITY(1, 1),
[TimeUnitsName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[SQLTimeUnits] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[TimeUnits] ADD CONSTRAINT [PK_TimeUnits] PRIMARY KEY CLUSTERED  ([TimeUnitsID])
GO
