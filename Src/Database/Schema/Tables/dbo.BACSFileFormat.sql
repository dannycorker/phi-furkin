CREATE TABLE [dbo].[BACSFileFormat]
(
[BACSFileFormatID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[FormatName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[FormatDescription] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[FileGenerationRoutine] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[BACSFileFormat] ADD CONSTRAINT [PK_BACSFileFormat] PRIMARY KEY CLUSTERED  ([BACSFileFormatID])
GO
