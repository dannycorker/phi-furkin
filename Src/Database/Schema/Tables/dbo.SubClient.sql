CREATE TABLE [dbo].[SubClient]
(
[SubClientID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CompanyName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[ClientTypeID] [int] NOT NULL,
[LanguageID] [int] NOT NULL,
[CountryID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[SubClient] ADD CONSTRAINT [PK_SubClient] PRIMARY KEY CLUSTERED  ([SubClientID])
GO
