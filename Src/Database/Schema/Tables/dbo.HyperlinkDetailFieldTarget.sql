CREATE TABLE [dbo].[HyperlinkDetailFieldTarget]
(
[HyperlinkDetailFieldTargetID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[HyperlinkDetailFieldID] [int] NOT NULL,
[Target] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[DetailFieldID] [int] NULL,
[TemplateTypeID] [int] NOT NULL,
[DetailFieldAlias] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[Notes] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[HyperlinkDetailFieldTarget] ADD CONSTRAINT [PK_HyperlinkDetailFieldTarget] PRIMARY KEY CLUSTERED  ([HyperlinkDetailFieldTargetID])
GO
ALTER TABLE [dbo].[HyperlinkDetailFieldTarget] ADD CONSTRAINT [FK_HyperlinkDetailFieldTarget_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[HyperlinkDetailFieldTarget] ADD CONSTRAINT [FK_HyperlinkDetailFieldTarget_DetailFields] FOREIGN KEY ([HyperlinkDetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[HyperlinkDetailFieldTarget] ADD CONSTRAINT [FK_HyperlinkDetailFieldTarget_TemplateType] FOREIGN KEY ([TemplateTypeID]) REFERENCES [dbo].[TemplateType] ([TemplateTypeID])
GO
