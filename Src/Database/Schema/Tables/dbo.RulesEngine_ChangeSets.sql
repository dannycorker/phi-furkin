CREATE TABLE [dbo].[RulesEngine_ChangeSets]
(
[ChangeSetID] [int] NOT NULL IDENTITY(1, 1),
[ParentChangeSetID] [int] NULL,
[ClientID] [int] NOT NULL,
[TagName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ValidFrom] [datetime] NULL,
[ValidTo] [datetime] NULL,
[WhenCreated] [datetime] NOT NULL CONSTRAINT [DF_RulesEngine_ChangeSets_WhenCreated] DEFAULT ([dbo].[fn_GetDate_Local]()),
[WhoCreated] [int] NOT NULL CONSTRAINT [DF_RulesEngine_ChangeSets_WhoCreated] DEFAULT ((0)),
[WhenModified] [datetime] NOT NULL CONSTRAINT [DF_RulesEngine_ChangeSets_WhenModified] DEFAULT ([dbo].[fn_GetDate_Local]()),
[WhoModified] [int] NOT NULL CONSTRAINT [DF_RulesEngine_ChangeSets_WhoModified] DEFAULT ((0))
)
GO
ALTER TABLE [dbo].[RulesEngine_ChangeSets] ADD CONSTRAINT [PK_RulesEngine_ChangeSets] PRIMARY KEY CLUSTERED  ([ChangeSetID])
GO
