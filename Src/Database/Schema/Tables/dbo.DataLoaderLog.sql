CREATE TABLE [dbo].[DataLoaderLog]
(
[DataLoaderLogID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DataLoaderMapID] [int] NOT NULL,
[DataLoaderMapSectionID] [int] NOT NULL,
[DataLoaderFileID] [int] NOT NULL,
[DataLoaderFieldDefinitionID] [int] NULL,
[DataLoaderObjectFieldID] [int] NULL,
[DetailFieldID] [int] NULL,
[ObjectName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[RowIndex] [int] NULL,
[ColIndex] [int] NULL,
[NodeName] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Message] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[LogLevel] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[DataLoaderLog] ADD CONSTRAINT [PK_DataLoaderLog] PRIMARY KEY CLUSTERED  ([DataLoaderLogID])
GO
CREATE NONCLUSTERED INDEX [IX_DataLoaderLogDataLoaderFile] ON [dbo].[DataLoaderLog] ([DataLoaderFileID])
GO
ALTER TABLE [dbo].[DataLoaderLog] ADD CONSTRAINT [FK_DataLoaderLog_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DataLoaderLog] ADD CONSTRAINT [FK_DataLoaderLog_DataLoaderFile] FOREIGN KEY ([DataLoaderFileID]) REFERENCES [dbo].[DataLoaderFile] ([DataLoaderFileID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DataLoaderLog] ADD CONSTRAINT [FK_DataLoaderLog_DataLoaderObjectField] FOREIGN KEY ([DataLoaderObjectFieldID]) REFERENCES [dbo].[DataLoaderObjectField] ([DataLoaderObjectFieldID])
GO
ALTER TABLE [dbo].[DataLoaderLog] ADD CONSTRAINT [FK_DataLoaderLog_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[DataLoaderLog] ADD CONSTRAINT [FK_DataLoaderLog_LogLevel] FOREIGN KEY ([LogLevel]) REFERENCES [dbo].[LogLevel] ([LogLevelID])
GO
