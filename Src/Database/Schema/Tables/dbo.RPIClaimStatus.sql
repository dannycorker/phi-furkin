CREATE TABLE [dbo].[RPIClaimStatus]
(
[RPIClaimStatusID] [int] NOT NULL IDENTITY(1, 1),
[MatterID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[ApplicationID] [varchar] (150) COLLATE Latin1_General_CI_AS NOT NULL,
[ActivityEngineGuid] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[PhaseCacheID] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[PhaseCacheName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[RPIAsUser] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[RPIUser] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[RPIPass] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Notes] [varchar] (500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[RPIClaimStatus] ADD CONSTRAINT [PK_RPIClaimStatus] PRIMARY KEY CLUSTERED  ([RPIClaimStatusID])
GO
ALTER TABLE [dbo].[RPIClaimStatus] ADD CONSTRAINT [FK_RPIClaimStatus_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[RPIClaimStatus] ADD CONSTRAINT [FK_RPIClaimStatus_Matter] FOREIGN KEY ([MatterID]) REFERENCES [dbo].[Matter] ([MatterID])
GO
ALTER TABLE [dbo].[RPIClaimStatus] ADD CONSTRAINT [FK_RPIClaimStatus_RPIClaimStatus] FOREIGN KEY ([RPIClaimStatusID]) REFERENCES [dbo].[RPIClaimStatus] ([RPIClaimStatusID])
GO
