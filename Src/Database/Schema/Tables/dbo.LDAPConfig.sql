CREATE TABLE [dbo].[LDAPConfig]
(
[LDAPConfigID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NULL,
[QueryString] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[QueryAccountName] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[QueryAccountPassword] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Enabled] [bit] NULL,
[Domain] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[LDAPConfig] ADD CONSTRAINT [PK_LDAPConfig] PRIMARY KEY CLUSTERED  ([LDAPConfigID])
GO
