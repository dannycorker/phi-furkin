CREATE TABLE [dbo].[EventTypeAutomatedEvent]
(
[EventTypeAutomatedEventID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[AutomatedEventTypeID] [int] NOT NULL,
[RunAsUserID] [int] NULL,
[ThreadToFollowUp] [int] NULL,
[InternalPriority] [tinyint] NULL,
[DelaySeconds] [int] NULL
)
GO
ALTER TABLE [dbo].[EventTypeAutomatedEvent] ADD CONSTRAINT [PK_EventTypeAutomatedEvent] PRIMARY KEY CLUSTERED  ([EventTypeAutomatedEventID])
GO
ALTER TABLE [dbo].[EventTypeAutomatedEvent] ADD CONSTRAINT [FK_EventTypeAutomatedEvent_AutomatedEventType] FOREIGN KEY ([AutomatedEventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[EventTypeAutomatedEvent] ADD CONSTRAINT [FK_EventTypeAutomatedEvent_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EventTypeAutomatedEvent] ADD CONSTRAINT [FK_EventTypeAutomatedEvent_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
