CREATE TABLE [dbo].[ObjectTypeRelationship]
(
[ObjectTypeRelationshipID] [int] NOT NULL IDENTITY(1, 1),
[FromObjectTypeID] [int] NOT NULL,
[ToObjectTypeID] [int] NOT NULL,
[RelationshipTypeID] [int] NOT NULL,
[RelationshipName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ObjectTypeRelationship] ADD CONSTRAINT [PK_ObjectTypeRelationship] PRIMARY KEY CLUSTERED  ([ObjectTypeRelationshipID])
GO
ALTER TABLE [dbo].[ObjectTypeRelationship] ADD CONSTRAINT [FK_ObjectTypeRelationship_ObjectTypeFrom] FOREIGN KEY ([FromObjectTypeID]) REFERENCES [dbo].[ObjectType] ([ObjectTypeID])
GO
ALTER TABLE [dbo].[ObjectTypeRelationship] ADD CONSTRAINT [FK_ObjectTypeRelationship_ObjectTypeTo] FOREIGN KEY ([ToObjectTypeID]) REFERENCES [dbo].[ObjectType] ([ObjectTypeID])
GO
