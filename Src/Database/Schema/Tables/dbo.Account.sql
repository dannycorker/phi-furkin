CREATE TABLE [dbo].[Account]
(
[AccountID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[AccountHolderName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[FriendlyName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[AccountNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Sortcode] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[Reference] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[CardToken] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[MaskedCardNumber] [varchar] (24) COLLATE Latin1_General_CI_AS NULL,
[ExpiryMonth] [int] NULL,
[ExpiryYear] [int] NULL,
[ExpiryDate] [datetime] NULL,
[AccountTypeID] [int] NULL,
[Active] [bit] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[NextPaymentDate] [datetime] NULL,
[NextPaymentTotal] [numeric] (18, 2) NULL,
[CustomerPaymentScheduleID] [int] NULL,
[Address1] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Address2] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Town] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[County] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Postcode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CountryID] [int] NULL,
[ObjectID] [int] NULL,
[ObjectTypeID] [int] NULL,
[CardType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FirstName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Bankname] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[AccountSubTypeID] [int] NULL,
[SourceID] [int] NULL,
[ClientAccountID] [int] NULL,
[Comments] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[AccountUseID] [int] NOT NULL CONSTRAINT [DF_Account_AccountUseID] DEFAULT ((1)),
[InstitutionNumber] [varchar] (100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED  ([AccountID])
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [FK_Account_AccountType] FOREIGN KEY ([AccountTypeID]) REFERENCES [dbo].[AccountType] ([AccountTypeID])
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [FK_Account_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[Account] ADD CONSTRAINT [FK_Account_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
