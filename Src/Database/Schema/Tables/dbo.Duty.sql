CREATE TABLE [dbo].[Duty]
(
[DutyID] [int] NOT NULL IDENTITY(1, 1),
[DutyName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[CountryID] [int] NOT NULL CONSTRAINT [DF__Duty__CountryID__054DCF89] DEFAULT ((232)),
[State] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[TaxPercent] [money] NOT NULL,
[From] [date] NULL,
[To] [date] NULL,
[TaxProportion] [money] NULL,
[GrossMultiplier] [money] NULL
)
GO
ALTER TABLE [dbo].[Duty] ADD CONSTRAINT [PK_Duty] PRIMARY KEY CLUSTERED  ([DutyID])
GO
ALTER TABLE [dbo].[Duty] ADD CONSTRAINT [FK_Duty_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
