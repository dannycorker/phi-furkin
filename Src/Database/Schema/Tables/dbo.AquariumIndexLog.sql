CREATE TABLE [dbo].[AquariumIndexLog]
(
[AquariumIndexLogID] [int] NOT NULL IDENTITY(1, 1),
[ObjectID] [int] NOT NULL,
[ObjectName] [varchar] (130) COLLATE Latin1_General_CI_AS NULL,
[IndexID] [int] NOT NULL,
[IndexName] [varchar] (130) COLLATE Latin1_General_CI_AS NULL,
[IndexTypeDesc] [nvarchar] (60) COLLATE Latin1_General_CI_AS NULL,
[Fragmentation] [float] NOT NULL,
[LastChecked] [datetime] NULL,
[LastReorganised] [datetime] NULL,
[LastRebuilt] [datetime] NULL,
[DurationSeconds] [int] NULL,
[HasLOB] [bit] NULL,
[ReorganiseCommand] [nvarchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[RebuildCommand] [nvarchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[AquariumIndexLog] ADD CONSTRAINT [PK_AquariumIndexLog] PRIMARY KEY CLUSTERED  ([AquariumIndexLogID])
GO
