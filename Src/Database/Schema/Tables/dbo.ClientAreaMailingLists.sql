CREATE TABLE [dbo].[ClientAreaMailingLists]
(
[ClientAreaMailingListID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Email] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[PostCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[OutcomeID] [int] NULL,
[YellowPagesAreaCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL,
[ClientQuestionnaireID] [int] NOT NULL,
[OnHold] [bit] NULL
)
GO
ALTER TABLE [dbo].[ClientAreaMailingLists] ADD CONSTRAINT [PK_ClientAreaMailingList] PRIMARY KEY CLUSTERED  ([ClientAreaMailingListID])
GO
ALTER TABLE [dbo].[ClientAreaMailingLists] ADD CONSTRAINT [FK_ClientAreaMailingLists_ClientQuestionnaires] FOREIGN KEY ([ClientQuestionnaireID]) REFERENCES [dbo].[ClientQuestionnaires] ([ClientQuestionnaireID])
GO
