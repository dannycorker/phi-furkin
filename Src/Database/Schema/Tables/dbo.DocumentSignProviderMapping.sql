CREATE TABLE [dbo].[DocumentSignProviderMapping]
(
[DSPMappingID] [int] NOT NULL IDENTITY(1, 1),
[DSPTypeID] [int] NOT NULL,
[DSPAccountID] [int] NOT NULL,
[DSPUserID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[EmailOutETID] [int] NOT NULL CONSTRAINT [DF__DocumentS__Email__34213060] DEFAULT ((0)),
[OutDocumentTypeID] [int] NULL CONSTRAINT [DF__DocumentS__OutDo__35155499] DEFAULT ((0)),
[DocInEventTypeID] [int] NOT NULL CONSTRAINT [DF__DocumentS__DocIn__360978D2] DEFAULT ((0)),
[BrandingKey] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF__DocumentS__Brand__36FD9D0B] DEFAULT (''),
[Enabled] [bit] NOT NULL CONSTRAINT [DF__DocumentS__Enabl__37F1C144] DEFAULT ((0)),
[DeclinedOrVoidedEventTypeID] [int] NULL
)
GO
ALTER TABLE [dbo].[DocumentSignProviderMapping] ADD CONSTRAINT [PK_DocumentSignProviderMapping] PRIMARY KEY CLUSTERED  ([DSPMappingID])
GO
