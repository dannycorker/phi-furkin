CREATE TABLE [dbo].[VATRates]
(
[VATRateID] [int] NOT NULL IDENTITY(1, 1),
[Type] [int] NOT NULL,
[ValidFromDate] [date] NOT NULL,
[ValidToDate] [date] NOT NULL,
[VATRate] [money] NOT NULL
)
GO
ALTER TABLE [dbo].[VATRates] ADD CONSTRAINT [PK_VATRates] PRIMARY KEY CLUSTERED  ([VATRateID])
GO
