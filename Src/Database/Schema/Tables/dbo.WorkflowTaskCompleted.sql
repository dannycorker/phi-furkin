CREATE TABLE [dbo].[WorkflowTaskCompleted]
(
[WorkflowTaskCompletedID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[WorkflowTaskID] [int] NOT NULL,
[WorkflowGroupID] [int] NOT NULL,
[AutomatedTaskID] [int] NOT NULL,
[Priority] [int] NOT NULL,
[AssignedTo] [int] NULL,
[AssignedDate] [datetime] NULL,
[LeadID] [int] NOT NULL,
[CaseID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[FollowUp] [bit] NOT NULL,
[Important] [bit] NOT NULL CONSTRAINT [DF__WorkflowT__Impor__17236851] DEFAULT ((0)),
[CreationDate] [datetime] NOT NULL,
[Escalated] [bit] NOT NULL CONSTRAINT [DF__WorkflowT__Escal__162F4418] DEFAULT ((0)),
[EscalatedBy] [int] NULL,
[EscalationReason] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[EscalationDate] [datetime] NULL,
[Disabled] [bit] NOT NULL CONSTRAINT [DF__WorkflowT__Disab__153B1FDF] DEFAULT ((0)),
[CompletedBy] [int] NOT NULL,
[CompletedOn] [datetime] NOT NULL,
[CompletionDescription] [varchar] (255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[WorkflowTaskCompleted] ADD CONSTRAINT [PK_WorkflowTaskCompleted] PRIMARY KEY CLUSTERED  ([WorkflowTaskCompletedID])
GO
ALTER TABLE [dbo].[WorkflowTaskCompleted] ADD CONSTRAINT [FK_WorkflowTaskCompleted_ClientPersonnel] FOREIGN KEY ([CompletedBy]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[WorkflowTaskCompleted] ADD CONSTRAINT [FK_WorkflowTaskCompleted_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
