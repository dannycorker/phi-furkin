CREATE TABLE [dbo].[FAS_Documents]
(
[FasDocumentID] [int] NOT NULL IDENTITY(1, 1),
[FasCheckerDataID] [int] NOT NULL,
[DocumentBlob] [image] NULL
)
GO
ALTER TABLE [dbo].[FAS_Documents] ADD CONSTRAINT [PK_FAS_Documents] PRIMARY KEY CLUSTERED  ([FasDocumentID])
GO
ALTER TABLE [dbo].[FAS_Documents] ADD CONSTRAINT [FK_FAS_Documents_FAS_CheckerData] FOREIGN KEY ([FasCheckerDataID]) REFERENCES [dbo].[FAS_CheckerData] ([FasCheckerDataID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
