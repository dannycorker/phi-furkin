CREATE TABLE [dbo].[TransactionType]
(
[TransactionTypeID] [int] NOT NULL IDENTITY(1, 1),
[TransactionType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [varchar] (500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[TransactionType] ADD CONSTRAINT [PK_TransactionType] PRIMARY KEY CLUSTERED  ([TransactionTypeID])
GO
