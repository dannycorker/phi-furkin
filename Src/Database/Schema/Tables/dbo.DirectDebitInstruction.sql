CREATE TABLE [dbo].[DirectDebitInstruction]
(
[DirectDebitInstructionID] [int] NOT NULL IDENTITY(1, 1),
[Code] [varchar] (2) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[DirectDebitInstruction] ADD CONSTRAINT [PK_DirectDebitInstruction] PRIMARY KEY CLUSTERED  ([DirectDebitInstructionID])
GO
