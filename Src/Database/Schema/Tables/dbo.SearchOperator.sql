CREATE TABLE [dbo].[SearchOperator]
(
[SearchOperatorID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[SearchOperatorName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[SearchOperator] ADD CONSTRAINT [PK_SearchOperator] PRIMARY KEY CLUSTERED  ([SearchOperatorID])
GO
ALTER TABLE [dbo].[SearchOperator] ADD CONSTRAINT [FK_SearchOperator_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SearchOperator] ADD CONSTRAINT [FK_SearchOperator_SubClient] FOREIGN KEY ([SubClientID]) REFERENCES [dbo].[SubClient] ([SubClientID])
GO
