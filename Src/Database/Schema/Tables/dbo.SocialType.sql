CREATE TABLE [dbo].[SocialType]
(
[SocialTypeID] [int] NOT NULL IDENTITY(1, 1),
[SocialType] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[SocialType] ADD CONSTRAINT [PK_SocialType] PRIMARY KEY CLUSTERED  ([SocialTypeID])
GO
