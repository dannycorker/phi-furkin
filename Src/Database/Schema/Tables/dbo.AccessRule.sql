CREATE TABLE [dbo].[AccessRule]
(
[AccessRuleID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[AccessRuleName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[AccessRuleDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[AccessRuleEnabled] [bit] NOT NULL,
[ClientPersonnelAdminGroupID] [int] NULL,
[ClientPersonnelID] [int] NULL,
[PortalUserID] [int] NULL,
[DataLoaderObjectTypeID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[DetailFieldSubTypeID] [int] NOT NULL,
[DetailFieldPageID] [int] NULL,
[DetailFieldID] [int] NULL,
[ClientQuestionnaireID] [int] NULL,
[ValueDecoder] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ValueDecoderColumnFieldID] [int] NULL,
[ValueToCompare] [int] NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[AccessLevel] [tinyint] NULL
)
GO
ALTER TABLE [dbo].[AccessRule] ADD CONSTRAINT [PK_AccessRule] PRIMARY KEY CLUSTERED  ([AccessRuleID])
GO
CREATE NONCLUSTERED INDEX [IX_AccessRule_ClientEnabled] ON [dbo].[AccessRule] ([ClientID], [AccessRuleEnabled])
GO
CREATE NONCLUSTERED INDEX [IX_AccessRule_SourceID] ON [dbo].[AccessRule] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[AccessRule] ADD CONSTRAINT [FK_AccessRule_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
