CREATE TABLE [dbo].[CreditCalculationCredential]
(
[CreditCalculationCredentialID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientPersonnelID] [int] NOT NULL,
[SecretKey] [varchar] (32) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[CreditCalculationCredential] ADD CONSTRAINT [PK_CreditCalculationCredential] PRIMARY KEY CLUSTERED  ([CreditCalculationCredentialID])
GO
ALTER TABLE [dbo].[CreditCalculationCredential] ADD CONSTRAINT [FK_CreditCalculationCredential_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[CreditCalculationCredential] ADD CONSTRAINT [FK_CreditCalculationCredential_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
