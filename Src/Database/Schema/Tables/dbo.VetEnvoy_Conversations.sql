CREATE TABLE [dbo].[VetEnvoy_Conversations]
(
[ConversationPK] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[AccountID] [int] NOT NULL,
[ConversationID] [uniqueidentifier] NOT NULL,
[ParentConversationID] [uniqueidentifier] NULL,
[Service] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[VetEnvoy_Conversations] ADD CONSTRAINT [PK_VetEnvoy_Conversations] PRIMARY KEY CLUSTERED  ([ConversationPK])
GO
CREATE NONCLUSTERED INDEX [IX_VetEnvoy_Conversations] ON [dbo].[VetEnvoy_Conversations] ([ConversationID])
GO
ALTER TABLE [dbo].[VetEnvoy_Conversations] ADD CONSTRAINT [FK_VetEnvoy_Conversations_VetEnvoy_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[VetEnvoy_Accounts] ([AccountID])
GO
