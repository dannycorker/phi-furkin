CREATE TABLE [dbo].[ReminderTimeshift]
(
[ReminderTimeshiftID] [int] NOT NULL IDENTITY(1, 1),
[ReminderTimeshiftName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[TimeUnitsID] [int] NULL,
[TimeUnitsQuantity] [int] NULL,
[DisplayOrder] [int] NOT NULL,
[TimeUnitsQuantityInMinutes] [int] NULL
)
GO
ALTER TABLE [dbo].[ReminderTimeshift] ADD CONSTRAINT [PK_ReminderTimeshift] PRIMARY KEY CLUSTERED  ([ReminderTimeshiftID])
GO
ALTER TABLE [dbo].[ReminderTimeshift] ADD CONSTRAINT [FK_ReminderTimeshift_TimeUnits] FOREIGN KEY ([TimeUnitsID]) REFERENCES [dbo].[TimeUnits] ([TimeUnitsID])
GO
