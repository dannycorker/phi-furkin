CREATE TABLE [dbo].[Patch]
(
[PatchID] [int] NOT NULL IDENTITY(1, 1),
[PatchDate] [datetime] NOT NULL,
[PatchVersion] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[Patch] ADD CONSTRAINT [PK_Patch] PRIMARY KEY CLUSTERED  ([PatchID])
GO
