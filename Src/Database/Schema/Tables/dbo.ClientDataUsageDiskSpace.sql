CREATE TABLE [dbo].[ClientDataUsageDiskSpace]
(
[ClientDataUsageDiskSpaceID] [int] NOT NULL IDENTITY(1, 1),
[ClientDataUsageBillingRunID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[FolderName] [sys].[sysname] NOT NULL,
[FileCount] [int] NULL,
[TotalBytesForThisClient] [bigint] NOT NULL
)
GO
ALTER TABLE [dbo].[ClientDataUsageDiskSpace] ADD CONSTRAINT [PK_ClientDataUsageDiskSpace] PRIMARY KEY CLUSTERED  ([ClientDataUsageDiskSpaceID])
GO
