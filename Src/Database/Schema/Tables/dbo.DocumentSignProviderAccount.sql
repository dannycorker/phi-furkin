CREATE TABLE [dbo].[DocumentSignProviderAccount]
(
[DSPAccountID] [int] NOT NULL IDENTITY(1, 1),
[DSPTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[IntegrationKey] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[AccountKey] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[DefaultEnvelopeXML] [xml] NULL,
[EndPointURL] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Enabled] [bit] NOT NULL CONSTRAINT [DF__DocumentS__Enabl__1B558296] DEFAULT ((1))
)
GO
ALTER TABLE [dbo].[DocumentSignProviderAccount] ADD CONSTRAINT [PK_DocumentSignProviderAccount] PRIMARY KEY CLUSTERED  ([DSPAccountID])
GO
