CREATE TABLE [dbo].[TableRowRelationship]
(
[TableRowRelationshipID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[FromTableRowID] [int] NOT NULL,
[ToTableRowID] [int] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[TableRowRelationship] ADD CONSTRAINT [PK_TableRowRelationship] PRIMARY KEY CLUSTERED  ([TableRowRelationshipID])
GO
CREATE NONCLUSTERED INDEX [IX_TableRowRelationship_FromTableRowID] ON [dbo].[TableRowRelationship] ([FromTableRowID] DESC)
GO
CREATE NONCLUSTERED INDEX [IX_TableRowRelationship_ToTableRowID] ON [dbo].[TableRowRelationship] ([ToTableRowID] DESC)
GO
