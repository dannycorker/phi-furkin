CREATE TABLE [dbo].[PanelItems]
(
[PanelItemID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[PanelItemName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PanelID] [int] NULL,
[PanelItemTypeID] [int] NOT NULL,
[CreatedBy] [int] NOT NULL,
[QueryID] [int] NOT NULL,
[IsGlobal] [bit] NULL,
[XAxisColumn] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Orientation] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Margin] [int] NULL
)
GO
ALTER TABLE [dbo].[PanelItems] ADD CONSTRAINT [PK_PanelItems] PRIMARY KEY CLUSTERED  ([PanelItemID])
GO
ALTER TABLE [dbo].[PanelItems] ADD CONSTRAINT [FK_PanelItems_ClientPersonnel] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[PanelItems] ADD CONSTRAINT [FK_PanelItems_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[PanelItems] ADD CONSTRAINT [FK_PanelItems_PanelItemTypes] FOREIGN KEY ([PanelItemTypeID]) REFERENCES [dbo].[PanelItemTypes] ([PanelItemTypeID])
GO
ALTER TABLE [dbo].[PanelItems] ADD CONSTRAINT [FK_PanelItems_Panels] FOREIGN KEY ([PanelID]) REFERENCES [dbo].[Panels] ([PanelID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PanelItems] ADD CONSTRAINT [FK_PanelItems_SqlQuery] FOREIGN KEY ([QueryID]) REFERENCES [dbo].[SqlQuery] ([QueryID])
GO
