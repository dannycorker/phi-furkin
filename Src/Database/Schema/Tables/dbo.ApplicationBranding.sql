CREATE TABLE [dbo].[ApplicationBranding]
(
[ApplicationBrandingID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[FileName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[VerticalSize] [int] NULL,
[HorizontalSize] [int] NULL,
[LeadManagerAlias] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LeadAlias] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LeadDetailsTabImageFileName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2009-04-03
-- Description:	Capture changes and store in History table
-- =============================================
CREATE TRIGGER [dbo].[trgiud_ApplicationBranding]
   ON  [dbo].[ApplicationBranding]
   AFTER INSERT,DELETE,UPDATE
AS
BEGIN
	SET NOCOUNT ON;

    -- Inserts appear in the inserted table,
    -- Deletes appear in the deleted table,
    -- and Updates appear in both tables at once identically.
    -- These are all 'logical' Sql Server tables.
	INSERT INTO dbo.ApplicationBrandingHistory(
		ApplicationBrandingID,
		ClientID,
		[FileName],
		VerticalSize,
		HorizontalSize,
		LeadManagerAlias,
		LeadAlias,
		LeadDetailsTabImageFileName,
		WhoChanged,
		WhenChanged,
		ChangeAction,
		ChangeNotes
	)
	SELECT
		COALESCE(i.ApplicationBrandingID, d.ApplicationBrandingID),
		COALESCE(i.ClientID, d.ClientID),
		COALESCE(i.[FileName], d.[FileName]),
		COALESCE(i.VerticalSize, d.VerticalSize),
		COALESCE(i.HorizontalSize, d.HorizontalSize),
		COALESCE(i.LeadManagerAlias, d.LeadManagerAlias),
		COALESCE(i.LeadAlias, d.LeadAlias),
		COALESCE(i.LeadDetailsTabImageFileName, d.LeadDetailsTabImageFileName),
		NULL, --i.WhoChanged is not available yet
		dbo.fn_GetDate_Local(),
		CASE WHEN d.ApplicationBrandingID IS NULL THEN 'Insert' WHEN i.ApplicationBrandingID IS NULL THEN 'Delete' ELSE 'Update' END,
		dbo.fnGetUserInfo()
	FROM inserted i
	FULL OUTER JOIN deleted d ON i.ApplicationBrandingID = d.ApplicationBrandingID

END
GO
ALTER TABLE [dbo].[ApplicationBranding] ADD CONSTRAINT [PK_ApplicationBranding] PRIMARY KEY CLUSTERED  ([ApplicationBrandingID])
GO
CREATE NONCLUSTERED INDEX [IX_ApplicationBranding_Client] ON [dbo].[ApplicationBranding] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_ApplicationBranding_Covering2017] ON [dbo].[ApplicationBranding] ([ClientID]) INCLUDE ([FileName], [HorizontalSize], [LeadAlias], [LeadDetailsTabImageFileName], [LeadManagerAlias], [VerticalSize])
GO
ALTER TABLE [dbo].[ApplicationBranding] ADD CONSTRAINT [FK_ApplicationBranding_ApplicationBranding] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
