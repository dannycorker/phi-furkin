CREATE TABLE [dbo].[ChangeInstance]
(
[ChangeInstanceID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ChangeDefinitionID] [int] NOT NULL,
[ChangeTypeID] [int] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[Deleted] [bit] NULL
)
GO
ALTER TABLE [dbo].[ChangeInstance] ADD CONSTRAINT [PK__ChangeIn__5D06F62A4FCD3B2A] PRIMARY KEY CLUSTERED  ([ChangeInstanceID])
GO
ALTER TABLE [dbo].[ChangeInstance] ADD CONSTRAINT [FK__ChangeIns__Clien__51B5839C] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ChangeInstance] ADD CONSTRAINT [FK__ChangeIns__WhoCr__52A9A7D5] FOREIGN KEY ([WhoCreated]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[ChangeInstance] ADD CONSTRAINT [FK__ChangeIns__WhoMo__539DCC0E] FOREIGN KEY ([WhoModified]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
