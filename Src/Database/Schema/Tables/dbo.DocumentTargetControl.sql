CREATE TABLE [dbo].[DocumentTargetControl]
(
[DocumentTargetControlID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DocumentTypeID] [int] NOT NULL,
[LastParsed] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[DocumentTargetControl] ADD CONSTRAINT [PK_DocumentTargetControl] PRIMARY KEY CLUSTERED  ([DocumentTargetControlID])
GO
ALTER TABLE [dbo].[DocumentTargetControl] ADD CONSTRAINT [FK_DocumentTargetControl_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DocumentTargetControl] ADD CONSTRAINT [FK_DocumentTargetControl_DocumentType] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentType] ([DocumentTypeID])
GO
