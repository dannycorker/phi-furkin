CREATE TABLE [dbo].[EFTServers]
(
[EFTServerID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[FileTypeID] [int] NOT NULL,
[ServerURL] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[PortNumber] [int] NULL,
[UserName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Password] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Key] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[Folder] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[EFTServers] ADD CONSTRAINT [PK_EFTServers] PRIMARY KEY CLUSTERED  ([EFTServerID])
GO
