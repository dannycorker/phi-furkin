CREATE TABLE [dbo].[AquariumPage]
(
[AquariumPageID] [int] NOT NULL IDENTITY(1, 1),
[PageName] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[PageDescription] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[AquariumPage] ADD CONSTRAINT [PK_AquariumPage] PRIMARY KEY CLUSTERED  ([AquariumPageID])
GO
