CREATE TABLE [dbo].[LeadViewHistory]
(
[LeadViewHistoryID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[WhenViewed] [datetime] NOT NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-04-22
-- Description:	Insert new records into LeadViewHistoryArchive
-- =============================================
CREATE TRIGGER [dbo].[trgi_LeadViewHistory]
   ON  [dbo].[LeadViewHistory]
   AFTER INSERT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers' OR CAST(CONTEXT_INFO() AS VARCHAR) = 'Migration'
	BEGIN
		RETURN
	END

	INSERT INTO dbo.LeadViewHistoryArchive (LeadViewHistoryID, ClientPersonnelID, ClientID, LeadID, WhenViewed)
	SELECT LeadViewHistoryID, ClientPersonnelID, ClientID, LeadID, WhenViewed
	FROM inserted i

END


GO
ALTER TABLE [dbo].[LeadViewHistory] ADD CONSTRAINT [PK_LeadViewHistory] PRIMARY KEY CLUSTERED  ([LeadViewHistoryID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadViewHistory_ClientPersonnel] ON [dbo].[LeadViewHistory] ([ClientPersonnelID], [WhenViewed])
GO
ALTER TABLE [dbo].[LeadViewHistory] ADD CONSTRAINT [FK_LeadViewHistory_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[LeadViewHistory] ADD CONSTRAINT [FK_LeadViewHistory_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[LeadViewHistory] ADD CONSTRAINT [FK_LeadViewHistory_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
