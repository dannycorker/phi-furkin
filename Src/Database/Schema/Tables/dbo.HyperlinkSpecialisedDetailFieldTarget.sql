CREATE TABLE [dbo].[HyperlinkSpecialisedDetailFieldTarget]
(
[HyperlinkSpecialisedDetailFieldTargetID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[HyperlinkDetailFieldID] [int] NOT NULL,
[Target] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[DetailFieldID] [int] NULL,
[ColumnField] [int] NULL,
[TemplateTypeID] [int] NOT NULL,
[DetailFieldAlias] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[ColumnFieldAlias] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[RowField] [int] NULL,
[Notes] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[HyperlinkSpecialisedDetailFieldTarget] ADD CONSTRAINT [PK_HyperlinkSpecialisedDetailFieldTarget] PRIMARY KEY CLUSTERED  ([HyperlinkSpecialisedDetailFieldTargetID])
GO
ALTER TABLE [dbo].[HyperlinkSpecialisedDetailFieldTarget] ADD CONSTRAINT [FK_HyperlinkSpecialisedDetailFieldTarget_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[HyperlinkSpecialisedDetailFieldTarget] ADD CONSTRAINT [FK_HyperlinkSpecialisedDetailFieldTarget_DetailFields] FOREIGN KEY ([HyperlinkDetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[HyperlinkSpecialisedDetailFieldTarget] ADD CONSTRAINT [FK_HyperlinkSpecialisedDetailFieldTarget_TemplateType] FOREIGN KEY ([TemplateTypeID]) REFERENCES [dbo].[TemplateType] ([TemplateTypeID])
GO
