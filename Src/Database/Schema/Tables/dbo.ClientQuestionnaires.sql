CREATE TABLE [dbo].[ClientQuestionnaires]
(
[ClientQuestionnaireID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[QuestionnaireTitle] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[QuestionnaireDescription] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[QuestionsPerPage] [int] NULL,
[QuestionnaireLogo] [image] NULL,
[QuestionnaireLogoFileName] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[QuestionnaireFlowDiagram] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[QuestionnaireIntroductionText] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[HideIntro] [bit] NULL,
[QuestionnaireFooterText] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[HideFooter] [bit] NULL,
[QuestionnaireAddressLookup] [bit] NULL,
[Published] [bit] NULL,
[CustomerInformationAtStart] [bit] NULL CONSTRAINT [DF__ClientQue__Custo__4707859D] DEFAULT ((1)),
[LinkedQuestionnaireClientQuestionnaireID] [int] NULL,
[QuestionnaireFooterIframe] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[QuestionnaireHeaderIframe] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[QuestionnaireFooterInternal] [bit] NULL,
[QuestionnaireHeaderInternal] [bit] NULL,
[QuestionnaireFooterIframeHeight] [int] NULL,
[QuestionnaireHeaderIframeHeight] [int] NULL,
[QuestionnaireFooterIframeWidth] [int] NULL,
[QuestionnaireHeaderIframeWidth] [int] NULL,
[QuestionnaireBackGroundImage] [image] NULL,
[QuestionnaireBackGroundImageFileName] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[DefaultEmailAddress] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[MailingListType] [int] NULL,
[FrameMode] [bit] NULL,
[LayoutCss] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[LayoutCssFileName] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[ImportDirectlyIntoLeadManager] [bit] NULL,
[RunAsClientPersonnelID] [int] NULL,
[RememberAnswers] [bit] NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2012-12-21
-- Description:	Use CONTEXT_INFO to prevent unwanted access to this table.
-- =============================================
CREATE TRIGGER [dbo].[trgi_ClientQuestionnaires]
   ON [dbo].[ClientQuestionnaires]
   AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END

	/*
		You cannot insert directly into this table now.
		You must use the AddClientQuestionnaire proc which gets the new id from AquariusMaster now.
		AddClientQuestionnaire sets CONTEXT_INFO to the correct value.
	*/
	IF CAST(CONTEXT_INFO() AS VARCHAR) IS NULL OR CAST(CONTEXT_INFO() AS VARCHAR) <> 'ClientQuestionnaires'
	BEGIN
		ROLLBACK
	END

END

GO
ALTER TABLE [dbo].[ClientQuestionnaires] ADD CONSTRAINT [PK_ClientQuestionares] PRIMARY KEY CLUSTERED  ([ClientQuestionnaireID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_ClientQuestionnaires_SourceID] ON [dbo].[ClientQuestionnaires] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[ClientQuestionnaires] ADD CONSTRAINT [FK_ClientQuestionares_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID]) ON UPDATE CASCADE
GO
GRANT CONTROL ON  [dbo].[ClientQuestionnaires] TO [sp_executeall]
GO
