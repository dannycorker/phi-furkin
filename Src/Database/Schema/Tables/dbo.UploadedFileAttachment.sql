CREATE TABLE [dbo].[UploadedFileAttachment]
(
[UploadedFileAttachmentID] [int] NOT NULL IDENTITY(1, 1),
[UploadedFileID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[UploadedFileAttachment] ADD CONSTRAINT [PK_UploadedFileAttachment] PRIMARY KEY CLUSTERED  ([UploadedFileAttachmentID])
GO
ALTER TABLE [dbo].[UploadedFileAttachment] ADD CONSTRAINT [FK_UploadedFileAttachment_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[UploadedFileAttachment] ADD CONSTRAINT [FK_UploadedFileAttachment_UploadedFile] FOREIGN KEY ([UploadedFileID]) REFERENCES [dbo].[UploadedFile] ([UploadedFileID])
GO
