CREATE TABLE [dbo].[_CaseTransferDetailFieldMap]
(
[CaseTransferDetailFieldMapID] [int] NOT NULL IDENTITY(1, 1),
[ClientRelationshipID] [int] NOT NULL,
[OriginatingClientID] [int] NOT NULL,
[OriginatingDetailFieldID] [int] NOT NULL,
[DestinationClientID] [int] NOT NULL,
[DestinationDetailFieldID] [int] NOT NULL,
[Direction] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[_CaseTransferDetailFieldMap] ADD CONSTRAINT [PK_CaseTransferDetailFieldMap] PRIMARY KEY CLUSTERED  ([CaseTransferDetailFieldMapID])
GO
