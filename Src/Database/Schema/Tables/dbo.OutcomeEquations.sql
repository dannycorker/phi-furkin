CREATE TABLE [dbo].[OutcomeEquations]
(
[OutcomeEquationID] [int] NOT NULL IDENTITY(63, 1),
[OutcomeID] [int] NOT NULL,
[EquationID] [int] NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[OutcomeEquations] ADD CONSTRAINT [PK_OutcomeEquations] PRIMARY KEY CLUSTERED  ([OutcomeEquationID])
GO
ALTER TABLE [dbo].[OutcomeEquations] ADD CONSTRAINT [FK_OutcomeEquations_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[OutcomeEquations] ADD CONSTRAINT [FK_OutcomeEquations_Outcomes] FOREIGN KEY ([OutcomeID]) REFERENCES [dbo].[Outcomes] ([OutcomeID])
GO
