CREATE TABLE [dbo].[Titles]
(
[TitleID] [int] NOT NULL IDENTITY(1, 1),
[Title] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Titles] ADD CONSTRAINT [PK_Titles] PRIMARY KEY CLUSTERED  ([TitleID])
GO
