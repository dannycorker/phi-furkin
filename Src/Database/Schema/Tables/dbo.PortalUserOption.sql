CREATE TABLE [dbo].[PortalUserOption]
(
[PortalUserOptionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[PortalUserID] [int] NOT NULL,
[PortalUserOptionTypeID] [int] NOT NULL,
[OptionValue] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PortalUserOption] ADD CONSTRAINT [PK_PortalUserOption] PRIMARY KEY CLUSTERED  ([PortalUserOptionID])
GO
ALTER TABLE [dbo].[PortalUserOption] ADD CONSTRAINT [FK_PortalUserOption_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[PortalUserOption] ADD CONSTRAINT [FK_PortalUserOption_PortalUser] FOREIGN KEY ([PortalUserID]) REFERENCES [dbo].[PortalUser] ([PortalUserID])
GO
ALTER TABLE [dbo].[PortalUserOption] ADD CONSTRAINT [FK_PortalUserOption_PortalUserOptionType] FOREIGN KEY ([PortalUserOptionTypeID]) REFERENCES [dbo].[PortalUserOptionType] ([PortalUserOptionTypeID])
GO
