CREATE TABLE [dbo].[HSKLeadDocumentToDelete]
(
[LeadDocumentID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[CaseID] [int] NOT NULL,
[DocumentDatabaseID] [int] NULL,
[DateRequested] [date] NOT NULL,
[IsComplete] [bit] NOT NULL,
[DateComplete] [datetime] NULL
)
GO
