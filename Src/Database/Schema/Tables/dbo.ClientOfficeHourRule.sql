CREATE TABLE [dbo].[ClientOfficeHourRule]
(
[ClientOfficeHourRuleID] [int] NOT NULL IDENTITY(1, 1),
[ClientOfficeID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[Monday] [bit] NOT NULL,
[Tuesday] [bit] NOT NULL,
[Wednesday] [bit] NOT NULL,
[Thursday] [bit] NOT NULL,
[Friday] [bit] NOT NULL,
[Saturday] [bit] NOT NULL,
[Sunday] [bit] NOT NULL,
[FromTime] [varchar] (5) COLLATE Latin1_General_CI_AS NOT NULL,
[ToTime] [varchar] (5) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ClientOfficeHourRule] ADD CONSTRAINT [PK_ClientOfficeHourRule] PRIMARY KEY CLUSTERED  ([ClientOfficeHourRuleID])
GO
ALTER TABLE [dbo].[ClientOfficeHourRule] ADD CONSTRAINT [FK_ClientOfficeHourRule_ClientOffices] FOREIGN KEY ([ClientOfficeID]) REFERENCES [dbo].[ClientOffices] ([ClientOfficeID])
GO
ALTER TABLE [dbo].[ClientOfficeHourRule] ADD CONSTRAINT [FK_ClientOfficeHourRule_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
