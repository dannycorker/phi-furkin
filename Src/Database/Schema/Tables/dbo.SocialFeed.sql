CREATE TABLE [dbo].[SocialFeed]
(
[SocialFeedID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SocialTypeID] [int] NOT NULL,
[Token] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[Secret] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ScreenName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[SocialUserID] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[WhenCreated] [datetime] NULL,
[WhoCreated] [int] NULL,
[WhenModified] [datetime] NULL,
[WhoModified] [int] NULL,
[SinceMessageID] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SocialFeed] ADD CONSTRAINT [PK_SocialFeed] PRIMARY KEY CLUSTERED  ([SocialFeedID])
GO
ALTER TABLE [dbo].[SocialFeed] ADD CONSTRAINT [FK_SocialFeed_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
