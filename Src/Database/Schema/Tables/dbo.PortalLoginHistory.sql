CREATE TABLE [dbo].[PortalLoginHistory]
(
[PortalLoginHistoryID] [int] NOT NULL IDENTITY(1, 1),
[UserName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[LoginDateTime] [datetime] NOT NULL,
[IsSuccess] [bit] NOT NULL,
[ClientID] [int] NULL,
[PortalUserID] [int] NULL,
[ClientPersonnelID] [int] NULL,
[AttemptedLogins] [int] NULL,
[AccountDisabled] [bit] NULL,
[Notes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PortalLoginHistory] ADD CONSTRAINT [PK_PortalLoginHistory] PRIMARY KEY CLUSTERED  ([PortalLoginHistoryID])
GO
