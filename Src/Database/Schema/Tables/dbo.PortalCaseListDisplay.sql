CREATE TABLE [dbo].[PortalCaseListDisplay]
(
[PortalCaseListDisplayID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[Field1Caption] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Field1TypeID] [int] NULL,
[Field1DetailFieldID] [int] NULL,
[Field1ColumnDetailFieldID] [int] NULL,
[Field2Caption] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Field2TypeID] [int] NULL,
[Field2DetailFieldID] [int] NULL,
[Field2ColumnDetailFieldID] [int] NULL,
[Field3Caption] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Field3TypeID] [int] NULL,
[Field3DetailFieldID] [int] NULL,
[Field3ColumnDetailFieldID] [int] NULL,
[Field4Caption] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Field4TypeID] [int] NULL,
[Field4DetailFieldID] [int] NULL,
[Field4ColumnDetailFieldID] [int] NULL,
[Field5Caption] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Field5TypeID] [int] NULL,
[Field5DetailFieldID] [int] NULL,
[Field5ColumnDetailFieldID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[SqlQueryText] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PortalCaseListDisplay] ADD CONSTRAINT [PK_PortalCaseListDisplay] PRIMARY KEY CLUSTERED  ([PortalCaseListDisplayID])
GO
ALTER TABLE [dbo].[PortalCaseListDisplay] ADD CONSTRAINT [FK_PortalCaseListDisplay_ClientPersonnel_Created] FOREIGN KEY ([WhoCreated]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[PortalCaseListDisplay] ADD CONSTRAINT [FK_PortalCaseListDisplay_ClientPersonnel_Modified] FOREIGN KEY ([WhoModified]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[PortalCaseListDisplay] ADD CONSTRAINT [FK_PortalCaseListDisplay_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[PortalCaseListDisplay] ADD CONSTRAINT [FK_PortalCaseListDisplay_DataLoaderObjectType1] FOREIGN KEY ([Field1TypeID]) REFERENCES [dbo].[DataLoaderObjectType] ([DataLoaderObjectTypeID])
GO
ALTER TABLE [dbo].[PortalCaseListDisplay] ADD CONSTRAINT [FK_PortalCaseListDisplay_DataLoaderObjectType2] FOREIGN KEY ([Field2TypeID]) REFERENCES [dbo].[DataLoaderObjectType] ([DataLoaderObjectTypeID])
GO
ALTER TABLE [dbo].[PortalCaseListDisplay] ADD CONSTRAINT [FK_PortalCaseListDisplay_DataLoaderObjectType3] FOREIGN KEY ([Field3TypeID]) REFERENCES [dbo].[DataLoaderObjectType] ([DataLoaderObjectTypeID])
GO
ALTER TABLE [dbo].[PortalCaseListDisplay] ADD CONSTRAINT [FK_PortalCaseListDisplay_DataLoaderObjectType4] FOREIGN KEY ([Field4TypeID]) REFERENCES [dbo].[DataLoaderObjectType] ([DataLoaderObjectTypeID])
GO
ALTER TABLE [dbo].[PortalCaseListDisplay] ADD CONSTRAINT [FK_PortalCaseListDisplay_DataLoaderObjectType5] FOREIGN KEY ([Field5TypeID]) REFERENCES [dbo].[DataLoaderObjectType] ([DataLoaderObjectTypeID])
GO
ALTER TABLE [dbo].[PortalCaseListDisplay] ADD CONSTRAINT [FK_PortalCaseListDisplay_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID]) ON DELETE CASCADE
GO
