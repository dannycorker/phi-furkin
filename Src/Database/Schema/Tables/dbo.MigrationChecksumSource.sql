CREATE TABLE [dbo].[MigrationChecksumSource]
(
[MigrationChecksumSourceID] [int] NOT NULL IDENTITY(1, 1),
[MigrationTableID] [int] NOT NULL,
[MigrationTableName] [varchar] (1024) COLLATE Latin1_General_CI_AS NULL,
[SourcePKID] [int] NOT NULL,
[SourceChecksumValue] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[MigrationChecksumSource] ADD CONSTRAINT [PK_MigrationChecksumSource] PRIMARY KEY CLUSTERED  ([MigrationChecksumSourceID])
GO
