CREATE TABLE [dbo].[ClientCustomerMails]
(
[ClientCustomerMailID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientQuestionnaireID] [int] NULL,
[CustomerID] [int] NULL,
[ClientAreaMailingListID] [int] NULL,
[DateSent] [datetime] NULL
)
GO
ALTER TABLE [dbo].[ClientCustomerMails] ADD CONSTRAINT [PK_ClientCustomerMails] PRIMARY KEY CLUSTERED  ([ClientCustomerMailID])
GO
