CREATE TABLE [dbo].[AutomatedTaskInfo]
(
[TaskID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[QueueID] [int] NOT NULL,
[SchedulerID] [int] NULL,
[ServerName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LockDateTime] [datetime] NULL,
[RunCount] [int] NOT NULL,
[LastRundate] [datetime] NULL,
[LastRuntime] [int] NOT NULL,
[LastRowcount] [int] NOT NULL,
[MaxRuntime] [int] NOT NULL,
[MaxRowcount] [int] NOT NULL,
[AvgRuntime] [int] NOT NULL,
[AvgRowcount] [int] NOT NULL,
[Comments] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[AutomatedTaskInfo] ADD CONSTRAINT [PK_AutomatedTaskInfo] PRIMARY KEY CLUSTERED  ([TaskID])
GO
CREATE NONCLUSTERED INDEX [IX_AutomatedTaskInfo_Task] ON [dbo].[AutomatedTaskInfo] ([TaskID])
GO
