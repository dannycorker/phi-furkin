CREATE TABLE [dbo].[TextToSpeak]
(
[TextToSpeakID] [int] NOT NULL IDENTITY(1, 1),
[ClientQuestionnaireID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[PageNumber] [int] NULL,
[SpeakText] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[IsShown] [bit] NULL
)
GO
ALTER TABLE [dbo].[TextToSpeak] ADD CONSTRAINT [PK_TextToSpeak] PRIMARY KEY CLUSTERED  ([TextToSpeakID])
GO
ALTER TABLE [dbo].[TextToSpeak] ADD CONSTRAINT [FK_TextToSpeak_ClientQuestionnaires] FOREIGN KEY ([ClientQuestionnaireID]) REFERENCES [dbo].[ClientQuestionnaires] ([ClientQuestionnaireID])
GO
