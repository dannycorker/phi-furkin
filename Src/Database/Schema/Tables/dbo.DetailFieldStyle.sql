CREATE TABLE [dbo].[DetailFieldStyle]
(
[DetailFieldStyleID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[StyleName] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[BackgroundColor] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ForegroundColor] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FontSize] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FontWeight] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Align] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Padding] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PaddingHorizontal] [nvarchar] (5) COLLATE Latin1_General_CI_AS NULL,
[TextDecoration] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FontStyle] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PaddingVertical] [nvarchar] (5) COLLATE Latin1_General_CI_AS NULL,
[Enabled] [bit] NULL
)
GO
ALTER TABLE [dbo].[DetailFieldStyle] ADD CONSTRAINT [PK_DetailFieldStyle] PRIMARY KEY CLUSTERED  ([DetailFieldStyleID])
GO
ALTER TABLE [dbo].[DetailFieldStyle] ADD CONSTRAINT [FK_DetailFieldStyle_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
