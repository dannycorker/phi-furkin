CREATE TABLE [dbo].[CheckMateFieldMapping]
(
[CheckMateFieldMappingID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[CheckMateFieldID] [int] NOT NULL,
[FieldID] [int] NOT NULL,
[ColumnFieldID] [int] NULL,
[DataLoaderObjectTypeID] [int] NOT NULL,
[CheckMateFieldGroupID] [int] NOT NULL,
[IsEnabled] [bit] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[CheckMateFieldMapping] ADD CONSTRAINT [PK_CheckMateFieldMapping] PRIMARY KEY CLUSTERED  ([CheckMateFieldMappingID])
GO
ALTER TABLE [dbo].[CheckMateFieldMapping] ADD CONSTRAINT [FK_CheckMateFieldMapping_CheckMateField] FOREIGN KEY ([CheckMateFieldID]) REFERENCES [dbo].[CheckMateField] ([CheckMateFieldID])
GO
ALTER TABLE [dbo].[CheckMateFieldMapping] ADD CONSTRAINT [FK_CheckMateFieldMapping_CheckMateFieldGroup] FOREIGN KEY ([CheckMateFieldGroupID]) REFERENCES [dbo].[CheckMateFieldGroup] ([CheckMateFieldGroupID])
GO
ALTER TABLE [dbo].[CheckMateFieldMapping] ADD CONSTRAINT [FK_CheckMateFieldMapping_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[CheckMateFieldMapping] ADD CONSTRAINT [FK_CheckMateFieldMapping_DataLoaderObjectType] FOREIGN KEY ([DataLoaderObjectTypeID]) REFERENCES [dbo].[DataLoaderObjectType] ([DataLoaderObjectTypeID])
GO
ALTER TABLE [dbo].[CheckMateFieldMapping] ADD CONSTRAINT [FK_CheckMateFieldMapping_DetailFields] FOREIGN KEY ([FieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[CheckMateFieldMapping] ADD CONSTRAINT [FK_CheckMateFieldMapping_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
