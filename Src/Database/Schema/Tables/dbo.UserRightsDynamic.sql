CREATE TABLE [dbo].[UserRightsDynamic]
(
[UserRightsDynamicID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelID] [int] NOT NULL,
[FunctionTypeID] [int] NOT NULL,
[LeadTypeID] [int] NULL,
[ObjectID] [int] NOT NULL,
[RightID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[UserRightsDynamic] ADD CONSTRAINT [PK_UserRightsDynamic] PRIMARY KEY CLUSTERED  ([UserRightsDynamicID])
GO
CREATE NONCLUSTERED INDEX [IX_UserRightsDynamicUser] ON [dbo].[UserRightsDynamic] ([ClientPersonnelID])
GO
CREATE NONCLUSTERED INDEX [IX_UserRightsDynamic_Covering2017] ON [dbo].[UserRightsDynamic] ([ClientPersonnelID]) INCLUDE ([FunctionTypeID], [LeadTypeID], [ObjectID], [RightID])
GO
ALTER TABLE [dbo].[UserRightsDynamic] ADD CONSTRAINT [FK_UserRightsDynamic_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[UserRightsDynamic] ADD CONSTRAINT [FK_UserRightsDynamic_FunctionType] FOREIGN KEY ([FunctionTypeID]) REFERENCES [dbo].[FunctionType] ([FunctionTypeID])
GO
ALTER TABLE [dbo].[UserRightsDynamic] ADD CONSTRAINT [FK_UserRightsDynamic_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
ALTER TABLE [dbo].[UserRightsDynamic] ADD CONSTRAINT [FK_UserRightsDynamic_Rights] FOREIGN KEY ([RightID]) REFERENCES [dbo].[Rights] ([RightID])
GO
