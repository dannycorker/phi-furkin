CREATE TABLE [dbo].[MigrationTrigger]
(
[MigrationTriggerID] [int] NOT NULL IDENTITY(1, 1),
[TableName] [varchar] (1024) COLLATE Latin1_General_CI_AS NOT NULL,
[DisableText] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[EnableText] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[IsEnabled] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[MigrationTrigger] ADD CONSTRAINT [PK_MigrationTrigger] PRIMARY KEY CLUSTERED  ([MigrationTriggerID])
GO
