CREATE TABLE [dbo].[HskLeadEventToDelete]
(
[LeadEventID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[CaseID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[InProcess] [bit] NOT NULL,
[IsComplete] [bit] NOT NULL,
[DocumentQueueID] [int] NULL,
[OtherUsageCount] [int] NULL,
[ClientStatusID] [int] NULL,
[AquariumEventTypeID] [int] NULL,
[AquariumStatusID] [int] NULL,
[CanBeDeleted] [bit] NULL,
[PreviousEventCount] [int] NULL,
[TopPreviousLeadEventID] [int] NULL,
[TopPreviousClientStatusID] [int] NULL,
[TopPreviousAquariumStatusID] [int] NULL,
[LETCDeleted] [bit] NULL,
[WorkflowDeleted] [bit] NULL,
[LatestCaseEventsSet] [bit] NULL,
[OtherPreviousEventsNulled] [bit] NULL,
[OtherPreviousEventsLETCCreated] [bit] NULL
)
GO
ALTER TABLE [dbo].[HskLeadEventToDelete] ADD CONSTRAINT [PK_HskLeadEventToDelete] PRIMARY KEY CLUSTERED  ([LeadEventID])
GO
CREATE NONCLUSTERED INDEX [IX_HskLeadEventToDelete_CaseEventType] ON [dbo].[HskLeadEventToDelete] ([CaseID], [EventTypeID], [IsComplete])
GO
CREATE NONCLUSTERED INDEX [IX_HskLeadEventToDelete_LeadEventCaseIsComplete] ON [dbo].[HskLeadEventToDelete] ([LeadEventID], [CaseID], [IsComplete])
GO
