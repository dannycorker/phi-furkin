CREATE TABLE [dbo].[AutomatedTask]
(
[TaskID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[Taskname] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Enabled] [bit] NOT NULL,
[RunAtHour] [int] NULL,
[RunAtMinute] [int] NULL,
[RepeatTimeUnitsID] [int] NULL,
[RepeatTimeQuantity] [int] NULL,
[NextRunDateTime] [datetime] NULL,
[WorkflowTask] [bit] NOT NULL CONSTRAINT [DF__Automated__Workf__3AA1AEB8] DEFAULT ((0)),
[AlreadyRunning] [bit] NULL,
[AutomatedTaskGroupID] [int] NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[MaximumAllowableErrors] [int] NULL,
[EventSubTypeThresholding] [bit] NOT NULL CONSTRAINT [DF__Automated__Event__39AD8A7F] DEFAULT ((0))
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2012-05-21
-- Description:	Store Automated Task Changes
-- JWG 2012-12-04 This seemed to be blocking the AutomatedTask table badly. Rewrite.
-- =============================================
CREATE TRIGGER [dbo].[trgu_AutomatedTask]
   ON [dbo].[AutomatedTask]
   AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END

	/* Populate deltas in the history table*/
	/*
	INSERT INTO AutomatedTaskHistory (
		TaskID, ClientID, Taskname, Description, Enabled, RunAtHour, RunAtMinute, RepeatTimeUnitsID,
		RepeatTimeQuantity, NextRunDateTime, WorkflowTask, AlreadyRunning, AutomatedTaskGroupID,
		SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified, MaximumAllowableErrors,
		EventSubTypeThresholding, ChangeAction, ChangeNotes)
	SELECT
		i.TaskID, i.ClientID, i.Taskname, NULL, i.Enabled, i.RunAtHour, i.RunAtMinute, i.RepeatTimeUnitsID,
		i.RepeatTimeQuantity, NULL, i.WorkflowTask, NULL, i.AutomatedTaskGroupID,
		i.SourceID, i.WhoCreated, i.WhenCreated, i.WhoModified, i.WhenModified, i.MaximumAllowableErrors,
		i.EventSubTypeThresholding,
		CASE WHEN d.TaskID IS NULL THEN 'Insert' WHEN i.TaskID IS NULL THEN 'Delete' ELSE 'Update' END,
		dbo.fnGetUserInfo()
	from inserted i
	inner join deleted d on i.TaskID = d.TaskID
		and (
			i.Taskname <> d.Taskname OR
			i.Enabled <> d.Enabled OR
			i.RunAtHour <> d.RunAtHour OR
			i.RunAtMinute <> d.RunAtMinute OR
			i.RepeatTimeUnitsID <> d.RepeatTimeUnitsID OR
			i.RepeatTimeQuantity <> d.RepeatTimeQuantity OR
			i.WorkflowTask <> d.WorkflowTask OR
			i.AutomatedTaskGroupID <> d.AutomatedTaskGroupID OR
			i.SourceID <> d.SourceID OR
			i.WhoModified <> d.WhoModified OR
			i.MaximumAllowableErrors <> d.MaximumAllowableErrors OR
			i.EventSubTypeThresholding <> d.EventSubTypeThresholding
		)
	*/
	IF UPDATE(Taskname)
	OR UPDATE(Enabled)
	OR UPDATE(RunAtHour)
	OR UPDATE(RunAtMinute)
	OR UPDATE(RepeatTimeUnitsID)
	OR UPDATE(RepeatTimeQuantity)
	OR UPDATE(WorkflowTask)
	OR UPDATE(AutomatedTaskGroupID)
	OR UPDATE(MaximumAllowableErrors)
	OR UPDATE(EventSubTypeThresholding)
	BEGIN
		INSERT INTO AutomatedTaskHistory (
			TaskID, ClientID, Taskname, Description, Enabled, RunAtHour, RunAtMinute, RepeatTimeUnitsID,
			RepeatTimeQuantity, NextRunDateTime, WorkflowTask, AlreadyRunning, AutomatedTaskGroupID,
			SourceID, WhoCreated, WhenCreated, WhoModified, WhenModified, MaximumAllowableErrors,
			EventSubTypeThresholding, ChangeAction, ChangeNotes)
		SELECT
			d.TaskID, d.ClientID, d.Taskname, NULL, d.Enabled, d.RunAtHour, d.RunAtMinute, d.RepeatTimeUnitsID,
			d.RepeatTimeQuantity, NULL, d.WorkflowTask, NULL, d.AutomatedTaskGroupID,
			d.SourceID, d.WhoCreated, d.WhenCreated, d.WhoModified, d.WhenModified, d.MaximumAllowableErrors,
			d.EventSubTypeThresholding,
			'Update',
			dbo.fnGetUserInfo()
		FROM deleted d
	END

END
GO
ALTER TABLE [dbo].[AutomatedTask] ADD CONSTRAINT [PK_AutomatedTask] PRIMARY KEY CLUSTERED  ([TaskID])
GO
CREATE NONCLUSTERED INDEX [IX_AutomatedTask_ClientEnabled] ON [dbo].[AutomatedTask] ([ClientID], [Enabled])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_AutomatedTask_SourceID] ON [dbo].[AutomatedTask] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[AutomatedTask] ADD CONSTRAINT [FK_AutomatedTask_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[AutomatedTask] ADD CONSTRAINT [FK_AutomatedTask_TimeUnits] FOREIGN KEY ([RepeatTimeUnitsID]) REFERENCES [dbo].[TimeUnits] ([TimeUnitsID])
GO
