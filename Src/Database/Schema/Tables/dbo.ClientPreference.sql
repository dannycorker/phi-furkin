CREATE TABLE [dbo].[ClientPreference]
(
[ClientPreferenceID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientPreferenceTypeID] [int] NOT NULL,
[PreferenceValue] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ClientPreference] ADD CONSTRAINT [PK_ClientPreference] PRIMARY KEY CLUSTERED  ([ClientPreferenceID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ClientPreference_ClientIDClientPreferenceTypeID] ON [dbo].[ClientPreference] ([ClientID], [ClientPreferenceTypeID])
GO
ALTER TABLE [dbo].[ClientPreference] ADD CONSTRAINT [FK_ClientPreference_ClientPreference] FOREIGN KEY ([ClientPreferenceTypeID]) REFERENCES [dbo].[ClientPreferenceType] ([ClientPreferenceTypeID])
GO
ALTER TABLE [dbo].[ClientPreference] ADD CONSTRAINT [FK_ClientPreference_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
