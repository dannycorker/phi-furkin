CREATE TABLE [dbo].[QuestionnaireFonts]
(
[QuestionnaireFontID] [int] NOT NULL IDENTITY(1, 1),
[ClientQuestionnaireID] [int] NOT NULL,
[PartNameID] [int] NULL,
[FontFamily] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FontSize] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FontColour] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FontWeight] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FontAlignment] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[QuestionnaireFonts] ADD CONSTRAINT [PK_QuestionnaireFonts] PRIMARY KEY CLUSTERED  ([QuestionnaireFontID])
GO
ALTER TABLE [dbo].[QuestionnaireFonts] ADD CONSTRAINT [FK_QuestionnaireFonts_ClientQuestionnaires] FOREIGN KEY ([ClientQuestionnaireID]) REFERENCES [dbo].[ClientQuestionnaires] ([ClientQuestionnaireID])
GO
ALTER TABLE [dbo].[QuestionnaireFonts] ADD CONSTRAINT [FK_QuestionnaireFonts_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[QuestionnaireFonts] ADD CONSTRAINT [FK_QuestionnaireFonts_QuestionnaireParts] FOREIGN KEY ([PartNameID]) REFERENCES [dbo].[QuestionnaireParts] ([PartNameID])
GO
