CREATE TABLE [dbo].[AquariumOption]
(
[AquariumOptionID] [int] NOT NULL,
[AquariumOptionTypeID] [int] NOT NULL,
[AquariumOptionName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[AquariumOptionDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[AquariumOption] ADD CONSTRAINT [PK_AquariumOption] PRIMARY KEY CLUSTERED  ([AquariumOptionID])
GO
ALTER TABLE [dbo].[AquariumOption] ADD CONSTRAINT [FK_AquariumOption_AquariumOptionType] FOREIGN KEY ([AquariumOptionTypeID]) REFERENCES [dbo].[AquariumOptionType] ([AquariumOptionTypeID])
GO
