CREATE TABLE [dbo].[API_AppAccess]
(
[AppAccessID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[AppKey] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ObjectKey] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[AccessLevel] [tinyint] NOT NULL,
[MaxByteSize] [int] NOT NULL CONSTRAINT [DF__API_AppAc__MaxBy__2C15FA44] DEFAULT ((10000000)),
[Enabled] [bit] NOT NULL,
[Config] [xml] NULL
)
GO
ALTER TABLE [dbo].[API_AppAccess] ADD CONSTRAINT [PK_API_AppAccess] PRIMARY KEY CLUSTERED  ([AppAccessID])
GO
