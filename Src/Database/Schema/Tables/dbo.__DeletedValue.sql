CREATE TABLE [dbo].[__DeletedValue]
(
[AnyID] [int] NOT NULL IDENTITY(1, 1),
[DetailFieldSubTypeID] [int] NOT NULL,
[DetailValueID] [int] NOT NULL,
[ParentID] [int] NOT NULL,
[DetailValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[WhenDeleted] [datetime] NOT NULL CONSTRAINT [DF____Deleted__WhenD__3492CE57] DEFAULT ([dbo].[fn_GetDate_Local]()),
[DetailFieldID] [int] NULL
)
GO
ALTER TABLE [dbo].[__DeletedValue] ADD CONSTRAINT [PK___DeletedValue] PRIMARY KEY CLUSTERED  ([AnyID])
GO
