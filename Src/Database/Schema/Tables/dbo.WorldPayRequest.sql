CREATE TABLE [dbo].[WorldPayRequest]
(
[WorldPayRequestID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[MatterID] [int] NOT NULL,
[RequestXml] [xml] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[WorldPayRequest] ADD CONSTRAINT [PK_WorldPayRequest] PRIMARY KEY CLUSTERED  ([WorldPayRequestID])
GO
