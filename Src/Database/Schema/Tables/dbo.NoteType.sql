CREATE TABLE [dbo].[NoteType]
(
[NoteTypeID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[NoteTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[NoteTypeDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[DefaultPriority] [smallint] NOT NULL CONSTRAINT [DF__NoteType__Defaul__789EE131] DEFAULT ((30)),
[AlertColour] [nchar] (7) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF__NoteType__AlertC__77AABCF8] DEFAULT ('Orange'),
[NormalColour] [nchar] (7) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF__NoteType__Normal__7993056A] DEFAULT ('#58839a'),
[Enabled] [bit] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[SourceID] [int] NULL,
[LeadTypeID] [int] NULL,
[IsShared] [bit] NULL
)
GO
ALTER TABLE [dbo].[NoteType] ADD CONSTRAINT [PK_NoteType] PRIMARY KEY CLUSTERED  ([NoteTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_NoteType1] ON [dbo].[NoteType] ([ClientID])
GO
