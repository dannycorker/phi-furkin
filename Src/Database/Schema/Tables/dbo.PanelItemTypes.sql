CREATE TABLE [dbo].[PanelItemTypes]
(
[PanelItemTypeID] [int] NOT NULL IDENTITY(1, 1),
[PanelItemTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PanelItemTypes] ADD CONSTRAINT [PK_PanelItemTypes] PRIMARY KEY CLUSTERED  ([PanelItemTypeID])
GO
ALTER TABLE [dbo].[PanelItemTypes] ADD CONSTRAINT [FK_PanelItemTypes_PanelItemTypes] FOREIGN KEY ([PanelItemTypeID]) REFERENCES [dbo].[PanelItemTypes] ([PanelItemTypeID])
GO
