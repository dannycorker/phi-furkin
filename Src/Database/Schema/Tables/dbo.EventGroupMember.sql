CREATE TABLE [dbo].[EventGroupMember]
(
[EventGroupMemberID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventGroupID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[EventGroupMemberDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[ChangeNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[SourceID] [int] NULL
)
GO
ALTER TABLE [dbo].[EventGroupMember] ADD CONSTRAINT [PK_EventGroupMember] PRIMARY KEY CLUSTERED  ([EventGroupMemberID])
GO
CREATE NONCLUSTERED INDEX [IX_EventGroupMember_SourceID] ON [dbo].[EventGroupMember] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[EventGroupMember] ADD CONSTRAINT [FK_EventGroupMember_EventGroup] FOREIGN KEY ([EventGroupID]) REFERENCES [dbo].[EventGroup] ([EventGroupID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
