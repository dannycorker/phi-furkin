CREATE TABLE [dbo].[EventChoice]
(
[EventChoiceID] [int] NOT NULL IDENTITY(1, 1),
[EventTypeID] [int] NOT NULL,
[Description] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[NextEventTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[ThreadNumber] [int] NOT NULL,
[EscalationEvent] [bit] NOT NULL,
[Field] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LogicalOperator] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Value1] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Value2] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SqlClauseForInclusion] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Weighting] [int] NULL,
[ValueTypeID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2013-04-18
-- Description:	Capture changes and store in History table
-- =============================================
CREATE TRIGGER [dbo].[trgiud_EventChoice]
   ON  [dbo].[EventChoice]
   AFTER INSERT,DELETE,UPDATE
AS
BEGIN
	SET NOCOUNT ON;

    -- Inserts appear in the inserted table,
    -- Deletes appear in the deleted table,
    -- and Updates appear in both tables at once identically.
    -- These are all 'logical' Sql Server tables.
	INSERT INTO dbo.EventChoiceHistory(
		EventChoiceID,
		EventTypeID,
		Description,
		NextEventTypeID,
		ClientID,
		LeadTypeID,
		ThreadNumber,
		EscalationEvent,
		Field,
		LogicalOperator,
		Value1,
		Value2,
		SqlClauseForInclusion,
		Weighting,
		WhoChanged,
		WhenChanged,
		ChangeAction,
		ChangeNotes
	)
	SELECT
		COALESCE(i.EventChoiceID, d.EventChoiceID),
		COALESCE(i.EventTypeID, d.EventTypeID),
		COALESCE(i.Description, d.Description),
		COALESCE(i.NextEventTypeID, d.NextEventTypeID),
		COALESCE(i.ClientID, d.ClientID),
		COALESCE(i.LeadTypeID, d.LeadTypeID),
		COALESCE(i.ThreadNumber, d.ThreadNumber),
		COALESCE(i.EscalationEvent, d.EscalationEvent),
		COALESCE(i.Field, d.Field),
		COALESCE(i.LogicalOperator, d.LogicalOperator),
		COALESCE(i.Value1, d.Value1),
		COALESCE(i.Value2, d.Value2),
		COALESCE(i.SqlClauseForInclusion, d.SqlClauseForInclusion),
		COALESCE(i.Weighting, d.Weighting),
		NULL,
		dbo.fn_GetDate_Local(),
		CASE WHEN d.EventTypeID IS NULL THEN 'Insert' WHEN i.EventTypeID IS NULL THEN 'Delete' ELSE 'Update' END,
		dbo.fnGetUserInfo()
	FROM inserted i
	FULL OUTER JOIN deleted d ON i.EventChoiceID = d.EventChoiceID

END
GO
ALTER TABLE [dbo].[EventChoice] ADD CONSTRAINT [PK_LeadEventChoice] PRIMARY KEY CLUSTERED  ([EventChoiceID])
GO
CREATE NONCLUSTERED INDEX [IX_EventChoice_EventTypeID] ON [dbo].[EventChoice] ([EventTypeID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_EventChoice_GetEventChoicesByCaseAndEvent2017] ON [dbo].[EventChoice] ([EventTypeID], [ThreadNumber], [EscalationEvent], [ClientID]) INCLUDE ([Description], [NextEventTypeID], [Weighting])
GO
CREATE NONCLUSTERED INDEX [IX_EventChoice_LeadTypeID] ON [dbo].[EventChoice] ([LeadTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_EventChoice_NextEventTypeID] ON [dbo].[EventChoice] ([NextEventTypeID], [ClientID])
GO
ALTER TABLE [dbo].[EventChoice] ADD CONSTRAINT [FK_EventChoice_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[EventChoice] ADD CONSTRAINT [FK_EventChoice_EventChoice] FOREIGN KEY ([ValueTypeID]) REFERENCES [dbo].[AquariumOption] ([AquariumOptionID])
GO
ALTER TABLE [dbo].[EventChoice] ADD CONSTRAINT [FK_EventChoice_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EventChoice] ADD CONSTRAINT [FK_LeadEventChoice_LeadEventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[EventChoice] ADD CONSTRAINT [FK_LeadEventChoice_LeadEventType_Choices] FOREIGN KEY ([NextEventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
