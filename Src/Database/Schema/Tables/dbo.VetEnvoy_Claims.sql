CREATE TABLE [dbo].[VetEnvoy_Claims]
(
[ClaimID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[AccountID] [int] NOT NULL,
[ConversationID] [uniqueidentifier] NOT NULL,
[Received] [datetime] NOT NULL,
[SenderID] [uniqueidentifier] NOT NULL,
[Xml] [xml] NOT NULL,
[PolicyRef] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[LeadID] [int] NULL,
[MatterIDs] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[VetEnvoy_Claims] ADD CONSTRAINT [PK_VetEnvoy_Claims] PRIMARY KEY CLUSTERED  ([ClaimID])
GO
CREATE NONCLUSTERED INDEX [IX_VetEnvoy_Claims] ON [dbo].[VetEnvoy_Claims] ([ConversationID])
GO
ALTER TABLE [dbo].[VetEnvoy_Claims] ADD CONSTRAINT [FK_VetEnvoy_Claims_VetEnvoy_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[VetEnvoy_Accounts] ([AccountID])
GO
