CREATE TABLE [dbo].[ClientPersonnelOptions]
(
[ClientPersonnelOptionID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelID] [int] NOT NULL,
[ClientPersonnelOptionTypeID] [int] NOT NULL,
[OptionValue] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2012-11-30
-- Description:	Remove duplicate values at source (these are being added by UserOptions.aspx.cs)
-- =============================================
CREATE TRIGGER [dbo].[trgi_ClientPersonnelOptions]
   ON [dbo].[ClientPersonnelOptions]
   AFTER INSERT
AS
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END

	/* The app is inserting rather than updating, so delete any old values for the same user and optiontype */
	DELETE dbo.ClientPersonnelOptions
	FROM dbo.ClientPersonnelOptions cpo
	INNER JOIN inserted i ON i.ClientPersonnelID = cpo.ClientPersonnelID
		AND i.ClientPersonnelOptionTypeID = cpo.ClientPersonnelOptionTypeID
		AND i.ClientPersonnelOptionID > cpo.ClientPersonnelOptionID

END
GO
ALTER TABLE [dbo].[ClientPersonnelOptions] ADD CONSTRAINT [PK_ClientPersonnelOptions] PRIMARY KEY CLUSTERED  ([ClientPersonnelOptionID])
GO
ALTER TABLE [dbo].[ClientPersonnelOptions] ADD CONSTRAINT [FK_ClientPersonnelOptions_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[ClientPersonnelOptions] ADD CONSTRAINT [FK_ClientPersonnelOptions_ClientPersonnelOptionTypes] FOREIGN KEY ([ClientPersonnelOptionTypeID]) REFERENCES [dbo].[ClientPersonnelOptionTypes] ([ClientPersonnelOptionTypeID])
GO
ALTER TABLE [dbo].[ClientPersonnelOptions] ADD CONSTRAINT [FK_ClientPersonnelOptions_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
