CREATE TABLE [dbo].[PurchasedProductPaymentScheduleType]
(
[PurchasedProductPaymentScheduleTypeID] [int] NOT NULL IDENTITY(1, 1),
[PurchasedProductPaymentScheduleTypeName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[ClientAccountID] [int] NULL
)
GO
ALTER TABLE [dbo].[PurchasedProductPaymentScheduleType] ADD CONSTRAINT [PK_PurchasedProductPaymentScheduleType] PRIMARY KEY CLUSTERED  ([PurchasedProductPaymentScheduleTypeID])
GO
