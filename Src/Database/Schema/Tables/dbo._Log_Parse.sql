CREATE TABLE [dbo].[_Log_Parse]
(
[LogParseID] [int] NOT NULL IDENTITY(1, 1),
[LogDateTime] [datetime] NOT NULL,
[LogEntry] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[QueryID] [int] NULL,
[SqtID] [int] NULL
)
GO
ALTER TABLE [dbo].[_Log_Parse] ADD CONSTRAINT [PK___Log_Par__153910D339FA0547] PRIMARY KEY CLUSTERED  ([LogParseID])
GO
