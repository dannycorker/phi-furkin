CREATE TABLE [dbo].[Customers]
(
[CustomerID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[TitleID] [int] NULL,
[IsBusiness] [bit] NOT NULL,
[FirstName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[MiddleName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[EmailAddress] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[DayTimeTelephoneNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DayTimeTelephoneNumberVerifiedAndValid] [bit] NULL,
[HomeTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[HomeTelephoneVerifiedAndValid] [bit] NULL,
[MobileTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MobileTelephoneVerifiedAndValid] [bit] NULL,
[CompanyTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CompanyTelephoneVerifiedAndValid] [bit] NULL,
[WorksTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[WorksTelephoneVerifiedAndValid] [bit] NULL,
[Address1] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Address2] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Town] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[County] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[PostCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Website] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[HasDownloaded] [bit] NULL,
[DownloadedOn] [datetime] NULL,
[AquariumStatusID] [int] NOT NULL CONSTRAINT [DF__Customers__AquariumStatusID] DEFAULT ((2)),
[ClientStatusID] [int] NULL,
[Test] [bit] NULL CONSTRAINT [DF__Customers__Test__5090EFD7] DEFAULT ((0)),
[CompanyName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Occupation] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Employer] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Fullname] AS (case [isbusiness] when (1) then [companyname] else (isnull([FirstName],'')+' ')+isnull([LastName],'') end),
[PhoneNumbersVerifiedOn] [datetime] NULL,
[DoNotEmail] [bit] NULL CONSTRAINT [DF__Customers__DoNot__4EA8A765] DEFAULT ((0)),
[DoNotSellToThirdParty] [bit] NULL CONSTRAINT [DF__Customers__DoNot__4F9CCB9E] DEFAULT ((0)),
[AgreedToTermsAndConditions] [bit] NULL,
[DateOfBirth] [datetime] NULL,
[DefaultContactID] [int] NULL,
[DefaultOfficeID] [int] NULL,
[AddressVerified] [bit] NULL,
[CountryID] [int] NULL CONSTRAINT [DF__Customers__Count__31B661AC] DEFAULT ((233)),
[SubClientID] [int] NULL,
[CustomerRef] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[WhoChanged] [int] NULL,
[WhenChanged] [datetime] NULL,
[ChangeSource] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[EmailAddressVerifiedAndValid] [bit] NULL,
[EmailAddressVerifiedOn] [datetime] NULL,
[Comments] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[AllowSmsCommandProcessing] [bit] NULL,
[LanguageID] [int] NULL,
[Longitude] [numeric] (25, 18) NULL,
[Latitude] [numeric] (25, 18) NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2008-01-01
-- Description:	Prevent accidental deletes
-- =============================================
CREATE TRIGGER [dbo].[trgd_Customers]
   ON  [dbo].[Customers]
   AFTER DELETE
AS
BEGIN
	SET NOCOUNT ON;

	-- Allow special user 'notriggers' to bypass trigger code
	IF system_user = 'notriggers' OR CAST(CONTEXT_INFO() AS VARCHAR) = 'DPADelete'
	BEGIN
		RETURN
	END

    -- Insert statements for trigger here
	ROLLBACK

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2012-01-25
-- Description:	Capture changes and store in History table
--				Customers cannot be deleted, so this trigger is a bit different to the rest (cf trgiud_DetailFieldPages)
-- =============================================
CREATE TRIGGER [dbo].[trgiu_Customers]
   ON  [dbo].[Customers]
   AFTER INSERT,UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	-- Allow special user 'notriggers' to bypass trigger code
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END

	/*
		Inserts and updates both appear in the inserted table, which is a 'logical' Sql Server table.
		Deletes are not permitted.
    */
	INSERT INTO dbo.CustomerHistory(
		CustomerID,
		ClientID,
		TitleID,
		IsBusiness,
		FirstName,
		MiddleName,
		LastName,
		EmailAddress,
		DayTimeTelephoneNumber,
		HomeTelephone,
		MobileTelephone,
		CompanyTelephone,
		WorksTelephone,
		Address1,
		Address2,
		Town,
		County,
		PostCode,
		Website,
		AquariumStatusID,
		ClientStatusID,
		Test,
		CompanyName,
		Occupation,
		Employer,
		DoNotEmail,
		DoNotSellToThirdParty,
		AgreedToTermsAndConditions,
		DateOfBirth,
		DefaultContactID,
		DefaultOfficeID,
		AddressVerified,
		CountryID,
		SubClientID,
		CustomerRef,
		WhoChanged,
		WhenChanged,
		ChangeAction,
		ChangeNotes,
		Comments
	)
	SELECT
		i.CustomerID,
		i.ClientID,
		i.TitleID,
		i.IsBusiness,
		i.FirstName,
		i.MiddleName,
		i.LastName,
		i.EmailAddress,
		i.DayTimeTelephoneNumber,
		i.HomeTelephone,
		i.MobileTelephone,
		i.CompanyTelephone,
		i.WorksTelephone,
		i.Address1,
		i.Address2,
		i.Town,
		i.County,
		i.PostCode,
		i.Website,
		i.AquariumStatusID,
		i.ClientStatusID,
		i.Test,
		i.CompanyName,
		i.Occupation,
		i.Employer,
		i.DoNotEmail,
		i.DoNotSellToThirdParty,
		i.AgreedToTermsAndConditions,
		i.DateOfBirth,
		i.DefaultContactID,
		i.DefaultOfficeID,
		i.AddressVerified,
		i.CountryID,
		i.SubClientID,
		i.CustomerRef,
		i.WhoChanged,
		ISNULL(i.WhenChanged, dbo.fn_GetDate_Local()),
		CASE WHEN d.CustomerID IS NULL THEN 'Insert' WHEN i.CustomerID IS NULL THEN 'Delete' ELSE 'Update' END,
		dbo.fnGetUserInfo() + CASE WHEN i.ChangeSource IS NULL THEN '' ELSE ' : ' + i.ChangeSource END, /* Record which app/class/method was used to make the change */
		i.Comments
	FROM inserted i
	FULL OUTER JOIN deleted d ON i.CustomerID = d.CustomerID

END
GO
ALTER TABLE [dbo].[Customers] ADD CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED  ([CustomerID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomersAquariumStatus] ON [dbo].[Customers] ([AquariumStatusID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomersClient] ON [dbo].[Customers] ([ClientID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_Customers_ClientCompanyNameTest] ON [dbo].[Customers] ([ClientID], [CompanyName], [Test]) WHERE ([CompanyName]>'')
GO
CREATE NONCLUSTERED INDEX [IX_Customers_ClientTest] ON [dbo].[Customers] ([ClientID], [Test]) INCLUDE ([SubClientID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomersEmail] ON [dbo].[Customers] ([EmailAddress])
GO
CREATE NONCLUSTERED INDEX [IX_CustomersLastName] ON [dbo].[Customers] ([LastName])
GO
CREATE NONCLUSTERED INDEX [IX_Customers_MobileClientTest] ON [dbo].[Customers] ([MobileTelephone], [ClientID], [Test], [AquariumStatusID]) INCLUDE ([CompanyTelephone], [DayTimeTelephoneNumber], [HomeTelephone], [WorksTelephone])
GO
CREATE NONCLUSTERED INDEX [IX_CustomersPostcode] ON [dbo].[Customers] ([PostCode])
GO
CREATE NONCLUSTERED INDEX [IX_CustomersTest] ON [dbo].[Customers] ([Test])
GO
ALTER TABLE [dbo].[Customers] ADD CONSTRAINT [FK_Customers_AquariumStatus] FOREIGN KEY ([AquariumStatusID]) REFERENCES [dbo].[AquariumStatus] ([AquariumStatusID])
GO
ALTER TABLE [dbo].[Customers] ADD CONSTRAINT [FK_Customers_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID]) ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Customers] ADD CONSTRAINT [FK_Customers_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
ALTER TABLE [dbo].[Customers] ADD CONSTRAINT [FK_Customers_CustomerOffice] FOREIGN KEY ([DefaultOfficeID]) REFERENCES [dbo].[CustomerOffice] ([OfficeID])
GO
ALTER TABLE [dbo].[Customers] ADD CONSTRAINT [FK_Customers_Titles] FOREIGN KEY ([TitleID]) REFERENCES [dbo].[Titles] ([TitleID])
GO
