CREATE TABLE [dbo].[PremiumFunderResponse]
(
[PremiumFunderResponseID] [int] NOT NULL IDENTITY(1, 1),
[PremiumFunderRequestID] [int] NULL,
[ClientID] [int] NOT NULL,
[CustomerID] [int] NULL,
[MatterID] [int] NULL,
[ResponseJson] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[PremiumFunderResponse] ADD CONSTRAINT [PK_PremiumFunderResponse] PRIMARY KEY CLUSTERED  ([PremiumFunderResponseID])
GO
