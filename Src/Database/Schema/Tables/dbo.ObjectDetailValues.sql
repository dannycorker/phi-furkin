CREATE TABLE [dbo].[ObjectDetailValues]
(
[ObjectDetailValueID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[ObjectID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[DetailValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[ErrorMsg] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[EncryptedValue] [varchar] (3000) COLLATE Latin1_General_CI_AS NULL,
[ValueInt] [int] NULL,
[ValueMoney] [money] NULL,
[ValueDate] [date] NULL,
[ValueDateTime] [datetime2] (0) NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2011-06-23
-- Description:	Store DetailValue as specific datatypes
-- 2011-07-13 JWG Remove duplicates instantly at source
-- =============================================
CREATE TRIGGER [dbo].[trgiu_ObjectDetailValues]
   ON [dbo].[ObjectDetailValues]
   AFTER INSERT, UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END


    /*
		If the DetailValue column has been updated then fire the trigger,
		otherwise we are here again because the trigger has just fired,
		in which case do nothing.
	*/
    IF UPDATE(DetailValue)
    BEGIN
		/* Check for duplicates (but only on insert, not update) */
		IF NOT EXISTS (SELECT * FROM deleted)
		BEGIN
			/* Only issue the delete if we know that any records exist, otherwise the delete operation gets attempted every time regardless */
			IF EXISTS(SELECT * FROM dbo.ObjectDetailValues dv WITH (NOLOCK) INNER JOIN inserted i ON i.ObjectID = dv.ObjectID AND i.DetailFieldID = dv.DetailFieldID AND i.ObjectDetailValueID > dv.ObjectDetailValueID )
			BEGIN
				/* Remove any older duplicates for this field */
				DELETE dbo.ObjectDetailValues
				OUTPUT 15, deleted.ObjectDetailValueID, deleted.ObjectID, deleted.DetailValue, deleted.DetailFieldID
				INTO dbo.__DeletedValue (DetailFieldSubTypeID, DetailValueID, ParentID, DetailValue, DetailFieldID)
				FROM dbo.ObjectDetailValues dv
				INNER JOIN inserted i ON i.ObjectID = dv.ObjectID AND i.DetailFieldID = dv.DetailFieldID AND i.ObjectDetailValueID > dv.ObjectDetailValueID
			END
		END

		/* Populate ValueInt et al */
		UPDATE dbo.ObjectDetailValues
		SET ValueInt = dbo.fnValueAsIntBitsIncluded(i.DetailValue) /*CASE WHEN dbo.fnIsInt(i.detailvalue) = 1 THEN convert(int, i.DetailValue) ELSE NULL END*/,
		ValueMoney = CASE WHEN dbo.fnIsMoney(i.DetailValue) = 1 THEN convert(money, i.DetailValue) ELSE NULL END,
		ValueDate = CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN convert(date, i.DetailValue) ELSE NULL END,
		ValueDateTime = CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN convert(datetime2, i.DetailValue) ELSE NULL END
		FROM dbo.ObjectDetailValues dv
		INNER JOIN inserted i ON i.ObjectDetailValueID = dv.ObjectDetailValueID
	END


END

GO
ALTER TABLE [dbo].[ObjectDetailValues] ADD CONSTRAINT [PK_ObjectDetailValues] PRIMARY KEY CLUSTERED  ([ObjectDetailValueID])
GO
CREATE NONCLUSTERED INDEX [IX_ObjectDetailValues_Client] ON [dbo].[ObjectDetailValues] ([ClientID], [SubClientID]) INCLUDE ([ObjectID])
GO
CREATE NONCLUSTERED INDEX [IX_ObjectDetailValuesDetailField] ON [dbo].[ObjectDetailValues] ([DetailFieldID]) INCLUDE ([ObjectID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_ObjectDetailValues_FieldValueDate] ON [dbo].[ObjectDetailValues] ([DetailFieldID], [ValueDate], [ClientID], [SubClientID]) INCLUDE ([ObjectID]) WHERE ([ValueDate] IS NOT NULL)
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_ObjectDetailValues_FieldValueInt] ON [dbo].[ObjectDetailValues] ([DetailFieldID], [ValueInt], [ClientID], [SubClientID]) INCLUDE ([ObjectID]) WHERE ([ValueInt] IS NOT NULL)
GO
CREATE NONCLUSTERED INDEX [IX_ObjectDetailValues_ObjectAndField] ON [dbo].[ObjectDetailValues] ([ObjectID], [DetailFieldID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_ObjectDetailValues_ObjectFieldValueInt] ON [dbo].[ObjectDetailValues] ([ObjectID], [DetailFieldID], [ValueInt], [ClientID], [SubClientID]) WHERE ([ValueInt] IS NOT NULL)
GO
