CREATE TABLE [dbo].[PageGroupDefaultPageLink]
(
[PageGroupDefaultPageLinkID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[PageGroupID] [int] NOT NULL,
[DetailFieldPageID] [int] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[PageGroupDefaultPageLink] ADD CONSTRAINT [PK_PageGroupDefaultPageLink] PRIMARY KEY CLUSTERED  ([PageGroupDefaultPageLinkID])
GO
ALTER TABLE [dbo].[PageGroupDefaultPageLink] ADD CONSTRAINT [FK_PageGroupDefaultPageLink_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[PageGroupDefaultPageLink] ADD CONSTRAINT [FK_PageGroupDefaultPageLink_DetailFieldPages] FOREIGN KEY ([DetailFieldPageID]) REFERENCES [dbo].[DetailFieldPages] ([DetailFieldPageID])
GO
ALTER TABLE [dbo].[PageGroupDefaultPageLink] ADD CONSTRAINT [FK_PageGroupDefaultPageLink_PageGroup] FOREIGN KEY ([PageGroupID]) REFERENCES [dbo].[PageGroup] ([PageGroupID])
GO
