CREATE TABLE [dbo].[ScriptPositionTarget]
(
[ScriptPositionTargetID] [int] NOT NULL IDENTITY(1, 1),
[ScriptID] [int] NOT NULL,
[SectionID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[PositionName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[LinkText] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ScriptPositionTarget] ADD CONSTRAINT [PK_ScriptPositionTarget] PRIMARY KEY CLUSTERED  ([ScriptPositionTargetID])
GO
ALTER TABLE [dbo].[ScriptPositionTarget] ADD CONSTRAINT [FK_ScriptPositionTarget_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ScriptPositionTarget] ADD CONSTRAINT [FK_ScriptPositionTarget_Script] FOREIGN KEY ([ScriptID]) REFERENCES [dbo].[Script] ([ScriptID])
GO
ALTER TABLE [dbo].[ScriptPositionTarget] ADD CONSTRAINT [FK_ScriptPositionTarget_ScriptSection] FOREIGN KEY ([SectionID]) REFERENCES [dbo].[ScriptSection] ([ScriptSectionID])
GO
