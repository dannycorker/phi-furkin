CREATE TABLE [dbo].[ExclusionType]
(
[ExclusionTypeID] [int] NOT NULL,
[IsEnabled] [bit] NOT NULL,
[ExclusionTypeName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[ExclusionTypeDescription] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[ExclusionType] ADD CONSTRAINT [PK_ExclusionType] PRIMARY KEY CLUSTERED  ([ExclusionTypeID])
GO
