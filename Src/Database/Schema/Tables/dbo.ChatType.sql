CREATE TABLE [dbo].[ChatType]
(
[ChatTypeID] [tinyint] NOT NULL,
[ChatType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ChatType] ADD CONSTRAINT [PK_ChatType] PRIMARY KEY CLUSTERED  ([ChatTypeID])
GO
