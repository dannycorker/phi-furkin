CREATE TABLE [dbo].[ScriptRule]
(
[ScriptRuleID] [int] NOT NULL IDENTITY(1, 1),
[ScriptID] [int] NOT NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ScriptRule] ADD CONSTRAINT [PK_ScriptRule] PRIMARY KEY CLUSTERED  ([ScriptRuleID])
GO
