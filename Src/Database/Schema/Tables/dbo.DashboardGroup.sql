CREATE TABLE [dbo].[DashboardGroup]
(
[DashboardGroupID] [int] NOT NULL IDENTITY(1, 1),
[GroupName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL,
[ClientPersonnelAdminGroupID] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoCreated] [int] NULL,
[WhenModified] [datetime] NULL,
[WhoModified] [int] NULL
)
GO
ALTER TABLE [dbo].[DashboardGroup] ADD CONSTRAINT [PK_DashboardGroup] PRIMARY KEY CLUSTERED  ([DashboardGroupID])
GO
