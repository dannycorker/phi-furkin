CREATE TABLE [dbo].[Timing]
(
[TimingID] [int] NOT NULL IDENTITY(1, 1),
[TimingObject] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[TimingStart] [datetime] NOT NULL,
[TimingMS] [int] NOT NULL,
[ID1] [int] NULL,
[ID1Desc] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ID2] [int] NULL,
[ID2Desc] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Timing] ADD CONSTRAINT [PK_Timing] PRIMARY KEY CLUSTERED  ([TimingID])
GO
