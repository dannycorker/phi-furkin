CREATE TABLE [dbo].[RulesEngine_RuleSetTypeID]
(
[RulesEngine_RuleSetTypeID] [int] NOT NULL IDENTITY(1, 1),
[RuleSetType] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[RulesEngine_RuleSetTypeID] ADD CONSTRAINT [PK_RulesEngine_RuleSetTypeID] PRIMARY KEY CLUSTERED  ([RulesEngine_RuleSetTypeID])
GO
