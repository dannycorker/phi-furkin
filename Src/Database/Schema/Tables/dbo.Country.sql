CREATE TABLE [dbo].[Country]
(
[CountryID] [int] NOT NULL,
[CountryName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[CountryNamePlain] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Alpha2Code] [char] (2) COLLATE Latin1_General_CI_AS NOT NULL,
[Alpha3Code] [char] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[NumericCode] [smallint] NOT NULL,
[ISO3166Dash2Code] [char] (13) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[UsePostcode] [bit] NULL,
[PostcodeRegex] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[DiallingCode] [varchar] (4) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Country] ADD CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED  ([CountryID])
GO
CREATE NONCLUSTERED INDEX [IX_Country_Alpha2Code] ON [dbo].[Country] ([Alpha2Code])
GO
CREATE NONCLUSTERED INDEX [IX_Country_Alpha3Code] ON [dbo].[Country] ([Alpha3Code])
GO
