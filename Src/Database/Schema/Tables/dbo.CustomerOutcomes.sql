CREATE TABLE [dbo].[CustomerOutcomes]
(
[CustomerOutcomeID] [int] NOT NULL IDENTITY(1, 1),
[ClientQuestionnaireID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[OutcomeID] [int] NOT NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[CustomerOutcomes] ADD CONSTRAINT [PK_CustomerOutcomes] PRIMARY KEY CLUSTERED  ([CustomerOutcomeID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerOutcomes_Client] ON [dbo].[CustomerOutcomes] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerOutcomes_ClientQuestionnaire] ON [dbo].[CustomerOutcomes] ([ClientQuestionnaireID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerOutcomes_Customer] ON [dbo].[CustomerOutcomes] ([CustomerID], [OutcomeID])
GO
ALTER TABLE [dbo].[CustomerOutcomes] ADD CONSTRAINT [FK_CustomerOutcomes_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[CustomerOutcomes] ADD CONSTRAINT [FK_CustomerOutcomes_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[CustomerOutcomes] ADD CONSTRAINT [FK_CustomerOutcomes_Outcomes] FOREIGN KEY ([OutcomeID]) REFERENCES [dbo].[Outcomes] ([OutcomeID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
