CREATE TABLE [dbo].[MatterDetailValues2]
(
[MatterDetailValueID] [int] NOT NULL IDENTITY(-2147483647, 1),
[ClientID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[MatterID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[DetailValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[ErrorMsg] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[OriginalDetailValueID] [int] NULL,
[OriginalLeadID] [int] NULL,
[EncryptedValue] [varchar] (3000) COLLATE Latin1_General_CI_AS NULL,
[ValueInt] [int] NULL,
[ValueMoney] [money] NULL,
[ValueDate] [date] NULL,
[ValueDateTime] [datetime2] (0) NULL,
[SourceID] [int] NULL
)
GO
CREATE NONCLUSTERED INDEX [IX_MatterDetailValues2_MatterAndFieldShort] ON [dbo].[MatterDetailValues2] ([MatterID], [DetailFieldID])
GO
