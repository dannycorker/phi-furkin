CREATE TABLE [dbo].[SqlQueryEditingCriteria]
(
[SqlQueryEditingCriteriaID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SqlQueryEditingID] [int] NOT NULL,
[SqlQueryEditingTableID] [int] NULL,
[SqlQueryEditingColumnID] [int] NULL,
[CriteriaText] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[Criteria1] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Criteria2] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[CriteriaName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[SubQueryID] [int] NULL,
[SubQueryLinkType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ParamValue] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[IsSecurityClause] [bit] NOT NULL,
[CriteriaSubstitutions] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[IsParameterizable] [bit] NULL,
[Comparison] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[IsJoinClause] [bit] NULL
)
GO
ALTER TABLE [dbo].[SqlQueryEditingCriteria] ADD CONSTRAINT [PK_SqlQueryEditingCriteria] PRIMARY KEY CLUSTERED  ([SqlQueryEditingCriteriaID])
GO
CREATE NONCLUSTERED INDEX [IX_SqlQueryEditingCriteria_Query] ON [dbo].[SqlQueryEditingCriteria] ([SqlQueryEditingID])
GO
ALTER TABLE [dbo].[SqlQueryEditingCriteria] ADD CONSTRAINT [FK_SqlQueryEditingCriteria_SqlQueryEditing] FOREIGN KEY ([SqlQueryEditingID]) REFERENCES [dbo].[SqlQueryEditing] ([SqlQueryEditingID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SqlQueryEditingCriteria] ADD CONSTRAINT [FK_SqlQueryEditingCriteria_SqlQueryEditingColumn] FOREIGN KEY ([SqlQueryEditingColumnID]) REFERENCES [dbo].[SqlQueryEditingColumns] ([SqlQueryEditingColumnID])
GO
