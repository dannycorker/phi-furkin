CREATE TABLE [dbo].[DataLoaderDataType]
(
[DataLoaderDataTypeID] [int] NOT NULL,
[DataTypeName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[DataTypeDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[DataLoaderDataType] ADD CONSTRAINT [PK_DataLoaderDataType] PRIMARY KEY CLUSTERED  ([DataLoaderDataTypeID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DataLoaderDataTypeName] ON [dbo].[DataLoaderDataType] ([DataTypeName])
GO
