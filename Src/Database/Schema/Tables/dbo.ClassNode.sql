CREATE TABLE [dbo].[ClassNode]
(
[ClassNodeID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ParentClassNodeID] [int] NULL,
[NodeType] [int] NOT NULL,
[Name] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ClassDescription] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[NodeOrder] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[Deleted] [bit] NULL
)
GO
ALTER TABLE [dbo].[ClassNode] ADD CONSTRAINT [PK__ClassNod__DEAB887E5C33120F] PRIMARY KEY CLUSTERED  ([ClassNodeID])
GO
ALTER TABLE [dbo].[ClassNode] ADD CONSTRAINT [FK__ClassNode__Clien__5E1B5A81] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ClassNode] ADD CONSTRAINT [FK__ClassNode__Paren__5F0F7EBA] FOREIGN KEY ([ParentClassNodeID]) REFERENCES [dbo].[ClassNode] ([ClassNodeID])
GO
ALTER TABLE [dbo].[ClassNode] ADD CONSTRAINT [FK__ClassNode__WhoCr__6003A2F3] FOREIGN KEY ([WhoCreated]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[ClassNode] ADD CONSTRAINT [FK__ClassNode__WhoMo__60F7C72C] FOREIGN KEY ([WhoModified]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
