CREATE TABLE [dbo].[Messages]
(
[MessageID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelIDTo] [int] NULL,
[ClientPersonnelIDFrom] [int] NULL,
[DateSent] [datetime] NOT NULL,
[DateRead] [datetime] NULL,
[Status] [int] NULL CONSTRAINT [DF__Messages__Status__76B698BF] DEFAULT ((1)),
[Subject] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[MessageText] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[DateReplied] [datetime] NULL,
[DateForwarded] [datetime] NULL,
[PreviousMessageID] [int] NULL,
[ClientID] [int] NOT NULL,
[PortalUserIDTo] [int] NULL,
[PortalUserIDFrom] [int] NULL
)
GO
ALTER TABLE [dbo].[Messages] ADD CONSTRAINT [PK_Messages] PRIMARY KEY CLUSTERED  ([MessageID])
GO
ALTER TABLE [dbo].[Messages] ADD CONSTRAINT [FK_Messages_ClientPersonnel_From] FOREIGN KEY ([ClientPersonnelIDFrom]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[Messages] ADD CONSTRAINT [FK_Messages_ClientPersonnel_To] FOREIGN KEY ([ClientPersonnelIDTo]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[Messages] ADD CONSTRAINT [FK_Messages_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[Messages] ADD CONSTRAINT [FK_Messages_PortalUser] FOREIGN KEY ([PortalUserIDTo]) REFERENCES [dbo].[PortalUser] ([PortalUserID])
GO
ALTER TABLE [dbo].[Messages] ADD CONSTRAINT [FK_Messages_PortalUser1] FOREIGN KEY ([PortalUserIDFrom]) REFERENCES [dbo].[PortalUser] ([PortalUserID])
GO
