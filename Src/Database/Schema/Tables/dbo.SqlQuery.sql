CREATE TABLE [dbo].[SqlQuery]
(
[QueryID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[QueryText] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[QueryTitle] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[AutorunOnline] [tinyint] NOT NULL CONSTRAINT [DF__SqlQuery__Autoru__004002F9] DEFAULT ((0)),
[OnlineLimit] [int] NOT NULL CONSTRAINT [DF__SqlQuery__Online__0CA5D9DE] DEFAULT ((0)),
[BatchLimit] [int] NOT NULL CONSTRAINT [DF__SqlQuery__BatchL__031C6FA4] DEFAULT ((0)),
[SqlQueryTypeID] [int] NULL,
[FolderID] [int] NULL,
[IsEditable] [bit] NOT NULL CONSTRAINT [DF__SqlQuery__IsEdit__05F8DC4F] DEFAULT ((0)),
[IsTemplate] [bit] NOT NULL CONSTRAINT [DF__SqlQuery__IsTemp__07E124C1] DEFAULT ((0)),
[IsDeleted] [bit] NOT NULL CONSTRAINT [DF__SqlQuery__IsDele__0504B816] DEFAULT ((0)),
[WhenCreated] [datetime] NOT NULL CONSTRAINT [DF__SqlQuery__WhenCr__41ECC975] DEFAULT ([dbo].[fn_GetDate_Local]()),
[CreatedBy] [int] NOT NULL CONSTRAINT [DF__SqlQuery__Create__041093DD] DEFAULT ((3)),
[OwnedBy] [int] NOT NULL CONSTRAINT [DF__SqlQuery__OwnedB__0D99FE17] DEFAULT ((3)),
[RunCount] [int] NOT NULL CONSTRAINT [DF__SqlQuery__RunCou__0E8E2250] DEFAULT ((0)),
[LastRundate] [datetime] NULL,
[LastRuntime] [int] NOT NULL CONSTRAINT [DF__SqlQuery__LastRu__09C96D33] DEFAULT ((0)),
[LastRowcount] [int] NOT NULL CONSTRAINT [DF__SqlQuery__LastRo__08D548FA] DEFAULT ((0)),
[MaxRuntime] [int] NOT NULL CONSTRAINT [DF__SqlQuery__MaxRun__0BB1B5A5] DEFAULT ((0)),
[MaxRowcount] [int] NOT NULL CONSTRAINT [DF__SqlQuery__MaxRow__0ABD916C] DEFAULT ((0)),
[AvgRuntime] [int] NOT NULL CONSTRAINT [DF__SqlQuery__AvgRun__02284B6B] DEFAULT ((0)),
[AvgRowcount] [int] NOT NULL CONSTRAINT [DF__SqlQuery__AvgRow__01342732] DEFAULT ((0)),
[Comments] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[WhenModified] [datetime] NULL,
[ModifiedBy] [int] NULL,
[LeadTypeID] [int] NULL,
[ParentQueryID] [int] NULL,
[IsParent] [bit] NOT NULL CONSTRAINT [DF__SqlQuery__IsPare__06ED0088] DEFAULT ((0)),
[SqlQueryTemplateID] [int] NULL,
[OutputFormat] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[ShowInCustomSearch] [bit] NULL,
[OutputFileExtension] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[OutputSeparatorCharmapID] [tinyint] NULL,
[OutputEncapsulatorCharmapID] [tinyint] NULL,
[SuppressHeaderRow] [bit] NULL,
[LockAllTables] [bit] NULL,
[SourceID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2008-06-01
-- Description:	Prevent accidental deletes
-- =============================================
CREATE TRIGGER [dbo].[trgd_SqlQuery]
   ON  [dbo].[SqlQuery]
   AFTER DELETE
AS
BEGIN
	SET NOCOUNT ON;

	---- Allow special user 'notriggers' to bypass trigger code
	--IF system_user = 'notriggers'
	--BEGIN
	--	RETURN
	--END

 --   -- Deny the delete
	--ROLLBACK

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2009-10-03
-- Description:	Capture changes and store in History table
-- =============================================
CREATE TRIGGER [dbo].[trgiud_SqlQuery]
   ON  [dbo].[SqlQuery]
   AFTER INSERT,DELETE,UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END

    -- Inserts appear in the inserted table,
    -- Deletes appear in the deleted table,
    -- and Updates appear in both tables at once identically.
    -- These are all 'logical' Sql Server tables.

    IF (
		((SELECT COUNT(*) FROM deleted) > (SELECT COUNT(*) FROM inserted))
		OR
		UPDATE(QueryText)
		OR
		UPDATE(QueryTitle)
		OR
		UPDATE(FolderID)
		OR
		UPDATE(IsEditable)
		OR
		UPDATE(IsDeleted)
		OR
		UPDATE(LeadTypeID)
		)
    BEGIN

		INSERT INTO dbo.SqlQueryHistory(
			QueryID,
			ClientID,
			QueryText,
			QueryTitle,
			AutorunOnline,
			OnlineLimit,
			BatchLimit,
			SqlQueryTypeID,
			FolderID,
			IsEditable,
			IsTemplate,
			IsDeleted,
			WhenCreated,
			CreatedBy,
			OwnedBy,
			RunCount,
			LastRundate,
			LastRuntime,
			LastRowcount,
			MaxRuntime,
			MaxRowcount,
			AvgRuntime,
			AvgRowcount,
			Comments,
			WhenModified,
			ModifiedBy,
			LeadTypeID,
			ParentQueryID,
			IsParent,
			SqlQueryTemplateID,
			OutputFormat,
			ShowInCustomSearch,
			OutputFileExtension,
			OutputSeparatorCharmapID,
			OutputEncapsulatorCharmapID,
			SuppressHeaderRow,
			WhoChanged,
			WhenChanged,
			ChangeAction,
			ChangeNotes,
			LockAllTables
		)
		SELECT
			COALESCE(i.QueryID, d.QueryID),
			COALESCE(i.ClientID, d.ClientID),
			COALESCE(i.QueryText, d.QueryText),
			COALESCE(i.QueryTitle, d.QueryTitle),
			COALESCE(i.AutorunOnline, d.AutorunOnline),
			COALESCE(i.OnlineLimit, d.OnlineLimit),
			COALESCE(i.BatchLimit, d.BatchLimit),
			COALESCE(i.SqlQueryTypeID, d.SqlQueryTypeID),
			COALESCE(i.FolderID, d.FolderID),
			COALESCE(i.IsEditable, d.IsEditable),
			COALESCE(i.IsTemplate, d.IsTemplate),
			COALESCE(i.IsDeleted, d.IsDeleted),
			COALESCE(i.WhenCreated, d.WhenCreated),
			COALESCE(i.CreatedBy, d.CreatedBy),
			COALESCE(i.OwnedBy, d.OwnedBy),
			COALESCE(i.RunCount, d.RunCount),
			COALESCE(i.LastRundate, d.LastRundate),
			COALESCE(i.LastRuntime, d.LastRuntime),
			COALESCE(i.LastRowcount, d.LastRowcount),
			COALESCE(i.MaxRuntime, d.MaxRuntime),
			COALESCE(i.MaxRowcount, d.MaxRowcount),
			COALESCE(i.AvgRuntime, d.AvgRuntime),
			COALESCE(i.AvgRowcount, d.AvgRowcount),
			COALESCE(i.Comments, d.Comments),
			COALESCE(i.WhenModified, d.WhenModified),
			COALESCE(i.ModifiedBy, d.ModifiedBy),
			COALESCE(i.LeadTypeID, d.LeadTypeID),
			COALESCE(i.ParentQueryID, d.ParentQueryID),
			COALESCE(i.IsParent, d.IsParent),
			COALESCE(i.SqlQueryTemplateID, d.SqlQueryTemplateID),
			COALESCE(i.OutputFormat, d.OutputFormat),
			COALESCE(i.ShowInCustomSearch, d.ShowInCustomSearch),
			COALESCE(i.OutputFileExtension, d.OutputFileExtension),
			COALESCE(i.OutputSeparatorCharmapID, d.OutputSeparatorCharmapID),
			COALESCE(i.OutputEncapsulatorCharmapID, d.OutputEncapsulatorCharmapID),
			COALESCE(i.SuppressHeaderRow, d.SuppressHeaderRow),
			NULL, --i.WhoChanged is not available yet
			dbo.fn_GetDate_Local(),
			CASE WHEN d.QueryID IS NULL THEN 'Insert' WHEN i.QueryID IS NULL THEN 'Delete' ELSE 'Update' END,
			dbo.fnGetUserInfo(),
			COALESCE(i.LockAllTables, d.LockAllTables)
		FROM inserted i
		FULL OUTER JOIN deleted d ON i.QueryID = d.QueryID

	END

END
GO
ALTER TABLE [dbo].[SqlQuery] ADD CONSTRAINT [PK_SqlQuery] PRIMARY KEY CLUSTERED  ([QueryID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_SqlQuery_SourceID] ON [dbo].[SqlQuery] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[SqlQuery] ADD CONSTRAINT [FK_SqlQuery_ClientPersonnel] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[SqlQuery] ADD CONSTRAINT [FK_SqlQuery_ClientPersonnel1] FOREIGN KEY ([OwnedBy]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[SqlQuery] ADD CONSTRAINT [FK_SqlQuery_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SqlQuery] ADD CONSTRAINT [FK_SqlQuery_Folder] FOREIGN KEY ([FolderID]) REFERENCES [dbo].[Folder] ([FolderID])
GO
ALTER TABLE [dbo].[SqlQuery] ADD CONSTRAINT [FK_SqlQuery_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
ALTER TABLE [dbo].[SqlQuery] ADD CONSTRAINT [FK_SqlQuery_SqlQuery] FOREIGN KEY ([ParentQueryID]) REFERENCES [dbo].[SqlQuery] ([QueryID])
GO
ALTER TABLE [dbo].[SqlQuery] ADD CONSTRAINT [FK_SqlQuery_SqlQueryTemplate] FOREIGN KEY ([SqlQueryTemplateID]) REFERENCES [dbo].[SqlQueryTemplate] ([SqlQueryTemplateID])
GO
ALTER TABLE [dbo].[SqlQuery] ADD CONSTRAINT [FK_SqlQuery_SqlQueryType] FOREIGN KEY ([SqlQueryTypeID]) REFERENCES [dbo].[SqlQueryType] ([SqlQueryTypeID])
GO
