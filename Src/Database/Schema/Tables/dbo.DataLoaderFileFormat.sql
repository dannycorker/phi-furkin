CREATE TABLE [dbo].[DataLoaderFileFormat]
(
[DataLoaderFileFormatID] [int] NOT NULL,
[FileFormatName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[FileFormatDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[DataLoaderFileFormat] ADD CONSTRAINT [PK_DataLoaderFileFormat] PRIMARY KEY CLUSTERED  ([DataLoaderFileFormatID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DataLoaderFileFormatName] ON [dbo].[DataLoaderFileFormat] ([FileFormatName])
GO
