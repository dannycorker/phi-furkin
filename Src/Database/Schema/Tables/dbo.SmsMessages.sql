CREATE TABLE [dbo].[SmsMessages]
(
[SmsMessageID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientPersonnelIDFrom] [int] NULL,
[ClientPersonnelIDTo] [int] NULL,
[CaseID] [int] NULL,
[MatterID] [int] NULL,
[EventID] [int] NULL,
[LeadID] [int] NULL,
[Message] [varchar] (256) COLLATE Latin1_General_CI_AS NULL,
[RequestReceiveReceipt] [bit] NULL,
[ReadReceiptReceived] [bit] NULL,
[DateTimeSent] [datetime] NULL,
[DateTimeReceived] [datetime] NULL
)
GO
ALTER TABLE [dbo].[SmsMessages] ADD CONSTRAINT [PK_SmsMessages] PRIMARY KEY CLUSTERED  ([SmsMessageID])
GO
