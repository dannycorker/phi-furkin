CREATE TABLE [dbo].[ScriptStandardTarget]
(
[ScriptStandardTargetID] [int] NOT NULL IDENTITY(1, 1),
[ScriptID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[Target] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[ObjectName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[PropertyName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ScriptStandardTarget] ADD CONSTRAINT [PK_ScriptStandardTarget] PRIMARY KEY CLUSTERED  ([ScriptStandardTargetID])
GO
ALTER TABLE [dbo].[ScriptStandardTarget] ADD CONSTRAINT [FK_ScriptStandardTarget_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ScriptStandardTarget] ADD CONSTRAINT [FK_ScriptStandardTarget_Script] FOREIGN KEY ([ScriptID]) REFERENCES [dbo].[Script] ([ScriptID])
GO
