CREATE TABLE [dbo].[SmtpClientConfig]
(
[SmtpClientConfigID] [int] NOT NULL IDENTITY(1, 1),
[FromClientID] [int] NOT NULL,
[ToDomain] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[Port] [int] NULL,
[Host] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[UserName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Password] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[EnableSSL] [bit] NULL
)
GO
ALTER TABLE [dbo].[SmtpClientConfig] ADD CONSTRAINT [PK_SmtpClientConfig] PRIMARY KEY CLUSTERED  ([SmtpClientConfigID])
GO
