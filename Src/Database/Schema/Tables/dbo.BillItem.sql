CREATE TABLE [dbo].[BillItem]
(
[BillItemID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[BillID] [int] NOT NULL,
[Description] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ClientPersonnelID] [int] NULL,
[WorkUnits] [int] NULL,
[Total] [decimal] (18, 2) NOT NULL,
[ChargeOutRate] [int] NULL,
[TotalIsCustomValue] [bit] NULL
)
GO
ALTER TABLE [dbo].[BillItem] ADD CONSTRAINT [PK_BillItem] PRIMARY KEY CLUSTERED  ([BillItemID])
GO
ALTER TABLE [dbo].[BillItem] ADD CONSTRAINT [FK_BillItem_Bill] FOREIGN KEY ([BillID]) REFERENCES [dbo].[Bill] ([BillID])
GO
ALTER TABLE [dbo].[BillItem] ADD CONSTRAINT [FK_BillItem_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[BillItem] ADD CONSTRAINT [FK_BillItem_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
