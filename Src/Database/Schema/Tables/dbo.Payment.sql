CREATE TABLE [dbo].[Payment]
(
[PaymentID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[PaymentDateTime] [datetime] NOT NULL,
[PaymentTypeID] [int] NOT NULL,
[DateReceived] [date] NULL,
[PaymentDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[PaymentReference] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[PaymentNet] [numeric] (18, 2) NULL,
[PaymentVAT] [numeric] (18, 2) NULL,
[PaymentGross] [numeric] (18, 2) NOT NULL,
[PaymentCurrency] [int] NULL,
[RelatedOrderRef] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[RelatedRequestDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[PayeeFullName] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[MaskedAccountNumber] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[AccountTypeDescription] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[PaymentStatusID] [int] NULL,
[DateReconciled] [date] NULL,
[ReconciliationOutcome] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ObjectID] [int] NULL,
[ObjectTypeID] [int] NULL,
[Comments] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[PaymentFileName] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[FailureCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[FailureReason] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[CustomerPaymentScheduleID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[CardTransactionID] [int] NULL,
[LeadEventID] [int] NULL,
[PremiumFunderTransactionID] [int] NULL
)
GO
ALTER TABLE [dbo].[Payment] ADD CONSTRAINT [PK_Payment] PRIMARY KEY CLUSTERED  ([PaymentID])
GO
CREATE NONCLUSTERED INDEX [IX_Payment_Customer] ON [dbo].[Payment] ([CustomerID], [ClientID])
GO
ALTER TABLE [dbo].[Payment] ADD CONSTRAINT [FK_Payment_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[Payment] ADD CONSTRAINT [FK_Payment_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[Payment] ADD CONSTRAINT [FK_Payment_PaymentStatus] FOREIGN KEY ([PaymentStatusID]) REFERENCES [dbo].[PaymentStatus] ([PaymentStatusID])
GO
ALTER TABLE [dbo].[Payment] ADD CONSTRAINT [FK_Payment_PaymentTypes] FOREIGN KEY ([PaymentTypeID]) REFERENCES [dbo].[PaymentTypes] ([PaymentTypeID])
GO
