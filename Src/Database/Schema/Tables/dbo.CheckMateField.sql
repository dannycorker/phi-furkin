CREATE TABLE [dbo].[CheckMateField]
(
[CheckMateFieldID] [int] NOT NULL IDENTITY(1, 1),
[CheckMateFieldName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[CheckMateFieldDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[QuestionTypeID] [int] NOT NULL,
[LookupListID] [int] NULL,
[IsEnabled] [bit] NOT NULL,
[WhenCreated] [datetime] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[CheckMateField] ADD CONSTRAINT [PK_CheckMateField] PRIMARY KEY CLUSTERED  ([CheckMateFieldID])
GO
ALTER TABLE [dbo].[CheckMateField] ADD CONSTRAINT [FK_CheckMateField_LookupList] FOREIGN KEY ([LookupListID]) REFERENCES [dbo].[LookupList] ([LookupListID])
GO
ALTER TABLE [dbo].[CheckMateField] ADD CONSTRAINT [FK_CheckMateField_QuestionTypes] FOREIGN KEY ([QuestionTypeID]) REFERENCES [dbo].[QuestionTypes] ([QuestionTypeID])
GO
