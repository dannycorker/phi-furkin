CREATE TABLE [dbo].[_SystemApplicationLog]
(
[Source] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[EventLog] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[RecordNumber] [int] NULL,
[TimeGenerated] [datetime] NULL,
[TimeWritten] [datetime] NULL,
[EventID] [int] NULL,
[EventType] [int] NULL,
[EventTypeName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[EventCategory] [int] NULL,
[EventCategoryName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[SourceName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Strings] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ComputerName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[SID] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Message] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Data] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[AnyID] [int] NOT NULL IDENTITY(1, 1)
)
GO
ALTER TABLE [dbo].[_SystemApplicationLog] ADD CONSTRAINT [PK__SystemApplicationLog] PRIMARY KEY CLUSTERED  ([AnyID])
GO
