CREATE TABLE [dbo].[RulesEngine_ParameterOptions]
(
[ParameterOptionID] [int] NOT NULL IDENTITY(1, 1),
[RuleParameterID] [int] NOT NULL,
[OperatorID] [int] NOT NULL,
[Val1] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Val2] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[OptionOrder] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[SourceID] [int] NULL
)
GO
ALTER TABLE [dbo].[RulesEngine_ParameterOptions] ADD CONSTRAINT [PK_RulesEngine_ParameterOptions] PRIMARY KEY CLUSTERED  ([ParameterOptionID])
GO
CREATE NONCLUSTERED INDEX [IX_RulesEngine_ParameterOptions] ON [dbo].[RulesEngine_ParameterOptions] ([ParameterOptionID], [RuleParameterID], [OperatorID], [Val1], [Val2])
GO
ALTER TABLE [dbo].[RulesEngine_ParameterOptions] ADD CONSTRAINT [FK_RulesEngine_ParameterOptions_RulesEngine_Operators] FOREIGN KEY ([OperatorID]) REFERENCES [dbo].[RulesEngine_Operators] ([OperatorID])
GO
ALTER TABLE [dbo].[RulesEngine_ParameterOptions] ADD CONSTRAINT [FK_RulesEngine_ParameterOptions_RulesEngine_RuleParameters] FOREIGN KEY ([RuleParameterID]) REFERENCES [dbo].[RulesEngine_RuleParameters] ([RuleParameterID])
GO
