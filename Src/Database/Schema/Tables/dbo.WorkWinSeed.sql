CREATE TABLE [dbo].[WorkWinSeed]
(
[WorkWinSeedID] [int] NOT NULL IDENTITY(1, 1),
[WhenCreated] [datetime] NULL CONSTRAINT [DF__WorkWinSe__WhenC__43D511E7] DEFAULT ([dbo].[fn_GetDate_Local]()),
[JobNotes] [varchar] (500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[WorkWinSeed] ADD CONSTRAINT [PK_WorkWinSeed] PRIMARY KEY CLUSTERED  ([WorkWinSeedID])
GO
