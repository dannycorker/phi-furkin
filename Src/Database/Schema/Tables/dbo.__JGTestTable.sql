CREATE TABLE [dbo].[__JGTestTable]
(
[SomeID] [int] NOT NULL IDENTITY(1, 1),
[SomeValue] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[__JGTestTable] ADD CONSTRAINT [PK___JGTest] PRIMARY KEY CLUSTERED  ([SomeID])
GO
