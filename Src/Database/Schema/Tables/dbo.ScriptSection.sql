CREATE TABLE [dbo].[ScriptSection]
(
[ScriptSectionID] [int] NOT NULL IDENTITY(1, 1),
[ScriptID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[SectionTitle] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[SectionOrder] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoCreated] [int] NULL,
[WhenModified] [datetime] NULL,
[WhoModified] [int] NULL,
[SourceID] [int] NULL,
[NextEventTypeID] [int] NULL
)
GO
ALTER TABLE [dbo].[ScriptSection] ADD CONSTRAINT [PK_ScriptSection] PRIMARY KEY CLUSTERED  ([ScriptSectionID])
GO
ALTER TABLE [dbo].[ScriptSection] ADD CONSTRAINT [FK_ScriptSection_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ScriptSection] ADD CONSTRAINT [FK_ScriptSection_Script] FOREIGN KEY ([ScriptID]) REFERENCES [dbo].[Script] ([ScriptID])
GO
