CREATE TABLE [dbo].[SqlQueryCriteria]
(
[SqlQueryCriteriaID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SqlQueryID] [int] NOT NULL,
[SqlQueryTableID] [int] NULL,
[SqlQueryColumnID] [int] NULL,
[CriteriaText] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[Criteria1] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Criteria2] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[CriteriaName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[SubQueryID] [int] NULL,
[SubQueryLinkType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ParamValue] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[TempTableID] [int] NULL,
[TempColumnID] [int] NULL,
[TempCriteriaID] [int] NULL,
[IsSecurityClause] [bit] NOT NULL,
[CriteriaSubstitutions] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[IsParameterizable] [bit] NULL,
[Comparison] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[IsJoinClause] [bit] NULL,
[SourceID] [int] NULL
)
GO
ALTER TABLE [dbo].[SqlQueryCriteria] ADD CONSTRAINT [PK_SqlQueryCriteria] PRIMARY KEY CLUSTERED  ([SqlQueryCriteriaID])
GO
CREATE NONCLUSTERED INDEX [IX_SqlQueryCriteria_ClientID] ON [dbo].[SqlQueryCriteria] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_SqlQueryCriteria_SourceID] ON [dbo].[SqlQueryCriteria] ([SourceID]) WHERE ([SourceID]>(0))
GO
CREATE NONCLUSTERED INDEX [IX_SqlQueryCriteria_SqlQueryID] ON [dbo].[SqlQueryCriteria] ([SqlQueryID])
GO
CREATE NONCLUSTERED INDEX [IX_SqlQueryCriteria_QueryTableColumn] ON [dbo].[SqlQueryCriteria] ([SqlQueryID], [SqlQueryTableID], [SqlQueryColumnID])
GO
ALTER TABLE [dbo].[SqlQueryCriteria] ADD CONSTRAINT [FK_SqlQueryCriteria_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SqlQueryCriteria] ADD CONSTRAINT [FK_SqlQueryCriteria_SubQuery] FOREIGN KEY ([SubQueryID]) REFERENCES [dbo].[SubQuery] ([SubQueryID])
GO
