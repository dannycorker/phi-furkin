CREATE TABLE [dbo].[CustomTableSQL]
(
[CustomTableSQLID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[QueryText] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[HeaderColumns] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[SQLQueryID] [int] NULL,
[RTFBorderOn] [bit] NOT NULL,
[RTFTwips] [int] NULL,
[HTMLTableProperties] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[AutoSizeColumns] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[CustomTableSQL] ADD CONSTRAINT [PK_CustomTableSQL] PRIMARY KEY CLUSTERED  ([CustomTableSQLID])
GO
