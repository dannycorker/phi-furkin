CREATE TABLE [dbo].[LeadDocumentEsignatureStatus]
(
[LeadDocumentEsignatureStatusID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadDocumentID] [int] NOT NULL,
[ElectronicSignatureDocumentKey] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DocumentName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[SentTo] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[DateSent] [datetime] NOT NULL,
[EsignatureStatusID] [int] NOT NULL,
[StatusCheckedDate] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[LeadDocumentEsignatureStatus] ADD CONSTRAINT [PK_LeadDocumentEsignatureStatus] PRIMARY KEY CLUSTERED  ([LeadDocumentEsignatureStatusID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadDocumentEsignatureStatus] ON [dbo].[LeadDocumentEsignatureStatus] ([ElectronicSignatureDocumentKey])
GO
ALTER TABLE [dbo].[LeadDocumentEsignatureStatus] ADD CONSTRAINT [FK_DocumentEsignatureStatus_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[LeadDocumentEsignatureStatus] ADD CONSTRAINT [FK_LeadDocumentEsignatureStatus_EsignatureStatus] FOREIGN KEY ([EsignatureStatusID]) REFERENCES [dbo].[EsignatureStatus] ([EsignatureStatusID])
GO
ALTER TABLE [dbo].[LeadDocumentEsignatureStatus] ADD CONSTRAINT [FK_LeadDocumentEsignatureStatus_LeadDocument] FOREIGN KEY ([LeadDocumentID]) REFERENCES [dbo].[LeadDocument] ([LeadDocumentID])
GO
