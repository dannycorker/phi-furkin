CREATE TABLE [dbo].[EventType]
(
[EventTypeID] [int] NOT NULL IDENTITY(170, 1),
[ClientID] [int] NOT NULL,
[EventTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[EventTypeDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Enabled] [bit] NOT NULL CONSTRAINT [DF__EventType__Enabl__658C0CBD] DEFAULT ((1)),
[UnitsOfEffort] [int] NOT NULL CONSTRAINT [DF__EventType__Units__6774552F] DEFAULT ((0)),
[FollowupTimeUnitsID] [int] NULL,
[FollowupQuantity] [int] NULL,
[AvailableManually] [bit] NOT NULL CONSTRAINT [DF__EventType__Avail__6497E884] DEFAULT ((1)),
[StatusAfterEvent] [int] NULL,
[AquariumEventAfterEvent] [int] NULL,
[EventSubtypeID] [int] NOT NULL,
[DocumentTypeID] [int] NULL,
[LeadTypeID] [int] NOT NULL,
[AllowCustomTimeUnits] [bit] NULL CONSTRAINT [DF__EventType__Allow__63A3C44B] DEFAULT ((0)),
[InProcess] [bit] NOT NULL,
[KeyEvent] [bit] NOT NULL CONSTRAINT [DF__EventType__KeyEv__668030F6] DEFAULT ((0)),
[UseEventCosts] [bit] NULL,
[UseEventUOEs] [bit] NULL,
[UseEventDisbursements] [bit] NULL,
[UseEventComments] [bit] NULL,
[SignatureRequired] [bit] NULL,
[SignatureOverride] [bit] NULL,
[VisioX] [decimal] (18, 8) NULL,
[VisioY] [decimal] (18, 8) NULL,
[AquariumEventSubtypeID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[FollowupWorkingDaysOnly] [bit] NULL,
[CalculateTableRows] [bit] NULL,
[SourceID] [int] NULL,
[SmsGatewayID] [int] NULL,
[IsShared] [bit] NULL,
[SocialFeedID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2009-04-06
-- Description:	Capture changes and store in History table
-- =============================================
CREATE TRIGGER [dbo].[trgiud_EventType]
   ON  [dbo].[EventType]
   AFTER INSERT,DELETE,UPDATE
AS
BEGIN
	SET NOCOUNT ON;

    -- Inserts appear in the inserted table,
    -- Deletes appear in the deleted table,
    -- and Updates appear in both tables at once identically.
    -- These are all 'logical' Sql Server tables.
	INSERT INTO dbo.EventTypeHistory(
		EventTypeID,
		ClientID,
		EventTypeName,
		EventTypeDescription,
		[Enabled],
		UnitsOfEffort,
		FollowupTimeUnitsID,
		FollowupQuantity,
		AvailableManually,
		StatusAfterEvent,
		AquariumEventAfterEvent,
		EventSubtypeID,
		DocumentTypeID,
		LeadTypeID,
		AllowCustomTimeUnits,
		InProcess,
		KeyEvent,
		UseEventCosts,
		UseEventUOEs,
		UseEventDisbursements,
		UseEventComments,
		SignatureRequired,
		SignatureOverride,
		VisioX,
		VisioY,
		AquariumEventSubtypeID,
		WhoChanged,
		WhenChanged,
		ChangeAction,
		ChangeNotes,
		FollowupWorkingDaysOnly,
		CalculateTableRows
	)
	SELECT
		COALESCE(i.EventTypeID, d.EventTypeID),
		COALESCE(i.ClientID, d.ClientID),
		COALESCE(i.EventTypeName, d.EventTypeName),
		COALESCE(i.EventTypeDescription, d.EventTypeDescription),
		COALESCE(i.[Enabled], d.[Enabled]),
		COALESCE(i.UnitsOfEffort, d.UnitsOfEffort),
		COALESCE(i.FollowupTimeUnitsID, d.FollowupTimeUnitsID),
		COALESCE(i.FollowupQuantity, d.FollowupQuantity),
		COALESCE(i.AvailableManually, d.AvailableManually),
		COALESCE(i.StatusAfterEvent, d.StatusAfterEvent),
		COALESCE(i.AquariumEventAfterEvent, d.AquariumEventAfterEvent),
		COALESCE(i.EventSubtypeID, d.EventSubtypeID),
		COALESCE(i.DocumentTypeID, d.DocumentTypeID),
		COALESCE(i.LeadTypeID, d.LeadTypeID),
		COALESCE(i.AllowCustomTimeUnits, d.AllowCustomTimeUnits),
		COALESCE(i.InProcess, d.InProcess),
		COALESCE(i.KeyEvent, d.KeyEvent),
		COALESCE(i.UseEventCosts, d.UseEventCosts),
		COALESCE(i.UseEventUOEs, d.UseEventUOEs),
		COALESCE(i.UseEventDisbursements, d.UseEventDisbursements),
		COALESCE(i.UseEventComments, d.UseEventComments),
		COALESCE(i.SignatureRequired, d.SignatureRequired),
		COALESCE(i.SignatureOverride, d.SignatureOverride),
		COALESCE(i.VisioX, d.VisioX),
		COALESCE(i.VisioY, d.VisioY),
		COALESCE(i.AquariumEventSubtypeID, d.AquariumEventSubtypeID),
		COALESCE(i.WhoModified, d.WhoModified),
		dbo.fn_GetDate_Local(),
		CASE WHEN d.EventTypeID IS NULL THEN 'Insert' WHEN i.EventTypeID IS NULL THEN 'Delete' ELSE 'Update' END,
		dbo.fnGetUserInfo(),
		COALESCE(i.FollowupWorkingDaysOnly, d.FollowupWorkingDaysOnly),
		COALESCE(i.CalculateTableRows, d.CalculateTableRows)
	FROM inserted i
	FULL OUTER JOIN deleted d ON i.EventTypeID = d.EventTypeID

END
GO
ALTER TABLE [dbo].[EventType] ADD CONSTRAINT [PK_LeadEventType] PRIMARY KEY CLUSTERED  ([EventTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadEventTypeClient] ON [dbo].[EventType] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_EventTypeClientIDEventSubtypeID2017] ON [dbo].[EventType] ([ClientID], [EventSubtypeID]) INCLUDE ([EventTypeName])
GO
CREATE NONCLUSTERED INDEX [IX_EventTypeClientIDEventTypeID2017] ON [dbo].[EventType] ([ClientID], [EventTypeID]) INCLUDE ([EventTypeName])
GO
CREATE NONCLUSTERED INDEX [IX_EventType_DocType] ON [dbo].[EventType] ([DocumentTypeID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_EventTypeLeadTypeSubType] ON [dbo].[EventType] ([LeadTypeID], [ClientID], [EventSubtypeID], [Enabled], [AvailableManually]) INCLUDE ([EventTypeName])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_EventType_SourceID] ON [dbo].[EventType] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[EventType] ADD CONSTRAINT [FK_EventType_DocumentType] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentType] ([DocumentTypeID])
GO
ALTER TABLE [dbo].[EventType] ADD CONSTRAINT [FK_EventType_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EventType] ADD CONSTRAINT [FK_LeadEventType_EventSubtype] FOREIGN KEY ([EventSubtypeID]) REFERENCES [dbo].[EventSubtype] ([EventSubtypeID])
GO
ALTER TABLE [dbo].[EventType] ADD CONSTRAINT [FK_LeadEventType_TimeUnits] FOREIGN KEY ([FollowupTimeUnitsID]) REFERENCES [dbo].[TimeUnits] ([TimeUnitsID])
GO
