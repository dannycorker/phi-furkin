CREATE TABLE [dbo].[USTaxCityandCounty]
(
[USTaxCityandCountyID] [int] NOT NULL IDENTITY(1, 1),
[TaxApplied] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[CountryID] [int] NOT NULL CONSTRAINT [DF__USTaxCity__Count__1BFC2AB7] DEFAULT ((233)),
[StateID] [int] NOT NULL,
[City] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[CityCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[Zip] [varchar] (5) COLLATE Latin1_General_CI_AS NULL,
[County] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[CountyCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[TaxPercent] [money] NOT NULL,
[TaxType] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[From] [date] NULL,
[To] [date] NULL,
[TaxProportion] [money] NULL,
[GrossMultiplier] [money] NULL
)
GO
ALTER TABLE [dbo].[USTaxCityandCounty] ADD CONSTRAINT [PK_USTaxCityandCounty] PRIMARY KEY CLUSTERED  ([USTaxCityandCountyID])
GO
ALTER TABLE [dbo].[USTaxCityandCounty] ADD CONSTRAINT [FK_USTaxCityandCounty_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
ALTER TABLE [dbo].[USTaxCityandCounty] ADD CONSTRAINT [FK_USTaxCityandCounty_State] FOREIGN KEY ([StateID]) REFERENCES [dbo].[UnitedStates] ([StateID])
GO
