CREATE TABLE [dbo].[VetEnvoy_Queries]
(
[QueryID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[AccountID] [int] NOT NULL,
[ConversationID] [uniqueidentifier] NOT NULL,
[Received] [datetime] NOT NULL,
[SenderID] [uniqueidentifier] NOT NULL,
[Xml] [xml] NOT NULL,
[LeadEventID] [int] NULL
)
GO
ALTER TABLE [dbo].[VetEnvoy_Queries] ADD CONSTRAINT [PK_VetEnvoy_Queries] PRIMARY KEY CLUSTERED  ([QueryID])
GO
CREATE NONCLUSTERED INDEX [IX_VetEnvoy_Queries] ON [dbo].[VetEnvoy_Queries] ([ConversationID])
GO
ALTER TABLE [dbo].[VetEnvoy_Queries] ADD CONSTRAINT [FK_VetEnvoy_Queries_VetEnvoy_Accounts] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[VetEnvoy_Accounts] ([AccountID])
GO
