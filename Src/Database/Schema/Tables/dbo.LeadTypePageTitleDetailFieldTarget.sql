CREATE TABLE [dbo].[LeadTypePageTitleDetailFieldTarget]
(
[LeadTypePageTitleDetailFieldTargetID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[Target] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[DetailFieldID] [int] NULL,
[TemplateTypeID] [int] NOT NULL,
[DetailFieldAlias] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[Notes] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[LeadTypePageTitleDetailFieldTarget] ADD CONSTRAINT [PK_LeadTypePageTitleDetailFieldTarget] PRIMARY KEY CLUSTERED  ([LeadTypePageTitleDetailFieldTargetID])
GO
ALTER TABLE [dbo].[LeadTypePageTitleDetailFieldTarget] ADD CONSTRAINT [FK_LeadTypePageTitleDetailFieldTarget_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[LeadTypePageTitleDetailFieldTarget] ADD CONSTRAINT [FK_LeadTypePageTitleDetailFieldTarget_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
ALTER TABLE [dbo].[LeadTypePageTitleDetailFieldTarget] ADD CONSTRAINT [FK_LeadTypePageTitleDetailFieldTarget_TemplateType] FOREIGN KEY ([TemplateTypeID]) REFERENCES [dbo].[TemplateType] ([TemplateTypeID])
GO
