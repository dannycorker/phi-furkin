CREATE TABLE [dbo].[AutomatedTaskQueue]
(
[AutomatedTaskQueueID] [int] NOT NULL,
[Enabled] [bit] NOT NULL,
[Description] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[MaxRecords] [int] NOT NULL,
[Notes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NULL
)
GO
ALTER TABLE [dbo].[AutomatedTaskQueue] ADD CONSTRAINT [PK_AutomatedTaskQueue] PRIMARY KEY CLUSTERED  ([AutomatedTaskQueueID])
GO
