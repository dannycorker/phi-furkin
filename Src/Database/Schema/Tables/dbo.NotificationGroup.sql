CREATE TABLE [dbo].[NotificationGroup]
(
[NotificationGroupID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[NotificationGroupName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Enabled] [bit] NOT NULL,
[Deleted] [bit] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[NotificationGroup] ADD CONSTRAINT [PK__Notifica__E722757277970AE5] PRIMARY KEY CLUSTERED  ([NotificationGroupID])
GO
ALTER TABLE [dbo].[NotificationGroup] ADD CONSTRAINT [FK__Notificat__Clien__797F5357] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
