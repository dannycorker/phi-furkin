CREATE TABLE [dbo].[MDVDelete]
(
[MDVDeleteID] [int] NOT NULL IDENTITY(1, 1),
[MatterID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[OrigMatterDetailValueID] [int] NOT NULL,
[WhenChanged] [datetime2] (0) NOT NULL CONSTRAINT [DF__MDVDelete__WhenC__3D281458] DEFAULT ([dbo].[fn_GetDate_Local]())
)
GO
