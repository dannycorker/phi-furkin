CREATE TABLE [dbo].[ValidationCriteriaFieldTypes]
(
[ValidationCriteriaFieldTypeID] [int] NOT NULL IDENTITY(1, 1),
[Allowable] [nvarchar] (25) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ValidationCriteriaFieldTypes] ADD CONSTRAINT [PK_ValidationCriteriaFieldTypes] PRIMARY KEY CLUSTERED  ([ValidationCriteriaFieldTypeID])
GO
