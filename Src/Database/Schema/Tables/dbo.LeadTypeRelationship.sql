CREATE TABLE [dbo].[LeadTypeRelationship]
(
[LeadTypeRelationshipID] [int] NOT NULL IDENTITY(1, 1),
[FromLeadTypeID] [int] NOT NULL,
[ToLeadTypeID] [int] NOT NULL,
[FromLeadID] [int] NULL,
[ToLeadID] [int] NULL,
[FromCaseID] [int] NULL,
[ToCaseID] [int] NULL,
[FromMatterID] [int] NULL,
[ToMatterID] [int] NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[LeadTypeRelationship] ADD CONSTRAINT [PK_LeadTypeRelationship] PRIMARY KEY CLUSTERED  ([LeadTypeRelationshipID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadTypeRelationship_CaseIDFromTo] ON [dbo].[LeadTypeRelationship] ([FromCaseID], [ToCaseID]) INCLUDE ([FromLeadTypeID], [ToLeadTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadTypeRelationship_LeadIDFromTo] ON [dbo].[LeadTypeRelationship] ([FromLeadID], [ToLeadID]) INCLUDE ([FromLeadTypeID], [ToLeadTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadTypeRelationship_MatterIDFromTo] ON [dbo].[LeadTypeRelationship] ([FromMatterID], [ToMatterID]) INCLUDE ([FromLeadTypeID], [ToLeadTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadTypeRelationship_CaseIDToFrom] ON [dbo].[LeadTypeRelationship] ([ToCaseID], [FromCaseID]) INCLUDE ([FromLeadTypeID], [ToLeadTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadTypeRelationship_LeadIDToFrom] ON [dbo].[LeadTypeRelationship] ([ToLeadID], [FromLeadID]) INCLUDE ([FromLeadTypeID], [ToLeadTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadTypeRelationship_MatterIDToFrom] ON [dbo].[LeadTypeRelationship] ([ToMatterID], [FromMatterID]) INCLUDE ([FromLeadTypeID], [ToLeadTypeID])
GO
