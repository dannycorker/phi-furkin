CREATE TABLE [dbo].[QuickSearchOptions]
(
[QuickSearchOptionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientPersonnelAdminGroupID] [int] NOT NULL,
[DropDownCaption] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DropDownCode] [varchar] (5) COLLATE Latin1_General_CI_AS NULL,
[DetailFieldID] [int] NULL,
[SortOrder] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[QuickSearchOptions] ADD CONSTRAINT [PK_QuickSearchOptions] PRIMARY KEY CLUSTERED  ([QuickSearchOptionID])
GO
CREATE NONCLUSTERED INDEX [IX_QuickSearchOptions_ClientID] ON [dbo].[QuickSearchOptions] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_QuickSearchOptions_ClientPersonnelAdminGroupID] ON [dbo].[QuickSearchOptions] ([ClientPersonnelAdminGroupID])
GO
