CREATE TABLE [dbo].[MigrationLog]
(
[MigrationLogID] [int] NOT NULL IDENTITY(1, 1),
[StageID] [int] NOT NULL,
[LogMessage] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[MigrationLog] ADD CONSTRAINT [PK_MigrationLog] PRIMARY KEY CLUSTERED  ([MigrationLogID])
GO
