CREATE TABLE [dbo].[CaseDetailValues]
(
[CaseDetailValueID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CaseID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[DetailValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[ErrorMsg] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[EncryptedValue] [varchar] (3000) COLLATE Latin1_General_CI_AS NULL,
[ValueInt] [int] NULL,
[ValueMoney] [money] NULL,
[ValueDate] [date] NULL,
[ValueDateTime] [datetime2] (0) NULL,
[SourceID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2010-08-15
-- Description:	Store DetailValue as specific datatypes
-- 2011-07-13 JWG Remove duplicates instantly at source
-- =============================================
CREATE TRIGGER [dbo].[trgiu_CaseDetailValues]
   ON [dbo].[CaseDetailValues]
   AFTER INSERT, UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END


    /*
		If the DetailValue column has been updated then fire the trigger,
		otherwise we are here again because the trigger has just fired,
		in which case do nothing.
	*/
    IF UPDATE(DetailValue)
    BEGIN
		/* Check for duplicates (but only on insert, not update) */
		IF NOT EXISTS (SELECT * FROM deleted)
		BEGIN
			/* Only issue the delete if we know that any records exist, otherwise the delete operation gets attempted every time regardless */
			IF EXISTS(SELECT * FROM dbo.CaseDetailValues dv WITH (NOLOCK) INNER JOIN inserted i ON i.CaseID = dv.CaseID AND i.DetailFieldID = dv.DetailFieldID AND i.CaseDetailValueID > dv.CaseDetailValueID)
			BEGIN
				/* Remove any older duplicates for this field */
				DELETE dbo.CaseDetailValues
				OUTPUT 11, deleted.CaseDetailValueID, deleted.CaseID, deleted.DetailValue, deleted.DetailFieldID
				INTO dbo.__DeletedValue (DetailFieldSubTypeID, DetailValueID, ParentID, DetailValue, DetailFieldID)
				FROM dbo.CaseDetailValues dv
				INNER JOIN inserted i ON i.CaseID = dv.CaseID AND i.DetailFieldID = dv.DetailFieldID AND i.CaseDetailValueID > dv.CaseDetailValueID
			END
		END

		/* Populate ValueInt et al */
		UPDATE dbo.CaseDetailValues
		SET ValueInt = dbo.fnValueAsIntBitsIncluded(i.DetailValue) /*CASE WHEN dbo.fnIsInt(i.detailvalue) = 1 THEN convert(int, i.DetailValue) ELSE NULL END*/,
		ValueMoney = CASE WHEN dbo.fnIsMoney(i.DetailValue) = 1 THEN convert(money, i.DetailValue) ELSE NULL END,
		ValueDate = CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN convert(date, i.DetailValue) ELSE NULL END,
		ValueDateTime = CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN convert(datetime2, i.DetailValue) ELSE NULL END
		FROM dbo.CaseDetailValues dv
		INNER JOIN inserted i ON i.CaseDetailValueID = dv.CaseDetailValueID
	END


END


GO
ALTER TABLE [dbo].[CaseDetailValues] ADD CONSTRAINT [PK_CaseDetailValues] PRIMARY KEY CLUSTERED  ([CaseDetailValueID])
GO
CREATE NONCLUSTERED INDEX [IX_CaseDetailValues_CaseAndField] ON [dbo].[CaseDetailValues] ([CaseID], [DetailFieldID], [ClientID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_CaseDetailValues_CaseFieldValueInt] ON [dbo].[CaseDetailValues] ([CaseID], [DetailFieldID], [ValueInt], [ClientID]) WHERE ([ValueInt] IS NOT NULL)
GO
CREATE NONCLUSTERED INDEX [IX_CaseDetailValues_Client] ON [dbo].[CaseDetailValues] ([ClientID]) INCLUDE ([CaseID])
GO
CREATE NONCLUSTERED INDEX [IX_CaseDetailValuesDetailField] ON [dbo].[CaseDetailValues] ([DetailFieldID]) INCLUDE ([CaseID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_CaseDetailValues_FieldValueDate] ON [dbo].[CaseDetailValues] ([DetailFieldID], [ValueDate], [ClientID]) INCLUDE ([CaseID]) WHERE ([ValueDate] IS NOT NULL)
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_CaseDetailValues_FieldValueInt] ON [dbo].[CaseDetailValues] ([DetailFieldID], [ValueInt], [ClientID]) INCLUDE ([CaseID]) WHERE ([ValueInt] IS NOT NULL)
GO
ALTER TABLE [dbo].[CaseDetailValues] ADD CONSTRAINT [FK_CaseDetailValues_Case] FOREIGN KEY ([CaseID]) REFERENCES [dbo].[Cases] ([CaseID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[CaseDetailValues] ADD CONSTRAINT [FK_CaseDetailValues_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
