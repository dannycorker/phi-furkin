CREATE TABLE [dbo].[ClientPaymentGateway]
(
[ClientPaymentGatewayID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[PaymentGatewayID] [int] NOT NULL,
[Login] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[UserName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Password] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[MerchantName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[AccountID] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[MerchantUrl] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[TransactionKey] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[TerminalID] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ApplySecure3D] [int] NULL,
[TestMode] [bit] NULL,
[IpAuthentication] [bit] NULL
)
GO
ALTER TABLE [dbo].[ClientPaymentGateway] ADD CONSTRAINT [PK_ClientPaymentGateway] PRIMARY KEY CLUSTERED  ([ClientPaymentGatewayID])
GO
