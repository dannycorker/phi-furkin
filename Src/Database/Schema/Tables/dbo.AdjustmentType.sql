CREATE TABLE [dbo].[AdjustmentType]
(
[AdjustmentTypeID] [int] NOT NULL IDENTITY(1, 1),
[AdjustmentType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[DisplayHexColourID] [int] NULL
)
GO
ALTER TABLE [dbo].[AdjustmentType] ADD CONSTRAINT [PK_AdjustmentType] PRIMARY KEY CLUSTERED  ([AdjustmentTypeID])
GO
