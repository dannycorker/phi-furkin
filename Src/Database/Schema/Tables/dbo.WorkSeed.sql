CREATE TABLE [dbo].[WorkSeed]
(
[WorkSeedID] [int] NOT NULL IDENTITY(1, 1),
[WhenCreated] [datetime] NULL CONSTRAINT [DF__WorkSeed__WhenCr__42E0EDAE] DEFAULT ([dbo].[fn_GetDate_Local]()),
[JobNotes] [varchar] (500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[WorkSeed] ADD CONSTRAINT [PK_WorkSeed] PRIMARY KEY CLUSTERED  ([WorkSeedID])
GO
