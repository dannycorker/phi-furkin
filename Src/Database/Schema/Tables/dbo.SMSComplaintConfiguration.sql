CREATE TABLE [dbo].[SMSComplaintConfiguration]
(
[ClientID] [int] NOT NULL,
[Title] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[LogoUrl] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[MessageReadOnly] [bit] NULL,
[ShowPolicyNumberField] [bit] NULL
)
GO
ALTER TABLE [dbo].[SMSComplaintConfiguration] ADD CONSTRAINT [PK_SMSComplaintConfiguration] PRIMARY KEY CLUSTERED  ([ClientID])
GO
