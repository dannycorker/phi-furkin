CREATE TABLE [dbo].[PortalUserOptionType]
(
[PortalUserOptionTypeID] [int] NOT NULL IDENTITY(1, 1),
[OptionTypeName] [nchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[PortalUserOptionType] ADD CONSTRAINT [PK_PortalUserOptionType] PRIMARY KEY CLUSTERED  ([PortalUserOptionTypeID])
GO
