CREATE TABLE [dbo].[MigrationStage]
(
[MigrationStageID] [int] NOT NULL,
[StageName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Notes] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[MigrationStage] ADD CONSTRAINT [PK_MigrationStage] PRIMARY KEY CLUSTERED  ([MigrationStageID])
GO
