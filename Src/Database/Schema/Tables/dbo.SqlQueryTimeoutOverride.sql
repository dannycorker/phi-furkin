CREATE TABLE [dbo].[SqlQueryTimeoutOverride]
(
[SqlQueryTimeoutOverrideID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SqlQueryID] [int] NOT NULL,
[TimeoutInSeconds] [int] NOT NULL,
[Notes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SqlQueryTimeoutOverride] ADD CONSTRAINT [PK_SqlQueryTimeoutOverride] PRIMARY KEY CLUSTERED  ([SqlQueryTimeoutOverrideID])
GO
ALTER TABLE [dbo].[SqlQueryTimeoutOverride] ADD CONSTRAINT [FK_SqlQueryTimeoutOverride_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SqlQueryTimeoutOverride] ADD CONSTRAINT [FK_SqlQueryTimeoutOverride_SqlQuery] FOREIGN KEY ([SqlQueryID]) REFERENCES [dbo].[SqlQuery] ([QueryID])
GO
