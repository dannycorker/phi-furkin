CREATE TABLE [dbo].[Substitutions]
(
[SubstitutionsID] [int] NOT NULL IDENTITY(1, 1),
[SubstitutionsVariable] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[SubstitutionsDisplayName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[SubstitutionsDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[Substitutions] ADD CONSTRAINT [PK_Substitutions] PRIMARY KEY CLUSTERED  ([SubstitutionsID])
GO
