CREATE TABLE [dbo].[PaymentTypes]
(
[PaymentTypeID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[PaymentTypeName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[PaymentTypeDescription] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[PaymentTypes] ADD CONSTRAINT [PK_PaymentTypes] PRIMARY KEY CLUSTERED  ([PaymentTypeID])
GO
