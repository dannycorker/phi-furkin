CREATE TABLE [dbo].[MigrationTable]
(
[MigrationTableID] [int] NOT NULL IDENTITY(1, 1),
[TableName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[FKToClientIDTableName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NULL,
[TotalRows] [int] NULL,
[ClientRows] [int] NULL,
[DropText] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[DeleteText] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[TruncateText] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[RepopulateText] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[IsClearedDown] [bit] NOT NULL,
[IsComplete] [bit] NOT NULL,
[AlwaysDrop] [bit] NOT NULL,
[NeverDrop] [bit] NOT NULL,
[Notes] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ExecutionOrder] [int] NULL,
[ClientGoneDropText] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ClientGoneDeleteText] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[DeleteThisManyAtATime] [int] NULL,
[IsEnum] [bit] NULL,
[IsAquariumStaticData] [bit] NULL,
[IgnoreForRepopulation] [bit] NULL,
[CompareStandalone] [bit] NULL,
[ManualUpdateStandalone] [bit] NULL,
[UseBCP] [bit] NULL,
[IsDALTable] [bit] NULL,
[CustomerIDIfSpecific] [bit] NOT NULL,
[LeadIDIfSpecific] [bit] NOT NULL,
[CaseIDIfSpecific] [bit] NOT NULL,
[MatterIDIfSpecific] [bit] NOT NULL,
[RequiresDistinct] [bit] NULL
)
GO
