CREATE TABLE [dbo].[QuestionnaireFrameSources]
(
[QuestionnaireFrameSourceID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientQuestionnaireID] [int] NOT NULL,
[PageNumber] [int] NULL,
[SourceUrl] [varchar] (555) COLLATE Latin1_General_CI_AS NULL,
[FrameType] [int] NULL
)
GO
ALTER TABLE [dbo].[QuestionnaireFrameSources] ADD CONSTRAINT [PK_QuestionnaireFrameSources] PRIMARY KEY CLUSTERED  ([QuestionnaireFrameSourceID])
GO
ALTER TABLE [dbo].[QuestionnaireFrameSources] ADD CONSTRAINT [FK_QuestionnaireFrameSources_ClientQuestionnaires] FOREIGN KEY ([ClientQuestionnaireID]) REFERENCES [dbo].[ClientQuestionnaires] ([ClientQuestionnaireID])
GO
