CREATE TABLE [dbo].[ChangeDefinition]
(
[ChangeDefinitionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ChangeDescription] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[RequiredDate] [datetime] NULL,
[Deleted] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[ChangeDefinition] ADD CONSTRAINT [PK__ChangeDe__062E9F0F49203D9B] PRIMARY KEY CLUSTERED  ([ChangeDefinitionID])
GO
ALTER TABLE [dbo].[ChangeDefinition] ADD CONSTRAINT [FK__ChangeDef__Clien__4B08860D] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ChangeDefinition] ADD CONSTRAINT [FK__ChangeDef__WhoCr__4BFCAA46] FOREIGN KEY ([WhoCreated]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[ChangeDefinition] ADD CONSTRAINT [FK__ChangeDef__WhoMo__4CF0CE7F] FOREIGN KEY ([WhoModified]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
