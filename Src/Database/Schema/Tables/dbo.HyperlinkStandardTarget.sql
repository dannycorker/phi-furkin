CREATE TABLE [dbo].[HyperlinkStandardTarget]
(
[HyperlinkStandardTargetID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[HyperlinkDetailFieldID] [int] NOT NULL,
[Target] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[ObjectName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[PropertyName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[TemplateTypeID] [int] NOT NULL,
[Notes] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[IsSpecial] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[HyperlinkStandardTarget] ADD CONSTRAINT [PK_HyperlinkStandardTarget] PRIMARY KEY CLUSTERED  ([HyperlinkStandardTargetID])
GO
ALTER TABLE [dbo].[HyperlinkStandardTarget] ADD CONSTRAINT [FK_HyperlinkStandardTarget_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[HyperlinkStandardTarget] ADD CONSTRAINT [FK_HyperlinkStandardTarget_DetailFields] FOREIGN KEY ([HyperlinkDetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[HyperlinkStandardTarget] ADD CONSTRAINT [FK_HyperlinkStandardTarget_TemplateType] FOREIGN KEY ([TemplateTypeID]) REFERENCES [dbo].[TemplateType] ([TemplateTypeID])
GO
