CREATE TABLE [dbo].[RemoteAuth]
(
[RemoteAuthID] [int] NOT NULL IDENTITY(1, 1),
[AuthKey] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[RequestedUsername] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[ClientPersonnelID] [int] NULL,
[ClientID] [int] NULL,
[WhenRequested] [datetime2] (0) NOT NULL,
[IPAddress] [varchar] (15) COLLATE Latin1_General_CI_AS NOT NULL,
[IsValid] [bit] NOT NULL,
[ValidUntil] [datetime2] (0) NULL,
[WhenFirstUsed] [datetime2] (0) NULL,
[WhenLastUsed] [datetime2] (0) NULL,
[UsageCount] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[RemoteAuth] ADD CONSTRAINT [PK_RemoteAuth] PRIMARY KEY CLUSTERED  ([RemoteAuthID])
GO
