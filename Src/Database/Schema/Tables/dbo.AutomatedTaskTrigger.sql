CREATE TABLE [dbo].[AutomatedTaskTrigger]
(
[AutomatedTasTriggerlID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[AutomatedTaskID] [int] NOT NULL,
[TaskIDToTrigger] [int] NOT NULL,
[TriggerIfIncomplete] [bit] NOT NULL,
[TriggerOnZeroRecords] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[AutomatedTaskTrigger] ADD CONSTRAINT [PK_AutomatedTaskSql] PRIMARY KEY CLUSTERED  ([AutomatedTaskID])
GO
ALTER TABLE [dbo].[AutomatedTaskTrigger] ADD CONSTRAINT [FK_AutomatedTaskSql_AutomatedTask] FOREIGN KEY ([AutomatedTaskID]) REFERENCES [dbo].[AutomatedTask] ([TaskID])
GO
ALTER TABLE [dbo].[AutomatedTaskTrigger] ADD CONSTRAINT [FK_AutomatedTaskSql_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
