CREATE TABLE [dbo].[SecureCoResponse]
(
[SecureCoResponseID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SecureCoRequestID] [int] NOT NULL,
[MetaData] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[ResponseJson] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[ResponseType] [varchar] (20) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[SecureCoResponse] ADD CONSTRAINT [PK_SecureCoResponse] PRIMARY KEY CLUSTERED  ([SecureCoResponseID])
GO
