CREATE TABLE [dbo].[SqlQueryEditingCriteriaSet]
(
[SqlQueryEditingCriteriaSetID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SqlQueryEditingID] [int] NOT NULL,
[SetName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[SqlQueryEditingCriteriaSet] ADD CONSTRAINT [PK_SqlQueryEditingCriteriaSet] PRIMARY KEY CLUSTERED  ([SqlQueryEditingCriteriaSetID])
GO
ALTER TABLE [dbo].[SqlQueryEditingCriteriaSet] ADD CONSTRAINT [FK_SqlQueryEditingCriteriaSet_SqlQueryEditing] FOREIGN KEY ([SqlQueryEditingID]) REFERENCES [dbo].[SqlQueryEditing] ([SqlQueryEditingID]) ON DELETE CASCADE
GO
