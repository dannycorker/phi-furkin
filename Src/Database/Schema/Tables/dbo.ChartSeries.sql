CREATE TABLE [dbo].[ChartSeries]
(
[ChartSeriesID] [int] NOT NULL IDENTITY(1, 1),
[ChartSeriesTypeID] [int] NULL,
[ChartSeriesOrder] [int] NOT NULL,
[ChartID] [int] NULL,
[ClientID] [int] NOT NULL,
[PanelItemChartingID] [int] NULL,
[ColumnName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ChartSeries] ADD CONSTRAINT [PK_ChartSeries] PRIMARY KEY CLUSTERED  ([ChartSeriesID])
GO
ALTER TABLE [dbo].[ChartSeries] ADD CONSTRAINT [FK_ChartSeries_Chart] FOREIGN KEY ([ChartID]) REFERENCES [dbo].[Chart] ([ChartID])
GO
ALTER TABLE [dbo].[ChartSeries] ADD CONSTRAINT [FK_ChartSeries_ChartSeriesType] FOREIGN KEY ([ChartSeriesTypeID]) REFERENCES [dbo].[ChartSeriesType] ([ChartSeriesTypeID])
GO
ALTER TABLE [dbo].[ChartSeries] ADD CONSTRAINT [FK_ChartSeries_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ChartSeries] ADD CONSTRAINT [FK_ChartSeries_PanelItemCharting] FOREIGN KEY ([PanelItemChartingID]) REFERENCES [dbo].[PanelItemCharting] ([PanelItemChartingID])
GO
