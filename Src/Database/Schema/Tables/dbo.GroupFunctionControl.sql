CREATE TABLE [dbo].[GroupFunctionControl]
(
[GroupFunctionControlID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelAdminGroupID] [int] NOT NULL,
[ModuleID] [int] NOT NULL,
[FunctionTypeID] [int] NOT NULL,
[HasDescendants] [bit] NOT NULL,
[RightID] [int] NOT NULL,
[LeadTypeID] [int] NULL
)
GO
ALTER TABLE [dbo].[GroupFunctionControl] ADD CONSTRAINT [PK_GroupFunctionControl] PRIMARY KEY CLUSTERED  ([GroupFunctionControlID])
GO
CREATE NONCLUSTERED INDEX [IX_GroupFunctionControlGroup] ON [dbo].[GroupFunctionControl] ([ClientPersonnelAdminGroupID])
GO
CREATE NONCLUSTERED INDEX [IX_GroupFunctionControl_Covering2017] ON [dbo].[GroupFunctionControl] ([ClientPersonnelAdminGroupID], [FunctionTypeID], [HasDescendants], [RightID]) INCLUDE ([LeadTypeID], [ModuleID])
GO
ALTER TABLE [dbo].[GroupFunctionControl] ADD CONSTRAINT [FK_FunctionControl_ClientPersonnelAdminGroups] FOREIGN KEY ([ClientPersonnelAdminGroupID]) REFERENCES [dbo].[ClientPersonnelAdminGroups] ([ClientPersonnelAdminGroupID])
GO
ALTER TABLE [dbo].[GroupFunctionControl] ADD CONSTRAINT [FK_FunctionControl_FunctionType] FOREIGN KEY ([FunctionTypeID]) REFERENCES [dbo].[FunctionType] ([FunctionTypeID])
GO
ALTER TABLE [dbo].[GroupFunctionControl] ADD CONSTRAINT [FK_FunctionControl_Module] FOREIGN KEY ([ModuleID]) REFERENCES [dbo].[Module] ([ModuleID])
GO
ALTER TABLE [dbo].[GroupFunctionControl] ADD CONSTRAINT [FK_FunctionControl_Rights] FOREIGN KEY ([RightID]) REFERENCES [dbo].[Rights] ([RightID])
GO
