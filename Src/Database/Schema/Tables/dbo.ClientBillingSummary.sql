CREATE TABLE [dbo].[ClientBillingSummary]
(
[ClientBillingSummaryID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SourceAquariumOptionID] [int] NOT NULL,
[ModuleID] [int] NOT NULL,
[Year] [smallint] NOT NULL,
[Month] [tinyint] NOT NULL,
[ChargeInPence] [decimal] (18, 4) NULL,
[Notes] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ClientBillingSummary] ADD CONSTRAINT [PK_ClientBillingSummary] PRIMARY KEY CLUSTERED  ([ClientBillingSummaryID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ClientBillingSummaryUnique] ON [dbo].[ClientBillingSummary] ([ClientID], [SourceAquariumOptionID], [ModuleID], [Year], [Month])
GO
