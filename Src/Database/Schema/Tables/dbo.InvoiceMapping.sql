CREATE TABLE [dbo].[InvoiceMapping]
(
[InvoiceMappingID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[SourceInvoiceNumberField] [int] NULL,
[SourceTotalClaimAmountField] [int] NULL,
[SourceFeesToPostToSageNetField] [int] NULL,
[SourceFeesToPostToSageVatField] [int] NULL,
[InvoiceTypeCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CreditNoteTypeCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[InvoiceNumberField] [int] NULL,
[DateField] [int] NULL,
[DatePostedField] [int] NULL,
[DetailsField] [int] NULL,
[SentToSageField] [int] NULL,
[NominalCodeField] [int] NULL,
[TaxCodeField] [int] NULL,
[InvoiceNetAmountField] [int] NULL,
[InvoiceTaxAmountField] [int] NULL,
[TypeField] [int] NULL,
[MatterIDField] [int] NULL,
[CaseNumberField] [int] NULL,
[CreditNoteNetAmountField] [int] NULL,
[CreditNoteTaxAmountField] [int] NULL,
[NominalCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[TaxCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[TableRowsDetailFieldID] [int] NULL,
[TableRowsDetailFieldPageID] [int] NULL,
[FeesInvoicedToSageField] [int] NULL,
[FeesPaidFromSageField] [int] NULL,
[FeesInvoicedOtherField] [int] NULL,
[FeesPaidOtherField] [int] NULL,
[CaseBalanceField] [int] NULL,
[SageCompanyName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[CostCentre] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[InvoiceMapping] ADD CONSTRAINT [PK_InvoiceMapping] PRIMARY KEY CLUSTERED  ([InvoiceMappingID])
GO
CREATE NONCLUSTERED INDEX [IX_InvoiceMappingLeadType] ON [dbo].[InvoiceMapping] ([LeadTypeID])
GO
ALTER TABLE [dbo].[InvoiceMapping] ADD CONSTRAINT [FK_InvoiceMapping_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[InvoiceMapping] ADD CONSTRAINT [FK_InvoiceMapping_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
