CREATE TABLE [dbo].[QuillCaseMapping]
(
[QuillCaseMappingID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CaseID] [int] NOT NULL,
[QuillCaseCode] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[QuillCaseMapping] ADD CONSTRAINT [PK_QuillCaseMapping] PRIMARY KEY CLUSTERED  ([QuillCaseMappingID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_QuillCaseMapping] ON [dbo].[QuillCaseMapping] ([CaseID])
GO
ALTER TABLE [dbo].[QuillCaseMapping] ADD CONSTRAINT [FK_QuillCaseMapping_Cases] FOREIGN KEY ([CaseID]) REFERENCES [dbo].[Cases] ([CaseID])
GO
ALTER TABLE [dbo].[QuillCaseMapping] ADD CONSTRAINT [FK_QuillCaseMapping_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
