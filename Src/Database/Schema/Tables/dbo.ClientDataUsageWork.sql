CREATE TABLE [dbo].[ClientDataUsageWork]
(
[ClientDataUsageWorkID] [int] NOT NULL IDENTITY(1, 1),
[ClientDataUsageBillingRunID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[TableName] [sys].[sysname] NOT NULL,
[RecordCount] [int] NULL,
[FixedBytesPerRow] [int] NOT NULL,
[FixedBytesForThisClient] [bigint] NOT NULL,
[VariableBytesForThisClient] [bigint] NOT NULL,
[TotalBytesForThisClient] [bigint] NOT NULL,
[IsCustomTable] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[ClientDataUsageWork] ADD CONSTRAINT [PK_ClientDataUsageWork] PRIMARY KEY CLUSTERED  ([ClientDataUsageWorkID])
GO
