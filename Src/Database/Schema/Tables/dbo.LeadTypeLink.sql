CREATE TABLE [dbo].[LeadTypeLink]
(
[LeadTypeLinkID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[OutcomeID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[LeadTypeLink] ADD CONSTRAINT [PK_MatterLinks] PRIMARY KEY CLUSTERED  ([LeadTypeLinkID])
GO
ALTER TABLE [dbo].[LeadTypeLink] ADD CONSTRAINT [IX_LeadTypeLink] UNIQUE NONCLUSTERED  ([ClientID], [LeadTypeID], [OutcomeID])
GO
ALTER TABLE [dbo].[LeadTypeLink] ADD CONSTRAINT [FK_LeadTypeLinks_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[LeadTypeLink] ADD CONSTRAINT [FK_LeadTypeLinks_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[LeadTypeLink] ADD CONSTRAINT [FK_LeadTypeLinks_Outcomes] FOREIGN KEY ([OutcomeID]) REFERENCES [dbo].[Outcomes] ([OutcomeID]) ON DELETE CASCADE
GO
