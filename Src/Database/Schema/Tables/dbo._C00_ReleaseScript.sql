CREATE TABLE [dbo].[_C00_ReleaseScript]
(
[ReleaseScriptID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[FromDB] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[ToDB] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[SQLToExec] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[DoExec] [bit] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenExecuted] [datetime] NULL,
[FailureText] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Source] [int] NULL
)
GO
ALTER TABLE [dbo].[_C00_ReleaseScript] ADD CONSTRAINT [PK__C00_ReleaseScript] PRIMARY KEY CLUSTERED  ([ReleaseScriptID])
GO
