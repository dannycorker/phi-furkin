CREATE TABLE [dbo].[BACSDDICImport]
(
[DDICImportID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[Reference] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[BACSProcessedDate] [date] NOT NULL,
[PaymentAmount] [numeric] (18, 2) NOT NULL,
[FailureCode] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[PaymentID] [int] NULL,
[AccountID] [int] NULL,
[Comments] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ImportStatusID] [int] NULL,
[ImportedDate] [datetime] NULL,
[FileName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[BACSDDICImport] ADD CONSTRAINT [PK_Billing_DDICImport] PRIMARY KEY CLUSTERED  ([DDICImportID])
GO
