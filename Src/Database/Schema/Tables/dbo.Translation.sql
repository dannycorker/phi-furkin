CREATE TABLE [dbo].[Translation]
(
[TranslationID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DetailFieldID] [int] NULL,
[ID] [int] NOT NULL,
[Translation] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[LanguageID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[Translation] ADD CONSTRAINT [PK_Translation] PRIMARY KEY CLUSTERED  ([TranslationID])
GO
CREATE NONCLUSTERED INDEX [IX_Translation_ID_DetailField_Language] ON [dbo].[Translation] ([ID], [DetailFieldID], [LanguageID]) INCLUDE ([ClientID])
GO
ALTER TABLE [dbo].[Translation] ADD CONSTRAINT [FK_Translation_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[Translation] ADD CONSTRAINT [FK_Translation_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID])
GO
