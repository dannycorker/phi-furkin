CREATE TABLE [dbo].[CalcsEngine_ParameterTypes]
(
[ParameterTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[CalcsEngine_ParameterTypes] ADD CONSTRAINT [PK_CalcEngine_ParameterTypes] PRIMARY KEY CLUSTERED  ([ParameterTypeID])
GO
