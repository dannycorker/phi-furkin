CREATE TABLE [dbo].[ClientCardType]
(
[ClientCardTypeID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CardTypeID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ClientCardType] ADD CONSTRAINT [PK_ClientCardType] PRIMARY KEY CLUSTERED  ([ClientCardTypeID])
GO
