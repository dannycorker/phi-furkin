CREATE TABLE [dbo].[SMSQuestionGroupMember]
(
[SMSQuestionGroupMemberID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SMSQuestionnaireID] [int] NOT NULL,
[SMSQuestionID] [int] NOT NULL,
[SMSQuestionGroupID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[SMSQuestionGroupMember] ADD CONSTRAINT [PK_SMSQuestionGroupMember] PRIMARY KEY CLUSTERED  ([SMSQuestionGroupMemberID])
GO
ALTER TABLE [dbo].[SMSQuestionGroupMember] ADD CONSTRAINT [FK_SMSQuestionGroupMember_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SMSQuestionGroupMember] ADD CONSTRAINT [FK_SMSQuestionGroupMember_SMSQuestion] FOREIGN KEY ([SMSQuestionID]) REFERENCES [dbo].[SMSQuestion] ([SMSQuestionID])
GO
ALTER TABLE [dbo].[SMSQuestionGroupMember] ADD CONSTRAINT [FK_SMSQuestionGroupMember_SMSQuestionGroup] FOREIGN KEY ([SMSQuestionGroupID]) REFERENCES [dbo].[SMSQuestionGroup] ([SMSQuestionGroupID])
GO
ALTER TABLE [dbo].[SMSQuestionGroupMember] ADD CONSTRAINT [FK_SMSQuestionGroupMember_SMSQuestionGroupMember] FOREIGN KEY ([SMSQuestionnaireID]) REFERENCES [dbo].[SMSQuestionnaire] ([SMSQuestionnaireID])
GO
