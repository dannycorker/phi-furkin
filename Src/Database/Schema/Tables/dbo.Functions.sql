CREATE TABLE [dbo].[Functions]
(
[FunctionID] [int] NOT NULL,
[FunctionName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[FunctionDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[FunctionTypeID] [int] NULL,
[ModuleID] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[Functions] ADD CONSTRAINT [PK_Functions] PRIMARY KEY CLUSTERED  ([FunctionID])
GO
ALTER TABLE [dbo].[Functions] ADD CONSTRAINT [FK_Functions_FunctionType] FOREIGN KEY ([FunctionTypeID]) REFERENCES [dbo].[FunctionType] ([FunctionTypeID])
GO
ALTER TABLE [dbo].[Functions] ADD CONSTRAINT [FK_Functions_Module] FOREIGN KEY ([ModuleID]) REFERENCES [dbo].[Module] ([ModuleID])
GO
