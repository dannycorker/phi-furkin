CREATE TABLE [dbo].[ClientQuestionnairesVisitedAndCompleted]
(
[ClientQuestionnairesVisitedAndCompletedID] [int] NOT NULL IDENTITY(1, 1),
[ClientQuestionnaireID] [int] NULL,
[VisitedDate] [datetime] NULL,
[IPAddress] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ClientQuestionnairesVisitedAndCompleted] ADD CONSTRAINT [PK_ClientQuestionnairesVisitedAndCompleted] PRIMARY KEY CLUSTERED  ([ClientQuestionnairesVisitedAndCompletedID])
GO
ALTER TABLE [dbo].[ClientQuestionnairesVisitedAndCompleted] ADD CONSTRAINT [FK_ClientQuestionnairesVisitedAndCompleted_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
