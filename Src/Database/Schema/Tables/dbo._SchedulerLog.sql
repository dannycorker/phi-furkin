CREATE TABLE [dbo].[_SchedulerLog]
(
[AnyID] [int] NOT NULL IDENTITY(1, 1),
[Source] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[EventLog] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[RecordNumber] [int] NULL,
[TimeGenerated] [datetime] NULL,
[TimeWritten] [datetime] NULL,
[EventID] [int] NULL,
[EventType] [int] NULL,
[EventTypeName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[EventCategory] [int] NULL,
[EventCategoryName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[SourceName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Strings] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ComputerName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[SID] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Message] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Data] [varchar] (255) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[_SchedulerLog] ADD CONSTRAINT [PK__SchedulerLog] PRIMARY KEY CLUSTERED  ([AnyID])
GO
GRANT INSERT ON  [dbo].[_SchedulerLog] TO [AquariumNet]
GO
