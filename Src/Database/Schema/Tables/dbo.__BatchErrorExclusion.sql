CREATE TABLE [dbo].[__BatchErrorExclusion]
(
[BatchErrorExclusionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[FinishDate] [date] NOT NULL,
[Reason] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[__BatchErrorExclusion] ADD CONSTRAINT [PK_BatchErrorExclusion] PRIMARY KEY CLUSTERED  ([BatchErrorExclusionID])
GO
