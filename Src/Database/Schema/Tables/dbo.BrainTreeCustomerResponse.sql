CREATE TABLE [dbo].[BrainTreeCustomerResponse]
(
[BrainTreeCustomerResponseID] [int] NOT NULL IDENTITY(1, 1),
[MatterID] [int] NOT NULL,
[CreateCustomerXML] [xml] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[BrainTreeCustomerResponse] ADD CONSTRAINT [PK_BrainTreeCustomerResponse] PRIMARY KEY CLUSTERED  ([BrainTreeCustomerResponseID])
GO
