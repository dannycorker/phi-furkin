CREATE TABLE [dbo].[PartnerMatchKey]
(
[PartnerMatchKeyID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[PartnerID] [int] NOT NULL,
[FirstName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Address1] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Postcode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DateOfBirth] [date] NULL
)
GO
ALTER TABLE [dbo].[PartnerMatchKey] ADD CONSTRAINT [PK_PartnerMatchKeyID] PRIMARY KEY CLUSTERED  ([PartnerMatchKeyID])
GO
CREATE NONCLUSTERED INDEX [IX_PartnerMatchKey_Combined] ON [dbo].[PartnerMatchKey] ([ClientID], [FirstName], [LastName], [Address1], [Postcode], [DateOfBirth])
GO
