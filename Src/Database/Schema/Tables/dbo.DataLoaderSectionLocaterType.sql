CREATE TABLE [dbo].[DataLoaderSectionLocaterType]
(
[DataLoaderSectionLocaterTypeID] [int] NOT NULL,
[LocaterTypeName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[LocaterTypeDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[DataLoaderSectionLocaterType] ADD CONSTRAINT [PK_DataLoaderSectionLocater] PRIMARY KEY CLUSTERED  ([DataLoaderSectionLocaterTypeID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DataLoaderSectionLocaterType] ON [dbo].[DataLoaderSectionLocaterType] ([LocaterTypeName])
GO
