CREATE TABLE [dbo].[SqlFunctionParam]
(
[SqlFunctionParamID] [int] NOT NULL IDENTITY(1, 1),
[SqlFunctionID] [int] NOT NULL,
[ParamName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[ParamDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[ParamOrder] [int] NOT NULL,
[IsMandatory] [int] NOT NULL,
[SqlFunctionDataTypeID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[IsRecurring] [bit] NOT NULL,
[DecimalPlaces] [int] NULL,
[MinValue] [int] NULL,
[MaxValue] [int] NULL,
[MaxLength] [int] NULL,
[UseQueryColumn] [bit] NOT NULL,
[QuoteValue] [bit] NOT NULL,
[PrefixText] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[SuffixText] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ListTypeID] [int] NULL
)
GO
ALTER TABLE [dbo].[SqlFunctionParam] ADD CONSTRAINT [PK_SqlFunctionParam] PRIMARY KEY CLUSTERED  ([SqlFunctionParamID])
GO
