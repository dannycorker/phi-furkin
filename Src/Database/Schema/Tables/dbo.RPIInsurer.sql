CREATE TABLE [dbo].[RPIInsurer]
(
[RID] [int] NOT NULL IDENTITY(1, 1),
[InsurerName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[ContactName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[RPIInsurerID] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[RPIInsurerName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[RPIInsurer] ADD CONSTRAINT [PK_RPIInsurer] PRIMARY KEY CLUSTERED  ([RID])
GO
