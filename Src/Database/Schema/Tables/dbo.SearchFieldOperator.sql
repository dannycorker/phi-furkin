CREATE TABLE [dbo].[SearchFieldOperator]
(
[SearchFieldOperatorID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[SearchFieldID] [int] NOT NULL,
[SearchOperatorID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[SearchFieldOperator] ADD CONSTRAINT [PK_SearchFieldOperator] PRIMARY KEY CLUSTERED  ([SearchFieldOperatorID])
GO
ALTER TABLE [dbo].[SearchFieldOperator] ADD CONSTRAINT [FK_SearchFieldOperator_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SearchFieldOperator] ADD CONSTRAINT [FK_SearchFieldOperator_SearchField] FOREIGN KEY ([SearchFieldID]) REFERENCES [dbo].[SearchField] ([SearchFieldID])
GO
ALTER TABLE [dbo].[SearchFieldOperator] ADD CONSTRAINT [FK_SearchFieldOperator_SearchOperator] FOREIGN KEY ([SearchOperatorID]) REFERENCES [dbo].[SearchOperator] ([SearchOperatorID])
GO
ALTER TABLE [dbo].[SearchFieldOperator] ADD CONSTRAINT [FK_SearchFieldOperator_SubClient] FOREIGN KEY ([SubClientID]) REFERENCES [dbo].[SubClient] ([SubClientID])
GO
