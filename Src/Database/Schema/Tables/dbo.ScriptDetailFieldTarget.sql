CREATE TABLE [dbo].[ScriptDetailFieldTarget]
(
[ScriptDetailFieldTargetID] [int] NOT NULL IDENTITY(1, 1),
[ScriptID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[Target] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[DetailFieldID] [int] NULL,
[ColumnFieldID] [int] NULL,
[Format] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[SectionID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ScriptDetailFieldTarget] ADD CONSTRAINT [PK_ScriptDetailFieldTarget] PRIMARY KEY CLUSTERED  ([ScriptDetailFieldTargetID])
GO
ALTER TABLE [dbo].[ScriptDetailFieldTarget] ADD CONSTRAINT [FK_ScriptDetailFieldTarget_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ScriptDetailFieldTarget] WITH NOCHECK ADD CONSTRAINT [FK_ScriptDetailFieldTarget_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[ScriptDetailFieldTarget] ADD CONSTRAINT [FK_ScriptDetailFieldTarget_Script] FOREIGN KEY ([ScriptID]) REFERENCES [dbo].[Script] ([ScriptID])
GO
ALTER TABLE [dbo].[ScriptDetailFieldTarget] NOCHECK CONSTRAINT [FK_ScriptDetailFieldTarget_DetailFields]
GO
