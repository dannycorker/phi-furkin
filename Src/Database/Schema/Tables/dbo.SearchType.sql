CREATE TABLE [dbo].[SearchType]
(
[SearchTypeID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[SearchTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[SearchType] ADD CONSTRAINT [PK_SearchType] PRIMARY KEY CLUSTERED  ([SearchTypeID])
GO
ALTER TABLE [dbo].[SearchType] ADD CONSTRAINT [FK_SearchType_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SearchType] ADD CONSTRAINT [FK_SearchType_SubClient] FOREIGN KEY ([SubClientID]) REFERENCES [dbo].[SubClient] ([SubClientID])
GO
