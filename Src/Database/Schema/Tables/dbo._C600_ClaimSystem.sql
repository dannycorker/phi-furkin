CREATE TABLE [dbo].[_C600_ClaimSystem]
(
[ClaimSystemID] [int] NOT NULL IDENTITY(1, 1),
[ClaimSystemName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[_C600_ClaimSystem] ADD CONSTRAINT [PK_C600_ClaimSystem] PRIMARY KEY CLUSTERED  ([ClaimSystemID])
GO
