CREATE TABLE [dbo].[ChartSeriesSetting]
(
[ChartSeriesSettingID] [int] NOT NULL IDENTITY(1, 1),
[ChartSeriesID] [int] NOT NULL,
[SettingName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[SettingValue] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ChartSeriesSetting] ADD CONSTRAINT [PK_ChartSeriesSetting] PRIMARY KEY CLUSTERED  ([ChartSeriesSettingID])
GO
ALTER TABLE [dbo].[ChartSeriesSetting] ADD CONSTRAINT [FK_ChartSeriesSetting_ChartSeries] FOREIGN KEY ([ChartSeriesID]) REFERENCES [dbo].[ChartSeries] ([ChartSeriesID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ChartSeriesSetting] ADD CONSTRAINT [FK_ChartSeriesSetting_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
