CREATE TABLE [dbo].[ReferenceValueSectionTypes]
(
[ReferenceValueSectionTypeID] [int] NOT NULL IDENTITY(1, 1),
[SectionTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ReferenceValueSectionTypes] ADD CONSTRAINT [PK_ReferenceValueSections] PRIMARY KEY CLUSTERED  ([ReferenceValueSectionTypeID])
GO
