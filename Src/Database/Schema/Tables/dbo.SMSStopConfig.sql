CREATE TABLE [dbo].[SMSStopConfig]
(
[SMSStopConfigID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[isLeadTypeSpecific] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[SMSStopConfig] ADD CONSTRAINT [PK_SMSStopConfig] PRIMARY KEY CLUSTERED  ([SMSStopConfigID])
GO
