CREATE TABLE [dbo].[AquariumStatus]
(
[AquariumStatusID] [int] NOT NULL,
[AquariumStatusName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[AquariumStatusDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Usable] [bit] NOT NULL CONSTRAINT [DF__AquariumS__Usabl__36D11DD4] DEFAULT ((1))
)
GO
ALTER TABLE [dbo].[AquariumStatus] ADD CONSTRAINT [PK_AquariumStatus] PRIMARY KEY CLUSTERED  ([AquariumStatusID])
GO
