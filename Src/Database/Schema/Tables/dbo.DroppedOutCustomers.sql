CREATE TABLE [dbo].[DroppedOutCustomers]
(
[CustomerID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[TitleID] [int] NULL,
[FirstName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[MiddleName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[EmailAddress] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[DayTimeTelephoneNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[HomeTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MobileTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CompanyTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[WorksTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Address1] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Address2] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Town] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[County] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[PostCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[HasDownloaded] [bit] NULL,
[DownloadedOn] [datetime] NULL,
[AquariumStatusID] [int] NOT NULL CONSTRAINT [DF__DroppedOu__Aquar__5EDF0F2E] DEFAULT ((1)),
[ClientStatusID] [int] NULL,
[Test] [bit] NULL CONSTRAINT [DF__DroppedOut__Test__60C757A0] DEFAULT ((0)),
[CompanyName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Occupation] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Employer] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ReminderEmailSentDate] [datetime] NULL,
[ReminderSmsSentDate] [datetime] NULL,
[CountryID] [int] NULL CONSTRAINT [DF__DroppedOu__Count__5FD33367] DEFAULT ((232))
)
GO
ALTER TABLE [dbo].[DroppedOutCustomers] ADD CONSTRAINT [PK_DroppedOutCustomers] PRIMARY KEY CLUSTERED  ([CustomerID])
GO
ALTER TABLE [dbo].[DroppedOutCustomers] ADD CONSTRAINT [FK_DroppedOutCustomers_AquariumStatus] FOREIGN KEY ([AquariumStatusID]) REFERENCES [dbo].[AquariumStatus] ([AquariumStatusID])
GO
ALTER TABLE [dbo].[DroppedOutCustomers] ADD CONSTRAINT [FK_DroppedOutCustomers_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
