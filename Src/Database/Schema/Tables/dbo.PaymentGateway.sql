CREATE TABLE [dbo].[PaymentGateway]
(
[PaymentGatewayID] [int] NOT NULL IDENTITY(1, 1),
[Gateway] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Name] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (300) COLLATE Latin1_General_CI_AS NULL,
[Url] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PaymentGateway] ADD CONSTRAINT [PK_PaymentGateway] PRIMARY KEY CLUSTERED  ([PaymentGatewayID])
GO
