CREATE TABLE [dbo].[CategoryGroup]
(
[CategoryGroupID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CategoryContextID] [int] NOT NULL,
[GroupName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[CategoryGroup] ADD CONSTRAINT [PK_CategoryGroup] PRIMARY KEY CLUSTERED  ([CategoryGroupID])
GO
ALTER TABLE [dbo].[CategoryGroup] ADD CONSTRAINT [FK_CategoryGroup_CategoryContext] FOREIGN KEY ([CategoryContextID]) REFERENCES [dbo].[CategoryContext] ([CategoryContextID])
GO
ALTER TABLE [dbo].[CategoryGroup] ADD CONSTRAINT [FK_CategoryGroup_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
