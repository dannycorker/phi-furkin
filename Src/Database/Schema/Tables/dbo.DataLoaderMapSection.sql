CREATE TABLE [dbo].[DataLoaderMapSection]
(
[DataLoaderMapSectionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DataLoaderMapID] [int] NOT NULL,
[DataLoaderObjectTypeID] [int] NOT NULL,
[DataLoaderObjectActionID] [int] NULL,
[DataLoaderSectionLocaterTypeID] [int] NULL,
[DetailFieldSubTypeID] [tinyint] NULL,
[HasAquariumID] [bit] NOT NULL,
[NumberOfFooterRowsToSkip] [int] NOT NULL,
[ImportIntoLeadManager] [bit] NULL,
[IsFixedLengthSection] [bit] NOT NULL,
[FixedLengthNumberOfRows] [int] NULL,
[IsMandatory] [bit] NOT NULL,
[IsMultipleAllowedWithinFile] [bit] NOT NULL,
[Notes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[TableDetailFieldID] [int] NULL,
[ResourceListDetailFieldID] [int] NULL,
[CreatePaymentSchedule] [bit] NULL
)
GO
ALTER TABLE [dbo].[DataLoaderMapSection] ADD CONSTRAINT [PK_DataLoaderMapSection] PRIMARY KEY CLUSTERED  ([DataLoaderMapSectionID])
GO
ALTER TABLE [dbo].[DataLoaderMapSection] ADD CONSTRAINT [FK_DataLoaderMapSection_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DataLoaderMapSection] ADD CONSTRAINT [FK_DataLoaderMapSection_DataLoaderMap] FOREIGN KEY ([DataLoaderMapID]) REFERENCES [dbo].[DataLoaderMap] ([DataLoaderMapID])
GO
ALTER TABLE [dbo].[DataLoaderMapSection] ADD CONSTRAINT [FK_DataLoaderMapSection_DataLoaderObjectAction] FOREIGN KEY ([DataLoaderObjectActionID]) REFERENCES [dbo].[DataLoaderObjectAction] ([DataLoaderObjectActionID])
GO
ALTER TABLE [dbo].[DataLoaderMapSection] ADD CONSTRAINT [FK_DataLoaderMapSection_DataLoaderObjectType] FOREIGN KEY ([DataLoaderObjectTypeID]) REFERENCES [dbo].[DataLoaderObjectType] ([DataLoaderObjectTypeID])
GO
ALTER TABLE [dbo].[DataLoaderMapSection] ADD CONSTRAINT [FK_DataLoaderMapSection_DataLoaderSectionLocaterType] FOREIGN KEY ([DataLoaderSectionLocaterTypeID]) REFERENCES [dbo].[DataLoaderSectionLocaterType] ([DataLoaderSectionLocaterTypeID])
GO
ALTER TABLE [dbo].[DataLoaderMapSection] ADD CONSTRAINT [FK_DataLoaderMapSection_DetailFieldSubType] FOREIGN KEY ([DetailFieldSubTypeID]) REFERENCES [dbo].[DetailFieldSubType] ([DetailFieldSubTypeID])
GO
