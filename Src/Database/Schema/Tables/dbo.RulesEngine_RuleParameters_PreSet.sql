CREATE TABLE [dbo].[RulesEngine_RuleParameters_PreSet]
(
[RuleParameterPreSetID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[ParameterTypeID] [int] NOT NULL,
[Value] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DataTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[Parent] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SourceID] [int] NULL
)
GO
ALTER TABLE [dbo].[RulesEngine_RuleParameters_PreSet] ADD CONSTRAINT [PK_RulesEngine_RuleParameters_PreSet] PRIMARY KEY CLUSTERED  ([RuleParameterPreSetID])
GO
ALTER TABLE [dbo].[RulesEngine_RuleParameters_PreSet] ADD CONSTRAINT [FK_RulesEngine_RuleParameters_PreSet_RulesEngine_ParameterTypes] FOREIGN KEY ([ParameterTypeID]) REFERENCES [dbo].[RulesEngine_ParameterTypes] ([ParameterTypeID])
GO
