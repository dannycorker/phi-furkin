CREATE TABLE [dbo].[QuestionPossibleAnswers]
(
[QuestionPossibleAnswerID] [int] NOT NULL IDENTITY(1, 1),
[ClientQuestionnaireID] [int] NULL,
[MasterQuestionID] [int] NULL,
[AnswerText] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Branch] [int] NULL,
[LinkedQuestionnaireQuestionPossibleAnswerID] [int] NULL,
[ClientID] [int] NOT NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[QuestionPossibleAnswers] ADD CONSTRAINT [PK_QuestionPossibleAnswers] PRIMARY KEY CLUSTERED  ([QuestionPossibleAnswerID])
GO
CREATE NONCLUSTERED INDEX [IX_QuestionPossibleAnswersClient] ON [dbo].[QuestionPossibleAnswers] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_QuestionPossibleAnswersClientQuestionnaire] ON [dbo].[QuestionPossibleAnswers] ([ClientQuestionnaireID])
GO
CREATE NONCLUSTERED INDEX [IX_QuestionPossibleAnswersMasterQuestion] ON [dbo].[QuestionPossibleAnswers] ([MasterQuestionID])
GO
CREATE NONCLUSTERED INDEX [IX_QuestionPossibleAnswers_SourceID] ON [dbo].[QuestionPossibleAnswers] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[QuestionPossibleAnswers] ADD CONSTRAINT [FK_QuestionPossibleAnswers_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[QuestionPossibleAnswers] ADD CONSTRAINT [FK_QuestionPossibleAnswers_MasterQuestions] FOREIGN KEY ([MasterQuestionID]) REFERENCES [dbo].[MasterQuestions] ([MasterQuestionID])
GO
