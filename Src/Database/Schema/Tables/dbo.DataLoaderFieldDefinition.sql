CREATE TABLE [dbo].[DataLoaderFieldDefinition]
(
[DataLoaderFieldDefinitionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DataLoaderMapID] [int] NOT NULL,
[DataLoaderObjectTypeID] [int] NOT NULL,
[DataLoaderMapSectionID] [int] NOT NULL,
[DataLoaderObjectFieldID] [int] NULL,
[DetailFieldID] [int] NULL,
[DetailFieldAlias] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[NamedValue] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Keyword] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[DataLoaderKeywordMatchTypeID] [int] NULL,
[RowRelativeToKeyword] [int] NULL,
[ColRelativeToKeyword] [int] NULL,
[SectionRelativeRow] [int] NULL,
[SectionAbsoluteCol] [int] NULL,
[ValidationRegex] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Equation] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[IsMatchField] [bit] NULL,
[DecodeTypeID] [int] NULL,
[DefaultLookupItemID] [int] NULL,
[SourceDataLoaderFieldDefinitionID] [int] NULL,
[Notes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[AllowErrors] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[DataLoaderFieldDefinition] ADD CONSTRAINT [CK__DataLoaderFieldD__1BE81D6E] CHECK (((2)>((case  when [DataLoaderObjectFieldID] IS NULL then (0) else (1) end+case  when [DetailFieldID] IS NULL then (0) else (1) end)+case  when [DetailFieldAlias] IS NULL then (0) else (1) end)))
GO
ALTER TABLE [dbo].[DataLoaderFieldDefinition] ADD CONSTRAINT [PK_DataLoaderFieldDefinition] PRIMARY KEY CLUSTERED  ([DataLoaderFieldDefinitionID])
GO
ALTER TABLE [dbo].[DataLoaderFieldDefinition] ADD CONSTRAINT [FK_DataLoaderFieldDefinition_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DataLoaderFieldDefinition] ADD CONSTRAINT [FK_DataLoaderFieldDefinition_DataLoaderDecodeType] FOREIGN KEY ([DecodeTypeID]) REFERENCES [dbo].[DataLoaderDecodeType] ([DataLoaderDecodeTypeID])
GO
ALTER TABLE [dbo].[DataLoaderFieldDefinition] ADD CONSTRAINT [FK_DataLoaderFieldDefinition_DataLoaderFieldDefinition] FOREIGN KEY ([SourceDataLoaderFieldDefinitionID]) REFERENCES [dbo].[DataLoaderFieldDefinition] ([DataLoaderFieldDefinitionID])
GO
ALTER TABLE [dbo].[DataLoaderFieldDefinition] ADD CONSTRAINT [FK_DataLoaderFieldDefinition_DataLoaderKeywordMatchType] FOREIGN KEY ([DataLoaderKeywordMatchTypeID]) REFERENCES [dbo].[DataLoaderKeywordMatchType] ([DataLoaderKeywordMatchTypeID])
GO
ALTER TABLE [dbo].[DataLoaderFieldDefinition] ADD CONSTRAINT [FK_DataLoaderFieldDefinition_DataLoaderMap] FOREIGN KEY ([DataLoaderMapID]) REFERENCES [dbo].[DataLoaderMap] ([DataLoaderMapID])
GO
ALTER TABLE [dbo].[DataLoaderFieldDefinition] ADD CONSTRAINT [FK_DataLoaderFieldDefinition_DataLoaderMapSection] FOREIGN KEY ([DataLoaderMapSectionID]) REFERENCES [dbo].[DataLoaderMapSection] ([DataLoaderMapSectionID])
GO
ALTER TABLE [dbo].[DataLoaderFieldDefinition] ADD CONSTRAINT [FK_DataLoaderFieldDefinition_DataLoaderObjectField] FOREIGN KEY ([DataLoaderObjectFieldID]) REFERENCES [dbo].[DataLoaderObjectField] ([DataLoaderObjectFieldID])
GO
ALTER TABLE [dbo].[DataLoaderFieldDefinition] ADD CONSTRAINT [FK_DataLoaderFieldDefinition_DataLoaderObjectType] FOREIGN KEY ([DataLoaderObjectTypeID]) REFERENCES [dbo].[DataLoaderObjectType] ([DataLoaderObjectTypeID])
GO
ALTER TABLE [dbo].[DataLoaderFieldDefinition] ADD CONSTRAINT [FK_DataLoaderFieldDefinition_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
