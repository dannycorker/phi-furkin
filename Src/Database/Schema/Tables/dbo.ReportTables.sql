CREATE TABLE [dbo].[ReportTables]
(
[ReportTableID] [int] NOT NULL IDENTITY(1, 1),
[ReportTableName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[DisplayOrder] [int] NOT NULL,
[RequiresClientSecurity] [bit] NOT NULL,
[IsLookupTable] [bit] NOT NULL CONSTRAINT [DF__ReportTab__IsLoo__7C6F7215] DEFAULT ((0)),
[ShowAquariumValues] [bit] NOT NULL,
[WhenCreated] [datetime] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[ReportTables] ADD CONSTRAINT [PK_ReportTables] PRIMARY KEY CLUSTERED  ([ReportTableID])
GO
