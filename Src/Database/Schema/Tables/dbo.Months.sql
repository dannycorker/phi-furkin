CREATE TABLE [dbo].[Months]
(
[MonthID] [tinyint] NOT NULL,
[MonthText] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[Month3Text] [char] (3) COLLATE Latin1_General_CI_AS NOT NULL,
[Month4Text] [varchar] (4) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[Months] ADD CONSTRAINT [PK_Months] PRIMARY KEY CLUSTERED  ([MonthID])
GO
