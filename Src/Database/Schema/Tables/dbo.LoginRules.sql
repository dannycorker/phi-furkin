CREATE TABLE [dbo].[LoginRules]
(
[ClientID] [int] NOT NULL,
[IsAllowed] [bit] NOT NULL CONSTRAINT [DF__LoginRule__IsAll__56AB528E] DEFAULT ((1))
)
GO
ALTER TABLE [dbo].[LoginRules] ADD CONSTRAINT [PK_LoginRules] PRIMARY KEY CLUSTERED  ([ClientID])
GO
ALTER TABLE [dbo].[LoginRules] ADD CONSTRAINT [FK_LoginRules_LoginRules] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[LoginRules] ([ClientID])
GO
