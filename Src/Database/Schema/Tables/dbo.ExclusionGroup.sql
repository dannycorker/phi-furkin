CREATE TABLE [dbo].[ExclusionGroup]
(
[ExclusionGroupID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[IsEnabled] [bit] NOT NULL,
[ExclusionGroupName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[ExclusionGroupDescription] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[ExclusionGroup] ADD CONSTRAINT [PK_ExclusionGroup] PRIMARY KEY CLUSTERED  ([ExclusionGroupID])
GO
CREATE NONCLUSTERED INDEX [IX_ExclusionGroup_ClientIsEnabled] ON [dbo].[ExclusionGroup] ([ClientID], [IsEnabled])
GO
CREATE NONCLUSTERED INDEX [IX_ExclusionGroup] ON [dbo].[ExclusionGroup] ([ExclusionGroupID])
GO
CREATE NONCLUSTERED INDEX [IX_ExclusionGroup_SourceID] ON [dbo].[ExclusionGroup] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[ExclusionGroup] ADD CONSTRAINT [FK_ExclusionGroup_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
