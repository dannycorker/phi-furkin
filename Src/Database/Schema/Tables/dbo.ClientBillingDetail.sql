CREATE TABLE [dbo].[ClientBillingDetail]
(
[ClientBillingDetailID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SourceAquariumOptionID] [int] NOT NULL,
[ModuleID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NULL,
[ChargeInPence] [decimal] (18, 4) NULL,
[ClientBillingSummaryID] [int] NULL,
[Notes] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[SourceObjectUID] [int] NULL,
[SourceTableName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[MessageGUID] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[MobileTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SmsGatewayID] [int] NULL
)
GO
ALTER TABLE [dbo].[ClientBillingDetail] ADD CONSTRAINT [PK_ClientBillingDetail] PRIMARY KEY CLUSTERED  ([ClientBillingDetailID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientBillingDetail_MessageGUID] ON [dbo].[ClientBillingDetail] ([MessageGUID], [SourceObjectUID], [ClientID]) WHERE ([MessageGUID] IS NOT NULL)
GO
