CREATE TABLE [dbo].[PortalUserGroupPanelItemChartingAccess]
(
[PortalUserGroupPanelItemChartingAccessID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[PortalUserGroupID] [int] NOT NULL,
[PanelItemChartingID] [int] NOT NULL,
[HasAccess] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[PortalUserGroupPanelItemChartingAccess] ADD CONSTRAINT [PK_PortalUserGroupPanelItemChartingAccess] PRIMARY KEY CLUSTERED  ([PortalUserGroupPanelItemChartingAccessID])
GO
ALTER TABLE [dbo].[PortalUserGroupPanelItemChartingAccess] ADD CONSTRAINT [FK_PortalUserGroupPanelItemChartingAccess_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PortalUserGroupPanelItemChartingAccess] ADD CONSTRAINT [FK_PortalUserGroupPanelItemChartingAccess_PanelItemCharting] FOREIGN KEY ([PanelItemChartingID]) REFERENCES [dbo].[PanelItemCharting] ([PanelItemChartingID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PortalUserGroupPanelItemChartingAccess] ADD CONSTRAINT [FK_PortalUserGroupPanelItemChartingAccess_PortalUserGroup] FOREIGN KEY ([PortalUserGroupID]) REFERENCES [dbo].[PortalUserGroup] ([PortalUserGroupID]) ON DELETE CASCADE
GO
