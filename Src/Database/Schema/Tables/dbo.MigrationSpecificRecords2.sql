CREATE TABLE [dbo].[MigrationSpecificRecords2]
(
[CustomerID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[CaseID] [int] NULL,
[MatterID] [int] NOT NULL,
[ClientID] [int] NOT NULL
)
GO
