CREATE TABLE [dbo].[PurchasedProductPaymentScheduleHistory]
(
[PurchasedProductPaymentScheduleHistoryID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[AccountID] [int] NOT NULL,
[PurchasedProductID] [int] NOT NULL,
[ActualCollectionDate] [datetime] NULL,
[CoverFrom] [datetime] NULL,
[CoverTo] [datetime] NULL,
[PaymentDate] [datetime] NULL,
[PaymentNet] [numeric] (18, 2) NULL,
[PaymentVAT] [numeric] (18, 2) NULL,
[PaymentGross] [numeric] (18, 2) NULL,
[PaymentStatusID] [int] NULL,
[CustomerLedgerID] [int] NULL,
[ReconciledDate] [datetime] NULL,
[CustomerPaymentScheduleID] [int] NULL,
[OriginalPurchasedProductPaymentScheduleID] [int] NOT NULL,
[OriginalWhoCreated] [int] NOT NULL,
[OriginalWhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[PurchasedProductPaymentScheduleParentID] [int] NULL,
[PurchasedProductPaymentScheduleTypeID] [int] NULL,
[PaymentGroupedIntoID] [int] NULL,
[ClientAccountID] [int] NULL,
[TransactionFee] [numeric] (18, 2) NULL
)
GO
ALTER TABLE [dbo].[PurchasedProductPaymentScheduleHistory] ADD CONSTRAINT [PK_ProductPaymentScheduleHistory] PRIMARY KEY CLUSTERED  ([PurchasedProductPaymentScheduleHistoryID])
GO
ALTER TABLE [dbo].[PurchasedProductPaymentScheduleHistory] ADD CONSTRAINT [FK_ProductPaymentScheduleHistory_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
GO
ALTER TABLE [dbo].[PurchasedProductPaymentScheduleHistory] ADD CONSTRAINT [FK_ProductPaymentScheduleHistory_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[PurchasedProductPaymentScheduleHistory] ADD CONSTRAINT [FK_ProductPaymentScheduleHistory_CustomerLedger] FOREIGN KEY ([CustomerLedgerID]) REFERENCES [dbo].[CustomerLedger] ([CustomerLedgerID])
GO
ALTER TABLE [dbo].[PurchasedProductPaymentScheduleHistory] ADD CONSTRAINT [FK_ProductPaymentScheduleHistory_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[PurchasedProductPaymentScheduleHistory] ADD CONSTRAINT [FK_ProductPaymentScheduleHistory_PurchasedProduct] FOREIGN KEY ([PurchasedProductID]) REFERENCES [dbo].[PurchasedProduct] ([PurchasedProductID])
GO
