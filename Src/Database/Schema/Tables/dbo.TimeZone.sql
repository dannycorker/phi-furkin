CREATE TABLE [dbo].[TimeZone]
(
[TimeZoneID] [int] NOT NULL IDENTITY(1, 1),
[TimeZoneKey] [varchar] (150) COLLATE Latin1_General_CI_AS NOT NULL,
[DisplayName] [varchar] (150) COLLATE Latin1_General_CI_AS NOT NULL,
[StandardName] [varchar] (150) COLLATE Latin1_General_CI_AS NOT NULL,
[DaylightName] [varchar] (150) COLLATE Latin1_General_CI_AS NOT NULL,
[BaseUtcOffset] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[SupportsDaylightSavingTime] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[TimeZone] ADD CONSTRAINT [PK_TimeZone] PRIMARY KEY CLUSTERED  ([TimeZoneID])
GO
