CREATE TABLE [dbo].[FieldTarget]
(
[FieldTargetID] [int] NOT NULL IDENTITY(1, 1),
[Target] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[ObjectName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[PropertyName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[FieldTarget] ADD CONSTRAINT [PK_FieldTarget] PRIMARY KEY CLUSTERED  ([FieldTargetID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_FieldTarget] ON [dbo].[FieldTarget] ([Target])
GO
