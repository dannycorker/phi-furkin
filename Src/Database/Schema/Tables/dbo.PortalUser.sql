CREATE TABLE [dbo].[PortalUser]
(
[PortalUserID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[Username] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Password] [varchar] (65) COLLATE Latin1_General_CI_AS NOT NULL,
[Salt] [varchar] (25) COLLATE Latin1_General_CI_AS NOT NULL,
[ClientPersonnelID] [int] NOT NULL,
[Enabled] [bit] NOT NULL,
[AttemptedLogins] [int] NOT NULL,
[PortalUserGroupID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2011-12-07
-- Description:	Added context info check to prevent unwanted access to table.
-- UPDATE:		Simon Brushett	2012-03-06	Changed to allow deletes but only if context has been set
-- =============================================
CREATE TRIGGER [dbo].[trgiud_PortalUser]
   ON [dbo].[PortalUser]
   AFTER INSERT, UPDATE, DELETE
AS
BEGIN
	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers' OR CAST(CONTEXT_INFO() AS VARCHAR) = 'DPADelete'
	BEGIN
		RETURN
	END

	DECLARE @InsertCount INT,
			@DeleteCount INT

	SELECT @InsertCount = COUNT(*)
	FROM  INSERTED

	SELECT @DeleteCount = COUNT(*)
	FROM  DELETED

	/*
	Only procs that call SET CONTEXT_INFO correctly can insert, update or delete from this table to protect the sync between AQ and AQMaster.
	*/
	IF (@DeleteCount > 0 AND @InsertCount = 0) OR UPDATE([Username])
	BEGIN
		IF CAST(CONTEXT_INFO() AS VARCHAR) IS NULL OR CAST(CONTEXT_INFO() AS VARCHAR) <> 'PortalUser'
		BEGIN
			ROLLBACK
		END
    END

    /*
		Inserts appear in the inserted table,
		Deletes appear in the deleted table,
		and Updates appear in both tables at once identically.
		These are all 'logical' Sql Server tables.
    */


END
GO
ALTER TABLE [dbo].[PortalUser] ADD CONSTRAINT [PK_PortalUser] PRIMARY KEY CLUSTERED  ([PortalUserID])
GO
ALTER TABLE [dbo].[PortalUser] ADD CONSTRAINT [IX_PortalUser_CustomerID] UNIQUE NONCLUSTERED  ([CustomerID])
GO
ALTER TABLE [dbo].[PortalUser] ADD CONSTRAINT [IX_PortalUser_Username] UNIQUE NONCLUSTERED  ([Username])
GO
ALTER TABLE [dbo].[PortalUser] ADD CONSTRAINT [FK_PortalUser_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[PortalUser] ADD CONSTRAINT [FK_PortalUser_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[PortalUser] ADD CONSTRAINT [FK_PortalUser_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[PortalUser] ADD CONSTRAINT [FK_PortalUser_PortalUserGroup] FOREIGN KEY ([PortalUserGroupID]) REFERENCES [dbo].[PortalUserGroup] ([PortalUserGroupID])
GO
GRANT CONTROL ON  [dbo].[PortalUser] TO [sp_executeall]
GO
