CREATE TABLE [dbo].[MandateStatus]
(
[MandateStatusID] [int] NOT NULL IDENTITY(1, 1),
[Status] [varchar] (25) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[MandateStatus] ADD CONSTRAINT [PK_MandateStatus] PRIMARY KEY CLUSTERED  ([MandateStatusID])
GO
