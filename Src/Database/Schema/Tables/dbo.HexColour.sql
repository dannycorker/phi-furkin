CREATE TABLE [dbo].[HexColour]
(
[HexColourID] [int] NOT NULL IDENTITY(1, 1),
[ColourName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[HexColourCode] [varchar] (7) COLLATE Latin1_General_CI_AS NULL
)
GO
