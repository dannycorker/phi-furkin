CREATE TABLE [dbo].[ClientAccount]
(
[ClientAccountID] [int] NOT NULL IDENTITY(1, 1),
[AccountNumber] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[SortCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[SUN] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[ValidFromDate] [date] NULL,
[ValidToDate] [date] NULL,
[FriendlyName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[UsersName] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[LinkedAccountID] [int] NULL,
[Comments] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[IncomingPaymentFileName] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[OutgoingPaymentFileName] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [date] NULL,
[WhoModified] [int] NULL,
[WhenModified] [date] NULL,
[AccountUseID] [int] NULL,
[OutgoingMandateFileName] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[CurrencyID] [int] NULL
)
GO
ALTER TABLE [dbo].[ClientAccount] ADD CONSTRAINT [PK_ClientAccount] PRIMARY KEY CLUSTERED  ([ClientAccountID])
GO
ALTER TABLE [dbo].[ClientAccount] ADD CONSTRAINT [FK_ClientAccount_AccountUse] FOREIGN KEY ([AccountUseID]) REFERENCES [dbo].[AccountUse] ([AccountUseID])
GO
