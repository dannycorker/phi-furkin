CREATE TABLE [dbo].[LookupListItems]
(
[LookupListItemID] [int] NOT NULL IDENTITY(1, 1),
[LookupListID] [int] NULL,
[ItemValue] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL,
[Enabled] [bit] NOT NULL,
[SortOrder] [int] NOT NULL CONSTRAINT [DF__LookupLis__SortO__72E607DB] DEFAULT ((0)),
[ValueInt] [int] NULL,
[ValueMoney] [money] NULL,
[ValueDate] [date] NULL,
[ValueDateTime] [datetime2] (0) NULL,
[SourceID] [int] NULL,
[IsShared] [bit] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-27
-- Description:	Store DetailValue as specific datatypes
-- =============================================
CREATE TRIGGER [dbo].[trgiu_LookupListItems]
   ON [dbo].[LookupListItems]
   AFTER INSERT, UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END


    /*
		If the DetailValue column has been updated then fire the trigger,
		otherwise we are here again because the trigger has just fired,
		in which case do nothing.
	*/
    IF UPDATE(ItemValue)
    BEGIN
		UPDATE dbo.LookupListItems
		SET ValueInt = dbo.fnValueAsIntBitsIncluded(i.itemvalue) /*CASE WHEN dbo.fnIsInt(i.itemvalue) = 1 THEN convert(int, i.itemvalue) ELSE NULL END*/,
		ValueMoney = CASE WHEN dbo.fnIsMoney(i.itemvalue) = 1 THEN convert(money, i.itemvalue) ELSE NULL END,
		ValueDate = CASE WHEN dbo.fnIsDateTime(i.itemvalue) = 1 THEN convert(date, i.itemvalue) ELSE NULL END,
		ValueDateTime = CASE WHEN dbo.fnIsDateTime(i.itemvalue) = 1 THEN convert(datetime2, i.itemvalue) ELSE NULL END
		FROM dbo.LookupListItems dvh
		INNER JOIN inserted i ON i.LookupListItemID = dvh.LookupListItemID
	END


END

GO
ALTER TABLE [dbo].[LookupListItems] ADD CONSTRAINT [PK_LookupListItems] PRIMARY KEY CLUSTERED  ([LookupListItemID])
GO
CREATE NONCLUSTERED INDEX [IX_LookupListItemsClient2017] ON [dbo].[LookupListItems] ([ClientID]) INCLUDE ([ItemValue])
GO
CREATE NONCLUSTERED INDEX [IX_LookupListItemsClientValueInt2017] ON [dbo].[LookupListItems] ([ClientID], [ValueInt]) INCLUDE ([ItemValue])
GO
CREATE NONCLUSTERED INDEX [IX_LookupListItemsLookupList] ON [dbo].[LookupListItems] ([LookupListID], [ClientID]) INCLUDE ([ItemValue])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_LookupListItems_SourceID] ON [dbo].[LookupListItems] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[LookupListItems] ADD CONSTRAINT [FK_LookupListItems_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[LookupListItems] ADD CONSTRAINT [FK_LookupListItems_LookupList] FOREIGN KEY ([LookupListID]) REFERENCES [dbo].[LookupList] ([LookupListID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
