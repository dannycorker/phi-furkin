CREATE TABLE [dbo].[ExclusionGroupMember]
(
[ExclusionGroupMemberID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ExclusionGroupID] [int] NOT NULL,
[ExclusionID] [int] NOT NULL,
[IsEnabled] [bit] NOT NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[ExclusionGroupMember] ADD CONSTRAINT [PK_ExclusionGroupMember] PRIMARY KEY CLUSTERED  ([ExclusionGroupMemberID])
GO
CREATE NONCLUSTERED INDEX [IX_ExclusionGroupMember_GroupIsEnabled] ON [dbo].[ExclusionGroupMember] ([ExclusionGroupID], [IsEnabled])
GO
CREATE NONCLUSTERED INDEX [IX_ExclusionGroupMember_SourceID] ON [dbo].[ExclusionGroupMember] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[ExclusionGroupMember] ADD CONSTRAINT [FK_ExclusionGroupMember_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ExclusionGroupMember] ADD CONSTRAINT [FK_ExclusionGroupMember_Exclusion] FOREIGN KEY ([ExclusionID]) REFERENCES [dbo].[Exclusion] ([ExclusionID])
GO
ALTER TABLE [dbo].[ExclusionGroupMember] ADD CONSTRAINT [FK_ExclusionGroupMember_ExclusionGroup] FOREIGN KEY ([ExclusionGroupID]) REFERENCES [dbo].[ExclusionGroup] ([ExclusionGroupID])
GO
