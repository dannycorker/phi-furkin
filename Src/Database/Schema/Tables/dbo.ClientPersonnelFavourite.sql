CREATE TABLE [dbo].[ClientPersonnelFavourite]
(
[ClientPersonnelFavouriteID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientPersonnelID] [int] NOT NULL,
[LeadID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ClientPersonnelFavourite] ADD CONSTRAINT [PK_ClientPersonnelFavourite] PRIMARY KEY CLUSTERED  ([ClientPersonnelFavouriteID])
GO
ALTER TABLE [dbo].[ClientPersonnelFavourite] ADD CONSTRAINT [FK_ClientPersonnelFavourite_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[ClientPersonnelFavourite] ADD CONSTRAINT [FK_ClientPersonnelFavourite_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ClientPersonnelFavourite] ADD CONSTRAINT [FK_ClientPersonnelFavourite_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID])
GO
