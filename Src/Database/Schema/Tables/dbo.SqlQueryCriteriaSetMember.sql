CREATE TABLE [dbo].[SqlQueryCriteriaSetMember]
(
[SqlQueryCriteriaSetMemberID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SqlQueryID] [int] NOT NULL,
[SqlQueryCriteriaSetID] [int] NOT NULL,
[SqlQueryColumnID] [int] NOT NULL,
[Criteria1] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Criteria2] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SqlQueryCriteriaSetMember] ADD CONSTRAINT [PK_SqlQueryCriteriaSetMember] PRIMARY KEY CLUSTERED  ([SqlQueryCriteriaSetMemberID])
GO
CREATE NONCLUSTERED INDEX [IX_SqlQueryCriteriaSetMember_Query_Criteria] ON [dbo].[SqlQueryCriteriaSetMember] ([SqlQueryID], [SqlQueryCriteriaSetID])
GO
ALTER TABLE [dbo].[SqlQueryCriteriaSetMember] ADD CONSTRAINT [FK_SqlQueryCriteriaSetMember_SqlQuery] FOREIGN KEY ([SqlQueryID]) REFERENCES [dbo].[SqlQuery] ([QueryID]) ON DELETE CASCADE
GO
