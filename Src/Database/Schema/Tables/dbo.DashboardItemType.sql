CREATE TABLE [dbo].[DashboardItemType]
(
[DashboardItemTypeID] [int] NOT NULL IDENTITY(1, 1),
[DashboardItemTypeName] [int] NULL,
[ClientID] [int] NULL
)
GO
ALTER TABLE [dbo].[DashboardItemType] ADD CONSTRAINT [PK_DashboardItemType] PRIMARY KEY CLUSTERED  ([DashboardItemTypeID])
GO
ALTER TABLE [dbo].[DashboardItemType] ADD CONSTRAINT [FK_DashboardItemType_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
