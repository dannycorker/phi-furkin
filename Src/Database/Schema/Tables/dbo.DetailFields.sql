CREATE TABLE [dbo].[DetailFields]
(
[DetailFieldID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadOrMatter] [tinyint] NOT NULL CONSTRAINT [DF__DetailFie__LeadO__536D5C82] DEFAULT ((1)),
[FieldName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[FieldCaption] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[QuestionTypeID] [int] NOT NULL,
[Required] [bit] NOT NULL,
[Lookup] [bit] NOT NULL,
[LookupListID] [int] NULL,
[LeadTypeID] [int] NOT NULL,
[Enabled] [bit] NULL,
[DetailFieldPageID] [int] NOT NULL,
[FieldOrder] [int] NULL,
[MaintainHistory] [bit] NOT NULL,
[EquationText] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[MasterQuestionID] [int] NULL,
[FieldSize] [int] NULL,
[LinkedDetailFieldID] [int] NULL,
[ValidationCriteriaFieldTypeID] [int] NULL,
[ValidationCriteriaID] [int] NULL,
[MinimumValue] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MaximumValue] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[RegEx] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ErrorMessage] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ResourceListDetailFieldPageID] [int] NULL,
[TableDetailFieldPageID] [int] NULL,
[DefaultFilter] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ColumnEquationText] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Editable] [bit] NULL,
[Hidden] [bit] NULL,
[LastReferenceInteger] [int] NULL,
[ReferenceValueFormatID] [int] NULL,
[Encrypt] [bit] NULL,
[ShowCharacters] [int] NULL,
[NumberOfCharactersToShow] [int] NULL,
[TableEditMode] [int] NULL,
[DisplayInTableView] [bit] NULL,
[ObjectTypeID] [int] NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[DetailFieldStyleID] [int] NULL,
[Hyperlink] [nvarchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[IsShared] [bit] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2008-04-01
-- Description:	Insert/Update trigger for DetailFields
-- =============================================
CREATE TRIGGER [dbo].[trgiu_detailfields]
   ON  [dbo].[DetailFields]
   AFTER INSERT,UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	-- Allow special user 'notriggers' to bypass trigger code
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END

    -- Show which fields use a LookupList (this should be a computed column really (if we even need it at all))
    IF NOT UPDATE([lookup])
	BEGIN
		update detailfields
		set detailfields.[lookup] = CASE WHEN detailfields.lookuplistid > -1 THEN 1 ELSE 0 END
		from inserted i
		where detailfields.detailfieldid = i.detailfieldid
		and detailfields.[lookup] <> CASE WHEN detailfields.lookuplistid > -1 THEN 1 ELSE 0 END
	END
END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2009-04-06
-- Description:	Capture changes and store in History table
-- =============================================
CREATE TRIGGER [dbo].[trgiud_DetailFields]
   ON  [dbo].[DetailFields]
   AFTER INSERT,DELETE,UPDATE
AS
BEGIN
	SET NOCOUNT ON;

    -- Inserts appear in the inserted table,
    -- Deletes appear in the deleted table,
    -- and Updates appear in both tables at once identically.
    -- These are all 'logical' Sql Server tables.
	INSERT INTO dbo.DetailFieldsHistory(
		DetailFieldID,
		ClientID,
		LeadOrMatter,
		FieldName,
		FieldCaption,
		QuestionTypeID,
		[Required],
		[Lookup],
		LookupListID,
		LeadTypeID,
		[Enabled],
		DetailFieldPageID,
		FieldOrder,
		MaintainHistory,
		EquationText,
		MasterQuestionID,
		FieldSize,
		LinkedDetailFieldID,
		ValidationCriteriaFieldTypeID,
		ValidationCriteriaID,
		MinimumValue,
		MaximumValue,
		RegEx,
		ErrorMessage,
		ResourceListDetailFieldPageID,
		TableDetailFieldPageID,
		DefaultFilter,
		ColumnEquationText,
		Editable,
		Hidden,
		LastReferenceInteger,
		ReferenceValueFormatID,
		Encrypt,
		ShowCharacters,
		NumberOfCharactersToShow,
		TableEditMode,
		DisplayInTableView,
		WhoChanged,
		WhenChanged,
		ChangeAction,
		ChangeNotes,
		ObjectTypeID
	)
	SELECT
		COALESCE(i.DetailFieldID, d.DetailFieldID),
		COALESCE(i.ClientID, d.ClientID),
		COALESCE(i.LeadOrMatter, d.LeadOrMatter),
		COALESCE(i.FieldName, d.FieldName),
		COALESCE(i.FieldCaption, d.FieldCaption),
		COALESCE(i.QuestionTypeID, d.QuestionTypeID),
		COALESCE(i.[Required], d.[Required]),
		COALESCE(i.[Lookup], d.[Lookup]),
		COALESCE(i.LookupListID, d.LookupListID),
		COALESCE(i.LeadTypeID, d.LeadTypeID),
		COALESCE(i.[Enabled], d.[Enabled]),
		COALESCE(i.DetailFieldPageID, d.DetailFieldPageID),
		COALESCE(i.FieldOrder, d.FieldOrder),
		COALESCE(i.MaintainHistory, d.MaintainHistory),
		COALESCE(i.EquationText, d.EquationText),
		COALESCE(i.MasterQuestionID, d.MasterQuestionID),
		COALESCE(i.FieldSize, d.FieldSize),
		COALESCE(i.LinkedDetailFieldID, d.LinkedDetailFieldID),
		COALESCE(i.ValidationCriteriaFieldTypeID, d.ValidationCriteriaFieldTypeID),
		COALESCE(i.ValidationCriteriaID, d.ValidationCriteriaID),
		COALESCE(i.MinimumValue, d.MinimumValue),
		COALESCE(i.MaximumValue, d.MaximumValue),
		COALESCE(i.RegEx, d.RegEx),
		COALESCE(i.ErrorMessage, d.ErrorMessage),
		COALESCE(i.ResourceListDetailFieldPageID, d.ResourceListDetailFieldPageID),
		COALESCE(i.TableDetailFieldPageID, d.TableDetailFieldPageID),
		COALESCE(i.DefaultFilter, d.DefaultFilter),
		COALESCE(i.ColumnEquationText, d.ColumnEquationText),
		COALESCE(i.Editable, d.Editable),
		COALESCE(i.Hidden, d.Hidden),
		COALESCE(i.LastReferenceInteger, d.LastReferenceInteger),
		COALESCE(i.ReferenceValueFormatID, d.ReferenceValueFormatID),
		COALESCE(i.Encrypt, d.Encrypt),
		COALESCE(i.ShowCharacters, d.ShowCharacters),
		COALESCE(i.NumberOfCharactersToShow, d.NumberOfCharactersToShow),
		COALESCE(i.TableEditMode, d.TableEditMode),
		COALESCE(i.DisplayInTableView, d.DisplayInTableView),
		COALESCE(i.WhoModified, d.WhoModified),
		dbo.fn_GetDate_Local(),
		CASE WHEN d.DetailFieldID IS NULL THEN 'Insert' WHEN i.DetailFieldID IS NULL THEN 'Delete' ELSE 'Update' END,
		dbo.fnGetUserInfo(),
		COALESCE(i.ObjectTypeID, d.ObjectTypeID)
	FROM inserted i
	FULL OUTER JOIN deleted d ON i.DetailFieldID = d.DetailFieldID

END
GO
ALTER TABLE [dbo].[DetailFields] ADD CONSTRAINT [PK_MatterFields] PRIMARY KEY CLUSTERED  ([DetailFieldID])
GO
CREATE NONCLUSTERED INDEX [IX_DetailFieldsPageID] ON [dbo].[DetailFields] ([DetailFieldPageID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_DetailFieldsFieldName] ON [dbo].[DetailFields] ([FieldName], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_DetailFields_MandatoryandHelperFields] ON [dbo].[DetailFields] ([LeadOrMatter], [Enabled]) INCLUDE ([DetailFieldPageID], [FieldCaption], [FieldOrder], [FieldSize], [LeadTypeID], [QuestionTypeID])
GO
CREATE NONCLUSTERED INDEX [IX_DetailFieldsLeadType] ON [dbo].[DetailFields] ([LeadTypeID], [ClientID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_DetailFields_SourceID] ON [dbo].[DetailFields] ([SourceID]) WHERE ([SourceID]>(0))
GO
CREATE NONCLUSTERED INDEX [IX_DetailFields_TableDetailFieldID] ON [dbo].[DetailFields] ([TableDetailFieldPageID], [ClientID], [Enabled])
GO
ALTER TABLE [dbo].[DetailFields] ADD CONSTRAINT [FK_DetailFields_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DetailFields] ADD CONSTRAINT [FK_DetailFields_DetailFieldPages] FOREIGN KEY ([DetailFieldPageID]) REFERENCES [dbo].[DetailFieldPages] ([DetailFieldPageID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DetailFields] ADD CONSTRAINT [FK_DetailFields_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[DetailFields] ADD CONSTRAINT [FK_DetailFields_DetailFieldStyle] FOREIGN KEY ([DetailFieldStyleID]) REFERENCES [dbo].[DetailFieldStyle] ([DetailFieldStyleID])
GO
ALTER TABLE [dbo].[DetailFields] ADD CONSTRAINT [FK_DetailFields_DetailFieldSubType] FOREIGN KEY ([LeadOrMatter]) REFERENCES [dbo].[DetailFieldSubType] ([DetailFieldSubTypeID])
GO
ALTER TABLE [dbo].[DetailFields] ADD CONSTRAINT [FK_DetailFields_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
ALTER TABLE [dbo].[DetailFields] ADD CONSTRAINT [FK_DetailFields_LookupList] FOREIGN KEY ([LookupListID]) REFERENCES [dbo].[LookupList] ([LookupListID])
GO
ALTER TABLE [dbo].[DetailFields] ADD CONSTRAINT [FK_DetailFields_MasterQuestions] FOREIGN KEY ([MasterQuestionID]) REFERENCES [dbo].[MasterQuestions] ([MasterQuestionID])
GO
ALTER TABLE [dbo].[DetailFields] ADD CONSTRAINT [FK_DetailFields_QuestionTypes] FOREIGN KEY ([QuestionTypeID]) REFERENCES [dbo].[QuestionTypes] ([QuestionTypeID])
GO
