CREATE TABLE [dbo].[AquariumConfigCategory]
(
[CategoryID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[AquariumConfigCategory] ADD CONSTRAINT [PK_AquariumConfigCategory] PRIMARY KEY CLUSTERED  ([CategoryID])
GO
