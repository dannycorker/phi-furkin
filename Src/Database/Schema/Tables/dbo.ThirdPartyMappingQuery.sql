CREATE TABLE [dbo].[ThirdPartyMappingQuery]
(
[ThirdPartyMappingQueryID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ThirdPartySystemID] [int] NOT NULL,
[ThirdPartyFieldGroupID] [int] NOT NULL,
[QueryID] [int] NOT NULL,
[Method] [int] NOT NULL,
[ActionURI] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[SchemaName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[BaseIndex] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ThirdPartyMappingQuery] ADD CONSTRAINT [PK_ThirdPartyMappingQuery] PRIMARY KEY CLUSTERED  ([ThirdPartyMappingQueryID])
GO
ALTER TABLE [dbo].[ThirdPartyMappingQuery] ADD CONSTRAINT [FK_ThirdPartyMappingQuery_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ThirdPartyMappingQuery] ADD CONSTRAINT [FK_ThirdPartyMappingQuery_SqlQuery] FOREIGN KEY ([QueryID]) REFERENCES [dbo].[SqlQuery] ([QueryID])
GO
ALTER TABLE [dbo].[ThirdPartyMappingQuery] ADD CONSTRAINT [FK_ThirdPartyMappingQuery_ThirdPartyFieldGroup] FOREIGN KEY ([ThirdPartyFieldGroupID]) REFERENCES [dbo].[ThirdPartyFieldGroup] ([ThirdPartyFieldGroupID])
GO
ALTER TABLE [dbo].[ThirdPartyMappingQuery] ADD CONSTRAINT [FK_ThirdPartyMappingQuery_ThirdPartySystem] FOREIGN KEY ([ThirdPartySystemID]) REFERENCES [dbo].[ThirdPartySystem] ([ThirdPartySystemId])
GO
