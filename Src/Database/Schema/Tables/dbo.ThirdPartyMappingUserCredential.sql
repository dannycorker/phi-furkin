CREATE TABLE [dbo].[ThirdPartyMappingUserCredential]
(
[ThirdPartyMappingUserCredentialID] [int] NOT NULL IDENTITY(1, 1),
[ThirdPartySystemID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[UserID] [int] NOT NULL,
[UserName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Password] [varchar] (65) COLLATE Latin1_General_CI_AS NOT NULL,
[Salt] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Domain] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[AttemptedLogins] [int] NULL,
[AccountDisabled] [bit] NULL CONSTRAINT [DF__ThirdPart__Accou__10766AC2] DEFAULT ((0)),
[ThirdPartyClientID] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LDAPConfigID] [int] NULL,
[ImpersonateThirdPartySystemID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2015-01-14
-- Description:	#30558 Stop blank LDAP records from being created. Remove unwanted ones.
-- =============================================
CREATE TRIGGER [dbo].[trgiu_ThirdPartyMappingUserCredential]
   ON [dbo].[ThirdPartyMappingUserCredential]
   AFTER INSERT, UPDATE
AS
BEGIN
	SET NOCOUNT ON;

	/* Any record inserted with a blank UserName, or updated to a blank UserName, should be deleted */
	DELETE dbo.[ThirdPartyMappingUserCredential]
	FROM dbo.[ThirdPartyMappingUserCredential] t
	INNER JOIN inserted i ON i.UserID = t.UserID
	WHERE i.UserName = ''

END
GO
ALTER TABLE [dbo].[ThirdPartyMappingUserCredential] ADD CONSTRAINT [PK_ThirdPartyMappingUserCredential] PRIMARY KEY CLUSTERED  ([ThirdPartyMappingUserCredentialID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientID_ThirdPartySystemID] ON [dbo].[ThirdPartyMappingUserCredential] ([ThirdPartySystemID], [ClientID])
GO
ALTER TABLE [dbo].[ThirdPartyMappingUserCredential] ADD CONSTRAINT [FK_ThirdPartyMappingUserCredential_ThirdPartySystem] FOREIGN KEY ([ThirdPartySystemID]) REFERENCES [dbo].[ThirdPartySystem] ([ThirdPartySystemId])
GO
