CREATE TABLE [dbo].[ClientOption]
(
[ClientOptionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ZenDeskOff] [bit] NOT NULL,
[ZenDeskURLPrefix] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ZenDeskText] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[ZenDeskTitle] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[iDiaryOff] [bit] NOT NULL,
[WorkflowOff] [bit] NOT NULL,
[UserDirectoryOff] [bit] NOT NULL,
[eCatcherOff] [bit] NOT NULL,
[LeadAssignmentOff] [bit] NOT NULL,
[UserMessagesOff] [bit] NOT NULL,
[SystemMessagesOff] [bit] NOT NULL,
[UserPortalOff] [bit] NOT NULL,
[ReportSearchOff] [bit] NULL CONSTRAINT [DF__ClientOpt__Repor__3AD6B8E2] DEFAULT ((1)),
[NormalSearchOff] [bit] NULL,
[UseXero] [bit] NULL,
[UseUltra] [bit] NULL,
[UseCaseSummary] [bit] NULL,
[UseMoreProminentReminders] [bit] NULL,
[UseSMSSurvey] [bit] NULL,
[UseNewAddLead] [bit] NULL,
[DiallerInsertMethod] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DiallerPrimaryKey] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[UseCHARMS] [bit] NULL,
[TextMessageService] [int] NULL,
[UseThunderhead] [bit] NULL,
[UseMemorableWordVerification] [bit] NULL,
[ApplyInactivityTimeout] [bit] NULL,
[InactivityTimeoutInSeconds] [int] NULL,
[InactivityRedirectUrl] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[UseSentimentAnalysis] [bit] NULL,
[ShowProcessInfoButton] [bit] NULL,
[LatestRecordsFirst] [bit] NULL,
[UseSecureMessage] [bit] NULL,
[EventCommentTooltipOff] [bit] NULL,
[EnableScripting] [bit] NULL,
[EnableBilling] [bit] NULL,
[UseDocumentTypeVersioning] [bit] NULL,
[EnableSMSSTOP] [bit] NULL,
[UseEngageMail] [bit] NULL,
[PreventForcedLogout] [bit] NULL
)
GO
ALTER TABLE [dbo].[ClientOption] ADD CONSTRAINT [PK_ClientMessage] PRIMARY KEY CLUSTERED  ([ClientOptionID])
GO
ALTER TABLE [dbo].[ClientOption] ADD CONSTRAINT [FK_ClientOption_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
