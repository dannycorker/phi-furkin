CREATE TABLE [dbo].[UserGroupMenuOption]
(
[UserGroupMenuOptionID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelAdminGroupID] [int] NULL,
[ClientID] [int] NOT NULL,
[MasterPageName] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[PanelItemAction] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[PanelItemName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[PanelItemCaption] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PanelItemIcon] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PanelItemURL] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[Comments] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ItemOrder] [int] NOT NULL,
[ShowInFancyBox] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[UserGroupMenuOption] ADD CONSTRAINT [PK_UserGroupMenuOption] PRIMARY KEY CLUSTERED  ([UserGroupMenuOptionID])
GO
CREATE NONCLUSTERED INDEX [IX_UserGroupMenuOption_ClientID] ON [dbo].[UserGroupMenuOption] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_UserGroupMenuOption_ClientPersonnelAdminGroupID] ON [dbo].[UserGroupMenuOption] ([ClientPersonnelAdminGroupID])
GO
CREATE NONCLUSTERED INDEX [IX_UserGroupMenuOption_PageName] ON [dbo].[UserGroupMenuOption] ([MasterPageName])
GO
