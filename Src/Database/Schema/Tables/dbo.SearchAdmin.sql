CREATE TABLE [dbo].[SearchAdmin]
(
[SearchAdminID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[SearchTypeID] [int] NOT NULL,
[ClientPersonnelAdminGroupID] [int] NOT NULL,
[SearchAdminName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[SearchAdmin] ADD CONSTRAINT [PK_SearchTypeAdmin_1] PRIMARY KEY CLUSTERED  ([SearchAdminID])
GO
ALTER TABLE [dbo].[SearchAdmin] ADD CONSTRAINT [FK_SearchAdmin_ClientPersonnelAdminGroups] FOREIGN KEY ([ClientPersonnelAdminGroupID]) REFERENCES [dbo].[ClientPersonnelAdminGroups] ([ClientPersonnelAdminGroupID])
GO
ALTER TABLE [dbo].[SearchAdmin] ADD CONSTRAINT [FK_SearchAdmin_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SearchAdmin] ADD CONSTRAINT [FK_SearchAdmin_SearchType] FOREIGN KEY ([SearchTypeID]) REFERENCES [dbo].[SearchType] ([SearchTypeID])
GO
ALTER TABLE [dbo].[SearchAdmin] ADD CONSTRAINT [FK_SearchAdmin_SubClient] FOREIGN KEY ([SubClientID]) REFERENCES [dbo].[SubClient] ([SubClientID])
GO
