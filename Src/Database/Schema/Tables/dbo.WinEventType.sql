CREATE TABLE [dbo].[WinEventType]
(
[WinEventTypeID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[Flags] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[EventTypeID] [int] NOT NULL,
[MakesAWin] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[WinEventType] ADD CONSTRAINT [PK_WinEventType] PRIMARY KEY CLUSTERED  ([WinEventTypeID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_WinEventType] ON [dbo].[WinEventType] ([ClientID], [LeadTypeID], [Flags], [EventTypeID])
GO
