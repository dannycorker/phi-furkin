CREATE TABLE [dbo].[AccountType]
(
[AccountTypeID] [int] NOT NULL IDENTITY(1, 1),
[AccountType] [varchar] (25) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[AccountType] ADD CONSTRAINT [PK_AccountType] PRIMARY KEY CLUSTERED  ([AccountTypeID])
GO
