CREATE TABLE [dbo].[Location]
(
[LocationID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LocationName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Note] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Enabled] [bit] NOT NULL,
[Deleted] [bit] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [PK__Location__E7FEA4776F01C4E4] PRIMARY KEY CLUSTERED  ([LocationID])
GO
ALTER TABLE [dbo].[Location] ADD CONSTRAINT [FK__Location__Client__70EA0D56] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
