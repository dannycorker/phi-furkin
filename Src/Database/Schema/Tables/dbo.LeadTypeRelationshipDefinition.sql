CREATE TABLE [dbo].[LeadTypeRelationshipDefinition]
(
[LeadTypeRelationshipDefinitionID] [int] NOT NULL IDENTITY(1, 1),
[FromLeadTypeID] [int] NOT NULL,
[ToLeadTypeID] [int] NOT NULL,
[LeadsAreLinked] [bit] NOT NULL CONSTRAINT [DF__LeadTypeR__Leads__70FDBF69] DEFAULT ((0)),
[CasesAreLinked] [bit] NOT NULL CONSTRAINT [DF__LeadTypeR__Cases__70099B30] DEFAULT ((0)),
[MattersAreLinked] [bit] NOT NULL CONSTRAINT [DF__LeadTypeR__Matte__71F1E3A2] DEFAULT ((0)),
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[LeadTypeRelationshipDefinition] ADD CONSTRAINT [PK_LeadTypeRelationshipDefinition] PRIMARY KEY CLUSTERED  ([LeadTypeRelationshipDefinitionID])
GO
