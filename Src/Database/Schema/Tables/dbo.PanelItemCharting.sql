CREATE TABLE [dbo].[PanelItemCharting]
(
[PanelItemChartingID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[PanelItemChartingName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[PanelID] [int] NOT NULL,
[CreatedBy] [int] NOT NULL,
[ChartID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[PanelItemCharting] ADD CONSTRAINT [PK_PanelItemCharting] PRIMARY KEY CLUSTERED  ([PanelItemChartingID])
GO
ALTER TABLE [dbo].[PanelItemCharting] ADD CONSTRAINT [FK_PanelItemCharting_Chart] FOREIGN KEY ([ChartID]) REFERENCES [dbo].[Chart] ([ChartID])
GO
ALTER TABLE [dbo].[PanelItemCharting] ADD CONSTRAINT [FK_PanelItemCharting_ClientPersonnel] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[PanelItemCharting] ADD CONSTRAINT [FK_PanelItemCharting_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[PanelItemCharting] ADD CONSTRAINT [FK_PanelItemCharting_Panels] FOREIGN KEY ([PanelID]) REFERENCES [dbo].[Panels] ([PanelID]) ON DELETE CASCADE
GO
