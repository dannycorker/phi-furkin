CREATE TABLE [dbo].[ClientSmsAccountNumber]
(
[ClientSmsAccountNumberID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ResourceListID] [int] NULL,
[PhoneNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[ClientSmsAccountNumber] ADD CONSTRAINT [PK_ClientSmsAccountNumber] PRIMARY KEY CLUSTERED  ([ClientSmsAccountNumberID])
GO
