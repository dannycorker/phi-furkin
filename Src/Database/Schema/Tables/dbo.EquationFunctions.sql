CREATE TABLE [dbo].[EquationFunctions]
(
[FunctionID] [int] NOT NULL IDENTITY(1, 1),
[FunctionName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[EquationFunctions] ADD CONSTRAINT [PK_EquationFunctions] PRIMARY KEY CLUSTERED  ([FunctionID])
GO
