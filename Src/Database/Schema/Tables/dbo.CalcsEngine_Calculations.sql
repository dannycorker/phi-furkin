CREATE TABLE [dbo].[CalcsEngine_Calculations]
(
[CalculationID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[ClientID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[Description] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CalcsEngine_Calculations] ADD CONSTRAINT [PK_CalcsEngine_Calculations] PRIMARY KEY CLUSTERED  ([CalculationID])
GO
