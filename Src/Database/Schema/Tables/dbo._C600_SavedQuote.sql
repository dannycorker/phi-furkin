CREATE TABLE [dbo].[_C600_SavedQuote]
(
[SavedQuoteID] [int] NOT NULL IDENTITY(1, 1),
[SavedQuoteKey] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL CONSTRAINT [DF___C327_Sav__Saved__320C68B7] DEFAULT (newid()),
[SavedQuoteXML] [xml] NOT NULL,
[EmailAddress] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[SecretQuestionID] [int] NOT NULL,
[SecretQuestionValue] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL CONSTRAINT [DF___C600_Sav__WhenC__32AA85E5] DEFAULT ([dbo].[fn_GetDate_Local]()),
[WhenExpire] [datetime] NOT NULL CONSTRAINT [DF___C600_Sav__WhenE__339EAA1E] DEFAULT ([dbo].[fn_GetDate_Local]()+(30)),
[AggregatorID] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[QuoteSessionID] [int] NULL CONSTRAINT [DF__C384_SavedQuote_QuoteSessionID] DEFAULT ((0)),
[RetrievedDate] [datetime] NULL CONSTRAINT [DF__C384_SavedQuote_RetrievedDate] DEFAULT ([dbo].[fn_GetDate_Local]()+(30)),
[PetQuoteDefinitionXml] [xml] NULL CONSTRAINT [DF___C600_Sav__PetQu__1A37412B] DEFAULT (NULL),
[EmailQueued] [bit] NULL,
[AggregatorClickThrough] [bit] NULL,
[ClickThroughDate] [datetime] NULL,
[AggregatorRef] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[IsExternal] [bit] NULL
)
GO
ALTER TABLE [dbo].[_C600_SavedQuote] ADD CONSTRAINT [PK___C327_Sa__083218DD6BA66F74] PRIMARY KEY CLUSTERED  ([SavedQuoteID])
GO
CREATE NONCLUSTERED INDEX [IX_C433_SavedQuote_QuoteSession] ON [dbo].[_C600_SavedQuote] ([QuoteSessionID])
GO
CREATE NONCLUSTERED INDEX [IX_C433_SavedQuote_SavedQuoteKey] ON [dbo].[_C600_SavedQuote] ([SavedQuoteKey]) INCLUDE ([SavedQuoteID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Allows us to flag when a QuoteEmail has been queued to be sent but hasn''t yet been sent', 'SCHEMA', N'dbo', 'TABLE', N'_C600_SavedQuote', 'COLUMN', N'EmailQueued'
GO
