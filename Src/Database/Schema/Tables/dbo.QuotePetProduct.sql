CREATE TABLE [dbo].[QuotePetProduct]
(
[QuotePetProductID] [int] NOT NULL IDENTITY(1, 1),
[QuoteSessionID] [int] NULL,
[QuoteID] [int] NULL,
[QuotePetID] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoCreated] [int] NULL,
[SourceID] [int] NULL,
[ChangeNotes] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ProductID] [int] NULL,
[ProductName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[RuleSetID] [int] NULL,
[RulesEngineOutcome] [decimal] (18, 2) NULL,
[EvaluatedXml] [xml] NULL,
[ParentQuotePetProductID] [int] NULL,
[AbControllerRuleSetID] [int] NULL,
[AlternativeRuleSetID] [int] NULL,
[Excess] [decimal] (18, 2) NULL
)
GO
ALTER TABLE [dbo].[QuotePetProduct] ADD CONSTRAINT [PK_QuotePetProduct] PRIMARY KEY CLUSTERED  ([QuotePetProductID])
GO
CREATE NONCLUSTERED INDEX [IX_QuotePetProduct_QuotePetQuoteQuoteSession] ON [dbo].[QuotePetProduct] ([QuotePetID], [QuoteID], [QuoteSessionID], [QuotePetProductID])
GO
CREATE NONCLUSTERED INDEX [IX_QuotePetProduct_WhenCreated_Ruleset] ON [dbo].[QuotePetProduct] ([WhenCreated]) INCLUDE ([QuotePetProductID], [RuleSetID])
GO
ALTER TABLE [dbo].[QuotePetProduct] ADD CONSTRAINT [FK_QuotePetProduct_Quote] FOREIGN KEY ([QuoteID]) REFERENCES [dbo].[Quote] ([QuoteID])
GO
