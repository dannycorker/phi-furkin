CREATE TABLE [dbo].[StyleRule]
(
[StyleRuleID] [int] NOT NULL IDENTITY(1, 1),
[RuleClass] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[RuleDescription] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[RuleValue] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[AquariumOnly] [bit] NOT NULL,
[IncludeLinkText] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[StyleRule] ADD CONSTRAINT [PK_StyleRule] PRIMARY KEY CLUSTERED  ([StyleRuleID])
GO
