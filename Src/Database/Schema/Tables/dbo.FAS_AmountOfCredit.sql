CREATE TABLE [dbo].[FAS_AmountOfCredit]
(
[FasAmountOfCreditID] [int] NOT NULL IDENTITY(1, 1),
[FasCheckerDataID] [int] NOT NULL,
[Description] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[CostAmount] [decimal] (19, 4) NULL
)
GO
ALTER TABLE [dbo].[FAS_AmountOfCredit] ADD CONSTRAINT [PK_FAS_AmountOfCredit] PRIMARY KEY CLUSTERED  ([FasAmountOfCreditID])
GO
ALTER TABLE [dbo].[FAS_AmountOfCredit] ADD CONSTRAINT [FK_FAS_AmountOfCredit_FAS_CheckerData] FOREIGN KEY ([FasCheckerDataID]) REFERENCES [dbo].[FAS_CheckerData] ([FasCheckerDataID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
