CREATE TABLE [dbo].[_C605_RulesEngine_ChangeLogEffectiveDate]
(
[ChangeLogEffectiveDateID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CountryID] [int] NOT NULL CONSTRAINT [DF___C605_Rul__Count__59C45306] DEFAULT ((233)),
[State] [varchar] (6) COLLATE Latin1_General_CI_AS NULL,
[TrendDate] [date] NULL,
[ValidFrom] [date] NULL,
[ValidTo] [date] NULL
)
GO
ALTER TABLE [dbo].[_C605_RulesEngine_ChangeLogEffectiveDate] ADD CONSTRAINT [PK_ChangeLogEffectiveDateID] PRIMARY KEY CLUSTERED  ([ChangeLogEffectiveDateID])
GO
ALTER TABLE [dbo].[_C605_RulesEngine_ChangeLogEffectiveDate] ADD CONSTRAINT [FK__C605_RulesEngine_ChangeLogEffectiveDate_ClientID] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[_C605_RulesEngine_ChangeLogEffectiveDate] ADD CONSTRAINT [FK__C605_RulesEngine_ChangeLogEffectiveDate_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
