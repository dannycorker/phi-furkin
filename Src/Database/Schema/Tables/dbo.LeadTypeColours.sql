CREATE TABLE [dbo].[LeadTypeColours]
(
[LeadTypeColourID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[Colour] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
