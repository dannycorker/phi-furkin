CREATE TABLE [dbo].[LeadTypePageTitleTargetControl]
(
[LeadTypePageTitleTargetControlID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[LastParsed] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[LeadTypePageTitleTargetControl] ADD CONSTRAINT [PK_LeadTypePageTitleTargetControl] PRIMARY KEY CLUSTERED  ([LeadTypePageTitleTargetControlID])
GO
ALTER TABLE [dbo].[LeadTypePageTitleTargetControl] ADD CONSTRAINT [FK_LeadTypePageTitleTargetControl_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[LeadTypePageTitleTargetControl] ADD CONSTRAINT [FK_LeadTypePageTitleTargetControl_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID]) ON DELETE CASCADE
GO
