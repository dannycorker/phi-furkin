CREATE TABLE [dbo].[OneVisionClaim]
(
[OneVisionClaimID] [int] NOT NULL IDENTITY(1, 1),
[VisionClaimID] [int] NOT NULL,
[VisionPolicyTermID] [int] NOT NULL,
[HistoricalPolicyTableRow] [int] NOT NULL,
[PALeadID] [int] NOT NULL,
[PAMatterID] [int] NOT NULL,
[ClaimAmount] [money] NULL,
[AccountID] [int] NULL,
[AccountNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SortCode] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[CustomerPaymentScheduleID] [int] NULL,
[CustomerLedgerID] [int] NULL,
[TreatmentStartDate] [datetime] NULL,
[TreatmentEndDate] [datetime] NULL
)
GO
ALTER TABLE [dbo].[OneVisionClaim] ADD CONSTRAINT [PK_OneVisionClaim] PRIMARY KEY CLUSTERED  ([OneVisionClaimID])
GO
ALTER TABLE [dbo].[OneVisionClaim] ADD CONSTRAINT [FK_OneVisionClaim_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
GO
ALTER TABLE [dbo].[OneVisionClaim] ADD CONSTRAINT [FK_OneVisionClaim_CustomerLedger] FOREIGN KEY ([CustomerLedgerID]) REFERENCES [dbo].[CustomerLedger] ([CustomerLedgerID])
GO
ALTER TABLE [dbo].[OneVisionClaim] ADD CONSTRAINT [FK_OneVisionClaim_CustomerPaymentSchedule] FOREIGN KEY ([CustomerPaymentScheduleID]) REFERENCES [dbo].[CustomerPaymentSchedule] ([CustomerPaymentScheduleID])
GO
ALTER TABLE [dbo].[OneVisionClaim] ADD CONSTRAINT [FK_OneVisionClaim_Lead] FOREIGN KEY ([PALeadID]) REFERENCES [dbo].[Lead] ([LeadID])
GO
ALTER TABLE [dbo].[OneVisionClaim] ADD CONSTRAINT [FK_OneVisionClaim_Matter] FOREIGN KEY ([PAMatterID]) REFERENCES [dbo].[Matter] ([MatterID])
GO
GRANT UPDATE ON  [dbo].[OneVisionClaim] TO [AquariumNet]
GO
