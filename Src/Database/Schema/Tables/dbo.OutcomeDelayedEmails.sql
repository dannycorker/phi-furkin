CREATE TABLE [dbo].[OutcomeDelayedEmails]
(
[OutcomeDelayedEmailID] [int] NOT NULL IDENTITY(1, 1),
[CustomerID] [int] NULL,
[ClientQuestionnaireID] [int] NULL,
[DateOutcomeEmailSent] [datetime] NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[OutcomeDelayedEmails] ADD CONSTRAINT [PK_OutcomeDelayedEmails] PRIMARY KEY CLUSTERED  ([OutcomeDelayedEmailID])
GO
ALTER TABLE [dbo].[OutcomeDelayedEmails] ADD CONSTRAINT [FK_OutcomeDelayedEmails_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
