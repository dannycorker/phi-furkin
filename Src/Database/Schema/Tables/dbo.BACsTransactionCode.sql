CREATE TABLE [dbo].[BACsTransactionCode]
(
[BACsTransactionCodeID] [int] NOT NULL IDENTITY(1, 1),
[Code] [varchar] (2) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[BACsTransactionCode] ADD CONSTRAINT [PK_BACsTransactionCode] PRIMARY KEY CLUSTERED  ([BACsTransactionCodeID])
GO
