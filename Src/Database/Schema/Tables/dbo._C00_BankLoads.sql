CREATE TABLE [dbo].[_C00_BankLoads]
(
[CEPPID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LogFilename] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[LineNum] [int] NULL,
[Text] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Processed] [bit] NULL,
[ISDuplicate] [bit] NULL,
[DuplicateOf] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[Passed] [int] NULL,
[MatterID] [int] NULL,
[TableRowID] [int] NULL,
[DateLoaded] [datetime] NULL,
[Value] [money] NULL,
[Reference] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[TransactionRef] [varchar] (5) COLLATE Latin1_General_CI_AS NULL,
[SubClientID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-05-21
-- Description:	Uncommented
-- =============================================
CREATE TRIGGER [dbo].[trgi__C00_BankLoads]
   ON  [dbo].[_C00_BankLoads]
   AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	-- Allow special user 'notriggers' to bypass trigger code
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END

	DECLARE @CEPPID int, @LogFilename varchar(255), @LineNum int, @Text varchar(255)

	Declare @CEPIDDupe int, @Value money

	Select @CEPPID = i.CEPPID, @LogFilename = i.LogFilename, @LineNum = i.LineNum, @Text = i.TEXT
	From inserted i

	IF (LEFT(@TEXT,3) NOT IN ('VOL','HDR','EOF','UTL','UHL'))
	BEGIN

		Select @Value = CONVERT(MONEY,SUBSTRING(@Text,36,11))/100

		Update _C00_BankLoads
		SET Value = @Value, TransactionRef = SUBSTRING(@Text,16,2),
			Reference = CASE WHEN RTRIM(SUBSTRING([Text],65,18)) != '' THEN RTRIM(SUBSTRING([Text],65,18)) ELSE RTRIM(SUBSTRING([Text],47,18)) END
		FROM _C00_BankLoads cepp
		Where cepp.CEPPID = @CEPPID

	END


	Select Top 1 @CEPIDDupe = cepp.CEPPID
	FROM _C00_BankLoads cepp
	WHERE Text = @TEXT
	AND LineNum = @LineNum
	AND LogFilename = @LogFilename
	AND CEPPID <> @CEPPID
	ORDER BY CEPPID DESC

	IF @CEPIDDupe IS NOT NULL
	BEGIN

		UPDATE _C00_BankLoads SET ISDuplicate = 1, DuplicateOf = @CEPIDDupe
		WHERE CEPPID = @CEPPID

	END

END
GO
ALTER TABLE [dbo].[_C00_BankLoads] ADD CONSTRAINT [PK__C00_BankLoads] PRIMARY KEY CLUSTERED  ([CEPPID])
GO
GRANT INSERT ON  [dbo].[_C00_BankLoads] TO [AquariumNet]
GO
