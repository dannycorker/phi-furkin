CREATE TABLE [dbo].[EventTypeHelperField]
(
[EventTypeHelperFieldID] [int] NOT NULL IDENTITY(1, 1),
[EventTypeID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[ForceEditableOverride] [bit] NULL
)
GO
ALTER TABLE [dbo].[EventTypeHelperField] ADD CONSTRAINT [PK_EventTypeHelperField] PRIMARY KEY CLUSTERED  ([EventTypeHelperFieldID])
GO
ALTER TABLE [dbo].[EventTypeHelperField] ADD CONSTRAINT [FK_EventTypeHelperField_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EventTypeHelperField] ADD CONSTRAINT [FK_EventTypeHelperField_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EventTypeHelperField] ADD CONSTRAINT [FK_EventTypeHelperField_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[EventTypeHelperField] ADD CONSTRAINT [FK_EventTypeHelperField_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
