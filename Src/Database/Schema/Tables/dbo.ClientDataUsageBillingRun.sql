CREATE TABLE [dbo].[ClientDataUsageBillingRun]
(
[ClientDataUsageBillingRunID] [int] NOT NULL IDENTITY(1, 1),
[WhoStarted] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenStarted] [datetime2] (0) NOT NULL,
[WhenFinished] [datetime2] (0) NULL,
[TimeInSeconds] [int] NULL,
[CurrentStage] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[UserNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ClientDataUsageBillingRun] ADD CONSTRAINT [PK_ClientDataUsageBillingRun] PRIMARY KEY CLUSTERED  ([ClientDataUsageBillingRunID])
GO
