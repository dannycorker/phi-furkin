CREATE TABLE [dbo].[SubQueryLinkage]
(
[SubQueryLinkageID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubQueryID] [int] NOT NULL,
[LinksToTableName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LinksToColumnName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LinkageDataType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ParamTableName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ParamColumnName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ParamColumnDataType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ParamColumnHelperSql] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[LeadTypeFilterSql] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SubQueryLinkage] ADD CONSTRAINT [PK_SubQueryLinkage] PRIMARY KEY CLUSTERED  ([SubQueryLinkageID])
GO
ALTER TABLE [dbo].[SubQueryLinkage] ADD CONSTRAINT [FK_SubQueryLinkage_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SubQueryLinkage] ADD CONSTRAINT [FK_SubQueryLinkage_SubQuery] FOREIGN KEY ([SubQueryID]) REFERENCES [dbo].[SubQuery] ([SubQueryID])
GO
