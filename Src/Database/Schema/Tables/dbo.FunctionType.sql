CREATE TABLE [dbo].[FunctionType]
(
[FunctionTypeID] [int] NOT NULL,
[ModuleID] [int] NOT NULL,
[ParentFunctionTypeID] [int] NULL,
[FunctionTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Level] [int] NOT NULL,
[IsVirtual] [bit] NOT NULL,
[WhenCreated] [datetime] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[FunctionType] ADD CONSTRAINT [PK_FunctionType] PRIMARY KEY CLUSTERED  ([FunctionTypeID])
GO
ALTER TABLE [dbo].[FunctionType] ADD CONSTRAINT [FK_FunctionType_FunctionType] FOREIGN KEY ([ParentFunctionTypeID]) REFERENCES [dbo].[FunctionType] ([FunctionTypeID])
GO
ALTER TABLE [dbo].[FunctionType] ADD CONSTRAINT [FK_FunctionType_Module] FOREIGN KEY ([ModuleID]) REFERENCES [dbo].[Module] ([ModuleID])
GO
