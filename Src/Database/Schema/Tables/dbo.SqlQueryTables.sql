CREATE TABLE [dbo].[SqlQueryTables]
(
[SqlQueryTableID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SqlQueryID] [int] NOT NULL,
[SqlQueryTableName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[TableAlias] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[TableDisplayOrder] [int] NOT NULL,
[JoinType] [varchar] (25) COLLATE Latin1_General_CI_AS NULL,
[JoinText] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[JoinTableID] [int] NULL,
[JoinRTRID] [int] NULL,
[TempTableID] [int] NULL,
[TempJoinTableID] [int] NULL,
[SourceID] [int] NULL
)
GO
ALTER TABLE [dbo].[SqlQueryTables] ADD CONSTRAINT [PK_SqlQueryTables] PRIMARY KEY CLUSTERED  ([SqlQueryTableID])
GO
CREATE NONCLUSTERED INDEX [IX_SqlQueryTables_SourceID] ON [dbo].[SqlQueryTables] ([SourceID]) WHERE ([SourceID]>(0))
GO
CREATE NONCLUSTERED INDEX [IX_SqlQueryTables_Query] ON [dbo].[SqlQueryTables] ([SqlQueryID])
GO
