CREATE TABLE [dbo].[PortalUserGroupQuestionnaireAccess]
(
[PortalUserGroupQuestionnaireAccessID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[PortalUserGroupID] [int] NOT NULL,
[ClientQuestionnaireID] [int] NOT NULL,
[HasAccess] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[PortalUserGroupQuestionnaireAccess] ADD CONSTRAINT [PK_PortalUserGroupQuestionnaireAccess] PRIMARY KEY CLUSTERED  ([PortalUserGroupQuestionnaireAccessID])
GO
ALTER TABLE [dbo].[PortalUserGroupQuestionnaireAccess] ADD CONSTRAINT [FK_PortalUserGroupQuestionnaireAccess_ClientQuestionnaires] FOREIGN KEY ([ClientQuestionnaireID]) REFERENCES [dbo].[ClientQuestionnaires] ([ClientQuestionnaireID])
GO
ALTER TABLE [dbo].[PortalUserGroupQuestionnaireAccess] ADD CONSTRAINT [FK_PortalUserGroupQuestionnaireAccess_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[PortalUserGroupQuestionnaireAccess] ADD CONSTRAINT [FK_PortalUserGroupQuestionnaireAccess_PortalUserGroup] FOREIGN KEY ([PortalUserGroupID]) REFERENCES [dbo].[PortalUserGroup] ([PortalUserGroupID])
GO
