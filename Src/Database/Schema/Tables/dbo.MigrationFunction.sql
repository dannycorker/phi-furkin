CREATE TABLE [dbo].[MigrationFunction]
(
[MigrationFunctionID] [int] NOT NULL IDENTITY(1, 1),
[FunctionName] [varchar] (1024) COLLATE Latin1_General_CI_AS NOT NULL,
[DropText] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[ClientID] [int] NOT NULL,
[IsComplete] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[MigrationFunction] ADD CONSTRAINT [PK_MigrationFunction] PRIMARY KEY CLUSTERED  ([MigrationFunctionID])
GO
