CREATE TABLE [dbo].[QuillConfigurationAquarium]
(
[QuillConfigurationAquariumID] [int] NOT NULL IDENTITY(1, 1),
[Vendor] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[APIKey] [uniqueidentifier] NOT NULL
)
GO
ALTER TABLE [dbo].[QuillConfigurationAquarium] ADD CONSTRAINT [PK_QuillConfigurationAquarium] PRIMARY KEY CLUSTERED  ([QuillConfigurationAquariumID])
GO
