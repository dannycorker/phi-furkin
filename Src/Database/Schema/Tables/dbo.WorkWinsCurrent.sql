CREATE TABLE [dbo].[WorkWinsCurrent]
(
[WorkWinsCurrentID] [int] NOT NULL IDENTITY(1, 1),
[WorkWinSeedID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[CaseID] [int] NOT NULL,
[MatterID] [int] NOT NULL,
[WinAmount] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[OfferAmount] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[WinCount] [int] NULL,
[FirstValidSave] [datetime] NULL,
[LastValidSave] [datetime] NULL
)
GO
ALTER TABLE [dbo].[WorkWinsCurrent] ADD CONSTRAINT [PK_WorkWinsCurrent] PRIMARY KEY CLUSTERED  ([WorkWinsCurrentID])
GO
CREATE NONCLUSTERED INDEX [IX_WorkWinsCurrent] ON [dbo].[WorkWinsCurrent] ([WorkWinSeedID], [CaseID])
GO
ALTER TABLE [dbo].[WorkWinsCurrent] ADD CONSTRAINT [FK_WorkWinsCurrent_WorkWinSeed] FOREIGN KEY ([WorkWinSeedID]) REFERENCES [dbo].[WorkWinSeed] ([WorkWinSeedID]) ON DELETE CASCADE
GO
