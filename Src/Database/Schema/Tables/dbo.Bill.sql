CREATE TABLE [dbo].[Bill]
(
[BillID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[Reference] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL,
[DiaryAppointmentID] [int] NULL,
[CustomerID] [int] NULL,
[LeadID] [int] NULL,
[CaseID] [int] NULL,
[BillStatusID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[Bill] ADD CONSTRAINT [PK_Bill] PRIMARY KEY CLUSTERED  ([BillID])
GO
ALTER TABLE [dbo].[Bill] ADD CONSTRAINT [FK_Bill_BillStatus] FOREIGN KEY ([BillStatusID]) REFERENCES [dbo].[BillStatus] ([BillStatusID])
GO
ALTER TABLE [dbo].[Bill] ADD CONSTRAINT [FK_Bill_Cases] FOREIGN KEY ([CaseID]) REFERENCES [dbo].[Cases] ([CaseID])
GO
ALTER TABLE [dbo].[Bill] ADD CONSTRAINT [FK_Bill_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[Bill] ADD CONSTRAINT [FK_Bill_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[Bill] ADD CONSTRAINT [FK_Bill_DiaryAppointment] FOREIGN KEY ([DiaryAppointmentID]) REFERENCES [dbo].[DiaryAppointment] ([DiaryAppointmentID])
GO
ALTER TABLE [dbo].[Bill] ADD CONSTRAINT [FK_Bill_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID])
GO
