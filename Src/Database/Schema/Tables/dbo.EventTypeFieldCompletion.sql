CREATE TABLE [dbo].[EventTypeFieldCompletion]
(
[EventTypeFieldCompletionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[InsertOptionID] [int] NOT NULL,
[DeleteOptionID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[EventTypeFieldCompletion] ADD CONSTRAINT [PK_EventTypeFieldCompletion] PRIMARY KEY CLUSTERED  ([EventTypeFieldCompletionID])
GO
ALTER TABLE [dbo].[EventTypeFieldCompletion] ADD CONSTRAINT [FK_EventTypeFieldCompletion_AquariumOption] FOREIGN KEY ([InsertOptionID]) REFERENCES [dbo].[AquariumOption] ([AquariumOptionID])
GO
ALTER TABLE [dbo].[EventTypeFieldCompletion] ADD CONSTRAINT [FK_EventTypeFieldCompletion_AquariumOption1] FOREIGN KEY ([DeleteOptionID]) REFERENCES [dbo].[AquariumOption] ([AquariumOptionID])
GO
ALTER TABLE [dbo].[EventTypeFieldCompletion] ADD CONSTRAINT [FK_EventTypeFieldCompletion_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EventTypeFieldCompletion] ADD CONSTRAINT [FK_EventTypeFieldCompletion_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[EventTypeFieldCompletion] ADD CONSTRAINT [FK_EventTypeFieldCompletion_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
