CREATE TABLE [dbo].[DetailFieldSubType]
(
[DetailFieldSubTypeID] [tinyint] NOT NULL,
[DetailFieldSubTypeName] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[DetailFieldSubTypeDescription] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[WhenCreated] [datetime] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[DetailFieldSubType] ADD CONSTRAINT [PK_DetailFieldSubType] PRIMARY KEY CLUSTERED  ([DetailFieldSubTypeID])
GO
