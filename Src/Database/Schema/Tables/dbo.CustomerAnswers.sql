CREATE TABLE [dbo].[CustomerAnswers]
(
[CustomerAnswerID] [int] NOT NULL IDENTITY(1, 1),
[CustomerQuestionnaireID] [int] NULL,
[MasterQuestionID] [int] NULL,
[Answer] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[QuestionPossibleAnswerID] [int] NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[CustomerAnswers] ADD CONSTRAINT [PK_CustomerAnswers1] PRIMARY KEY CLUSTERED  ([CustomerAnswerID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerAnswersCustomerQuestionnaireID] ON [dbo].[CustomerAnswers] ([CustomerQuestionnaireID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerAnswersMasterQuestion] ON [dbo].[CustomerAnswers] ([MasterQuestionID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerAnswersQPA] ON [dbo].[CustomerAnswers] ([QuestionPossibleAnswerID])
GO
ALTER TABLE [dbo].[CustomerAnswers] ADD CONSTRAINT [FK_CustomerAnswers_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[CustomerAnswers] ADD CONSTRAINT [FK_CustomerAnswers_CustomerQuestionnaires] FOREIGN KEY ([CustomerQuestionnaireID]) REFERENCES [dbo].[CustomerQuestionnaires] ([CustomerQuestionnaireID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
