CREATE TABLE [dbo].[FAS_FeesAndCharges]
(
[FasFeesAndChargesID] [int] NOT NULL IDENTITY(1, 1),
[FasCheckerDataID] [int] NOT NULL,
[Description] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[Amount] [decimal] (18, 0) NULL,
[WhenPaid] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[FAS_FeesAndCharges] ADD CONSTRAINT [PK_FAS_FeesAndCharges] PRIMARY KEY CLUSTERED  ([FasFeesAndChargesID])
GO
ALTER TABLE [dbo].[FAS_FeesAndCharges] ADD CONSTRAINT [FK_FAS_FeesAndCharges_FAS_CheckerData] FOREIGN KEY ([FasCheckerDataID]) REFERENCES [dbo].[FAS_CheckerData] ([FasCheckerDataID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
