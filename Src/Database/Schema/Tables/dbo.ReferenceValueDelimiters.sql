CREATE TABLE [dbo].[ReferenceValueDelimiters]
(
[ReferenceValueDelimiterID] [int] NOT NULL IDENTITY(1, 1),
[Delimiter] [char] (1) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ReferenceValueDelimiters] ADD CONSTRAINT [PK_ReferenceValueDelimiters] PRIMARY KEY CLUSTERED  ([ReferenceValueDelimiterID])
GO
