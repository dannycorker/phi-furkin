CREATE TABLE [dbo].[RulesEngine_ParameterTypes]
(
[ParameterTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[RulesEngine_ParameterTypes] ADD CONSTRAINT [PK_RulesEngine_ParameterTypes] PRIMARY KEY CLUSTERED  ([ParameterTypeID])
GO
