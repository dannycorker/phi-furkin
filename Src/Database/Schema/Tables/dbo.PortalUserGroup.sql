CREATE TABLE [dbo].[PortalUserGroup]
(
[PortalUserGroupID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[GroupName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[GroupDescription] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[ShowGroupMessage] [bit] NULL,
[MessageTitle] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[MessageBody] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[HideOwnCustomer] [bit] NULL
)
GO
ALTER TABLE [dbo].[PortalUserGroup] ADD CONSTRAINT [PK_PortalUserGroup] PRIMARY KEY CLUSTERED  ([PortalUserGroupID])
GO
CREATE NONCLUSTERED INDEX [IX_PortalUserGroup_Client] ON [dbo].[PortalUserGroup] ([ClientID])
GO
ALTER TABLE [dbo].[PortalUserGroup] ADD CONSTRAINT [FK_PortalUserGroup_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
