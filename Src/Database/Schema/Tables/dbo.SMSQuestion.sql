CREATE TABLE [dbo].[SMSQuestion]
(
[SMSQuestionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SMSQuestionnaireID] [int] NOT NULL,
[Question] [varchar] (960) COLLATE Latin1_General_CI_AS NOT NULL,
[QuestionOrder] [int] NULL,
[DocumentTypeID] [int] NULL,
[Enabled] [bit] NULL,
[Timeout] [int] NULL,
[Repeat] [int] NULL,
[RepeatPrefix] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[DetailFieldToSave] [int] NULL,
[EvaluateResponse] [bit] NULL,
[VoiceAction] [int] NULL,
[RedirectTo] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[GatherNumDigits] [int] NULL,
[RecordingMaxLength] [int] NULL
)
GO
ALTER TABLE [dbo].[SMSQuestion] ADD CONSTRAINT [PK_SMSQuestion] PRIMARY KEY CLUSTERED  ([SMSQuestionID])
GO
ALTER TABLE [dbo].[SMSQuestion] ADD CONSTRAINT [FK_SMSQuestion_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SMSQuestion] ADD CONSTRAINT [FK_SMSQuestion_DocumentType] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentType] ([DocumentTypeID])
GO
ALTER TABLE [dbo].[SMSQuestion] ADD CONSTRAINT [FK_SMSQuestion_SMSQuestionnaire] FOREIGN KEY ([SMSQuestionnaireID]) REFERENCES [dbo].[SMSQuestionnaire] ([SMSQuestionnaireID])
GO
