CREATE TABLE [dbo].[ChartType]
(
[ChartTypeID] [int] NOT NULL,
[ChartTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ChartType] ADD CONSTRAINT [PK_ChartType] PRIMARY KEY CLUSTERED  ([ChartTypeID])
GO
