CREATE TABLE [dbo].[Objects]
(
[ObjectID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[ObjectTypeID] [int] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[Name] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Details] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[SourceID] [int] NULL
)
GO
ALTER TABLE [dbo].[Objects] ADD CONSTRAINT [PK_Objects] PRIMARY KEY CLUSTERED  ([ObjectID])
GO
CREATE NONCLUSTERED INDEX [IX_Objects_SourceID] ON [dbo].[Objects] ([SourceID]) WHERE ([SourceID]>(0))
GO
