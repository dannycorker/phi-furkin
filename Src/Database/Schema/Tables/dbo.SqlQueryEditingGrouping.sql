CREATE TABLE [dbo].[SqlQueryEditingGrouping]
(
[SqlQueryEditingGroupingID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SqlQueryEditingID] [int] NOT NULL,
[GroupByClause] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[HavingClause] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[HavingColumnID] [int] NULL,
[HavingCriteria1] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[HavingCriteria2] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SqlQueryEditingGrouping] ADD CONSTRAINT [PK_SqlQueryEditingGrouping] PRIMARY KEY CLUSTERED  ([SqlQueryEditingGroupingID])
GO
ALTER TABLE [dbo].[SqlQueryEditingGrouping] ADD CONSTRAINT [FK_SqlQueryEditingGrouping_SqlQueryEditing] FOREIGN KEY ([SqlQueryEditingID]) REFERENCES [dbo].[SqlQueryEditing] ([SqlQueryEditingID]) ON DELETE CASCADE
GO
