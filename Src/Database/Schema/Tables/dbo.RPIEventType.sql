CREATE TABLE [dbo].[RPIEventType]
(
[RPIEventTypeID] [int] NOT NULL,
[RPIEventTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[RPIEventTypeDescription] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[IsAquariumTriggered] [bit] NOT NULL,
[IsEnabled] [bit] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[RPIEventType] ADD CONSTRAINT [PK_RPIEventType] PRIMARY KEY CLUSTERED  ([RPIEventTypeID])
GO
