CREATE TABLE [dbo].[UserRightsDynamicEditing]
(
[UserRightsDynamicEditingID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelID] [int] NOT NULL,
[FunctionTypeID] [int] NOT NULL,
[LeadTypeID] [int] NULL,
[ObjectID] [int] NOT NULL,
[RightID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[UserRightsDynamicEditing] ADD CONSTRAINT [PK_UserRightsDynamicEditing] PRIMARY KEY CLUSTERED  ([UserRightsDynamicEditingID])
GO
ALTER TABLE [dbo].[UserRightsDynamicEditing] ADD CONSTRAINT [FK_UserRightsDynamicEditing_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[UserRightsDynamicEditing] ADD CONSTRAINT [FK_UserRightsDynamicEditing_FunctionType] FOREIGN KEY ([FunctionTypeID]) REFERENCES [dbo].[FunctionType] ([FunctionTypeID])
GO
ALTER TABLE [dbo].[UserRightsDynamicEditing] ADD CONSTRAINT [FK_UserRightsDynamicEditing_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
ALTER TABLE [dbo].[UserRightsDynamicEditing] ADD CONSTRAINT [FK_UserRightsDynamicEditing_Rights] FOREIGN KEY ([RightID]) REFERENCES [dbo].[Rights] ([RightID])
GO
