CREATE TABLE [dbo].[_C600_ImportXMLPart1]
(
[ImportID] [int] NOT NULL IDENTITY(1, 1),
[PolicyXML] [xml] NOT NULL,
[FileName] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[BatchID] [int] NULL,
[CustomerID] [int] NULL,
[PolicyLeadID] [int] NULL
)
GO
ALTER TABLE [dbo].[_C600_ImportXMLPart1] ADD CONSTRAINT [PK__C600_ImportXMLPart1] PRIMARY KEY CLUSTERED  ([ImportID])
GO
