CREATE TABLE [dbo].[ThirdPartySystemEvent]
(
[ThirdPartySystemEventID] [int] NOT NULL IDENTITY(1, 1),
[ThirdPartySystemID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[Name] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[EventTypeID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[ThirdPartySystemKey] [int] NULL,
[MessageName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ThirdPartySystemEvent] ADD CONSTRAINT [PK_ThirdPartySystemEvent] PRIMARY KEY CLUSTERED  ([ThirdPartySystemEventID])
GO
ALTER TABLE [dbo].[ThirdPartySystemEvent] ADD CONSTRAINT [FK_ThirdPartySystemEvent_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ThirdPartySystemEvent] ADD CONSTRAINT [FK_ThirdPartySystemEvent_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[ThirdPartySystemEvent] ADD CONSTRAINT [FK_ThirdPartySystemEvent_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
ALTER TABLE [dbo].[ThirdPartySystemEvent] ADD CONSTRAINT [FK_ThirdPartySystemEvent_ThirdPartySystem] FOREIGN KEY ([ThirdPartySystemID]) REFERENCES [dbo].[ThirdPartySystem] ([ThirdPartySystemId])
GO
