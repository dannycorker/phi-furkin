CREATE TABLE [dbo].[SystemMessage]
(
[SystemMessageID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SystemMessageName] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[SystemMessageText] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Enabled] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[SystemMessage] ADD CONSTRAINT [PK_SystemMessage] PRIMARY KEY CLUSTERED  ([SystemMessageID])
GO
ALTER TABLE [dbo].[SystemMessage] ADD CONSTRAINT [FK_SystemMessage_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
