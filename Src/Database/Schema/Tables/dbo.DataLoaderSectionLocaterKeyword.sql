CREATE TABLE [dbo].[DataLoaderSectionLocaterKeyword]
(
[DataLoaderSectionLocaterKeywordID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DataLoaderMapID] [int] NOT NULL,
[DataLoaderObjectTypeID] [int] NOT NULL,
[DataLoaderMapSectionID] [int] NOT NULL,
[Keyword] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[DataLoaderKeywordMatchTypeID] [int] NOT NULL,
[KeywordNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[RelativeRow] [int] NOT NULL,
[AbsoluteCol] [int] NOT NULL,
[IsForSectionStart] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[DataLoaderSectionLocaterKeyword] ADD CONSTRAINT [PK_DataLoaderSectionLocaterKeyword] PRIMARY KEY CLUSTERED  ([DataLoaderSectionLocaterKeywordID])
GO
ALTER TABLE [dbo].[DataLoaderSectionLocaterKeyword] ADD CONSTRAINT [FK_DataLoaderSectionLocaterKeyword_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DataLoaderSectionLocaterKeyword] ADD CONSTRAINT [FK_DataLoaderSectionLocaterKeyword_DataLoaderKeywordMatchType] FOREIGN KEY ([DataLoaderKeywordMatchTypeID]) REFERENCES [dbo].[DataLoaderKeywordMatchType] ([DataLoaderKeywordMatchTypeID])
GO
ALTER TABLE [dbo].[DataLoaderSectionLocaterKeyword] ADD CONSTRAINT [FK_DataLoaderSectionLocaterKeyword_DataLoaderMap] FOREIGN KEY ([DataLoaderMapID]) REFERENCES [dbo].[DataLoaderMap] ([DataLoaderMapID])
GO
ALTER TABLE [dbo].[DataLoaderSectionLocaterKeyword] ADD CONSTRAINT [FK_DataLoaderSectionLocaterKeyword_DataLoaderMapSection] FOREIGN KEY ([DataLoaderMapSectionID]) REFERENCES [dbo].[DataLoaderMapSection] ([DataLoaderMapSectionID])
GO
ALTER TABLE [dbo].[DataLoaderSectionLocaterKeyword] ADD CONSTRAINT [FK_DataLoaderSectionLocaterKeyword_DataLoaderObjectType] FOREIGN KEY ([DataLoaderObjectTypeID]) REFERENCES [dbo].[DataLoaderObjectType] ([DataLoaderObjectTypeID])
GO
