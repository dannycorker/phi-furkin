CREATE TABLE [dbo].[ScriptOption]
(
[ScriptOptionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientPersonnelID] [int] NOT NULL,
[ScriptEditorTheme] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ShowLineNumbers] [bit] NULL,
[CollapseTopPanel] [bit] NULL,
[CollapseLeftPanel] [bit] NULL,
[CollapseRightPanel] [bit] NULL
)
GO
ALTER TABLE [dbo].[ScriptOption] ADD CONSTRAINT [PK_ScriptOption] PRIMARY KEY CLUSTERED  ([ScriptOptionID])
GO
ALTER TABLE [dbo].[ScriptOption] ADD CONSTRAINT [FK_ScriptOption_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[ScriptOption] ADD CONSTRAINT [FK_ScriptOption_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
