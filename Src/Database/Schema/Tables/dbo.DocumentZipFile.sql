CREATE TABLE [dbo].[DocumentZipFile]
(
[DocumentZipFileID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DocumentZipInformationID] [int] NOT NULL,
[StatusID] [int] NOT NULL,
[ServerMapPath] [varchar] (1000) COLLATE Latin1_General_CI_AS NOT NULL,
[FileName] [varchar] (1000) COLLATE Latin1_General_CI_AS NOT NULL,
[ZipCount] [int] NULL,
[WhoStored] [int] NULL,
[WhenStored] [datetime] NULL,
[WhenCreated] [datetime] NULL,
[WhoDownloaded] [int] NULL,
[WhenDownloaded] [datetime] NULL
)
GO
ALTER TABLE [dbo].[DocumentZipFile] ADD CONSTRAINT [PK_DocumentZipFile] PRIMARY KEY CLUSTERED  ([DocumentZipFileID])
GO
