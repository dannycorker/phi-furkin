CREATE TABLE [dbo].[OneIncPolicy]
(
[OneIncPolicyID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[PAMatterID] [int] NULL,
[OneIncRequestID] [int] NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[OneIncPolicy] ADD CONSTRAINT [PK_OneIncPolicy] PRIMARY KEY CLUSTERED  ([OneIncPolicyID])
GO
