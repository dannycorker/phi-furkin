CREATE TABLE [dbo].[SMSCommandGroup]
(
[SMSCommandGroupID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[SMSCommandGroupName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[SMSCommandGroupDescription] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[SMSIncomingPhoneNumber] [varchar] (16) COLLATE Latin1_General_CI_AS NOT NULL,
[CommandNotRecognisedResponse] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[Enabled] [bit] NULL
)
GO
ALTER TABLE [dbo].[SMSCommandGroup] ADD CONSTRAINT [PK_SMSCommandGroup] PRIMARY KEY CLUSTERED  ([SMSCommandGroupID])
GO
CREATE NONCLUSTERED INDEX [IX_SMSCommandGroup] ON [dbo].[SMSCommandGroup] ([SMSIncomingPhoneNumber])
GO
ALTER TABLE [dbo].[SMSCommandGroup] ADD CONSTRAINT [FK_SMSCommandGroup_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
