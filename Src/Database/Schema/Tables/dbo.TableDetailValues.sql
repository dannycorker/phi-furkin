CREATE TABLE [dbo].[TableDetailValues]
(
[TableDetailValueID] [int] NOT NULL IDENTITY(1, 1),
[TableRowID] [int] NOT NULL,
[ResourceListID] [int] NULL,
[DetailFieldID] [int] NOT NULL,
[DetailValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[LeadID] [int] NULL,
[MatterID] [int] NULL,
[ClientID] [int] NOT NULL,
[EncryptedValue] [varchar] (3000) COLLATE Latin1_General_CI_AS NULL,
[ErrorMsg] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ValueInt] [int] NULL,
[ValueMoney] [money] NULL,
[ValueDate] [date] NULL,
[ValueDateTime] [datetime2] (0) NULL,
[CustomerID] [int] NULL,
[CaseID] [int] NULL,
[ClientPersonnelID] [int] NULL,
[ContactID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2011-06-01
-- Description:	Prevent massive deletes from being committed
-- =============================================
CREATE TRIGGER [dbo].[trgd_TableDetailValues]
   ON [dbo].[TableDetailValues]
   AFTER DELETE
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @DeletedCount int,
	@MaxDeleteLimit int = 1000000,
	@ErrorMessage varchar(500)

	SELECT @DeletedCount = count(*) FROM deleted

	/* Every ing, as the logfile grows by  */
	IF @DeletedCount > @MaxDeleteLimit
	BEGIN
		SELECT @ErrorMessage = 'trgd_TableDetailValues has blocked the delete of ' + CAST(@DeletedCount as varchar) + ' TableDetailValues because the limit is set to ' + CAST(@MaxDeleteLimit as varchar) + '. Please split the delete into smaller chunks to avoid


swamping the log file, which increases by 3GB for every 1 million deletes!'
		RAISERROR(@ErrorMessage, 16, 1)
		ROLLBACK
		RETURN
	END

		/* Populate deltas in the history table*/
	INSERT INTO TableDetailValuesHistory
	(TableRowID, DetailFieldID, ClientID, ClientPersonnelID, CustomerID, ContactID, LeadID, CaseID, MatterID, DetailValue, EncryptedValue, ValueInt, ValueMoney, ValueDate, ValueDateTime, WhenSaved, WhoSaved)
	SELECT
	d.TableRowID, d.DetailFieldID, d.ClientID, d.ClientPersonnelID, d.CustomerID, d.ContactID, d.LeadID, d.CaseID, d.MatterID,
	d.DetailValue, d.EncryptedValue,
	dbo.fnValueAsIntBitsIncluded(d.DetailValue),
	CASE WHEN dbo.fnIsMoney(d.DetailValue) = 1 THEN convert(money, d.DetailValue) ELSE NULL END,
	CASE WHEN dbo.fnIsDateTime(d.DetailValue) = 1 THEN convert(date, d.DetailValue) ELSE NULL END,
	CASE WHEN dbo.fnIsDateTime(d.DetailValue) = 1 THEN convert(datetime2, d.DetailValue) ELSE NULL END,
	dbo.fn_GetDate_Local(), 0
	from deleted d
	inner join detailfields df WITH (NOLOCK) on df.detailfieldid = d.detailfieldid and df.maintainhistory = 1

END
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-27
-- Description:	Store DetailValue as specific datatypes
-- 2011-07-13 JWG Remove duplicates instantly at source
-- =============================================
CREATE TRIGGER [dbo].[trgiu_TableDetailValues]
   ON [dbo].[TableDetailValues]
   AFTER INSERT, UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF SYSTEM_USER = 'notriggers'
	BEGIN
		RETURN
	END


    /*
		If the DetailValue column has been updated then fire the trigger,
		otherwise we are here again because the trigger has just fired,
		in which case do nothing.
	*/
    IF UPDATE(DetailValue)
    BEGIN
		/* Check for duplicates (but only on insert, not update) */
		IF NOT EXISTS (SELECT * FROM deleted)
		BEGIN
			/* Only issue the delete if we know that any records exist, otherwise the delete operation gets attempted every time regardless */
			IF EXISTS(SELECT * FROM dbo.TableDetailValues dv WITH (NOLOCK) INNER JOIN inserted i ON i.TableRowID = dv.TableRowID AND i.DetailFieldID = dv.DetailFieldID AND i.TableDetailValueID > dv.TableDetailValueID )
			BEGIN
				/* Remove any older duplicates for this field */
				DELETE dbo.TableDetailValues
				OUTPUT 6, deleted.TableDetailValueID, deleted.TableRowID, deleted.DetailValue, deleted.DetailFieldID
				INTO dbo.__DeletedValue (DetailFieldSubTypeID, DetailValueID, ParentID, DetailValue, DetailFieldID)
				FROM dbo.TableDetailValues dv
				INNER JOIN inserted i ON i.TableRowID = dv.TableRowID AND i.DetailFieldID = dv.DetailFieldID AND i.TableDetailValueID > dv.TableDetailValueID
			END
		END

		/* Populate ValueInt et al */
		UPDATE dbo.TableDetailValues
		SET ValueInt = dbo.fnValueAsIntBitsIncluded(i.DetailValue) /*CASE WHEN dbo.fnIsInt(i.detailvalue) = 1 THEN convert(int, i.DetailValue) ELSE NULL END*/,
		ValueMoney = CASE WHEN dbo.fnIsMoney(i.DetailValue) = 1 THEN CONVERT(MONEY, i.DetailValue) ELSE NULL END,
		ValueDate = CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN CONVERT(DATE, i.DetailValue) ELSE NULL END,
		ValueDateTime = CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN CONVERT(DATETIME2, i.DetailValue) ELSE NULL END
		FROM dbo.TableDetailValues dv
		INNER JOIN inserted i ON i.TableDetailValueID = dv.TableDetailValueID

		/* Populate deltas in the history table */
		INSERT INTO TableDetailValuesHistory
		(TableRowID, DetailFieldID, ClientID, ClientPersonnelID, CustomerID, ContactID, LeadID, CaseID, MatterID, DetailValue, EncryptedValue, ValueInt, ValueMoney, ValueDate, ValueDateTime, WhenSaved, WhoSaved, ResourceListID)
		SELECT
		i.TableRowID, i.DetailFieldID, i.ClientID, i.ClientPersonnelID, i.CustomerID, i.ContactID, i.LeadID, i.CaseID, i.MatterID,
		i.DetailValue, i.EncryptedValue,
		dbo.fnValueAsIntBitsIncluded(i.DetailValue),
		CASE WHEN dbo.fnIsMoney(i.DetailValue) = 1 THEN CONVERT(MONEY, i.DetailValue) ELSE NULL END,
		CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN CONVERT(DATE, i.DetailValue) ELSE NULL END,
		CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN CONVERT(DATETIME2, i.DetailValue) ELSE NULL END,
		dbo.fn_GetDate_Local(), 1, i.ResourceListID
		FROM inserted i
		LEFT JOIN deleted d ON i.tabledetailvalueid = d.tabledetailvalueid AND i.detailvalue <> d.detailvalue
		INNER JOIN detailfields df WITH (NOLOCK) ON df.detailfieldid = i.detailfieldid AND df.maintainhistory = 1


	END


END
GO
ALTER TABLE [dbo].[TableDetailValues] ADD CONSTRAINT [PK_TableDetailValues] PRIMARY KEY CLUSTERED  ([TableDetailValueID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValues_CaseField] ON [dbo].[TableDetailValues] ([CaseID], [DetailFieldID], [ClientID]) INCLUDE ([TableRowID]) WHERE ([CaseID] IS NOT NULL)
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValues_Client] ON [dbo].[TableDetailValues] ([ClientID]) INCLUDE ([TableRowID])
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValues_ClientField] ON [dbo].[TableDetailValues] ([ClientID], [DetailFieldID]) INCLUDE ([TableRowID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValues_ClientPersonnelField] ON [dbo].[TableDetailValues] ([ClientPersonnelID], [DetailFieldID], [ClientID]) INCLUDE ([TableRowID]) WHERE ([ClientPersonnelID] IS NOT NULL)
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValues_ContactField] ON [dbo].[TableDetailValues] ([ContactID], [DetailFieldID], [ClientID]) INCLUDE ([TableRowID]) WHERE ([ContactID] IS NOT NULL)
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValues_CustomerField] ON [dbo].[TableDetailValues] ([CustomerID], [DetailFieldID], [ClientID]) INCLUDE ([TableRowID]) WHERE ([CustomerID] IS NOT NULL)
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValues_FieldTableRow] ON [dbo].[TableDetailValues] ([DetailFieldID], [TableRowID], [ClientID], [ValueInt]) INCLUDE ([ResourceListID], [ValueDateTime], [ValueMoney])
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValues_FieldTableRow2017V2] ON [dbo].[TableDetailValues] ([DetailFieldID], [TableRowID], [ClientID], [ValueInt]) INCLUDE ([ResourceListID], [ValueDateTime], [ValueMoney])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValues_FieldValueDate] ON [dbo].[TableDetailValues] ([DetailFieldID], [ValueDate], [ClientID]) INCLUDE ([TableRowID]) WHERE ([ValueDate] IS NOT NULL)
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValues_FieldValueInt] ON [dbo].[TableDetailValues] ([DetailFieldID], [ValueInt], [ClientID]) INCLUDE ([TableRowID]) WHERE ([ValueInt] IS NOT NULL)
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValues_LeadFieldRow] ON [dbo].[TableDetailValues] ([LeadID], [DetailFieldID], [ClientID], [TableRowID])
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValues_MatterFieldRow] ON [dbo].[TableDetailValues] ([MatterID], [DetailFieldID], [ClientID], [TableRowID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValues_ResourceListField] ON [dbo].[TableDetailValues] ([ResourceListID], [DetailFieldID], [ClientID]) INCLUDE ([TableRowID]) WHERE ([ResourceListID] IS NOT NULL)
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValues_TableRowField] ON [dbo].[TableDetailValues] ([TableRowID], [DetailFieldID], [ClientID])
GO
ALTER TABLE [dbo].[TableDetailValues] ADD CONSTRAINT [FK_TableDetailValues_TableRows] FOREIGN KEY ([TableRowID]) REFERENCES [dbo].[TableRows] ([TableRowID]) ON DELETE CASCADE
GO
