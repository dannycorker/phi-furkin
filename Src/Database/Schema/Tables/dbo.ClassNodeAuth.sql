CREATE TABLE [dbo].[ClassNodeAuth]
(
[ClassNodeAuthID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClassNodeID] [int] NOT NULL,
[ClientPersonnelAdminGroupID] [int] NULL,
[RightsLevel] [int] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[Deleted] [bit] NULL
)
GO
ALTER TABLE [dbo].[ClassNodeAuth] ADD CONSTRAINT [PK__ClassNod__C27889F81847FC16] PRIMARY KEY CLUSTERED  ([ClassNodeAuthID])
GO
ALTER TABLE [dbo].[ClassNodeAuth] ADD CONSTRAINT [FK__ClassNode__Class__1B2468C1] FOREIGN KEY ([ClassNodeID]) REFERENCES [dbo].[ClassNode] ([ClassNodeID])
GO
ALTER TABLE [dbo].[ClassNodeAuth] ADD CONSTRAINT [FK__ClassNode__Clien__1A304488] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ClassNodeAuth] ADD CONSTRAINT [FK__ClassNode__Clien__1C188CFA] FOREIGN KEY ([ClientPersonnelAdminGroupID]) REFERENCES [dbo].[ClientPersonnelAdminGroups] ([ClientPersonnelAdminGroupID])
GO
ALTER TABLE [dbo].[ClassNodeAuth] ADD CONSTRAINT [FK__ClassNode__WhoCr__1D0CB133] FOREIGN KEY ([WhoCreated]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[ClassNodeAuth] ADD CONSTRAINT [FK__ClassNode__WhoMo__1E00D56C] FOREIGN KEY ([WhoModified]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
