CREATE TABLE [dbo].[_C00_CDL_Data]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[Branch] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Client] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Reference] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[PolicyNumber] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Title] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Firstname] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Lastname] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Address1] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Address2] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Address3] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Address4] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[PostCode] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Email] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Telephone1] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Telephone2] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[SchemeCode] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[AffinityGroup] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[DateOfPolicyInception] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[DateOfPolicyStart] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[DateOfPolicyEnd] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[CurrentStatus] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Type] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Sex] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[PetName] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Breed] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[PetDateOfBirth] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[PurchasePrice] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[VoluntaryExcess] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[VIPFlag] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[PremiumPaidSinceInception] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[PremiumOwed] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[CancellationDate] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ModifiedDate] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[BreedID] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[CustomerID] [int] NULL,
[LeadID] [int] NULL,
[Processed] [datetime] NULL,
[Imported] [datetime] NULL,
[Comment] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[_C00_CDL_Data] ADD CONSTRAINT [PK__C00_CDL_Data] PRIMARY KEY CLUSTERED  ([ID])
GO
