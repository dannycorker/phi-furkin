CREATE TABLE [dbo].[DocumentSignProviderUser]
(
[DSPUserID] [int] NOT NULL IDENTITY(1, 1),
[DSPTypeID] [int] NOT NULL,
[DSPAccountID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[UserName] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[Password] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[DocumentSignProviderUser] ADD CONSTRAINT [PK_DocumentSignProviderUser] PRIMARY KEY CLUSTERED  ([DSPUserID])
GO
