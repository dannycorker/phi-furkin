CREATE TABLE [dbo].[BraintreePaymentResponse]
(
[BraintreePaymentResponseID] [int] NOT NULL IDENTITY(1, 1),
[CardTransactionID] [int] NOT NULL,
[ResponseXML] [xml] NOT NULL,
[WhenCreated] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[BraintreePaymentResponse] ADD CONSTRAINT [PK_BraintreePaymentRequest] PRIMARY KEY CLUSTERED  ([BraintreePaymentResponseID])
GO
