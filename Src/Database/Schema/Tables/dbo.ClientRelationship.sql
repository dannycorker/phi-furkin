CREATE TABLE [dbo].[ClientRelationship]
(
[ClientRelationshipID] [int] NOT NULL IDENTITY(1, 1),
[ClientRelationshipName] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[OutgoingClientID] [int] NOT NULL,
[ReceivingClientID] [int] NOT NULL,
[OutgoingLeadTypeID] [int] NOT NULL,
[IncomingLeadTypeID] [int] NOT NULL,
[OutgoingEventTypeID] [int] NOT NULL,
[IncomingEventTypeID] [int] NOT NULL,
[Enabled] [bit] NOT NULL CONSTRAINT [DF__ClientRel__Enabl__48EFCE0F] DEFAULT ((0)),
[ClearLeadRefs] [bit] NOT NULL CONSTRAINT [DF__ClientRel__Clear__47FBA9D6] DEFAULT ((0))
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2009-04-06
-- Description:	Capture changes and store in History table
-- =============================================
CREATE TRIGGER [dbo].[trgiud_ClientRelationship]
   ON  [dbo].[ClientRelationship]
   AFTER INSERT,DELETE,UPDATE
AS
BEGIN
	SET NOCOUNT ON;

    -- Inserts appear in the inserted table,
    -- Deletes appear in the deleted table,
    -- and Updates appear in both tables at once identically.
    -- These are all 'logical' Sql Server tables.
	INSERT INTO dbo.ClientRelationshipHistory(
		ClientRelationshipID,
		ClientRelationshipName,
		OutgoingClientID,
		ReceivingClientID,
		OutgoingLeadTypeID,
		IncomingLeadTypeID,
		OutgoingEventTypeID,
		IncomingEventTypeID,
		[Enabled],
		ClearLeadRefs,
		WhoChanged,
		WhenChanged,
		ChangeAction,
		ChangeNotes
	)
	SELECT
		COALESCE(i.ClientRelationshipID, d.ClientRelationshipID),
		COALESCE(i.ClientRelationshipName, d.ClientRelationshipName),
		COALESCE(i.OutgoingClientID, d.OutgoingClientID),
		COALESCE(i.ReceivingClientID, d.ReceivingClientID),
		COALESCE(i.OutgoingLeadTypeID, d.OutgoingLeadTypeID),
		COALESCE(i.IncomingLeadTypeID, d.IncomingLeadTypeID),
		COALESCE(i.OutgoingEventTypeID, d.OutgoingEventTypeID),
		COALESCE(i.IncomingEventTypeID, d.IncomingEventTypeID),
		COALESCE(i.[Enabled], d.[Enabled]),
		COALESCE(i.ClearLeadRefs, d.ClearLeadRefs),
		NULL, --i.WhoChanged is not available yet
		dbo.fn_GetDate_Local(),
		CASE WHEN d.ClientRelationshipID IS NULL THEN 'Insert' WHEN i.ClientRelationshipID IS NULL THEN 'Delete' ELSE 'Update' END,
		dbo.fnGetUserInfo()
	FROM inserted i
	FULL OUTER JOIN deleted d ON i.ClientRelationshipID = d.ClientRelationshipID

END
GO
ALTER TABLE [dbo].[ClientRelationship] ADD CONSTRAINT [PK_ClientRelationship] PRIMARY KEY CLUSTERED  ([ClientRelationshipID])
GO
ALTER TABLE [dbo].[ClientRelationship] ADD CONSTRAINT [FK_ClientRelationship_EventType] FOREIGN KEY ([OutgoingEventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[ClientRelationship] ADD CONSTRAINT [FK_ClientRelationship_EventType1] FOREIGN KEY ([IncomingEventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[ClientRelationship] ADD CONSTRAINT [FK_ClientRelationship_LeadType] FOREIGN KEY ([OutgoingLeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
ALTER TABLE [dbo].[ClientRelationship] ADD CONSTRAINT [FK_ClientRelationship_LeadType1] FOREIGN KEY ([IncomingLeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
ALTER TABLE [dbo].[ClientRelationship] ADD CONSTRAINT [FK_ClientRelationship_OutgoingClients] FOREIGN KEY ([OutgoingClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ClientRelationship] ADD CONSTRAINT [FK_ClientRelationship_ReceivingClients] FOREIGN KEY ([ReceivingClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
