CREATE TABLE [dbo].[ClientPersonnelDetailValues]
(
[ClientPersonnelDetailValueID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[DetailValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[ErrorMsg] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[EncryptedValue] [varchar] (3000) COLLATE Latin1_General_CI_AS NULL,
[ValueInt] [int] NULL,
[ValueMoney] [money] NULL,
[ValueDate] [date] NULL,
[ValueDateTime] [datetime2] (0) NULL,
[SourceID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2010-11-23
-- Description:	Store DetailValue as specific datatypes
-- 2011-07-13 JWG Remove duplicates instantly at source
-- =============================================
CREATE TRIGGER [dbo].[trgiu_ClientPersonnelDetailValues]
   ON [dbo].[ClientPersonnelDetailValues]
   AFTER INSERT, UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END


    /*
		If the DetailValue column has been updated then fire the trigger,
		otherwise we are here again because the trigger has just fired,
		in which case do nothing.
	*/
    IF UPDATE(DetailValue)
    BEGIN
		/* Check for duplicates (but only on insert, not update) */
		IF NOT EXISTS (SELECT * FROM deleted)
		BEGIN
			/* Only issue the delete if we know that any records exist, otherwise the delete operation gets attempted every time regardless */
			IF EXISTS(SELECT * FROM dbo.ClientPersonnelDetailValues dv WITH (NOLOCK) INNER JOIN inserted i ON i.ClientPersonnelID = dv.ClientPersonnelID AND i.DetailFieldID = dv.DetailFieldID AND i.ClientPersonnelDetailValueID > dv.ClientPersonnelDetailValueID)
			BEGIN
				/* Remove any older duplicates for this field */
				DELETE dbo.ClientPersonnelDetailValues
				OUTPUT 13, deleted.ClientPersonnelDetailValueID, deleted.ClientPersonnelID, deleted.DetailValue, deleted.DetailFieldID
				INTO dbo.__DeletedValue (DetailFieldSubTypeID, DetailValueID, ParentID, DetailValue, DetailFieldID)
				FROM dbo.ClientPersonnelDetailValues dv
				INNER JOIN inserted i ON i.ClientPersonnelID = dv.ClientPersonnelID AND i.DetailFieldID = dv.DetailFieldID AND i.ClientPersonnelDetailValueID > dv.ClientPersonnelDetailValueID
			END
		END

		/* Populate ValueInt et al */
		UPDATE dbo.ClientPersonnelDetailValues
		SET ValueInt = dbo.fnValueAsIntBitsIncluded(i.DetailValue) /*CASE WHEN i.detailvalue = 'true' THEN 1 WHEN i.detailvalue = 'false' THEN 0 WHEN dbo.fnIsInt(i.detailvalue) = 1 THEN convert(int, i.DetailValue) ELSE NULL END*/,
		ValueMoney = CASE WHEN dbo.fnIsMoney(i.DetailValue) = 1 THEN convert(money, i.DetailValue) ELSE NULL END,
		ValueDate = CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN convert(date, i.DetailValue) ELSE NULL END,
		ValueDateTime = CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN convert(datetime2, i.DetailValue) ELSE NULL END
		FROM dbo.ClientPersonnelDetailValues dv
		INNER JOIN inserted i ON i.ClientPersonnelDetailValueID = dv.ClientPersonnelDetailValueID
	END


END



GO
ALTER TABLE [dbo].[ClientPersonnelDetailValues] ADD CONSTRAINT [PK_ClientPersonnelDetailValues] PRIMARY KEY CLUSTERED  ([ClientPersonnelDetailValueID])
GO
ALTER TABLE [dbo].[ClientPersonnelDetailValues] ADD CONSTRAINT [FK_ClientPersonnelDetailValues_Client] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ClientPersonnelDetailValues] ADD CONSTRAINT [FK_ClientPersonnelDetailValues_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ClientPersonnelDetailValues] ADD CONSTRAINT [FK_ClientPersonnelDetailValues_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
