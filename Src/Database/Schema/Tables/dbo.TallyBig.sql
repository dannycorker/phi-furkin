CREATE TABLE [dbo].[TallyBig]
(
[N] [int] NOT NULL IDENTITY(1, 1)
)
GO
ALTER TABLE [dbo].[TallyBig] ADD CONSTRAINT [PK_TallyBig_N] PRIMARY KEY CLUSTERED  ([N])
GO
