CREATE TABLE [dbo].[HskIndexUsage]
(
[AnyID] [int] NOT NULL IDENTITY(1, 1),
[TableName] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[TableID] [bigint] NOT NULL,
[IsPK] [bit] NOT NULL,
[TableRowCount] [bigint] NOT NULL,
[IndexName] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[IndexOrderID] [int] NOT NULL,
[IndexPageCount] [bigint] NOT NULL,
[UserSeeks] [bigint] NULL,
[UserScans] [bigint] NULL,
[UserLookups] [bigint] NULL,
[UserUpdates] [bigint] NULL,
[LastChecked] [datetime] NOT NULL,
[FragmentationPctBefore] [decimal] (7, 4) NULL,
[LookupTimeInSeconds] [int] NULL,
[FragmentationPctAfter] [decimal] (7, 4) NULL,
[DefragTimeInSeconds] [int] NULL,
[CommandToApply] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[CommandOrder] [int] NULL
)
GO
ALTER TABLE [dbo].[HskIndexUsage] ADD CONSTRAINT [PK_HskIndexUsage] PRIMARY KEY CLUSTERED  ([AnyID])
GO
