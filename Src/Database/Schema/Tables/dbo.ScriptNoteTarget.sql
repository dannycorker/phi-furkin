CREATE TABLE [dbo].[ScriptNoteTarget]
(
[ScriptNoteTargetID] [int] NOT NULL IDENTITY(1, 1),
[ScriptID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[NoteTypeID] [int] NOT NULL,
[PriorityID] [int] NOT NULL,
[ApplyToAllCases] [bit] NULL
)
GO
ALTER TABLE [dbo].[ScriptNoteTarget] ADD CONSTRAINT [PK_ScriptNoteTarget] PRIMARY KEY CLUSTERED  ([ScriptNoteTargetID])
GO
ALTER TABLE [dbo].[ScriptNoteTarget] ADD CONSTRAINT [FK_ScriptNoteTarget_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ScriptNoteTarget] ADD CONSTRAINT [FK_ScriptNoteTarget_NoteType] FOREIGN KEY ([NoteTypeID]) REFERENCES [dbo].[NoteType] ([NoteTypeID])
GO
ALTER TABLE [dbo].[ScriptNoteTarget] ADD CONSTRAINT [FK_ScriptNoteTarget_Script] FOREIGN KEY ([ScriptID]) REFERENCES [dbo].[Script] ([ScriptID])
GO
