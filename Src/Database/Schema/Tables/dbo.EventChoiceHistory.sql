CREATE TABLE [dbo].[EventChoiceHistory]
(
[EventChoiceHistoryID] [int] NOT NULL IDENTITY(1, 1),
[EventChoiceID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[Description] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[NextEventTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[ThreadNumber] [int] NOT NULL,
[EscalationEvent] [bit] NOT NULL,
[Field] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[LogicalOperator] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Value1] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Value2] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SqlClauseForInclusion] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Weighting] [int] NULL,
[WhoChanged] [int] NULL,
[WhenChanged] [datetime] NOT NULL,
[ChangeAction] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ChangeNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[EventChoiceHistory] ADD CONSTRAINT [PK_EventChoiceHistory] PRIMARY KEY CLUSTERED  ([EventChoiceHistoryID])
GO
