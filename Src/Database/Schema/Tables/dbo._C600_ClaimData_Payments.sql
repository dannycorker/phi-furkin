CREATE TABLE [dbo].[_C600_ClaimData_Payments]
(
[ClaimPaymentID] [int] NOT NULL IDENTITY(1, 1),
[ClaimSystemClaimID] [int] NOT NULL,
[C600_ClaimData_ClaimID] [int] NOT NULL,
[PaymentGross] [numeric] (18, 2) NULL,
[AccountName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AccountNumber] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[AccountSortcode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[PaymentMethod] [varchar] (15) COLLATE Latin1_General_CI_AS NULL,
[PaymentMethodID] [int] NULL,
[AccountID] [int] NULL,
[CustomerPaymentScheduleID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[_C600_ClaimData_Payments] ADD CONSTRAINT [PK__C600_ClaimData_Payments] PRIMARY KEY CLUSTERED  ([ClaimPaymentID])
GO
ALTER TABLE [dbo].[_C600_ClaimData_Payments] ADD CONSTRAINT [FK__C600_ClaimData_Payments_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
GO
ALTER TABLE [dbo].[_C600_ClaimData_Payments] ADD CONSTRAINT [FK__C600_ClaimData_Payments_CustomerPaymentSchedule] FOREIGN KEY ([CustomerPaymentScheduleID]) REFERENCES [dbo].[CustomerPaymentSchedule] ([CustomerPaymentScheduleID])
GO
