CREATE TABLE [dbo].[PremiumFunderApplicationResponse]
(
[PremiumFunderApplicationResponseID] [int] NOT NULL IDENTITY(1, 1),
[PremiumFunderApplicationRequestID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[CustomerID] [int] NULL,
[MatterID] [int] NULL,
[ResponseJson] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[PremiumFunderRequestID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[PremiumFunderApplicationResponse] ADD CONSTRAINT [PK_PremiumFunderApplicationResponse] PRIMARY KEY CLUSTERED  ([PremiumFunderApplicationResponseID])
GO
