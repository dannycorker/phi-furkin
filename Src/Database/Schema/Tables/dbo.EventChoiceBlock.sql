CREATE TABLE [dbo].[EventChoiceBlock]
(
[EventChoiceBlockID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[EventChoiceBlock] ADD CONSTRAINT [PK_EventChoiceBlock] PRIMARY KEY CLUSTERED  ([EventChoiceBlockID])
GO
