CREATE TABLE [dbo].[PremiumFunderCancelAgreementResponse]
(
[PremiumFunderCancelAgreementResponseID] [int] NOT NULL IDENTITY(1, 1),
[PremiumFunderCancelAgreementRequestID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[MatterID] [int] NULL,
[ResponseJson] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[PremiumFunderRequestID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[PremiumFunderCancelAgreementResponse] ADD CONSTRAINT [PK_PremiumFunderCancelAgreementResponse] PRIMARY KEY CLUSTERED  ([PremiumFunderCancelAgreementResponseID])
GO
