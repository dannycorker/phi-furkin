CREATE TABLE [dbo].[WorkingDays]
(
[WorkingDayID] [int] NOT NULL IDENTITY(1, 1),
[Year] [int] NOT NULL,
[Month] [int] NOT NULL,
[Day] [int] NOT NULL,
[DayNumber] [int] NOT NULL,
[IsWorkDay] [bit] NOT NULL,
[IsWeekDay] [bit] NOT NULL,
[IsBankHoliday] [bit] NOT NULL,
[Date] [datetime] NOT NULL,
[WeekNumber] [tinyint] NOT NULL,
[DayNumberOfWeek] [tinyint] NOT NULL,
[CharDate] [char] (10) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[WorkingDays] ADD CONSTRAINT [PK_WorkingDays] PRIMARY KEY CLUSTERED  ([WorkingDayID])
GO
CREATE NONCLUSTERED INDEX [IX_WorkingDays_CharDate] ON [dbo].[WorkingDays] ([CharDate])
GO
CREATE NONCLUSTERED INDEX [IX_WorkingDays_CharDate2017] ON [dbo].[WorkingDays] ([CharDate], [DayNumberOfWeek])
GO
CREATE NONCLUSTERED INDEX [IX_WorkingDays_Date] ON [dbo].[WorkingDays] ([Date])
GO
