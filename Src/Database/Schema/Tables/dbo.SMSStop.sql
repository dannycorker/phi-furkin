CREATE TABLE [dbo].[SMSStop]
(
[SMSStopID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[MobileNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[StopSMS] [bit] NOT NULL,
[OptOutRequestSent] [bit] NOT NULL,
[Updated] [datetime] NOT NULL,
[Created] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[SMSStop] ADD CONSTRAINT [PK_SMSStop] PRIMARY KEY CLUSTERED  ([SMSStopID])
GO
