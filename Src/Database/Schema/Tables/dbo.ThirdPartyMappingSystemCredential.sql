CREATE TABLE [dbo].[ThirdPartyMappingSystemCredential]
(
[ThirdPartyMappingSystemCredentialID] [int] NOT NULL IDENTITY(1, 1),
[ThirdPartySystemID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[ConsumerKey] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[ConsumerSecret] [varchar] (500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ThirdPartyMappingSystemCredential] ADD CONSTRAINT [PK_ThirdPartyMappingSystemCredential] PRIMARY KEY CLUSTERED  ([ThirdPartyMappingSystemCredentialID])
GO
ALTER TABLE [dbo].[ThirdPartyMappingSystemCredential] ADD CONSTRAINT [FK_ThirdPartyMappingSystemCredential_ThirdPartySystem] FOREIGN KEY ([ThirdPartySystemID]) REFERENCES [dbo].[ThirdPartySystem] ([ThirdPartySystemId])
GO
