CREATE TABLE [dbo].[LeadDocumentFS]
(
[LeadDocumentID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[DocumentTypeID] [int] NULL,
[LeadDocumentTitle] [varchar] (1000) COLLATE Latin1_General_CI_AS NOT NULL,
[UploadDateTime] [datetime] NOT NULL,
[WhoUploaded] [int] NULL,
[FileName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[DocumentFormat] [varchar] (24) COLLATE Latin1_General_CI_AS NULL,
[EmailFrom] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[EmailTo] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[CcList] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[BccList] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ElectronicSignatureDocumentKey] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Encoding] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ContentFormat] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ZipFormat] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DocumentBLOB] [varbinary] (max) FILESTREAM NULL,
[EmailBLOB] [varbinary] (max) FILESTREAM NULL,
[LeadDocumentGUID] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF__LeadDocum__LeadD__0425A276] DEFAULT (newid()),
[DocumentBLOBSize] AS (datalength([DocumentBLOB])) PERSISTED,
[EmailBLOBSize] AS (datalength([EmailBLOB])) PERSISTED,
CONSTRAINT [UQ__LeadDocu__75C278E3023D5A04] UNIQUE NONCLUSTERED  ([LeadDocumentGUID])
)
GO
ALTER TABLE [dbo].[LeadDocumentFS] ADD CONSTRAINT [PK__LeadDocu__5AC778F57F60ED59] PRIMARY KEY CLUSTERED  ([LeadDocumentID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadDocumentFS_DocumentType] ON [dbo].[LeadDocumentFS] ([DocumentTypeID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadDocumentFS_LeadDocType] ON [dbo].[LeadDocumentFS] ([LeadID], [DocumentTypeID], [ClientID])
GO
GRANT CONTROL ON  [dbo].[LeadDocumentFS] TO [sp_executeall]
GO
