CREATE TABLE [dbo].[UserFunctionControl]
(
[UserFunctionControlID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelID] [int] NOT NULL,
[ModuleID] [int] NOT NULL,
[FunctionTypeID] [int] NOT NULL,
[HasDescendants] [int] NOT NULL,
[RightID] [int] NOT NULL,
[LeadTypeID] [int] NULL
)
GO
ALTER TABLE [dbo].[UserFunctionControl] ADD CONSTRAINT [PK_UserFunctionControl] PRIMARY KEY CLUSTERED  ([UserFunctionControlID])
GO
CREATE NONCLUSTERED INDEX [IX_UserFunctionControlUser] ON [dbo].[UserFunctionControl] ([ClientPersonnelID])
GO
CREATE NONCLUSTERED INDEX [IX_UserFunctionControl_Covering2017] ON [dbo].[UserFunctionControl] ([ClientPersonnelID], [FunctionTypeID], [HasDescendants], [RightID]) INCLUDE ([LeadTypeID], [ModuleID])
GO
ALTER TABLE [dbo].[UserFunctionControl] ADD CONSTRAINT [FK_UserFunctionControl_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[UserFunctionControl] ADD CONSTRAINT [FK_UserFunctionControl_FunctionType] FOREIGN KEY ([FunctionTypeID]) REFERENCES [dbo].[FunctionType] ([FunctionTypeID])
GO
ALTER TABLE [dbo].[UserFunctionControl] ADD CONSTRAINT [FK_UserFunctionControl_Module] FOREIGN KEY ([ModuleID]) REFERENCES [dbo].[Module] ([ModuleID])
GO
ALTER TABLE [dbo].[UserFunctionControl] ADD CONSTRAINT [FK_UserFunctionControl_Rights] FOREIGN KEY ([RightID]) REFERENCES [dbo].[Rights] ([RightID])
GO
