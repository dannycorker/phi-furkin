CREATE TABLE [dbo].[SqlQueryGrouping]
(
[SqlQueryGroupingID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SqlQueryID] [int] NOT NULL,
[GroupByClause] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[HavingClause] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[HavingColumnID] [int] NULL,
[HavingCriteria1] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[HavingCriteria2] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[TempHavingColumnID] [int] NULL,
[TempHavingID] [int] NULL,
[TempGroupingID] [int] NULL
)
GO
ALTER TABLE [dbo].[SqlQueryGrouping] ADD CONSTRAINT [PK_SqlQueryGrouping] PRIMARY KEY CLUSTERED  ([SqlQueryGroupingID])
GO
ALTER TABLE [dbo].[SqlQueryGrouping] ADD CONSTRAINT [FK_SqlQueryGrouping_SqlQuery] FOREIGN KEY ([SqlQueryID]) REFERENCES [dbo].[SqlQuery] ([QueryID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[SqlQueryGrouping] ADD CONSTRAINT [FK_SqlQueryGrouping_SqlQueryColumns] FOREIGN KEY ([HavingColumnID]) REFERENCES [dbo].[SqlQueryColumns] ([SqlQueryColumnID])
GO
