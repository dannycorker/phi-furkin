CREATE TABLE [dbo].[IncendiaConfigurationAquarium]
(
[IncendiaConfigurationAquariumID] [int] NOT NULL IDENTITY(1, 1),
[AccountID] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[IncendiaConfigurationAquarium] ADD CONSTRAINT [PK_IncendiaConfigurationAquarium] PRIMARY KEY CLUSTERED  ([IncendiaConfigurationAquariumID])
GO
