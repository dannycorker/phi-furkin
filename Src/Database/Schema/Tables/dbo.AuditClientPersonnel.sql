CREATE TABLE [dbo].[AuditClientPersonnel]
(
[AuditClientPersonnelID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime2] (0) NOT NULL,
[IsInsert] [bit] NOT NULL,
[IsDisabled] [bit] NOT NULL,
[NewClientPersonnelAdminGroupID] [int] NOT NULL,
[ChangeNotes] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[AuditClientPersonnel] ADD CONSTRAINT [PK_AuditClientPersonnel] PRIMARY KEY CLUSTERED  ([AuditClientPersonnelID])
GO
ALTER TABLE [dbo].[AuditClientPersonnel] ADD CONSTRAINT [FK_AuditClientPersonnel_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[AuditClientPersonnel] ADD CONSTRAINT [FK_AuditClientPersonnel_WhoModified] FOREIGN KEY ([WhoModified]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
