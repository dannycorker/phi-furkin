CREATE TABLE [dbo].[UserGroupMenuSubOption]
(
[UserGroupMenuSubOptionID] [int] NOT NULL IDENTITY(1, 1),
[UserGroupMenuOptionID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[ClientPersonnelAdminGroupID] [int] NULL,
[PanelItemName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[PanelItemCaption] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[PanelItemURL] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[ShowInFancyBox] [bit] NOT NULL,
[SubItemOrder] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[UserGroupMenuSubOption] ADD CONSTRAINT [PK_UserGroupMenuSubOption] PRIMARY KEY CLUSTERED  ([UserGroupMenuSubOptionID])
GO
CREATE NONCLUSTERED INDEX [IX_UserGroupMenuSubOption_ClientID] ON [dbo].[UserGroupMenuSubOption] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_UserGroupMenuSubOption_UserGroupMenuOptionID] ON [dbo].[UserGroupMenuSubOption] ([UserGroupMenuOptionID])
GO
