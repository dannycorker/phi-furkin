CREATE TABLE [dbo].[CustomFieldConfig]
(
[CustomFieldConfigID] [int] NOT NULL IDENTITY(1, 1),
[DetailFieldID] [int] NOT NULL,
[OptionType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[OptionValue] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[CustomFieldConfig] ADD CONSTRAINT [PK_CustomFieldConfig] PRIMARY KEY CLUSTERED  ([CustomFieldConfigID])
GO
