CREATE TABLE [dbo].[WorkflowTask]
(
[WorkflowTaskID] [int] NOT NULL IDENTITY(1, 1),
[WorkflowGroupID] [int] NOT NULL,
[AutomatedTaskID] [int] NOT NULL,
[Priority] [int] NOT NULL,
[AssignedTo] [int] NULL,
[AssignedDate] [datetime] NULL,
[LeadID] [int] NOT NULL,
[CaseID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[FollowUp] [bit] NOT NULL,
[Important] [bit] NOT NULL CONSTRAINT [DF__WorkflowT__Impor__1446FBA6] DEFAULT ((0)),
[CreationDate] [datetime] NOT NULL,
[Escalated] [bit] NOT NULL CONSTRAINT [DF__WorkflowT__Escal__1352D76D] DEFAULT ((0)),
[EscalatedBy] [int] NULL,
[EscalationReason] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[EscalationDate] [datetime] NULL,
[Disabled] [bit] NOT NULL CONSTRAINT [DF__WorkflowT__Disab__125EB334] DEFAULT ((0)),
[DisabledReason] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[DisabledDate] [datetime] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
-- =============================================
-- Author:		Jim Green
-- Create date: 2008-02-27
-- Description:	Prevent duplicate workflow tasks being created
-- Edits:		CT: 260809: Added additional case for Kerobo so that new copies of tasks replace old copies instead
--				CT: 270809: Added check to see whether the WorkflowGroup has changed - leave it be if it hasn't
--              JWG 2010-11-01 For Redress, clear out old tasks that have been duplicated. This allows them to change the order in which customers get called/chased for money.
--              JWG 2014-01-22 #22327 Rewrite to check for any matching records before issuing any delete statements.
-- =============================================
*/
CREATE TRIGGER [dbo].[trgi_workflowtask]
   ON [dbo].[WorkflowTask]
   AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END

	/*
		For Kerobo, replace the existing task with the new task if it is not already assigned and the WorkflowGroupID has changed.
		So, for example, changing a group assignment for a task from 1 to 2 will create a new task with the same details
		within WorkflowGroup 2 and remove the task from WorkflowGroup 1
		JWG/AE 2012-10-12 added C145 (PPIClaimLine) to this branch.
		JWG 2014-01-22 Check for any matching records before issuing a delete.
	*/
	IF EXISTS
	(
		SELECT *
		FROM inserted i
		WHERE i.ClientID IN (138, 145)
	)
	BEGIN

		IF EXISTS
		(
			SELECT *
			FROM inserted i
			INNER JOIN dbo.WorkflowTask wt WITH (NOLOCK) ON wt.CaseID = i.CaseID AND wt.EventTypeID = i.EventTypeID AND i.Followup = wt.Followup
			WHERE i.ClientID IN (138, 145)
			/* inserted taskid must be greater because it is an IDENTITY column, so no need for clause: AND i.WorkflowTaskID > wt.WorkflowTaskID */
			AND i.WorkflowGroupID <> wt.WorkflowGroupID /* Only do this if the group has changed  */
			AND wt.AssignedTo IS NULL
		)
		BEGIN
			DELETE dbo.WorkflowTask
			FROM dbo.WorkflowTask wt
			WHERE wt.ClientID IN (138, 145)
			AND wt.AssignedTo IS NULL
			AND EXISTS (
				SELECT *
				FROM inserted i
				WHERE i.CaseID = wt.CaseID
				AND i.EventTypeID = wt.EventTypeID
				AND i.Followup = wt.Followup
				/* inserted taskid must be greater because it is an IDENTITY column, so no need for clause: AND i.WorkflowTaskID > wt.WorkflowTaskID */
				AND i.WorkflowGroupID <> wt.WorkflowGroupID /* Only do this if the group has changed  */
			)
		END
	END

	/*
		For Redress, replace the existing task with the new task if it is not already assigned.
		The ORDER BY clause of the report determines the order in which they want to call customers,
		(just because someone was top of the list yesterday doesn't mean they are today) so they
		repopulate the whole list every time, putting the biggest debtors at the top each time.
		Similar to Kerobo, but without the check to see if the group has changed.
		JWG 2014-01-22 Check for any matching records before issuing a delete.
	*/
	IF EXISTS
	(
		SELECT *
		FROM inserted i
		WHERE i.ClientID IN (150)
	)
	BEGIN

		IF EXISTS
		(
			SELECT *
			FROM inserted i
			INNER JOIN dbo.WorkflowTask wt WITH (NOLOCK) ON wt.CaseID = i.CaseID AND wt.EventTypeID = i.EventTypeID
			WHERE i.ClientID IN (150)
			AND i.Followup = wt.Followup
			/* inserted taskid must be greater because it is an IDENTITY column, so no need for clause: AND i.WorkflowTaskID > wt.WorkflowTaskID */
			AND wt.AssignedTo IS NULL
		)
		BEGIN
			DELETE dbo.WorkflowTask
			FROM dbo.WorkflowTask wt
			WHERE wt.ClientID IN (150) /*213 removed by CS 2012-10-23; was added by CS for support ticket #16441 originally */
			AND wt.AssignedTo IS NULL
			AND EXISTS (
				SELECT *
				FROM inserted i
				WHERE i.CaseID = wt.CaseID
				AND i.EventTypeID = wt.EventTypeID
				AND i.Followup = wt.Followup
				/* inserted taskid must be greater because it is an IDENTITY column, so no need for clause: AND i.WorkflowTaskID > wt.WorkflowTaskID */
			)
		END
	END

	/*
		For all other clients, remove the newly-added task if it is a duplicate. Keep the older one as it may already be assigned/escalated etc.
		NB this code also executes for the special clients above, which is required functionality.  If the section above was invoked, this is fine because
		their requirements (such as delete the older record) mean that no match will be found in this section.  And if the above section was not relevant (ie one
		or more clauses failed the test), they actually want the newer records to be deleted here.
	*/
	IF EXISTS
	(
		SELECT *
		FROM inserted i
		INNER JOIN dbo.WorkflowTask wt WITH (NOLOCK) ON wt.CaseID = i.CaseID AND wt.EventTypeID = i.EventTypeID AND i.Followup = wt.Followup
		WHERE wt.WorkflowTaskID < i.WorkflowTaskID  /* Don't delete the ones being inserted right now! */
	)
	BEGIN

		/*
			Delete newly-inserted duplicates, keeping the old ones.
		*/
		DELETE wt_new
		FROM dbo.WorkflowTask wt_new
		INNER JOIN inserted i ON wt_new.WorkflowTaskID = i.WorkflowTaskID
		INNER JOIN dbo.WorkflowTask wt_existing ON wt_existing.CaseID = i.CaseID AND wt_existing.EventTypeID = i.EventTypeID AND wt_existing.Followup = i.Followup
		WHERE wt_existing.WorkflowTaskID < i.WorkflowTaskID

	END
END
GO
ALTER TABLE [dbo].[WorkflowTask] ADD CONSTRAINT [PK_WorkflowTask] PRIMARY KEY CLUSTERED  ([WorkflowTaskID])
GO
CREATE NONCLUSTERED INDEX [IX_WorkflowTask_AssignedTo] ON [dbo].[WorkflowTask] ([AssignedTo], [Important])
GO
CREATE NONCLUSTERED INDEX [IX_WorkflowTask_CaseEventFollowup] ON [dbo].[WorkflowTask] ([CaseID], [EventTypeID], [FollowUp])
GO
CREATE NONCLUSTERED INDEX [IX_WorkflowTask_Client] ON [dbo].[WorkflowTask] ([ClientID], [Important])
GO
ALTER TABLE [dbo].[WorkflowTask] ADD CONSTRAINT [FK_WorkflowTask_AutomatedTask] FOREIGN KEY ([AutomatedTaskID]) REFERENCES [dbo].[AutomatedTask] ([TaskID])
GO
ALTER TABLE [dbo].[WorkflowTask] ADD CONSTRAINT [FK_WorkflowTask_Cases] FOREIGN KEY ([CaseID]) REFERENCES [dbo].[Cases] ([CaseID])
GO
ALTER TABLE [dbo].[WorkflowTask] ADD CONSTRAINT [FK_WorkflowTask_ClientPersonnel] FOREIGN KEY ([AssignedTo]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[WorkflowTask] ADD CONSTRAINT [FK_WorkflowTask_ClientPersonnel1] FOREIGN KEY ([EscalatedBy]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[WorkflowTask] ADD CONSTRAINT [FK_WorkflowTask_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[WorkflowTask] ADD CONSTRAINT [FK_WorkflowTask_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[WorkflowTask] ADD CONSTRAINT [FK_WorkflowTask_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID])
GO
ALTER TABLE [dbo].[WorkflowTask] ADD CONSTRAINT [FK_WorkflowTask_WorkflowGroup] FOREIGN KEY ([WorkflowGroupID]) REFERENCES [dbo].[WorkflowGroup] ([WorkflowGroupID])
GO
