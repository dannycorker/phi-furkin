CREATE TABLE [dbo].[LeadTypeShare]
(
[LeadTypeShareID] [int] NOT NULL IDENTITY(1, 1),
[SharedFrom] [int] NOT NULL,
[SharedTo] [int] NOT NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[LeadTypeShare] ADD CONSTRAINT [PK_LeadTypeImport] PRIMARY KEY CLUSTERED  ([LeadTypeShareID])
GO
