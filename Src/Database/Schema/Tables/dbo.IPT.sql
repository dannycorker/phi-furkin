CREATE TABLE [dbo].[IPT]
(
[IPTID] [int] NOT NULL IDENTITY(1, 1),
[TaxName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[CountryID] [int] NOT NULL CONSTRAINT [DF__IPT__CountryID__0C6844C1] DEFAULT ((232)),
[Region] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[TaxPercent] [money] NOT NULL,
[From] [date] NULL,
[To] [date] NULL,
[TaxProportion] [money] NULL,
[GrossMultiplier] [money] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-08-08
-- Description:	Store IPTPercent as proportion * gross factor
-- =============================================
CREATE TRIGGER [dbo].[trgiu_IPT]
   ON [dbo].[IPT]
   AFTER INSERT, UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END


    /*
		If the Percent column has been updated then fire the trigger,
		otherwise we are here again because the trigger has just fired,
		in which case do nothing.
	*/
    IF UPDATE(TaxPercent)
    BEGIN

		/* Populate ValueInt et al */
		UPDATE dbo.IPT
		SET TaxProportion = i.TaxPercent/100,
		GrossMultiplier = (i.TaxPercent/100) + 1
		FROM dbo.IPT dv
		INNER JOIN inserted i ON i.IPTID = dv.IPTID
	END


END


GO
ALTER TABLE [dbo].[IPT] ADD CONSTRAINT [PK_IPT] PRIMARY KEY CLUSTERED  ([IPTID])
GO
ALTER TABLE [dbo].[IPT] ADD CONSTRAINT [FK_IPT_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
