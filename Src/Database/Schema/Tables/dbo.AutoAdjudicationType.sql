CREATE TABLE [dbo].[AutoAdjudicationType]
(
[AutoAdjudicationTypeID] [int] NOT NULL IDENTITY(1, 1),
[AutoAdjudicationTypeName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[AutoAdjudicationType] ADD CONSTRAINT [PK_AutoAdjudicationType] PRIMARY KEY CLUSTERED  ([AutoAdjudicationTypeID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_AutoAdjudicationType] ON [dbo].[AutoAdjudicationType] ([AutoAdjudicationTypeName])
GO
