CREATE TABLE [dbo].[_C00_PremiumCalculationDetailValuesTEMP]
(
[PremiumCalculationDetailValueID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[PremiumCalculationID] [int] NOT NULL,
[RuleSequence] [int] NOT NULL,
[RuleID] [int] NOT NULL,
[RuleInput] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[RuleCheckpoint] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[RuleTransform] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[RuleTransformValue] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[RuleOutput] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[RuleOutputFirstMonth] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[RuleOutputOtherMonth] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[RuleInputOutputDelta] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[RuleInputOutputDeltaFirstMonth] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[RuleInputOutputDeltaOtherMonth] [varchar] (100) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[_C00_PremiumCalculationDetailValuesTEMP] ADD CONSTRAINT [PK__C00_PremiumCalculationDetailValuesTEMP] PRIMARY KEY CLUSTERED  ([PremiumCalculationDetailValueID])
GO
