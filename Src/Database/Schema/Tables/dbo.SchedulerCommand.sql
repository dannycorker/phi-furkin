CREATE TABLE [dbo].[SchedulerCommand]
(
[SchedulerIdentifier] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[SchedulerCommand] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[IsEnabled] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[SchedulerCommand] ADD CONSTRAINT [PK_SchedulerCommand] PRIMARY KEY CLUSTERED  ([SchedulerIdentifier])
GO
