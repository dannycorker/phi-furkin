CREATE TABLE [dbo].[UploadedFile]
(
[UploadedFileID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[FolderID] [int] NOT NULL,
[UploadedFileName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[UploadedFileTitle] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[UploadedFileDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[UploadedFile] [varbinary] (max) FILESTREAM NOT NULL,
[UploadedFileGUID] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF__UploadedF__Uploa__0BC6C43E] DEFAULT (newid()),
[UploadedFileSize] AS (datalength([UploadedFile])) PERSISTED,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
CONSTRAINT [UQ__Uploaded__6AC5AB1A09DE7BCC] UNIQUE NONCLUSTERED  ([UploadedFileGUID])
)
GO
ALTER TABLE [dbo].[UploadedFile] ADD CONSTRAINT [PK__Uploaded__E2C7E7E607020F21] PRIMARY KEY CLUSTERED  ([UploadedFileID])
GO
ALTER TABLE [dbo].[UploadedFile] ADD CONSTRAINT [FK_UploadedFile_ClientPersonnel] FOREIGN KEY ([WhoCreated]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[UploadedFile] ADD CONSTRAINT [FK_UploadedFile_ClientPersonnel1] FOREIGN KEY ([WhoModified]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[UploadedFile] ADD CONSTRAINT [FK_UploadedFile_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[UploadedFile] ADD CONSTRAINT [FK_UploadedFile_Folder] FOREIGN KEY ([FolderID]) REFERENCES [dbo].[Folder] ([FolderID])
GO
