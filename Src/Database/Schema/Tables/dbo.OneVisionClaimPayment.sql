CREATE TABLE [dbo].[OneVisionClaimPayment]
(
[OneVisionClaimPaymentID] [int] NOT NULL IDENTITY(1, 1),
[OneVisionClaimID] [int] NOT NULL,
[VisionPaymentID] [int] NOT NULL,
[Gross] [numeric] (18, 2) NULL,
[AccountID] [int] NULL,
[AccountNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[SortCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[CustomerPaymentScheduleID] [int] NULL,
[CustomerLedgerID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[OneVisionClaimPayment] ADD CONSTRAINT [PK_OneVisionClaimPayment] PRIMARY KEY CLUSTERED  ([OneVisionClaimPaymentID])
GO
ALTER TABLE [dbo].[OneVisionClaimPayment] ADD CONSTRAINT [FK_OneVisionClaimPayment_Account] FOREIGN KEY ([AccountID]) REFERENCES [dbo].[Account] ([AccountID])
GO
ALTER TABLE [dbo].[OneVisionClaimPayment] ADD CONSTRAINT [FK_OneVisionClaimPayment_CustomerLedger] FOREIGN KEY ([CustomerLedgerID]) REFERENCES [dbo].[CustomerLedger] ([CustomerLedgerID])
GO
ALTER TABLE [dbo].[OneVisionClaimPayment] ADD CONSTRAINT [FK_OneVisionClaimPayment_CustomerPaymentSchedule] FOREIGN KEY ([CustomerPaymentScheduleID]) REFERENCES [dbo].[CustomerPaymentSchedule] ([CustomerPaymentScheduleID])
GO
ALTER TABLE [dbo].[OneVisionClaimPayment] ADD CONSTRAINT [FK_OneVisionClaimPayment_OneVisionClaim] FOREIGN KEY ([OneVisionClaimID]) REFERENCES [dbo].[OneVisionClaim] ([OneVisionClaimID])
GO
