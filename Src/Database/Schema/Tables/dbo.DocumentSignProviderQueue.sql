CREATE TABLE [dbo].[DocumentSignProviderQueue]
(
[DSPQueueID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[CaseID] [int] NOT NULL,
[OutLeadEventID] [int] NOT NULL,
[OutEventTypeID] [int] NOT NULL,
[OutDocumentTypeID] [int] NOT NULL,
[InEventTypeID] [int] NOT NULL,
[InLeadEventID] [int] NULL,
[DSPTypeID] [int] NOT NULL,
[DSPAccountID] [int] NOT NULL,
[DSPUserID] [int] NOT NULL,
[DSPSignatureTemplate] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[DSPBrandingKey] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DSPEnvelopeKey] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DSPEnvelopeStatus] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[RequestXML] [xml] NULL,
[ResponseXML] [xml] NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[DeclinedOrVoidedEventTypeID] [int] NULL,
[DeclinedOrVoidedLeadEventID] [int] NULL
)
GO
ALTER TABLE [dbo].[DocumentSignProviderQueue] ADD CONSTRAINT [PK_DocumentSignProviderQueue] PRIMARY KEY CLUSTERED  ([DSPQueueID])
GO
