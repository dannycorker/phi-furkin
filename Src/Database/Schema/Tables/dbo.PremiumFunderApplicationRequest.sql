CREATE TABLE [dbo].[PremiumFunderApplicationRequest]
(
[PremiumFunderApplicationRequestID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NULL,
[MatterID] [int] NULL,
[RequestJson] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[PremiumFunderRequestID] [int] NOT NULL,
[externalQuoteReference] [nvarchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[PremiumFunderApplicationRequest] ADD CONSTRAINT [PK_PremiumFunderApplicationRequest] PRIMARY KEY CLUSTERED  ([PremiumFunderApplicationRequestID])
GO
