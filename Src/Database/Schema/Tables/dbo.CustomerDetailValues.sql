CREATE TABLE [dbo].[CustomerDetailValues]
(
[CustomerDetailValueID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[DetailValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[ErrorMsg] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[EncryptedValue] [varchar] (3000) COLLATE Latin1_General_CI_AS NULL,
[ValueInt] [int] NULL,
[ValueMoney] [money] NULL,
[ValueDate] [date] NULL,
[ValueDateTime] [datetime2] (0) NULL,
[SourceID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2010-08-15
-- Description:	Store DetailValue as specific datatypes
-- 2011-07-13 JWG Remove duplicates instantly at source
-- =============================================
CREATE TRIGGER [dbo].[trgiu_CustomerDetailValues]
   ON [dbo].[CustomerDetailValues]
   AFTER INSERT, UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END


    /*
		If the DetailValue column has been updated then fire the trigger,
		otherwise we are here again because the trigger has just fired,
		in which case do nothing.
	*/
    IF UPDATE(DetailValue)
    BEGIN
		/* Check for duplicates (but only on insert, not update) */
		IF NOT EXISTS (SELECT * FROM deleted)
		BEGIN
			/* Only issue the delete if we know that any records exist, otherwise the delete operation gets attempted every time regardless */
			IF EXISTS(SELECT * FROM dbo.CustomerDetailValues dv WITH (NOLOCK) INNER JOIN inserted i ON i.CustomerID = dv.CustomerID AND i.DetailFieldID = dv.DetailFieldID AND i.CustomerDetailValueID > dv.CustomerDetailValueID )
			BEGIN
				/* Remove any older duplicates for this field */
				DELETE dbo.CustomerDetailValues
				OUTPUT 10, deleted.CustomerDetailValueID, deleted.CustomerID, deleted.DetailValue, deleted.DetailFieldID
				INTO dbo.__DeletedValue (DetailFieldSubTypeID, DetailValueID, ParentID, DetailValue, DetailFieldID)
				FROM dbo.CustomerDetailValues dv
				INNER JOIN inserted i ON i.CustomerID = dv.CustomerID AND i.DetailFieldID = dv.DetailFieldID AND i.CustomerDetailValueID > dv.CustomerDetailValueID
			END
		END

		/* Populate ValueInt et al */
		UPDATE dbo.CustomerDetailValues
		SET ValueInt = dbo.fnValueAsIntBitsIncluded(i.DetailValue) /*CASE WHEN dbo.fnIsInt(i.detailvalue) = 1 THEN convert(int, i.DetailValue) ELSE NULL END*/,
		ValueMoney = CASE WHEN dbo.fnIsMoney(i.DetailValue) = 1 THEN convert(money, i.DetailValue) ELSE NULL END,
		ValueDate = CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN convert(date, i.DetailValue) ELSE NULL END,
		ValueDateTime = CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN convert(datetime2, i.DetailValue) ELSE NULL END
		FROM dbo.CustomerDetailValues dv
		INNER JOIN inserted i ON i.CustomerDetailValueID = dv.CustomerDetailValueID
	END


END


GO
ALTER TABLE [dbo].[CustomerDetailValues] ADD CONSTRAINT [PK_CustomerDetailValues] PRIMARY KEY CLUSTERED  ([CustomerDetailValueID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerDetailValues_Client] ON [dbo].[CustomerDetailValues] ([ClientID]) INCLUDE ([CustomerID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerDetailValues_CustomerAndField_Client] ON [dbo].[CustomerDetailValues] ([CustomerID], [DetailFieldID], [ClientID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_CustomerDetailValues_CustomerFieldValueInt] ON [dbo].[CustomerDetailValues] ([CustomerID], [DetailFieldID], [ValueInt], [ClientID]) WHERE ([ValueInt] IS NOT NULL)
GO
CREATE NONCLUSTERED INDEX [IX_CustomerDetailValuesDetailField] ON [dbo].[CustomerDetailValues] ([DetailFieldID]) INCLUDE ([CustomerID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_CustomerDetailValues_FieldValueDate] ON [dbo].[CustomerDetailValues] ([DetailFieldID], [ValueDate], [ClientID]) INCLUDE ([CustomerID]) WHERE ([ValueDate] IS NOT NULL)
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_CustomerDetailValues_FieldValueInt] ON [dbo].[CustomerDetailValues] ([DetailFieldID], [ValueInt], [ClientID]) INCLUDE ([CustomerID]) WHERE ([ValueInt] IS NOT NULL)
GO
ALTER TABLE [dbo].[CustomerDetailValues] ADD CONSTRAINT [FK_CustomerDetailValues_Customer] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[CustomerDetailValues] ADD CONSTRAINT [FK_CustomerDetailValues_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
