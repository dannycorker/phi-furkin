CREATE TABLE [dbo].[ClientUsage]
(
[ClientID] [int] NOT NULL,
[Validity] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[ValidityHash] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Usage] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[UsageHash] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ClientUsage] ADD CONSTRAINT [PK_ClientUsage] PRIMARY KEY CLUSTERED  ([ClientID])
GO
