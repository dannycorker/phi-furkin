CREATE TABLE [dbo].[LeadInfoCustomSQL]
(
[LeadInfoCustomSQLID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SQLQueryID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[ClientPersonnelAdminGroupID] [int] NULL,
[ViewOrder] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[LeadInfoCustomSQL] ADD CONSTRAINT [PK_LeadInfoCustomSQL] PRIMARY KEY CLUSTERED  ([LeadInfoCustomSQLID])
GO
