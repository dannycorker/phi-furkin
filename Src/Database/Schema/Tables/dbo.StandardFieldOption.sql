CREATE TABLE [dbo].[StandardFieldOption]
(
[StandardFieldOptionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[TableName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[ColumnName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[ColumnCaption] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Mandatory] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[StandardFieldOption] ADD CONSTRAINT [PK_StandardFieldOption] PRIMARY KEY CLUSTERED  ([StandardFieldOptionID])
GO
