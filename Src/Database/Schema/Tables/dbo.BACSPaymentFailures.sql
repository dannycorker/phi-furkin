CREATE TABLE [dbo].[BACSPaymentFailures]
(
[PaymentFailureID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[FileName] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[FailureFileType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[RowNum] [int] NULL,
[AccountNumber] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[SortCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PaymentAmount] [numeric] (18, 2) NULL,
[BACSProcessedDate] [datetime] NULL,
[FailureReason] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[FailureCode] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Reference] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[AccountID] [int] NULL,
[PaymentID] [int] NULL,
[ImportStatusID] [int] NULL,
[ImportedDate] [datetime] NULL,
[EventToApply] [int] NULL,
[LeadEventID] [int] NULL,
[CaseID] [int] NULL,
[Comments] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[BACSPaymentFailures] ADD CONSTRAINT [PK_BACSPaymentFailures] PRIMARY KEY CLUSTERED  ([PaymentFailureID])
GO
