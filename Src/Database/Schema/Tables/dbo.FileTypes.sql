CREATE TABLE [dbo].[FileTypes]
(
[FileTypeID] [int] NOT NULL IDENTITY(1, 1),
[FileTypeName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[FileTypes] ADD CONSTRAINT [PK_FileTypes] PRIMARY KEY CLUSTERED  ([FileTypeID])
GO
