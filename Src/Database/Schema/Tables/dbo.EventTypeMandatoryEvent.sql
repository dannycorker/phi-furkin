CREATE TABLE [dbo].[EventTypeMandatoryEvent]
(
[EventTypeMandatoryEventID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[ToEventTypeID] [int] NOT NULL,
[RequiredEventID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[EventTypeMandatoryEvent] ADD CONSTRAINT [PK_EventTypeMandatoryEvent] PRIMARY KEY CLUSTERED  ([EventTypeMandatoryEventID])
GO
ALTER TABLE [dbo].[EventTypeMandatoryEvent] ADD CONSTRAINT [FK_EventTypeMandatoryEvent_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EventTypeMandatoryEvent] ADD CONSTRAINT [FK_EventTypeMandatoryEvent_EventType_RequiredEvent] FOREIGN KEY ([RequiredEventID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[EventTypeMandatoryEvent] ADD CONSTRAINT [FK_EventTypeMandatoryEvent_EventType_ToEvent] FOREIGN KEY ([ToEventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[EventTypeMandatoryEvent] ADD CONSTRAINT [FK_EventTypeMandatoryEvent_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
