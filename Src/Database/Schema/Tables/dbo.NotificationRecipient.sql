CREATE TABLE [dbo].[NotificationRecipient]
(
[NotificationRecipientID] [int] NOT NULL IDENTITY(1, 1),
[NotificationID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[ClientPersonnelID] [int] NOT NULL,
[NotificationStatusID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[NotificationRecipient] ADD CONSTRAINT [PK_NotificationRecipient] PRIMARY KEY CLUSTERED  ([NotificationRecipientID])
GO
ALTER TABLE [dbo].[NotificationRecipient] ADD CONSTRAINT [FK_NotificationRecipient_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[NotificationRecipient] ADD CONSTRAINT [FK_NotificationRecipient_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[NotificationRecipient] ADD CONSTRAINT [FK_NotificationRecipient_Notification] FOREIGN KEY ([NotificationID]) REFERENCES [dbo].[Notification] ([NotificationID])
GO
ALTER TABLE [dbo].[NotificationRecipient] ADD CONSTRAINT [FK_NotificationRecipient_NotificationStatus] FOREIGN KEY ([NotificationStatusID]) REFERENCES [dbo].[NotificationStatus] ([NotificationStatusID])
GO
ALTER TABLE [dbo].[NotificationRecipient] ADD CONSTRAINT [FK_NotificationRecipient_SubClient] FOREIGN KEY ([SubClientID]) REFERENCES [dbo].[SubClient] ([SubClientID])
GO
