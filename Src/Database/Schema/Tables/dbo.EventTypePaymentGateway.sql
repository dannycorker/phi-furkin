CREATE TABLE [dbo].[EventTypePaymentGateway]
(
[EventTypePaymentGatewayID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[ClientPaymentGatewayID] [int] NOT NULL,
[TransactionTypeID] [int] NOT NULL,
[CardMaskingMethodID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[EventTypePaymentGateway] ADD CONSTRAINT [PK_EventTypePaymentGateway] PRIMARY KEY CLUSTERED  ([EventTypePaymentGatewayID])
GO
ALTER TABLE [dbo].[EventTypePaymentGateway] ADD CONSTRAINT [FK_EventTypePaymentGateway_ClientPaymentGateway] FOREIGN KEY ([ClientPaymentGatewayID]) REFERENCES [dbo].[ClientPaymentGateway] ([ClientPaymentGatewayID])
GO
ALTER TABLE [dbo].[EventTypePaymentGateway] ADD CONSTRAINT [FK_EventTypePaymentGateway_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EventTypePaymentGateway] ADD CONSTRAINT [FK_EventTypePaymentGateway_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[EventTypePaymentGateway] ADD CONSTRAINT [FK_EventTypePaymentGateway_EventTypePaymentGateway] FOREIGN KEY ([EventTypePaymentGatewayID]) REFERENCES [dbo].[EventTypePaymentGateway] ([EventTypePaymentGatewayID])
GO
