CREATE TABLE [dbo].[SqlQueryColumnTypeMapping]
(
[SqlQueryColumnTypeMappingID] [int] NOT NULL IDENTITY(1, 1),
[SqlQueryType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[SqlQueryReportType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[SqlQueryColumnTypeMapping] ADD CONSTRAINT [PK_SqlQueryColumnTypeMapping] PRIMARY KEY CLUSTERED  ([SqlQueryColumnTypeMappingID])
GO
