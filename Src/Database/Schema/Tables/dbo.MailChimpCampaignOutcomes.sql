CREATE TABLE [dbo].[MailChimpCampaignOutcomes]
(
[MailChimpCampaignOutcomeID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EmailAddress] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[CampainID] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[Opened] [int] NULL,
[Clicked] [int] NULL,
[Bounced] [int] NULL,
[UnsubscribeReason] [varchar] (200) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[MailChimpCampaignOutcomes] ADD CONSTRAINT [PK_MailChimpCampaignOutcomes] PRIMARY KEY CLUSTERED  ([MailChimpCampaignOutcomeID])
GO
