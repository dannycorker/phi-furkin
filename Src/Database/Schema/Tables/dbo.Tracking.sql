CREATE TABLE [dbo].[Tracking]
(
[TrackingID] [int] NOT NULL IDENTITY(1, 1),
[TrackingName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[TrackingDescription] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[ClientQuestionnaireID] [int] NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[Tracking] ADD CONSTRAINT [PK_Campaigns] PRIMARY KEY CLUSTERED  ([TrackingID])
GO
ALTER TABLE [dbo].[Tracking] ADD CONSTRAINT [FK_Tracking_ClientQuestionnaires] FOREIGN KEY ([ClientQuestionnaireID]) REFERENCES [dbo].[ClientQuestionnaires] ([ClientQuestionnaireID])
GO
