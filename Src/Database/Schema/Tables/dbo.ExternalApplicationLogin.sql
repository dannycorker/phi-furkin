CREATE TABLE [dbo].[ExternalApplicationLogin]
(
[ExternalApplicationLoginID] [int] NOT NULL IDENTITY(1, 1),
[ThirdPartySystemID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[UserName] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Password] [varchar] (65) COLLATE Latin1_General_CI_AS NOT NULL,
[Salt] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ClientPersonnelID] [int] NOT NULL,
[ClientPersonnelAdminGroupID] [int] NOT NULL,
[AttemptedLogins] [int] NOT NULL CONSTRAINT [DF__ExternalA__Attem__68687968] DEFAULT ((0)),
[AccountDisabled] [bit] NOT NULL,
[CustomerID] [int] NULL
)
GO
ALTER TABLE [dbo].[ExternalApplicationLogin] ADD CONSTRAINT [PK_ExternalApplicationLogin] PRIMARY KEY CLUSTERED  ([ExternalApplicationLoginID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ExternalApplicationLogin_UserName_ThirdPartySystemId] ON [dbo].[ExternalApplicationLogin] ([UserName], [ThirdPartySystemID])
GO
ALTER TABLE [dbo].[ExternalApplicationLogin] ADD CONSTRAINT [FK_ExternalApplicationLogin_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[ExternalApplicationLogin] ADD CONSTRAINT [FK_ExternalApplicationLogin_ClientPersonnelAdminGroups] FOREIGN KEY ([ClientPersonnelAdminGroupID]) REFERENCES [dbo].[ClientPersonnelAdminGroups] ([ClientPersonnelAdminGroupID])
GO
ALTER TABLE [dbo].[ExternalApplicationLogin] ADD CONSTRAINT [FK_ExternalApplicationLogin_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ExternalApplicationLogin] ADD CONSTRAINT [FK_ExternalApplicationLogin_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[ExternalApplicationLogin] ADD CONSTRAINT [FK_ExternalApplicationLogin_ExternalApplicationLogin] FOREIGN KEY ([ExternalApplicationLoginID]) REFERENCES [dbo].[ExternalApplicationLogin] ([ExternalApplicationLoginID])
GO
ALTER TABLE [dbo].[ExternalApplicationLogin] ADD CONSTRAINT [FK_ExternalApplicationLogin_SubClient] FOREIGN KEY ([SubClientID]) REFERENCES [dbo].[SubClient] ([SubClientID])
GO
ALTER TABLE [dbo].[ExternalApplicationLogin] ADD CONSTRAINT [FK_ExternalApplicationLogin_ThirdPartySystem] FOREIGN KEY ([ThirdPartySystemID]) REFERENCES [dbo].[ThirdPartySystem] ([ThirdPartySystemId])
GO
