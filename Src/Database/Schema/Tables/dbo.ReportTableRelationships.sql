CREATE TABLE [dbo].[ReportTableRelationships]
(
[ReportTableRelationshipID] [int] NOT NULL IDENTITY(1, 1),
[TableFrom] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[TableTo] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[TableJoin] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[ReportTableRelationships] ADD CONSTRAINT [PK_ReportTableRelationships] PRIMARY KEY CLUSTERED  ([ReportTableRelationshipID])
GO
