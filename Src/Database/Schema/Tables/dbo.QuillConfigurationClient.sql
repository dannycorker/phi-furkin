CREATE TABLE [dbo].[QuillConfigurationClient]
(
[QuillConfigurationClientID] [int] NOT NULL IDENTITY(1, 1),
[AquariumClientID] [int] NOT NULL,
[QuillClientID] [int] NOT NULL,
[Username] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Password] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[URI] [varchar] (150) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[QuillConfigurationClient] ADD CONSTRAINT [PK_QuillConfigurationClient] PRIMARY KEY CLUSTERED  ([QuillConfigurationClientID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_QuillConfigurationClient] ON [dbo].[QuillConfigurationClient] ([AquariumClientID])
GO
ALTER TABLE [dbo].[QuillConfigurationClient] ADD CONSTRAINT [FK_QuillConfigurationClient_Clients] FOREIGN KEY ([AquariumClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
