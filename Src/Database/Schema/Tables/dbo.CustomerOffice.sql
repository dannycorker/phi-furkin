CREATE TABLE [dbo].[CustomerOffice]
(
[OfficeID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[OfficeName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[MainPhoneNumber] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[FaxNumber] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Address1] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Address2] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Town] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[County] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Postcode] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Country] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Notes] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[CountryID] [int] NULL CONSTRAINT [DF__CustomerO__Count__4BCC3ABA] DEFAULT ((232))
)
GO
ALTER TABLE [dbo].[CustomerOffice] ADD CONSTRAINT [PK_CustomerOffice] PRIMARY KEY CLUSTERED  ([OfficeID])
GO
ALTER TABLE [dbo].[CustomerOffice] ADD CONSTRAINT [FK_CustomerOffice_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[CustomerOffice] ADD CONSTRAINT [FK_CustomerOffice_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
ALTER TABLE [dbo].[CustomerOffice] ADD CONSTRAINT [FK_CustomerOffice_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
