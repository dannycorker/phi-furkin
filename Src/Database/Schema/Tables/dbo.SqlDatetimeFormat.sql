CREATE TABLE [dbo].[SqlDatetimeFormat]
(
[SqlDatetimeFormatID] [tinyint] NOT NULL,
[DisplayOrder] [tinyint] NOT NULL,
[Description] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[IsPreferred] [bit] NOT NULL,
[DisplayFormat] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[SqlDatetimeFormat] ADD CONSTRAINT [PK_SqlDatetimeFormat] PRIMARY KEY CLUSTERED  ([SqlDatetimeFormatID])
GO
