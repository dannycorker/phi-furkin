CREATE TABLE [dbo].[LookupList]
(
[LookupListID] [int] NOT NULL IDENTITY(1, 1),
[LookupListName] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[LookupListDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL,
[Enabled] [bit] NOT NULL,
[SortOptionID] [int] NOT NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[LeadTypeID] [int] NULL,
[IsShared] [bit] NULL
)
GO
ALTER TABLE [dbo].[LookupList] ADD CONSTRAINT [PK_LookupList] PRIMARY KEY CLUSTERED  ([LookupListID])
GO
CREATE NONCLUSTERED INDEX [IX_LookupListClient] ON [dbo].[LookupList] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_LookupList_SourceID] ON [dbo].[LookupList] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[LookupList] ADD CONSTRAINT [FK_LookupList_AquariumOption] FOREIGN KEY ([SortOptionID]) REFERENCES [dbo].[AquariumOption] ([AquariumOptionID])
GO
ALTER TABLE [dbo].[LookupList] ADD CONSTRAINT [FK_LookupList_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
