CREATE TABLE [dbo].[DetailFieldPages]
(
[DetailFieldPageID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadOrMatter] [tinyint] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[PageName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PageCaption] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PageOrder] [int] NOT NULL,
[Enabled] [bit] NOT NULL,
[ResourceList] [bit] NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL,
[IsShared] [bit] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2009-04-06
-- Description:	Capture changes and store in History table
-- =============================================
CREATE TRIGGER [dbo].[trgiud_DetailFieldPages]
   ON  [dbo].[DetailFieldPages]
   AFTER INSERT,DELETE,UPDATE
AS
BEGIN
	SET NOCOUNT ON;

    -- Inserts appear in the inserted table,
    -- Deletes appear in the deleted table,
    -- and Updates appear in both tables at once identically.
    -- These are all 'logical' Sql Server tables.
	INSERT INTO dbo.DetailFieldPagesHistory(
		DetailFieldPageID,
		ClientID,
		LeadOrMatter,
		LeadTypeID,
		PageName,
		PageCaption,
		PageOrder,
		[Enabled],
		ResourceList,
		WhoChanged,
		WhenChanged,
		ChangeAction,
		ChangeNotes
	)
	SELECT
		COALESCE(i.DetailFieldPageID, d.DetailFieldPageID),
		COALESCE(i.ClientID, d.ClientID),
		COALESCE(i.LeadOrMatter, d.LeadOrMatter),
		COALESCE(i.LeadTypeID, d.LeadTypeID),
		COALESCE(i.PageName, d.PageName),
		COALESCE(i.PageCaption, d.PageCaption),
		COALESCE(i.PageOrder, d.PageOrder),
		COALESCE(i.[Enabled], d.[Enabled]),
		COALESCE(i.ResourceList, d.ResourceList),
		COALESCE(i.WhoModified, d.WhoModified),
		dbo.fn_GetDate_Local(),
		CASE WHEN d.DetailFieldPageID IS NULL THEN 'Insert' WHEN i.DetailFieldPageID IS NULL THEN 'Delete' ELSE 'Update' END,
		dbo.fnGetUserInfo()
	FROM inserted i
	FULL OUTER JOIN deleted d ON i.DetailFieldPageID = d.DetailFieldPageID

END
GO
ALTER TABLE [dbo].[DetailFieldPages] ADD CONSTRAINT [PK_DetailFieldPages] PRIMARY KEY CLUSTERED  ([DetailFieldPageID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_DetailFieldPages_SourceID] ON [dbo].[DetailFieldPages] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[DetailFieldPages] ADD CONSTRAINT [FK_DetailFieldPages_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DetailFieldPages] ADD CONSTRAINT [FK_DetailFieldPages_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID]) ON DELETE CASCADE
GO
