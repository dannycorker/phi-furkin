CREATE TABLE [dbo].[RulesEngine_Rules]
(
[RuleID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[RuleSetID] [int] NOT NULL,
[RuleOrder] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[Description] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[SourceID] [int] NULL,
[RuleCheckpoint] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[RuleValidFrom] [datetime] NULL,
[RuleValidTo] [datetime] NULL
)
GO
ALTER TABLE [dbo].[RulesEngine_Rules] ADD CONSTRAINT [PK_RulesEngine_Rules] PRIMARY KEY CLUSTERED  ([RuleID])
GO
CREATE NONCLUSTERED INDEX [IX_RulesEngine_Rules_RuleSetID] ON [dbo].[RulesEngine_Rules] ([RuleSetID])
GO
ALTER TABLE [dbo].[RulesEngine_Rules] ADD CONSTRAINT [FK_RulesEngine_Rules_RulesEngine_RuleSets] FOREIGN KEY ([RuleSetID]) REFERENCES [dbo].[RulesEngine_RuleSets] ([RuleSetID])
GO
