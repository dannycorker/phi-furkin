CREATE TABLE [dbo].[DataLoaderKeywordMatchType]
(
[DataLoaderKeywordMatchTypeID] [int] NOT NULL IDENTITY(1, 1),
[MatchTypeName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[MatchTypeDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[DataLoaderKeywordMatchType] ADD CONSTRAINT [PK_DataLoaderKeywordMatchType] PRIMARY KEY CLUSTERED  ([DataLoaderKeywordMatchTypeID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DataLoaderKeywordMatchType] ON [dbo].[DataLoaderKeywordMatchType] ([DataLoaderKeywordMatchTypeID])
GO
