CREATE TABLE [dbo].[RulesEngine_Transforms]
(
[TransformID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[RulesEngine_Transforms] ADD CONSTRAINT [PK_RulesEngine_Transforms] PRIMARY KEY CLUSTERED  ([TransformID])
GO
