CREATE TABLE [dbo].[LeadEventLog]
(
[LeadEventLogID] [int] NOT NULL IDENTITY(1, 1),
[LeadEventID] [int] NULL,
[EventTypeID] [int] NULL,
[LastUpdated] [datetime] NOT NULL,
[WhenFollowedUp] [datetime] NULL,
[NextEventID] [int] NULL,
[StepNumber] [int] NOT NULL,
[Notes] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[TriggerGUID] [uniqueidentifier] NOT NULL
)
GO
ALTER TABLE [dbo].[LeadEventLog] ADD CONSTRAINT [PK_LeadEventLog] PRIMARY KEY CLUSTERED  ([LeadEventLogID])
GO
