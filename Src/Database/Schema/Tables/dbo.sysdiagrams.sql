CREATE TABLE [dbo].[sysdiagrams]
(
[name] [sys].[sysname] NOT NULL,
[principal_id] [int] NOT NULL,
[diagram_id] [int] NOT NULL IDENTITY(1, 1),
[version] [int] NULL,
[definition] [varbinary] (max) NULL
)
GO
