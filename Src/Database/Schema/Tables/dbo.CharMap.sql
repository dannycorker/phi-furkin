CREATE TABLE [dbo].[CharMap]
(
[AsciiValue] [tinyint] NOT NULL,
[CharValue] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[CharName] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[CharDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[HtmlEncoding] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[IsAlpha] [bit] NOT NULL,
[IsAlphaNumeric] [bit] NOT NULL,
[IsMathematical] [bit] NOT NULL,
[IsNumeric] [bit] NOT NULL,
[IsPrintable] [bit] NOT NULL,
[IsValidFileName] [bit] NOT NULL,
[IsValidPath] [bit] NOT NULL,
[RTFEscapeCode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[CharMap] ADD CONSTRAINT [PK_CharMap] PRIMARY KEY CLUSTERED  ([AsciiValue])
GO
CREATE NONCLUSTERED INDEX [IX_CharMap_AsciiValueAndAllFlags] ON [dbo].[CharMap] ([AsciiValue])
GO
CREATE NONCLUSTERED INDEX [IX_CharMap_CharValueAndAllFlags] ON [dbo].[CharMap] ([CharValue])
GO
CREATE NONCLUSTERED INDEX [IX_CharMap_Alpha] ON [dbo].[CharMap] ([IsAlpha])
GO
CREATE NONCLUSTERED INDEX [IX_CharMap_AlphaNumeric] ON [dbo].[CharMap] ([IsAlphaNumeric])
GO
CREATE NONCLUSTERED INDEX [IX_CharMap_Numeric] ON [dbo].[CharMap] ([IsNumeric])
GO
CREATE NONCLUSTERED INDEX [IX_CharMap_ValidFileName] ON [dbo].[CharMap] ([IsValidFileName])
GO
