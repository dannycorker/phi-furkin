CREATE TABLE [dbo].[__CustomerAddressCheck]
(
[CustomerID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[HouseNumber] [int] NULL,
[Address1] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Address2] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[Town] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[County] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[PostCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[IsBusiness] [bit] NULL,
[PAFAddress1] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[PAFAddress2] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[PAFTown] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PAFNotFound] [bit] NULL,
[PAFMultiples] [bit] NULL,
[PAFAddress1Different] [bit] NULL,
[PAFAddress2Different] [bit] NULL,
[PAFTownDifferent] [bit] NULL,
[PAFMatch] [bit] NULL,
[PAFPostCodeNotFound] [bit] NULL,
[WhenCreated] [datetime] NULL,
[WhenChecked] [datetime] NULL
)
GO
ALTER TABLE [dbo].[__CustomerAddressCheck] ADD CONSTRAINT [PK___CustomerAddressCheck] PRIMARY KEY CLUSTERED  ([CustomerID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerAddressCheck_Client] ON [dbo].[__CustomerAddressCheck] ([ClientID])
GO
