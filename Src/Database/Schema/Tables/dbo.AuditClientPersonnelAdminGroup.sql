CREATE TABLE [dbo].[AuditClientPersonnelAdminGroup]
(
[AuditClientPersonnelAdminGroupID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelAdminGroupID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime2] (0) NOT NULL,
[IsInsert] [bit] NOT NULL,
[GroupName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ChangeNotes] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[AuditClientPersonnelAdminGroup] ADD CONSTRAINT [PK_AuditClientPersonnelAdminGroup] PRIMARY KEY CLUSTERED  ([AuditClientPersonnelAdminGroupID])
GO
ALTER TABLE [dbo].[AuditClientPersonnelAdminGroup] ADD CONSTRAINT [FK_AuditClientPersonnelAdminGroup_ClientPersonnelAdminGroup] FOREIGN KEY ([ClientPersonnelAdminGroupID]) REFERENCES [dbo].[ClientPersonnelAdminGroups] ([ClientPersonnelAdminGroupID])
GO
ALTER TABLE [dbo].[AuditClientPersonnelAdminGroup] ADD CONSTRAINT [FK_AuditClientPersonnelAdminGroup_WhoModified] FOREIGN KEY ([WhoModified]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
