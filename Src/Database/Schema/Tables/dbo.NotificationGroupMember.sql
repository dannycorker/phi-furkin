CREATE TABLE [dbo].[NotificationGroupMember]
(
[NotificationGroupMemberID] [int] NOT NULL IDENTITY(1, 1),
[NotificationGroupID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[ClientPersonnelID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[NotificationGroupMember] ADD CONSTRAINT [PK__Notifica__C866C82F7C5BC002] PRIMARY KEY CLUSTERED  ([NotificationGroupMemberID])
GO
ALTER TABLE [dbo].[NotificationGroupMember] ADD CONSTRAINT [FK__Notificat__Clien__002C50E6] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[NotificationGroupMember] ADD CONSTRAINT [FK__Notificat__Clien__7F382CAD] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[NotificationGroupMember] ADD CONSTRAINT [FK__Notificat__Notif__7E440874] FOREIGN KEY ([NotificationGroupID]) REFERENCES [dbo].[NotificationGroup] ([NotificationGroupID])
GO
