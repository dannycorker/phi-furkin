CREATE TABLE [dbo].[ClientStyle]
(
[ClientStyleID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[StyleRuleID] [int] NOT NULL,
[StyleValue] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ClientStyle] ADD CONSTRAINT [PK_ClientStyle] PRIMARY KEY CLUSTERED  ([ClientStyleID])
GO
ALTER TABLE [dbo].[ClientStyle] ADD CONSTRAINT [FK_ClientStyle_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[ClientStyle] ADD CONSTRAINT [FK_ClientStyle_StyleRule] FOREIGN KEY ([StyleRuleID]) REFERENCES [dbo].[StyleRule] ([StyleRuleID])
GO
