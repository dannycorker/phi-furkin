CREATE TABLE [dbo].[CustomerQuestionnaires]
(
[CustomerQuestionnaireID] [int] NOT NULL IDENTITY(1, 1),
[ClientQuestionnaireID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[SubmissionDate] [datetime] NOT NULL,
[TrackingID] [int] NULL,
[Referrer] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[SearchTerms] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[CustomerQuestionnaires] ADD CONSTRAINT [PK_CustomerQuestionnaires] PRIMARY KEY CLUSTERED  ([CustomerQuestionnaireID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerQuestionnairesClientID] ON [dbo].[CustomerQuestionnaires] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerQuestionnairesClientQuestionnaireID] ON [dbo].[CustomerQuestionnaires] ([ClientQuestionnaireID])
GO
CREATE NONCLUSTERED INDEX [IX_CustomerQuestionnairesCustomerID] ON [dbo].[CustomerQuestionnaires] ([CustomerID])
GO
ALTER TABLE [dbo].[CustomerQuestionnaires] ADD CONSTRAINT [FK_CustomerQuestionnaires_ClientQuestionnaires] FOREIGN KEY ([ClientQuestionnaireID]) REFERENCES [dbo].[ClientQuestionnaires] ([ClientQuestionnaireID])
GO
ALTER TABLE [dbo].[CustomerQuestionnaires] ADD CONSTRAINT [FK_CustomerQuestionnaires_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[CustomerQuestionnaires] ADD CONSTRAINT [FK_CustomerQuestionnaires_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
