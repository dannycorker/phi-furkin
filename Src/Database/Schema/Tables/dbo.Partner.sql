CREATE TABLE [dbo].[Partner]
(
[PartnerID] [int] NOT NULL IDENTITY(1, 1),
[CustomerID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[UseCustomerAddress] [bit] NOT NULL CONSTRAINT [DF__Partner__UseCust__7B7B4DDC] DEFAULT ((1)),
[TitleID] [int] NULL,
[FirstName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[MiddleName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[EmailAddress] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[DayTimeTelephoneNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[HomeTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[MobileTelephone] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Address1] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Address2] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[Town] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[County] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[PostCode] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Occupation] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Employer] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[DateOfBirth] [datetime] NULL,
[FullName] AS (([FirstName]+' ')+[LastName]),
[CountryID] [int] NULL CONSTRAINT [DF__Partner__Country__7A8729A3] DEFAULT ((232)),
[WhenModified] [datetime] NULL,
[WhoModified] [int] NULL,
[Longitude] [numeric] (25, 18) NULL,
[Latitude] [numeric] (25, 18) NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow, Jim Green
-- Create date: 2015-01-14
-- Description:	Prevent duplicate Partner records being created (eg by SDK users)
-- =============================================
Create TRIGGER [dbo].[trgi_Partner]
   ON [dbo].[Partner]
   AFTER INSERT
AS
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END


	/* Check for duplicates (only on insert) */
	/* Only issue the delete if we know that any records exist, otherwise the delete operation gets attempted every time regardless */
	IF EXISTS(SELECT * FROM dbo.Partner p WITH (NOLOCK) INNER JOIN inserted i ON i.CustomerID = p.CustomerID AND i.PartnerID > p.PartnerID)
	BEGIN
		/* Remove any older duplicate Partners for this Customer */
		DELETE dbo.Partner 
		OUTPUT deleted.[PartnerID], deleted.[CustomerID], deleted.[ClientID], deleted.[UseCustomerAddress], deleted.[TitleID], deleted.[FirstName], deleted.[MiddleName], deleted.[LastName], deleted.[EmailAddress], deleted.[DayTimeTelephoneNumber], deleted.[HomeTelephone], deleted.[MobileTelephone], deleted.[Address1], deleted.[Address2], deleted.[Town], deleted.[County], deleted.[PostCode], deleted.[Occupation], deleted.[Employer], deleted.[DateOfBirth], deleted.[CountryID]
		INTO dbo.DeletedPartner(PartnerID, CustomerID, ClientID, UseCustomerAddress, TitleID, FirstName, MiddleName, LastName, EmailAddress, DayTimeTelephoneNumber, HomeTelephone, MobileTelephone, Address1, Address2, Town, County, PostCode, Occupation, Employer, DateOfBirth, CountryID)
		FROM dbo.Partner p
		INNER JOIN inserted i ON i.CustomerID = p.CustomerID AND i.PartnerID > p.PartnerID
	END


END

GO
ALTER TABLE [dbo].[Partner] ADD CONSTRAINT [PK_Partner] PRIMARY KEY CLUSTERED  ([PartnerID])
GO
CREATE NONCLUSTERED INDEX [IX_PartnerClient] ON [dbo].[Partner] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_Partner_Customer] ON [dbo].[Partner] ([CustomerID], [ClientID]) INCLUDE ([LastName], [PostCode])
GO
CREATE NONCLUSTERED INDEX [IX_PartnerEmail] ON [dbo].[Partner] ([EmailAddress])
GO
CREATE NONCLUSTERED INDEX [IX_PartnerLastName] ON [dbo].[Partner] ([LastName]) INCLUDE ([ClientID], [CustomerID])
GO
CREATE NONCLUSTERED INDEX [IX_PartnerPostcode] ON [dbo].[Partner] ([PostCode])
GO
ALTER TABLE [dbo].[Partner] ADD CONSTRAINT [FK_Partner_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[Partner] ADD CONSTRAINT [FK_Partner_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
ALTER TABLE [dbo].[Partner] ADD CONSTRAINT [FK_Partner_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
