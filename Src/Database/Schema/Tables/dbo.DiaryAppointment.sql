CREATE TABLE [dbo].[DiaryAppointment]
(
[DiaryAppointmentID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DiaryAppointmentTitle] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[DiaryAppointmentText] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[CreatedBy] [int] NOT NULL,
[DueDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[AllDayEvent] [bit] NOT NULL,
[Completed] [bit] NOT NULL,
[LeadID] [int] NULL,
[CaseID] [int] NULL,
[CustomerID] [int] NULL,
[Version] [int] NOT NULL CONSTRAINT [DF__DiaryAppo__Versi__546180BB] DEFAULT ((1)),
[ExportVersion] [int] NOT NULL,
[RecurrenceInfo] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[DiaryAppointmentEventType] [int] NULL,
[ResourceInfo] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[TempReminderTimeshiftID] [int] NULL,
[StatusID] [int] NULL,
[LabelID] [int] NULL
)
GO
ALTER TABLE [dbo].[DiaryAppointment] ADD CONSTRAINT [PK_DiaryAppointment] PRIMARY KEY CLUSTERED  ([DiaryAppointmentID])
GO
CREATE NONCLUSTERED INDEX [IX_DiaryAppointment_Case] ON [dbo].[DiaryAppointment] ([CaseID], [ClientID])
GO
ALTER TABLE [dbo].[DiaryAppointment] ADD CONSTRAINT [FK_DiaryAppointment_Cases] FOREIGN KEY ([CaseID]) REFERENCES [dbo].[Cases] ([CaseID])
GO
ALTER TABLE [dbo].[DiaryAppointment] ADD CONSTRAINT [FK_DiaryAppointment_ClientPersonnel] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[DiaryAppointment] ADD CONSTRAINT [FK_DiaryAppointment_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DiaryAppointment] ADD CONSTRAINT [FK_DiaryAppointment_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
ALTER TABLE [dbo].[DiaryAppointment] ADD CONSTRAINT [FK_DiaryAppointment_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID]) ON DELETE CASCADE
GO
