CREATE TABLE [dbo].[CardTransactionPolicy]
(
[CardTransactionPolicyID] [int] NULL,
[CardTransactionID] [int] NOT NULL,
[PAMatterID] [int] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[PolicyPaymentAmount] [numeric] (18, 2) NULL
)
GO
