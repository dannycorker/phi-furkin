CREATE TABLE [dbo].[DetailFieldLink]
(
[DetailFieldLinkID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[MasterQuestionID] [int] NOT NULL,
[LeadTypeID] [int] NULL
)
GO
ALTER TABLE [dbo].[DetailFieldLink] ADD CONSTRAINT [PK_DetailFieldLink] PRIMARY KEY CLUSTERED  ([DetailFieldLinkID])
GO
ALTER TABLE [dbo].[DetailFieldLink] ADD CONSTRAINT [FK_DetailFieldLink_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DetailFieldLink] ADD CONSTRAINT [FK_DetailFieldLink_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[DetailFieldLink] ADD CONSTRAINT [FK_DetailFieldLink_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
ALTER TABLE [dbo].[DetailFieldLink] ADD CONSTRAINT [FK_DetailFieldLink_MasterQuestions] FOREIGN KEY ([MasterQuestionID]) REFERENCES [dbo].[MasterQuestions] ([MasterQuestionID])
GO
