CREATE TABLE [dbo].[ClientDataUsage]
(
[ClientDataUsageID] [int] NOT NULL IDENTITY(1, 1),
[ClientDataUsageBillingRunID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[DocumentBytes] [bigint] NULL,
[ClientImagesBytes] [bigint] NULL,
[StandardTablesBytes] [bigint] NULL,
[CustomTablesBytes] [bigint] NULL,
[TotalBytes] [bigint] NULL,
[UserNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ClientDataUsage] ADD CONSTRAINT [PK_ClientDataUsage] PRIMARY KEY CLUSTERED  ([ClientDataUsageID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientDataUsageBillingRun] ON [dbo].[ClientDataUsage] ([ClientDataUsageBillingRunID], [ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientDataUsageHistoryBillingRun] ON [dbo].[ClientDataUsage] ([ClientDataUsageBillingRunID], [ClientID])
GO
