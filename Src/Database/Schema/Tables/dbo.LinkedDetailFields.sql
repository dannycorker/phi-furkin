CREATE TABLE [dbo].[LinkedDetailFields]
(
[LinkedDetailFieldID] [int] NOT NULL IDENTITY(1, 1),
[LeadTypeIDTo] [int] NOT NULL,
[LeadTypeIDFrom] [int] NOT NULL,
[DetailFieldIDTo] [int] NULL,
[DetailFieldIDFrom] [int] NULL,
[DisplayOnPageID] [int] NULL,
[FieldOrder] [int] NULL,
[Enabled] [bit] NULL,
[History] [bit] NULL,
[LeadOrMatter] [int] NULL,
[FieldName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[LeadLinkedTo] [int] NULL,
[LeadLinkedFrom] [int] NULL,
[IncludeLinkedField] [bit] NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[LinkedDetailFields] ADD CONSTRAINT [PK_LinkedDetailFields] PRIMARY KEY CLUSTERED  ([LinkedDetailFieldID])
GO
ALTER TABLE [dbo].[LinkedDetailFields] ADD CONSTRAINT [FK_LinkedDetailFields_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
