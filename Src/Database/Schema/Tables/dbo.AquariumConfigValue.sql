CREATE TABLE [dbo].[AquariumConfigValue]
(
[ConfigValueID] [int] NOT NULL IDENTITY(1, 1),
[ConfigID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[SubClientID] [int] NOT NULL,
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Value] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ValueInt] [int] NULL,
[ValueMoney] [money] NULL,
[ValueDate] [date] NULL,
[ValueDateTime] [datetime] NULL,
[Salt] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[AquariumConfigValue] ADD CONSTRAINT [PK_AquariumConfigValue] PRIMARY KEY CLUSTERED  ([ConfigValueID])
GO
ALTER TABLE [dbo].[AquariumConfigValue] ADD CONSTRAINT [FK_AquariumConfigValue_AquariumConfig] FOREIGN KEY ([ConfigID]) REFERENCES [dbo].[AquariumConfig] ([ConfigID])
GO
ALTER TABLE [dbo].[AquariumConfigValue] ADD CONSTRAINT [FK_AquariumConfigValue_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[AquariumConfigValue] ADD CONSTRAINT [FK_AquariumConfigValue_SubClient] FOREIGN KEY ([SubClientID]) REFERENCES [dbo].[SubClient] ([SubClientID])
GO
