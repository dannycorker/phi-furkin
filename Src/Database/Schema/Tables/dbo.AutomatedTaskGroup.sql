CREATE TABLE [dbo].[AutomatedTaskGroup]
(
[AutomatedTaskGroupID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[IsEnabled] [bit] NOT NULL,
[AutomatedTaskGroupName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[AutomatedTaskGroupDescription] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[ExclusionGroupID] [int] NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[AutomatedTaskGroup] ADD CONSTRAINT [PK_AutomatedTaskGroup] PRIMARY KEY CLUSTERED  ([AutomatedTaskGroupID])
GO
CREATE NONCLUSTERED INDEX [IX_AutomatedTaskGroup] ON [dbo].[AutomatedTaskGroup] ([AutomatedTaskGroupID])
GO
CREATE NONCLUSTERED INDEX [IX_AutomatedTaskGroup_ClientIsEnabled] ON [dbo].[AutomatedTaskGroup] ([ClientID], [IsEnabled])
GO
CREATE NONCLUSTERED INDEX [IX_AutomatedTaskGroup_SourceID] ON [dbo].[AutomatedTaskGroup] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[AutomatedTaskGroup] ADD CONSTRAINT [FK_AutomatedTaskGroup_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[AutomatedTaskGroup] ADD CONSTRAINT [FK_AutomatedTaskGroup_ExclusionGroup] FOREIGN KEY ([ExclusionGroupID]) REFERENCES [dbo].[ExclusionGroup] ([ExclusionGroupID])
GO
