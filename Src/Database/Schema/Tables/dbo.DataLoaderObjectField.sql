CREATE TABLE [dbo].[DataLoaderObjectField]
(
[DataLoaderObjectFieldID] [int] NOT NULL,
[DataLoaderObjectTypeID] [int] NOT NULL,
[FieldName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[DataTypeID] [int] NOT NULL,
[IsMandatory] [bit] NOT NULL,
[IsNormalisedField] [bit] NOT NULL,
[IsABitField] [bit] NOT NULL,
[IsClientOwned] [bit] NOT NULL,
[IsUniqueTableID] [bit] NOT NULL,
[IsWriteOnce] [bit] NOT NULL,
[HelperText] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[DataLoaderObjectField] ADD CONSTRAINT [PK_DataLoaderObjectField] PRIMARY KEY CLUSTERED  ([DataLoaderObjectFieldID])
GO
ALTER TABLE [dbo].[DataLoaderObjectField] ADD CONSTRAINT [FK_DataLoaderObjectField_DataLoaderDataType] FOREIGN KEY ([DataTypeID]) REFERENCES [dbo].[DataLoaderDataType] ([DataLoaderDataTypeID])
GO
ALTER TABLE [dbo].[DataLoaderObjectField] ADD CONSTRAINT [FK_DataLoaderObjectField_DataLoaderObjectType] FOREIGN KEY ([DataLoaderObjectTypeID]) REFERENCES [dbo].[DataLoaderObjectType] ([DataLoaderObjectTypeID])
GO
