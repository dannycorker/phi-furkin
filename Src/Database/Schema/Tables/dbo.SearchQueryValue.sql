CREATE TABLE [dbo].[SearchQueryValue]
(
[SearchQueryValueID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[SearchQueryID] [int] NOT NULL,
[SearchFieldID] [int] NOT NULL,
[SearchFieldOperatorID] [int] NOT NULL,
[FieldValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[FieldValueID] [int] NULL
)
GO
ALTER TABLE [dbo].[SearchQueryValue] ADD CONSTRAINT [PK_SearchFieldQueryValue] PRIMARY KEY CLUSTERED  ([SearchQueryValueID])
GO
ALTER TABLE [dbo].[SearchQueryValue] ADD CONSTRAINT [FK_SearchQueryValue_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SearchQueryValue] ADD CONSTRAINT [FK_SearchQueryValue_SearchField] FOREIGN KEY ([SearchFieldID]) REFERENCES [dbo].[SearchField] ([SearchFieldID])
GO
ALTER TABLE [dbo].[SearchQueryValue] ADD CONSTRAINT [FK_SearchQueryValue_SearchFieldOperator] FOREIGN KEY ([SearchFieldOperatorID]) REFERENCES [dbo].[SearchFieldOperator] ([SearchFieldOperatorID])
GO
ALTER TABLE [dbo].[SearchQueryValue] ADD CONSTRAINT [FK_SearchQueryValue_SearchQuery] FOREIGN KEY ([SearchQueryID]) REFERENCES [dbo].[SearchQuery] ([SearchQueryID])
GO
ALTER TABLE [dbo].[SearchQueryValue] ADD CONSTRAINT [FK_SearchQueryValue_SubClient] FOREIGN KEY ([SubClientID]) REFERENCES [dbo].[SubClient] ([SubClientID])
GO
