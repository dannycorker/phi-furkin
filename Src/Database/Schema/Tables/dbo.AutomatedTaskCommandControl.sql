CREATE TABLE [dbo].[AutomatedTaskCommandControl]
(
[AutomatedTaskCommandControlID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[JobName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[JobNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[FiringOrder] [int] NOT NULL,
[SuppressAllLaterJobs] [bit] NOT NULL,
[IsConfigFileRequired] [bit] NOT NULL,
[ConfigFileName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[IsBatFileRequired] [bit] NOT NULL,
[BatFileName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Enabled] [bit] NOT NULL,
[MaxWaitTimeMS] [int] NOT NULL,
[TaskID] [int] NOT NULL,
[OutputExtension] [varchar] (4) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[AutomatedTaskCommandControl] ADD CONSTRAINT [PK_AutomatedTaskCommandControl] PRIMARY KEY CLUSTERED  ([AutomatedTaskCommandControlID])
GO
CREATE NONCLUSTERED INDEX [IX_AutomatedTaskCommandControl] ON [dbo].[AutomatedTaskCommandControl] ([AutomatedTaskCommandControlID])
GO
CREATE NONCLUSTERED INDEX [IX_AutomatedTaskCommandControl_Batname] ON [dbo].[AutomatedTaskCommandControl] ([ClientID], [BatFileName])
GO
CREATE NONCLUSTERED INDEX [IX_AutomatedTaskCommandControl_Configname] ON [dbo].[AutomatedTaskCommandControl] ([ClientID], [ConfigFileName])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_AutomatedTaskCommandControl_Jobname] ON [dbo].[AutomatedTaskCommandControl] ([ClientID], [JobName])
GO
ALTER TABLE [dbo].[AutomatedTaskCommandControl] ADD CONSTRAINT [FK_AutomatedTaskCommandControl_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
