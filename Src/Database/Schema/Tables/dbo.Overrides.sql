CREATE TABLE [dbo].[Overrides]
(
[OverrideID] [int] NOT NULL IDENTITY(1, 1),
[OverrideTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[EventTypeID] [int] NULL,
[QueryID] [int] NULL,
[DetailFieldID] [int] NULL,
[DocumentTypeID] [int] NULL,
[ClientPersonnelAdminGroupID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[Overrides] ADD CONSTRAINT [PK_Overrides] PRIMARY KEY CLUSTERED  ([OverrideID])
GO
