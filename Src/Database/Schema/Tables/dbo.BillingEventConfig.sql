CREATE TABLE [dbo].[BillingEventConfig]
(
[BillingEventConfigID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientAccountID] [int] NOT NULL,
[FileGenerationType] [int] NOT NULL,
[EventToApply] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[BillingEventConfig] ADD CONSTRAINT [PK_BillingEventConfig] PRIMARY KEY CLUSTERED  ([BillingEventConfigID])
GO
