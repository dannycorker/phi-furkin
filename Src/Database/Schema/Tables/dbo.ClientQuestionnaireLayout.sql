CREATE TABLE [dbo].[ClientQuestionnaireLayout]
(
[ClientQuestionnaireLayoutID] [int] NOT NULL IDENTITY(1, 1),
[ClientQuestionnaireID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[PageNumber] [int] NOT NULL,
[LayoutCss] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[LayoutCssFileName] [varchar] (512) COLLATE Latin1_General_CI_AS NULL,
[QuestionColumns] [int] NULL
)
GO
ALTER TABLE [dbo].[ClientQuestionnaireLayout] ADD CONSTRAINT [PK_ClientQuestionnaireLayouts] PRIMARY KEY CLUSTERED  ([ClientQuestionnaireLayoutID])
GO
ALTER TABLE [dbo].[ClientQuestionnaireLayout] ADD CONSTRAINT [FK_ClientQuestionnaireLayout_ClientQuestionnaires] FOREIGN KEY ([ClientQuestionnaireID]) REFERENCES [dbo].[ClientQuestionnaires] ([ClientQuestionnaireID])
GO
