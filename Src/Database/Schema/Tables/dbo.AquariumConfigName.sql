CREATE TABLE [dbo].[AquariumConfigName]
(
[NameID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[AquariumConfigName] ADD CONSTRAINT [PK_AquariumConfigName] PRIMARY KEY CLUSTERED  ([NameID])
GO
