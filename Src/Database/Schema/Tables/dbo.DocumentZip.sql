CREATE TABLE [dbo].[DocumentZip]
(
[DocumentZipID] [int] NOT NULL IDENTITY(1, 1),
[DocumentZipInformationID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[DocumentQueueID] [int] NOT NULL,
[StatusID] [int] NOT NULL,
[DocumentFileName] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ZipFileName] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[WhenStored] [datetime] NULL,
[WhenCreated] [datetime] NULL,
[DocumentZipFileID] [int] NULL
)
GO
ALTER TABLE [dbo].[DocumentZip] ADD CONSTRAINT [PK_DocumentZip] PRIMARY KEY CLUSTERED  ([DocumentZipID])
GO
CREATE NONCLUSTERED INDEX [IX_DocumentZipClient] ON [dbo].[DocumentZip] ([ClientID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DocumentZipDocumentQueue] ON [dbo].[DocumentZip] ([DocumentQueueID])
GO
CREATE NONCLUSTERED INDEX [IX_DocumentZipDocumentZipInformation] ON [dbo].[DocumentZip] ([DocumentZipInformationID])
GO
CREATE NONCLUSTERED INDEX [IX_DocumentZipStatus] ON [dbo].[DocumentZip] ([StatusID])
GO
ALTER TABLE [dbo].[DocumentZip] ADD CONSTRAINT [FK_DocumentZip_DocumentQueue] FOREIGN KEY ([DocumentQueueID]) REFERENCES [dbo].[DocumentQueue] ([DocumentQueueID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[DocumentZip] ADD CONSTRAINT [FK_DocumentZip_DocumentZipInformation] FOREIGN KEY ([DocumentZipInformationID]) REFERENCES [dbo].[DocumentZipInformation] ([DocumentZipInformationID]) ON DELETE CASCADE
GO
