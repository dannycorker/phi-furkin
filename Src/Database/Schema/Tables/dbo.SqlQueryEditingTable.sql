CREATE TABLE [dbo].[SqlQueryEditingTable]
(
[SqlQueryEditingTableID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SqlQueryEditingID] [int] NOT NULL,
[SqlQueryTableName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[TableAlias] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[TableDisplayOrder] [int] NOT NULL,
[JoinType] [varchar] (25) COLLATE Latin1_General_CI_AS NULL,
[JoinText] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[JoinTableID] [int] NULL,
[JoinRTRID] [int] NULL,
[RealSqlQueryTableID] [int] NULL
)
GO
ALTER TABLE [dbo].[SqlQueryEditingTable] ADD CONSTRAINT [PK_SqlQueryTableEditing] PRIMARY KEY CLUSTERED  ([SqlQueryEditingTableID])
GO
CREATE NONCLUSTERED INDEX [IX_SqlQueryEditingTable_Query] ON [dbo].[SqlQueryEditingTable] ([SqlQueryEditingID])
GO
ALTER TABLE [dbo].[SqlQueryEditingTable] ADD CONSTRAINT [FK_SqlQueryEditingTable_SqlQueryEditing] FOREIGN KEY ([SqlQueryEditingID]) REFERENCES [dbo].[SqlQueryEditing] ([SqlQueryEditingID]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SqlQueryEditingTable] ADD CONSTRAINT [FK_SqlQueryEditingTable_SqlQueryEditingTable] FOREIGN KEY ([SqlQueryEditingTableID]) REFERENCES [dbo].[SqlQueryEditingTable] ([SqlQueryEditingTableID])
GO
