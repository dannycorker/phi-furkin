CREATE TABLE [dbo].[EventTypeFieldDefault]
(
[EventTypeFieldDefaultID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[DefaultValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[EventTypeFieldDefault] ADD CONSTRAINT [PK_EventTypeFieldDefault] PRIMARY KEY CLUSTERED  ([EventTypeFieldDefaultID])
GO
