CREATE TABLE [dbo].[ClientsHistory]
(
[ClientHistoryID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CompanyName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[WebAddress] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[IPAddress] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DefaultEmailAddress] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[ClientTypeID] [int] NULL,
[AllowSMS] [bit] NULL,
[SecurityCode] [varchar] (36) COLLATE Latin1_General_CI_AS NULL,
[LeadsBelongToOffices] [bit] NULL,
[UseEventCosts] [bit] NULL,
[UseEventUOEs] [bit] NULL,
[UseEventDisbursements] [bit] NULL,
[UseEventComments] [bit] NULL,
[WhoChanged] [int] NULL,
[WhenChanged] [datetime] NOT NULL,
[ChangeAction] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ChangeNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[VerifyAddress] [bit] NULL,
[UseTapi] [bit] NULL,
[UseRPI] [bit] NULL,
[UseGBAddress] [bit] NULL,
[UsePinpoint] [bit] NULL,
[UseSage] [bit] NULL,
[UseSAS] [bit] NULL,
[UseCreditCalculation] [bit] NULL,
[FollowupWorkingDaysOnly] [bit] NULL,
[AddLeadByQuestionnaire] [bit] NULL,
[UseIncendia] [bit] NULL,
[LanguageID] [int] NULL,
[CountryID] [int] NULL,
[UseMobileInterface] [bit] NULL
)
GO
ALTER TABLE [dbo].[ClientsHistory] ADD CONSTRAINT [PK_ClientsHistory] PRIMARY KEY CLUSTERED  ([ClientHistoryID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientsHistory_Clients] ON [dbo].[ClientsHistory] ([ClientID])
GO
