CREATE TABLE [dbo].[SqlQueryType]
(
[SqlQueryTypeID] [int] NOT NULL IDENTITY(1, 1),
[TypeName] [nchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[SqlQueryType] ADD CONSTRAINT [PK_SqlQueryType] PRIMARY KEY CLUSTERED  ([SqlQueryTypeID])
GO
