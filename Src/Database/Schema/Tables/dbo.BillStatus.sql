CREATE TABLE [dbo].[BillStatus]
(
[BillStatusID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[BillStatus] ADD CONSTRAINT [PK_BillStatus] PRIMARY KEY CLUSTERED  ([BillStatusID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_BillStatus] ON [dbo].[BillStatus] ([Name])
GO
