CREATE TABLE [dbo].[RulesEngine_OutputCoordinates]
(
[OutputCoordinateID] [int] NOT NULL IDENTITY(1, 1),
[RuleOutputID] [int] NOT NULL,
[ParameterOptionID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[RulesEngine_OutputCoordinates] ADD CONSTRAINT [PK_RulesEngine_OutputCoordinates] PRIMARY KEY CLUSTERED  ([OutputCoordinateID])
GO
CREATE NONCLUSTERED INDEX [IX_RulesEngine_OutputCoordinates] ON [dbo].[RulesEngine_OutputCoordinates] ([ParameterOptionID], [RuleOutputID])
GO
CREATE NONCLUSTERED INDEX [IX_RulesEngine_OutputCoordinates_RuleOutputID] ON [dbo].[RulesEngine_OutputCoordinates] ([RuleOutputID])
GO
ALTER TABLE [dbo].[RulesEngine_OutputCoordinates] ADD CONSTRAINT [FK_RulesEngine_OutputCoordinates_RulesEngine_ParameterOptions] FOREIGN KEY ([ParameterOptionID]) REFERENCES [dbo].[RulesEngine_ParameterOptions] ([ParameterOptionID])
GO
ALTER TABLE [dbo].[RulesEngine_OutputCoordinates] ADD CONSTRAINT [FK_RulesEngine_OutputCoordinates_RulesEngine_RuleOutputs] FOREIGN KEY ([RuleOutputID]) REFERENCES [dbo].[RulesEngine_RuleOutputs] ([RuleOutputID])
GO
