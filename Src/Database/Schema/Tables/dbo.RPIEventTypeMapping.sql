CREATE TABLE [dbo].[RPIEventTypeMapping]
(
[RPIEventTypeMappingID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[RPIEventTypeID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[IsEnabled] [bit] NOT NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[RPIEventTypeMapping] ADD CONSTRAINT [PK_RPIEventTypeMapping] PRIMARY KEY CLUSTERED  ([RPIEventTypeMappingID])
GO
