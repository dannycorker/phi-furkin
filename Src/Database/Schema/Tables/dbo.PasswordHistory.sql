CREATE TABLE [dbo].[PasswordHistory]
(
[PasswordHistoryID] [int] NOT NULL IDENTITY(1, 1),
[DateChanged] [datetime] NOT NULL,
[Password] [varchar] (65) COLLATE Latin1_General_CI_AS NOT NULL,
[Salt] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ClientPersonnelID] [int] NULL,
[PortalUserID] [int] NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[PasswordHistory] ADD CONSTRAINT [PK_PasswordHistory] PRIMARY KEY CLUSTERED  ([PasswordHistoryID])
GO
