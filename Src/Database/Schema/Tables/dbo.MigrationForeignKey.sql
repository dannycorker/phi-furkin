CREATE TABLE [dbo].[MigrationForeignKey]
(
[MigrationForeignKeyID] [int] NOT NULL IDENTITY(1, 1),
[TableName] [varchar] (1024) COLLATE Latin1_General_CI_AS NOT NULL,
[ForeignKeyName] [varchar] (1024) COLLATE Latin1_General_CI_AS NOT NULL,
[DropText] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[CreateText] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[IsEnabled] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[MigrationForeignKey] ADD CONSTRAINT [PK_MigrationForeignKey] PRIMARY KEY CLUSTERED  ([MigrationForeignKeyID])
GO
