CREATE TABLE [dbo].[DashboardItem]
(
[DashboardItemID] [int] NOT NULL IDENTITY(1, 1),
[DashboardID] [int] NOT NULL,
[DashboardItemTypeID] [int] NOT NULL,
[ItemID] [int] NOT NULL,
[ClientID] [int] NULL,
[LayoutPosition] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[DashboardItem] ADD CONSTRAINT [PK_DashboardItem] PRIMARY KEY CLUSTERED  ([DashboardItemID])
GO
ALTER TABLE [dbo].[DashboardItem] ADD CONSTRAINT [FK_DashboardItem_Dashboard] FOREIGN KEY ([DashboardID]) REFERENCES [dbo].[Dashboard] ([DashboardID])
GO
ALTER TABLE [dbo].[DashboardItem] ADD CONSTRAINT [FK_DashboardItem_DashboardItemType] FOREIGN KEY ([DashboardItemTypeID]) REFERENCES [dbo].[DashboardItemType] ([DashboardItemTypeID])
GO
