CREATE TABLE [dbo].[LeadStatus]
(
[StatusID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[StatusName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[StatusDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[SourceID] [int] NULL,
[LeadTypeID] [int] NULL,
[IsShared] [bit] NULL
)
GO
ALTER TABLE [dbo].[LeadStatus] ADD CONSTRAINT [PK_LeadStatus] PRIMARY KEY CLUSTERED  ([StatusID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadStatus1] ON [dbo].[LeadStatus] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadStatus_SourceID] ON [dbo].[LeadStatus] ([SourceID]) WHERE ([SourceID]>(0))
GO
