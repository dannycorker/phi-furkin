CREATE TABLE [dbo].[ReferenceValueDateFormat]
(
[ReferenceValueDateFormatID] [int] NOT NULL IDENTITY(1, 1),
[DateFormat] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ReferenceValueDateFormat] ADD CONSTRAINT [PK_ReferenceValueDateFormat] PRIMARY KEY CLUSTERED  ([ReferenceValueDateFormatID])
GO
