CREATE TABLE [dbo].[InvoiceMappingEvent]
(
[InvoiceMappingEventID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[Type] [char] (1) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[InvoiceMappingEvent] ADD CONSTRAINT [CK__InvoiceMap__Type__1DD065E0] CHECK (([Type]='C' OR [Type]='I'))
GO
ALTER TABLE [dbo].[InvoiceMappingEvent] ADD CONSTRAINT [PK_InvoiceMappingEvent] PRIMARY KEY CLUSTERED  ([InvoiceMappingEventID])
GO
CREATE NONCLUSTERED INDEX [IX_InvoiceMappingEventLeadTypeEventType] ON [dbo].[InvoiceMappingEvent] ([LeadTypeID], [EventTypeID], [Type])
GO
ALTER TABLE [dbo].[InvoiceMappingEvent] ADD CONSTRAINT [FK_InvoiceMappingEvent_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[InvoiceMappingEvent] ADD CONSTRAINT [FK_InvoiceMappingEvent_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[InvoiceMappingEvent] ADD CONSTRAINT [FK_InvoiceMappingEvent_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
