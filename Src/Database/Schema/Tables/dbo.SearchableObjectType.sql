CREATE TABLE [dbo].[SearchableObjectType]
(
[SearchableObjectTypeID] [int] NOT NULL,
[SearchableObject] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[PKColumnName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[JoinTable] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[JoinColumn] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[SearchableObjectType] ADD CONSTRAINT [PK_SearchableObjectType] PRIMARY KEY CLUSTERED  ([SearchableObjectTypeID])
GO
