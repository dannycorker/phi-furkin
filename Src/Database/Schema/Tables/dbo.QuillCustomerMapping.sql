CREATE TABLE [dbo].[QuillCustomerMapping]
(
[QuillCustomerMappingID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[QuillClientCode] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[QuillCustomerMapping] ADD CONSTRAINT [PK_QuillCustomerMapping] PRIMARY KEY CLUSTERED  ([QuillCustomerMappingID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_QuillCustomerMapping] ON [dbo].[QuillCustomerMapping] ([CustomerID])
GO
ALTER TABLE [dbo].[QuillCustomerMapping] ADD CONSTRAINT [FK_QuillCustomerMapping_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[QuillCustomerMapping] ADD CONSTRAINT [FK_QuillCustomerMapping_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
