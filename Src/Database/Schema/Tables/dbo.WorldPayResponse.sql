CREATE TABLE [dbo].[WorldPayResponse]
(
[WorldPayResponseID] [int] NOT NULL IDENTITY(1, 1),
[WorldPayRequestID] [int] NULL,
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[MatterID] [int] NOT NULL,
[ResponseXml] [xml] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[WorldPayResponse] ADD CONSTRAINT [PK_WorldPayResponse] PRIMARY KEY CLUSTERED  ([WorldPayResponseID])
GO
