CREATE TABLE [dbo].[SearchQuery]
(
[SearchQueryID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[ClientPersonnelAdminGroupID] [int] NOT NULL,
[ClientPersonnelID] [int] NOT NULL,
[SearchQueryName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[SearchQuery] ADD CONSTRAINT [PK_SearchFieldQuery] PRIMARY KEY CLUSTERED  ([SearchQueryID])
GO
ALTER TABLE [dbo].[SearchQuery] ADD CONSTRAINT [FK_SearchQuery_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[SearchQuery] ADD CONSTRAINT [FK_SearchQuery_ClientPersonnelAdminGroups] FOREIGN KEY ([ClientPersonnelAdminGroupID]) REFERENCES [dbo].[ClientPersonnelAdminGroups] ([ClientPersonnelAdminGroupID])
GO
ALTER TABLE [dbo].[SearchQuery] ADD CONSTRAINT [FK_SearchQuery_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SearchQuery] ADD CONSTRAINT [FK_SearchQuery_SearchQuery] FOREIGN KEY ([SearchQueryID]) REFERENCES [dbo].[SearchQuery] ([SearchQueryID])
GO
ALTER TABLE [dbo].[SearchQuery] ADD CONSTRAINT [FK_SearchQuery_SubClient] FOREIGN KEY ([SubClientID]) REFERENCES [dbo].[SubClient] ([SubClientID])
GO
