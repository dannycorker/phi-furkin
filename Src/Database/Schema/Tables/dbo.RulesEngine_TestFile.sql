CREATE TABLE [dbo].[RulesEngine_TestFile]
(
[TestFileID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[RuleSetID] [int] NOT NULL,
[ChangeSetID] [int] NOT NULL,
[FileName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenProcessed] [datetime] NULL,
[WhenCreated] [datetime] NOT NULL CONSTRAINT [DF_RulesEngine_TestFile_WhenCreated] DEFAULT ([dbo].[fn_GetDate_Local]()),
[WhoCreated] [int] NOT NULL,
[SourceID] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[RulesEngine_TestFile] ADD CONSTRAINT [PK_RulesEngine_TestFile] PRIMARY KEY CLUSTERED  ([TestFileID])
GO
