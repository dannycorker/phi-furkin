CREATE TABLE [dbo].[HskLeadTypeToDelete]
(
[LeadTypeID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[WhenRequested] [datetime] NOT NULL,
[Comments] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[FailedToDelete] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[HskLeadTypeToDelete] ADD CONSTRAINT [PK_HskLeadTypeToDelete] PRIMARY KEY CLUSTERED  ([LeadTypeID])
GO
ALTER TABLE [dbo].[HskLeadTypeToDelete] ADD CONSTRAINT [FK_HskLeadTypeToDelete_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID]) ON DELETE CASCADE
GO
