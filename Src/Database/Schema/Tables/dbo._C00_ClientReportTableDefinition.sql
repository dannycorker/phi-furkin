CREATE TABLE [dbo].[_C00_ClientReportTableDefinition]
(
[ClientReportTableDefinitionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[TableName] [varchar] (1024) COLLATE Latin1_General_CI_AS NOT NULL,
[LeadTypeID] [int] NOT NULL,
[TableSubtypeID] [int] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime2] (0) NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime2] (0) NOT NULL
)
GO
