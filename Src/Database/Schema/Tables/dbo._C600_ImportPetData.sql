CREATE TABLE [dbo].[_C600_ImportPetData]
(
[ImportedPetID] [int] NOT NULL IDENTITY(1, 1),
[ImportID] [int] NOT NULL,
[BatchID] [int] NULL,
[BreedID] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Pre_ExistingConditionExists] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[VaccinationStatus] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[SPAYEDNEUTERED] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[DatePurchase] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[DateLastVaccination] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Microchipnumber] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[AnimalName] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[DateBirth] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[PrimaryColour] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Sex] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[PurchasePrice] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Species] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[BreedName] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[CustomerID] [int] NULL,
[LeadID] [int] NULL,
[CaseID] [int] NULL,
[MatterID] [int] NULL
)
GO
ALTER TABLE [dbo].[_C600_ImportPetData] ADD CONSTRAINT [PK__C600_ImportPetData] PRIMARY KEY CLUSTERED  ([ImportedPetID])
GO
