CREATE TABLE [dbo].[WorldPayNotification]
(
[WorldPayNotificationID] [int] NOT NULL IDENTITY(1, 1),
[CardTransactionID] [int] NULL,
[OrderCode] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[NotificationXml] [xml] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[HasBeenParsed] [bit] NULL,
[ParsedOn] [datetime] NULL
)
GO
ALTER TABLE [dbo].[WorldPayNotification] ADD CONSTRAINT [PK_WorldPayNotification] PRIMARY KEY CLUSTERED  ([WorldPayNotificationID])
GO
EXEC sp_addextendedproperty N'MS_Description', N'This is orderCode in the xml', 'SCHEMA', N'dbo', 'TABLE', N'WorldPayNotification', 'COLUMN', N'CardTransactionID'
GO
