CREATE TABLE [dbo].[_C600_EQB_AggregatorClickThrough]
(
[AggregatorClickThroughID] [int] NOT NULL IDENTITY(1, 1),
[QuoteSessionID] [int] NOT NULL,
[SavedQuoteID] [int] NOT NULL,
[DateLoaded] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[_C600_EQB_AggregatorClickThrough] ADD CONSTRAINT [PK___C600_EQ__AB904B92D1AE5DE1] PRIMARY KEY CLUSTERED  ([AggregatorClickThroughID])
GO
CREATE NONCLUSTERED INDEX [FK_C600_IndexQuoteSessionID] ON [dbo].[_C600_EQB_AggregatorClickThrough] ([QuoteSessionID])
GO
CREATE NONCLUSTERED INDEX [FK_C600_SavedQuoteID] ON [dbo].[_C600_EQB_AggregatorClickThrough] ([SavedQuoteID])
GO
ALTER TABLE [dbo].[_C600_EQB_AggregatorClickThrough] ADD CONSTRAINT [FK___C600_EQB__Quote__464B3127] FOREIGN KEY ([QuoteSessionID]) REFERENCES [dbo].[_C600_QuoteSessions] ([QuoteSessionID])
GO
ALTER TABLE [dbo].[_C600_EQB_AggregatorClickThrough] ADD CONSTRAINT [FK___C600_EQB__Saved__473F5560] FOREIGN KEY ([SavedQuoteID]) REFERENCES [dbo].[_C600_SavedQuote] ([SavedQuoteID])
GO
