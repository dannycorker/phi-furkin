CREATE TABLE [dbo].[EventTypeResourceFieldCopy]
(
[EventTypeResourceListFieldCopyID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[DestinationDetailFieldID] [int] NOT NULL,
[ResourceDetailFieldID] [int] NOT NULL,
[ResourceContentsDetailFieldID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[EventTypeResourceFieldCopy] ADD CONSTRAINT [PK_EventTypeResourceFieldCopy] PRIMARY KEY CLUSTERED  ([EventTypeResourceListFieldCopyID])
GO
