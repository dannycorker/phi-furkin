CREATE TABLE [dbo].[LeadDetailValues]
(
[LeadDetailValueID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[DetailValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[ErrorMsg] [varchar] (1000) COLLATE Latin1_General_CI_AS NULL,
[OriginalDetailValueID] [int] NULL,
[OriginalLeadID] [int] NULL,
[EncryptedValue] [varchar] (3000) COLLATE Latin1_General_CI_AS NULL,
[ValueInt] [int] NULL,
[ValueMoney] [money] NULL,
[ValueDate] [date] NULL,
[ValueDateTime] [datetime2] (0) NULL,
[SourceID] [int] NULL
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-27
-- Description:	Store DetailValue as specific datatypes
-- 2011-07-13 JWG Remove duplicates instantly at source
-- =============================================
CREATE TRIGGER [dbo].[trgiu_LeadDetailValues]
   ON [dbo].[LeadDetailValues]
   AFTER INSERT, UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	/* Allow special user 'notriggers' to bypass trigger code */
	IF system_user = 'notriggers'
	BEGIN
		RETURN
	END


    /*
		If the DetailValue column has been updated then fire the trigger,
		otherwise we are here again because the trigger has just fired,
		in which case do nothing.
	*/
    IF UPDATE(DetailValue)
    BEGIN
		/* Check for duplicates (but only on insert, not update) */
		IF NOT EXISTS (SELECT * FROM deleted)
		BEGIN
			/* Only issue the delete if we know that any records exist, otherwise the delete operation gets attempted every time regardless */
			IF EXISTS(SELECT * FROM dbo.LeadDetailValues dv WITH (NOLOCK) INNER JOIN inserted i ON i.LeadID = dv.LeadID AND i.DetailFieldID = dv.DetailFieldID AND i.LeadDetailValueID > dv.LeadDetailValueID )
			BEGIN
				/* Remove any older duplicates for this field */
				DELETE dbo.LeadDetailValues
				OUTPUT 1, deleted.LeadDetailValueID, deleted.LeadID, deleted.DetailValue, deleted.DetailFieldID
				INTO dbo.__DeletedValue (DetailFieldSubTypeID, DetailValueID, ParentID, DetailValue, DetailFieldID)
				FROM dbo.LeadDetailValues dv
				INNER JOIN inserted i ON i.LeadID = dv.LeadID AND i.DetailFieldID = dv.DetailFieldID AND i.LeadDetailValueID > dv.LeadDetailValueID
			END
		END

		/* Populate ValueInt et al */
		UPDATE dbo.LeadDetailValues
		SET ValueInt = dbo.fnValueAsIntBitsIncluded(i.DetailValue) /*CASE WHEN dbo.fnIsInt(i.detailvalue) = 1 THEN convert(int, i.DetailValue) ELSE NULL END*/,
		ValueMoney = CASE WHEN dbo.fnIsMoney(i.DetailValue) = 1 THEN convert(money, i.DetailValue) ELSE NULL END,
		ValueDate = CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN convert(date, i.DetailValue) ELSE NULL END,
		ValueDateTime = CASE WHEN dbo.fnIsDateTime(i.DetailValue) = 1 THEN convert(datetime2, i.DetailValue) ELSE NULL END
		FROM dbo.LeadDetailValues dv
		INNER JOIN inserted i ON i.LeadDetailValueID = dv.LeadDetailValueID
	END


END

GO
ALTER TABLE [dbo].[LeadDetailValues] ADD CONSTRAINT [PK_LeadDetailValues] PRIMARY KEY CLUSTERED  ([LeadDetailValueID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadDetailValues_Client] ON [dbo].[LeadDetailValues] ([ClientID]) INCLUDE ([LeadID])
GO
CREATE NONCLUSTERED INDEX [IX_LeadDetailValuesDetailField] ON [dbo].[LeadDetailValues] ([DetailFieldID]) INCLUDE ([LeadID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_LeadDetailValues_FieldValueDate] ON [dbo].[LeadDetailValues] ([DetailFieldID], [ValueDate], [ClientID]) INCLUDE ([LeadID]) WHERE ([ValueDate] IS NOT NULL)
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_LeadDetailValues_FieldValueInt] ON [dbo].[LeadDetailValues] ([DetailFieldID], [ValueInt], [ClientID]) INCLUDE ([LeadID]) WHERE ([ValueInt] IS NOT NULL)
GO
CREATE NONCLUSTERED INDEX [IX_LeadDetailValues_LeadAndField] ON [dbo].[LeadDetailValues] ([LeadID], [DetailFieldID])
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE NONCLUSTERED INDEX [IX_LeadDetailValues_LeadFieldValueInt] ON [dbo].[LeadDetailValues] ([LeadID], [DetailFieldID], [ValueInt], [ClientID]) WHERE ([ValueInt] IS NOT NULL)
GO
ALTER TABLE [dbo].[LeadDetailValues] ADD CONSTRAINT [FK_LeadDetailValues_DetailFields] FOREIGN KEY ([DetailFieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
ALTER TABLE [dbo].[LeadDetailValues] ADD CONSTRAINT [FK_LeadDetailValues_Lead] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[Lead] ([LeadID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
