CREATE TABLE [dbo].[DetailFieldOption]
(
[DetailFieldOptionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[Mandatory] [bit] NOT NULL,
[FieldOrder] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[DetailFieldOption] ADD CONSTRAINT [PK_DetailFieldOption] PRIMARY KEY CLUSTERED  ([DetailFieldOptionID])
GO
