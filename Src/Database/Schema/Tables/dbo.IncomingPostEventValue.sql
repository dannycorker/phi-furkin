CREATE TABLE [dbo].[IncomingPostEventValue]
(
[IncomingPostEventValueID] [int] NOT NULL IDENTITY(1, 1),
[IncomingPostEventID] [int] NOT NULL,
[PostKey] [varchar] (256) COLLATE Latin1_General_CI_AS NOT NULL,
[PostValue] [varchar] (512) COLLATE Latin1_General_CI_AS NOT NULL,
[ClientID] [int] NULL,
[CustomerID] [int] NULL,
[LeadID] [int] NULL,
[CaseID] [int] NULL,
[MatterID] [int] NULL
)
GO
ALTER TABLE [dbo].[IncomingPostEventValue] ADD CONSTRAINT [PK_IncomingPostEventValue] PRIMARY KEY CLUSTERED  ([IncomingPostEventValueID])
GO
ALTER TABLE [dbo].[IncomingPostEventValue] ADD CONSTRAINT [FK_IncomingPostEventValue_IncomingPostEvent] FOREIGN KEY ([IncomingPostEventID]) REFERENCES [dbo].[IncomingPostEvent] ([IncomingPostEventID])
GO
