CREATE TABLE [dbo].[ClientPersonnelAdminGroups]
(
[ClientPersonnelAdminGroupID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[GroupName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[ClientPersonnelAdminGroups] ADD CONSTRAINT [PK_ClientPersonelAdminGroups] PRIMARY KEY CLUSTERED  ([ClientPersonnelAdminGroupID])
GO
ALTER TABLE [dbo].[ClientPersonnelAdminGroups] ADD CONSTRAINT [FK_ClientPersonnelAdminGroups_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
