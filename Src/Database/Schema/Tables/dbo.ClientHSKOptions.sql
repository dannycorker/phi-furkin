CREATE TABLE [dbo].[ClientHSKOptions]
(
[ClientHSKOptionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[IncludeLETCCleanUp] [bit] NOT NULL,
[IncludeMobileCleanUp] [bit] NOT NULL,
[IncludeFollowupDateCleanUp] [bit] NOT NULL,
[UseLeadDocumentFS] [bit] NULL
)
GO
ALTER TABLE [dbo].[ClientHSKOptions] ADD CONSTRAINT [PK_ClientHSKOptions] PRIMARY KEY CLUSTERED  ([ClientHSKOptionID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_ClientHSKOptions_Covering] ON [dbo].[ClientHSKOptions] ([ClientID], [IncludeLETCCleanUp], [IncludeMobileCleanUp])
GO
ALTER TABLE [dbo].[ClientHSKOptions] ADD CONSTRAINT [FK_ClientHSKOptions_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
