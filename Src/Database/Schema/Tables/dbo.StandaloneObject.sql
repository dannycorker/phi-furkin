CREATE TABLE [dbo].[StandaloneObject]
(
[StandaloneObjectID] [int] NOT NULL IDENTITY(1, 1),
[StandaloneObjectType] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[StandaloneObjectName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[StandaloneObjectDescription] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[StandaloneObject] ADD CONSTRAINT [PK_StandaloneObject] PRIMARY KEY CLUSTERED  ([StandaloneObjectID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_StandaloneObject_Name] ON [dbo].[StandaloneObject] ([StandaloneObjectName])
GO
