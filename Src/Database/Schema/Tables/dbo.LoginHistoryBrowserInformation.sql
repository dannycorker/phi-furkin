CREATE TABLE [dbo].[LoginHistoryBrowserInformation]
(
[LoginHistoryBrowserInformationID] [int] NOT NULL IDENTITY(1, 1),
[LoginHistoryID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[ClientPersonnelID] [int] NOT NULL,
[UserAgentString] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[BrowserFamily] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[BrowserName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[BrowserVersion] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[BrowserMajor] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[BrowserMinor] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[BrowserPatch] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[DeviceFamily] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DeviceName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DeviceVersion] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DeviceMajor] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[DeviceMinor] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[DevicePatch] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[DeviceType] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[DeviceManufacturer] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[OperatingSystemFamily] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[OperatingSystemName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[OperatingSystemVersion] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[OperatingSystemMajor] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[OperatingSystemMinor] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[OperatingSystemPatch] [varchar] (10) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[LoginHistoryBrowserInformation] ADD CONSTRAINT [PK_LoginHistoryBrowserInformation] PRIMARY KEY CLUSTERED  ([LoginHistoryBrowserInformationID])
GO
