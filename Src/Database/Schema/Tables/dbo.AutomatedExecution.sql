CREATE TABLE [dbo].[AutomatedExecution]
(
[AutomatedExecutionID] [bigint] NOT NULL IDENTITY(1, 1),
[SQLToRun] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[ExecutionSubTypeID] [int] NULL,
[EarliestRunDateTime] [datetime] NULL,
[CompletionDateTime] [datetime] NULL,
[ErrorDateTime] [datetime] SPARSE NULL,
[ClientID] [int] NOT NULL,
[Attempted] [bit] NOT NULL CONSTRAINT [DF_AutomatedExecution_Attempted] DEFAULT ((0)),
[InsertedBy] [varchar] (50) COLLATE Latin1_General_CI_AS NULL CONSTRAINT [DF_AutomatedExecution_InsertedBy] DEFAULT ((suser_name()+' - ')+isnull(object_name(@@procid),'')),
[SQLHash] AS (CONVERT([varbinary](50),hashbytes('SHA1',[SQLToRun]),(0))) PERSISTED
)
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-08-04
-- Description:	Prevent updates to the InsertedBy column
-- =============================================
CREATE TRIGGER [dbo].[trgiud_AutomatedExecutionInsertedBy] 
   ON [dbo].[AutomatedExecution] 
   AFTER UPDATE 
AS 
BEGIN
	SET NOCOUNT ON;
	
	/* Allow special user 'notriggers' to bypass trigger code */
	IF SYSTEM_USER = 'notriggers'
	BEGIN
		RETURN
	END
	
  IF UPDATE(InsertedBy)
  BEGIN
  
    ROLLBACK
    RAISERROR('Updates to InsertedBy not allowed', 16, 1);
    
  END

END


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-08-04
-- Description:	Prevent the same command being put into the table several times
-- =============================================
CREATE TRIGGER [dbo].[trgiud_AutomatedExecutionPreventDuplication] 
   ON [dbo].[AutomatedExecution] 
   AFTER UPDATE 
AS 
BEGIN
	SET NOCOUNT ON;
	
	/* Allow special user 'notriggers' to bypass trigger code */
	IF SYSTEM_USER = 'notriggers'
	BEGIN
		RETURN
	END
	
	IF EXISTS (SELECT *
		FROM inserted i
		INNER JOIN dbo.AutomatedExecution ae WITH (NOLOCK) ON ae.SQLHash = i.SQLHash
		WHERE i.Attempted = 0)
	BEGIN

		DELETE ae
		FROM AutomatedExecution ae WITH (NOLOCK)  
		INNER JOIN inserted i ON i.AutomatedExecutionID = ae.AutomatedExecutionID
		WHERE ae.AutomatedExecutionID = i.AutomatedExecutionID
		
	END

END


GO
ALTER TABLE [dbo].[AutomatedExecution] ADD CONSTRAINT [PK_AutomatedExecution] PRIMARY KEY CLUSTERED  ([AutomatedExecutionID])
GO
CREATE NONCLUSTERED INDEX [IX_AutomatedExecution_Covering] ON [dbo].[AutomatedExecution] ([ClientID], [Attempted], [EarliestRunDateTime])
GO
