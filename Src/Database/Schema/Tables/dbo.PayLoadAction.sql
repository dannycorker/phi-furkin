CREATE TABLE [dbo].[PayLoadAction]
(
[PayLoadActionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ActionToPerform] [varchar] (255) COLLATE Latin1_General_CI_AS NOT NULL,
[Description] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[PayLoadAction] ADD CONSTRAINT [PK_PayLoadAction] PRIMARY KEY CLUSTERED  ([PayLoadActionID])
GO
ALTER TABLE [dbo].[PayLoadAction] ADD CONSTRAINT [FK_PayLoadAction_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
