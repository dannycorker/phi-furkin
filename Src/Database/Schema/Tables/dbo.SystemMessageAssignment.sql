CREATE TABLE [dbo].[SystemMessageAssignment]
(
[SystemMessageAssignmentID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientPersonnelAdminGroupID] [int] NULL,
[ClientPersonnelID] [int] NULL,
[SystemMessageID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[SystemMessageAssignment] ADD CONSTRAINT [PK_SystemMessageAssignment] PRIMARY KEY CLUSTERED  ([SystemMessageAssignmentID])
GO
ALTER TABLE [dbo].[SystemMessageAssignment] ADD CONSTRAINT [FK_SystemMessageAssignment_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[SystemMessageAssignment] ADD CONSTRAINT [FK_SystemMessageAssignment_ClientPersonnelAdminGroups] FOREIGN KEY ([ClientPersonnelAdminGroupID]) REFERENCES [dbo].[ClientPersonnelAdminGroups] ([ClientPersonnelAdminGroupID])
GO
ALTER TABLE [dbo].[SystemMessageAssignment] ADD CONSTRAINT [FK_SystemMessageAssignment_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SystemMessageAssignment] ADD CONSTRAINT [FK_SystemMessageAssignment_SystemMessage] FOREIGN KEY ([SystemMessageID]) REFERENCES [dbo].[SystemMessage] ([SystemMessageID])
GO
