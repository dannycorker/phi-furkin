CREATE TABLE [dbo].[VetEnvoy_Accounts]
(
[AccountID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[Name] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[Username] [uniqueidentifier] NOT NULL,
[Password] [uniqueidentifier] NOT NULL,
[Pollref] [bigint] NOT NULL CONSTRAINT [DF__VetEnvoy___Pollr__6D8EB7E6] DEFAULT ((0)),
[Enabled] [bit] NOT NULL CONSTRAINT [DF__VetEnvoy___Enabl__6E82DC1F] DEFAULT ((1)),
[Url] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[AccountType] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[VetEnvoy_Accounts] ADD CONSTRAINT [PK_VetEnvoy_Accounts] PRIMARY KEY CLUSTERED  ([AccountID])
GO
