CREATE TABLE [dbo].[DetailFieldPagesHistory]
(
[DetailFieldPageHistoryID] [int] NOT NULL IDENTITY(1, 1),
[DetailFieldPageID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[LeadOrMatter] [tinyint] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[PageName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PageCaption] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PageOrder] [int] NOT NULL,
[Enabled] [bit] NOT NULL,
[ResourceList] [bit] NULL,
[WhoChanged] [int] NULL,
[WhenChanged] [datetime] NOT NULL,
[ChangeAction] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[ChangeNotes] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[DetailFieldPagesHistory] ADD CONSTRAINT [PK_DetailFieldPagesHistory] PRIMARY KEY CLUSTERED  ([DetailFieldPageHistoryID])
GO
CREATE NONCLUSTERED INDEX [IX_DetailFieldPagesHistory_Clients] ON [dbo].[DetailFieldPagesHistory] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_DetailFieldPagesHistory_DetailFieldPage] ON [dbo].[DetailFieldPagesHistory] ([DetailFieldPageID])
GO
