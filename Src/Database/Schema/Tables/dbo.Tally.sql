CREATE TABLE [dbo].[Tally]
(
[N] [int] NOT NULL IDENTITY(1, 1)
)
GO
ALTER TABLE [dbo].[Tally] ADD CONSTRAINT [PK_Tally_N] PRIMARY KEY CLUSTERED  ([N])
GO
