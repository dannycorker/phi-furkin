CREATE TABLE [dbo].[PortalUserGroupMenuOption]
(
[PortalUserGroupMenuOptionID] [int] NOT NULL IDENTITY(1, 1),
[PortalUserGroupID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[PanelItemAction] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[PanelItemName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[PanelItemCaption] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PanelItemIcon] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PanelItemURL] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[Comments] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[PortalUserGroupMenuOption] ADD CONSTRAINT [PK_PortalUserGroupMenuOption] PRIMARY KEY CLUSTERED  ([PortalUserGroupMenuOptionID])
GO
CREATE NONCLUSTERED INDEX [IX_PortalUserGroupMenuOption] ON [dbo].[PortalUserGroupMenuOption] ([PortalUserGroupID], [ClientID])
GO
