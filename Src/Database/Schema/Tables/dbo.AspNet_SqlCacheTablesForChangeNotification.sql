CREATE TABLE [dbo].[AspNet_SqlCacheTablesForChangeNotification]
(
[tableName] [varchar] (450) COLLATE Latin1_General_CI_AS NOT NULL,
[notificationCreated] [datetime] NOT NULL CONSTRAINT [DF__AspNet_Sq__notif__367B16C9] DEFAULT ([dbo].[fn_GetDate_Local]()),
[changeId] [int] NOT NULL CONSTRAINT [DF__AspNet_Sq__chang__37C5420D] DEFAULT ((0))
)
GO
ALTER TABLE [dbo].[AspNet_SqlCacheTablesForChangeNotification] ADD CONSTRAINT [PK__AspNet_SqlCacheT__67267583] PRIMARY KEY CLUSTERED  ([tableName])
GO
