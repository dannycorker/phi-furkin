CREATE TABLE [dbo].[PatchNote]
(
[PatchNoteID] [int] NOT NULL IDENTITY(1, 1),
[PatchID] [int] NOT NULL,
[Location] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[PatchNoteText] [varchar] (200) COLLATE Latin1_General_CI_AS NOT NULL,
[AdminOnly] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[PatchNote] ADD CONSTRAINT [PK_PatchNote] PRIMARY KEY CLUSTERED  ([PatchNoteID])
GO
ALTER TABLE [dbo].[PatchNote] ADD CONSTRAINT [FK_PatchNote_Patch] FOREIGN KEY ([PatchID]) REFERENCES [dbo].[Patch] ([PatchID])
GO
