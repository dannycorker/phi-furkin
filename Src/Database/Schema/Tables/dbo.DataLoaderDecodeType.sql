CREATE TABLE [dbo].[DataLoaderDecodeType]
(
[DataLoaderDecodeTypeID] [int] NOT NULL,
[DecodeTypeName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[DecodeTypeDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[DataLoaderDecodeType] ADD CONSTRAINT [PK_DataLoaderDecodeType] PRIMARY KEY CLUSTERED  ([DataLoaderDecodeTypeID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DataLoaderDecodeType] ON [dbo].[DataLoaderDecodeType] ([DecodeTypeName])
GO
