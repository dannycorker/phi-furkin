CREATE TABLE [dbo].[TableRowsTableOption]
(
[TableRowsTableOptionID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[WhereClauseCode] [varchar] (10) COLLATE Latin1_General_CI_AS NOT NULL,
[SQLToUse] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[Twips] [int] NULL,
[CompleteSQLOverride] [bit] NOT NULL,
[WhoCreated] [varchar] (201) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoChanged] [varchar] (201) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenChanged] [datetime] NOT NULL
)
GO
