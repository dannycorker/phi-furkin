CREATE TABLE [dbo].[UnitedStates]
(
[StateID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CountryID] [int] NOT NULL CONSTRAINT [DF__UnitedSta__Count__7205F0EB] DEFAULT ((233)),
[StateName] [varchar] (40) COLLATE Latin1_General_CI_AS NOT NULL,
[StateCode] [varchar] (6) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[UnitedStates] ADD CONSTRAINT [PK_StateID] PRIMARY KEY CLUSTERED  ([StateID])
GO
ALTER TABLE [dbo].[UnitedStates] ADD CONSTRAINT [FK_UnitedStates_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[UnitedStates] ADD CONSTRAINT [FK_UnitedStates_Country] FOREIGN KEY ([CountryID]) REFERENCES [dbo].[Country] ([CountryID])
GO
