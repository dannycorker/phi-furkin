CREATE TABLE [dbo].[sysproperties]
(
[id] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[smallid] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[type] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[name] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
[value] [nvarchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[AnyID] [int] NOT NULL IDENTITY(1, 1)
)
GO
ALTER TABLE [dbo].[sysproperties] ADD CONSTRAINT [PK_sysproperties] PRIMARY KEY CLUSTERED  ([AnyID])
GO
