CREATE TABLE [dbo].[EventTypeLimitation]
(
[EventTypeLimitationID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[EventTypeIDFrom] [int] NOT NULL,
[EventTypeIDTo] [int] NOT NULL,
[LimitationDays] [int] NOT NULL,
[Description] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[EventTypeLimitation] ADD CONSTRAINT [PK_EventTypeLimitation] PRIMARY KEY CLUSTERED  ([EventTypeLimitationID])
GO
CREATE NONCLUSTERED INDEX [IX_EventTypeLimitationClient] ON [dbo].[EventTypeLimitation] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_EventTypeLimitationFromEvent] ON [dbo].[EventTypeLimitation] ([EventTypeIDFrom])
GO
CREATE NONCLUSTERED INDEX [IX_EventTypeLimitationToEvent] ON [dbo].[EventTypeLimitation] ([EventTypeIDTo])
GO
CREATE NONCLUSTERED INDEX [IX_EventTypeLimitationLeadType] ON [dbo].[EventTypeLimitation] ([LeadTypeID])
GO
ALTER TABLE [dbo].[EventTypeLimitation] ADD CONSTRAINT [FK_EventTypeLimitation_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EventTypeLimitation] ADD CONSTRAINT [FK_EventTypeLimitation_EventType_From] FOREIGN KEY ([EventTypeIDFrom]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[EventTypeLimitation] ADD CONSTRAINT [FK_EventTypeLimitation_EventType_To] FOREIGN KEY ([EventTypeIDTo]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[EventTypeLimitation] ADD CONSTRAINT [FK_EventTypeLimitation_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID]) ON DELETE CASCADE
GO
