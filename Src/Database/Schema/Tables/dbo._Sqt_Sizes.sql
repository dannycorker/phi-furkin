CREATE TABLE [dbo].[_Sqt_Sizes]
(
[SqtSizeID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NULL,
[SqtID] [int] NULL,
[ByteSize] [int] NULL,
[TemplatePath] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[_Sqt_Sizes] ADD CONSTRAINT [PK___Sqt_Siz__21B7C30236297463] PRIMARY KEY CLUSTERED  ([SqtSizeID])
GO
