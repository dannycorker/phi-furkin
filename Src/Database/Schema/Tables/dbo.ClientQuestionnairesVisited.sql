CREATE TABLE [dbo].[ClientQuestionnairesVisited]
(
[ClientQuestionnairesVisitedID] [int] NOT NULL IDENTITY(1, 1),
[ClientQuestionnaireID] [int] NULL,
[VisitedDate] [datetime] NULL,
[IPAddress] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ClientQuestionnairesVisited] ADD CONSTRAINT [PK_ClientQuestionnaireVisited] PRIMARY KEY CLUSTERED  ([ClientQuestionnairesVisitedID])
GO
ALTER TABLE [dbo].[ClientQuestionnairesVisited] ADD CONSTRAINT [FK_ClientQuestionnairesVisited_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
