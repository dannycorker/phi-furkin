CREATE TABLE [dbo].[API_App]
(
[AppKey] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Platform] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Version] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Config] [xml] NULL,
[Enabled] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[API_App] ADD CONSTRAINT [PK_API_App] PRIMARY KEY CLUSTERED  ([AppKey])
GO
