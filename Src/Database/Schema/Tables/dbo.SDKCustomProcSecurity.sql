CREATE TABLE [dbo].[SDKCustomProcSecurity]
(
[SDKCustomProcID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[ProcName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[IsAllowed] [bit] NOT NULL
)
GO
ALTER TABLE [dbo].[SDKCustomProcSecurity] ADD CONSTRAINT [PK_SDKCustomProcSecurity] PRIMARY KEY CLUSTERED  ([SDKCustomProcID])
GO
