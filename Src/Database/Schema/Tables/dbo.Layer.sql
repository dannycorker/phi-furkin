CREATE TABLE [dbo].[Layer]
(
[ClientID] [int] NOT NULL IDENTITY(1, 1),
[EntityID] [int] NOT NULL,
[ParentEntityID] [int] NOT NULL,
[EntityName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[EntityType] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[XPos] [decimal] (18, 2) NOT NULL,
[YPos] [decimal] (18, 2) NOT NULL
)
GO
