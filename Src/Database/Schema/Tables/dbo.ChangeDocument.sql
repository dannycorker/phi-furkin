CREATE TABLE [dbo].[ChangeDocument]
(
[ChangeDocumentID] [int] NOT NULL IDENTITY(1, 1),
[DocumentData] [varbinary] (max) NOT NULL,
[DocumentExtension] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[WhenCreated] [datetime] NOT NULL CONSTRAINT [DF__ChangeDoc__WhenC__38635F3B] DEFAULT ([dbo].[fn_GetDate_Local]()),
[WhoCreated] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ChangeDocument] ADD CONSTRAINT [PK__ChangeDo__75E4137C455C6227] PRIMARY KEY CLUSTERED  ([ChangeDocumentID])
GO
ALTER TABLE [dbo].[ChangeDocument] ADD CONSTRAINT [FK__ChangeDoc__Creat__4B153B7D] FOREIGN KEY ([WhoCreated]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
