CREATE TABLE [dbo].[DocumentDetailFieldTarget]
(
[DocumentDetailFieldTargetID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DocumentTypeID] [int] NOT NULL,
[Target] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[DetailFieldID] [int] NULL,
[TemplateTypeID] [int] NOT NULL,
[DetailFieldAlias] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[Notes] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[ExcelSheetLocation] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Format] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[DocumentTypeVersionID] [int] NULL
)
GO
ALTER TABLE [dbo].[DocumentDetailFieldTarget] ADD CONSTRAINT [PK_DocumentDetailFieldTarget] PRIMARY KEY CLUSTERED  ([DocumentDetailFieldTargetID])
GO
ALTER TABLE [dbo].[DocumentDetailFieldTarget] ADD CONSTRAINT [FK_DocumentDetailFieldTarget_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[DocumentDetailFieldTarget] ADD CONSTRAINT [FK_DocumentDetailFieldTarget_DocumentType] FOREIGN KEY ([TemplateTypeID]) REFERENCES [dbo].[TemplateType] ([TemplateTypeID])
GO
ALTER TABLE [dbo].[DocumentDetailFieldTarget] ADD CONSTRAINT [FK_DocumentDetailFieldTarget_DocumentType1] FOREIGN KEY ([DocumentTypeID]) REFERENCES [dbo].[DocumentType] ([DocumentTypeID])
GO
ALTER TABLE [dbo].[DocumentDetailFieldTarget] ADD CONSTRAINT [FK_DocumentDetailFieldTarget_DocumentTypeVersion] FOREIGN KEY ([DocumentTypeVersionID]) REFERENCES [dbo].[DocumentTypeVersion] ([DocumentTypeVersionID])
GO
