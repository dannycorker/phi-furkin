CREATE TABLE [dbo].[CategoryKeyword]
(
[CategoryKeywordID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CategoryGroupID] [int] NOT NULL,
[Keyword] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[CategoryKeyword] ADD CONSTRAINT [PK_CategoryKeyword] PRIMARY KEY CLUSTERED  ([CategoryKeywordID])
GO
ALTER TABLE [dbo].[CategoryKeyword] ADD CONSTRAINT [FK_CategoryKeyword_CategoryGroup] FOREIGN KEY ([CategoryGroupID]) REFERENCES [dbo].[CategoryGroup] ([CategoryGroupID])
GO
ALTER TABLE [dbo].[CategoryKeyword] ADD CONSTRAINT [FK_CategoryKeyword_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
