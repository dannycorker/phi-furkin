CREATE TABLE [dbo].[ResourceList]
(
[ResourceListID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[DetailFieldPageID] [int] NULL,
[ResourceListHelperCode] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ResourceListHelperName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[ResourceList] ADD CONSTRAINT [PK_ResourceList] PRIMARY KEY CLUSTERED  ([ResourceListID])
GO
CREATE NONCLUSTERED INDEX [IX_ResourceList_SourceID] ON [dbo].[ResourceList] ([SourceID]) WHERE ([SourceID]>(0))
GO
