CREATE TABLE [dbo].[UserFunctionControlEditing]
(
[UserFunctionControlEditingID] [int] NOT NULL IDENTITY(1, 1),
[ClientPersonnelID] [int] NOT NULL,
[ModuleID] [int] NOT NULL,
[FunctionTypeID] [int] NOT NULL,
[HasDescendants] [int] NOT NULL,
[RightID] [int] NOT NULL,
[LeadTypeID] [int] NULL
)
GO
ALTER TABLE [dbo].[UserFunctionControlEditing] ADD CONSTRAINT [PK_UserFunctionControlEditing] PRIMARY KEY CLUSTERED  ([UserFunctionControlEditingID])
GO
ALTER TABLE [dbo].[UserFunctionControlEditing] ADD CONSTRAINT [FK_UserFunctionControlEditing_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[UserFunctionControlEditing] ADD CONSTRAINT [FK_UserFunctionControlEditing_FunctionType] FOREIGN KEY ([FunctionTypeID]) REFERENCES [dbo].[FunctionType] ([FunctionTypeID])
GO
ALTER TABLE [dbo].[UserFunctionControlEditing] ADD CONSTRAINT [FK_UserFunctionControlEditing_Module] FOREIGN KEY ([ModuleID]) REFERENCES [dbo].[Module] ([ModuleID])
GO
ALTER TABLE [dbo].[UserFunctionControlEditing] ADD CONSTRAINT [FK_UserFunctionControlEditing_Rights] FOREIGN KEY ([RightID]) REFERENCES [dbo].[Rights] ([RightID])
GO
