CREATE TABLE [dbo].[SMSCommand]
(
[SMSCommandID] [int] NOT NULL IDENTITY(1, 1),
[SMSCommandGroupID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[SubClientID] [int] NULL,
[Command] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Parameters] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[SMSTypeOfActionID] [int] NOT NULL,
[Action] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[SuccessResponse] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[FailureResponse] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[ResultSetResponseColumns] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[IncludeResultSetResponseColumnNames] [bit] NULL
)
GO
ALTER TABLE [dbo].[SMSCommand] ADD CONSTRAINT [PK_SMSCommand] PRIMARY KEY CLUSTERED  ([SMSCommandID])
GO
ALTER TABLE [dbo].[SMSCommand] ADD CONSTRAINT [FK_SMSCommand_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SMSCommand] ADD CONSTRAINT [FK_SMSCommand_SMSCommandGroup] FOREIGN KEY ([SMSCommandGroupID]) REFERENCES [dbo].[SMSCommandGroup] ([SMSCommandGroupID])
GO
ALTER TABLE [dbo].[SMSCommand] ADD CONSTRAINT [FK_SMSCommand_SMSTypeOfAction] FOREIGN KEY ([SMSTypeOfActionID]) REFERENCES [dbo].[SMSTypeOfAction] ([SMSTypeOfActionID])
GO
