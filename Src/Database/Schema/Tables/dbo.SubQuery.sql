CREATE TABLE [dbo].[SubQuery]
(
[SubQueryID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[SubQueryName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[SubQueryDesc] [varchar] (500) COLLATE Latin1_General_CI_AS NOT NULL,
[SubQueryText] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[SubQuery] ADD CONSTRAINT [PK_SubQuery] PRIMARY KEY CLUSTERED  ([SubQueryID])
GO
ALTER TABLE [dbo].[SubQuery] ADD CONSTRAINT [FK_SubQuery_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
