CREATE TABLE [dbo].[CustomerInformationFieldDefinitions]
(
[CustomerInformationFieldDefinitionID] [int] NOT NULL IDENTITY(1, 1),
[ClientQuestionnaireID] [int] NOT NULL,
[FieldAtBeginningOfQuestionnaire] [bit] NULL,
[OrdinalPosition] [int] NULL,
[FieldName] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[FieldLabel] [varchar] (900) COLLATE Latin1_General_CI_AS NULL,
[Mandatory] [bit] NULL,
[VerifyPhoneNumber] [bit] NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[CustomerInformationFieldDefinitions] ADD CONSTRAINT [PK_CustomerInformationFieldDefinitions] PRIMARY KEY CLUSTERED  ([CustomerInformationFieldDefinitionID])
GO
ALTER TABLE [dbo].[CustomerInformationFieldDefinitions] ADD CONSTRAINT [FK_CustomerInformationFieldDefinitions_Clients] FOREIGN KEY ([ClientQuestionnaireID]) REFERENCES [dbo].[ClientQuestionnaires] ([ClientQuestionnaireID])
GO
