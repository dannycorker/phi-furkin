CREATE TABLE [dbo].[PendingAccount]
(
[PendingAccountID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ClientPersonnelID] [int] NOT NULL,
[GUID] [varchar] (36) COLLATE Latin1_General_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[LinkSentBy] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[PendingAccount] ADD CONSTRAINT [PK_PendingAccount] PRIMARY KEY CLUSTERED  ([PendingAccountID])
GO
ALTER TABLE [dbo].[PendingAccount] ADD CONSTRAINT [FK_PendingAccount_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[PendingAccount] ADD CONSTRAINT [FK_PendingAccount_ClientPersonnel1] FOREIGN KEY ([LinkSentBy]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[PendingAccount] ADD CONSTRAINT [FK_PendingAccount_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
