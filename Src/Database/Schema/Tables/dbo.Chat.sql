CREATE TABLE [dbo].[Chat]
(
[ChatID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ObjectID] [int] NOT NULL,
[ChatGuid] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[ChatTypeID] [tinyint] NOT NULL,
[ObjectTypeID] [tinyint] NOT NULL
)
GO
ALTER TABLE [dbo].[Chat] ADD CONSTRAINT [PK_Chat] PRIMARY KEY CLUSTERED  ([ChatID])
GO
ALTER TABLE [dbo].[Chat] ADD CONSTRAINT [FK_Chat_ChatType] FOREIGN KEY ([ChatTypeID]) REFERENCES [dbo].[ChatType] ([ChatTypeID])
GO
ALTER TABLE [dbo].[Chat] ADD CONSTRAINT [FK_Chat_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[Chat] ADD CONSTRAINT [FK_Chat_DetailFieldSubType] FOREIGN KEY ([ObjectTypeID]) REFERENCES [dbo].[DetailFieldSubType] ([DetailFieldSubTypeID])
GO
