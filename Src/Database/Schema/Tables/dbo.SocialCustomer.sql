CREATE TABLE [dbo].[SocialCustomer]
(
[SocialCustomerID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[SocialTypeID] [int] NULL,
[ScreenName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[SocialUserID] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[WhenCreated] [datetime] NULL,
[WhoCreated] [int] NULL,
[WhenModified] [datetime] NULL,
[WhoModified] [int] NULL
)
GO
ALTER TABLE [dbo].[SocialCustomer] ADD CONSTRAINT [PK_SocialCustomer] PRIMARY KEY CLUSTERED  ([SocialCustomerID])
GO
ALTER TABLE [dbo].[SocialCustomer] ADD CONSTRAINT [FK_SocialCustomer_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[SocialCustomer] ADD CONSTRAINT [FK_SocialCustomer_Customers] FOREIGN KEY ([CustomerID]) REFERENCES [dbo].[Customers] ([CustomerID])
GO
