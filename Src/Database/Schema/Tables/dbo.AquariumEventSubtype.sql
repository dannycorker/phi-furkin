CREATE TABLE [dbo].[AquariumEventSubtype]
(
[AquariumEventSubtypeID] [int] NOT NULL,
[AquariumEventTypeID] [int] NOT NULL,
[AquariumEventSubtypeName] [varchar] (100) COLLATE Latin1_General_CI_AS NOT NULL,
[AquariumEventSubtypeDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[ClientID] [int] NULL,
[StoredProcedure] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[AquariumEventSubtype] ADD CONSTRAINT [PK_AquariumEventSubtype_1] PRIMARY KEY CLUSTERED  ([AquariumEventSubtypeID])
GO
ALTER TABLE [dbo].[AquariumEventSubtype] ADD CONSTRAINT [FK_AquariumEventSubtype_AquariumEventType] FOREIGN KEY ([AquariumEventTypeID]) REFERENCES [dbo].[AquariumEventType] ([AquariumEventTypeID])
GO
