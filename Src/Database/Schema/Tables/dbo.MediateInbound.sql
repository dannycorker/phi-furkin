CREATE TABLE [dbo].[MediateInbound]
(
[MediateInboundRequest] [int] NOT NULL IDENTITY(1, 1),
[ResponseDateTime] [datetime] NOT NULL,
[ResponseBody] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[MediateInbound] ADD CONSTRAINT [PK_MediateInbound] PRIMARY KEY CLUSTERED  ([MediateInboundRequest])
GO
GRANT INSERT ON  [dbo].[MediateInbound] TO [AquariumNet]
GO
GRANT UPDATE ON  [dbo].[MediateInbound] TO [AquariumNet]
GO
