CREATE TABLE [dbo].[Work1]
(
[Work1ID] [int] NOT NULL IDENTITY(1, 1),
[WorkSeedID] [int] NOT NULL,
[CustomerID] [int] NULL,
[LeadID] [int] NULL,
[CaseID] [int] NULL,
[MatterID] [int] NULL,
[Value1] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Value2] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Value3] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Value4] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Value5] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Value6] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Value7] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Value8] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Value9] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Value10] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Value11] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Int1] [int] NULL,
[Int2] [int] NULL,
[Int3] [int] NULL,
[Int4] [int] NULL,
[Int5] [int] NULL,
[Int6] [int] NULL,
[Int7] [int] NULL,
[Int8] [int] NULL,
[Int9] [int] NULL,
[Int10] [int] NULL,
[Int11] [int] NULL
)
GO
ALTER TABLE [dbo].[Work1] ADD CONSTRAINT [PK_Work1] PRIMARY KEY CLUSTERED  ([Work1ID])
GO
CREATE NONCLUSTERED INDEX [IX_Work1Seed] ON [dbo].[Work1] ([WorkSeedID])
GO
CREATE NONCLUSTERED INDEX [IX_Work1Cases] ON [dbo].[Work1] ([WorkSeedID], [CaseID])
GO
CREATE NONCLUSTERED INDEX [IX_Work1Customer] ON [dbo].[Work1] ([WorkSeedID], [CustomerID])
GO
CREATE NONCLUSTERED INDEX [IX_Work1Lead] ON [dbo].[Work1] ([WorkSeedID], [LeadID])
GO
CREATE NONCLUSTERED INDEX [IX_Work1Matter] ON [dbo].[Work1] ([WorkSeedID], [MatterID])
GO
ALTER TABLE [dbo].[Work1] ADD CONSTRAINT [FK_Work1_WorkSeed] FOREIGN KEY ([WorkSeedID]) REFERENCES [dbo].[WorkSeed] ([WorkSeedID]) ON DELETE CASCADE
GO
