CREATE TABLE [dbo].[EventSubtypeDefault]
(
[EventSubtypeDefaultID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[EventSubtypeID] [int] NOT NULL,
[EventTypeID] [int] NOT NULL,
[LeadTypeID] [int] NOT NULL,
[ClientPersonnelID] [int] NULL
)
GO
ALTER TABLE [dbo].[EventSubtypeDefault] ADD CONSTRAINT [PK_EventSubtypeDefault] PRIMARY KEY CLUSTERED  ([EventSubtypeDefaultID])
GO
ALTER TABLE [dbo].[EventSubtypeDefault] ADD CONSTRAINT [FK_EventSubtypeDefault_ClientPersonnel] FOREIGN KEY ([ClientPersonnelID]) REFERENCES [dbo].[ClientPersonnel] ([ClientPersonnelID])
GO
ALTER TABLE [dbo].[EventSubtypeDefault] ADD CONSTRAINT [FK_EventSubtypeDefault_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[EventSubtypeDefault] ADD CONSTRAINT [FK_EventSubtypeDefault_EventSubtype] FOREIGN KEY ([EventSubtypeID]) REFERENCES [dbo].[EventSubtype] ([EventSubtypeID])
GO
ALTER TABLE [dbo].[EventSubtypeDefault] ADD CONSTRAINT [FK_EventSubtypeDefault_EventType] FOREIGN KEY ([EventTypeID]) REFERENCES [dbo].[EventType] ([EventTypeID])
GO
ALTER TABLE [dbo].[EventSubtypeDefault] ADD CONSTRAINT [FK_EventSubtypeDefault_LeadType] FOREIGN KEY ([LeadTypeID]) REFERENCES [dbo].[LeadType] ([LeadTypeID])
GO
