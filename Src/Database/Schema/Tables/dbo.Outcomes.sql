CREATE TABLE [dbo].[Outcomes]
(
[OutcomeID] [int] NOT NULL IDENTITY(1, 1),
[ClientQuestionnaireID] [int] NULL,
[OutcomeName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[OutcomeDescription] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[SmsWhenFound] [bit] NULL,
[mobileNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[EmailCustomer] [bit] NULL,
[CustomersEmail] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ThankYouPage] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[EmailFromAddress] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[SmsToCustomer] [varchar] (160) COLLATE Latin1_General_CI_AS NULL,
[CustomersEmailSubject] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL,
[EmailClient] [bit] NULL,
[ClientEmail] [varchar] (255) COLLATE Latin1_General_CI_AS NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[Outcomes] ADD CONSTRAINT [PK_Outcomes] PRIMARY KEY CLUSTERED  ([OutcomeID])
GO
CREATE NONCLUSTERED INDEX [IX_OutcomesClient] ON [dbo].[Outcomes] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_OutcomesClientQuestionnaire] ON [dbo].[Outcomes] ([ClientQuestionnaireID])
GO
CREATE NONCLUSTERED INDEX [IX_Outcomes_SourceID] ON [dbo].[Outcomes] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[Outcomes] ADD CONSTRAINT [FK_Outcomes_ClientQuestionnaires] FOREIGN KEY ([ClientQuestionnaireID]) REFERENCES [dbo].[ClientQuestionnaires] ([ClientQuestionnaireID])
GO
ALTER TABLE [dbo].[Outcomes] ADD CONSTRAINT [FK_Outcomes_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
