CREATE TABLE [dbo].[Equations]
(
[EquationID] [int] NOT NULL IDENTITY(1, 1),
[EquationName] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Equation] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[ClientQuestionnaireID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[SourceID] [int] NULL,
[WhoCreated] [int] NULL,
[WhenCreated] [datetime] NULL,
[WhoModified] [int] NULL,
[WhenModified] [datetime] NULL
)
GO
ALTER TABLE [dbo].[Equations] ADD CONSTRAINT [PK_Equations] PRIMARY KEY CLUSTERED  ([EquationID])
GO
CREATE NONCLUSTERED INDEX [IX_Equations_SourceID] ON [dbo].[Equations] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[Equations] ADD CONSTRAINT [FK_Equations_ClientQuestionnaires] FOREIGN KEY ([ClientQuestionnaireID]) REFERENCES [dbo].[ClientQuestionnaires] ([ClientQuestionnaireID])
GO
ALTER TABLE [dbo].[Equations] ADD CONSTRAINT [FK_Equations_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
