CREATE TABLE [dbo].[AccountHistory]
(
[AccountHistoryID] [int] NOT NULL IDENTITY(1, 1),
[AccountID] [int] NULL,
[ClientID] [int] NOT NULL,
[CustomerID] [int] NOT NULL,
[AccountHolderName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[FriendlyName] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[AccountNumber] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[Sortcode] [varchar] (20) COLLATE Latin1_General_CI_AS NULL,
[Reference] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[CardToken] [varchar] (250) COLLATE Latin1_General_CI_AS NULL,
[MaskedCardNumber] [varchar] (24) COLLATE Latin1_General_CI_AS NULL,
[ExpiryMonth] [int] NULL,
[ExpiryYear] [int] NULL,
[ExpiryDate] [datetime] NULL,
[AccountTypeID] [int] NULL,
[Active] [bit] NULL,
[WhoCreated] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoModified] [int] NOT NULL,
[WhenModified] [datetime] NOT NULL,
[NextPaymentDate] [datetime] NULL,
[NextPaymentTotal] [numeric] (18, 2) NULL,
[CustomerPaymentScheduleID] [int] NULL,
[OriginalWhoCreated] [int] NOT NULL,
[OriginalWhenCreated] [datetime] NOT NULL,
[OriginalWhoModified] [int] NOT NULL,
[OriginalWhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[AccountHistory] ADD CONSTRAINT [PK_AccountHistory] PRIMARY KEY CLUSTERED  ([AccountHistoryID])
GO
