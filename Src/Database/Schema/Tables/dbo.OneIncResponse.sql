CREATE TABLE [dbo].[OneIncResponse]
(
[OneIncResponseID] [int] NOT NULL IDENTITY(1, 1),
[OneIncRequestID] [int] NULL,
[ClientID] [int] NOT NULL,
[CustomerID] [int] NULL,
[MatterID] [int] NULL,
[ResponseJson] [nvarchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[OneIncResponse] ADD CONSTRAINT [PK_OneIncResponse] PRIMARY KEY CLUSTERED  ([OneIncResponseID])
GO
