CREATE TABLE [dbo].[Script]
(
[ScriptID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadTypeID] [int] NULL,
[ScriptName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[ScriptDescription] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[WhenCreated] [datetime] NULL,
[WhoCreated] [int] NULL,
[WhenModified] [datetime] NULL,
[WhoModified] [int] NULL,
[FinishEventTypeID] [int] NULL,
[Published] [bit] NULL,
[Disabled] [bit] NULL,
[ScriptParentID] [int] NULL,
[StartEventTypeID] [int] NULL,
[ShowNotes] [bit] NULL,
[NumberOfNotesToShow] [int] NULL,
[IntegrateWithDialler] [bit] NULL,
[ApplyFinishEventTo] [int] NULL,
[Redact] [bit] NULL
)
GO
ALTER TABLE [dbo].[Script] ADD CONSTRAINT [PK_Script] PRIMARY KEY CLUSTERED  ([ScriptID])
GO
ALTER TABLE [dbo].[Script] ADD CONSTRAINT [FK_Script_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
