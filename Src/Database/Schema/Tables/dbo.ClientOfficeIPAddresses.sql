CREATE TABLE [dbo].[ClientOfficeIPAddresses]
(
[ClientOfficeIpAddressID] [int] NOT NULL IDENTITY(1, 1),
[ClientOfficeID] [int] NOT NULL,
[ClientOfficeIPAddress] [char] (15) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL,
[Notes] [varchar] (200) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[ClientOfficeIPAddresses] ADD CONSTRAINT [PK_ClientOfficeIPAddresses] PRIMARY KEY CLUSTERED  ([ClientOfficeIpAddressID])
GO
CREATE NONCLUSTERED INDEX [IX_ClientOfficeIPAddresses] ON [dbo].[ClientOfficeIPAddresses] ([ClientOfficeID])
GO
ALTER TABLE [dbo].[ClientOfficeIPAddresses] ADD CONSTRAINT [FK_ClientOfficeIPAddresses_ClientOffices] FOREIGN KEY ([ClientOfficeID]) REFERENCES [dbo].[ClientOffices] ([ClientOfficeID])
GO
ALTER TABLE [dbo].[ClientOfficeIPAddresses] ADD CONSTRAINT [FK_ClientOfficeIPAddresses_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
