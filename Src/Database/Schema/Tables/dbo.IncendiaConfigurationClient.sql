CREATE TABLE [dbo].[IncendiaConfigurationClient]
(
[IncendiaConfigurationClientID] [int] NOT NULL IDENTITY(1, 1),
[AquariumClientID] [int] NOT NULL,
[Username] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[Password] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[URI] [varchar] (150) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
ALTER TABLE [dbo].[IncendiaConfigurationClient] ADD CONSTRAINT [PK_IncendiaConfigurationClient] PRIMARY KEY CLUSTERED  ([IncendiaConfigurationClientID])
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_IncendiaConfigurationClient] ON [dbo].[IncendiaConfigurationClient] ([AquariumClientID])
GO
