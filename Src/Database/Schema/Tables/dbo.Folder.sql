CREATE TABLE [dbo].[Folder]
(
[FolderID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[FolderTypeID] [int] NOT NULL,
[FolderParentID] [int] NULL,
[FolderName] [varchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
[FolderDescription] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NOT NULL,
[Personal] [bit] NOT NULL CONSTRAINT [DF__Folder__Personal__695C9DA1] DEFAULT ((0)),
[SourceID] [int] NULL
)
GO
ALTER TABLE [dbo].[Folder] ADD CONSTRAINT [PK_Folder] PRIMARY KEY CLUSTERED  ([FolderID])
GO
CREATE NONCLUSTERED INDEX [IX_Folder_SourceID] ON [dbo].[Folder] ([SourceID]) WHERE ([SourceID]>(0))
GO
ALTER TABLE [dbo].[Folder] ADD CONSTRAINT [FK_Folder_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Folder] ADD CONSTRAINT [FK_Folder_Folder] FOREIGN KEY ([FolderParentID]) REFERENCES [dbo].[Folder] ([FolderID])
GO
ALTER TABLE [dbo].[Folder] ADD CONSTRAINT [FK_Folder_FolderType] FOREIGN KEY ([FolderTypeID]) REFERENCES [dbo].[FolderType] ([FolderTypeID])
GO
