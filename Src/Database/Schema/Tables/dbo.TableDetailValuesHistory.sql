CREATE TABLE [dbo].[TableDetailValuesHistory]
(
[TableDetailValuesHistoryID] [int] NOT NULL IDENTITY(2, 1),
[TableRowID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[ClientPersonnelID] [int] NULL,
[CustomerID] [int] NULL,
[ContactID] [int] NULL,
[LeadID] [int] NULL,
[CaseID] [int] NULL,
[MatterID] [int] NULL,
[DetailValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[EncryptedValue] [varchar] (3000) COLLATE Latin1_General_CI_AS NULL,
[ValueInt] [int] NULL,
[ValueMoney] [money] NULL,
[ValueDate] [date] NULL,
[ValueDateTime] [datetime2] (0) NULL,
[WhenSaved] [datetime] NOT NULL,
[WhoSaved] [int] NOT NULL,
[ResourceListID] [int] NULL
)
GO
ALTER TABLE [dbo].[TableDetailValuesHistory] ADD CONSTRAINT [PK_TableDetailValuesHistory] PRIMARY KEY CLUSTERED  ([TableDetailValuesHistoryID])
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValuesHistory_Case] ON [dbo].[TableDetailValuesHistory] ([CaseID])
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValuesHistory_Client] ON [dbo].[TableDetailValuesHistory] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValuesHistory_Customer] ON [dbo].[TableDetailValuesHistory] ([CustomerID])
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValuesHistory_DetailField] ON [dbo].[TableDetailValuesHistory] ([DetailFieldID])
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValuesHistory_Lead] ON [dbo].[TableDetailValuesHistory] ([LeadID])
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValuesHistory_Matter] ON [dbo].[TableDetailValuesHistory] ([MatterID])
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValuesHistory_TableRow] ON [dbo].[TableDetailValuesHistory] ([TableRowID])
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValuesHistory_ValueDate] ON [dbo].[TableDetailValuesHistory] ([ValueDate])
GO
CREATE NONCLUSTERED INDEX [IX_TableDetailValuesHistory_ValueInt] ON [dbo].[TableDetailValuesHistory] ([ValueInt])
GO
