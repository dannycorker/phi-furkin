CREATE TABLE [dbo].[PageControlText]
(
[PageControlTextID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LanguageID] [int] NOT NULL,
[PageName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[ControlName] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[ControlText] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[TooltipText] [varchar] (max) COLLATE Latin1_General_CI_AS NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhenModified] [datetime] NOT NULL
)
GO
ALTER TABLE [dbo].[PageControlText] ADD CONSTRAINT [PK_PageControlText] PRIMARY KEY CLUSTERED  ([PageControlTextID])
GO
CREATE NONCLUSTERED INDEX [IX_PageControlText_ClientLanguagePage] ON [dbo].[PageControlText] ([ClientID], [LanguageID], [PageName])
GO
CREATE NONCLUSTERED INDEX [IX_PageControlText_ClientLanguagePageControl] ON [dbo].[PageControlText] ([ClientID], [LanguageID], [PageName], [ControlName])
GO
ALTER TABLE [dbo].[PageControlText] ADD CONSTRAINT [FK_PageControlText_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[PageControlText] ADD CONSTRAINT [FK_PageControlText_Language] FOREIGN KEY ([LanguageID]) REFERENCES [dbo].[Language] ([LanguageID])
GO
