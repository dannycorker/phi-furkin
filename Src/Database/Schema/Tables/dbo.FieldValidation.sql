CREATE TABLE [dbo].[FieldValidation]
(
[FieldValidationID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[FieldID] [int] NOT NULL,
[ValidationGroup] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ValidationType] [int] NOT NULL,
[MaxValue] [int] NULL,
[MinValue] [int] NULL,
[ErrorMessage] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[AccessLevelRestrictions] [varchar] (50) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[FieldValidation] ADD CONSTRAINT [PK_MatterValidation] PRIMARY KEY CLUSTERED  ([FieldValidationID])
GO
ALTER TABLE [dbo].[FieldValidation] ADD CONSTRAINT [FK_MatterValidation_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
ALTER TABLE [dbo].[FieldValidation] ADD CONSTRAINT [FK_MatterValidation_DetailFields] FOREIGN KEY ([FieldID]) REFERENCES [dbo].[DetailFields] ([DetailFieldID])
GO
