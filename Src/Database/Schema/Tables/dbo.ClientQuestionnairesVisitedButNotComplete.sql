CREATE TABLE [dbo].[ClientQuestionnairesVisitedButNotComplete]
(
[ClientQuestionnairesVisitedButNotCompleteID] [int] NOT NULL IDENTITY(1, 1),
[ClientQuestionnaireID] [int] NULL,
[VisitedDate] [datetime] NULL,
[IPAddress] [varchar] (50) COLLATE Latin1_General_CI_AS NULL,
[ClientID] [int] NOT NULL
)
GO
ALTER TABLE [dbo].[ClientQuestionnairesVisitedButNotComplete] ADD CONSTRAINT [PK_ClientQuestionnairesVisitedButNotComplete] PRIMARY KEY CLUSTERED  ([ClientQuestionnairesVisitedButNotCompleteID])
GO
ALTER TABLE [dbo].[ClientQuestionnairesVisitedButNotComplete] ADD CONSTRAINT [FK_ClientQuestionnairesVisitedButNotComplete_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ClientID])
GO
