CREATE TABLE [dbo].[DocumentZipInformation]
(
[DocumentZipInformationID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[ServerMapPath] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[SecurePath] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[HostPath] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[WordTempDirectory] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[DocumentsPerZip] [int] NOT NULL,
[ZipPrefix] [varchar] (1000) COLLATE Latin1_General_CI_AS NOT NULL,
[StatusID] [int] NOT NULL,
[TriggeredByAutomatedTaskID] [int] NULL,
[TriggeredByDocumentTypeID] [int] NULL,
[WhoStored] [int] NULL,
[WhenStored] [datetime] NULL,
[WhenCreated] [datetime] NULL,
[EmailOnSuccess] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[EmailOnError] [varchar] (500) COLLATE Latin1_General_CI_AS NULL
)
GO
ALTER TABLE [dbo].[DocumentZipInformation] ADD CONSTRAINT [PK_DocumentZipInformation] PRIMARY KEY CLUSTERED  ([DocumentZipInformationID])
GO
CREATE NONCLUSTERED INDEX [IX_DocumentZipInformationClient] ON [dbo].[DocumentZipInformation] ([ClientID])
GO
CREATE NONCLUSTERED INDEX [IX_DocumentZipInformationStatus] ON [dbo].[DocumentZipInformation] ([StatusID])
GO
