CREATE TYPE [dbo].[tvpBankLoad_OLD] AS TABLE
(
[FirstName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Reference] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Account] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Amount] [money] NULL,
[Description] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpBankLoad_OLD] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpBankLoad_OLD] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpBankLoad_OLD] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpBankLoad_OLD] TO [sp_executeall]
GO
