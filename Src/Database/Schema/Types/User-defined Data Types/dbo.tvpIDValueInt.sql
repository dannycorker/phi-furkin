CREATE TYPE [dbo].[tvpIDValueInt] AS TABLE
(
[AnyID] [int] NOT NULL,
[AnyValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[AnyInt] [int] NULL,
PRIMARY KEY CLUSTERED  ([AnyID])
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpIDValueInt] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpIDValueInt] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpIDValueInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpIDValueInt] TO [sp_executeall]
GO
