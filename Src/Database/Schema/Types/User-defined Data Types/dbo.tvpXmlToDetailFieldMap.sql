CREATE TYPE [dbo].[tvpXmlToDetailFieldMap] AS TABLE
(
[DetailFieldID] [int] NOT NULL,
[AttributeName] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[XmlDataType] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[DecodeResourceListDetailFieldID] [int] NULL,
[ValueIfNull] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpXmlToDetailFieldMap] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpXmlToDetailFieldMap] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpXmlToDetailFieldMap] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpXmlToDetailFieldMap] TO [sp_executeall]
GO
