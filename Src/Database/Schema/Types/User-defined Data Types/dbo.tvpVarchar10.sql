CREATE TYPE [dbo].[tvpVarchar10] AS TABLE
(
[ID] [int] NULL,
[Val1] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val2] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val3] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val4] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val5] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val6] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val7] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val8] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val9] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val10] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpVarchar10] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpVarchar10] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpVarchar10] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpVarchar10] TO [sp_executeall]
GO
