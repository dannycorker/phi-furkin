CREATE TYPE [dbo].[tvpIntMoney] AS TABLE
(
[ID] [int] NOT NULL,
[Value] [money] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpIntMoney] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpIntMoney] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpIntMoney] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpIntMoney] TO [sp_executeall]
GO
