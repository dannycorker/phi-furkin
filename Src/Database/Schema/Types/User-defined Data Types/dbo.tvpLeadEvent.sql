CREATE TYPE [dbo].[tvpLeadEvent] AS TABLE
(
[LeadEventID] [int] NOT NULL IDENTITY(1, 1),
[ClientID] [int] NOT NULL,
[LeadID] [int] NOT NULL,
[WhenCreated] [datetime] NOT NULL,
[WhoCreated] [int] NULL,
[Cost] [money] NULL,
[Comments] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[EventTypeID] [int] NULL,
[NoteTypeID] [int] NULL,
[FollowupDateTime] [datetime] NULL,
[WhenFollowedUp] [datetime] NULL,
[AquariumEventType] [int] NULL,
[NextEventID] [int] NULL,
[CaseID] [int] NULL,
[LeadDocumentID] [int] NULL,
[NotePriority] [int] NULL,
[DocumentQueueID] [int] NULL,
[EventDeleted] [bit] NOT NULL,
[WhoDeleted] [int] NULL,
[DeletionComments] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ContactID] [int] NULL,
[BaseCost] [money] NULL,
[DisbursementCost] [money] NULL,
[DisbursementDescription] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[ChargeOutRate] [money] NULL,
[UnitsOfEffort] [int] NULL,
[CostEnteredManually] [bit] NULL,
[IsOnHold] [bit] NULL,
[HoldLeadEventID] [int] NULL,
[LeadEventIDToFollowUp] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpLeadEvent] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpLeadEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpLeadEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpLeadEvent] TO [sp_executeall]
GO
