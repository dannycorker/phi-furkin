CREATE TYPE [dbo].[tvpObjectWhere] AS TABLE
(
[DetailFieldID] [int] NOT NULL,
[ComparisonTypeID] [int] NOT NULL,
[Value] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[OrGroup] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpObjectWhere] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpObjectWhere] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpObjectWhere] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpObjectWhere] TO [sp_executeall]
GO
