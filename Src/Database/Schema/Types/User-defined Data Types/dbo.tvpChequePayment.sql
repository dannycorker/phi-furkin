CREATE TYPE [dbo].[tvpChequePayment] AS TABLE
(
[ChequeNumber] [int] NULL,
[Amount] [money] NULL,
[PayeeFullname] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AddresseeFullname] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AddressLine1] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AddressLine2] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AddressLine3] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AddressTown] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AddressCounty] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AddressPostcode] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AccountRefNo] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ParagraphText] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[EntryDate] [date] NULL,
[Reference] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[PaymentReference] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[IncidentDate] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[InsuredName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpChequePayment] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpChequePayment] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpChequePayment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpChequePayment] TO [sp_executeall]
GO
