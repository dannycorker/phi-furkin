CREATE TYPE [dbo].[tvpDetailValueData] AS TABLE
(
[DetailValueID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[DetailFieldID] [int] NOT NULL,
[DetailValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[EncryptedValue] [varchar] (3000) COLLATE Latin1_General_CI_AS NULL,
[ObjectID] [int] NOT NULL,
[DetailFieldSubType] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpDetailValueData] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpDetailValueData] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpDetailValueData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpDetailValueData] TO [sp_executeall]
GO
