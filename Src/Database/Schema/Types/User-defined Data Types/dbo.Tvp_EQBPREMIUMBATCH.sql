CREATE TYPE [dbo].[Tvp_EQBPREMIUMBATCH] AS TABLE
(
[ID] [uniqueidentifier] NULL,
[BrandID] [int] NULL,
[BreedID] [int] NULL,
[GenderID] [int] NULL,
[BirthDate] [datetime2] NULL,
[StartDate] [datetime2] NULL,
[IsNeutered] [bit] NULL,
[MicrochipNumber] [varchar] (500) COLLATE Latin1_General_CI_AS NULL,
[PurchasePrice] [money] NULL,
[PolicySchemeID] [int] NULL,
[Postcode] [varchar] (10) COLLATE Latin1_General_CI_AS NULL,
[TotalPetsInRequest] [int] NULL,
[PetNumber] [int] NULL,
[DiscountCode] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[County] [varchar] (200) COLLATE Latin1_General_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[Tvp_EQBPREMIUMBATCH] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[Tvp_EQBPREMIUMBATCH] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[Tvp_EQBPREMIUMBATCH] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[Tvp_EQBPREMIUMBATCH] TO [sp_executeall]
GO
