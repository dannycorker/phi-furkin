CREATE TYPE [dbo].[tvpTablePropertyValue] AS TABLE
(
[Table] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Property] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Value] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[ID] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpTablePropertyValue] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpTablePropertyValue] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpTablePropertyValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpTablePropertyValue] TO [sp_executeall]
GO
