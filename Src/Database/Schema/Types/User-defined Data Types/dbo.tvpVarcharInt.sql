CREATE TYPE [dbo].[tvpVarcharInt] AS TABLE
(
[AnyValue] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[AnyID] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpVarcharInt] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpVarcharInt] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpVarcharInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpVarcharInt] TO [sp_executeall]
GO
