CREATE TYPE [dbo].[tvpObject] AS TABLE
(
[ObjectID] [int] NOT NULL,
[ObjectTypeID] [int] NOT NULL,
[Name] [varchar] (250) COLLATE Latin1_General_CI_AS NOT NULL,
[Details] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpObject] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpObject] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpObject] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpObject] TO [sp_executeall]
GO
