CREATE TYPE [dbo].[tvpDetailValue] AS TABLE
(
[DetailFieldID] [int] NOT NULL,
[ClientID] [int] NOT NULL,
[DetailFieldSubTypeID] [int] NOT NULL,
[AnyID] [int] NOT NULL,
[DetailValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpDetailValue] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpDetailValue] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpDetailValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpDetailValue] TO [sp_executeall]
GO
