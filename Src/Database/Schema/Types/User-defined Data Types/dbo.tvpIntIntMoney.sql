CREATE TYPE [dbo].[tvpIntIntMoney] AS TABLE
(
[AnyID1] [int] NULL,
[AnyID2] [int] NULL,
[ValueMoney] [money] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpIntIntMoney] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpIntIntMoney] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpIntIntMoney] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpIntIntMoney] TO [sp_executeall]
GO
