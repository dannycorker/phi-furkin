CREATE TYPE [dbo].[tvpTransfersLoad] AS TABLE
(
[CustomerRef] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[FirstName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[FriendlyName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[SortCode] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[AccountNumber] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Amount] [money] NULL,
[Reference] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[RTI] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Date] [date] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpTransfersLoad] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpTransfersLoad] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpTransfersLoad] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpTransfersLoad] TO [sp_executeall]
GO
