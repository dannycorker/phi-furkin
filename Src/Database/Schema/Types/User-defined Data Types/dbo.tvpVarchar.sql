CREATE TYPE [dbo].[tvpVarchar] AS TABLE
(
[AnyValue] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpVarchar] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpVarchar] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpVarchar] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpVarchar] TO [sp_executeall]
GO
