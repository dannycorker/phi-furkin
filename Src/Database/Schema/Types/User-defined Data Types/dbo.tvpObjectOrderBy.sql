CREATE TYPE [dbo].[tvpObjectOrderBy] AS TABLE
(
[DetailFieldID] [int] NOT NULL,
[OrderDirectionID] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpObjectOrderBy] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpObjectOrderBy] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpObjectOrderBy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpObjectOrderBy] TO [sp_executeall]
GO
