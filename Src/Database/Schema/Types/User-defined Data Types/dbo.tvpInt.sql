CREATE TYPE [dbo].[tvpInt] AS TABLE
(
[AnyID] [int] NOT NULL,
PRIMARY KEY CLUSTERED  ([AnyID])
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpInt] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpInt] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpInt] TO [sp_executeall]
GO
