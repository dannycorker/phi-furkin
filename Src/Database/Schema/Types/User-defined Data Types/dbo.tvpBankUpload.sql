CREATE TYPE [dbo].[tvpBankUpload] AS TABLE
(
[CustomerRef] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[FirstName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[LastName] [varchar] (201) COLLATE Latin1_General_CI_AS NULL,
[FriendlyName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[BankName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[BranchLocation] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[SortCode] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AccountNumber] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AccountName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[Reference] [varchar] (100) COLLATE Latin1_General_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpBankUpload] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpBankUpload] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpBankUpload] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpBankUpload] TO [sp_executeall]
GO
