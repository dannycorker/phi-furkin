CREATE TYPE [dbo].[tvpVarchar20] AS TABLE
(
[ID] [int] NULL,
[ClientID] [int] NULL,
[SubClientID] [int] NULL,
[CustomerID] [int] NULL,
[LeadID] [int] NULL,
[CaseID] [int] NULL,
[MatterID] [int] NULL,
[TableRowID] [int] NULL,
[Val1] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val2] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val3] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val4] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val5] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val6] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val7] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val8] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val9] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val10] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val11] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val12] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val13] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val14] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val15] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val16] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val17] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val18] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val19] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Val20] [varchar] (max) COLLATE Latin1_General_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpVarchar20] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpVarchar20] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpVarchar20] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpVarchar20] TO [sp_executeall]
GO
