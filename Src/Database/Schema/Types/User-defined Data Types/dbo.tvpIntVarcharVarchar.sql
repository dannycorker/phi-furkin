CREATE TYPE [dbo].[tvpIntVarcharVarchar] AS TABLE
(
[AnyID] [int] NOT NULL,
[AnyValue1] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[AnyValue2] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpIntVarcharVarchar] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpIntVarcharVarchar] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpIntVarcharVarchar] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpIntVarcharVarchar] TO [sp_executeall]
GO
