CREATE TYPE [dbo].[tvpCalendarEvent] AS TABLE
(
[EventID] [int] NOT NULL,
[Name] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Description] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[FromTime] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[ToTime] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[StatusID] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[ReqEmpID] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[ReqDate] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[StatusEmpID] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[StatusDate] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[WPEventID] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[PaidTypeID] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[SickTypeID] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[CostCentreID] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[Location] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[CostTypeID] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[CostRate] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[CostUnit] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[CostAmount] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[AssetID] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[TypeID] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[AllDay] [varchar] (2000) COLLATE Latin1_General_CI_AS NOT NULL,
[StatusDesc] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[DurationUnit] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Duration] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[SubClientID] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[SourceID] [int] NOT NULL,
[Operation] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpCalendarEvent] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpCalendarEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpCalendarEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpCalendarEvent] TO [sp_executeall]
GO
