CREATE TYPE [dbo].[tvpObjectSearch] AS TABLE
(
[AnyID] [int] NOT NULL,
[ColumnDetailFieldID] [int] NULL,
[SearchableObjectTypeID] [int] NOT NULL,
[OrGroupID] [int] NULL,
[SearchValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[OutputHeader] [varchar] (250) COLLATE Latin1_General_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpObjectSearch] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpObjectSearch] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpObjectSearch] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpObjectSearch] TO [sp_executeall]
GO
