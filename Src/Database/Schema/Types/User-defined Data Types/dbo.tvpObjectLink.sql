CREATE TYPE [dbo].[tvpObjectLink] AS TABLE
(
[ObjectTypeRelationshipID] [int] NOT NULL,
[FromObjectID] [int] NOT NULL,
[ToObjectID] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpObjectLink] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpObjectLink] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpObjectLink] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpObjectLink] TO [sp_executeall]
GO
