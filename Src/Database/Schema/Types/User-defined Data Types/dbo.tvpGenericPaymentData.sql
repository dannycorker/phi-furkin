CREATE TYPE [dbo].[tvpGenericPaymentData] AS TABLE
(
[PaymentGroupType] [varchar] (30) COLLATE Latin1_General_CI_AS NULL,
[OrginatorAccountName] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[OrginatorAccountSortCode] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[OrginatorAccountNumber] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ServiceUserNumber] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[OrginatorNarrative] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[DestinationNarrative] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[SettlementDate] [date] NULL,
[AccountName] [varchar] (200) COLLATE Latin1_General_CI_AS NULL,
[SortCode] [varchar] (6) COLLATE Latin1_General_CI_AS NULL,
[AccountNumber] [varchar] (8) COLLATE Latin1_General_CI_AS NULL,
[BacsTransactionCode] [varchar] (2) COLLATE Latin1_General_CI_AS NULL,
[Amount] [money] NULL,
[Reference] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ChequeProfileKey] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ChequeNumber] [int] NULL,
[PayeeFullname] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AddresseeFullname] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AddressLine1] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AddressLine2] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AddressLine3] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AddressCounty] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[AddressPostcode] [varchar] (100) COLLATE Latin1_General_CI_AS NULL,
[ParagraphText] [varchar] (max) COLLATE Latin1_General_CI_AS NULL,
[TableRowID] [int] NULL,
[MatterID] [int] NULL,
[LeadID] [int] NULL,
[CustomerID] [int] NULL,
[ScratchText] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
[Done] [int] NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpGenericPaymentData] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpGenericPaymentData] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpGenericPaymentData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpGenericPaymentData] TO [sp_executeall]
GO
