CREATE TYPE [dbo].[tvpIntVarchar] AS TABLE
(
[AnyID] [int] NOT NULL,
[AnyValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpIntVarchar] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpIntVarchar] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpIntVarchar] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpIntVarchar] TO [sp_executeall]
GO
