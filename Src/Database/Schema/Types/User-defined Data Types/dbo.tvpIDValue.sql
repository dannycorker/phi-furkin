CREATE TYPE [dbo].[tvpIDValue] AS TABLE
(
[AnyID] [int] NOT NULL,
[AnyValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL,
PRIMARY KEY CLUSTERED  ([AnyID])
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpIDValue] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpIDValue] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpIDValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpIDValue] TO [sp_executeall]
GO
