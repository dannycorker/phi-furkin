CREATE TYPE [dbo].[tvpIntInt] AS TABLE
(
[ID1] [int] NOT NULL,
[ID2] [int] NOT NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpIntInt] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpIntInt] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpIntInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpIntInt] TO [sp_executeall]
GO
