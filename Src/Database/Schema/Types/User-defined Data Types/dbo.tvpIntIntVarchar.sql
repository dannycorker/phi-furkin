CREATE TYPE [dbo].[tvpIntIntVarchar] AS TABLE
(
[AnyID1] [int] NOT NULL,
[AnyID2] [int] NULL,
[AnyValue] [varchar] (2000) COLLATE Latin1_General_CI_AS NULL
)
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpIntIntVarchar] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpIntIntVarchar] TO [ReadOnly]
GO
GRANT EXECUTE ON TYPE:: [dbo].[tvpIntIntVarchar] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON TYPE:: [dbo].[tvpIntIntVarchar] TO [sp_executeall]
GO
