SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Conor Reeves
-- Create date: 2019-06-18
-- Description:	To concatenate the PetBreed into a single string 
-- 2019-07-16 CR updated the function to resolve the issue with the commas. 
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_ConcatPetBreed]
(
	@CustomerID	INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE @MyStringOfPetBreed VARCHAR(2000) = ''

	SELECT @MyStringOfPetBreed += rdv.DetailValue + ', ' 
	FROM Lead l WITH ( NOLOCK )
	INNER JOIN LeadDetailValues pet_type WITH ( NOLOCK ) ON pet_type.LeadID = l.LeadID AND pet_type.DetailFieldID = 144272  -- Pet Type
	INNER JOIN dbo.ResourceListDetailValues rdv WITH ( NOLOCK ) on rdv.ResourceListID = pet_type.ValueInt AND rdv.DetailFieldID = 144270 /*Pet Breed*/
	INNER JOIN dbo.Matter m WITH ( NOLOCK ) on m.LeadID = l.LeadID
	INNER JOIN MatterDetailValues PolicyStatus WITH (NOLOCK) on PolicyStatus.MatterID = m.MatterID AND PolicyStatus.DetailFieldID = 170038 --Policy Status
	INNER JOIN MatterDetailValues OMFOutcomes	WITH (NOLOCK) on OMFOutcomes.MatterID = m.MatterID AND OMFOutcomes.DetailFieldID = 179964 -- OMF Outcomes
	WHERE l.CustomerID = @CustomerID
	and m.MatterRef = 'One Month Free'
	AND PolicyStatus.ValueInt not in (43003,43004,74535,76595) -- Cancelled Lapsed CancelPending and Void
	and OMFOutcomes.ValueInt not in (76418)

	IF @@ROWCOUNT > 0
	BEGIN
		 SELECT @MyStringOfPetBreed =  LEFT(@MyStringOfPetBreed,LEN(@MyStringOfPetBreed)-1)
	END
	
	RETURN @MyStringOfPetBreed

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_ConcatPetBreed] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_ConcatPetBreed] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_ConcatPetBreed] TO [sp_executeall]
GO
