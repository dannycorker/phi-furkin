SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author	: Aquarium (2020-08-20)
-- Jira		: [SDPRU-9]
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_Policy_GetPolicyExcessOrDefault]
(
	@ClientID INT,
	@PolicyMatterID INT,
	@PostCodeGroup VARCHAR(2),
	@Breed VARCHAR(2000),
	@BirthDate DATE,
	@DateOfLoss DATE,
	@SpeciesID INT,
	@VoluntaryExcess MONEY,
	@TreatmentDate DATE,
	@PolicyAnniversary DATE,
	@PolicyAgeStart INT,
	@PolicyAgeEnd INT,
	@Excess MONEY
)
RETURNS MONEY
BEGIN

	DECLARE @VetFeesID INT = dbo.fn_C00_1272_GetVetFeesResourceList(@ClientID)

	DECLARE @DefaultExcess MONEY = NULL,
			@ExcessOptions VARCHAR(2000) = NULL
	SELECT	TOP 1 
			@DefaultExcess = CASE WHEN [exs].ExcessPercentage > 0 THEN [exs].ExcessPercentage ELSE Excess END,
			@ExcessOptions = [exs].Options
	FROM dbo.fn_C00_1272_Policy_GetPolicyExcessWithData(@PolicyMatterID, @PostCodeGroup, @Breed, @BirthDate, @DateOfLoss, @SpeciesID, 0, @TreatmentDate, @PolicyAnniversary, @PolicyAgeStart, @Excess) [exs]
	WHERE [exs].ResourceListID = @VetFeesID

	-- If no value has been passed in or if that value is not in the options then default
	IF @Excess IS NULL OR NOT EXISTS (
		SELECT	* 
		FROM	dbo.fnTableOfValuesFromCSV(@ExcessOptions)
		WHERE	AnyValue = @Excess
	)
	BEGIN
		SELECT @Excess = @DefaultExcess
	END

	RETURN @Excess

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPolicyExcessOrDefault] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_Policy_GetPolicyExcessOrDefault] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPolicyExcessOrDefault] TO [sp_executeall]
GO
