SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-12-02
-- Description:	Returns any multi-pet discount due for the policy
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetDiscount]
(
	@SchemeID INT,
	@StartDate DATE,
	@Net MONEY,
	@PetNumber INT = 0
)
RETURNS MONEY	
AS
BEGIN

--declare
--	@SchemeID INT =121569,
--	@StartDate DATE = '2015-01-20',
--	@Net MONEY = 100,
--	@PetNumber INT = 0

	
	DECLARE @Discount MONEY = 0,
			@Gross MONEY


	-- Get the discount appropriate for the scheme
	SELECT @Discount=disc.ValueMoney
	FROM MatterDetailValues scheme 
	INNER JOIN TableRows tr WITH (NOLOCK) ON scheme.MatterID=tr.MatterID AND tr.DetailFieldID=175437
	INNER JOIN TableDetailValues from_date WITH (NOLOCK) ON tr.TableRowID=from_date.TableRowID AND from_date.DetailFieldID=175433
	INNER JOIN TableDetailValues to_date WITH (NOLOCK) ON tr.TableRowID=to_date.TableRowID AND to_date.DetailFieldID=175434
	INNER JOIN TableDetailValues np WITH (NOLOCK) ON tr.TableRowID=np.TableRowID AND np.DetailFieldID=175435
	INNER JOIN TableDetailValues disc WITH (NOLOCK) ON tr.TableRowID=disc.TableRowID AND disc.DetailFieldID=175436
	WHERE scheme.DetailFieldID=145689 AND scheme.ValueInt=@SchemeID
	AND (from_date.ValueDate IS NULL OR @StartDate >= from_date.ValueDate)
	AND (to_date.ValueDate IS NULL OR @StartDate <= to_date.ValueDate)
	AND np.ValueInt=@PetNumber
	
	-- no discount. Find the discount for the max number of pets that is less than the number passed in.
	IF @Discount=0
	BEGIN
	
		SELECT TOP 1 @Discount=disc.ValueMoney
		FROM MatterDetailValues scheme 
		INNER JOIN TableRows tr WITH (NOLOCK) ON scheme.MatterID=tr.MatterID AND tr.DetailFieldID=175437
		INNER JOIN TableDetailValues from_date WITH (NOLOCK) ON tr.TableRowID=from_date.TableRowID AND from_date.DetailFieldID=175433
		INNER JOIN TableDetailValues to_date WITH (NOLOCK) ON tr.TableRowID=to_date.TableRowID AND to_date.DetailFieldID=175434
		INNER JOIN TableDetailValues np WITH (NOLOCK) ON tr.TableRowID=np.TableRowID AND np.DetailFieldID=175435
		INNER JOIN TableDetailValues disc WITH (NOLOCK) ON tr.TableRowID=disc.TableRowID AND disc.DetailFieldID=175436
		WHERE scheme.DetailFieldID=145689 AND scheme.ValueInt=@SchemeID
		AND (from_date.ValueDate IS NULL OR @StartDate >= from_date.ValueDate)
		AND (to_date.ValueDate IS NULL OR @StartDate <= to_date.ValueDate)
		AND np.ValueInt<@PetNumber
		ORDER BY np.ValueInt DESC

	END

	SELECT @Gross = @NET * ISNULL(@Discount,0) / 100

	RETURN ROUND(@Gross,2)
 
END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetDiscount] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetDiscount] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetDiscount] TO [sp_executeall]
GO
