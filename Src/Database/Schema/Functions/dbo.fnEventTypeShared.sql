SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-07-21
-- Description:	Merges in shared lead types
-- =============================================
CREATE FUNCTION [dbo].[fnEventTypeShared] 
(	
	@ClientID INT
)
RETURNS TABLE 
AS
RETURN 
(	
	SELECT [EventTypeID]
		  ,CASE WHEN s.LeadTypeShareID IS NULL THEN et.ClientID ELSE s.ClientID END AS ClientID
		  ,[EventTypeName]
		  ,[EventTypeDescription]
		  ,[Enabled]
		  ,[UnitsOfEffort]
		  ,[FollowupTimeUnitsID]
		  ,[FollowupQuantity]
		  ,[AvailableManually]
		  ,[StatusAfterEvent]
		  ,[AquariumEventAfterEvent]
		  ,[EventSubtypeID]
		  ,[DocumentTypeID]
		  ,CASE WHEN s.LeadTypeShareID IS NULL THEN et.LeadTypeID ELSE s.SharedTo END AS LeadTypeID
		  ,[AllowCustomTimeUnits]
		  ,[InProcess]
		  ,[KeyEvent]
		  ,[UseEventCosts]
		  ,[UseEventUOEs]
		  ,[UseEventDisbursements]
		  ,[UseEventComments]
		  ,[SignatureRequired]
		  ,[SignatureOverride]
		  ,[VisioX]
		  ,[VisioY]
		  ,[AquariumEventSubtypeID]
		  ,[WhoCreated]
		  ,[WhenCreated]
		  ,[WhoModified]
		  ,[WhenModified]
		  ,[FollowupWorkingDaysOnly]
		  ,[CalculateTableRows]
		  ,[SourceID]
		  ,[SmsGatewayID]
		  ,CASE WHEN s.LeadTypeShareID IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS IsShared
		  ,SocialFeedID
	FROM dbo.EventType et WITH (NOLOCK)
	LEFT JOIN dbo.LeadTypeShare s WITH (NOLOCK) ON et.LeadTypeID = s.SharedFrom AND s.ClientID = @ClientID
)
GO
GRANT VIEW DEFINITION ON  [dbo].[fnEventTypeShared] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnEventTypeShared] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnEventTypeShared] TO [sp_executeall]
GO
