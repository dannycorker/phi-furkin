SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 14-July-2009
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[fnGetDvAsMoney]
(
	@DetailFieldID		int,
	@CaseID				int
)
RETURNS money
AS
BEGIN

DECLARE @DetailValue money,
		@LeadOrMatter int

	Select @LeadOrMatter = LeadOrmatter
	From DetailFields with (nolock) 
	Where DetailFieldID = @DetailFieldID

	If @LeadOrMatter = 1
	Begin

		Select @DetailValue = ldv.ValueMoney 
		From Cases c with (nolock) 
		Inner Join LeadDetailValues ldv with (nolock) on c.LeadID = ldv.LeadID and ldv.DetailFieldID = @DetailFieldID
		Where c.CaseID = @CaseID 
	
	End

	If @LeadOrMatter = 2
	Begin

		Select @DetailValue = mdv.ValueMoney 
		From Matter m with (nolock) 
		Inner Join MatterDetailValues mdv with (nolock) on m.MatterID = mdv.MatterID and mdv.DetailFieldID = @DetailFieldID
		Where m.CaseID = @CaseID
	
	End

	If @LeadOrMatter = 10
	Begin

		Select @DetailValue = cdv.ValueMoney 
		From Matter m with (nolock) 
		Inner Join CustomerDetailValues cdv with (nolock) on m.CustomerID = cdv.CustomerID and cdv.DetailFieldID = @DetailFieldID
		Where m.CaseID = @CaseID
	
	End
	
	IF @LeadOrMatter = 11
	BEGIN

		SELECT @DetailValue = cdv.ValueMoney
		FROM CaseDetailValues cdv WITH (NOLOCK) 
		WHERE cdv.CaseID = @CaseID
		and cdv.DetailFieldID = @DetailFieldID

	END

	IF @LeadOrMatter = 12
	BEGIN

		SELECT @DetailValue = cdv.ValueMoney
		FROM Cases c WITH (NOLOCK) 
		INNER JOIN ClientDetailValues cdv WITH (NOLOCK) on cdv.ClientID = c.ClientID
		WHERE c.CaseID = @CaseID
		and cdv.DetailFieldID = @DetailFieldID

	END

	RETURN @DetailValue

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDvAsMoney] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetDvAsMoney] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDvAsMoney] TO [sp_executeall]
GO
