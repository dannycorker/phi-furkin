SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- ALTER date: 2014-10-13
-- Description:	Shreds the CustomerInfo from XML 
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_CustomerInfo_ShredXML]
(
	@XmlRequest XML
)
RETURNS 
	@CustomerInfo TABLE (
		CustomerId INT, 
		TitleId INT, 
		FirstName VARCHAR(250),
		LastName VARCHAR(250),
		HomePhone VARCHAR(250),
		MobilePhone VARCHAR(250),
		Email VARCHAR(250),
		Address1 VARCHAR(250),
		Address2 VARCHAR(250),
		TownCity VARCHAR(250),
		County VARCHAR(250),
		Postcode VARCHAR(250),
		SecondaryTitleId INT,
		SecondaryFirstName VARCHAR(250),
		SecondaryLastName VARCHAR(250),
		SecondaryEmail VARCHAR(250),
		SecondaryHomePhone VARCHAR(250),
		SecondaryMobilePhone VARCHAR(250)
	)
AS
BEGIN

	INSERT INTO @CustomerInfo
	SELECT	Tbl.p.value('CustomerId[1]', 'INT'),
			Tbl.p.value('TitleId[1]', 'INT'),
			Tbl.p.value('FirstName[1]', 'VARCHAR(250)'),
			Tbl.p.value('LastName[1]', 'VARCHAR(250)'),
			Tbl.p.value('HomePhone[1]', 'VARCHAR(250)'),
			Tbl.p.value('MobilePhone[1]', 'VARCHAR(250)'),
			Tbl.p.value('Email[1]', 'VARCHAR(250)'),
			Tbl.p.value('Address1[1]', 'VARCHAR(250)'),
			Tbl.p.value('Address2[1]', 'VARCHAR(250)'),
			Tbl.p.value('TownCity[1]', 'VARCHAR(250)'),
			Tbl.p.value('County[1]', 'VARCHAR(250)'),
			Tbl.p.value('Postcode[1]', 'VARCHAR(250)'),
			Tbl.p.value('SecondaryTitleId[1]', 'INT'),
			Tbl.p.value('SecondaryFirstName[1]', 'VARCHAR(250)'),
			Tbl.p.value('SecondaryLastName[1]', 'VARCHAR(250)'),
			Tbl.p.value('SecondaryEmail[1]', 'VARCHAR(250)'),
			Tbl.p.value('SecondaryHomePhone[1]', 'VARCHAR(250)'),
			Tbl.p.value('SecondaryMobilePhone[1]', 'VARCHAR(250)')
	FROM	@XmlRequest.nodes('//CustomerInfo') Tbl(p)
	
	RETURN

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_CustomerInfo_ShredXML] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1273_CustomerInfo_ShredXML] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_CustomerInfo_ShredXML] TO [sp_executeall]
GO
