SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-04-20
-- Description:	Return the later of 2 dates
-- =============================================
CREATE FUNCTION [dbo].[fnMaxOfDate] 
(
	@Date1 DATETIME,
	@Date2 DATETIME
)
RETURNS DATETIME
AS
BEGIN
	DECLARE @LaterDate DATETIME

	IF @Date1 IS NULL
	BEGIN
		RETURN @Date2
	END
	
	IF @Date2 IS NULL
	BEGIN
		RETURN @Date1
	END
	
	SELECT @LaterDate = CASE WHEN @Date1 > @Date2 THEN @Date1 ELSE @Date2 END

	RETURN @LaterDate

END









GO
GRANT VIEW DEFINITION ON  [dbo].[fnMaxOfDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnMaxOfDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnMaxOfDate] TO [sp_executeall]
GO
