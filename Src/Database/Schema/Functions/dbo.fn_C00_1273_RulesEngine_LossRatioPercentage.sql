SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-07-19
-- Description:	Returns the postcode sector (part 1 + first digit of part 2
-- Mods
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_RulesEngine_LossRatioPercentage]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS INT	
AS
BEGIN

--declare
--	@CustomerID INT = 20011607,
--	@LeadID INT = 30015298,
--	@CaseID INT = 40022238,
--	@MatterID INT = 83,
--	@Overrides dbo.tvpIntVarcharVarchar
--insert into @Overrides values
--(2,'YearStart','2016-02-17')
 
	DECLARE @LossRatio REAL,
			@LossRatioPercentage INT = 0
	
	-- loss ratio percentage is zero if we are called without a MatterID (i.e. this is a new purchase)
	IF @MatterID IS NOT NULL
	BEGIN

		SELECT @LossRatio=ISNULL(( CAST(claimed.ValueMoney AS REAL) / CASE WHEN earnt.ValueMoney = 0 THEN NULL ELSE earnt.ValueMoney END ) * 100,0)
		FROM MatterDetailValues claimed WITH (NOLOCK)
		INNER JOIN MatterDetailValues earnt WITH (NOLOCK) ON claimed.MatterID=earnt.MatterID
			AND earnt.DetailFieldID=176955 
		WHERE claimed.MatterID=@MatterID 
		AND claimed.DetailFieldID=176954
		
	END
	
	SELECT @LossRatioPercentage=CASE WHEN @LossRatio <= 0.00000001 
									THEN 0 
								ELSE CASE WHEN @LossRatio > 0.00000001 AND @LossRatio < 1 
									THEN 1 
								ELSE 
									CAST(@LossRatio AS INT) END END

	RETURN @LossRatioPercentage
 
END


 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_LossRatioPercentage] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_RulesEngine_LossRatioPercentage] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_LossRatioPercentage] TO [sp_executeall]
GO
