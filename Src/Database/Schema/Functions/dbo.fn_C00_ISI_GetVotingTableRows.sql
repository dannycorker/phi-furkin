SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2013-11-22
-- Description:	For ISI Clients, return live values for voting rights tables
--				Third party mapped, using field group 30
--				To be used in TableRows__GetBy.... procs
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ISI_GetVotingTableRows] 
(	
	@CustomerID INT,
	@Level		INT
)
RETURNS
@TableOut TABLE 
(
	TableRowID INT,
	Creditor VARCHAR(2000),
	[Total Debt Vote] MONEY,
	[Voting Rights] MONEY,
	DenyEdit VARCHAR(5),
	DenyDelete VARCHAR(5)
)
AS
BEGIN
	
	DECLARE	@ClientID							INT,
			@UnsecuredLenderNameDetailFieldID	INT,
			@SecuredLenderNameDetailFieldID		INT,
			@AmountOwedSecured					INT,
			@AmountOwedUnsecured				INT,
			@PercentageDebtSecured				INT,
			@PercentageDebtUnsecured			INT,
			@SecuredTableDetailFieldID			INT,
			@UnsecuredTableDetailFieldID		INT
	
	SELECT @ClientID = cu.ClientID
	FROM Customers cu WITH (NOLOCK) 
	WHERE cu.CustomerID = @CustomerID

	SELECT	@UnsecuredLenderNameDetailFieldID	= [684],
			@SecuredLenderNameDetailFieldID		= [685],
			@AmountOwedSecured					= [686],
			@AmountOwedUnsecured				= [687],
			@PercentageDebtSecured				= [688],
			@PercentageDebtUnsecured			= [689],
			@SecuredTableDetailFieldID			= [690],
			@UnsecuredTableDetailFieldID		= [691]
	FROM		
		(	
		SELECT pm.ThirdPartyFieldID, ISNULL(pm.ColumnFieldID, DetailFieldID) [DetailFieldID]
		FROM ThirdPartyFieldMapping pm WITH (NOLOCK) 
		WHERE pm.ThirdPartyFieldGroupID = 30
		and pm.ClientID = @ClientID
		)
		AS ToPivot		
	PIVOT		
	(		
		Max([DetailFieldID])		
	FOR		
	[ThirdPartyFieldID]
		IN ( [684],[685],[686],[687],[688],[689],[690],[691] )
	) AS Pivoted


	IF @Level = 1 /*Threshold 1 - 65% Vote Required:*/
	BEGIN
		;With Total as 
		(
		SELECT tr.TableRowID, li.ItemValue [Creditor], tdv_owe.ValueMoney [Total Debt Vote], tdv_pct.ValueMoney [Voting Rights], ROW_NUMBER() OVER (PARTITION BY tr.TableRowID ORDER BY tr.TableRowID desc) [Row]
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID IN(@SecuredLenderNameDetailFieldID,@UnsecuredLenderNameDetailFieldID) /*Lender Name*/ 
		LEFT JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = tdv.ValueInt
		LEFT JOIN TableDetailValues tdv_owe WITH (NOLOCK) on tdv_owe.TableRowID = tr.TableRowID and tdv_owe.DetailFieldID IN(@AmountOwedSecured, @AmountOwedUnsecured)
		LEFT JOIN TableDetailValues tdv_pct WITH (NOLOCK) on tdv_pct.TableRowID = tr.TableRowID and tdv_pct.DetailFieldID IN(@PercentageDebtSecured, @PercentageDebtUnsecured) 
		WHERE tr.DetailFieldID in(@SecuredTableDetailFieldID, @UnsecuredTableDetailFieldID) 
		and tr.CustomerID = @CustomerID
		),
		PreAdjustment as
		(
		SELECT t.TableRowID, t.Creditor, t.[Total Debt Vote], ( t.[Total Debt Vote] / ( SELECT SUM(t2.[Total Debt Vote]) FROM total t2 WHERE t2.Row = 1 ) ) * 100 [Voting Rights], ROW_NUMBER() OVER (ORDER BY ( t.[Total Debt Vote] / (SELECT SUM(t2.[Total Debt Vote]) FROM total t2 WHERE t2.Row = 1 )) desc) [Row]
		FROM Total t 
		WHERE t.Row = 1
		)
		INSERT @TableOut (TableRowID, Creditor, [Total Debt Vote], [Voting Rights], [DenyEdit], [DenyDelete])
		SELECT pa.TableRowID, pa.Creditor, pa.[Total Debt Vote] , pa.[Voting Rights] + CASE pa.Row WHEN 1 THEN 100.00 - ( SELECT SUM(pa2.[Voting Rights]) FROM PreAdjustment pa2 ) ELSE 0.00 END [Voting Rights], 'true' [DenyEdit], 'true' [DenyDelete]
		FROM PreAdjustment pa 
		ORDER BY [Voting Rights] DESC
	END
	ELSE
	IF @Level = 2 /*Threshold 2 - 50% Secured debt vote required*/
	BEGIN
		INSERT @TableOut (TableRowID, Creditor, [Total Debt Vote], [Voting Rights], [DenyEdit], [DenyDelete])
		SELECT tr.TableRowID, li.ItemValue [Creditor], tdv_owe.ValueMoney [Total Debt Vote], tdv_pct.ValueMoney [Voting Rights], 'true' [DenyEdit], 'true' [DenyDelete]
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = @SecuredLenderNameDetailFieldID /*Lender Name*/
		LEFT JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = tdv.ValueInt
		LEFT JOIN TableDetailValues tdv_owe WITH (NOLOCK) on tdv_owe.TableRowID = tr.TableRowID and tdv_owe.DetailFieldID = @AmountOwedSecured
		LEFT JOIN TableDetailValues tdv_pct WITH (NOLOCK) on tdv_pct.TableRowID = tr.TableRowID and tdv_pct.DetailFieldID = @PercentageDebtSecured 
		WHERE tr.DetailFieldID = @SecuredTableDetailFieldID /*Enter Secured Creditor Details*/
		and tr.CustomerID = @CustomerID 
		ORDER BY [Voting Rights] DESC
	END
	ELSE
	IF @Level = 3 /*Threshold 3 - 50% Unsecured debt vote required*/
	BEGIN
		INSERT @TableOut (TableRowID, Creditor, [Total Debt Vote], [Voting Rights], [DenyEdit], [DenyDelete])
		SELECT tr.TableRowID, li.ItemValue [Creditor], tdv_owe.ValueMoney [Total Debt Vote], tdv_pct.ValueMoney [Voting Rights], 'true' [DenyEdit], 'true' [DenyDelete]
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = @UnsecuredLenderNameDetailFieldID /*Lender Name*/
		LEFT JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = tdv.ValueInt
		LEFT JOIN TableDetailValues tdv_owe WITH (NOLOCK) on tdv_owe.TableRowID = tr.TableRowID and tdv_owe.DetailFieldID = @AmountOwedUnsecured
		LEFT JOIN TableDetailValues tdv_pct WITH (NOLOCK) on tdv_pct.TableRowID = tr.TableRowID and tdv_pct.DetailFieldID = @PercentageDebtUnsecured 
		WHERE tr.DetailFieldID = @UnsecuredTableDetailFieldID /*Enter Secured Creditor Details*/
		and tr.CustomerID = @CustomerID 
		ORDER BY [Voting Rights] DESC
	END	
	
	RETURN
END






GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetVotingTableRows] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_ISI_GetVotingTableRows] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetVotingTableRows] TO [sp_executeall]
GO
