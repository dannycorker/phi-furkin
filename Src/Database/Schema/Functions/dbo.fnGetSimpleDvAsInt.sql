SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-07-22
-- Description:	Selects a simple detail field from the appropriate detail value table as Integer
--				2014-10-08 ACE Updated so that basic tables would work
-- =============================================
CREATE FUNCTION [dbo].[fnGetSimpleDvAsInt]
(
	@DetailFieldID INT,
	@ObjectID INT
)
RETURNS INT
AS
BEGIN

	DECLARE @ValueInt		INT,
			@LeadOrMatter	INT

	SELECT @LeadOrMatter = LeadOrmatter
	FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @DetailFieldID

	IF @LeadOrMatter = 1
	BEGIN
		SELECT @ValueInt = ValueInt 
		FROM LeadDetailValues WITH (NOLOCK)
		WHERE LeadID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END

	IF @LeadOrMatter = 2
	BEGIN
		SELECT @ValueInt = ValueInt 
		FROM MatterDetailValues WITH (NOLOCK)
		WHERE MatterID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END

	IF @LeadOrMatter = 4
	BEGIN
		SELECT @ValueInt = ValueInt 
		FROM ResourceListDetailValues WITH (NOLOCK)
		WHERE ResourceListID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END

	IF @LeadOrMatter IN (6,8)
	BEGIN
		SELECT @ValueInt = ValueInt 
		FROM TableDetailValues WITH (NOLOCK)
		WHERE TableRowID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 10
	BEGIN
		SELECT @ValueInt = ValueInt 
		FROM CustomerDetailValues WITH (NOLOCK)
		WHERE CustomerID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 11
	BEGIN
		SELECT @ValueInt = ValueInt 
		FROM CaseDetailValues WITH (NOLOCK)
		WHERE CaseID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 12
	BEGIN
		SELECT @ValueInt = ValueInt 
		FROM ClientDetailValues WITH (NOLOCK)
		WHERE ClientID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 13
	BEGIN
		SELECT @ValueInt = ValueInt 
		FROM ClientPersonnelDetailValues WITH (NOLOCK)
		WHERE ClientPersonnelID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 14
	BEGIN
		SELECT @ValueInt = ValueInt 
		FROM ContactDetailValues WITH (NOLOCK)
		WHERE ContactID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END

	RETURN @ValueInt

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleDvAsInt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetSimpleDvAsInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleDvAsInt] TO [sp_executeall]
GO
