SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-06-10
-- Description:	Appends a DV to the string calling argument
--              Useful where the DV is potentially a long string of digits that the equation editor
--              is likely to convert to std number format
-- =============================================
CREATE FUNCTION [dbo].[fnAppendDVToString]
(
	@String VARCHAR(2000),
	@DetailFieldID INT,
	@ObjectID INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE	@LeadOrMatter int
	
	SELECT @LeadOrMatter = df.LeadOrMatter
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @DetailFieldID
	
	IF @LeadOrMatter = 1
	BEGIN
		SELECT @String=@String + DetailValue FROM LeadDetailValues WITH (NOLOCK) WHERE DetailFieldID=@DetailFieldID and LeadID=@ObjectID
	END
	ELSE IF @LeadOrMatter = 2
	BEGIN
		SELECT @String=@String + DetailValue FROM MatterDetailValues WITH (NOLOCK) WHERE DetailFieldID=@DetailFieldID and MatterID=@ObjectID
	END
	ELSE IF @LeadOrMatter = 10
	BEGIN
		SELECT @String=@String + DetailValue FROM CustomerDetailValues WITH (NOLOCK) WHERE DetailFieldID=@DetailFieldID and CustomerID=@ObjectID
	END
	ELSE IF @LeadOrMatter = 11
	BEGIN
		SELECT @String=@String + DetailValue FROM CaseDetailValues WITH (NOLOCK) WHERE DetailFieldID=@DetailFieldID and CaseID=@ObjectID
	END	
	ELSE IF @LeadOrMatter = 12
	BEGIN
		SELECT @String=@String + DetailValue FROM ClientDetailValues WITH (NOLOCK) WHERE DetailFieldID=@DetailFieldID and ClientID=@ObjectID
	END	

	RETURN @String

END





GO
GRANT VIEW DEFINITION ON  [dbo].[fnAppendDVToString] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnAppendDVToString] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnAppendDVToString] TO [sp_executeall]
GO
