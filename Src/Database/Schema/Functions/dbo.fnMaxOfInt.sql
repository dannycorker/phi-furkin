SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2010-02-24
-- Description:	Return the larger of 2 numbers (int)
-- =============================================
CREATE FUNCTION [dbo].[fnMaxOfInt] 
(
	@Number1 int,
	@Number2 int
)
RETURNS int
AS
BEGIN
	DECLARE @BiggerNumber int
	
	IF @Number1 IS NULL
	BEGIN
		RETURN @Number2
	END
	
	IF @Number2 IS NULL
	BEGIN
		RETURN @Number1
	END
	
	SELECT @BiggerNumber = CASE WHEN @Number1 > @Number2 THEN @Number1 ELSE @Number2 END

	RETURN @BiggerNumber

END











GO
GRANT VIEW DEFINITION ON  [dbo].[fnMaxOfInt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnMaxOfInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnMaxOfInt] TO [sp_executeall]
GO
