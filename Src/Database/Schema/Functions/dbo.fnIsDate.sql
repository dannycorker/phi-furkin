SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-20
-- Description:	Check to see if a varchar contains a valid date.
-- This is for the Date and Datetime2 datatypes, where years 0000 to 9999 are valid
-- =============================================
CREATE FUNCTION [dbo].[fnIsDate] 
(
	@value varchar(2000)
)
RETURNS bit
AS
BEGIN

	DECLARE @IsDate bit = 1
	
	SELECT @value = ltrim(rtrim(@value))
	
	IF @value IS NULL OR @value = '' OR datalength(@value) < 10 
	BEGIN
		SET @IsDate = 0
	END
	ELSE
	BEGIN

		IF substring(@value, 1, 10) NOT LIKE '[0-9][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]' 	
		BEGIN 
			SET @IsDate = 0
		END
		ELSE
		BEGIN
			/* Assume it is a date by default at this point */
			SET @IsDate = 1

			DECLARE @year int, @month tinyint, @day tinyint
			
			SELECT @year = convert(int, substring(@value, 1, 4)), @month = convert(tinyint, substring(@value, 6, 2)), @day = convert(tinyint, substring(@value, 9, 2))
			
			IF @month NOT BETWEEN 1 AND 12
			BEGIN
				SET @IsDate = 0
			END
			ELSE
			BEGIN 
				IF @month IN (1, 3, 5, 7, 8, 10, 12) AND (@day NOT BETWEEN 1 AND 31)
				BEGIN
					SET @IsDate = 0
				END
				ELSE
				BEGIN 
					IF @month IN (4, 6, 9, 11) AND (@day NOT BETWEEN 1 AND 30)
					BEGIN
						SET @IsDate = 0
					END
					ELSE
					BEGIN 
						IF @month = 2 AND (@day NOT BETWEEN 1 AND 28 + dbo.fnIsLeapYear(@year))
						BEGIN
							SET @IsDate = 0
						END
					END
				END
			END
		END	
	END
	
	RETURN @IsDate
			
END






GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnIsDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsDate] TO [sp_executeall]
GO
