SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-04-01
-- Description:	Recursively identifies all fields which influence the outcome of an equation
-- =============================================
CREATE FUNCTION [dbo].[fnEquationTargetsGetAllRecursive] 
(	
	@EquationDetailFieldID INT
)
RETURNS TABLE 
AS
RETURN 
(
	WITH MyCte as 
	(
	SELECT et.DetailFieldID, et.IsEquation, 0 [Level]
	FROM EquationTarget et WITH (NOLOCK) 
	WHERE et.EquationDetailFieldID = @EquationDetailFieldID
		UNION ALL
	SELECT et.DetailFieldID, et.IsEquation, cte.Level + 1
	FROM EquationTarget et WITH (NOLOCK) 
	INNER JOIN MyCte cte on cte.DetailFieldID = et.EquationDetailFieldID
	)
	SELECT DISTINCT cte.DetailFieldID, cte.IsEquation, cte.Level
	FROM MyCte cte
)






GO
GRANT VIEW DEFINITION ON  [dbo].[fnEquationTargetsGetAllRecursive] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnEquationTargetsGetAllRecursive] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnEquationTargetsGetAllRecursive] TO [sp_executeall]
GO
