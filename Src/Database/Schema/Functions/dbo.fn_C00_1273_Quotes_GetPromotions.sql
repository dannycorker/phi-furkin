SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-12-03
-- Description:	Returns the promotions
-- Modified DCM 2014-09-16 Copied from 235
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_Quotes_GetPromotions]
(
	@Now DATE, 
	@CodeOfferList VARCHAR(MAX),
	@BrandRLID INT,
	@LanguageID INT
)
RETURNS 
@Data TABLE
(
	ResourceListID INT,
	MarketingCode VARCHAR(50),
	Description VARCHAR(2000),
	Amount MONEY,
	IsPercentage INT,
	LandingPageUrl VARCHAR(500),	
	AssociatedGroup VARCHAR(500),
	GroupLogo VARCHAR(500),
	Duration INT,
	OfferCodeRule VARCHAR(500)
)	
AS
BEGIN

	DECLARE @ClientID INT
	
	SELECT @ClientID=rldv.ClientID FROM ResourceListDetailValues rldv WITH (NOLOCK) 
	WHERE rldv.DetailFieldID = 170994 -- TODO not mapped
	AND rldv.ResourceListID = @BrandRLID

	/*
		I have left the date as an input so we can filter these promotions on date if required.	
	*/
	
	INSERT INTO @Data
	SELECT	tdvRL.ResourceListID, llCode.ItemValue AS MarketingCode, ISNULL(t.Translation, rdvDesc.DetailValue) AS Description, rdvAmount.ValueMoney AS Amount, rdvPercent.ValueInt AS IsPercentage,
			llUrl.ItemValue AS LandingPageUrl, llGroup.ItemValue AS AssociatedGroup, 
			'https://aqnet.aquarium-software.com/ClientImages/Client_' + CONVERT(VARCHAR(10),@ClientID) + '_Images/' + rdvLogo.DetailValue AS GroupLogo,
			CASE WHEN rdvDuration.ValueInt > 0 THEN rdvDuration.ValueInt ELSE NULL END AS Duration,
			llRule.ItemValue AS OfferCodeRule
	FROM dbo.Lead l WITH (NOLOCK) 
	INNER JOIN dbo.TableRows r WITH (NOLOCK) ON l.LeadID = r.LeadID AND r.DetailFieldID = 1154472 -- TODO not mapped
	INNER JOIN dbo.TableDetailValues tdvRL WITH (NOLOCK) ON r.TableRowID = tdvRL.TableRowID AND tdvRL.DetailFieldID = 170063
	INNER JOIN dbo.ResourceListDetailValues rdvCode WITH (NOLOCK) ON tdvRL.ResourceListID = rdvCode.ResourceListID AND rdvCode.DetailFieldID = 170057
	INNER JOIN dbo.LookupListItems llCode WITH (NOLOCK) ON rdvCode.ValueInt = llCode.LookupListItemID
	INNER JOIN dbo.ResourceListDetailValues rdvDesc WITH (NOLOCK) ON tdvRL.ResourceListID = rdvDesc.ResourceListID AND rdvDesc.DetailFieldID = 170058
	LEFT JOIN dbo.Translation t WITH (NOLOCK) ON rdvDesc.ResourceListID = t.ID AND rdvDesc.DetailFieldID = t.DetailFieldID AND t.LanguageID = @LanguageID AND t.ClientID = @ClientID
	
	INNER JOIN dbo.ResourceListDetailValues rdvAmount WITH (NOLOCK) ON tdvRL.ResourceListID = rdvAmount.ResourceListID AND rdvAmount.DetailFieldID = 170059
	INNER JOIN dbo.ResourceListDetailValues rdvPercent WITH (NOLOCK) ON tdvRL.ResourceListID = rdvPercent.ResourceListID AND rdvPercent.DetailFieldID = 170060
	
	LEFT JOIN dbo.fnTableOfValuesFromCSV(@CodeOfferList) codes ON llCode.ItemValue = codes.AnyValue	
	LEFT JOIN dbo.ResourceListDetailValues rdvUrl WITH (NOLOCK) ON tdvRL.ResourceListID = rdvUrl.ResourceListID AND rdvUrl.DetailFieldID = 1171270 -- TODO not mapped
	LEFT JOIN dbo.LookupListItems llUrl WITH (NOLOCK) ON rdvUrl.ValueInt = llUrl.LookupListItemID
	LEFT JOIN dbo.ResourceListDetailValues rdvLogo WITH (NOLOCK) ON tdvRL.ResourceListID = rdvLogo.ResourceListID AND rdvLogo.DetailFieldID = 1171271 -- TODO not mapped
	LEFT JOIN dbo.ResourceListDetailValues rdvGroup WITH (NOLOCK) ON tdvRL.ResourceListID = rdvGroup.ResourceListID AND rdvGroup.DetailFieldID = 1171272 -- TODO not mapped
	LEFT JOIN dbo.LookupListItems llGroup WITH (NOLOCK) ON rdvGroup.ValueInt = llGroup.LookupListItemID
	
	LEFT JOIN dbo.ResourceListDetailValues rdvDuration WITH (NOLOCK) ON tdvRL.ResourceListID = rdvDuration.ResourceListID AND rdvDuration.DetailFieldID = 170061
	LEFT JOIN dbo.ResourceListDetailValues rdvRule WITH (NOLOCK) ON tdvRL.ResourceListID = rdvRule.ResourceListID AND rdvRule.DetailFieldID = 170062
	LEFT JOIN dbo.LookupListItems llRule WITH (NOLOCK) ON rdvRule.ValueInt = llRule.LookupListItemID
	
--	WHERE l.LeadID = 4215904
	WHERE l.LeadID = dbo.fn_C00_1273_GetIllnessSchemeLeadIDFromBrandRLID(ISNULL(@BrandRLID,1138780)) -- TODO not mapped
	AND (( @CodeOfferList IS NULL OR @CodeOfferList = '') OR codes.AnyValue > '')

	RETURN
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_Quotes_GetPromotions] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1273_Quotes_GetPromotions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_Quotes_GetPromotions] TO [sp_executeall]
GO
