SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-02-07
-- Description:	Returns a list of claims and their parent claim where relevant
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetClaimRelationships]
(
	@LeadID INT
)
RETURNS 
	@Relationships TABLE 
	(
		TableRowID INT,
		ClaimID INT,
		ParentID INT
	)
AS
BEGIN


	;WITH InnerSql AS 
	(
	SELECT	r.TableRowID, tdvMatterID.ValueInt AS MatterID, tdvParent.ValueInt AS ParentID,
			ROW_NUMBER() OVER(PARTITION BY tdvMatterID.ValueInt, tdvParent.ValueInt ORDER BY r.TableRowID) as rn 
	FROM dbo.TableRows r WITH (NOLOCK)
	INNER JOIN dbo.TableDetailValues tdvMatterID WITH (NOLOCK) ON r.TableRowID = tdvMatterID.TableRowID AND tdvMatterID.DetailFieldID = 146355
	INNER JOIN dbo.TableDetailValues tdvParent WITH (NOLOCK) ON r.TableRowID = tdvParent.TableRowID AND tdvParent.DetailFieldID = 146356
	WHERE r.LeadID = @LeadID
	)

	-- We left join to the cte so that if any links have not yet been defined we still get all the matters listed separately
	INSERT @Relationships (TableRowID, ClaimID, ParentID)
	SELECT i.TableRowID, m.MatterID, i.ParentID
	FROM dbo.Matter m WITH (NOLOCK)
	LEFT JOIN InnerSql i ON m.MatterID = i.MatterID AND i.rn = 1
	WHERE m.LeadID = @LeadID

	RETURN

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetClaimRelationships] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1272_GetClaimRelationships] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetClaimRelationships] TO [sp_executeall]
GO
