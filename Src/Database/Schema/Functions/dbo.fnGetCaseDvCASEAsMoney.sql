SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-06-12
-- Description:	Get CaseDV directly #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetCaseDvCASEAsMoney]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS money
AS
BEGIN

	DECLARE @ValueMoney AS money

	SELECT @ValueMoney = cdv.ValueMoney
	FROM CaseDetailValues cdv WITH (NOLOCK) 
	WHERE cdv.CaseID = @CaseID 
	AND cdv.DetailFieldID = @DetailFieldID

	RETURN @ValueMoney

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCaseDvCASEAsMoney] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetCaseDvCASEAsMoney] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCaseDvCASEAsMoney] TO [sp_executeall]
GO
