SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-11-12
-- Description:	Returns the appropriate policy history row based on the terms date from the override data passed in or looked up
-- Mods
-- DCM Default start date for situation where we are called by the rules engine test interface
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_RulesEngine_GetPremiumHistoryRow]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS INT	
AS
BEGIN
 
	DECLARE @YearStart DATE,
			@TableRowID INT
	
	-- Year start date is always passed in
	SELECT @YearStart = AnyValue2
	FROM @Overrides
	WHERE AnyID = 2
	AND AnyValue1 = 'YearStart'
	
	-- default start date
	IF @YearStart IS NULL SELECT @YearStart=dbo.fn_GetDate_Local()

	SELECT @TableRowID = dbo.fn_C00_1273_GetPremiumCalculationPolicyHistoryRow(@CaseID, @YearStart)	
	
	RETURN @TableRowID
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_GetPremiumHistoryRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_RulesEngine_GetPremiumHistoryRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_GetPremiumHistoryRow] TO [sp_executeall]
GO
