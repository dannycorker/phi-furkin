SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2015-05-12
-- Description:	Shred an XML string into its constituent values
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ShredXml] 
(	
	 @MyXml						XML
	,@NumberRepeatingSections	BIT
)
RETURNS
@TableOut TABLE 
(
	 Nestinglevel	INT
	,FullPath		VARCHAR(MAX)
	,ElementName	VARCHAR(2000)
	,ElementValue	VARCHAR(2000)
) 
AS
BEGIN

	;WITH cte AS
	(
		SELECT 
			 1											NestingLevel
			,x.c.query('.')								RemainingXML
			,x.c.value('.', 'VARCHAR(2000)')			ElementValue
			,x.c.value('local-name(.)', 'VARCHAR(2000)')ElementName
			,CAST ( NULL AS VARCHAR(MAX) )				FullPath
		FROM @MyXml.nodes('/*') x(c)

			UNION ALL

		SELECT 
			 NestingLevel + 1
			,n.c.query('*')									RemainingXML
			,n.c.value('.', 'VARCHAR(2000)')				ElementValue
			,n.c.value('local-name(.)', 'VARCHAR(2000)')	ElementName
			,ISNULL( c.FullPath + '/', '' ) + n.c.value('local-name(.)', 'VARCHAR(MAX)') + CASE WHEN @NumberRepeatingSections = 0 THEN '' ELSE REPLACE(RIGHT('000' + CONVERT(VARCHAR,ROW_NUMBER() OVER (PARTITION BY n.c.value('local-name(.)', 'VARCHAR(2000)') ORDER BY NestingLevel)),4),'0001','') END FullPath
		FROM cte c
			CROSS APPLY c.RemainingXML.nodes('*') n(c)
	)
	INSERT @TableOut ( NestingLevel, FullPath, ElementName, ElementValue )
	SELECT cte.NestingLevel, cte.FullPath, cte.elementName, cte.elementValue
	FROM cte

	UPDATE fn
	SET ElementValue = NULL
	FROM @TableOut fn
	WHERE exists (	SELECT * 
					FROM @TableOut fn2
					WHERE fn2.FullPath like fn.FullPath + '/%' )
		or fn.FullPath is NULL
	
	RETURN
END






GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ShredXml] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_ShredXml] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ShredXml] TO [sp_executeall]
GO
