SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-08-17
-- Description:	Function that returns ruleset override if evaluates
-- IS 2016-08-17 Added Evaluate Ruleset By RuleSet/ByChangeSet/ByEffectiveDate
-- dcm 2017-02-28 Update ruleset selection filter for evaluate by date
-- =============================================
CREATE FUNCTION [dbo].[fn_RulesEngine_GetRuleSetOverride]
(
	@RuleSetID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS INT
AS
BEGIN

	DECLARE
		@OverrideRuleSetID INT = NULL,
		@ClientID INT,
		@EvaluateByChangeSetID VARCHAR(250) = '',
		@EvaluateByEffectiveDate VARCHAR(250) = ''
	
	SELECT	@ClientID = ClientID
	FROM	RulesEngine_RuleSets rs WITH (NOLOCK) 
	WHERE	RuleSetID = @RuleSetID
	
	SELECT	@EvaluateByChangeSetID = AnyValue2
	FROM	@Overrides
	WHERE	AnyValue1 = 'EvaluateByChangeSetID'
	
	SELECT	@EvaluateByEffectiveDate = AnyValue2
	FROM	@Overrides
	WHERE	AnyValue1 = 'EvaluateByEffectiveDate'
	
	IF ISNULL(@EvaluateByChangeSetID, '') <> ''
	BEGIN
		
		-- Evaluate ruleset by a specific changeset passed in the overrides
		SELECT	TOP 1	@OverrideRuleSetID = rs.RuleSetID
		FROM	RulesEngine_ChangeSets cs WITH (NOLOCK) 
		INNER JOIN RulesEngine_RuleSets rs WITH (NOLOCK) ON rs.ChangeSetID = cs.ChangeSetID
		WHERE	cs.ClientID = @ClientID
		AND		cs.ChangeSetID = @EvaluateByChangeSetID
		AND (
			rs.ParentRuleSetID IN 
			(
				SELECT	ParentRuleSetID
				FROM	RulesEngine_RuleSets WITH (NOLOCK)
				WHERE	RuleSetID = @RuleSetID
				UNION
				SELECT	RuleSetID
				FROM	RulesEngine_RuleSets WITH (NOLOCK)
				WHERE	RuleSetID = @RuleSetID
			)
			OR rs.RuleSetID IN 
			(
				SELECT	ParentRuleSetID
				FROM	RulesEngine_RuleSets WITH (NOLOCK)
				WHERE	RuleSetID = @RuleSetID
				UNION
				SELECT	RuleSetID
				FROM	RulesEngine_RuleSets WITH (NOLOCK)
				WHERE	RuleSetID = @RuleSetID
			)
		)
		ORDER BY cs.ChangeSetID ASC

		SELECT @RuleSetID = ISNULL(@OverrideRuleSetID,@RuleSetID)
		
	END
	ELSE IF ISNULL(@EvaluateByEffectiveDate, '') <> ''
	BEGIN
	
		-- Evaluate ruleset by a specific changeset based on a effective date passed in the overrides
		SELECT	TOP 1	@OverrideRuleSetID = rs.RuleSetID
		FROM	RulesEngine_ChangeSets cs WITH (NOLOCK) 
		INNER JOIN RulesEngine_RuleSets rs WITH (NOLOCK) ON rs.ChangeSetID = cs.ChangeSetID
		WHERE cs.ClientID = @ClientID
		AND	(@EvaluateByEffectiveDate >= cs.ValidFrom OR cs.ValidFrom IS NULL) AND (@EvaluateByEffectiveDate <= cs.ValidTo OR cs.ValidTo IS NULL)
		AND NOT ( cs.ValidFrom IS NULL AND cs.ValidTo IS NULL )
		AND (
			rs.ParentRuleSetID IN 
			(
				SELECT	ParentRuleSetID
				FROM	RulesEngine_RuleSets WITH (NOLOCK)
				WHERE	RuleSetID = @RuleSetID
				UNION
				SELECT	RuleSetID
				FROM	RulesEngine_RuleSets WITH (NOLOCK)
				WHERE	RuleSetID = @RuleSetID
			)
			OR rs.RuleSetID IN 
			(
				SELECT	ParentRuleSetID
				FROM	RulesEngine_RuleSets WITH (NOLOCK)
				WHERE	RuleSetID = @RuleSetID
				UNION
				SELECT	RuleSetID
				FROM	RulesEngine_RuleSets WITH (NOLOCK)
				WHERE	RuleSetID = @RuleSetID
			)
		)
		ORDER BY cs.ChangeSetID ASC

		SELECT @RuleSetID = ISNULL(@OverrideRuleSetID,@RuleSetID)
		
	END

	RETURN @RuleSetID
END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_GetRuleSetOverride] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_RulesEngine_GetRuleSetOverride] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_GetRuleSetOverride] TO [sp_executeall]
GO
