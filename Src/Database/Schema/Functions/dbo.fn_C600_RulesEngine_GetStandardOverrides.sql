SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2018-07-04
-- Description:	Find all standard rules engine override values for a case
-- GPR 2019-10-13 created to 603 for LPC-35
-- IES 20200820 [SDPRU-9] excess+copay or defaults
-- ALM 2020-09-02 PPET-200 added underwriter questions
-- GPR/ALM 2020-09-14 [PPET-510] Added overrides for Wellness parameters
-- 2020-11-19 CPS for JIRA SDPRU-155 | Include CalculationTypeID so we can vary the start date if this is a renewal calculation
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_RulesEngine_GetStandardOverrides]
(
	 @CustomerID		INT
	,@LeadID			INT
	,@CaseID			INT
	,@MatterID			INT
	,@CalculationTypeID	INT
)
RETURNS 
@Overrides TABLE
(
	  ParameterValue	VARCHAR(50)	NOT NULL
	 ,EvaluatedValue	VARCHAR(2000)	NULL
)	
AS
BEGIN

	DECLARE  @InceptionDateVarchar	VARCHAR(10) = dbo.fnGetSimpleDv(170035,@MatterID) /*Policy Inception Date*/
			,@StartDateVarchar		VARCHAR(10) = CASE WHEN @CalculationTypeID = 2 /*Renewal*/
													   THEN dbo.fnGetSimpleDv(175307,@MatterID) /*Policy Renewal Date*/
													   ELSE dbo.fnGetSimpleDv(170036,@MatterID) /*Policy Start Date*/
												  END

	/*GPR 2019-10-23*/
	DECLARE @NewBusiness VARCHAR(1), @PolicyYear INT
	SELECT @PolicyYear = COUNT(tr.TableRowID) FROM TableRows tr WITH ( NOLOCK ) WHERE tr.DetailFieldID = 170033 /*Historical Policy*/ AND tr.MatterID = @MatterID

	IF @PolicyYear > 1
	BEGIN
		SELECT @NewBusiness = '0'
	END
	ELSE
	BEGIN
		SELECT @NewBusiness = '1'
	END

	INSERT @Overrides ( ParameterValue, EvaluatedValue )
	SELECT 'PostCode', cu.PostCode
	FROM dbo.Customers cu WITH ( NOLOCK ) 
	WHERE cu.CustomerID = @CustomerID
		UNION ALL
	SELECT 'TermsDate', @InceptionDateVarchar
		UNION ALL
	SELECT 'YearStart', @StartDateVarchar
		UNION ALL
	SELECT 'NewBusiness',@NewBusiness --CASE WHEN @StartDateVarchar = @InceptionDateVarchar THEN '1' ELSE '0' END
		UNION ALL
	SELECT 'PetNumber', CAST(COUNT(l.LeadID) + 1  AS VARCHAR(3))
	FROM dbo.Lead l WITH ( NOLOCK ) 
	WHERE l.LeadTypeID = 1492 /*Policy Admin*/
	 AND l.CustomerID = @CustomerID 
	 AND l.LeadID < @LeadID
		UNION ALL
	SELECT 'InceptionExitPremium', '0.00'
		UNION ALL
	SELECT 'Brand', dbo.fnGetSimpleRlDv(170144,176965,@CustomerID,1) /*Affinity Details | Affinity Brand*/
		UNION ALL
	SELECT 'ProductName', dbo.fnGetSimpleRldv(170034,146200,@MatterID,0) /*Current Policy | Product*/
		UNION ALL
	SELECT 'ItemName', ''
		UNION ALL
	SELECT 'HistoricClaimAmount', '0.00'
		UNION ALL
	SELECT '170257', dbo.fnGetSimpleDv(170257,@CustomerID) /*Send Documents By (why is ths not marked as type "detail field"?)*/
		UNION ALL
	SELECT '313932', dbo.fnGetSimpleDv(313932,@LeadID) /*Excess*/
		UNION ALL
	SELECT '313930', dbo.fnGetSimpleDv(313930,@LeadID) /*CoInsurance*/
		UNION ALL 
	SELECT '313929', dbo.fnGetSimpleDv(313929,@LeadID) /*Working Dog - Y/N*/  
		UNION ALL 
	SELECT '313933', dbo.fnGetSimpleDv(313933, @LeadID) /*Veteran - Y/N*/ /*ALM 2020-12-17 SDPRU-199*/
		UNION ALL 
	SELECT '313936', dbo.fnGetSimpleDv(313936, @CustomerID) /*Vet/Staff - Y/N*/ /*ALM 2020-12-17 SDPRU-199*/
		UNION ALL 
	SELECT '177501', dbo.fnGetSimpleDv(177501, @LeadID) /*Rescue Pet - Y/N*/
		UNION ALL 
	SELECT '313935', dbo.fnGetSimpleDv(313935, @CustomerID) /*Medical - Y/N*/ /*ALM 2020-12-17 SDPRU-199*/
		UNION ALL 
	SELECT '313934', dbo.fnGetSimpleDv(313934, @LeadID) /*Selected Wellness - Y/N*/
		UNION ALL 
	SELECT '313931', dbo.fnGetSimpleDv(313931, @LeadID) /*Exam Fees - Y/N*/
		UNION ALL 
	SELECT '144274', dbo.fnGetSimpleDv(144274, @LeadID) /*Pet Date of Birth*/

	/*GPR/ALM 2020-09-14 for PPET-510 WELLNESS*/
	IF dbo.fnGetSimpleDv(314235, @MatterID) = 2002261 /*Wellness benefit - Low*/
	BEGIN
		INSERT @Overrides ( ParameterValue, EvaluatedValue )
		SELECT '314015', 5144 -- Wellness Low
	END
	ELSE
	BEGIN
		INSERT @Overrides ( ParameterValue, EvaluatedValue )
		SELECT '314015', 5145 -- Wellness Low
	END
	
	IF dbo.fnGetSimpleDv(314235, @MatterID) = 2002262 /*Wellness benefit - Medium*/
	BEGIN
		INSERT @Overrides ( ParameterValue, EvaluatedValue )
		SELECT '314013', 5144 -- Wellness Medium
	END
	ELSE
	BEGIN
		INSERT @Overrides ( ParameterValue, EvaluatedValue )
		SELECT '314013', 5145 -- Wellness Medium
	END
	
	IF dbo.fnGetSimpleDv(314235, @MatterID) = 2002263 /*Wellness benefit - High*/
	BEGIN
		INSERT @Overrides ( ParameterValue, EvaluatedValue )
		SELECT '314014', 5144 -- Wellness High
	END
	ELSE
	BEGIN
		INSERT @Overrides ( ParameterValue, EvaluatedValue )
		SELECT '314014', 5145 -- Wellness High
	END
	
	RETURN

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_RulesEngine_GetStandardOverrides] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_RulesEngine_GetStandardOverrides] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_RulesEngine_GetStandardOverrides] TO [sp_executeall]
GO
