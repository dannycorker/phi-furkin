SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-28
-- Description:	Return address formatted according to the ISI XML requirements
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ISI_XmlPush_Asset]
(
	 @CustomerID INT
)
RETURNS XML
AS
BEGIN

	DECLARE @AssetXML XML
	
	SELECT @AssetXML = 
		(
		SELECT (
			SELECT dbo.fn_C00_ISI_ReturnXmlAddress (tdv_adl1.DetailValue, tdv_adl2.DetailValue, tdv_adl3.DetailValue, tdv_adl4.DetailValue, tdv_adl5.DetailValue, '', ISNULL(luli.ItemValue, 'Ireland')) AS [Address], (
					SELECT tdv_cmv.ValueMoney AS [CurrentMarketValue], tdv_ownership.ValueMoney AS [DebtorOwnership]
					FROM TableDetailValues tdv_cmv WITH (NOLOCK)
					INNER JOIN TableDetailValues tdv_ownership WITH (NOLOCK) ON tdv_ownership.TableRowID = tdv_cmv.TableRowID AND tdv_ownership.DetailFieldID = 297589
					WHERE tdv_cmv.TableRowID = tdv.TableRowID
					AND tdv_cmv.DetailFieldID = 297588
					FOR XML PATH (''), type
					) AS [AssetDetail], tdv.TableRowID AS [AssetUniqueId], ISNULL(tdv_comment.DetailValue, '') AS [Comment]
			FROM TableDetailValues tdv WITH (NOLOCK)
			INNER JOIN TableDetailValues tdv_adl1 WITH (NOLOCK) ON tdv_adl1.TableRowID = tdv.TableRowID AND tdv_adl1.DetailFieldID = 297580
			INNER JOIN TableDetailValues tdv_adl2 WITH (NOLOCK) ON tdv_adl2.TableRowID = tdv.TableRowID AND tdv_adl2.DetailFieldID = 297581
			INNER JOIN TableDetailValues tdv_adl3 WITH (NOLOCK) ON tdv_adl3.TableRowID = tdv.TableRowID AND tdv_adl3.DetailFieldID = 297582
			INNER JOIN TableDetailValues tdv_adl4 WITH (NOLOCK) ON tdv_adl4.TableRowID = tdv.TableRowID AND tdv_adl4.DetailFieldID = 297583
			INNER JOIN TableDetailValues tdv_adl5 WITH (NOLOCK) ON tdv_adl5.TableRowID = tdv.TableRowID AND tdv_adl5.DetailFieldID = 297584
			INNER JOIN TableDetailValues tdv_Country WITH (NOLOCK) ON tdv_Country.TableRowID = tdv.TableRowID AND tdv_Country.DetailFieldID = 297579
			LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_Country.ValueInt
			INNER JOIN TableDetailValues tdv_comment WITH (NOLOCK) ON tdv_comment.TableRowID = tdv.TableRowID AND tdv_comment.DetailFieldID = 297590
			WHERE tdv.DetailFieldID = 297592
			AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
			AND tdv.CustomerID = @CustomerID 
			FOR XML PATH ('AssetPrincipalPrivateResidence'), Type
			), (SELECT luli.ItemValue AS [Type], '' AS [OtherDetails], dbo.fn_C00_ISI_ReturnXmlAddress (tdv_adl1.DetailValue, tdv_adl2.DetailValue, tdv_adl3.DetailValue, tdv_adl4.DetailValue, tdv_adl5.DetailValue, '', ISNULL(luli_country.ItemValue, 'Ireland')) AS [Address],
				(	SELECT tdv_cmv.DetailValue AS [CurrentMarketValue], tdv_ownership.DetailValue AS [DebtorOwnership], ISNULL(tdv_monthlyincome.DetailValue, '') AS [MonthlyIncome]
					FROM TableDetailValues tdv_cmv WITH (NOLOCK)
					LEFT JOIN TableDetailValues tdv_monthlyincome WITH (NOLOCK) ON tdv_monthlyincome.TableRowID = tdv_cmv.TableRowID AND tdv_monthlyincome.DetailFieldID = 297609
					LEFT JOIN TableDetailValues tdv_ownership WITH (NOLOCK) ON tdv_ownership.TableRowID = tdv_cmv.TableRowID AND tdv_ownership.DetailFieldID = 297608
					WHERE tdv_cmv.TableRowID = tdv.TableRowID AND tdv_cmv.DetailFieldID = 297607
					FOR XML PATH (''), TYPE
				) AS [AssetDetail], ISNULL(tdv_monthlyExpenditure.DetailValue, '') AS [MonthlyExpenditure], tdv.TableRowID AS [AssetUniqueId], ISNULL(tdv_comments.DetailValue, '') AS [Comment]
			FROM TableDetailValues tdv WITH (NOLOCK) 
			INNER JOIN TableDetailValues tdv_type WITH (NOLOCK) ON tdv_type.TableRowID = tdv.TableRowID AND tdv_type.DetailFieldID = 297596
			INNER JOIN TableDetailValues tdv_adl1 WITH (NOLOCK) ON tdv_adl1.TableRowID = tdv.TableRowID AND tdv_adl1.DetailFieldID = 297598
			INNER JOIN TableDetailValues tdv_adl2 WITH (NOLOCK) ON tdv_adl2.TableRowID = tdv.TableRowID AND tdv_adl2.DetailFieldID = 297599
			INNER JOIN TableDetailValues tdv_adl3 WITH (NOLOCK) ON tdv_adl3.TableRowID = tdv.TableRowID AND tdv_adl3.DetailFieldID = 297600
			INNER JOIN TableDetailValues tdv_adl4 WITH (NOLOCK) ON tdv_adl4.TableRowID = tdv.TableRowID AND tdv_adl4.DetailFieldID = 297601
			INNER JOIN TableDetailValues tdv_adl5 WITH (NOLOCK) ON tdv_adl5.TableRowID = tdv.TableRowID AND tdv_adl5.DetailFieldID = 297602
			INNER JOIN TableDetailValues tdv_Country WITH (NOLOCK) ON tdv_Country.TableRowID = tdv.TableRowID AND tdv_Country.DetailFieldID = 297597
			LEFT JOIN LookupListItems luli_country WITH (NOLOCK) ON luli_country.LookupListItemID = tdv_Country.ValueInt
			INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_type.ValueInt
			INNER JOIN TableDetailValues tdv_monthlyExpenditure WITH (NOLOCK) ON tdv_monthlyExpenditure.TableRowID = tdv.TableRowID AND tdv_monthlyExpenditure.DetailFieldID = 297610
			INNER JOIN TableDetailValues tdv_comments WITH (NOLOCK) ON tdv_comments.TableRowID = tdv.TableRowID AND tdv_comments.DetailFieldID = 297611
			WHERE tdv.DetailFieldID = 297613 
			AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
			AND tdv.CustomerID = @CustomerID
			FOR XML PATH ('AssetInvestmentProperty'), Type
			), (SELECT '', 
				(SELECT '' AS [IssuerName], '' AS [Address], (SELECT tdv_cmv.DetailValue AS [CurrentMarketValue], tdv_do.DetailValue AS [DebtorOwnership], tdv_mi.DetailValue AS [MonthlyIncome]
					FROM TableDetailValues tdv_cmv WITH (NOLOCK)
					INNER JOIN TableDetailValues tdv_do WITH (NOLOCK) ON tdv_do.TableRowID = tdv_cmv.TableRowID AND tdv_do.DetailFieldID = 297621
					INNER JOIN TableDetailValues tdv_mi WITH (NOLOCK) ON tdv_mi.TableRowID = tdv.TableRowID AND tdv_mi.DetailFieldID = 297623
					WHERE tdv_cmv.TableRowID = tdv.TableRowID
					AND tdv_cmv.DetailFieldID = 297620
				FOR XML PATH ('InvestmentDetails'), TYPE
				), tdv.TableRowID AS [AssetUniqueId], ISNULL(tdv_comments.DetailValue, '') AS [Comment]
				FROM TableDetailValues tdv WITH (NOLOCK) 
				INNER JOIN TableDetailValues tdv_comments WITH (NOLOCK) ON tdv_comments.TableRowID = tdv.TableRowID AND tdv_comments.DetailFieldID = 297625
				INNER JOIN TableDetailValues tdv_type WITH (NOLOCK) ON tdv_type.TableRowID = tdv.TableRowID AND tdv_type.DetailFieldID = 297616 AND tdv_type.ValueInt = 178950
				WHERE tdv.DetailFieldID = 297627
				AND tdv.ValueInt = 179658
				AND tdv.CustomerID = @CustomerID
				FOR XML PATH ('StocksShares'), Type
				),
				(SELECT '' AS [IssuerName], '' AS [Address], (SELECT tdv_cmv.DetailValue AS [CurrentMarketValue], tdv_do.DetailValue AS [DebtorOwnership], tdv_mi.DetailValue AS [MonthlyIncome]
					FROM TableDetailValues tdv_cmv WITH (NOLOCK)
					INNER JOIN TableDetailValues tdv_do WITH (NOLOCK) ON tdv_do.TableRowID = tdv_cmv.TableRowID AND tdv_do.DetailFieldID = 297621
					INNER JOIN TableDetailValues tdv_mi WITH (NOLOCK) ON tdv_mi.TableRowID = tdv.TableRowID AND tdv_mi.DetailFieldID = 297623
					WHERE tdv_cmv.TableRowID = tdv.TableRowID
					AND tdv_cmv.DetailFieldID = 297620
				FOR XML PATH ('InvestmentDetails'), TYPE
				), tdv.TableRowID AS [AssetUniqueId], ISNULL(tdv_comments.DetailValue, '') AS [Comment]
				FROM TableDetailValues tdv WITH (NOLOCK) 
				INNER JOIN TableDetailValues tdv_comments WITH (NOLOCK) ON tdv_comments.TableRowID = tdv.TableRowID AND tdv_comments.DetailFieldID = 297625
				INNER JOIN TableDetailValues tdv_type WITH (NOLOCK) ON tdv_type.TableRowID = tdv.TableRowID AND tdv_type.DetailFieldID = 297616 AND tdv_type.ValueInt = 178951
				WHERE tdv.DetailFieldID = 297627
				AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
				AND tdv.CustomerID = @CustomerID
				FOR XML PATH ('Bonds'), Type
				),
				(SELECT '' AS [IssuerName], '' AS [Address], (SELECT tdv_cmv.DetailValue AS [CurrentMarketValue], tdv_do.DetailValue AS [DebtorOwnership], tdv_mi.DetailValue AS [MonthlyIncome]
					FROM TableDetailValues tdv_cmv WITH (NOLOCK)
					INNER JOIN TableDetailValues tdv_do WITH (NOLOCK) ON tdv_do.TableRowID = tdv_cmv.TableRowID AND tdv_do.DetailFieldID = 297621
					INNER JOIN TableDetailValues tdv_mi WITH (NOLOCK) ON tdv_mi.TableRowID = tdv.TableRowID AND tdv_mi.DetailFieldID = 297623
					WHERE tdv_cmv.TableRowID = tdv.TableRowID
					AND tdv_cmv.DetailFieldID = 297620
				FOR XML PATH ('InvestmentDetails'), TYPE
				), tdv.TableRowID AS [AssetUniqueId], ISNULL(tdv_comments.DetailValue, '') AS [Comment]
				FROM TableDetailValues tdv WITH (NOLOCK) 
				INNER JOIN TableDetailValues tdv_comments WITH (NOLOCK) ON tdv_comments.TableRowID = tdv.TableRowID AND tdv_comments.DetailFieldID = 297625
				INNER JOIN TableDetailValues tdv_type WITH (NOLOCK) ON tdv_type.TableRowID = tdv.TableRowID AND tdv_type.DetailFieldID = 297616 AND tdv_type.ValueInt = 178952
				WHERE tdv.DetailFieldID = 297627
				AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
				AND tdv.CustomerID = @CustomerID
				FOR XML PATH ('EndowmentPolicies'), Type
				),
				(SELECT '' AS [IssuerName], '' AS [Address], (SELECT tdv_cmv.DetailValue AS [CurrentMarketValue], tdv_do.DetailValue AS [DebtorOwnership], tdv_mi.DetailValue AS [MonthlyIncome]
					FROM TableDetailValues tdv_cmv WITH (NOLOCK)
					INNER JOIN TableDetailValues tdv_do WITH (NOLOCK) ON tdv_do.TableRowID = tdv_cmv.TableRowID AND tdv_do.DetailFieldID = 297621
					INNER JOIN TableDetailValues tdv_mi WITH (NOLOCK) ON tdv_mi.TableRowID = tdv.TableRowID AND tdv_mi.DetailFieldID = 297623
					WHERE tdv_cmv.TableRowID = tdv.TableRowID
					AND tdv_cmv.DetailFieldID = 297620
				FOR XML PATH ('InvestmentDetails'), TYPE
				), tdv.TableRowID AS [AssetUniqueId], ISNULL(tdv_comments.DetailValue, '') AS [Comment]
				FROM TableDetailValues tdv WITH (NOLOCK) 
				INNER JOIN TableDetailValues tdv_comments WITH (NOLOCK) ON tdv_comments.TableRowID = tdv.TableRowID AND tdv_comments.DetailFieldID = 297625
				INNER JOIN TableDetailValues tdv_type WITH (NOLOCK) ON tdv_type.TableRowID = tdv.TableRowID AND tdv_type.DetailFieldID = 297616 AND tdv_type.ValueInt = 178953
				WHERE tdv.DetailFieldID = 297627
				AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
				AND tdv.CustomerID = @CustomerID
				FOR XML PATH ('Pension'), Type
				),
				(SELECT '' , (SELECT tdv_cmv.DetailValue AS [CurrentMarketValue], tdv_do.DetailValue AS [DebtorOwnership], tdv_mi.DetailValue AS [MonthlyIncome]
					FROM TableDetailValues tdv_cmv WITH (NOLOCK)
					INNER JOIN TableDetailValues tdv_do WITH (NOLOCK) ON tdv_do.TableRowID = tdv_cmv.TableRowID AND tdv_do.DetailFieldID = 297621
					INNER JOIN TableDetailValues tdv_mi WITH (NOLOCK) ON tdv_mi.TableRowID = tdv.TableRowID AND tdv_mi.DetailFieldID = 297623
					WHERE tdv_cmv.TableRowID = tdv.TableRowID
					AND tdv_cmv.DetailFieldID = 297620
				FOR XML PATH ('InvestmentDetails'), TYPE
				), luli_is.ItemValue AS [IsAssetInTheState], tdv.TableRowID AS [AssetUniqueId], ISNULL(tdv_comments.DetailValue, '') AS [Comment]
				FROM TableDetailValues tdv WITH (NOLOCK) 
				INNER JOIN TableDetailValues tdv_instate WITH (NOLOCK) ON tdv_instate.TableRowID = tdv.TableRowID AND tdv_instate.DetailFieldID = 297624
				LEFT JOIN LookupListItems luli_is WITH (NOLOCK) ON luli_is.LookupListItemID = tdv_instate.ValueInt
				INNER JOIN TableDetailValues tdv_comments WITH (NOLOCK) ON tdv_comments.TableRowID = tdv.TableRowID AND tdv_comments.DetailFieldID = 297625
				INNER JOIN TableDetailValues tdv_type WITH (NOLOCK) ON tdv_type.TableRowID = tdv.TableRowID AND tdv_type.DetailFieldID = 297616 AND tdv_type.ValueInt = 178954
				WHERE tdv.DetailFieldID = 297627
				AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
				AND tdv.CustomerID = @CustomerID
				FOR XML PATH ('Antiques'), Type
				),
				(SELECT '' , (SELECT tdv_cmv.DetailValue AS [CurrentMarketValue], tdv_do.DetailValue AS [DebtorOwnership], tdv_mi.DetailValue AS [MonthlyIncome]
					FROM TableDetailValues tdv_cmv WITH (NOLOCK)
					INNER JOIN TableDetailValues tdv_do WITH (NOLOCK) ON tdv_do.TableRowID = tdv_cmv.TableRowID AND tdv_do.DetailFieldID = 297621
					INNER JOIN TableDetailValues tdv_mi WITH (NOLOCK) ON tdv_mi.TableRowID = tdv.TableRowID AND tdv_mi.DetailFieldID = 297623
					WHERE tdv_cmv.TableRowID = tdv.TableRowID
					AND tdv_cmv.DetailFieldID = 297620
				FOR XML PATH ('InvestmentDetails'), TYPE
				), luli_is.ItemValue AS [IsAssetInTheState], tdv.TableRowID AS [AssetUniqueId], ISNULL(tdv_comments.DetailValue, '') AS [Comment]
				FROM TableDetailValues tdv WITH (NOLOCK) 
				INNER JOIN TableDetailValues tdv_instate WITH (NOLOCK) ON tdv_instate.TableRowID = tdv.TableRowID AND tdv_instate.DetailFieldID = 297624
				LEFT JOIN LookupListItems luli_is WITH (NOLOCK) ON luli_is.LookupListItemID = tdv_instate.ValueInt
				INNER JOIN TableDetailValues tdv_comments WITH (NOLOCK) ON tdv_comments.TableRowID = tdv.TableRowID AND tdv_comments.DetailFieldID = 297625
				INNER JOIN TableDetailValues tdv_type WITH (NOLOCK) ON tdv_type.TableRowID = tdv.TableRowID AND tdv_type.DetailFieldID = 297616 AND tdv_type.ValueInt = 178955
				WHERE tdv.DetailFieldID = 297627
				AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
				AND tdv.CustomerID = @CustomerID
				FOR XML PATH ('PreciousMetalsJewellery'), Type
				),
				(SELECT '' , (SELECT tdv_cmv.DetailValue AS [CurrentMarketValue], tdv_do.DetailValue AS [DebtorOwnership], tdv_mi.DetailValue AS [MonthlyIncome]
					FROM TableDetailValues tdv_cmv WITH (NOLOCK)
					INNER JOIN TableDetailValues tdv_do WITH (NOLOCK) ON tdv_do.TableRowID = tdv_cmv.TableRowID AND tdv_do.DetailFieldID = 297621
					INNER JOIN TableDetailValues tdv_mi WITH (NOLOCK) ON tdv_mi.TableRowID = tdv.TableRowID AND tdv_mi.DetailFieldID = 297623
					WHERE tdv_cmv.TableRowID = tdv.TableRowID
					AND tdv_cmv.DetailFieldID = 297620
				FOR XML PATH ('InvestmentDetails'), TYPE
				), luli_is.ItemValue AS [IsAssetInTheState], tdv.TableRowID AS [AssetUniqueId], ISNULL(tdv_comments.DetailValue, '') AS [Comment]
				FROM TableDetailValues tdv WITH (NOLOCK) 
				INNER JOIN TableDetailValues tdv_instate WITH (NOLOCK) ON tdv_instate.TableRowID = tdv.TableRowID AND tdv_instate.DetailFieldID = 297624
				LEFT JOIN LookupListItems luli_is WITH (NOLOCK) ON luli_is.LookupListItemID = tdv_instate.ValueInt
				INNER JOIN TableDetailValues tdv_comments WITH (NOLOCK) ON tdv_comments.TableRowID = tdv.TableRowID AND tdv_comments.DetailFieldID = 297625
				INNER JOIN TableDetailValues tdv_type WITH (NOLOCK) ON tdv_type.TableRowID = tdv.TableRowID AND tdv_type.DetailFieldID = 297616 AND tdv_type.ValueInt = 178956
				WHERE tdv.DetailFieldID = 297627
				AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
				AND tdv.CustomerID = @CustomerID
				FOR XML PATH ('PrizeBonds'), Type
				),
				(SELECT '' , (SELECT tdv_cmv.DetailValue AS [CurrentMarketValue], tdv_do.DetailValue AS [DebtorOwnership], tdv_mi.DetailValue AS [MonthlyIncome]
					FROM TableDetailValues tdv_cmv WITH (NOLOCK)
					INNER JOIN TableDetailValues tdv_do WITH (NOLOCK) ON tdv_do.TableRowID = tdv_cmv.TableRowID AND tdv_do.DetailFieldID = 297621
					INNER JOIN TableDetailValues tdv_mi WITH (NOLOCK) ON tdv_mi.TableRowID = tdv.TableRowID AND tdv_mi.DetailFieldID = 297623
					WHERE tdv_cmv.TableRowID = tdv.TableRowID
					AND tdv_cmv.DetailFieldID = 297620
				FOR XML PATH ('InvestmentDetails'), TYPE
				), luli_is.ItemValue AS [IsAssetInTheState], tdv.TableRowID AS [AssetUniqueId], ISNULL(tdv_comments.DetailValue, '') AS [Comment]
				FROM TableDetailValues tdv WITH (NOLOCK) 
				INNER JOIN TableDetailValues tdv_instate WITH (NOLOCK) ON tdv_instate.TableRowID = tdv.TableRowID AND tdv_instate.DetailFieldID = 297624
				LEFT JOIN LookupListItems luli_is WITH (NOLOCK) ON luli_is.LookupListItemID = tdv_instate.ValueInt
				INNER JOIN TableDetailValues tdv_comments WITH (NOLOCK) ON tdv_comments.TableRowID = tdv.TableRowID AND tdv_comments.DetailFieldID = 297625
				INNER JOIN TableDetailValues tdv_type WITH (NOLOCK) ON tdv_type.TableRowID = tdv.TableRowID AND tdv_type.DetailFieldID = 297616 AND tdv_type.ValueInt = 178957
				WHERE tdv.DetailFieldID = 297627
				AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
				AND tdv.CustomerID = @CustomerID
				FOR XML PATH ('LiveStock'), Type
				),
				(SELECT '' AS [OtherDetails], (SELECT tdv_cmv.DetailValue AS [CurrentMarketValue], tdv_do.DetailValue AS [DebtorOwnership], tdv_mi.DetailValue AS [MonthlyIncome]
					FROM TableDetailValues tdv_cmv WITH (NOLOCK)
					INNER JOIN TableDetailValues tdv_do WITH (NOLOCK) ON tdv_do.TableRowID = tdv_cmv.TableRowID AND tdv_do.DetailFieldID = 297621
					INNER JOIN TableDetailValues tdv_mi WITH (NOLOCK) ON tdv_mi.TableRowID = tdv.TableRowID AND tdv_mi.DetailFieldID = 297623
					WHERE tdv_cmv.TableRowID = tdv.TableRowID
					AND tdv_cmv.DetailFieldID = 297620
				FOR XML PATH ('InvestmentDetails'), TYPE
				), luli_is.ItemValue AS [IsAssetInTheState], tdv.TableRowID AS [AssetUniqueId], ISNULL(tdv_comments.DetailValue, '') AS [Comment]
				FROM TableDetailValues tdv WITH (NOLOCK) 
				INNER JOIN TableDetailValues tdv_instate WITH (NOLOCK) ON tdv_instate.TableRowID = tdv.TableRowID AND tdv_instate.DetailFieldID = 297624
				LEFT JOIN LookupListItems luli_is WITH (NOLOCK) ON luli_is.LookupListItemID = tdv_instate.ValueInt
				INNER JOIN TableDetailValues tdv_comments WITH (NOLOCK) ON tdv_comments.TableRowID = tdv.TableRowID AND tdv_comments.DetailFieldID = 297625
				INNER JOIN TableDetailValues tdv_type WITH (NOLOCK) ON tdv_type.TableRowID = tdv.TableRowID AND tdv_type.DetailFieldID = 297616 AND tdv_type.ValueInt = 178958
				WHERE tdv.DetailFieldID = 297627
				AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
				AND tdv.CustomerID = @CustomerID
				FOR XML PATH ('InvestmentOther'), Type
				)
			FOR XML PATH ('NonPropertyInvestment'), Type
			),(
			SELECT tdv_desc.DetailValue AS [Description], (
					SELECT tdv_cmv.ValueMoney AS [CurrentMarketValue], tdv_ownership.ValueMoney AS [DebtorOwnership]
					FROM TableDetailValues tdv_cmv WITH (NOLOCK)
					INNER JOIN TableDetailValues tdv_ownership WITH (NOLOCK) ON tdv_ownership.TableRowID = tdv_cmv.TableRowID AND tdv_ownership.DetailFieldID = 297635
					WHERE tdv_cmv.TableRowID = tdv.TableRowID
					AND tdv_cmv.DetailFieldID = 297634
					FOR XML PATH (''), type
					) AS [InvestmentDetails], luli.ItemValue [IsAssetInTheState], tdv.TableRowID AS [AssetUniqueId], ISNULL(tdv_comment.DetailValue, '') AS [Comment]
			FROM TableDetailValues tdv WITH (NOLOCK)
			INNER JOIN TableDetailValues tdv_desc WITH (NOLOCK) ON tdv_desc.TableRowID = tdv.TableRowID AND tdv_desc.DetailFieldID = 297630
			INNER JOIN TableDetailValues tdv_assetinstate WITH (NOLOCK) ON tdv_assetinstate.TableRowID = tdv.TableRowID AND tdv_assetinstate.DetailFieldID = 297637
			LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_assetinstate.ValueInt
			INNER JOIN TableDetailValues tdv_comment WITH (NOLOCK) ON tdv_comment.TableRowID = tdv.TableRowID AND tdv_comment.DetailFieldID = 297638
			WHERE tdv.DetailFieldID = 297639
			AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
			AND tdv.CustomerID = @CustomerID 
			FOR XML PATH ('PlantEquipmentTools'), Type
			) ,(
			SELECT tdv_make.DetailValue AS [Make], 
					tdv_model.DetailValue AS [Model], 
					tdv_year.DetailValue AS [Year], 
					tdv_regno.DetailValue AS [RegistrationNumber],
					tdv_km.DetailValue AS [Kilometers],
					tdv_needforveh.DetailValue AS [NeedForVehicle],
					tdv_cmr.DetailValue AS [CurrentMarketValue],
					luli_stf.ItemValue AS [SubjectToFinance],
					tdv_outstandingbalance.DetailValue AS [OutstandingBalance],
					tdv_monthlyInstallment.DetailValue AS [MonthlyInstalment],
					luli_adapted.ItemValue AS [AdaptedForDisabledUse],
					luli.ItemValue [IsAssetInTheState], 
					tdv.TableRowID AS [AssetUniqueId], 
					ISNULL(tdv_comment.DetailValue, '') AS [Comment]
			FROM TableDetailValues tdv WITH (NOLOCK)
			INNER JOIN TableDetailValues tdv_adapted WITH (NOLOCK) ON tdv_adapted.TableRowID = tdv.TableRowID AND tdv_adapted.DetailFieldID = 297653
			LEFT JOIN LookupListItems luli_adapted WITH (NOLOCK) ON luli_adapted.LookupListItemID = tdv_adapted.ValueInt
			INNER JOIN TableDetailValues tdv_monthlyInstallment WITH (NOLOCK) ON tdv_monthlyInstallment.TableRowID = tdv.TableRowID AND tdv_monthlyInstallment.DetailFieldID = 297657
			INNER JOIN TableDetailValues tdv_outstandingbalance WITH (NOLOCK) ON tdv_outstandingbalance.TableRowID = tdv.TableRowID AND tdv_outstandingbalance.DetailFieldID = 297656
			INNER JOIN TableDetailValues tdv_subjecttofinance WITH (NOLOCK) ON tdv_subjecttofinance.TableRowID = tdv.TableRowID AND tdv_subjecttofinance.DetailFieldID = 297652
			LEFT JOIN LookupListItems luli_stf WITH (NOLOCK) ON luli_stf.LookupListItemID = tdv_subjecttofinance.ValueInt 
			INNER JOIN TableDetailValues tdv_cmr WITH (NOLOCK) ON tdv_cmr.TableRowID = tdv.TableRowID AND tdv_cmr.DetailFieldID = 297651
			INNER JOIN TableDetailValues tdv_needforveh WITH (NOLOCK) ON tdv_needforveh.TableRowID = tdv.TableRowID AND tdv_needforveh.DetailFieldID = 297647
			INNER JOIN TableDetailValues tdv_km WITH (NOLOCK) ON tdv_km.TableRowID = tdv.TableRowID AND tdv_km.DetailFieldID = 297646
			INNER JOIN TableDetailValues tdv_regno WITH (NOLOCK) ON tdv_regno.TableRowID = tdv.TableRowID AND tdv_regno.DetailFieldID = 297645
			INNER JOIN TableDetailValues tdv_year WITH (NOLOCK) ON tdv_year.TableRowID = tdv.TableRowID AND tdv_year.DetailFieldID = 297644
			INNER JOIN TableDetailValues tdv_model WITH (NOLOCK) ON tdv_model.TableRowID = tdv.TableRowID AND tdv_model.DetailFieldID = 297643
			INNER JOIN TableDetailValues tdv_make WITH (NOLOCK) ON tdv_make.TableRowID = tdv.TableRowID AND tdv_make.DetailFieldID = 297642
			INNER JOIN TableDetailValues tdv_assetinstate WITH (NOLOCK) ON tdv_assetinstate.TableRowID = tdv.TableRowID AND tdv_assetinstate.DetailFieldID = 297654
			LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_assetinstate.ValueInt
			INNER JOIN TableDetailValues tdv_comment WITH (NOLOCK) ON tdv_comment.TableRowID = tdv.TableRowID AND tdv_comment.DetailFieldID = 297655
			WHERE tdv.DetailFieldID = 297658
			AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
			AND tdv.CustomerID = @CustomerID 
			FOR XML PATH ('Vehicle'), Type
			), (
			SELECT tdv_cmr.DetailValue AS [CurrentMarketValue],
					luli.ItemValue [IsAssetInTheState], 
					tdv.TableRowID AS [AssetUniqueId], 
					ISNULL(tdv_comment.DetailValue, '') AS [Comment]
			FROM TableDetailValues tdv WITH (NOLOCK)
			INNER JOIN TableDetailValues tdv_cmr WITH (NOLOCK) ON tdv_cmr.TableRowID = tdv.TableRowID AND tdv_cmr.DetailFieldID = 297661
			INNER JOIN TableDetailValues tdv_assetinstate WITH (NOLOCK) ON tdv_assetinstate.TableRowID = tdv.TableRowID AND tdv_assetinstate.DetailFieldID = 297662
			LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_assetinstate.ValueInt
			INNER JOIN TableDetailValues tdv_comment WITH (NOLOCK) ON tdv_comment.TableRowID = tdv.TableRowID AND tdv_comment.DetailFieldID = 297663
			WHERE tdv.DetailFieldID = 297664
			AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
			AND tdv.CustomerID = @CustomerID 
			FOR XML PATH ('StockInTrade'), Type
			), (
			SELECT tdv_debtorName.DetailValue AS [DebtorName],
					dbo.fn_C00_ISI_ReturnXmlAddress (tdv_adl1.DetailValue, tdv_adl2.DetailValue, tdv_adl3.DetailValue, tdv_adl4.DetailValue, tdv_adl5.DetailValue, '', ISNULL(luli.ItemValue, 'Ireland')) AS [Address],
					tdv_bookValue.DetailValue AS [BookValue],
					tdv_realisableValue.DetailValue AS [RealisableValue],
					tdv.TableRowID AS [AssetUniqueId], 
					ISNULL(tdv_comment.DetailValue, '') AS [Comment]
			FROM TableDetailValues tdv WITH (NOLOCK)
			INNER JOIN TableDetailValues tdv_realisableValue WITH (NOLOCK) ON tdv_realisableValue.TableRowID = tdv.TableRowID AND tdv_realisableValue.DetailFieldID = 297675
			INNER JOIN TableDetailValues tdv_bookValue WITH (NOLOCK) ON tdv_bookValue.TableRowID = tdv.TableRowID AND tdv_bookValue.DetailFieldID = 297674
			INNER JOIN TableDetailValues tdv_adl1 WITH (NOLOCK) ON tdv_adl1.TableRowID = tdv.TableRowID AND tdv_adl1.DetailFieldID = 297669
			INNER JOIN TableDetailValues tdv_adl2 WITH (NOLOCK) ON tdv_adl2.TableRowID = tdv.TableRowID AND tdv_adl2.DetailFieldID = 297670
			INNER JOIN TableDetailValues tdv_adl3 WITH (NOLOCK) ON tdv_adl3.TableRowID = tdv.TableRowID AND tdv_adl3.DetailFieldID = 297671
			INNER JOIN TableDetailValues tdv_adl4 WITH (NOLOCK) ON tdv_adl4.TableRowID = tdv.TableRowID AND tdv_adl4.DetailFieldID = 297672
			INNER JOIN TableDetailValues tdv_adl5 WITH (NOLOCK) ON tdv_adl5.TableRowID = tdv.TableRowID AND tdv_adl5.DetailFieldID = 297673
			INNER JOIN TableDetailValues tdv_Country WITH (NOLOCK) ON tdv_Country.TableRowID = tdv.TableRowID AND tdv_Country.DetailFieldID = 297668
			LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_Country.ValueInt
			INNER JOIN TableDetailValues tdv_debtorName WITH (NOLOCK) ON tdv_debtorName.TableRowID = tdv.TableRowID AND tdv_debtorName.DetailFieldID = 297667
			INNER JOIN TableDetailValues tdv_comment WITH (NOLOCK) ON tdv_comment.TableRowID = tdv.TableRowID AND tdv_comment.DetailFieldID = 297676
			WHERE tdv.DetailFieldID = 297677
			AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
			AND tdv.CustomerID = @CustomerID 
			FOR XML PATH ('MoneyOwedToYou'), Type
			), (
			SELECT tdv_bankName.DetailValue AS [BankName],
					dbo.fn_C00_ISI_ReturnXmlAddress (tdv_adl1.DetailValue, tdv_adl2.DetailValue, tdv_adl3.DetailValue, tdv_adl4.DetailValue, tdv_adl5.DetailValue, '', ISNULL(luli.ItemValue, 'Ireland')) AS [Address],
					tdv_accountName.DetailValue AS [AccountName],
					tdv_accountNumber.DetailValue AS [AccountNumber],
					tdv_balance.DetailValue AS [Balance],
					tdv_debtorOwnership.DetailValue AS [DebtorOwnership],
					tdv.TableRowID AS [AssetUniqueId], 
					ISNULL(tdv_comment.DetailValue, '') AS [Comment]
			FROM TableDetailValues tdv WITH (NOLOCK)
			INNER JOIN TableDetailValues tdv_debtorOwnership WITH (NOLOCK) ON tdv_debtorOwnership.TableRowID = tdv.TableRowID AND tdv_debtorOwnership.DetailFieldID = 297691
			INNER JOIN TableDetailValues tdv_balance WITH (NOLOCK) ON tdv_balance.TableRowID = tdv.TableRowID AND tdv_balance.DetailFieldID = 297690
			INNER JOIN TableDetailValues tdv_accountName WITH (NOLOCK) ON tdv_accountName.TableRowID = tdv.TableRowID AND tdv_accountName.DetailFieldID = 297687
			INNER JOIN TableDetailValues tdv_accountNumber WITH (NOLOCK) ON tdv_accountNumber.TableRowID = tdv.TableRowID AND tdv_accountNumber.DetailFieldID = 297688
			INNER JOIN TableDetailValues tdv_bankName WITH (NOLOCK) ON tdv_bankName.TableRowID = tdv.TableRowID AND tdv_bankName.DetailFieldID = 297680
			INNER JOIN TableDetailValues tdv_adl1 WITH (NOLOCK) ON tdv_adl1.TableRowID = tdv.TableRowID AND tdv_adl1.DetailFieldID = 297682
			INNER JOIN TableDetailValues tdv_adl2 WITH (NOLOCK) ON tdv_adl2.TableRowID = tdv.TableRowID AND tdv_adl2.DetailFieldID = 297683
			INNER JOIN TableDetailValues tdv_adl3 WITH (NOLOCK) ON tdv_adl3.TableRowID = tdv.TableRowID AND tdv_adl3.DetailFieldID = 297684
			INNER JOIN TableDetailValues tdv_adl4 WITH (NOLOCK) ON tdv_adl4.TableRowID = tdv.TableRowID AND tdv_adl4.DetailFieldID = 297685
			INNER JOIN TableDetailValues tdv_adl5 WITH (NOLOCK) ON tdv_adl5.TableRowID = tdv.TableRowID AND tdv_adl5.DetailFieldID = 297686
			INNER JOIN TableDetailValues tdv_Country WITH (NOLOCK) ON tdv_Country.TableRowID = tdv.TableRowID AND tdv_Country.DetailFieldID = 297681
			LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_Country.ValueInt
			INNER JOIN TableDetailValues tdv_comment WITH (NOLOCK) ON tdv_comment.TableRowID = tdv.TableRowID AND tdv_comment.DetailFieldID = 297693
			WHERE tdv.DetailFieldID = 297694
			AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
			AND tdv.CustomerID = @CustomerID 
			FOR XML PATH ('BankAccount'), Type
			), (
			SELECT tdv_unionName.DetailValue AS [CreditUnionName],
					dbo.fn_C00_ISI_ReturnXmlAddress (tdv_adl1.DetailValue, tdv_adl2.DetailValue, tdv_adl3.DetailValue, tdv_adl4.DetailValue, tdv_adl5.DetailValue, '', ISNULL(luli.ItemValue, 'Ireland')) AS [Address],
					tdv_accountName.DetailValue AS [AccountName],
					tdv_accountNumber.DetailValue AS [AccountNumber],
					tdv_cmv.DetailValue AS [CurrentMarketValue],
					tdv_debtorOwnership.DetailValue AS [DebtorOwnership],
					tdv.TableRowID AS [AssetUniqueId], 
					ISNULL(tdv_comment.DetailValue, '') AS [Comment]
			FROM TableDetailValues tdv WITH (NOLOCK)
			INNER JOIN TableDetailValues tdv_debtorOwnership WITH (NOLOCK) ON tdv_debtorOwnership.TableRowID = tdv.TableRowID AND tdv_debtorOwnership.DetailFieldID = 297707
			INNER JOIN TableDetailValues tdv_cmv WITH (NOLOCK) ON tdv_cmv.TableRowID = tdv.TableRowID AND tdv_cmv.DetailFieldID = 297706
			INNER JOIN TableDetailValues tdv_accountName WITH (NOLOCK) ON tdv_accountName.TableRowID = tdv.TableRowID AND tdv_accountName.DetailFieldID = 297704
			INNER JOIN TableDetailValues tdv_accountNumber WITH (NOLOCK) ON tdv_accountNumber.TableRowID = tdv.TableRowID AND tdv_accountNumber.DetailFieldID = 297705
			INNER JOIN TableDetailValues tdv_unionName WITH (NOLOCK) ON tdv_unionName.TableRowID = tdv.TableRowID AND tdv_unionName.DetailFieldID = 297697
			INNER JOIN TableDetailValues tdv_adl1 WITH (NOLOCK) ON tdv_adl1.TableRowID = tdv.TableRowID AND tdv_adl1.DetailFieldID = 297699
			INNER JOIN TableDetailValues tdv_adl2 WITH (NOLOCK) ON tdv_adl2.TableRowID = tdv.TableRowID AND tdv_adl2.DetailFieldID = 297700
			INNER JOIN TableDetailValues tdv_adl3 WITH (NOLOCK) ON tdv_adl3.TableRowID = tdv.TableRowID AND tdv_adl3.DetailFieldID = 297701
			INNER JOIN TableDetailValues tdv_adl4 WITH (NOLOCK) ON tdv_adl4.TableRowID = tdv.TableRowID AND tdv_adl4.DetailFieldID = 297702
			INNER JOIN TableDetailValues tdv_adl5 WITH (NOLOCK) ON tdv_adl5.TableRowID = tdv.TableRowID AND tdv_adl5.DetailFieldID = 297703
			INNER JOIN TableDetailValues tdv_Country WITH (NOLOCK) ON tdv_Country.TableRowID = tdv.TableRowID AND tdv_Country.DetailFieldID = 297698
			LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_Country.ValueInt
			INNER JOIN TableDetailValues tdv_comment WITH (NOLOCK) ON tdv_comment.TableRowID = tdv.TableRowID AND tdv_comment.DetailFieldID = 297709
			WHERE tdv.DetailFieldID = 297710
			AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
			AND tdv.CustomerID = @CustomerID 
			FOR XML PATH ('CreditUnionAccount'), Type
			),(
			SELECT tdv_amount.DetailValue AS [Amount],
					luli.ItemValue AS [IsAssetInTheState],
					tdv.TableRowID AS [AssetUniqueId], 
					ISNULL(tdv_comment.DetailValue, '') AS [Comment]
			FROM TableDetailValues tdv WITH (NOLOCK)
			INNER JOIN TableDetailValues tdv_amount WITH (NOLOCK) ON tdv_amount.TableRowID = tdv.TableRowID AND tdv_amount.DetailFieldID = 297713
			INNER JOIN TableDetailValues tdv_comment WITH (NOLOCK) ON tdv_comment.TableRowID = tdv.TableRowID AND tdv_comment.DetailFieldID = 297715
			INNER JOIN TableDetailValues tdv_assetinstate WITH (NOLOCK) ON tdv_assetinstate.TableRowID = tdv.TableRowID AND tdv_assetinstate.DetailFieldID = 297714
			LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_assetinstate.ValueInt
			WHERE tdv.DetailFieldID = 297716
			AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
			AND tdv.CustomerID = @CustomerID 
			FOR XML PATH ('CashOnHand'), TYPE
			),(
			SELECT tdv_desc.DetailValue AS [Description],
					tdv_estValue.DetailValue AS [EstimatedValue],
					tdv_recDate.DetailValue AS [ReceiptEstimatedDate],
					luli.ItemValue AS [IsAssetInTheState],
					tdv.TableRowID AS [AssetUniqueId], 
					ISNULL(tdv_comment.DetailValue, '') AS [Comment]
			FROM TableDetailValues tdv WITH (NOLOCK)
			INNER JOIN TableRows tr WITH (NOLOCK) ON tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID = 297520
			INNER JOIN TableDetailValues tdv_desc WITH (NOLOCK) ON tdv_desc.TableRowID = tdv.TableRowID AND tdv_desc.DetailFieldID = 297719
			INNER JOIN TableDetailValues tdv_estValue WITH (NOLOCK) ON tdv_estValue.TableRowID = tdv.TableRowID AND tdv_estValue.DetailFieldID = 297720
			INNER JOIN TableDetailValues tdv_recDate WITH (NOLOCK) ON tdv_recDate.TableRowID = tdv.TableRowID AND tdv_recDate.DetailFieldID = 297721
			INNER JOIN TableDetailValues tdv_comment WITH (NOLOCK) ON tdv_comment.TableRowID = tdv.TableRowID AND tdv_comment.DetailFieldID = 297723
			INNER JOIN TableDetailValues tdv_assetinstate WITH (NOLOCK) ON tdv_assetinstate.TableRowID = tdv.TableRowID AND tdv_assetinstate.DetailFieldID = 297722
			LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_assetinstate.ValueInt
			WHERE tdv.DetailFieldID = 297724
			AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
			AND tdv.CustomerID = @CustomerID 
			FOR XML PATH ('ProspectiveAsset'), TYPE
			),(
			SELECT tdv_desc.DetailValue AS [Description],
					tdv_estValue.DetailValue AS [EstimatedValue],
					tdv_recDate.DetailValue AS [ReceiptEstimatedDate],
					luli.ItemValue AS [IsAssetInTheState],
					tdv.TableRowID AS [AssetUniqueId], 
					ISNULL(tdv_comment.DetailValue, '') AS [Comment]
			FROM TableDetailValues tdv WITH (NOLOCK)
			INNER JOIN TableRows tr WITH (NOLOCK) ON tr.TableRowID = tdv.TableRowID AND tr.DetailFieldID = 297521
			INNER JOIN TableDetailValues tdv_desc WITH (NOLOCK) ON tdv_desc.TableRowID = tdv.TableRowID AND tdv_desc.DetailFieldID = 297719
			INNER JOIN TableDetailValues tdv_estValue WITH (NOLOCK) ON tdv_estValue.TableRowID = tdv.TableRowID AND tdv_estValue.DetailFieldID = 297720
			INNER JOIN TableDetailValues tdv_recDate WITH (NOLOCK) ON tdv_recDate.TableRowID = tdv.TableRowID AND tdv_recDate.DetailFieldID = 297721
			INNER JOIN TableDetailValues tdv_comment WITH (NOLOCK) ON tdv_comment.TableRowID = tdv.TableRowID AND tdv_comment.DetailFieldID = 297723
			INNER JOIN TableDetailValues tdv_assetinstate WITH (NOLOCK) ON tdv_assetinstate.TableRowID = tdv.TableRowID AND tdv_assetinstate.DetailFieldID = 297722
			LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_assetinstate.ValueInt
			WHERE tdv.DetailFieldID = 297724
			AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
			AND tdv.CustomerID = @CustomerID 
			FOR XML PATH ('ContingentAsset'), TYPE
			),(
			SELECT tdv_desc.DetailValue AS [Description],
					tdv_estValue.DetailValue AS [EstimatedValue],
					luli.ItemValue AS [IsAssetInTheState],
					tdv.TableRowID AS [AssetUniqueId], 
					ISNULL(tdv_comment.DetailValue, '') AS [Comment]
			FROM TableDetailValues tdv WITH (NOLOCK)
			INNER JOIN TableDetailValues tdv_desc WITH (NOLOCK) ON tdv_desc.TableRowID = tdv.TableRowID AND tdv_desc.DetailFieldID = 297727
			INNER JOIN TableDetailValues tdv_estValue WITH (NOLOCK) ON tdv_estValue.TableRowID = tdv.TableRowID AND tdv_estValue.DetailFieldID = 297728
			INNER JOIN TableDetailValues tdv_comment WITH (NOLOCK) ON tdv_comment.TableRowID = tdv.TableRowID AND tdv_comment.DetailFieldID = 297730
			INNER JOIN TableDetailValues tdv_assetinstate WITH (NOLOCK) ON tdv_assetinstate.TableRowID = tdv.TableRowID AND tdv_assetinstate.DetailFieldID = 297729
			LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_assetinstate.ValueInt
			WHERE tdv.DetailFieldID = 297731
			AND (tdv.ValueInt = 179658 or tdv.ValueInt = 0)
			AND tdv.CustomerID = @CustomerID 
			FOR XML PATH ('Other'), TYPE
			)
		FOR XML PATH ('Asset')
		)

	RETURN @AssetXML
END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_XmlPush_Asset] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ISI_XmlPush_Asset] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_XmlPush_Asset] TO [sp_executeall]
GO
