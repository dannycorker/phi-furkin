SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-07-03
-- Description:	Returns 1 if any of the customer's leads have an uploaded to ultra tick
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_IsCustomerInDialler]
(
	@CustomerID INT
)
RETURNS INT
AS
BEGIN

	DECLARE @ReturnValue INT = -1
	
	IF EXISTS 
		(
		SELECT * 
		FROM dbo.Lead l WITH (NOLOCK) 
		INNER JOIN dbo.LeadDetailValues ldv WITH (NOLOCK) ON ldv.LeadID = l.LeadID
		INNER JOIN dbo.ThirdPartyFieldMapping tfm WITH (NOLOCK) ON tfm.DetailFieldID = ldv.DetailFieldID
		WHERE l.CustomerID = @CustomerID
		AND tfm.ThirdPartyFieldID = 114 /*Uploaded to Ultra*/
		AND tfm.ClientID = l.ClientID
		AND ldv.ValueInt = 1
		)
	BEGIN
		SELECT @ReturnValue = 1
	END
	ELSE
	BEGIN
		SELECT @ReturnValue = 0
	END
	
	RETURN @ReturnValue

END










GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_IsCustomerInDialler] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_IsCustomerInDialler] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_IsCustomerInDialler] TO [sp_executeall]
GO
