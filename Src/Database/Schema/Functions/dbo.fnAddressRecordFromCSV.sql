SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2008-09-30
-- Description:	Return a 1-row table of address details from an input string
-- =============================================
CREATE FUNCTION [dbo].[fnAddressRecordFromCSV]
(
	@instr nvarchar(max)
)
RETURNS 
@TableOut TABLE 
(
	Address1 nvarchar(200),
	Address2 nvarchar(200),
	Town nvarchar(200),
	County nvarchar(200),
	Postcode nvarchar(10)
)
AS
BEGIN

	DECLARE @delimiter nchar(1)
	DECLARE @whichfield varchar(10)
	DECLARE @position  int

	SET @delimiter = ','
	SET @instr = LTRIM(RTRIM(@instr)) + ','
	SET @position = CHARINDEX(@delimiter, @instr, 1)
	SET @whichfield = 'Address1'

	IF REPLACE(@instr, @delimiter, '') <> ''
	BEGIN
		WHILE @position > 0
		BEGIN
			IF @whichfield = 'Postcode'
			BEGIN
				UPDATE @TableOut 
				SET Postcode = LTRIM(RTRIM(LEFT(@instr, @position - 1)))

				SET @whichfield = '' -- Ignore any extra fields from now on
			END
			IF @whichfield = 'County'
			BEGIN
				UPDATE @TableOut 
				SET County = LTRIM(RTRIM(LEFT(@instr, @position - 1)))

				SET @whichfield = 'Postcode'
			END
			IF @whichfield = 'Town'
			BEGIN
				UPDATE @TableOut 
				SET Town = LTRIM(RTRIM(LEFT(@instr, @position - 1)))

				SET @whichfield = 'County'
			END
			IF @whichfield = 'Address2'
			BEGIN
				UPDATE @TableOut 
				SET Address2 = LTRIM(RTRIM(LEFT(@instr, @position - 1)))

				SET @whichfield = 'Town'
			END
			IF @whichfield = 'Address1'
			BEGIN
				INSERT INTO @TableOut (Address1) 
				SELECT LTRIM(RTRIM(LEFT(@instr, @position - 1)))

				SET @whichfield = 'Address2'
			END
			
			SET @instr = RIGHT(@instr, LEN(@instr) - @position)
			SET @position = CHARINDEX(@delimiter, @instr, 1)
		END
	END	
	RETURN
END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnAddressRecordFromCSV] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnAddressRecordFromCSV] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnAddressRecordFromCSV] TO [sp_executeall]
GO
