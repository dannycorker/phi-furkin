SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2021-04-14
-- Description:	Get db date format
-- =============================================
CREATE FUNCTION [dbo].[fnGetDbDateFormat]
(
	@ClientID INT
)
RETURNS INT
AS
BEGIN

	DECLARE @DBDateFormat INT = 103

	IF @ClientID IN (607)
	BEGIN

		SELECT @DBDateFormat = 101

	END

	RETURN @DbDateFormat 

END
GO
GRANT EXECUTE ON  [dbo].[fnGetDbDateFormat] TO [sp_executeall]
GO
