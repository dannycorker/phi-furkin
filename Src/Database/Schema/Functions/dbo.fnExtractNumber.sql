SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Paul Richardson>
-- Create date: <Create Date,,03/12/2009>
-- Description:	<Description,,Extracts all the numbers from a string>
-- =============================================
CREATE FUNCTION [dbo].[fnExtractNumber](@String VARCHAR(2000))
	RETURNS VARCHAR(1000)
AS
BEGIN
	DECLARE @Count INT
	DECLARE @IntNumbers VARCHAR(1000)
	SET @Count = 0
	SET @IntNumbers = ''

	WHILE @Count <= LEN(@String)
	BEGIN
		IF SUBSTRING(@String,@Count,1) >= '0' AND SUBSTRING(@String,@Count,1) <= '9'		
		BEGIN
			SET @IntNumbers = @IntNumbers + SUBSTRING(@String,@Count,1)
		END
		SET @Count = @Count + 1
	END
	RETURN @IntNumbers
END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnExtractNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnExtractNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnExtractNumber] TO [sp_executeall]
GO
