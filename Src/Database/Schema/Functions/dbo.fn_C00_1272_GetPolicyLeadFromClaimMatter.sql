SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-09-08
-- Description:	Returns the policy lead from a claim
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetPolicyLeadFromClaimMatter]
(
	@ClaimMatter INT
)
RETURNS INT	
AS
BEGIN

	DECLARE @ClaimLead INT,
			@PolicyLead INT
			
	SELECT @ClaimLead = LeadID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @ClaimMatter
	
	SELECT @PolicyLead = dbo.fn_C00_1272_GetPolicyLeadFromClaimLead(@ClaimLead)
	
	RETURN @PolicyLead

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicyLeadFromClaimMatter] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_GetPolicyLeadFromClaimMatter] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicyLeadFromClaimMatter] TO [sp_executeall]
GO
