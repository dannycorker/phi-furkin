SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2008-08-22
-- Description:	Calculate minutes between two times in format '(hhhhhh)hh:mm'
-- =============================================
CREATE FUNCTION [dbo].[fnTimeFromMinutes] 
(
	-- Add the parameters for the function here
	@totalmins int 
)
RETURNS varchar(11)
AS
BEGIN

	DECLARE @endtime varchar(11)

	-- CONVERT end time '15:30' to minutes (15 * 60) + 30
	-- Ditto for start time '09:15' (9 * 60) + 15
	-- Return the difference
	-- IF we go round the clock, add 24 hours to cater for night shifts like '23:00'to'07:00'
	SELECT @endtime = convert(varchar,(@totalmins / 60)) + ':' + right('00' + convert(varchar,(@totalmins % 60)),2)

	IF @endtime IS NULL 
	BEGIN
		SET @endtime = ''
	END 

	RETURN @endtime

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnTimeFromMinutes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnTimeFromMinutes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnTimeFromMinutes] TO [sp_executeall]
GO
