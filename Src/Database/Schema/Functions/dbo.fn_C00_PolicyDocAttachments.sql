SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Nessa Green
-- Create date: 2020-09-15
-- Description:	Return the titles of the attached policy documents
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_PolicyDocAttachments]
(
	@MatterID		INT,
	@TransType		INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE 
	@DocsList			VARCHAR(2000) = '',
	@EventTypeID		INT,
	@SVDetailFieldID	INT = dbo._C600_GetStateVarianceRLDetailFieldID (@MatterID, @TransType),
	@Separator			VARCHAR(10) = CHAR(13) + CHAR(10)

	SELECT @EventTypeID =	CASE @TransType WHEN 1 THEN 150139 /*new business*/
											WHEN 2 THEN 156864 /*MTA*/
											WHEN 3 THEN 150149 /*Renewal/MTA Transfer Ownership*/
											WHEN 4 THEN 157575 /*Reinstatement*/
							END

	/*Select the corresponding value from the correct detail field*/

	SELECT @DocsList += CASE WHEN @DocsList = '' THEN '' ELSE @Separator END + uf.UploadedFileTitle
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN CustomerDetailValues cdv WITH (NOLOCK) on cdv.CustomerID = m.CustomerID AND cdv.DetailFieldID = 314016 /*State Variance*/
	INNER JOIN ResourceListDetailValues rdv WITH (NOLOCK) on rdv.ResourceListID = cdv.ValueInt AND rdv.DetailFieldID = @SVDetailFieldID
	CROSS APPLY dbo.fnTableOfIDsFromCSV(rdv.DetailValue) fn
	INNER JOIN dbo.UploadedFileAttachment ufa WITH (NOLOCK) on ufa.UploadedFileID = fn.AnyID AND ufa.EventTypeID = @EventTypeID
	INNER JOIN dbo.UploadedFile uf WITH (NOLOCK) on uf.UploadedFileID = ufa.UploadedFileID
	WHERE m.MatterID = @MatterID
	ORDER BY uf.UploadedFileName
	
	RETURN @DocsList

END

GO
