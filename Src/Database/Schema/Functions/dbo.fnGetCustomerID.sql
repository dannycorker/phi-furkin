SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 12-10-2012
-- Description:	Returns the customer ID from the given identity 
-- =============================================
CREATE FUNCTION [dbo].[fnGetCustomerID]
(
	@ID INT,
	@DetailFieldSubTypeID INT
)
RETURNS INT
AS
BEGIN
	
		DECLARE @CustomerID INT
	
		IF @DetailFieldSubTypeID = 1 -- LEAD ID
		BEGIN
		
			SELECT @CustomerID = l.CustomerID FROM Lead l WITH (NOLOCK) WHERE l.LeadID=@ID
		
		END
		
		RETURN @CustomerID

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerID] TO [sp_executeall]
GO
