SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-12-18
-- Description:	Returns table of policy numbers & tokens for all unpaid CP premium rows
-- Mods
-- =============================================


CREATE FUNCTION [dbo].[fn_C384_GetUnpaidCPRows]
(
)
RETURNS 
@Data TABLE
(
	TableRowID INT,
	PolicyNumber VARCHAR(100),
	CPToken VARCHAR(100)
)	
AS
BEGIN
		
	INSERT @Data (TableRowID,PolicyNumber,CPToken)
	SELECT tr.TableRowID, m.MatterID, cptoken.DetailValue 
	FROM TableRows tr WITH (NOLOCK)
	INNER JOIN TableDetailValues stat WITH (NOLOCK) ON tr.TableRowID=stat.TableRowID AND stat.DetailFieldID=175469
	INNER JOIN TableDetailValues ptrid WITH (NOLOCK) ON tr.TableRowID=ptrid.TableRowID AND ptrid.DetailFieldID=175471
	INNER JOIN TableDetailValues cptoken WITH (NOLOCK) ON tr.TableRowID=cptoken.TableRowID AND cptoken.DetailFieldID=175466
	INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.ToMatterID=tr.MatterID AND ltr.FromLeadTypeID=1492
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID=ltr.FromMatterID
	INNER JOIN Lead l WITH (NOLOCK) ON m.LeadID=l.LeadID
	INNER JOIN Customers c WITH (NOLOCK) ON l.CustomerID=c.CustomerID AND c.Test=0
	WHERE tr.DetailFieldID=175472
	AND stat.ValueInt=72149 -- status=pending
	AND NOT EXISTS (  -- a row with the same parent table row id & a paid or failed status
	SELECT * FROM TableRows tr2 WITH (NOLOCK) 
	INNER JOIN TableDetailValues stat2 WITH (NOLOCK) ON tr2.TableRowID=stat2.TableRowID AND stat.DetailFieldID=stat2.DetailFieldID
	INNER JOIN TableDetailValues ptrid2 WITH (NOLOCK) ON tr2.TableRowID=ptrid2.TableRowID AND ptrid.DetailFieldID=ptrid2.DetailFieldID
	WHERE tr2.DetailFieldID=tr.DetailFieldID AND tr2.MatterID=tr.MatterID
	AND ptrid2.ValueInt=ptrid.ValueInt
	AND stat2.ValueInt IN (72150,72151) 
	)
		
	RETURN

END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C384_GetUnpaidCPRows] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C384_GetUnpaidCPRows] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C384_GetUnpaidCPRows] TO [sp_executeall]
GO
