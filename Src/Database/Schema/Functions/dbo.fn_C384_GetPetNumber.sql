SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-01-26
-- Description:	Gets the number of this pet in a multipet PH/PH account
--              by finding all live policies for this PH Account
--				NOTE. Logic in this function should match 
-- Mods
--  2105-10-12 DCM Changed to live policies for this customer
-- =============================================
CREATE FUNCTION [dbo].[fn_C384_GetPetNumber]
(
	@MatterID INT
)
RETURNS INT	
AS
BEGIN
	
	DECLARE @CaseID INT,
			@CustomerID INT,
			@LeadID INT,
			@PetNumber INT = 1,
			@PetNumberOverride INT = 0
			
	SELECT @CaseID=CaseID, @LeadID=LeadID, @CustomerID=CustomerID
	FROM Matter WITH (NOLOCK) WHERE MatterID=@MatterID

	SELECT @PetNumberOverride=dbo.fnGetDvAsInt (175461,@CaseID)
	
	IF @PetNumberOverride > 0
	BEGIN
	
		SELECT @PetNumber=@PetNumberOverride
		
	END
	ELSE
	BEGIN

		SELECT @PetNumber=PetNumber 
		FROM dbo.fn_C00_1272_Policy_GetPetList(@CustomerID)
		WHERE LeadID=@LeadID
		
	END	

	RETURN @PetNumber

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C384_GetPetNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C384_GetPetNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C384_GetPetNumber] TO [sp_executeall]
GO
