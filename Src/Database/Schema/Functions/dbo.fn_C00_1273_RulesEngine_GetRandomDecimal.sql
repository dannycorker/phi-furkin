SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-05-17
-- Description:	Return a random 4-digit number between 0 and 1
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_RulesEngine_GetRandomDecimal]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS DECIMAL(5,4)	
AS
BEGIN
 
	DECLARE @ReturnValue DECIMAL(5,4)
	
	/*
	Have to use this method, because RAND() is blocked from user-defined functions
	https://blog.sqlauthority.com/2012/11/20/sql-server-using-rand-in-user-defined-functions-udf/
	*/
	SELECT @ReturnValue = r.RandomNumber
	FROM v_RulesEngine_Rand r 

	RETURN @ReturnValue
 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_GetRandomDecimal] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_RulesEngine_GetRandomDecimal] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_GetRandomDecimal] TO [sp_executeall]
GO
