SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-02-19
-- Description:	Returns the State code by Country from PostCode
-- 2020-02-23 CPS for JIRA AAG-186 | Don't try and calculate from a numeric postcode if you don't have one
-- 2020-07-06 GPR modified to include United States State logic
-- 2020-12-01 GPR updated select to use @Postcode in place of @PostcodeInt for PPET-764
-- =============================================
CREATE FUNCTION [dbo].[fn_RulesEngine_GetState]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS VARCHAR(6)	
AS
BEGIN

	DECLARE @Postcode VARCHAR(20),
			@ClientID INT,
			@CountryID INT,
			@State VARCHAR(6),
			@PostcodeInt INT,
			@StateID INT,
			@Postcode1 VARCHAR(1),
			@Postcode3 VARCHAR(3),
			@Postcode6 VARCHAR(6)


	/*Get ClientID and CountryID*/
	--SELECT @ClientID = ClientID FROM Customers WITH (NOLOCK) WHERE CustomerID = @CustomerID
	SELECT @ClientID = [dbo].[fnGetPrimaryClientID]() /*GPR 2020-05-27*/
	SELECT @CountryID = CountryID FROM Clients WITH (NOLOCK) WHERE ClientID = @ClientID

	SELECT @Postcode=AnyValue2 FROM @Overrides WHERE AnyID = 2 AND AnyValue1 = 'PostCode'

	/*
		AUSTRALIA - Work out State from PostCode
		CPS 2020-02-23 - don't hit this section if the postcode isn't an integer
	*/
	IF @CountryID = 14 /*Australia*/ AND dbo.fnIsInt(@Postcode) = 1 -- 2020-02-23 CPS for JIRA AAG-186 | Don't hit this section if we have a narrative Postcode
	BEGIN

		SELECT @PostcodeInt = CONVERT(int, @Postcode)

		/*
			Australian States / Territory		
			NSW - New South Wales
			ACT - Australian Capital Territory
			VIC - Victoria
			QLD - Queensland
			SA - South Australia
			WA - Western Australia
			TAS - Tasmania
			NT - Northern Territory
		*/

		SELECT @State = CASE
							WHEN @PostcodeInt BETWEEN 200 AND 299 THEN 'ACT'
							WHEN @PostcodeInt BETWEEN 800 AND 999 THEN 'NT'
							WHEN @PostcodeInt BETWEEN 1000 AND 2599 THEN 'NSW'
							WHEN @PostcodeInt BETWEEN 2600 AND 2618 THEN 'ACT'
							WHEN @PostcodeInt BETWEEN 2619 AND 2899 THEN 'NSW'
							WHEN @PostcodeInt BETWEEN 2900 AND 2920 THEN 'ACT'
							WHEN @PostcodeInt BETWEEN 2921 AND 2999 THEN 'NSW'
							WHEN @PostcodeInt BETWEEN 3000 AND 3999 THEN 'VIC'
							WHEN @PostcodeInt BETWEEN 4000 AND 4999 THEN 'QLD'
							WHEN @PostcodeInt BETWEEN 5000 AND 5999 THEN 'SA'
							WHEN @PostcodeInt BETWEEN 6000 AND 6999 THEN 'WA'
							WHEN @PostcodeInt BETWEEN 7000 AND 7999 THEN 'TAS'
							WHEN @PostcodeInt BETWEEN 8000 AND 8999 THEN 'VIC'
							WHEN @PostcodeInt BETWEEN 9000 AND 9999 THEN 'QLD'
							ELSE NULL
						END
	END

	/*GPR 2020-12-01 updated to use @Postcode in place of @PostcodeInt for PPET-764*/
	IF @CountryID = 233 /*United States*/ --AND dbo.fnIsInt(@Postcode) = 1
	BEGIN
		
		SELECT TOP(1) @StateID = sbz.StateID
		FROM StateByZip sbz WITH (NOLOCK)
		WHERE sbz.Zip = @Postcode
		ORDER BY sbz.StateID ASC /*GPR 2020-09-07 for SDPRU-30 Return NYMAIC over NY if in both*/ /*GPR 2020-09-16 for SDPRU-35 return NY over NYMAIC*/

		SELECT @State = USA.StateCode
		FROM UnitedStates USA WITH (NOLOCK)
		WHERE StateID = @StateID

	END

	IF @CountryID = 39
	BEGIN
		
		/*2021-01-22 ALM FURKIN-24*/
		/*2021-01-22 ALM/GPR FURKIN-24*/
		/*IF Left 6 match use the selected value*/
		/*ELSEIF Left 3 match use the selected value*/
		/*ELSEIF Left 1 match use the selected value*/
		/*ELSE no match*/

		SELECT @Postcode1 = LEFT(@Postcode, 1)
		SELECT @Postcode3 = LEFT(@Postcode, 3)
		SELECT @Postcode6 = LEFT(@Postcode, 6)

		IF @StateID IS NULL AND (SELECT COUNT(pbp.StateID)
			FROM ProvinceByPostalCode pbp WITH (NOLOCK)
			WHERE pbp.PostalCode = @Postcode6) > 0
		BEGIN
			SELECT TOP(1) @StateID = pbp.StateID
			FROM ProvinceByPostalCode pbp WITH (NOLOCK)
			WHERE pbp.PostalCode = @Postcode6
			ORDER BY pbp.StateID ASC 
		END

		IF @StateID IS NULL AND (SELECT COUNT(pbp.StateID)
			FROM ProvinceByPostalCode pbp WITH (NOLOCK)
			WHERE pbp.PostalCode = @Postcode3) > 0
		BEGIN
			SELECT TOP(1) @StateID = pbp.StateID
			FROM ProvinceByPostalCode pbp WITH (NOLOCK)
			WHERE pbp.PostalCode = @Postcode3
			ORDER BY pbp.StateID ASC
		END

		IF @StateID IS NULL AND (SELECT COUNT(pbp.StateID)
			FROM ProvinceByPostalCode pbp WITH (NOLOCK)
			WHERE pbp.PostalCode = @Postcode1) > 0
		BEGIN
			SELECT TOP(1) @StateID = pbp.StateID
			FROM ProvinceByPostalCode pbp WITH (NOLOCK)
			WHERE pbp.PostalCode = @Postcode1
			ORDER BY pbp.StateID ASC
		END

		/*GPR 2021-01-26*/
		SELECT @State = Canada.StateCode
		FROM UnitedStates Canada WITH (NOLOCK)
		WHERE Canada.StateID = @StateID
		AND Canada.CountryID = 39
		
	END

	RETURN ISNULL(@State,'CW') /*GPR 2020-07-06 Default to CountryWide*/
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_GetState] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_RulesEngine_GetState] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_GetState] TO [sp_executeall]
GO
