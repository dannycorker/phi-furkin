SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-07-21
-- Description:	Merges in shared lead types
-- =============================================
CREATE FUNCTION [dbo].[fnDetailFieldPagesShared] 
(	
	@ClientID INT
)
RETURNS TABLE 
AS
RETURN 
(	
	SELECT DetailFieldPageID
		  ,CASE WHEN s.LeadTypeShareID IS NULL THEN dfp.ClientID ELSE s.ClientID END AS ClientID
		  ,LeadOrMatter
		  ,	CASE 
				WHEN s.LeadTypeShareID IS NULL THEN dfp.LeadTypeID 
				ELSE 
					CASE dfp.LeadOrMatter
						WHEN 10 THEN 0
						WHEN 12 THEN 1
						WHEN 13 THEN 2
						WHEN 14 THEN 3
						WHEN 15 THEN 4
						ELSE s.SharedTo 
					END
			END AS LeadTypeID
		  ,PageName
		  ,PageCaption
		  ,PageOrder
		  ,Enabled
		  ,ResourceList
		  ,SourceID
		  ,WhoCreated
		  ,WhenCreated
		  ,WhoModified
		  ,WhenModified
		  ,CASE WHEN s.LeadTypeShareID IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS IsShared
	FROM dbo.DetailFieldPages dfp WITH (NOLOCK)
	LEFT JOIN dbo.LeadTypeShare s WITH (NOLOCK) ON dfp.LeadTypeID = s.SharedFrom AND s.ClientID = @ClientID
)


GO
GRANT VIEW DEFINITION ON  [dbo].[fnDetailFieldPagesShared] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnDetailFieldPagesShared] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnDetailFieldPagesShared] TO [sp_executeall]
GO
