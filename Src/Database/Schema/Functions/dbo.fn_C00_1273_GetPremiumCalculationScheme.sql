SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-10-08
-- Description:	Returns the correct scheme for a policy calculation
-- Modified:	2014-10-17	SB	Pass premium date through to fn_C235_GetPremiumCalculationPolicyHistoryRow to fix bug
-- JEL 2018-11-29 Added the ability to return a value if no historical policy is in force as we don;t have a historical policy row for the renewal until we renew the policy
-- =============================================
 
 
CREATE FUNCTION [dbo].[fn_C00_1273_GetPremiumCalculationScheme]
(
	@CaseID INT,
	@PremiumDate DATE
)
RETURNS INT	
AS
BEGIN
	
	DECLARE @SchemeID INT,
			@MatterID INT
	
	DECLARE @TableRowID INT
	SELECT @TableRowID = dbo.fn_C00_1273_GetPremiumCalculationPolicyHistoryRow(@CaseID, @PremiumDate)
	
	IF @TableRowID IS NULL 
	BEGIN 
		
		SELECT @MatterID = m.MatterID FROM Matter m with (NOLOCK)
		WHERE m.CaseID = @CaseID 

		SELECT @SchemeID = dbo.fnGetSimpleDvAsInt(170034,@MatterID)

	END
	ELSE
	BEGIN


		SELECT @SchemeID = tdvScheme.ResourceListID
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvScheme WITH (NOLOCK) ON r.TableRowID = tdvScheme.TableRowID AND tdvScheme.DetailFieldID = 145665 /*Policy*/
		WHERE r.TableRowID = @TableRowID

	END


	
	RETURN @SchemeID
 
END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetPremiumCalculationScheme] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetPremiumCalculationScheme] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetPremiumCalculationScheme] TO [sp_executeall]
GO
