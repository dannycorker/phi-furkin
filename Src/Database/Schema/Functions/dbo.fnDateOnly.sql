SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim
-- Create date: 2007-08-08
-- Description:	Returns date from datetime
-- =============================================
CREATE FUNCTION [dbo].[fnDateOnly] 
(
	-- Add the parameters for the function here
	@DT datetime
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result datetime

	SET @Result = convert(datetime, convert(char(10), @DT, 101))

	-- Return the result of the function
	RETURN @Result

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnDateOnly] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnDateOnly] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnDateOnly] TO [sp_executeall]
GO
