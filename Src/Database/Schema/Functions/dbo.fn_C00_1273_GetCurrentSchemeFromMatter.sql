SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-06-05
-- Description:	Returns the current scheme for the product associated with a MatterID
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetCurrentSchemeFromMatter]
(
	 @PolicyOrClaimMatterID INT
)
RETURNS INT	
AS
BEGIN

	DECLARE  @PolicyMatterID	INT
			,@PolicyLeadID		INT
			,@SchemeMatterID	INT
 
	IF 1490 =	(
				SELECT TOP 1 l.LeadTypeID
				FROM Lead l WITH ( NOLOCK ) 
				INNER JOIN Matter m WITH (NOLOCK) on m.LeadID = l.LeadID 
				WHERE m.MatterID = @PolicyOrClaimMatterID
				)
	BEGIN
		SELECT @PolicyMatterID = dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(@PolicyOrClaimMatterID)
	END
	ELSE
	BEGIN
		SELECT @PolicyMatterID = @PolicyOrClaimMatterID
	END
	
	SELECT @PolicyLeadID = m.LeadID
	FROM Matter m WITH ( NOLOCK ) 
	WHERE m.MatterID = @PolicyMatterID
	
	SELECT @SchemeMatterID = dbo.fn_C00_1273_GetCurrentSchemeWithSpecies	
								(  
								 dbo.fnGetSimpleDvAsInt(170034,@PolicyMatterID) /*Current Policy*/
								,dbo.fn_GetDate_Local()
								,dbo.fnGetSimpleDvAsInt	(144269 /*Pet Type*/
														,dbo.fnGetSimpleDvAsInt(144272,@PolicyLeadID) /*Pet Type*/
														)
								)

	RETURN @SchemeMatterID
	 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetCurrentSchemeFromMatter] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetCurrentSchemeFromMatter] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetCurrentSchemeFromMatter] TO [sp_executeall]
GO
