SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 2016-06-21
-- Description:	Gets the first payment date from the preferred payment date and the starting date
--				Counts backwards by single days if the date is invalid eg., 31-02-2016 would be changed to 29-02-2016 since 2016 is a leap year
-- =============================================
CREATE FUNCTION [dbo].[fnGetFirstPaymentDate] 
(
	@StartingDate DATETIME,
	@PreferredPaymentDay TINYINT
)
RETURNS DATETIME
AS
BEGIN

	DECLARE @StartDate DATETIME    
	DECLARE @ValidDate BIT
	
    SELECT @ValidDate = ISDATE(CAST(DATEPART(yyyy,@StartingDate) AS VARCHAR) + '-' + CAST(DATEPART(mm, @StartingDate) AS VARCHAR) + '-' + CAST(@PreferredPaymentDay AS VARCHAR)) 
	IF @ValidDate=1 
	BEGIN
		SELECT @StartDate = CAST(CAST(DATEPART(yyyy,@StartingDate) AS VARCHAR) + '-' + CAST(DATEPART(mm, @StartingDate) AS VARCHAR) + '-' + CAST(@PreferredPaymentDay AS VARCHAR) AS DATETIME)
	END    
	ELSE
	BEGIN
		-- 1st attempt 1 day previous
		SELECT @ValidDate = ISDATE(CAST(DATEPART(yyyy,@StartingDate) AS VARCHAR) + '-' + CAST(DATEPART(mm, @StartingDate) AS VARCHAR) + '-' + CAST((@PreferredPaymentDay -1) AS VARCHAR)) 
		IF @ValidDate=1
		BEGIN
			SELECT @StartDate = CAST(CAST(DATEPART(yyyy,@StartingDate) AS VARCHAR) + '-' + CAST(DATEPART(mm, @StartingDate) AS VARCHAR) + '-' + CAST((@PreferredPaymentDay -1) AS VARCHAR) AS DATETIME)
		END    
		ELSE
		BEGIN	
			-- 2nd attempt 2 days previous
			SELECT @ValidDate = ISDATE(CAST(DATEPART(yyyy,@StartingDate) AS VARCHAR) + '-' + CAST(DATEPART(mm, @StartingDate) AS VARCHAR) + '-' + CAST((@PreferredPaymentDay -2) AS VARCHAR)) 
			IF @ValidDate=1
			BEGIN
				SELECT @StartDate = CAST(CAST(DATEPART(yyyy,@StartingDate) AS VARCHAR) + '-' + CAST(DATEPART(mm, @StartingDate) AS VARCHAR) + '-' + CAST((@PreferredPaymentDay -2) AS VARCHAR) AS DATETIME)
			END    	
			ELSE
			BEGIN
				-- 3rd attempt 3 days previous
				SELECT @ValidDate = ISDATE(CAST(DATEPART(yyyy,@StartingDate) AS VARCHAR) + '-' + CAST(DATEPART(mm, @StartingDate) AS VARCHAR) + '-' + CAST((@PreferredPaymentDay -3) AS VARCHAR)) 
				IF @ValidDate=1
				BEGIN
					SELECT @StartDate = CAST(CAST(DATEPART(yyyy,@StartingDate) AS VARCHAR) + '-' + CAST(DATEPART(mm, @StartingDate) AS VARCHAR) + '-' + CAST((@PreferredPaymentDay -3) AS VARCHAR) AS DATETIME)
				END  
			END
		END  	
	END
	
	RETURN @StartDate

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetFirstPaymentDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetFirstPaymentDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetFirstPaymentDate] TO [sp_executeall]
GO
