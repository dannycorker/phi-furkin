SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-12-03
-- Description:	DateDiff equivalent for InvokeFunctions
-- =============================================
CREATE FUNCTION [dbo].[fnDateDiff]
(
@DatePart VARCHAR(10),
@FromDate date,
@ToDate		Date
)
RETURNS VARCHAR(20)
AS
BEGIN

	DECLARE @ReturnValue VARCHAR(20) = ''

	/*Somebody please do this better*/
	SELECT @ReturnValue = CASE @DatePart
							WHEN 'year' THEN DATEDIFF(year,@FromDate,@ToDate)
							WHEN 'yy' THEN DATEDIFF(yy,@FromDate,@ToDate)
							WHEN 'yyyy' THEN DATEDIFF(yyyy,@FromDate,@ToDate)
							WHEN 'quarter' THEN DATEDIFF(quarter,@FromDate,@ToDate)
							WHEN 'qq' THEN DATEDIFF(qq,@FromDate,@ToDate)
							WHEN 'q' THEN DATEDIFF(q,@FromDate,@ToDate)
							WHEN 'month' THEN DATEDIFF(month,@FromDate,@ToDate)
							WHEN 'mm' THEN DATEDIFF(mm,@FromDate,@ToDate)
							WHEN 'm' THEN DATEDIFF(m,@FromDate,@ToDate)
							WHEN 'dayofyear' THEN DATEDIFF(dayofyear,@FromDate,@ToDate)
							WHEN 'dy' THEN DATEDIFF(dy,@FromDate,@ToDate)
							WHEN 'y' THEN DATEDIFF(y,@FromDate,@ToDate)
							WHEN 'day' THEN DATEDIFF(day,@FromDate,@ToDate)
							WHEN 'dd' THEN DATEDIFF(dd,@FromDate,@ToDate)
							WHEN 'd' THEN DATEDIFF(d,@FromDate,@ToDate)
							WHEN 'week' THEN DATEDIFF(week,@FromDate,@ToDate)
							WHEN 'wk' THEN DATEDIFF(wk,@FromDate,@ToDate)
							WHEN 'ww' THEN DATEDIFF(ww,@FromDate,@ToDate)
							WHEN 'hour' THEN DATEDIFF(hour,@FromDate,@ToDate)
							WHEN 'hh' THEN DATEDIFF(hh,@FromDate,@ToDate)
							WHEN 'minute' THEN DATEDIFF(minute,@FromDate,@ToDate)
							WHEN 'mi' THEN DATEDIFF(mi,@FromDate,@ToDate)
							WHEN 'n' THEN DATEDIFF(n,@FromDate,@ToDate)
							WHEN 'second' THEN DATEDIFF(second,@FromDate,@ToDate)
							WHEN 'ss' THEN DATEDIFF(ss,@FromDate,@ToDate)
							WHEN 's' THEN DATEDIFF(s,@FromDate,@ToDate)
							WHEN 'millisecond' THEN DATEDIFF(millisecond,@FromDate,@ToDate)
							WHEN 'ms' THEN DATEDIFF(ms,@FromDate,@ToDate)
							WHEN 'microsecond' THEN DATEDIFF(microsecond,@FromDate,@ToDate)
							WHEN 'mcs' THEN DATEDIFF(mcs,@FromDate,@ToDate)
							WHEN 'nanosecond' THEN DATEDIFF(nanosecond,@FromDate,@ToDate)
							WHEN 'ns	' THEN DATEDIFF(ns	,@FromDate,@ToDate)
						  ELSE NULL
						  END
	
	RETURN @ReturnValue

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnDateDiff] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnDateDiff] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnDateDiff] TO [sp_executeall]
GO
