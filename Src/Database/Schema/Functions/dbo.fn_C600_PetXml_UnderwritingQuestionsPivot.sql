SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-10-17
-- Description:	Take Pet XML and break out the underwriting questions, then pivot them to one row
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_PetXml_UnderwritingQuestionsPivot]
(
	@PetXml XML
)
RETURNS 
@Table TABLE 
(
	 RescueBit			Bit
	,RescueComment		VARCHAR(2000)
	,PreExistingBit		BIT
	,PreExistingComment	VARCHAR(2000)
	,WorkingDogBit		BIT
	,WorkingDogComment	VARCHAR(2000)
	,ViciousBit			BIT
	,ViciousComment		VARCHAR(2000)
)
AS
BEGIN

	/*CPS 2017-10-17 May be better to hit that function once, then pull out one item at a time.  Two-column PIVOT is not allowed*/

	INSERT @Table ( RescueBit, RescueComment, PreExistingBit, PreExistingComment, WorkingDogBit, WorkingDogComment, ViciousBit, ViciousComment )
	SELECT	TOP 1  
			CASE WHEN fnRescue.AnswerID = 5144 THEN 1 ELSE 0 END
			,fnRescue.AnswerText
			,CASE WHEN fnPreExisting.AnswerID = 5144 THEN 1 ELSE 0 END
			,fnPreExisting.AnswerText
			,CASE WHEN fnWorking.AnswerID = 5144 THEN 1 ELSE 0 END
			,fnWorking.AnswerText
			,CASE WHEN fnVicious.AnswerID = 5144 THEN 1 ELSE 0 END
			,fnVicious.AnswerText
	FROM dbo.fn_C600_PetXml_UnderwritingQuestions(@PetXml) fnRescue
		,dbo.fn_C600_PetXml_UnderwritingQuestions(@PetXml) fnPreExisting
		,dbo.fn_C600_PetXml_UnderwritingQuestions(@PetXml) fnWorking
		,dbo.fn_C600_PetXml_UnderwritingQuestions(@PetXml) fnVicious
	WHERE fnRescue.QuestionID = 147276 /*Is your Pet a rescue animal?*/
	AND fnPreExisting.QuestionID = 147280 /*Has your pet shown any sign or symptom of any injury or illness or been unwell, either now or at any time in the past?*/
	AND fnWorking.QuestionID = 147277 /*Is your dog a working dog i.e. gun dog, farm dog, racing dog, emergency rescue dog, guard dog or used for security purposes?*/
	AND fnVicious.QuestionID = 149241 /*Has anyone complained or have you seen anybody regarding your dog's behaviour or has your dog ever damaged another person's property, shown aggression towards or injured another person?*/
	
	RETURN
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_PetXml_UnderwritingQuestionsPivot] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_PetXml_UnderwritingQuestionsPivot] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_PetXml_UnderwritingQuestionsPivot] TO [sp_executeall]
GO
