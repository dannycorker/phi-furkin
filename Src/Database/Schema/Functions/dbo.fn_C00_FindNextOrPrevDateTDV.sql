SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:  Jim Green
-- Create date: 2013-05-16
-- Description: Find the next or previous date within table rows. Created for LA2Plus payment dates originally.
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_FindNextOrPrevDateTDV]
(
 @AnyID INT,
 @DetailFieldSubTypeID INT,
 @DetailFieldID INT,
 @SearchDirection INT,  /* 1 = Next, 2 = Prev */
 @BlankNotNull BIT,   /* Option to return an empty string rather than NULL */
 @ClientID INT,
 @TableDetailFieldID INT
)
RETURNS VARCHAR(10)
AS
BEGIN
 DECLARE @NextOrPrevDate VARCHAR(10) 
 
 /* TableDetailValues for a Matter */
 IF @DetailFieldSubTypeID = 2
 BEGIN
  IF @SearchDirection = 1
  BEGIN
   /* Find next date */
   SELECT TOP 1 @NextOrPrevDate = tdv.ValueDate 
   FROM dbo.TableDetailValues tdv WITH (NOLOCK) 
   INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID and tr.DetailFieldID = @TableDetailFieldID
   WHERE tdv.ClientID = @ClientID 
   AND tdv.MatterID = @AnyID 
   AND tdv.DetailFieldID = @DetailFieldID 
   AND tdv.ValueDate >= dbo.fnDateOnly(dbo.fn_GetDate_Local()) 
   ORDER BY tdv.ValueDate ASC
  END
  ELSE
  BEGIN
   /* Find previous date */
   SELECT TOP 1 @NextOrPrevDate = tdv.ValueDate
   FROM dbo.TableDetailValues tdv WITH (NOLOCK) 
   INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID and tr.DetailFieldID = @TableDetailFieldID
   WHERE tdv.ClientID = @ClientID 
   AND tdv.MatterID = @AnyID 
   AND tdv.DetailFieldID = @DetailFieldID 
   AND tdv.ValueDate <= dbo.fnDateOnly(dbo.fn_GetDate_Local()) 
   ORDER BY tdv.ValueDate DESC
  END
 END
 
 /* Option to return an empty string rather than NULL */
 IF @BlankNotNull = 1 AND @NextOrPrevDate IS NULL
 BEGIN
  SET @NextOrPrevDate = ''
 END
 
 RETURN @NextOrPrevDate

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_FindNextOrPrevDateTDV] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_FindNextOrPrevDateTDV] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_FindNextOrPrevDateTDV] TO [sp_executeall]
GO
