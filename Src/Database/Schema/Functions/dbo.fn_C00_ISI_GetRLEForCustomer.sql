SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-08-20
-- Description:	Get RLE for children for the irish
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ISI_GetRLEForCustomer]
(	
	@CustomerID INT,
	@DetailFieldIDOFRle INT
)
RETURNS 
@RLE TABLE 
(
	[Child] VARCHAR(2000),
	[Year1] NUMERIC(18,2),
	[Year2] NUMERIC(18,2),
	[Year3] NUMERIC(18,2),
	[Year4] NUMERIC(18,2),
	[Year5] NUMERIC(18,2),
	[Year6] NUMERIC(18,2)
)
AS
BEGIN

	DECLARE @Percentage NUMERIC(18,2)

	SELECT @Percentage = dbo.fnGetSimpleDVAsMoney(@DetailFieldIDOFRle,@CustomerID)/100

	INSERT INTO @RLE (Child, Year1, Year2, Year3, Year4, Year5, Year6)
	VALUES ('Child 1',dbo.fnGetSimpleDvAsMoney(296216,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296794,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296818,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296842,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296866,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296890,@CustomerID)*@Percentage),
			('Child 2', dbo.fnGetSimpleDvAsMoney(296217,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296795,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296819,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296843,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296867,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296891,@CustomerID)*@Percentage),
			('Child 3', dbo.fnGetSimpleDvAsMoney(296218,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296796,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296820,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296844,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296868,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296892,@CustomerID)*@Percentage),
			('Child 4', dbo.fnGetSimpleDvAsMoney(296219,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296797,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296821,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296845,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296869,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296893,@CustomerID)*@Percentage),
			('Child 5', dbo.fnGetSimpleDvAsMoney(296268,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296798,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296822,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296846,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296870,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296894,@CustomerID)*@Percentage),
			('Child 6', dbo.fnGetSimpleDvAsMoney(296269,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296799,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296823,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296847,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296871,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296895,@CustomerID)*@Percentage),
			('Child 7', dbo.fnGetSimpleDvAsMoney(296270,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296800,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296824,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296848,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296872,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296896,@CustomerID)*@Percentage),
			('Child 8', dbo.fnGetSimpleDvAsMoney(296271,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296801,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296825,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296849,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296873,@CustomerID)*@Percentage,dbo.fnGetSimpleDvAsMoney(296897,@CustomerID)*@Percentage)
	
	RETURN 
END




















GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetRLEForCustomer] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_ISI_GetRLEForCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetRLEForCustomer] TO [sp_executeall]
GO
