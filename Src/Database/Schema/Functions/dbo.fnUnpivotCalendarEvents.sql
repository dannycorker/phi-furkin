SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack
-- Create date: 2012-04-04
-- Description:	Unpivots Calendar Events
-- =============================================
CREATE FUNCTION [dbo].[fnUnpivotCalendarEvents] 
(	
	@SelectTable tvpCalendarEvent READONLY
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT TableRowID, SourceID, Operation, ColumnName DetailFieldID, RealValue DetailValue
	FROM
	(
		SELECT
			[EventID] TableRowID,
			SourceID,
			Operation,
			COALESCE(Name,'') [155873],
			COALESCE(Description,'') [155874],
			COALESCE(FromTime,NULL) [152889],
			COALESCE(ToTime,NULL) [152890],
			COALESCE(StatusID,'0') [152891],
			COALESCE(ReqEmpID,'0') [152892], 
			COALESCE(ReqDate,NULL) [152893],
			COALESCE(StatusEmpID,'0') [152894],
			COALESCE(StatusDate,NULL) [152895],
			COALESCE(WPEventID,'0') [152896],
			COALESCE(PaidTypeID,'0') [152897],
			COALESCE(SickTypeID,'0') [152898],
			COALESCE(CostCentreID,'0') [152899],
			COALESCE(Location,'') [152900],
			COALESCE(CostTypeID,'0') [152901],
			COALESCE(CostRate,'0') [152902],
			COALESCE(CostUnit,'0') [152903],
			COALESCE(CostAmount,'0') [152904],
			COALESCE(AssetID,'0') [152905],
			COALESCE(TypeID,'0') [152888], 
			COALESCE(AllDay,'0') [153049], 
			COALESCE(StatusDesc,'') [153247], 
			COALESCE(DurationUnit,'0') [153481], 
			COALESCE(Duration,'0') [154233],
			COALESCE(SubClientID,'0') [156059]
		from	@SelectTable
	) pvt
	UNPIVOT
	(
		RealValue FOR ColumnName IN
		(
			[156059],[152889],[152890],[152891],[152892],[152893],[152894],[152895],[152896],[152897],[152898],[152899],[152900],[152901],[152902],[152903],[152904],[152905],[152888],[153049],[153247],[153481],[154233],[155873],[155874]
		)
	) as unpvt
)








GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotCalendarEvents] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnUnpivotCalendarEvents] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotCalendarEvents] TO [sp_executeall]
GO
