SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-05-14
-- Description:	Get Data for ISI
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ISI_SetCostsBreakdown]
(	
	@CustomerID INT
)
RETURNS TABLE 
AS
RETURN 
(
	WITH DATA AS (

	SELECT 'Food' AS [Type], CONVERT (NUMERIC(18,2), CASE dbo.fnGetSimpleDv (296230,@CustomerID) 
	WHEN 5 THEN '250.98'
	WHEN 6 THEN '250.98'
	WHEN 1 THEN '222.51'
	WHEN 2 THEN '222.51'
	WHEN 3 THEN '283.21'
	WHEN 4 THEN '283.21'
	WHEN 7 THEN '369.37'
	WHEN 8 THEN '369.37'
	END
	+
	dbo.fngetsimpledvasmoney (296253,@CustomerID)*135.44+
	dbo.fngetsimpledvasmoney (296254,@CustomerID)*105.11+
	dbo.fngetsimpledvasmoney (296255,@CustomerID)*162.43+
	dbo.fngetsimpledvasmoney (296256,@CustomerID)*215.88) AS [Amount]
	
	
	UNION ALL

	SELECT 'Clothing',CONVERT (NUMERIC(18,2), CASE dbo.fnGetSimpleDv (296230,@CustomerID)
	WHEN 5 THEN '35.34'
	WHEN 6 THEN '35.34'
	WHEN 1 THEN '35.33'
	WHEN 2 THEN '35.33'
	WHEN 3 THEN '66.73'
	WHEN 4 THEN '66.73'
	WHEN 7 THEN '67.00'
	WHEN 8 THEN '67.00'
	END

	+
	dbo.fngetsimpledvasmoney (296253,@CustomerID)*71.99+
	dbo.fngetsimpledvasmoney (296254,@CustomerID)*21.86+
	dbo.fngetsimpledvasmoney (296255,@CustomerID)*29.90+
	dbo.fngetsimpledvasmoney (296256,@CustomerID)*54.44)

	UNION ALL

	SELECT 'Personal Care',CONVERT (NUMERIC(18,2), CASE dbo.fnGetSimpleDv (296230,@CustomerID)
	WHEN 5 THEN '33.06'
	WHEN 6 THEN '33.06'
	WHEN 1 THEN '31.40'
	WHEN 2 THEN '31.40'
	WHEN 3 THEN '64.90'
	WHEN 4 THEN '64.90'
	WHEN 7 THEN '73.70'
	WHEN 8 THEN '73.70'
	END

	+
	dbo.fngetsimpledvasmoney (296253,@CustomerID)*46.51+
	dbo.fngetsimpledvasmoney (296254,@CustomerID)*6.09+
	dbo.fngetsimpledvasmoney (296255,@CustomerID)*11.74+
	dbo.fngetsimpledvasmoney (296256,@CustomerID)*36.43)

	UNION ALL

	SELECT 'Health',CONVERT (NUMERIC(18,2), CASE dbo.fnGetSimpleDv (296230,@CustomerID)
	WHEN 5 THEN '31.34'
	WHEN 6 THEN '31.34'
	WHEN 1 THEN '29.20'
	WHEN 2 THEN '29.20'
	WHEN 3 THEN '45.17'
	WHEN 4 THEN '45.17'
	WHEN 7 THEN '49.39'
	WHEN 8 THEN '49.39'
	END

	+
	dbo.fngetsimpledvasmoney (296253,@CustomerID)*35.98+
	dbo.fngetsimpledvasmoney (296254,@CustomerID)*18.85+
	dbo.fngetsimpledvasmoney (296255,@CustomerID)*18.60+
	dbo.fngetsimpledvasmoney (296256,@CustomerID)*24.50)

	UNION ALL

	SELECT 'Household Goods',CONVERT (NUMERIC(18,2), CASE dbo.fnGetSimpleDv (296230,@CustomerID)
	WHEN 5 THEN '26.38'
	WHEN 6 THEN '26.38'
	WHEN 1 THEN '62.37'
	WHEN 2 THEN '62.37'
	WHEN 3 THEN '67.63'
	WHEN 4 THEN '67.63'
	WHEN 7 THEN '30.71'
	WHEN 8 THEN '30.71'
	END

	+
	dbo.fngetsimpledvasmoney (296253,@CustomerID)*45.58+
	dbo.fngetsimpledvasmoney (296254,@CustomerID)*11.78+
	dbo.fngetsimpledvasmoney (296255,@CustomerID)*13.17+
	dbo.fngetsimpledvasmoney (296256,@CustomerID)*16.04)

	UNION ALL

	SELECT 'Household Services',CONVERT (NUMERIC(18,2), CASE dbo.fnGetSimpleDv (296230,@CustomerID)
	WHEN 5 THEN '32.54'
	WHEN 6 THEN '32.54'
	WHEN 1 THEN '32.54'
	WHEN 2 THEN '32.54'
	WHEN 3 THEN '40.87'
	WHEN 4 THEN '40.87'
	WHEN 7 THEN '40.87'
	WHEN 8 THEN '40.87'
	END)

	UNION ALL

	SELECT 'Communications',CONVERT (NUMERIC(18,2), CASE dbo.fnGetSimpleDv (296230,@CustomerID)
	WHEN 5 THEN '41.21'
	WHEN 6 THEN '41.21'
	WHEN 1 THEN '41.07'
	WHEN 2 THEN '41.07'
	WHEN 3 THEN '61.56'
	WHEN 4 THEN '61.56'
	WHEN 7 THEN '61.70'
	WHEN 8 THEN '61.70'
	END

	+

	dbo.fngetsimpledvasmoney (296256,@CustomerID)*20.48)

	UNION ALL

	SELECT 'Social Inclusion & Participation',CONVERT (NUMERIC(18,2), CASE dbo.fnGetSimpleDv (296230,@CustomerID)
	WHEN 5 THEN '126.10'
	WHEN 6 THEN '126.10'
	WHEN 1 THEN '99.31'
	WHEN 2 THEN '99.31'
	WHEN 3 THEN '158.02'
	WHEN 4 THEN '158.02'
	WHEN 7 THEN '232.47'
	WHEN 8 THEN '232.47'
	END

	+
	dbo.fngetsimpledvasmoney (296253,@CustomerID)*7.72+
	dbo.fngetsimpledvasmoney (296254,@CustomerID)*10.04+
	dbo.fngetsimpledvasmoney (296255,@CustomerID)*49.58+
	dbo.fngetsimpledvasmoney (296256,@CustomerID)*93.33)

	UNION ALL

	SELECT 'Education',CONVERT (NUMERIC(18,2), CASE dbo.fnGetSimpleDv (296230,@CustomerID)
	WHEN 5 THEN '23.72'
	WHEN 6 THEN '23.72'
	WHEN 1 THEN '10.34'
	WHEN 2 THEN '10.34'
	WHEN 3 THEN '10.34'
	WHEN 4 THEN '10.34'
	WHEN 7 THEN '38.13'
	WHEN 8 THEN '38.13'
	END

	+

	dbo.fngetsimpledvasmoney (296255,@CustomerID)*29.16+
	dbo.fngetsimpledvasmoney (296256,@CustomerID)*66.27)

	UNION ALL

	SELECT 'Travel (Public/Transport)',CONVERT (NUMERIC(18,2), CASE dbo.fnGetSimpleDv (296230,@CustomerID)
	WHEN 5 THEN '149.70'
	WHEN 6 THEN '237.79'
	WHEN 2 THEN '250.04'
	WHEN 4 THEN '250.50'
	WHEN 7 THEN '287.05'
	WHEN 8 THEN '238.26'
	WHEN 1 THEN '125.00' + dbo.fngetsimpledvasmoney (296254,@CustomerID)*12.25+dbo.fngetsimpledvasmoney (296255,@CustomerID)*12.25+dbo.fngetsimpledvasmoney (296256,@CustomerID)*12.25
	WHEN 3 THEN '250.00'+ dbo.fngetsimpledvasmoney (296254,@CustomerID)*12.25+dbo.fngetsimpledvasmoney (296255,@CustomerID)*12.25+dbo.fngetsimpledvasmoney (296256,@CustomerID)*12.25
	END)
	  
	UNION ALL
	
	SELECT 'Household Electricity',CONVERT (NUMERIC(18,2), CASE dbo.fnGetSimpleDv (296230,@CustomerID)
	WHEN 5 THEN '60.37'
	WHEN 6 THEN '60.37'
	WHEN 1 THEN '86.08'
	WHEN 2 THEN '86.08'
	WHEN 3 THEN '112.39'
	WHEN 4 THEN '112.39'
	WHEN 7 THEN '73.30'
	WHEN 8 THEN '73.30'
	END

	+
	dbo.fngetsimpledvasmoney (296253,@CustomerID)*6.30)

	UNION ALL

	SELECT 'Home Heating',CONVERT (NUMERIC(18,2), CASE dbo.fnGetSimpleDv (296230,@CustomerID)
	WHEN 5 THEN '70.80'
	WHEN 6 THEN '70.80'
	WHEN 1 THEN '91.45'
	WHEN 2 THEN '91.45'
	WHEN 3 THEN '114.46'
	WHEN 4 THEN '114.46'
	WHEN 7 THEN '106.65'
	WHEN 8 THEN '106.65'
	END)

	UNION ALL

	SELECT 'Personal Costs',CONVERT (NUMERIC(18,2), CASE dbo.fnGetSimpleDv (296230,@CustomerID)
	WHEN 5 THEN '0.97'
	WHEN 6 THEN '0.97'
	WHEN 1 THEN '0.95'
	WHEN 2 THEN '0.95'
	WHEN 3 THEN '1.91'
	WHEN 4 THEN '1.91'
	WHEN 7 THEN '1.93'
	WHEN 8 THEN '1.93'
	END

	+
	dbo.fngetsimpledvasmoney (296253,@CustomerID)*1.05+
	dbo.fngetsimpledvasmoney (296254,@CustomerID)*0.85+
	dbo.fngetsimpledvasmoney (296255,@CustomerID)*0.85+
	dbo.fngetsimpledvasmoney (296256,@CustomerID)*0.85)

	UNION ALL

	SELECT 'Home Insurance',CONVERT (NUMERIC(18,2), CASE dbo.fnGetSimpleDv (296230,@CustomerID)
	WHEN 5 THEN '12.25'
	WHEN 6 THEN '12.25'
	WHEN 1 THEN '17.59'
	WHEN 2 THEN '17.59'
	WHEN 3 THEN '17.59'
	WHEN 4 THEN '17.59'
	WHEN 7 THEN '12.25'
	WHEN 8 THEN '12.25'
	END)

	UNION ALL

	SELECT 'Car Insurance',CONVERT (NUMERIC(18,2), CASE dbo.fnGetSimpleDv (296230,@CustomerID)

	WHEN 6 THEN '24.25'
	WHEN 2 THEN '26.40'
	WHEN 4 THEN '46.70'
	WHEN 8 THEN '25.82'
	END)

	UNION ALL

	SELECT 'Savings & Contingencies',CONVERT (NUMERIC(18,2), CASE dbo.fnGetSimpleDv (296230,@CustomerID)
	WHEN 5 THEN '43.38'
	WHEN 6 THEN '43.38'
	WHEN 1 THEN '59.57'
	WHEN 2 THEN '59.57'
	WHEN 3 THEN '78.85'
	WHEN 4 THEN '78.85'
	WHEN 7 THEN '65.07'
	WHEN 8 THEN '65.07'
	END

	+

	dbo.fngetsimpledvasmoney (296253,@CustomerID)*21.69+
	dbo.fngetsimpledvasmoney (296254,@CustomerID)*21.69+
	dbo.fngetsimpledvasmoney (296255,@CustomerID)*21.69+
	dbo.fngetsimpledvasmoney (296256,@CustomerID)*21.69)

	)

	SELECT t.Type, t.Amount
	FROM DATA t

	UNION ALL

	SELECT 'Total' AS [Type], SUM (T.Amount) AS [Amount]
	FROM DATA T
	
	UNION ALL
	
	SELECT 'Additional Child Allowance - 3 or more children', 
	CASE 
	WHEN dbo.fngetsimpledvasint(296258,@CustomerID) >= 3
	THEN 10.81 + ((dbo.fngetsimpledvasint(296258,@CustomerID) - 3)*52.72)
	ELSE 0
	END
	
	UNION ALL

	SELECT '- Less Child Benefit', (dbo.fngetsimpledvasmoney (296253,@CustomerID)*135.00+
	dbo.fngetsimpledvasmoney (296254,@CustomerID)*135.00+
	dbo.fngetsimpledvasmoney (296255,@CustomerID)*135.00+
	dbo.fngetsimpledvasmoney (296256,@CustomerID)*135.00)*-1

	UNION ALL

	SELECT 'Total Monthly Set Costs' AS [Type], (SUM (T.Amount)+	CASE 
	WHEN dbo.fngetsimpledvasint(296258,@CustomerID) >= 3
	THEN 10.81 + ((dbo.fngetsimpledvasint(296258,@CustomerID) - 3)*52.72)
	ELSE 0
	END)-(dbo.fngetsimpledvasmoney (296253,@CustomerID)*135.00+
	dbo.fngetsimpledvasmoney (296254,@CustomerID)*135.00+
	dbo.fngetsimpledvasmoney (296255,@CustomerID)*135.00+
	dbo.fngetsimpledvasmoney (296256,@CustomerID)*135.00)
	
	FROM DATA T
)
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_SetCostsBreakdown] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_ISI_SetCostsBreakdown] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_SetCostsBreakdown] TO [sp_executeall]
GO
