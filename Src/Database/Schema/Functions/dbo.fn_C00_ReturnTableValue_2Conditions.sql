SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-08-11
-- Description: Return a value from a table, specify 2 columns with matching conditions
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ReturnTableValue_2Conditions]
(
@ObjectID						INT /*Customer,Lead,Case Or Matter*/
,@TableDetailFieldID			INT
,@ReturnColumnDetailFieldID		INT
,@ColumnADetailFieldID			INT 
,@ColumnAValue					Varchar(200)
,@DateColumnBDetailFieldID		INT 
,@ColumnBValue					VARCHAR(200) /*set to 'high' or 'low' to order for dates, if neither high or low it will match on exact value*/
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE	@ClientID		INT
			,@LeadTypeID	INT
			,@TableRowID	INT
			,@LeadOrMatter	INT
			,@ReturnValue	VARCHAR(200)
			,@LookupListID	INT
			,@DetailFieldSubtypeID INT
			
	SELECT @DetailFieldSubtypeID = df.LeadOrMatter, @ClientID = df.ClientID, @LeadTypeID = df.LeadTypeID
	FROM DetailFields df WITH ( NOLOCK ) 
	WHERE df.DetailFieldID = @TableDetailFieldID

	/*Lead ###############################################*/
	IF @DetailFieldSubtypeID = 1 
	BEGIN
		
		IF @ColumnBValue = 'High' /*Order dates DESC*/
		BEGIN
				
			SELECT TOP 1 @ReturnValue  = tdvReturnValue.DetailValue
			FROM tablerows tr WITH (NOLOCK) 
			Inner join TableDetailValues tdvColumnA WITH (NOLOCK) on tdvColumnA.TableRowID = TR.TableRowID AND tdvColumnA.DetailFieldID = @ColumnADetailFieldID
			INNER JOIN TableDetailValues tdvDateColumnB WITH (NOLOCK) on tdvDateColumnB.TableRowID = tr.TableRowID AND tdvDateColumnB.DetailFieldID = @DateColumnBDetailFieldID
			INNER JOIN TableDetailValues tdvReturnValue WITH (NOLOCK) on tdvReturnValue.TableRowID = tr.TableRowID AND tdvReturnValue.DetailFieldID = @ReturnColumnDetailFieldID
			WHERE tr.detailfieldID = @TableDetailFieldID
			and tr.ClientID = @ClientID 
			AND tdvColumnA.DetailValue = @ColumnAValue
			and tr.LeadID = @ObjectID 
			ORDER BY tdvDateColumnB.ValueDate DESC

		END

		IF @ColumnBValue = 'Low' /*Order dates ASC*/
		BEGIN
				
			SELECT TOP 1 @ReturnValue  = tdvReturnValue.DetailValue
			FROM tablerows tr WITH (NOLOCK) 
			Inner join TableDetailValues tdvColumnA WITH (NOLOCK) on tdvColumnA.TableRowID = TR.TableRowID AND tdvColumnA.DetailFieldID = @ColumnADetailFieldID
			INNER JOIN TableDetailValues tdvDateColumnB WITH (NOLOCK) on tdvDateColumnB.TableRowID = tr.TableRowID AND tdvDateColumnB.DetailFieldID = @DateColumnBDetailFieldID
			INNER JOIN TableDetailValues tdvReturnValue WITH (NOLOCK) on tdvReturnValue.TableRowID = tr.TableRowID AND tdvReturnValue.DetailFieldID = @ReturnColumnDetailFieldID
			WHERE tr.detailfieldID = @TableDetailFieldID
			AND tdvColumnA.DetailValue = @ColumnAValue
			and tr.LeadID = @ObjectID 
			and tr.ClientID = @ClientID 
			ORDER BY tdvDateColumnB.ValueDate ASC

		END

		IF @ColumnBValue not in ('high','low') /*IF no date, match on a string*/
		BEGIN

			SELECT TOP 1 @ReturnValue  = tdvReturnValue.DetailValue
			FROM tablerows tr WITH (NOLOCK) 
			Inner join TableDetailValues tdvColumnA WITH (NOLOCK) on tdvColumnA.TableRowID = TR.TableRowID AND tdvColumnA.DetailFieldID = @ColumnADetailFieldID
			INNER JOIN TableDetailValues tdvDateColumnB WITH (NOLOCK) on tdvDateColumnB.TableRowID = tr.TableRowID AND tdvDateColumnB.DetailFieldID = @DateColumnBDetailFieldID
			INNER JOIN TableDetailValues tdvReturnValue WITH (NOLOCK) on tdvReturnValue.TableRowID = tr.TableRowID AND tdvReturnValue.DetailFieldID = @ReturnColumnDetailFieldID
			WHERE tr.detailfieldID = @TableDetailFieldID
			AND tdvColumnA.DetailValue = @ColumnAValue
			and tr.LeadID = @ObjectID 
			and tr.ClientID = @ClientID 
			AND tdvDateColumnB.DetailValue = @ColumnBValue

		END
		
	END
	
	/*Customer##################################################*/
	IF @DetailFieldSubtypeID = 10 
	BEGIN
		
		IF @ColumnBValue = 'High' /*Order dates DESC*/
		BEGIN
				
			SELECT TOP 1 @ReturnValue  = tdvReturnValue.DetailValue
			FROM tablerows tr WITH (NOLOCK) 
			Inner join TableDetailValues tdvColumnA WITH (NOLOCK) on tdvColumnA.TableRowID = TR.TableRowID AND tdvColumnA.DetailFieldID = @ColumnADetailFieldID
			INNER JOIN TableDetailValues tdvDateColumnB WITH (NOLOCK) on tdvDateColumnB.TableRowID = tr.TableRowID AND tdvDateColumnB.DetailFieldID = @DateColumnBDetailFieldID
			INNER JOIN TableDetailValues tdvReturnValue WITH (NOLOCK) on tdvReturnValue.TableRowID = tr.TableRowID AND tdvReturnValue.DetailFieldID = @ReturnColumnDetailFieldID
			WHERE tr.detailfieldID = @TableDetailFieldID
			AND tdvColumnA.DetailValue = @ColumnAValue
			and tr.CustomerID = @ObjectID 
			and tr.ClientID = @ClientID 
			ORDER BY tdvDateColumnB.ValueDate DESC

		END

		IF @ColumnBValue = 'Low' /*Order dates ASC*/
		BEGIN
				
			SELECT TOP 1 @ReturnValue  = tdvReturnValue.DetailValue
			FROM tablerows tr WITH (NOLOCK) 
			Inner join TableDetailValues tdvColumnA WITH (NOLOCK) on tdvColumnA.TableRowID = TR.TableRowID AND tdvColumnA.DetailFieldID = @ColumnADetailFieldID
			INNER JOIN TableDetailValues tdvDateColumnB WITH (NOLOCK) on tdvDateColumnB.TableRowID = tr.TableRowID AND tdvDateColumnB.DetailFieldID = @DateColumnBDetailFieldID
			INNER JOIN TableDetailValues tdvReturnValue WITH (NOLOCK) on tdvReturnValue.TableRowID = tr.TableRowID AND tdvReturnValue.DetailFieldID = @ReturnColumnDetailFieldID
			WHERE tr.detailfieldID = @TableDetailFieldID
			AND tdvColumnA.DetailValue = @ColumnAValue
			and tr.CustomerID = @ObjectID 
			and tr.ClientID = @ClientID 
			ORDER BY tdvDateColumnB.ValueDate ASC

		END

		IF @ColumnBValue not in ('high','low') /*IF no date, match on a string*/
		BEGIN

			SELECT TOP 1 @ReturnValue  = tdvReturnValue.DetailValue
			FROM tablerows tr WITH (NOLOCK) 
			Inner join TableDetailValues tdvColumnA WITH (NOLOCK) on tdvColumnA.TableRowID = TR.TableRowID AND tdvColumnA.DetailFieldID = @ColumnADetailFieldID
			INNER JOIN TableDetailValues tdvDateColumnB WITH (NOLOCK) on tdvDateColumnB.TableRowID = tr.TableRowID AND tdvDateColumnB.DetailFieldID = @DateColumnBDetailFieldID
			INNER JOIN TableDetailValues tdvReturnValue WITH (NOLOCK) on tdvReturnValue.TableRowID = tr.TableRowID AND tdvReturnValue.DetailFieldID = @ReturnColumnDetailFieldID
			WHERE tr.detailfieldID = @TableDetailFieldID
			AND tdvColumnA.DetailValue = @ColumnAValue
			and tr.CustomerID = @ObjectID 
			and tr.ClientID = @ClientID 
			AND tdvDateColumnB.DetailValue = @ColumnBValue

		END
		
	END
	
	/*Matter####################################################*/
	IF @DetailFieldSubtypeID = 2 
	BEGIN
		
		IF @ColumnBValue = 'High' /*Order dates DESC*/
		BEGIN
				
			SELECT TOP 1 @ReturnValue  = tdvReturnValue.DetailValue
			FROM tablerows tr WITH (NOLOCK) 
			Inner join TableDetailValues tdvColumnA WITH (NOLOCK) on tdvColumnA.TableRowID = TR.TableRowID AND tdvColumnA.DetailFieldID = @ColumnADetailFieldID
			INNER JOIN TableDetailValues tdvDateColumnB WITH (NOLOCK) on tdvDateColumnB.TableRowID = tr.TableRowID AND tdvDateColumnB.DetailFieldID = @DateColumnBDetailFieldID
			INNER JOIN TableDetailValues tdvReturnValue WITH (NOLOCK) on tdvReturnValue.TableRowID = tr.TableRowID AND tdvReturnValue.DetailFieldID = @ReturnColumnDetailFieldID
			WHERE tr.detailfieldID = @TableDetailFieldID
			AND tdvColumnA.DetailValue = @ColumnAValue
			and tr.MatterID = @ObjectID 
			and tr.ClientID = @ClientID 
			ORDER BY tdvDateColumnB.ValueDate DESC

		END

		IF @ColumnBValue = 'Low' /*Order dates ASC*/
		BEGIN
				
			SELECT TOP 1 @ReturnValue  = tdvReturnValue.DetailValue
			FROM tablerows tr WITH (NOLOCK) 
			Inner join TableDetailValues tdvColumnA WITH (NOLOCK) on tdvColumnA.TableRowID = TR.TableRowID AND tdvColumnA.DetailFieldID = @ColumnADetailFieldID
			INNER JOIN TableDetailValues tdvDateColumnB WITH (NOLOCK) on tdvDateColumnB.TableRowID = tr.TableRowID AND tdvDateColumnB.DetailFieldID = @DateColumnBDetailFieldID
			INNER JOIN TableDetailValues tdvReturnValue WITH (NOLOCK) on tdvReturnValue.TableRowID = tr.TableRowID AND tdvReturnValue.DetailFieldID = @ReturnColumnDetailFieldID
			WHERE tr.detailfieldID = @TableDetailFieldID
			AND tdvColumnA.DetailValue = @ColumnAValue
			and tr.MatterID = @ObjectID 
			and tr.ClientID = @ClientID 
			ORDER BY tdvDateColumnB.ValueDate ASC

		END

		IF @ColumnBValue not in ('high','low') /*IF no date, match on a string*/
		BEGIN

			SELECT TOP 1 @ReturnValue  = tdvReturnValue.DetailValue
			FROM tablerows tr WITH (NOLOCK) 
			Inner join TableDetailValues tdvColumnA WITH (NOLOCK) on tdvColumnA.TableRowID = TR.TableRowID AND tdvColumnA.DetailFieldID = @ColumnADetailFieldID
			INNER JOIN TableDetailValues tdvDateColumnB WITH (NOLOCK) on tdvDateColumnB.TableRowID = tr.TableRowID AND tdvDateColumnB.DetailFieldID = @DateColumnBDetailFieldID
			INNER JOIN TableDetailValues tdvReturnValue WITH (NOLOCK) on tdvReturnValue.TableRowID = tr.TableRowID AND tdvReturnValue.DetailFieldID = @ReturnColumnDetailFieldID
			WHERE tr.detailfieldID = @TableDetailFieldID
			AND tdvColumnA.DetailValue = @ColumnAValue
			and tr.MatterID = @ObjectID 
			and tr.ClientID = @ClientID 
			AND tdvDateColumnB.DetailValue = @ColumnBValue

		END
		
	END
	
	/*Case######################################################*/
	IF @DetailFieldSubtypeID = 11 
	BEGIN

		IF @ColumnBValue = 'High' /*Order dates DESC*/
		BEGIN
				
			SELECT TOP 1 @ReturnValue  = tdvReturnValue.DetailValue
			FROM tablerows tr WITH (NOLOCK) 
			Inner join TableDetailValues tdvColumnA WITH (NOLOCK) on tdvColumnA.TableRowID = TR.TableRowID AND tdvColumnA.DetailFieldID = @ColumnADetailFieldID
			INNER JOIN TableDetailValues tdvDateColumnB WITH (NOLOCK) on tdvDateColumnB.TableRowID = tr.TableRowID AND tdvDateColumnB.DetailFieldID = @DateColumnBDetailFieldID
			INNER JOIN TableDetailValues tdvReturnValue WITH (NOLOCK) on tdvReturnValue.TableRowID = tr.TableRowID AND tdvReturnValue.DetailFieldID = @ReturnColumnDetailFieldID
			WHERE tr.detailfieldID = @TableDetailFieldID
			AND tdvColumnA.DetailValue = @ColumnAValue
			and tr.CaseID = @ObjectID 
			and tr.ClientID = @ClientID 
			ORDER BY tdvDateColumnB.ValueDate DESC

		END

		IF @ColumnBValue = 'Low' /*Order dates ASC*/
		BEGIN
				
			SELECT TOP 1 @ReturnValue  = tdvReturnValue.DetailValue
			FROM tablerows tr WITH (NOLOCK) 
			Inner join TableDetailValues tdvColumnA WITH (NOLOCK) on tdvColumnA.TableRowID = TR.TableRowID AND tdvColumnA.DetailFieldID = @ColumnADetailFieldID
			INNER JOIN TableDetailValues tdvDateColumnB WITH (NOLOCK) on tdvDateColumnB.TableRowID = tr.TableRowID AND tdvDateColumnB.DetailFieldID = @DateColumnBDetailFieldID
			INNER JOIN TableDetailValues tdvReturnValue WITH (NOLOCK) on tdvReturnValue.TableRowID = tr.TableRowID AND tdvReturnValue.DetailFieldID = @ReturnColumnDetailFieldID
			WHERE tr.detailfieldID = @TableDetailFieldID
			AND tdvColumnA.DetailValue = @ColumnAValue
			and tr.CaseID = @ObjectID 
			and tr.ClientID = @ClientID 
			ORDER BY tdvDateColumnB.ValueDate ASC

		END

		IF @ColumnBValue not in ('high','low') /*IF no date, match on a string*/
		BEGIN

			SELECT TOP 1 @ReturnValue  = tdvReturnValue.DetailValue
			FROM tablerows tr WITH (NOLOCK) 
			Inner join TableDetailValues tdvColumnA WITH (NOLOCK) on tdvColumnA.TableRowID = TR.TableRowID AND tdvColumnA.DetailFieldID = @ColumnADetailFieldID
			INNER JOIN TableDetailValues tdvDateColumnB WITH (NOLOCK) on tdvDateColumnB.TableRowID = tr.TableRowID AND tdvDateColumnB.DetailFieldID = @DateColumnBDetailFieldID
			INNER JOIN TableDetailValues tdvReturnValue WITH (NOLOCK) on tdvReturnValue.TableRowID = tr.TableRowID AND tdvReturnValue.DetailFieldID = @ReturnColumnDetailFieldID
			WHERE tr.detailfieldID = @TableDetailFieldID
			AND tdvColumnA.DetailValue = @ColumnAValue
			and tr.CaseID = @ObjectID 
			and tr.ClientID = @ClientID 
			AND tdvDateColumnB.DetailValue = @ColumnBValue

		END		
		
	END 
			


			

	
	
	/*Find out if @ReturnColumnDetailFieldID is a lookuplist, get the lookuplistID if it is*/			
	SELECT @LookupListID = df.LookupListID  
	from DetailFieldS df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @ReturnColumnDetailFieldID
	
	if @LookupListID IS NOT NULL 
	begin 
	
		SELECT TOP 1 @ReturnValue = l.ItemValue
		from LookupListItems l WITH (NOLOCK) 
		WHERE cast(l.LookupListItemID as varchar(300)) = @ReturnValue
		and l.LookupListID = @LookupListID
		
	end
	
	RETURN @ReturnValue

END


-- SELECT [Aquarius].[dbo].[fn_C00_ReturnTableValue_2Conditions](12895258 /*LeadID*/,173524/*Surce Claimstable*/,173823 /*ReturnColumn*/,173517 /*ColumnA*/,'Lloyds' /*ColumnA value*/,173525/*DateColumnB*/,'high') 
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReturnTableValue_2Conditions] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ReturnTableValue_2Conditions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReturnTableValue_2Conditions] TO [sp_executeall]
GO
