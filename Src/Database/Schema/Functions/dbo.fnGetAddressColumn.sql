SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2016-05-12
-- Description:	Get a particular column of an address (for invokefunctions)
-- =============================================
CREATE FUNCTION [dbo].[fnGetAddressColumn]
(
	@InputString nvarchar(max),
	@Delimiter VARCHAR(10)  = ',',
	@RequiredColumnName VARCHAR(100)
)
RETURNS VARCHAR(200)
AS
BEGIN

	IF @Delimiter = '%2C'
	BEGIN
	
		SELECT @Delimiter = ','
	
	END

	DECLARE @ReturnValue VARCHAR(200)

	SELECT @ReturnValue = CASE @RequiredColumnName
		WHEN 'AddressLine1' THEN a.Address1
		WHEN 'AddressLine2' THEN a.Address2
		WHEN 'Town' THEN a.Town
		WHEN 'County' THEN a.County
		WHEN 'Postcode' THEN a.Postcode
		END
	FROM dbo.fnAddressRecordFromCSVWithParam(@InputString, @Delimiter) a
	
	IF @ReturnValue = @InputString
	BEGIN
	
		SELECT @ReturnValue = ''
	
	END

	RETURN @ReturnValue

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetAddressColumn] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetAddressColumn] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetAddressColumn] TO [sp_executeall]
GO
