SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-01-29
-- Description:	Constructs the mandate reference for this matterID
-- Mods
--  2015-08-05 DCM Added checks to enusre using collections MatterID & always a 3 digit affinity code
-- =============================================

CREATE FUNCTION [dbo].[fn_C600_GetMandateReference]
(
      @MatterID INT
)
RETURNS VARCHAR(50)     
AS
BEGIN
      
      DECLARE @MandateReference VARCHAR(50), @CaseID INT
      
      -- make sure we are dealing with a collections matterID
      IF NOT EXISTS ( SELECT * FROM Lead l WITH (NOLOCK)
                        INNER JOIN Matter m WITH (NOLOCK) ON l.LeadID=m.LeadID AND m.MatterID=@MatterID
                        WHERE l.LeadTypeID=1493 )
      BEGIN
      
            SELECT @MatterID=ltr.ToMatterID FROM LeadTypeRelationship ltr WITH (NOLOCK) 
            WHERE ltr.FromMatterID=@MatterID AND ltr.ToLeadTypeID=1493
            
      END
      
      SELECT @CaseID=m.CaseID FROM Matter m WITH (NOLOCK) WHERE m.MatterID=@MatterID
                  
      SELECT @MandateReference=scli.ItemValue 
      FROM CustomerDetailValues sc WITH (NOLOCK) 
      INNER JOIN Lead cl WITH (NOLOCK) ON sc.CustomerID=cl.CustomerID
      INNER JOIN Matter cm WITH (NOLOCK) ON cm.LeadID=cl.LeadID AND cm.MatterID=@MatterID
      INNER JOIN ResourceListDetailValues scrl WITH (NOLOCK) ON sc.ValueInt=scrl.ResourceListID and scrl.DetailFieldID=170127
      INNER JOIN LookupListItems scli WITH (NOLOCK) ON scrl.ValueInt=scli.LookupListItemID
      WHERE sc.DetailFieldID=170144
      
      IF LEN(@MandateReference) > 3 
      BEGIN 
      
		SELECT @MandateReference =  LEFT(ISNULL(@MandateReference,'xxx') + 'xxx',3)
      
      END 
      
      SELECT @MandateReference = CASE WHEN ISNULL(dbo.fnGetDv(177131,@CaseID),'') = '' THEN 
                                                      ISNULL(@MandateReference,'xxx') + CONVERT(VARCHAR(30),@MatterID) 
                                             ELSE
                                                      dbo.fnGetDv(177131,@CaseID)
                                             END


	/*Clean out special characters*/ 
	SELECT @MandateReference = REPLACE(@MandateReference,'&','')

    RETURN @MandateReference

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetMandateReference] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetMandateReference] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetMandateReference] TO [sp_executeall]
GO
