SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Umer Hamid
-- Create date: 2018-10-18
-- Description: Check if preferred payment date for renewed policy is within 10 days after the renewal date
-- 2018-12-13	AHOD	Updated logic see mods below
-- 2018-12-14	AHOD	Expanded the logic to cater for Preferred Col date being before the Renewal Date
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_IsPaymentDayWithinTenWorkingDaysOfRenewal]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS INT	
AS
BEGIN

DECLARE		@PaymentDayWithinTenWorkingDays INT = 0,
			@FirstPaymentDateAfterRenewal DATE, 
			@RenewalDate DATE,
			@RenewalDatePlusTenWorkingDays DATE, 
			@RenewalPPID INT,
			@PrefCollectionDay INT,
			@PrefCollectionDate DATE,
			@ValidFrom DATE,
			@ValueDate DATE 


	-- The first step is to get the PPPS ID of the renewal schedule 
	SELECT top 1 @RenewalPPID =  pp.PurchasedProductID FROM PurchasedProduct pp WITH (NOLOCK)
	WHERE pp.ObjectID = @MatterID
	
	----Select the first collection date of the first row in the PPPS for the renewal schedule. 
	--SELECT TOP 1 @FirstPaymentDateAfterRenewal =  ppps.ActualCollectionDate
	--FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
	--where ppps.PurchasedProductID = @RenewalPPID 
	--order by ActualCollectionDate asc
	
	--/*Assign Renewal Date */
	--SELECT @RenewalDate = ValueDate 
	--FROM MatterDetailValues mdv 
	--WHERE DetailFieldID = 175307 
	--AND mdv.MatterID = @MatterID
	
	/*Customer Preferred Collection Day*/
	SELECT @PrefCollectionDay = mdv.ValueInt 
	FROM MatterDetailValues mdv 
	WHERE DetailFieldID = 170168 
	AND mdv.MatterID = @MatterID
	
	--SELECT @PrefCollectionDate = DATEADD(m, DATEDIFF(m, 0, @RenewalDate), 0) + @PrefCollectionDay -1
	
	--/*
	--AHOD 2018-12-14 Added If block to cater for Preferred Collection being calculated before renewal date 
	--*/
	
	--IF @PrefCollectionDate <= @RenewalDate
	--	BEGIN
	--		SELECT @PrefCollectionDate = DATEADD(m, DATEDIFF(m, 0, @FirstPaymentDateAfterRenewal), 0) + @PrefCollectionDay -1
	--	END
		
	--/*
	--Here we are checking if th first payment day of the renewed policy schedule is greater than the renewal date itself (It will be unless something weird happens), 
	--then we are checking if the same renewal date is less than the renewal + 10 working days. If both these condition
	--*/
		
	--IF @PrefCollectionDate <= @FirstPaymentDateAfterRenewal
	--	BEGIN 
	--		SELECT @PaymentDayWithinTenWorkingDays = 1
	--	END
	
	SELECT @ValidFrom = pp.ValidFrom  FROM PurchasedProduct pp WITH (NOLOCK)
	WHERE pp.PurchasedProductID = @RenewalPPID

	/* JEL 2019-01-22 new logic for Defect #1338*/
	 DECLARE @YearPart VARCHAR(4) = CAST(DATEPART(Year, @ValidFrom) AS VARCHAR)
	 DECLARE @MonthPart VARCHAR(2) = CAST(DATEPART(Month, @ValidFrom) AS VARCHAR)
	 DECLARE @DayPart VARCHAR(2)
 
	 SELECT @DayPart = CASE WHEN @PrefCollectionDay <= 9 THEN '0' + @PrefCollectionDay ELSE @PrefCollectionDay END
 
	 SELECT @ValueDate = CAST(@YearPart + '-' + @MonthPart + '-' + @DayPart AS VARCHAR)				
 
	 IF @ValueDate < dbo.fnAddWorkingDays(@ValidFrom,13)
	 BEGIN 
		
		  SELECT @PaymentDayWithinTenWorkingDays = 1


	 END 
	 ELSE 
	 BEGIN 
	
		SELECT @PaymentDayWithinTenWorkingDays = 0

	 END

	RETURN @PaymentDayWithinTenWorkingDays
 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_IsPaymentDayWithinTenWorkingDaysOfRenewal] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_IsPaymentDayWithinTenWorkingDaysOfRenewal] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_IsPaymentDayWithinTenWorkingDaysOfRenewal] TO [sp_executeall]
GO
