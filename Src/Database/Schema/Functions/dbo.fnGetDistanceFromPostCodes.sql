SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- ALTER date: 18-July-2009
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[fnGetDistanceFromPostCodes]
(
	@PostCode varchar(10),
	@DestinationPostCode varchar(10),
	@FudgeFactor numeric(4,2) = 0
)
RETURNS numeric(16,12)
AS
BEGIN

	DECLARE @Distance as numeric(16,12),
			@Lat numeric(25,18), 
			@Long numeric(25,18),
			@PartPostCode varchar(5),
			@DestinationPartPostCode varchar(5),
			@DestinationLat numeric(25,18), 
			@DestinationLong numeric(25,18),
			@Len int


	Select @Len = charindex(' ', @PostCode)

	If len(replace(@PostCode, ' ', '')) <= 4 Select @Len = 4

	If @Len = 0 Select @Len = 4

	Select @PartPostCode = left(replace(@PostCode, ' ', ''), @Len)

	Select @Lat = pcl.Latitude, @Long = pcl.Longitude
	From [AquariusMaster].dbo.PostCodeLookup pcl WITH (NOLOCK)
	Where pcl.PostCode = @PartPostCode

	Select @Len = charindex(' ', @DestinationPostCode)

	If len(@DestinationPostCode) <= 4 Select @Len = 4

	Select @DestinationPartPostCode = left(replace(@DestinationPostCode, ' ', ''), @Len)

	Select top 1 @DestinationLat = pcl.Latitude, @DestinationLong = pcl.Longitude
	From [AquariusMaster].dbo.PostCodeLookup pcl WITH (NOLOCK)
	Where pcl.PostCode Like @DestinationPartPostCode + '%'

	Select @Distance = ((3963.105*ACOS(COS(RADIANS(90-@DestinationLat))*COS(RADIANS(90-@Lat))+SIN(RADIANS(90-@DestinationLat))*SIN(RADIANS(90-@Lat))*COS(RADIANS(@DestinationLong-@Long)))) * @FudgeFactor)

	RETURN isnull(@Distance, '0.00')

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDistanceFromPostCodes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetDistanceFromPostCodes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDistanceFromPostCodes] TO [sp_executeall]
GO
