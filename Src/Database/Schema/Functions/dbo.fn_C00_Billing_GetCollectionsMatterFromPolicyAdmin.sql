SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		James Lewis
-- Create date: 2018-08-17
-- Description:	Takes the Collections MatterId and returns the associated Policy Admin matter 
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_Billing_GetCollectionsMatterFromPolicyAdmin]
(
	 @MatterID		INT
)
RETURNS INT
AS
BEGIN
	
	DECLARE @CollectionsMatterID INT 
	
	SELECT @CollectionsMatterID = a.ObjectID
	FROM PurchasedProduct p WITH ( NOLOCK ) 
	INNER JOIN Account a WITH ( NOLOCK ) on a.AccountID = p.AccountID
	Where p.ObjectID = @MatterID

	RETURN @CollectionsMatterID
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_Billing_GetCollectionsMatterFromPolicyAdmin] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_Billing_GetCollectionsMatterFromPolicyAdmin] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_Billing_GetCollectionsMatterFromPolicyAdmin] TO [sp_executeall]
GO
