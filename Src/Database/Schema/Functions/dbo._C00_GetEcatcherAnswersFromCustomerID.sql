SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-08-11
-- Description: Return a value from a table, specify 2 columns with matching conditions
-- =============================================
CREATE FUNCTION [dbo].[_C00_GetEcatcherAnswersFromCustomerID]
(
		 @CustomerID  int 
		,@QuestionID int 
)
RETURNS varchar(2000)
AS
BEGIN

Declare @ClientID int = (select top 1 ClientID from Customers WITH (NOLOCK)  where CustomerID = @CustomerID)
DECLARE @Answer Varchar(2000)

	SELECT top 1 @Answer  = 
	CASE When cpa.AnswerText is null then ca.Answer else cpa.AnswerText end
	FROM Customers C WITH (NOLOCK) 
	INNER JOIN CustomerQuestionnaires cq WITH (NOLOCK) ON cq.TrackingID = c.CustomerID
	inner join CustomerOutcomes co WITH (NOLOCK) on co.CustomerID=cq.CustomerID
	iNNER JOIN CustomerAnswers ca WITH (NOLOCK) on ca.CustomerQuestionnaireID=cq.CustomerQuestionnaireID
	LEFT JOIN QuestionPossibleAnswers cpa WITH (NOLOCK) on cpa.QuestionPossibleAnswerID=ca.QuestionPossibleAnswerID
	inner join MasterQuestions mq WITH (NOLOCK) on ca.MasterQuestionID=mq.MasterQuestionID
	WHERE cq.ClientID = @ClientID
	AND C.CustomerID = @CustomerID
	and mq.MasterQuestionID = @QuestionID
	order by ca.CustomerAnswerID desc

	RETURN @Answer

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetEcatcherAnswersFromCustomerID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetEcatcherAnswersFromCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetEcatcherAnswersFromCustomerID] TO [sp_executeall]
GO
