SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-12-02
-- Description:	Returns the Policy Admin Fee (PAF) based on the PAF in force at start date.
--				Return value is negative if fee to be applied before IPT
-- Mods
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetPolicyAdminFee]
(
	@SchemeID INT,
	@StartDate DATE
)
RETURNS MONEY	
AS
BEGIN

	
	DECLARE @ClientID INT,
			@Gross MONEY,
			@PAF MONEY,
			@Timing INT
	
	-- get PAF
	SELECT @PAF=paf.ValueMoney,@Timing=CASE WHEN timing.ValueInt=72062 THEN -1 ELSE 1 END
	FROM MatterDetailValues scheme 
	INNER JOIN TableRows tr WITH (NOLOCK) ON scheme.MatterID=tr.MatterID AND tr.DetailFieldID=175395
	INNER JOIN TableDetailValues from_date WITH (NOLOCK) ON tr.TableRowID=from_date.TableRowID AND from_date.DetailFieldID=175391
	INNER JOIN TableDetailValues to_date WITH (NOLOCK) ON tr.TableRowID=to_date.TableRowID AND to_date.DetailFieldID=175392
	INNER JOIN TableDetailValues paf WITH (NOLOCK) ON tr.TableRowID=paf.TableRowID AND paf.DetailFieldID=175394
	INNER JOIN TableDetailValues timing WITH (NOLOCK) ON from_date.TableRowID=timing.TableRowID AND timing.DetailFieldID=175393
	WHERE scheme.DetailFieldID=145689 AND scheme.ValueInt=@SchemeID
	AND (from_date.ValueDate IS NULL OR @StartDate >= from_date.ValueDate)
	AND (to_date.ValueDate IS NULL OR @StartDate <= to_date.ValueDate)

	SELECT @PAF=ISNULL(@PAF,0.00)*@Timing

	RETURN ISNULL(@PAF, 0)
 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetPolicyAdminFee] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetPolicyAdminFee] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetPolicyAdminFee] TO [sp_executeall]
GO
