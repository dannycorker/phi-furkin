SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-09-23
-- Description:	Returns the contents of the Risk Bordereau view
-- =============================================
 
CREATE FUNCTION [dbo].[fn_C384_RiskBordereau]
(
	@Bordereau VARCHAR(50) = 'Alpha'
)
RETURNS
 
--declare @Bordereau VARCHAR(50) = 'Alpha'
--declare
@Data TABLE
(
	AffinityShortCode INT,
	CustomerID INT,
	PolAdminLeadID INT,
	PolAdminMatterID INT,
	PolHistoryTableRowID INT,
	StartDate DATE,
	EndDate DATE,
	SystemKey VARCHAR(100),
	AlphaReference VARCHAR(100),
	PolicyReference VARCHAR(100),
	Inception VARCHAR(100),
	Expiry VARCHAR(100),
	RiskTypeCode VARCHAR(100),
	Currency VARCHAR(100),
	InsuredReference INT,
	InsuredName VARCHAR(100),
	InsuredCountry VARCHAR(100),
	InsuredPostalCode VARCHAR(100),
	InsuredAddress VARCHAR(200),
	InsuredEmailAddress VARCHAR(200),
	InsuredTelNoHome VARCHAR(100),
	InsuredTelNoMobile VARCHAR(100),
	UnderwriterID VARCHAR(100),
	OriginalInception VARCHAR(100),
	TypeOfBusiness VARCHAR(100),
	Status VARCHAR(100),
	StatusDate VARCHAR(100),
	Share MONEY,
	LimitCurrency VARCHAR(100),
	SumInsured MONEY,
	DeductibleSum MONEY,
	DeductibleCompulsoryExcess MONEY,
	DeductibleVoluntaryExcess MONEY,
	NoOfInstalments INT,
	Affinity VARCHAR(100),
	Product VARCHAR(100),
	PetType VARCHAR(100),
	Breed VARCHAR(100),
	BreedCode VARCHAR(100),
	PetSex VARCHAR(100),
	PetDateOfBirth VARCHAR(100),
	Neutered VARCHAR(100),
	PurchasePrice MONEY,
	DiscountCode VARCHAR(100),
	AnnualRounding MONEY,
	WrittenBasePremiumNet MONEY,
	WrittenBinderCommission MONEY,
	WrittenOriginalBrokerage MONEY,
	WrittenBaseCommissionBroker MONEY,
	WrittenBaseCommissionAffinity MONEY,
	WrittenBaseCommissionCompany MONEY,
	WrittenDiscount MONEY,
	WrittenDiscountMultiPet MONEY,
	WrittenDiscountMarketingCode MONEY,
	WrittenIPT MONEY,
	WrittenPolicyFee MONEY,
	PaidBasePremiumNet MONEY,
	PaidBinderCommission MONEY,
	PaidOriginalBrokerage MONEY,
	PaidBaseCommissionBroker MONEY,
	PaidBaseCommissionAffinity MONEY,
	PaidBaseCommissionCompany MONEY,
	PaidDiscount MONEY,
	PaidDiscountMultiPet MONEY,
	PaidDiscountMarketingCode MONEY,
	PaidIPT MONEY,
	PaidPolicyFee MONEY
)	

AS
BEGIN

	
	/* Table variables.  NB CTEs make this function crash and burn with just a handful of records */

	DECLARE @CheckpointRiskTotals TABLE (
							IsAnnual BIT,
							IsWritten BIT,
							IsTotal BIT,
							PolHistTableRowID INT,
							RuleCheckpoint VARCHAR(100),
							RiskType VARCHAR(100),
							Total FLOAT,
							CountIncs INT
							)

	DECLARE @CheckpointTotals TABLE (
							IsAnnual BIT,
							IsWritten BIT,
							PolHistTableRowID INT,
							RuleCheckpoint VARCHAR(100),
							Total FLOAT,
							CountIncs INT
							)
							
	DECLARE @PolicyLimits TABLE (
							PolHistTableRowID INT,
							RiskType VARCHAR(100),
							ValueMoney FLOAT
							)

	DECLARE @PremiumCalcs TABLE ( 
							PolAdminMatterID INT,
							PolPremTableRowID INT,
							PolHistTableRowID INT,
							PremiumCalculationID INT,
							PremiumRowStatus INT,
							PremiumRowMonth INT,
							PremiumRowFromDate DATE,
							PremiumRowToDate DATE,
							RNO INT 
							)

	DECLARE @PremiumHistoryRows TABLE ( 
							PolAdminMatterID INT,
							PolHistTableRowID INT,
							StartDate DATE,
							EndDate DATE 
							)
							
	DECLARE @PremiumMatters TABLE ( 
							PolAdminMatterID INT, 
							TRID INT,
							IsAnnual INT 
							)
							
	DECLARE @RiskFields TABLE (
							PolHistTableRowID INT,
							RiskType VARCHAR(100),
							CoIns FLOAT,
							Limit FLOAT,
							volXS FLOAT,
							comXS FLOAT
							)
							
	DECLARE @RiskSchemeMatters TABLE (
							PolAdminMatterID INT,
							SchemeMatterID INT,
							PolHistTableRowID INT
							)
							
	DECLARE @RiskTotals TABLE (
							PolHistTableRowID INT,
							RiskType VARCHAR(100),
							ColumnName VARCHAR(50),
							CellValue FLOAT,
							NoIncs INT
							)
							
	DECLARE @RiskTypes TABLE ( 
							PolHistTableRowID INT,
							QandB VARCHAR(100),
							RiskType VARCHAR(100),
							RiskTypeCode VARCHAR(100),
							AquariumTableText VARCHAR(100)
							)
							
	DECLARE @RiskValues TABLE (
							IsAnnual BIT,
							IsWritten BIT,
							PolHistTableRowID INT,
							RiskType VARCHAR(100),
							QandB VARCHAR(100),
							RiskTypeNet FLOAT,
							CountIncs INT
							)
							
	DECLARE @Rounding TABLE (
							PolHistTableRowID INT,
							ActualWritten FLOAT,
							ActualPaid FLOAT,
							ReportWritten FLOAT,
							ReportPaid FLOAT,
							FinalReportWritten FLOAT,
							FinalReportPaid FLOAT,
							IsAnnual INT
							)
							
	DECLARE @RoundingRaw TABLE (
							PolHistTableRowID INT,
							IsAnnual BIT,
							IsCancelled BIT,
							Status INT,
							PayType INT,
							Num INT,
							Total MONEY
							)

	DECLARE @StatusInfo TABLE (
							PolAdminMatterID INT,
							Status VARCHAR(100),
							StatusDate VARCHAR(20),
							RNO INT
							)
	
	/* 
	   
	   Find all premium matters which have used rules engine checkpointing and where the customer
	   is not test.
	   
	   This may need an additional filter for underwriter in future
	   
	 */
	 
	INSERT INTO @PremiumMatters
	SELECT polprem.MatterID [PolAdminMatterID]
			, MAX(polprem.TableRowID) [TRID]
			, CASE WHEN ia.ValueInt=69944 THEN 1 ELSE 0 END
	FROM TableRows polprem WITH (NOLOCK) 
	INNER JOIN TableDetailValues pcid WITH (NOLOCK) ON polprem.TableRowID=pcid.TableRowID AND pcid.DetailFieldID IN (175710)--,175709)
	INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.FromMatterID=polprem.MatterID AND ltr.ToLeadTypeID=1493
	INNER JOIN MatterDetailValues ia WITH (NOLOCK) ON ltr.ToMatterID=ia.MatterID AND ia.DetailFieldID=170145
	WHERE polprem.DetailFieldID IN (170088)--,175336) 
	AND pcid.ValueInt > 0
--and polprem.MatterID IN (50038107,50037608,50038100)
	GROUP BY polprem.MatterID, ia.ValueInt 
	
	--SELECT 'PremiumMatters',* FROM @PremiumMatters

	/* Get the premium history rows for the premium matters */
	INSERT INTO @PremiumHistoryRows
	SELECT pm.PolAdminMatterID, ph.TableRowID [TableRowID]
			, sd.DetailValue [StartDate]
			, ed.DetailValue [EndDate]
	FROM @PremiumMatters pm
	INNER JOIN TableRows ph WITH (NOLOCK) ON pm.PolAdminMatterID=ph.MatterID AND ph.DetailFieldID=170033
	INNER JOIN TableDetailValues sd WITH (NOLOCK) ON ph.TableRowID=sd.TableRowID AND sd.DetailFieldID=145663
	INNER JOIN TableDetailValues ed WITH (NOLOCK) ON ph.TableRowID=ed.TableRowID AND ed.DetailFieldID=145664

	--SELECT 'PremiumHistoryRows',* FROM @PremiumHistoryRows
	
	/* Get the premium rows related to each history row where dates match and not an adjustment or record of failed payment */

	-- 1. Monthly Accounts
	INSERT INTO @PremiumCalcs
	SELECT polprem.MatterID [PolAdminMatterID]
			, polprem.TableRowID [PolPremTableRowID]
			, phr.PolHistTableRowID
			, polprem.ValueInt [PremiumCalculationID]
			, stat.ValueInt [PremiumRowStatus]
			, mon.ValueInt [PremiumRowMonth]
			, fd.ValueDate [PremiumRowFromDate]
			, td.ValueDate [PremiumRowToDate]
			, ROW_NUMBER() OVER(PARTITION BY polprem.ValueInt ORDER BY polprem.TableRowID ASC) as [RNO]
	FROM TableDetailValues polprem WITH (NOLOCK)
	INNER JOIN TableDetailValues fd WITH (NOLOCK) ON polprem.TableRowID=fd.TableRowID AND fd.DetailFieldID=170076
	INNER JOIN TableDetailValues td WITH (NOLOCK) ON polprem.TableRowID=td.TableRowID AND td.DetailFieldID=170077
	INNER JOIN @PremiumHistoryRows phr ON polprem.MatterID=phr.PolAdminMatterID AND fd.ValueDate>=phr.StartDate AND td.ValueDate<=phr.EndDate
	INNER JOIN TableDetailValues ptype WITH (NOLOCK) ON polprem.TableRowID=ptype.TableRowID AND ptype.DetailFieldID=175383
	INNER JOIN TableDetailValues mon WITH (NOLOCK) ON polprem.TableRowID=mon.TableRowID AND mon.DetailFieldID=170067
	INNER JOIN TableDetailValues stat WITH (NOLOCK) ON polprem.TableRowID=stat.TableRowID AND stat.DetailFieldID=170078
	INNER JOIN @PremiumMatters pm ON polprem.MatterID=pm.PolAdminMatterID AND pm.IsAnnual=0
	WHERE polprem.DetailFieldID=175710
		AND stat.ValueInt IN (69898,69903,72154) -- only paid, pending or ready
		AND ptype.ValueInt <> 72140 -- not adjustment rows
		AND polprem.ValueInt IS NOT NULL
	
	--SELECT 'PremiumCalcs - Monthly',* FROM @PremiumCalcs	
	
	-- 2. Annual accounts (these use the Premium Detail History Table)
	INSERT INTO @PremiumCalcs
	SELECT polprem.MatterID [PolAdminMatterID]
			, polprem.TableRowID [PolPremTableRowID]
			, phr.PolHistTableRowID
			, polprem.ValueInt [PremiumCalculationID]
			, stat.ValueInt [PremiumRowStatus]
			, 0 [PremiumRowMonth]
			, fd.ValueDate [PremiumRowFromDate]
			, CASE WHEN td.ValueDate IS NULL THEN phr.EndDate ELSE td.ValueDate END [PremiumRowToDate]
			, ROW_NUMBER() OVER(PARTITION BY polprem.ValueInt ORDER BY polprem.TableRowID ASC) as [RNO]
	FROM TableDetailValues polprem WITH (NOLOCK)
	INNER JOIN TableDetailValues fd WITH (NOLOCK) ON polprem.TableRowID=fd.TableRowID AND fd.DetailFieldID=175346
	INNER JOIN TableDetailValues td WITH (NOLOCK) ON polprem.TableRowID=td.TableRowID AND td.DetailFieldID=175347
	INNER JOIN @PremiumHistoryRows phr ON polprem.MatterID=phr.PolAdminMatterID AND fd.ValueDate>=phr.StartDate AND (td.ValueDate<=phr.EndDate OR td.ValueDate IS NULL)
	INNER JOIN TableDetailValues ppPCID WITH (NOLOCK) ON polprem.ValueInt=ppPCID.ValueInt AND ppPCID.DetailFieldID=175710 AND polprem.ValueInt<>0
	INNER JOIN TableDetailValues stat WITH (NOLOCK) ON ppPCID.TableRowID=stat.TableRowID AND stat.DetailFieldID=170078
	INNER JOIN @PremiumMatters pm ON polprem.MatterID=pm.PolAdminMatterID AND pm.IsAnnual=1
	WHERE polprem.DetailFieldID=175709
		AND stat.ValueInt IN (69898,69903,72154) -- Paid, Pending or Ready
		AND polprem.ValueInt IS NOT NULL

	--SELECT '@PremiumCalcs',* FROM @PremiumCalcs	

	/* Find the risk types included in each rating calcuation */
	INSERT INTO @RiskTypes
	SELECT DISTINCT pc.PolHistTableRowID
			, QandB.DetailValue [QandB]
			, rt.DetailValue [RiskType]
			, rtc.DetailValue [RiskTypeCode]
			, att.DetailValue [AquariumTableText]
	FROM @PremiumCalcs pc 
	INNER JOIN PremiumCalculationDetailValues pcdv WITH (NOLOCK) ON pc.PremiumCalculationID=pcdv.PremiumCalculationDetailID
	INNER JOIN ResourceListDetailValues QandB WITH (NOLOCK) ON pcdv.RuleCheckpoint=QandB.DetailValue AND QandB.DetailFieldID=175714
	INNER JOIN ResourceListDetailValues rt WITH (NOLOCK) ON QandB.ResourceListID=rt.ResourceListID AND rt.DetailFieldID=175715
	INNER JOIN ResourceListDetailValues rtc WITH (NOLOCK) ON QandB.ResourceListID=rtc.ResourceListID AND rtc.DetailFieldID=175716
	INNER JOIN ResourceListDetailValues att WITH (NOLOCK) ON QandB.ResourceListID=att.ResourceListID AND att.DetailFieldID=175717
	WHERE pc.RNO=1

	--SELECT '@RiskTypes',* FROM @RiskTypes	
	
	/* 
	
		CALCULATE WRITTEN AND PAID PREMIUMS
		
		Using above raw information calculate the Written premium, i.e. the total annual premium, 
		and Paid Premium, i.e. the amount paid to date, for each premium matter.
		
		This must be done separately for monthly and annual accounts.  
		For monthly premiums:
		
			* Paid is the sum of premium payment premium rows that have a paid status
			* Written is the sum of paid premium rows plus the data for the most recent ready row multiplied by 
			  the number of increments left to pay
			
		For annual premiums:
			
			* Written is the sum of the fractional premium due for each MTA period 
			* Paid equals written where the amount actually paid is greater than (assumes that a refund is in progress) or equal to the written amount, 
			  or the actual amount paid if the actual amount paid is less than written  

		The premium is split between risk types (= cover types).  These have a specific format of rules engine checkpoint name. Each risk type is
		reported on a separate row of the bordereau.
		
		Other rule checkpoint values (i.e. that are not risk types), e.g. IPT, commission, etc, are split between risk types, using the proportion 
		of its risk premium value to the total risk premium for all of the risks defined as included in that checkpoint. Inclusion of risk types in 
		a checkpoint is defined by resource list.
		
		Various resource list are used to configure the way in which the bordereau is partitioned. See RLs whose names begin with Bordereau.
		  	
	*/
	
	-- 1a. MONTHLY ACCOUNTS - PAID
	
	INSERT INTO @RiskValues
	SELECT 0 [IsAnnual]
	, 0 [IsWritten]
	, pc.PolHistTableRowID
	, rt.RiskType
	, rt.QandB
	, CASE WHEN rt.RiskType='VET' 
			THEN SUM ( CAST( CASE WHEN pc.PremiumRowMonth=69896 THEN pcdv.RuleOutputFirstMonth ELSE pcdv.RuleOutputOtherMonth END AS FLOAT) )
			ELSE SUM ( CAST( CASE WHEN pc.PremiumRowMonth=69896 THEN pcdv.RuleInputOutputDeltaFirstMonth ELSE pcdv.RuleInputOutputDeltaOtherMonth END AS FLOAT) ) 
			END [RiskTypeNet]
	, ISNULL(COUNT(pcdv.RuleOutputFirstMonth),0) [CountIncs]	 
	FROM @PremiumCalcs pc
	INNER JOIN PremiumCalculationDetailValues pcdv WITH (NOLOCK) ON pc.PremiumCalculationID=pcdv.PremiumCalculationDetailID
	INNER JOIN @RiskTypes rt ON pc.PolHistTableRowID=rt.PolHistTableRowID AND rt.QandB=pcdv.RuleCheckpoint
	INNER JOIN @PremiumMatters pm ON pc.PolAdminMatterID=pm.PolAdminMatterID AND pm.IsAnnual=0
	WHERE pc.PremiumRowStatus IN (69898,69903)
	GROUP BY pc.PolHistTableRowID, rt.RiskType, rt.QandB
	
	--SELECT 'Paid - RiskValues',* FROM @RiskValues	

	INSERT INTO @CheckpointTotals
	SELECT 0 [IsAnnual]
		, 0	[IsWritten]
		, pc.PolHistTableRowID
		, pcdv.RuleCheckpoint [ColumnName]
		, SUM(ISNULL(CAST(CASE WHEN pc.PremiumRowMonth=69896 THEN pcdv.RuleInputOutputDeltaFirstMonth ELSE pcdv.RuleInputOutputDeltaOtherMonth END AS FLOAT),0) ) [CellValue]
		, 0
	FROM @PremiumCalcs pc
	LEFT JOIN PremiumCalculationDetailValues pcdv WITH (NOLOCK) ON pc.PremiumCalculationID=pcdv.PremiumCalculationDetailID
	INNER JOIN @PremiumMatters pm ON pc.PolAdminMatterID=pm.PolAdminMatterID AND pm.IsAnnual=0
	WHERE  pc.PremiumRowStatus IN (69898,69903)
		AND NOT EXISTS ( SELECT * FROM ResourceListDetailValues bord WITH (NOLOCK) 
							INNER JOIN ResourceListDetailValues rt WITH (NOLOCK) ON bord.ResourceListID=rt.ResourceListID AND rt.DetailFieldID=175714
							WHERE bord.DetailFieldID=175744 
								AND bord.DetailValue=@Bordereau
								AND rt.DetailValue=pcdv.RuleCheckpoint
						)
	GROUP BY pc.PolHistTableRowID, pcdv.RuleCheckpoint

	-- 1b. MONTHLY ACCOUNTS - WRITTEN
	-- get the current total including ALL ready rows
	INSERT INTO @RiskValues
	SELECT 0 [IsAnnual]
	, 1 [IsWritten]
	, pc.PolHistTableRowID
	, rt.RiskType
	, rt.QandB
	, CASE WHEN rt.RiskType='VET' 
			THEN SUM ( CAST( CASE WHEN pc.PremiumRowMonth=69896 THEN pcdv.RuleOutputFirstMonth ELSE pcdv.RuleOutputOtherMonth END AS FLOAT) )
			ELSE SUM ( CAST( CASE WHEN pc.PremiumRowMonth=69896 THEN pcdv.RuleInputOutputDeltaFirstMonth ELSE pcdv.RuleInputOutputDeltaOtherMonth END AS FLOAT) ) 
			END [RiskTypeNet]
	, ISNULL(COUNT(pcdv.RuleOutputFirstMonth),0) [CountIncs]	 
	FROM @PremiumCalcs pc
	INNER JOIN PremiumCalculationDetailValues pcdv WITH (NOLOCK) ON pc.PremiumCalculationID=pcdv.PremiumCalculationDetailID
	INNER JOIN @RiskTypes rt ON pc.PolHistTableRowID=rt.PolHistTableRowID AND rt.QandB=pcdv.RuleCheckpoint
	INNER JOIN @PremiumMatters pm ON pc.PolAdminMatterID=pm.PolAdminMatterID AND pm.IsAnnual=0
	WHERE pc.PremiumRowStatus IN (69898,69903,72154)
	GROUP BY pc.PolHistTableRowID, rt.RiskType, rt.QandB
	--SELECT 'WrittenPremiumRiskValuesint',* FROM @RiskValues	
	-- now make up the remaining increments using the most recent ready row
	UPDATE wprv
	SET RiskTypeNet=RiskTypeNet + 
		( CASE WHEN rt.RiskType='VET' THEN CAST(pcdv.RuleOutputOtherMonth AS FLOAT) 
									ELSE CAST(pcdv.RuleInputOutputDeltaOtherMonth AS FLOAT) END * (12 - wprv.CountIncs) ) 
	FROM @RiskValues wprv
	INNER JOIN  @PremiumCalcs pc ON pc.PolHistTableRowID=wprv.PolHistTableRowID
	INNER JOIN PremiumCalculationDetailValues pcdv WITH (NOLOCK) ON pc.PremiumCalculationID=pcdv.PremiumCalculationDetailID
	INNER JOIN @RiskTypes rt ON rt.PolHistTableRowID=pc.PolHistTableRowID AND rt.QandB=pcdv.RuleCheckpoint AND rt.RiskType=wprv.RiskType
	WHERE pc.PremiumRowStatus IN (72154)
		AND wprv.IsAnnual=0
		AND wprv.IsWritten=1 
		AND NOT EXISTS ( SELECT * FROM @PremiumCalcs pc2 WHERE pc.PolHistTableRowID=pc2.PolHistTableRowID AND pc2.PremiumRowFromDate>pc.PremiumRowFromDate AND pc2.PremiumRowStatus IN (72154) )
		AND NOT EXISTS ( SELECT * FROM TableDetailValues stat WITH (NOLOCK) WHERE stat.TableRowID=pc.PolHistTableRowID AND stat.DetailFieldID=145666
			AND stat.ValueInt=43003 ) -- Unless cancelled. For monthly, written=paid if policy has been cancelled
	
	--SELECT 'Written - RiskValues',* FROM @RiskValues	

	INSERT INTO @CheckpointTotals
	SELECT  0 [IsAnnual]
		, 1	[IsWritten]
		, pc.PolHistTableRowID
		, pcdv.RuleCheckpoint [ColumnName]
		, SUM(ISNULL(CAST(CASE WHEN pc.PremiumRowMonth=69896 THEN pcdv.RuleInputOutputDeltaFirstMonth ELSE pcdv.RuleInputOutputDeltaOtherMonth END AS FLOAT),0) ) [CellValue]
		, ISNULL(COUNT(pcdv.RuleOutputFirstMonth),0) [CountIncs]	 
	FROM @PremiumCalcs pc
	LEFT JOIN PremiumCalculationDetailValues pcdv WITH (NOLOCK) ON pc.PremiumCalculationID=pcdv.PremiumCalculationDetailID
	INNER JOIN @PremiumMatters pm ON pc.PolAdminMatterID=pm.PolAdminMatterID AND pm.IsAnnual=0
	WHERE  pc.PremiumRowStatus IN (69898,69903,72154)
		AND NOT EXISTS ( SELECT * FROM ResourceListDetailValues bord WITH (NOLOCK) 
							INNER JOIN ResourceListDetailValues rt WITH (NOLOCK) ON bord.ResourceListID=rt.ResourceListID AND rt.DetailFieldID=175714
							WHERE bord.DetailFieldID=175744 
								AND bord.DetailValue=@Bordereau
								AND rt.DetailValue=pcdv.RuleCheckpoint
						)
	GROUP BY pc.PolHistTableRowID, pcdv.RuleCheckpoint
	UPDATE ct
	SET Total=Total + CAST(pcdv.RuleInputOutputDeltaOtherMonth AS FLOAT) * (12 - ct.CountIncs) 
	FROM @CheckpointTotals ct
	INNER JOIN  @PremiumCalcs pc ON pc.PolHistTableRowID=ct.PolHistTableRowID
	INNER JOIN PremiumCalculationDetailValues pcdv WITH (NOLOCK) ON pc.PremiumCalculationID=pcdv.PremiumCalculationDetailID
		AND pcdv.RuleCheckpoint=ct.RuleCheckpoint
	WHERE ct.IsWritten=1
		AND pc.PremiumRowStatus IN (72154) 
		AND NOT EXISTS ( SELECT * FROM @PremiumCalcs pc2 WHERE pc.PolHistTableRowID=pc2.PolHistTableRowID AND pc2.PremiumRowFromDate>pc.PremiumRowFromDate AND pc2.PremiumRowStatus IN (72154) )
		AND NOT EXISTS ( SELECT * FROM TableDetailValues stat WITH (NOLOCK) WHERE stat.TableRowID=pc.PolHistTableRowID AND stat.DetailFieldID=145666
			AND stat.ValueInt=43003 ) -- Unless cancelled. For monthly, written=paid if policy has been cancelled
	
	--SELECT 'Written - CheckpointTotals',* FROM @CheckpointTotals ct WHERE ct.IsWritten=1	
	
	-- 2a. ANNUAL ACCOUNTS - PAID
	INSERT INTO @RiskValues
	SELECT 1 [IsAnnual]
	, 0 [IsWritten]
	, pc.PolHistTableRowID
	, rt.RiskType
	, rt.QandB
	, CASE WHEN rt.RiskType='VET' 
			THEN SUM ( CAST( pcdv.RuleOutput AS FLOAT) * ( DATEDIFF(DAY,pc.PremiumRowFromDate,pc.PremiumRowToDate) / CAST(DATEDIFF(day,phr.StartDate,dateadd(YEAR,1,phr.StartDate)) AS FLOAT)  ) )
			ELSE SUM ( CAST( pcdv.RuleInputOutputDelta AS FLOAT) * ( DATEDIFF(DAY,pc.PremiumRowFromDate,pc.PremiumRowToDate) / CAST(DATEDIFF(day,phr.StartDate,dateadd(YEAR,1,phr.StartDate)) AS FLOAT)  ) ) 
			END [RiskTypeNet]
	, 0 [CountIncs]	 
	FROM @PremiumCalcs pc
	INNER JOIN PremiumCalculationDetailValues pcdv WITH (NOLOCK) ON pc.PremiumCalculationID=pcdv.PremiumCalculationDetailID
	INNER JOIN @RiskTypes rt ON pc.PolHistTableRowID=rt.PolHistTableRowID AND rt.QandB=pcdv.RuleCheckpoint
	INNER JOIN @PremiumMatters pm ON pc.PolAdminMatterID=pm.PolAdminMatterID AND pm.IsAnnual=1
	INNER JOIN @PremiumHistoryRows phr ON pc.PolHistTableRowID=phr.PolHistTableRowID
	WHERE pc.PremiumRowStatus IN (69898,69903)
	GROUP BY pc.PolHistTableRowID, rt.RiskType, rt.QandB

	--SELECT 'Paid/Annual - RiskValues',* FROM @RiskValues	WHERE IsAnnual=1 AND IsWritten=0

	INSERT INTO @CheckpointTotals
	SELECT  1 [IsAnnual]
		, 0	[IsWritten]
		, pc.PolHistTableRowID
		, pcdv.RuleCheckpoint [ColumnName]
		, SUM ( CAST( pcdv.RuleInputOutputDelta AS FLOAT) * ( DATEDIFF(DAY,pc.PremiumRowFromDate,pc.PremiumRowToDate) / CAST(DATEDIFF(day,phr.StartDate,dateadd(YEAR,1,phr.StartDate)) AS FLOAT)  ) )  [CellValue]
		, 0
	FROM @PremiumCalcs pc
	LEFT JOIN PremiumCalculationDetailValues pcdv WITH (NOLOCK) ON pc.PremiumCalculationID=pcdv.PremiumCalculationDetailID
	INNER JOIN @PremiumMatters pm ON pc.PolAdminMatterID=pm.PolAdminMatterID AND pm.IsAnnual=1
	INNER JOIN @PremiumHistoryRows phr ON pc.PolHistTableRowID=phr.PolHistTableRowID
	WHERE  pc.PremiumRowStatus IN (69898,69903)
		AND NOT EXISTS ( SELECT * FROM ResourceListDetailValues bord WITH (NOLOCK) 
							INNER JOIN ResourceListDetailValues rt WITH (NOLOCK) ON bord.ResourceListID=rt.ResourceListID AND rt.DetailFieldID=175714
							WHERE bord.DetailFieldID=175744 
								AND bord.DetailValue=@Bordereau
								AND rt.DetailValue=pcdv.RuleCheckpoint
						)
	GROUP BY pc.PolHistTableRowID, pcdv.RuleCheckpoint

	--SELECT 'Paid/Annual - CheckpointTotals',* FROM @CheckpointTotals ct WHERE ct.IsWritten=0 AND ct.IsAnnual=1
	
	-- 2b. ANNUAL ACCOUNTS - WRITTEN PREMIUMS
	INSERT INTO @RiskValues
	SELECT 1 [IsAnnual]
	, 1 [IsWritten]
	, pc.PolHistTableRowID
	, rt.RiskType
	, rt.QandB
	, CASE WHEN rt.RiskType='VET' 
			THEN SUM ( CAST( pcdv.RuleOutput AS FLOAT) * ( DATEDIFF(DAY,pc.PremiumRowFromDate,pc.PremiumRowToDate) / CAST(DATEDIFF(day,phr.StartDate,dateadd(YEAR,1,phr.StartDate)) AS FLOAT)  ) )
			ELSE SUM ( CAST( pcdv.RuleInputOutputDelta AS FLOAT) * ( DATEDIFF(DAY,pc.PremiumRowFromDate,pc.PremiumRowToDate) / CAST(DATEDIFF(day,phr.StartDate,dateadd(YEAR,1,phr.StartDate)) AS FLOAT)  ) ) 
			END [RiskTypeNet]
	, 0 [CountIncs]	 
	FROM @PremiumCalcs pc
	INNER JOIN PremiumCalculationDetailValues pcdv WITH (NOLOCK) ON pc.PremiumCalculationID=pcdv.PremiumCalculationDetailID
	INNER JOIN @RiskTypes rt ON pc.PolHistTableRowID=rt.PolHistTableRowID AND rt.QandB=pcdv.RuleCheckpoint
	INNER JOIN @PremiumMatters pm ON pc.PolAdminMatterID=pm.PolAdminMatterID AND pm.IsAnnual=1
	INNER JOIN @PremiumHistoryRows phr ON pc.PolHistTableRowID=phr.PolHistTableRowID
	WHERE pc.PremiumRowStatus IN (69898,69903,72154)
	GROUP BY pc.PolHistTableRowID, rt.RiskType, rt.QandB
	
	--SELECT 'Written/Annual - RiskValues',* FROM @RiskValues	WHERE IsAnnual=1 AND IsWritten=1

	INSERT INTO @CheckpointTotals
	SELECT  1 [IsAnnual]
		, 1	[IsWritten]
		, pc.PolHistTableRowID
		, pcdv.RuleCheckpoint [ColumnName]
		, SUM ( CAST( pcdv.RuleInputOutputDelta AS FLOAT) * ( DATEDIFF(DAY,pc.PremiumRowFromDate,pc.PremiumRowToDate) / CAST(DATEDIFF(day,phr.StartDate,dateadd(YEAR,1,phr.StartDate)) AS FLOAT)  ) )  [CellValue]
		, 0
	FROM @PremiumCalcs pc
	LEFT JOIN PremiumCalculationDetailValues pcdv WITH (NOLOCK) ON pc.PremiumCalculationID=pcdv.PremiumCalculationDetailID
	INNER JOIN @PremiumMatters pm ON pc.PolAdminMatterID=pm.PolAdminMatterID AND pm.IsAnnual=1
	INNER JOIN @PremiumHistoryRows phr ON pc.PolHistTableRowID=phr.PolHistTableRowID
	WHERE  pc.PremiumRowStatus IN (69898,69903,72154)
		AND NOT EXISTS ( SELECT * FROM ResourceListDetailValues bord WITH (NOLOCK) 
							INNER JOIN ResourceListDetailValues rt WITH (NOLOCK) ON bord.ResourceListID=rt.ResourceListID AND rt.DetailFieldID=175714
							WHERE bord.DetailFieldID=175744 
								AND bord.DetailValue=@Bordereau
								AND rt.DetailValue=pcdv.RuleCheckpoint
						)
	GROUP BY pc.PolHistTableRowID, pcdv.RuleCheckpoint

	--SELECT 'Written/Annual - CheckpointTotals',* FROM @CheckpointTotals ct WHERE ct.IsWritten=1 AND ct.IsAnnual=1

	-- ANNUAL AND MONTHLY, PAID AND WRITTEN CHECKPOINT TOTALS	

	INSERT INTO @CheckpointRiskTotals
	SELECT  rv.IsAnnual [IsAnnual]
	, rv.IsWritten [IsWritten]
	, 0 [IsTotal]
	, rv.PolHistTableRowID
	, chk.DetailValue
	, rv.RiskType
	, rv.RiskTypeNet
	, 0 
	FROM @RiskValues rv
	INNER JOIN ResourceListDetailValues brt WITH (NOLOCK) ON brt.DetailValue=rv.RiskType AND brt.DetailFieldID=175741
		AND brt.DetailValue=rv.RiskType
	INNER JOIN ResourceListDetailValues chk WITH (NOLOCK) ON chk.ResourceListID=brt.ResourceListID AND chk.DetailFieldID=175742
	INNER JOIN ResourceListDetailValues bord WITH (NOLOCK) ON bord.ResourceListID=brt.ResourceListID AND bord.DetailFieldID=175748
		AND bord.DetailValue=@Bordereau
	INNER JOIN ResourceListDetailValues incl WITH (NOLOCK) ON incl.ResourceListID=brt.ResourceListID AND incl.DetailFieldID=175743
		AND incl.ValueInt=5144
	--AND rv.IsWritten=1

	INSERT INTO @CheckpointRiskTotals
	SELECT crt.IsAnnual [IsAnnual]
	, crt.IsWritten [IsWritten]
	, 1 [IsTotal]
	, crt.PolHistTableRowID,crt.RuleCheckpoint,'',SUM(crt.Total),0
	FROM @CheckpointRiskTotals crt
	WHERE crt.IsTotal=0
	GROUP BY crt.PolHistTableRowID,crt.RuleCheckpoint,crt.IsAnnual,crt.IsWritten
	
	--SELECT 'CheckpointRiskTotals',* FROM @CheckpointRiskTotals crt	

	INSERT INTO @RiskTotals
	SELECT ct.PolHistTableRowID
		, crt.RiskType
		, CASE WHEN ct.IsWritten=1 THEN 'Written' ELSE 'Paid' END + ct.RuleCheckpoint [ColumnName]
		, ct.Total * CASE WHEN crtsum.Total=0 THEN 1 ELSE ( crt.Total / crtsum.Total ) END [CellValue]
		, 0 [NoIncs] 
	FROM @CheckpointTotals ct
	INNER JOIN @CheckpointRiskTotals crt ON ct.PolHistTableRowID=crt.PolHistTableRowID 
				AND ct.RuleCheckpoint=crt.RuleCheckpoint
				AND ct.IsWritten=crt.IsWritten
				AND crt.IsTotal=0
	INNER JOIN @CheckpointRiskTotals crtsum ON ct.PolHistTableRowID=crtsum.PolHistTableRowID 
				AND ct.RuleCheckpoint=crtsum.RuleCheckpoint
				AND ct.IsWritten=crtsum.IsWritten
				AND ct.IsAnnual=crtsum.IsAnnual
				AND crtsum.IsTotal=1

	--SELECT 'RiskTotals',* FROM @RiskTotals rt order by RiskType
	
	/* Finally need to adjust for rounding in the premium and paid values actually transacted with the customer  
	
		1. Lookup the actual annual premium and paid values
		
		2. Calculate the report annual premium and report paid values by summing the RiskTotals
		
		3. Apply the actual/report proportion to each of the RiskTotals values
		
		4. Add a rounding column to the view to sanity check each row. Rounding should be to within £1,
		   since for Alpha annual premiums are rounded to the nearest pound.
		
	*/

	-- part summarised raw actual info
	INSERT INTO @RoundingRaw
	SELECT phr.PolHistTableRowID,pm.IsAnnual, CASE WHEN phrst.ValueInt=43003 THEN 1 ELSE 0 END,
		ISNULL(ppst.ValueInt,69898),
		ISNULL(pppt.ValueInt, CASE WHEN pm.IsAnnual=0 THEN 72054 ELSE 72053 END),
		ISNULL(COUNT(*),12),
		ISNULL(SUM(ppam.ValueMoney),0) 
	FROM @PremiumMatters pm
	INNER JOIN @PremiumHistoryRows phr ON pm.PolAdminMatterID=phr.PolAdminMatterID
	INNER JOIN TableDetailValues phrfd WITH (NOLOCK) ON phr.PolHistTableRowID=phrfd.TableRowID AND phrfd.DetailFieldID=145663
	INNER JOIN TableDetailValues phrtd WITH (NOLOCK) ON phr.PolHistTableRowID=phrtd.TableRowID AND phrtd.DetailFieldID=145664
	INNER JOIN TableDetailValues phrst WITH (NOLOCK) ON phr.PolHistTableRowID=phrst.TableRowID AND phrst.DetailFieldID=145666
	LEFT JOIN TableDetailValues ppfd WITH (NOLOCK) ON phr.PolAdminMatterID=ppfd.MatterID AND  ppfd.DetailFieldID=170076
	LEFT JOIN TableDetailValues pptd WITH (NOLOCK) ON ppfd.TableRowID=pptd.TableRowID AND pptd.DetailFieldID=170077
	LEFT JOIN TableDetailValues ppst WITH (NOLOCK) ON ppfd.TableRowID=ppst.TableRowID AND ppst.DetailFieldID=170078
	LEFT JOIN TableDetailValues pppt WITH (NOLOCK) ON ppfd.TableRowID=pppt.TableRowID AND pppt.DetailFieldID=175383
	LEFT JOIN TableDetailValues ppam WITH (NOLOCK) ON ppfd.TableRowID=ppam.TableRowID AND ppam.DetailFieldID=170072
	WHERE 
	--pm.IsAnnual=0
	--AND 
	ppfd.ValueDate>=phrfd.ValueDate AND pptd.ValueDate<phrtd.ValueDate
	AND ppst.ValueInt IN (69898,  -- Paid
						  69903,  -- Pending
						  72154,  -- Ready
						  69900)  -- Refunded
	GROUP BY phr.PolHistTableRowID,pm.IsAnnual,ppst.ValueInt,pppt.ValueInt,CASE WHEN phrst.ValueInt=43003 THEN 1 ELSE 0 END

	-- add rows for monthly payments yet to be taken (Status=1000)
	INSERT INTO @RoundingRaw
	SELECT rr.PolHistTableRowID,rr.IsAnnual,0,1000,1001,12-SUM(rr.Num),0
	FROM @RoundingRaw rr
	WHERE rr.IsAnnual=0 --AND rr.Status IN (72154,69903) --(69898,69903)--
	AND rr.PayType <> 72140 -- not a paid adjustment
	AND rr.IsCancelled = 0
	GROUP BY rr.PolHistTableRowID,rr.IsAnnual

--69898	Paid
--69899	Reversed
--69900	Refunded
--69901	Waived
--69902	Partially refunded
--69903	Pending
--69904	Refund
--69940	Collection Failed
--72139	Payment returned
--72154	Ready	

	-- calculate the total for monthly payment yet to be taken
	UPDATE rr
	SET Total= CASE WHEN rr.Num=12 THEN pdhfm.ValueMoney + ( pdhom.ValueMoney * 11) ELSE pdhom.ValueMoney * rr.Num END
	FROM @RoundingRaw rr
	--INNER JOIN @PremiumHistoryRows phr ON rr.PolHistTableRowID=phr.PolHistTableRowID
	INNER JOIN TableDetailValues phrfd WITH (NOLOCK) ON rr.PolHistTableRowID=phrfd.TableRowID AND phrfd.DetailFieldID=145663
	INNER JOIN TableDetailValues pdhfd WITH (NOLOCK) ON pdhfd.MatterID=phrfd.MatterID AND pdhfd.DetailFieldID=175346
	INNER JOIN TableDetailValues pdhst WITH (NOLOCK) ON pdhfd.TableRowID=pdhst.TableRowID AND pdhst.DetailFieldID=175722
	INNER JOIN TableDetailValues pdhfm WITH (NOLOCK) ON pdhfd.TableRowID=pdhfm.TableRowID AND pdhfm.DetailFieldID=175350
	INNER JOIN TableDetailValues pdhom WITH (NOLOCK) ON pdhfd.TableRowID=pdhom.TableRowID AND pdhom.DetailFieldID=175351
	WHERE pdhst.ValueInt IN (72326) -- live
	AND rr.Status=1000
	AND rr.Num > 0 

	-- load the actual part of the @Rounding table

	-- paid, not cancelled
	INSERT INTO @Rounding ( PolHistTableRowID, ActualPaid, IsAnnual )
	SELECT rr.PolHistTableRowID, SUM(rr.Total), rr.IsAnnual
	FROM @RoundingRaw rr
	WHERE rr.Status IN (69898,  -- Paid
						69903,  -- Pending
						69900)  -- refunded
	AND rr.IsCancelled=0
	GROUP BY rr.PolHistTableRowID, rr.IsAnnual
	
	-- no payments yet, not cancelled
	INSERT INTO @Rounding ( PolHistTableRowID, ActualPaid, IsAnnual )
	SELECT rr.PolHistTableRowID, 0, rr.IsAnnual
	FROM @RoundingRaw rr
	WHERE rr.Status IN (72154)  -- ready
	AND rr.IsCancelled=0
	AND NOT EXISTS ( SELECT * FROM @Rounding r WHERE rr.PolHistTableRowID=r.PolHistTableRowID)
	GROUP BY rr.PolHistTableRowID, rr.IsAnnual
	
	-- paid & written cancelled
	INSERT INTO @Rounding ( PolHistTableRowID, ActualPaid, ActualWritten, IsAnnual )
	SELECT rr.PolHistTableRowID, SUM(rr.Total), SUM(rr.Total), rr.IsAnnual
	FROM @RoundingRaw rr
	WHERE rr.Status IN (69898,  -- Paid
						69903,  -- Pending
						69900)  -- refunded
	AND rr.IsCancelled=1
	GROUP BY rr.PolHistTableRowID, rr.IsAnnual
	
	-- written, not cancelled, annual
	UPDATE r
	SET ActualWritten=(
			SELECT SUM(rr.Total)
			FROM @RoundingRaw rr
			WHERE rr.Status IN (69898,  -- Paid
								69903,  -- Pending
								72154,  -- Ready
								69900)  -- refunded
			AND rr.IsAnnual=1 AND rr.IsCancelled=0 AND r.PolHistTableRowID=rr.PolHistTableRowID
			GROUP BY rr.PolHistTableRowID )
	FROM @Rounding r 
	WHERE r.IsAnnual=1
	
	-- written, not cancelled, monthly
	UPDATE r
	SET ActualWritten=(
			SELECT SUM(rr.Total)
			FROM @RoundingRaw rr
			WHERE rr.Status IN (69898,  -- Paid
								69903,  -- Pending
								72154,  -- Ready
								1000,   -- to be collected
								69900)  -- refunded
			AND rr.IsAnnual=0 AND rr.IsCancelled=0 AND r.PolHistTableRowID=rr.PolHistTableRowID
			GROUP BY rr.PolHistTableRowID )
	FROM @Rounding r 
	WHERE r.IsAnnual=0

	-- load the report part of the @Rounding table
	
	-- written, net
	UPDATE r
	SET ReportWritten=ISNULL((
			SELECT SUM(rv.RiskTypeNet)
			FROM @RiskValues rv
			WHERE rv.IsWritten=1
			AND r.PolHistTableRowID=rv.PolHistTableRowID
			GROUP BY rv.PolHistTableRowID ),0)
	FROM @Rounding r
	-- written, checkpoints
	UPDATE r
	SET ReportWritten=ReportWritten+ISNULL((
			SELECT SUM(rt.CellValue)
			FROM @RiskTotals rt
			INNER JOIN ResourceListDetailValues chk WITH (NOLOCK) ON rt.ColumnName = 'Written' + chk.DetailValue AND chk.DetailFieldID=176172
			INNER JOIN ResourceListDetailValues bord WITH (NOLOCK) ON chk.ResourceListID=bord.ResourceListID AND bord.DetailFieldID=176171
			WHERE r.PolHistTableRowID=rt.PolHistTableRowID AND bord.DetailValue=@Bordereau
			GROUP BY rt.PolHistTableRowID ),0)
	FROM @Rounding r
	
	-- paid, net
	UPDATE r
	SET ReportPaid=ISNULL((
			SELECT SUM(rv.RiskTypeNet)
			FROM @RiskValues rv
			WHERE rv.IsWritten=0
			AND r.PolHistTableRowID=rv.PolHistTableRowID
			GROUP BY rv.PolHistTableRowID ),0)
	FROM @Rounding r
	-- paid, checkpoints
	UPDATE r
	SET ReportPaid=ReportPaid+ISNULL((
			SELECT SUM(rt.CellValue)
			FROM @RiskTotals rt
			INNER JOIN ResourceListDetailValues chk WITH (NOLOCK) ON rt.ColumnName = 'Paid' + chk.DetailValue AND chk.DetailFieldID=176172
			INNER JOIN ResourceListDetailValues bord WITH (NOLOCK) ON chk.ResourceListID=bord.ResourceListID AND bord.DetailFieldID=176171
			WHERE r.PolHistTableRowID=rt.PolHistTableRowID AND bord.DetailValue=@Bordereau
			GROUP BY rt.PolHistTableRowID ),0)
	FROM @Rounding r

	-- Now update the Risk Values and Risk Totals to account for the rounding
	UPDATE rv
	SET RiskTypeNet=CAST(RiskTypeNet * ( CASE WHEN rv.IsWritten = 1 THEN CASE WHEN r.ReportWritten=0 THEN 1 ELSE r.ActualWritten/r.ReportWritten END
																ELSE CASE WHEN r.ReportPaid=0 THEN 1 ELSE r.ActualPaid/r.ReportPaid END END ) AS MONEY)
	FROM @RiskValues rv
	INNER JOIN @Rounding r ON rv.PolHistTableRowID=r.PolHistTableRowID	
	
	UPDATE rt
	SET CellValue=CAST(CellValue * ( CASE WHEN rt.ColumnName LIKE 'Written%' THEN CASE WHEN r.ReportWritten=0 THEN 1 ELSE r.ActualWritten/r.ReportWritten END
																ELSE CASE WHEN r.ReportPaid=0 THEN 1 ELSE r.ActualPaid/r.ReportPaid END END ) AS MONEY)
	FROM @RiskTotals rt
	INNER JOIN @Rounding r ON rt.PolHistTableRowID=r.PolHistTableRowID	

	-- last step in the rounding. Absorb the rounding error rounding error into the VET Risk - BasePremiumNet cell.
	-- This is so that the sum of all of the components exactly equals the actual written & actual paid.
	-- written, net
	UPDATE r
	SET FinalReportWritten=ISNULL((
			SELECT SUM(rv.RiskTypeNet)
			FROM @RiskValues rv
			WHERE rv.IsWritten=1
			AND r.PolHistTableRowID=rv.PolHistTableRowID
			GROUP BY rv.PolHistTableRowID ),0)
	FROM @Rounding r
	-- written, checkpoints
	UPDATE r
	SET FinalReportWritten=FinalReportWritten+ISNULL((
			SELECT SUM(rt.CellValue)
			FROM @RiskTotals rt
			INNER JOIN ResourceListDetailValues chk WITH (NOLOCK) ON rt.ColumnName = 'Written' + chk.DetailValue AND chk.DetailFieldID=176172
			INNER JOIN ResourceListDetailValues bord WITH (NOLOCK) ON chk.ResourceListID=bord.ResourceListID AND bord.DetailFieldID=176171
			WHERE r.PolHistTableRowID=rt.PolHistTableRowID AND bord.DetailValue=@Bordereau
			GROUP BY rt.PolHistTableRowID ),0)
	FROM @Rounding r
	
	-- paid, net
	UPDATE r
	SET FinalReportPaid=ISNULL((
			SELECT SUM(rv.RiskTypeNet)
			FROM @RiskValues rv
			WHERE rv.IsWritten=0
			AND r.PolHistTableRowID=rv.PolHistTableRowID
			GROUP BY rv.PolHistTableRowID ),0)
	FROM @Rounding r
	-- paid, checkpoints
	UPDATE r
	SET FinalReportPaid=FinalReportPaid+ISNULL((
			SELECT SUM(rt.CellValue)
			FROM @RiskTotals rt
			INNER JOIN ResourceListDetailValues chk WITH (NOLOCK) ON rt.ColumnName = 'Paid' + chk.DetailValue AND chk.DetailFieldID=176172
			INNER JOIN ResourceListDetailValues bord WITH (NOLOCK) ON chk.ResourceListID=bord.ResourceListID AND bord.DetailFieldID=176171
			WHERE r.PolHistTableRowID=rt.PolHistTableRowID AND bord.DetailValue=@Bordereau
			GROUP BY rt.PolHistTableRowID ),0)
	FROM @Rounding r

	-- we are going to absorb the rounding error into the base premium of the non-optional risk type
	DECLARE @NonOptRT VARCHAR(10)
	SELECT TOP 1 @NonOptRT=rt.DetailValue FROM ResourceListDetailValues isOpt WITH (NOLOCK) 
	INNER JOIN ResourceListDetailValues rt WITH (NOLOCK) ON isOpt.ResourceListID=rt.ResourceListID AND rt.DetailFieldID=175715
	WHERE isOpt.DetailFieldID=175754 
	AND isOpt.ValueInt=5145
	
	UPDATE rv
	SET RiskTypeNet=CAST(RiskTypeNet + ( CASE WHEN rv.IsWritten = 1 THEN r.ActualWritten - r.FinalReportWritten 
																ELSE r.ActualPaid - r.FinalReportPaid END ) AS MONEY)
	FROM @RiskValues rv
	INNER JOIN @Rounding r ON rv.PolHistTableRowID=r.PolHistTableRowID
	WHERE rv.RiskType=@NonOptRT	

	-- finally add entries for 
	
	/* OTHER NON-FINANCIAL INFORMATION */
	
	
	INSERT INTO @StatusInfo
	SELECT pm.PolAdminMatterID, mistat.DetailValue [Status], '#' + CONVERT(VARCHAR(10),stat.WhenSaved,120) + '#' [StatusDate],
			ROW_NUMBER() OVER(PARTITION BY pm.PolAdminMatterID ORDER BY stat.WhenSaved DESC) as [RNO]
	FROM @PremiumMatters pm
	INNER JOIN DetailValueHistory stat ON pm.PolAdminMatterID=stat.MatterID AND stat.DetailFieldID=170038
	INNER JOIN ResourceListDetailValues aqstat WITH (NOLOCK) ON stat.ValueInt=aqstat.ValueInt AND aqstat.DetailFieldID=175718
	INNER JOIN ResourceListDetailValues mistat WITH (NOLOCK) ON aqstat.ResourceListID=mistat.ResourceListID AND mistat.DetailFieldID=175719
	
	--SELECT '@StatusInfo',* FROM @StatusInfo
	
	INSERT INTO @RiskSchemeMatters
	SELECT phr.PolAdminMatterID, schemeScheme.MatterID [SchemeMatterID], phr.PolHistTableRowID
	FROM @PremiumHistoryRows phr 
	INNER JOIN MatterDetailValues polScheme WITH (NOLOCK) ON phr.PolAdminMatterID=polScheme.MatterID AND polScheme.DetailFieldID=170034
	INNER JOIN MatterDetailValues schemeScheme WITH (NOLOCK) ON polScheme.ValueInt=schemeScheme.ValueInt AND schemeScheme.DetailFieldID=145689
		
	--SELECT '@RiskSchemeMatters',* FROM @RiskSchemeMatters
	
	-- Limits may come from the Policy Limits table (if mandatory) or the Optional Coverage Overrides table (if optional)
	INSERT INTO @PolicyLimits
	SELECT rt.PolHistTableRowID
		,rt.RiskType
		,MAX(ISNULL(pollimsi.ValueMoney,0.00)) [ValueMoney]
	FROM @RiskSchemeMatters rsm
	--INNER JOIN @PremiumCalcs pc ON rsm.PolAdminMatterID=pc.PolAdminMatterID
	INNER JOIN @RiskTypes rt ON rsm.PolHistTableRowID=rt.PolHistTableRowID
	INNER JOIN ResourceListDetailValues brt WITH (NOLOCK) ON brt.DetailValue=rt.RiskType AND brt.DetailFieldID=175715
	INNER JOIN ResourceListDetailValues bb WITH (NOLOCK) ON bb.ResourceListID=brt.ResourceListID AND bb.DetailFieldID=175744 AND bb.DetailValue=@Bordereau
	INNER JOIN ResourceListDetailValues bopt WITH (NOLOCK) ON bopt.ResourceListID=brt.ResourceListID AND bopt.DetailFieldID=175754 AND bopt.ValueInt=5145
	LEFT JOIN  TableRows pollim WITH (NOLOCK) ON rsm.SchemeMatterID=pollim.MatterID AND pollim.DetailFieldID=145692
	LEFT JOIN TableDetailValues pollimsi WITH (NOLOCK) ON pollim.TableRowID=pollimsi.TableRowID AND pollimsi.DetailFieldID=144358
	LEFT JOIN TableDetailValues pollimps WITH (NOLOCK) ON pollim.TableRowID=pollimps.TableRowID AND pollimps.DetailFieldID=144357
	LEFT JOIN ResourceListDetailValues pollimpsps WITH (NOLOCK) ON pollimps.ResourceListID=pollimpsps.ResourceListID AND pollimpsps.DetailFieldID=146189
	LEFT JOIN LookupListItems pollimpspsli WITH (NOLOCK) ON pollimpsps.ValueInt=pollimpspsli.LookupListItemID
	WHERE ( pollimpspsli.ItemValue=rt.AquariumTableText OR pollimpspsli.ItemValue IS NULL )
	--AND pc.RNO=1
	GROUP BY rt.PolHistTableRowID,rt.RiskType
	INSERT INTO @PolicyLimits
	SELECT rt.PolHistTableRowID
		,rt.RiskType
		,MAX(ISNULL(pollimsi.ValueMoney,0.00)) [ValueMoney]
	FROM @RiskSchemeMatters rsm
	--INNER JOIN @PremiumCalcs pc ON rsm.PolAdminMatterID=pc.PolAdminMatterID
	INNER JOIN @RiskTypes rt ON rsm.PolHistTableRowID=rt.PolHistTableRowID
	INNER JOIN ResourceListDetailValues brt WITH (NOLOCK) ON brt.DetailValue=rt.RiskType AND brt.DetailFieldID=175715
	INNER JOIN ResourceListDetailValues bb WITH (NOLOCK) ON bb.ResourceListID=brt.ResourceListID AND bb.DetailFieldID=175744 AND bb.DetailValue=@Bordereau
	INNER JOIN ResourceListDetailValues bopt WITH (NOLOCK) ON bopt.ResourceListID=brt.ResourceListID AND bopt.DetailFieldID=175754 AND bopt.ValueInt=5144
	LEFT JOIN TableRows pollim WITH (NOLOCK) ON rsm.SchemeMatterID=pollim.MatterID AND pollim.DetailFieldID=175736
	LEFT JOIN TableDetailValues pollimsi WITH (NOLOCK) ON pollim.TableRowID=pollimsi.TableRowID AND pollimsi.DetailFieldID=175734
	LEFT JOIN TableDetailValues pollimcg WITH (NOLOCK) ON pollim.TableRowID=pollimcg.TableRowID AND pollimcg.DetailFieldID=175735
	LEFT JOIN LookupListItems pollimpspsli WITH (NOLOCK) ON pollimcg.ValueInt=pollimpspsli.LookupListItemID
	WHERE ( pollimpspsli.ItemValue=rt.AquariumTableText OR pollimpspsli.ItemValue IS NULL )
	--AND pc.RNO=1
	GROUP BY rt.PolHistTableRowID,rt.RiskType
	
	--SELECT '@PolicyLimits',* FROM @PolicyLimits	

	INSERT INTO @RiskFields
	SELECT phr.PolHistTableRowID, 
			rt.RiskType,
			CASE rt.RiskType 
				WHEN 'VET' THEN 1-(coins.ValueMoney/100)
				ELSE 1
				END [CoIns],			 
			pl.ValueMoney [Limit],
			volxs.ValueMoney [volXS],
			CASE rt.RiskType 
				WHEN 'TPL' THEN tplxs.ValueMoney
				WHEN 'VET' THEN comxs.ValueMoney
				ELSE 0.00
				END [comXS]			 
	FROM @PremiumHistoryRows phr
	INNER JOIN @RiskTypes rt ON phr.PolHistTableRowID=rt.PolHistTableRowID
	INNER JOIN @PolicyLimits pl ON rt.PolHistTableRowID=pl.PolHistTableRowID AND rt.RiskType=pl.RiskType
	INNER JOIN MatterDetailValues comxs WITH (NOLOCK) ON phr.PolAdminMatterID=comxs.MatterID AND comxs.DetailFieldID=175503
	INNER JOIN MatterDetailValues tplxs WITH (NOLOCK) ON phr.PolAdminMatterID=tplxs.MatterID AND tplxs.DetailFieldID=170051
	INNER JOIN MatterDetailValues coins WITH (NOLOCK) ON phr.PolAdminMatterID=coins.MatterID AND coins.DetailFieldID=175504
	INNER JOIN MatterDetailValues volxs WITH (NOLOCK) ON phr.PolAdminMatterID=volxs.MatterID AND volxs.DetailFieldID=170039
	--WHERE pc.RNO=1
	
	--SELECT '@RiskFields',* FROM @RiskFields

	/* CREATE THE BORDEREAU RECORDS */
	
	INSERT @Data 
	SELECT 
	[AffinityShortCode],
	[CustomerID],
	[PolAdminLeadID],
	[PolAdminMatterID],
	[PolHistoryTableRowID],
	[StartDate],
	[EndDate],
	[SystemKey],
	[AlphaReference],
	[PolicyReference],
	[Inception],
	[Expiry],
	[RiskTypeCode],
	[Currency],
	[InsuredReference],
	[InsuredName],
	[InsuredCountry],
	[InsuredPostalCode],
	[InsuredAddress],
	[InsuredEmailAddress],
	[InsuredTelNoHome],
	[InsuredTelNoMobile],
	[UnderwriterID],
	[OriginalInception],
	[Type of Business],
	[Status],
	[StatusDate],
	[Share],
	[LimitCurrency],
	[SumInsured],
	[DeductibleSum],
	[DeductibleCompulsoryExcess],
	[DeductibleVoluntaryExcess],
	[NoOfInstalments],
	[Affinity],
	[Product],
	[PetType],
	[Breed],
	[BreedCode],
	[PetSex],
	[PetDateOfBirth],
	[Neutered?],
	[PurchasePrice],
	[DiscountCode],
	[AnnualRounding],
	ISNULL([WrittenBasePremiumNet],0.00),
	ISNULL([WrittenBinderCommission],0.00),
	ISNULL([WrittenOriginalBrokerage],0.00),
	ISNULL([WrittenBroker],0.00),
	ISNULL([WrittenAffinity],0.00),
	ISNULL([WrittenCompany],0.00),
	ISNULL([WrittenDiscount],0.00),
	ISNULL([WrittenDiscount-MultiPet],0.00),
	ISNULL([WrittenDiscount-MarketingCode],0.00),
	ISNULL([WrittenIPT],0.00),
	ISNULL([WrittenPAF],0.00),
	ISNULL([PaidBasePremiumNet],0.00),
	ISNULL([PaidBinderCommission],0.00),
	ISNULL([PaidOriginalBrokerage],0.00),
	ISNULL([PaidBroker],0.00),
	ISNULL([PaidAffinity],0.00),
	ISNULL([PaidCompany],0.00),
	ISNULL([PaidDiscount],0.00),
	ISNULL([PaidDiscount-MultiPet],0.00),
	ISNULL([PaidDiscount-MarketingCode],0.00),
	ISNULL([PaidIPT],0.00),
	ISNULL([PaidPAF],0.00)
	FROM (
	
		SELECT
		affSC.ValueInt																	[AffinityShortCode],
		c.CustomerID																	[CustomerID],
		l.LeadID																		[PolAdminLeadID],
		pam.MatterID																	[PolAdminMatterID],
		phr.PolHistTableRowID															[PolHistoryTableRowID]
		,
		phr.StartDate																	[StartDate],
		phr.EndDate																		[EndDate],
		CAST(pam.MatterID AS VARCHAR) + '/' 
			+ CAST(hp.TableRowID AS VARCHAR) + '/' 
			+ rt.RiskType																[SystemKey],
		'AA110121TIS-MHE'																[AlphaReference],
		pn.DetailValue																	[PolicyReference],
		'#' + CONVERT(VARCHAR(10),phr.StartDate,120) + '#'								[Inception],
		'#' + CONVERT(VARCHAR(10),phr.EndDate,120) + '#'								[Expiry],
		rt.RiskTypeCode																	[RiskTypeCode],
		'GBP'																			[Currency],
		c.CustomerID																	[InsuredReference],
		c.LastName + '/' + c.FirstName													[InsuredName],
		'UK'																			[InsuredCountry],
		UPPER(c.PostCode)																[InsuredPostalCode],
		REPLACE(REPLACE(c.Address1,', ','/'),',','/') 
			+ CASE WHEN c.Address2 <> '' THEN '/' + REPLACE(REPLACE(c.Address2,', ','/'),',','/') ELSE '' END 
			+ CASE WHEN c.Town <> '' THEN '/' + REPLACE(REPLACE(c.Town,', ','/'),',','/') ELSE '' END 
			+ CASE WHEN c.County <> '' THEN '/' + REPLACE(REPLACE(c.County,', ','/'),',','/') ELSE '' END
																						[InsuredAddress],
		CASE WHEN c.EmailAddress <> '' THEN c.EmailAddress ELSE 'NA' END                [InsuredEmailAddress],
		CASE WHEN NOT (c.HomeTelephone = '' OR c.HomeTelephone IS NULL) 
			THEN REPLACE(c.HomeTelephone,' ','') ELSE 'NA' END			                [InsuredTelNoHome],
		CASE WHEN NOT (c.MobileTelephone = '' OR c.MobileTelephone IS NULL) 
			THEN REPLACE(c.MobileTelephone,' ','') ELSE 'NA' END		                [InsuredTelNoMobile],
		uwli.ItemValue																	[UnderwriterID],
		'#' + inc.DetailValue + '#'														[OriginalInception],
		'Direct'																		[Type of Business],
		si.Status																		[Status],
		si.StatusDate																	[StatusDate],
		rf.CoIns																		[Share],
		'GBP'																			[LimitCurrency],
		rf.Limit																		[SumInsured],
		rf.volXS+rf.comXS																[DeductibleSum],
		rf.comXS																		[DeductibleCompulsoryExcess],
		rf.volXS																		[DeductibleVoluntaryExcess],
		CASE WHEN payint.ValueInt=69944 THEN 1 ELSE 12 END								[NoOfInstalments],
		saff.DetailValue																[Affinity],
		prod.DetailValue																[Product],
		ISNULL(UPPER(ptypeli.ItemValue),'')												[PetType],
		pbreed.DetailValue																[Breed],
		pgroup.DetailValue																[BreedCode],
		ISNULL(psexli.ItemValue,'')														[PetSex],
		'#' + LEFT(pDOB.DetailValue,10) + '#'											[PetDateOfBirth],
		ISNULL(pneutli.ItemValue,'')													[Neutered?],
		CASE WHEN ISNULL(ppp.ValueMoney,0)=0 THEN '' ELSE CAST(ppp.ValueMoney AS VARCHAR) END	[PurchasePrice],
		mc.DetailValue																	[DiscountCode],
		ROUND(r.ActualWritten-r.ReportWritten,2)										[AnnualRounding],
		ISNULL(wprv.RiskTypeNet,0.00)													[WrittenBasePremiumNet],
		ISNULL(pprv.RiskTypeNet,0.00)													[PaidBasePremiumNet],
		rtot.CellValue,
		rtot.ColumnName,
		rtot.RiskType
		FROM Customers c WITH (NOLOCK) 
		INNER JOIN CustomerDetailValues aff WITH (NOLOCK) ON c.CustomerID=aff.CustomerID AND aff.DetailFieldID=170144
		INNER JOIN ResourceListDetailValues affSC WITH (NOLOCK) ON aff.ValueInt=affSC.ResourceListID AND affSC.DetailFieldID=170127
		INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
		INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
		INNER JOIN Matter pam WITH (NOLOCK) ON ca.CaseID=pam.CaseID
		INNER JOIN @PremiumMatters pm ON pam.MatterID=pm.PolAdminMatterID
		INNER JOIN MatterDetailValues pn WITH (NOLOCK) ON pam.MatterID=pn.MatterID AND pn.DetailFieldID=170050
		LEFT JOIN MatterDetailValues mc WITH (NOLOCK) ON pam.MatterID=mc.MatterID AND mc.DetailFieldID=175488
		INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.FromMatterID=pam.MatterID AND ltr.ToLeadTypeID=1493
		INNER JOIN MatterDetailValues payint WITH (NOLOCK) ON ltr.ToMatterID=payint.MatterID AND payint.DetailFieldID=170145
		INNER JOIN TableRows hp WITH (NOLOCK) ON pam.MatterID=hp.MatterID AND hp.DetailFieldID=170033
		LEFT JOIN TableDetailValues inc WITH (NOLOCK) ON hp.TableRowID=inc.TableRowID AND inc.DetailFieldID=145662
		LEFT JOIN TableDetailValues pol WITH (NOLOCK) ON hp.TableRowID=pol.TableRowID AND pol.DetailFieldID=145665
		LEFT JOIN ResourceListDetailValues uw WITH (NOLOCK) ON pol.ResourceListID=uw.ResourceListID AND uw.DetailFieldID=145661
		LEFT JOIN LookupListItems uwli WITH (NOLOCK) ON uw.ValueInt=uwli.LookupListItemID
		LEFT JOIN ResourceListDetailValues prod WITH (NOLOCK) ON pol.ResourceListID=prod.ResourceListID AND prod.DetailFieldID=146200
		LEFT JOIN ResourceListDetailValues saff WITH (NOLOCK) ON pol.ResourceListID=saff.ResourceListID AND saff.DetailFieldID=144314
		-- pet details
		INNER JOIN LeadDetailValues pt WITH (NOLOCK) ON l.LeadID=pt.LeadID AND pt.DetailFieldID=144272
		INNER JOIN ResourceListDetailValues ptype WITH (NOLOCK) ON pt.ValueInt=ptype.ResourceListID AND ptype.DetailFieldID=144269
		LEFT JOIN LookupListItems ptypeli WITH (NOLOCK) ON ptype.ValueInt=ptypeli.LookupListItemID
		INNER JOIN ResourceListDetailValues pbreed WITH (NOLOCK) ON pt.ValueInt=pbreed.ResourceListID AND pbreed.DetailFieldID=144270
		INNER JOIN ResourceListDetailValues pgroup WITH (NOLOCK) ON pt.ValueInt=pgroup.ResourceListID AND pgroup.DetailFieldID=170012
		INNER JOIN ResourceListDetailValues pbl WITH (NOLOCK) ON pt.ValueInt=pbl.ResourceListID AND pbl.DetailFieldID=170010 -- breed list, must be breed list used by the customer's affinity
		INNER JOIN LeadDetailValues psex WITH (NOLOCK) ON l.LeadID=psex.LeadID AND psex.DetailFieldID=144275
		LEFT JOIN LookupListItems psexli WITH (NOLOCK) ON psex.ValueInt=psexli.LookupListItemID
		INNER JOIN LeadDetailValues pDOB WITH (NOLOCK) ON l.LeadID=pDOB.LeadID AND pDOB.DetailFieldID=144274
		INNER JOIN LeadDetailValues pneut WITH (NOLOCK) ON l.LeadID=pneut.LeadID AND pneut.DetailFieldID=152783
		LEFT JOIN LookupListItems pneutli WITH (NOLOCK) ON pneut.ValueInt=pneutli.LookupListItemID
		LEFT JOIN LeadDetailValues ppp WITH (NOLOCK) ON l.LeadID=ppp.LeadID AND ppp.DetailFieldID=144339
		INNER JOIN @PremiumHistoryRows phr ON pam.MatterID=phr.PolAdminMatterID
		INNER JOIN @RiskTypes rt ON phr.PolHistTableRowID=rt.PolHistTableRowID
		INNER JOIN @StatusInfo si ON si.PolAdminMatterID=phr.PolAdminMatterID AND si.RNO=1
		INNER JOIN @RiskFields rf ON phr.PolHistTableRowID=rf.PolHistTableRowID AND rt.RiskType=rf.RiskType
		INNER JOIN @RiskValues wprv ON phr.PolHistTableRowID=wprv.PolHistTableRowID AND rt.RiskType=wprv.RiskType AND wprv.IsWritten=1
		LEFT JOIN @RiskValues pprv ON pprv.PolHistTableRowID=phr.PolHistTableRowID AND rt.RiskType=pprv.RiskType AND pprv.IsWritten=0
		LEFT JOIN @RiskTotals rtot ON rtot.PolHistTableRowID=phr.PolHistTableRowID AND rtot.RiskType=rt.RiskType
		INNER JOIN @Rounding r ON phr.PolHistTableRowID=r.PolHistTableRowID
		WHERE
		-- not test
		c.Test=0
		-- link policy info to particular history row
		AND hp.TableRowID=phr.PolHistTableRowID
	) AS s 
	PIVOT
	(
		SUM([CellValue])
		FOR [ColumnName] IN ([WrittenBinderCommission],[WrittenOriginalBrokerage],[WrittenBroker],[WrittenAffinity],[WrittenCompany],
							[WrittenDiscount],[WrittenDiscount-MultiPet],[WrittenDiscount-MarketingCode],
							[WrittenIPT],[WrittenPAF],
							[PaidBinderCommission],[PaidOriginalBrokerage],[PaidBroker],[PaidAffinity],[PaidCompany],
							[PaidDiscount],[PaidDiscount-MultiPet],[PaidDiscount-MarketingCode],
							[PaidIPT],[PaidPAF])
	) AS p
	ORDER BY PolAdminMatterID, PolHistoryTableRowID--, RiskType DESC

--select * from @Data	
	RETURN
 
END

-- FOR DEBUG:
--SELECT br.PolHistoryTableRowID, 
--SUM(br.PaidBasePremiumNet + br.PaidBinderCommission + br.PaidOriginalBrokerage + br.PaidDiscountMarketingCode + br.PaidDiscountMultiPet + br.PaidPolicyFee + br.PaidIPT) [Paid], 
--SUM(br.WrittenBasePremiumNet + br.WrittenBinderCommission + br.WrittenOriginalBrokerage + br.WrittenDiscountMarketingCode + br.WrittenDiscountMultiPet + br.WrittenPolicyFee + br.WrittenIPT) [Written],
--SUM(br.WrittenBasePremiumNet + br.WrittenOriginalBrokerage + br.WrittenDiscountMarketingCode + br.WrittenDiscountMultiPet + br.WrittenPolicyFee + br.WrittenIPT) [test no rounding]
--FROM @Data br
----where br.PolHistoryTableRowID=21863787
--group by br.PolHistoryTableRowID
--order by br.PolHistoryTableRowID
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C384_RiskBordereau] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C384_RiskBordereau] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C384_RiskBordereau] TO [sp_executeall]
GO
