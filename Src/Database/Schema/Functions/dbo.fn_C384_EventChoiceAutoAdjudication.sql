SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Robin Hall
-- Create date: 2014-12-05
-- Description:	Auto-adjudication for event choices
-- =============================================
CREATE FUNCTION [dbo].[fn_C384_EventChoiceAutoAdjudication]
(
	@ToEventTypeID INT
	,@CaseID INT
)
RETURNS BIT	
AS
BEGIN
	
	DECLARE 
		@Result BIT
		,@CustomerID INT
		,@LeadID INT
		,@MatterID INT
	
	-- Get other IDs
	SELECT TOP 1 @CustomerID = CustomerID, @LeadID = LeadID, @MatterID = MatterID
	FROM Matter 
	WHERE CaseID = @CaseID	
	
	/* EventType specific adjudication */	
	
	-- Rejection letter by email VET
	IF @ToEventTypeID = 155279 
	BEGIN
		
		SELECT @Result = 0 -- Default no
	
		DECLARE 
			@RejectedPayee INT
		
		SELECT @RejectedPayee = ValueInt
		FROM MatterDetailValues 
		WHERE DetailfieldID = 144521
		AND MatterID = @MatterID

		IF @RejectedPayee IN (66953, 66954) -- Rejected payee = Vet or Referral vet
		BEGIN
			
			-- Get vet or referral vet email address and check (approximate) validity
			SELECT 
				@Result = CASE WHEN (rldvVetEmail.DetailValue LIKE '%@%.%') THEN 1 ELSE 0 END
			FROM 
				Matter m WITH (NOLOCK)
				INNER JOIN dbo.LeadTypeRelationship ltr WITH (NOLOCK) ON ltr.FromLeadTypeID = 1492 AND ltr.toleadtypeid = 1490 AND ltr.ToLeadID = m.LeadID
				LEFT JOIN dbo.LeadDetailValues ldvCurVet WITH (NOLOCK) ON ldvCurVet.LeadID = ltr.FromLeadID AND ldvCurVet.DetailFieldID = 146215
				LEFT JOIN dbo.MatterDetailValues mdvRefVet WITH (NOLOCK) ON mdvRefVet.MatterID = m.MatterID AND mdvRefVet.DetailFieldID = 146236
				LEFT JOIN dbo.ResourceListDetailValues rldvVetEmail WITH (NOLOCK) ON rldvVetEmail.DetailFieldID = 144482
					AND rldvVetEmail.ResourceListID = CASE @RejectedPayee WHEN 66953 THEN ldvCurVet.ValueInt WHEN 66954 THEN mdvRefVet.ValueInt END
			WHERE 
				m.MatterID = @MatterID
			
		END
		
		RETURN @Result
		
	END

	RETURN 1

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C384_EventChoiceAutoAdjudication] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C384_EventChoiceAutoAdjudication] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C384_EventChoiceAutoAdjudication] TO [sp_executeall]
GO
