SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2012-07-03
-- Description:	Get LeadDocument records with blobs from all relevant databases
-- =============================================
CREATE FUNCTION [dbo].[fnGetLeadDocumentList] 
(	
	@LeadID int
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT
		ld.LeadDocumentID,
		ld.ClientID,
		ld.LeadID,
		ld.DocumentTypeID,
		ld.LeadDocumentTitle,
		ld.UploadDateTime,
		ld.WhoUploaded,
		COALESCE(fs.DocumentBLOB, CAST(0x AS VARBINARY)) AS DocumentBLOB,
		ld.FileName,
		fs.EmailBLOB,
		ld.DocumentFormat,
		ld.EmailFrom,
		ld.EmailTo,
		ld.CcList,
		ld.BccList,
		ld.ElectronicSignatureDocumentKey,
		ld.Encoding,
		ld.ContentFormat,
		ld.ZipFormat,
		ld.DocumentBlobSize, 
		ld.EmailBlobSize,
		ld.DocumentDatabaseID,
		ld.WhenArchived,
		ld.DocumentTypeVersionID
	FROM dbo.LeadDocument ld WITH (NOLOCK) 
	INNER JOIN dbo.LeadDocumentFS fs WITH (NOLOCK) ON fs.LeadDocumentID = ld.LeadDocumentID 

)









GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLeadDocumentList] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnGetLeadDocumentList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLeadDocumentList] TO [sp_executeall]
GO
