SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2008-01-23
-- Description:	Get the previous week-ending date
-- =============================================
CREATE FUNCTION [dbo].[fnGetLastWorkWeekEndingChardate]
(
	@startdow tinyint
)
RETURNS char(10)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @weekending datetime, @adjustment smallint

	-- If the week starts on a Monday (day 2) and this is a Friday (day 6),
	-- then we get (2-6) = 4 days to go back. 
	SELECT @adjustment = @startdow - datepart(dw,dbo.fn_GetDate_Local())

	-- Cater for not reaching the next week-ending date yet. If today is 
	-- Sunday (1) then we have (2-1) = 1 day forwards. Subtracting 7 days
	-- means we go back the required 6 instead.
	IF @adjustment > 0
	BEGIN
		SET @adjustment = @adjustment - 7
	END

	-- If the date passed in is greater-than-or-equal-to the computed start date
	-- of the current working week, then the result is a success.
	SELECT @weekending = dateadd(dd, @adjustment, dbo.fn_GetDate_Local())

	-- Return the result of the function
	RETURN substring(convert(varchar, @weekending, 126), 1, 10)

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLastWorkWeekEndingChardate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetLastWorkWeekEndingChardate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLastWorkWeekEndingChardate] TO [sp_executeall]
GO
