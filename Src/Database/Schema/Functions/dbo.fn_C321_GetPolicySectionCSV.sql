SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ================================================================================================================
-- Author:		Robin Hall
-- Create date: 2014-10-01
-- Description:	Returns comma separated policy sections for claim
-- =================================================================================================================

CREATE FUNCTION [dbo].[fn_C321_GetPolicySectionCSV] 
(
	@MatterID INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

DECLARE @csv VARCHAR(MAX)

;WITH PolicySections (Section, Subsection, rn) AS (
SELECT 
	lliSection.ItemValue, lliSubSection.ItemValue, ROW_NUMBER() OVER(PARTITION BY lliSection.ItemValue, lliSubSection.ItemValue ORDER BY r.TableRowID)
FROM 	
	Matter m WITH (NOLOCK)
	INNER JOIN dbo.TableRows r WITH (NOLOCK) ON r.MatterID = m.MatterID AND r.DetailFieldID = 144355
	INNER JOIN dbo.TableDetailValues tSection WITH (NOLOCK) ON tSection.TableRowID = r.TableRowID AND tSection.DetailFieldID = 144350
	INNER JOIN dbo.ResourceListDetailValues rldvSection WITH (NOLOCK) ON rldvSection.ResourceListID = tSection.ResourceListID AND rldvSection.DetailFieldID = 146189
	LEFT JOIN dbo.ResourceListDetailValues rldvSubSection WITH (NOLOCK) ON rldvSubSection.ResourceListID = tSection.ResourceListID AND rldvSubSection.DetailFieldID = 146190
	INNER JOIN LookupListItems lliSection WITH (NOLOCK) ON lliSection.LookupListItemID = rldvSection.ValueInt
	LEFT JOIN LookupListItems lliSubSection WITH (NOLOCK) ON lliSubSection.LookupListItemID = rldvSubSection.ValueInt
WHERE 
	m.MatterID = @MatterID
)

SELECT 
	@csv = 
		ISNULL(@csv + ', ', '') 
		+ Section
		+ CASE WHEN LEN(Subsection) > 1 THEN ' (' + Subsection + ')' ELSE '' END
FROM 
	PolicySections
WHERE 
	rn = 1	
ORDER BY 
	CASE Section WHEN 'Veterinary fees' THEN 0 ELSE 1 END
	,Section, Subsection


RETURN @csv

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C321_GetPolicySectionCSV] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C321_GetPolicySectionCSV] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C321_GetPolicySectionCSV] TO [sp_executeall]
GO
