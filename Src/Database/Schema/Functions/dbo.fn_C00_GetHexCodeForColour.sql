SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- ALTER date:	2018-04-09
-- Description:	Take a colour name and get the equivalent hex code
-- GPR 2018-08=03 ported from C433 to C600
-- =============================================

CREATE FUNCTION [dbo].[fn_C00_GetHexCodeForColour]
(
	@ColourName	VARCHAR(50)
)
RETURNS VARCHAR(7)
AS
BEGIN
	
	DECLARE @ReturnValue VARCHAR(7)
	
	SELECT @ReturnValue = hc.HexColourCode
	FROM HexColour hc WITH ( NOLOCK ) 
	WHERE hc.ColourName = @ColourName
		
	RETURN @ReturnValue

END









GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetHexCodeForColour] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_GetHexCodeForColour] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetHexCodeForColour] TO [sp_executeall]
GO
