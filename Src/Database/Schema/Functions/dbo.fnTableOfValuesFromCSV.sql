SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2008-09-30
-- Description:	Return a table of varchars from an input string
-- =============================================
CREATE FUNCTION [dbo].[fnTableOfValuesFromCSV]
(
	@instr NVARCHAR(MAX)
)
RETURNS 
@TableOut TABLE 
(
	AnyValue NVARCHAR(MAX)
)
AS
BEGIN

	DECLARE @delimiter NCHAR(1)
	DECLARE @testvalue NVARCHAR(MAX)
	DECLARE @position  INT

	SET @delimiter = ','
	SET @instr = LTRIM(RTRIM(@instr)) + ','
	SET @position = CHARINDEX(@delimiter, @instr, 1)

	IF REPLACE(@instr, @delimiter, '') <> ''
	BEGIN
		WHILE @position > 0
		BEGIN
			INSERT INTO @TableOut (AnyValue) 
			SELECT LTRIM(RTRIM(LEFT(@instr, @position - 1)))

			SET @instr = RIGHT(@instr, LEN(@instr) - @position)
			SET @position = CHARINDEX(@delimiter, @instr, 1)
		END
	END	
	RETURN
END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfValuesFromCSV] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnTableOfValuesFromCSV] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfValuesFromCSV] TO [sp_executeall]
GO
GRANT SELECT ON  [dbo].[fnTableOfValuesFromCSV] TO [sp_executehelper]
GO
