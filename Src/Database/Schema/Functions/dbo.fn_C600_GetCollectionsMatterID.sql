SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-01-13
-- Description:	Gets the Collections matterID from passed MatterID
-- Mods
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetCollectionsMatterID]
(
	@MatterID INT
)
RETURNS INT	
AS
BEGIN

	DECLARE @CollectionsMatterID INT
	
	SELECT @CollectionsMatterID=@MatterID
			
	-- make sure we are dealing with a collections matterID
	IF NOT EXISTS ( SELECT * FROM Lead l WITH (NOLOCK)
				INNER JOIN Matter m WITH (NOLOCK) ON l.LeadID=m.LeadID AND m.MatterID=@MatterID
				WHERE l.LeadTypeID=1493 )
	BEGIN

		IF EXISTS ( SELECT * FROM Lead l WITH (NOLOCK)
					INNER JOIN Matter m WITH (NOLOCK) ON l.LeadID=m.LeadID AND m.MatterID=@MatterID
					WHERE l.LeadTypeID=1492 )
		BEGIN
	
			-- @MatterID is from the Policy Admin leadtype
			SELECT @CollectionsMatterID=ltr.ToMatterID FROM LeadTypeRelationship ltr WITH (NOLOCK) 
			WHERE ltr.FromMatterID=@MatterID AND ltr.ToLeadTypeID=1493
		
		END
		ELSE
		BEGIN
		
			-- @MatterID is from the Pet Claim leadtype
			SELECT @CollectionsMatterID=ltr2.ToMatterID 
			FROM Matter m WITH (NOLOCK) 
			INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON m.LeadID=ltr.ToLeadID AND ltr.ToLeadTypeID=1490
			INNER JOIN LeadTypeRelationship ltr2 WITH (NOLOCK) ON ltr.FromMatterID=ltr2.FromMatterID AND ltr2.ToLeadTypeID=1493
			WHERE m.MatterID=@MatterID
		
		END
		
	END

	RETURN @CollectionsMatterID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetCollectionsMatterID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetCollectionsMatterID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetCollectionsMatterID] TO [sp_executeall]
GO
