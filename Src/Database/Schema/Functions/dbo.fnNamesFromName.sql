SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2008-09-30
-- Description:	Separates a fullname into firstname and lastname
-- =============================================
CREATE FUNCTION [dbo].[fnNamesFromName]
(
	@fullname nvarchar(max)
)
RETURNS @Names TABLE 
(
	FirstName nvarchar(200),
	LastName nvarchar(200)
)
AS
BEGIN

	DECLARE @delimiter nchar(1),
	@position int,
	@retval bit

	SELECT @delimiter = ' ',
	@retval = 1,
	@fullname = LTRIM(RTRIM(@fullname)),
	@position = CHARINDEX(@delimiter, @fullname, 1)

	IF @position > 0
	BEGIN
		INSERT INTO @Names (FirstName, LastName)
		SELECT LEFT(@fullname, @position - 1), LTRIM(RTRIM(RIGHT(@fullname, LEN(@fullname) - @position)))
	END	
	ELSE
	BEGIN
		INSERT INTO @Names (FirstName, LastName)
		SELECT @fullname, ''
	END	

	RETURN

END

--grant select on  [dbo].[fnNamesFromName] to sp_executeall








GO
GRANT VIEW DEFINITION ON  [dbo].[fnNamesFromName] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnNamesFromName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnNamesFromName] TO [sp_executeall]
GO
