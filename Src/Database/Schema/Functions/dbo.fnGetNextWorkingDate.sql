SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2008-08-27
-- Description:	Find the next available working day [n] days after the date passed in.
-- =============================================
CREATE FUNCTION [dbo].[fnGetNextWorkingDate] 
(
	@date datetime, @daystoadd int
)
RETURNS datetime
AS
BEGIN

	-- The date passed in can be a date or datetime in any valid sql server format.  
	-- There is a sister function called dbo.fnGetNextWorkingCharDate that works with 
	-- char(10) values instead, eg '2008-12-31' without the time element.

	-- The WorkingDays table knows all about weekends and bank holidays,
	-- so if you need to set a field to a date 5 days after some event fires, 
	-- but it has to be a work day, this will start at (d+5) and then crawl forwards
	-- one day at a time until a work day is reached.

	-- Example: SELECT dbo.fnGetNextWorkingDate(LeadEvent.WhenCreated, 1) 
	-- This starts at (d+1), but that would be a Saturday (23rd)
	-- so it then keeps going past the Sunday (24th) AND the Bank Holiday Monday (25th) and
	-- therefore returns '2008-08-26 11:05:31:123' which is the Tuesday. 
	SELECT @date = min(w.[Date]) 
	FROM workingdays w 
	WHERE w.[Date] >= dateadd(dd, @daystoadd, convert(char(10), @date, 126)) 
	AND w.[IsWorkDay] = 1 

	RETURN @date
END









GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetNextWorkingDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetNextWorkingDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetNextWorkingDate] TO [sp_executeall]
GO
