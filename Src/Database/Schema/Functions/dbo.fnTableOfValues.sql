SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2009-05-13
-- Description:	Return a table of varchars from an input string
-- =============================================
CREATE FUNCTION [dbo].[fnTableOfValues]
(
	@instr varchar(max),
	@delimiter char(1)
)
RETURNS 
@TableOut TABLE 
(
	AnyValue varchar(max)
)
AS
BEGIN

	DECLARE @testvalue varchar(max)
	DECLARE @position  int

	SET @instr = @instr + @delimiter
	SET @position = CHARINDEX(@delimiter, @instr, 1)

	IF REPLACE(@instr, @delimiter, '') <> ''
	BEGIN
		WHILE @position > 0
		BEGIN
			INSERT INTO @TableOut (AnyValue) 
			SELECT LEFT(@instr, @position - 1)

			SET @instr = RIGHT(@instr, datalength(@instr) - @position)
			SET @position = CHARINDEX(@delimiter, @instr, 1)
		END
	END	
	RETURN
END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfValues] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnTableOfValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfValues] TO [sp_executeall]
GO
GRANT SELECT ON  [dbo].[fnTableOfValues] TO [sp_executehelper]
GO
