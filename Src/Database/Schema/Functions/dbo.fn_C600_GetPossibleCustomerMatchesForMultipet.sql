SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2019-07-25
-- Description:	Take customer details and return possible matches from existing customers
-- 2019-07-25 CPS for JIRA BAULAG-64 | Moved this to a function so that multiple procedures can call the same code in case we need to make changes in future.
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetPossibleCustomerMatchesForMultipet]
(
	 @BrandID				INT
	,@ClientID				INT
	,@Postcode				VARCHAR(10)
	,@CustomerSurname		VARCHAR(100)
	,@CustomerFirstName		VARCHAR(100)
	,@CustomerAddress1		VARCHAR(200)
	,@XMLRequest			XML
	,@PetName				varchar(2000)
)
RETURNS @Data TABLE
(
	 CustomerID				INT
	,BrandID				INT
	,ClientID				INT
	,Postcode				VARCHAR(10)
	,LastName				VARCHAR(100)
	,FirstName				VARCHAR(100)
	,Address1				VARCHAR(200)
)
AS
BEGIN

	DECLARE @PetNames dbo.tvpVarchar
	INSERT @PetNames( AnyValue )
	SELECT b.value('(PetInfo/PetName)[1]','VARCHAR(2000)')
	FROM @XmlRequest.nodes('(//PetQuote)') a(b)
		UNION
	SELECT @PetName
	WHERE @PetName <> ''

	INSERT @Data 
	SELECT   cu.CustomerID
			,cdv.ValueInt	[BrandID]
			,cu.ClientID	
			,cu.PostCode
			,cu.LastName
			,cu.FirstName
			,cu.Address1
	FROM Customers cu WITH (NOLOCK) 
	INNER JOIN dbo.CustomerDetailValues cdv WITH (NOLOCK) on cdv.CustomerID = cu.CustomerID AND cdv.DetailFieldID = 170144 /*Affinity Details*/
	WHERE (cu.Test		= 0)
	AND (cdv.ValueInt	= @BrandID)
	AND (cu.ClientID	= @ClientID)
	AND (cu.PostCode	= @Postcode)
	AND (cu.LastName	= @CustomerSurname)
	AND (cu.FirstName	= @CustomerFirstName)
	AND (cu.Address1	= @CustomerAddress1)
	AND EXISTS ( SELECT *
	             FROM Lead l WITH (NOLOCK)
				 INNER JOIN LeadDetailValues ldv WITH ( NOLOCK ) ON ldv.LeadID = l.LeadID AND ldv.DetailFieldID = 144268 /*PetName GPR 2019-01-28 C600 CR41*/ 
				 INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.LeadID = l.LeadID AND mdv.DetailFieldID = 170038 /*Policy Status*/
				 WHERE l.CustomerID = cu.CustomerID
				 AND l.LeadTypeID = 1492 /*PolicyAdmin*/
				 AND mdv.ValueInt = 43002 /*Live*/ 
				 AND ldv.DetailValue NOT IN (SELECT AnyValue FROM @PetNames))
	-- And return it
	RETURN

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetPossibleCustomerMatchesForMultipet] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_GetPossibleCustomerMatchesForMultipet] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetPossibleCustomerMatchesForMultipet] TO [sp_executeall]
GO
