SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-07-12
-- =============================================
CREATE FUNCTION [dbo].[fn_RulesEngine_BreadCrumbs]( 
	@ClientID INT,
	@ChangeSetID INT,
	@RuleSetID INT,
	@RuleID INT,
	@RuleParameterID INT,
	@ParameterOptionID INT 
) 
RETURNS TABLE 
AS RETURN 
(
	WITH BreadCrumbs AS
	(
		SELECT	cs.ChangeSetID, rs.RuleSetID, r.RuleID, rp.RuleParameterID, po.ParameterOptionID,
				cs.TagName CSName, rs.Name RSName, r.Name RName, rp.Name RPName, po.Val1 OName
		FROM dbo.RulesEngine_ChangeSets cs WITH (NOLOCK) 
		LEFT JOIN dbo.RulesEngine_RuleSets rs WITH (NOLOCK) ON cs.ChangeSetID = rs.ChangeSetID AND rs.RuleSetID = @RuleSetID
		LEFT JOIN dbo.RulesEngine_Rules r WITH (NOLOCK) ON rs.RuleSetID = r.RuleSetID AND r.RuleID = @RuleID
		LEFT JOIN dbo.RulesEngine_RuleParameters rp WITH (NOLOCK) ON rp.RuleID = r.RuleID AND rp.RuleParameterID = @RuleParameterID
		LEFT JOIN dbo.RulesEngine_ParameterOptions po WITH (NOLOCK) ON po.RuleParameterID = rp.RuleParameterID AND po.ParameterOptionID = @ParameterOptionID
		WHERE	cs.ChangeSetID = @ChangeSetID
		AND		cs.ClientID = @ClientID
	)
	SELECT	'Premiums Engine' AS Title, '#/' AS Path, 1 AS Ord 
	FROM	BreadCrumbs
	WHERE	ChangeSetID IS NOT NULL
	UNION ALL
	SELECT	CSName AS Title, '#/ruleset/' + CAST(ChangeSetID AS VARCHAR) AS Path, 2 AS Ord  
	FROM	BreadCrumbs
	WHERE	RuleSetID IS NOT NULL
	UNION ALL
	SELECT	RSName AS Title, '#/ruleset/' + CAST(RuleSetID AS VARCHAR) + '/rules' AS Path, 3 AS Ord 
	FROM	BreadCrumbs
	WHERE	RuleID IS NOT NULL
	UNION ALL
	SELECT	RName AS Title, '#/rule/' + CAST(RuleID AS VARCHAR) AS Path, 4 AS Ord 
	FROM	BreadCrumbs
	WHERE	RuleParameterID IS NOT NULL
	UNION ALL
	SELECT	RPName AS Title, '#/parameter/' + CAST(RuleParameterID AS VARCHAR) + '/options' AS Path, 5 AS Ord  
	FROM	BreadCrumbs
	WHERE	ParameterOptionID IS NOT NULL
)
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_BreadCrumbs] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_RulesEngine_BreadCrumbs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_BreadCrumbs] TO [sp_executeall]
GO
