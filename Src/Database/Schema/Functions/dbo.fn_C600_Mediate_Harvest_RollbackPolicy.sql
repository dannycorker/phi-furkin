SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire
-- Create date:	2020-07-30
-- Description:	harvests rollback policy json object
-- 2021-02-05 ALM Include new status for vision: 1 Active, 2 Cancelled, 3 Cancelled to Inception
-- 2021-03-30 ALM Updated status for Vision: 1 Live, 2 Cancelled, 3 Cancellation Pending
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Mediate_Harvest_RollbackPolicy]
(
	 @PolicyID			INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE  @JsonData	VARCHAR(MAX) =''

	IF @PolicyID > 0
	BEGIN
		SELECT  @JsonData +=
		(
			SELECT	  l.LeadRef													[reference]
					 ,1															[type]
					 ,mdvDateIncep.ValueDateTime								[inceptionDate]
					 ,0.00														[premium]
					 ,CASE WHEN mdvStatus.ValueInt = 43003 THEN 2 /*Cancelled*/
						   WHEN mdvStatus.ValueInt = 74535 THEN 3 /*Cancellation Pending*/
						   ELSE 1 /*Live*/
					  END														[status]
					 ,mdvDateTerms.ValueDateTime								[termsDate]
			FROM Customers cu								WITH (NOLOCK)
			INNER JOIN Lead l								WITH (NOLOCK) on l.CustomerID = cu.CustomerID
			INNER JOIN Cases c								WITH (NOLOCK) on c.LeadID = l.LeadID
			INNER JOIN Matter m								WITH (NOLOCK) on m.CaseID = c.CaseID
			INNER JOIN MatterDetailValues mdvStatus			WITH (NOLOCK) on mdvStatus.MatterID = m.MatterID					AND mdvStatus.DetailFieldID			= 170038 /*Policy Status*/
			LEFT JOIN MatterDetailValues mdvDateIncep		WITH (NOLOCK) on mdvDateIncep.MatterID = m.MatterID					AND mdvDateIncep.DetailFieldID		= 170035 /*Policy Inception Date*/
			INNER JOIN MatterDetailValues mdvDateEnd		WITH (NOLOCK) on mdvDateEnd  .MatterID = m.MatterID					AND mdvDateEnd.DetailFieldID		= 170037 /*Policy End Date*/
			LEFT JOIN MatterDetailValues mdvDateTerms		WITH (NOLOCK) on mdvDateTerms.MatterID = m.MatterID					AND mdvDateTerms.DetailFieldID		= 176925 /*Policy Terms Date*/
			WHERE m.MatterID = @PolicyID
			AND l.LeadTypeID = 1492 /*Policy Admin*/

			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		)
	END


	RETURN @JsonData

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Mediate_Harvest_RollbackPolicy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_Mediate_Harvest_RollbackPolicy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Mediate_Harvest_RollbackPolicy] TO [sp_executeall]
GO
