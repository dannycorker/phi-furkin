SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-08-27
-- Description:	Returns the wait date for the wait type specified (date after which we can start taking payments)
-- Mods
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetWaitDate]
(
	@MatterID INT,
	@WaitType INT
)
RETURNS DATE	
AS
BEGIN

	DECLARE @AccountID INT,
			@CaseID INT,
			@ClientID INT,
			@CollectionsMatterID INT,
			@WaitDate DATE,
			@WaitPeriod INT

	-- find appropriate collections matter for the matter passed in
	IF EXISTS ( SELECT * FROM Lead l WITH (NOLOCK)
				INNER JOIN Matter m WITH (NOLOCK) ON l.LeadID=m.LeadID AND m.MatterID=@MatterID
				WHERE l.LeadTypeID=1493 )
	BEGIN
	
		SELECT @CollectionsMatterID=@MatterID
		
	END
	ELSE
	BEGIN
	
		SELECT @CollectionsMatterID=ltr.ToMatterID FROM LeadTypeRelationship ltr WITH (NOLOCK) 
		WHERE ltr.FromMatterID=@MatterID AND ltr.ToLeadTypeID=1493

	END
	
	-- find the account being used & if it is a DD account look up the mandate insruction clearance date, otherwise use today
	SELECT @CaseID=CaseID, @ClientID=ClientID 
	FROM Matter WITH (NOLOCK) WHERE MatterID=@CollectionsMatterID

	SELECT @WaitPeriod=CASE @WaitType 
				WHEN 2 THEN RegularPaymentWait
				WHEN 3 THEN OneOffAdjustmentWait
				WHEN 4 THEN MandateWait
			END
	FROM BillingConfiguration WITH (NOLOCK) 
	WHERE ClientID=@ClientID


	IF @WaitType = 4 -- mandate wait; look up wait date
	BEGIN
	
		SELECT @AccountID=dbo.fnGetDvAsInt(176973,@CaseID)

		IF dbo.fnGetDvAsInt(170115,@CaseID) = 69930 -- BACS
		BEGIN
		
			SELECT TOP 1 @WaitDate=FirstAcceptablePaymentDate 
			FROM AccountMandate WITH (NOLOCK) 
			WHERE AccountID=@AccountID 
				AND MandateStatusID IN (1,2,3)
			ORDER BY AccountMandateID DESC
			
			-- get waitdate from schedule if mandate hasn't been requested yet		
			IF @WaitDate IS NULL -- mandate request not sent yet; try to get date from any other schedules on this account
			BEGIN
			
				SELECT TOP 1 @WaitDate=ActualCollectionDate 
				FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
				WHERE AccountID=@AccountID
				AND PaymentStatusID IN (1,5)
				ORDER BY ActualCollectionDate
			
			END
		
		END
		ELSE
		BEGIN
		
			SELECT @WaitDate=dbo.fn_GetDate_Local()
		
		END
		
	END
	ELSE -- another wait type; just add wait period to now
	BEGIN
	
		SELECT @WaitDate = DATEADD(DAY,@WaitPeriod,dbo.fn_GetDate_Local())
	
	END

	RETURN @WaitDate

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetWaitDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetWaitDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetWaitDate] TO [sp_executeall]
GO
