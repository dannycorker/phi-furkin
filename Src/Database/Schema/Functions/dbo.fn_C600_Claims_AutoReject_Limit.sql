SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		James Lewis
-- Create date: 2017-12-05
-- Description:	Returns a value for SQLClauseForInclusion 
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Claims_AutoReject_Limit]
(
	@CaseID INT
)
RETURNS INT	
AS
BEGIN
	
	--DECLARE @CaseID INT = 14983
	
	DECLARE @MatterID INT ,
			@Result INT = 0,
			@LeadID INT,
			@PolicyMatterID INT ,
			@PolicyStart DATE,
			@DaysSinceStart INT ,
			@CurrentPolicyMatter INT ,
			@PolicyEnd DATE 
									
	SELECT @MatterID = m.MatterID, @LeadID = m.LeadID
	FROM Matter m WITH ( NOLOCK ) 
	where m.CaseID = @CaseID 
	
	SELECT @PolicyMatterID =  dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(@MatterID)
	
	SELECT @PolicyStart = dbo.fnGetSimpleDvAsDate( 170036,@PolicyMatterID ) /*Policy Start Date*/
	SELECT @PolicyEnd = dbo.fnGetSimpleDvAsDate ( 170037,@PolicyMatterID ) /*Policy End Date*/
	
	SELECT @Result = 1 
	FROM TableRows tr WITH ( NOLOCK ) 
 	INNER JOIN  TableDetailValues tdv WITH ( NOLOCK ) on tr.TableRowID = tdv.TableRowID
	INNER JOIN TableDetailValues tdv1 WITH ( NOLOCK ) on tdv.TableRowID = tdv1.TableRowID 
	INNER JOIN TableDetailValues tdv2 WITH ( NOLOCK ) on tdv2.TableRowID = tr.TableRowID
	INNER JOIN TableDetailValues tdv3 WITH ( NOLOCK ) on tdv3.TableRowID = tr.TableRowID 
	Where 	 tr.MatterID = @MatterID
	AND tdv.DetailFieldID = 146408 -- limit
	AND tdv1.DetailFieldID = 144352 -- Total  
	AND tdv2.DetailFieldID= 144349 -- Treatment Start
	AND tdv3.DetailFieldID = 144351 /*Treatment End*/
	AND tdv2.ValueDate BETWEEN @PolicyStart and @PolicyEnd 
	AND tdv3.ValueDate BETWEEN @PolicyStart AND @PolicyEnd
	AND tdv.ValueMoney < 0 
	AND tdv1.ValueMoney  = 0 
					
	Return  @Result

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Claims_AutoReject_Limit] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_Claims_AutoReject_Limit] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Claims_AutoReject_Limit] TO [sp_executeall]
GO
