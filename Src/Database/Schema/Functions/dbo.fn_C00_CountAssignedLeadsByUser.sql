SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2015-04-14
-- Description:	Count the number of leads assigned to users for a client
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_CountAssignedLeadsByUser] 
(	
	 @ClientID		INT
	,@LeadTypeID	INT
)
RETURNS
@TableOut TABLE 
(
	 ClientPersonnelID				INT
	,UserName						VARCHAR(201)
	,ClientPersonnelAdminGroupID	INT
	,ClientOfficeID					INT
	,IsAquarium						BIT
	,Leads							INT
) 
AS
BEGIN	

	INSERT @TableOut ( ClientPersonnelID, UserName, ClientPersonnelAdminGroupID, ClientOfficeID, IsAquarium, Leads )
	SELECT cp.ClientPersonnelID, cp.UserName, cp.ClientPersonnelAdminGroupID, cp.ClientOfficeID, cp.IsAquarium, COUNT(l.LeadID) [Leads]
	FROM ClientPersonnel cp WITH ( NOLOCK ) 
	LEFT JOIN Lead l WITH (NOLOCK) on l.AssignedTo = cp.ClientPersonnelID AND ( l.LeadTypeID = @LeadTypeID or @LeadTypeID is NULL )
	WHERE cp.AccountDisabled = 0
	AND cp.ClientID = @ClientID
	GROUP BY cp.ClientPersonnelID, cp.UserName, cp.ClientPersonnelAdminGroupID, cp.ClientOfficeID, cp.IsAquarium

	RETURN
END








GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_CountAssignedLeadsByUser] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_CountAssignedLeadsByUser] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_CountAssignedLeadsByUser] TO [sp_executeall]
GO
