SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Conor Reeves
-- Create date: 2019-06-20
-- Description:	To concatenate the LeadID's into a single string 
-- 2019-07-16 CR updated the function to resolve the issue with the commas.
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_ConcatStartDate]
(
	@CustomerID	INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE @StartDate VARCHAR(2000) = ''

	SELECT @StartDate  += CAST (FORMAT(mdv.ValueDate, N'dd/MM/yyyy') AS VARCHAR(2000)) + ', '
	FROM MatterDetailValues mdv WITH ( NOLOCK )
	INNER JOIN dbo.matter m WITH ( NOLOCK ) on m.matterid = mdv.matterid
	INNER JOIN dbo.lead l WITH ( NOLOCK ) on l.LeadID = m.LeadID
	INNER JOIN MatterDetailValues PolicyStatus WITH (NOLOCK) on PolicyStatus.MatterID = m.MatterID AND PolicyStatus.DetailFieldID = 170038 --Policy Status
	INNER JOIN MatterDetailValues OMFOutcomes	WITH (NOLOCK) on OMFOutcomes.MatterID = m.MatterID AND OMFOutcomes.DetailFieldID = 179964 -- OMF Outcomes
	WHERE l.CustomerID = @CustomerID
	AND mdv.DetailFieldID = 170036
	and m.MatterRef = 'One Month Free'
	AND l.LeadTypeID = 1492
	AND PolicyStatus.ValueInt not in (43003,43004,74535,76595) -- Cancelled Lapsed CancelPending and Void
	and OMFOutcomes.ValueInt not in (76418)

	--SELECT @StartDate  += CAST (mdv.ValueDate as Varchar) + ', '
	--FROM MatterDetailValues mdv WITH ( NOLOCK )
	--INNER JOIN dbo.matter m WITH ( NOLOCK ) on m.matterid = mdv.matterid
	--INNER JOIN dbo.lead l WITH ( NOLOCK ) on l.LeadID = m.LeadID
	--WHERE l.CustomerID = @CustomerID
	--AND mdv.DetailFieldID = 170036
	--and m.MatterRef = 'One Month Free'
	--AND l.LeadTypeID = 1492


	IF @@ROWCOUNT > 0
	BEGIN
		SELECT @StartDate = LEFT (@StartDate, LEN(@StartDate) -1)
	END

	RETURN @StartDate

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_ConcatStartDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_ConcatStartDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_ConcatStartDate] TO [sp_executeall]
GO
