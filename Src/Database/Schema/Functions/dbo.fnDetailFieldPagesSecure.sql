SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






-- =============================================
-- Author:		Jim Green
-- Create date: 2007-10-16
-- Description:	Only returns Pages this user is allowed to see.
--              See detailed comments about half way down the script.
-- JWG 2009-01-19 Bugfix: added @LeadTypeID to the UserFunctions12 section.
-- SB  2014-07-09 Updated to use view which includes shared pages
-- =============================================
CREATE FUNCTION [dbo].[fnDetailFieldPagesSecure] 
(	
	@UserID int, 
	@LeadTypeID int
)
RETURNS 
@Data TABLE 
(
	ObjectID INT,
	RightID INT
)
AS
BEGIN

	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.ClientPersonnel WITH (NOLOCK) 
	WHERE ClientPersonnelID = @UserID
	
	;with UserGroup as
	(
		select ClientPersonnelAdminGroupID as 'GroupID', ClientID
		from ClientPersonnel 
		where ClientPersonnelID = @UserID
	),
	UserRights as
	(
		select objectid, rightid
		from grouprightsdynamic 
		where clientpersonneladmingroupid = (select GroupID from UserGroup)
		and grouprightsdynamic.FunctionTypeID = 12
		and grouprightsdynamic.LeadTypeID = @LeadTypeID
		and grouprightsdynamic.rightid > 0
		and not exists (select * from userrightsdynamic where userrightsdynamic.functiontypeid = grouprightsdynamic.functiontypeid and isnull(userrightsdynamic.leadtypeid,-1) = isnull(grouprightsdynamic.leadtypeid, -1) and userrightsdynamic.objectid = grouprightsdynamic.objectid and userrightsdynamic.clientpersonnelid = @UserID)
		union
		select objectid, rightid
		from userrightsdynamic 
		where clientpersonnelid = @UserID
		and userrightsdynamic.FunctionTypeID = 12
		and userrightsdynamic.LeadTypeID = @LeadTypeID
		and userrightsdynamic.rightid > 0
	),
	UserLeadTypes as
	(
		SELECT gfc.ModuleID, gfc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, gfc.HasDescendants, gfc.RightID 
		FROM GroupFunctionControl gfc INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID and ft.ModuleID = gfc.ModuleID 
		WHERE gfc.ClientPersonnelAdminGroupID = (select GroupID from UserGroup)
		and gfc.FunctionTypeID = 23
		and gfc.LeadTypeID = @LeadTypeID
		AND NOT EXISTS (SELECT * FROM UserFunctionControl ufc1 WHERE ufc1.functiontypeid = gfc.functiontypeid and ufc1.ClientPersonnelID = @UserID)
		UNION
		SELECT ufc.ModuleID, ufc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, ufc.HasDescendants, ufc.RightID 
		FROM UserFunctionControl ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID and ft.ModuleID = ufc.ModuleID 
		WHERE ufc.ClientPersonnelID = @UserID
		and ufc.FunctionTypeID = 23
		and ufc.LeadTypeID = @LeadTypeID
	),
	UserFunctions as 
	(
		SELECT gfc.ModuleID, gfc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, gfc.HasDescendants, gfc.RightID 
		FROM GroupFunctionControl gfc INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID and ft.ModuleID = gfc.ModuleID 
		WHERE gfc.ClientPersonnelAdminGroupID = (select GroupID from UserGroup)
		and gfc.FunctionTypeID = 7
		AND NOT EXISTS (SELECT * FROM UserFunctionControl ufc1 WHERE ufc1.functiontypeid = gfc.functiontypeid and ufc1.ClientPersonnelID = @UserID)
		UNION
		SELECT ufc.ModuleID, ufc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, ufc.HasDescendants, ufc.RightID 
		FROM UserFunctionControl ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID and ft.ModuleID = ufc.ModuleID 
		WHERE ufc.ClientPersonnelID = @UserID
		and ufc.FunctionTypeID = 7
	),
	UserFunctions12 as 
	(
		SELECT gfc.ModuleID, gfc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, gfc.HasDescendants, gfc.RightID 
		FROM GroupFunctionControl gfc INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID and ft.ModuleID = gfc.ModuleID 
		WHERE gfc.ClientPersonnelAdminGroupID = (select GroupID from UserGroup)
		and gfc.FunctionTypeID = 12
		and gfc.LeadTypeID = @LeadTypeID
		AND NOT EXISTS (SELECT * FROM UserFunctionControl ufc1 WHERE ufc1.functiontypeid = gfc.functiontypeid and ufc1.LeadTypeID = @LeadTypeID and ufc1.ClientPersonnelID = @UserID)
		UNION
		SELECT ufc.ModuleID, ufc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, ufc.HasDescendants, ufc.RightID 
		FROM UserFunctionControl ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID and ft.ModuleID = ufc.ModuleID 
		WHERE ufc.ClientPersonnelID = @UserID
		and ufc.FunctionTypeID = 12
		and ufc.LeadTypeID = @LeadTypeID
	),
	UserLeadManagerModule as 
	(
		SELECT gfc.ModuleID, gfc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, gfc.HasDescendants, gfc.RightID 
		FROM GroupFunctionControl gfc INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID and ft.ModuleID = gfc.ModuleID 
		WHERE gfc.ClientPersonnelAdminGroupID = (select GroupID from UserGroup)
		and gfc.FunctionTypeID = 1
		AND NOT EXISTS (SELECT * FROM UserFunctionControl ufc1 WHERE ufc1.functiontypeid = gfc.functiontypeid and ufc1.ClientPersonnelID = @UserID)
		UNION
		SELECT ufc.ModuleID, ufc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, ufc.HasDescendants, ufc.RightID 
		FROM UserFunctionControl ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID and ft.ModuleID = ufc.ModuleID 
		WHERE ufc.ClientPersonnelID = @UserID
		and ufc.FunctionTypeID = 1
	)
	/*
		A user has one of these things: 
		(1) a full set of individual records specifying which pages they can see 
			and which they cannot see, by Lead Type ("UserRights" above)
		or
		(2) an overall record specifying that they can or cannot see this Lead Type ("UserLeadTypes" above)
		or
		(3) an overall record specifying that they can or cannot see all Pages ("UserFunctions" above)
		or
		(4) individual records specifying what they can do with each Page ("UserFunctions12" above)
		or
		(5) an overall Lead Manager module control record
	*/

	INSERT @Data(ObjectID, RightID)
	select UserRights.objectid, UserRights.RightID 
	from UserRights
	where not exists (select * from UserLeadTypes where RightID = 0 and HasDescendants = 0)

	union

	select DetailFieldPageID, UserLeadTypes.RightID 
	from fnDetailFieldPagesShared(@ClientID) df INNER JOIN UserGroup ON df.ClientID = UserGroup.ClientID  
	CROSS JOIN UserLeadTypes
	where LeadTypeID = @LeadTypeID
	and UserLeadTypes.RightID > 0
	and UserLeadTypes.HasDescendants = 0

	union

	select DetailFieldPageID, UserFunctions.RightID 
	from fnDetailFieldPagesShared(@ClientID) df INNER JOIN UserGroup ON df.ClientID = UserGroup.ClientID   
	CROSS JOIN UserFunctions
	where LeadTypeID = @LeadTypeID
	and not exists (select * from UserLeadTypes where RightID = 0)
	and UserFunctions.RightID > 0
	and UserFunctions.HasDescendants = 0

	union

	select DetailFieldPageID, UserFunctions12.RightID 
	from fnDetailFieldPagesShared(@ClientID) df INNER JOIN UserGroup ON df.ClientID = UserGroup.ClientID   
	CROSS JOIN UserFunctions12
	where LeadTypeID = @LeadTypeID
	and not exists (select * from UserLeadTypes where RightID = 0)
	and UserFunctions12.RightID > 0
	and UserFunctions12.HasDescendants = 0

	union

	select DetailFieldPageID, UserLeadManagerModule.RightID 
	from fnDetailFieldPagesShared(@ClientID) df INNER JOIN UserGroup ON df.ClientID = UserGroup.ClientID   
	CROSS JOIN UserLeadManagerModule
	where LeadTypeID = @LeadTypeID
	and not exists (select * from UserFunctions where RightID = 0 and HasDescendants = 0)
	and not exists (select * from UserFunctions12 where RightID = 0 and HasDescendants = 0)
	and not exists (select * from UserLeadTypes where RightID = 0 and HasDescendants = 0)
	and UserLeadManagerModule.RightID > 0
	and UserLeadManagerModule.HasDescendants = 0
	
	
	RETURN
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnDetailFieldPagesSecure] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnDetailFieldPagesSecure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnDetailFieldPagesSecure] TO [sp_executeall]
GO
