SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-10-12
-- Description:	Returns a table of pets with live policies
-- Modified:	
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_Policy_GetPetList]
(
	@CustomerID INT
)
RETURNS 
	@PetList TABLE 
	(
		PetName VARCHAR(100),
		LeadID INT,
		PetNumber INT IDENTITY(1,1) NOT NULL
	)
AS
BEGIN
 
	DECLARE @PrePetList TABLE ( LeadID INT, PetName VARCHAR(100), rn INT )
	
	DECLARE @Count INT
	
	DECLARE @Now DATE = dbo.fn_GetDate_Local()
	SELECT @Now = dbo.fnDateOnly(@Now)
	
	DECLARE @MatterIDs TABLE (MatterID INT)
	
	INSERT @MatterIDs (MatterID)
	SELECT MatterID 
	FROM dbo.Matter m WITH (NOLOCK) 
	WHERE m.CustomerID = @CustomerID
	
	
	INSERT @PrePetList
	SELECT m.LeadID, ldvName.DetailValue,
	ROW_NUMBER() OVER(PARTITION BY m.LeadID ORDER BY m.MatterID) as rn 
	FROM dbo.Matter m WITH (NOLOCK)
	INNER JOIN @MatterIDs i ON m.MatterID = i.MatterID
	INNER JOIN dbo.MatterDetailValues mdvProduct WITH (NOLOCK) ON m.MatterID = mdvProduct.MatterID AND mdvProduct.DetailFieldID = 170034
	INNER JOIN dbo.MatterDetailValues mdvStatus WITH (NOLOCK) ON m.MatterID = mdvStatus.MatterID AND mdvStatus.DetailFieldID = 170038
	INNER JOIN dbo.MatterDetailValues mdvInception WITH (NOLOCK) ON m.MatterID = mdvInception.MatterID AND mdvInception.DetailFieldID = 170035
	INNER JOIN dbo.MatterDetailValues mdvEnd WITH (NOLOCK) ON m.MatterID = mdvEnd.MatterID AND mdvEnd.DetailFieldID = 170037
	INNER JOIN dbo.LeadDetailValues ldvName WITH (NOLOCK) ON ldvName.LeadID=m.LeadID AND ldvName.DetailFieldID = 144268
	--LEFT JOIN dbo.MatterDetailValues mdvQuote WITH (NOLOCK) ON m.MatterID = mdvQuote.MatterID AND mdvQuote.DetailFieldID = 171338 -- TODO map DFID when quoting implemented
	WHERE mdvStatus.ValueInt = 43002 -- Live policies
	AND mdvEnd.ValueDate >= @Now -- Double check on end date
	AND mdvInception.ValueDate <= @Now -- Prevent future start policies from being included
	--AND (mdvQuote.ValueInt IS NULL OR mdvQuote.ValueInt = 0) -- Ignore quotes   -- TODO when quoting implemented
	
	INSERT @PetList
	SELECT PetName, LeadID FROM @PrePetList WHERE rn=1 ORDER BY LeadID

	RETURN

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPetList] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1272_Policy_GetPetList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPetList] TO [sp_executeall]
GO
