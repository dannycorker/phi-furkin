SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		James Lewis
-- Create date: 2016-11-15
-- Description:	Gets the remaining excess on this cliam for this grouping for vet fees
-- Mods
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Claim_GetRemainingVetExcess]
(
	@MatterID INT
)
RETURNS MONEY	
AS
BEGIN
	
	DECLARE @ConditionID INT, 
			@ExcessRemaining DECIMAL (18,2),
			@LeadID INT ,
			@PetAge INT ,
			@PetDob DATE,
			@ExcessUsed DECIMAL (18,2),
			@PolicyMatterID INT
				
	SELECT @ConditionID = dbo.fnGetSimpleDv(144504,@MatterID)
	SELECT @PolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)


	SELECT @ExcessUsed  =   ISNULL(SUM(ex.ValueMoney) * -1,0.00), @PetDob = ldv.ValueDate --  , @Section = sec.ResourceListID 
	FROM dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 0) m 
	INNER JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID and mdv.DetailFieldID = 144504 /*Condition*/ 
	INNER JOIN dbo.TableDetailValues ex WITH (NOLOCK) on ex.MatterID = mdv.MatterID and ex.DetailFieldID = 146406
	INNER JOIN dbo.TableDetailValues sec WITH (NOLOCK) on sec.TableRowID = ex.TableRowID and sec.DetailFieldID = 144350
	INNER JOIN dbo.ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = sec.ResourceListID and rldv.DetailFieldID = 146189
	INNER JOIN dbo.LeadTypeRelationship ltr WITH (NOLOCK) on ltr.ToMatterID = @MatterID and ltr.ToLeadTypeID = 1490 
	INNER JOIN dbo.LeadDetailValues ldv WITH (NOLOCK) on ldv.LeadID = ltr.FromLeadID and ltr.FromLeadTypeID = 1492 
	WHERE  mdv.ValueInt = @ConditionID 
	and rldv.ValueInt = 74284
	and ex.ValueMoney IS NOT NULL
	and ldv.DetailFieldID = 144274
	GROUP BY ldv.ValueDate


	SELECT @PetAge = DATEDIFF(YEAR,@PetDob,dbo.fn_GetDate_Local()) 

	SELECT top 1 @ExcessRemaining = @ExcessUsed - tdv1.ValueMoney 
	FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = 144359 /*Section*/
	INNER JOIN dbo.ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = tdv.ResourceListID and rldv.DetailFieldID = 146189
	INNER JOIN dbo.TableDetailValues tdv1 WITH (NOLOCK) on tdv1.TableRowID = tr.TableRowID and tdv1.DetailFieldID = 144360 /*Amount*/ 
	INNER JOIN dbo.TableDetailValues tdv2 WITH (NOLOCK) on tdv2.TableRowID = tr.TableRowID and tdv2.DetailFieldID = 147931 /*Age*/ 
	where tr.MatterID = @PolicyMatterID
	and tdv2.ValueInt < @PetAge 
	and rldv.ValueInt = 74284
	Order BY tdv2.ValueInt DESC

	RETURN ISNULL(@ExcessRemaining,0.00) 

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Claim_GetRemainingVetExcess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_Claim_GetRemainingVetExcess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Claim_GetRemainingVetExcess] TO [sp_executeall]
GO
