SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire
-- Create date:	2020-10-02
-- Description:	harvests Policy term json object
-- 2021-02-16 ALM Include new status for vision: 1 Active, 2 Cancelled, 3 Cancelled to Inception
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Mediate_Harvest_PolicyTerm]
(
	 @PolicyID		INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE  @JsonData	VARCHAR(MAX) = '',
			 @CountryID INT 

	SELECT @CountryID = cu.CountryID 
	FROM Customers cu WITH (NOLOCK) 
	JOIN Lead l WITH (NOLOCK) ON l.CustomerID = cu.CustomerID 
	JOIN Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID
	WHERE m.MatterID = @PolicyID

	DECLARE @Products Table (ProductID INT, MatterID INT, Wellness INT, StartDate DATE, EndDate DATE)

	INSERT INTO @Products
	SELECT mdvClaimProductID.ValueInt, @PolicyID, 0, NULL, NULL 
	FROM MatterDetailValues mdvClaimProductID WITH (NOLOCK)
	WHERE mdvClaimProductID.MatterID = dbo.fn_C00_1273_GetCurrentSchemeFromMatter (@PolicyID)
	AND mdvClaimProductID.DetailFieldID = 313875 /*Claim Product ID*/

	IF @PolicyID <> 0
	BEGIN
		SELECT  @JsonData +=
		(
		
						SELECT	TOP 1
								p.productid [productId]
								,CASE WHEN @CountryID IN (233,39) AND p.Wellness = 0 AND p.productID = 12 THEN CAST(tdvExcess.ValueMoney AS DECIMAL(18,2)) END [fixedExcessLimit]
								,CASE WHEN @CountryID IN (233,39) AND p.Wellness = 0 AND p.productID = 12 THEN CAST(tdvCoInsurance.ValueMoney AS DECIMAL(18,2)) END	[coInsuranceLimit]
								,CASE WHEN p.Wellness = 1 THEN p.StartDate ELSE tdvFromDate.ValueDateTime END [startDate]
								,CASE WHEN p.Wellness = 1 THEN p.EndDate ELSE tdvToDate.ValueDateTime END [endDate]
								,CASE WHEN mdvStatus.ValueInt = 43003 AND mdvDateEnd.ValueDate <= mdvDateInc.ValueDate THEN 3 
								WHEN mdvStatus.ValueInt = 43003 AND mdvDateEnd.ValueDate > mdvDateInc.ValueDate THEN 2
								ELSE 1 END [status]
						FROM 
						@Products p 
						JOIN MatterDetailValues mdvProd WITH (NOLOCK) ON mdvProd.MatterID = p.matterid
						INNER JOIN TableRows tr							WITH (NOLOCK) on tr.MatterID = p.MatterID					AND tr.DetailFieldID			= 170033 /*Historical Policy*/
						INNER JOIN TableDetailValues tdvFromDate		WITH (NOLOCK) on tdvFromDate.TableRowID = tr.TableRowID		AND tdvFromDate.DetailFieldID	= 145663 /*Start*/
						INNER JOIN TableDetailValues tdvToDate			WITH (NOLOCK) on tdvToDate.TableRowID = tr.TableRowID		AND tdvToDate.DetailFieldID		= 145664 /*End*/
						INNER JOIN TableDetailValues tdvIncDate			WITH (NOLOCK) on tdvIncDate.TableRowID = tr.TableRowID		AND tdvIncDate.DetailFieldID	= 145662 /*Inception*/
						INNER JOIN TableDetailValues tdvStatus			WITH (NOLOCK) on tdvStatus.TableRowID = tr.TableRowID		AND tdvStatus.DetailFieldID		= 145666 /*Status*/
						INNER JOIN LookupListItems	 lliStatus			WITH (NOLOCK) on lliStatus.LookupListItemID = tdvStatus.ValueInt
						INNER JOIN ResourceListDetailValues rdvProd		WITH (NOLOCK) on rdvProd.ResourceListID = mdvProd.ValueInt	AND rdvProd.DetailFieldID		= 146200 /*Product*/
						INNER JOIN TableDetailValues tdvExcess			WITH (NOLOCK) ON tdvExcess.TableRowID = tr.TableRowID		AND tdvExcess.DetailFieldID		= 177416 /*Excess*/
						INNER JOIN TableDetailValues tdvCoInsurance		WITH (NOLOCK) ON tdvCoInsurance.TableRowID = tr.TableRowID	AND tdvCoInsurance.DetailFieldID = 177415 /*CoInsurance*/
						INNER JOIN MatterDetailValues mdvDateInc		WITH (NOLOCK) ON mdvDateInc.MatterID = p.MatterID					AND mdvDateInc.DetailFieldID		= 170035 /*Policy Inception Date*/
						INNER JOIN MatterDetailValues mdvDateEnd		WITH (NOLOCK) on mdvDateEnd  .MatterID = p.MatterID					AND mdvDateEnd.DetailFieldID		= 170037 /*Policy End Date*/
						INNER JOIN MatterDetailValues mdvStatus			WITH (NOLOCK) on mdvStatus.MatterID = p.MatterID					AND mdvStatus.DetailFieldID			= 170038 /*Policy Status*/
						JOIN OneVision oneVision						WITH (NOLOCK) ON oneVision.PAMatterID = mdvProd.MatterID	AND oneVision.PolicyTermType = 'Policy'	  /*One Vision Table*/
						WHERE p.MatterID = @PolicyID
						AND mdvProd.DetailFieldID = 170034
						ORDER BY tr.TableRowID DESC
						FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
					 

		)
	END

	RETURN @JsonData

END
GO
