SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Robin Hall
-- Create date: 2013-03-27
-- Description:	Check for level of cover change
-- 2013-05-10 JWG Added OPTION (RECOMPILE) to each select in this function. It was caching plans that caused huge slow downs at times (20 seconds to return instead of sub-second) 
-- 2013-10-23 ROH Revised rules for upgrade/downgrade claims
-- 2014-05-09 ROH Post Office rules
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_CheckUpgradeDowngrade]
(
	@ClaimMatterID INT
	,@DateOfLoss DATE
	,@Treatmentstart DATE
)
RETURNS INT	
AS
BEGIN

	DECLARE @SchemeAtLoss VARCHAR(10),
			@SchemeAtTreatment VARCHAR(10),
			@LevelAtLoss INT,
			@LevelAtTreatment INT
	
	DECLARE @ReturnCode INT
		
	-- Get the SchemeCodes in effect at date of loss and treatment start
	
	SELECT @SchemeAtLoss= lliSchemeCode.ItemValue, @LevelAtLoss = ISNULL(rldvSchemeProductLevel.ValueInt,0)
	FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@ClaimMatterID) r
	INNER JOIN dbo.TableDetailValues tdvScheme WITH (NOLOCK) ON r.TableRowID = tdvScheme.TableRowID AND tdvScheme.DetailFieldID = 145665
	INNER JOIN dbo.ResourceListDetailValues rldvSchemeCode WITH (NOLOCK) ON rldvSchemeCode.ResourceListID = tdvScheme.ResourceListID AND rldvSchemeCode.DetailFieldID = 144318
	INNER JOIN dbo.LookupListItems lliSchemeCode WITH (NOLOCK) ON lliSchemeCode.lookuplistitemid = rldvSchemeCode.ValueInt
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664
	LEFT JOIN dbo.ResourceListDetailValues rldvSchemeProductCode WITH (NOLOCK) ON rldvSchemeProductCode.DetailValue = lliSchemeCode.ItemValue AND rldvSchemeProductCode.DetailFieldID = 162647
	LEFT JOIN dbo.ResourceListDetailValues rldvSchemeProductLevel WITH (NOLOCK) ON rldvSchemeProductLevel.ResourceListID = rldvSchemeProductCode.ResourceListID AND rldvSchemeProductLevel.DetailFieldID = 162649
	WHERE @DateOfLoss IS NOT NULL
	AND @DateOfLoss BETWEEN tdvStart.ValueDate AND tdvEnd.ValueDate 
	OPTION (RECOMPILE)
	
	SELECT @SchemeAtTreatment = lliSchemeCode.ItemValue, @LevelAtTreatment = ISNULL(rldvSchemeProductLevel.ValueInt,1)
	FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@ClaimMatterID) r
	INNER JOIN dbo.TableDetailValues tdvScheme WITH (NOLOCK) ON r.TableRowID = tdvScheme.TableRowID AND tdvScheme.DetailFieldID = 145665
	INNER JOIN dbo.ResourceListDetailValues rldvSchemeCode WITH (NOLOCK) ON rldvSchemeCode.ResourceListID = tdvScheme.ResourceListID AND rldvSchemeCode.DetailFieldID = 144318
	INNER JOIN dbo.LookupListItems lliSchemeCode WITH (NOLOCK) ON lliSchemeCode.lookuplistitemid = rldvSchemeCode.ValueInt
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664
	LEFT JOIN dbo.ResourceListDetailValues rldvSchemeProductCode WITH (NOLOCK) ON rldvSchemeProductCode.DetailValue = lliSchemeCode.ItemValue AND rldvSchemeProductCode.DetailFieldID = 162647
	LEFT JOIN dbo.ResourceListDetailValues rldvSchemeProductLevel WITH (NOLOCK) ON rldvSchemeProductLevel.ResourceListID = rldvSchemeProductCode.ResourceListID AND rldvSchemeProductLevel.DetailFieldID = 162649
	WHERE @TreatmentStart IS NOT NULL
	AND @TreatmentStart BETWEEN tdvStart.ValueDate AND tdvEnd.ValueDate
	OPTION (RECOMPILE)
	
	IF (@SchemeAtLoss = @SchemeAtTreatment OR @LevelAtLoss = @LevelAtTreatment)
	BEGIN
		-- No change of cover 
		SELECT @ReturnCode = 0
	END
	ELSE
	BEGIN
	
		DECLARE @CoverChangeDate DATE
		,@Underwriter VARCHAR(50)
		,@Affinity VARCHAR(50)
		
		;WITH PolicyHistory AS (
		-- All PolicyYears relevant between DateOfLoss and TreatmentStart
		SELECT 
			rPolicyYear.TableRowID, tdvPolStart.ValueDate PolicyStart, tdvPolEnd.ValueDate PolicyEnd, tdvSchemeRL.ResourceListID SchemeRLID, rldvSchemeProductLevel.ValueInt CoverLevel, lliUW.ItemValue Underwriter, rldvAff.DetailValue [Affinity]
			,ROW_NUMBER() OVER(ORDER BY tdvPolstart.valuedate) AS rnYear
			,ROW_NUMBER() OVER(PARTITION BY rldvSchemeProductLevel.ValueInt ORDER BY tdvPolStart.valuedate) AS rnProduct
		FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@ClaimMatterID) rPolicyYear
		INNER JOIN dbo.TableDetailValues tdvPolStart WITH (NOLOCK) ON tdvPolStart.TableRowID = rPolicyYear.TableRowID AND tdvPolStart.DetailFieldID = 145663
		INNER JOIN dbo.TableDetailValues tdvPolEnd WITH (NOLOCK) ON tdvPolEnd.TableRowID = rPolicyYear.TableRowID AND tdvPolEnd.DetailFieldID = 145664
		INNER JOIN dbo.TableDetailValues tdvSchemeRL WITH (NOLOCK) ON tdvSchemeRL.TableRowID = rPolicyYear.TableRowID AND tdvSchemeRL.DetailFieldID = 145665
		INNER JOIN dbo.ResourceListDetailValues rldvSchemeCode WITH (NOLOCK) ON rldvSchemeCode.ResourceListID = tdvSchemeRL.ResourceListID AND rldvSchemeCode.DetailFieldID = 144318
		INNER JOIN dbo.LookupListItems lliSchemeCode WITH (NOLOCK) ON lliSchemeCode.LookupListItemID = rldvSchemeCode.ValueInt
		LEFT JOIN dbo.ResourceListDetailValues rldvSchemeProductCode WITH (NOLOCK) ON rldvSchemeProductCode.DetailValue = lliSchemeCode.ItemValue AND rldvSchemeProductCode.DetailFieldID = 162647
		LEFT JOIN dbo.ResourceListDetailValues rldvSchemeProductLevel WITH (NOLOCK) ON rldvSchemeProductLevel.ResourceListID = rldvSchemeProductCode.ResourceListID AND rldvSchemeProductLevel.DetailFieldID = 162649
		INNER JOIN dbo.ResourceListDetailValues rldvUW WITH (NOLOCK) ON rldvUW.ResourceListID = tdvSchemeRL.ResourceListID AND rldvUW.DetailFieldID = 145661
		INNER JOIN dbo.LookupListItems lliUW WITH (NOLOCK) ON lliUW.LookupListItemID = rldvUW.ValueInt 
		INNER JOIN dbo.ResourceListDetailValues rldvAff WITH (NOLOCK) ON rldvAff.ResourceListID = tdvSchemeRL.ResourceListID AND rldvAff.DetailFieldID = 144314
		WHERE tdvPolEnd.ValueDate >= @DateOfLoss AND tdvPolStart.ValueDate <= @TreatmentStart
		)
		-- Get the renewal date of the first change in cover level
		SELECT TOP 1 @CoverChangeDate = p.PolicyStart, @Underwriter = p.Underwriter, @Affinity = p.[Affinity]
		FROM PolicyHistory p
		WHERE rnYear > 1 AND rnProduct= 1 
		ORDER BY PolicyStart
		OPTION(RECOMPILE)
		
		-- We can allow a client level override here if required
		DECLARE @ClientOverride_NoChangeOfCoverAllowed BIT = 0
		
		IF (@ClientOverride_NoChangeOfCoverAllowed = 1)
		BEGIN
			-- No cover
			SELECT @ReturnCode = NULL
		END
		ELSE
		BEGIN
			SELECT @ReturnCode = CASE 
									WHEN @LevelAtLoss < @LevelAtTreatment THEN 1
									WHEN @LevelAtLoss > @LevelAtTreatment THEN -1
									END
		END
		
	END

	RETURN @ReturnCode
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_CheckUpgradeDowngrade] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_CheckUpgradeDowngrade] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_CheckUpgradeDowngrade] TO [sp_executeall]
GO
