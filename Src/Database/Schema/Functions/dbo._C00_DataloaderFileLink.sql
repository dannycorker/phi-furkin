SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-08-11
-- Description: returns html url to a DataloaderFile
-- =============================================
CREATE FUNCTION [dbo].[_C00_DataloaderFileLink]
(
	@DataloaderFileID int 
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE @ClientID int 
			,@FileLink VARCHAR(2000) = '<p><a href="https://aqnet.aquarium-software.com/Dataloader/'
	SELECT @FileLink = @FileLink + 'Passed' + '/Client_' + CAST(df.clientid as varchar(10)) + '/' + Cast (@DataloaderFileID as varchar(90)) +
	RIGHT(DF.targetfilename,(CHARINDEX('.',reverse(df.TargetFileName),0)))
	+ '">Download Dataloaded File</a></p>'
	FROM DataLoaderFile df WITH (NOLOCK) 
	WHERE df.DataLoaderFileID = @DataloaderFileID

	RETURN @FileLink

END



GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DataloaderFileLink] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_DataloaderFileLink] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DataloaderFileLink] TO [sp_executeall]
GO
