SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire
-- Create date:	2020-07-30
-- Description:	harvests rollback policy term dates json object
-- 2020-10-01 ALM Included ProductID
-- 2020-10-14 ALM Included Excess and CoInsurance
-- 2021-02-05 ALM Include new status for vision: 1 Active, 2 Cancelled, 3 Cancelled to Inception
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Mediate_Harvest_RollbackPolicyTerm]
(
	 @PolicyID			INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE  @JsonData	VARCHAR(MAX) =''

	DECLARE @Products Table (ProductID INT, MatterID INT, Wellness INT, StartDate DATE, EndDate DATE)

	INSERT INTO @Products
	SELECT mdvClaimProductID.ValueInt, @PolicyID, 0, NULL, NULL 
	FROM MatterDetailValues mdvClaimProductID WITH (NOLOCK)
	WHERE mdvClaimProductID.MatterID = dbo.fn_C00_1273_GetCurrentSchemeFromMatter (@PolicyID)
	AND mdvClaimProductID.DetailFieldID = 313875

	IF @PolicyID > 0
	BEGIN
		SELECT  @JsonData +=
		(
			SELECT TOP 1
						p.productid [productId]
						,CASE WHEN p.productID = 12 THEN CAST(tdvExcess.ValueMoney AS DECIMAL(18,2)) END [fixedExcessLimit]
						,CASE WHEN p.productID = 12 THEN CAST(tdvCoInsurance.ValueMoney AS DECIMAL(18,2)) END [coInsuranceLimit]
						,mdvDateStart.ValueDateTime		[startDate]
						,mdvDateEnd.ValueDateTime		[endDate]
						,CASE WHEN mdvStatus.ValueInt = 43003 AND mdvDateEnd.ValueDate <= mdvDateInc.ValueDate THEN 3 
						WHEN mdvStatus.ValueInt = 43003 AND mdvDateEnd.ValueDate > mdvDateInc.ValueDate THEN 2
						ELSE 1 END [status]
			FROM @Products p
			INNER JOIN Matter m								WITH (NOLOCK) ON m.MatterID = p.MatterID 
			INNER JOIN Lead l								WITH (NOLOCK) ON l.LeadID = m.LeadID
			INNER JOIN OneVision ov							WITH (NOLOCK) ON ov.PAMatterID = m.MatterID							AND ov.PolicyTermType = 'Policy'
			INNER JOIN MatterDetailValues mdvDateStart		WITH (NOLOCK) on mdvDateStart.MatterID = m.MatterID					AND mdvDateStart.DetailFieldID		= 170036 /*Policy Start Date*/
			INNER JOIN MatterDetailValues mdvDateEnd		WITH (NOLOCK) on mdvDateEnd  .MatterID = m.MatterID					AND mdvDateEnd.DetailFieldID		= 170037 /*Policy End Date*/
			INNER JOIN TableRows tr							WITH (NOLOCK) on tr.MatterID = m.MatterID							AND tr.DetailFieldID				= 170033 /*Historical Policy*/
			INNER JOIN TableDetailValues tdvExcess			WITH (NOLOCK) ON tdvExcess.TableRowID = tr.TableRowID				AND tdvExcess.DetailFieldID			= 177416 /*Excess*/
			INNER JOIN TableDetailValues tdvCoInsurance		WITH (NOLOCK) ON tdvCoInsurance.TableRowID = tr.TableRowID			AND tdvCoInsurance.DetailFieldID	= 177415 /*CoInsurance*/
			INNER JOIN MatterDetailValues mdvDateInc		WITH (NOLOCK) ON mdvDateInc.MatterID = m.MatterID					AND mdvDateInc.DetailFieldID		= 170035 /*Policy Inception Date*/
			INNER JOIN MatterDetailValues mdvStatus			WITH (NOLOCK) on mdvStatus.MatterID = m.MatterID					AND mdvStatus.DetailFieldID			= 170038 /*Policy Status*/
			WHERE m.MatterID = @PolicyID
			AND l.LeadTypeID = 1492 /*Policy Admin*/

			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		)
	END


	RETURN @JsonData

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Mediate_Harvest_RollbackPolicyTerm] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_Mediate_Harvest_RollbackPolicyTerm] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Mediate_Harvest_RollbackPolicyTerm] TO [sp_executeall]
GO
