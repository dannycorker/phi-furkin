SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2014-04-25
-- Description:	JSON shredding demo
-- =============================================
CREATE FUNCTION [dbo].[fnJSONShredToTable]
(
	@JSON VARCHAR(MAX) 
)
RETURNS 
@NameValuePairs TABLE 
(
	AnyID INT IDENTITY(1, 1), 
	RawText VARCHAR(2000), 
	NamePart VARCHAR(2000), 
	ValuePart VARCHAR(2000)
)
AS
BEGIN
	
	DECLARE @ClientID INT

	SELECT @ClientID = dbo.fnGetPrimaryClientID()

	INSERT INTO @NameValuePairs (RawText) 
	SELECT f.AnyValue 
	FROM dbo.fnTableOfValuesFromCSV(@JSON) f 

	UPDATE @NameValuePairs 
	SET RawText = REPLACE(REPLACE(RawText, CHAR(10), ''), CHAR(13), '')

	UPDATE @NameValuePairs 
	SET RawText = LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(RawText, '{', ''), '}', ''), '[', ''), ']', '')))

	UPDATE @NameValuePairs 
	SET NamePart = REPLACE(dbo.fnGetCharsBeforeOrAfterSeparator(RawText, ':', 1, 1), '"', '')

	UPDATE @NameValuePairs 
	SET ValuePart = REPLACE(dbo.fnGetCharsBeforeOrAfterSeparator(RawText, ':', 1, 0), '"', '')

	IF @ClientID = 605
	BEGIN 
		UPDATE @NameValuePairs 
		SET NamePart = REPLACE(dbo.fnGetCharsBeforeOrAfterSeparator(RawText, ':', 0, 1), '"', '')

		UPDATE @NameValuePairs 
		SET ValuePart = REPLACE(dbo.fnGetCharsBeforeOrAfterSeparator(RawText, ':', 0, 0), '"', '')
	END 

	RETURN 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnJSONShredToTable] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnJSONShredToTable] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnJSONShredToTable] TO [sp_executeall]
GO
