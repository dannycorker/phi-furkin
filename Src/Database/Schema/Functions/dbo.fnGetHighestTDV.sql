SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-07-05
-- Description:	Function to return the highest value from a table
-- =============================================
CREATE FUNCTION [dbo].[fnGetHighestTDV]
(
	@MatterID INT,
	@TableFieldID INT,
	@ColumnFieldID INT
)
RETURNS NUMERIC(18,2)
AS
BEGIN

	DECLARE @MaxValue NUMERIC(18,2)
	
	/*
		For a particular Matter, look through all the rows on the specified table (@TableFieldID)
		Return the highest money value in the column specified (@ColumnFieldID)
	*/
	SELECT @MaxValue = MAX(tdv.ValueMoney) 
	FROM dbo.TableRows tr WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID =  @ColumnFieldID 
	WHERE tr.MatterID = @MatterID 
	AND tr.DetailFieldID = @TableFieldID 
	
	/* This can be NULL */
	RETURN @MaxValue

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetHighestTDV] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetHighestTDV] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetHighestTDV] TO [sp_executeall]
GO
