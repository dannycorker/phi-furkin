SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-02-19
-- Description:	Return the case id of a case if it was created today from a customer id
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_GetCaseCreatedTodayFromCustomerRef]
(
	@CustomerREF VARCHAR(200),
	@LeadTypeID INT,
	@ClientID INT
)
RETURNS int
AS
BEGIN

	DECLARE @CaseID INT = 0

	SELECT @CaseID = ca.CaseID
	FROM Customers c WITH (NOLOCK)
	INNER JOIN Lead l ON l.CustomerID = c.CustomerID AND l.LeadTypeID = @LeadTypeID
	INNER JOIN Cases ca WITH (NOLOCK) ON ca.LeadID = l.LeadID and ca.WhenCreated > CONVERT(DATE,dbo.fn_GetDate_Local())
	WHERE c.ClientID = @ClientID
	AND c.CustomerRef = @CustomerRef
	AND c.Test = 0
	
	RETURN @CaseID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetCaseCreatedTodayFromCustomerRef] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_GetCaseCreatedTodayFromCustomerRef] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetCaseCreatedTodayFromCustomerRef] TO [sp_executeall]
GO
