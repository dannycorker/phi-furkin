SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-06-17
-- Description:	Get Start or end of month
-- =============================================
CREATE FUNCTION [dbo].[fnGetStartorEndOfMonth]
(
@ReturnEnd int = 0
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here

	Declare @ReturnDate datetime

	IF @ReturnEnd = 0
	BEGIN

		SELECT @ReturnDate = convert(varchar,DATEPART(yyyy,dateadd(mm,-1,dbo.fn_GetDate_Local()))) + '-' + convert(varchar,DATEPART(mm,dateadd(mm,-1,dbo.fn_GetDate_Local()))) + '-01'
		
	END

	IF @ReturnEnd = 1
	BEGIN
	
		SELECT @ReturnDate = dateadd(ms,-3,convert(varchar,DATEPART(yyyy,dbo.fn_GetDate_Local())) + '-' + convert(varchar,DATEPART(mm,dbo.fn_GetDate_Local())) + '-01')
	
	END

	Return @ReturnDate

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetStartorEndOfMonth] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetStartorEndOfMonth] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetStartorEndOfMonth] TO [sp_executeall]
GO
