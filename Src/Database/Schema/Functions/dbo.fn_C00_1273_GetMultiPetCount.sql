SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-01-08
-- Description:	Returns the number of active accident / illness policies for a customer
-- DCM 2014-09-15 Copied from 235
-- DCM 2015-10-19 refer to fn_C00_1272_Policy_GetPetList
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetMultiPetCount]
(
	@CustomerID INT
)
RETURNS INT	
AS
BEGIN
 
	-- BEU needs updating so that 384's ruling that first pet doesn't qualify
	RETURN ( SELECT COUNT(*) FROM dbo.fn_C00_1272_Policy_GetPetList(@CustomerID) )
 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetMultiPetCount] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetMultiPetCount] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetMultiPetCount] TO [sp_executeall]
GO
