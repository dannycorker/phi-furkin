SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2019-06-28
-- Description:	Take the value of BuyPolicyRequest/CustomerInfo/DoNot{x} and flipps logic for C600 EQB
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_MarketingPreferenceRealignment] 
(
	@DoNotValue BIT,
	@IsEQB BIT,
	@IsAgg BIT
)
RETURNS BIT
AS
BEGIN

	DECLARE @MarketingPreference BIT
	
	IF @IsEQB = 1 
	BEGIN
		SELECT @MarketingPreference = CASE @DoNotValue WHEN 1 THEN 1 ELSE 0 END
	END
	
	IF @IsEQB = 0
	BEGIN
		SELECT @MarketingPreference = CASE ISNULL(@DoNotValue,1) WHEN 1 THEN 0 ELSE 1 END
	END
	
	/*If Aggregator = 1 then replace Marketing preference with false*/
	IF @IsAgg = 1
	BEGIN
		SELECT @MarketingPreference = CASE @DoNotValue WHEN 1 THEN 1 ELSE 0 END /*GPR / AMG / CR 2019-09-20*/
	END

	/*Value to use*/
	RETURN @MarketingPreference

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_MarketingPreferenceRealignment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_MarketingPreferenceRealignment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_MarketingPreferenceRealignment] TO [sp_executeall]
GO
