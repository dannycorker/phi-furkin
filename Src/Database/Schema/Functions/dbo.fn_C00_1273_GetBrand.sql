SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-08-04
-- Description:	Returns the brand
-- GPR 2018-02 removed hardcoded @Brand value used previously for 384 testing
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetBrand]
(
	@CustomerID INT
)
RETURNS VARCHAR(200)	
AS
BEGIN

	DECLARE @Brand VARCHAR(200)
	
	-- BEU add DF to resource list and update DFID 

	SELECT @Brand=br.DetailValue 
	FROM CustomerDetailValues aff WITH (NOLOCK)
	INNER JOIN ResourceListDetailValues br WITH (NOLOCK) ON aff.ValueInt=br.ResourceListID AND br.DetailFieldID=176965
	WHERE aff.CustomerID=@CustomerID AND aff.DetailFieldID=170144
	
	RETURN @Brand
 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetBrand] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetBrand] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetBrand] TO [sp_executeall]
GO
