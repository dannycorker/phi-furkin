SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-07-16
-- Description:	Return a table of row number, column number, varchar cell value from an input string
-- =============================================
CREATE FUNCTION [dbo].[fnTableOfRowColumnValueFromCSV]
(
	@instr NVARCHAR(MAX),
	@NumberColumns INT,
	@delimiter NCHAR(1) = ','
)
RETURNS
@TableOut TABLE 
(
	RowNumber INT,
	ColumnNumber INT,
	AnyValue NVARCHAR(MAX)
)
AS
BEGIN

	DECLARE @RowNumber INT = 0,
			@ColumnNumber INT = 0

	DECLARE @testvalue NVARCHAR(MAX)
	DECLARE @position  INT

	SET @instr = LTRIM(RTRIM(@instr)) + ','
	SET @position = CHARINDEX(@delimiter, @instr, 1)

	IF REPLACE(@instr, @delimiter, '') <> ''
	BEGIN
		WHILE @position > 0
		BEGIN
		
			SELECT @RowNumber += 1, @ColumnNumber=0
			
			WHILE @position > 0 AND @ColumnNumber < @NumberColumns
			BEGIN
			
				SELECT @ColumnNumber += 1
				INSERT INTO @TableOut (RowNumber,ColumnNumber,AnyValue) 
				SELECT @RowNumber,@ColumnNumber,REPLACE(REPLACE(LTRIM(RTRIM(LEFT(@instr, @position - 1))),CHAR(10),''),CHAR(13),'')

				SET @instr = RIGHT(@instr, LEN(@instr) - @position)
				SET @position = CHARINDEX(@delimiter, @instr, 1)

			END
		END	
	END
	
	RETURN
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfRowColumnValueFromCSV] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnTableOfRowColumnValueFromCSV] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfRowColumnValueFromCSV] TO [sp_executeall]
GO
