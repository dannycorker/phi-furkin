SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-12-11
-- Description:	Get Customer DetailValue
-- =============================================
CREATE FUNCTION [dbo].[fnGetCustomerDv]
(
	@DetailFieldID		int,
	@CustomerID			int
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE @DetailValue as varchar(2000)

	Select @DetailValue = cdv.DetailValue 
	From CustomerDetailValues cdv with (nolock) 
	Where cdv.CustomerID = @CustomerID 
	and cdv.DetailFieldID = @DetailFieldID
	
	RETURN @DetailValue

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerDv] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetCustomerDv] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerDv] TO [sp_executeall]
GO
