SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Nessa Green
-- Create date: 2020-12-02
-- Description:	Return payment schedule totals
-- JEL 2020-12-04 changed selects to work off the ProductID so we don't repeat function calls. Also added catch logic for future dated policies
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_PaymentScheduleTotals]
(
	@MatterID		INT,
	@Value			INT
)
RETURNS MONEY
AS
BEGIN

	DECLARE 
	@TotalAmount		VARCHAR(100)
	,@ProductID			INT


	/*Select the total amount you want to return*/

	SELECT @ProductID = pp.PurchasedProductID FROM PurchasedProduct pp WITH (NOLOCK)
	WHERE pp.ValidFrom < dbo.fn_GetDate_Local()
	AND pp.ValidTo > dbo.fn_GetDate_Local()
	AND pp.ObjectID = @MatterID

	/*If we can't find a product via that method, it is a future dated productID*/ 
	IF @ProductID IS NULL 
	BEGIN 
		
		SELECT TOP 1 @ProductID = pp.PurchasedProductID FROM PurchasedProduct pp with (NOLOCK)
		WHERE pp.ObjectID = @MatterID 
		AND pp.ValidTo > dbo.fn_GetDate_Local() 

	END

	IF @Value = 1 /*Net Annual Cost of Premium*/

	BEGIN

		SELECT @TotalAmount = SUM(ppps.paymentnet)
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
		INNER JOIN PurchasedProduct pp  WITH (NOLOCK) on ppps.PurchasedProductID = pp.PurchasedProductID
		WHERE pp.ObjectID = @MatterID
		AND pp.PurchasedProductID = @ProductID
	END

	IF @Value = 2 /*Annual Tax for Premium*/

	BEGIN

		SELECT @TotalAmount = SUM(ppps.PaymentVAT)
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
		INNER JOIN PurchasedProduct pp  WITH (NOLOCK) on ppps.PurchasedProductID = pp.PurchasedProductID
		WHERE pp.ObjectID = @MatterID
		AND pp.PurchasedProductID = @ProductID

	END

	IF @Value = 3 /*Gross Annual Cost of Premium*/

	BEGIN

		SELECT @TotalAmount = SUM(ppps.PaymentGross)
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
		INNER JOIN PurchasedProduct pp  WITH (NOLOCK) on ppps.PurchasedProductID = pp.PurchasedProductID
		WHERE pp.ObjectID = @MatterID
		AND pp.PurchasedProductID = @ProductID

	END

	IF @Value = 4 /*Net Annual Remaining Premium Cost*/

	BEGIN

		SELECT @TotalAmount = SUM(ppps.paymentnet)
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
		INNER JOIN PurchasedProduct pp  WITH (NOLOCK) on ppps.PurchasedProductID = pp.PurchasedProductID
		WHERE pp.ObjectID = @MatterID
		AND pp.PurchasedProductID = @ProductID
		AND ppps.PaymentStatusID = 1

	END

	IF @Value = 5 /*Annual Tax Remaining for Premium*/

	BEGIN

		SELECT @TotalAmount = SUM(ppps.PaymentVAT)
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
		INNER JOIN PurchasedProduct pp  WITH (NOLOCK) on ppps.PurchasedProductID = pp.PurchasedProductID
		WHERE pp.ObjectID = @MatterID
		AND pp.PurchasedProductID = @ProductID
		AND ppps.PaymentStatusID = 1

	END

	IF @Value = 6 /*Gross Annual Remaining Premium Cost*/

	BEGIN

		SELECT @TotalAmount = SUM(ppps.PaymentGross)
		FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
		INNER JOIN PurchasedProduct pp  WITH (NOLOCK) on ppps.PurchasedProductID = pp.PurchasedProductID
		WHERE pp.ObjectID = @MatterID
		AND pp.PurchasedProductID = @ProductID
		AND ppps.PaymentStatusID = 1

	END


	RETURN @TotalAmount

END





GO
