SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2019-11-13
-- Description:	Gets a list of all affinities - helpful when requiring BrandID in writing procs/functions
-- =============================================
CREATE FUNCTION [dbo].[_C00_AffinityDetails]
(	
	@ClientID INT,
	@AffinityID INT NULL
)
RETURNS @Data TABLE
(
	BrandID INT,
	ShortCode VARCHAR(10),
	CompanyName VARCHAR(50),
	AffinityBrand VARCHAR(50)
)
AS
BEGIN

	DECLARE @BrandID INT
			,@ShortCode INT
			,@CompanyName  VARCHAR(50)
			,@AffinityBrand VARCHAR(50)


	IF @AffinityID IS NOT NULL
	BEGIN

		INSERT	@Data (BrandID, ShortCode, CompanyName, AffinityBrand)
		SELECT	ISNULL(brandid.ResourceListID,0),
				ISNULL(shortcodeli.ItemValue,''),
				ISNULL(companyname.DetailValue,''),
				ISNULL(affinitybrand.DetailValue,'')
		FROM ResourceListDetailValues brandid WITH (NOLOCK)
		INNER JOIN LookupListItems shortcodeli WITH (NOLOCK) ON shortcodeli.LookupListItemID = brandid.ValueInt
		INNER JOIN ResourceListDetailValues companyname WITH (NOLOCK) ON brandid.ResourceListID = companyname.ResourceListID AND companyname.DetailFieldID = 170128
		INNER JOIN ResourceListDetailValues affinitybrand WITH (NOLOCK) ON brandid.ResourceListID = affinitybrand.ResourceListID AND affinitybrand.DetailFieldID = 176965
		WHERE brandid.DetailfieldID = 170127 /*Affinity Details*/
		AND brandid.ClientID = @ClientID
		AND companyname.ClientID = @ClientID
		AND affinitybrand.ClientID = @ClientID
		AND brandid.ResourcelistID = @AffinityID

	END
	ELSE
	BEGIN

		INSERT	@Data (BrandID, ShortCode, CompanyName, AffinityBrand)
		SELECT	ISNULL(brandid.ResourceListID,0),
				ISNULL(shortcodeli.ItemValue,''),
				ISNULL(companyname.DetailValue,''),
				ISNULL(affinitybrand.DetailValue,'')
		FROM ResourceListDetailValues brandid WITH (NOLOCK)
		INNER JOIN LookupListItems shortcodeli WITH (NOLOCK) ON shortcodeli.LookupListItemID = brandid.ValueInt
		INNER JOIN ResourceListDetailValues companyname WITH (NOLOCK) ON brandid.ResourceListID = companyname.ResourceListID AND companyname.DetailFieldID = 170128
		INNER JOIN ResourceListDetailValues affinitybrand WITH (NOLOCK) ON brandid.ResourceListID = affinitybrand.ResourceListID AND affinitybrand.DetailFieldID = 176965
		WHERE brandid.DetailfieldID = 170127 /*Affinity Details*/
		AND brandid.ClientID = @ClientID
		AND companyname.ClientID = @ClientID
		AND affinitybrand.ClientID = @ClientID

	END

RETURN

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AffinityDetails] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[_C00_AffinityDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_AffinityDetails] TO [sp_executeall]
GO
