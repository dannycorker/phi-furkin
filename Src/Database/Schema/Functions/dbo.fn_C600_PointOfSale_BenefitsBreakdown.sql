SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		James Lewis
-- Create date: 2016-11-01
-- Description:	Returns the full amount paid to a policy section
-- 2017-05-24 CPS Updated to 433 from 384
-- 2017-06-15 CPS Handle Excluded policy sections
-- 2017-08-22 CPS use dbo.fn_GetDate_Local instead of inception date for #44801
-- 2017-12-   SA  Updated to 600 from 433 (GPR added comment)
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- 2020-05-07 NG  for T-Pet Localisation change currency symbol based on country
-- 2020-09-16 NG  updated to use CoPay from table on Scheme
-- 2020-09-29 NG  updated to order benefits by RLID
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_PointOfSale_BenefitsBreakdown]
(
	@MatterID INT
)
RETURNS 
@Table TABLE 
(
	 ParentSection	VARCHAR(100)
	,Amount			VARCHAR(50)
	,Excess			VARCHAR(100)
	,CoPay			VARCHAR(100)
	,ResourceListID	INT
)
AS
BEGIN

	DECLARE  @PostCode			VARCHAR (30)
			,@BirthDate			DATE
			,@SpeciesID			INT
			,@Breed				VARCHAR(30)
			,@PolStart			DATE 
			,@LeadID			INT
			,@SchemeMatterID	INT 
			,@Aggressive		BIT
			,@ClientID			INT = dbo.fnGetPrimaryClientID()
			
	SELECT @LeadID = m.LeadID 
	FROM Matter m WITH ( NOLOCK ) 
	WHERE m.MatterID = @MatterID


	/*GPR 2020-10-16 for SDPRU-97*/
	DECLARE @SelectedCoPay VARCHAR(10)
	DECLARE @SelectedExcess VARCHAR(10)

	SELECT @SelectedCoPay = ldvCoPay.DetailValue, @SelectedExcess = ldvExcess.DetailValue
	FROM Matter m WITH (NOLOCK)
	INNER JOIN LeadDetailValues ldvCoPay WITH (NOLOCK) ON ldvCoPay.LeadID = m.LeadID
	INNER JOIN LeadDetailValues ldvExcess WITH (NOLOCK) ON ldvExcess.LeadID = m.LeadID
	WHERE ldvCoPay.DetailFieldID = 313930
	AND ldvExcess.DetailFieldID = 313932
	AND m.MatterID = @MatterID
	
	SELECT @Aggressive = 1
	FROM TableRows tr WITH ( NOLOCK ) 
	INNER JOIN TableDetailValues tdvResponse WITH ( NOLOCK ) on tdvResponse.TableRowID = tr.TableRowID AND tdvResponse.DetailFieldID = 177306 /*Customer Response*/
	INNER JOIN TableDetailValues tdvResource WITH ( NOLOCK ) on tdvResource.TableRowID = tr.TableRowID AND tdvResource.DetailFieldID = 177126 /*Existing Condition*/
	WHERE tr.LeadID = @LeadID
	AND tr.DetailFieldID = 177128 /*Existing Conditions*/
	AND dbo.fnGetSimpleDvAsInt(177492,tdvResource.ResourceListID) = 1 /*Yes Blocks Third Party Liability Cover*/
	AND tdvResponse.ValueInt = 5144 /*Yes*/ 
		
	
	SELECT @SchemeMatterID = dbo.fn_C00_1273_GetCurrentSchemeWithSpecies	(dbo.fnGetSimpleDvAsInt(170034,@MatterID) /*Current Policy*/
																			,dbo.fnGetSimpleDvAsDate(170036,@MatterID) /*Policy Start Date*/ 
																			,dbo.fnGetSimpleDvAsInt(144269 /*Species*/, dbo.fnGetSimpleDvAsInt(144272,@LeadID) /*Pet Type*/ ) 
																			)
			
	INSERT @Table ( ParentSection, Amount, ResourceListID, CoPay ) /*copay added by NG*/
	SELECT	 ISNULL(liSubSection.ItemValue,liSection.ItemValue)
			,COALESCE(tdvNonFinancialLimit.DetailValue + ' ' + liLimitType.ItemValue
					 ,CASE WHEN cl.CountryID in (14,233, 39) THEN '$' ELSE '£' END + CONVERT(VARCHAR,CAST(CASE WHEN liSection.LookupListItemID = 74546 /*Third Party*/ AND @Aggressive = 1 THEN 0.00 ELSE tdvSum.ValueMoney END as MONEY),1)
					 ,'')
				--+ CASE 
				--	WHEN rdvSection.ResourceListID = 148140 /*Vet Fees*/ AND rdvSchemeType.ValueInt = 42933 /*Reinstatement*/ THEN ' per policy year'
				--	WHEN rdvSection.ResourceListID = 148140 /*Vet Fees*/ AND rdvSchemeType.ValueInt = 42932 /*Maximum benefit*/ THEN ' maximum'
				--	ELSE ''
				--  END
			,rdvSection.ResourceListID /*used by setdocumentfields proc*/
			,tdvcopay.DetailValue + '%' /*copay added by NG*/ /*GPR 2020-10-16 removed for SDPRU-97*/ /*NG 2020-11-03 added back in for SDPRU-128*/
	FROM TableRows tr WITH ( NOLOCK ) 
	INNER JOIN TableDetailValues tdvSection WITH ( NOLOCK ) on tdvSection.TableRowID = tr.TableRowID AND tdvSection.DetailFieldID = 144357 /*Policy Section*/
	INNER JOIN ResourceListDetailValues rdvSection WITH ( NOLOCK ) on rdvSection.ResourceListID = tdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189 /*Policy Section*/
	INNER JOIN LookupListItems liSection WITH ( NOLOCK ) on liSection.LookupListItemID = rdvSection.ValueInt
	 LEFT JOIN ResourceListDetailValues rdvSubSection WITH ( NOLOCK ) on rdvSubSection.ResourceListID = tdvSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190 /*Sub-Section*/
	 LEFT JOIN LookupListItems liSubSection WITH ( NOLOCK ) on liSubSection.LookupListItemID = rdvSubSection.ValueInt AND liSubSection.ItemValue <> '-'
	 LEFT JOIN TableDetailValues tdvSum WITH ( NOLOCK ) on tdvSum.TableRowID = tr.TableRowID AND tdvSum.DetailFieldID = 144358 /*Sum Insured*/
	 LEFT JOIN TableDetailValues tdvNonFinancialLimit WITH ( NOLOCK ) on tdvNonFinancialLimit.TableRowID = tr.TableRowID AND tdvNonFinancialLimit.DetailFieldID = 144267 /*Non financial limit*/
	 LEFT JOIN TableDetailValues tdvNonFinancialLimitType WITH ( NOLOCK ) on tdvNonFinancialLimitType.TableRowID = tr.TableRowID AND tdvNonFinancialLimitType.DetailFieldID = 175388 /*Custom Limit Type*/
	 LEFT JOIN LookupListItems liLimitType WITH ( NOLOCK ) on liLimitType.LookupListItemID = tdvNonFinancialLimitType.ValueInt
	INNER JOIN MatterDetailValues mdvProduct WITH ( NOLOCK ) on mdvProduct.MatterID = @MatterID AND mdvProduct.DetailFieldID = 170034 /*Current Policy*/
	INNER JOIN ResourceListDetailValues rdvSchemeType WITH ( NOLOCK ) on rdvSchemeType.ResourceListID = mdvProduct.ValueInt AND rdvSchemeType.DetailFieldID = 144319 /*Scheme Type*/
	INNER JOIN Clients cl WITH (NOLOCK) on tr.ClientID = cl.ClientID /*find country for localisation of currency*/
	/*copay rows added by NG*/ /*GPR 2020-10-16 removed for SDPRU-97*/ /*NG 2020-11-03 added back in for SDPRU-128*/
	LEFT JOIN TableRows trc WITH (NOLOCK) on trc.MatterID = tr.MatterID AND trc.DetailFieldID = 147195
	LEFT JOIN TableDetailvalues tdvcopaysec WITH (NOLOCK) on trc.TableRowID = tdvcopaysec.TableRowID AND tdvcopaysec.DetailFieldID = 147192 AND tdvcopaysec.ResourceListID = tdvSection.ResourceListID 
	LEFT JOIN TableDetailvalues tdvcopay WITH (NOLOCK) on tdvcopaysec.TableRowID = tdvcopay.TableRowID AND tdvcopay.DetailFieldID = 147194
	WHERE tr.MatterID = @SchemeMatterID
	AND tr.DetailFieldID = 145692 /*Policy Limits*/
	ORDER BY 3
	
	UPDATE t
	SET Amount = 'Cover not Included'/*, CoPay = '' commented out NG*/
	FROM @Table t 
	INNER JOIN TableRows tr WITH ( NOLOCK ) on tr.MatterID = @MatterID
	INNER JOIN TableDetailValues tdvSection WITH ( NOLOCK ) on tdvSection.TableRowID = tr.TableRowID AND tdvSection.DetailFieldID = 177503 /*Policy Section*/
	INNER JOIN TableDetailValues tdvFromDate WITH ( NOLOCK ) on tdvFromDate.TableRowID = tr.TableRowID AND tdvFromDate.DetailFieldID = 177513 /*Excluded From Date*/
	WHERE tr.DetailFieldID = 177506 /*Excluded Policy Sections*/
	AND tdvSection.ResourceListID = t.ResourceListID
    AND tdvFromDate.ValueDate <= dbo.fn_GetDate_Local()--dbo.fnGetSimpleDvAsDate(170035,@MatterID) /*Policy Inception Date*/
	
	UPDATE t
	SET Excess = CASE WHEN cl.CountryID in (14,233, 39) THEN '$' ELSE '£' END + ISNULL(CONVERT(VARCHAR,dbo.fnGetSimpleDvAsMoney(175503,@MatterID)),'0.00') /*Compulsory excess*/
	FROM @Table t
	INNER JOIN TableRows tr WITH ( NOLOCK ) on tr.MatterID = @MatterID
	INNER JOIN Clients cl WITH (NOLOCK) on tr.ClientID = cl.ClientID /*find country for localisation of currency*/
	WHERE t.ResourceListID = dbo.fn_C00_1272_GetVetFeesResourceList(@ClientID)
	
	UPDATE t
	SET Excess = CASE WHEN cl.CountryID in (14,233, 39) THEN '$' ELSE '£' END + ISNULL(CONVERT(VARCHAR,dbo.fnGetSimpleDvAsMoney(170051,@MatterID)),'0.00') /*Compulsory excess*/
	FROM @Table t
	INNER JOIN TableRows tr WITH ( NOLOCK ) on tr.MatterID = @MatterID
	INNER JOIN Clients cl WITH (NOLOCK) on tr.ClientID = cl.ClientID /*find country for localisation of currency*/
	WHERE t.ResourceListID = 148149
	
	UPDATE @Table
	SET CoPay = '-'
	WHERE CoPay IS NULL

	UPDATE @Table
	SET Excess = '-'
	WHERE Excess IS NULL

	/*GPR 2020-10-16 for SDPRU-97*/  /*NG 2020-11-03 amended for SDPRU-128*/
	IF @ClientID IN (605, 607) /*GPR 2021-02-01 made IN list and added 607 for FURKIN-85*/
	BEGIN
		UPDATE t
		SET CoPay = @SelectedCoPay + '%' /*Selected CoPay*/
		FROM @Table t
		WHERE t.copay <> '-'
		
		UPDATE t
		SET Excess = '$' + @SelectedExcess /*Selected Excess*/
		FROM @Table t
		WHERE t.Excess <> '-'
		

	END

	RETURN

END






















GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_PointOfSale_BenefitsBreakdown] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_PointOfSale_BenefitsBreakdown] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_PointOfSale_BenefitsBreakdown] TO [sp_executeall]
GO
