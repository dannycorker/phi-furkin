SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-09-24
-- Description:	Returns the age of a pet at the inception date from the override data passed in or looked up
--				Based on fn_C00_1273_RulesEngine_GetAgeInMonths
-- Mods
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_RulesEngine_GetAgeAtInceptionInWeeks]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS INT	
AS
BEGIN


	DECLARE @AgeInMonths INT,
			@BirthDate DATE,
			@YearStart DATE,
			@Inception DATE
	
	-- Birth date will be passed in as an override for quotes	
	SELECT @BirthDate = AnyValue2
	FROM @Overrides
	WHERE AnyID = 1
	AND AnyValue1 = '144274'
	
	-- For existing policies we need to look this up
	IF @BirthDate IS NULL
	BEGIN
	
		SELECT @BirthDate = ValueDate
		FROM dbo.LeadDetailValues WITH (NOLOCK) 
		WHERE LeadID = @LeadID
		AND DetailFieldID = 144274
	
	END
	
	-- Inception date passed in?	
	SELECT @Inception = AnyValue2
	FROM @Overrides
	WHERE AnyID = 1
	AND AnyValue1 = '170035'
	
	-- If not in overrides, check in existing policies
	IF @Inception IS NULL
	BEGIN
	
		SELECT @Inception = ValueDate
		FROM MatterDetailValues WITH (NOLOCK) 
		WHERE MatterID = @MatterID
		AND DetailFieldID = 170035
	
	END
	-- if still not set use year start
	-- Year start date is always passed in
	IF @Inception IS NULL
	BEGIN
	
		SELECT @Inception = AnyValue2
		FROM @Overrides
		WHERE AnyID = 2
		AND AnyValue1 = 'YearStart'
		
	END
	
	-- if still not set, set to now
	IF @Inception IS NULL SELECT @Inception=dbo.fn_GetDate_Local()

	SELECT @AgeInMonths =	DATEDIFF(DAY,@BirthDate, @Inception) / 7

	RETURN @AgeInMonths
 
END
GO
