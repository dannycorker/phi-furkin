SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-06-12
-- Description:	Recursively identifies all dependencies from a particular stored procedure
-- =============================================
CREATE FUNCTION [dbo].[fnStoredProceduresGetAllRecursive] 
(	
	@TopLevelStoredProcedure VARCHAR(2000) = '_C600_PA_Policy_BuyPolicy'
)
RETURNS TABLE 
AS
RETURN 
(
	WITH RecursiveCte AS
	(
	SELECT distinct OBJECT_Name(sm.object_id) [FromProc], sm.object_id [FromId], OBJECT_Name(fn.referenced_id) [DepName], fn.referenced_id [DepId], 0 [Level]
	FROM sys.sql_modules sm WITH ( NOLOCK ) 
	CROSS APPLY sys.dm_sql_referenced_entities('dbo.' + OBJECT_NAME(object_id),'OBJECT') fn
	INNER JOIN sys.sql_modules sm2 WITH ( NOLOCK ) on sm2.object_id = fn.referenced_id
	WHERE OBJECT_NAME(sm.object_id) = @TopLevelStoredProcedure
		UNION ALL
	SELECT OBJECT_Name(cte.DepId) [FromProc], cte.DepId [FromId], OBJECT_Name(fn.referenced_id) [DepName], fn.referenced_id [DepId], cte.Level+1 [Level]
	FROM RecursiveCte cte 
	CROSS APPLY sys.dm_sql_referenced_entities('dbo.' + cte.DepName,'OBJECT') fn
	INNER JOIN sys.sql_modules sm WITH ( NOLOCK ) on sm.object_id = fn.referenced_id
	WHERE @TopLevelStoredProcedure NOT IN  ( fn.referenced_entity_name , cte.DepName )
	AND cte.[Level] < 99 /*CPS 2017-10-08*/
	)
	SELECT distinct * 
	FROM RecursiveCte cte
)
GO
GRANT VIEW DEFINITION ON  [dbo].[fnStoredProceduresGetAllRecursive] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnStoredProceduresGetAllRecursive] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnStoredProceduresGetAllRecursive] TO [sp_executeall]
GO
