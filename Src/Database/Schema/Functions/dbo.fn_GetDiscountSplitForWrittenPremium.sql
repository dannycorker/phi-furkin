SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-03-06
-- Description:	Get discount split for WrittenPremium by Party (Underwriter, Agency, Distributor)
-- =============================================
CREATE FUNCTION [dbo].[fn_GetDiscountSplitForWrittenPremium]
(
	@WrittenPremiumID	INT,
	@Party										VARCHAR(11)
)
RETURNS DECIMAL(18,2)
AS
BEGIN
 
	DECLARE @ReturnValue		DECIMAL(18,2)
	,@QuotePetProductID			INT
	,@DiscountDeltaDecimal		DECIMAL(18,2)
	,@PremiumCalculationID		INT
	,@DiscountRLID				INT
	,@AccountabilityPercentage	DECIMAL(18,2)
	,@MatterID					INT

	SELECT @QuotePetProductID = QuotePetProductID, @PremiumCalculationID = PremiumCalculationID, @MatterID = MatterID
	FROM WrittenPremium WITH (NOLOCK)
	WHERE WrittenPremiumID = @WrittenPremiumID

	/*Get discount pricing delta*/
	SELECT @DiscountDeltaDecimal = ABS(qppc.DeltaDecimal) /*GPR 2020-03-11 altered to take the absolute value of the discount delta*/
	FROM QuotePetProductCheckpoint qppc WITH (NOLOCK)
	WHERE qppc.QuotePetProductID = @QuotePetProductID
	AND qppc.CheckpointName IN ('Discount')

	/*Get Discount RLID from Matter*/
	SELECT @DiscountRLID = dbo.fnGetSimpleDvAsInt(175488, @MatterID) /*Campaign Code MDV*/
	
	IF @DiscountDeltaDecimal IS NOT NULL
	BEGIN

		IF @Party = 'Underwriter'
		BEGIN

			SELECT @AccountabilityPercentage = ValueMoney
			FROM ResourceListDetailValues rldv WITH (NOLOCK)
			WHERE rldv.ResourceListID = @DiscountRLID
			AND rldv.DetailFieldID = 313850 /*Underwriter (Insurer) Percentage Split*/

			SELECT @ReturnValue = (@DiscountDeltaDecimal * (@AccountabilityPercentage / 100.00))

		END
	

		IF @Party = 'Agency'
		BEGIN

			SELECT @AccountabilityPercentage = ValueMoney
			FROM ResourceListDetailValues rldv WITH (NOLOCK)
			WHERE rldv.ResourceListID = @DiscountRLID
			AND rldv.DetailFieldID = 313851 /*Agency Percentage Split*/

			SELECT @ReturnValue = (@DiscountDeltaDecimal * (@AccountabilityPercentage / 100.00))

		END


		IF @Party = 'Distributor'
		BEGIN

			SELECT @AccountabilityPercentage = ValueMoney
			FROM ResourceListDetailValues rldv WITH (NOLOCK)
			WHERE rldv.ResourceListID = @DiscountRLID
			AND rldv.DetailFieldID = 313849 /*Distributor Percentage Split*/

			SELECT @ReturnValue = (@DiscountDeltaDecimal * (@AccountabilityPercentage / 100.00))
		END

	END


	RETURN ISNULL(@ReturnValue, 0.00) /*Remove 0.00 from Commission if NULL, otherwise return the monetary value to deduct from Commission by Party*/
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_GetDiscountSplitForWrittenPremium] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_GetDiscountSplitForWrittenPremium] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_GetDiscountSplitForWrittenPremium] TO [sp_executeall]
GO
