SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE FUNCTION [dbo].[RegionalToGmt] (@regional [datetime], @region [nvarchar] (4000))
RETURNS [datetime]
WITH EXECUTE AS CALLER
EXTERNAL NAME [SqlServerFunctions].[Aquarium.DateTime.TimeZone].[RegionalToGmt]
GO
GRANT VIEW DEFINITION ON  [dbo].[RegionalToGmt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RegionalToGmt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RegionalToGmt] TO [sp_executeall]
GO
