SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-03-08
-- Description:	Separates a fullname into firstname and lastname and titles
-- =============================================
CREATE FUNCTION [dbo].[fnTitleAndNamesFromName]
(
	@fullname nvarchar(max)
)
RETURNS @Names TABLE 
(
	Title varchar(20), 
	TitleID int,
	FirstName nvarchar(200),
	LastName nvarchar(200)
)
AS
BEGIN

	DECLARE @delimiter nchar(1),
	@position int,
	@retval bit,
	@TitleID int,
	@Title varchar(20)

	SELECT @TitleID = t.TitleID, @Title = t.Title
	FROM Titles t WITH (NOLOCK)
	WHERE @fullname LIKE t.Title + '%' 
	
	IF @Title IS NOT NULL
	BEGIN
		SELECT @fullname = REPLACE(@fullname, @Title + ' ', '')
	END

	SELECT @delimiter = ' ',
	@retval = 1,
	@fullname = LTRIM(RTRIM(@fullname)),
	@position = CHARINDEX(@delimiter, @fullname, 1)

	IF @position > 0
	BEGIN
		INSERT INTO @Names (FirstName, LastName, Title, TitleID)
		SELECT LEFT(@fullname, @position - 1), LTRIM(RTRIM(RIGHT(@fullname, LEN(@fullname) - @position))), @Title, @TitleID
	END	
	ELSE
	BEGIN
		INSERT INTO @Names (FirstName, LastName, Title, TitleID)
		SELECT @fullname, '', @Title, @TitleID
	END	

	RETURN

END

--grant select on  [dbo].[fnNamesFromName] to sp_executeall








GO
GRANT VIEW DEFINITION ON  [dbo].[fnTitleAndNamesFromName] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnTitleAndNamesFromName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnTitleAndNamesFromName] TO [sp_executeall]
GO
