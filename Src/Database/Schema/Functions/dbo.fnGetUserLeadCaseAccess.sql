SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 20011-08-05
-- Description:	Get the list of cases for this Lead, excluding any that this user implicitly cannot see
-- =============================================
CREATE FUNCTION [dbo].[fnGetUserLeadCaseAccess]
(
	@UserID INT,
	@LeadID INT
)
RETURNS @CaseInfo TABLE 
(
	CaseID INT,
	CaseRef VARCHAR(250)
)
AS
BEGIN

	DECLARE @GroupID INT,
			@ClientID INT
	
	SELECT @GroupID = cp.ClientPersonnelAdminGroupID , @ClientID = ClientID 
	FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
	WHERE cp.ClientPersonnelID = @UserID
	
	IF NOT EXISTS (SELECT * FROM dbo.ClientPersonnelAccess cpa WITH (NOLOCK) 
					WHERE cpa.LeadID = @LeadID 
					AND (cpa.ClientPersonnelID = @UserID OR (cpa.ClientPersonnelAdminGroupID = @GroupID AND cpa.ClientID = @ClientID)))
	BEGIN
	
		INSERT INTO @CaseInfo (CaseID, CaseRef)
		SELECT ca.CaseID, ca.CaseRef 
		FROM dbo.Cases ca WITH (NOLOCK) 
		WHERE ca.LeadID = @LeadID
		ORDER BY ca.CaseID
	
	END
	ELSE
	BEGIN
	
		INSERT INTO @CaseInfo (CaseID, CaseRef)
		SELECT DISTINCT ca.CaseID, ca.CaseRef 
		FROM dbo.Cases ca WITH (NOLOCK)
		INNER JOIN dbo.ClientPersonnelAccess cpa WITH (NOLOCK) ON ca.LeadID = cpa.LeadID 
		WHERE (cpa.ClientPersonnelID = @UserID OR cpa.ClientPersonnelAdminGroupID = @GroupID)
		AND ca.LeadID = @LeadID
		AND (cpa.CaseID IS NULL OR ca.CaseID = cpa.CaseID)
		ORDER BY ca.CaseID
	
	END

	RETURN

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetUserLeadCaseAccess] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnGetUserLeadCaseAccess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetUserLeadCaseAccess] TO [sp_executeall]
GO
