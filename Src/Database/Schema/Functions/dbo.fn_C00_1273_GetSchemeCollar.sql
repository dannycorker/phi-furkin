SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-02-03
-- Description:	Returns the schemes collar (min premium)
-- Mods
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetSchemeCollar]
(
	@SchemeID INT
)
RETURNS MONEY	
AS
BEGIN

	
	DECLARE @ClientID INT,
			@Collar MONEY
	
	-- get PAF
	SELECT @Collar=ISNULL(collar.ValueMoney,0)
	FROM MatterDetailValues scheme
	LEFT JOIN MatterDetailValues collar WITH (NOLOCK) ON scheme.MatterID=collar.MatterID AND collar.DetailFieldID=175464
	WHERE scheme.DetailFieldID=145689 AND scheme.ValueInt=@SchemeID

	RETURN @Collar
 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetSchemeCollar] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetSchemeCollar] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetSchemeCollar] TO [sp_executeall]
GO
