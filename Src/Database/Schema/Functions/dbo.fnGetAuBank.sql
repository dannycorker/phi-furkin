SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-03-11
-- Description:	Returns the AU Bank e.g. ANZ from the Sortcode/BsbCode
-- =============================================
CREATE FUNCTION [dbo].[fnGetAuBank]
(
	@Sortcode VARCHAR(100)
	
)
RETURNS VARCHAR(3)
AS
BEGIN

	DECLARE @Bsb VARCHAR(7)
	DECLARE @Bank VARCHAR(3)

	SELECT @Bsb = [dbo].[fnGetAuBsb](@Sortcode)

	SELECT @Bank = bsb.Bank
	FROM [AquariusMaster].dbo.BankStateBranch bsb WITH (NOLOCK)
	WHERE bsb.BsbCode = @Bsb

	Return @Bank

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetAuBank] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetAuBank] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetAuBank] TO [sp_executeall]
GO
