SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-08-27
-- Description:	Returns a table of policy excesses applicable
-- Modified:	SB	2014-10-09	Change to use key date logic to look up voluntary excess rather than use the current value
--				SB	2015-10-26 Fix / finish the work to define the date used for age related excess.  Add in policy age filter.
--				SB	2015-12-17 Policy age now needs to be two values for age at start and end so can return multiple excess rows if appropriate
--				JWG 2020-09-21 Added "Options" to match the GPR 2020-07-24 change in fn_C00_1272_Policy_GetPolicyExcessWithData
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_Policy_GetPolicyExcess]
(
	@CurrentPolicyMatterID INT,
	@ClaimMatterID INT
)
RETURNS 
	@ExcessData TABLE 
	(
		Out_ResourceListID INT,
		ResourceListID INT,
		Section VARCHAR(2000),
		SubSection VARCHAR(2000),
		Excess MONEY,
		ExcessPercentage MONEY,
		Postcode VARCHAR(2000),
		Breed VARCHAR(2000),
		PetAge INT,
		PetType INT,
		ExcessType INT,
		ExcessCeiling MONEY,
		VoluntaryExcess MONEY,
		PolicyAge INT,
		ExcessRule INT,
		Options VARCHAR(2000)
	)
AS
BEGIN
	
	DECLARE @PolicyLeadID INT
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@ClaimMatterID)

	DECLARE @CustomerID INT,
			@LeadID INT
	SELECT	@CustomerID = CustomerID,
			@LeadID = LeadID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @ClaimMatterID
	
	DECLARE @DateOfLoss DATE,
			@TreatmentStart DATE,
			@TreatmentEnd DATE,
			@PolicyAnniversary DATE,
			@PolicyInception DATE,
			@PolicyAgeStart INT,
			@PolicyAgeEnd INT
	
	SELECT @TreatmentStart = mdvTreatmentStart.ValueDate, @DateOfLoss = mdvDateOfLoss.ValueDate, @TreatmentEnd = mdvTreatmentEnd.ValueDate
	FROM dbo.Matter m WITH (NOLOCK) 
	INNER JOIN dbo.MatterDetailValues mdvDateOfLoss WITH (NOLOCK) ON m.MatterID = mdvDateOfLoss.MatterID AND mdvDateOfLoss.DetailFieldID = 144892 --DateOfLoss
	LEFT JOIN dbo.MatterDetailValues mdvTreatmentStart WITH (NOLOCK) ON m.MatterID = mdvTreatmentStart.MatterID AND mdvTreatmentStart.DetailFieldID = 144366 --TreatmentStart
	LEFT JOIN dbo.MatterDetailValues mdvTreatmentEnd WITH (NOLOCK) ON m.MatterID = mdvTreatmentEnd.MatterID AND mdvTreatmentEnd.DetailFieldID = 145674 --TreatmentEnd
	WHERE m.MatterID = @ClaimMatterID
	
	IF @TreatmentStart IS NULL
	-- Look on the claim rows (common scenario for SAP imported claims)
	BEGIN
	
		SELECT @TreatmentStart = MIN(tdvTreatmentStart.ValueDate)
		FROM dbo.Matter m WITH (NOLOCK)
		INNER JOIN dbo.TableRows r WITH (NOLOCK) ON r.MatterID = m.MatterID AND r.DetailFieldID = 144355 --ClaimRowData
		INNER JOIN dbo.TableDetailValues tdvTreatmentStart WITH (NOLOCK) ON tdvTreatmentStart.TableRowID = r.TableRowID AND tdvTreatmentStart.DetailFieldID = 144349 --TreatmentStart
		WHERE m.MatterID = @ClaimMatterID

	END
	
	IF @TreatmentEnd IS NULL
	-- Look on the claim rows (common scenario for SAP imported claims)
	BEGIN
	
		SELECT @TreatmentEnd = MAX(tdvTreatmentEnd.ValueDate)
		FROM dbo.Matter m WITH (NOLOCK)
		INNER JOIN dbo.TableRows r WITH (NOLOCK) ON r.MatterID = m.MatterID AND r.DetailFieldID = 144355 --ClaimRowData
		INNER JOIN dbo.TableDetailValues tdvTreatmentEnd WITH (NOLOCK) ON tdvTreatmentEnd.TableRowID = r.TableRowID AND tdvTreatmentEnd.DetailFieldID = 144351 --TreatmentEnd
		WHERE m.MatterID = @ClaimMatterID

	END
	
	-- the policy from date of the policy year where treatment started for this claim
	SELECT @PolicyAnniversary = tdvFrom.ValueDate, @PolicyInception = tdvInception.ValueDate
	FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@ClaimMatterID) r
	INNER JOIN dbo.TableDetailValues tdvFrom WITH (NOLOCK) ON r.TableRowID = tdvFrom.TableRowID AND tdvFrom.DetailFieldID = 145663
	INNER JOIN dbo.TableDetailValues tdvTo WITH (NOLOCK) ON r.TableRowID = tdvTo.TableRowID AND tdvTo.DetailFieldID = 145664
	INNER JOIN dbo.TableDetailValues tdvInception WITH (NOLOCK) ON r.TableRowID = tdvInception.TableRowID AND tdvInception.DetailFieldID = 145662
	WHERE @TreatmentStart >= tdvFrom.ValueDate
	AND @TreatmentStart < tdvTo.ValueDate
	
	SELECT @PolicyAgeStart = ISNULL(DATEDIFF(YEAR, @PolicyInception, @PolicyAnniversary), 0) + 1
	
	-- the policy from date of the policy year where treatment ended for this claim
	SELECT @PolicyAnniversary = tdvFrom.ValueDate
	FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@ClaimMatterID) r
	INNER JOIN dbo.TableDetailValues tdvFrom WITH (NOLOCK) ON r.TableRowID = tdvFrom.TableRowID AND tdvFrom.DetailFieldID = 145663
	INNER JOIN dbo.TableDetailValues tdvTo WITH (NOLOCK) ON r.TableRowID = tdvTo.TableRowID AND tdvTo.DetailFieldID = 145664
	WHERE @TreatmentEnd >= tdvFrom.ValueDate
	AND @TreatmentEnd < tdvTo.ValueDate
	
	SELECT @PolicyAgeEnd = ISNULL(DATEDIFF(YEAR, @PolicyInception, @PolicyAnniversary), 0) + 1
	
	
	-- Select the animal breed to see if there is a breed premium
	DECLARE @AnimalBreed VARCHAR(2000)
			,@PetDOB DATE
			,@AnimalType INT
	SELECT @AnimalBreed = rdv.DetailValue, @PetDOB = ldvPetDOB.ValueDate, @AnimalType = rdvPetType.ValueInt
	FROM dbo.Lead l WITH (NOLOCK) 
	LEFT JOIN dbo.LeadDetailValues ldv WITH (NOLOCK) ON l.LeadID = ldv.LeadID AND ldv.DetailFieldID = 144272
	LEFT JOIN dbo.LeadDetailValues ldvPetDOB WITH (NOLOCK) ON ldvPetDOB.LeadID = l.LeadID AND ldvPetDOB.DetailFieldID = 144274
	LEFT JOIN dbo.ResourceListDetailValues rdv WITH (NOLOCK) ON ldv.ValueInt = rdv.ResourceListID AND rdv.DetailFieldID = 144270
	LEFT JOIN dbo.ResourceListDetailValues rdvPetType WITH (NOLOCK) ON ldv.ValueInt = rdvPetType.ResourceListID AND rdvPetType.DetailFieldID = 144269
	WHERE l.LeadID = @PolicyLeadID
	
	
	-- Select the customer postcode to see if there is a postcode premium
	DECLARE @CustomerPostcode VARCHAR(2)
	/*CPS 2017-06-12 Replace with matter-level post code group field*/
	--SELECT @CustomerPostcode = 
	--CASE 
	--	WHEN dbo.fnIsInt(SUBSTRING(c.PostCode, 2, 1)) = 1 THEN SUBSTRING(c.PostCode, 1, 1)
	--	ELSE SUBSTRING(c.PostCode, 1, 2)
	--END
	--FROM dbo.Customers c WITH (NOLOCK)
	--WHERE c.CustomerID = @CustomerID
	
	SELECT @CustomerPostcode = mdv.DetailValue
	FROM Matter m WITH ( NOLOCK ) 
	INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID AND mdv.DetailFieldID = 177489 /*PostCode Group at Start Date*/
	WHERE m.LeadID = @PolicyLeadID
	AND mdv.DetailValue <> ''
	
	
	DECLARE @DateToUse DATE
	SELECT @DateToUse = dbo.fn_C00_1272_GetDateToUseForCalcs(@ClaimMatterID, @DateOfLoss, @TreatmentStart)
	
	-- Instead of getting the current vol. excess we now go to the appropriate policy history row	
	-- Check to see if there has been a voluntary excess defined.  
	DECLARE @VoluntaryExcess MONEY = 0
	
	SELECT @VoluntaryExcess = ISNULL(tdvVolEx.ValueMoney, 0)
	FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@ClaimMatterID) r
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664
	INNER JOIN dbo.TableDetailValues tdvVolEx WITH (NOLOCK) ON r.TableRowID = tdvVolEx.TableRowID AND tdvVolEx.DetailFieldID = 145667
	WHERE @DateToUse >= tdvStart.ValueDate
	AND @DateToUse < tdvEnd.ValueDate
	
	-- Return the highest excess
	-- Waiving voluntary excess for Minimum Contribution rows 
	INSERT @ExcessData
	SELECT *
	FROM dbo.fn_C00_1272_Policy_GetPolicyExcessWithData(@CurrentPolicyMatterID, @CustomerPostCode, @AnimalBreed, @PetDOB, @DateOfLoss, @AnimalType, @VoluntaryExcess, @TreatmentStart, @PolicyAnniversary, @PolicyAgeStart, @PolicyAgeEnd)
	
	RETURN

END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPolicyExcess] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1272_Policy_GetPolicyExcess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPolicyExcess] TO [sp_executeall]
GO
