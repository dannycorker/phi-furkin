SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2015-02-20
-- Description:	For ISI Clients, produce the table to display at matter level
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ISI_GetQuarterlyPaymentsTableForMatterID] 
(	
	@MatterID INT
)
RETURNS
@TableOut TABLE 
(
	 TableRowID		INT
	,CreditorName	VARCHAR(2000)
	,Year			INT
	,Month1Name		VARCHAR(20)
	,Month1Value	DECIMAL(18,2)
	,Month2Name		VARCHAR(20)
	,Month2Value	DECIMAL(18,2)
	,Month3Name		VARCHAR(20)
	,Month3Value	DECIMAL(28,2)
	,Total			DECIMAL(28,2)
)
AS
BEGIN
		
	DECLARE @MyCustomerID INT

	SELECT @MyCustomerID = m.CustomerID
	FROM Matter m WITH ( NOLOCK ) 
	WHERE m.MatterID = @MatterID

	DECLARE @Quarter VARCHAR(20)

	SELECT @Quarter = dbo.fnGetSimpleDvLuli(286987,@MyCustomerID)

	DECLARE @ShowQuarter TABLE (CreditorName VARCHAR(200), Year INT, [January] NUMERIC(18,2), [February] NUMERIC(18,2),     [March] NUMERIC(18,2), [April] NUMERIC(18,2), [May] NUMERIC(18,2), [June] NUMERIC(18,2), [July] NUMERIC(18,2), [August] NUMERIC(18,2), [September] NUMERIC(18,2), [October] NUMERIC(18,2), [November] NUMERIC(18,2), [December] NUMERIC(18,2), TableRowID int)

	INSERT INTO @ShowQuarter 
	SELECT *
	FROM dbo.fn_C00_ISI_GetMonthlyRepaymentsByCustomerAndYear (@MyCustomerID, dbo.fnGetSimpleDvLuli(286986,@MyCustomerID))

	;WITH RealNumbers AS 
	(
		  Select q.TableRowID, q.CreditorName, q.Year, 
		  CASE 
		  WHEN @Quarter = 'JAN-MAR' THEN 'JAN'
		  WHEN @Quarter = 'APR-JUN' THEN 'APR'
		  WHEN @Quarter = 'JUL-SEPT' THEN 'JUL'
		  WHEN @Quarter = 'OCT-DEC' THEN 'OCT'
		  END AS [Month1Name],
		  CASE
		  WHEN @Quarter = 'JAN-MAR' THEN January
		  WHEN @Quarter = 'APR-JUN' THEN April
		  WHEN @Quarter = 'JUL-SEPT' THEN July
		  WHEN @Quarter = 'OCT-DEC' THEN October
		  END AS [Month1Value],

		  CASE 
		  WHEN @Quarter = 'JAN-MAR' THEN 'FEB'
		  WHEN @Quarter = 'APR-JUN' THEN 'MAY'
		  WHEN @Quarter = 'JUL-SEPT' THEN 'AUG'
		  WHEN @Quarter = 'OCT-DEC' THEN 'NOV'
		  END AS [Month2Name],
		  CASE
		  WHEN @Quarter = 'JAN-MAR' THEN February
		  WHEN @Quarter = 'APR-JUN' THEN May
		  WHEN @Quarter = 'JUL-SEPT' THEN August
		  WHEN @Quarter = 'OCT-DEC' THEN November
		  END AS [Month2Value],

		  CASE 
		  WHEN @Quarter = 'JAN-MAR' THEN 'MAR'
		  WHEN @Quarter = 'APR-JUN' THEN 'JUN'
		  WHEN @Quarter = 'JUL-SEPT' THEN 'SEPT'
		  WHEN @Quarter = 'OCT-DEC' THEN 'DEC'
		  END AS [Month3Name],
		  CASE
		  WHEN @Quarter = 'JAN-MAR' THEN March
		  WHEN @Quarter = 'APR-JUN' THEN June
		  WHEN @Quarter = 'JUL-SEPT' THEN September
		  WHEN @Quarter = 'OCT-DEC' THEN December
		  END AS [Month3Value],
	      
		  CASE
		  WHEN @Quarter = 'JAN-MAR' THEN January
		  WHEN @Quarter = 'APR-JUN' THEN April
		  WHEN @Quarter = 'JUL-SEPT' THEN July
		  WHEN @Quarter = 'OCT-DEC' THEN October
		  END 
		  +
		  CASE
		  WHEN @Quarter = 'JAN-MAR' THEN February
		  WHEN @Quarter = 'APR-JUN' THEN May
		  WHEN @Quarter = 'JUL-SEPT' THEN August
		  WHEN @Quarter = 'OCT-DEC' THEN November
		  END
		  +
		  CASE
		  WHEN @Quarter = 'JAN-MAR' THEN March
		  WHEN @Quarter = 'APR-JUN' THEN June
		  WHEN @Quarter = 'JUL-SEPT' THEN September
		  WHEN @Quarter = 'OCT-DEC' THEN December
		  END AS [Total],    
		  'true' AS [DenyEdit],
		  'true' AS [DenyDelete]
		  FROM @ShowQuarter q
	)
	INSERT @TableOut ( TableRowID, CreditorName, Year, Month1Name, Month1Value, Month2Name, Month2Value, Month3Name, Month3Value, Total )
	SELECT	 rn.TableRowID
			,rn.CreditorName
			,rn.Year
			,rn.Month1Name
			,rn.Month1Value
			,rn.Month2Name
			,rn.Month2Value
			,rn.Month3Name
			,rn.Month3Value
			,rn.Total
	FROM RealNumbers rn 

	RETURN
END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetQuarterlyPaymentsTableForMatterID] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_ISI_GetQuarterlyPaymentsTableForMatterID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetQuarterlyPaymentsTableForMatterID] TO [sp_executeall]
GO
