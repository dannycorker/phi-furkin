SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Umer Hamid
-- Create date: 2018-05-11
-- Description:	Returns the number of policies where the refund is due
-- 2018-06-12 UH added rules engine params
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ReturnNumberofPoliciesWhereRefundDue]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS INT	
AS
BEGIN

	DECLARE @NoOfPoliciesWhereRefundDue INT = 0
			
	-- Return the number of policies where refund is due
	SELECT @NoOfPoliciesWhereRefundDue = COUNT(*) FROM Matter m WITH ( NOLOCK ) 
	INNER JOIN PurchasedProduct p WITH ( NOLOCK ) on p.ObjectID = m.MatterID 
	INNER JOIN PurchasedProductPaymentSchedule pp WITH ( NOLOCK ) on pp.PurchasedProductID = p.PurchasedProductID 
	WHERE pp.PurchasedProductPaymentScheduleTypeID = 7 
	AND pp.PaymentGross < 0
	AND m.matterid = @Matterid 
	AND pp.PaymentStatusID  IN (1,2) 

	RETURN @NoOfPoliciesWhereRefundDue
 
END
 

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReturnNumberofPoliciesWhereRefundDue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ReturnNumberofPoliciesWhereRefundDue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReturnNumberofPoliciesWhereRefundDue] TO [sp_executeall]
GO
