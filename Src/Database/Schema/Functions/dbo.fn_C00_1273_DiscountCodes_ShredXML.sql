SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jan Wilson
-- Create date: 2014-10-13
-- Description:	Shreds the DiscountCodes from XML 
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_DiscountCodes_ShredXML]
(
	@XmlRequest XML
)
RETURNS 
	@DiscountCodes TABLE (
		DiscountCode VARCHAR(250)
	)
AS
BEGIN

	INSERT INTO @DiscountCodes
	SELECT	Tbl.p.value('string[1]', 'VARCHAR(250)')
	FROM	@XmlRequest.nodes('//DiscountCodes') Tbl(p)
	
	RETURN

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_DiscountCodes_ShredXML] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1273_DiscountCodes_ShredXML] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_DiscountCodes_ShredXML] TO [sp_executeall]
GO
