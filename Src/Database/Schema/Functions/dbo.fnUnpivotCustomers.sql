SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Unpivot the columns in the Customers table 
-- =============================================
CREATE FUNCTION [dbo].[fnUnpivotCustomers] 
(	
	@CustomerID int
)
RETURNS TABLE 
AS
RETURN 
(
	
	SELECT ColumnName as [RawColumnName], '[!' + ColumnName + ']' as [FormattedColumnName], RealValue
	FROM
	(
		SELECT 
		COALESCE(CONVERT(varchar(255), c.Address1), '') as [Address1],
		COALESCE(CONVERT(varchar(255), c.Address2), '') as [Address2],
		COALESCE(CONVERT(varchar(255), c.AddressVerified), '') as [AddressVerified],
		COALESCE(CONVERT(varchar(255), c.AgreedToTermsAndConditions), '') as [AgreedToTermsAndConditions],
		COALESCE(CONVERT(varchar(255), c.AquariumStatusID), '') as [AquariumStatusID],
		COALESCE(CONVERT(varchar(255), c.ClientID), '') as [ClientID],
		COALESCE(CONVERT(varchar(255), c.ClientStatusID), '') as [ClientStatusID],
		COALESCE(CONVERT(varchar(255), c.CompanyName), '') as [CompanyName],
		COALESCE(CONVERT(varchar(255), c.CompanyTelephone), '') as [CompanyTelephone],
		COALESCE(CONVERT(varchar(255), c.CompanyTelephoneVerifiedAndValid), '') as [CompanyTelephoneVerifiedAndValid],
		COALESCE(CONVERT(varchar(255), c.CountryID), '') as [CountryID],
		COALESCE(CONVERT(varchar(255), c.County), '') as [County],
		COALESCE(CONVERT(varchar(255), c.CustomerID), '') as [CustomerID],
		COALESCE(CONVERT(varchar(255), c.CustomerRef), '') as [CustomerRef],
		COALESCE(CONVERT(varchar(255), c.DateOfBirth, 103), '') as [DateOfBirth],
		COALESCE(CONVERT(varchar(255), c.DayTimeTelephoneNumber), '') as [DayTimeTelephoneNumber],
		COALESCE(CONVERT(varchar(255), c.DayTimeTelephoneNumberVerifiedAndValid), '') as [DayTimeTelephoneNumberVerifiedAndValid],
		COALESCE(CONVERT(varchar(255), c.DefaultContactID), '') as [DefaultContactID],
		COALESCE(CONVERT(varchar(255), c.DefaultOfficeID), '') as [DefaultOfficeID],
		COALESCE(CONVERT(varchar(255), c.DoNotEmail), '') as [DoNotEmail],
		COALESCE(CONVERT(varchar(255), c.DoNotSellToThirdParty), '') as [DoNotSellToThirdParty],
		COALESCE(CONVERT(varchar(255), c.DownloadedOn), '') as [DownloadedOn],
		COALESCE(CONVERT(varchar(255), c.EmailAddress), '') as [EmailAddress],
		COALESCE(CONVERT(varchar(255), c.Employer), '') as [Employer],
		COALESCE(CONVERT(varchar(255), c.FirstName), '') as [FirstName],
		COALESCE(CONVERT(varchar(255), c.Fullname), '') as [Fullname],
		COALESCE(CONVERT(varchar(255), c.HasDownloaded), '') as [HasDownloaded],
		COALESCE(CONVERT(varchar(255), c.HomeTelephone), '') as [HomeTelephone],
		COALESCE(CONVERT(varchar(255), c.HomeTelephoneVerifiedAndValid), '') as [HomeTelephoneVerifiedAndValid],
		COALESCE(CONVERT(varchar(255), c.IsBusiness), '') as [IsBusiness],
		COALESCE(CONVERT(varchar(255), c.LastName), '') as [LastName],
		COALESCE(CONVERT(varchar(255), c.MiddleName), '') as [MiddleName],
		COALESCE(CONVERT(varchar(255), c.MobileTelephone), '') as [MobileTelephone],
		COALESCE(CONVERT(varchar(255), c.MobileTelephoneVerifiedAndValid), '') as [MobileTelephoneVerifiedAndValid],
		COALESCE(CONVERT(varchar(255), c.Occupation), '') as [Occupation],
		COALESCE(CONVERT(varchar(255), c.PhoneNumbersVerifiedOn), '') as [PhoneNumbersVerifiedOn],
		COALESCE(CONVERT(varchar(255), c.PostCode), '') as [PostCode],
		COALESCE(CONVERT(varchar(255), c.SubClientID), '') as [SubClientID],
		COALESCE(CONVERT(varchar(255), c.Test), '') as [Test],
		COALESCE(CONVERT(varchar(255), c.TitleID), '') as [TitleID],
		COALESCE(CONVERT(varchar(255), c.Town), '') as [Town],
		COALESCE(CONVERT(varchar(255), c.Website), '') as [Website],
		COALESCE(CONVERT(varchar(255), c.WorksTelephone), '') as [WorksTelephone],
		COALESCE(CONVERT(varchar(255), c.WorksTelephoneVerifiedAndValid), '') as [WorksTelephoneVerifiedAndValid]
		FROM dbo.Customers c WITH (NOLOCK) 
		WHERE c.CustomerID = @CustomerID) p
	UNPIVOT
	(
		RealValue FOR ColumnName IN
		(
			[Address1],[Address2],[AddressVerified],[AgreedToTermsAndConditions],[AquariumStatusID],[ClientID],[ClientStatusID],[CompanyName],[CompanyTelephone],[CompanyTelephoneVerifiedAndValid],[CountryID],[County],[CustomerID],[CustomerRef],[DateOfBirth],[DayTimeTelephoneNumber],[DayTimeTelephoneNumberVerifiedAndValid],[DefaultContactID],[DefaultOfficeID],[DoNotEmail],[DoNotSellToThirdParty],[DownloadedOn],[EmailAddress],[Employer],[FirstName],[Fullname],[HasDownloaded],[HomeTelephone],[HomeTelephoneVerifiedAndValid],[IsBusiness],[LastName],[MiddleName],[MobileTelephone],[MobileTelephoneVerifiedAndValid],[Occupation],[PhoneNumbersVerifiedOn],[PostCode],[SubClientID],[Test],[TitleID],[Town],[Website],[WorksTelephone],[WorksTelephoneVerifiedAndValid]
		)
	) as unpvt
)




GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotCustomers] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnUnpivotCustomers] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotCustomers] TO [sp_executeall]
GO
