SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-11-13
-- Description:	Return the document type for an event and case based on predicate rules
-- =============================================
CREATE FUNCTION [dbo].[fn_DocumentType__GetByRuleEvaluation]
(
	@EventTypeID	int,
	@CaseID			int,
	@RunAsUserID	int 
)
RETURNS INT
AS
BEGIN


	DECLARE @DocumentTypeID INT, 
	@BaseDocumentTypeID INT, 
	@CustomerID INT, 
	@LeadID INT, 
	@SqlStatement NVARCHAR(MAX) = ''
	,@ReturnValue INT
	
	/* Get default details */
	SELECT	TOP 1 
			@BaseDocumentTypeID = ISNULL(et.DocumentTypeID, 0),
			@DocumentTypeID  = ISNULL(et.DocumentTypeID, 0)
	FROM	EventType et WITH (NOLOCK)
	WHERE	et.EventTypeID = @EventTypeID
	
	/* If there are any rules for this event, evaluate them next */
	IF EXISTS(SELECT * FROM PredicateRuleAssigned pra WITH (NOLOCK) WHERE	pra.EventTypeID = @EventTypeID) 
	BEGIN

		/* Get extra details for the Customer & Lead */
		SELECT 
			@CustomerID = l.CustomerID,
			@LeadID = l.LeadID
		FROM Cases ca WITH (NOLOCK)
		INNER JOIN  Lead l WITH (NOLOCK) ON ca.LeadID = l.LeadID
		WHERE ca.CaseID = @CaseID
		
		/* Build up the list of   WHEN...THEN clauses */
		SELECT	@SqlStatement +=  'WHEN (' + ISNULL(pr.RuleSQL, '1=1')+ ') THEN '+ CAST(pra.OverrideDocumentTypeID AS VARCHAR) + ' '
		FROM	PredicateRuleAssigned pra WITH (NOLOCK) 
		LEFT JOIN PredicateRule pr WITH (NOLOCK) ON pr.PredicateRuleID = pra.PredicateRuleID
		WHERE	pra.EventTypeID = @EventTypeID
		ORDER BY pra.RuleOrder, pra.PredicateRuleAssignedID ASC
	
		/* Combine all the above into a CASE  WHEN...THEN WHEN...THEN WHEN...THEN ELSE n END statement */
		SELECT @SqlStatement = 
			'SELECT	@DocumentTypeID = CASE '+@SqlStatement+' ELSE '+ CONVERT(VARCHAR,@BaseDocumentTypeID) +' END
			FROM Customers WITH (NOLOCK) 
			INNER JOIN Lead WITH (NOLOCK) ON Customers.CustomerID = Lead.CustomerID
			INNER JOIN Cases WITH (NOLOCK) ON Cases.LeadID = Lead.LeadID
			WHERE Cases.CaseID = @CaseID'

		SELECT @SqlStatement = REPLACE(@SqlStatement,'@CaseID', CONVERT(VARCHAR,@CaseID))

		/* Run the sql and get the final DocumentTypeID to use */ 
		--EXEC @SqlStatement

		/* If the sql failed to return any data, assign an invalid DocumentTypeID */
		IF (@@ROWCOUNT = 0) 
		BEGIN
			SELECT @DocumentTypeID = -1
		END
		
	END
	
	/* Send back a DAL DocumentType object */
	SELECT	@ReturnValue = @DocumentTypeID
	
	RETURN @ReturnValue

END





GO
GRANT VIEW DEFINITION ON  [dbo].[fn_DocumentType__GetByRuleEvaluation] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_DocumentType__GetByRuleEvaluation] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_DocumentType__GetByRuleEvaluation] TO [sp_executeall]
GO
