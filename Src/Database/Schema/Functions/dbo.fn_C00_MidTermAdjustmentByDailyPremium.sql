SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-08-17
-- Description:	Inspired by fn_C00_CancellationAdjustmentByDailyPremium
-- Modified: 
-- 2021-01-07 GPR altered calculation variables to use 6 decimal places, this precision when multiplied allows the Gross to calculate correctly when including Tax values as used in Kentucky.
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_MidTermAdjustmentByDailyPremium]
(
	@MatterID INT,
	@AdjustmentDate DATE
)
RETURNS 
	@OutPut TABLE 
	(
		AdjustmentGross DECIMAL(18,2),
		AdjustmentNet DECIMAL(18,2),
		AdjustmentNationalTax DECIMAL(18,2),
		AdjustmentLocalTax DECIMAL(18,2),
		MatterID INT
	)
AS
BEGIN
 	 
	DECLARE @PurchasedProductID INT,
			@ProductStart DATE,
			@ProductEnd DATE,
			@ProductLength INT,
			@CoverFrom DATE,
			@CoverTo DATE,
			@ClientID INT,
			@OriginalPremiumGross DECIMAL(18,2),
			@OriginalPremiumNet DECIMAL(18,2),
			@OriginalPremiumNationalTax DECIMAL(18,2),
			@OriginalPremiumLocalTax DECIMAL(18,2),
			@MTAPremiumGross DECIMAL(18,2),
			@MTAPremiumNet DECIMAL(18,2),
			@MTAPremiumNationalTax DECIMAL(18,2),
			@MTAPremiumLocalTax DECIMAL(18,2),
			@DailyPremiumGrossBefore DECIMAL(18,6),
			@DailyPremiumGrossAfter DECIMAL(18,6),
			@DailyPremiumNetBefore DECIMAL(18,6),
			@DailyPremiumNetAfter DECIMAL(18,6),
			@DailyPremiumNationalTaxBefore DECIMAL(18,6),
			@DailyPremiumNationalTaxAfter DECIMAL(18,6),
			@DailyPremiumLocalTaxBefore DECIMAL(18,6),
			@DailyPremiumLocalTaxAfter DECIMAL(18,6),
			@TrueProductGross	DECIMAL(18,6),
			@TrueProductNet	DECIMAL(18,6),
			@TrueProductNationalTax	DECIMAL(18,6),
			@TrueProductLocalTax	DECIMAL(18,6),
			@AdjustmentGross	DECIMAL(18,6),
			@AdjustmentNet	DECIMAL(18,6),
			@AdjustmentNationalTax DECIMAL(18,6),
			@AdjustmentLocalTax DECIMAL(18,6),
			@PreviousWrittenPremiumID INT,
			@MTAWrittenPremiumID INT
	
	/*Identify Purchased Product*/
	SELECT @PurchasedProductID = mdv.ValueINT 
	FROM MatterDetailValues mdv WITH ( NOLOCK ) 
	where mdv.DetailFieldID = 177074
	AND mdv.MatterID = @MatterID

	/*Identify full Policy Term for this Purchased Product*/
	DECLARE @UsedProduct INT
	DECLARE @FutureProduct INT

	SELECT @ProductStart = p.ValidFrom, 
			@ProductEnd = p.ValidTo, 
			@ProductLength = DATEDIFF(DAY, @ProductStart, @ProductEnd) + 1, 
			@UsedProduct = DATEDIFF(DAY,@ProductStart,@AdjustmentDate), 
			@FutureProduct = DATEDIFF(DAY,@AdjustmentDate,@ProductEnd) + 1
	FROM PurchasedProduct p WITH ( NOLOCK ) 
	WHERE p.PurchasedProductID = @PurchasedProductID

	DECLARE @MTAQuoteWrittenPremiumID INT 
	
	SELECT TOP(1) @MTAQuoteWrittenPremiumID = wp.WrittenPremiumID 
	FROM WrittenPremium wp WITH (NOLOCK)
	WHERE wp.MatterID = @MatterID
	AND wp.AdjustmentTypeID = 9
	ORDER BY wp.WrittenPremiumID DESC

	/*Identify pre-MTA Annual values*/	
	SELECT TOP(1) @PreviousWrittenPremiumID = wp.WrittenPremiumID 
	FROM WrittenPremium wp WITH (NOLOCK)
	WHERE wp.MatterID = @MatterID
	AND wp.WrittenPremiumID < @MTAQuoteWrittenPremiumID
	--AND wp.AdjustmentTypeID <> 9
	--AND wp.ValidFrom < @AdjustmentDate
	ORDER BY wp.WrittenPremiumID DESC

	SELECT @OriginalPremiumGross = wp.AnnualPriceForRiskGross, 
			@OriginalPremiumNet = wp.AnnualPriceForRiskNET, 
			@OriginalPremiumNationalTax = wp.AnnualPriceForRiskNationalTax, 
			@OriginalPremiumLocalTax = wp.AnnualPriceForRiskLocalTax
	FROM WrittenPremium wp WITH (NOLOCK)
	WHERE wp.WrittenPremiumID = @PreviousWrittenPremiumID					

	/*Identify post-MTA Annual Gross Value*/
	SET @MTAWrittenPremiumID = (SELECT TOP(1) wp.WrittenPremiumID 
								FROM WrittenPremium wp WITH (NOLOCK)
								WHERE wp.MatterID = @MatterID
								AND wp.AdjustmentTypeID = 2
								AND wp.WrittenPremiumID > @PreviousWrittenPremiumID)	

	SELECT @MTAPremiumGross = wp.AnnualPriceForRiskGross, 
			@MTAPremiumNet = wp.AnnualPriceForRiskNET, 
			@MTAPremiumNationalTax = wp.AnnualPriceForRiskNationalTax, 
			@MTAPremiumLocalTax = wp.AnnualPriceForRiskLocalTax
	FROM WrittenPremium wp WITH (NOLOCK)
	WHERE wp.WrittenPremiumID = @MTAWrittenPremiumID	

	/*Work out the daily rate*/
	SELECT @DailyPremiumGrossBefore = (@OriginalPremiumGross/@ProductLength) * @UsedProduct
	SELECT @DailyPremiumGrossAfter = (@MTAPremiumGross/@ProductLength) * @FutureProduct

	SELECT @DailyPremiumNetBefore = (@OriginalPremiumNet/@ProductLength) * @UsedProduct
	SELECT @DailyPremiumNetAfter = (@MTAPremiumNet/@ProductLength) * @FutureProduct

	SELECT @DailyPremiumNationalTaxBefore = (@OriginalPremiumNationalTax/@ProductLength) * @UsedProduct
	SELECT @DailyPremiumNationalTaxAfter = (@MTAPremiumNationalTax/@ProductLength) * @FutureProduct

	SELECT @DailyPremiumLocalTaxBefore = (@OriginalPremiumLocalTax/@ProductLength) * @UsedProduct
	SELECT @DailyPremiumLocalTaxAfter = (@MTAPremiumLocalTax/@ProductLength) * @FutureProduct

	/*True premim for the full year*/
	SELECT @TrueProductGross = (@DailyPremiumGrossBefore + @DailyPremiumGrossAfter)
	SELECT @TrueProductNet = (@DailyPremiumNetBefore + @DailyPremiumNetAfter)
	SELECT @TrueProductNationalTax = (@DailyPremiumNationalTaxBefore + @DailyPremiumNationalTaxAfter)
	SELECT @TrueProductLocalTax = (@DailyPremiumLocalTaxBefore + @DailyPremiumLocalTaxAfter)

	/*Calculate Adjustment to Written Premium*/
	SET @AdjustmentGross = @OriginalPremiumGross - ABS(@TrueProductGross)
	SET @AdjustmentNet = @OriginalPremiumNet - ABS(@TrueProductNet)
	SET @AdjustmentNationalTax = @OriginalPremiumNationalTax - ABS(@TrueProductNationalTax)
	SET @AdjustmentLocalTax = @OriginalPremiumLocalTax - ABS(@TrueProductLocalTax)

	INSERT INTO @OutPut (AdjustmentGross, AdjustmentNet, AdjustmentNationalTax, AdjustmentLocalTax,MatterID) 
	VALUES (ISNULL(@AdjustmentGross,0.00) /*GPR 2020-12-07 (ISNULL(@AdjustmentNet,0.00) + (ISNULL(@AdjustmentNationalTax,0.00) + ISNULL(@AdjustmentLocalTax,0.00))*/, ISNULL(@AdjustmentNet,0.00), ISNULL(@AdjustmentNationalTax,0.00), ISNULL(@AdjustmentLocalTax,0.00), @MatterID)

	RETURN

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_MidTermAdjustmentByDailyPremium] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_MidTermAdjustmentByDailyPremium] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_MidTermAdjustmentByDailyPremium] TO [sp_executeall]
GO
