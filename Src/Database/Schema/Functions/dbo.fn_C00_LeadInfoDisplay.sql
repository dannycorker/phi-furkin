SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-11-21
-- Description:	Adapted from report by JL.  Moved into function for change control
-- 2017-11-21 CPS Added PolicyNumber and LeadID
-- 2017-12-02 CPS Included @ClientPersonnelID and workflow completion targets
-- 201[dbo].[fn_C00_CancellationFeeAdjustmentByDailyPremium]7-12-13 CPS Included a link to scripting for work requests
-- 2017-12-21 CPS Removed that link.  It is now provided by ReportID 40196 and appears above the event section
-- 2018-04-09 CPS use @ClientID, include payment method
-- 2019-05-14 JEL Changed logic to retrieve collections lead ID using leadtypeReleationship rather than leadtypeshare.
-- 2019-11-07 GPR Removed 'Rescue Pet' and 'Underwiting Status' from view as not in use on C600
-- 2019-12-11 CPS | Added Comms preference and used fnConvertCamelCase on the return to keep the labels consistent
-- 2020-01-13 GPR | Removed fnConvertCamelCase as caused display defect from Test team on 'LeadID'
-- 2020-06-23 GPR | Updates to Payment Interval for SDAAG-67
-- 2020-09-23 NG  | Hide 'current vet' as not captured
-- 2021-03-08 ACE | Removed dbo.fn_C00_GetUrlByDatabase() from collections urls (causes localhost issues)
-- =============================================


CREATE FUNCTION [dbo].[fn_C00_LeadInfoDisplay]
(
	 @LeadID			INT
	,@ClientPersonnelID	INT = NULL
)
RETURNS 
@Display TABLE
(
	 FieldCaption	VARCHAR(200)
	,DetailValue	VARCHAR(200)
	,ToolTip		VARCHAR(200)
	,HyperLink		VARCHAR(500)
)	
AS
BEGIN

	DECLARE	 @ClaimLeadID		INT
			,@PolicyLeadID		INT
			,@CollectionsLeadID	INT  
			,@LeadRef			VARCHAR(100)
			,@CustomerID		INT
			,@LeadTypeID		INT
			,@ClientID			INT
			,@Locale			VARCHAR(5)
			,@CountryID			INT
			,@MatterID			INT

 
/*GPR 2020-09-14 Locale for PPET-424*/
SELECT @ClientID = [dbo].[fnGetPrimaryClientID]()

SELECT @CountryID = (SELECT CountryID FROM Clients WITH (NOLOCK) WHERE ClientID = @ClientID)

SELECT @MatterID = MatterID FROM Matter WITH (NOLOCK) WHERE Matter.LeadID = @LeadID

IF @CountryID IN (233, 39) /*ALM 2021-03-04 FURKIN-320*/
BEGIN
    SELECT @Locale = 'en-US' /*United States of America*/ /*Canada*/
END
ELSE
BEGIN
	SELECT @Locale = 'en-GB' /*Great Britain / Australia*/ 
END

	SELECT @LeadRef = l.LeadRef, @CustomerID = l.CustomerID, @LeadTypeID = l.LeadTypeID, @ClientID = l.ClientID
	FROM Lead l WITH ( NOLOCK )
	WHERE l.LeadID = @LeadID

	SELECT	 @ClaimLeadID		= dbo.fn_C00_1272_GetClaimLeadFromPolicyLead(@LeadID)
			,@PolicyLeadID		= dbo.fn_C00_1272_GetPolicyLeadFromClaimLead(@LeadID)
			,@CustomerID		= l.CustomerID
	FROM Lead l WITH ( NOLOCK ) 
	WHERE l.LeadID = @LeadID

	SELECT @CollectionsLeadID = ltr.ToLeadID FROM LeadTyperelationship ltr with (NOLOCK) 
	where ltr.FromLeadID = @PolicyLeadID 
	and ltr.FromLeadTypeID = 1492
	and ltr.ToLeadTypeID = 1493 
	
	SELECT @LeadRef = l.LeadRef
	FROM Lead l WITH ( NOLOCK )
	WHERE l.LeadID = @PolicyLeadID

	-- Policy Admin
	DECLARE @Table TABLE (FieldCaption VARCHAR(200),DetailValue VARCHAR(200),ToolTip VARCHAR(200),HyperLink VARCHAR(500), [Order] INT)

	INSERT @Table ( FieldCaption, DetailValue, ToolTip, HyperLink, [Order] )
	SELECT 'Lead ID' , CONVERT(VARCHAR,@LeadID)	, NULL, NULL, 0
		UNION
	SELECT 'Policy Number', @LeadRef, NULL, NULL, 0 WHERE @LeadTypeID IN ( 1490, 1492, 1493 )

	INSERT INTO @Table (FieldCaption,DetailValue,ToolTip,HyperLink, [Order])
	SELECT TOP (100)	 DetailFields.FieldCaption + ISNULL(' | ' + NULLIF(dfResource.FieldCaption,DetailFields.FieldCaption),'')
						,CASE	WHEN ISNULL(ll.ItemValue,'') <> '' THEN ll.ItemValue 
								WHEN ISNULL(LookupListItems.ItemValue,'') <> '' THEN LookupListItems.ItemValue
								WHEN ISNULL(rldv.DetailValue,'') <>  '' THEN rldv.DetailValue 
								WHEN LeadDetailValues.ValueDate IS NOT NULL THEN CONVERT(VARCHAR,FORMAT(LeadDetailValues.ValueDate, 'd', @Locale),103) /*GPR 2020-09-14 for PPET-424*/
								ELSE  LeadDetailValues.DetailValue 
						 END  AS [DetailValue]
						,NULL AS [ToolTip]
						,NULL AS [Hyperlink]
						,10 AS [order]
	FROM DetailFields DetailFields WITH (NOLOCK) 
	INNER JOIN LeadDetailValues LeadDetailValues WITH (NOLOCK) ON LeadDetailValues.DetailFieldID = DetailFields.DetailFieldID
	LEFT JOIN LookupListItems LookupListItems WITH (NOLOCK) ON LookupListItems.LookupListItemID = LeadDetailValues.ValueInt AND LookupListItems.ClientID IN (0, @ClientID)  AND (LookupListItems.LookupListID = DetailFields.LookupListID) 
	INNER JOIN Lead Lead WITH (NOLOCK) ON LeadDetailValues.LeadID = Lead.LeadID
	LEFT JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) on rldv.ResourceListID = LeadDetailValues.ValueInt AND (rldv.DetailFieldID in (144270 /*Pet Breed*/
																																		  ,144269 /*Pet Type*/
																																		  ,144473 /*Business Name*/
																																		  ,144480 /*Tel*/
																																		  ))
	LEFT JOIN DetailFields dfResource WITH ( NOLOCK ) on dfResource.DetailFieldID = rldv.DetailFieldID
	LEFT join LookupListItems ll WITH ( NOLOCK ) on ll.LookupListItemID = rldv.ValueInt 
	WHERE DetailFields.ClientID = @ClientID
	AND (DetailFields.DetailFieldID IN ( 144268 /*Pet Name*/
										,144275 /*Pet Sex*/
										,144272 /*Pet Type*/
										--,144273 /*Pet Colour*/ -- GPR 2019-11-13 removed as not in use on C600
										,144274 /*Pet Date of Birth*/
										--,146215 /*Current Vet*/ -- NG removed as not in use
										,170030 /*Microchip number*/
										--,177501 /*Rescue Pet*/ -- GPR 2019-11-07 removed as not in use on C600
										--,313630 /*Underwriting Status*/ -- GPR 2019-11-07 removed as not in use on C600
										,313930 /*Selected Copay*/
										,313932 /*Selected Deductible*/
										,315882 /*Selected Deductible Option Group*/
										--,313931 /*Exam Fees*/ /*GPR 2021-02-01 for FURKIN-85 removed Exam Fees*/
										))
	AND LeadDetailValues.ClientID = @ClientID
	AND Lead.ClientID = @ClientID
	AND (Lead.LeadID = @PolicyLeadID)
	
	INSERT @Table ( FieldCaption, DetailValue, ToolTip, HyperLink, [Order] )
	SELECT df.FieldCaption, ISNULL(li.ItemValue, mdv.DetailValue), NULL, NULL, 20
	FROM DetailFields df WITH ( NOLOCK )
	INNER JOIN Matter m WITH (NOLOCK) on m.LeadID = @PolicyLeadID
	LEFT JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = m.MatterID AND mdv.DetailFieldID = df.DetailFieldID
	LEFT JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListID = df.LookupListID AND li.LookupListItemID = mdv.ValueInt
	WHERE df.LeadTypeID = @LeadTypeID
	AND df.DetailFieldID IN (	 170114 /*Payment Method*/ )
								--,170176 /*Payment Interval*/ ) /*GPR 2020-06-23 removed for SDAAG-67*/
		UNION /*GPR 2020-06-23 for SDAAG-67 | Calculate the current Interval from the Payment Schedule of the current valid Product*/
	SELECT 'Current Payment Interval', CASE COUNT(ppps.PurchasedProductPaymentScheduleID) WHEN 1 THEN 'Annually' WHEN 12 THEN 'Monthly' ELSE 'Policy Not Active' END, NULL, NULL, 21 /*GPR 2020-12-02 updated to display 'Policy Not Active' when no valid/acitive PP is found*/
	FROM Matter m WITH (NOLOCK)
	INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON pp.ObjectID = m.MatterID
	INNER JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) ON ppps.PurchasedProductID = pp.PurchasedProductID
	WHERE m.LeadID = @LeadID
	AND ppps.PurchasedProductPaymentScheduleTypeID = 1 /*Scheduled*/
	AND dbo.fn_GetDate_Local() BETWEEN pp.ValidFrom AND pp.ValidTo
		UNION
	/*GPR 2020-06-23 for SDAAG-67 | Rename 'Payment Interval' as 'Renewal Payment Interval', when the installment is toggled this value is updated and presented to the user by may not mirror the current interval of the Policy*/
	SELECT 'Renewal Payment Interval', ISNULL(li.ItemValue, mdv.DetailValue), NULL, NULL, 22
	FROM DetailFields df WITH ( NOLOCK )
	INNER JOIN Matter m WITH (NOLOCK) on m.LeadID = @PolicyLeadID
	LEFT JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = m.MatterID AND mdv.DetailFieldID = df.DetailFieldID
	LEFT JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListID = df.LookupListID AND li.LookupListItemID = mdv.ValueInt
	WHERE df.LeadTypeID = @LeadTypeID
	AND df.DetailFieldID IN (170176 /*Payment Interval*/ )
		UNION
	-- 2019-12-11 CPS | Added Comms preference
	SELECT df.FieldCaption, lli.ItemValue, NULL, NULL, 25
	FROM CustomerDetailValues cdv WITH (NOLOCK) 
	INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = cdv.DetailFieldID
	INNER JOIN LookupListItems lli WITH (NOLOCK) on lli.LookupListItemID = cdv.ValueInt
	WHERE cdv.CustomerID = @CustomerID
	AND cdv.DetailFieldID = 170257 /*Send documents by*/
		UNION
	-- 2020-01-07 GPR | Added Renewal Type
	SELECT df.FieldCaption, lli.ItemValue, NULL, NULL, 26
	FROM DetailFields df WITH ( NOLOCK )
	INNER JOIN Matter m WITH (NOLOCK) on m.LeadID = @PolicyLeadID
	LEFT JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = m.MatterID AND mdv.DetailFieldID = df.DetailFieldID
	INNER JOIN LookupListItems lli WITH (NOLOCK) on lli.LookupListItemID = mdv.ValueInt
	WHERE df.LeadTypeID = @LeadTypeID
	AND df.DetailFieldID = 177110 /*Renewal Type*/
	/*GPR 2021-02-01 for FURKIN-85 removed Wellness*/
	--	UNION
	--SELECT 'Wellness',dbo.fn_C600_GetCurrentWellnessProductName(@MatterID), NULL, NULL, 27
		UNION
	SELECT '', '', NULL, NULL, 29 -- blank space

	/*GPR 2020-01-09 removed for LPC-302*/
	/*GPR 2020-01-16 added back to assist testing*/
	/*NG  2020-06-04 removed for good, no more Claims on V1*/
	/*
	INSERT INTO @Table (FieldCaption,DetailValue,ToolTip,HyperLink, [Order])
	SELECT 'Claims' as [FieldCaption],'View Claims (V1)' as [DetailValue], 'Claims' as [Tooltip], dbo.fn_C00_GetUrlByDatabase() + 'CustomersLeadDetails2.aspx?lid='+ CAST(@ClaimLeadID as VARCHAR)  AS [Hyperlink]
	,30 as [order] FROM Lead l WITH ( NOLOCK ) 
	WHERE l.LeadID = @ClaimLeadID
	AND @ClaimLeadID <> @LeadID
--	AND @ClientPersonnelID IN ( 58540, 63167 ) -- CPS 2020-01-09 for CR.  We need to see this for the LPC>L&G Claims work.  Ideally change this to check if the user is an Aquarium user.
	*/
	
	/*INSERT INTO @Table (FieldCaption,DetailValue,ToolTip,HyperLink,[Order])
	SELECT 'Claims','View Claims','Claims','https://vision-cweyodm0.uksouth.cloudapp.azure.com/customer-page/' + cu.CustomerRef,30 [Order]
	FROM Customers cu WITH (NOLOCK) 
	WHERE cu.CustomerID = @CustomerID 
	AND cu.CustomerRef <> ''*/
	

/*Trupanion Claims*/

	DECLARE @VisionCustomerID INT, @VisionPetID INT
	SELECT @VisionCustomerID = ov.VisionCustomerID, @VisionPetID = ov.VisionPetID FROM OneVision ov WITH (NOLOCK) WHERE ov.PALeadID = @LeadID

	IF DB_NAME() = 'Aquarius607Dev'
	BEGIN
	
		INSERT INTO @Table (FieldCaption,DetailValue,ToolTip,HyperLink,[Order])
		SELECT 'Claims','View Claims','Claims','https://trupanion-uat.aqvision.pet/customer/' + CAST(@VisionCustomerID AS VARCHAR) + '/pet/' + CAST(@VisionPetID AS VARCHAR),30 [Order]
	END

	IF DB_NAME() = 'Aquarius607QA'
	BEGIN

		INSERT INTO @Table (FieldCaption,DetailValue,ToolTip,HyperLink,[Order])
		SELECT 'Claims','View Claims','Claims','https://trupanion-uat.aqvision.pet/customer/' + CAST(@VisionCustomerID AS VARCHAR) + '/pet/' + CAST(@VisionPetID AS VARCHAR),30 [Order]
	END

	IF DB_NAME() = 'Aquarius607Staging'
	BEGIN

		INSERT INTO @Table (FieldCaption,DetailValue,ToolTip,HyperLink,[Order])
		SELECT 'Claims','View Claims','Claims','https://trupanion-uat.aqvision.pet/customer/' + CAST(@VisionCustomerID AS VARCHAR) + '/pet/' + CAST(@VisionPetID AS VARCHAR),30 [Order]
	END

	IF DB_NAME() = 'Aquarius607'
	BEGIN

		INSERT INTO @Table (FieldCaption,DetailValue,ToolTip,HyperLink,[Order])
		SELECT 'Claims','View Claims','Claims','https://trupanion.aqvision.pet/customer/' + CAST(@VisionCustomerID AS VARCHAR) + '/pet/' + CAST(@VisionPetID AS VARCHAR),30 [Order]
	END

/*Trupanion Claims End*/



	INSERT INTO @Table (FieldCaption,DetailValue,ToolTip,HyperLink, [Order])
	SELECT 'Collections' as [FieldCaption],'View Collections' as [DetailValue], 'Collections' as [Tooltip], '/CustomersLeadDetails2.aspx?lid='+ CAST(dbo.fn_C00_1272_GetCollectionsLeadFromCustomer(@CustomerID) as VARCHAR)  AS [Hyperlink]
	,40 as [order]
	FROM Lead l WITH ( NOLOCK ) 
	WHERE l.LeadID = @CollectionsLeadID
	AND @CollectionsLeadID <> @LeadID

	INSERT INTO @Table (FieldCaption,DetailValue,ToolTip,HyperLink, [Order])
	SELECT 'Policy Admin' as [FieldCaption],'View Policy Admin' as [DetailValue], 'Policy Admin' as [Tooltip], '/CustomersLeadDetails2.aspx?lid='+ CAST(@PolicyLeadID as VARCHAR)  AS [Hyperlink]
	,50 as [order]
	FROM Lead l WITH ( NOLOCK ) 
	WHERE l.LeadID = @PolicyLeadID
	AND @PolicyLeadID <> @LeadID

	INSERT INTO @Table (FieldCaption,DetailValue,ToolTip,HyperLink, [Order])
	SELECT 'Other Pets' as [FieldCaption],ldvName.DetailValue  as [DetailValue], 'Other Pets' as [Tooltip], '/CustomersLeadDetails2.aspx?lid='+ CAST(lOther.LeadID as VARCHAR)  AS [Hyperlink]
	,60 as [order] FROM dbo.Lead l WITH (NOLOCK) 
	INNER JOIN dbo.Lead lOther WITH (NOLOCK) ON l.CustomerID = lOther.CustomerID 
	INNER JOIN dbo.LeadDetailValues ldvName WITH (NOLOCK) ON lOther.LeadID = ldvName.LeadID AND ldvName.DetailFieldID = 144268 /*Pet Name*/
	WHERE l.LeadID = @LeadID
	AND lOther.LeadID != @LeadID
	AND lOther.LeadTypeID = 1492 /*Policy Admin*/
	

	INSERT @Display ( FieldCaption, DetailValue, ToolTip, HyperLink )
	SELECT FieldCaption, DetailValue,ToolTip,HyperLink
	FROM @Table 
	ORDER BY [Order] ASC

	RETURN

END




























GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_LeadInfoDisplay] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_LeadInfoDisplay] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_LeadInfoDisplay] TO [sp_executeall]
GO
