SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-10-28
-- Description:	Return Dataloader Estimated Times Of Completion
--
--
--		     ,_         _,
--		     |\\.-"""-.//|		|-------------------------------------------|	
--		     \`         `/		| To do: Output last 10 DataLoaderFileID's  |
--		    /    _   _    \		| if Null is provided as an input           |
--		    |    a _ a    |		|	                                        |
--		    '.=    Y    =.'	----|-------------------------------------------|
--		      >._  ^  _.<
--		     /   `````   \
--		     )           (
--		    ,(           ),
--		   / )   /   \   ( \
--		   ) (   )   (   ) (
--		   ( )   (   )   ( )
--		   )_(   )   (   )_(-.._
--		  (  )_  (._.)  _(  )_, `\
--		   ``(   )   (   )`` .' .'
--		      ```     ```   ( (`
--		                     '-'
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_DataLoaderETA] 
(	
	@DataloaderFileID INT = NULL
)
RETURNS @DataLoaderETC TABLE 
(
	ClientID			INT
	,MapID				INT
	,MapName			VARCHAR(2000)
	,FileID				INT
	,StartTime			DATETIME
	,FinishedTime		DATETIME
	,SecondsProcessing	INT
	,SecondsEachRow		Numeric(18,10)
	,CurrentRow			INT
	,RowsInFile			INT
	,RemainingRows		INT
	,ETAMinutes			INT
	,ETATime			DATETIME
	,[Completion%]			NUMERIC(18,2)
)
AS
BEGIN 

	IF @DataloaderFileID is null
	BEGIN
		
		SELECT TOP 1 @DataloaderFileID = DF.DataLoaderFileID
		FROM DataLoaderFile df WITH (NOLOCK) 
		WHERE df.TargetFileLocation ='inbox' and df.FileStatusID=3
		order by df.DataLoaderFileID desc

	END
	
	DECLARE @RowIndex		INT 
			,@StartTime     DATETIME
			,@RowsInFile    INT
			,@FinishedTime  DATETIME 
			,@ClientID int 
			,@MapID int
			,@FileID int
			,@MapName Varchar(2000)			

	SELECT @RowsInFile = df.RowsInFile, @FinishedTime = DF.DataLoadedDateTime, @ClientID = df.ClientID, @FileID = df.DataLoaderFileID,@MapID= df.DataLoaderMapID
	,@MapName = dm.MapName
	FROM DataLoaderFile df WITH (NOLOCK) 
	INNER JOIN DataLoaderMap dm WITH (NOLOCK) on dm.DataLoaderMapID = df.DataLoaderMapID
	WHERE df.DataLoaderFileID = @DataloaderFileID

	SELECT TOP 1 @RowIndex = ISNULL(dl.RowIndex,0) 
	FROM DataLoaderLog dl WITH (NOLOCK) 
	WHERE dl.DataLoaderFileID=@DataloaderFileID
	order by dl.DataLoaderLogID desc

	SELECT TOP 1 @StartTime = Convert(varchar(12),cast(CONVERT(DATE,(Left(RIGHT(Dl.Message,22),10)),103) as date),120) + ' ' + right(LEFT(RIGHT(Dl.Message,22),19),8) + '.000'
	FROM DataLoaderLog dl WITH (NOLOCK)
	WHERE dl.DataLoaderFileID=@DataloaderFileID
	order by dl.DataLoaderLogID asc

	DECLARE @SecondsSinceStart NUMERIC (18,2) = (SELECT CAST(DATEDIFF(SECOND,@StartTime,dbo.fn_GetDate_Local()) AS NUMERIC(18,5)))
	DECLARE @RowIndexN NUMERIC (18,5) = (SELECT cast(@RowIndex as NUMERIC(18,5))) 
	DECLARE @RowsInFileN NUMERIC (18,5) = (SELECT cast(@RowsInFile as NUMERIC(18,5))	)
	DECLARE @StartAndFinishDifSeconds NUMERIC(18,5) = ( SELECT CAST(DATEDIFF(SECOND,@StartTime,@FinishedTime) AS NUMERIC(18,5)))
	DECLARE @StartAndFinishDif int = ( SELECT DATEDIFF(SECOND,@StartTime,@FinishedTime) )
	
	INSERT INTO @DataLoaderETC
	SELECT 
	 @ClientID [ClientID]
	,@MapID [MapID]	
	,@MapName [MapName]
	,@FileID [FileID]
	,@StartTime [StartTime]
	,CASE WHEN ((@RowIndex) >= @RowsInFile) 
			THEN @FinishedTime
		ELSE null
	 END [FinishedTime]
	,CASE WHEN ((@RowIndex) >= @RowsInFile) 
			THEN @StartAndFinishDif
		ELSE @SecondsSinceStart
	END [SecondsProcessing] 
	,CASE WHEN ((@RowIndex) >= @RowsInFile) AND (@RowIndex) > 0
			THEN @StartAndFinishDifSeconds / @RowIndex 
		WHEN @RowIndex <= 0
			THEN 0
		ELSE @SecondsSinceStart	/ @RowIndex END [Seconds Each Row] 
	,@RowIndex [CurrentRow]
	,@RowsInFile [RowsInFile] 
	,ABS(@RowIndex - @RowsInFile) [RemainingRows]
	,CAST (CASE WHEN @RowIndex < @RowsInFile AND @RowIndex > 0 /*Don't Divide by 0*/
			THEN (	((@SecondsSinceStart / @RowIndexN ) * (@RowsInFileN - @RowIndexN )) / 60 )
		ELSE 0.00
	END AS NUMERIC (18,2)) [ETA Minutes]
	,DATEADD(MINUTE,
		CAST (CASE WHEN (@RowIndex) < @RowsInFile AND (@RowIndex) > 0
			THEN ( ((@SecondsSinceStart / @RowIndexN ) * (@RowsInFileN - @RowIndexN)) / 60 )
		ELSE 0.00
	END AS NUMERIC (18,2)),dbo.fn_GetDate_Local()) [ETATime]
	,CASE WHEN ( @RowIndex ) <= 0 THEN NULL 
	ELSE CAST ((( @RowIndexN / @RowsInFileN ) * 100 ) AS NUMERIC (18,2)) END AS [Completion]

RETURN

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_DataLoaderETA] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_DataLoaderETA] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_DataLoaderETA] TO [sp_executeall]
GO
