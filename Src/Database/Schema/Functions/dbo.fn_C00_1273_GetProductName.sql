SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-08-04
-- Description:	Returns the product name
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetProductName]
(
	@SchemeID INT
)
RETURNS VARCHAR(200)	
AS
BEGIN

	DECLARE @ProductName VARCHAR(200)
	
	SELECT @ProductName=pn.DetailValue 
	FROM ResourceListDetailValues pn WITH (NOLOCK) 
	WHERE pn.ResourceListID=@SchemeID AND pn.DetailFieldID=146200
 
	-- BEU testing only
--select @ProductName='Dog Plus'

	RETURN @ProductName
 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetProductName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetProductName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetProductName] TO [sp_executeall]
GO
