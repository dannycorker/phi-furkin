SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:      Louis Bromilow
-- Create date: 2014-02-20
-- Description: Select chars from string by ASCII range
-- CPS 2020-02-04: Replace call to master..spt_values with Tally so that this can compile through SSDT
-- =============================================

CREATE FUNCTION [dbo].[fnASCIIRangeFromString] 
( 
    @String NVARCHAR(MAX),
    @ASCIIFrom INT = 65,
    @ASCIITo INT = 90 
) 
RETURNS NVARCHAR(MAX) 
AS 
BEGIN 

DECLARE @RetCapWord VARCHAR(100)
SET @RetCapWord=''

;WITH CTE AS
(
	SELECT @String AS oldVal,1 AS TotalLen,SUBSTRING(@String,1,1) AS newVal,
	ASCII(SUBSTRING(@String,1,1)) AS AsciVal
	
	UNION ALL
	
	SELECT oldVal,TotalLen+1 AS TotalLen,
	SUBSTRING(@String,TotalLen+1,1) AS newVal,
	ASCII(SUBSTRING(@String,TotalLen+1,1)) AS AsciVal
	FROM CTE
	WHERE CTE.TotalLen<=LEN(@String)
)
SELECT @RetCapWord=@RetCapWord+newVal
FROM CTE
INNER JOIN Tally t WITH (NOLOCK) ON CTE.AsciVal=t.N AND CTE.AsciVal BETWEEN @ASCIIFrom AND @ASCIITo

RETURN @RetCapWord

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnASCIIRangeFromString] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnASCIIRangeFromString] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnASCIIRangeFromString] TO [sp_executeall]
GO
