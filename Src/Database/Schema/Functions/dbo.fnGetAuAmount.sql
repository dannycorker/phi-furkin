SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 06/03/2020
-- Description:	Gets a string representation of a two decimal value,
--				without the decimal place eg.,	123.44 would be 12344
--												20 would be 2000
--												20.4 would be 2040
--												0 would be 000
-- GPR 2020-03-26	| Added if block to transform the amount to non-negative if the amount is less than zero
-- =============================================
CREATE FUNCTION [dbo].[fnGetAuAmount]
(
	@Amount Decimal (18,2)
)
RETURNS VARCHAR(20)
AS
BEGIN

	DECLARE @Result VARCHAR(10)

	/*GPR 2020-03-26 for AAG-505 | If the amount is a negative, remove the sign*/
	IF @Amount < 0
	BEGIN
		SELECT @Amount = @Amount * - 1
	END

	SELECT @Result = Format( @Amount ,'N','en-US' )
	return Replace(@Result, '.', '')

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetAuAmount] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetAuAmount] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetAuAmount] TO [sp_executeall]
GO
