SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-08-05
-- Description:	Get DetailFieldID From Third party ID's 
-- =============================================
CREATE FUNCTION [dbo].[fnGetDetailFieldIDFromThirdPartyIDs]
(
	@ClientID INT,
	@LeadTypeID INT,
	@ThirdPartyFieldGroupID INT,
	@ThirdPartyFieldID INT
)
RETURNS INT
AS
BEGIN

	DECLARE @DetailFieldID INT

	SELECT @DetailFieldID = tpfm.DetailFieldID
	FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK)
	WHERE tpfm.ClientID = @ClientID
	AND tpfm.LeadTypeID = @LeadTypeID
	AND tpfm.ThirdPartyFieldGroupID = @ThirdPartyFieldGroupID
	AND tpfm.ThirdPartyFieldID = @ThirdPartyFieldID

	RETURN @DetailFieldID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDetailFieldIDFromThirdPartyIDs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetDetailFieldIDFromThirdPartyIDs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDetailFieldIDFromThirdPartyIDs] TO [sp_executeall]
GO
