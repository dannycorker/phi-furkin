SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2015-09-21
-- Description:	Produce XML from an Insolvency Customer to push to the ISI
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ISI_XmlPush]
(
	 @CustomerID									INT = 11468809
	,@FirstSubmission								BIT = 1
	,@PrimaryApplicant								BIT = 1
)
RETURNS XML
AS
BEGIN

	DECLARE  @OutputXML										XML
			,@XmlCaseDetails								XML
			,@XmlAppForm									XML
			,@XmlPFS										XML
			,@XmlKeyDates									XML
			,@XmlAttachment									XML
			,@XmlAppFormQuestions							XML
			,@XmlAppFormDebtor								XML
			,@XmlAppFormDebtorPersonal						XML
			,@XmlAppFormDebtorPreviousAddress				XML
			,@XmlAppFormDebtorEmployment					XML
			,@XmlAppFormDebtorPreviousBusiness				XML
			,@XmlAppFormDebtorInitialInfo					XML
			,@XmlAppFormDebtorInsolvencyStatus				XML
			,@XmlAppFormDebtorPriorInsolvency				XML
			,@XmlAppFormDebtorOtherEligibilityCriteria		XML
			,@XmlAppFormRLE									XML
			,@XmlAppFormRleDetails							XML
			,@XmlAppFormRleChildren							XML
			,@XmlAppFormRleSpecialCircumstances				XML
			,@XmlAppFormJurisdiction						XML
			,@EmploymentStatus								VARCHAR(2000)

	/*Construct each XML cluster one at a time*/
	SELECT @EmploymentStatus = dbo.fnGetSimpleDvLuli(295729,cu.CustomerID) /*Employment Status*/
	FROM Customers cu WITH ( NOLOCK )
	WHERE cu.CustomerID = @CustomerID 
				 
	/*Initial Info*/
	SELECT @XmlAppFormDebtorInitialInfo =
	(
	SELECT	 dbo.fnGetSimpleDvLuli(296388,cu.CustomerID) /*Has the Debtor received advice from a PIP...?*/	[InitialInfoPIA/II_DSA_Q1]
			,dbo.fnGetSimpleDvLuli(296389,cu.CustomerID) /*Has the PIP confirmed the advice in writing.?*/	[InitialInfoPIA/II_DSA_Q2]
			,dbo.fnGetSimpleDvLuli(296390,cu.CustomerID) /*Has the Debtor instructed the PIP in writing.*/	[InitialInfoPIA/II_DSA_Q3]
			,dbo.fnGetSimpleDvLuli(296391,cu.CustomerID) /*Does the Debtor agree to receiving notices...*/	[InitialInfoPIA/II_DSA_Q4]
			,dbo.fnGetSimpleDvLuli(296392,cu.CustomerID) /*Does the Debtor agree to receiving notices...*/	[InitialInfoPIA/II_DSA_Q5]
			,dbo.fnGetSimpleDvLuli(296388,cu.CustomerID) /*Has the Debtor received advice from a PIP...?*/	[InitialInfoDSA/II_PIA_Q1]
			,dbo.fnGetSimpleDvLuli(296389,cu.CustomerID) /*Has the PIP confirmed the advice in writing.?*/	[InitialInfoDSA/II_PIA_Q2]
			,dbo.fnGetSimpleDvLuli(296390,cu.CustomerID) /*Has the Debtor instructed the PIP in writing.*/	[InitialInfoDSA/II_PIA_Q3]
			,dbo.fnGetSimpleDvLuli(296391,cu.CustomerID) /*Does the Debtor agree to receiving notices...*/	[InitialInfoDSA/II_PIA_Q4]
			,dbo.fnGetSimpleDvLuli(296392,cu.CustomerID) /*Does the Debtor agree to receiving notices...*/	[InitialInfoDSA/II_PIA_Q5]
	FROM Customers cu WITH ( NOLOCK ) 
	WHERE cu.CustomerID = @CustomerID
	FOR XML PATH('')
	)

	/*Insolvency Status*/
	SELECT @XmlAppFormDebtorInsolvencyStatus =
	(
	SELECT	 dbo.fnGetSimpleDvLuli(296412,cu.CustomerID) /* Is the debtor an undischarged bankrupt?*/	[InsolvencyStatusDSA/IS_DSA_Q1]
			,dbo.fnGetSimpleDvLuli(296413,cu.CustomerID) /* Is the Debtor a discharged bankrupt sub*/	[InsolvencyStatusDSA/IS_DSA_Q2]
			,dbo.fnGetSimpleDvLuli(296414,cu.CustomerID) /* Is the Debtor a person who is a specifi*/	[InsolvencyStatusDSA/IS_DSA_Q3]
			,dbo.fnGetSimpleDvLuli(296415,cu.CustomerID) /* Is the Debtor a person who, as a debtor*/	[InsolvencyStatusDSA/IS_DSA_Q4]
			,dbo.fnGetSimpleDvLuli(296417,cu.CustomerID) /* control of the court under Part IV of t*/	[InsolvencyStatusDSA/IS_DSA_Q5]
			,dbo.fnGetSimpleDvLuli(296412,cu.CustomerID) /* Is the debtor an undischarged bankrupt?*/	[InsolvencyStatusPIA/IS_PIA_Q1]
			,dbo.fnGetSimpleDvLuli(296413,cu.CustomerID) /* Is the Debtor a discharged bankrupt sub*/	[InsolvencyStatusPIA/IS_PIA_Q2]
			,dbo.fnGetSimpleDvLuli(296414,cu.CustomerID) /* Is the Debtor a person who is a specifi*/	[InsolvencyStatusPIA/IS_PIA_Q3]
			,dbo.fnGetSimpleDvLuli(296415,cu.CustomerID) /* Is the Debtor a person who, as a debtor*/	[InsolvencyStatusPIA/IS_PIA_Q4]
			,dbo.fnGetSimpleDvLuli(296417,cu.CustomerID) /* control of the court under Part IV of t*/	[InsolvencyStatusPIA/IS_PIA_Q5]
	FROM Customers cu WITH ( NOLOCK ) 
	WHERE cu.CustomerID = @CustomerID
	FOR XML PATH('')
	)

	/*Prior Insolvency*/
	SELECT @XmlAppFormDebtorPriorInsolvency =
	(
	SELECT	 dbo.fnGetSimpleDvLuli(296420,cu.CustomerID) /*section 95 of the Act less th*/	[PriorInsolvencyDSA/PI_DSA_Q1]
			,dbo.fnGetSimpleDvLuli(296422,cu.CustomerID) /*Relief Notice less than 3 yea*/	[PriorInsolvencyDSA/PI_DSA_Q2]
			,dbo.fnGetSimpleDvLuli(296424,cu.CustomerID) /*Arrangement less than 5 years*/	[PriorInsolvencyDSA/PI_DSA_Q3]
			,dbo.fnGetSimpleDvLuli(296426,cu.CustomerID) /*the date of this application?*/	[PriorInsolvencyDSA/PI_DSA_Q4]
			,dbo.fnGetSimpleDvLuli(296427,cu.CustomerID) /*XML Prior Insolvency Has The */	[PriorInsolvencyDSA/PI_DSA_Q5]
			,dbo.fnGetSimpleDvLuli(296942,cu.CustomerID) /*Q5. Has the Debtor ever enter*/	[PriorInsolvencyDSA/PI_DSA_Q6]
			,dbo.fnGetSimpleDvLuli(296420,cu.CustomerID) /*section 95 of the Act less th*/	[PriorInsolvencyPIA/PI_PIA_Q1]
			,dbo.fnGetSimpleDvLuli(296422,cu.CustomerID) /*Relief Notice less than 3 yea*/	[PriorInsolvencyPIA/PI_PIA_Q2]
			,dbo.fnGetSimpleDvLuli(296424,cu.CustomerID) /*Arrangement less than 5 years*/	[PriorInsolvencyPIA/PI_PIA_Q3]
			,dbo.fnGetSimpleDvLuli(296426,cu.CustomerID) /*the date of this application?*/	[PriorInsolvencyPIA/PI_PIA_Q4]
			,dbo.fnGetSimpleDvLuli(296427,cu.CustomerID) /*XML Prior Insolvency Has The */	[PriorInsolvencyPIA/PI_PIA_Q5]
			,dbo.fnGetSimpleDvLuli(296942,cu.CustomerID) /*Q5. Has the Debtor ever enter*/	[PriorInsolvencyPIA/PI_PIA_Q6]
	FROM Customers cu WITH ( NOLOCK ) 
	WHERE cu.CustomerID = @CustomerID
	FOR XML PATH('')
	)

	/*Other Eligibility Criteria*/
	SELECT @XmlAppFormDebtorOtherEligibilityCriteria =
	(
	SELECT	 dbo.fnGetSimpleDvLuli(296431,cu.CustomerID) /*Other business in the State?*/ 	[OC_Q1]
			,dbo.fnGetSimpleDvLuli(296943,cu.CustomerID) /*XML Other Criteria Q1*/			[OC_Q1A]
			,dbo.fnGetSimpleDvLuli(296432,cu.CustomerID) /*Q2. Is the Debtor insolvent within the meaning of the Act?*/	[OC_Q2]
	FROM Customers cu WITH ( NOLOCK ) 
	WHERE cu.CustomerID = @CustomerID
	FOR XML PATH('')
	)

	/*Application Form Questions*/
	SELECT @XmlAppFormQuestions =
	(
	SELECT	 CASE	WHEN dbo.fnGetSimpleDvLuli(297052,cu.CustomerID) = 'Joint'
					THEN 'Joint'
					ELSE 'Individual' 
			 END 												[AQ_DSA_Q1]
			,dbo.fnGetSimpleDvLuliOrValue(297747,cu.CustomerID) [AQ_DSA_Q2]
			,dbo.fnGetSimpleDvLuliOrValue(297748,cu.CustomerID) [AQ_DSA_Q3]
			,CASE	WHEN dbo.fnGetSimpleDvLuli(297052,cu.CustomerID) = 'Joint'
					THEN 'Joint'
					ELSE 'Individual' 
			 END 												[AQ_PIA_Q1]
			,dbo.fnGetSimpleDvLuliOrValue(297745,cu.CustomerID) [AQ_PIA_Q2]
			,dbo.fnGetSimpleDvLuliOrValue(297746,cu.CustomerID) [AQ_PIA_Q2A]
			,dbo.fnGetSimpleDvLuliOrValue(297747,cu.CustomerID) [AQ_PIA_Q3]
			,dbo.fnGetSimpleDvLuliOrValue(297748,cu.CustomerID) [AQ_PIA_Q4]
			,dbo.fnGetSimpleDvLuliOrValue(297749,cu.CustomerID) [AQ_PIA_Q5]
			,dbo.fnGetSimpleDvLuliOrValue(297750,cu.CustomerID) [AQ_PIA_Q5A]
			,dbo.fnGetSimpleDvLuliOrValue(296944,cu.CustomerID) [AQ_PIA_Q6]
	FROM Customers cu WITH ( NOLOCK )
	WHERE cu.CustomerID = @CustomerID
	FOR XML PATH('')
	)

	/*Employment Details*/
	IF @EmploymentStatus = 'Employed'
	BEGIN
		SELECT @XmlAppFormDebtorEmployment = 
		(
		SELECT	 'Employed'															[EmploymentStatus]
				,dbo.fnGetSimpleDv(295732,cu.CustomerID) /*Occupation*/				[Occupation]
				,dbo.fnGetSimpleDv(295733,cu.CustomerID) /*EmployerName*/			[EmployerName]
				,dbo.fn_C00_ISI_ReturnXmlAddress(dbo.fnGetSimpleDv(295679,cu.CustomerID) /*Employers Address - Line 1*/
												,dbo.fnGetSimpleDv(295737,cu.CustomerID) /*Employers Address - Line 2*/
												,dbo.fnGetSimpleDv(295738,cu.CustomerID) /*Employers Address - Line 3*/
												,dbo.fnGetSimpleDv(295739,cu.CustomerID) /*Employers Address - Town*/
												,dbo.fnGetSimpleDv(295740,cu.CustomerID) /*Employers Address - County*/
												,''
												,'IRELAND')							[BusinessAddress]
				,	 'P'
					+ CONVERT(VARCHAR,dbo.fnGetSimpleDvAsInt(295680,cu.CustomerID)) + 'Y' /*Employed - Years*/
					+ CONVERT(VARCHAR,dbo.fnGetSimpleDvAsInt(295726,cu.CustomerID)) + 'M' /*Employed - Months*/ [LengthOfService]
				,dbo.fnGetSimpleDv(295731,cu.CustomerID) /*Comment*/				[Comment]
		FROM Customers cu WITH ( NOLOCK ) 
		WHERE cu.CustomerID = @CustomerID
		FOR XML PATH('EmplEmployed')
		)
	END
	ELSE IF @EmploymentStatus = 'Self-Employed/Trading'
	BEGIN
		SELECT @XmlAppFormDebtorEmployment = 
		(
		SELECT	 'Self-employed/Trading'											[EmploymentStatus]
				,dbo.fnGetSimpleDv(295732,cu.CustomerID) /*Occupation*/				[Occupation]
				,''																	[BusinessType]
				,dbo.fnGetSimpleDv(295733,cu.CustomerID) /*EmployerName*/			[BusinessName]
				,dbo.fn_C00_ISI_ReturnXmlAddress(dbo.fnGetSimpleDv(295679,cu.CustomerID) /*Employers Address - Line 1*/
												,dbo.fnGetSimpleDv(295737,cu.CustomerID) /*Employers Address - Line 2*/
												,dbo.fnGetSimpleDv(295738,cu.CustomerID) /*Employers Address - Line 3*/
												,dbo.fnGetSimpleDv(295739,cu.CustomerID) /*Employers Address - Town*/
												,dbo.fnGetSimpleDv(295740,cu.CustomerID) /*Employers Address - County*/
												,''
												,'IRELAND')							[BusinessAddress]
				,'Yes'																[IsBusinessVatRegistered]
				,''																	[VATNumber]
				,'Yes'																[IsDebtorSoleTrader]
				,100																[OwnershipPercentage]
				,CONVERT(VARCHAR,CURRENT_TIMESTAMP,121)								[TradingStartDate]
				,dbo.fnGetSimpleDv(295731,cu.CustomerID) /*Comment*/				[Comment]
		FROM Customers cu WITH ( NOLOCK ) 
		WHERE cu.CustomerID = @CustomerID
		FOR XML PATH('EmplSelfEmployed')
		)
	END
	ELSE IF @EmploymentStatus = 'Unemployed'
	BEGIN
		SELECT @XmlAppFormDebtorEmployment = 
		(
		SELECT	 'Unemployed'														[EmploymentStatus]
				,dbo.fnGetSimpleDv(295731,cu.CustomerID) /*Comment*/				[Comment]
		FROM Customers cu WITH ( NOLOCK ) 
		WHERE cu.CustomerID = @CustomerID
		FOR XML PATH('EmplUnemployed')
		)
	END
	ELSE IF @EmploymentStatus = 'Retired'
	BEGIN
		SELECT @XmlAppFormDebtorEmployment = 
		(
		SELECT	 'Retired'															[EmploymentStatus]
				,dbo.fnGetSimpleDv(295731,cu.CustomerID) /*Comment*/				[Comment]
		FROM Customers cu WITH ( NOLOCK )
		WHERE cu.CustomerID = @CustomerID
		FOR XML PATH('EmplRetired')
		)
	END
	ELSE IF @EmploymentStatus = 'Student'
	BEGIN
		SELECT @XmlAppFormDebtorEmployment = 
		(
		SELECT	 'Student'															[EmploymentStatus]
				,dbo.fnGetSimpleDv(295731,cu.CustomerID) /*Comment*/				[Comment]
		FROM Customers cu WITH ( NOLOCK )
		WHERE cu.CustomerID = @CustomerID
		FOR XML PATH('EmplStudent')
		)
	END
	ELSE IF @EmploymentStatus = 'Housewife/Husband'
	BEGIN
		SELECT @XmlAppFormDebtorEmployment = 
		(
		SELECT	 'Housewife/husband'															[EmploymentStatus]
				,dbo.fnGetSimpleDv(295731,cu.CustomerID) /*Comment*/				[Comment]
		FROM Customers cu WITH ( NOLOCK )
		WHERE cu.CustomerID = @CustomerID
		FOR XML PATH('EmplHousewifeHusband')
		)
	END
	ELSE IF @EmploymentStatus = 'Other (Please specify)'
	BEGIN
		SELECT @XmlAppFormDebtorEmployment = 
		(
		SELECT	 'Other'															[EmploymentStatus]
				,dbo.fnGetSimpleDv(295730,cu.CustomerID) /*Details*/				[Details]
				,dbo.fnGetSimpleDv(295731,cu.CustomerID) /*Comment*/				[Comment]
		FROM Customers cu WITH ( NOLOCK )
		WHERE cu.CustomerID = @CustomerID
		FOR XML PATH('EmplOther')
		)
	END

	/*Personal*/
	SELECT @XmlAppFormDebtorPersonal = 
	(
	SELECT	 CASE WHEN t.Title IN('Mr','Mrs','Ms','Miss') THEN t.Title	ELSE 'Other' END 	[Title]
			,CASE WHEN t.Title IN('Mr','Mrs','Ms','Miss') THEN ''		ELSE t.Title END	[OtherTitle]
			,cu.FirstName																	[FirstName]
			,cu.LastName																	[LastName]
			,dbo.fnGetSimpleDv(296394,cu.CustomerID)/*Name as it on Birth Certificate*/		[BirthCertName]
			,dbo.fnGetSimpleDv(296395,cu.CustomerID)/*Previous Name(s)*/					[PreviouslyKnownNames]
			,dbo.fnGetSimpleDv(296397,cu.CustomerID)/*PPS Number*/							[PPSN]
			,dbo.fnGetSimpleDv(296396,cu.CustomerID)/*Gender*/								[Gender]
			,CONVERT(VARCHAR(10),cu.DateOfBirth,121)										[BirthDate]
			,dbo.fnGetSimpleDvLuli(296398,cu.CustomerID)/*Country of Birth*/				[BirthCountry]
			,dbo.fnGetSimpleDv(296399,cu.CustomerID)/*Nationality*/							[Nationality]
			,dbo.fnGetSimpleDvLuli(295678,cu.CustomerID)/*Marital Status*/					[MaritalStatus]
			,cu.HomeTelephone																[Phone]
			,cu.MobileTelephone																[Mobile]
			,cu.EmailAddress																[Email]
			,dbo.fnGetSimpleDv(295731,cu.CustomerID) /*Comment*/							[Comment]
	FROM Customers cu WITH ( NOLOCK )
	LEFT JOIN Titles t WITH ( NOLOCK ) on t.TitleID = cu.TitleID 
	WHERE cu.CustomerID = @CustomerID
	FOR XML PATH('')
	)

	/*Debtor Section*/
	SELECT @XmlAppFormDebtor = 
	(
	SELECT	 @XmlAppFormDebtorPersonal					[Personal]
			,@XmlAppFormDebtorPreviousAddress			[PreviousAddress]
			,@XmlAppFormDebtorEmployment				[Employment]
			,@XmlAppFormDebtorPreviousBusiness			[PreviousBusiness]
			,@XmlAppFormDebtorInitialInfo				[InitialInfo]
			,@XmlAppFormDebtorInsolvencyStatus			[InsolvencyStatus]
			,@XmlAppFormDebtorPriorInsolvency			[PriorInsolvency]
			,@XmlAppFormDebtorOtherEligibilityCriteria	[OtherEligibilityCriteria]
	FOR XML PATH('')
	)

	/*RLE Details*/
	SELECT @XmlAppFormRleDetails =
	(
	SELECT	 CASE	WHEN dbo.fnGetSimpleDvLuli(295707,cu.CustomerID) = 2 
					THEN 'Two Adult Household' ELSE 'One Adult Household' 
			 END															[RLE_Details_Q1]
			,CASE	WHEN dbo.fnGetSimpleDv(296258,cu.CustomerID) > '0' 
					THEN 'Yes' ELSE 'No'
			 END															[RLE_Details_Q2]
			,CASE	WHEN dbo.fnGetSimpleDvLuli(295708,cu.CustomerID) > '0' 
					THEN 'Yes' ELSE 'No'
			 END															[RLE_Details_Q3]
			,'Yes' /*Does the debtor require a car?*/						[RLE_Details_Q3_1]  
			,CASE	WHEN dbo.fnGetSimpleDvAsMoney(295785,cu.CustomerID) > 0.00
					THEN 'Yes' ELSE 'No'
			 END															[RLE_Details_Q4]
			,dbo.fnGetSimpleDvAsMoney(295785,cu.CustomerID)					[RLE_Details_Q4_1]
			,ISNULL(dbo.fnGetSimpleDvAsMoney(295783,cu.CustomerID),0.00)					
				+ISNULL(dbo.fnGetSimpleDvAsMoney(295784,cu.CustomerID),0.00)[RLE_Details_Q5] 
			,CASE	WHEN dbo.fnGetSimpleDvAsMoney(295725,cu.CustomerID) > 0.00
					THEN 'Yes' ELSE 'No'
			 END															[RLE_Details_Q6]
			,CASE	WHEN dbo.fnGetSimpleDvAsMoney(296280,cu.CustomerID) > 0.00
					THEN 'No' ELSE 'Yes'
			 END															[RLE_Details_Q7]
			,dbo.fnGetSimpleDvAsMoney(296280,cu.CustomerID)					[RLE_Details_Q7_1]
	FROM Customers cu WITH ( NOLOCK ) 
	WHERE cu.CustomerID = @CustomerID
	FOR XML PATH('')
	)

	/*Children*/
	SELECT @XmlAppFormRleChildren =
	(
	SELECT	CASE cdv.DetailFieldID 
			WHEN 296253 THEN 'Infant (0-2)' 
			WHEN 296254 THEN 'Pre-school (3)'
			WHEN 296255 THEN 'Primary school (4-11)'
			WHEN 296256 THEN 'Secondary school (12-18)'
			END [Category]
	FROM CustomerDetailValues cdv WITH ( NOLOCK ) 
	INNER JOIN Tally t WITH ( NOLOCK ) on t.N <= cdv.ValueInt
	WHERE cdv.DetailFieldID IN(296253,296254,296255,296256)
	AND cdv.CustomerID = @CustomerID
	FOR XML PATH('Child')
	)

	/*Special Circumstances*/
	SELECT @XmlAppFormRleSpecialCircumstances = 
	(
	SELECT	 tdvAmount.ValueMoney	[RLEAmount]
			,tdvReason.DetailValue	[RLEExpenditureDetails]
			,tdvCommnt.DetailValue	[RLEComment]
	FROM TableRows tr WITH ( NOLOCK ) 
	INNER JOIN TableDetailValues tdvAmount WITH (NOLOCK) on tdvAmount.TableRowID = tr.TableRowID AND tdvAmount.DetailFieldID = 298607 /*Amount*/
	INNER JOIN TableDetailValues tdvReason WITH (NOLOCK) on tdvReason.TableRowID = tr.TableRowID AND tdvReason.DetailFieldID = 298608 /*Reason for special circumstance*/
	INNER JOIN TableDetailValues tdvCommnt WITH (NOLOCK) on tdvCommnt.TableRowID = tr.TableRowID AND tdvCommnt.DetailFieldID = 298609 /*Comments*/
	WHERE tr.CustomerID = @CustomerID 
	AND tr.DetailFieldID = 298606 /*Detail of special circumstance costs*/
	FOR XML PATH('')
	)

	/*Jurisdiction*/
	SELECT @XmlAppFormJurisdiction =
	(
	SELECT	 REPLACE(dbo.fnGetSimpleDvLuli(296406,cu.CustomerID),'COUNTY OF ','')	[County]
			,REPLACE(dbo.fnGetSimpleDvLuli(296408,cu.CustomerID),' Circuit','')		[Court]
			,dbo.fnGetSimpleDvLuliOrValue(296407,cu.CustomerID)						[Reason]
	FROM Customers cu WITH ( NOLOCK ) 
	WHERE cu.CustomerID = @CustomerID
	FOR XML PATH('')
	)

	/*RLE*/
	SELECT @XmlAppFormRLE =
	(
	SELECT	 @XmlAppFormRleDetails				[Details]				-- DONE
			,@XmlAppFormRleChildren										-- DONE
			,@XmlAppFormRleSpecialCircumstances	[SpecialCircumstance]	-- DONE
	FOR XML PATH('')
	)

	/*Case Details*/
	SELECT @XmlCaseDetails = 
	(
	SELECT	 TOP(1)
			 ISNULL(dbo.fnGetSimpleDv(297057,cu.CustomerID),'') /*ISI Case Reference Number*/	[CaseRefNo]
			,CONVERT(VARCHAR,cu.CustomerID)														[OwnRefNo]
			,ISNULL(dbo.fnGetSimpleDv(296923,cu.CustomerID),'') /*XML CaseUNID*/				[CaseUNID]
			,ISNULL(dbo.fnGetRLDv(297434,295668,m.CaseID),'')	 /*Pip Name*/					[PIPName]
			,ISNULL(dbo.fnGetRLDv(297434,295670,m.CaseID),'')	 /*Pip Ref*/					[PIPRef]
			,'PIA'																				[CaseType]
			,CASE	WHEN dbo.fnGetSimpleDvLuli(297052,cu.CustomerID) = 'Joint'
					THEN 'Joint'
					ELSE 'Individual' 
			 END 																				[AppType]
			,ISNULL(dbo.fnGetSimpleDv(296928,cu.CustomerID),'')	/*XML CaseDescription*/			[CaseDescription] 
	FROM Customers cu WITH ( NOLOCK ) 
	INNER JOIN Lead l WITH (NOLOCK) on l.CustomerID = cu.CustomerID
	INNER JOIN Matter m WITH (NOLOCK) on m.LeadID = l.LeadID 
	WHERE cu.CustomerID = @CustomerID
	FOR XML PATH('')
	)

	/*App Form*/
	SELECT @XmlAppForm = 
	(
	SELECT	 --TOP(1)
			 ISNULL(dbo.fnGetSimpleDv(296928,@CustomerID),'')															[OwnDescription]
			,dbo.fn_C00_ISI_ReturnXmlAddress( dbo.fnGetSimpleDv(297913,tr.TableRowID) /*Street Line 1*/
											, dbo.fnGetSimpleDv(297914,tr.TableRowID) /*Street Line 2*/
											, dbo.fnGetSimpleDv(297915,tr.TableRowID) /*Street Line 3*/
											, dbo.fnGetSimpleDv(297916,tr.TableRowID) /*Town*/
											, ''									  /*No County for PPR in AQ*/
											, dbo.fnGetSimpleDv(297917,tr.TableRowID) /*Zip Code*/
											, UPPER(dbo.fnGetSimpleDv(297912,tr.TableRowID)) /*Country*/
											)																			[PPRAddress]
			,@XmlAppFormQuestions																						[ApplicationQuestions]
			,@XmlAppFormDebtor																							[Debtor]
			,@XmlAppFormRLE																								[RLE]
			,@XmlAppFormJurisdiction																					[Jurisdiction]
	FROM Customers cu WITH ( NOLOCK ) 
	LEFT JOIN TableRows tr WITH ( NOLOCK ) on tr.CustomerID = cu.CustomerID AND tr.DetailFieldID = 297762 /*Principal Private Residence Lender*/
	WHERE cu.CustomerID = @CustomerID
	AND NOT EXISTS ( SELECT * FROM TableRows tr2 WITH ( NOLOCK ) WHERE tr2.DetailFieldID = tr.DetailFieldID AND tr2.CustomerID = tr.CustomerID AND tr2.TableRowID < tr.TableRowID )
	FOR XML PATH('')
	)

	/*Key Dates*/
	SELECT @XmlKeyDates = 
	(
	SELECT	 LEFT(dbo.fnGetSimpleDv(297049,cu.CustomerID),10) /*Case Created*/	[CaseCreated]
			,LEFT(dbo.fnGetSimpleDv(297050,cu.CustomerID),10) /*Case Completed*/ [CaseCompleted]
	FROM Customers cu WITH ( NOLOCK ) 
	WHERE cu.CustomerID = @CustomerID
	FOR XML PATH('')
	)				
	
	/*Prescribed Financial Statement*/
	SELECT @XmlPFS = 
	(
	SELECT	 dbo.fn_C00_ISI_XmlPush_Asset(@CustomerID)	[Asset]
			,dbo.fn_C00_ISI_XmlPush_Debt(@CustomerID)	[Debt]
	FOR XML PATH ('')
	)

	/*Combine Everything*/
	SELECT @OutputXml = 
	(
	SELECT	 CASE WHEN @FirstSubmission = 1 THEN 'New' ELSE 'Resubmit' END		[Submission]	-- DONE
			,@XmlCaseDetails													[CaseDetails]	-- DONE
			,@XmlAppForm														[AppForm]		-- DONE
			,@XmlPFS															[PFS]
			,@XmlKeyDates														[KeyDates]		-- DONE
			,@XmlAttachment														[Attachment]	-- N/A
	FOR XML PATH(''), ROOT('ISICase')
	)
	
	RETURN @OutputXml

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_XmlPush] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ISI_XmlPush] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_XmlPush] TO [sp_executeall]
GO
