SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Joseph Gama  http://stackoverflow.com/questions/9194968/sql-server-2008-advanced-search-sorting
-- Create date: 2016-01-21
-- Description: Returns the minimum number of single-character edits (i.e. insertions, deletions or substitutions) required to change one word into the other
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_LevenshteinDistance]
(
	@s nVarchar(MAX)
	,@t nVarchar(MAX) 
)
RETURNS INT
AS
BEGIN

DECLARE @d NVARCHAR(MAX), @LD INT, @m INT, @n INT, @i INT, @j INT,
    @s_i NCHAR(1), @t_j NCHAR(1),@cost INT

  SET @n = LEN(@s) 
  SET @m = LEN(@t) 
  SET @d = REPLICATE(NCHAR(0),(@n+1)*(@m+1))
  
  IF @n = 0 or @m=0
  BEGIN
    SET @LD = @m
  END
  ELSE
  BEGIN

	  SET @i = 0
	  WHILE @i <= @n 
	  BEGIN
		SET @d = STUFF(@d,@i+1,1,NCHAR(@i))        --d(i, 0) = i
		SET @i = @i+1
	  END

	  SET @i = 0
	  WHILE @i <= @m 
	  BEGIN
		SET @d = STUFF(@d,@i*(@n+1)+1,1,NCHAR(@i))    --d(0, j) = j
		SET @i = @i+1
	  END

	  SET @i = 1
	  WHILE @i <= @n BEGIN
		SET @s_i = SUBSTRING(@s,@i,1)

		SET @j = 1
		WHILE @j <= @m BEGIN
		  SET @t_j = SUBSTRING(@t,@j,1)

		  IF @s_i = @t_j
			SET @cost = 0
		  ELSE
			SET @cost = 1

		  SET @d = STUFF(@d,@j*(@n+1)+@i+1,1,
			NCHAR(
			dbo.fnminof(dbo.fnminof(UNICODE(SUBSTRING(@d,@j*(@n+1)+@i-1+1,1))+1,
			  UNICODE(SUBSTRING(@d,(@j-1)*(@n+1)+@i+1,1))+1)
			 ,(UNICODE(SUBSTRING(@d,(@j-1)*(@n+1)+@i-1+1,1))+@cost)
			  )
			))

		  SET @j = @j+1
		END
		SET @i = @i+1
	  END      

		SET @LD = UNICODE(SUBSTRING(@d,@n*(@m+1)+@m+1,1))
	END

	RETURN @LD

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_LevenshteinDistance] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_LevenshteinDistance] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_LevenshteinDistance] TO [sp_executeall]
GO
