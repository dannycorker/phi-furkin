SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2019-04-09
-- Description:	Modified structure found in fn_C600_GetTemplateSavedQuoteXml for L&G implementation - Include Address1, Address2, TownCity
-- 2021-02-15 GPR Added Enrollment Fee structure
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetTemplateSavedQuoteXml_AdditionalAddressData]
(
)
RETURNS XML
AS
BEGIN
	
	DECLARE @Xml XML
	
	SELECT @Xml =
	'<BuyPolicyRequest>
	  <ClientId>_ClientId_</ClientId>
	  <ClientPersonnelId>_ClientPersonnelId_</ClientPersonnelId>
	  <Password>_Password_</Password>
	  <BrandingId>_BrandingId_</BrandingId>
	  <QuoteId>_QuoteId_</QuoteId>
	  <SessionId>_SessionId_</SessionId>
	  <AdminFee>_AdminFee_</AdminFee>
	  <TransactionRef>_TransactionRef_</TransactionRef>
	  <CustomerInfo>
		<CustomerId>_CustomerId_</CustomerId>
		<TitleId>_TitleId_</TitleId>
		<FirstName>_FirstName_</FirstName>
		<LastName>_LastName_</LastName>
		<HomePhone>_HomePhone_</HomePhone>
		<MobilePhone>_MobilePhone_</MobilePhone>
		<Email>_Email_</Email>
		<Address1>_Address1_</Address1>
		<Address2>_Address2_</Address2>
		<TownCity>_TownCity_</TownCity>
		<Postcode>_Postcode_</Postcode>
		<SecondaryTitleId>0</SecondaryTitleId>
		<SecondaryFirstName />
		<SecondaryLastName />
		<SecondaryEmail />
		<SecondaryHomePhone />
		<SecondaryMobilePhone />
		<ExistingPolicyHolder>_ExistingPolicyHolder_</ExistingPolicyHolder>
		<DoNotSms>_DoNotSms_</DoNotSms>
		<DoNotEmail>_DoNotEmail_</DoNotEmail>
		<DoNotPhone>_DoNotPhone_</DoNotPhone>
		<DoNotPost>_DoNotPost_</DoNotPost>
		<DateOfBirth>_DateOfBirth_</DateOfBirth>
		<ContactMethodId>_ContactMethodId_</ContactMethodId>
		<MarketingPreferenceId>_MarketingPreferenceId_</MarketingPreferenceId>
	  </CustomerInfo>
	  <HouseholdInfo>
		<NumberOfPeople>0</NumberOfPeople>
		<MaritalStatusID>0</MaritalStatusID>
		<NumberOfPets>0</NumberOfPets>
	  </HouseholdInfo>
	  <PaymentInfo>
		<PaymentDay>0</PaymentDay>
		<PaymentMethodTypeId>0</PaymentMethodTypeId>
		<PaymentIntervalId>0</PaymentIntervalId>
		<SortCode />
		<AccountNumber />
		<BankName />
		<BankAccountName />
		<CreditCardToken />
		<CreditCardHolderName />
		<CreditCardNumberMasked />
		<CreditCardExpireDate />
		<CreditCardType />
		<RenewalMethodId>0</RenewalMethodId>
		<Authority>false</Authority>
		<NotAccountHolder>false</NotAccountHolder>
	  </PaymentInfo>
	  <PetQuotes>
		<PetQuote>
		  <PetInfo>
			<PetRef>_PetRef_</PetRef>
			<PetName>_PetName_</PetName>
			<SpeciesId>_SpeciesId_</SpeciesId>
			<BreedId>_BreedId_</BreedId>
			<Gender>_Gender_</Gender>
			<BirthDate>_BirthDate_</BirthDate>
			<IsNeutered>_IsNeutered_</IsNeutered>
			<HasMicrochip>_HasMicrochip_</HasMicrochip>
			<MicrochipNo>_MicrochipNo_</MicrochipNo>
			<PurchasePrice>_PurchasePrice_</PurchasePrice>
			<VaccinationsUpToDate>_VaccinationsUpToDate_</VaccinationsUpToDate>
			<HasExistingConditions>_HasExistingConditions_</HasExistingConditions>
			<PetColorId>_PetColorId_</PetColorId>
			<CoInsuranceNextYear>_CoInsuranceNextYear_</CoInsuranceNextYear>
		  </PetInfo>
		  <VetID>_VetID_</VetID>
		  <PolicyValues>
			<PolicyValue>
			  <ProductId>_ProductId1_</ProductId>
			  <PolicyLimit>0</PolicyLimit>
			  <Excess>0</Excess>
			  <VoluntaryExcess>0</VoluntaryExcess>
			  <CoInsurance>0</CoInsurance>
			</PolicyValue>
		  </PolicyValues>
		  <PolicyValueEnrollmentFees>
			<EnrollmentFee>
				<FeeTableRowID>_FeeTableRowID_</FeeTableRowID>
				<EnrollmentFee>_EnrollmentFee_</EnrollmentFee>
				<TaxTableRowID>_TaxTableRowID_</TaxTableRowID>
				<Tax>_Tax_</Tax>
				<Total>_Total_</Total>
			</EnrollmentFee>
		  </PolicyValueEnrollmentFees>
		  <StartDate>_StartDate_</StartDate>
		  <UnderwritingList>
			<UnderwritingAnswerItemType>
			  <AnswerId>_AnswerId1_</AnswerId>
			  <QuestionId>_QuestionId1_</QuestionId>
			</UnderwritingAnswerItemType>
			<UnderwritingAnswerItemType>
			  <AnswerId>_AnswerId2_</AnswerId>
			  <QuestionId>_QuestionId2_</QuestionId>
			  <AnswerText>_AnswerText2_</AnswerText>
			</UnderwritingAnswerItemType>
		  </UnderwritingList>
		</PetQuote>
	  </PetQuotes>
	  <OtherPetsInsured>_OtherPetsInsured_</OtherPetsInsured>
	  <DiscountCodes>_DiscountCodes_</DiscountCodes>
	  <KeyValues>_KeyValues_</KeyValues>
	</BuyPolicyRequest>'
	
	RETURN @Xml
	
END










GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetTemplateSavedQuoteXml_AdditionalAddressData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetTemplateSavedQuoteXml_AdditionalAddressData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetTemplateSavedQuoteXml_AdditionalAddressData] TO [sp_executeall]
GO
