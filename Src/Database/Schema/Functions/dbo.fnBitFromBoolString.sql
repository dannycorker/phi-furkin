SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Chris Townsend
-- Create date: 2011-01-19
-- Description:	Return a bit from a string representation of a boolean (false, true, 1, 0, yes, no)
-- =============================================
CREATE FUNCTION [dbo].[fnBitFromBoolString]
(
	@Instr varchar(5)
)
RETURNS bit
AS
BEGIN

	/* Return value is 0 (false) by default */
	DECLARE @retVal bit = 0

	/* Choose 'true' or 'false' if possible */
	SELECT @retVal = CASE @Instr 
						 WHEN '1' THEN 1
						 WHEN 'Y' THEN 1
						 WHEN 'YES' THEN 1
						 WHEN 'TRUE' THEN 1
						 WHEN '0' THEN 0
						 WHEN 'N' THEN 0
						 WHEN 'NO' THEN 0
						 WHEN 'FALSE' THEN 0
					 END 
	
	RETURN @retVal

END









GO
GRANT VIEW DEFINITION ON  [dbo].[fnBitFromBoolString] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnBitFromBoolString] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnBitFromBoolString] TO [sp_executeall]
GO
