SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author	: Aquarium (2020-08-20)
-- Jira		: [SDPRU-9]
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_Policy_GetPolicyCoPayOrDefault]
(
	@ClientID INT,
	@PolicyMatterID INT,
	@StartDate DATE,
	@BirthDate DATE,
	@SpeciesID INT,
	@SpecialProcessing INT,
	@Breed VARCHAR(2000),
	@CoPay MONEY
)
RETURNS MONEY
BEGIN
	
	
	DECLARE @VetFeesID INT = dbo.fn_C00_1272_GetVetFeesResourceList(@ClientID)

	DECLARE @DefaultCoIns MONEY = NULL,
			@CoInsOptions VARCHAR(2000) = NULL
	SELECT	TOP 1 
			@DefaultCoIns = ExcessPercentage, 
			@CoInsOptions = Options
	FROM	dbo.fn_C00_1272_Policy_GetPolicyCoInsuranceWithData(@PolicyMatterID, @BirthDate, @SpeciesID, @SpecialProcessing, @Breed) cop
	WHERE	cop.ResourceListID = @VetFeesID
	AND		cop.DateCutOff <= @StartDate
	ORDER BY cop.DateCutOff DESC

	-- If no value has been passed in or if that value is not in the options then default
	IF @CoPay IS NULL OR NOT EXISTS (
		SELECT	* 
		FROM	dbo.fnTableOfValuesFromCSV(@CoInsOptions)
		WHERE	AnyValue = @CoPay
	)
	BEGIN
		SELECT @CoPay = @DefaultCoIns
	END

	RETURN @CoPay

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPolicyCoPayOrDefault] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_Policy_GetPolicyCoPayOrDefault] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPolicyCoPayOrDefault] TO [sp_executeall]
GO
