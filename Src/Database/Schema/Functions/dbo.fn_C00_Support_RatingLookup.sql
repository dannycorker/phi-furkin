SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2019-11-14
-- Description:	Gets relevent rating details - saves time performing manual lookups when debugging a rating
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_Support_RatingLookup]
(	
	@InceptionDate DATE = NULL
	,@YearStart DATE = NULL
	,@BirthDate DATE = NULL
	,@GrossPremium MONEY = NULL
	,@DiscountValue MONEY = NULL
	,@AdminFee MONEY = NULL
	,@BreedName VARCHAR(100) = NULL
	,@AilmentName VARCHAR(100) = NULL
	,@PostCode VARCHAR(10) = NULL
)
RETURNS @Data TABLE
(
	AgeInMonthsAtYearStart INT,
	AgeInMonthsAtInception INT,
	LastYearExitPremium MONEY,
	AgeType VARCHAR(10),
	AilmentRecurrence VARCHAR(10),
	IPTfree VARCHAR(3)
)
AS
BEGIN


DECLARE @AgeInMonths INT
		,@AgeInMonthsAtInception INT
		,@PremiumAtLastPeriodExit MONEY
		,@IPT MONEY
		,@Net MONEY
		,@AgeType VARCHAR(10)
		,@Recurrence VARCHAR(10)
		,@RegionString VARCHAR(100)
		,@IPTMultiplier DECIMAL(18,2)

		/*Calculate AgeInMonthsAtYearStart*/
		SELECT @AgeInMonths =	CASE 
									WHEN DATEPART(DAY, @BirthDate) > DATEPART(DAY, @YearStart)
									THEN DATEDIFF(MONTH, @BirthDate, @YearStart) - 1
									ELSE DATEDIFF(MONTH, @BirthDate, @YearStart)
								END

		/*Calculate AgeInMonthsAtInception*/
		SELECT @AgeInMonthsAtInception =	CASE 
												WHEN DATEPART(DAY, @BirthDate) > DATEPART(DAY, @InceptionDate)
												THEN DATEDIFF(MONTH, @BirthDate, @InceptionDate) - 1
												ELSE DATEDIFF(MONTH, @BirthDate, @InceptionDate)
											END
		
		/*Calculate PremiumAtLastPeriodExit*/
		IF LEFT(@PostCode,3) IN ('GY1','GY2','GY3','GY4','GY5','GY6','GY7','GY8','GY9','GY10','IM1','IM2','IM3','IM4','IM5','IM6','IM7','IM8','IM9','JE2','JE3')
		BEGIN
			SELECT @IPTMultiplier = 1
		END
		ELSE
		BEGIN
			SELECT TOP 1 @IPTMultiplier=i.GrossMultiplier 
			FROM IPT i WITH (NOLOCK) 
			WHERE (i.[From] IS NULL OR DATEADD(DAY,-1,@YearStart) /*Last terms IPT*/ >= i.[From])
			AND (i.[To] IS NULL OR DATEADD(DAY,-1,@YearStart) /*Last terms IPT*/ <= i.[To])
			AND i.Region IS NULL
		END

		SELECT @Net = @GrossPremium / @IPTMultiplier
		SELECT @IPT = @GrossPremium - @Net
		SELECT @PremiumAtLastPeriodExit = ISNULL(@GrossPremium,0.00) - ISNULL(@IPT,0.00) + ISNULL(@DiscountValue, 0.00) - ISNULL(@AdminFee, 0.00)


		/*Calculate AgeType value*/
		SELECT @AgeType = agetypeli.ItemValue
		FROM ResourceListDetailValues petbreed WITH (NOLOCK)
		INNER JOIN ResourceListDetailValues agetype WITH (NOLOCK) ON petbreed.ResourceListID = agetype.ResourceListID AND agetype.DetailFieldID = 179682
		INNER JOIN LookupListItems agetypeli WITH (NOLOCK) ON agetypeli.LookupListItemID = agetype.ValueInt
		WHERE petbreed.DetailfieldID = 144270
		AND petbreed.DetailValue LIKE '%'+@BreedName+'%'
	
		/*Calculate Recurrence value*/
		SELECT @Recurrence = recurrenceli.ItemValue
		FROM ResourceListDetailValues cause WITH (NOLOCK)
		INNER JOIN ResourceListDetailValues recurrence WITH (NOLOCK) ON cause.ResourceListID = recurrence.ResourceListID AND recurrence.DetailFieldID = 180185
		INNER JOIN LookupListItems recurrenceli WITH (NOLOCK) ON recurrenceli.LookupListItemID = recurrence.ValueInt
		WHERE cause.DetailfieldID = 144340
		AND cause.DetailValue LIKE '%'+@AilmentName+'%'


		/*Collate outputs as the output*/
		INSERT	@Data (AgeInMonthsAtYearStart, AgeInMonthsAtInception, LastYearExitPremium, AgeType, AilmentRecurrence, IPTfree)
		SELECT @AgeInMonths, @AgeInMonthsAtInception, @PremiumAtLastPeriodExit, @AgeType, @Recurrence, CASE @IPTMultiplier WHEN 1 THEN 'Yes' ELSE 'No' END

RETURN

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_Support_RatingLookup] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_Support_RatingLookup] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_Support_RatingLookup] TO [sp_executeall]
GO
