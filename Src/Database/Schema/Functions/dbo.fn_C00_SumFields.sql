SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-10-18
-- Description:	Function to sum a given level of data based on 
--				a given id Lead Matter Etc
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_SumFields] 
(
@DetailFieldSubTypeLevel int,
@Ident int,
@DetailFieldID int
)
RETURNS money
AS
BEGIN
	

	/*
		@DetailFieldSubTypeLevel Is the level of ID you will pass in, IE CustomerID LeadID
		@Ident is the identity to use
		@DetailFieldID is the field to SUM
	*/	
	
	DECLARE @LeadOrMatter int,
			@Money money
	
	SELECT @LeadOrMatter = df.LeadOrMatter
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @DetailFieldID
	
	/*2 = MatterDetailValues*/
	IF @LeadOrMatter = 2
	BEGIN
	
		/*@Ident = CaseID*/
		IF @DetailFieldSubTypeLevel = 11
		BEGIN

			SELECT @Money = SUM(mdv.ValueMoney)
			FROM MatterDetailValues mdv WITH(NOLOCK)
			INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = mdv.MatterID and m.CaseID = @Ident
			WHERE mdv.DetailFieldID = @DetailFieldID

		END
	
		/*@Ident = LeadID*/
		IF @DetailFieldSubTypeLevel = 1
		BEGIN

			SELECT @Money = SUM(mdv.ValueMoney)
			FROM MatterDetailValues mdv WITH(NOLOCK)
			INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = mdv.MatterID and m.LeadID = @Ident
			WHERE mdv.DetailFieldID = @DetailFieldID

		END

		/*@Ident = CustomerID*/
		IF @DetailFieldSubTypeLevel = 10
		BEGIN

			SELECT @Money = SUM(mdv.ValueMoney)
			FROM MatterDetailValues mdv WITH(NOLOCK)
			INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = mdv.MatterID and m.CustomerID = @Ident
			WHERE mdv.DetailFieldID = @DetailFieldID

		END
	
	END

	RETURN ISNULL(@Money,0.00) 

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumFields] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_SumFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumFields] TO [sp_executeall]
GO
