SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE FUNCTION [dbo].[fnHashLeadID] (@LeadID int)
RETURNS varchar(2000) AS  
BEGIN 

	/*
		JWG 2007 
		This is a SELECTable function that generates a defaced security code
		for the LeadID you pass in.  This is one way only, ie there's no way
		to get back to a unique lead or customer id from this hash. It's just
		to make it really difficult for a hacker to create a valid hash themselves.
		The '0x0' at the start and the money datatype for the @change variables are 
		both there just to confuse people further.
		
		Example usage: 
		
		SELECT l.LeadID, l.CaseID, dbo.fnHashLeadID (l.LeadID)
		FROM dbo.Lead l 
		WHERE l.ClientID = 2 
		
	*/
	
	declare @change_cust money, 
	@change_lead money, 
	@change_total money, 
	@encryption varchar(2000)  

	set @change_cust = 7
	set @change_lead = 56
	set @change_total = 91

	/* 
		1. Take the customerid and subtract a small amount from it
		2. Do a similar thing to the leadid
		3. Multiply these values together
		4. Remove yet another small value
		5. Convert all this to varchar
		6. Add a prefix of '0x0' to make it all look a bit like a HEX value
		7. Return the result
	*/
	set @encryption = (select '0x0' + convert(varchar,((Lead.CustomerID - @change_cust) * (Lead.LeadID - @change_lead)) - @change_total) 
	from Lead 
	where Lead.LeadID = @LeadID)

	return @encryption 
END

GO
GRANT VIEW DEFINITION ON  [dbo].[fnHashLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnHashLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnHashLeadID] TO [sp_executeall]
GO
