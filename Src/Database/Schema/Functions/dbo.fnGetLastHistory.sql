SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-11-05
-- Description:	Get Last History Value
-- =============================================
CREATE FUNCTION [dbo].[fnGetLastHistory] 
(	
@MatterID int,
@LeadID int,
@DetailFieldID int
)
RETURNS int 
AS
Begin

	declare @DVHID int

	Select top 1 @DVHID = DetailValueHistoryID
	FROM DetailValueHistory
	Where DetailFieldID = @DetailFieldID
	and (MatterID = @MatterID or MatterID is NULL)
	and LeadID = @LeadID
	Order By DetailValueHistoryID desc
	
	RETURN @DVHID

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLastHistory] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetLastHistory] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLastHistory] TO [sp_executeall]
GO
