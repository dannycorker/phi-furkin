SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire / Gavin Reynolds
-- Create date: 2016-08-08
-- Description:	returns the Tax gross factor for the US state specified.  
--              The factor is the multiplier required to add IPT to a net premium
-- Modified:	2020-08-26	SB	Changed to return the TaxProportion rather than the GrossMultiplier
-- =============================================
CREATE FUNCTION [dbo].[fn_RulesEngine_GetStateTax]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS REAL	
AS
BEGIN
	
	DECLARE @Tax MONEY,
			@ZipCode VARCHAR(20),
			@StateCode VARCHAR(500),
			@YearStart DATE,
			@ClientID INT,
			@CountryID INT

	/*Get ClientID*/
	SELECT @ClientID = [dbo].[fnGetPrimaryClientID]()

	SELECT @CountryID = CountryID FROM Clients WITH (NOLOCK) WHERE ClientID = @ClientID
	
	-- Year start date is always passed in
	SELECT @YearStart = AnyValue2
	FROM @Overrides
	WHERE AnyID = 2
	AND AnyValue1 = 'YearStart'
	-- except not always from RulesEngine test interface so default it
	IF @YearStart IS NULL SELECT @YearStart=dbo.fn_GetDate_Local()

	/*Get the ZipCode from the Customer*/
	/*GPR 2020-08-26 removed below line*/
	--SELECT @ZipCode = c.PostCode FROM Customers c WITH (NOLOCK) WHERE c.CustomerID = @CustomerID
			
	IF NULLIF(@ZipCode,'') IS NULL /*GPR 2020-05-12*/
	BEGIN
		SELECT @ZipCode = AnyValue2 FROM @Overrides WHERE AnyID = 2 AND AnyValue1 = 'PostCode'
		
		IF NULLIF(@ZipCode,'') IS NULL
		BEGIN
			SELECT @ZipCode = c.PostCode FROM Customers c WITH (NOLOCK) WHERE c.CustomerID = @CustomerID
		END
	END

	IF @CountryID = 233
	BEGIN 

		/*Lookup the State from the ZipCode*/
		SELECT @StateCode = StateID 
		FROM StateByZip sz WITH (NOLOCK) 
		WHERE sz.Zip = @ZipCode

		/*Return the valid GrossMultiplier for the Surcharge Tax by State and Date*/
		SELECT TOP 1 @Tax=tax.TaxProportion 
		FROM USTaxByState tax WITH (NOLOCK) 
		WHERE tax.CountryID = @CountryID 
		AND (tax.[From] IS NULL OR @YearStart >= tax.[From])
		AND (tax.[To] IS NULL OR @YearStart <= tax.[To])
		AND tax.StateID IS NOT NULL
		AND tax.TaxName  <> 'Kentucky Municipal Tax'
		AND tax.StateID = @StateCode
	
	END

	/*ALM/GPR FURKIN-132*/
	IF @CountryID = 39
	BEGIN 

		/*Lookup the State from the ZipCode*/
		SELECT @StateCode = StateID
		FROM ProvinceByPostalCode pbp WITH (NOLOCK) 
		WHERE pbp.PostalCode = LEFT(@ZipCode,3)

		/*Return the valid GrossMultiplier for the Surcharge Tax by State and Date*/
		SELECT TOP 1 @Tax=tax.TaxProportion /*ALM/GPR 2021-01-29 updated to use TaxProportion in place of GrossMultiplier*/
		FROM USTaxByState tax WITH (NOLOCK) 
		WHERE tax.CountryID = @CountryID 
		AND (tax.[From] IS NULL OR @YearStart >= tax.[From])
		AND (tax.[To] IS NULL OR @YearStart <= tax.[To])
		AND tax.StateID = @StateCode

		--IF @Tax IS NULL
		--BEGIN
		--	SELECT @Tax = 1.0000 /*If no tax is found return 1 as default*/
		--END

	END

	RETURN ISNULL(@Tax,0.0000) /*If no tax is found return 0 as default*/
 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_GetStateTax] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_RulesEngine_GetStateTax] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_GetStateTax] TO [sp_executeall]
GO
