SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-02-03
-- Description:	Returns table of SIG account numbers for payment reports
-- Mods
-- =============================================


CREATE FUNCTION [dbo].[fn_C384_GetSIGAccountNumbers]
(
)
RETURNS 
@AccNumbers TABLE
(
	Seq INT, AccNumCheques INT, SUNPremiums INT, SUNClaims INT
)	
AS
BEGIN
		
	DECLARE @AccNumCheques tvpIntInt, @SUNPremiums tvpIntInt, @SUNClaims tvpIntInt,
			@CountANC INT, @CountSP INT, @CountSC INT

	INSERT INTO @AccNumCheques
	SELECT ROW_NUMBER() OVER(ORDER BY i.ValueInt ASC) as [RNO], ValueInt 
	FROM ( SELECT DISTINCT ValueInt FROM ResourceListDetailValues WITH (NOLOCK) WHERE DetailFieldID=170244 ) as i

	INSERT INTO @SUNPremiums
	SELECT ROW_NUMBER() OVER(ORDER BY i.ValueInt ASC) as [RNO], ValueInt 
	FROM ( SELECT DISTINCT ValueInt FROM ResourceListDetailValues WITH (NOLOCK) WHERE DetailFieldID=170238 ) as i

	INSERT INTO @SUNClaims
	SELECT ROW_NUMBER() OVER(ORDER BY i.ValueInt ASC) as [RNO], ValueInt 
	FROM ( SELECT DISTINCT ValueInt FROM ResourceListDetailValues WITH (NOLOCK) WHERE DetailFieldID=170241 ) as i

	SELECT @CountANC=COUNT(*) FROM @AccNumCheques
	SELECT @CountSP=COUNT(*) FROM @SUNPremiums
	SELECT @CountSC=COUNT(*) FROM @SUNClaims

	IF @CountANC >= @CountSP AND @CountANC >= @CountSC
	BEGIN

		INSERT INTO @AccNumbers
		SELECT ID1, ID2, 0, 0 
		FROM @AccNumCheques
		
		UPDATE @AccNumbers
		SET SUNPremiums=ID2
		FROM @SUNPremiums 
		WHERE Seq=ID1
		
		UPDATE @AccNumbers
		SET SUNClaims=ID2
		FROM @SUNClaims 
		WHERE Seq=ID1

	END
	ELSE IF @CountSP >= @CountANC AND @CountSP >= @CountSC
	BEGIN

		INSERT INTO @AccNumbers
		SELECT ID1, 0, ID2, 0 
		FROM @SUNPremiums
		
		UPDATE @AccNumbers
		SET AccNumCheques=ID2
		FROM @AccNumCheques 
		WHERE Seq=ID1
		
		UPDATE @AccNumbers
		SET SUNClaims=ID2
		FROM @SUNClaims 
		WHERE Seq=ID1

	END
	ELSE
	BEGIN

		INSERT INTO @AccNumbers
		SELECT ID1, 0, 0, ID2 
		FROM @SUNClaims
		
		UPDATE @AccNumbers
		SET AccNumCheques=ID2
		FROM @AccNumCheques 
		WHERE Seq=ID1
			
		UPDATE @AccNumbers
		SET SUNPremiums=ID2
		FROM @SUNPremiums 
		WHERE Seq=ID1

	END
	
	RETURN

END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C384_GetSIGAccountNumbers] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C384_GetSIGAccountNumbers] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C384_GetSIGAccountNumbers] TO [sp_executeall]
GO
