SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-02-08
-- Description:	Call [CalculateColumnSum] SP
-- =============================================
CREATE FUNCTION [dbo].[fnCalculateColumnSum]
(
	@identity int, 
	@detailFieldSubTypeID int, 
	@detailFieldID int, 	
	@columnDetailFieldID int,
	@clientID int
)
RETURNS numeric(18,10)
AS
BEGIN

	Declare @Value numeric(18,10)
	
	exec @Value = dbo.[CalculateColumnSum] @identity, @detailFieldSubTypeID, @detailFieldID, @columnDetailFieldID, @clientID
	
	return @Value

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnCalculateColumnSum] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnCalculateColumnSum] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnCalculateColumnSum] TO [sp_executeall]
GO
