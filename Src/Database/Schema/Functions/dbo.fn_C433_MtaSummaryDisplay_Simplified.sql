SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-09-25
-- Description:	Created to replace Tom's custom table SQL report
-- 2020-02-05 CPS for JIRA AAG-91	| Replace TableRows and TableDetailValues references with PurchasedProductPaymentScheduleDetail
-- 2020-03-18 GPR for JIRA AAG-512	| Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- 2020-04-15 GPR for JIRA AAG-639	| Added declaration and assignment of @TopCount variable for returning the Top{X} for NumberedRows, when dealing with MultiPolicy
-- 2020-08-26 GPR for JIRA PPET-339 | Added TotalPriceChangeNet, TaxLevel1, and TaxLevel2
-- =============================================
CREATE FUNCTION [dbo].[fn_C433_MtaSummaryDisplay_Simplified]
(
	 @MatterID	INT
	,@MultiPet	BIT
)
RETURNS 
@Output TABLE
(
	 Label		VARCHAR(2000)
	,[Before]	VARCHAR(2000)
	,[After]	VARCHAR(2000)
)	
AS
BEGIN

	DECLARE @CustomerID INT
	SELECT @CustomerID = m.CustomerID
	FROM Matter m WITH ( NOLOCK ) 
	WHERE m.MatterID = @MatterID

		DECLARE @PetsAndValues TABLE (	 MatterID			INT
									,PetName			VARCHAR(2000)
									,PolicyNo			VARCHAR(100)
									,IsAnnual			BIT
									,BeforeAfter		BIT
									,TableRowID			INT
									,Annual				MONEY
									,Discounted			MONEY
									,AdminFee			MONEY
									,RegularCollection	MONEY
									,TotalPriceChange	MONEY
									,RegularPriceChange	MONEY
									,Net				MONEY
									,TaxLevel1			MONEY
									,TaxLevel2			MONEY
									,TotalPriceChangeNet MONEY
								)

	DECLARE @DisplayValues	TABLE ( SortOrder INT, Label VARCHAR(2000), ShowIfAnnual BIT )
	INSERT @DisplayValues ( SortOrder, ShowIfAnnual, Label )
	VALUES	 ( 1, 1, 'Pet' )
			,( 2, 1, 'Full Year Price' )
			,( 3, 1, 'Full Year Net' )
			,( 4, 1, 'Full Year National Tax' )
			,( 5, 1, 'Full Year Provincial Tax' )
			,( 6, 1, 'Full Year Price (Discounted)' )
			--,( 7, 1, 'Admin Fee' )
			--,( 5, 1, 'Customer Pays' )
			,( 6, 0, 'Regular Collection Amount' )
			--,( 7, 1, 'Next Collection Amount' )
			,( 8, 1, 'Total Price Change (Gross)' )
			,( 9, 1, 'Total Price Change (Net)' )
			,( 10, 1, 'Total Price Change (National Tax)' )
			,( 11, 1, 'Total Price Change (Provincial Tax)' )
			,( 12, 0, 'Regular Collection Change' )
			,(100, 1, '' ) --Divider

	INSERT @PetsAndValues ( MatterID, PetName, PolicyNo, BeforeAfter, IsAnnual )
	SELECT m.MatterID, ldv.DetailValue, l.LeadRef, t.N - 1, CASE WHEN pp.NumberOfInstallments = 1 THEN 1 ELSE 0 END
	FROM Lead l WITH ( NOLOCK ) 
	INNER JOIN LeadDetailValues ldv WITH ( NOLOCK ) on ldv.LeadID = l.LeadID AND ldv.DetailFieldID = 144268 /*Pet Name*/
	INNER JOIN Matter m WITH (NOLOCK) on m.LeadID = l.LeadID
	INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID AND mdv.DetailFieldID = 170038 /*Policy Status*/
	INNER JOIN Tally t WITH ( NOLOCK ) on t.N <= 2
	LEFT JOIN MatterDetailValues mdvMTA WITH ( NOLOCK ) on mdvMTA.MatterID = m.MatterID AND mdvMTA.DetailFieldID = 177906 /*Customer MTA In Progress*/
	INNER JOIN PurchasedProduct pp WITH ( NOLOCK ) on pp.PurchasedProductID = dbo.fn_C600_GetPurchasedProductForMatter(@MatterID,dbo.fn_GetDate_Local())
	WHERE l.CustomerID = @CustomerID 
	AND l.LeadTypeID = 1492 /*Policy Admin*/
	AND mdv.ValueInt = 43002 /*Live*/
	AND ( ( @MultiPet = 1 AND mdvMTA.ValueDate is not NULL ) 
		or ( @MultiPet = 0 AND m.MatterID = @MatterID ) 
		)

	
	/*GPR 2020-04-15 for AAG-639*/
	DECLARE @TopCount INT
	SELECT @TopCount = (SELECT COUNT(MatterID) FROM @PetsAndValues)

	;WITH NumberedRows AS
	(
		SELECT TOP (@TopCount) /*GPR 2020-04-15*/ --(2) 
				 wp.MatterID
				,wp.WrittenPremiumID
				,wp.AdjustmentTypeID
				,CASE WHEN wp.AdjustmentTypeID IN ( 1,2,3,6 ) THEN 0 ELSE 1 END [BeforeOrAfter] /*GPR 2020-04-11 added 9 to in list and flipped bit assignment around, previously THEN 0 ELSE 1*/
				,ROW_NUMBER() OVER (PARTITION BY wp.MatterID, wp.AdjustmentTypeID ORDER BY wp.WrittenPremiumID DESC) [RowNum]
		FROM WrittenPremium wp WITH ( NOLOCK ) 
		INNER JOIN @PetsAndValues pav on pav.MatterID = wp.MatterID
		WHERE pav.BeforeAfter = 0
		AND	wp.AdjustmentTypeID IN ( 1 /*New*/,2 /*Renewal*/,3 /*MTA*/, 6 /*Reinstatement*/, 9 /*MTA Quote*/ )
		ORDER BY wp.WrittenPremiumID DESC
	)
	UPDATE pav
	SET  TableRowID			= nr.WrittenPremiumID
		,Annual				= wp.AnnualPriceForRiskGross
		,Discounted			= wp.AnnualPriceForRiskGross
		,RegularCollection	= (SELECT fn.RecurringMonthly from dbo.fn_C00_1273_SplitPremium(wp.AnnualPriceForRiskGross) fn )
		,AdminFee			= ISNULL(dbo.fnGetSimpleDvAsMoney(177484,pav.MatterID),0.00) /*Initial Admin Fee Charged*/
		,Net				= wp.AnnualPriceForRiskNET
		,TaxLevel1			= wp.AnnualPriceForRiskNationalTax
		,TaxLevel2			= wp.AnnualPriceForRiskLocalTax
	FROM NumberedRows nr
	INNER JOIN @PetsAndValues pav on pav.MatterID = nr.MatterID AND pav.BeforeAfter	= nr.BeforeOrAfter
	INNER JOIN WrittenPremium wp WITH (NOLOCK) on wp.WrittenPremiumID = nr.WrittenPremiumID
	WHERE nr.RowNum = 1
	
	UPDATE pav
	SET  TotalPriceChange = pav.Discounted - before.Discounted
		,RegularPriceChange = pav.RegularCollection - before.RegularCollection
		,TotalPriceChangeNet = pav.Net - before.Net
	FROM @PetsAndValues pav
	INNER JOIN @PetsAndValues before on before.MatterID = pav.MatterID AND before.BeforeAfter = 0
	WHERE pav.BeforeAfter = 1

	DECLARE @Display TABLE ( ID INT IDENTITY, Label VARCHAR(2000), Before VARCHAR(2000), [After] VARCHAR(2000), AfterNumeric MONEY )

	INSERT @Display ( Label, Before, AfterNumeric )
	SELECT dv.Label
				,ISNULL( 
				 CASE dv.Label 
					WHEN 'Pet' THEN before.PolicyNo + ' (' + before.PetName + ')'
					WHEN 'Full Year Price' THEN CONVERT(VARCHAR,before.Annual)
					WHEN 'Full Year Price (Discounted)' THEN CONVERT(VARCHAR,before.Discounted)
					WHEN 'Admin Fee' THEN CONVERT(VARCHAR,before.AdminFee)
					WHEN 'Full Year Net' THEN CONVERT(VARCHAR,before.Net)
					WHEN 'Full Year State Tax' THEN CONVERT(VARCHAR,before.TaxLevel1)
					WHEN 'Full Year Municipal Tax' THEN CONVERT(VARCHAR,before.TaxLevel2)
					WHEN 'Customer Pays' THEN dbo.fnGetSimpleDv(175337,before.MatterID) /*Current/New Premium - Annual*/
					WHEN 'Regular Collection Amount' THEN CONVERT(VARCHAR,before.RegularCollection)
					WHEN 'Total Price Change (Gross)' THEN CONVERT(VARCHAR,before.TotalPriceChange)
					WHEN 'Total Price Change (Net)' THEN CONVERT(VARCHAR,before.TotalPriceChangeNet)
					WHEN 'Total Price Change (State Tax)' THEN CONVERT(VARCHAR,before.TaxLevel1)
					WHEN 'Total Price Change (Municipal Tax)' THEN CONVERT(VARCHAR,before.TaxLevel2)
				 END,'') [Before]
				 
				,CASE dv.Label 
					WHEN 'Full Year Price' THEN pav.Annual
					WHEN 'Full Year Price (Discounted)' THEN pav.Discounted
					WHEN 'Admin Fee' THEN pav.AdminFee
					WHEN 'Full Year Net' THEN pav.Net
					WHEN 'Full Year State Tax' THEN pav.TaxLevel1
					WHEN 'Full Year Municipal Tax' THEN pav.TaxLevel2
					WHEN 'Customer Pays' THEN pav.Discounted + pav.AdminFee
					WHEN 'Regular Collection Amount' THEN pav.RegularCollection
					WHEN 'Total Price Change (Gross)' THEN pav.TotalPriceChange
					WHEN 'Total Price Change (Net)' THEN pav.TotalPriceChangeNet
					WHEN 'Total Price Change (State Tax)' THEN pav.TaxLevel1
					WHEN 'Total Price Change (Municipal Tax)' THEN pav.TaxLevel2
					WHEN 'Regular Collection Change' THEN pav.RegularPriceChange
					WHEN 'Next Collection Amount' THEN pav.RegularCollection + dbo.fnGetSimpleDvAsMoney(175379,pav.MatterID) /*Adjustment Value  (for adjustment cover period)*/
				 END	[AfterNumeric]
				 
	FROM @PetsAndValues before
	CROSS APPLY @DisplayValues dv
	INNER JOIN @PetsAndValues pav on pav.MatterID = before.MatterID AND pav.BeforeAfter = 1
	WHERE before.BeforeAfter = 0
	AND (pav.IsAnnual = 0 or dv.ShowIfAnnual = 1)
	ORDER BY pav.MatterID, dv.SortOrder

	UPDATE d 
	SET [After] = CASE WHEN d.Label like '%Change%' AND d.AfterNumeric > 0 THEN '+' ELSE '' END + CONVERT(VARCHAR,d.AfterNumeric)
	FROM @Display d 
	WHERE d.AfterNumeric is not NULL
	AND d.[After] is NULL


	IF ( SELECT COUNT(*) FROM @PetsAndValues pav ) > 2 /*If MultiPet*/
	BEGIN
		INSERT @Display (Label, Before, After )
		SELECT 'Total Annual Change','' , CASE WHEN SUM(d.AfterNumeric) > 0 THEN '+' ELSE '' END + CONVERT(VARCHAR,SUM( d.AfterNumeric ))
		FROM @Display d 
		WHERE d.Label = 'Total Price Change (Gross)'
			UNION
		SELECT 'Total Regular Change','', CASE WHEN SUM(d.AfterNumeric) > 0 THEN '+' ELSE '' END + CONVERT(VARCHAR,SUM( d.AfterNumeric ))
		FROM @Display d 
		WHERE d.Label = 'Regular Collection Change'
			UNION
		SELECT 'New Regular Collection','', CONVERT(VARCHAR,SUM( d.AfterNumeric ))
		FROM @Display d 
		WHERE d.Label = 'Regular Collection Amount'
		ORDER BY 1 DESC

	END

	INSERT @Output ( Label, Before, After )
	SELECT d.Label, ISNULL(d.Before,'') [Before], ISNULL(d.[After],'') [After]
	FROM @Display d  
	ORDER BY d.ID	
	
	RETURN

END


















GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C433_MtaSummaryDisplay_Simplified] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C433_MtaSummaryDisplay_Simplified] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C433_MtaSummaryDisplay_Simplified] TO [sp_executeall]
GO
