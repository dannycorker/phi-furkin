SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2014-05-30
-- Description:	Split a CamelCase word into "Camel Case" with spaces.
-- =============================================
CREATE FUNCTION [dbo].[fnSplitCamelCaseWord] 
(
	@Input VARCHAR(2000)
)
RETURNS VARCHAR(2000)
AS
BEGIN
	DECLARE @Output VARCHAR(2000) = ''

	DECLARE @tt TABLE (
		SortCol INT IDENTITY(1, 1), 
		CharValue CHAR
	)

	;WITH Madness AS 
	(
		/* Create a table row for each character in the input string */
		SELECT t.N AS [POSITION], 
		cm.CharValue, 
		cm.CharName, 
		cm.AsciiValue, 
		2 AS SortOrder
		FROM dbo.Tally t WITH (NOLOCK) 
		INNER JOIN dbo.CharMap cm WITH (NOLOCK) ON cm.AsciiValue = ASCII(SUBSTRING(@Input, t.N, 1)) 
		WHERE t.N <= LEN(@Input)
		
		UNION 
		
		/* Create additional spaces for each UpperCaseCharacter found in the input string (except the first char) */
		SELECT t.N AS [POSITION], 
		'' AS CharValue, 
		'space' AS CharName, 
		32 AS AsciiValue,						/* 32 = space */
		1 AS SortOrder
		FROM dbo.Tally t WITH (NOLOCK) 
		INNER JOIN dbo.CharMap cm WITH (NOLOCK) ON cm.AsciiValue = ASCII(SUBSTRING(@Input, t.N, 1)) 
		WHERE t.N <= LEN(@Input)
		AND cm.AsciiValue BETWEEN 65 AND 90		/* 65=A, 90=Z */
		AND t.N BETWEEN 2 AND (LEN(@Input) - 1)								/* Ignore the first character of all */
		
	)
	INSERT INTO @tt (CharValue)
	SELECT m.CharValue FROM Madness m
	ORDER BY m.POSITION, m.SortOrder

	/* Concatenate the string including the spaces in the right places. ORDER BY stops this from working, hence the @tt trick here. */
	SELECT @Output += t.CharValue 
	FROM @tt t

	RETURN @Output


END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnSplitCamelCaseWord] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnSplitCamelCaseWord] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnSplitCamelCaseWord] TO [sp_executeall]
GO
