SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire / Gavin Reynolds
-- Create date: 2016-08-08
-- Description:	returns the Tax gross factor for the US state specified.  
--              The factor is the multiplier required to add IPT to a net premium
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_RulesEngine_GetUSTax]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS REAL	
AS
BEGIN
	
	DECLARE @Tax MONEY,
			@ZipCode VARCHAR(20),
			@StateCode VARCHAR(500),
			@YearStart DATE,
			@ClientID INT,
			@CountryID INT

	/*Get ClientID*/
	SELECT @ClientID = [dbo].[fnGetPrimaryClientID]()

	SELECT @CountryID = CountryID FROM Clients WITH (NOLOCK) WHERE ClientID = @ClientID
	
	-- Year start date is always passed in
	SELECT @YearStart = AnyValue2
	FROM @Overrides
	WHERE AnyID = 2
	AND AnyValue1 = 'YearStart'
	-- except not always from RulesEngine test interface so default it
	IF @YearStart IS NULL SELECT @YearStart=dbo.fn_GetDate_Local()

	/*Get the ZipCode from the Customer*/
	SELECT @ZipCode = c.PostCode FROM Customers c WITH (NOLOCK) WHERE c.CustomerID = @CustomerID
			
	IF NULLIF(@ZipCode,'') IS NULL /*GPR 2020-05-12*/
	BEGIN
		SELECT @ZipCode = AnyValue2 FROM @Overrides WHERE AnyID = 2 AND AnyValue1 = 'PostCode'
	END

	/*Lookup the State from the ZipCode*/
	SELECT @StateCode = StateID 
	FROM StateByZip sz WITH (NOLOCK) 
	WHERE sz.Zip = @ZipCode
	
	/*Return the valid GrossMultiplier for the Surcharge Tax by State and Date*/
	SELECT TOP 1 @Tax=tax.GrossMultiplier 
	FROM USTaxByState tax WITH (NOLOCK) 
	WHERE tax.CountryID= @CountryID 
	AND (tax.[From] IS NULL OR @YearStart >= tax.[From])
	AND (tax.[To] IS NULL OR @YearStart <= tax.[To])
	AND tax.StateID IS NOT NULL
	AND tax.TaxName = 'State Surcharge Tax'

	RETURN ISNULL(@Tax,1.0000) /*If no tax is found return a multiply by 1 so not to cause the rules engine to crash*/
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_GetUSTax] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_RulesEngine_GetUSTax] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_GetUSTax] TO [sp_executeall]
GO
