SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-12-21
-- Description: Return TablerowID
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_GetTablerowID]
(
	@LeadOrMatter INT 
	,@ObjectID INT
	,@TableDetailFieldID  INT
	,@ColumnDetailFieldID INT
)
RETURNS varchar(2000)
AS
BEGIN
 
 DECLARE @ReturnValue INT

	SELECT TOP 1 @ReturnValue = tdv.TableRowID
	FROM TableDetailValues tdv WITH (NOLOCK) 
	WHERE tdv.DetailFieldID = @ColumnDetailFieldID
	AND tdv.MatterID = @ObjectID
	AND tdv.ValueInt <> tdv.TableRowID /*Find rows where the equation result does not equal the TablerowID */
	ORDER BY tdv.TableRowID asc	

	SELECT @ReturnValue = ISNULL(@ReturnValue,0)

	RETURN @ReturnValue
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetTablerowID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_GetTablerowID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetTablerowID] TO [sp_executeall]
GO
