SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2018-06-12
-- Description:	Find extra detail about tasks and notifications for this lead/case
-- 2018-07-02 CPS added MatterID to the workflow section to llow distinction between claims
-- 2018-08-03 GPR ported to C600 from C433
-- 2018-11-08 CPS changed to _C00_
-- 2020-01-22 CPS for LPC			| Added section to show Aquarium employees which batch jobs are about to run
-- 2020-02-12 CPS for JIRA AAG-106	| Added section to show Aquarium employees AutomatedEventQueue errors
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_HUD_GetLeadSpecificNotifications]
(
	 @LeadID		INT
	,@CaseID		INT
	,@CurrentUserID	INT
)
RETURNS 
@OutputTable TABLE
(
	 MessageContent			VARCHAR(2000)
	,BackgroundColourHex	VARCHAR(2000)
	,ForegroundColourHex	VARCHAR(2000)
	,SortOrder				INT
	,SourceID				iNT
)	
AS
BEGIN

	/*Workflow Tasks (assigned to you)*/
	INSERT @OutputTable ( MessageContent, BackgroundColourHex, ForegroundColourHex, SortOrder, SourceID )
	SELECT	CASE et.LeadTypeID 
				WHEN 1492 THEN 'Policy '
				WHEN 1490 THEN 'Claim '
				ELSE 'MatterID '
			END
				+ CONVERT(VARCHAR,m.MatterID) + ' is assigned to you in the "' + wg.Name + '" workflow group. '
			+'To complete the task, apply ' + CASE WHEN wt.FollowUp = 0 THEN CASE WHEN et.InProcess = 0 THEN 'out of process' ELSE '' END + ' event ' ELSE ' the next action to ' END + et.EventTypeName 
	
			, dbo.fn_C00_GetHexCodeForColour('Yellow'), dbo.fn_C00_GetHexCodeForColour('Black'), 3
			, wt.WorkflowGroupID
	FROM WorkflowTask wt WITH ( NOLOCK )
	INNER JOIN WorkflowGroup wg WITH ( NOLOCK ) on wg.WorkflowGroupID = wt.WorkflowGroupID
	INNER JOIN EventType et WITH ( NOLOCK ) on et.EventTypeID = wt.EventTypeID
	INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID = wt.CaseID
	WHERE wt.LeadID = @LeadID
	AND wt.AssignedTo = @CurrentUserID

	/*Workflow Tasks (assigned to others, or unassigned)*/
	INSERT @OutputTable ( MessageContent, BackgroundColourHex, ForegroundColourHex, SortOrder )
	SELECT CASE et.LeadTypeID 
				WHEN 1492 THEN 'Policy '
				WHEN 1490 THEN 'Claim '
				ELSE 'MatterID '
			END
				+ CONVERT(VARCHAR,m.MatterID) + ' appears in the "' + wg.Name + '" workflow group.' + ISNULL(' Assigned to ' + cp.UserName,' It is currently unassigned') + '.'
			+' To complete the task, apply ' + CASE WHEN wt.FollowUp = 0 THEN CASE WHEN et.InProcess = 0 THEN 'out of process' ELSE '' END + ' event ' ELSE ' the next action to ' END + et.EventTypeName
			, dbo.fn_C00_GetHexCodeForColour('Blue')
			, dbo.fn_C00_GetHexCodeForColour('Black')
			, 3
	FROM WorkflowTask wt WITH ( NOLOCK ) 
	INNER JOIN WorkflowGroup wg WITH ( NOLOCK ) on wg.WorkflowGroupID = wt.WorkflowGroupID
	LEFT JOIN ClientPersonnel cp WITH ( NOLOCK ) on cp.ClientPersonnelID = wt.AssignedTo
	INNER JOIN Matter m WITH (NOLOCK) on m.CaseID = wt.CaseID
	INNER JOIN EventType et WITH ( NOLOCK ) ON et.EventTypeID = wt.EventTypeID
	WHERE wt.LeadID = @LeadID
	AND (wt.AssignedTo <> @CurrentUserID or wt.AssignedTo is NULL)
	AND NOT EXISTS ( SELECT *
	                 FROM @OutputTable ot 
					 WHERE ot.SourceID = wt.WorkflowGroupID )

	-- 2020-01-22 CPS for LPC | Section for Aquarium employees only
	IF 1 = ( SELECT cp.IsAquarium FROM ClientPersonnel cp WITH (NOLOCK) WHERE cp.ClientPersonnelID = @CurrentUserID )
	BEGIN
		/*What batch jobs are about to run?*/
		INSERT @OutputTable ( MessageContent, BackgroundColourHex, ForegroundColourHex, SortOrder )
		SELECT 'AQ Only - Batch Jobs Waiting to Run.  Current System Time [' + ISNULL(CONVERT(VARCHAR,dbo.fn_GetDate_Local(),121),'NULL') + '] [dbo.fn_GetDate_Local()]' -- 2021-01-27 CPS for Trupanion Testing | Include the current system time for context
				, dbo.fn_C00_GetHexCodeForColour('Blue')
				, dbo.fn_C00_GetHexCodeForColour('Black')
				, 4
		WHERE EXISTS ( SELECT *
		               FROM AutomatedTask at WITH (NOLOCK) 
					   WHERE at.Enabled = 1
					   AND at.NextRunDateTime < dbo.fn_GetDate_Local() )

		INSERT @OutputTable ( MessageContent, BackgroundColourHex, ForegroundColourHex, SortOrder )
		SELECT	ISNULL('Queue ' + CONVERT(VARCHAR,ati.QueueID),'') + ' | '
					+ ISNULL(CONVERT(VARCHAR,at.NextRunDateTime,121),' (Running Now) ') + ' | '
					+ CONVERT(VARCHAR,at.TaskID) + ' - ' + at.Taskname
				, dbo.fn_C00_GetHexCodeForColour('Blue')
				, dbo.fn_C00_GetHexCodeForColour('Black')
				, 4
		FROM AutomatedTask at WITH (NOLOCK) 
		LEFT JOIN AutomatedTaskInfo ati WITH (NOLOCK) on ati.TaskID = at.TaskID
		WHERE at.Enabled = 1
		AND (at.NextRunDateTime < dbo.fn_GetDate_Local() or at.AlreadyRunning = 1)
		ORDER BY ati.QueueID, at.NextRunDateTime


		-- 2020-02-12 CPS for JIRA AAG-106 | Include AutomatedEventQueue errors for this lead/case
		INSERT @OutputTable ( MessageContent, BackgroundColourHex, ForegroundColourHex, SortOrder )
		SELECT 'AQ Only - Failed AutomatedEventQueues'
				, dbo.fn_C00_GetHexCodeForColour('Blue')
				, dbo.fn_C00_GetHexCodeForColour('Black')
				, 5
		WHERE EXISTS (	SELECT *
						FROM AutomatedEventQueue aeq WITH (NOLOCK) 
						INNER JOIN EventType et WITH ( NOLOCK ) on et.EventTypeID = aeq.AutomatedEventTypeID
						WHERE aeq.LeadID = @LeadID
						AND (aeq.CaseID = @CaseID or NULLIF(@CaseID,0) is NULL)
						AND aeq.ErrorDateTime is not NULL )
			UNION ALL
		SELECT CONVERT(VARCHAR,aeq.ErrorDateTime,121) + ' | QueueID ' + CONVERT(VARCHAR,aeq.AutomatedEventQueueID) + ' | ' + et.EventTypeName + ' (' + CONVERT(VARCHAR,et.EventTypeID) + ') | ' + aeq.ErrorMessage
				, dbo.fn_C00_GetHexCodeForColour('Blue')
				, dbo.fn_C00_GetHexCodeForColour('Black')
				, 5
		FROM AutomatedEventQueue aeq WITH (NOLOCK) 
		INNER JOIN EventType et WITH ( NOLOCK ) on et.EventTypeID = aeq.AutomatedEventTypeID
		WHERE aeq.LeadID = @LeadID
		AND (aeq.CaseID = @CaseID or NULLIF(@CaseID,0) is NULL)
		AND aeq.ErrorDateTime is not NULL

	END

	RETURN

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_HUD_GetLeadSpecificNotifications] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_HUD_GetLeadSpecificNotifications] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_HUD_GetLeadSpecificNotifications] TO [sp_executeall]
GO
