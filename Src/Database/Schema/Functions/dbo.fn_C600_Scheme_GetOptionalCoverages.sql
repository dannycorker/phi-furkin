SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-10-21
-- Description:	Returns a table of optional coverages... first at the matter level but defaulting to the lead level
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Scheme_GetOptionalCoverages]
(
	@MatterID INT
)
RETURNS 
	@Coverage TABLE 
	(
		GroupID INT, 
		GroupText VARCHAR(2000), 
		MatterID INT, 
		LeadID INT, 
		ResourceListID INT, 
		SumInsured MONEY, 
		SectionID INT, 
		Section VARCHAR(2000), 
		SubSectionID INT, 
		SubSection VARCHAR(2000),
		CoverageText VARCHAR(2000),
		AllowedCount INT,
		LimitTypeID INT,
		LimitType VARCHAR(2000),
		PetTypeID INT,
		IsOverride BIT
	)
AS
BEGIN
 
	DECLARE @LeadID INT
	SELECT @LeadID = LeadID 
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @MatterID
	
	;WITH CoverageOptions AS 
	(
		SELECT	tdvGroup.ValueInt AS GroupID, llGroup.ItemValue AS GroupText, r.MatterID, r.LeadID, tdvSection.ResourceListID, tdvAmount.ValueMoney AS SumInsured, 
				llSection.LookupListItemID AS SectionID, llSection.ItemValue AS Section, llSub.LookupListItemID AS SubSectionID, llSub.ItemValue AS SubSection,
				tdvText.DetailValue AS CoverageText, tdvCount.ValueInt AS AllowedCount, tdvLimitType.ValueInt AS LimitTypeID, llType.ItemValue AS LimitType,
				tdvPetType.ValueInt AS PetTypeID
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvSection WITH (NOLOCK) ON r.TableRowID = tdvSection.TableRowID AND tdvSection.DetailFieldID = 175733
		INNER JOIN dbo.ResourceListDetailValues rdvSection WITH (NOLOCK) ON tdvSection.ResourceListID = rdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189
		INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rdvSection.ValueInt = llSection.LookupListItemID
		INNER JOIN dbo.ResourceListDetailValues rdvSub WITH (NOLOCK) ON tdvSection.ResourceListID = rdvSub.ResourceListID AND rdvSub.DetailFieldID = 146190
		INNER JOIN dbo.LookupListItems llSub WITH (NOLOCK) ON rdvSub.ValueInt = llSub.LookupListItemID
		INNER JOIN dbo.TableDetailValues tdvGroup WITH (NOLOCK) ON r.TableRowID = tdvGroup.TableRowID AND tdvGroup.DetailFieldID = 175735
		INNER JOIN dbo.LookupListItems llGroup WITH (NOLOCK) ON tdvGroup.ValueInt = llGroup.LookupListItemID
		INNER JOIN dbo.TableDetailValues tdvAmount WITH (NOLOCK) ON r.TableRowID = tdvAmount.TableRowID AND tdvAmount.DetailFieldID = 175734
		LEFT JOIN dbo.TableDetailValues tdvText WITH (NOLOCK) ON r.TableRowID = tdvText.TableRowID AND tdvText.DetailFieldID = 175749
		LEFT JOIN dbo.TableDetailValues tdvCount WITH (NOLOCK) ON r.TableRowID = tdvCount.TableRowID AND tdvCount.DetailFieldID = 175739
		LEFT JOIN dbo.TableDetailValues tdvLimitType WITH (NOLOCK) ON r.TableRowID = tdvLimitType.TableRowID AND tdvLimitType.DetailFieldID = 175740
		LEFT JOIN dbo.LookupListItems llType WITH (NOLOCK) ON tdvLimitType.ValueInt = llType.LookupListItemID
		LEFT JOIN dbo.TableDetailValues tdvPetType WITH (NOLOCK) ON r.TableRowID = tdvPetType.TableRowID AND tdvPetType.DetailFieldID = 175859
		WHERE (r.MatterID = @MatterID OR r.LeadID = @LeadID)
		AND r.DetailFieldID IN (175736, 175753)
	)			
	-- Get the lead level options if none are defined at matter level otherwise get matter options
	INSERT @Coverage (GroupID, GroupText, MatterID, LeadID, ResourceListID, SumInsured, SectionID, Section, SubSectionID, SubSection, CoverageText,
						AllowedCount, LimitTypeID, LimitType, PetTypeID, IsOverride)
	SELECT	GroupID, GroupText, MatterID, LeadID, ResourceListID, SumInsured, SectionID, Section, SubSectionID, SubSection, CoverageText,
			c.AllowedCount, c.LimitTypeID, c.LimitType, c.PetTypeID, 0 AS IsOverride
	FROM CoverageOptions c
	WHERE c.LeadID = @LeadID
	AND NOT EXISTS (SELECT * FROM CoverageOptions m WHERE m.MatterID = @MatterID) -- Don't include the global options if matter level options are defined
	UNION
	SELECT	GroupID, GroupText, MatterID, LeadID, ResourceListID, SumInsured, SectionID, Section, SubSectionID, SubSection, CoverageText,
			AllowedCount, LimitTypeID, LimitType, PetTypeID, 1 AS IsOverride
	FROM CoverageOptions
	WHERE MatterID = @MatterID
	
	RETURN

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Scheme_GetOptionalCoverages] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_Scheme_GetOptionalCoverages] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Scheme_GetOptionalCoverages] TO [sp_executeall]
GO
