SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2014-11-21
-- Description:	Get the current date and time in a format that can be used for file names and table names
-- =============================================
CREATE FUNCTION [dbo].[fnDateTimeForName]
(
)
RETURNS VARCHAR(30)
AS
BEGIN
	DECLARE @DateTimeForName VARCHAR(30)
	
	/*
		Get the date and time in the format 
		2014-11-21 11:31:10
		
		Replace all dashes, spaces and colons to give this result, nice and safe for file names and sql server table names
		2014_11_21_11_31_10
	*/
	SELECT @DateTimeForName = REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR, dbo.fn_GetDate_Local(), 120), '-', '_'), ' ', '_'), ':', '_')
	
	RETURN @DateTimeForName

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnDateTimeForName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnDateTimeForName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnDateTimeForName] TO [sp_executeall]
GO
