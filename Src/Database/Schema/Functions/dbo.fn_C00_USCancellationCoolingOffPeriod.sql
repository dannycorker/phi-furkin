SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire
-- Create date: 2020-08-18
-- Description:	Returns the number of days to be used for the cooling off period for US Cancellation
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_USCancellationCoolingOffPeriod]
(
	@State VARCHAR(10)
	,@CancellationType INT /*1 = Customer, 2 = Company*/
	,@IsRenewal BIT
	,@DaysIntoPolicyYear INT
)
RETURNS INT
AS
BEGIN

	DECLARE @CoolingOffPeriod INT

	IF @IsRenewal = 0
		BEGIN
			IF @CancellationType = 1
			BEGIN
				SELECT @CoolingOffPeriod = uscancel.CustomerCancelNonPay 
				FROM USCancellationRule uscancel WITH (NOLOCK) 
				WHERE uscancel.State = @State
				AND @DaysIntoPolicyYear BETWEEN uscancel.GraceDayStart AND uscancel.GraceDayEnd
			END 
			ELSE 
			BEGIN 
				SELECT @CoolingOffPeriod = uscancel.CompanyCancelOther 
				FROM USCancellationRule uscancel WITH (NOLOCK) 
				WHERE uscancel.State = @State
				AND @DaysIntoPolicyYear BETWEEN uscancel.GraceDayStart AND uscancel.GraceDayEnd
			END
		END
	ELSE
		BEGIN 
			IF @CancellationType = 1
			BEGIN
				SELECT @CoolingOffPeriod = uscancel.CustomerCancelNonPay 
				FROM USCancellationRule uscancel WITH (NOLOCK) 
				WHERE uscancel.State = @State
				AND uscancel.IsRenewal = 1
			END
		ELSE
		BEGIN
			SELECT @CoolingOffPeriod = uscancel.CompanyCancelOther 
			FROM USCancellationRule uscancel WITH (NOLOCK) 
			WHERE uscancel.State = @State
			AND uscancel.IsRenewal = 1
		END
		END
	

RETURN @CoolingOffPeriod
END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_USCancellationCoolingOffPeriod] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_USCancellationCoolingOffPeriod] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_USCancellationCoolingOffPeriod] TO [sp_executeall]
GO
