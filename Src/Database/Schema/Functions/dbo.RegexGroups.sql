SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE FUNCTION [dbo].[RegexGroups] (@input [nvarchar] (max), @pattern [nvarchar] (4000))
RETURNS TABLE (
[Index] [int] NULL,
[Group] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL,
[Text] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL)
WITH EXECUTE AS CALLER
EXTERNAL NAME [SqlServerProject_Regex].[UserDefinedFunctions].[RegexGroups]
GO
GRANT VIEW DEFINITION ON  [dbo].[RegexGroups] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[RegexGroups] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RegexGroups] TO [sp_executeall]
GO
