SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2016-20-05
-- Description:	Concatenate text stored in a table column to form a string
-- =============================================
Create FUNCTION [dbo].[fn_C00_ConcatenateTableColumn]
(
	@DetailFieldID INT,
	@ColumnDetailFieldID INT,
	@ObjectID int
)
RETURNS VARCHAR(MAX)
AS
BEGIN
			
	DECLARE @LeadOrMatter int,
			@ReturnValue VARCHAR(MAX)

	SELECT @LeadOrMatter = df.LeadOrMatter
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @DetailFieldID

	SELECT @ReturnValue = LEFT(result,LEN(result) - 1)
	FROM
	 (SELECT
	  CONVERT(varchar(max),
	   (
		SELECT tdv.DetailValue as [text()], ', ' as [text()]
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
		INNER JOIN TableDetailValues tdvval WITH (NOLOCK) on tdvval.TableRowID = tr.TableRowID
		WHERE tr.DetailFieldID = @DetailFieldID
		AND	tdv.DetailFieldID = @ColumnDetailFieldID
		AND (
			(tr.LeadID = @ObjectID AND @LeadOrMatter = 1) 
			OR
			(tr.MatterID = @ObjectID AND @LeadOrMatter = 2) 
			OR
			(tr.CustomerID = @ObjectID AND @LeadOrMatter = 10) 
			OR
			(tr.CaseID = @ObjectID AND @LeadOrMatter = 11)
			)
		FOR XML PATH('')
	) 
	)AS result
	) s

	RETURN @ReturnValue

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ConcatenateTableColumn] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ConcatenateTableColumn] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ConcatenateTableColumn] TO [sp_executeall]
GO
