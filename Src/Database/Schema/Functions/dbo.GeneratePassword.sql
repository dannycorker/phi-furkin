SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE FUNCTION [dbo].[GeneratePassword] ()
RETURNS [nvarchar] (4000)
WITH EXECUTE AS CALLER
EXTERNAL NAME [SqlServerProject_Regex].[UserDefinedFunctions].[GeneratePassword]
GO
GRANT VIEW DEFINITION ON  [dbo].[GeneratePassword] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GeneratePassword] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GeneratePassword] TO [sp_executeall]
GO
