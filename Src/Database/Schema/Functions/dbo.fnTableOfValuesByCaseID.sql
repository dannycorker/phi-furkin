SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-07-13
-- Description:	Return a table of varchars and matterid's
-- =============================================
CREATE FUNCTION [dbo].[fnTableOfValuesByCaseID]
(
	@DetailFieldID		int,
	@CaseID				int
)
RETURNS 
@TableOut TABLE 
(
	LeadID int,
	MatterID int,
	DetailValue varchar(2000)
)
AS
BEGIN

	Insert Into @TableOut (LeadID, MatterID, DetailValue)
	Select m.LeadID, m.MatterID, mdv.DetailValue
	From MatterDetailValues mdv
	Inner Join Matter m on m.MatterID = mdv.MatterID and m.CaseID = @CaseID
	Where mdv.DetailFieldID = @DetailFieldID

	RETURN
END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfValuesByCaseID] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnTableOfValuesByCaseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfValuesByCaseID] TO [sp_executeall]
GO
