SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-20
-- Description:	Table of values from CSV using CTE
-- =============================================
CREATE FUNCTION [dbo].[fnTableOfValuesFromCSVCTE]
(	
	@list varchar(max)
)
RETURNS @tmp TABLE
(
    AnyID int identity (1, 1),
    Data varchar(max)
)
BEGIN

	;
	WITH CTETable (start, finish)
	AS
	(
		SELECT start = convert(bigint, 1), finish = charindex(',', @list + ',')
		
		UNION ALL 
		
		SELECT start = finish + 1, finish = charindex(',', @list + ',', finish + 1) 
		FROM   CTETable
		WHERE  finish > 0
	)
	INSERT INTO @tmp (Data)
	SELECT ltrim(rtrim(substring(@list, start, CASE WHEN finish > 0 THEN finish - start ELSE 0 END))) AS Data
	FROM CTETable
	WHERE finish > 0
	
	RETURN

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfValuesFromCSVCTE] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnTableOfValuesFromCSVCTE] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfValuesFromCSVCTE] TO [sp_executeall]
GO
