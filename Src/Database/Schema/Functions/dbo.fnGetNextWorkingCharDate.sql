SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2008-08-27
-- Description:	Find the next available working day [n] days after the date passed in.
-- =============================================
CREATE FUNCTION [dbo].[fnGetNextWorkingCharDate] 
(
	@chardate char(10), @daystoadd int
)
RETURNS char(10)
AS
BEGIN

	-- The date passed in must be in the format yyyy-mm-dd.  There is a sister function 
	-- called dbo.fnGetNextWorkingDate that works with datetime values instead of char dates.

	-- The WorkingDays table knows all about weekends and bank holidays,
	-- so if you need to set a field to a date 5 days after some event fires, 
	-- but it has to be a work day, this will start at (d+5) and then crawl forwards
	-- one day at a time until a work day is reached.

	-- Example: SELECT dbo.fnGetNextWorkingCharDate('2008-08-22', 1) 
	-- This starts at (d+1), but that would be a Saturday (23rd)
	-- so it then keeps going past the Sunday (24th) AND the Bank Holiday Monday (25th) and
	-- therefore returns '2008-08-26' which is the Tuesday.
	SELECT @chardate = min(w.[CharDate]) 
	FROM workingdays w 
	WHERE w.[Date] >= dateadd(dd, @daystoadd, @chardate) 
	AND w.[IsWorkDay] = 1 

	RETURN @chardate
END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetNextWorkingCharDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetNextWorkingCharDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetNextWorkingCharDate] TO [sp_executeall]
GO
