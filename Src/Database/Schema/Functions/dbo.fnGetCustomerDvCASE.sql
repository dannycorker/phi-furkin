SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-05-08
-- Description:	Get CustomerDV directly #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetCustomerDvCASE]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE @DetailValue AS VARCHAR(2000)

	SELECT @DetailValue = cdv.DetailValue 
	FROM Matter c WITH (NOLOCK) 
	INNER JOIN CustomerDetailValues cdv WITH (NOLOCK) ON c.CustomerID = cdv.CustomerID AND cdv.DetailFieldID = @DetailFieldID
	WHERE c.CaseID = @CaseID 
	
	RETURN @DetailValue

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerDvCASE] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetCustomerDvCASE] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerDvCASE] TO [sp_executeall]
GO
