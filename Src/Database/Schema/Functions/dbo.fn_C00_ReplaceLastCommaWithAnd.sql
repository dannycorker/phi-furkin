SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-10-19
-- Description:	Replaces final ', ' in a ', ' separated string with ' and ' 
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ReplaceLastCommaWithAnd]
(
		@DetailValue VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	
	SELECT @DetailValue = CASE WHEN CHARINDEX(' ,',REVERSE(@DetailValue)) > 0 
							THEN REVERSE(LEFT(REVERSE(@DetailValue),CHARINDEX(' ,',REVERSE(@DetailValue))-1) 
								+ ' dna ' 
								+ SUBSTRING(REVERSE(@DetailValue),CHARINDEX(' ,',REVERSE(@DetailValue))+2,LEN(REVERSE(@DetailValue))))
							ELSE @DetailValue END
	
	RETURN (@DetailValue)
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReplaceLastCommaWithAnd] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ReplaceLastCommaWithAnd] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReplaceLastCommaWithAnd] TO [sp_executeall]
GO
