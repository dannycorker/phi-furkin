SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2009-04-06
-- Description:	Get info about the database user.
--              This gets passed to history tables and stored with each update.
-- =============================================
CREATE FUNCTION [dbo].[fnGetUserInfo] ()
RETURNS varchar(2000)
AS
BEGIN
	DECLARE @UserInfo varchar(2000)

	/*
	Permissions FAIL!
	
	Created AquariumNet as a user in the Master database
	Granted it db_DataReader role
	Granted SELECT on sys.dm_exec_connections
	
	Still gets "permission denied" trying to run this:
	
	SELECT @UserInfo = 'user=' + USER_NAME() + ' host=' + HOST_NAME() + ' ipa=' + client_net_address + ' ct=' + convert(varchar, dm.connect_time, 126) + ' mode=' + net_transport + ' session=' + convert(varchar, dm.session_id) 
	FROM sys.dm_exec_connections dm 
	WHERE Session_id = @@SPID 
	*/

	SELECT @UserInfo = 'user=' + USER_NAME() + ', host=' + HOST_NAME() + ', spid=' + convert(varchar, @@SPID)
	
	-- Return the result of the function
	RETURN @UserInfo

END










GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetUserInfo] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetUserInfo] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetUserInfo] TO [sp_executeall]
GO
