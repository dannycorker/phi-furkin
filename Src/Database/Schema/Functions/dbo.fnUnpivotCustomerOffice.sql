SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Unpivot the columns in the CustomerOffice table 
-- =============================================
CREATE FUNCTION [dbo].[fnUnpivotCustomerOffice] 
(	
	@CustomerOfficeID int
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT ColumnName as [RawColumnName], '[!' + ColumnName + ']' as [FormattedColumnName], RealValue
	FROM
	(
		SELECT 
		COALESCE(CONVERT(varchar(255), c.Address1), '') as [Address1],
		COALESCE(CONVERT(varchar(255), c.Address2), '') as [Address2],
		COALESCE(CONVERT(varchar(255), c.ClientID), '') as [ClientID],
		COALESCE(CONVERT(varchar(255), c.Country), '') as [Country],
		COALESCE(CONVERT(varchar(255), c.CountryID), '') as [CountryID],
		COALESCE(CONVERT(varchar(255), c.County), '') as [County],
		COALESCE(CONVERT(varchar(255), c.CustomerID), '') as [CustomerID],
		COALESCE(CONVERT(varchar(255), c.FaxNumber), '') as [FaxNumber],
		COALESCE(CONVERT(varchar(255), c.MainPhoneNumber), '') as [MainPhoneNumber],
		COALESCE(CONVERT(varchar(255), c.Notes), '') as [Notes],
		COALESCE(CONVERT(varchar(255), c.OfficeID), '') as [OfficeID],
		COALESCE(CONVERT(varchar(255), c.OfficeName), '') as [OfficeName],
		COALESCE(CONVERT(varchar(255), c.Postcode), '') as [Postcode],
		COALESCE(CONVERT(varchar(255), c.Town), '') as [Town]
		FROM dbo.CustomerOffice c WITH (NOLOCK) 
		WHERE c.OfficeID = @CustomerOfficeID) p
	UNPIVOT
	(
		RealValue FOR ColumnName IN
		(
			[Address1],[Address2],[ClientID],[Country],[CountryID],[County],[CustomerID],[FaxNumber],[MainPhoneNumber],[Notes],[OfficeID],[OfficeName],[Postcode],[Town]
		)
	) as unpvt

)





GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotCustomerOffice] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnUnpivotCustomerOffice] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotCustomerOffice] TO [sp_executeall]
GO
