SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-02-18
-- Description:	Gets the co-insurance applicable to this policy instance
-- Mods
--	2015-04-21 DCM ticket 32118 Pet Age >= policy age (as opposed =)
--  2016-01-14 DCM Added @StartDate & update calculation of age
--  2016-12-14 DCM Updated secrl.ValueInt for 600
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetPolicyCoInsurance]
(
	@MatterID INT,
	@StartDate DATE
)
RETURNS INT	
AS
BEGIN

--declare @MatterID INT = 50022200
	
	DECLARE 
			@AnyValue VARCHAR(MAX),
			@AgeInYears INT,
			@Breed VARCHAR(100),
			@CaseID INT,
			@CustomerID INT,
			@CoIn INT,
			@LeadID INT,
			@SchemeMatterID INT,
			@SpeciesID INT,
			@TableRowID INT
	
	DECLARE @AgeRL tvpIntInt, @BreedRL tvpIntInt, @SpeciesRL tvpIntInt, @Breeds tvpIntVarchar, @BreedLists tvpIntIntVarchar
			
	SELECT @CaseID=CaseID,@leadID=LeadID 
	FROM Matter WITH (NOLOCK) WHERE MatterID=@MatterID

	-- find the scheme matterid
	SELECT @SchemeMatterID=schsch.MatterID 
	FROM MatterDetailValues sch WITH (NOLOCK) 
	INNER JOIN MatterDetailValues schsch WITH (NOLOCK) ON sch.ValueInt=schsch.ValueInt AND schsch.DetailFieldID=145689
	WHERE sch.MatterID=@MatterID AND sch.DetailFieldID=170034

	-- Get the policy specific information
	SELECT @Breed=rlbr.DetailValue, @SpeciesID=rlsp.ValueInt--, @AgeInYears=dbo.fnAgeFromDate(page.ValueDate)
	FROM LeadDetailValues ptype WITH (NOLOCK) 
	INNER JOIN ResourceListDetailValues rlsp WITH (NOLOCK) ON ptype.ValueInt=rlsp.ResourceListID AND rlsp.DetailFieldID=144269
	INNER JOIN ResourceListDetailValues rlbr WITH (NOLOCK) ON ptype.ValueInt=rlbr.ResourceListID AND rlbr.DetailFieldID=144270
	INNER JOIN LeadDetailValues page WITH (NOLOCK) ON page.LeadID=ptype.LeadID AND page.DetailFieldID=144274
	WHERE ptype.LeadID=@leadID AND ptype.DetailFieldID=144272

	DECLARE @Overrides dbo.tvpIntVarcharVarchar
	IF @StartDate IS NOT NULL
	BEGIN
	
		INSERT INTO @Overrides VALUES
		(2,'YearStart',CONVERT(VARCHAR(10),@StartDate,120))
	
	END
	
	SELECT @CustomerID=l.CustomerID, @LeadID=l.LeadID, @CaseID=m.CaseID 
	FROM Matter m WITH (NOLOCK)
	INNER JOIN Lead l WITH (NOLOCK) ON m.LeadID=l.LeadID 
	WHERE m.MatterID=@MatterID
	
	SELECT @AgeInYears=CAST(dbo.fn_C00_1273_RulesEngine_GetAgeInMonths (	@CustomerID, @LeadID, @CaseID, @MatterID, @Overrides)/12 AS INT)
	
	-- Age
	INSERT INTO @AgeRL (ID1,ID2)
	SELECT ag.TableRowID, CASE WHEN ag.ValueInt IS NULL THEN 1 ELSE 0 END
	FROM TableDetailValues ag WITH (NOLOCK) 
	INNER JOIN TableDetailValues sec WITH (NOLOCK) ON ag.TableRowID=sec.TableRowID AND sec.DetailFieldID=147192
	INNER JOIN ResourceListDetailValues secrl WITH (NOLOCK) ON sec.ResourceListID=secrl.ResourceListID AND secrl.DetailFieldID=146189 AND secrl.ValueInt=74284
	WHERE ag.MatterID=@SchemeMatterID AND ag.DetailFieldID=146211 
	AND ( ag.ValueInt<=@AgeInYears OR ag.ValueInt IS NULL )
	AND NOT EXISTS (
	SELECT *
	FROM TableDetailValues ag2 WITH (NOLOCK) 
	INNER JOIN TableDetailValues sec WITH (NOLOCK) ON ag2.TableRowID=sec.TableRowID AND sec.DetailFieldID=147192
	INNER JOIN ResourceListDetailValues secrl WITH (NOLOCK) ON sec.ResourceListID=secrl.ResourceListID AND secrl.DetailFieldID=146189 AND secrl.ValueInt=74284
	WHERE ag2.MatterID=@SchemeMatterID AND ag2.DetailFieldID=146211
	AND ag2.TableRowID<>ag.TableRowID AND ag2.ValueInt>ag.ValueInt AND ag2.ValueInt<=@AgeInYears
	)	

	-- Breed
	-- Make breeds list
	INSERT INTO @BreedLists
	SELECT 0,bl.TableRowID,bl.DetailValue
	FROM TableDetailValues bl WITH (NOLOCK)
	INNER JOIN TableDetailValues sec WITH (NOLOCK) ON bl.TableRowID=sec.TableRowID AND sec.DetailFieldID=147192
	INNER JOIN ResourceListDetailValues secrl WITH (NOLOCK) ON sec.ResourceListID=secrl.ResourceListID AND secrl.DetailFieldID=146189 AND secrl.ValueInt=74284
	WHERE bl.MatterID=@SchemeMatterID AND bl.DetailFieldID=148232 AND bl.DetailValue<>''

	WHILE EXISTS ( SELECT * FROM @BreedLists WHERE AnyID1=0 )
	BEGIN
	
		SELECT TOP 1 @TableRowID=AnyID2,@AnyValue=AnyValue FROM @BreedLists WHERE AnyID1=0
		
		INSERT INTO @Breeds
		SELECT @TableRowID,v.AnyValue
		FROM dbo.fnTableOfValuesFromCSV(@AnyValue) v
		WHERE v.AnyValue <> '' --Because users sometimes end their CSV list with a comma *sigh*
	
		UPDATE @BreedLists SET AnyID1=1 WHERE AnyID2=@TableRowID
		
	END
	SELECT @TableRowID=NULL
	INSERT INTO @Breeds
	SELECT tbr.TableRowID,''
	FROM TableDetailValues tbr WITH (NOLOCK) 
	INNER JOIN TableDetailValues sec WITH (NOLOCK) ON tbr.TableRowID=sec.TableRowID AND sec.DetailFieldID=147192
	INNER JOIN ResourceListDetailValues secrl WITH (NOLOCK) ON sec.ResourceListID=secrl.ResourceListID AND secrl.DetailFieldID=146189 AND secrl.ValueInt=74284
	WHERE tbr.MatterID=@SchemeMatterID AND tbr.DetailFieldID=148232 
	AND (tbr.DetailValue='' OR tbr.DetailValue IS NULL)

	INSERT INTO @BreedRL(ID1,ID2)
	SELECT AnyID, CASE WHEN AnyValue='' THEN 1 ELSE 0 END
	FROM @Breeds tbr 
	WHERE @Breed LIKE tbr.AnyValue OR tbr.AnyValue=''

	-- Species
	INSERT INTO @SpeciesRL(ID1,ID2)
	SELECT sp.TableRowID, CASE WHEN (sp.ValueInt=0 OR sp.ValueInt IS NULL) THEN 1 ELSE 0 END
	FROM TableDetailValues sp WITH (NOLOCK) 
	INNER JOIN TableDetailValues sec WITH (NOLOCK) ON sp.TableRowID=sec.TableRowID AND sec.DetailFieldID=147192
	INNER JOIN ResourceListDetailValues secrl WITH (NOLOCK) ON sec.ResourceListID=secrl.ResourceListID AND secrl.DetailFieldID=146189 AND secrl.ValueInt=74284
	WHERE sp.MatterID=@SchemeMatterID AND sp.DetailFieldID=146210 
	AND (sp.ValueInt=@SpeciesID OR sp.ValueInt=0 OR sp.ValueInt IS NULL)

	-- best match table row
	SELECT TOP 1 @TableRowID=a.ID1 FROM @AgeRL a
	INNER JOIN @BreedRL b ON a.ID1=b.ID1
	INNER JOIN @SpeciesRL s ON a.ID1=s.ID1
	ORDER BY (a.ID2+b.ID2+s.ID2)

	SELECT @CoIn=CAST(ValueMoney AS INT) FROM TableDetailValues WITH (NOLOCK) 
	WHERE TableRowID=@TableRowID AND DetailFieldID=147194

	--SELECT ISNULL(@CoIn,0)
	RETURN ISNULL(@CoIn,0)

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetPolicyCoInsurance] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetPolicyCoInsurance] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetPolicyCoInsurance] TO [sp_executeall]
GO
