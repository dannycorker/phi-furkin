SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Conor Reeves
-- Create date: 2019-06-18
-- Description:	To concatenate the Policy numbers into a single string for OMF 
-- 2019-07-16 CR updated the function to resolve the issue with the commas.
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_ConcatPremier]
(
	@CustomerID	INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE @MyListOfPremier VARCHAR(2000) = ''

	SELECT @MyListOfPremier += CAST (mdv.ValueMoney as VARCHAR (10))+ ', '
	FROM Matter m WITH ( NOLOCK )
	--INNER JOIN dbo.Lead l WITH ( NOLOCK ) on c.CustomerID = l.leadid
	INNER JOIN dbo.lead l WITH ( NOLOCK ) on l.LeadID = m.LeadID
	INNER JOIN dbo.MatterDetailValues mdv WITH ( NOLOCK ) on mdv.LeadID = m.LeadID and mdv.DetailFieldID = 179945 
	INNER JOIN MatterDetailValues PolicyStatus WITH (NOLOCK) on PolicyStatus.MatterID = m.MatterID AND PolicyStatus.DetailFieldID = 170038 --Policy Status
	INNER JOIN MatterDetailValues OMFOutcomes	WITH (NOLOCK) on OMFOutcomes.MatterID = m.MatterID AND OMFOutcomes.DetailFieldID = 179964 -- OMF Outcomes
	WHERE l.CustomerID = @CustomerID
	and m.MatterRef = 'One Month Free'
	and l.LeadTypeID = 1492
	AND PolicyStatus.ValueInt not in (43003,43004,74535,76595) -- Cancelled Lapsed CancelPending and Void
	and OMFOutcomes.ValueInt not in (76418)

	--SELECT @MyListOfPolicyRef += mdv.DetailValue + ', '
	--FROM Matter m WITH ( NOLOCK )
	----INNER JOIN dbo.Lead l WITH ( NOLOCK ) on c.CustomerID = l.leadid
	--INNER JOIN dbo.lead l WITH ( NOLOCK ) on l.LeadID = m.LeadID
	--INNER JOIN dbo.MatterDetailValues mdv WITH ( NOLOCK ) on mdv.LeadID = m.LeadID and mdv.DetailFieldID = 170050 
	--WHERE l.CustomerID = @CustomerID
	--and m.MatterRef = 'One Month Free'
	--and l.LeadTypeID = 1492


	IF @@ROWCOUNT > 0
	BEGIN
		SELECT @MyListOfPremier = LEFT (@MyListOfPremier, LEN(@MyListOfPremier) -1)
	END

	RETURN @MyListOfPremier

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_ConcatPremier] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_ConcatPremier] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_ConcatPremier] TO [sp_executeall]
GO
