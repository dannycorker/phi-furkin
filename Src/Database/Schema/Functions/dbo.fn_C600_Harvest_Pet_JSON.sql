SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Gavin Reynolds
-- Create date:	2020-01-27
-- Description:	harvests Animal json object
-- 2020-01-27 GPR						| Altered to use @Include flag
-- 2020-01-27 CPS for JIRA LAGCLAIM-170	| Update for C60x fields
-- 2020-01-27 CPS for JIRA LAGCLAIM-170 | Change to XML
-- 2020-01-27 CPS for JIRA LAGCLAIM-170 | Change back to JSON for Ian
-- 2020-01-27 CPS for JIRA LAGCLAIM-369 | Update to match end to end solution document
-- 2020-01-29 MAB for Jira LAGCLAIM-366 | modified data definitions to match requirements and synch with related data definitions
-- 2020-01-31 CPS for JIRA LAGCLAIM-369 | Included @IncludePolicy and @IncludeClaims parameters
-- 2020-02-05 MAB added vet prefix to vet attributes
-- 2020-02-05 CPS for JIRA LAGCLAIM-369 | Return Object ID  
-- 2020-02-19 CPS for JIRA LAGCLAIM-452 | Include claims
-- 2020-03-12 CPS for JIRA LAGCLAIM-452 | Don't include claims if there are no claims to include
-- 2020-03-24 CPS for JIRA LAGCLAIM-VIS | Updated to match vision shapes
-- 2020-07-23 JEL for SANITY added OneVision lookup of VisionCustomerID
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Harvest_Pet_JSON]
(
	 @LeadID			INT
	,@VisionMode		BIT = 0
	,@IncludePolicy		BIT = 0
	,@IncludeClaims		BIT = 0
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE  @JsonData		VARCHAR(MAX) = ''
			,@PolicyJson	VARCHAR(MAX) = ''
			,@ClaimJson		VARCHAR(MAX) = ''
			,@VisionCustomerID INT 

	-- 2020-03-12 CPS for JIRA LAGCLAIM-452 | Don't include claims if there are no claims to include.
	--										| Otherwise the API tries to create a dummy, which ends up invalid
	IF NOT EXISTS ( SELECT *
					FROM LeadTypeRelationship ltr WITH (NOLOCK) 
					INNER JOIN Matter mClaim WITH (NOLOCK) on mClaim.LeadID = ltr.ToLeadID
					INNER JOIN MatterDetailValues mdvType WITH (NOLOCK) on mdvType.MatterID = mClaim.MatterID AND mdvType.DetailFieldID = 144483 /*Claim Type*/
					WHERE ltr.FromLeadID = @LeadID
					AND ltr.ToLeadTypeID = 1490 /*PetClaim*/
					AND ltr.FromLeadTypeID = 1492 /*PolicyAdmin*/
					AND mdvType.ValueInt <> 0
				  )
	BEGIN
		SELECT @IncludeClaims = 0
	END

	SELECT @VisionCustomerID = v.VisionCustomerID FROM OneVision v with (NOLOCK)
	WHERE v.PALeadID = @LeadID 

	IF @LeadID > 0
	BEGIN
		SELECT  @JsonData +=
		(
			SELECT TOP 1
				 @VisionCustomerID														[customerId]
				,CASE WHEN @VisionMode = 1 AND @IncludePolicy = 1 THEN '{PolicyJson}' END [policies]
				,CASE WHEN @VisionMode = 0 AND @IncludePolicy = 1 THEN '{PolicyJson}' END [policy]

				/*Name*/
				,CASE WHEN @VisionMode = 1 THEN AnimalName.DetailValue	END				[name]
				,CASE WHEN @VisionMode = 0 THEN AnimalName.DetailValue	END				[petName] -- L&G request to clarify ambiguous fields such as "name"

				/*Microchip*/
				,ldvMicrochipNo.DetailValue												[microchipNumber]

				,CASE WHEN @VisionMode = 0 THEN
					CASE WHEN ldvMicrochipStatus.ValueInt = 5144 THEN '1' ELSE '0' END
						ELSE
					CASE WHEN ldvMicrochipStatus.ValueInt = 5144 THEN 'true' ELSE 'false' END
				 END																	[microchipStatus]

				/*Neutered*/
				,CASE WHEN @VisionMode = 0 THEN
					CASE WHEN ldvNeutered.ValueInt = 5144 THEN '1' ELSE '0' END
						ELSE
					CASE WHEN ldvNeutered.ValueInt = 5144 THEN 'true' ELSE 'false' END
				 END																	[neutered]

				/*Sex*/
				,CASE WHEN @VisionMode = 1 THEN CASE WHEN Sex.ValueInt = 5168 THEN 1 ELSE 2 END END	[gender]
				,CASE WHEN @VisionMode = 0 THEN liSex.ItemValue	END						[petSex]

				/*Birth Date*/
				,CASE WHEN @VisionMode = 1 THEN DateBirth.ValueDateTime	END				[birthDate]
				,CASE WHEN @VisionMode = 0 THEN DateBirth.ValueDateTime	END				[petBirthDate]

				,CASE WHEN @VisionMode = 0 THEN DateDeath.ValueDateTime	END 			[deathDate]

				,CAST(ROUND(ldvPrice.ValueMoney,0) as int)								[purchasePrice]     --CR Changed to ValueMoney as this was causing a 4dp issue

				,CASE WHEN @VisionMode = 0 THEN
					CASE WHEN ldvAggressive.ValueInt = 5144 THEN 1 ELSE 0 END			
				 END																	[aggressiveTendencies]

				,CASE WHEN @VisionMode = 1 THEN
					CASE WHEN ldvPreExistingCon.ValueInt = 5144 THEN 'true' ELSE 'false' END
						ELSE
					CASE WHEN ldvPreExistingCon.ValueInt = 5144 THEN '1' ELSE '0' END
				 END																	[preExistingConditions]

				/*Breed*/
				,CASE WHEN @VisionMode = 1 THEN 2 END	/*Hard coded for LPC testing*/	[breedId]
				,CASE WHEN @VisionMode = 0 THEN Breed.ValueInt END						[breedResourceListId]

				/*Colour*/
				,CASE WHEN @VisionMode = 1 THEN 4 END									[colorId]
				,(	
					SELECT 
						 vet.ValueInt				[vetResourceListId]
						,pracName.DetailValue		[petPracticeName]
						,vetAddress1.DetailValue	[petAddress1]
						,vetAddress2.DetailValue	[petAddress2]
						,vetTown.DetailValue		[petTown]
						,vetPostcode.DetailValue	[petPostcode]
						,vetTelephone.DetailValue	[petTelephoneNumber]
					FROM LeadDetailValues vet WITH(NOLOCK)
						LEFT JOIN ResourceListDetailValues pracName	WITH (NOLOCK)     ON pracName.ResourceListID = vet.ValueInt		AND pracName.DetailFieldID		= 144473 /*Vet Name*/
						LEFT JOIN ResourceListDetailValues vetAddress1	WITH (NOLOCK) ON vetAddress1.ResourceListID = vet.ValueInt	AND vetAddress1.DetailFieldID	= 144475 /*Vet Address1*/
						LEFT JOIN ResourceListDetailValues vetAddress2	WITH (NOLOCK) ON vetAddress2.ResourceListID = vet.ValueInt	AND vetAddress2.DetailFieldID	= 144476 /*Vet Address2*/	
						LEFT JOIN ResourceListDetailValues vetTown		WITH (NOLOCK) ON vetTown.ResourceListID = vet.ValueInt		AND vetTown.DetailFieldID		= 144477 /*Vet Town*/					
						LEFT JOIN ResourceListDetailValues vetPostcode	WITH (NOLOCK) ON vetPostcode.ResourceListID = vet.ValueInt	AND vetPostcode.DetailFieldID	= 144479 /*Vet Postcode*/	
						LEFT JOIN ResourceListDetailValues vetTelephone	WITH (NOLOCK) ON vetTelephone.ResourceListID = vet.ValueInt	AND vetTelephone.DetailFieldID	= 144480 /*Vet Tel*/	
					WHERE vet.LeadID = l.LeadID
					AND vet.DetailFieldID = 146215 /*Current Vet*/
					AND @VisionMode = 0
					FOR JSON PATH
				) [vets]
				,CASE WHEN @VisionMode = 0 THEN ldvGuid.DetailValue								END [objectID]
				,CASE WHEN @VisionMode = 0 THEN cdvParentID.DetailValue							END [parentObjectId]
				,CASE WHEN @VisionMode = 0 THEN CASE WHEN @IncludeClaims = 1 THEN '{ClaimJson}' END END [claims]
			FROM Lead l WITH (NOLOCK) 
			INNER JOIN Customers cu							WITH (NOLOCK) on cu.CustomerID = l.CustomerID
			INNER JOIN LeadDetailValues AnimalName			WITH (NOLOCK) on l.LeadID = AnimalName.LeadID							AND AnimalName.DetailFieldID			= 144268 /*Pet Name*/
			INNER JOIN LeadDetailValues DateBirth			WITH (NOLOCK) on l.LeadID = DateBirth.LeadID							AND DateBirth.DetailFieldID				= 144274 /*Pet Date of Birth*/
			LEFT JOIN LeadDetailValues DateDeath			WITH (NOLOCK) on l.LeadID = DateDeath.LeadID							AND DateDeath.DetailFieldID				= 144271 /*Date Pet Deceased or Lost*/
			INNER JOIN LeadDetailValues Sex					WITH (NOLOCK) on l.LeadID = Sex.LeadID									AND Sex.DetailFieldID					= 144275 /*Pet Sex*/
			INNER JOIN LookupListItems	liSex				WITH (NOLOCK) on liSex.LookupListItemID = Sex.ValueInt															
			LEFT JOIN LeadDetailValues ldvNeutered			WITH (NOLOCK) on l.LeadID = ldvNeutered.LeadID							AND ldvNeutered.DetailFieldID			= 152783 /*Pet Neutered*/
			LEFT JOIN LeadDetailValues ldvMicrochipStatus	WITH (NOLOCK) on ldvMicrochipStatus.LeadID = l.LeadID					AND ldvMicrochipStatus.DetailFieldID	= 177372 /*Microchip*/
			LEFT JOIN LeadDetailValues ldvMicrochipNo		WITH (NOLOCK) on ldvMicrochipNo.LeadID = l.LeadID						AND ldvMicrochipNo.DetailFieldID		= 170030 /*Microchip number*/
			INNER JOIN LeadDetailValues Breed				WITH (NOLOCK) on Breed.LeadID = l.LeadID								AND Breed.DetailFieldID					= 144272 /*Pet Type*/
			INNER JOIN ResourceListDetailValues	BreedName	WITH (NOLOCK) on BreedName.ResourceListID = Breed.ValueInt				AND BreedName.DetailFieldID				= 144270 /*Pet Breed*/
			INNER JOIN ResourceListDetailValues Species		WITH (NOLOCK) on Species.ResourceListID = Breed.ValueInt				AND Species.DetailFieldID				= 144269 /*Pet Type*/
			INNER JOIN LookupListItems	liSpecies			WITH (NOLOCK) on liSpecies.LookupListItemID = Species.ValueInt									 
			LEFT JOIN LeadDetailValues ldvPrice				WITH (NOLOCK) on ldvPrice.LeadID = l.LeadID								AND ldvPrice.DetailFieldID				= 144339 /*Pet Purchase Cost*/
			LEFT JOIN LeadDetailValues ldvPreExistingCon	WITH (NOLOCK) on ldvPreExistingCon.LeadID = l.LeadID					AND ldvPreExistingCon.DetailFieldID		= 175496 /*Any Existing Conditions?*/
			LEFT JOIN LeadDetailValues ldvAggressive		WITH (NOLOCK) on ldvAggressive.LeadID = l.LeadID						AND ldvAggressive.DetailFieldID			= 177452 /*Has your pet ever shown aggressive tendencies?*/
			INNER JOIN LeadDetailValues ldvGuid				WITH (NOLOCK) on ldvGuid.LeadID = l.LeadID and ldvGuid.DetailFieldID = 313787 /*API Transfer Mapping GUID*/
			LEFT JOIN CustomerDetailValues cdvParentID		WITH (NOLOCK) on cdvParentID.CustomerID  = l.CustomerID					AND cdvParentID.DetailFieldID			= 313784 /*API Transfer Mapping GUID*/
			WHERE l.LeadID = @LeadID
			ORDER BY l.LeadID DESC
			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		)
	END

	IF @IncludePolicy = 1
	BEGIN
		IF @VisionMode = 0
		BEGIN
			SELECT TOP 1 @PolicyJson = dbo.fn_C600_Harvest_Policy_JSON(m.MatterID,@VisionMode)
			FROM Matter m WITH (NOLOCK) 
			WHERE m.LeadID = @LeadID

			SELECT @JsonData = REPLACE(@JsonData,'"{PolicyJson}"',@PolicyJson)
		END
		ELSE
		BEGIN
			SELECT @PolicyJson = '['

			SELECT @PolicyJson += dbo.fn_C600_Harvest_Policy_JSON(m.MatterID,@VisionMode) + ','
			FROM Matter m WITH (NOLOCK) 
			WHERE m.LeadID = @LeadID

			SELECT @PolicyJson = LEFT(@PolicyJson,LEN(@PolicyJson)-1)
			SELECT @PolicyJson += ']'

			SELECT @JsonData = REPLACE(@JsonData,'"{PolicyJson}"',ISNULL(@PolicyJson,'[{}]'))
		END
	END

	IF @IncludeClaims = 1
	BEGIN
		SELECT @ClaimJson = '['

		SELECT @ClaimJson += dbo.fn_C600_Harvest_Claim_JSON(mClaim.MatterID,0) + ','
		FROM LeadTypeRelationship ltr WITH (NOLOCK) 
		INNER JOIN Matter mClaim WITH (NOLOCK) on mClaim.LeadID = ltr.ToLeadID
		INNER JOIN MatterDetailValues mdvType WITH (NOLOCK) on mdvType.MatterID = mClaim.MatterID AND mdvType.DetailFieldID = 144483 /*Claim Type*/
		WHERE ltr.FromLeadID = @LeadID
		AND ltr.ToLeadTypeID = 1490 /*PetClaim*/
		AND ltr.FromLeadTypeID = 1492 /*PolicyAdmin*/
		AND mdvType.ValueInt <> 0

		SELECT @ClaimJson = LEFT(@ClaimJson,LEN(@ClaimJson)-1)
		SELECT @ClaimJson += ']'

		SELECT @JsonData = REPLACE(@JsonData,'"{ClaimJson}"',ISNULL(@ClaimJson,'[{}]'))
	END

	RETURN @JsonData

END

















GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Harvest_Pet_JSON] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_Harvest_Pet_JSON] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Harvest_Pet_JSON] TO [sp_executeall]
GO
