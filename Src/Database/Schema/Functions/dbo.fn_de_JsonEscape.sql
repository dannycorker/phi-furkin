SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   FUNCTION [dbo].[fn_de_JsonEscape](@value NVARCHAR(MAX))
RETURNS NVARCHAR(MAX)
AS BEGIN
 
	IF (@value is null) return 'null'
	--IF (TRY_PARSE(@value as float) IS NOT NULL) RETURN @value

	SET @value=REPLACE(@value,'\','\\')
	SET @value=REPLACE(@value,'"','\"')

	RETURN '"'+@value+'"'

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_de_JsonEscape] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_de_JsonEscape] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_de_JsonEscape] TO [sp_executeall]
GO
