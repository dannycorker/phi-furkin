SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-06-09
-- Description:	Returns the County Tax factor by County from Zip
-- Modified:	2020-08-26	SB	Changed to return the TaxProportion rather than the GrossMultiplier
-- 2020-12-01 GPR updated select to use @Postcode in place of @PostcodeInt for PPET-764
-- =============================================
CREATE FUNCTION [dbo].[fn_RulesEngine_GetCountyTax]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS REAL
AS
BEGIN

	DECLARE @CountyTax MONEY,
			@Postcode VARCHAR(20),
			@StateString VARCHAR(500),
			@YearStart DATE,
			@ClientID INT,
			@CountryID INT,
			@State VARCHAR(3),
			@PostcodeInt INT

	/*Get ClientID and CountryID*/
	/*GPR 2020-08-26 removed below line*/
	--SELECT @ClientID = ClientID FROM Customers WITH (NOLOCK) WHERE CustomerID = @CustomerID
	
	IF @ClientID IS NULL
	BEGIN
		SELECT @ClientID = [dbo].[fnGetPrimaryClientID]()
	END

	SELECT @CountryID = CountryID FROM Clients WITH (NOLOCK) WHERE ClientID = @ClientID

	/*Year start date is always passed in*/
	SELECT @YearStart = AnyValue2
	FROM @Overrides
	WHERE AnyID = 2
	AND AnyValue1 = 'YearStart'
	/*except not always from RulesEngine test interface so default it*/
	IF @YearStart IS NULL SELECT @YearStart=dbo.fn_GetDate_Local()
	
	--/*Get Postcode*/
	--IF @CustomerID IS NOT NULL
	--BEGIN
	--	SELECT @Postcode=c.PostCode 
	--	FROM Customers c WITH (NOLOCK) WHERE c.CustomerID=@CustomerID
	--END
	
	IF NULLIF(@Postcode,'') IS NULL /*GPR 2020-05-12*/
	BEGIN
		SELECT @Postcode=AnyValue2 FROM @Overrides WHERE AnyID = 2 AND AnyValue1 = 'PostCode'

		IF NULLIF(@Postcode,'') IS NULL
		BEGIN
			SELECT @Postcode = c.PostCode FROM Customers c WITH (NOLOCK) WHERE c.CustomerID = @CustomerID
		END
	
	END

	--IF dbo.fnIsInt(@Postcode) = 1
	--BEGIN
	--	SELECT @PostcodeInt = CONVERT(int, @Postcode)
	--END
	IF @CountryID = 233
	BEGIN
		SELECT TOP(1) @CountyTax = ctax.TaxProportion 
		FROM USTaxCityandCounty ctax WITH (NOLOCK) 
		WHERE ctax.CountryID= @CountryID
		AND (ctax.[From] IS NULL OR @YearStart >= ctax.[From])
		AND (ctax.[To] IS NULL OR @YearStart <= ctax.[To])
		AND (CONVERT(VARCHAR,ctax.Zip) = @Postcode)
	END

	RETURN  ISNULL(@CountyTax,0.0000) /*If no tax is found return 0 as default*/
 
END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_GetCountyTax] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_RulesEngine_GetCountyTax] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_GetCountyTax] TO [sp_executeall]
GO
