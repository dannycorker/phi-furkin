SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Conor Reeves
-- Create date: 2019-06-18
-- Description:	To concatenate the Petnames into a single string 
-- 2019-07-16 CR updated the function to resolve the issue with the commas.
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_ConcatPetNames]
(
                @CustomerID   INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

                DECLARE @MyStringOfPetNames VARCHAR(2000) = ''

                SELECT @MyStringOfPetNames += ldv.DetailValue + ', '
                FROM Lead l WITH ( NOLOCK ) 
                INNER JOIN dbo.LeadDetailValues ldv WITH ( NOLOCK ) on ldv.LeadID = l.LeadID AND ldv.DetailFieldID = 144268 /*Pet Name*/
                INNER JOIN dbo.matter m WITH ( NOLOCK ) on m.LeadID = l.LeadID
                INNER JOIN MatterDetailValues PolicyStatus WITH (NOLOCK) on PolicyStatus.MatterID = m.MatterID AND PolicyStatus.DetailFieldID = 170038 --Policy Status
                INNER JOIN MatterDetailValues OMFOutcomes WITH (NOLOCK) on OMFOutcomes.MatterID = m.MatterID AND OMFOutcomes.DetailFieldID = 179964 -- OMF Outcomes
                WHERE l.CustomerID = @CustomerID
                and m.MatterRef = 'One Month Free'
                AND PolicyStatus.ValueInt not in (43003,43004,74535,76595) -- Cancelled Lapsed CancelPending and Void
                and OMFOutcomes.ValueInt not in (76418)

                IF @@ROWCOUNT > 0
                BEGIN
                                SELECT @MyStringOfPetNames = LEFT (@MyStringOfPetNames, LEN(@MyStringOfPetNames) -1)
                END

                RETURN @MyStringOfPetNames

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_ConcatPetNames] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_ConcatPetNames] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_ConcatPetNames] TO [sp_executeall]
GO
