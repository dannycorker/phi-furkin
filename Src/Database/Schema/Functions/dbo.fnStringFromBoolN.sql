SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2010-09-28
-- Description:	Return true or false from 1, 0 or null
--              Same as fnStringFromBool but allows NULL as well.
-- =============================================
CREATE FUNCTION [dbo].[fnStringFromBoolN]
(
	@Instr varchar(5)
)
RETURNS varchar(5)
AS
BEGIN

	/* Return value is null by default */
	DECLARE @Outstr varchar(5) = null

	/* Choose 'true' or 'false' if possible */
	SELECT @Outstr = CASE @Instr 
						 WHEN '1' THEN 'true'
						 WHEN 'Y' THEN 'true'
						 WHEN 'YES' THEN 'true'
						 WHEN 'TRUE' THEN 'true'
						 WHEN '0' THEN 'false'
						 WHEN 'N' THEN 'false'
						 WHEN 'NO' THEN 'false'
						 WHEN 'FALSE' THEN 'false'
					 END 
	
	RETURN @Outstr

END









GO
GRANT VIEW DEFINITION ON  [dbo].[fnStringFromBoolN] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnStringFromBoolN] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnStringFromBoolN] TO [sp_executeall]
GO
