SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-06-30
-- Description:	Returns a static data object name (e.g. EventTypeName) from an ID, with or without HTML styling.
-- CPS 2017-07-06 Added DetailFields support
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_ReturnAquariumObjectName]
(
	  @ObjectID				INT
	 ,@AquariumObjectTypeID	INT
	 ,@ReturnAsHtml			BIT
)
RETURNS VARCHAR(2000)
AS
BEGIN
			
	DECLARE  @ReturnValue		VARCHAR(2000) = ''
			,@ObjectName		VARCHAR(2000) = ''
			,@ClientID			INT = dbo.fnGetPrimaryClientID()
			,@FontColour		VARCHAR(5)	  = 'black'

	IF @AquariumObjectTypeID = 2 /*DetailFields*/
	BEGIN
		SELECT	 @ObjectName = df.FieldName
				,@FontColour = 'green'
		FROM DetailFields df WITH ( NOLOCK ) 
		WHERE df.ClientID = @ClientID
		AND df.DetailFieldID = @ObjectID
		
		IF @@ROWCOUNT = 0
		BEGIN
			SELECT	 @ObjectName = 'ERROR:  No Detail Field Matched'
					,@FontColour = 'red'
		END
	END
	ELSE
	IF @AquariumObjectTypeID = 3 /*EventType*/
	BEGIN
		SELECT	 @ObjectName = et.EventTypeName
				,@FontColour = 'green'
		FROM EventType et WITH ( NOLOCK ) 
		WHERE et.ClientID = @ClientID
		AND et.EventTypeID = @ObjectID
		
		IF @@ROWCOUNT = 0
		BEGIN
			SELECT	 @ObjectName = 'ERROR:  No Event Type Matched'
					,@FontColour = 'red'
		END
	END
	ELSE
	IF @AquariumObjectTypeID = 6 /*DocumentType*/
	BEGIN
		SELECT	 @ObjectName = dt.DocumentTypeName
				,@FontColour = 'green'
		FROM DocumentType dt WITH ( NOLOCK ) 
		WHERE dt.ClientID = @ClientID
		AND dt.DocumentTypeID = @ObjectID
		
		IF @@ROWCOUNT = 0
		BEGIN
			SELECT	 @ObjectName = 'ERROR:  No Document Type Matched'
					,@FontColour = 'red'
		END
	END
	ELSE
	BEGIN
		SELECT	 @ObjectName = 'ERROR:  ObjectTypeID ' + ISNULL(CONVERT(VARCHAR,@ObjectID),'NULL') + ' is not supported by function [' + OBJECT_NAME(@@PROCID) + '].'
				,@FontColour = 'Red'
	END

	IF @ReturnAsHtml = 1
	BEGIN
		SELECT @ReturnValue = '<font color="' + @FontColour + '">' + @ObjectName + '</font>'
	END
	ELSE
	BEGIN
		SELECT @ReturnValue = @ObjectName
	END

	SELECT  @ReturnValue = ISNULL(@ReturnValue,'')
	
	RETURN  @ReturnValue

END






GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_ReturnAquariumObjectName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_ReturnAquariumObjectName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_ReturnAquariumObjectName] TO [sp_executeall]
GO
