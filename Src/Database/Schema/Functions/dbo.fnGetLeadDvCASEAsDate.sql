SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-06-12
-- Description:	Get LeadDV directly #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetLeadDvCASEAsDate]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS date
AS
BEGIN

	DECLARE @ValueDate AS date

	SELECT @ValueDate = ldv.ValueDate 
	FROM Cases c WITH (NOLOCK) 
	INNER JOIN LeadDetailValues ldv WITH (NOLOCK) ON c.LeadID = ldv.LeadID AND ldv.DetailFieldID = @DetailFieldID
	WHERE c.CaseID = @CaseID 
	
	RETURN @ValueDate

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLeadDvCASEAsDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetLeadDvCASEAsDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLeadDvCASEAsDate] TO [sp_executeall]
GO
