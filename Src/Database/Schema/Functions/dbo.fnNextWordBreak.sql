SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2014-01-10
-- Description:	Find the next break-of-word position
-- =============================================
CREATE FUNCTION [dbo].[fnNextWordBreak] 
(
	@sentence VARCHAR(MAX),
	@cursorpos INT			/* 1 is the normal start point for Sql Server */
)
RETURNS INT 
AS
BEGIN
	
	/* Now find the next instance of each possible break-of-word type */
	DECLARE @sep TABLE (pos INT)
	
	INSERT @sep(pos) SELECT CHARINDEX('.', @sentence, @cursorpos)
	INSERT @sep(pos) SELECT CHARINDEX(',', @sentence, @cursorpos)
	INSERT @sep(pos) SELECT CHARINDEX('!', @sentence, @cursorpos)
	INSERT @sep(pos) SELECT CHARINDEX('?', @sentence, @cursorpos)
	INSERT @sep(pos) SELECT CHARINDEX(':', @sentence, @cursorpos)
	INSERT @sep(pos) SELECT CHARINDEX(' ', @sentence, @cursorpos)	
	
	/* 
		Where pos is zero it means not found from the start point onwards,
		So @cursurpos will remain zero if no more separators are found. 
	*/
	SELECT @cursorpos = MIN(s.pos) 
	FROM @sep s 
	WHERE s.pos > 0 
	
	/* Return the minimum pos found that is greater than zero (in net terms) */
	RETURN @cursorpos

END



GO
GRANT VIEW DEFINITION ON  [dbo].[fnNextWordBreak] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnNextWordBreak] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnNextWordBreak] TO [sp_executeall]
GO
