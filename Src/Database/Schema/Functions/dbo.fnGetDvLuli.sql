SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 14-July-2009
-- Description:	Get an item value from a lookup list, based on the detail value and caseid passed in
-- 2014-04-23 DCM Added CDV
-- =============================================
CREATE FUNCTION [dbo].[fnGetDvLuli]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

DECLARE @DetailValue AS VARCHAR(2000),
		@LeadOrMatter INT

	SELECT @LeadOrMatter = LeadOrmatter
	FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @DetailFieldID

	IF @LeadOrMatter = 1
	BEGIN

		SELECT @DetailValue = luli.ItemValue 
		FROM Cases c WITH (NOLOCK) 
		INNER JOIN LeadDetailValues ldv WITH (NOLOCK) ON c.LeadID = ldv.LeadID AND ldv.DetailFieldID = @DetailFieldID
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = ldv.ValueInt
		WHERE c.CaseID = @CaseID 
	
	END

	IF @LeadOrMatter = 2
	BEGIN

		SELECT @DetailValue = luli.ItemValue 
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND mdv.DetailFieldID = @DetailFieldID
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = mdv.ValueInt
		WHERE m.CaseID = @CaseID
	
	END

	IF @LeadOrMatter = 10 -- added dcm 23/04/2014
	BEGIN

		SELECT @DetailValue = luli.ItemValue 
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN CustomerDetailValues cdv WITH (NOLOCK) ON m.CustomerID = cdv.CustomerID AND cdv.DetailFieldID = @DetailFieldID
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = cdv.ValueInt
		WHERE m.CaseID = @CaseID
	
	END

	IF @LeadOrMatter = 11
	BEGIN

		SELECT @DetailValue = luli.ItemValue 
		FROM CaseDetailValues cdv WITH (NOLOCK)
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = cdv.ValueInt
		WHERE cdv.CaseID = @CaseID
		AND cdv.DetailFieldID = @DetailFieldID
	
	END

	RETURN @DetailValue

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDvLuli] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetDvLuli] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDvLuli] TO [sp_executeall]
GO
