SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-07-13
-- Description:	Return a table of varchars and matterid's
-- =============================================
CREATE FUNCTION [dbo].[fnTableOfRLValuesByCaseID]
(
	@DetailFieldID		int,
	@RLDetailFieldID	int,
	@CaseID				int
)
RETURNS 
@TableOut TABLE 
(
	LeadID int,
	MatterID int,
	DetailValue varchar(2000)
)
AS
BEGIN

	declare @LeadOrMatter int

	Select @LeadOrMatter = LeadOrmatter
	From DetailFields
	Where DetailFieldID = @DetailFieldID

	If @LeadOrMatter = 1
	Begin

		Insert Into @TableOut (LeadID, MatterID, DetailValue)
		Select ldv.LeadID, '', rldv.DetailValue
		From LeadDetailValues ldv
		Inner Join Cases c on c.LeadID = ldv.LeadID and c.CaseID = @CaseID
		Inner Join ResourceListDetailValues rldv on rldv.ResourcelistId = ldv.DetailValue and rldv.DetailFieldID = @RLDetailFieldID
		Where ldv.DetailFieldID = @DetailFieldID

	End

	If @LeadOrMatter = 2
	Begin

		Insert Into @TableOut (LeadID, MatterID, DetailValue)
		Select m.LeadID, m.MatterID, rldv.DetailValue
		From MatterDetailValues mdv
		Inner Join Matter m on m.MatterID = mdv.MatterID and m.CaseID = @CaseID
		Inner Join ResourceListDetailValues rldv on rldv.ResourcelistId = mdv.DetailValue and rldv.DetailFieldID = @RLDetailFieldID
		Where mdv.DetailFieldID = @DetailFieldID

	End

	RETURN
END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfRLValuesByCaseID] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnTableOfRLValuesByCaseID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfRLValuesByCaseID] TO [sp_executeall]
GO
