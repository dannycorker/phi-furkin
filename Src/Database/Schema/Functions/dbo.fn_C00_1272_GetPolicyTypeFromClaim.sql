SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-02-07
-- Description:	Returns the policy type for a customer's claim
-- ROH 2014-04-30 Use the condition onset date instead of treatment start to determine policy type (because it might have changed)
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetPolicyTypeFromClaim]
(
	@ClaimMatterID INT
)
RETURNS INT	
AS
BEGIN

	DECLARE @PolicyType INT
	
	DECLARE @SchemeID INT,
			@TreatmentStart DATE,
			@OnsetDate DATE
	
	-- Look up the treatment date
	SELECT @TreatmentStart = mdv.ValueDate, @OnsetDate = mdvOnset.ValueDate
	FROM dbo.Matter m WITH (NOLOCK) 
	INNER JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND mdv.DetailFieldID = 144366
	INNER JOIN dbo.MatterDetailValues mdvOnset WITH (NOLOCK) ON m.MatterID = mdvOnset.MatterID AND mdvOnset.DetailFieldID = 144892
	WHERE m.MatterID = @ClaimMatterID
	
	-- Now find the matching scheme
	--SELECT @SchemeID = dbo.fn_C00_1272_GetPolicyRowFromClaimAndDate(@ClaimMatterID, @TreatmentStart)
	SELECT @SchemeID = dbo.fn_C00_1272_GetPolicyRowFromClaimAndDate(@ClaimMatterID, @OnsetDate)
	
	SELECT @PolicyType = rdvType.ValueInt
	FROM dbo.ResourceListDetailValues rdvType WITH (NOLOCK)
	WHERE rdvType.DetailFieldID = 144319
	AND rdvType.ResourceListID = @SchemeID
	
	RETURN @PolicyType

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicyTypeFromClaim] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_GetPolicyTypeFromClaim] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicyTypeFromClaim] TO [sp_executeall]
GO
