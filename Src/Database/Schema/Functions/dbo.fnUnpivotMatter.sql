SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Unpivot the columns in the Matter table 
-- =============================================
CREATE FUNCTION [dbo].[fnUnpivotMatter] 
(	
	@MatterID int
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT ColumnName as [RawColumnName], '[!' + ColumnName + ']' as [FormattedColumnName], RealValue
	FROM
	(
		SELECT 
		COALESCE(CONVERT(varchar(255), c.BrandNew), '') as [BrandNew],
		COALESCE(CONVERT(varchar(255), c.CaseID), '') as [CaseID],
		COALESCE(CONVERT(varchar(255), c.ClientID), '') as [ClientID],
		COALESCE(CONVERT(varchar(255), c.CustomerID), '') as [CustomerID],
		COALESCE(CONVERT(varchar(255), c.LeadID), '') as [LeadID],
		COALESCE(CONVERT(varchar(255), c.MatterID), '') as [MatterID],
		COALESCE(CONVERT(varchar(255), c.MatterRef), '') as [MatterRef],
		COALESCE(CONVERT(varchar(255), c.MatterStatus), '') as [MatterStatus],
		COALESCE(CONVERT(varchar(255), c.RefLetter), '') as [RefLetter]
		FROM dbo.Matter c WITH (NOLOCK) 
		WHERE c.MatterID = @MatterID) p
	UNPIVOT
	(
		RealValue FOR ColumnName IN
		(
			[BrandNew],[CaseID],[ClientID],[CustomerID],[LeadID],[MatterID],[MatterRef],[MatterStatus],[RefLetter]
		)
	) as unpvt

)





GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotMatter] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnUnpivotMatter] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotMatter] TO [sp_executeall]
GO
