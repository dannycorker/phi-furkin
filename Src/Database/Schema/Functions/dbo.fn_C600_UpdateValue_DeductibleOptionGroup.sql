SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2021-01-27
-- Description:	Returns the Deductible Value by Option Group and PetAge
-- Modified:
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_UpdateValue_DeductibleOptionGroup]
(
	@OptionGroupID INT,
	@PetAgeInYears INT,
	@SpeciesID INT
)
RETURNS REAL
AS
BEGIN

	DECLARE @Deductible INT

	DECLARE @Options TABLE(OptionGroupID INT, SpeciesID INT, MinAge INT, MaxAge INT, Deductible INT)

	-- 193352 (A)
	-- 193353 (B)
	-- 193354 (C)

	-- 42989 (Canine)
	-- 42990 (Feline)

	/*Canine*/
	INSERT INTO @Options (OptionGroupID, SpeciesID, MinAge, MaxAge, Deductible)
	VALUES	(193352,42989,0,5, 100),
			(193353,42989,0,5, 300),
			(193354,42989,0,5, 750),
			(193352,42989,6,10, 300),
			(193353,42989,6,10, 500),
			(193354,42989,6,10, 950),
			(193352,42989,11,99, 500),
			(193353,42989,11,99, 700),
			(193354,42989,11,99, 1150)

	/*Feline*/
	INSERT INTO @Options (OptionGroupID, SpeciesID, MinAge, MaxAge, Deductible)
	VALUES	(193352,42990,0,5, 100),
			(193353,42990,0,5, 300),
			(193354,42990,0,5, 750),
			(193352,42990,6,10, 200),
			(193353,42990,6,10, 400),
			(193354,42990,6,10, 850),
			(193352,42990,11,99, 300),
			(193353,42990,11,99, 500),
			(193354,42990,11,99, 950)
	
	SELECT @Deductible = o.Deductible
	FROM @Options o
	WHERE o.OptionGroupID = @OptionGroupID
	AND o.MinAge <= @PetAgeInYears
	AND o.MaxAge >= @PetAgeInYears
	AND o.SpeciesID = @SpeciesID
	
	RETURN  @Deductible
 
END

GO
