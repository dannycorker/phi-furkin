SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim
-- Create date: 2008-06-26
-- Description:	Reverses a string-date for display
--              eg 2008-06-26 becomes 26-06-2008 
-- =============================================
CREATE FUNCTION [dbo].[fnDisplayDate] 
(
	@YMD varchar(10)
)
RETURNS varchar(10)
AS
BEGIN

	RETURN substring(@YMD, 9, 2) + '-' + substring(@YMD, 6, 2) + '-' + substring(@YMD, 1, 4)

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnDisplayDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnDisplayDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnDisplayDate] TO [sp_executeall]
GO
