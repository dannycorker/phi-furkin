SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2013-09-05
-- Description:	Return Fizzback survey Detailed Results for last six weeks (survey start date)
-- =============================================
CREATE FUNCTION [dbo].[fn_261_DetailedResultsSurveyStart]
(
	@Dummy varchar(30)
)
RETURNS
--DECLARE 
@TableOut TABLE 
(
	[Import Date] DATETIME,
	[Transaction Date] DATETIME, 
	[Survey Start Date] DATETIME,
	[Date First Response] DATETIME,
	[Mobile In Use] VARCHAR(30),
	[Orig Main Mobile] VARCHAR(30),
	[Orig Alternative Mobile] VARCHAR(30),
	[Store Name] VARCHAR(100),
	[Store Number] VARCHAR(50),
	Fullname VARCHAR(50),
	[Employee ID] VARCHAR(50),
	[Employee First Name] VARCHAR(100),
	[Employee Surname] VARCHAR(100),
	[Transaction Type] VARCHAR(50),
	[Network] VARCHAR(50),
	[Manufacturer] VARCHAR(50),
	[Model] VARCHAR(50),
	[Rating of Purchase Satisfaction] VARCHAR(20),
	[Revised RPS] VARCHAR(20),
	[Likelihood to Recommend] VARCHAR(20),
	[Revised LR] VARCHAR(20),
	[Comments] VARCHAR(2000),
	[Revising Agent] VARCHAR(100), 
	LeadID INT
)
AS
BEGIN

	Declare @RawData TABLE
			(
			[Mobile In Use] VARCHAR(50), 
			[Orig Main Mobile] VARCHAR(50),
			[Orig Alternative Mobile] VARCHAR(50),
			Fullname VARCHAR(50),
			LeadRef VARCHAR(50),
			CaseNum INT,
			[Start Date] DATETIME, 
			LeadID INT,
			MatterID INT,
			TableRowID INT,
			DetailFieldID INT,
			[Score] INT,
			[Revised Score] INT,
			[Comments] VARCHAR(2000),
			[Store Name] VARCHAR(100),
			[Store Number] VARCHAR(50),
			[Employee First Name] VARCHAR(100),
			[Employee Surname] VARCHAR(100),
			[Employee ID] VARCHAR(50),
			[Transaction Date] VARCHAR(50),
			[Transaction Type] VARCHAR(50),
			[Network] VARCHAR(50),
			[Manufacturer] VARCHAR(50),
			[Model] VARCHAR(50),
			[Import Date] DATETIME,
			[Survey Start Date] DATETIME,
			[Date First Response] DATETIME,
			rn INT
			)
	Declare @Agent TABLE
			(
			LeadID INT,
			UserName VARCHAR(100),
			RNO INT
			)

	INSERT INTO @RawData
	SELECT 
	c.MobileTelephone [Mobile In Use], 
	mdv11.detailvalue as [Orig Main Mobile],
	mdv5.detailvalue as [Orig Alternative Mobile],
	c.Fullname, l.LeadRef, ca.CaseNum, ca.WhenCreated as [Start Date], 
	tdv.LeadID, tdv.MatterID, tdv.TableRowID, tdv.DetailFieldID
	,tdv.ValueInt as [Score]
	,tdv4.DetailValue as [Revised Score]
	,tdv2.DetailValue as [Comments]
	, mdv.DetailValue as [Store Name],
	mdv1.DetailValue as [Store Number],
	mdv2.detailvalue as [Employee First Name],
	mdv3.detailvalue as [Employee Surname],
	mdv4.detailvalue as [Employee ID],
	mdv6.detailvalue as [Transaction Date],
	mdv7.detailvalue as [Transaction Type],
	mdv8.detailvalue as [Network],
	mdv9.detailvalue as [Manufacturer],
	mdv10.detailvalue as [Model],
	convert(datetime,mdv12.DetailValue) as [Import Date],
	lestart.whencreated as [Survey Start Date],
	tdv6.ValueDateTime as [Date First Response],
	ROW_NUMBER() OVER(PARTITION BY tdv.MatterID ORDER BY tdv.TableRowID) as rn
	FROM dbo.Customers c WITH (NOLOCK) 
	INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.CustomerID = c.CustomerID
	INNER JOIN dbo.Cases ca WITH (NOLOCK) ON ca.LeadID = l.LeadID
	INNER JOIN LeadEvent le WITH (NOLOCK) ON le.CaseID=ca.CaseID and le.EventDeleted=0 and le.EventTypeID in (128047,128048,128049,128069)
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON m.CaseID = ca.CaseID
	INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID AND (mdv.DetailFieldID = 161259) -- store name
	and mdv.DetailValue <> ''
	INNER JOIN MatterDetailValues mdv1 WITH (NOLOCK) ON mdv1.MatterID = m.MatterID AND (mdv1.DetailFieldID = 161257) -- Team Number
	INNER JOIN MatterDetailValues mdv2 WITH (NOLOCK) ON mdv2.MatterID = m.MatterID AND (mdv2.DetailFieldID = 161284) -- Employee First Name
	INNER JOIN MatterDetailValues mdv3 WITH (NOLOCK) ON mdv3.MatterID = m.MatterID AND (mdv3.DetailFieldID = 161261) -- Employee Surname
	INNER JOIN MatterDetailValues mdv4 WITH (NOLOCK) ON mdv4.MatterID = m.MatterID AND (mdv4.DetailFieldID = 161260) -- Employee ID
	INNER JOIN MatterDetailValues mdv5 WITH (NOLOCK) ON mdv5.MatterID = m.MatterID AND (mdv5.DetailFieldID = 161267) -- Alternative Mobile
	INNER JOIN MatterDetailValues mdv6 WITH (NOLOCK) ON mdv6.MatterID = m.MatterID AND (mdv6.DetailFieldID = 161256) -- Transaction Date
	INNER JOIN MatterDetailValues mdv7 WITH (NOLOCK) ON mdv7.MatterID = m.MatterID AND (mdv7.DetailFieldID = 161262) -- Transaction Type
	INNER JOIN MatterDetailValues mdv8 WITH (NOLOCK) ON mdv8.MatterID = m.MatterID AND (mdv8.DetailFieldID = 161263) -- Network
	INNER JOIN MatterDetailValues mdv9 WITH (NOLOCK) ON mdv9.MatterID = m.MatterID AND (mdv9.DetailFieldID = 161264) -- Manyfacturer
	INNER JOIN MatterDetailValues mdv10 WITH (NOLOCK) ON mdv10.MatterID = m.MatterID AND (mdv10.DetailFieldID = 161265) -- Model
	INNER JOIN MatterDetailValues mdv11 WITH (NOLOCK) ON mdv11.MatterID = m.MatterID AND (mdv11.DetailFieldID = 161266) -- Main Mobile
	INNER JOIN MatterDetailValues mdv12 WITH (NOLOCK) ON mdv12.MatterID = m.MatterID AND (mdv12.DetailFieldID = 161281) -- Import Date
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.MatterID = m.MatterID AND tdv.DetailFieldID = 161231 -- Score
	INNER JOIN dbo.TableDetailValues tdv2 with (nolock) on tdv2.TableRowID  = tdv.TableRowID and tdv2.detailfieldid = 161226 -- SMS Message Reply From Customer
	INNER JOIN dbo.TableDetailValues tdv3 with (nolock) on tdv3.TableRowID = tdv.TableRowID and tdv3.detailfieldid = 161226 and tdv3.DetailValue <> '' 
	INNER JOIN dbo.TableDetailValues tdv4 with (nolock) on tdv4.TableRowID  = tdv.TableRowID and tdv4.detailfieldid = 161243 -- revised score
	INNER JOIN dbo.TableDetailValues tdv7 WITH (NOLOCK) ON tdv7.TableRowID  = tdv.TableRowID AND tdv7.DetailFieldID = 161224 and tdv7.ValueInt<>17 -- exclude any replies to the pre-amble question
	---- first response date
	INNER JOIN LeadEvent lestart WITH (NOLOCK) ON lestart.CaseID=ca.CaseID and lestart.EventDeleted=0 and lestart.EventTypeID in (128041)
	INNER JOIN dbo.TableDetailValues tdv5 WITH (NOLOCK) ON tdv5.MatterID = m.MatterID AND tdv5.DetailFieldID = 161224 and tdv5.ValueInt=18 -- SMS QID
	INNER JOIN dbo.TableDetailValues tdv6 with (nolock) on tdv6.TableRowID  = tdv5.TableRowID and tdv6.detailfieldid = 161227 -- Date SMS Reply Rcvd
	WHERE c.ClientID = 261 and c.Test = 0 
	and lestart.WhenCreated > DATEADD(week,-6,dbo.fn_GetDate_Local())
	--and lestart.WhenCreated > DATEADD (MM, -1, [dbo].[fnGetStartorEndOfMonth] (0))
	--AND tdv.DetailValue > ''
	AND NOT EXISTS  
	(SELECT * FROM dbo.LeadEvent Sub_LeadEvent WITH (NOLOCK) WHERE Sub_LeadEvent.CaseID = le.CaseID AND Sub_LeadEvent.EventTypeID IN (128047,128048,128049,128069) AND Sub_LeadEvent.LeadEventID > le.LeadEventID AND Sub_LeadEvent.ClientID = 261 AND Sub_LeadEvent.EventDeleted = 0)
	AND NOT EXISTS  
	(SELECT * FROM dbo.LeadEvent Sub_lestart WITH (NOLOCK) WHERE Sub_lestart.CaseID = le.CaseID AND Sub_lestart.EventTypeID IN (128041) AND Sub_lestart.LeadEventID > lestart.LeadEventID AND Sub_lestart.ClientID = 261 AND Sub_lestart.EventDeleted = 0)
	AND NOT EXISTS  
	(SELECT * FROM dbo.TableDetailValues sub_tdv5 WITH (NOLOCK) where sub_tdv5.MatterID = m.MatterID AND sub_tdv5.DetailFieldID = 161224 and sub_tdv5.ValueInt=18 and sub_tdv5.TableRowID > tdv5.TableRowID)

	INSERT INTO @Agent
	SELECT le.LeadID,cp.UserName
	,ROW_NUMBER() OVER(PARTITION BY le.caseid ORDER BY le.whencreated DESC) as [RNO] 
	FROM LeadEvent le WITH (NOLOCK) 
	INNER JOIN clientpersonnel cp WITH (NOLOCK) ON le.whocreated=cp.clientpersonnelid
	WHERE le.EventTypeID IN (128033,128035,128039,128036)
	and cp.ClientPersonnelID<>23396
	and le.WhenCreated > DATEADD(week,-6,dbo.fn_GetDate_Local())
	AND EXISTS  
	(SELECT * FROM Cases carev WITH (NOLOCK) 
	INNER JOIN Matter m WITH (NOLOCK) ON carev.CaseID=m.CaseID
	INNER JOIN TableRows tr WITH (NOLOCK) ON m.MatterID=tr.MatterID and tr.DetailFieldID=161232
	INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tr.TableRowID=tdv.TableRowID and tdv.DetailFieldID=161243
		and tdv.DetailValue <> '' and tdv.DetailValue is not null
	WHERE carev.CaseID=le.CaseID)

	INSERT INTO @TableOut 
	SELECT i.[Import Date] as [Import Date],i.[Transaction Date] as [Transaction Date], 
	i.[Survey Start Date] as [Survey Start Date],
	i.[Date First Response] as [Date First Response],
	i.[Mobile In Use],i.[Orig Main Mobile],i.[Orig Alternative Mobile], i.[Store Name] as [Store Name], i.[Store Number] as [Store Number], i.Fullname, i.[Employee ID] as [Employee Number], i.[Employee First Name] as [Employee First Name], i.[Employee Surname] as [Employee Surname], i.[Transaction Type] as [Transaction Type], i.Network as [Network], i.Manufacturer as [Manufacturer], i.Model as [Model],
	MAX (CASE rn WHEN 1 THEN i.Score ELSE '' END) AS [Rating of Purchase Satisfaction],
	MAX (CASE rn WHEN 1 THEN i.[Revised Score] ELSE '' END) AS [Revised RPS],
	MAX (CASE rn WHEN 2 THEN i.Score ELSE '' END) AS [Likelihood to Recommend],
	MAX (CASE rn WHEN 2 THEN i.[Revised Score] ELSE '' END) AS [Revised LR],
	MAX(CASE rn WHEN 3 THEN i.[Comments] ELSE '' END) AS [Comments],
	isnull(a.UserName,'') as [Revising Agent], i.LeadID
	FROM @RawData i
	LEFT JOIN @Agent a ON i.LeadID=a.LeadID and a.RNO=1 
	GROUP BY i.[Import Date],i.[Transaction Date],i.[Survey Start Date], i.[Date First Response],
	i.[Mobile In Use],i.[Orig Main Mobile] , i.[Store Name] , i.Fullname, i.[Employee ID] , i.[Employee First Name] , i.[Employee Surname] , i.[Transaction Type] , i.Network , i.Manufacturer , i.Model, i.[Orig Alternative Mobile],i.[Store Number],a.UserName, i.LeadID
	ORDER BY i.[Transaction Date] asc
	
	--SELECT * FROM @TableOut

	RETURN
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_261_DetailedResultsSurveyStart] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_261_DetailedResultsSurveyStart] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_261_DetailedResultsSurveyStart] TO [sp_executeall]
GO
