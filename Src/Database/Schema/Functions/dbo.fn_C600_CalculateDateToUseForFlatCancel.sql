SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2021-03-10
-- Description:	FURKIN-389
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_CalculateDateToUseForFlatCancel]
(
	@MatterID INT
)
RETURNS VARCHAR(10)
AS
BEGIN

	DECLARE @Output VARCHAR(10),
			@PurchasedProductID INT,
			@PaidPremium DECIMAL(18,2),
			@DailyPremiumValue DECIMAL(18,6),
			@Days DECIMAL(18,2),
			@StartDate DATE,
			@ClientID INT = [dbo].[fnGetPrimaryClientID](),
			@Locale VARCHAR(5)
			
	-- Localisation

	IF @ClientID = 607
	BEGIN
		SELECT @Locale = 'en-US' /*United States of America*/ /*Canada*/
	END
	ELSE
	BEGIN
		SELECT @Locale = 'en-GB' /*Great Britain / Australia*/ 
	END



	-- Get PurchasedProductID

	SELECT @PurchasedProductID = pp.PurchasedProductID
	FROM PurchasedProduct pp WITH (NOLOCK)
	WHERE pp.ObjectID = @MatterID

	-- Get paid premium value
	SELECT @PaidPremium = SUM(ppps.PaymentGross)
	FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
	WHERE ppps.PurchasedProductID = @PurchasedProductID
	AND ppps.PaymentStatusID IN (6) -- Paid

	-- Calculate daily premium value for full annual
	SELECT @DailyPremiumValue  = SUM(pp.ProductCostGross) / 365
	FROM PurchasedProduct pp WITH (NOLOCK) 
	WHERE pp.PurchasedProductID = @PurchasedProductID

	-- Premium paid / Daily Premium = Days

	SELECT @Days = @PaidPremium / @DailyPremiumValue

	-- Start Date + Days = Date to use

	SELECT @StartDate = dbo.fnGetSimpleDvAsDate(170035,@MatterID)

	SELECT @Output = CONVERT(VARCHAR,FORMAT(DATEADD(DAY,ISNULL(@Days,0),@StartDate), 'd', @Locale),103)

	RETURN @Output

END
GO
