SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Dvae Morgan
-- Create date: 2013-10-31
-- Description:	Take a table field, column field and match value and return the first matching row for this object
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_SumMatchTableRowNumberByColumnFieldMatchValueAndObjectIDAndType]
(
	@TableDetailFieldID		INT,
	@ColumnDetailFieldID	INT,
	@SumColumnDetailFieldID INT,
	@ObjectID				INT,
	@ObjectTypeID			INT,
	@MatchValue				VARCHAR(2000),
	@SearchAscending		BIT
)
RETURNS Numeric(18,2)
AS
BEGIN
	
	DECLARE @ReturnValue			Numeric(18,2) = 0
	
	;With NumberedValues as 
	(	
		SELECT CASE df.QuestionTypeID WHEN 4 THEN luli.ItemValue ELSE tdv.DetailValue END as DetailValue, ROW_NUMBER() OVER (ORDER BY CASE @SearchAscending WHEN 1 THEN tr.TableRowID ELSE tr.TableRowID * -1 END ASC) [Row],
		tsum.ValueMoney [Total]
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = tdv.DetailFieldID
		LEFT JOIN LookupListItems luli WITH (NOLOCK) ON df.LookupListID = luli.LookupListID and luli.LookupListItemID = tdv.ValueInt
		INNER JOIN TableDetailValues tsum WITH (NOLOCK) ON tsum.TableRowID=tr.TableRowID and tsum.DetailFieldID=@SumColumnDetailFieldID
		WHERE tr.DetailFieldID = @TableDetailFieldID
		and tdv.DetailFieldID  = @ColumnDetailFieldID
		and (
			@ObjectTypeID = 1  AND tr.LeadID			= @ObjectID
		OR	@ObjectTypeID = 2  AND tr.MatterID			= @ObjectID
		OR	@ObjectTypeID = 11 AND tr.CaseID			= @ObjectID
		OR	@ObjectTypeID = 10 AND tr.CustomerID		= @ObjectID
		OR	@ObjectTypeID = 13 AND tr.ClientPersonnelID = @ObjectID
		OR	@ObjectTypeID = 12 AND tr.CLientID			= @ObjectID
			)
	)
	SELECT @ReturnValue = sum(nv.Total)
	FROM NumberedValues nv
	WHERE nv.DetailValue = @MatchValue
	
	RETURN @ReturnValue
END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumMatchTableRowNumberByColumnFieldMatchValueAndObjectIDAndType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_SumMatchTableRowNumberByColumnFieldMatchValueAndObjectIDAndType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumMatchTableRowNumberByColumnFieldMatchValueAndObjectIDAndType] TO [sp_executeall]
GO
