SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ben Crinion
-- Create date: 28-May-2008
-- Description:	Add the provided number of provided time units to the provided date for following up events.
-- =============================================
CREATE FUNCTION [dbo].[fnAddTimeUnitsToDate]
(
	@Date				datetime,
	@TimeUnitsID		int,
	@TimeUnitsQuantity	int
)
RETURNS datetime
AS
BEGIN

DECLARE @Return as datetime 

--Time Units Table
--1	MilliSeconds	ms
--2	Seconds			s
--3	Minutes			mi
--4	Hours			hh
--5	Days			d
--6	Weeks			wk
--7	Months			m
--8	Years			yy

select @Return = CASE @TimeUnitsID
	WHEN 1 THEN DATEADD(ms, @TimeUnitsQuantity, @Date)
	WHEN 2 THEN DATEADD(s, @TimeUnitsQuantity, @Date)
	WHEN 3 THEN DATEADD(mi, @TimeUnitsQuantity, @Date)
	WHEN 4 THEN DATEADD(hh, @TimeUnitsQuantity, @Date)
	WHEN 5 THEN DATEADD(d, @TimeUnitsQuantity, @Date)
	WHEN 6 THEN DATEADD(wk, @TimeUnitsQuantity, @Date)
	WHEN 7 THEN DATEADD(m, @TimeUnitsQuantity, @Date)
	WHEN 8 THEN DATEADD(yy, @TimeUnitsQuantity, @Date)
END


	RETURN @Return

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnAddTimeUnitsToDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnAddTimeUnitsToDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnAddTimeUnitsToDate] TO [sp_executeall]
GO
