SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2013-06-12
-- Description:	Count blanks in a particular column in a table field
-- =============================================
CREATE FUNCTION [dbo].[fnCountBlankRowsByTableFieldColumnFieldAndObjectID]
(
	@ObjectID				int,
	@DetailFieldID			int,
	@ColumnDetailFieldID	int
)
RETURNS INT 
AS
BEGIN

	DECLARE	@LeadOrMatter int,
			@ReturnValue int = 0
	
	SELECT @LeadOrMatter = df.LeadOrMatter
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @DetailFieldID
	
	IF @LeadOrMatter = 1
	BEGIN
		SELECT @ReturnValue = COUNT(tr.TableRowID)
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
		WHERE	tr.DetailFieldID = @DetailFieldID
		and		tdv.DetailFieldID = @ColumnDetailFieldID
		and		( tdv.DetailValue = '' or tdv.DetailValue is null )
		and		tr.LeadID = @ObjectID
	END
	ELSE IF @LeadOrMatter = 2
	BEGIN
		SELECT @ReturnValue = COUNT(tr.TableRowID)
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
		WHERE	tr.DetailFieldID = @DetailFieldID
		and		tdv.DetailFieldID = @ColumnDetailFieldID
		and		( tdv.DetailValue = '' or tdv.DetailValue is null )
		and		tr.MatterID = @ObjectID
	END
	ELSE IF @LeadOrMatter = 10
	BEGIN
		SELECT @ReturnValue = COUNT(tr.TableRowID)
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
		WHERE	tr.DetailFieldID = @DetailFieldID
		and		tdv.DetailFieldID = @ColumnDetailFieldID
		and		( tdv.DetailValue = '' or tdv.DetailValue is null )
		and		tr.CustomerID = @ObjectID
	END
	ELSE IF @LeadOrMatter = 11
	BEGIN
		SELECT @ReturnValue = COUNT(tr.TableRowID)
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
		WHERE	tr.DetailFieldID = @DetailFieldID
		and		tdv.DetailFieldID = @ColumnDetailFieldID
		and		( tdv.DetailValue = '' or tdv.DetailValue is null )
		and		tr.CaseID = @ObjectID
	END	

	RETURN @ReturnValue
END






GO
GRANT VIEW DEFINITION ON  [dbo].[fnCountBlankRowsByTableFieldColumnFieldAndObjectID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnCountBlankRowsByTableFieldColumnFieldAndObjectID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnCountBlankRowsByTableFieldColumnFieldAndObjectID] TO [sp_executeall]
GO
