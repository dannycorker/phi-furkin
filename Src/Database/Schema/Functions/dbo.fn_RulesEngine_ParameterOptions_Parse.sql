SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-07-26
-- =============================================
CREATE FUNCTION [dbo].[fn_RulesEngine_ParameterOptions_Parse]( 
    @ClientID INT,
	@UserID INT,
	@RuleParameterID INT,
	@ImportXml XML
) 
RETURNS TABLE 
AS RETURN 
(
	WITH DataSet AS
	(
		SELECT	ISNULL(T.c.value('col0[1]','VARCHAR(250)'),'') AS Operator,
				ISNULL(T.c.value('col1[1]','VARCHAR(250)'),'') AS Value1,
				ISNULL(T.c.value('col2[1]','VARCHAR(250)'),'') AS Value2,
				ISNULL(T.c.value('col3[1]','VARCHAR(250)'),'') AS ID,
				ROW_NUMBER() OVER(ORDER BY T.c) RowID
		FROM	@ImportXml.nodes('/table/row') T(c)
	)
		SELECT	
			row.Operator,
			row.Value1,
			row.Value2,
			ll.LookupListItemID LookupListItemID1,
			ll2.LookupListItemID LookupListItemID2,
			row.ID,
			row.RowID,
			op.OperatorID,
			po.ParameterOptionID,
			@RuleParameterID RuleParameterID,
			CASE
				WHEN op.OperatorID IS NULL THEN 'operator not found'
				WHEN row.Value1 IS NULL THEN 'value 1 required'
				WHEN op.IncludeVal2 = 1 AND row.Value2 IS NULL THEN 'value 2 required'
				WHEN op.OperatorID <> 10 AND rp.DataTypeID = 6 AND ll.ItemValue IS NULL THEN 'value 1 no lookup match'
				WHEN op.OperatorID <> 10 AND op.IncludeVal2 = 1 AND rp.DataTypeID = 6 AND ll2.ItemValue IS NULL THEN 'value 2 no lookup match'
				ELSE ''
			END Error
	FROM	DataSet row
	LEFT JOIN RulesEngine_Operators op WITH (NOLOCK) ON row.Operator = op.Name
	LEFT JOIN RulesEngine_RuleParameters rp WITH (NOLOCK) ON rp.RuleParameterID = @RuleParameterID
	LEFT JOIN RulesEngine_ParameterOptions po WITH (NOLOCK) ON row.ID = CAST(po.ParameterOptionID AS VARCHAR) AND po.RuleParameterID = @RuleParameterID
	LEFT JOIN dbo.DetailFields df WITH (NOLOCK) ON CAST(df.DetailFieldID AS VARCHAR) = rp.Value AND rp.DataTypeID = 6
	LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON df.LookupListID = ll.LookupListID AND row.Value1 = ll.ItemValue
	LEFT JOIN dbo.LookupListItems ll2 WITH (NOLOCK) ON df.LookupListID = ll2.LookupListID AND row.Value2 = ll2.ItemValue
)
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_ParameterOptions_Parse] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_RulesEngine_ParameterOptions_Parse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_ParameterOptions_Parse] TO [sp_executeall]
GO
