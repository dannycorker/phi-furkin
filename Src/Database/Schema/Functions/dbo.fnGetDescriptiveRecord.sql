SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 06/03/2020
-- Description:	AU Domestic Payments File Format, Get Descriptive Record:
--				The Descriptive Record contains AU Domestic Payment batch header details.
--				ABA files are required to be prepared as 120 byte fixed length records.
-- =============================================
CREATE FUNCTION [dbo].[fnGetDescriptiveRecord]
(	
	@BSB VARCHAR(7),								-- Bank/State/Branch number e.g. 013-999. 2 8 7 Mandatory
	@Account VARCHAR(9),							-- Funds account number. 9 17 9 Mandatory
	@NameOfUserFinancialInstitution VARCHAR(3),		-- Must contain the bank mnemonic that is associated with the BSB of the funds account. e.g. ‘ANZ’. 21 23 3 Mandatory	
	@NameOfUserSupplyingFile VARCHAR(26),			-- User Preferred Name as registered with ANZ 31 56 26 Mandatory
	@UserIdentificationNumber VARCHAR(6),			-- Direct Entry User ID. Right-justified, zero-filled. 57 62 6 Mandatory
	@DescriptionOfEntriesOnFile VARCHAR(12),		-- Description of payments in the file (e.g. Payroll, Creditors etc.). 63 74 12 Mandatory
	@DateToBeProcessed VARCHAR(6),					-- Date on which the payment is to be processed. DDMMYY (e.g. 010111). 75 80 6 Mandatory
	@TimeToBeProcessed VARCHAR(4)					-- Time Alpha Time on which the payment is to be processed. 24 hour format - HHmm.  If blank or spaces, process now. 81 84 4 Optional	
)
RETURNS VARCHAR(120)
AS
BEGIN
	
	DECLARE @RecordType VARCHAR(1) = '0'						-- Must be ‘0’. 1 1 1 Mandatory
	DECLARE @ReservedOne VARCHAR(1) = ' '						-- Blank filled. 18 18 1 Optional
	DECLARE @SequenceNumber VARCHAR(2) = '01'					-- Must be ‘01’. 19 20 2 Mandatory
	DECLARE @ReservedTwo VARCHAR(7) = REPLICATE(' ', 7)			-- Blank filled. 24 30 7 Optional
	DECLARE @ReservedThree VARCHAR(36) = REPLICATE(' ', 36)		-- Blank filled. 85 120 36 Optional

	DECLARE @Record VARCHAR(120)

	SELECT @Record = @RecordType + @BSB + dbo.fnPadRight(@ACCOUNT, ' ', 9) + @ReservedOne + @SequenceNumber + @NameOfUserFinancialInstitution + @ReservedTwo + dbo.fnPadRight(@NameOfUserSupplyingFile, ' ', 26)
					+ dbo.fnPadLeft(@UserIdentificationNumber, '0', 6) + dbo.fnPadRight(@DescriptionOfEntriesOnFile, ' ', 12) + @DateToBeProcessed + @TimeToBeProcessed + @ReservedThree

	return @Record

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDescriptiveRecord] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetDescriptiveRecord] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDescriptiveRecord] TO [sp_executeall]
GO
