SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-09-22
-- Description:	Find customer based on Matter DetailValues
-- =============================================
CREATE FUNCTION [dbo].[_C00_FindCustomerByDetailValues]
(
	@DetailFieldID1 INT,
	@DetailFieldID2 INT,
	@DetailFieldID3 INT,
	@DetailValue1 VARCHAR(2000),
	@DetailValue2 VARCHAR(2000),
	@DetailValue3 VARCHAR(2000)
)
RETURNS INT
AS
BEGIN
	
	DECLARE @CustomerID INT
	
	IF @DetailValue1 = '0' SELECT @DetailValue1 = ''
	IF @DetailValue2 = '0' SELECT @DetailValue2 = ''
	IF @DetailValue3 = '0' SELECT @DetailValue3 = ''

	SELECT TOP (1) @CustomerID = m.CustomerID
	FROM Matter m WITH (NOLOCK)
	INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = m.CustomerID AND c.Test = 0
	INNER JOIN MatterDetailValues mdv_phone WITH (NOLOCK) ON mdv_phone.MatterID = m.MatterID AND mdv_phone.DetailFieldID IN (@DetailFieldID1,@DetailFieldID2,@DetailFieldID3)
	WHERE mdv_phone.DetailValue IN (LTRIM(RTRIM(@DetailValue1)),LTRIM(RTRIM(@DetailValue2)),LTRIM(RTRIM(@DetailValue3)))
	AND mdv_phone.DetailValue <> ''

	RETURN @CustomerID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FindCustomerByDetailValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_FindCustomerByDetailValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FindCustomerByDetailValues] TO [sp_executeall]
GO
