SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim
-- Create date: 2007-08-05
-- Description:	Returns 1 if the date passed in is a Bank Holiday
-- =============================================
CREATE FUNCTION [dbo].[fnIsBankHoliday] 
(
	-- Add the parameters for the function here
	@yourdate datetime
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result bit

	DECLARE @Year int, @Month int, @Day int

	SELECT @Year = year(@yourdate), @Month = month(@yourdate), @Day = day(@yourdate)

	SELECT @Result = IsBankHoliday
	FROM WorkingDays
	WHERE [Year] = @Year
	AND [Month] = @Month
	AND [Day] = @Day

	-- Return the result of the function
	RETURN @Result

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsBankHoliday] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnIsBankHoliday] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsBankHoliday] TO [sp_executeall]
GO
