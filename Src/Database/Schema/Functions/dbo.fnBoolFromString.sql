SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2010-04-29
-- Description:	Return 1, 0 from strings like "Yes", "False", "".
--              Useful for the RPI XML generation, which demands consistent
--              "1" and "0" entries rather than Yes/No, True/False.
--              Same as fnBoolFromStringN but replaces NULL with 0.
-- =============================================
CREATE FUNCTION [dbo].[fnBoolFromString]
(
	@Instr varchar(2000)
)
RETURNS bit
AS
BEGIN

	/* Return value is 0 (=false) by default */
	DECLARE @bool bit = 0

	/* Choose 1 or 0 if possible */
	SELECT @bool = CASE @instr 
					WHEN '1' THEN 1
					WHEN 'Y' THEN 1
					WHEN 'YES' THEN 1
					WHEN 'TRUE' THEN 1
					WHEN '0' THEN 0
					WHEN 'N' THEN 0
					WHEN 'NO' THEN 0
					WHEN 'FALSE' THEN 0
					ELSE 0
					END 
	
	RETURN @bool

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnBoolFromString] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnBoolFromString] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnBoolFromString] TO [sp_executeall]
GO
