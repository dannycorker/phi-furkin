SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-03-27
-- Description:	Creates an RTF bullet list
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_RTFBulletList]
(
	@Items dbo.tvpVarchar READONLY
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE @RTF VARCHAR(MAX) = ''
	
	DECLARE @Count INT
	SELECT @Count = COUNT(*) 
	FROM @Items
	
	IF @Count > 0
	BEGIN
	
		DECLARE @Data TABLE
		(
			ID INT IDENTITY,
			Value VARCHAR(MAX)
		)
		
		INSERT @Data (Value)
		SELECT AnyValue
		FROM @Items
		
		SELECT @RTF += '\fi-360\li720\wrapdefault\ls1\ls7\contextualspace'
		
		SELECT @RTF +=	CASE ID
							WHEN 1 THEN '{' + Value
							ELSE '\par {\listtext\tab}' + Value
						END
		FROM @Data	
		
		SELECT @RTF += '\par }'
	
	END
	
	RETURN @RTF

END






GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_RTFBulletList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_RTFBulletList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_RTFBulletList] TO [sp_executeall]
GO
