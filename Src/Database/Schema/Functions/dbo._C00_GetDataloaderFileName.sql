SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-08-11
-- Description: Return a value from a table, specify 2 columns with matching conditions
-- =============================================
CREATE FUNCTION [dbo].[_C00_GetDataloaderFileName]
(
	@DataloaderFileID INT
)
RETURNS varchar(2000)
AS
BEGIN

DECLARE @Answer Varchar(2000)

	SELECT @Answer = isnull(DF.TargetFileName,'Failed') FROM  DataLoaderFile df WITH (NOLOCK) 
	WHERE df.DataLoaderFileID=@DataloaderFileID

	RETURN @Answer

END

GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetDataloaderFileName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetDataloaderFileName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetDataloaderFileName] TO [sp_executeall]
GO
