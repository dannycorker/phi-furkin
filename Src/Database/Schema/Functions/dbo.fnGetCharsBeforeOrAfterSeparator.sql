SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2011-06-16
-- Description:	Get all characters before or after something like a comma or full stop
-- =============================================
CREATE FUNCTION [dbo].[fnGetCharsBeforeOrAfterSeparator]
(
	@Input VARCHAR(MAX), 
	@Separator VARCHAR(50), 
	@UseFirstSeparatorRatherThanLast BIT, 
	@GetCharsBeforeRatherThanAfter BIT 
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	
	DECLARE @Output VARCHAR(MAX),
	@CharIndex INT, 
	@InputLength INT,
	@SeparatorLength INT
	
	SELECT @InputLength = LEN(@Input),
	@SeparatorLength = CASE WHEN @Separator = ' ' THEN 1 ELSE LEN(@Separator) END, /* LEN and varchar combine to ignore "whitespace" after the last "real" character  */
	@Output = @Input /* Default the output to be the input, in case no separators are found */
	
	IF @InputLength > 0 AND @SeparatorLength > 0 
	BEGIN
	
		/* Find the first or last occurrence of the separator char(s) as appropriate */
		IF @UseFirstSeparatorRatherThanLast = 1
		BEGIN
		
			/* Find the first hit */
			SELECT @CharIndex = CHARINDEX(@Separator, @Input)
			
			IF @CharIndex > 0 BEGIN
				/* Get the left/right text from that separator as requested */
				IF @GetCharsBeforeRatherThanAfter = 1
				BEGIN
					SELECT @Output = LEFT(@Input, @CharIndex - 1)
				END
				ELSE
				BEGIN
					/* 
						@Input = 'This.is..quite.a..silly.string' 
						@InputLength = 30
						@CharIndex = 8
						@SeparatorLength = 2
						@Output = 'quite.a..silly.string'
						which is 21 chars long, ie RIGHT(30 - 8 - 2 + 1)
					*/
					SELECT @Output = RIGHT(@Input, @InputLength - @CharIndex - @SeparatorLength + 1)
				END
			END
			
		END
		ELSE
		BEGIN
		
			/* Find the last hit by reversing the input string */
			
			/* Select the chars to keep */
			IF @GetCharsBeforeRatherThanAfter = 1
			BEGIN
				/* 
					@Input = 'This.is...quite.a...silly.string' 
					@InputLength = 32
					@CharIndex (reading backwards) = 13
					so
					@CharIndex (reading forwards) = 18 (32 - 13 - 3 + 1 + 1)
					which comes from (length) - (start of separator reading backwards) - (length of separator) + (give one char back for the end of the separator) 
				*/
				SELECT @CharIndex = @InputLength - CHARINDEX(REVERSE(@Separator), REVERSE(@Input)) - @SeparatorLength + 1
				IF @CharIndex > 0 BEGIN
					SELECT @Output = LEFT(@Input, @CharIndex)
				END
			END
			ELSE
			BEGIN
				/* 
					@Input = 'Finally, just remove the first word in this sentence' 
					@InputLength = 52
					@Separator = ' '
					@SeparatorLength = 1
					@CharIndex (reading backwards) = 13
					@Output = 'silly.string'
					which is 12 chars long, ie RIGHT(13 - 1)
				*/
				SELECT @CharIndex = CHARINDEX(REVERSE(@Separator), REVERSE(@Input)) - 1
				IF @CharIndex > 0 BEGIN
					SELECT @Output = RIGHT(@Input, @CharIndex)
				END
			END
			
		END
		
	END

	RETURN @Output

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCharsBeforeOrAfterSeparator] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetCharsBeforeOrAfterSeparator] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCharsBeforeOrAfterSeparator] TO [sp_executeall]
GO
