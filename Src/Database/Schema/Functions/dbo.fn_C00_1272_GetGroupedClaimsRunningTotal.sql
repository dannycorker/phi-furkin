SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:			Robin Hall
-- Create date:		2012-11-30
-- Description:		Returns the total settled on all linked claims
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetGroupedClaimsRunningTotal]
(
      @MatterID INT
)
RETURNS MONEY     
AS
BEGIN

DECLARE @RunningTotal MONEY = 0

;WITH ClaimLinkCTE AS
(
SELECT tdvMatter.ValueInt ChildMatterID, ISNULL(tdvParent.ValueInt,tdvMatter.ValueInt) ParentMatterID
FROM TableDetailValues tdvMatter WITH (NOLOCK)
INNER JOIN dbo.TableDetailValues tdvParent WITH (NOLOCK) ON tdvParent.TableRowID = tdvMatter.TableRowID AND tdvParent.DetailFieldID = 146356 --ParentMatter
WHERE tdvMatter.DetailFieldID = 146355 --ChildMatter
)

SELECT @RunningTotal = SUM(tdvSettleAmount.ValueMoney)
FROM Matter m WITH (NOLOCK)
INNER JOIN ClaimLinkCTE thisMatter ON thisMatter.ChildMatterID = m.MatterID
INNER JOIN ClaimLinkCTE mySiblings ON mySiblings.ParentMatterID = thisMatter.ParentMatterID
INNER JOIN TableDetailValues tdvSettleAmount WITH (NOLOCK) ON tdvSettleAmount.MatterID = mySiblings.ChildMatterID AND tdvSettleAmount.DetailFieldID = 144352
INNER JOIN dbo.matterdetailvalues mdvApproved WITH (NOLOCK) on mdvApproved.MatterID = mysiblings.ChildMatterID and mdvApproved.DetailFieldID = 144524
WHERE m.MatterID = @MatterID
and mdvApproved.ValueDate is not null
      
RETURN @RunningTotal

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetGroupedClaimsRunningTotal] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_GetGroupedClaimsRunningTotal] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetGroupedClaimsRunningTotal] TO [sp_executeall]
GO
