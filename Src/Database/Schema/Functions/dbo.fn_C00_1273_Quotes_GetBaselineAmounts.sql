SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-12-03
-- Description:	Returns the base line amounts for all the active policies
-- Modified DCM 2014-09-16 Copied from 235
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_Quotes_GetBaselineAmounts]
(
	@CalculationDate DATE,
	@BreedRatingRLID INT,
	@PostalCode VARCHAR(100),
	@SchemeMatterID INT,
	@BrandRLID INT ,
	@LanguageID INT
)								
RETURNS 
@BaselineAmounts TABLE
(
	MatterID INT,
	ProductID INT, 
	ProductName VARCHAR(2000), 
	BaselinePremium MONEY, 
	EnrolmentFee MONEY, 
	MaxMarketingDiscount MONEY,
	PetTypeUplift MONEY, 
	BreedUplift MONEY, 
	PostalCodeUplift MONEY,
	SubTotalBaselinePremium MONEY
)
AS
BEGIN
	
	DECLARE @PetTypeID INT,
			@RatingID INT,
			@PostalCodeID INT,
			@PetTypeBaseID INT,
			@PetTerritoryID INT

	-- ALL TODO. Function temporarily frigged to return fixed values
	INSERT @BaselineAmounts (MatterID, ProductID, ProductName, BaselinePremium, EnrolmentFee, MaxMarketingDiscount, PetTypeUplift, BreedUplift, PostalCodeUplift, SubTotalBaselinePremium) VALUES
	(50009067, 123, 'Product Name',	100, 10, 20, 0,0,0, 80)
				
	---- Find the pet type and rating on the breed resource list
	--SELECT @PetTypeID = rdvPetType.ValueInt, @RatingID = rdvRating.ValueInt
	--FROM dbo.ResourceList r WITH (NOLOCK) 
	--INNER JOIN dbo.ResourceListDetailValues rdvPetType WITH (NOLOCK) ON r.ResourceListID = rdvPetType.ResourceListID AND rdvPetType.DetailFieldID = 153537
	--INNER JOIN dbo.ResourceListDetailValues rdvRating WITH (NOLOCK) ON r.ResourceListID = rdvRating.ResourceListID AND rdvRating.DetailFieldID = 153539
	--WHERE r.ResourceListID = @BreedRatingRLID
	
	---- Match against the longest partial postal code... the most specific
	--;WITH Postcodes AS 
	--(
	--	SELECT rdvRating.ValueInt AS PostalCodeID, 
	--	ROW_NUMBER() OVER(ORDER BY LEN(rdvPostalCode.DetailValue) DESC) as rn 
	--	FROM dbo.ResourceList r WITH (NOLOCK) 
	--	INNER JOIN dbo.ResourceListDetailValues rdvPostalCode WITH (NOLOCK) ON r.ResourceListID = rdvPostalCode.ResourceListID AND rdvPostalCode.DetailFieldID = 153540
	--	INNER JOIN dbo.ResourceListDetailValues rdvRating WITH (NOLOCK) ON r.ResourceListID = rdvRating.ResourceListID AND rdvRating.DetailFieldID = 153541
	--	WHERE @PostalCode LIKE rdvPostalCode.DetailValue + '%'
	--)
	--SELECT @PostalCodeID = PostalCodeID
	--FROM Postcodes
	--WHERE rn = 1 
	
	---- Match to the correct detail field IDs for pet type, breed and postal code
	
	--SELECT @PetTypeBaseID =	CASE @PetTypeID
	--							WHEN 57238 THEN 154448 -- Cat
	--							WHEN 57239 THEN 154442 -- Dog
	--						END	
							
	--SELECT @PetTerritoryID = CASE @RatingID
	--							WHEN 57240 THEN -- C1
	--								CASE @PostalCodeID
	--									WHEN 57248 THEN 154465 -- A
	--									WHEN 57249 THEN 163132 -- B
	--									WHEN 57250 THEN 163133 -- C
	--								END 
	--							WHEN 57241 THEN -- C2
	--								CASE @PostalCodeID
	--									WHEN 57248 THEN 155998 -- A
	--									WHEN 57249 THEN 163134 -- B
	--									WHEN 57250 THEN 154466 -- C
	--								END 
	--							--WHEN 57242 THEN 154452 -- C3
	--							--WHEN 57243 THEN 154453 -- C4
	--							WHEN 57244 THEN -- D1
	--								CASE @PostalCodeID
	--									WHEN 57248 THEN 154449 -- A
	--									WHEN 57249 THEN 154450 -- B
	--									WHEN 57250 THEN 154451 -- C
	--								END 
	--							WHEN 57245 THEN -- D2
	--								CASE @PostalCodeID
	--									WHEN 57248 THEN 154452 -- A
	--									WHEN 57249 THEN 154453 -- B
	--									WHEN 57250 THEN 154454 -- C
	--								END 
	--							WHEN 57246 THEN -- D3
	--								CASE @PostalCodeID
	--									WHEN 57248 THEN 154455 -- A
	--									WHEN 57249 THEN 154456 -- B
	--									WHEN 57250 THEN 154457 -- C
	--								END 
	--							WHEN 57247 THEN -- D4
	--								CASE @PostalCodeID
	--									WHEN 57248 THEN 154458 -- A
	--									WHEN 57249 THEN 154459 -- B
	--									WHEN 57250 THEN 154460 -- C
	--								END 
	--						END		
								
		
	--INSERT @BaselineAmounts (MatterID, ProductID, ProductName, BaselinePremium, EnrolmentFee, MaxMarketingDiscount, PetTypeUplift, BreedUplift, PostalCodeUplift, SubTotalBaselinePremium)
	--SELECT	m.MatterID, mdvScheme.ValueInt AS ProductID, ISNULL(t.Translation, rdvProduct.DetailValue) AS ProductName, 
	--		mdvBaseLine.ValueMoney AS BaselinePremium, ldvEnrolment.ValueMoney AS EnrolmentFee, mdvMaxDiscount.ValueMoney AS MaxMarketingDiscount, 
	--		/* @Taxes AS Taxes, */
	--		0 AS PetTypeUplift, 
	--		0 AS BreedUplift, 
	--		0 AS PostalCodeUplift, 
	--		CASE 
	--			WHEN mdvPetTerritory.ValueMoney > 0 AND mdvLossRatio.ValueMoney > 0
	--				THEN CAST((ISNULL(mdvPetTerritory.ValueMoney, 1) * (mdvBaseline.ValueMoney * 100) / ISNULL(mdvLossRatio.ValueMoney, 100)) AS DECIMAL(18, 2))
	--			WHEN mdvLossRatio.ValueMoney > 0 
	--				THEN CAST((mdvBaseline.ValueMoney * 100) / ISNULL(mdvLossRatio.ValueMoney, 100) AS DECIMAL(18, 2))
	--			ELSE CAST((mdvBaseline.ValueMoney * 100) / 100 AS DECIMAL(18, 2))
	--		END AS SubTotalBaselinePremium
	--FROM dbo.Matter m WITH (NOLOCK) 
	--INNER JOIN dbo.Lead l WITH (NOLOCK) ON m.LeadID = l.LeadID
	--INNER JOIN dbo.MatterDetailValues mdvStart WITH (NOLOCK) ON m.MatterID = mdvStart.MatterID AND mdvStart.DetailFieldID = 154439 AND mdvStart.ValueDate <= @CalculationDate
	--INNER JOIN dbo.MatterDetailValues mdvEnd WITH (NOLOCK) ON m.MatterID = mdvEnd.MatterID AND mdvEnd.DetailFieldID = 154441 AND (mdvEnd.ValueDate >= @CalculationDate OR mdvEnd.ValueDate IS NULL) 
	--INNER JOIN dbo.MatterDetailValues mdvScheme WITH (NOLOCK) ON m.MatterID = mdvScheme.MatterID AND mdvScheme.DetailFieldID = 156009
	----INNER JOIN dbo.ResourceListDetailValues rdvAffinity WITH (NOLOCK) ON mdvScheme.ValueInt = rdvAffinity.ResourceListID AND rdvAffinity.DetailFieldID = 156007
	--INNER JOIN dbo.ResourceListDetailValues rdvProduct WITH (NOLOCK) ON mdvScheme.ValueInt = rdvProduct.ResourceListID AND rdvProduct.DetailFieldID = 156008
	--LEFT JOIN dbo.Translation t WITH (NOLOCK) ON rdvProduct.ResourceListID = t.ID AND rdvProduct.DetailFieldID = t.DetailFieldID AND t.LanguageID = @LanguageID AND t.ClientID = 235
	--INNER JOIN dbo.ResourceListDetailValues rdvType WITH (NOLOCK) ON mdvScheme.ValueInt = rdvType.ResourceListID AND rdvType.DetailFieldID = 160146
	--INNER JOIN dbo.MatterDetailValues mdvBaseLine WITH (NOLOCK) ON m.MatterID = mdvBaseLine.MatterID AND mdvBaseLine.DetailFieldID = @PetTypeBaseID
	--INNER JOIN dbo.LeadDetailValues ldvEnrolment WITH (NOLOCK) ON l.LeadID = ldvEnrolment.LeadID AND ldvEnrolment.DetailFieldID = 159961
	--LEFT JOIN dbo.MatterDetailValues mdvPetTerritory WITH (NOLOCK) ON m.MatterID = mdvPetTerritory.MatterID AND mdvPetTerritory.DetailFieldID = @PetTerritoryID
	--INNER JOIN dbo.MatterDetailValues mdvMaxDiscount WITH (NOLOCK) ON m.MatterID = mdvMaxDiscount.MatterID AND mdvMaxDiscount.DetailFieldID = 154467
	--INNER JOIN dbo.MatterDetailValues mdvOrder WITH (NOLOCK) ON m.MatterID = mdvOrder.MatterID AND mdvOrder.DetailFieldID = 156902
	--LEFT JOIN dbo.MatterDetailValues mdvLossRatio WITH (NOLOCK) ON m.MatterID = mdvLossRatio.MatterID AND mdvLossRatio.DetailFieldID = 163135
	----WHERE l.LeadID = 4215904
	--WHERE l.LeadID = dbo.fn_C235_GetIllnessSchemeLeadIDFromBrandRLID(ISNULL(@BrandRLID,138780))
	--AND (@SchemeMatterID IS NULL OR m.MatterID = @SchemeMatterID)
	--AND mdvOrder.ValueInt > 0 -- we used to just filter on having an order... left it in for flexibility later
	--AND rdvType.ValueInt IN (67771, 67772) -- Just accident and accident / illness
	--ORDER BY mdvOrder.ValueInt

	RETURN

END





GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_Quotes_GetBaselineAmounts] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1273_Quotes_GetBaselineAmounts] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_Quotes_GetBaselineAmounts] TO [sp_executeall]
GO
