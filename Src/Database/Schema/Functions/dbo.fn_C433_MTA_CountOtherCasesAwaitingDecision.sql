SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-07-17
-- Description:	Count other matters awaiting a decision on customer MTAs
-- 2018-07-24 GPR created C00 version
-- =============================================
CREATE FUNCTION [dbo].[fn_C433_MTA_CountOtherCasesAwaitingDecision]
(
	 @CaseID INT
)
RETURNS INT
AS
BEGIN
		
	DECLARE @Count INT = 0
	
	SELECT @Count = dbo.fn_C00_MTA_CountOtherCasesAwaitingDecision(@CaseID)

	RETURN @Count

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C433_MTA_CountOtherCasesAwaitingDecision] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C433_MTA_CountOtherCasesAwaitingDecision] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C433_MTA_CountOtherCasesAwaitingDecision] TO [sp_executeall]
GO
