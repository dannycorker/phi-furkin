SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2013-01-14
-- Description:	Returns just date this many days ago
-- =============================================
CREATE FUNCTION [dbo].[fnDateOnlyNDaysAgo] 
(
	@DaysAgo int
)
RETURNS datetime
AS
BEGIN
	DECLARE @Result datetime

	SET @Result = convert(datetime, convert(char(10), dbo.fn_GetDate_Local() - @DaysAgo, 101))

	RETURN @Result

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnDateOnlyNDaysAgo] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnDateOnlyNDaysAgo] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnDateOnlyNDaysAgo] TO [sp_executeall]
GO
