SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-02-26
-- Description:	Calls the CLR RegionalToGmt but allows you to specify the timezone ID rather than the string
-- =============================================
CREATE FUNCTION [dbo].[fnRegionalToGmt] 
(
	@Regional DATETIME,
	@TimezoneID INT
)
RETURNS DATETIME
AS
BEGIN

	DECLARE @Gmt DATETIME,
			@TimeZoneKey VARCHAR(200)
	
	SELECT @TimeZoneKey = TimeZoneKey
	FROM dbo.TimeZone WITH (NOLOCK) 
	WHERE TimeZoneID = @TimeZoneID
	
	SELECT @Gmt = dbo.RegionalToGmt(@Regional, @TimeZoneKey)
	
	RETURN @Gmt
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnRegionalToGmt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnRegionalToGmt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnRegionalToGmt] TO [sp_executeall]
GO
