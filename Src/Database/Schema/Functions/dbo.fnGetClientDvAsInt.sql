SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-06-15
-- Description:	Get ClientDV directly - New Bypass CaseID version #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetClientDvAsInt]
(
	@DetailFieldID		INT,
	@ClientID			INT
)
RETURNS int
AS
BEGIN

	DECLARE @ValueInt AS int

	SELECT @ValueInt = cldv.ValueInt
	FROM ClientDetailValues cldv WITH (NOLOCK) 
	WHERE cldv.ClientID = @ClientID 
	AND cldv.DetailFieldID = @DetailFieldID

	RETURN @ValueInt

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetClientDvAsInt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetClientDvAsInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetClientDvAsInt] TO [sp_executeall]
GO
