SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-09-23
-- Description:	Returns a list of policy history table rows from the correct place
--	2019-04-24 AWG -- added code to pull back the OMF claim history lines #56026
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_Policy_GetPolicyHistoryRows]
(
	@ClaimMatterID INT 
)
RETURNS 
	@TableRows TABLE 
	(
		TableRowID INT,
		ClientID INT, 
		LeadID INT, 
		MatterID INT, 
		DetailFieldID INT, 
		DetailFieldPageID INT, 
		DenyEdit BIT, 
		DenyDelete BIT, 
		CustomerID INT, 
		CaseID INT, 
		ClientPersonnelID INT, 
		ContactID INT, 
		SourceID INT
	)
AS
BEGIN

	DECLARE @PolicyOnLinkedLead BIT
	SELECT @PolicyOnLinkedLead = dbo.fn_C00_1272_IsPolicyOnLinkedLead(@ClaimMatterID)
	
	DECLARE @PolicyMatterID INT,
			@ClaimLeadID INT
	
	IF @PolicyOnLinkedLead = 1
	BEGIN
		
		SELECT @PolicyMatterID = dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(@ClaimMatterID)
		
	END
	ELSE 
	BEGIN
	
		SELECT @ClaimLeadID = LeadID
		FROM dbo.Matter WITH (NOLOCK) 
		WHERE MatterID = @ClaimMatterID
	
	END
--- AWG '2019-04-24 - added to pull back the OMF claim history lines #56026.
	Declare @OMFCID INT	-- OMF Convertion matterid
	SELECT @OMFCID = MDV.DetailValue
	FROM MatterDetailValues MDV
	where DetailFieldID = 180224
	and MatterID = @PolicyMatterID 
	
		
	INSERT @TableRows (TableRowID, ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID, CaseID, ClientPersonnelID, ContactID, SourceID)
	SELECT TableRowID, ClientID, LeadID, MatterID, DetailFieldID, DetailFieldPageID, DenyEdit, DenyDelete, CustomerID, CaseID, ClientPersonnelID, ContactID, SourceID
	FROM dbo.TableRows r WITH (NOLOCK) 
	WHERE (r.MatterID IN (@PolicyMatterID,@OMFCID)  AND r.DetailFieldID = 170033)--AWG '2019-04-24 - added to pull back the OMF claim history lines #56026 
	OR (r.LeadID = @ClaimLeadID AND r.DetailFieldID = 145668)
	
	RETURN

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPolicyHistoryRows] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1272_Policy_GetPolicyHistoryRows] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPolicyHistoryRows] TO [sp_executeall]
GO
