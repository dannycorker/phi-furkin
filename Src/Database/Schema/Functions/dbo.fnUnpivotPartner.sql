SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Unpivot the columns in the Partner table 
--				2013-01-25 Added case switches for use same address
-- =============================================
CREATE FUNCTION [dbo].[fnUnpivotPartner] 
(	
	@PartnerID int
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT ColumnName as [RawColumnName], '[!' + ColumnName + ']' as [FormattedColumnName], RealValue
	FROM
	(
		SELECT 
		COALESCE(CONVERT(varchar(255), CASE c.UseCustomerAddress WHEN 1 THEN cu.Address1 ELSE c.Address1 END), '') as [Address1],
		COALESCE(CONVERT(varchar(255), CASE c.UseCustomerAddress WHEN 1 THEN cu.Address2 ELSE c.Address2 END), '') as [Address2],
		COALESCE(CONVERT(varchar(255), c.ClientID), '') as [ClientID],
		COALESCE(CONVERT(varchar(255), CASE c.UseCustomerAddress WHEN 1 THEN cu.County ELSE c.County END), '') as [County],
		COALESCE(CONVERT(varchar(255), CASE c.UseCustomerAddress WHEN 1 THEN cu.CountryID ELSE c.CountryID END), '') as [CountryID],
		COALESCE(CONVERT(varchar(255), c.CustomerID), '') as [CustomerID],
		COALESCE(CONVERT(varchar(255), c.DateOfBirth, 103), '') as [DateOfBirth],
		COALESCE(CONVERT(varchar(255), c.DayTimeTelephoneNumber), '') as [DayTimeTelephoneNumber],
		COALESCE(CONVERT(varchar(255), c.EmailAddress), '') as [EmailAddress],
		COALESCE(CONVERT(varchar(255), c.Employer), '') as [Employer],
		COALESCE(CONVERT(varchar(255), c.FirstName), '') as [FirstName],
		COALESCE(CONVERT(varchar(255), c.FullName), '') as [FullName],
		COALESCE(CONVERT(varchar(255), c.HomeTelephone), '') as [HomeTelephone],
		COALESCE(CONVERT(varchar(255), c.LastName), '') as [LastName],
		COALESCE(CONVERT(varchar(255), c.MiddleName), '') as [MiddleName],
		COALESCE(CONVERT(varchar(255), c.MobileTelephone), '') as [MobileTelephone],
		COALESCE(CONVERT(varchar(255), c.Occupation), '') as [Occupation],
		COALESCE(CONVERT(varchar(255), c.PartnerID), '') as [PartnerID],
		COALESCE(CONVERT(varchar(255), CASE c.UseCustomerAddress WHEN 1 THEN cu.PostCode ELSE c.PostCode END), '') as [PostCode],
		COALESCE(CONVERT(varchar(255), c.TitleID), '') as [TitleID],
		COALESCE(CONVERT(varchar(255), CASE c.UseCustomerAddress WHEN 1 THEN cu.Town ELSE c.Town END), '') as [Town],
		COALESCE(CONVERT(varchar(255), c.UseCustomerAddress), '') as [UseCustomerAddress]
		FROM dbo.[Partner] c WITH (NOLOCK) 
		INNER JOIN Customers cu WITH (NOLOCK) ON cu.CustomerID = c.CustomerID
		WHERE c.PartnerID = @PartnerID) p
	UNPIVOT
	(
		RealValue FOR ColumnName IN
		(
			[Address1],[Address2],[ClientID],[CountryID],[County],[CustomerID],[DateOfBirth],[DayTimeTelephoneNumber],[EmailAddress],[Employer],[FirstName],[FullName],[HomeTelephone],[LastName],[MiddleName],[MobileTelephone],[Occupation],[PartnerID],[PostCode],[TitleID],[Town],[UseCustomerAddress]
		)
	) as unpvt

)







GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotPartner] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnUnpivotPartner] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotPartner] TO [sp_executeall]
GO
