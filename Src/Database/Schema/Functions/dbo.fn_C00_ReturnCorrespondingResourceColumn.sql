SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-07-29
-- Description:	Return a corresponding columns value from a resource
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ReturnCorrespondingResourceColumn]
(
	@MatchFieldID INT,
	@MatchValue VARCHAR(2000),
	@ReturnColumn INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE @ReturnValue VARCHAR(2000)

	/*Select the corresponding value from a resource*/
	SELECT @ReturnValue = rlv_return.DetailValue
	FROM ResourceListDetailValues rldv WITH (NOLOCK)
	INNER JOIN ResourceListDetailValues rlv_return WITH (NOLOCK) ON rlv_return.ResourceListID = rldv.ResourceListID AND rlv_return.DetailFieldID = @ReturnColumn
	WHERE rldv.DetailFieldID = @MatchFieldID 
	AND rldv.DetailValue = @MatchValue

	RETURN @ReturnValue

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReturnCorrespondingResourceColumn] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ReturnCorrespondingResourceColumn] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReturnCorrespondingResourceColumn] TO [sp_executeall]
GO
