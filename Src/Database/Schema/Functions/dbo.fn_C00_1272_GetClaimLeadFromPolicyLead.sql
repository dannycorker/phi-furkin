SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-09-08
-- Description:	Returns the claim lead from a policy
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetClaimLeadFromPolicyLead]
(
	@PolicyLead INT
)
RETURNS INT	
AS
BEGIN

	DECLARE @ClaimLead INT,
			@ClientID INT,
			@ClaimLeadTypeID INT,
			@PolicyAdminLeadTypeID INT
			
	SELECT @ClientID = ClientID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @PolicyLead
	
	SELECT @ClaimLeadTypeID = SharedTo
	FROM dbo.LeadTypeShare WITH (NOLOCK) 
	WHERE SharedFrom = 1272
	AND ClientID = @ClientID
	
	SELECT @PolicyAdminLeadTypeID = SharedTo
	FROM dbo.LeadTypeShare WITH (NOLOCK) 
	WHERE SharedFrom = 1273
	AND ClientID = @ClientID
	
	DECLARE @LinkedLead INT
	SELECT @LinkedLead = ToLeadID
	FROM dbo.LeadTypeRelationship WITH (NOLOCK) 
	WHERE FromLeadTypeID = @PolicyAdminLeadTypeID 
	AND ToLeadTypeID = @ClaimLeadTypeID 
	AND FromLeadID = @PolicyLead
	
	IF @LinkedLead > 0
	BEGIN
		
		SELECT @ClaimLead = @LinkedLead
		
	END
	ELSE
	BEGIN
	
		SELECT @ClaimLead = @PolicyLead
	
	END
	
	
	RETURN @ClaimLead

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetClaimLeadFromPolicyLead] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_GetClaimLeadFromPolicyLead] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetClaimLeadFromPolicyLead] TO [sp_executeall]
GO
