SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-09-24
-- Description:	Returns a row representing all of the policy fields that can either be at lead or matter level
-- MODs JEL 2019-02-18 IF this is linked to an OMF Policy, override the inception date with the OMF inception
-- JEL 2019-02-25 Created this from fn_C00_1272_Policy_GetPolicyDetails for 600 specific OMF functionality 
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Policy_GetPolicyDetails]
(
	@ClaimMatterID INT
)
RETURNS
	@PolicyDetails TABLE 
	(
		VIP BIT,
		StaffPolicy BIT,
		CurrentPolicyID INT,
		PolicyInception DATE,
		PolicyStart DATE,
		PolicyEnd DATE,
		PolicyStatusID INT,
		VolExcess MONEY,
		TotalPremiumPaid MONEY,
		AffinityGroup VARCHAR(2000),
		ImportedPolicyRef VARCHAR(2000),
		ImportedUpdateDate DATE,
		CurrentPolicySchemeMatterID INT
	)
AS
BEGIN

	DECLARE @PolicyOnLinkedLead BIT
	SELECT @PolicyOnLinkedLead = dbo.fn_C00_1272_IsPolicyOnLinkedLead(@ClaimMatterID)
	
	DECLARE @PolicyMatterID INT,
			@ClaimLeadID INT,
			@ClientID INT,
			@PolicyLeadID INT

	SELECT @ClientID = m.ClientID FROM Matter m with (NOLOCK) 
	where m.MatterID = @ClaimMatterID
	
	SELECT @PolicyMatterID = dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(@ClaimMatterID)
	
	SELECT @PolicyLeadID = m.LeadID FROM Matter m with (NOLOCK) 
	where m.MatterID = @PolicyMatterID
	
	IF @PolicyOnLinkedLead = 1
	BEGIN
		
		INSERT @PolicyDetails (VIP, StaffPolicy, CurrentPolicyID, PolicyInception, PolicyStart, PolicyEnd, PolicyStatusID, VolExcess, 
								TotalPremiumPaid, AffinityGroup, ImportedPolicyRef, ImportedUpdateDate, CurrentPolicySchemeMatterID)
		SELECT	mdvVip.ValueInt, mdvStaff.ValueInt, mdvPolicy.ValueInt, mdvIncept.ValueDate, mdvStart.ValueDate, mdvEnd.ValueDate,
				mdvStatus.ValueInt,mdvVolEx.ValueMoney, mdvPaid.ValueMoney, mdvGroup.DetailValue, mdvRef.DetailValue, mdvUpdate.ValueDate,
				mdvScheme.ValueInt
		FROM dbo.Matter m WITH (NOLOCK)		
		LEFT JOIN dbo.MatterDetailValues mdvVip WITH (NOLOCK) ON m.MatterID = mdvVip.MatterID AND mdvVip.DetailFieldID = 170042
		LEFT JOIN dbo.MatterDetailValues mdvStaff WITH (NOLOCK) ON m.MatterID = mdvStaff.MatterID AND mdvStaff.DetailFieldID = 170046
		LEFT JOIN dbo.MatterDetailValues mdvPolicy WITH (NOLOCK) ON m.MatterID = mdvPolicy.MatterID AND mdvPolicy.DetailFieldID = 170034
		LEFT JOIN dbo.MatterDetailValues mdvIncept WITH (NOLOCK) ON m.MatterID = mdvIncept.MatterID AND mdvIncept.DetailFieldID = 170035
		LEFT JOIN dbo.MatterDetailValues mdvStart WITH (NOLOCK) ON m.MatterID = mdvStart.MatterID AND mdvStart.DetailFieldID = 170036
		LEFT JOIN dbo.MatterDetailValues mdvEnd WITH (NOLOCK) ON m.MatterID = mdvEnd.MatterID AND mdvEnd.DetailFieldID = 170037
		LEFT JOIN dbo.MatterDetailValues mdvStatus WITH (NOLOCK) ON m.MatterID = mdvStatus.MatterID AND mdvStatus.DetailFieldID = 170038
		LEFT JOIN dbo.MatterDetailValues mdvVolEx WITH (NOLOCK) ON m.MatterID = mdvVolEx.MatterID AND mdvVolEx.DetailFieldID = 170039
		LEFT JOIN dbo.MatterDetailValues mdvPaid WITH (NOLOCK) ON m.MatterID = mdvPaid.MatterID AND mdvPaid.DetailFieldID = 170043
		LEFT JOIN dbo.MatterDetailValues mdvGroup WITH (NOLOCK) ON m.MatterID = mdvGroup.MatterID AND mdvGroup.DetailFieldID = 170045
		LEFT JOIN dbo.MatterDetailValues mdvRef WITH (NOLOCK) ON m.MatterID = mdvRef.MatterID AND mdvRef.DetailFieldID = 170041
		LEFT JOIN dbo.MatterDetailValues mdvUpdate WITH (NOLOCK) ON m.MatterID = mdvUpdate.MatterID AND mdvUpdate.DetailFieldID = 170044
		LEFT JOIN dbo.MatterDetailValues mdvScheme WITH (NOLOCK) ON m.MatterID = mdvScheme.MatterID AND mdvScheme.DetailFieldID = 170047
		WHERE m.MatterID = @PolicyMatterID
		
	END
	ELSE 
	BEGIN
	
		SELECT @ClaimLeadID = LeadID
		FROM dbo.Matter WITH (NOLOCK) 
		WHERE MatterID = @ClaimMatterID
		
		INSERT @PolicyDetails (VIP, StaffPolicy, CurrentPolicyID, PolicyInception, PolicyStart, PolicyEnd, PolicyStatusID, VolExcess, 
								TotalPremiumPaid, AffinityGroup, ImportedPolicyRef, ImportedUpdateDate, CurrentPolicySchemeMatterID)
		SELECT	ldvVip.ValueInt, ldvStaff.ValueInt, ldvPolicy.ValueInt, ldvIncept.ValueDate, ldvStart.ValueDate, ldvEnd.ValueDate,
				ldvStatus.ValueInt,ldvVolEx.ValueMoney, ldvPaid.ValueMoney, ldvGroup.DetailValue, ldvRef.DetailValue, ldvUpdate.ValueDate,
				ldvScheme.ValueInt
		FROM dbo.Lead l WITH (NOLOCK)
		LEFT JOIN dbo.LeadDetailValues ldvVip WITH (NOLOCK) ON l.LeadID = ldvVip.LeadID AND ldvVip.DetailFieldID = 149862
		LEFT JOIN dbo.LeadDetailValues ldvStaff WITH (NOLOCK) ON l.LeadID = ldvStaff.LeadID AND ldvStaff.DetailFieldID = 162670
		LEFT JOIN dbo.LeadDetailValues ldvPolicy WITH (NOLOCK) ON l.LeadID = ldvPolicy.LeadID AND ldvPolicy.DetailFieldID = 146193
		LEFT JOIN dbo.LeadDetailValues ldvIncept WITH (NOLOCK) ON l.LeadID = ldvIncept.LeadID AND ldvIncept.DetailFieldID = 146194
		LEFT JOIN dbo.LeadDetailValues ldvStart WITH (NOLOCK) ON l.LeadID = ldvStart.LeadID AND ldvStart.DetailFieldID = 146195
		LEFT JOIN dbo.LeadDetailValues ldvEnd WITH (NOLOCK) ON l.LeadID = ldvEnd.LeadID AND ldvEnd.DetailFieldID = 146196
		LEFT JOIN dbo.LeadDetailValues ldvStatus WITH (NOLOCK) ON l.LeadID = ldvStatus.LeadID AND ldvStatus.DetailFieldID = 146197
		LEFT JOIN dbo.LeadDetailValues ldvVolEx WITH (NOLOCK) ON l.LeadID = ldvVolEx.LeadID AND ldvVolEx.DetailFieldID = 146198
		LEFT JOIN dbo.LeadDetailValues ldvPaid WITH (NOLOCK) ON l.LeadID = ldvPaid.LeadID AND ldvPaid.DetailFieldID = 151296
		LEFT JOIN dbo.LeadDetailValues ldvGroup WITH (NOLOCK) ON l.LeadID = ldvGroup.LeadID AND ldvGroup.DetailFieldID = 161559
		LEFT JOIN dbo.LeadDetailValues ldvRef WITH (NOLOCK) ON l.LeadID = ldvRef.LeadID AND ldvRef.DetailFieldID = 148277
		LEFT JOIN dbo.LeadDetailValues ldvUpdate WITH (NOLOCK) ON l.LeadID = ldvUpdate.LeadID AND ldvUpdate.DetailFieldID = 157349
		LEFT JOIN dbo.LeadDetailValues ldvScheme WITH (NOLOCK) ON l.LeadID = ldvScheme.LeadID AND ldvScheme.DetailFieldID = 162694
		WHERE l.LeadID = @ClaimLeadID
	
	END
	/*JEL 2019-02-18 IF this is linked to an OMF Policy, override the inception date with the OMF inception*/ 
	IF dbo.fnGetPrimaryClientID() = 600 and dbo.fnGetSimpledv(180019,@PolicyLeadID) = 'true'
	BEGIN 

		Update @PolicyDetails 
		SET PolicyInception = inception.ValueDate
		FROM MatterDetailValues omfID with (NOLOCK) 
		INNER JOIN MatterDetailValues inception with (NOLOCK) on inception.MatterID = omfID.ValueINT and inception.DetailFieldID = 170035
		WHERE omfID.DetailFieldID = 180224
		AND omfID.MatterID = @PolicyMatterID

	END
	
	RETURN

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Policy_GetPolicyDetails] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_Policy_GetPolicyDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Policy_GetPolicyDetails] TO [sp_executeall]
GO
