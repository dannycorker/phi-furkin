SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-10-01
-- Description:	Refactored more to collect co-ins without a customer
-- Modified:	2015-06-18	SB	Change to allow multiple levels of age driven coinsurance
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_Policy_GetPolicyCoInsuranceWithData]
(
	@CurrentPolicyMatterID INT,
	@PetDoB DATE,
	@PetType INT,
	@SpecialProcessing INT,
	@AnimalBreed VARCHAR(2000)
)
RETURNS 
	@CoInsData TABLE 
	(
		ResourceListID INT,
		Section VARCHAR(2000),
		SubSection VARCHAR(2000),
		ExcessPercentage MONEY,
		DateCutOff DATE,
		CoInsLinkedTo INT,
		MinThreshold MONEY,
		Breed VARCHAR(2000),
		Options VARCHAR(2000)
	)
AS
BEGIN
	
	
	DECLARE @Raw TABLE
	(
		ResourceListID INT,
		Section VARCHAR(2000),
		SubSection VARCHAR(2000),
		ExcessPercentage MONEY,
		DateCutOff DATETIME,
		CoInsLinkedTo INT,
		MinThreshold MONEY,
		Breed VARCHAR(2000),
		Options VARCHAR(2000)
	)
	
	;WITH Raw AS 
	(
		SELECT  DISTINCT s.Out_ResourceListID AS ResourceListID, llSection.ItemValue AS Section, llSubSection.ItemValue AS SubSection,
				tdvExcessPercentage.ValueMoney AS ExcessPercentage, DATEADD(YEAR, ISNULL(tdvPetAge.ValueInt, 0), @PetDoB) AS DateCutOff, 
				tdvCoInsLink.ValueInt AS CoInsLinkedTo, ISNULL(tdvMinThresh.ValueMoney, 0) AS MinThreshold, tdvBreed.DetailValue AS Breed, tdvOptions.DetailValue AS Options
		FROM dbo.fn_C00_1272_GetPolicySectionRelationships(@CurrentPolicyMatterID) s
		INNER JOIN dbo.ResourceListDetailValues rdvSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189
		INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rdvSection.ValueInt = llSection.LookupListItemID 
		INNER JOIN dbo.ResourceListDetailValues rdvSubSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSubSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190
		 LEFT JOIN dbo.LookupListItems llSubSection WITH (NOLOCK) ON rdvSubSection.ValueInt = llSubSection.LookupListItemID  /*CS 2017-05-25 LEFT joined*/
		INNER JOIN dbo.TableDetailValues tdvRL WITH (NOLOCK) ON s.Out_ResourceListID = tdvRL.ResourceListID AND tdvRL.DetailFieldID = 147192
		INNER JOIN dbo.TableDetailValues tdvExcessPercentage WITH (NOLOCK) ON tdvRL.TableRowID = tdvExcessPercentage.TableRowID AND tdvExcessPercentage.DetailFieldID = 147194
		INNER JOIN dbo.TableDetailValues tdvPetType WITH (NOLOCK) ON tdvRL.TableRowID = tdvPetType.TableRowID AND tdvPetType.DetailFieldID = 146210
		INNER JOIN dbo.TableDetailValues tdvPetAge WITH (NOLOCK) ON tdvRL.TableRowID = tdvPetAge.TableRowID AND tdvPetAge.DetailFieldID = 146211
		INNER JOIN dbo.TableDetailValues tdvCoInsLink WITH (NOLOCK) ON tdvRL.TableRowID = tdvCoInsLink.TableRowID AND tdvCoInsLink.DetailFieldID = 147193
		LEFT JOIN dbo.TableDetailValues tdvMinThresh WITH (NOLOCK) ON tdvRL.TableRowID = tdvMinThresh.TableRowID AND tdvMinThresh.DetailFieldID = 147941
		LEFT JOIN dbo.TableDetailValues tdvBreed WITH (NOLOCK) ON tdvRL.TableRowID = tdvBreed.TableRowID AND tdvBreed.DetailFieldID = 148232
		LEFT JOIN dbo.TableDetailValues tdvOptions WITH (NOLOCK) ON tdvRL.TableRowID = tdvOptions.TableRowID AND tdvOptions.DetailFieldID = 170195 /*GPR 2020-07-24 reinstated Options*/
		WHERE tdvRL.MatterID = @CurrentPolicyMatterID
		AND (tdvPetType.ValueInt = @PetType or tdvPetType.ValueInt IS NULL or tdvPetType.ValueInt = 0)
	)
	
	INSERT @Raw (ResourceListID, Section, SubSection, ExcessPercentage, DateCutOff, CoInsLinkedTo, MinThreshold, Breed, Options)
	SELECT *
	FROM Raw
	
	
	;WITH NoBreed AS
	(
		SELECT *
		FROM @Raw 
		WHERE Breed IS NULL
		OR Breed = ''
	), BreedSplit AS
	(
		SELECT r.ResourceListID, r.Section, r.SubSection, r.ExcessPercentage, r.DateCutOff, r.CoInsLinkedTo, r.MinThreshold, REPLACE(v.AnyValue, '[', '¬[') AS Breed, Options
		FROM @Raw r
		CROSS APPLY dbo.fnTableOfValuesFromCSV(r.Breed) v
		WHERE r.Breed > ''
		AND v.AnyValue > ''
	), MatchedBreed AS
	(
		SELECT * 
		FROM BreedSplit
		WHERE @AnimalBreed LIKE Breed ESCAPE '¬'
	), Merged AS
	(
		SELECT * 
		FROM NoBreed
		UNION
		SELECT *
		FROM MatchedBreed
	), Sorted AS
	(
		SELECT *, ROW_NUMBER() OVER(PARTITION BY ResourceListID, DateCutOff ORDER BY ExcessPercentage DESC) as rn 
		FROM Merged
	)
	
	INSERT @CoInsData
	SELECT ResourceListID, Section, SubSection, ExcessPercentage, DateCutOff, CoInsLinkedTo, MinThreshold, REPLACE(Breed, '¬[', '[') AS Breed, Options
	FROM Sorted
	WHERE rn = 1
	
	
	RETURN

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPolicyCoInsuranceWithData] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1272_Policy_GetPolicyCoInsuranceWithData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPolicyCoInsuranceWithData] TO [sp_executeall]
GO
