SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-10-08
-- Description:	Returns the monthly premiums split from the annual
-- 2019-09-11	GPR/AMG altered logic to force FirstMonthly to be greater than RecurringMonthly for C600 Aggregator Multipet SummaryOfQuotes node - ZD#59538/#1886
-- 2020-06-16	GPR removed operation from rounding to avoid truncation of value
-- =============================================
 
CREATE FUNCTION [dbo].[fn_C00_1273_SplitPremium]
(
	@AnnualPremium MONEY
)
RETURNS 
@Data TABLE
(
	FirstMonthly MONEY,
	RecurringMonthly MONEY
)	
AS
BEGIN
	
	DECLARE @FirstMonthly MONEY,
			@RecurringMonthly MONEY,
			@Down2dp DECIMAL(18, 2),
			@RecurringSum MONEY,
			@Remainder MONEY
			
	DECLARE @Split DECIMAL(18, 4)
	SELECT @Split = @AnnualPremium / 12

	SELECT @Down2dp = ROUND(@Split, 2)--, 1) /*GPR 2020-06-16 for JIRA SDAAG-61 | removed operation to avoid truncation that results in rounding down*/
	
	SELECT @RecurringSum = @Down2dp * 12

	SELECT @Remainder = @AnnualPremium - @RecurringSum
	
	INSERT @Data (FirstMonthly, RecurringMonthly)
	VALUES (@Down2dp + @Remainder, @Down2dp)
	
	RETURN
 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_SplitPremium] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1273_SplitPremium] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_SplitPremium] TO [sp_executeall]
GO
