SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-01
-- Description:	Return the smaller of 2 numbers (int)
-- =============================================
CREATE FUNCTION [dbo].[fnMinOfInt] 
(
	@Number1 int,
	@Number2 int
)
RETURNS int
AS
BEGIN
	DECLARE @SmallerNumber int

	IF @Number1 IS NULL
	BEGIN
		RETURN @Number2
	END
	
	IF @Number2 IS NULL
	BEGIN
		RETURN @Number1
	END
	
	SELECT @SmallerNumber = CASE WHEN @Number1 < @Number2 THEN @Number1 ELSE @Number2 END

	RETURN @SmallerNumber

END









GO
GRANT VIEW DEFINITION ON  [dbo].[fnMinOfInt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnMinOfInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnMinOfInt] TO [sp_executeall]
GO
