SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-09-16
-- Description:	Returns brand from product
-- Modified DCM 2014-09-16 Copied from 235
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetBrandRLIDFromProductRLID]
(
	@ProductRLID INT
)
RETURNS INT	
AS
BEGIN
	
	DECLARE @BrandRLID INT = NULL
	
	SELECT @BrandRLID = rldvBrandRLID.ValueInt
	FROM ResourceListDetailValues rldvBrandRLID 
	WHERE rldvBrandRLID.ResourceListID = @ProductRLID
	AND rldvBrandRLID.DetailFieldID = 1170994 -- TODO not mapped
	
	RETURN @BrandRLID
 
END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetBrandRLIDFromProductRLID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetBrandRLIDFromProductRLID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetBrandRLIDFromProductRLID] TO [sp_executeall]
GO
