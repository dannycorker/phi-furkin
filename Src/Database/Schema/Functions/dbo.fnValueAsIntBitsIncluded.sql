SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2011-06-22
-- Description:	Convert varchar to int or bit, returning NULL if not applicable
--              This counts "true" and "false" as 1 and 0 respectively for fast indexing of these values
-- =============================================
CREATE FUNCTION [dbo].[fnValueAsIntBitsIncluded] 
(
	@value varchar(2000)
)
RETURNS int
AS
BEGIN
	
	SELECT @value = ltrim(rtrim(@value))
	
	/*
		Here are the expected results:
		Input:			Output:
		NULL			NULL
		''				0
		'true'			1
		'false'			0
		'9999999999'	NULL (too big for an int)
		'50'			50 
		'-1234'			-1234 
		'1.234'			NULL (not integer)
		'-1.234'		NULL (not integer)
	*/
	RETURN CASE
			WHEN @value IS NULL THEN NULL
			WHEN @value = '' THEN 0
			WHEN @value = 'true' THEN 1
			WHEN @value = 'false' THEN 0
			WHEN datalength(@value) > 9 THEN NULL
			WHEN PatIndex('%[^0-9]%', @value) = 0 THEN CAST(@value AS INT)
			ELSE 
				CASE
				WHEN @value LIKE '-%' AND PatIndex('%[^0-9]%', substring(@value, 2, len(@value))) = 0 THEN CAST(@value AS INT)
				ELSE NULL
				END	
			END
			
END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnValueAsIntBitsIncluded] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnValueAsIntBitsIncluded] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnValueAsIntBitsIncluded] TO [sp_executeall]
GO
