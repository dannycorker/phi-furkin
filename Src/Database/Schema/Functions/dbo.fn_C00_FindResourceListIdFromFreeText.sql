SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-08-01
-- Description:	Take a text value, search a resource to return a matching ID
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_FindResourceListIdFromFreeText]
(
	 @MatchValue				VARCHAR(2000)
	,@MatchDetailFieldID		INT
	,@SynonymDetailFieldID		INT
	,@DefaultValue				INT
)
RETURNS INT
AS
BEGIN

	DECLARE	 @ResourceListID			INT
			
	;WITH AllEntries AS
	(
	SELECT rdv.ResourceListID, rdv.DetailValue, 0 [SeqOrder]		/*Search in the primary field*/
	FROM ResourceListDetailValues rdv WITH (NOLOCK) 
	WHERE rdv.DetailFieldID = @MatchDetailFieldID
		UNION
	SELECT rdv.ResourceListID, csv.AnyValue, 1 [SeqOrder]			/*Allow a comma-separated field full of synonyms.  Search this only if there's no match for the primary field*/
	FROM ResourceListDetailValues rdv WITH (NOLOCK) 
	CROSS APPLY dbo.fnTableOfValuesFromCSV(rdv.DetailValue) csv
	WHERE rdv.DetailFieldID = @SynonymDetailFieldID
	)
	SELECT TOP(1) @ResourceListID = ae.ResourceListID
	FROM AllEntries ae 
	WHERE ae.DetailValue = @MatchValue
	ORDER BY ae.SeqOrder 

	SELECT @ResourceListID = ISNULL(@ResourceListID,@DefaultValue)	/*Value to be returned if there's no match*/
	RETURN @ResourceListID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_FindResourceListIdFromFreeText] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_FindResourceListIdFromFreeText] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_FindResourceListIdFromFreeText] TO [sp_executeall]
GO
