SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- ALTER date: 2014-10-13
-- Description:	Shreds the PetInfo from XML 
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_PetInfo_ShredXML]
(
	@XmlRequest XML
)
RETURNS 
	@PetInfo TABLE (
		RowID INT IDENTITY,
		PetRef VARCHAR(250),
		PetName VARCHAR(250),
		SpeciesId INT,
		BreedId INT,
		Gender VARCHAR(250),
		BirthDate DATE,
		IsNeutered BIT,
		HasMicrochip BIT,
		MicrochipNo  VARCHAR(250)
	)
AS
BEGIN

	INSERT INTO @PetInfo
	SELECT	Tbl.p.value('PetRef[1]', 'VARCHAR(250)'),
			Tbl.p.value('PetName[1]', 'VARCHAR(250)'),
			Tbl.p.value('SpeciesId[1]', 'INT'),
			Tbl.p.value('BreedId[1]', 'INT'),
			Tbl.p.value('Gender[1]', 'VARCHAR(250)'),
			Tbl.p.value('BirthDate[1]', 'DATE'),
			Tbl.p.value('IsNeutered[1]', 'BIT'),
			Tbl.p.value('HasMicrochip[1]', 'BIT'),
			Tbl.p.value('MicrochipNo[1]', 'VARCHAR(250)')
	FROM	@XmlRequest.nodes('//PetInfo') Tbl(p)
	
	RETURN

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_PetInfo_ShredXML] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1273_PetInfo_ShredXML] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_PetInfo_ShredXML] TO [sp_executeall]
GO
