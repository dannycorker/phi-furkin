SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-12-01
-- Description:	Returns the number of years policy has been in force
-- =============================================
 
CREATE FUNCTION [dbo].[fn_C00_1273_GetPolicyYearNumber]
(
	@CaseID INT,
	@CalcDate DATE
)
RETURNS INT	
AS
BEGIN

	DECLARE @RenewalYearNumber INT
 
	DECLARE @MatterID INT
	SELECT @MatterID = MatterID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE CaseID = @CaseID

	SELECT @RenewalYearNumber = COUNT(*)
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvFromDate WITH (NOLOCK) ON r.TableRowID = tdvFromDate.TableRowID AND tdvFromDate.DetailFieldID = 145663
	WHERE  r.MatterID = @MatterID AND
	r.DetailFieldID=170033 
	AND @CalcDate >= tdvFromDate.ValueDate

	-- add one if the calc date is in the next period
	IF @CalcDate >= ( 	SELECT MAX(tdvToDate.ValueDate)
						FROM dbo.TableRows r WITH (NOLOCK) 
						INNER JOIN dbo.TableDetailValues tdvToDate WITH (NOLOCK) ON r.TableRowID = tdvToDate.TableRowID AND tdvToDate.DetailFieldID = 145664
						WHERE  r.MatterID = @MatterID AND
						r.DetailFieldID=170033 
					)
		SELECT @RenewalYearNumber += 1
			
	-- cases where there isn't a historical policy yet
	IF @RenewalYearNumber < 0 
		SELECT @RenewalYearNumber = 0
	
	RETURN @RenewalYearNumber
 
END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetPolicyYearNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetPolicyYearNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetPolicyYearNumber] TO [sp_executeall]
GO
