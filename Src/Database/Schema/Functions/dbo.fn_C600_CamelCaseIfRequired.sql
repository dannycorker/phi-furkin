SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2015-07-02
-- Description:	Forces uppercase first letter if no characters are upper case.  For ZD #32648 
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_CamelCaseIfRequired]
(
	@Word VARCHAR(2000)
)
RETURNS VARCHAR(2000)	
AS
BEGIN
	
	IF LOWER(@Word) = @Word COLLATE latin1_general_cs_as
	BEGIN
		SELECT @Word = dbo.fnConvertCamelCase(@Word)
	END

	RETURN @Word

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_CamelCaseIfRequired] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_CamelCaseIfRequired] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_CamelCaseIfRequired] TO [sp_executeall]
GO
