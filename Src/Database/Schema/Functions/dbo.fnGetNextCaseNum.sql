SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-10-12
-- Description:	Get next case num for Dataloader
-- =============================================
CREATE FUNCTION [dbo].[fnGetNextCaseNum]
(
@LeadID INT
)
RETURNS INT
AS
BEGIN

	DECLARE @CaseNum INT

	SELECT @CaseNum = COUNT(*) + 1
	FROM Cases WITH (NOLOCK) 
	WHERE LeadID = @LeadID

	RETURN @CaseNum

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetNextCaseNum] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetNextCaseNum] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetNextCaseNum] TO [sp_executeall]
GO
