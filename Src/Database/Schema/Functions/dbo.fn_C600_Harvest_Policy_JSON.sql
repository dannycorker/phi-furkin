SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack / Gavin Reynolds
-- Create date:	2019-08-07
-- Description:	havests Policy json object
-- 2019-08-08 GPR						| Populated stub function
-- 2019-08-27 GPR						| Altered to use @Include flag
-- 2019-11-12 CPS for JIRA LAGCLAIM-162	| Changed to XML
-- 2019-11-14 CPS for JIRA LAGCLAIM-162 | Added PolicyTerms
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Harvest_Policy_JSON]
(
	 @PolicyID		INT
	,@VisionMode	BIT = 0
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE  @JsonData VARCHAR(MAX) = ''

	IF @PolicyID <> 0
	BEGIN
		SELECT  @JsonData +=
		(
			SELECT	 cu.CustomerID				[customerId]
					,l.LeadID					[petId]
					,m.MatterID					[policyId]
					,l.LeadRef					[reference]
					,lliStatus.ItemValue		[status]
					,mdvDateIncep.ValueDate		[inceptionDate]
					,mdvDateStart.ValueDate		[startDate]
					,mdvDateEnd.ValueDate		[endDate]
					,li.ItemValue				[affinityName]
					,li.LookupListItemID		[affinityId]
					,mdvPremium.ValueMoney		[premium]
					,mdvAccountID.ValueInt		[accountId]
					,(
						SELECT
						 tr.TableRowID				[policyTermId]
						,tdvFromDate.ValueDate		[startDate]
						,tdvToDate.ValueDate		[endDate]
						,tdvIncDate.ValueDate		[inceptionDate]
						,mdvProd.ValueInt			[productId]
						,rdvProdName.DetailValue	[productName]
						FROM MatterDetailValues mdvProd WITH (NOLOCK)
						 LEFT JOIN ResourceListDetailValues rdvProdName WITH (NOLOCK) on rdvProdName.ResourceListID = mdvProd.ValueInt	AND rdvProdName.DetailFieldID	= 146200 /*Product*/
						INNER JOIN TableRows tr							WITH (NOLOCK) on tr.MatterID = m.MatterID							AND tr.DetailFieldID			= 170033 /*Historical Policy*/
						INNER JOIN TableDetailValues tdvFromDate		WITH (NOLOCK) on tdvFromDate.TableRowID = tr.TableRowID				AND tdvFromDate.DetailFieldID	= 145663 /*Start*/
						INNER JOIN TableDetailValues tdvToDate			WITH (NOLOCK) on tdvToDate.TableRowID = tr.TableRowID				AND tdvToDate.DetailFieldID		= 145664 /*End*/
						INNER JOIN TableDetailValues tdvIncDate			WITH (NOLOCK) on tdvIncDate.TableRowID = tr.TableRowID				AND tdvIncDate.DetailFieldID	= 145662 /*Inception*/
						WHERE mdvProd.MatterID = @PolicyID
						AND mdvProd.DetailFieldID = 170034 /*Current Policy*/
						FOR JSON PATH
					 ) [policyTerms]
					,(
						SELECT
						 CASE WHEN @VisionMode = 1 THEN NULL ELSE ac.AccountID				END [accountId]
						,CASE WHEN @VisionMode = 1 THEN NULL ELSE ac.Active					END [active]
						,CASE WHEN @VisionMode = 1 THEN NULL ELSE ac.WhenCreated			END [whenCreated]
						,CASE WHEN @VisionMode = 1 THEN NULL ELSE ac.WhenModified			END [whenModified]
						,CASE WHEN @VisionMode = 1 THEN NULL ELSE lliPayMethod.ItemValue	END [paymentMethod]
						,CASE WHEN @VisionMode = 1 THEN NULL ELSE lliPayInteval.ItemValue	END [paymentInterval]
						,										  ac.AccountHolderName			[payee]
						,										  ac.AccountNumber				[accountNumber]
						,										  ac.Sortcode					[sortCode]
						,										  ac.Bankname					[bankName]
						,										  ac.AccountTypeID				[accountType]
						,										  cdv.ValueInt					[affinityId]
						,										  li.ItemValue					[affinityName]
					FROM Matter mBill WITH (NOLOCK) 
					INNER JOIN CustomerDetailValues cdv			WITH (NOLOCK) on cdv.CustomerID = mBill.CustomerID			AND cdv.DetailFieldID = 170144 /*Affinity Details*/
					INNER JOIN ResourceListDetailValues rdv		WITH (NOLOCK) on rdv.ResourceListID = cdv.DetailValue		AND rdv.DetailFieldID = 170127 /*Short Code*/
					INNER JOIN LookupListItems li				WITH (NOLOCK) on li.LookupListItemID = rdv.ValueInt
					INNER JOIN LeadTypeRelationship ltr			WITH (NOLOCK) on ltr.FromLeadID = mBill.LeadID AND ltr.ToLeadTypeID = 1493 /*Collections*/
					INNER JOIN MatterDetailValues mdvAccountID	WITH (NOLOCK) on mdvAccountID.MatterID = ltr.ToMatterID		AND mdvAccountID.DetailFieldID = 176973 /*Billing System Account ID*/
					INNER JOIN Account ac						WITH (NOLOCK) on ac.AccountID = mdvAccountID.ValueInt
					INNER JOIN MatterDetailValues mdvPayMethod	WITH (NOLOCK) on mdvPayMethod.LeadID = mBill.LeadID			AND mdvPayMethod.DetailFieldID = 170114 /*Payment Method*/
					INNER JOIN LookupListItems lliPayMethod		WITH (NOLOCK) on lliPayMethod.LookupListItemID = mdvPayMethod.ValueInt
					INNER JOIN MatterDetailValues mdvPayInteval WITH (NOLOCK) on mdvPayInteval.LeadID = mBill.LeadID		AND mdvPayInteval.DetailFieldID = 170176 /*Payment Interval*/
					INNER JOIN LookupListItems lliPayInteval	WITH (NOLOCK) on lliPayInteval.LookupListItemID = mdvPayInteval.ValueInt
					WHERE mBill.MatterID = @PolicyID
					FOR JSON PATH
					) [billings]
			FROM Customers cu								WITH (NOLOCK)
			INNER JOIN Lead l								WITH (NOLOCK) on l.CustomerID = cu.CustomerID
			INNER JOIN Cases c								WITH (NOLOCK) on c.LeadID = l.LeadID
			INNER JOIN Matter m								WITH (NOLOCK) on m.CaseID = c.CaseID
			INNER JOIN MatterDetailValues mdvStatus			WITH (NOLOCK) on mdvStatus.MatterID = m.MatterID					AND mdvStatus.DetailFieldID		= 170038 /*Policy Status*/
			INNER JOIN LookupListItems lliStatus			WITH (NOLOCK) on lliStatus.LookupListItemID = mdvStatus.ValueInt	
			 LEFT JOIN MatterDetailValues mdvDateIncep		WITH (NOLOCK) on mdvDateIncep.MatterID = m.MatterID					AND mdvDateIncep.DetailFieldID	= 170035 /*Policy Inception Date*/
			 LEFT JOIN MatterDetailValues mdvDateStart		WITH (NOLOCK) on mdvDateStart.MatterID = m.MatterID					AND mdvDateStart.DetailFieldID	= 170036 /*Policy Start Date*/
			 LEFT JOIN MatterDetailValues mdvDateEnd		WITH (NOLOCK) on mdvDateEnd  .MatterID = m.MatterID					AND mdvDateEnd.DetailFieldID	= 170037 /*Policy End Date*/
			 LEFT JOIN CustomerDetailValues cdv				WITH (NOLOCK) on cdv.CustomerID = l.CustomerID						AND cdv.DetailFieldID			= 170144 /*Affinity Details*/
			 LEFT JOIN ResourceListDetailValues rdv			WITH (NOLOCK) on rdv.ResourceListID = cdv.DetailValue				AND rdv.DetailFieldID			= 170127 /*Short Code*/
			 LEFT JOIN LookupListItems li					WITH (NOLOCK) on li.LookupListItemID = rdv.ValueInt
			 LEFT JOIN LeadTypeRelationship ltr				WITH (NOLOCK) on ltr.FromLeadID = l.LeadID							AND ltr.ToLeadTypeID = 1493
			 LEFT JOIN Matter mColls						WITH (NOLOCK) on mColls.MatterID = ltr.ToMatterID					AND ltr.ToLeadTypeID = 1493
			 LEFT JOIN MatterDetailValues mdvAccountID		WITH (NOLOCK) on mdvAccountID.MatterID = mColls.MatterID			AND mdvAccountID.DetailFieldID = 176973 /*Billing System Account ID*/
			 LEFT JOIN MatterDetailValues mdvPremium		WITH (NOLOCK) on mdvPremium.MatterID = m.MatterID					AND mdvPremium.DetailFieldID	= 177898 /*Current/New Premium - After Discount*/
			WHERE m.MatterID = @PolicyID
			AND l.LeadTypeID = 1492 /*Policy Admin*/
			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		)
	END

	RETURN @JsonData

END
















GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Harvest_Policy_JSON] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_Harvest_Policy_JSON] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Harvest_Policy_JSON] TO [sp_executeall]
GO
