SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2020-08-18
-- Description: Accepts the state code, PaymentType and PaymentFrequency and returns the relevent refundability of a fee
-- =============================================
CREATE FUNCTION [dbo].[_C605_ReturnTransactionRefundableByStatePaymentTypeAndFrequency]
(
	@StateCode VARCHAR(10),
	@PaymentType INT,
	@PaymentFreq INT
)
RETURNS DECIMAL(18,2)
AS
BEGIN

	DECLARE @YesNo INT

	/*Select the Fee based on the values passed in*/
	SELECT @YesNo = rldv_refundable.ValueMoney
	--SELECT rldv_StateCode.DetailValue AS [StateCode], rldv_paymentType.ValueInt AS [PaymentType], rldv_paymentFreq.ValueInt as [PaymentFrequency], rldv_refundable.ValueMoney as [Refundable]
	FROM ResourceListDetailValues rldv_StateCode WITH (NOLOCK)
	INNER JOIN ResourceListDetailValues rldv_paymentType WITH (NOLOCK) ON rldv_StateCode.ResourceListID = rldv_paymentType.ResourceListID
	INNER JOIN ResourceListDetailValues rldv_paymentFreq WITH (NOLOCK) ON rldv_StateCode.ResourceListID = rldv_paymentFreq.ResourceListID
	INNER JOIN ResourceListDetailValues rldv_refundable WITH (NOLOCK) ON rldv_StateCode.ResourceListID = rldv_refundable.ResourceListID
	WHERE rldv_StateCode.DetailFieldID = 313947 /*StateCode*/
	AND rldv_paymentType.DetailFieldID = 313948 /*PaymentType*/
	AND rldv_paymentFreq.DetailFieldID = 313949 /*PaymentFrequency*/
	AND rldv_refundable.DetailFieldID = 313951
	AND rldv_StateCode.DetailValue = @StateCode
	AND rldv_paymentType.ValueInt = @PaymentType
	AND rldv_paymentFreq.ValueInt = @PaymentFreq

	RETURN @YesNo

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C605_ReturnTransactionRefundableByStatePaymentTypeAndFrequency] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C605_ReturnTransactionRefundableByStatePaymentTypeAndFrequency] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C605_ReturnTransactionRefundableByStatePaymentTypeAndFrequency] TO [sp_executeall]
GO
