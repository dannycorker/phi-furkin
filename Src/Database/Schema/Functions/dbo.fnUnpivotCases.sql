SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Unpivot the columns in the Cases table 
-- =============================================
CREATE FUNCTION [dbo].[fnUnpivotCases] 
(	
	@CaseID int
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT ColumnName as [RawColumnName], '[!' + ColumnName + ']' as [FormattedColumnName], RealValue
	FROM
	(
		SELECT 
		COALESCE(CONVERT(varchar(255), c.AquariumStatusID), '') as [AquariumStatusID],
		COALESCE(CONVERT(varchar(255), c.CaseID), '') as [CaseID],
		COALESCE(CONVERT(varchar(255), c.CaseNum), '') as [CaseNum],
		COALESCE(CONVERT(varchar(255), c.CaseRef), '') as [CaseRef],
		COALESCE(CONVERT(varchar(255), c.ClientID), '') as [ClientID],
		COALESCE(CONVERT(varchar(255), c.ClientStatusID), '') as [ClientStatusID],
		COALESCE(CONVERT(varchar(255), c.DefaultContactID), '') as [DefaultContactID],
		COALESCE(CONVERT(varchar(255), c.LatestInProcessLeadEventID), '') as [LatestInProcessLeadEventID],
		COALESCE(CONVERT(varchar(255), c.LatestLeadEventID), '') as [LatestLeadEventID],
		COALESCE(CONVERT(varchar(255), c.LatestNonNoteLeadEventID), '') as [LatestNonNoteLeadEventID],
		COALESCE(CONVERT(varchar(255), c.LatestNoteLeadEventID), '') as [LatestNoteLeadEventID],
		COALESCE(CONVERT(varchar(255), c.LatestOutOfProcessLeadEventID), '') as [LatestOutOfProcessLeadEventID],
		COALESCE(CONVERT(varchar(255), c.LeadID), '') as [LeadID],
		COALESCE(CONVERT(varchar(255), c.WhenCreated), '') as [WhenCreated],
		COALESCE(CONVERT(varchar(255), c.WhenModified), '') as [WhenModified],
		COALESCE(CONVERT(varchar(255), c.WhoCreated), '') as [WhoCreated],
		COALESCE(CONVERT(varchar(255), c.WhoModified), '') as [WhoModified]
		FROM dbo.Cases c WITH (NOLOCK) 
		WHERE c.CaseID = @CaseID) p
	UNPIVOT
	(
		RealValue FOR ColumnName IN
		(
			[AquariumStatusID],[CaseID],[CaseNum],[CaseRef],[ClientID],[ClientStatusID],[DefaultContactID],[LatestInProcessLeadEventID],[LatestLeadEventID],[LatestNonNoteLeadEventID],[LatestNoteLeadEventID],[LatestOutOfProcessLeadEventID],[LeadID],[WhenCreated],[WhenModified],[WhoCreated],[WhoModified] 
		)
	) as unpvt
)




GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotCases] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnUnpivotCases] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotCases] TO [sp_executeall]
GO
