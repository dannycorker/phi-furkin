SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim
-- Create date: 2007-08-05
-- Description:	Returns 1 if the year passed in is a leap year
-- =============================================
CREATE FUNCTION [dbo].[fnIsLeapYear] 
(
	-- Add the parameters for the function here
	@Year int
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result bit

	-- 2008,2012,2016 will be leap years
	-- 2100,2200,2300 will not be leap years
	-- 2400,2800,3200 will be leap years
	IF ( (@Year%4 = 0) AND ( (@Year%100 <> 0) OR ( (@Year%100 = 0) AND (@Year%400 = 0) ) ) )
	BEGIN
		SET @Result = 1
	END
	ELSE
	BEGIN
		SET @Result = 0
	END

	-- Return the result of the function
	RETURN @Result

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsLeapYear] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnIsLeapYear] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsLeapYear] TO [sp_executeall]
GO
