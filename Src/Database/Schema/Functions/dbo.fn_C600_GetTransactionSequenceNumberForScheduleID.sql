SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-10-23
-- Description:	Find a RowNumber for a PurchasedProductPaymentSchedule entry
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetTransactionSequenceNumberForScheduleID]
(
	 @PurchasedProductPaymentScheduleID	INT
)
RETURNS INT
AS
BEGIN
	
	DECLARE  @SequenceNumber		INT
			,@PurchasedProductID	INT
			
	SELECT   @PurchasedProductID = pps.PurchasedProductID
	FROM PurchasedProductPaymentSchedule pps WITH ( NOLOCK )
	WHERE pps.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID

	;WITH Numbered AS
	(
	SELECT pps.PurchasedProductPaymentScheduleID, pps.CoverFrom, ROW_NUMBER() OVER (ORDER BY pps.CoverFrom,pps.ActualCollectionDate) [RowNum]
	FROM PurchasedProductPaymentSchedule pps WITH ( NOLOCK ) 
	WHERE pps.PurchasedProductID = @PurchasedProductID
	AND pps.PurchasedProductPaymentScheduleTypeID NOT IN ( 5 )
	)
	SELECT @SequenceNumber = n.RowNum
	FROM Numbered n 
	WHERE n.PurchasedProductPaymentScheduleID = @PurchasedProductPaymentScheduleID
	
	RETURN @SequenceNumber

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetTransactionSequenceNumberForScheduleID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetTransactionSequenceNumberForScheduleID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetTransactionSequenceNumberForScheduleID] TO [sp_executeall]
GO
