SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		James Lewis
-- Create date: 2017-12-05
-- Description:	Returns the policy matter from a claim
-- 2018-01-11 GPR Added to 600 implementation
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Claims_AutoReject_PreExisting]
(
	@CaseID INT
)
RETURNS INT	
AS
BEGIN
	
	DECLARE @MatterID INT ,
			@Result INT = 0,
			@Condition INT ,
			@LeadID INT
			
							
	SELECT @MatterID = m.MatterID
	FROM Matter m WITH ( NOLOCK ) 
	where m.CaseID = @CaseID 
	
	SELECT @Condition = dbo.fnGetSimpleDvAsInt(144504, @MatterID) 
	
	SELECT @LeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@MatterID)
	
	IF EXISTS ( SELECT * FROM TableRows tr WITH ( NOLOCK ) 
				INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 177426
				--INNER JOIN ResourceListDetailValues rldv WITH ( NOLOCK ) on rldv.ResourceListID = tdv.ResourceListID and rldv.DetailFieldID = 311805
				--CROSS APPLY dbo.fnTableOfIDsFromCSV(rldv.DetailValue) fn
				where tr.LeadID = @LeadID 
				AND tr.DetailFieldID =  177428
				AND tdv.ResourceListID = @Condition)
					
	BEGIN 
	
		SELECT @Result = 1
		
	END			

	Return  @Result

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Claims_AutoReject_PreExisting] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_Claims_AutoReject_PreExisting] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Claims_AutoReject_PreExisting] TO [sp_executeall]
GO
