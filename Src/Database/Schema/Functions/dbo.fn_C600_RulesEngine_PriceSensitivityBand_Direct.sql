SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-06-14
-- Description:	Returns a price sensitivity band based on an incoming postcode DIRECTLY, rather than using Rules Engine inputs.
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_RulesEngine_PriceSensitivityBand_Direct]
(
	@PostCode VARCHAR(7) = 'LE9 1UF'
)
RETURNS VARCHAR(10)	
AS
BEGIN

	DECLARE	 @PriceSensitivityBand	INT
			,@ProductOfPrimes		BIGINT = 1
			,@StaticMultiplier		DECIMAL(18,5)
			,@MultipliedProduct		DECIMAL(18,5)

	/*Each letter or digit in the post code gets allocated to a unique prime number*/
	DECLARE @TableOfPrimes TABLE ( ID INT IDENTITY, LetterOrDigit VARCHAR(1), PrimeInteger INT )
	INSERT	@TableOfPrimes ( LetterOrDigit, PrimeInteger )
	VALUES	('A',2),	('B',3),	('C',5),
			('D',7),	('E',11),	('F',13),
			('G',17),	('H',19),	('I',23),
			('J',29),	('K',31),	('L',37),
			('M',41),	('N',43),	('O',47),
			('P',53),	('Q',59),	('R',61),
			('S',67),	('T',71),	('U',73),
			('V',79),	('W',83),	('X',89),
			('Y',97),	('Z',101),	('1',103),
			('2',107),	('3',109),	('4',113),
			('5',127),	('6',131),	('7',137),
			('8',139),	('9',149),	('0',151),
			(' ',1)

	/*
	Multiply up all of the prime numbers
	SQL Server has no aggregate function for PRODUCT, so SUM the LOGs and take the Exponent of that SUM.
	*/
	;WITH AllocatedPrimes AS
	(
	SELECT t.N, pr.*, LOG(pr.PrimeInteger) [PrimeLog]
	FROM Tally t WITH ( NOLOCK ) 
	INNER JOIN @TableOfPrimes pr on pr.LetterOrDigit = SUBSTRING(@PostCode,t.n,1)
	WHERE t.N <= LEN(@PostCode)
	)
	SELECT @ProductOfPrimes = ROUND(EXP(SUM(ap.PrimeLog)),1)
	FROM AllocatedPrimes ap
	
	/*Scale by a static ratio (provided by Cardif)*/
	SELECT @MultipliedProduct = @ProductOfPrimes * 10000000000000.00000 / 1125899839733750.00000
	
	/*Each Price Sensitivity Band has its own allocation based on the decimal difference between the multiplied product and its integer*/
	DECLARE	@LookupGroup	TABLE ( DifferenceInPrimeInt DECIMAL(18,2), PriceSensitivityBand INT )
	INSERT	@LookupGroup	( DifferenceInPrimeInt, PriceSensitivityBand )
	VALUES	(0.00,1),	(0.07,2),	(0.13,3),	(0.20,4),
			(0.27,5),	(0.33,6),	(0.40,7),	(0.47,8),
			(0.53,9),	(0.60,10),	(0.67,11),	(0.73,12),
			(0.80,13),	(0.87,14),	(0.93,15),	(1.00,16)

	SELECT TOP 1 @PriceSensitivityBand = lg.PriceSensitivityBand
	FROM @LookupGroup lg 
	WHERE lg.DifferenceInPrimeInt < CAST(@MultipliedProduct as DECIMAL(18,5)) - CAST(FLOOR(@MultipliedProduct) as DECIMAL(18,5))
	ORDER BY lg.PriceSensitivityBand DESC

	RETURN	@PriceSensitivityBand

END


 






GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_RulesEngine_PriceSensitivityBand_Direct] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_RulesEngine_PriceSensitivityBand_Direct] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_RulesEngine_PriceSensitivityBand_Direct] TO [sp_executeall]
GO
