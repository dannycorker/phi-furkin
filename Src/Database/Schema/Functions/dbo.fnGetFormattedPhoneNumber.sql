SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-07-21
-- Description:	Format Phone Number
-- =============================================
CREATE FUNCTION [dbo].[fnGetFormattedPhoneNumber]
(
	@PhoneNumber VARCHAR(100)
)
RETURNS VARCHAR(100)
AS
BEGIN

	SELECT @PhoneNumber = REPLACE(@PhoneNumber, ' ','')

	SELECT @PhoneNumber = REPLACE(@PhoneNumber, '+44(0)','')

	SELECT @PhoneNumber = REPLACE(@PhoneNumber, '+44','0')
	
	IF @PhoneNumber LIKE '44%'
	BEGIN
	
		SELECT @PhoneNumber = RIGHT(@PhoneNumber, (LEN(@PhoneNumber)-2))
	
	END

	IF @PhoneNumber LIKE '7%' OR @PhoneNumber LIKE '1%' OR @PhoneNumber LIKE '2%' OR @PhoneNumber LIKE '8%'
	BEGIN

		SELECT @PhoneNumber = '0' + @PhoneNumber

	END

	RETURN @PhoneNumber

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetFormattedPhoneNumber] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetFormattedPhoneNumber] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetFormattedPhoneNumber] TO [sp_executeall]
GO
