SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2014-11-12
-- Description:	Get an output string from an input table of chars
-- ROH 2015-06-23 Changed to return VARCHAR(MAX). Kudos to Cathal on this one. Zen#29913 
-- =============================================
CREATE FUNCTION [dbo].[fnGetRTFEscapedValue] (
	@RawValue VARCHAR(2000)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	
	/* Return empty string by default */
	DECLARE @RTFEscapedValue VARCHAR(2000) = ''
	
	/* If there is anything to do... */
	IF ISNULL(@RawValue, '') > ''
	BEGIN
		/*
			Create a table of values from the input string, so that 'Numéro' becomes
			1 N 
			2 u 
			3 m 
			4 é 
			5 r 
			6 o
			
			Then translate each character into RTF using the RTFEscapeCode column in the CharMap table
			1 N 
			2 u 
			3 m 
			4 \'e9
			5 r 
			6 o
			
			Stitch it all back together again using the familiar SELECT @RTFEscapedValue += [table.column] technique
			For once, there is no need to add commas between the values / remove the unwanted trailing comma from the end.
		*/
		SELECT @RTFEscapedValue += ISNULL(cm.RTFEscapeCode, cm.CharValue) 
		FROM dbo.Tally t WITH (NOLOCK) 
		INNER JOIN dbo.CharMap cm WITH (NOLOCK) ON cm.AsciiValue = ASCII(SUBSTRING(@RawValue, t.N, 1)) 
		WHERE t.N <= LEN(@RawValue)
	END
	
	RETURN @RTFEscapedValue
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetRTFEscapedValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetRTFEscapedValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetRTFEscapedValue] TO [sp_executeall]
GO
