SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds / Alexandra Maguire
-- Create date: 2020-09-19
-- Description:	Returns values for discounts and taxes from latest WrittenPremium row for Policy - Created for Nessa's fields
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_DiscountValuesFromWrittenPremiumForPolicy]
(
	@MatterID INT
)
RETURNS 
	@Values TABLE (
		DiscountLabel VARCHAR(200),
		DiscountPercentage DECIMAL(18,2)

	)
AS
BEGIN

	DECLARE @WrittenPremiumID INT
	DECLARE @ClientID INT = [dbo].[fnGetPrimaryClientID]()

	SELECT @WrittenPremiumID = (SELECT TOP(1) WrittenPremiumID FROM WrittenPremium WITH (NOLOCK) WHERE MatterID = @MatterID)

	INSERT INTO @Values
	SELECT
	qpcDiscount.CheckpointName,
	ISNULL(qpcDiscount.ValueDecimal * 100.00,0.00)
	FROM WrittenPremium wp WITH (NOLOCK)
	INNER JOIN QuotePetProduct qp WITH (NOLOCK) on  qp.QuotePetProductID = wp.QuotePetProductID
	INNER JOIN QuotePetProductCheckpoint qpcDiscount WITH (NOLOCK) ON qp.QuotePetProductID = qpcDiscount.QuotePetProductID and qpcDiscount.CheckpointName IN ('Animal Shelter', 'Corporate Group Benefit', 'Medical Services', 'Multiple Pet', 'Strategic Partner', 'Military', 'Vet or Staff', 'Wellness', 'Internet Partner')
	WHERE wp.WrittenPremiumID = @WrittenPremiumID
	AND wp.ClientID = @ClientID

	INSERT INTO @Values
	SELECT
	'Total % Discount Applied',ISNULL(100 - qpcDiscount.ValueDecimal * 100.00,0.00)
	FROM WrittenPremium wp WITH (NOLOCK)
	INNER JOIN QuotePetProduct qp WITH (NOLOCK) on  qp.QuotePetProductID = wp.QuotePetProductID
	INNER JOIN QuotePetProductCheckpoint qpcDiscount WITH (NOLOCK) ON qp.QuotePetProductID = qpcDiscount.QuotePetProductID and qpcDiscount.CheckpointName = 'Discounts'
	WHERE wp.WrittenPremiumID = @WrittenPremiumID
	AND wp.ClientID = @ClientID
	
	RETURN

END


GO
