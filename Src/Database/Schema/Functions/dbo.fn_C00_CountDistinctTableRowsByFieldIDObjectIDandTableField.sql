SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2013-04-10
-- Description:	Count Table Rows by FieldID and ObjectID (Customer/Lead/Case/Matter) - distinct by a specific column
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_CountDistinctTableRowsByFieldIDObjectIDandTableField]
(
	@ObjectID INT,
	@DetailFieldID INT,
	@ColumnFieldID INT
)
RETURNS INT
AS
BEGIN

	DECLARE @ReturnValue INT = 0,
			@DetailFieldSubTypeID INT,
			@ClientID INT
	
	SELECT	@DetailFieldSubTypeID = df.LeadOrMatter,
			@ClientID = df.ClientID
	FROM dbo.DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @DetailFieldID
	
	;With DistinctRows as
	(
	SELECT DISTINCT ISNULL(CAST(tdv.ResourceListID AS VARCHAR(200)),tdv.DetailValue) [Value] /*Cast added by LB 2014-02-05 */
	FROM dbo.TableRows tr WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = @ColumnFieldID
	WHERE tr.DetailFieldID = @DetailFieldID
	AND tr.ClientID = @ClientID
	AND @ObjectID = CASE @DetailFieldSubTypeID	
					WHEN 1 THEN tr.LeadID
					WHEN 2 THEN tr.MatterID
					WHEN 10 THEN tr.CustomerID
					WHEN 11 THEN tr.CaseID
					END 
	)				
	SELECT @ReturnValue = COUNT(*)
	FROM DistinctRows

	RETURN @ReturnValue

END






GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_CountDistinctTableRowsByFieldIDObjectIDandTableField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_CountDistinctTableRowsByFieldIDObjectIDandTableField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_CountDistinctTableRowsByFieldIDObjectIDandTableField] TO [sp_executeall]
GO
