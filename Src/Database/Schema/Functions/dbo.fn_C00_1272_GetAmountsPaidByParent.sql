SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		James Lewis
-- Create date: 2016-06-27
-- Description:	Returns the full amount paid to a policy section
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetAmountsPaidByParent]
(
	@MatterID INT
)
RETURNS 
	@Amounts TABLE 
	(
		PlanTitle VARCHAR(500),
		Covered DECIMAL (18,2),
		Paid DECIMAL (18,2),
		Remaining DECIMAL (18,2)
	)
AS
BEGIN


DECLARE @CurrentPolicyMatterID INT 

/*First Get the policy matter*/ 


	SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID)

	DECLARE @ParentSections TABLE (ResourceListID INT,Section VARCHAR(500),SubSection VARCHAR(500),SumInsured DECIMAL (18,2),AllowedCound INT, LimitTypeID INT, LimitType VARCHAR(500))
	INSERT INTO @ParentSections 
	SELECT DISTINCT s.Out_ResourceListID AS ResourceListID, llSection.ItemValue AS Section, llSubSection.ItemValue AS SubSection, tdvSumInsured.ValueMoney AS SumInsured,
					tdvCount.ValueInt AS AllowedCount, tdvLimitType.ValueInt AS LimitTypeID, llType.ItemValue AS LimitType
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvRLID WITH (NOLOCK) ON r.TableRowID = tdvRLID.TableRowID AND tdvRLID.DetailFieldID = 144350
	INNER JOIN dbo.fn_C00_1272_GetPolicySectionRelationships(@CurrentPolicyMatterID) s ON tdvRLID.ResourceListID = s.ResourceListID
	INNER JOIN dbo.ResourceListDetailValues rdvSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189
	INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rdvSection.ValueInt = llSection.LookupListItemID 
	INNER JOIN dbo.ResourceListDetailValues rdvSubSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSubSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190
	INNER JOIN dbo.LookupListItems llSubSection WITH (NOLOCK) ON rdvSubSection.ValueInt = llSubSection.LookupListItemID 
	INNER JOIN dbo.TableDetailValues tdvRL WITH (NOLOCK) ON s.Out_ResourceListID = tdvRL.ResourceListID AND tdvRL.DetailFieldID = 144357
	INNER JOIN dbo.TableDetailValues tdvSumInsured WITH (NOLOCK) ON tdvRL.TableRowID = tdvSumInsured.TableRowID AND tdvSumInsured.DetailFieldID = 144358
	LEFT JOIN dbo.TableDetailValues tdvPetType WITH (NOLOCK) ON tdvRL.TableRowID = tdvPetType.TableRowID AND tdvPetType.DetailFieldID = 170013
	LEFT JOIN dbo.TableDetailValues tdvCount WITH (NOLOCK) ON tdvRL.TableRowID = tdvCount.TableRowID AND tdvCount.DetailFieldID = 144267
	LEFT JOIN dbo.TableDetailValues tdvLimitType WITH (NOLOCK) ON tdvRL.TableRowID = tdvLimitType.TableRowID AND tdvLimitType.DetailFieldID = 175388
	LEFT JOIN dbo.LookupListItems llType WITH (NOLOCK) ON tdvLimitType.ValueInt = llType.LookupListItemID
	INNER JOIN dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 1) m ON r.MatterID = m.MatterID 
	WHERE tdvRL.MatterID = @CurrentPolicyMatterID
	and llSubSection.ItemValue = '-' 
	AND @CurrentPolicyMatterID NOT IN (/*Quick Care Matter IDs*/792,793,794,795,796,/*TIME Product Matter IDs*/828,829,830,831,832,834,840,841,/*Condition Product Matter IDs*/706,707,708,712)
	
	DECLARE @AllSections TABLE (Section VARCHAR(500),SubSection VARCHAR(500),Total Decimal(18,2)) 
	INSERT INTO @AllSections (Section,SubSection,Total) 
	SELECT	pvt.Section, pvt.SubSection,pvt.Total --pvt.* --mdv.ValueInt AS ConditionID, rdvAilment1.DetailValue AS Ailment1, rdvAilment2.DetailValue AS Ailment2, llTpye.LookupListItemID AS ClaimRowTypeID
	FROM (
		SELECT r.MatterID, m.ParentID, r.TableRowID, 
		CASE 
			WHEN ISNULL(rlDf.DetailFieldID, df.DetailFieldID) IN (144349, 144351, 144362, 144354) THEN CONVERT(VARCHAR, tdv.ValueDate, 120)
			ELSE COALESCE(ll.ItemValue, ll2.ItemValue, tdv.DetailValue)
		END AS DetailValue,
		CASE ISNULL(rlDf.DetailFieldID, df.DetailFieldID)
			WHEN 146189 THEN 'Section'
			WHEN 146190 THEN 'SubSection'
			WHEN 144352 THEN 'Total'
			WHEN 149778 THEN 'ClaimRowType'
		END AS FieldCaption
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.fn_C00_1272_GetRelatedClaims(@MatterID, 0) m ON r.MatterID = m.MatterID 
		INNER JOIN dbo.Cases ca WITH (NOLOCK) ON m.CaseID = ca.CaseID
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON r.TableRowID = tdv.TableRowID 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON tdv.DetailFieldID = df.DetailFieldID 
		LEFT JOIN dbo.ResourceListDetailValues rdv WITH (NOLOCK) ON tdv.ResourceListID = rdv.ResourceListID AND rdv.DetailFieldID IN (146189, 146190)
		LEFT JOIN dbo.DetailFields rlDf WITH (NOLOCK) ON rdv.DetailFieldID = rlDf.DetailFieldID 
		LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON rdv.ValueInt = ll.LookupListItemID AND rlDf.LookupListID = ll.LookupListID AND rlDf.QuestionTypeID IN (2, 4)
		LEFT JOIN dbo.LookupListItems ll2 WITH (NOLOCK) ON tdv.ValueInt = ll2.LookupListItemID AND df.LookupListID = ll2.LookupListID AND df.QuestionTypeID IN (2, 4)
		WHERE r.DetailFieldID = 144355
		AND r.DetailFieldPageID = 16157
		AND ca.ClientStatusID IN (4470,4029)) src
		
	PIVOT (MAX(DetailValue) FOR FieldCaption IN (Section, SubSection, Total, Approved, ClaimRowType)) AS pvt
	ORDER BY ISNULL(Approved, dbo.fn_GetDate_Local()) DESC, MatterID DESC, TableRowID DESC


	INSERT INTO @Amounts (PlanTitle,Covered,Paid,Remaining)
	SELECT LTRIM(RIGHT(p.Section, LEN(p.Section) - CHARINDEX('-',p.Section,0))), p.SumInsured, SUM(a.Total) , p.SumInsured - SUM(a.Total)
	FROM @ParentSections p 
	INNER JOIN @AllSections a on a.Section = p.Section 
	GROUP BY p.Section,p.SumInsured

	RETURN

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetAmountsPaidByParent] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1272_GetAmountsPaidByParent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetAmountsPaidByParent] TO [sp_executeall]
GO
