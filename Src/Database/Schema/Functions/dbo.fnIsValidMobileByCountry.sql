SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2011-05-18
-- Description:	Validate a mobile phone number by country
-- =============================================
CREATE FUNCTION [dbo].[fnIsValidMobileByCountry]
(
	@MobileNumber varchar(20),	/* The number to validate */
	@CountryCode int			/* The country */
)
RETURNS bit
AS
BEGIN

	DECLARE 
	@IsValid bit = 1, 
	@PosOfSeven int,
	@Prefix varchar(4), 
	@NextDigit varchar(1), 
	@RemainingDigits varchar(20) 

	/* All mobiles */
	
	/* Remove all spaces for these tests */
	SELECT @MobileNumber = REPLACE(@MobileNumber, ' ', '')
	IF @MobileNumber IS NULL OR @MobileNumber = '' OR @MobileNumber LIKE '% %' OR LEN(@MobileNumber) < 10
	BEGIN
		/* NULL, blank, too short, too full of spaces */
		SELECT @IsValid = 0
	END
	ELSE
	BEGIN

		/* Specific country checks now */
		
		/* UK mobiles */
		IF @CountryCode = 232
		BEGIN

			/* Numbers that come from Excel can lose their leading zero, so help them out */
			IF @MobileNumber LIKE '7%'
			BEGIN
				SELECT @MobileNumber = '0' + @MobileNumber
			END
			
			SELECT @PosOfSeven = CHARINDEX('7', @MobileNumber)
			IF @PosOfSeven < 2
			BEGIN
				/* UK mobiles must contain a "7" */
				SELECT @IsValid = 0
			END
			ELSE
			BEGIN
				SELECT @Prefix = LEFT(@MobileNumber, @PosOfSeven - 1)
				IF @Prefix NOT IN ('0', '44', '+44', '0044')
				BEGIN
					/* Prefix is not valid */
					SELECT @IsValid = 0
				END
				ELSE
				BEGIN
					/* Get the rest of the number after the prefix, including the "7" */
					SELECT @RemainingDigits = RIGHT(@MobileNumber, LEN(@MobileNumber) - LEN(@Prefix))
					IF LEN(@RemainingDigits) <> 10
					BEGIN
						/* Remaining digits don't add up */
						SELECT @IsValid = 0
					END
				END
			END /* PosOfSeven */
			
		END /* UK */
		
	END /* Not NULL */

	RETURN @IsValid

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsValidMobileByCountry] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnIsValidMobileByCountry] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsValidMobileByCountry] TO [sp_executeall]
GO
