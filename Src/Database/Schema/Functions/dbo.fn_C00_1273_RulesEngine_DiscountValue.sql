SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-11-27
-- Description:	Returns a campaign code discount value as a 4-dp decimal
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_RulesEngine_DiscountValue]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS DECIMAL(18,4)
AS
BEGIN

	DECLARE  @ReturnValue	DECIMAL(18,4)	= 0.00
			,@CampaignCode	VARCHAR(2000)	= ''
	
	SELECT @CampaignCode = o.AnyValue2
	FROM @Overrides o 
	WHERE o.AnyValue1 = '175488' /*Campaign Code*/
	
	SELECT TOP 1 @ReturnValue = rdvValue.ValueMoney
	FROM ResourceListDetailValues rdv WITH ( NOLOCK )
	INNER JOIN ResourceListDetailValues rdvValue WITH ( NOLOCK ) on rdvValue.ResourceListID = rdv.ResourceListID AND rdvValue.DetailFieldID = 311767 /*Discount Value*/
	WHERE rdv.DetailFieldID = 177363 /*Campaign Code*/
	AND rdv.DetailValue = @CampaignCode

	RETURN @ReturnValue
 
END
 
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_DiscountValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_RulesEngine_DiscountValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_DiscountValue] TO [sp_executeall]
GO
