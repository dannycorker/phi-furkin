SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-11-29
-- Description:	Returns the correct policy row from the history for a claim and date
-- ROH 2015-12-03 Bug fix. Find policy rows on the Policy Matter not the Claim Lead (hangover from pre-Sterling LeadType structure) Zen#35730
-- GPR/JWG 2019-07-22 Move fn_C00_1272_GetPolicyMatterFromClaimMatter out of the WHERE clause
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetPolicyRowFromClaimAndDate]
(
	@ClaimMatterID INT,
	@Date DATE
)
RETURNS INT	
AS
BEGIN
	
	DECLARE @SchemeID INT, 
	@PolicyMatterID INT
	
	/* GPR/JWG 2019-07-22 */
	SELECT @PolicyMatterID = dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(@ClaimMatterID)

	SELECT @SchemeID = tdvScheme.ResourceListID
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON r.MatterID = m.MatterID --r.LeadID = m.LeadID
	INNER JOIN dbo.TableDetailValues tdvScheme WITH (NOLOCK) ON r.TableRowID = tdvScheme.TableRowID AND tdvScheme.DetailFieldID = 145665
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664
	WHERE 
	--m.MatterID = @ClaimMatterID
	/* GPR/JWG 2019-07-22 */
	/*m.MatterID = dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(@ClaimMatterID)*/
	m.MatterID = @PolicyMatterID
	AND @Date >= tdvStart.ValueDate
	AND @Date < tdvEnd.ValueDate
	
	RETURN @SchemeID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicyRowFromClaimAndDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_GetPolicyRowFromClaimAndDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicyRowFromClaimAndDate] TO [sp_executeall]
GO
