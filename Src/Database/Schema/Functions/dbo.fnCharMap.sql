SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2012-06-12
-- Description:	Show the ascii value of each character within a string
-- =============================================
CREATE FUNCTION [dbo].[fnCharMap]
(	
	@Input NVARCHAR(2000)
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT t.N AS [POSITION], 
	cm.CharValue, 
	cm.CharName, 
	cm.AsciiValue
	FROM dbo.Tally t WITH (NOLOCK) 
	INNER JOIN dbo.CharMap cm WITH (NOLOCK) ON cm.AsciiValue = ASCII(SUBSTRING(@Input, t.N, 1)) 
	WHERE t.N <= LEN(@Input)
)








GO
GRANT VIEW DEFINITION ON  [dbo].[fnCharMap] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnCharMap] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnCharMap] TO [sp_executeall]
GO
