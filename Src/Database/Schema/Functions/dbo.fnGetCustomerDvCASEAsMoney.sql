SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-06-12
-- Description:	Get CustomerDV directly #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetCustomerDvCASEAsMoney]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS money
AS
BEGIN

	DECLARE @ValueMoney AS money

	SELECT @ValueMoney = cdv.ValueMoney 
	FROM Matter c WITH (NOLOCK) 
	INNER JOIN CustomerDetailValues cdv WITH (NOLOCK) ON c.CustomerID = cdv.CustomerID AND cdv.DetailFieldID = @DetailFieldID
	WHERE c.CaseID = @CaseID 
	
	RETURN @ValueMoney

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerDvCASEAsMoney] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetCustomerDvCASEAsMoney] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerDvCASEAsMoney] TO [sp_executeall]
GO
