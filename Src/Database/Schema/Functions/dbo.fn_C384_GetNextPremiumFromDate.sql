SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-05-02
-- Description:	Returns the next premium from date for a policy calculation
-- Mods
-- DCM 2014-12-01 Copied from 266
-- DCM 2014-12-12 Allowed for case where no premium row
-- DCM 2016-01-18 Allow for renewals first month
-- DCM 2016-03-16 Allow for renewals first month whilst in earlier period
-- =============================================


CREATE FUNCTION [dbo].[fn_C384_GetNextPremiumFromDate]
(
	@MatterID INT,
	@Type INT
)
RETURNS 
@Data TABLE
(
	FromDate DATE,
	Type INT
)	
AS
BEGIN

	
	DECLARE @FromDate DATE
	
	-- We get the latest end date and add 1 day
	SELECT @FromDate = DATEADD(DAY, 1, MAX(tdvTo.ValueDate))
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvTo WITH (NOLOCK) ON r.TableRowID = tdvTo.TableRowID AND tdvTo.DetailFieldID = 170077
	INNER JOIN dbo.TableDetailValues tdvStatus WITH (NOLOCK) ON r.TableRowID = tdvStatus.TableRowID AND tdvStatus.DetailFieldID = 170078
	WHERE r.DetailFieldID = 170088
	AND r.MatterID = @MatterID
	AND tdvStatus.ValueInt IN (69898,69901,69903,72154) -- Only paid, waived, collecting and ready
	
	IF @FromDate IS NULL
	BEGIN

		-- If this date is still null then the first payment must have failed so set the type to first and get the earliest from date
		SELECT @Type = 69896 -- First
		
		SELECT @FromDate = MIN(tdvFrom.ValueDate)
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvFrom WITH (NOLOCK) ON r.TableRowID = tdvFrom.TableRowID AND tdvFrom.DetailFieldID = 170076
		WHERE r.DetailFieldID = 170088
		AND r.MatterID = @MatterID
		
	END
	
	IF @FromDate IS NULL
	BEGIN

		-- If this date is still null then no payments have been taken yet
		SELECT @Type = 69896 -- First
		
		SELECT @FromDate = MAX(tdvFrom.ValueDate)
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvFrom WITH (NOLOCK) ON r.TableRowID = tdvFrom.TableRowID AND tdvFrom.DetailFieldID = 145663
		INNER JOIN dbo.TableDetailValues tdvStatus WITH (NOLOCK) ON r.TableRowID = tdvStatus.TableRowID AND tdvStatus.DetailFieldID = 145666
		WHERE r.DetailFieldID = 170033
		AND tdvStatus.ValueInt=43002
		AND r.MatterID = @MatterID
		
	END
	
	-- allow for renewals (FromDate=Matter end date - calculating first renewal premium before actual renewal)
	--                     or (FromDate = Matter start date - calculating first renewal premium after actual renewal)
	IF @FromDate = ( SELECT ValueDate FROM MatterDetailValues startdate WITH (NOLOCK) 
						WHERE startdate.MatterID=@MatterID AND startdate.DetailFieldID=170036 )
		OR
	   @FromDate = ( SELECT ValueDate FROM MatterDetailValues enddate WITH (NOLOCK) 
						WHERE enddate.MatterID=@MatterID AND enddate.DetailFieldID=170037  )
					
		SELECT @Type = 69896 -- First					
	
	INSERT @Data (FromDate, Type)
	VALUES (@FromDate, @Type)
	
	RETURN

END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C384_GetNextPremiumFromDate] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C384_GetNextPremiumFromDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C384_GetNextPremiumFromDate] TO [sp_executeall]
GO
