SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2012-09-07
-- Description:	Debt Management Function to sum all creditor 'actual debt level' 
--				for a customer
--				excluding those that are not enforceable or have ORP override set
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_DM_SumEnforceableExcludingFixedORP]
(
@CustomerID int,
@DetailFieldID int,
@EnforceDetailFieldID int,
@ORPDetailFieldID int
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE @ReturnValue VARCHAR(2000)

	
	SELECT @ReturnValue = ISNULL(SUM(mdv.ValueMoney),0)
	FROM MatterDetailValues mdv WITH (NOLOCK)
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND m.CustomerID = @CustomerID
	INNER JOIN MatterDetailValues enf WITH (NOLOCK) ON m.MatterID=enf.MatterID 
		AND enf.DetailFieldID=@EnforceDetailFieldID 
		AND enf.ValueInt=0
	INNER JOIN MatterDetailValues orp WITH (NOLOCK) ON m.MatterID=orp.MatterID 
		AND orp.DetailFieldID=@ORPDetailFieldID 
		AND orp.ValueMoney=0
	WHERE mdv.DetailFieldID = @DetailFieldID 

	RETURN @ReturnValue

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_DM_SumEnforceableExcludingFixedORP] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_DM_SumEnforceableExcludingFixedORP] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_DM_SumEnforceableExcludingFixedORP] TO [sp_executeall]
GO
