SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2013-04-05
-- Description:	Sum Detail Values by ObjectID
--				Allows summing matter/case/etc fields by lead/customer/etc
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_SumDetailFieldsByObjectID]
(
@ObjectID int,
@ObjectDetailFieldSubTypeID int,
@DetailFieldID int
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE @ReturnValue VARCHAR(2000),
			@FieldDetailFieldSubTypeID int
	
	SELECT @FieldDetailFieldSubTypeID = df.LeadOrMatter
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @DetailFieldID

	/*Could add in Table fields to all of these*/
	IF @ObjectDetailFieldSubTypeID = 10 /*Customer.  Can have Customer,Lead,Case or Matter fields*/
	BEGIN
		IF @FieldDetailFieldSubTypeID = 1
		BEGIN
			SELECT @ReturnValue = SUM(ldv.ValueMoney)
			FROM LeadDetailValues ldv WITH (NOLOCK) 
			INNER JOIN dbo.Lead l WITH (NOLOCK) on l.LeadID = ldv.LeadID
			WHERE ldv.DetailFieldID = @DetailFieldID
			and l.CustomerID = @ObjectID
		END
		ELSE
		IF @FieldDetailFieldSubTypeID = 2
		BEGIN
			SELECT @ReturnValue = SUM(mdv.ValueMoney)
			FROM MatterDetailValues mdv WITH (NOLOCK) 
			INNER JOIN dbo.Matter m WITH (NOLOCK) on m.MatterID = mdv.MatterID
			WHERE mdv.DetailFieldID = @DetailFieldID
			and m.CustomerID = @ObjectID
		END
		ELSE
		IF @FieldDetailFieldSubTypeID = 11
		BEGIN
			SELECT @ReturnValue = SUM(cdv.ValueMoney)
			FROM CaseDetailValues cdv WITH (NOLOCK) 
			INNER JOIN dbo.Cases c WITH (NOLOCK) on c.CaseID = cdv.CaseID
			INNER JOIN dbo.Lead l WITH (NOLOCK) on l.LeadID = c.LeadID
			WHERE cdv.DetailFieldID = @DetailFieldID
			and l.CustomerID = @ObjectID
		END
		ELSE
		IF @FieldDetailFieldSubTypeID = 10
		BEGIN
			SELECT @ReturnValue = SUM(cdv.ValueMoney)
			FROM CustomerDetailValues cdv WITH (NOLOCK) 
			WHERE cdv.DetailFieldID = @DetailFieldID
			and cdv.CustomerID = @ObjectID
		END
	END
	
	IF @ObjectDetailFieldSubTypeID = 1 /*Lead.  Can have Lead,Case or Matter fields*/
	BEGIN
		IF @FieldDetailFieldSubTypeID = 1
		BEGIN
			SELECT @ReturnValue = SUM(ldv.ValueMoney)
			FROM LeadDetailValues ldv WITH (NOLOCK) 
			WHERE ldv.DetailFieldID = @DetailFieldID
			and ldv.LeadID = @ObjectID
		END
		ELSE
		IF @FieldDetailFieldSubTypeID = 2
		BEGIN
			SELECT @ReturnValue = SUM(mdv.ValueMoney)
			FROM MatterDetailValues mdv WITH (NOLOCK) 
			WHERE mdv.DetailFieldID = @DetailFieldID
			and mdv.LeadID = @ObjectID
		END
		ELSE
		IF @FieldDetailFieldSubTypeID = 11
		BEGIN
			SELECT @ReturnValue = SUM(cdv.ValueMoney)
			FROM CaseDetailValues cdv WITH (NOLOCK) 
			INNER JOIN dbo.Cases c WITH (NOLOCK) on c.CaseID = cdv.CaseID
			WHERE cdv.DetailFieldID = @DetailFieldID
			and c.LeadID = @ObjectID
		END
	END

	IF @ObjectDetailFieldSubTypeID = 11 /*Case.  Can have Case or Matter fields*/
	BEGIN
		IF @FieldDetailFieldSubTypeID = 2
		BEGIN
			SELECT @ReturnValue = SUM(mdv.ValueMoney)
			FROM MatterDetailValues mdv WITH (NOLOCK)
			INNER JOIN dbo.Matter m WITH (NOLOCK) on m.MatterID = mdv.MatterID 
			WHERE mdv.DetailFieldID = @DetailFieldID
			and m.CaseID = @ObjectID
		END
		ELSE
		IF @FieldDetailFieldSubTypeID = 11
		BEGIN
			SELECT @ReturnValue = SUM(cdv.ValueMoney)
			FROM CaseDetailValues cdv WITH (NOLOCK) 
			WHERE cdv.DetailFieldID = @DetailFieldID
			and cdv.CaseID = @ObjectID
		END
	END
	
	RETURN @ReturnValue

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumDetailFieldsByObjectID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_SumDetailFieldsByObjectID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumDetailFieldsByObjectID] TO [sp_executeall]
GO
