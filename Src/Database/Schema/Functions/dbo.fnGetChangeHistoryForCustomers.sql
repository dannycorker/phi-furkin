SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[fnGetChangeHistoryForCustomers] 
(	
	@ClientID INT,
	@SubClientID INT,
	@UserCustomerFields tvpIntInt READONLY,
	@LowerDateRange DATETIME,
	@UpperDateRange DATETIME = NULL
)
RETURNS 
@ChangeHistory TABLE 
(
	TableRowID INT,
	CustomerID INT,
	DetailFieldID INT,
	WhenChanged DATETIME,
	DetailValue VARCHAR(2000)
)
AS
BEGIN

	IF @UpperDateRange IS NULL
	BEGIN
		SELECT @UpperDateRange = dbo.fn_GetDate_Local()
	END
	
	;WITH
	HistoryData AS
	(
			SELECT CustomerID, 0 TableRowID, DetailFieldID, WhenChanged, DetailValue 
			FROM [fnUnpivotCustomerHistoryWithDetailFields] (@ClientID, @SubClientID)
		UNION ALL
			SELECT dvh.CustomerID, 0 TableRowID, dvh.DetailFieldID, dvh.WhenSaved WhenChanged, dvh.FieldValue DetailValue
			FROM DetailValueHistory dvh WITH (NOLOCK)
			inner join @UserCustomerFields cids on cids.ID1 = dvh.CustomerID AND dvh.ClientID = @ClientID AND dvh.DetailFieldID = cids.ID2
		UNION ALL
			SELECT tdvh.CustomerID, tr.TableRowID, tdvh.DetailFieldID, tdvh.WhenSaved WhenChanged, tdvh.DetailValue
			FROM @UserCustomerFields cids 
			INNER JOIN TableDetailValuesHistory tdvh WITH (NOLOCK) on 
				 tdvh.CustomerID = cids.ID1 AND tdvh.DetailFieldID = cids.ID2
			LEFT JOIN TableRows tr WITH (NOLOCK) on tdvh.TableRowID = tr.TableRowID
	),
	ChangeDataPrev AS
	(
		select CustomerID, DetailFieldID, TableRowID, MAX(WhenChanged) WhenChanged
		from HistoryData
		where WhenChanged <= @LowerDateRange
		group by CustomerID, DetailFieldID, TableRowID
	),
	ChangeDataCurr AS
	(
		select CustomerID, DetailFieldID, TableRowID, MAX(WhenChanged) WhenChanged
		from HistoryData
		where WhenChanged <= @UpperDateRange
		group by CustomerID, DetailFieldID, TableRowID
	)
	INSERT INTO @ChangeHistory
	SELECT
		hcurr.TableRowID TableRowID,
		hcurr.CustomerID CustomerID,
		hcurr.DetailFieldID DetailFieldID,
		hcurr.WhenChanged WhenChanged,
		COALESCE(hcurr.DetailValue, hprev.DetailValue) DetailValue
	FROM	
		ChangeDataCurr cdc
		left join ChangeDataPrev cdp 
			on cdc.CustomerID = cdp.CustomerID and cdc.DetailFieldID = cdp.DetailFieldID and cdc.TableRowID = cdp.TableRowID
		left join HistoryData hprev 
			on cdp.WhenChanged = hprev.WhenChanged and cdp.CustomerID = hprev.CustomerID and cdp.DetailFieldID = hprev.DetailFieldID and cdp.TableRowID = hprev.TableRowID
		left join HistoryData hcurr 
			on cdc.WhenChanged = hcurr.WhenChanged and cdc.CustomerID = hcurr.CustomerID and cdc.DetailFieldID = hcurr.DetailFieldID and cdc.TableRowID = hcurr.TableRowID
	where 
	(
		hprev.DetailValue <> hcurr.DetailValue
		or
		hprev.CustomerID is null or hcurr.CustomerID is null
	)
	group by hcurr.TableRowID,
			hcurr.CustomerID,
			hcurr.DetailFieldID,
			hcurr.WhenChanged,
			COALESCE(hcurr.DetailValue, hprev.DetailValue)

	RETURN
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetChangeHistoryForCustomers] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnGetChangeHistoryForCustomers] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetChangeHistoryForCustomers] TO [sp_executeall]
GO
