SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alexandra Maguire
-- Create date: 2020-11-02
-- Description:	Cancellations days from USCancellationRules
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetDateAddXDaysCancel]
(
	@MatterID INT
)
RETURNS DATE
AS
BEGIN

	DECLARE @Days INT,
			@Date DATE,
			@Zip VARCHAR(6),
			@State VARCHAR(6),
			@StartDate DATE, 
			@DaysIntoPolicyYear INT,
			@IsRenewal BIT, 
			@CoolingOffPeriod INT,
			@PurchasedProductID INT,
			@NotificationDate DATE

	SELECT @Zip = cu.PostCode
	FROM Customers cu WITH (NOLOCK) 
	JOIN Matter m WITH (NOLOCK) ON m.CustomerID = cu.CustomerID
	WHERE m.MatterID = @MatterID

	SELECT @State = us.StateCode
	FROM StateByZip sbz WITH (NOLOCK) 
	INNER JOIN UnitedStates us WITH (NOLOCK) ON us.StateID = sbz.StateID
	WHERE sbz.Zip = @Zip

	SELECT @StartDate = dbo.fnGetSimpleDvAsDate(170036,@MatterID)

	SELECT @PurchasedProductID = p.PurchasedProductID
	FROM PurchasedProduct p WITH (NOLOCK) 
	WHERE p.ObjectID = @MatterID 
	AND p.ValidFrom < dbo.fn_GetDate_Local()
	AND NOT EXISTS (SELECT * FROM PurchasedProduct pp WITH (NOLOCK) 
					WHERE pp.PurchasedProductID > p.PurchasedProductID 
					AND pp.ObjectID = p.ObjectID
					AND pp.AccountID = p.AccountID
					AND pp.ValidFrom < dbo.fn_GetDate_Local())

	SELECT @NotificationDate = ppps.PaymentDate 
	FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
	INNER JOIN Customers cu WITH (NOLOCK) on cu.CustomerID = ppps.CustomerID and cu.Test = 0
	WHERE ppps.PurchasedProductID = @PurchasedProductID 
	AND ppps.PaymentStatusID = 4
	AND NOT EXISTS (SELECT * FROM PurchasedProductPaymentSchedule pppsSub WITH (NOLOCK) 
					WHERE pppsSub.PurchasedProductID = @PurchasedProductID 
					AND pppsSub.PaymentDate > ppps.PaymentDate 
					AND pppsSub.PaymentStatusID = 4) 

	SELECT @DaysIntoPolicyYear = DATEDIFF(DD,@StartDate,dbo.fn_GetDate_Local())
			
	SELECT @IsRenewal = 
		CASE 
			WHEN COUNT (*) > 1 THEN 1 
			ELSE 0 
		END 
	FROM TableRows tr WITH (NOLOCK) 
	WHERE tr.DetailFieldID = 170033
	AND tr.MatterID = @MatterID

	/*@State, @CancellationType, @IsRenewal, @DaysIntoPolicyYear*/
	SELECT @CoolingOffPeriod = dbo.fn_C00_USCancellationCoolingOffPeriod (@State, 1, @IsRenewal, @DaysIntoPolicyYear) + 5 /*mailing days*/

	SELECT @Date = DATEADD(DAY,@CoolingOffPeriod,@NotificationDate)

	RETURN @Date

END
GO
