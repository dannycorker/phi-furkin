SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-06-01
-- Description:	Return a prioritised list of phone numbers for a greenlight customer
-- =============================================
CREATE FUNCTION [dbo].[fn_C272_GetCustomerNumberList]
(
	@CustomerID INT
)
RETURNS 
@TableOut TABLE 
(
	 NumberType			VARCHAR(100)
	,Number				VARCHAR(50)
	,AquariumField		VARCHAR(50)
	,Priority			INT
)
AS
BEGIN

	DECLARE  @Priority	TABLE ( NumberType VARCHAR(100), Priority INT )
	INSERT	 @Priority ( NumberType, Priority )
	VALUES	( 'HomeTelephone', 1 )
			,( 'MobileTelephone', 2 )
			,( 'DaytimeTelephoneNumber', 3 )
			,( 'WorksTelephone', 4 )
			,( 'CompanyTelephone', 5 )

	INSERT INTO @TableOut ( NumberType, Number, AquariumField, Priority )
	SELECT 
		CASE ROW_NUMBER() OVER (ORDER BY p.Priority)
			WHEN 1 THEN 'Primary'
			ELSE 'Alternative' 
			END [NumberType]
			,			
			REPLACE(unpvt.Number,' ','') [Number]
			,
			unpvt.NumberType [AquariumField]
			,
			ROW_NUMBER() OVER (ORDER BY p.Priority) [Row]
	FROM
	(
	SELECT CustomerID, HomeTelephone,MobileTelephone,DaytimeTelephoneNumber,WorksTelephone,CompanyTelephone
	FROM Customers cu WITH (NOLOCK) 
	WHERE cu.CustomerID = @CustomerID
	) c
	UNPIVOT
	(	Number FOR NumberType
		IN(HomeTelephone,MobileTelephone,DaytimeTelephoneNumber,WorksTelephone,CompanyTelephone)
	) AS unpvt
	INNER JOIN @Priority p on p.NumberType = unpvt.NumberType 
	WHERE unpvt.Number > ''
	
	RETURN 

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C272_GetCustomerNumberList] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C272_GetCustomerNumberList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C272_GetCustomerNumberList] TO [sp_executeall]
GO
