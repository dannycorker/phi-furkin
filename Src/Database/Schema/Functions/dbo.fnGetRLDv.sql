SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 28-July-2010
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[fnGetRLDv]
(
	@DetailFieldID		int, /* L/M Field */
	@ResourceFieldID	int, /* ResourceList Detail Field ID */
	@CaseID				int
)
RETURNS varchar(2000)
AS
BEGIN

DECLARE @DetailValue as varchar(2000),
		@LeadOrMatter int

	Select @LeadOrMatter = LeadOrmatter
	From DetailFields with (nolock) 
	Where DetailFieldID = @DetailFieldID

	If @LeadOrMatter = 1
	Begin

		Select @DetailValue = rldv.DetailValue 
		From Cases c with (nolock) 
		Inner Join LeadDetailValues ldv with (nolock) on c.LeadID = ldv.LeadID and ldv.DetailFieldID = @DetailFieldID
		Inner Join ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = ldv.ValueInt and rldv.DetailFieldID = @ResourceFieldID
		Where c.CaseID = @CaseID 
	
	End

	If @LeadOrMatter = 2
	Begin

		Select @DetailValue = rldv.DetailValue 
		From Matter m with (nolock) 
		Inner Join MatterDetailValues mdv with (nolock) on m.MatterID = mdv.MatterID and mdv.DetailFieldID = @DetailFieldID
		Inner Join ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = mdv.ValueInt and rldv.DetailFieldID = @ResourceFieldID
		Where m.CaseID = @CaseID
	
	End
	
	If @LeadOrMatter = 10
	Begin

		Select @DetailValue = rldv.DetailValue 
		From Matter m with (nolock) 
		Inner Join CustomerDetailValues cdv with (nolock) on m.CustomerID = cdv.CustomerID and cdv.DetailFieldID = @DetailFieldID
		Inner Join ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = cdv.ValueInt and rldv.DetailFieldID = @ResourceFieldID
		Where m.CaseID = @CaseID 
	
	End

	RETURN @DetailValue

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetRLDv] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetRLDv] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetRLDv] TO [sp_executeall]
GO
