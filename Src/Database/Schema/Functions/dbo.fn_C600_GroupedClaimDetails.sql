SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		James Lewis
-- Create date: 2016-07-25
-- Description:	Returns a table of policy sections and their mappings to parent sections
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GroupedClaimDetails]
(
	@ClaimMatterID INT
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT	pvt.*, mdv.ValueInt AS ConditionID, rdvAilment1.DetailValue + ' - ' + pvt.SubSection AS Ailment1, rdvAilment2.DetailValue AS Ailment2, llTpye.LookupListItemID AS ClaimRowTypeID
	FROM (
		SELECT r.MatterID, m.ParentID, r.TableRowID, 
		CASE 
			WHEN ISNULL(rlDf.DetailFieldID, df.DetailFieldID) IN (144349, 144351, 144362, 144354) THEN CONVERT(VARCHAR, tdv.ValueDate, 120)
			ELSE COALESCE(ll.ItemValue, ll2.ItemValue, tdv.DetailValue)
		END AS DetailValue,
		CASE ISNULL(rlDf.DetailFieldID, df.DetailFieldID)
			WHEN 146189 THEN 'Section'
			WHEN 146190 THEN 'SubSection'
			WHEN 144349 THEN 'TreatmentStart'
			WHEN 144351 THEN 'TreatmentEnd'
			WHEN 144353 THEN 'Claim'
			WHEN 146179 THEN 'UserDeductions'
			WHEN 145678 THEN 'Settle'
			WHEN 144352 THEN 'Total'
			WHEN 144362 THEN 'Approved'
			WHEN 144354 THEN 'Paid'
			WHEN 145679 THEN 'Payee'
			WHEN 146406 THEN 'Excess'
			WHEN 146407 THEN 'CoInsurance'
			WHEN 146408 THEN 'Limit'
			WHEN 147001 THEN 'OutsidePolicyCover'
			WHEN 147434 THEN 'ExcessRebate'
			WHEN 147602 THEN 'PayTo'
			WHEN 147605 THEN 'OwedPremium'
			WHEN 149778 THEN 'ClaimRowType'
			WHEN 158537 THEN 'SAPPaymentResult'
			WHEN 158538 THEN 'SAPDocumentCode'
			WHEN 158802 THEN 'VoluntaryExcess'
		END AS FieldCaption
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.fn_C00_1272_GetRelatedClaims(@ClaimMatterID, 0) m ON r.MatterID = m.MatterID 
		INNER JOIN dbo.Cases ca WITH (NOLOCK) ON m.CaseID = ca.CaseID
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON r.TableRowID = tdv.TableRowID 
		INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON tdv.DetailFieldID = df.DetailFieldID 
		LEFT JOIN dbo.ResourceListDetailValues rdv WITH (NOLOCK) ON tdv.ResourceListID = rdv.ResourceListID AND rdv.DetailFieldID IN (146189, 146190)
		LEFT JOIN dbo.DetailFields rlDf WITH (NOLOCK) ON rdv.DetailFieldID = rlDf.DetailFieldID 
		LEFT JOIN dbo.LookupListItems ll WITH (NOLOCK) ON rdv.ValueInt = ll.LookupListItemID AND rlDf.LookupListID = ll.LookupListID AND rlDf.QuestionTypeID IN (2, 4)
		LEFT JOIN dbo.LookupListItems ll2 WITH (NOLOCK) ON tdv.ValueInt = ll2.LookupListItemID AND df.LookupListID = ll2.LookupListID AND df.QuestionTypeID IN (2, 4)
		WHERE r.DetailFieldID = 144355
		AND r.DetailFieldPageID = 16157
		) src
	PIVOT (MAX(DetailValue) FOR FieldCaption IN (Section, SubSection, TreatmentStart, TreatmentEnd, 
												Claim, UserDeductions, Settle, Total, Approved, Paid, Payee, Excess, VoluntaryExcess,
												CoInsurance, Limit, OutsidePolicyCover, ExcessRebate, PayTo, OwedPremium, ClaimRowType, 
												SAPPaymentResult, SAPDocumentCode)) AS pvt
	LEFT JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON pvt.MatterID = mdv.MatterID AND mdv.DetailFieldID = 144504
	LEFT JOIN dbo.ResourceListDetailValues rdvAilment1 WITH (NOLOCK) ON rdvAilment1.ResourceListID = mdv.ValueInt AND rdvAilment1.DetailFieldID = 144340
	LEFT JOIN dbo.ResourceListDetailValues rdvAilment2 WITH (NOLOCK) ON rdvAilment2.ResourceListID = mdv.ValueInt AND rdvAilment2.DetailFieldID = 144341
	INNER JOIN dbo.LookupListItems llTpye WITH (NOLOCK) ON pvt.ClaimRowType = llTpye.ItemValue AND llTpye.LookupListID = 3924
	--ORDER BY ISNULL(Paid, dbo.fn_GetDate_Local()) DESC, ISNULL(Approved, dbo.fn_GetDate_Local()) DESC, TreatmentStart DESC, MatterID DESC, TableRowID DESC
	
)
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GroupedClaimDetails] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_GroupedClaimDetails] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GroupedClaimDetails] TO [sp_executeall]
GO
