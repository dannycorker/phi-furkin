SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-12-03
-- Description:	DateAdd equivalent for InvokeFunctions
-- =============================================
CREATE FUNCTION [dbo].[fnDateAdd]
(
@DatePart VARCHAR(10),
@Increment INT,
@FromDate DATETIME,
@OutputDateFormat INT
)
RETURNS VARCHAR(20)
AS
BEGIN

	DECLARE @ReturnValue VARCHAR(20) = ''
	DECLARE @DateValue	DATETIME

	/*Somebody please do this better*/
	SELECT @DateValue  = CASE @DatePart
							WHEN 'year' THEN DATEADD(year,@Increment,@FromDate)
							WHEN 'yy' THEN DATEADD(yy,@Increment,@FromDate)
							WHEN 'yyyy' THEN DATEADD(yyyy,@Increment,@FromDate)
							WHEN 'quarter' THEN DATEADD(quarter,@Increment,@FromDate)
							WHEN 'qq' THEN DATEADD(qq,@Increment,@FromDate)
							WHEN 'q' THEN DATEADD(q,@Increment,@FromDate)
							WHEN 'month' THEN DATEADD(month,@Increment,@FromDate)
							WHEN 'mm' THEN DATEADD(mm,@Increment,@FromDate)
							WHEN 'm' THEN DATEADD(m,@Increment,@FromDate)
							WHEN 'dayofyear' THEN DATEADD(dayofyear,@Increment,@FromDate)
							WHEN 'dy' THEN DATEADD(dy,@Increment,@FromDate)
							WHEN 'y' THEN DATEADD(y,@Increment,@FromDate)
							WHEN 'day' THEN DATEADD(day,@Increment,@FromDate)
							WHEN 'dd' THEN DATEADD(dd,@Increment,@FromDate)
							WHEN 'd' THEN DATEADD(d,@Increment,@FromDate)
							WHEN 'week' THEN DATEADD(week,@Increment,@FromDate)
							WHEN 'wk' THEN DATEADD(wk,@Increment,@FromDate)
							WHEN 'ww' THEN DATEADD(ww,@Increment,@FromDate)
							WHEN 'hour' THEN DATEADD(hour,@Increment,@FromDate)
							WHEN 'hh' THEN DATEADD(hh,@Increment,@FromDate)
							WHEN 'minute' THEN DATEADD(minute,@Increment,@FromDate)
							WHEN 'mi' THEN DATEADD(mi,@Increment,@FromDate)
							WHEN 'n' THEN DATEADD(n,@Increment,@FromDate)
							WHEN 'second' THEN DATEADD(second,@Increment,@FromDate)
							WHEN 'ss' THEN DATEADD(ss,@Increment,@FromDate)
							WHEN 's' THEN DATEADD(s,@Increment,@FromDate)
							WHEN 'millisecond' THEN DATEADD(millisecond,@Increment,@FromDate)
							WHEN 'ms' THEN DATEADD(ms,@Increment,@FromDate)
							WHEN 'microsecond' THEN DATEADD(microsecond,@Increment,@FromDate)
							WHEN 'mcs' THEN DATEADD(mcs,@Increment,@FromDate)
							WHEN 'nanosecond' THEN DATEADD(nanosecond,@Increment,@FromDate)
							WHEN 'ns	' THEN DATEADD(ns	,@Increment,@FromDate)
						  ELSE NULL
						  END
	
	SELECT @ReturnValue = CONVERT(VARCHAR,@DateValue,@OutputDateFormat)
	RETURN @ReturnValue

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnDateAdd] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnDateAdd] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnDateAdd] TO [sp_executeall]
GO
