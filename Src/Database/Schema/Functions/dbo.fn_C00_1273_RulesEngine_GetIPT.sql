SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-08-08
-- Description:	returns the IPT gross factor for the UK tax region specified.  
--              The factor is the multiplier required to add IPT to a net premium
-- Mods
-- 2020-02-18 GPR added declaration and assignment of @CountryID from @ClientID
-- 2020-05-12 GPR for JIRA SDAAG-26 | Adjusted the way we pick up the Postcode as this was NULL in quoting meaning reduced premium
-- 2020-05-14 GPR | Replace hard-coded ClientID with function
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_RulesEngine_GetIPT]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS DECIMAL(18,4)	
AS
BEGIN
	
	DECLARE @IPT DECIMAL(18,4),
			@Postcode VARCHAR(20),
			@RegionString VARCHAR(500),
			@YearStart DATE,
			@CountryID INT

	/*Get ClientID and CountryID*/
	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()

	SELECT @CountryID = CountryID
	FROM Clients WITH (NOLOCK)
	WHERE ClientID = @ClientID
	
	/*Year start date is always passed in*/
	SELECT @YearStart = AnyValue2
	FROM @Overrides
	WHERE AnyID = 2
	AND AnyValue1 = 'YearStart'
	/*Except not always from RulesEngine test interface so default it*/
	IF @YearStart IS NULL SELECT @YearStart=dbo.fn_GetDate_Local()

	/*Get Postcode*/
	IF @CustomerID IS NOT NULL
	BEGIN
		SELECT @Postcode=c.PostCode 
		FROM Customers c WITH (NOLOCK) WHERE c.CustomerID=@CustomerID
	END
	
	IF NULLIF(@Postcode,'') IS NULL /*GPR 2020-05-12*/
	BEGIN
		SELECT @Postcode=AnyValue2
		FROM @Overrides
		WHERE AnyID = 2
		AND AnyValue1 = 'PostCode'
	END

	-- Tax Free
	SELECT TOP 1 @RegionString=Region, @IPT=i.GrossMultiplier 
	FROM IPT i WITH (NOLOCK) 
	WHERE i.CountryID= @CountryID -- 232
	AND (i.[From] IS NULL OR @YearStart >= i.[From])
	AND (i.[To] IS NULL OR @YearStart <= i.[To])
	AND i.Region IS NOT NULL

	IF @RegionString IS NOT NULL
	BEGIN
		IF NOT EXISTS (SELECT * FROM [dbo].[fnTableOfValues] (@RegionString,',') rlist WHERE @Postcode LIKE rlist.AnyValue + '%')
		SELECT @IPT = NULL
	END
	
	IF @IPT IS NULL
	BEGIN
		SELECT TOP 1 @IPT=i.GrossMultiplier 
		FROM IPT i WITH (NOLOCK) 
		WHERE i.CountryID= @CountryID -- 232
		AND (i.[From] IS NULL OR @YearStart >= i.[From])
		AND (i.[To] IS NULL OR @YearStart <= i.[To])
		AND i.Region IS NULL
	END

	RETURN ISNULL(@IPT,1.0000)
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_GetIPT] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_RulesEngine_GetIPT] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_GetIPT] TO [sp_executeall]
GO
