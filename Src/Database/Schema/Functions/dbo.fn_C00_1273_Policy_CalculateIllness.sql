SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-01-08
-- Description:	Calculates the base level quote information and returns the user variables
-- SB 2013-06-28 Added effective date for MB tax change
-- RH 2013-09-16 Determine Brand from Product and pass to later functions
-- SB 2014-06-25 Added Language ID
-- DCM 2014-09-15 Copied from 235
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_Policy_CalculateIllness] 
(
	@PostalCode VARCHAR(100),
	@ProvinceCode CHAR(2),
	@TotalPets INT,
	@IllnessProductID INT,
	@BreedID INT,
	@DateOfBirth DATE,
	@DeductableLevel INT,
	@CoInsuranceLevel INT,
	@Promotions dbo.tvpInt READONLY,
	@CalculationDate DATE,
	@EffectiveDate DATE
)
RETURNS
@Calculations TABLE
(
	DeductableLevel INT,
	CoInsuranceLevel INT,
	CoInsDeductAmount DECIMAL(18, 2),
	Premium DECIMAL(18, 2),
	Discount DECIMAL(18, 2),
	SubTotal DECIMAL(18, 2),
	Taxes DECIMAL(18, 2),
	GrandTotal DECIMAL(18, 2),
	DiscountDescription VARCHAR(2000),
	AirmilesAmount MONEY,
	DeductableRate DECIMAL(18, 2), -- these are the figures that are applied to the claim calculations
	CoInsuranceRate DECIMAL(18, 2)
)
AS
BEGIN
 
	
	DECLARE @CoInsDeductAmount DECIMAL(18, 2),
			@Premium DECIMAL(18, 2),
			@Discount DECIMAL(18, 2),
			@SubTotal DECIMAL(18, 2),
			@Taxes DECIMAL(18, 2),
			@GrandTotal DECIMAL(18, 2),
			@DiscountDescription VARCHAR(2000),
			@AirmilesAmount MONEY,
			@DeductableRate DECIMAL(18, 2),
			@CoInsuranceRate DECIMAL(18, 2)	,
			@BrandRLID INT
			
			
	
	-- Check tax rate from province
	DECLARE @TaxRate MONEY = 0
	SELECT @TaxRate = dbo.fn_C00_1273_GetSalesTaxFromProvinceCode(@ProvinceCode, @EffectiveDate)
	 
	-- Get the current scheme	
	DECLARE @CurrentSchemeMatterID INT
	SELECT @CurrentSchemeMatterID = ISNULL(dbo.fn_C00_1273_GetCurrentScheme(@IllnessProductID, @CalculationDate), 0)
	
	-- Get the BrandRLID
	SELECT @BrandRLID = dbo.fn_C00_1273_GetBrandRLIDFromProductRLID(@IllnessProductID)
	
	-- And now the promotions
	DECLARE @ValidPromotions TABLE
	(
		ID INT IDENTITY,
		LeadID INT, 
		ResourceListID INT, 
		MarketingCode VARCHAR(2000), 
		Description VARCHAR(2000), 
		Amount MONEY, 
		IsPercentage INT,
		Total MONEY,
		Percentage MONEY
	)
	
	INSERT @ValidPromotions (ResourceListID, MarketingCode, Description, Amount, IsPercentage)
	SELECT ResourceListID, MarketingCode, Description, Amount, IsPercentage
	FROM dbo.fn_C00_1273_Quotes_GetPromotions(@CalculationDate, NULL, @BrandRLID, NULL)
	
	---- Look up the uplift for deductables and coinsurance	
	--SELECT @DeductableRate = DeductibleAmount
	--FROM dbo.fn_C00_1273_Quotes_GetDeductables(@DateOfBirth, @BreedID, @CurrentSchemeMatterID, ISNULL(@EffectiveDate, @CalculationDate), @BrandRLID)
	--WHERE DeductibleLevel = @DeductableLevel
	
	--SELECT @CoInsuranceRate = CoinsuranceAmount
	--FROM dbo.fn_C00_1273_Quotes_GetCoinsurance(@CurrentSchemeMatterID, @BrandRLID)
	--WHERE CoinsuranceLevel = @CoInsuranceLevel 
	SELECT @DeductableRate=0,@CoInsuranceRate=0 -- TODO DCM  2014-09-15 these calls ignore until premium calc overhauled
	
	DECLARE @MaxMarketingDiscount DECIMAL(18, 2)
	
	-- Get all the figures we need from the baseline amounts
	SELECT	@Premium = ISNULL(SubTotalBaselinePremium, 0),
			@MaxMarketingDiscount = MaxMarketingDiscount
	FROM dbo.fn_C00_1273_Quotes_GetBaselineAmounts(@CalculationDate, @BreedID, @PostalCode, @CurrentSchemeMatterID, @BrandRLID, NULL)
	
	SELECT @CoInsDeductAmount =	CASE
									WHEN @CoInsuranceLevel = 1 AND @DeductableLevel = 1 THEN CAST(((Coins1Deduct1Uplift * @Premium) / 100) AS DECIMAL(18, 2))
									WHEN @CoInsuranceLevel = 1 AND @DeductableLevel = 2 THEN CAST(((Coins1Deduct2Uplift * @Premium) / 100) AS DECIMAL(18, 2))
									WHEN @CoInsuranceLevel = 1 AND @DeductableLevel = 3 THEN CAST(((Coins1Deduct3Uplift * @Premium) / 100) AS DECIMAL(18, 2))
									WHEN @CoInsuranceLevel = 2 AND @DeductableLevel = 1 THEN CAST(((Coins2Deduct1Uplift * @Premium) / 100) AS DECIMAL(18, 2))
									WHEN @CoInsuranceLevel = 2 AND @DeductableLevel = 2 THEN CAST(((Coins2Deduct2Uplift * @Premium) / 100) AS DECIMAL(18, 2))
									WHEN @CoInsuranceLevel = 2 AND @DeductableLevel = 3 THEN CAST(((Coins2Deduct3Uplift * @Premium) / 100) AS DECIMAL(18, 2))
									WHEN @CoInsuranceLevel = 3 AND @DeductableLevel = 1 THEN CAST(((Coins3Deduct1Uplift * @Premium) / 100) AS DECIMAL(18, 2))
									WHEN @CoInsuranceLevel = 3 AND @DeductableLevel = 2 THEN CAST(((Coins3Deduct2Uplift * @Premium) / 100) AS DECIMAL(18, 2))
									WHEN @CoInsuranceLevel = 3 AND @DeductableLevel = 3 THEN CAST(((Coins3Deduct3Uplift * @Premium) / 100) AS DECIMAL(18, 2))
								END
	FROM dbo.fn_C00_1273_Quotes_GetCoinsDeductUplifts(@BreedID, @CurrentSchemeMatterID, @BrandRLID)
	
	SELECT @Premium  += ISNULL(@CoInsDeductAmount, 0)
	
	
	-- Convert percentage discounts to amounts
	UPDATE @ValidPromotions
	SET Total =	CASE
					WHEN IsPercentage = 1 THEN (Amount * @Premium) / 100
					ELSE Amount
				END,
		Percentage =	CASE
							WHEN IsPercentage = 1 THEN Amount
							ELSE (Amount / @Premium) * 100
						END
						
	DECLARE @PercentageTotal DECIMAL(18, 2)	
				
	SELECT @Discount = SUM(Total), @PercentageTotal = SUM(Percentage)
	FROM @ValidPromotions v
	INNER JOIN @Promotions p ON v.ResourceListID = p.AnyID
	
	
	-- Check to see if we have a multi-pet discount
	;WITH MultiPet AS 
	(
		SELECT tdvDiscount.ValueMoney AS Amount, 
		ROW_NUMBER() OVER(ORDER BY tdvNo.ValueInt DESC) as rn 
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvNo WITH (NOLOCK) ON r.TableRowID = tdvNo.TableRowID AND tdvNo.DetailFieldID = 1161762 -- TODO not mapped
		INNER JOIN dbo.TableDetailValues tdvDiscount WITH (NOLOCK) ON r.TableRowID = tdvDiscount.TableRowID AND tdvDiscount.DetailFieldID = 1161763 -- TODO not mapped
		--WHERE r.LeadID = 4215904
		WHERE r.LeadID = dbo.fn_C00_1273_GetSchemeLeadIDFromProductRLID(@IllnessProductID)
		AND tdvNo.ValueInt <= @TotalPets
	)
	-- Apply the multi pet discount
	SELECT	@Discount = ISNULL(@Discount, 0) + ((Amount * @Premium) / 100),
			@PercentageTotal = ISNULL(@PercentageTotal, 0) + Amount
	FROM MultiPet
	WHERE rn = 1
	
	
	-- Make sure the discount doesn't exceed the max						
	IF @PercentageTotal > @MaxMarketingDiscount
	BEGIN
		SELECT @Discount = (@MaxMarketingDiscount * @Premium) / 100
	END
			
	-- Apply the discount, taxes and save		
	SELECT @SubTotal = @Premium - ISNULL(@Discount, 0)
	
	SELECT @Taxes = (@SubTotal * @TaxRate) / 100
	
	SELECT @GrandTotal = @SubTotal + @Taxes
	
	-- Air miles
	--SELECT @AirmilesAmount = @GrandTotal / 20
	---- No air miles for any brand other than PPU
	--IF @BrandRLID != 138780
	--BEGIN
		SELECT @AirmilesAmount = 0
	--END
	
	INSERT @Calculations (DeductableLevel, CoInsuranceLevel, CoInsDeductAmount, Premium, Discount, SubTotal, Taxes, GrandTotal, DiscountDescription, AirmilesAmount, DeductableRate, CoInsuranceRate)
	VALUES (@DeductableLevel, @CoInsuranceLevel, @CoInsDeductAmount, @Premium, ISNULL(@Discount, 0), @SubTotal, @Taxes, @GrandTotal, @DiscountDescription, @AirmilesAmount, @DeductableRate, @CoInsuranceRate)
 
	
	RETURN
	
END
 
 
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_Policy_CalculateIllness] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1273_Policy_CalculateIllness] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_Policy_CalculateIllness] TO [sp_executeall]
GO
