SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2015-08-13
-- Description:	Return address formatted according to the ISI XML requirements
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ISI_ReturnXmlAddress]
(
	 @Line1		VARCHAR(2000) = ''
	,@Line2		VARCHAR(2000)
	,@Line3		VARCHAR(2000)
	,@Town		VARCHAR(2000)
	,@County	VARCHAR(2000) = ''
	,@PostCode	VARCHAR(2000) = ''
	,@Country	VARCHAR(2000) = 'IRELAND'
)
RETURNS XML
AS
BEGIN
	DECLARE  @XmlAddress XML

	IF @Country = 'IRELAND'
	BEGIN
		SELECT @XmlAddress =
		(
		SELECT	 ISNULL(@Line1		,'') [Line1]
				,ISNULL(@Line2		,'') [Line2]
				,ISNULL(@Line3		,'') [Line3]
				,ISNULL(@Town		,'') [Town]
				,ISNULL(@County		,'') [County]
		FOR XML PATH(''), ROOT('AddressIrish')
		)
	END
	ELSE
	BEGIN
		SELECT @XmlAddress = 
		(
		SELECT	 ISNULL(@Country	,'') [Country]
				,ISNULL(@Line1		,'') [Line1]
				,ISNULL(@Line2		,'') [Line2]
				,ISNULL(@Line3		,'') [Line3]
				,ISNULL(@Town		,'') [Town]
				,ISNULL(@PostCode	,'') [PostCode]
		FOR XML PATH(''), ROOT('AddressNonIrish')
		)
	END

	RETURN @XmlAddress
END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_ReturnXmlAddress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ISI_ReturnXmlAddress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_ReturnXmlAddress] TO [sp_executeall]
GO
