SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-11-09
-- Description:	Returns the amount already paid on a claim
-- Modified:	2013-05-20 Include cancelled in refunded calc
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetAlreadyPaid]
(
	@MatterID INT
)
RETURNS MONEY	
AS
BEGIN

	DECLARE @AlreadyPaid MONEY = 0,
			@Paid MONEY = 0,
			@Refunded MONEY = 0

	SELECT @Paid = SUM(tdvAmount.ValueMoney)
	FROM dbo.TableRows r WITH (NOLOCK)
	INNER JOIN dbo.TableDetailValues tdvAmount WITH (NOLOCK) ON r.TableRowID = tdvAmount.TableRowID AND tdvAmount.DetailFieldID = 154518
	INNER JOIN dbo.TableDetailValues tdvApproved WITH (NOLOCK) ON r.TableRowID = tdvApproved.TableRowID AND tdvApproved.DetailFieldID = 159407
	INNER JOIN dbo.TableDetailValues tdvType WITH (NOLOCK) ON r.TableRowID = tdvType.TableRowID AND tdvType.DetailFieldID = 154517
	LEFT JOIN dbo.TableDetailValues tdvFail WITH ( NOLOCK ) on r.TableRowID = tdvFail.TableRowID AND tdvFail.DetailFieldID = 179902 
	WHERE r.MatterID = @MatterID
	AND tdvApproved.ValueDate IS NOT NULL
	AND tdvType.DetailValue = '0'
	AND ISNULL(tdvFail.DetailValue,'') = '' 

	SELECT @Refunded = SUM(tdvAmount.ValueMoney)
	FROM dbo.TableRows r WITH (NOLOCK)
	INNER JOIN dbo.TableDetailValues tdvAmount WITH (NOLOCK) ON r.TableRowID = tdvAmount.TableRowID AND tdvAmount.DetailFieldID = 154518
	INNER JOIN dbo.TableDetailValues tdvApproved WITH (NOLOCK) ON r.TableRowID = tdvApproved.TableRowID AND tdvApproved.DetailFieldID = 159407
	INNER JOIN dbo.TableDetailValues tdvType WITH (NOLOCK) ON r.TableRowID = tdvType.TableRowID AND tdvType.DetailFieldID = 154517
	WHERE r.MatterID = @MatterID
	AND tdvApproved.ValueDate IS NOT NULL
	AND tdvType.DetailValue IN ('1','2') -- Refunded or cancelled

	SELECT @AlreadyPaid = ISNULL(@Paid, 0) - ISNULL(@Refunded, 0)

	RETURN @AlreadyPaid

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetAlreadyPaid] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_GetAlreadyPaid] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetAlreadyPaid] TO [sp_executeall]
GO
