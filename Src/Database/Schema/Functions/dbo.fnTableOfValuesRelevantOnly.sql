SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2008-09-30
-- Description:	Return a table of varchars from an input string, but ignoring commas in phrases like DATEADD(day, 1, getdate)
--              and "Poplar House, 126a Ashley Road", "Hale"
-- =============================================
CREATE FUNCTION [dbo].[fnTableOfValuesRelevantOnly]
(
	@InputString VARCHAR(MAX),
	@Delimiter CHAR(1)
)
RETURNS 
@TableOut TABLE 
(
	AnyValue VARCHAR(MAX)
)
AS
BEGIN

	DECLARE 
	@KeepLooping BIT = 1,
	@TestValue VARCHAR(MAX),
	@DelimiterPos INT,
	@QuoteOpenPos INT,
	@QuoteClosePos INT,
	@SquareBracketOpenPos INT,
	@SquareBracketClosePos INT,
	@RoundBracketOpenPos INT,
	@RoundBracketClosePos INT,
	@NextSafePos INT = 1,
	@Quote CHAR(1) = '''', /* single quote is char(39) if required */
	@SquareBracketOpen CHAR(1) = '[',
	@SquareBracketClose CHAR(1) = ']',
	@RoundBracketOpen CHAR(1) = '(',
	@RoundBracketClose CHAR(1) = ')'
	
	/*
		Given an input string of this:
		@InputStr = 'SELECT l.LeadID, c.FullName, ca.CaseRef, DATEADD(day, 1, getdate) AS [Tricky Test]'
		
		Return a column list of this:
		l.LeadID 
		c.FullName
		ca.CaseRef 
		DATEADD(day, 1, getdate) AS [Tricky Test]
	*/

	/* Start by adding a final delimiter to the end of the input string, to guarantee one result if any text is found */
	SET @InputString = LTRIM(RTRIM(@InputString)) + @Delimiter


	/* Check that the string is not made entirely of delimiters, eg the one we just added above */
	IF REPLACE(@InputString, @Delimiter, '') <> ''
	BEGIN

		/* Keep looping round, removing one element at a time and putting it into a table variable */
		WHILE @KeepLooping = 1
		BEGIN
			
			/* First of all, find the next comma or whatever we are looking for */
			SET @DelimiterPos = CHARINDEX(@Delimiter, @InputString, @NextSafePos)
			
			/* Now look for anything that can legitimately enclose it: '' and [] and () */
			/* 'Quotes' */
			SET @QuoteOpenPos = CHARINDEX(@Quote, @InputString, @NextSafePos)
			IF @QuoteOpenPos > 0
			BEGIN
				/* If an opening quote was found, look for the closing quote that matches it */
				SET @QuoteClosePos = CHARINDEX(@Quote, @InputString, @QuoteOpenPos + 1)
			END
			ELSE
			BEGIN
				SET @QuoteClosePos = 0
			END
			/* [Square Brackets] */
			SET @SquareBracketOpenPos = CHARINDEX(@SquareBracketOpen, @InputString, @NextSafePos)
			IF @SquareBracketOpenPos > 0
			BEGIN
				/* If an opening square bracket was found, look for the closing square bracket that matches it */
				SET @SquareBracketClosePos = CHARINDEX(@SquareBracketClose, @InputString, @SquareBracketOpenPos + 1)
			END
			ELSE
			BEGIN
				SET @SquareBracketClosePos = 0
			END
			/* (Round Brackets) */
			SET @RoundBracketOpenPos = CHARINDEX(@RoundBracketOpen, @InputString, @NextSafePos)
			IF @RoundBracketOpenPos > 0
			BEGIN
				/* If an opening round bracket was found, look for the closing round bracket that matches it */
				SET @RoundBracketClosePos = CHARINDEX(@RoundBracketClose, @InputString, @RoundBracketOpenPos + 1)
			END
			ELSE
			BEGIN
				SET @RoundBracketClosePos = 0
			END

			/* Ignore this comma and look for the next one: 'house name, street' */
			IF (@DelimiterPos BETWEEN @QuoteOpenPos AND @QuoteClosePos)
			BEGIN
				SELECT @NextSafePos = @QuoteClosePos
			END
			ELSE
			BEGIN
				/* Ignore both these commas and look for the next one: '[first, middle, last name]' */
				IF (@DelimiterPos BETWEEN @SquareBracketOpenPos AND @SquareBracketClosePos)
				BEGIN
					SELECT @NextSafePos = @SquareBracketClosePos
				END
				ELSE
				BEGIN
					/* Ignore both these commas and look for the next one: 'DATEADD(day, 1, dbo.fn_GetDate_Local())' */
					IF (@DelimiterPos BETWEEN @RoundBracketOpenPos AND @RoundBracketClosePos)
					BEGIN
						SELECT @NextSafePos = @RoundBracketClosePos
					END
					ELSE
					BEGIN
						/* No traps detected! */
						SELECT @NextSafePos = 0
					END
				END
			END
			
			/* We have a match... */
			IF @DelimiterPos > 1
			BEGIN
				/*
					If the delimiter was found in the middle of an encapsulating term [like, this] then take
					the position of ] as the next start point and loop round again.
					Otherwise this is a valid match so go ahead and use it.
				*/
				IF @NextSafePos = 0
				BEGIN
					/* Take everything to the left of this delimiter and put it into the table variable ready for final output */
					INSERT INTO @TableOut (AnyValue) 
					SELECT LTRIM(RTRIM(LEFT(@InputString, @DelimiterPos - 1)))
					
					/* Chop this term from the start of the input string so it doesn't get matched again */
					SET @InputString = RIGHT(@InputString, LEN(@InputString) - @DelimiterPos)
				END
			END
			ELSE
			BEGIN
				/* All finished */
				SET @KeepLooping = 0
			END
			
		END /* While loop */
		
	END	
	RETURN
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfValuesRelevantOnly] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnTableOfValuesRelevantOnly] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfValuesRelevantOnly] TO [sp_executeall]
GO
