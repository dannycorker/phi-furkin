SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-01-15
-- Description:	Returns the coinsurance and deductible uplifts
-- Modified DCM 2014-09-16 Copied from 235
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_Quotes_GetCoinsDeductUplifts]
(
	@BreedRatingRLID INT, 
	@SchemeMatterID INT
	,@BrandRLID INT 
)
RETURNS 
@Deductibles TABLE
(
	MatterID INT,
	Coins1Deduct1Uplift MONEY,
	Coins1Deduct2Uplift MONEY,
	Coins1Deduct3Uplift MONEY,
	Coins2Deduct1Uplift MONEY,
	Coins2Deduct2Uplift MONEY,
	Coins2Deduct3Uplift MONEY,
	Coins3Deduct1Uplift MONEY,
	Coins3Deduct2Uplift MONEY,
	Coins3Deduct3Uplift MONEY
) 
AS
BEGIN 
 
 
	DECLARE @PetType INT,
			@BaselineID INT,
			@Coins1Deduct1UpliftID INT,
			@Coins1Deduct2UpliftID INT,
			@Coins1Deduct3UpliftID INT,
			@Coins2Deduct1UpliftID INT,
			@Coins2Deduct2UpliftID INT,
			@Coins2Deduct3UpliftID INT,
			@Coins3Deduct1UpliftID INT,
			@Coins3Deduct2UpliftID INT,
			@Coins3Deduct3UpliftID INT

	-- ALL TODO.  Temporarily frigged to return fixed values
		INSERT @Deductibles (MatterID,	Coins1Deduct1Uplift,
									Coins1Deduct2Uplift,
									Coins1Deduct3Uplift,
									Coins2Deduct1Uplift,
									Coins2Deduct2Uplift,
									Coins2Deduct3Uplift,
									Coins3Deduct1Uplift,
									Coins3Deduct2Uplift,
									Coins3Deduct3Uplift) VALUES (50009067, 11, 12, 13, 21, 22, 23, 31, 32, 33)
			

	--SELECT @PetType = rdvType.ValueInt
	--FROM dbo.ResourceList r WITH (NOLOCK)
	--INNER JOIN dbo.ResourceListDetailValues rdvType WITH (NOLOCK) ON r.ResourceListID = rdvType.ResourceListID AND rdvType.DetailFieldID = 153537
	--INNER JOIN dbo.ResourceListDetailValues rdvBreed WITH (NOLOCK) ON r.ResourceListID = rdvBreed.ResourceListID AND rdvBreed.DetailFieldID = 153538
	--WHERE rdvBreed.ResourceListID = @BreedRatingRLID
 
	--IF (@PetType = 57239) -- Dog
	--BEGIN
		
	--	SELECT	@BaselineID = 154442,
	--			@Coins1Deduct1UpliftID = 155993,
	--			@Coins1Deduct2UpliftID = 154461,
	--			@Coins1Deduct3UpliftID = 155994,
	--			@Coins2Deduct1UpliftID = 154462,
	--			@Coins2Deduct2UpliftID = 155995,
	--			@Coins2Deduct3UpliftID = 154463,
	--			@Coins3Deduct1UpliftID = 155996,
	--			@Coins3Deduct2UpliftID = 154464,
	--			@Coins3Deduct3UpliftID = 155997
	
	--END
	--ELSE -- Cat
	--BEGIN
 
	--	SELECT	@BaselineID = 154448,
	--			@Coins1Deduct1UpliftID = 163090,
	--			@Coins1Deduct2UpliftID = 163091,
	--			@Coins1Deduct3UpliftID = 163092,
	--			@Coins2Deduct1UpliftID = 163093,
	--			@Coins2Deduct2UpliftID = 163094,
	--			@Coins2Deduct3UpliftID = 163095,
	--			@Coins3Deduct1UpliftID = 163096,
	--			@Coins3Deduct2UpliftID = 163097,
	--			@Coins3Deduct3UpliftID = 163098
	
	--END
 
	--INSERT @Deductibles (MatterID,	Coins1Deduct1Uplift,
	--								Coins1Deduct2Uplift,
	--								Coins1Deduct3Uplift,
	--								Coins2Deduct1Uplift,
	--								Coins2Deduct2Uplift,
	--								Coins2Deduct3Uplift,
	--								Coins3Deduct1Uplift,
	--								Coins3Deduct2Uplift,
	--								Coins3Deduct3Uplift)
	--SELECT m.MatterID,	mdv11.ValueMoney Coins1Deduct1Uplift,
	--					mdv12.ValueMoney Coins1Deduct2Uplift,
	--					mdv13.ValueMoney Coins1Deduct3Uplift,
	--					mdv21.ValueMoney Coins2Deduct1Uplift,
	--					mdv22.ValueMoney Coins2Deduct2Uplift,
	--					mdv23.ValueMoney Coins2Deduct3Uplift,
	--					mdv31.ValueMoney Coins3Deduct1Uplift,
	--					mdv32.ValueMoney Coins3Deduct2Uplift,
	--					mdv33.ValueMoney Coins3Deduct3Uplift
	--FROM dbo.Lead l WITH (NOLOCK) 
	--INNER JOIN dbo.Matter m WITH (NOLOCK) ON l.LeadID = m.LeadID
	--INNER JOIN dbo.MatterDetailValues mdvBaseline WITH (NOLOCK) ON m.MatterID = mdvBaseline.MatterID AND mdvBaseline.DetailFieldID = @BaselineID
	--INNER JOIN dbo.MatterDetailValues mdvScheme WITH (NOLOCK) ON m.MatterID = mdvScheme.MatterID AND mdvScheme.DetailFieldID = 145689
	--INNER JOIN dbo.ResourceListDetailValues rdvType WITH (NOLOCK) ON mdvScheme.ValueInt = rdvType.ResourceListID AND rdvType.DetailFieldID = 144319
	--INNER JOIN dbo.MatterDetailValues mdv11 WITH (NOLOCK) ON m.MatterID = mdv11.MatterID AND mdv11.DetailFieldID = @Coins1Deduct1UpliftID
	--INNER JOIN dbo.MatterDetailValues mdv12 WITH (NOLOCK) ON m.MatterID = mdv12.MatterID AND mdv12.DetailFieldID = @Coins1Deduct2UpliftID
	--INNER JOIN dbo.MatterDetailValues mdv13 WITH (NOLOCK) ON m.MatterID = mdv13.MatterID AND mdv13.DetailFieldID = @Coins1Deduct3UpliftID
	--INNER JOIN dbo.MatterDetailValues mdv21 WITH (NOLOCK) ON m.MatterID = mdv21.MatterID AND mdv21.DetailFieldID = @Coins2Deduct1UpliftID
	--INNER JOIN dbo.MatterDetailValues mdv22 WITH (NOLOCK) ON m.MatterID = mdv22.MatterID AND mdv22.DetailFieldID = @Coins2Deduct2UpliftID
	--INNER JOIN dbo.MatterDetailValues mdv23 WITH (NOLOCK) ON m.MatterID = mdv23.MatterID AND mdv23.DetailFieldID = @Coins2Deduct3UpliftID
	--INNER JOIN dbo.MatterDetailValues mdv31 WITH (NOLOCK) ON m.MatterID = mdv31.MatterID AND mdv31.DetailFieldID = @Coins3Deduct1UpliftID
	--INNER JOIN dbo.MatterDetailValues mdv32 WITH (NOLOCK) ON m.MatterID = mdv32.MatterID AND mdv32.DetailFieldID = @Coins3Deduct2UpliftID
	--INNER JOIN dbo.MatterDetailValues mdv33 WITH (NOLOCK) ON m.MatterID = mdv33.MatterID AND mdv33.DetailFieldID = @Coins3Deduct3UpliftID
	----WHERE l.LeadID = 4215904
	--WHERE l.LeadID = dbo.fn_C0_GetIllnessSchemeLeadIDFromBrandRLID(ISNULL(@BrandRLID,138780))
	--AND rdvType.ValueInt IN (67771, 67772) -- Just accident and accident / illness
	--AND (@SchemeMatterID IS NULL OR m.MatterID = @SchemeMatterID)
 
	RETURN
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_Quotes_GetCoinsDeductUplifts] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1273_Quotes_GetCoinsDeductUplifts] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_Quotes_GetCoinsDeductUplifts] TO [sp_executeall]
GO
