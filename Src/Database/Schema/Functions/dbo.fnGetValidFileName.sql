SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2010-09-13
-- Description:	Replace invalid characters in a string to make it a valid filename (with or without path)
-- =============================================
CREATE FUNCTION [dbo].[fnGetValidFileName] 
(
	@FileName varchar(1000), 
	@IncludesPath bit  
)
RETURNS varchar(1000)
AS
BEGIN
	
	
	DECLARE @CharCount int, 
	@ValidFileName varchar(1000) = ''
	
	/* 
		If a real value was passed in, check all the characters now.
		Otherwise it is null or empty and therefore invalid. 
	*/
	IF @FileName > ''
	BEGIN
	
		IF dbo.fnIsValidFileName(@FileName, @IncludesPath) = 1
		BEGIN
			SELECT @ValidFileName = @FileName
		END
		ELSE
		BEGIN
		
			SELECT @CharCount = datalength(@FileName)
			
			/*
				These two branches look identical, but they check different columns on the CharMap table.
			*/
			IF @IncludesPath = 0
			BEGIN
			
				/* 
					Filename only is quite simple. 
					Replace any invalid characters with hyphens '-'.
				*/
				;
				WITH chars AS 
				(
					/* Pivot the input string into a "table" so it can be joined to the CharMap table */
					SELECT t.N as [Position], 
					SUBSTRING(@FileName, t.N, 1) as [Char], 
					ASCII(SUBSTRING(@FileName, t.N, 1)) as [AsciiValue], 
					t.N as rn 
					FROM dbo.Tally t WITH (NOLOCK) 
					WHERE t.N <= @CharCount 
				)
				SELECT @ValidFileName += CASE cm.IsValidFileName WHEN 1 THEN cm.CharValue ELSE '-' END 
				FROM chars c 
				INNER JOIN dbo.CharMap cm WITH (NOLOCK) ON cm.AsciiValue = c.AsciiValue 
				ORDER BY c.rn 
				
			END
			ELSE
			BEGIN 
				
				/* Path is slightly different as a colon is allowed, but only in pos 2 */
				;
				WITH chars AS 
				(
					/* Pivot the input string into a "table" so it can be joined to the CharMap table */
					SELECT t.N as [Position], 
					SUBSTRING(@FileName, t.N, 1) as [Char], 
					ASCII(SUBSTRING(@FileName, t.N, 1)) as [AsciiValue], 
					t.N as rn 
					FROM dbo.Tally t WITH (NOLOCK) 
					WHERE t.N <= @CharCount 
				)
				SELECT @ValidFileName += CASE cm.IsValidPath WHEN 1 THEN CASE WHEN c.rn <> 2 AND cm.CharValue = ':' THEN '-' ELSE cm.CharValue END  ELSE '-' END 
				FROM chars c 
				INNER JOIN dbo.CharMap cm WITH (NOLOCK) ON cm.AsciiValue = c.AsciiValue 
				ORDER BY c.rn 
				
			END /* File only, or full path and file */
			
		END /* Valid already or needs fixing */
		
	END /* @FileName > '' */
	ELSE
	BEGIN
	
		/* Safety net - return datetime.txt as the filename */
		SELECT @ValidFileName = convert(varchar, dbo.fn_GetDate_Local(), 126) + '.txt'
		
	END
	
	RETURN @ValidFileName
	
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetValidFileName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetValidFileName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetValidFileName] TO [sp_executeall]
GO
