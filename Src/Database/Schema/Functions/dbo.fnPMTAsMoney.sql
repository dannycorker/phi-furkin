SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2015-06-14
-- Description:	Equivalent of Excel's PMT function, returning as Money
--				Adapted from http://stackoverflow.com/questions/17194957/replicate-pmt-function-in-sql-use-column-values-as-inputs
-- =============================================
CREATE FUNCTION [dbo].[fnPMTAsMoney]
(
	 @rate		FLOAT
	,@periods	SMALLINT
	,@principal	NUMERIC(20,2)
)
RETURNS money
AS
BEGIN

	DECLARE @pmt NUMERIC (38,9)

	DECLARE  @WK_periods	FLOAT
			,@WK_principal	FLOAT
			,@wk_One		FLOAT
			,@WK_power		FLOAT

	SELECT   @WK_periods	= @periods
			,@WK_principal	= @principal
			,@WK_One		= 1

	SELECT  @pmt =	ROUND(
							( @WK_principal * (@rate*POWER(@WK_One+@rate,@WK_periods)))
							/ (POWER(@WK_One+@rate,@WK_periods)-@WK_One)
					,9)

	SELECT @pmt = CAST(@pmt as NUMERIC(18,2))

	RETURN @pmt
END



GO
GRANT VIEW DEFINITION ON  [dbo].[fnPMTAsMoney] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnPMTAsMoney] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnPMTAsMoney] TO [sp_executeall]
GO
