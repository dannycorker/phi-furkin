SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Someone who likes infinite loops
-- Create date: A dark day
-- Description:	Converts a passed in string to camel case
-- Modified:	2015-07-09	SB	Added null check which was causing infinte loop and gave it some nicer formatting
-- =============================================
CREATE FUNCTION [dbo].[fnConvertCamelCase]
(
	@Str VARCHAR(8000)
)

RETURNS VARCHAR(8000) 
WITH RETURNS NULL ON NULL INPUT AS

BEGIN

	DECLARE @Result VARCHAR(2000)

	SET @Str = LOWER(@Str) + ' '
	SET @Result = ''

	WHILE 1=1
	BEGIN

		IF PATINDEX('% %', @Str) = 0 BREAK
		SET @Result = @Result + UPPER(LEFT(@Str, 1)) + SUBSTRING(@Str, 2, CHARINDEX(' ', @Str) -1)
		SET @Str = SUBSTRING(@Str, CHARINDEX(' ', @Str) + 1, LEN(@Str))

	END

	SET @Result = LEFT(@Result,LEN(@Result))
	
	RETURN @Result

END
 








GO
GRANT VIEW DEFINITION ON  [dbo].[fnConvertCamelCase] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnConvertCamelCase] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnConvertCamelCase] TO [sp_executeall]
GO
