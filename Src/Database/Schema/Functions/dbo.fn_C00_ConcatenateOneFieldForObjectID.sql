SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2015-06-04
-- Description:	Concatenate the same field across all subordinate objects
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ConcatenateOneFieldForObjectID]
(
	 @DetailFieldID INT					= 287254
	,@Delimiter		VARCHAR(2000)		= ', '
	,@ObjectID		INT					= 9670335
	,@LeadOrMatter	INT					= 1
)
RETURNS VARCHAR(2000)
AS
BEGIN
			
	DECLARE	 @ReturnValue			VARCHAR(2000) = ''
			,@DetailFieldSubtypeID	INT
			,@ClientID				INT
			,@LeadTypeID			INT
			,@CustomerID			INT
			,@LeadID				INT
			,@CaseID				INT
			
	SELECT @DetailFieldSubtypeID = df.LeadOrMatter, @ClientID = df.ClientID, @LeadTypeID = df.LeadTypeID
	FROM DetailFields df WITH ( NOLOCK ) 
	WHERE df.DetailFieldID = @DetailFieldID

	IF @DetailFieldSubtypeID = 1 /*Lead*/
	BEGIN
		SELECT @ReturnValue += dv.DetailValue + @Delimiter
		FROM LeadDetailValues dv WITH ( NOLOCK ) 
		INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = dv.LeadID
		WHERE dv.DetailFieldID = @DetailFieldID
		AND	(
				( @LeadOrMatter = 1  AND l.LeadID		= @ObjectID )
			OR	( @LeadOrMatter = 10 AND l.CustomerID	= @ObjectID )
			)
		ORDER BY dv.LeadID
	END
	ELSE
	IF @DetailFieldSubtypeID = 2 /*Matter*/
	BEGIN
		SELECT @ReturnValue += dv.DetailValue + @Delimiter
		FROM MatterDetailValues dv WITH ( NOLOCK ) 
		INNER JOIN Matter m WITH (NOLOCK) on m.MatterID = dv.MatterID
		WHERE dv.DetailFieldID = @DetailFieldID
		AND	(
				( @LeadOrMatter = 1  AND m.LeadID		= @ObjectID )
			OR	( @LeadOrMatter = 2	 AND m.MatterID		= @ObjectID )
			OR	( @LeadOrMatter = 10 AND m.CustomerID	= @ObjectID )
			OR	( @LeadOrMatter = 11 AND m.CaseID		= @ObjectID )
			)
		ORDER BY dv.MatterID
	END
	IF @DetailFieldSubtypeID = 11 /*Case*/
	BEGIN
		SELECT @ReturnValue += dv.DetailValue + @Delimiter
		FROM CaseDetailValues dv WITH ( NOLOCK ) 
		INNER JOIN Cases c WITH (NOLOCK) on c.CaseID = dv.CaseID
		INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = c.LeadID 
		WHERE dv.DetailFieldID = @DetailFieldID
		AND	(
				( @LeadOrMatter = 1  AND c.LeadID		= @ObjectID )
			OR	( @LeadOrMatter = 10 AND l.CustomerID	= @ObjectID )
			OR	( @LeadOrMatter = 11 AND c.CaseID		= @ObjectID )
			)
		ORDER BY dv.CaseID
	END

	SELECT @ReturnValue = ISNULL(@ReturnValue,'')

	IF LEN(@ReturnValue) > 0
	BEGIN
		SELECT @ReturnValue = LEFT(@ReturnValue,LEN(@ReturnValue)-1) /*Remove the trailing delimiter*/
	END

	RETURN @ReturnValue

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ConcatenateOneFieldForObjectID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ConcatenateOneFieldForObjectID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ConcatenateOneFieldForObjectID] TO [sp_executeall]
GO
