SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-07-21
-- Description:	Merges in shared lead types
-- =============================================
CREATE FUNCTION [dbo].[fnNoteTypeShared] 
(	
	@ClientID INT
)
RETURNS TABLE 
AS
RETURN 
(	
	SELECT [NoteTypeID]
		  ,CASE WHEN s.LeadTypeShareID IS NULL THEN nt.ClientID ELSE s.ClientID END AS ClientID
		  ,[NoteTypeName]
		  ,[NoteTypeDescription]
		  ,[DefaultPriority]
		  ,[AlertColour]
		  ,[NormalColour]
		  ,[Enabled]
		  ,[WhoCreated]
		  ,[WhenCreated]
		  ,[WhoModified]
		  ,[WhenModified]
		  ,[SourceID]
		  ,CASE WHEN s.LeadTypeShareID IS NULL THEN nt.LeadTypeID ELSE s.SharedTo END AS LeadTypeID
		  ,CASE WHEN s.LeadTypeShareID IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS IsShared
	FROM dbo.NoteType nt WITH (NOLOCK)
	LEFT JOIN dbo.LeadTypeShare s WITH (NOLOCK) ON nt.LeadTypeID = s.SharedFrom AND s.ClientID = @ClientID
)


GO
GRANT VIEW DEFINITION ON  [dbo].[fnNoteTypeShared] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnNoteTypeShared] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnNoteTypeShared] TO [sp_executeall]
GO
