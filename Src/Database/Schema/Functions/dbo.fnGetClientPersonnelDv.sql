SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-01-25
-- Description:	Get Clientpersonnel DetailValue
-- =============================================
CREATE FUNCTION [dbo].[fnGetClientPersonnelDv]
(
	@DetailFieldID		int,
	@ClientPersonnelID	int
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE @DetailValue as varchar(2000)

	Select @DetailValue = cdv.DetailValue 
	From ClientPersonnelDetailValues cdv with (nolock) 
	Where cdv.ClientPersonnelID = @ClientPersonnelID
	and cdv.DetailFieldID = @DetailFieldID
	
	RETURN @DetailValue

END






GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetClientPersonnelDv] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetClientPersonnelDv] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetClientPersonnelDv] TO [sp_executeall]
GO
