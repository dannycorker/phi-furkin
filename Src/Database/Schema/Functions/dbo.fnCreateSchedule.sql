SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-07-10
-- Description:	Returns a schedule of dates
-- ROH 2016-04-07 Bug fix for monthly - old code jumped over short months if starting on (eg) 31-Mar. Zen#37574
-- =============================================
CREATE FUNCTION [dbo].[fnCreateSchedule]
(
	@Frequency INT, -- 1 = Daily, 2 = Weekly, 3 = 4-Weekly, 4 = Monthly
	@Top INT,
	@Start DATE,
	@AfterDate Date, -- Allows you to pass a start date in the past but still get a schedule in the future
	@PreferredPaymentDay TINYINT = NULL -- this the day of the month that the customer would like the payment taken
)
RETURNS 
	@Dates TABLE 
	(
		ID INT IDENTITY,
		Date DATE
	)
AS
BEGIN

	IF @Frequency = 1
	BEGIN
		SELECT @Start = dbo.fnGetFirstPaymentDate(@Start, @PreferredPaymentDay)
		INSERT @Dates (Date)
		-- Daily
		SELECT TOP (@Top) wd.Date 
		FROM dbo.WorkingDays wd WITH (NOLOCK) 
		WHERE wd.Date >= @Start
		AND (@AfterDate IS NULL OR wd.Date > @AfterDate)
	END
	ELSE IF @Frequency = 2
	BEGIN
		SELECT @Start = dbo.fnGetFirstPaymentDate(@Start, @PreferredPaymentDay)
		INSERT @Dates (Date)
		-- Weekly
		SELECT TOP (@Top) wd.Date
		FROM dbo.WorkingDays wd WITH (NOLOCK) 
		INNER JOIN dbo.WorkingDays wdStart WITH (NOLOCK) ON wd.DayNumberOfWeek = wdStart.DayNumberOfWeek
		WHERE wdStart.Date = @Start
		AND wd.Date >= @Start
		AND (@AfterDate IS NULL OR wd.Date > @AfterDate)
	END
	ELSE IF @Frequency = 3
	BEGIN
		SELECT @Start = dbo.fnGetFirstPaymentDate(@Start, @PreferredPaymentDay)
		-- 4 Weekly
		;WITH InnerSql AS 
		(
			SELECT 0 AS N
			UNION
			SELECT N
			FROM dbo.TallyBig WITH (NOLOCK) 
			WHERE N%4 = 0 
		)
		INSERT @Dates (Date)
		SELECT TOP (@Top) DATEADD(WEEK, N, @Start) AS Date
		FROM InnerSql
		WHERE (@AfterDate IS NULL OR DATEADD(WEEK, N, @Start) > @AfterDate)
	END
	ELSE IF @Frequency = 4
	BEGIN
		INSERT @Dates (Date)
		-- Monthly
		
		/* BUG!
		SELECT TOP (@Top) wd.Date
		FROM dbo.WorkingDays wd WITH (NOLOCK) 
		INNER JOIN dbo.WorkingDays wdStart WITH (NOLOCK) ON wd.Day = wdStart.Day
		WHERE wdStart.Date = @Start
		AND wd.Date >= @Start
		AND (@AfterDate IS NULL OR wd.Date > @AfterDate)
		*/
		
		/* Fix - 2016-04-07 Zen#37574 */
		/*SELECT TOP (@Top) DATEADD(MONTH, T.N-1, @Start)
		FROM Tally t
		WHERE (@AfterDate IS NULL OR DATEADD(MONTH, T.N-1, @Start) > @Afterdate)
		ORDER BY t.N         */

		SELECT TOP (12) dbo.fnGetFirstPaymentDate(DateAdd(mm,t.n-1,@Start), @PreferredPaymentDay)
		FROM Tally t
		WHERE (@AfterDate IS NULL OR DATEADD(MONTH, T.N-1, @Start) > DATEADD(DD,-1, @AfterDate))
		ORDER BY t.N    

	END	
	
	RETURN

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnCreateSchedule] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnCreateSchedule] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnCreateSchedule] TO [sp_executeall]
GO
