SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-02-021
-- Description:	Returns a table of policy sections and their mappings to parent sections
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetPolicySectionRelationships]
(
	@CurrentPolicyMatterID INT
)
RETURNS 
	@PolicySections TABLE 
	(
		ResourceListID INT,
		Out_ResourceListID INT
	)
AS
BEGIN

	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @CurrentPolicyMatterID

	;WITH Sections AS 
	(
	SELECT tdvRl.ResourceListID, rlSection.ValueInt AS SectionID, rlSubSection.ValueInt AS SubSectionID
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvRl WITH (NOLOCK) ON r.TableRowID = tdvRl.TableRowID AND tdvRl.DetailFieldID = 144357
	INNER JOIN dbo.ResourceListDetailValues rlSection WITH (NOLOCK) ON tdvRl.ResourceListID = rlSection.ResourceListID AND rlSection.DetailFieldID = 146189
	INNER JOIN dbo.ResourceListDetailValues rlSubSection WITH (NOLOCK) ON tdvRl.ResourceListID = rlSubSection.ResourceListID AND rlSubSection.DetailFieldID = 146190
	WHERE r.MatterID = @CurrentPolicyMatterID
	), 
	AllSections AS
	(
	SELECT r.ResourceListID, rlSection.ValueInt AS SectionID, rlSubSection.ValueInt AS SubSectionID
	FROM dbo.ResourceList r WITH (NOLOCK) 
	INNER JOIN dbo.ResourceListDetailValues rlSection WITH (NOLOCK) ON r.ResourceListID = rlSection.ResourceListID AND rlSection.DetailFieldID = 146189
	INNER JOIN dbo.ResourceListDetailValues rlSubSection WITH (NOLOCK) ON r.ResourceListID = rlSubSection.ResourceListID AND rlSubSection.DetailFieldID = 146190
	WHERE r.ClientID = dbo.fnGetPrimaryClientID() --600
	)	

	INSERT @PolicySections (ResourceListID, Out_ResourceListID)
	SELECT s1.ResourceListID, s1.ResourceListID AS Out_ResourceListID
	FROM Sections s1
	UNION
	SELECT s2.ResourceListID, s1.ResourceListID AS Out_ResourceListID
	FROM Sections s1
	INNER JOIN AllSections s2 ON s2.SubSectionID != 74283 AND s1.SectionID = s2.SectionID
	WHERE s1.SubSectionID = 74283
	RETURN

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicySectionRelationships] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1272_GetPolicySectionRelationships] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicySectionRelationships] TO [sp_executeall]
GO
