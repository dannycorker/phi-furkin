SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack / Gavin Reynolds
-- Create date:	2019-08-07
-- Description:	havests customer json object
-- 2019-08-27 GPR						| Altered to use @Include flag
-- 2019-11-05 CPS for JIRA LAGCLAIM-160 | Updated to return XML
-- 2019-11-15 CPS for JIRA LAGCLAIM-266 | Updated to match Vision customer shape
-- 2019-11-21 CPS for JIRA LAGCLAIM-266 | Updated back to JSON for Ian
-- 2019-12-13 MAB vulnerability / DDA replaced with call to fn_C600_GetVulnerabilityDdaDetailsAsString
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Harvest_Customer_JSON]
(
	 @CustomerID	INT
	,@VisionMode	BIT = 0
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE @JsonData VARCHAR(MAX) =''

	IF @CustomerID > 0
	BEGIN
		SELECT  @JsonData +=
		(
			SELECT TOP 1
			 CASE WHEN @VisionMode = 1 THEN NULL ELSE	cu.CustomerID		END												[customerId]
			,CASE WHEN @VisionMode = 1 THEN NULL ELSE	t.Title				END 											[title]
			,CASE WHEN @VisionMode = 1 THEN NULL ELSE	cu.MiddleName		END 											[middleName]
			,CASE WHEN @VisionMode = 1 THEN NULL ELSE	cu.Test				END 											[test]
			,CASE WHEN @VisionMode = 1 THEN NULL ELSE	cu.CustomerRef		END 											[customerRef]
			,CASE WHEN @VisionMode = 1 THEN NULL ELSE	cu.WhoChanged		END 											[whoChanged]
			,CASE WHEN @VisionMode = 1 THEN NULL ELSE	cu.WhenChanged		END 											[whenChanged]
			,CASE WHEN @VisionMode = 1 THEN NULL ELSE	cu.Comments			END 											[comments]
			,CASE WHEN @VisionMode = 1 THEN NULL ELSE	cdv.ValueInt		END 											[affinityId]
			,CASE WHEN @VisionMode = 1 THEN NULL ELSE	li.ItemValue		END 											[affinityName]
			,											t.TitleID															[titleId]
			,											cu.FirstName														[firstName]
			,											cu.LastName															[lastName]
			,											cu.EmailAddress														[email]
			,											cu.HomeTelephone													[phoneNumber]
			,											cu.MobileTelephone													[mobileNumber]
			,											cu.DateOfBirth														[birthdayDate]
			,											lang.LanguageName													[language]
			,											liMethod.ItemValue													[communicationPreference]
			,											0																	[ddaStatus]
			,											dbo.fn_C600_GetVulnerabilityDdaDetailsAsString(@CustomerID,':',';')	[ddaDescription] /*2019-12-13 MAB */
			,											(	
															SELECT 
																0					[id]
																,1					[addressType]
																,[addr].Address1	[address1]
																,[addr].Address2	[address2]
																,[addr].Town		[town]
																,[addr].County		[county]
																,[addr].PostCode	[postcode]
																,co.CountryName		[countryName] 
															FROM Customers [addr] WITH(NOLOCK)
															LEFT JOIN Country co WITH(NOLOCK) on co.CountryID = [addr].CountryID
															WHERE [addr].CustomerID = [cu].CustomerID
															FOR JSON PATH
														) [addresses]
			FROM	Customers cu WITH (NOLOCK)
			INNER JOIN CustomerDetailValues cdv			WITH (NOLOCK) on cdv.CustomerID = cu.CustomerID			AND cdv.DetailFieldID = 170144 /*Affinity Details*/
			LEFT JOIN ResourceListDetailValues rdv		WITH (NOLOCK) on rdv.ResourceListID = cdv.DetailValue	AND rdv.DetailFieldID = 170127 /*Short Code*/
			LEFT JOIN LookupListItems li				WITH (NOLOCK) on li.LookupListItemID = rdv.ValueInt
			LEFT JOIN CustomerDetailValues cdvMethod	WITH (NOLOCK) on cdvMethod.CustomerID = cu.CustomerID	AND cdvMethod.DetailFieldID = 170257 /*Send documents by*/
			LEFT JOIN LookupListItems liMethod			WITH (NOLOCK) on liMethod.LookupListItemID = cdvMethod.ValueInt
			LEFT JOIN Language lang						WITH (NOLOCK) on lang.LanguageID = ISNULL(cu.LanguageID,1)
			INNER JOIN Titles t							WITH (NOLOCK) on t.TitleID = ISNULL(cu.TitleID,0)
			WHERE	cu.IsBusiness = 0
			AND		cu.CustomerID = @CustomerID
			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		)
	END

	RETURN @JsonData

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Harvest_Customer_JSON] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_Harvest_Customer_JSON] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Harvest_Customer_JSON] TO [sp_executeall]
GO
