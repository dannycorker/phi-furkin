SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 18/09/2013
-- Description:	Splits a string by the given delimiter
-- =============================================
CREATE FUNCTION [dbo].[fnSplitString] 
(
	@str NVARCHAR(MAX), 
    @separator CHAR(1)
)
RETURNS TABLE
AS
RETURN (
WITH tokens(p, a, b) AS (
    SELECT 
        CAST(1 AS BIGINT), 
        CAST(1 AS BIGINT), 
        CHARINDEX(@separator, @str)
    UNION ALL
    SELECT
        p + 1, 
        b + 1, 
        CHARINDEX(@separator, @str, b + 1)
    FROM tokens
    WHERE b > 0
)
SELECT
    p-1 ItemIndex,
    SUBSTRING(
        @str, 
        a, 
        CASE WHEN b > 0 THEN b-a ELSE LEN(@str) END) 
    AS Item
FROM tokens
);





GO
GRANT VIEW DEFINITION ON  [dbo].[fnSplitString] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnSplitString] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnSplitString] TO [sp_executeall]
GO
