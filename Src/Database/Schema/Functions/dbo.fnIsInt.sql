SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-20
-- Description:	Check to see if a varchar contains a valid int
-- =============================================
CREATE FUNCTION [dbo].[fnIsInt] 
(
	@value varchar(2000)
)
RETURNS bit
AS
BEGIN
	
	SELECT @value = ltrim(rtrim(@value))
	
	RETURN CASE
			WHEN @value IS NULL THEN 0
			WHEN @value = '' THEN 1
			WHEN datalength(@value) > 9 THEN 0
			WHEN PatIndex('%[^0-9]%', @value) = 0 THEN 1
			ELSE 
				CASE
				WHEN @value LIKE '-%' AND PatIndex('%[^0-9]%', substring(@value, 2, len(@value))) = 0 THEN 1
				ELSE 0
				END	
			END
			
END









GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsInt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnIsInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsInt] TO [sp_executeall]
GO
