SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-10-05
-- Description:	Get the default commercial VAT Rate for the date specified.
-- =============================================
CREATE FUNCTION [dbo].[GetVATRateByDate] 
(
@ValidDate DATE
)
RETURNS numeric(18,2)
AS
BEGIN

	DECLARE @Rate numeric(18,2) 
	
	SELECT @Rate = v.VATRate
	FROM VATRates v WITH (NOLOCK)
	WHERE @ValidDate BETWEEN v.ValidFromDate and v.ValidToDate

	RETURN @Rate

END








GO
GRANT VIEW DEFINITION ON  [dbo].[GetVATRateByDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetVATRateByDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetVATRateByDate] TO [sp_executeall]
GO
