SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2007-10-15
-- Description:	Only returns Lead Types this user is allowed to see
-- JWG 2010-11-03 NOLOCKS added throughout
-- =============================================
CREATE FUNCTION [dbo].[fnLeadTypeSecure] 
(	
	@UserID int
)
RETURNS TABLE 
AS
RETURN 
(
	with UserGroup as
	(
		select ClientPersonnel.ClientPersonnelAdminGroupID as 'GroupID', ClientPersonnel.ClientID as 'ClientID', ClientPersonnel.ClientOfficeID as 'OfficeID'  
		from dbo.ClientPersonnel WITH (NOLOCK) 
		where ClientPersonnelID = @UserID
	),
	UserLeadTypes as
	(
		SELECT gfc.ModuleID, gfc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, gfc.HasDescendants, gfc.RightID, gfc.LeadTypeID 
		FROM GroupFunctionControl gfc WITH (NOLOCK) 
		INNER JOIN FunctionType ft WITH (NOLOCK) ON ft.FunctionTypeID = gfc.FunctionTypeID and ft.ModuleID = gfc.ModuleID 
		WHERE gfc.ClientPersonnelAdminGroupID = (select GroupID from UserGroup WITH (NOLOCK))
		and gfc.FunctionTypeID = 23
		AND NOT EXISTS (SELECT * FROM UserFunctionControl ufc1 WITH (NOLOCK) WHERE ufc1.functiontypeid = gfc.functiontypeid and ufc1.ClientPersonnelID = @UserID)
		UNION
		SELECT ufc.ModuleID, ufc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, ufc.HasDescendants, ufc.RightID, ufc.LeadTypeID 
		FROM UserFunctionControl ufc WITH (NOLOCK) 
		INNER JOIN FunctionType ft WITH (NOLOCK) ON ft.FunctionTypeID = ufc.FunctionTypeID and ft.ModuleID = ufc.ModuleID 
		WHERE ufc.ClientPersonnelID = @UserID
		and ufc.FunctionTypeID = 23
	),
	LeadTypesByOffice as 
	(
		select objectid, rightid, LeadTypeID
		from grouprightsdynamic WITH (NOLOCK) 
		where clientpersonneladmingroupid = (select GroupID from UserGroup WITH (NOLOCK))
		and grouprightsdynamic.FunctionTypeID = 11
		and objectid = (select OfficeID from UserGroup WITH (NOLOCK))
		and not exists (select * from userrightsdynamic WITH (NOLOCK) where userrightsdynamic.functiontypeid = grouprightsdynamic.functiontypeid and isnull(userrightsdynamic.leadtypeid,-1) = isnull(grouprightsdynamic.leadtypeid, -1) and userrightsdynamic.objectid = grouprightsdynamic.objectid and userrightsdynamic.clientpersonnelid = @UserID)
		union
		select objectid, rightid, LeadTypeID
		from userrightsdynamic WITH (NOLOCK) 
		where clientpersonnelid = @UserID
		and userrightsdynamic.FunctionTypeID = 11
		and objectid = (select OfficeID from UserGroup)
	),
	UserFunctions as 
	(
		SELECT gfc.ModuleID, gfc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, gfc.HasDescendants, gfc.RightID 
		FROM GroupFunctionControl gfc WITH (NOLOCK) 
		INNER JOIN FunctionType ft WITH (NOLOCK) ON ft.FunctionTypeID = gfc.FunctionTypeID and ft.ModuleID = gfc.ModuleID 
		WHERE gfc.ClientPersonnelAdminGroupID = (select GroupID from UserGroup WITH (NOLOCK))
		and gfc.FunctionTypeID = 7
		AND NOT EXISTS (SELECT * FROM UserFunctionControl ufc1 WITH (NOLOCK) WHERE ufc1.functiontypeid = gfc.functiontypeid and ufc1.ClientPersonnelID = @UserID)
		UNION
		SELECT ufc.ModuleID, ufc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, ufc.HasDescendants, ufc.RightID 
		FROM UserFunctionControl ufc WITH (NOLOCK) 
		INNER JOIN FunctionType ft WITH (NOLOCK) ON ft.FunctionTypeID = ufc.FunctionTypeID and ft.ModuleID = ufc.ModuleID 
		WHERE ufc.ClientPersonnelID = @UserID
		and ufc.FunctionTypeID = 7
	),
	UserLeadManagerModule as 
	(
		SELECT gfc.ModuleID, gfc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, gfc.HasDescendants, gfc.RightID 
		FROM GroupFunctionControl gfc WITH (NOLOCK) 
		INNER JOIN FunctionType ft WITH (NOLOCK) ON ft.FunctionTypeID = gfc.FunctionTypeID and ft.ModuleID = gfc.ModuleID 
		WHERE gfc.ClientPersonnelAdminGroupID = (select GroupID from UserGroup WITH (NOLOCK))
		and gfc.FunctionTypeID = 1
		AND NOT EXISTS (SELECT * FROM UserFunctionControl ufc1 WITH (NOLOCK) WHERE ufc1.functiontypeid = gfc.functiontypeid and ufc1.ClientPersonnelID = @UserID)
		UNION
		SELECT ufc.ModuleID, ufc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, ufc.HasDescendants, ufc.RightID 
		FROM UserFunctionControl ufc WITH (NOLOCK) 
		INNER JOIN FunctionType ft WITH (NOLOCK) ON ft.FunctionTypeID = ufc.FunctionTypeID and ft.ModuleID = ufc.ModuleID 
		WHERE ufc.ClientPersonnelID = @UserID
		and ufc.FunctionTypeID = 1
	),
	AggregatedLeadTypes as
	(
		select LeadTypeID, RightID 
		from UserLeadTypes  WITH (NOLOCK) where RightID > 0
		and not exists (select * from LeadTypesByOffice WITH (NOLOCK) where LeadTypesByOffice.LeadTypeID = UserLeadTypes.LeadTypeID and LeadTypesByOffice.RightID < UserLeadTypes.RightID)
		union
		select LeadTypeID, RightID 
		from LeadTypesByOffice where RightID > 0
		and not exists (select * from UserLeadTypes WITH (NOLOCK) where UserLeadTypes.LeadTypeID = LeadTypesByOffice.LeadTypeID and UserLeadTypes.RightID < LeadTypesByOffice.RightID)
	)
	/*
		A user has one of these things: 
		(1) an overall record specifying that they can or cannot see this Lead Type ("UserLeadTypes" above)
		and/or
		(2) Lead Types restricted by office ("LeadTypesByOffice" above)
		and/or
		(3) an overall record specifying that they can or cannot see all Lead Types ("UserFunctions" above)
		or
		(4) an overall Lead Manager module control record
	*/
	-- (1) and (2) are now combined to form AggregatedLeadTypes
	-- Take the most restrictive rights found in the event of a tie
	-- So if this group has full access to the PPI lead type in general,
	-- but view-only access from the Bolton office, then make sure Bolton
	-- users get the view-only rights, not full rights.
	select LeadTypeID, RightID 
	from AggregatedLeadTypes where RightID > 0

	union

	select LeadTypeID, RightID  
	from LeadType WITH (NOLOCK) 
	INNER JOIN UserGroup WITH (NOLOCK) ON LeadType.ClientID = UserGroup.ClientID 
	CROSS JOIN UserFunctions WITH (NOLOCK)
	where not exists (select * from AggregatedLeadTypes WITH (NOLOCK) where AggregatedLeadTypes.LeadTypeID = LeadType.LeadTypeID and AggregatedLeadTypes.RightID = 0)
	and UserFunctions.RightID > 0
	and UserFunctions.HasDescendants = 0

	union

	select LeadTypeID, RightID  
	from LeadType WITH (NOLOCK) 
	INNER JOIN UserGroup WITH (NOLOCK) ON LeadType.ClientID = UserGroup.ClientID  
	CROSS JOIN UserLeadManagerModule WITH (NOLOCK)
	where not exists (select * from UserFunctions WITH (NOLOCK) where RightID = 0 and HasDescendants = 0)
	and not exists (select * from AggregatedLeadTypes WITH (NOLOCK) where AggregatedLeadTypes.LeadTypeID = LeadType.LeadTypeID and AggregatedLeadTypes.RightID = 0)
	and UserLeadManagerModule.RightID > 0
	and UserLeadManagerModule.HasDescendants = 0
	
)












GO
GRANT VIEW DEFINITION ON  [dbo].[fnLeadTypeSecure] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnLeadTypeSecure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnLeadTypeSecure] TO [sp_executeall]
GO
