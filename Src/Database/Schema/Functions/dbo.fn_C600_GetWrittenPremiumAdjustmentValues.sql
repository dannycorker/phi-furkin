SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2019-10-24
-- Description:	Calculate adjustment values between two table rows
-- 2020-02-12 CPS for JIRA AAG-106 | No longer used in its previous form.  Update to calculate the adj. values in advance of a new PPPSD (wp) record.
-- 2020-03-18 GPR for AAG-512	| Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- 2020-03-20 GPR | Updated tax related column names for AAG-507
-- 2020-03-24 GPR | Added AdjustmentValueLocalTax for AAG-516
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetWrittenPremiumAdjustmentValues]
(
	 @MatterID								INT
	,@CoverPeriodStartDate					DATE
	,@CoverPeriodEndDate					DATE
	,@AdjustmentTypeID						INT
	,@NewAnnualPremiumGross					DECIMAL(18,2)
	,@NewAnnualPremiumNet					DECIMAL(18,2)
	,@NewAnnualDiscountCommissionSacrifice	DECIMAL(18,2)
	,@NewAnnualDiscountPremiumAffecting		DECIMAL(18,2)
	,@NewAnnualPremiumNationalTax			DECIMAL(18,2)
	,@NewAnnualPremiumLocalTax				DECIMAL(18,2)
)
RETURNS 
@AdjustmentValues TABLE
(
	 AdjustmentValueGross					DECIMAL(18,8)
	,AdjustmentValueNET						DECIMAL(18,8)
	,AdjustmentDiscountCommissionSacrifice	DECIMAL(18,8)
	,AdjustmentDiscountCommissionPremium	DECIMAL(18,8)
	,AdjustmentValueNationalTax				DECIMAL(18,4) /*2020-04-15 GPR AAG-642*/
	,AdjustmentValueLocalTax				DECIMAL(18,4) /*2020-03-24 GPR AAG-516*/
)
AS
BEGIN

	--[Adjustment To Written Premium] = [Total Payable For This Year] - [Sum of Adjustment Values So Far]
	DECLARE	 @ThisYearStartDate DATE

	SELECT TOP 1 @ThisYearStartDate = tdvDateSta.ValueDate
	FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN TableDetailValues tdvDateSta WITH (NOLOCK) on tdvDateSta.TableRowID = tr.TableRowID AND tdvDateSta.DetailFieldID = 145663 /*Start*/
	INNER JOIN TableDetailValues tdvDateEnd WITH (NOLOCK) on tdvDateEnd.TableRowID = tr.TableRowID AND tdvDateEnd.DetailFieldID = 145664 /*End*/
	WHERE tr.MatterID = @MatterID
	AND tr.DetailFieldID = 170033 /*Historical Policy*/
	AND @CoverPeriodStartDate >  tdvDateSta.ValueDate
	AND @CoverPeriodStartDate <= tdvDateEnd.ValueDate

	/*Work out the new values by subtracting old from new*/
	INSERT @AdjustmentValues (	 AdjustmentValueGross					
								,AdjustmentValueNET						
								--,AdjustmentValueNationalTax						
								,AdjustmentDiscountCommissionSacrifice	
								,AdjustmentDiscountCommissionPremium
								,AdjustmentValueNationalTax	/*2020-04-16 GPR*/
								,AdjustmentValueLocalTax /*2020-03-24 GPR AAG-516*/	
							 )
	SELECT	 (ISNULL(@NewAnnualPremiumGross					,0.00)	- ISNULL(soFar.AnnualPriceForRiskGross				,0.00) )
			,(ISNULL(@NewAnnualPremiumNet					,0.00)	- ISNULL(soFar.AnnualPriceForRiskNET				,0.00) )
			,(ISNULL(@NewAnnualDiscountCommissionSacrifice	,0.00)	- ISNULL(soFar.AnnualDiscountCommissionSacrifice	,0.00) )
			,(ISNULL(@NewAnnualDiscountPremiumAffecting		,0.00)	- ISNULL(soFar.AnnualDiscountPremiumAffecting		,0.00) )
			,(ISNULL(@NewAnnualPremiumNationalTax			,0.00)	- ISNULL(soFar.AnnualPriceForRiskNationalTax		,0.00) ) /*2020-04-15 GOR AAG-642*/
			,(ISNULL(@NewAnnualPremiumLocalTax				,0.00)	- ISNULL(soFar.AnnualPriceForRiskLocalTax			,0.00) ) /*2020-03-24 GPR AAG-516*/
	FROM 
		(	/*AdjustmentValuesSoFar*/
			SELECT	 SUM(wp.PremiumCalculationID					) [PremiumCalculationDetailID] /*2020-04-08 GPR AAG-634 | updated from wp.WrittenPremiumID*/
					,SUM(wp.AdjustmentValueGross					) [AnnualPriceForRiskGross]
					,SUM(wp.AdjustmentValueNet						) [AnnualPriceForRiskNET]
					,SUM(wp.AdjustmentDiscountCommissionSacrifice	) [AnnualDiscountCommissionSacrifice]
					,SUM(wp.AdjustmentDiscountCommissionPremium		) [AnnualDiscountPremiumAffecting]
					,SUM(wp.AnnualPriceForRiskNationalTax			) [AnnualPriceForRiskNationalTax] /*2020-04-16 GPR AAG-642*/
					,SUM(wp.AnnualPriceForRiskLocalTax				) [AnnualPriceForRiskLocalTax]  /*2020-03-24 GPR AAG-516*/
			FROM WrittenPremium wp WITH (NOLOCK) 
			WHERE wp.MatterID = @MatterID
			AND wp.ValidFrom >= @ThisYearStartDate
		) soFar
	
	RETURN

END






GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetWrittenPremiumAdjustmentValues] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_GetWrittenPremiumAdjustmentValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetWrittenPremiumAdjustmentValues] TO [sp_executeall]
GO
