SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 14-July-2009
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[fnGetTableColumnSum]
(
	@LMDetailFieldID		INT,
	@ColumnDetailFieldID	INT,
	@CaseID					INT
)
RETURNS MONEY
AS
BEGIN

DECLARE @LeadOrMatter INT,
		@DetailFieldPageID INT,
		@DetailValue MONEY = 0.00

	SELECT @LeadOrMatter = LeadOrmatter, @DetailFieldPageID = DetailFieldPageID
	FROM DetailFields WITH (NOLOCK)
	WHERE DetailFieldID = @LMDetailFieldID

	IF @LeadOrMatter = 1
	BEGIN

		SELECT @DetailValue = SUM(tdv.ValueMoney)
		FROM Cases c WITH (NOLOCK) 
		INNER JOIN TableRows tr WITH (NOLOCK) ON tr.LeadID = c.LeadID AND tr.DetailFieldPageID =  @DetailFieldPageID
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = @ColumnDetailFieldID
		WHERE c.CaseID = @CaseID 
	
	END

	IF @LeadOrMatter = 2
	BEGIN

		SELECT @DetailValue = SUM(tdv.ValueMoney)
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN TableRows tr WITH (NOLOCK) ON tr.MatterID = m.MatterID AND tr.DetailFieldPageID =  @DetailFieldPageID
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = @ColumnDetailFieldID
		WHERE m.CaseID = @CaseID 
	
	END

	IF @LeadOrMatter = 10
	BEGIN

		SELECT @DetailValue = SUM(tdv.ValueMoney)
		FROM Matter m WITH (NOLOCK) 
		INNER JOIN TableRows tr WITH (NOLOCK) ON tr.CustomerID = m.CustomerID AND tr.DetailFieldPageID =  @DetailFieldPageID
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = @ColumnDetailFieldID
		WHERE m.CaseID = @CaseID 
	
	END

	RETURN @DetailValue

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetTableColumnSum] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetTableColumnSum] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetTableColumnSum] TO [sp_executeall]
GO
