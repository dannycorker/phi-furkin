SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-02-28
-- Description:	Return all wins by lead type and date range
--              Very similar to fnGetWinsDisputedByLeadType so consider changes to both reports
-- JWG 2011-06-24 Make sure the Client/Matter and Field/Date indexes are hit on DVH every time. NOLOCK throughout.
-- =============================================
CREATE FUNCTION [dbo].[fnGetWinsByLeadType] 
(
	@LeadTypeID int, 
	@DateFrom datetime,
	@DateTo datetime,
	@Flags varchar(2000) 
)
RETURNS 
@CurrentWins TABLE 
(
	CustomerID int,
	LeadID int,
	CaseID int,
	MatterID int,
	WinAmount varchar(100),
	OfferAmount varchar(100),
	WinCount int,
	FirstValidSave datetime,
	LastValidSave datetime
)
AS
BEGIN

	-- Make sure we are selecting all possible times within the date range specified
	SELECT  @DateFrom = convert(datetime, convert(char(10), @DateFrom, 126) + ' 00:00:00'),
			@DateTo = convert(datetime, convert(char(10), @DateTo, 126) + ' 23:59:59')

	-- Set up various fields of interest, depending on the lead type and flags requested
	DECLARE @WinAmountFieldID1 int, 
	@WinAmountFieldID2 int, 
	@InitialOfferFieldID int, 
	@ClientID int

	DECLARE @EventTypes TABLE (
		EventTypeID int,
		MakesAWin bit,
		StopsAWin bit
	)

	--- (Card PPI)  
	IF @LeadTypeID = 150
	BEGIN
		SELECT @WinAmountFieldID1 = 6718, 
		@WinAmountFieldID2 = NULL, 
		@InitialOfferFieldID = 6718, 
		@ClientID = 3
	END

	--- (Loan Charges)
	IF @LeadTypeID = 114
	BEGIN
		SELECT @WinAmountFieldID1 = 10458, 
		@WinAmountFieldID2 = NULL, 
		@InitialOfferFieldID = 10457, 
		@ClientID = 3
	END

	--- (Whole of Life)
	IF @LeadTypeID = 137
	BEGIN
		SELECT @WinAmountFieldID1 = 6538, 
		@WinAmountFieldID2 = NULL, 
		@InitialOfferFieldID = 6538, 
		@ClientID = 3
	END


	-- BF PPI Charges = 37
	IF @LeadTypeID = 37
	BEGIN
		-- There is no field for initial offer amount in PPI. The same field
		-- is used, and the sub-status changes to show when it gets accepted.
		SELECT @WinAmountFieldID1 = 1070, 
		@WinAmountFieldID2 = NULL, 
		@InitialOfferFieldID = 1070, 
		@ClientID = 3

	END
	-- BF Bank Charges = 46 (contains some Credit Card charges as well for historical reasons though)
	IF @LeadTypeID = 46
	BEGIN
		SELECT @WinAmountFieldID1 = 2123, 
		@WinAmountFieldID2 = 741, 
		@InitialOfferFieldID = 2122, 
		@ClientID = 3

		IF @Flags = 'BC'
		BEGIN
			INSERT INTO @EventTypes(EventTypeID, MakesAWin, StopsAWin)
			VALUES (1802, 0, 1)
		END
		IF @Flags = 'CC'
		BEGIN
			INSERT INTO @EventTypes(EventTypeID, MakesAWin, StopsAWin)
			VALUES (1802, 1, 0)
		END
	END
	-- BF Credit Card Charges = 122
	IF @LeadTypeID = 122
	BEGIN
		SELECT @WinAmountFieldID1 = 4634, 
		@WinAmountFieldID2 = NULL, 
		@InitialOfferFieldID = 4633, 
		@ClientID = 3
	END
	-- BF Mortgage Charges = 152
	IF @LeadTypeID = 152
	BEGIN
		SELECT @WinAmountFieldID1 = 7123, 
		@WinAmountFieldID2 = NULL, 
		@InitialOfferFieldID = 7122, 
		@ClientID = 3
	END


	-- Select all Matters with a win recorded in that range (some will be filtered out below)	
	DECLARE @CasesWithBlockEvents TABLE (
		bCaseID int
	)

	DECLARE @CasesWithWinEvents TABLE (
		wCaseID int
	)

	DECLARE @PossibleValidMatters TABLE (
		MatterID int,
		DetailFieldID int
	)

	INSERT @PossibleValidMatters (MatterID, DetailFieldID) 
	SELECT dvh.MatterID, @WinAmountFieldID1
	FROM DetailValueHistory dvh WITH (NOLOCK) 
	WHERE dvh.ClientID = @ClientID 
	AND dvh.DetailFieldID = @WinAmountFieldID1
	AND dvh.WhenSaved BETWEEN @DateFrom AND @DateTo

	IF @WinAmountFieldID2 IS NOT NULL
	BEGIN
		INSERT @PossibleValidMatters (MatterID, DetailFieldID) 
		SELECT dvh2.MatterID, @WinAmountFieldID2
		FROM DetailValueHistory dvh2 WITH (NOLOCK)
		WHERE dvh2.ClientID = @ClientID 
		AND dvh2.DetailFieldID = @WinAmountFieldID2
		AND dvh2.WhenSaved BETWEEN @DateFrom AND @DateTo
		AND NOT EXISTS (
			SELECT *
			FROM @PossibleValidMatters pvm1 
			WHERE pvm1.MatterID = dvh2.MatterID
		)
	END

	-- Note the date of the first NON-BLANK entry (ie no time-wasters!)
	DECLARE @FirstValidSave TABLE (
		MatterID int,
		DetailFieldID int,
		WhenSaved datetime
	)

	INSERT @FirstValidSave (MatterID, WhenSaved, DetailFieldID)
	SELECT dvh.MatterID, MIN(dvh.WhenSaved), dvh.DetailFieldID
	FROM @PossibleValidMatters pvm
	INNER JOIN DetailValueHistory dvh WITH (NOLOCK) ON dvh.MatterID = pvm.MatterID AND dvh.DetailFieldID = pvm.DetailFieldID AND replace(dvh.FieldValue, '£', '') <> ''
	GROUP BY dvh.MatterID, dvh.DetailFieldID 


	-- For Matters that do have a NON-BLANK entry somewhere in their history, note down the
	-- latest date that a value was saved so that we can count the valid entries.
	DECLARE @LatestValidSave TABLE (
		MatterID int,
		DetailFieldID int,
		WhenSaved datetime
	)

	INSERT @LatestValidSave (MatterID, WhenSaved, DetailFieldID)
	SELECT dvh.MatterID, MAX(dvh.WhenSaved), dvh.DetailFieldID
	FROM DetailValueHistory dvh WITH (NOLOCK)
	INNER JOIN @FirstValidSave fvs ON fvs.MatterID = dvh.MatterID AND fvs.DetailFieldID = dvh.DetailFieldID
	WHERE dvh.WhenSaved >= fvs.WhenSaved
	GROUP BY dvh.MatterID, dvh.DetailFieldID 


	-- Now pick out the results to return
	-- This selects Matters with a valid win between the date range 
	-- requested (from @FirstValidSave and @LastValidSave),
	-- the current Win Amount figure (from MatterDetailValues),
	-- and the total count of valid saves for all time (DetailValueHistory, @FirstValidSave and @LastValidSave)
	-- REPLACE currency symbol and commas with blanks so they can be converted to numbers.
	INSERT @CurrentWins (CustomerID, LeadID, CaseID, MatterID, WinAmount, OfferAmount, FirstValidSave, LastValidSave, WinCount) 
	SELECT m.CustomerID, m.LeadID, m.CaseID, m.MatterID, replace(replace(mdv_win.DetailValue, '£', ''), ',', ''), replace(replace(isnull(mdv_offer.DetailValue, ''), '£', ''), ',', ''), fvs.WhenSaved, lvs.WhenSaved, count(*)
	FROM @LatestValidSave lvs 
	INNER JOIN @FirstValidSave fvs ON fvs.MatterID = lvs.MatterID
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = lvs.MatterID
	INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = m.CustomerID
	INNER JOIN MatterDetailValues mdv_win WITH (NOLOCK) ON mdv_win.MatterID = fvs.MatterID AND mdv_win.DetailFieldID = fvs.DetailFieldID
	LEFT JOIN MatterDetailValues mdv_offer WITH (NOLOCK) ON mdv_offer.MatterID = fvs.MatterID AND mdv_offer.DetailFieldID = @InitialOfferFieldID 
	INNER JOIN DetailValueHistory dvh WITH (NOLOCK) ON dvh.MatterID = lvs.MatterID AND dvh.DetailFieldID = mdv_win.DetailFieldID AND dvh.WhenSaved BETWEEN fvs.WhenSaved AND lvs.WhenSaved
	WHERE c.LastName <> 'Test'  /*Changed from not like 'Test%' on request of AoH on support ticket 4727*/
	GROUP BY m.CustomerID, m.LeadID, m.CaseID, m.MatterID, mdv_win.DetailValue, mdv_offer.DetailValue, fvs.WhenSaved, lvs.WhenSaved


	-- Finally, apply any Event-based rules specified in the Flags etc
	-- We need to ignore records if a "blocking event" is in place
	INSERT @CasesWithBlockEvents (bCaseID)
	SELECT cw.CaseID 
	FROM @CurrentWins cw 
	WHERE EXISTS (
		SELECT CaseID 
		FROM @EventTypes et 
		INNER JOIN LeadEvent le WITH (NOLOCK) ON le.EventTypeID = et.EventTypeID AND le.EventDeleted = 0
		WHERE cw.CaseID = le.CaseID 
		AND et.StopsAWin = 1
	)
	
	-- We also need to ignore records if a "winning event" is not in place
	INSERT @CasesWithWinEvents (wCaseID)
	SELECT cw.CaseID 
	FROM @CurrentWins cw 
	WHERE EXISTS (
		SELECT * FROM @EventTypes et WHERE et.MakesAWin = 1
	)
	AND EXISTS (
		SELECT CaseID 
		FROM @EventTypes et 
		INNER JOIN LeadEvent le WITH (NOLOCK) ON le.EventTypeID = et.EventTypeID AND le.EventDeleted = 0
		WHERE cw.CaseID = le.CaseID 
		AND et.MakesAWin = 1
	)
	UNION
	-- unless of course no "winning event" is required
	SELECT cw.CaseID 
	FROM @CurrentWins cw 
	WHERE NOT EXISTS (
		SELECT * FROM @EventTypes et WHERE et.MakesAWin = 1
	)

	-- Remove blocked wins
	DELETE @CurrentWins 
	FROM @CasesWithBlockEvents b
	WHERE CaseID = b.bCaseID

	-- Remove unwanted wins
	DELETE @CurrentWins 
	WHERE NOT EXISTS (
		SELECT * FROM @CasesWithWinEvents w WHERE w.wCaseID = CaseID
	)


	-- Allow the function to return @CurrentWins to the calling SELECT statement
	RETURN 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetWinsByLeadType] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnGetWinsByLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetWinsByLeadType] TO [sp_executeall]
GO
