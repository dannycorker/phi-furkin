SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2007-10-04
-- Description:	Get this User's rights for this Lead
-- JWG 2010-07-01 New security for leads that are assigned to others, or not assigned at all.
-- JWG 2011-08-03 New ClientPersonnelAccess table for granting access without (re)assigning the lead.
-- JWG 2014-02-06 #25493 Specific Access to include level of access: Full,View or None
-- JWG 2015-04-08 #31591 User group access request 
-- =============================================
CREATE FUNCTION [dbo].[fnUserLeadAccess] 
(
	@UserID INT, 
	@LeadID INT
)
RETURNS TINYINT
AS
BEGIN

	DECLARE @Allowed TINYINT = 0,
	@Decided BIT = 0,
	@AssignedTo INT,
	@GroupID INT, @ClientID INT, @OfficeID INT, 
	@LeadTypeID INT, @LeadClientID INT, 
	@LMRights TINYINT, @LMDescendants BIT,
	@LeadTypeRights TINYINT, @LeadTypeDescendants BIT,
	@OfficeRights TINYINT, @OfficeDescendants BIT,
	@OutcomeRights TINYINT, @OutcomeDescendants BIT,
	@DynamicRecordsFound BIT, @LeadTypeOther TINYINT, 
	@UnassignedLeads TINYINT, @LeadsAssignedToOthers TINYINT,
	@SpecificAccessLevel TINYINT = NULL 


	/*
		1) If the lead is assigned to the user, they have full access, no questions asked,
		otherwise start checking user and group function control records.
		
		2) If the user has access to LeadManager, with no descendant rights, then use those LM rights (can be NONE, VIEW or FULL).
		
		3) Provisionally set the access rights to whatever is found in LM Lead Types
		
		4) Check individual rights for specific Lead Types
		
		5) Check office rights
		
		6) Check outcome rights
		
		A definitive "NONE" at any point means the user has no access and stops any further checks from taking place.
		
	*/
	
	
	/* 
		Initially, set access to "NONE" and mark that
		we have not decided the permissions yet.
		JWG #31591 @OfficeID needs to come from the lead you are trying to access, not your own office
	*/
	SELECT	@GroupID = ClientPersonnelAdminGroupID, 
			@ClientID = ClientID 
			-- ,@OfficeID = ClientOfficeID 
	FROM ClientPersonnel WITH (NOLOCK) 
	WHERE ClientPersonnelID = @UserID


	/* 
		Make sure this is a valid user and group
	*/
	IF @GroupID IS NULL
	BEGIN
		SELECT @Allowed = 0, @Decided = 1
	END
	ELSE
	BEGIN
		
		/*
			If the lead is assigned to the user then the answer is 
			"Yes" regardless of any other conditions that exist
		*/
		SELECT @AssignedTo = AssignedTo, @LeadTypeID = LeadTypeID, @LeadClientID = ClientID 
		FROM Lead WITH (NOLOCK) 
		WHERE LeadID = @LeadID

		IF @ClientID <> @LeadClientID
		BEGIN
		
			SELECT @Allowed = 0, @Decided = 1
		
		END
		ELSE
		BEGIN
		
			/* Full rights (4) to amend this lead */
			IF @AssignedTo = @UserID
			BEGIN
				SELECT @Allowed = 4, @Decided = 1
			END
			
		END
		
		/*
			JWG 2014-02-06 #25493
			
			Specific lead/case access can now be revoked as well as granted.
			Possible options are now: NONE, VIEW or FULL.
			The ClientPersonnel level overrides the group level if they both exist.
		*/
		SELECT TOP (1) @SpecificAccessLevel = cpa.AccessLevel 
		FROM dbo.ClientPersonnelAccess cpa WITH (NOLOCK) 
		WHERE cpa.ClientPersonnelID = @UserID 
		AND cpa.LeadID = @LeadID 
		
		/* If any record is found at the ClientPersonnel level then that is a final decision. */
		IF @SpecificAccessLevel IS NOT NULL
		BEGIN
			/* Decision made */
			SELECT @Allowed = @SpecificAccessLevel, @Decided = 1
		END
		ELSE
		BEGIN
			/* Otherwise look up at the group level. Again, if any record is found then that is a final decision. */
			SELECT TOP (1) @SpecificAccessLevel = cpa.AccessLevel 
			FROM dbo.ClientPersonnelAccess cpa WITH (NOLOCK) 
			WHERE cpa.ClientPersonnelAdminGroupID = @GroupID 
			AND cpa.LeadID = @LeadID 
			
			IF @SpecificAccessLevel IS NOT NULL
			BEGIN
				/* Decision made */
				SELECT @Allowed = @SpecificAccessLevel, @Decided = 1
			END
		END
		
		/*
			If specific lead/case access has been granted to the user then the answer is 
			"Yes" regardless of any other conditions that exist
		*/
		/* JWG 2014-02-06 #25493 old code commented out
			IF EXISTS(SELECT * FROM dbo.ClientPersonnelAccess cpa WITH (NOLOCK) WHERE cpa.ClientPersonnelID = @UserID AND cpa.LeadID = @LeadID)
			OR EXISTS(SELECT * FROM dbo.ClientPersonnelAccess cpa WITH (NOLOCK) WHERE cpa.ClientPersonnelAdminGroupID = @GroupID AND cpa.LeadID = @LeadID)
			BEGIN
			
				/* Full rights (4) to amend this lead */
				SELECT @Allowed = 4, @Decided = 1
				
			END
		*/
	END
	
	
	/*
		For each of the following tests, look for individual
		user rights first, but if no overrides are found,
		look for the corresponding group rights instead.
	*/
	IF @Decided = 0
	BEGIN
		
		/*
			If the user/group has full Lead Manager rights
			then look no further
		*/
		SELECT @LMRights = RightID, @LMDescendants = HasDescendants 
		FROM UserFunctionControl WITH (NOLOCK) 
		WHERE ClientPersonnelID = @UserID 
		AND FunctionTypeID = 1

		IF @@ROWCOUNT = 0
		BEGIN
			SELECT @LMRights = RightID, @LMDescendants = HasDescendants 
			FROM GroupFunctionControl WITH (NOLOCK) 
			WHERE ClientPersonnelAdminGroupID = @GroupID 
			AND FunctionTypeID = 1
		END
		
		/*
			If there are no descendants of a FunctionControl record, then
			it has the final say about access to the function type.
		*/
		IF @LMDescendants = 0
		BEGIN
			SELECT @Allowed = @LMRights, @Decided = 1
		END
		
	END


	/*
		There are some descendants for the LM FunctionControl record, 
		so check all the relevant ones to see what they say.
	*/
	IF @Decided = 0
	BEGIN
	
		/*
			Check the LeadManager Other rights, looking for a definitive "No"
		*/
		SELECT @LeadTypeOther = RightID, @LeadTypeDescendants = HasDescendants 
		FROM UserFunctionControl WITH (NOLOCK) 
		WHERE ClientPersonnelID = @UserID 
		AND FunctionTypeID = 10

		IF @@ROWCOUNT = 0
		BEGIN
			SELECT @LeadTypeOther = RightID, @LeadTypeDescendants = HasDescendants 
			FROM GroupFunctionControl WITH (NOLOCK) 
			WHERE ClientPersonnelAdminGroupID = @GroupID 
			AND FunctionTypeID = 10
		END

		SELECT @UnassignedLeads = @LeadTypeOther, @LeadsAssignedToOthers = @LeadTypeOther 

		IF @LeadTypeDescendants = 0
		BEGIN

			/* Provisional rights if nothing overrides it later */
			SET @Allowed = @LeadTypeOther

			/* Definite decision if this is a "No" */
			IF @LeadTypeOther = 0
			BEGIN
				SET @Decided = 1
			END

		END
		ELSE
		BEGIN


			/*
				Reaching this section of code means that LeadManager:Other has descendants,
				so look for the individual rights for access to (109) unassigned leads, and 
				(110) leads that are assigned to other users.
			*/
			
			
			/* User rights for UnassignedLeads */
			SELECT @UnassignedLeads = r.RightID 
			FROM UserRightsDynamic r WITH (NOLOCK) 
			WHERE r.ClientPersonnelID = @UserID 
			AND r.FunctionTypeID = 10
			AND r.ObjectID = 109

			IF @@ROWCOUNT = 0
			BEGIN
				/* Group rights for UnassignedLeads */
				SELECT @UnassignedLeads = r.RightID 
				FROM GroupRightsDynamic r WITH (NOLOCK) 
				WHERE r.ClientPersonnelAdminGroupID = @GroupID 
				AND r.FunctionTypeID = 10
				AND r.ObjectID = 109
			END

			/* User rights for LeadsAssignedToOthers */
			SELECT @LeadsAssignedToOthers = r.RightID 
			FROM UserRightsDynamic r WITH (NOLOCK) 
			WHERE r.ClientPersonnelID = @UserID 
			AND r.FunctionTypeID = 10
			AND r.ObjectID = 110

			IF @@ROWCOUNT = 0
			BEGIN
				/* Group rights for LeadsAssignedToOthers */
				SELECT @LeadsAssignedToOthers = r.RightID 
				FROM GroupRightsDynamic r WITH (NOLOCK) 
				WHERE r.ClientPersonnelAdminGroupID = @GroupID 
				AND r.FunctionTypeID = 10
				AND r.ObjectID = 110
			END

		END
	END
	
	
	/*
		Check LeadType rights
	*/
	IF @Decided = 0
	BEGIN
	
		/*
			Check the Lead rights, looking for a definitive "No"
			User rights
		*/
		SELECT @LeadTypeRights = RightID, @LeadTypeDescendants = HasDescendants 
		FROM UserFunctionControl WITH (NOLOCK) 
		WHERE ClientPersonnelID = @UserID 
		AND FunctionTypeID = 7

		IF @@ROWCOUNT = 0
		BEGIN
			/* Group rights */
			SELECT @LeadTypeRights = RightID, @LeadTypeDescendants = HasDescendants 
			FROM GroupFunctionControl WITH (NOLOCK) 
			WHERE ClientPersonnelAdminGroupID = @GroupID 
			AND FunctionTypeID = 7
		END

		IF @LeadTypeDescendants = 0
		BEGIN

			/* Provisional rights if nothing overrides it later */
			SET @Allowed = @LeadTypeRights

			/* Definite decision if this is a "No" */
			IF @LeadTypeRights = 0
			BEGIN
				SET @Decided = 1
			END

		END
		ELSE
		BEGIN
		
			/*
				Check the specific (23 Lead Type rights, looking for a definitive "No"
				User rights
			*/
			SELECT @LeadTypeRights = RightID, @LeadTypeDescendants = HasDescendants 
			FROM UserFunctionControl WITH (NOLOCK) 
			WHERE ClientPersonnelID = @UserID 
			AND FunctionTypeID = 23
			AND LeadTypeID = @LeadTypeID

			IF @@ROWCOUNT > 0
			BEGIN
				SET @Allowed = @LeadTypeRights

				/* Definite decision if this is a "No" */
				IF @LeadTypeRights = 0
				BEGIN
					SET @Decided = 1
				END

			END
			ELSE
			BEGIN
				/* Group rights */
				SELECT @LeadTypeRights = RightID, @LeadTypeDescendants = HasDescendants 
				FROM GroupFunctionControl WITH (NOLOCK) 
				WHERE ClientPersonnelAdminGroupID = @GroupID 
				AND FunctionTypeID = 23
				AND LeadTypeID = @LeadTypeID

				IF @@ROWCOUNT > 0
				BEGIN
					SET @Allowed = @LeadTypeRights

					/* Definite decision if this is a "No" */
					IF @LeadTypeRights = 0
					BEGIN
						SET @Decided = 1
					END

				END
			END

		END
	END


	/*
		If this code is reached and the decision has not been made then
		it means we have general lead access for this lead type, but we
		now need to check Office security and Outcome security too. 
	*/
	IF @Decided = 0
	BEGIN

		/* User rights */
		SELECT @OfficeRights = RightID, @OfficeDescendants = HasDescendants 
		FROM UserFunctionControl WITH (NOLOCK) 
		WHERE ClientPersonnelID = @UserID 
		AND FunctionTypeID = 8

		IF @@ROWCOUNT = 0
		BEGIN
			/* Group rights */
			SELECT @OfficeRights = RightID, @OfficeDescendants = HasDescendants 
			FROM GroupFunctionControl WITH (NOLOCK) 
			WHERE ClientPersonnelAdminGroupID = @GroupID 
			AND FunctionTypeID = 8
		END

		IF @OfficeDescendants = 0
		BEGIN

			/* No means no. */
			IF @OfficeRights = 0
			BEGIN
				SELECT @Decided = 1, @Allowed = 0
			END

			/* 
				If full access to lead type, but only view/no access
				for this office, then reduce access to that level.
			*/
			IF @OfficeRights < @Allowed
			BEGIN
				SET @Allowed = @OfficeRights
			END

		END
		ELSE
		BEGIN

			/* Goto individual Office dynamic record if necessary */
			SET @DynamicRecordsFound = 0
			
			/*
				JWG #31591 @OfficeID needs to come from the lead you are trying to access, not your own office 
				
				If the lead is not assigned to anyone then we need to rely on the GroupFunctionControl rights above.
			*/
			SELECT @OfficeID = col.ClientOfficeID
			FROM dbo.ClientOfficeLead col WITH (NOLOCK) 
			WHERE col.LeadID = @LeadID 
			
			IF @OfficeID IS NOT NULL
			BEGIN

				SELECT @OfficeRights = RightID, @DynamicRecordsFound = 1 
				FROM UserRightsDynamic  WITH (NOLOCK) 
				WHERE ClientPersonnelID = @UserID 
				AND FunctionTypeID = 11
				AND LeadTypeID = @LeadTypeID
				AND ObjectID = @OfficeID

				IF @@ROWCOUNT = 0
				BEGIN
					/* Group rights */
					SELECT @OfficeRights = RightID, @DynamicRecordsFound = 1 
					FROM GroupRightsDynamic WITH (NOLOCK) 
					WHERE ClientPersonnelAdminGroupID = @GroupID 
					AND FunctionTypeID = 11
					AND LeadTypeID = @LeadTypeID
					AND ObjectID = @OfficeID
				END

				IF @DynamicRecordsFound = 1
				BEGIN

					/* No means no. */
					IF @OfficeRights = 0
					BEGIN
						SELECT @Decided = 1, @Allowed = 0
					END

					/*
						If full access to lead type, but only view/no access
						for this office, then reduce access to that level.
					*/
					IF @OfficeRights < @Allowed
					BEGIN
						SET @Allowed = @OfficeRights
					END

				END
				
			END /* @OfficeID IS NOT NULL */
			
		END
	END
		

	/* Finally, check Outcomes if necessary */
	IF @Decided = 0
	BEGIN

		/* User rights */
		SELECT @OutcomeRights = RightID, @OutcomeDescendants = HasDescendants 
		FROM UserFunctionControl WITH (NOLOCK) 
		WHERE ClientPersonnelID = @UserID 
		AND FunctionTypeID = 9

		IF @@ROWCOUNT = 0
		BEGIN
			/* Group rights */
			SELECT @OutcomeRights = RightID, @OutcomeDescendants = HasDescendants 
			FROM GroupFunctionControl WITH (NOLOCK) 
			WHERE ClientPersonnelAdminGroupID = @GroupID 
			AND FunctionTypeID = 9
		END

		IF @OutcomeDescendants = 0
		BEGIN

			/* No means no. */
			IF @OutcomeRights = 0
			BEGIN
				SELECT @Decided = 1, @Allowed = 0
			END

			/*
				If full access to lead type, but only view/no access
				for this outcome, then reduce access to that level.
			*/
			IF @OutcomeRights < @Allowed
			BEGIN
				SET @Allowed = @OutcomeRights
			END

		END
		ELSE
		BEGIN

			/* Goto individual Office dynamic record if necessary */
			SET @DynamicRecordsFound = 0

			/* User rights */
			SELECT TOP (1) @OutcomeRights = u.RightID, @DynamicRecordsFound = 1 
			FROM UserRightsDynamic u WITH (NOLOCK) 
			INNER JOIN dbo.LeadTypeLink ltl WITH (NOLOCK) ON ltl.OutcomeID = u.ObjectID 
			INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = @LeadID 
			INNER JOIN dbo.Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID 
			INNER JOIN dbo.CustomerOutcomes co WITH (NOLOCK) ON co.CustomerID = c.CustomerID AND co.OutcomeID = ltl.OutcomeID 
			WHERE u.ClientPersonnelID = @UserID 
			AND u.FunctionTypeID = 9
			AND u.LeadTypeID IS NULL
			AND ltl.LeadTypeID = @LeadTypeID 
			ORDER BY u.RightID ASC 

			IF @@ROWCOUNT = 0
			BEGIN
				/* Group rights */
				SELECT TOP (1) @OutcomeRights = g.RightID, @DynamicRecordsFound = 1 
				FROM GroupRightsDynamic g WITH (NOLOCK) 
				INNER JOIN dbo.LeadTypeLink ltl WITH (NOLOCK) ON ltl.OutcomeID = g.ObjectID 
				INNER JOIN dbo.Lead l WITH (NOLOCK) ON l.LeadID = @LeadID 
				INNER JOIN dbo.Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID 
				INNER JOIN dbo.CustomerOutcomes co WITH (NOLOCK) ON co.CustomerID = c.CustomerID AND co.OutcomeID = ltl.OutcomeID 
				WHERE g.ClientPersonnelAdminGroupID = @GroupID 
				AND g.FunctionTypeID = 9 
				AND g.LeadTypeID IS NULL 
				AND ltl.LeadTypeID = @LeadTypeID 
				ORDER BY g.RightID ASC 
			END

			IF @DynamicRecordsFound = 1
			BEGIN

				/* No means no. */
				IF @OutcomeRights = 0
				BEGIN
					SELECT @Decided = 1, @Allowed = 0
				END

				/* 
					If full access to lead type, but only view/no access
					for this office, then reduce access to that level.
				*/
				IF @OutcomeRights < @Allowed
				BEGIN
					SET @Allowed = @OutcomeRights
				END

			END
			ELSE
			BEGIN
				SELECT @Decided = 1, @Allowed = @OutcomeRights
			END
		END

	END


	/*
		If still undecided, review the individual rights to view leads assigned to others etc before returning the final access level.
	*/
	IF @Decided = 0 AND @Allowed > 0
	BEGIN
		
		IF @AssignedTo IS NULL
		BEGIN
			SELECT @Allowed = dbo.fnMinOfInt(@Allowed, @UnassignedLeads)
		END
		ELSE
		BEGIN
			IF @AssignedTo <> @UserID
			BEGIN
				SELECT @Allowed = dbo.fnMinOfInt(@Allowed, @LeadsAssignedToOthers)
			END
		END
				
	END
	
		
	/* Return the result of the function */
	RETURN ISNULL(@Allowed, 0)

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnUserLeadAccess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnUserLeadAccess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnUserLeadAccess] TO [sp_executeall]
GO
