SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-09-01
-- Description:	Looks up the BACS reason code and text
-- Mods
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetBACSReasonText]
(
	@BACSCodeType INT,
	@BACSCode VARCHAR(500)
)
RETURNS VARCHAR(500)	
AS
BEGIN

	DECLARE @FailureReason VARCHAR(500)
	
	-- If an ARUDD row is being added the @BACSCode will have the code description in it, so we do
	-- a reverse lookup for the code
	IF @BACSCodeType = 69985
	BEGIN
	
		DECLARE @ScratchText VARCHAR(100) = ''
		SELECT @ScratchText=cod.DetailValue 
		FROM ResourceListDetailValues typ WITH (NOLOCK) 
		INNER JOIN ResourceListDetailValues cod WITH (NOLOCK) ON typ.ResourceListID=cod.ResourceListID AND cod.DetailFieldID=170198
		INNER JOIN ResourceListDetailValues rea WITH (NOLOCK) ON typ.ResourceListID=rea.ResourceListID
			AND rea.DetailFieldID=170199 AND rea.DetailValue=@BACSCode
		INNER JOIN LookupListItems typli WITH (NOLOCK) ON typ.ValueInt=typli.LookupListItemID
		WHERE typ.DetailFieldID=170200 AND typ.ValueInt=@BACSCodeType

		-- if @scratchtext is blank we can't find the BACS code set to our default unknown and record the text sent
		IF @ScratchText=''
		BEGIN

			SELECT @ScratchText=code.DetailValue, @BACSCode=descr.DetailValue + ' (' + @BACSCode + ')' 
			FROM ResourceListDetailValues code WITH (NOLOCK)
			INNER JOIN ResourceListDetailValues descr WITH (NOLOCK) ON code.ResourceListID=descr.ResourceListID AND descr.DetailFieldID=170199
			WHERE code.DetailFieldID=170198 AND code.ResourceListID=131831		
		
		END
		
		SELECT @FailureReason = '(ARUDD) ' + @BACSCode

		SELECT @BACSCode = CASE WHEN @ScratchText='' THEN 'UN' ELSE @ScratchText END
		
		SELECT @FailureReason = @BACSCode + '|' + @FailureReason
	
	END
	ELSE
	BEGIN
	
		SELECT @FailureReason = @BACSCode + '|' + '(' + typli.ItemValue + ') ' + rea.DetailValue 
		FROM ResourceListDetailValues typ WITH (NOLOCK) 
		INNER JOIN ResourceListDetailValues cod WITH (NOLOCK) ON typ.ResourceListID=cod.ResourceListID 
			AND cod.DetailFieldID=170198
			AND cod.DetailValue=@BACSCode
		INNER JOIN ResourceListDetailValues rea WITH (NOLOCK) ON typ.ResourceListID=rea.ResourceListID
			AND rea.DetailFieldID=170199
		INNER JOIN LookupListItems typli WITH (NOLOCK) ON typ.ValueInt=typli.LookupListItemID
		WHERE typ.DetailFieldID=170200 AND typ.ValueInt=@BACSCodeType
	
	END

	RETURN @FailureReason
	
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetBACSReasonText] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetBACSReasonText] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetBACSReasonText] TO [sp_executeall]
GO
