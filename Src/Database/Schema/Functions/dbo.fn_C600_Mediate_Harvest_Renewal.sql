SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire
-- Create date:	2020-07-30
-- Description:	harvests renewal json object
-- 2021-02-05 ALM Include new status for vision: 1 Active, 2 Cancelled, 3 Cancelled to Inception
-- 2021-03-30 ALM Updated status for Vision: 1 Live, 2 Cancelled, 3 Cancellation Pending
-- 2021-05-07 ALM Updated to include Excess and CoInsurance FURKIN-600
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Mediate_Harvest_Renewal]
(
	 @PolicyID			INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE  @JsonData	VARCHAR(MAX) =''
			,@PetJson	VARCHAR(MAX) = ''

	DECLARE @Products Table (ProductID INT, MatterID INT, Wellness INT, StartDate DATE, EndDate DATE)

	INSERT INTO @Products
	--SELECT TOP 1 rldv.DetailValue, tr1.MatterID, 1, tdv1.ValueDate, tdv2.ValueDate
	--FROM Tablerows tr1 WITH (NOLOCK) 
	--INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr1.TableRowID AND tdv.DetailFieldID = 314234
	--INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = tdv.ResourceListID AND rldv.DetailFieldID = 314268
	--INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tdv.TableRowID AND tdv1.DetailFieldID = 314237
	--INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = tdv.TableRowID AND tdv2.DetailFieldID = 314239
	--INNER JOIN TableDetailValues tdv3 WITH (NOLOCK) ON tdv3.TableRowID = tdv.TableRowID AND tdv3.DetailFieldID = 314236
	--WHERE 
	--tr1.DetailFieldID = 314240
	--AND tr1.MatterID = @PolicyID
	--AND tdv3.ValueInt = 74577
	--UNION
	SELECT mdvClaimProductID.ValueInt, @PolicyID, 0, NULL, NULL 
	FROM MatterDetailValues mdvClaimProductID WITH (NOLOCK)
	WHERE mdvClaimProductID.MatterID = dbo.fn_C00_1273_GetCurrentSchemeFromMatter (@PolicyID)
	AND mdvClaimProductID.DetailFieldID = 313875 /*Claim Product ID*/

	IF @PolicyID > 0
	BEGIN
		SELECT  @JsonData +=
		(

		SELECT TOP 1
			p.productid				[productId]
			,CAST(tdvExcess.ValueMoney AS DECIMAL(18,2)) [fixedExcessLimit]
			,CAST(tdvCoInsurance.ValueMoney AS DECIMAL(18,2)) [coInsuranceLimit]	
			,mdv.ValueDateTime		[startDate]
			,mdv1.ValueDateTime		[endDate]
			,CASE WHEN mdvStatus.ValueInt = 43003 THEN 2 /*Cancelled*/
				  WHEN mdvStatus.ValueInt = 74535 THEN 3 /*Cancellation Pending*/
				  ELSE 1 /*Live*/
			 END					[status]
			FROM @Products p
			INNER JOIN Matter m								WITH (NOLOCK) ON m.MatterID = p.MatterID
			INNER JOIN Lead l								WITH (NOLOCK) ON l.LeadID = m.LeadID
			INNER JOIN Customers cu							WITH (NOLOCK) ON cu.CustomerID = l.CustomerID
			JOIN MatterDetailValues mdv						WITH (NOLOCK) ON mdv.MatterID = m.MatterID AND mdv.DetailFieldID = 170036
			JOIN MatterDetailValues mdv1					WITH (NOLOCK) ON mdv1.MatterID = m.MatterID AND mdv1.DetailFieldID = 170037
			INNER JOIN MatterDetailValues mdvStatus			WITH (NOLOCK) on mdvStatus.MatterID = m.MatterID					AND mdvStatus.DetailFieldID			= 170038 /*Policy Status*/
			LEFT JOIN MatterDetailValues mdvDateIncep		WITH (NOLOCK) on mdvDateIncep.MatterID = m.MatterID					AND mdvDateIncep.DetailFieldID		= 170035 /*Policy Inception Date*/
			INNER JOIN TableRows tr							WITH (NOLOCK) on tr.MatterID = m.MatterID							AND tr.DetailFieldID				= 170033 /*Historical Policy*/
			INNER JOIN TableDetailValues tdvExcess			WITH (NOLOCK) ON tdvExcess.TableRowID = tr.TableRowID				AND tdvExcess.DetailFieldID			= 177416 /*Excess*/
			INNER JOIN TableDetailValues tdvCoInsurance		WITH (NOLOCK) ON tdvCoInsurance.TableRowID = tr.TableRowID			AND tdvCoInsurance.DetailFieldID	= 177415 /*CoInsurance*/
			WHERE m.MatterID = @PolicyID
			AND l.LeadTypeID = 1492 /*Policy Admin*/

			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		)
	END


	RETURN @JsonData

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Mediate_Harvest_Renewal] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_Mediate_Harvest_Renewal] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Mediate_Harvest_Renewal] TO [sp_executeall]
GO
