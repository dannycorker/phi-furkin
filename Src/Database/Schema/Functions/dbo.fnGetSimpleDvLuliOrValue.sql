SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-12-04
-- Description:	Selects a simple detail field from the appropriate detail value
-- decoding lookup list items only where the field is a lookup field
-- used when the DetailFieldID to look up is dynamically selected
-- =============================================
CREATE FUNCTION [dbo].[fnGetSimpleDvLuliOrValue]
(
	@DetailFieldID INT,
	@ObjectID INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE @ItemValue		VARCHAR(2000),
			@LeadOrMatter	INT,
			@IsLookup		BIT

	SELECT @LeadOrMatter = LeadOrMatter, @IsLookup = [Lookup]
	FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @DetailFieldID
	
	IF @IsLookup = 1
	BEGIN
		SELECT @ItemValue = dbo.fnGetSimpleDvLuli(@DetailFieldID,@ObjectID)
	END
	ELSE
	BEGIN
		SELECT @ItemValue = dbo.fnGetSimpleDv(@DetailFieldID,@ObjectID)
	END
	
	RETURN @ItemValue

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleDvLuliOrValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetSimpleDvLuliOrValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleDvLuliOrValue] TO [sp_executeall]
GO
