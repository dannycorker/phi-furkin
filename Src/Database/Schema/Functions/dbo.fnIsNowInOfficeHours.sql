SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-07-08
-- Description:	Determine if now is within office hours.
-- =============================================
CREATE FUNCTION [dbo].[fnIsNowInOfficeHours]
(
	@OpeningTimeFieldID INT,
	@ClosingTimeFieldID INT,
	@MondayFieldID INT,
	@TuesdayFieldID INT,
	@WednesdayFieldID INT,
	@ThursdayFieldID INT,
	@FridayFieldID INT,
	@SaturdayFieldID INT,
	@SundayFieldID INT
)
RETURNS INT
AS
BEGIN

	/*Return 1 for in office hours 0 for outside*/

	DECLARE @ReturnValue INT = 0,
			@IsValidDay INT,
			@DayNum INT,
			@DayFieldID INT,
			@StartTime TIME,
			@EndTime TIME

	/*Daynum, 1=Sunday*/
	SELECT @DayNum = DATEPART(WEEKDAY, dbo.fn_GetDate_Local())
	
	SELECT @DayFieldID = CASE 
			WHEN @DayNum = 1 THEN @SundayFieldID
			WHEN @DayNum = 2 THEN @MondayFieldID
			WHEN @DayNum = 3 THEN @TuesdayFieldID
			WHEN @DayNum = 4 THEN @WednesdayFieldID
			WHEN @DayNum = 5 THEN @ThursdayFieldID
			WHEN @DayNum = 6 THEN @FridayFieldID
			WHEN @DayNum = 7 THEN @SaturdayFieldID
			END

	SELECT @IsValidDay = dbo.fnGetSimpleDvAsInt(@DayFieldID, 372)
	
	IF @IsValidDay = 1
	BEGIN
	
		/*Ok, so today is valid, now is the time valid*/
		SELECT @StartTime = dbo.fnGetSimpleDv(@OpeningTimeFieldID, 372),
				@EndTime = dbo.fnGetSimpleDv(@ClosingTimeFieldID, 372)
				
		IF CONVERT(TIME,dbo.fn_GetDate_Local()) BETWEEN @StartTime AND @EndTime
		BEGIN
		
			SELECT @ReturnValue = 1
		
		END
	
	END
	
	RETURN @ReturnValue

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsNowInOfficeHours] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnIsNowInOfficeHours] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsNowInOfficeHours] TO [sp_executeall]
GO
