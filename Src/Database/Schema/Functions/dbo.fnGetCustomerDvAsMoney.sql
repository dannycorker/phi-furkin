SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-12-11
-- Description:	Get Customer DetailValue
-- =============================================
CREATE FUNCTION [dbo].[fnGetCustomerDvAsMoney]
(
	@DetailFieldID		int,
	@CustomerID			int
)
RETURNS money
AS
BEGIN

	DECLARE @DetailValue as money

	Select @DetailValue = cdv.ValueMoney 
	From CustomerDetailValues cdv with (nolock) 
	Where cdv.CustomerID = @CustomerID 
	and cdv.DetailFieldID = @DetailFieldID
	
	RETURN @DetailValue

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerDvAsMoney] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetCustomerDvAsMoney] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerDvAsMoney] TO [sp_executeall]
GO
