SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Conor Reeves
-- Create date: 2019-06-20
-- Description:	To concatenate the LeadID's into a single string 
-- 2019-07-16 CR updated the function to resolve the issue with the commas.
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_ConcatLeadID]
(
	@CustomerID	INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE @MyListOfLeadID VARCHAR(2000) = ''

	SELECT @MyListOfLeadID  += CAST (l.Leadid as Varchar) + ', '
	FROM Lead l WITH ( NOLOCK ) 
	INNER JOIN dbo.matter m WITH ( NOLOCK ) on m.LeadID = l.LeadID
	INNER JOIN MatterDetailValues PolicyStatus WITH (NOLOCK) on PolicyStatus.MatterID = m.MatterID AND PolicyStatus.DetailFieldID = 170038 --Policy Status
	INNER JOIN MatterDetailValues OMFOutcomes	WITH (NOLOCK) on OMFOutcomes.MatterID = m.MatterID AND OMFOutcomes.DetailFieldID = 179964 -- OMF Outcomes
	WHERE l.CustomerID = 2023868 --@CustomerID
	and l.LeadTypeID = 1492
	and m.MatterRef = 'One Month Free'
	AND PolicyStatus.ValueInt not in (43003,43004,74535,76595) -- Cancelled Lapsed CancelPending and Void
	and OMFOutcomes.ValueInt not in (76418)

	
	IF @@ROWCOUNT > 0
	BEGIN
		SELECT @MyListOfLeadID = LEFT (@MyListOfLeadID, LEN(@MyListOfLeadID) -1)
	END

	RETURN @MyListOfLeadID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_ConcatLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_ConcatLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_ConcatLeadID] TO [sp_executeall]
GO
