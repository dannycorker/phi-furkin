SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-04-16
-- Description:	Take a table field, column field and sum all column values for rows that belong to the object
--				i.e. if the table is at matter level and a CustomerID is specified, sum all rows for all matters
--              belonging to the Customer
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_SumTableColumnForAllRowsBelongingToObjectID]
(
	@TableDetailFieldID		INT,
	@ColumnDetailFieldID	INT,
	@ObjectID				INT,
	@ObjectSubTypeID		INT
)
RETURNS Numeric(18,2)
AS
BEGIN
	
	DECLARE @DetailFieldSubTypeID	INT,
			@ReturnValue			Numeric(18,2) = 0,
			@ClientID				INT,
			@CustomerID				INT,
			@LeadID                 INT,
			@CaseID					INT,
			@MatterID				INT,
			@SQLCmd					VARCHAR(MAX)
			
	DECLARE @ObjectIDList dbo.tvpIntInt		
			
	
	SELECT @DetailFieldSubTypeID = df.LeadOrMatter
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @TableDetailFieldID

	IF @ObjectSubTypeID NOT IN (1,2,11,10,12) RETURN 0
	
	/* This is very long winded but using a select statement like:

		SELECT m.MatterID,COUNT(*) 
		FROM Customers c WITH (NOLOCK)
		INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
		INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
		INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID=ca.CaseID
		WHERE (@ObjectSubTypeID = 1 and l.LeadID = @ObjectID)
				OR
			  (@ObjectSubTypeID = 2 and m.MatterID = @ObjectID) 
				OR etc
	
		has very poor performance
	 */

	-- get candidate objects list
	-- 1. Client Object
	IF @ObjectSubTypeID=12
	BEGIN
		-- 1a. Matter Table
		IF @DetailFieldSubTypeID=2
		BEGIN
			INSERT INTO @ObjectIDList (ID1,ID2)
			SELECT m.MatterID,COUNT(*) 
			FROM Customers c WITH (NOLOCK)
			INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
			INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
			INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID=ca.CaseID
			WHERE c.ClientID=@ObjectID
			GROUP BY m.MatterID
		END
		-- 1b. Case Table
		IF @DetailFieldSubTypeID=11
		BEGIN
			INSERT INTO @ObjectIDList (ID1,ID2)
			SELECT ca.CaseID,COUNT(*) 
			FROM Customers c WITH (NOLOCK)
			INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
			INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
			WHERE c.ClientID=@ObjectID
			GROUP BY ca.CaseID
		END
		-- 1c. Lead Table
		IF @DetailFieldSubTypeID=11
		BEGIN
			INSERT INTO @ObjectIDList (ID1,ID2)
			SELECT l.LeadID,COUNT(*) 
			FROM Customers c WITH (NOLOCK)
			INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
			WHERE c.ClientID=@ObjectID
			GROUP BY l.LeadID
		END
		-- 1d. Customer Table
		IF @DetailFieldSubTypeID=11
		BEGIN
			INSERT INTO @ObjectIDList (ID1,ID2)
			SELECT c.CustomerID,COUNT(*) 
			FROM Customers c WITH (NOLOCK)
			WHERE c.ClientID=@ObjectID
			GROUP BY c.CustomerID
		END
		-- 1d. Client Table
		IF @DetailFieldSubTypeID=11
		BEGIN
			INSERT INTO @ObjectIDList (ID1,ID2)
			SELECT c.ClientID,COUNT(*) 
			FROM Customers c WITH (NOLOCK)
			WHERE c.ClientID = @ObjectID
			GROUP BY c.ClientID
		END
	END
	-- 2. Customer Object
	ELSE IF @ObjectSubTypeID=10
	BEGIN
		-- 2a. Matter Table
		IF @DetailFieldSubTypeID=2
		BEGIN
			INSERT INTO @ObjectIDList (ID1,ID2)
			SELECT m.MatterID,COUNT(*) 
			FROM Customers c WITH (NOLOCK)
			INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
			INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
			INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID=ca.CaseID
			WHERE c.CustomerID=@ObjectID
			GROUP BY m.MatterID
		END
		-- 2b. Case Table
		IF @DetailFieldSubTypeID=11
		BEGIN
			INSERT INTO @ObjectIDList (ID1,ID2)
			SELECT ca.CaseID,COUNT(*) 
			FROM Customers c WITH (NOLOCK)
			INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
			INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
			WHERE c.CustomerID=@ObjectID
			GROUP BY ca.CaseID
		END
		-- 2c. Lead Table
		IF @DetailFieldSubTypeID=11
		BEGIN
			INSERT INTO @ObjectIDList (ID1,ID2)
			SELECT l.LeadID,COUNT(*) 
			FROM Customers c WITH (NOLOCK)
			INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
			WHERE c.CustomerID=@ObjectID
			GROUP BY l.LeadID
		END
		-- 2d. Customer Table
		IF @DetailFieldSubTypeID=11
		BEGIN
			INSERT INTO @ObjectIDList (ID1,ID2)
			SELECT c.CustomerID,COUNT(*) 
			FROM Customers c WITH (NOLOCK)
			WHERE c.CustomerID=@ObjectID
			GROUP BY c.CustomerID
		END
	END
	-- 3. Lead Object
	ELSE IF @ObjectSubTypeID=1
	BEGIN
		-- 3a. Matter Table
		IF @DetailFieldSubTypeID=2
		BEGIN
			INSERT INTO @ObjectIDList (ID1,ID2)
			SELECT m.MatterID,COUNT(*) 
			FROM Customers c WITH (NOLOCK)
			INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
			INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
			INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID=ca.CaseID
			WHERE l.LeadID=@ObjectID
			GROUP BY m.MatterID
		END
		-- 3b. Case Table
		IF @DetailFieldSubTypeID=11
		BEGIN
			INSERT INTO @ObjectIDList (ID1,ID2)
			SELECT ca.CaseID,COUNT(*) 
			FROM Customers c WITH (NOLOCK)
			INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
			INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
			WHERE l.LeadID=@ObjectID
			GROUP BY ca.CaseID
		END
		-- 3c. Lead Table
		IF @DetailFieldSubTypeID=11
		BEGIN
			INSERT INTO @ObjectIDList (ID1,ID2)
			SELECT l.LeadID,COUNT(*) 
			FROM Customers c WITH (NOLOCK)
			INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
			WHERE l.LeadID=@ObjectID
			GROUP BY l.LeadID
		END
	END
	-- 4. Case Object
	ELSE IF @ObjectSubTypeID=11
	BEGIN
		-- 4a. Matter Table
		IF @DetailFieldSubTypeID=2
		BEGIN
			INSERT INTO @ObjectIDList (ID1,ID2)
			SELECT m.MatterID,COUNT(*) 
			FROM Customers c WITH (NOLOCK)
			INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
			INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
			INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID=ca.CaseID
			WHERE ca.CaseID=@ObjectID
			GROUP BY m.MatterID
		END
		-- 4b. Case Table
		IF @DetailFieldSubTypeID=11
		BEGIN
			INSERT INTO @ObjectIDList (ID1,ID2)
			SELECT ca.CaseID,COUNT(*) 
			FROM Customers c WITH (NOLOCK)
			INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
			INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
			WHERE ca.CaseID=@ObjectID
			GROUP BY ca.CaseID
		END
	END
	-- 5. Matter Object
	ELSE IF @ObjectSubTypeID=2
	BEGIN
		-- 5a. Matter Table
		IF @DetailFieldSubTypeID=2
		BEGIN
			INSERT INTO @ObjectIDList (ID1,ID2)
			SELECT m.MatterID,COUNT(*) 
			FROM Customers c WITH (NOLOCK)
			INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
			INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
			INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID=ca.CaseID
			WHERE m.MatterID=@ObjectID
			GROUP BY m.MatterID
		END
	END
	
	-- calculate column sum for all rows in object list

	SELECT @ReturnValue=ISNULL(SUM(tdv.ValueMoney),0) 
	FROM TableRows tr		
	INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID=tr.TableRowID
	WHERE tr.DetailFieldID = @TableDetailFieldID
	and tdv.DetailFieldID  = @ColumnDetailFieldID
	and (
		@DetailFieldSubTypeID = 1  AND tr.LeadID IN ( SELECT ID1 FROM @ObjectIDList  )
	OR	@DetailFieldSubTypeID = 2  AND tr.MatterID IN ( SELECT ID1 FROM @ObjectIDList  )
	OR	@DetailFieldSubTypeID = 11 AND tr.CaseID IN ( SELECT ID1 FROM @ObjectIDList  )
	OR	@DetailFieldSubTypeID = 10 AND tr.CustomerID IN ( SELECT ID1 FROM @ObjectIDList  )
	OR	@DetailFieldSubTypeID = 12 AND tr.CLientID IN ( SELECT ID1 FROM @ObjectIDList  )
		)

	RETURN @ReturnValue
END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumTableColumnForAllRowsBelongingToObjectID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_SumTableColumnForAllRowsBelongingToObjectID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumTableColumnForAllRowsBelongingToObjectID] TO [sp_executeall]
GO
