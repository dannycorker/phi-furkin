SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-07-05
-- Description:	Function to return the highest value from a table
-- =============================================
CREATE FUNCTION [dbo].[fnGetHighestDateTDV]
(
	@AnyID INT,
	@DetailFieldSubTypeID INT,
	@TableFieldID INT,
	@ColumnFieldID INT
)
RETURNS VARCHAR(10)
AS
BEGIN

	DECLARE @MaxValue VARCHAR(10)
	
	/*
		For a particular Lead, Matter, Customer or Case look through all the rows on the specified 
		table (@TableFieldID) and return the highest money value in the column specified (@ColumnFieldID)
	*/
	IF @DetailFieldSubTypeID = 1
	BEGIN
	
		SELECT @MaxValue = CONVERT(VARCHAR(10),MAX(tdv.ValueDate),120)
		FROM dbo.TableRows tr WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID =  @ColumnFieldID 
		WHERE tr.LeadID = @AnyID 
		AND tr.DetailFieldID = @TableFieldID 
	
	END
	ELSE
	IF @DetailFieldSubTypeID = 2
	BEGIN

		SELECT @MaxValue = CONVERT(VARCHAR(10),MAX(tdv.ValueDate),120)
		FROM dbo.TableRows tr WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID =  @ColumnFieldID 
		WHERE tr.MatterID = @AnyID 
		AND tr.DetailFieldID = @TableFieldID 
	
	END
	ELSE
	IF @DetailFieldSubTypeID = 10
	BEGIN

		SELECT @MaxValue = CONVERT(VARCHAR(10),MAX(tdv.ValueDate),120)
		FROM dbo.TableRows tr WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID =  @ColumnFieldID 
		WHERE tr.CustomerID = @AnyID 
		AND tr.DetailFieldID = @TableFieldID 
	
	END
	ELSE
	IF @DetailFieldSubTypeID = 11
	BEGIN

		SELECT @MaxValue = CONVERT(VARCHAR(10),MAX(tdv.ValueDate),120)
		FROM dbo.TableRows tr WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID =  @ColumnFieldID 
		WHERE tr.CaseID = @AnyID 
		AND tr.DetailFieldID = @TableFieldID 
	
	END
	
	/* This can be NULL */
	RETURN @MaxValue

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetHighestDateTDV] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetHighestDateTDV] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetHighestDateTDV] TO [sp_executeall]
GO
