SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2018-07-06
-- Description:	Take a PremiumCalculationID and display its pricing breakdown horizontally.  Used by fast rater within Aquarium.
-- 2019-09-18 CPS for Offline Renewals	| Split premium based on the post-discount price
-- 2020-02-13 CPS for JIRA AAG-091		| Return Net as Gross-Tax
-- 2020-03-24 GPR for JIRA AAG-516		| Return LocalTax
-- 2020-08-26 SB						| Change tax selections to get the output rather than delta for US setup
-- =============================================
CREATE FUNCTION [dbo].[fn_RulesEngine_PremiumCalculationDataBreakdown]
(
	 @PremiumCalculationDetailID	INT
)
RETURNS @Data TABLE
(
	 TermsDate				DATE			NULL
	,SchemeID				INT				NULL
	,PolicyMatterID			INT				NULL
	,RuleSetID				INT				NULL
	,AnnualPremium			DECIMAL(18,4)	NULL
	,Discount				DECIMAL(18,4)	NULL
	,PremiumLessDiscount	DECIMAL(18,4)	NULL
	,FirstMonthly			DECIMAL(18,4)	NULL
	,RecurringMonthly		DECIMAL(18,4)	NULL
	,Net					DECIMAL(18,4)	NULL
	,Commission				DECIMAL(18,4)	NULL
	,GrossNet				DECIMAL(18,4)	NULL
	,DiscountGrossNet		DECIMAL(18,4)	NULL
	,PAFIfBeforeIPT			DECIMAL(18,4)	NULL
	,PAFBeforeIPTGrossNet	DECIMAL(18,4)	NULL
	,IPT					DECIMAL(18,4)	NULL
	,IPTGrossNet			DECIMAL(18,4)	NULL
	,PAFIfAfterIPT			DECIMAL(18,4)	NULL
	,GrossGross				DECIMAL(18,4)	NULL
	,PremiumCalculationID	INT				NULL
	,LocalTax				DECIMAL(18,4)	NULL
)									
AS									
BEGIN								

	DECLARE  @GrossNet					DECIMAL(18,4) = 0.0
			,@GrossNetWithDiscount		DECIMAL(18,4) = 0.0
			,@GrossGross				DECIMAL(18,4) = 0.0
			,@IPT						DECIMAL(18,4) = 0.0
			,@IPTGrossNet				DECIMAL(18,4) = 0.0
			,@Commission				DECIMAL(18,4) = 0.0
			,@Discount					DECIMAL(18,4) = 0.0
			,@PAF						DECIMAL(18,4) = 0.0
			,@PAFIfBeforeIPT			DECIMAL(18,4) = 0.0
			,@PAFBeforeIPTGrossNet		DECIMAL(18,4) = 0.0
			,@PAFIfAfterIPT				DECIMAL(18,4) = 0.0
			,@FirstMonthly				DECIMAL(18,4) = 0.0
			,@RecurringMonthly			DECIMAL(18,4) = 0.0
			,@GrossGrossPlusDiscount	DECIMAL(18,4) = 0.0
			,@AnnualPremium				DECIMAL(18,4) = 0.00
			,@TermsDate					DATE
			,@SchemeID					INT
			,@PolicyMatterID			INT
			,@RuleSetID					INT
			,@LocalTax					DECIMAL(18,4) = 0.0

	SELECT TOP (1)	 @AnnualPremium = CAST(pcdv.RuleOutput AS DECIMAL(18,2))
					,@RuleSetID		= r.RuleSetID
	FROM dbo.PremiumCalculationDetailValues pcdv WITH ( NOLOCK ) 
	INNER JOIN dbo.RulesEngine_Rules r WITH ( NOLOCK ) ON r.RuleID = pcdv.RuleID
	WHERE pcdv.PremiumCalculationDetailID = @PremiumCalculationDetailID
	AND ISNUMERIC(pcdv.RuleOutput) = 1
	ORDER BY pcdv.RuleSequence DESC

	-- Calculate key premium data
	IF @AnnualPremium > 0
	BEGIN
				
		SELECT	 @FirstMonthly		= FirstMonthly
				,@RecurringMonthly	= RecurringMonthly
		FROM dbo.fn_C00_1273_SplitPremium(@AnnualPremium)

		SELECT @Discount = ROUND(SUM(CAST(pcdv.RuleInputOutputDelta as DECIMAL(18,4))),2)
		FROM dbo.PremiumCalculationDetailValues pcdv WITH ( NOLOCK ) 
		WHERE pcdv.PremiumCalculationDetailID = @PremiumCalculationDetailID AND pcdv.RuleCheckpoint IN ('Discount', 'Promotion') /*GPR 2021-03-29 added Promotion for PETSURE-843*/
		AND ISNUMERIC(pcdv.RuleInputOutputDelta) = 1

		-- In our TPet default and AAG modes then the transform tax scaler is like 1.015 then we take the delta
		-- for Prudent and other US the transform is like 0.015 and so we can take the output
		SELECT @IPT = 
		--CASE 
		--	WHEN pcdv.RuleTransformValue > 1.00 AND ISNUMERIC(pcdv.RuleInputOutputDelta) = 1 
		--		THEN ROUND(CAST(pcdv.RuleInputOutputDelta AS DECIMAL(18,2)),2)
		--	WHEN pcdv.RuleTransformValue < 1.00 AND ISNUMERIC(pcdv.RuleOutput) = 1
		--		THEN ROUND(CAST(pcdv.RuleOutput AS DECIMAL(18,2)),2)
		--	ELSE
		--		0.00
		--END
		pcdv.RuleOutput
		FROM dbo.PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
		WHERE pcdv.PremiumCalculationDetailID = @PremiumCalculationDetailID AND pcdv.RuleCheckpoint IN ('IPT','GST', 'State Tax') /*GPR 2020-03-09 changed to GST for AAG*/ /*GPR 2020-08-05 added State Tax*/ /*GPR 2021-02-08 added Retail Sales Tax as 'Province Tax'*/ /*GPR 2021-03-09 removed 'Province Tax'*/

		SELECT @LocalTax = 
		--CASE 
		--	WHEN pcdv.RuleTransformValue > 1.00 AND ISNUMERIC(pcdv.RuleInputOutputDelta) = 1 
		--		THEN ROUND(CAST(pcdv.RuleInputOutputDelta AS DECIMAL(18,2)),2)
		--	WHEN pcdv.RuleTransformValue < 1.00 AND ISNUMERIC(pcdv.RuleOutput) = 1
		--		THEN ROUND(CAST(pcdv.RuleOutput AS DECIMAL(18,2)),2)
		--	ELSE
		--		0.00
		--END
		pcdv.RuleOutput
		FROM dbo.PremiumCalculationDetailValues pcdv WITH (NOLOCK) 
		WHERE pcdv.PremiumCalculationDetailID = @PremiumCalculationDetailID AND pcdv.RuleCheckpoint IN ('Stamp Duty', 'Municipal Tax', 'Province Tax') /*GPR 2020-08-05 added Municipal Tax*/ /*GPR 2021-03-09 added Retail Sales Tax as 'Province Tax'*/

		-- Old code was duplicating the select above.  Now this has been edited to handle both AAG and Prudent tax evaluations
		-- and there is confusion over what this is, we are just defaulting to the @LocalTax value.
		-- This can almost certainly be removed if needed!
		SELECT @PAFIfAfterIPT = @LocalTax

		IF @LocalTax IS NULL
		BEGIN
			SELECT @LocalTax = 0.00
		END

		IF @IPT IS NULL
		BEGIN
			SELECT @IPT = 0.00
		END

		SELECT	 @GrossGrossPlusDiscount	= ISNULL(@AnnualPremium,0.00)-- + ISNULL(ABS(@Discount),0.00)
				,@GrossGross				= @AnnualPremium
				,@GrossNetWithDiscount		= @AnnualPremium
				/*GPR 2020-04-23 calculate Gross Net*/
				,@GrossNet					= @AnnualPremium - (ABS(@LocalTax) + ABS(@IPT))

		INSERT @Data (TermsDate, SchemeID, PolicyMatterID, RuleSetID, AnnualPremium, Discount, PremiumLessDiscount, FirstMonthly, RecurringMonthly, Net, Commission, GrossNet, DiscountGrossNet, PAFIfBeforeIPT, PAFBeforeIPTGrossNet, IPT, IPTGrossNet, PAFIfAfterIPT, GrossGross, PremiumCalculationID, LocalTax)
		VALUES (@TermsDate, @SchemeID, @PolicyMatterID, @RuleSetID, @GrossGrossPlusDiscount, @Discount, @GrossGross, @FirstMonthly, @RecurringMonthly, @AnnualPremium-(@IPT+@LocalTax) /*GPR 2020-03-24 included @LocalTax*/, @Commission, @GrossNet, @GrossNetWithDiscount, @PAFIfBeforeIPT, @PAFBeforeIPTGrossNet, @IPT, @IPTGrossNet, @PAFIfAfterIPT, @GrossGross, @PremiumCalculationDetailID, @LocalTax)

	END

	RETURN

END


















GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_PremiumCalculationDataBreakdown] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_RulesEngine_PremiumCalculationDataBreakdown] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_PremiumCalculationDataBreakdown] TO [sp_executeall]
GO
