SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-02-07
-- Description:	Returns the correct policy for a customer's policy and claim
-- Modified by Robin Hall 7/1/2013 to look elsewhere for the date to use if not found on the matter
-- Modified by Robin Hall 11/3/2013 Changes to the way the scheme is determined by Policy Type and Affinity.
-- Modified by Robin Hall 27/3/2013 Checks for Upgrade/Downgrade and returns NULL (no cover) if so
-- Modified by Robin Hall 23/10/2013 Revised upgrade/downgrade checking
-- ROH 2014-04-30 Get the policy type from the claim (condition onset) instead of current policy
-- ROH 2014-06-10 All reinstatement claims to use TreatmentStart, not just continuation Zen#27132
-- SB  2014-10-09 Refactored date logic into fn_C224_GetDateToUseForCalcs for use elsewhere
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetPolicyFromClaim]
(
	@ClaimMatterID INT
)
RETURNS INT	
AS
BEGIN
	
	DECLARE  @SchemeID				INT
			,@TreatmentStart					DATE
			,@DateOfLoss						DATE
			,@DateToUse							DATE
			,@CurrentPolicyMatterID		INT
			,@PolicyStart							DATE
			,@IsRenewal							BIT = 0
			,@UpgradeDowngrade			INT = 0
			,@SpeciesID							INT
		


	-- Determine date to use in the following order 
	-- Matter Treatment Start, Claim Row Treatment Start, Date of Loss, Today
	
	-- Get Matter Treatment Start
	SELECT @TreatmentStart = mdvTreatmentStart.ValueDate,@DateOfLoss = mdvDateOfLoss.ValueDate
	FROM dbo.Matter m WITH (NOLOCK) 
	INNER JOIN dbo.MatterDetailValues mdvTreatmentStart WITH (NOLOCK) ON m.MatterID = mdvTreatmentStart.MatterID AND mdvTreatmentStart.DetailFieldID = 144366 --TreatmentStart
	INNER JOIN dbo.MatterDetailValues mdvDateOfLoss WITH (NOLOCK) ON m.MatterID = mdvDateOfLoss.MatterID AND mdvDateOfLoss.DetailFieldID = 144892 --DateOfLoss
	WHERE m.MatterID = @ClaimMatterID

	IF @TreatmentStart IS NULL
	-- Look on the claim rows (common scenario for SAP imported claims)
	BEGIN
	
		SELECT @TreatmentStart = MIN(tdvTreatmentStart.ValueDate)
		FROM dbo.Matter m WITH (NOLOCK)
		INNER JOIN dbo.TableRows r WITH (NOLOCK) ON r.MatterID = m.MatterID AND r.DetailFieldID = 144355 --ClaimRowData
		INNER JOIN dbo.TableDetailValues tdvTreatmentStart WITH (NOLOCK) ON tdvTreatmentStart.TableRowID = r.TableRowID AND tdvTreatmentStart.DetailFieldID = 144349 --TreatmentStart
		WHERE m.MatterID = @ClaimMatterID

	END
	
	/* CHECK FOR LEVEL OF COVER CHANGE BETWEEN DATE OF LOSS AND TREATMENT START */
	--SELECT @UpgradeDowngrade = dbo.fn_C00_1272_CheckUpgradeDowngrade(@ClaimMatterID,@DateOfLoss,@TreatmentStart)
	--IF @UpgradeDowngrade IS NULL
	--BEGIN
	--	-- Policy has changed cover between DateOfLoss and TreatmentStart and cover is denied
	--	RETURN NULL
	--END

	SELECT @SpeciesID = rdv.ValueInt
	FROM LeadDetailValues ldv WITH ( NOLOCK ) 
	INNER JOIN ResourceListDetailValues rdv WITH ( NOLOCK ) on rdv.ResourceListID = ldv.ValueInt
	WHERE ldv.LeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimMatter(@ClaimMatterID)
	AND ldv.DetailFieldID = 144272 /*Pet Type*/
	AND rdv.DetailFieldID = 144269 /*Pet Type (Species)*/
	
	SELECT @DateToUse = dbo.fn_C00_1272_GetDateToUseForCalcs(@ClaimMatterID, @DateOfLoss, @TreatmentStart)
	
	-- Now find the matching scheme resource list and the policy start date	
	SELECT @SchemeID = tdvScheme.ResourceListID, @PolicyStart = tdvStart.ValueDate
	,@IsRenewal = CASE WHEN tdvStart.ValueDate > tdvInception.ValueDate THEN 1 ELSE 0 END
	FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@ClaimMatterID) r
	INNER JOIN dbo.TableDetailValues tdvScheme WITH (NOLOCK) ON r.TableRowID = tdvScheme.TableRowID AND tdvScheme.DetailFieldID = 145665
	INNER JOIN dbo.TableDetailValues tdvInception WITH (NOLOCK) ON r.TableRowID = tdvInception.TableRowID AND tdvInception.DetailFieldID = 145662
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664
	WHERE @DateToUse >= tdvStart.ValueDate
	AND @DateToUse <= tdvEnd.ValueDate

	-- Now get the right scheme matter
	--SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_GetPolicyFromTypeAndDate(@SchemeID, @PolicyStart, @IsRenewal)
	SELECT @CurrentPolicyMatterID = dbo.fn_C00_1273_GetCurrentSchemeWithSpecies(@SchemeID, @PolicyStart, @SpeciesID)
	-- Check whether "Old terms" apply (Sainsbury's) and switch to old SchemeMatter if so
	--SELECT @CurrentPolicyMatterID = dbo.fn_C00_1272_ApplyOldExcessTerms(@ClaimMatterID,@CurrentPolicyMatterID)
	
	-- And return it
	RETURN @CurrentPolicyMatterID

END









GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicyFromClaim] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_GetPolicyFromClaim] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicyFromClaim] TO [sp_executeall]
GO
