SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-06-12
-- Description:	Get CustomerDV directly #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetCustomerDvCASEAsDate]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS date
AS
BEGIN

	DECLARE @ValueDate AS date

	SELECT @ValueDate = cdv.ValueDate 
	FROM Matter c WITH (NOLOCK) 
	INNER JOIN CustomerDetailValues cdv WITH (NOLOCK) ON c.CustomerID = cdv.CustomerID AND cdv.DetailFieldID = @DetailFieldID
	WHERE c.CaseID = @CaseID 
	
	RETURN @ValueDate

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerDvCASEAsDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetCustomerDvCASEAsDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerDvCASEAsDate] TO [sp_executeall]
GO
