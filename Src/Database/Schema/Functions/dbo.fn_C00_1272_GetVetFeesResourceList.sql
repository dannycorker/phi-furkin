SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-10-01
-- Description:	Returns the vet fees resrource list
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetVetFeesResourceList]
(
	@ClientID INT
)
RETURNS INT	
AS
BEGIN
	
	DECLARE @VetFeesID INT
	
	SELECT  @VetFeesID = rl.ResourceListID
	FROM dbo.ResourceList rl WITH (NOLOCK) 
	INNER JOIN dbo.ResourceListDetailValues rdvSec WITH (NOLOCK) ON rl.ResourceListID = rdvSec.ResourceListID AND rdvSec.DetailFieldID = 146189
	INNER JOIN dbo.ResourceListDetailValues rdvSub WITH (NOLOCK) ON rl.ResourceListID = rdvSub.ResourceListID AND rdvSub.DetailFieldID = 146190
	WHERE rdvSec.ValueInt in (74540)--(73961,74284,74301,74317,74302)
	AND rdvSub.ValueInt = 74283 
	AND rl.ClientID = @ClientID
	
	-- And return it
	RETURN @VetFeesID

END









GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetVetFeesResourceList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_GetVetFeesResourceList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetVetFeesResourceList] TO [sp_executeall]
GO
