SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2020-11-02
-- Description:	Get expiry date from year and month
-- =============================================
CREATE FUNCTION [dbo].[fnBillingGetExpiryFromYearAndMonth]
(
	@ExpiryYear VARCHAR(20),
	@ExpiryMonth VARCHAR(20)
)
RETURNS DATE
AS
BEGIN

	DECLARE @ExpiryDate DATE

	/*
		Construct the date set to the first of the month
		Add one month to get the first of the next month
		Take off one day to get the last date of the current month
	*/
	SELECT @ExpiryDate = DATEADD(DD, -1, DATEADD(MM, 1, CONVERT(DATE,'20' + @ExpiryYear + '-' + @ExpiryMonth + '-' + '01')))

	RETURN @ExpiryDate

END
GO
GRANT EXECUTE ON  [dbo].[fnBillingGetExpiryFromYearAndMonth] TO [sp_executeall]
GO
