SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-10-01
-- Description:	More refactoring to get the excess without customer details for quote
-- Modified		ROH 2013-01-17 Added selection for pet age dependent excess rows
--				ROH 2013-01-17 Added processing for applying old excess terms (Sainsbury's)
--				ROH 2013-01-18 Added retreival of ExcessCeiling field
--              ROH 2013-01-29 Added second sort term to final selection of excess row so that e.g. £125/15% returned before £125.
--              ROH 2013-01-31 Waive voluntary excess for Minimum Contribution excess rows
--              ROH 2013-02-21 Handle policies with no pet type defined 
--				ROH 2014-07-08 Modified priority when multiple matches for Excess rows. Breed > Postcode > Species > Age > ExcessValue
--				ROH 2015-03-02 Bug fix with Partition clause
--				SB	2015-10-26 Fix / finish the work to define the date used for age related excess.  Add in policy age filter.
--				SB	2015-12-10 Allow excesses to be pulled down from sections only defined at optional coverage level
--				SB	2015-12-17 Policy age now needs to be two values for age at start and end so can return multiple excess rows if appropriate
--				JL  2016-22-08 Added ExcessRule to draw from the excess table (section level rather than scheme level again) 
--				CPS 2017-05-31 Allowed Split.PetAge = 0, and LEFT joined Excess Percentage
--				GPR 2020-07-24 Added Options
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_Policy_GetPolicyExcessWithData]
(
	@CurrentPolicyMatterID INT,
	@CustomerPostCode VARCHAR(2),
	@AnimalBreed VARCHAR(2000),
	@PetDOB DATE,
	@DateOfLoss DATE,
	@AnimalType INT,
	@VoluntaryExcess MONEY,
	@TreatmentDate DATE,
	@PolicyAnniversary DATE,
	@PolicyAgeStart INT,
	@PolicyAgeEnd INT
)
RETURNS 
	@ExcessData TABLE 
	(
		Out_ResourceListID INT,
		ResourceListID INT,
		Section VARCHAR(2000),
		SubSection VARCHAR(2000),
		Excess MONEY,
		ExcessPercentage MONEY,
		Postcode VARCHAR(2000),
		Breed VARCHAR(2000),
		PetAge INT,
		PetType INT,
		ExcessType INT,
		ExcessCeiling MONEY,
		VoluntaryExcess MONEY,
		PolicyAge INT,
		ExcessRule INT,
		Options VARCHAR(2000)
	)
AS
BEGIN
	
	-- Handle the multiple postcodes and breeds
	DECLARE @Raw TABLE
	(
		ID INT IDENTITY,
		ResourceListID INT,
		Excess MONEY,
		ExcessPercentage MONEY,
		Postcode VARCHAR(2000),
		Breed VARCHAR(2000),
		PetAge INT,
		PetType INT,
		ExcessType INT,
		ExcessCeiling MONEY,
		PolicyAge INT,
		ExcessRule INT,
		Options VARCHAR(2000)
	)

	INSERT @Raw (ResourceListID, Excess, ExcessPercentage, Postcode, Breed, PetAge, PetType, ExcessType, ExcessCeiling, PolicyAge,ExcessRule, Options)
	SELECT	tdvRL.ResourceListID AS ResourceListID
	, tdvExcess.ValueMoney AS Excess, tdvExcessPercentage.ValueMoney AS ExcessPercentage
	, tdvPostcode.DetailValue AS Postcode, tdvBreed.DetailValue AS Breed
	, ISNULL(tdvPetAge.ValueInt,0) AS PetAge
	, ISNULL(tdvPetType.ValueInt,0) AS PetType
	, ISNULL(tdvExcessType.valueint,0) as ExcessType
	, ISNULL(tdvExcessCeiling.ValueMoney,0) as ExcessCeiling
	, tdvPolAge.ValueInt as PolicyAge
	, tdvExcessRule.ValueInt as ExcessRule
	, tdvOptions.DetailValue as Options
	FROM dbo.TableDetailValues tdvRL WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvExcess WITH (NOLOCK) ON tdvRL.TableRowID = tdvExcess.TableRowID AND tdvExcess.DetailFieldID = 144360 /*Excess amount*/
	LEFT JOIN dbo.TableDetailValues tdvExcessPercentage WITH (NOLOCK) ON tdvRL.TableRowID = tdvExcessPercentage.TableRowID AND tdvExcessPercentage.DetailFieldID = 146202 /*Min contribution Perc*/ 
	INNER JOIN TableDetailValues tdvExcessRule WITH (NOLOCK) on tdvExcessRule.TableRowID = tdvRL.TableRowID and tdvExcessRule.DetailFieldID = 177073
	LEFT JOIN dbo.TableDetailValues tdvPostcode WITH (NOLOCK) ON tdvRL.TableRowID = tdvPostcode.TableRowID AND tdvPostcode.DetailFieldID = 146212 /*Postcode*/ 
	LEFT JOIN dbo.TableDetailValues tdvBreed WITH (NOLOCK) ON tdvRL.TableRowID = tdvBreed.TableRowID AND tdvBreed.DetailFieldID = 146213 /*Breed*/
	LEFT JOIN dbo.TableDetailValues tdvPetAge WITH (NOLOCK) ON tdvPetAge.TableRowID = tdvRL.TableRowID AND tdvPetAge.DetailFieldID = 147931 /*Pet Age*/ 
	LEFT JOIN dbo.TableDetailValues tdvPetType WITH (NOLOCK) ON tdvPetType.TableRowID = tdvRL.TableRowID AND tdvPetType.DetailFieldID = 161534 /*Pet Type*/
	LEFT JOIN dbo.TableDetailValues tdvExcessType WITH (NOLOCK) on tdvExcessType.TableRowID = tdvrl.TableRowID and tdvExcessType.DetailFieldID = 146209 /*Excess Type*/ 
	LEFT JOIN dbo.TableDetailValues tdvExcessCeiling WITH (NOLOCK) on tdvExcessCeiling.TableRowID = tdvrl.TableRowID and tdvExcessCeiling.DetailFieldID = 162634 /*Excess ceiling*/ 
	LEFT JOIN dbo.TableDetailValues tdvOptions WITH (NOLOCK) ON tdvRL.TableRowID = tdvOptions.TableRowID AND tdvOptions.DetailFieldID = 170196 /*GPR 2020-07-24 reinstated Options in query*/
	LEFT JOIN dbo.TableDetailValues tdvPolAge WITH (NOLOCK) ON tdvRL.TableRowID = tdvPolAge.TableRowID AND tdvPolAge.DetailFieldID = 175862 /*Policy Age*/ 
	WHERE tdvRL.DetailFieldID = 144359 /*Policy Section*/
	AND tdvRL.MatterID = @CurrentPolicyMatterID

	DECLARE @Split TABLE
	(
		ResourceListID INT,
		Excess MONEY,
		ExcessPercentage MONEY,
		Postcode VARCHAR(2000),
		Breed VARCHAR(2000),
		PetAge INT,
		PetType INT,
		ExcessType INT,
		ExcessCeiling MONEY,
		PolicyAge INT,
		ExcessRule INT,
		Options VARCHAR(200)
	)

	INSERT @Split (ResourceListID, Excess, ExcessPercentage, Postcode, Breed, PetAge, PetType, ExcessType, ExcessCeiling, PolicyAge, ExcessRule, Options)
	SELECT ResourceListID, Excess, ExcessPercentage, Postcode, Breed, PetAge, PetType, ExcessType, ExcessCeiling, PolicyAge, ExcessRule, Options
	FROM @Raw
	WHERE Postcode IS NULL
	AND Breed IS NULL


	DECLARE @Count INT,
			@Index INT = 0,
			@ResourceListID INT,
			@Excess MONEY,
			@ExcessPercentage MONEY,
			@Postcode VARCHAR(2000),
			@Breed VARCHAR(2000),
			@PetAge INT,
			@PetType INT,
			@ExcessType INT,
			@ExcessCeiling MONEY,
			@PolicyAge INT,
			@ExcessRule INT
			
	SELECT @Count = COUNT(*) 
	FROM @Raw

	WHILE @Index < @Count
	BEGIN

		SELECT @Index = @Index + 1
		
		SELECT	@ResourceListID = ResourceListID, @Excess = Excess, @ExcessPercentage = ExcessPercentage, @Postcode = Postcode, @Breed = Breed, @PetAge = PetAge, 
				@PetType = PetType, @ExcessType = ExcessType, @ExcessCeiling = ExcessCeiling, @PolicyAge = PolicyAge, @ExcessRule = ExcessRule
		FROM @Raw
		WHERE ID = @Index
		
		IF @Postcode IS NOT NULL
		BEGIN
		
			INSERT @Split (ResourceListID, Excess, ExcessPercentage, Postcode, PetAge, PetType,ExcessType, ExcessCeiling, PolicyAge)
			SELECT @ResourceListID, @Excess, @ExcessPercentage, v.AnyValue, @PetAge, @PetType,@ExcessType, @ExcessCeiling, @PolicyAge
			FROM dbo.fnTableOfValuesFromCSV(@Postcode) v
			WHERE v.AnyValue <> '' --Because users sometimes end their CSV list with a comma *sigh*
		
		END
		
		IF @Breed IS NOT NULL
		BEGIN
		
			INSERT @Split (ResourceListID, Excess, ExcessPercentage, Breed, PetAge, PetType,ExcessType, ExcessCeiling, PolicyAge)
			SELECT @ResourceListID, @Excess, @ExcessPercentage, v.AnyValue, @PetAge, @PetType, @ExcessType, @ExcessCeiling, @PolicyAge
			FROM dbo.fnTableOfValuesFromCSV(@Breed) v
			WHERE v.AnyValue <> '' --Because users sometimes end their CSV list with a comma *sigh*
		
		END

	END
	
	DECLARE @DateToUse DATE,
			@AgeRelatedExcessID INT
			
	SELECT @AgeRelatedExcessID = ValueInt
	FROM dbo.MatterDetailValues
	WHERE MatterID = @CurrentPolicyMatterID
	AND DetailFieldID = 170205
	

	
	SELECT @DateToUse =	CASE @AgeRelatedExcessID
							WHEN 45241 THEN @DateOfLoss
							WHEN 45242 THEN @TreatmentDate
							WHEN 52783 THEN @PolicyAnniversary
							ELSE @DateOfLoss
						END
	
	;WITH Sections AS 
	(
		SELECT * 
		FROM dbo.fn_C00_1272_GetPolicySectionRelationships(@CurrentPolicyMatterID)
		UNION
		SELECT * 
		FROM dbo.fn_C00_1272_GetOptionalSectionRelationships(@CurrentPolicyMatterID)
		
	), InnerSql AS 
	(
		SELECT  s.Out_ResourceListID, s.ResourceListID, llSection.ItemValue AS Section, llSubSection.ItemValue AS SubSection, 
				split.Excess, split.ExcessPercentage, split.Postcode, split.Breed, split.PetAge,split.PetType, split.ExcessType ,split.ExcessCeiling, split.PolicyAge, split.ExcessRule, split.Options as Options
				--ROW_NUMBER() OVER(PARTITION BY s.ResourceListID ORDER BY split.Excess DESC, split.ExcessPercentage DESC) AS rn
				--ROW_NUMBER() OVER(PARTITION BY s.Out_ResourceListID ORDER BY split.breed DESC, split.Postcode DESC, split.Petage DESC, split.PetType DESC, split.Excess DESC, split.ExcessPercentage DESC) AS rn
				,ROW_NUMBER() OVER(PARTITION BY s.ResourceListID, split.PolicyAge ORDER BY CASE WHEN llSubSection.ItemValue = '-' THEN 1 ELSE 0 END, split.breed DESC, split.Postcode DESC, split.Petage DESC, split.PetType DESC, split.Excess DESC, split.ExcessPercentage DESC) AS rn
		FROM @Split split
		INNER JOIN Sections s ON s.Out_ResourceListID = split.ResourceListID
		INNER JOIN dbo.ResourceListDetailValues rdvSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189
		INNER JOIN dbo.LookupListItems llSection WITH (NOLOCK) ON rdvSection.ValueInt = llSection.LookupListItemID 
		INNER JOIN dbo.ResourceListDetailValues rdvSubSection WITH (NOLOCK) ON s.Out_ResourceListID = rdvSubSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190
		INNER JOIN dbo.LookupListItems llSubSection WITH (NOLOCK) ON rdvSubSection.ValueInt = llSubSection.LookupListItemID 
		WHERE (split.Postcode IS NULL OR split.Postcode = '' OR @CustomerPostCode = split.Postcode)
		AND (split.Breed IS NULL OR split.Breed = '' OR @AnimalBreed LIKE split.Breed)
		AND (DATEADD(YEAR, split.PetAge,@PetDOB) <= @DateToUse or split.PetAge = 0)
		AND (split.PolicyAge IS NULL OR split.PolicyAge BETWEEN @PolicyAgeStart AND @PolicyAgeEnd)
		AND (split.PetType = 0 OR split.PetType = @AnimalType OR @AnimalType IS NULL)
	)

	
	-- Return the highest excess
	-- Waiving voluntary excess for Minimum Contribution rows 
	INSERT @ExcessData
	SELECT	Out_ResourceListID, ResourceListID, Section, SubSection
	--,CASE WHEN ExcessPercentage > 0 THEN Excess ELSE Excess + @VoluntaryExcess END AS Excess 
	,Excess
	,ExcessPercentage, Postcode, Breed
	,PetAge
	,PetType
	,ExcessType
	,ExcessCeiling
	,CASE WHEN ExcessPercentage > 0 THEN 0 ELSE @VoluntaryExcess END AS VoluntaryExcess
	,PolicyAge
	,ExcessRule
	,Options
	FROM InnerSql
	WHERE rn = 1 /*GPR 2021-01-27 reintroduced for FURKIN-142*/
	
	RETURN

END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPolicyExcessWithData] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1272_Policy_GetPolicyExcessWithData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPolicyExcessWithData] TO [sp_executeall]
GO
