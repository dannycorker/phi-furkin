SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-06-17
-- Description:	Get Balance based on table, fallback to matter field
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_DM_GetCurrentBalance]
(
@MatterID INT,
@OriginalField INT,
@BalanceTable INT,
@BalanceFieldID INT
)
RETURNS numeric(18,2)
AS
BEGIN

	DECLARE @LastBalanceAmount numeric(18,2)

	SELECT TOP (1) @LastBalanceAmount = tdv.ValueMoney
	FROM TableRows tr WITH (NOLOCK)
	INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = @BalanceFieldID
	WHERE tr.DetailFieldID = @BalanceTable
	AND tr.MatterID = @MatterID
	ORDER BY tr.TableRowID DESC

	IF @LastBalanceAmount IS NULL
	BEGIN

		SELECT @LastBalanceAmount = mdv.ValueMoney
		FROM MatterDetailValues mdv WITH (NOLOCK) 
		WHERE mdv.MatterID = @MatterID
		AND mdv.DetailFieldID = @OriginalField	

	END

	Return ISNULL(@LastBalanceAmount, 0.00)

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_DM_GetCurrentBalance] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_DM_GetCurrentBalance] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_DM_GetCurrentBalance] TO [sp_executeall]
GO
