SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-07-21
-- Description:	Merges in shared lead types
-- =============================================
CREATE FUNCTION [dbo].[fnLeadStatusShared] 
(	
	@ClientID INT
)
RETURNS TABLE 
AS
RETURN 
(	
	SELECT [StatusID]
      ,CASE WHEN s.LeadTypeShareID IS NULL THEN ls.ClientID ELSE s.ClientID END AS ClientID
      ,[StatusName]
      ,[StatusDescription]
      ,[SourceID]
      ,CASE WHEN s.LeadTypeShareID IS NULL THEN ls.LeadTypeID ELSE s.SharedTo END AS LeadTypeID
      ,CASE WHEN s.LeadTypeShareID IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS IsShared
	FROM dbo.LeadStatus ls WITH (NOLOCK)
	LEFT JOIN dbo.LeadTypeShare s WITH (NOLOCK) ON ls.LeadTypeID = s.SharedFrom AND s.ClientID = @ClientID
)


GO
GRANT VIEW DEFINITION ON  [dbo].[fnLeadStatusShared] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnLeadStatusShared] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnLeadStatusShared] TO [sp_executeall]
GO
