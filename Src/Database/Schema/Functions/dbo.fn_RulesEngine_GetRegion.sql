SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-02-19
-- Description:	Returns the Region by CountryID and Postcode
-- 2020-02-23 CPS for JIRA AAG-186 | Don't try and calculate from a numeric postcode if you don't have one
-- =============================================
CREATE FUNCTION [dbo].[fn_RulesEngine_GetRegion]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS VARCHAR(5)
AS
BEGIN

	DECLARE @Postcode VARCHAR(20),
			@ClientID INT,
			@CountryID INT,
			@Region VARCHAR(5),
			@PostcodeInt INT

	/*Get ClientID and CountryID*/
	--SELECT @ClientID = ClientID FROM Customers WITH (NOLOCK) WHERE CustomerID = @CustomerID
	SELECT @ClientID = [dbo].[fnGetPrimaryClientID]() /*GPR 2020-05-27*/
	SELECT @CountryID = CountryID FROM Clients WITH (NOLOCK) WHERE ClientID = @ClientID
	
	SELECT @Postcode=AnyValue2 FROM @Overrides WHERE AnyID = 2 AND AnyValue1 = 'PostCode'

	SELECT @PostcodeInt = CONVERT(int, @Postcode)
	WHERE dbo.fnIsInt(@Postcode) = 1 -- CPS 2020-02-23 for JIRA AAG-186 | don't hit this section if the postcode isn't an integer

	/*GPR 2020-02-24*/
	IF dbo.fnIsInt(@Postcode) = 1
	BEGIN
	SELECT @Region =	rbp.Region
						FROM RegionByPostcode rbp WITH (NOLOCK)
						WHERE rbp.CountryID = @CountryID
						AND rbp.PostcodeInt = @PostcodeInt
	END
	
	RETURN ISNULL(@Region,'UNK') /*GPR 2020-02-24 - Default as testing with CW9 8UB until address lookup is altered*/
 
END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_GetRegion] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_RulesEngine_GetRegion] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_GetRegion] TO [sp_executeall]
GO
