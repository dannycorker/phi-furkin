SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2007-09-25
-- Description:	Only returns Event Types this user is allowed to see.
--              See detailed comments about half way down the script.
-- SB  2014-07-10 Updated to use view which includes shared event types
-- SLACKY-PERF-20190621- added (nolock) where missing
-- =============================================
CREATE FUNCTION [dbo].[fnEventTypeSecure] 
(	
	@UserID int, 
	@LeadTypeID int = null
)
RETURNS 
@Data TABLE 
(
	ObjectID INT,
	RightID INT
)
AS
BEGIN

	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.ClientPersonnel WITH (NOLOCK) 
	WHERE ClientPersonnelID = @UserID

	;with UserGroup as
	(
		-- SLACKY-PERF-20190621- added (nolock) where missing
		select ClientPersonnelAdminGroupID as 'GroupID', ClientID
		from ClientPersonnel (nolock)
		where ClientPersonnelID = @UserID
	),
	UserRights as
	(
		-- SLACKY-PERF-20190621- added (nolock) where missing
		select objectid, rightid
		from grouprightsdynamic (nolock)
		where clientpersonneladmingroupid = (select GroupID from UserGroup)
		and grouprightsdynamic.FunctionTypeID = 13
		and (grouprightsdynamic.LeadTypeID = @LeadTypeID OR @LeadTypeID is null)
		and grouprightsdynamic.rightid > 0
		and not exists (select * from userrightsdynamic where userrightsdynamic.functiontypeid = grouprightsdynamic.functiontypeid and isnull(userrightsdynamic.leadtypeid,-1) = isnull(grouprightsdynamic.leadtypeid, -1) and userrightsdynamic.objectid = grouprightsdynamic.objectid and userrightsdynamic.clientpersonnelid = @UserID)
		union
		select objectid, rightid
		from userrightsdynamic (nolock)
		where clientpersonnelid = @UserID
		and userrightsdynamic.FunctionTypeID = 13
		and (userrightsdynamic.LeadTypeID = @LeadTypeID OR @LeadTypeID is null)
		and userrightsdynamic.rightid > 0
	),
	UserLeadTypes as
	(
		-- SLACKY-PERF-20190621- added (nolock) where missing
		SELECT gfc.ModuleID, gfc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, gfc.HasDescendants, gfc.RightID, gfc.LeadTypeID 
		FROM GroupFunctionControl gfc (nolock) INNER JOIN FunctionType ft (nolock) ON ft.FunctionTypeID = gfc.FunctionTypeID and ft.ModuleID = gfc.ModuleID 
		WHERE gfc.ClientPersonnelAdminGroupID = (select GroupID from UserGroup)
		and gfc.FunctionTypeID = 23
		and (gfc.LeadTypeID = @LeadTypeID OR @LeadTypeID is null)
		AND NOT EXISTS (SELECT * FROM UserFunctionControl ufc1 (nolock) WHERE ufc1.functiontypeid = gfc.functiontypeid and ufc1.ClientPersonnelID = @UserID)
		UNION
		SELECT ufc.ModuleID, ufc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, ufc.HasDescendants, ufc.RightID, ufc.LeadTypeID 
		FROM UserFunctionControl ufc (nolock) INNER JOIN FunctionType ft (nolock) ON ft.FunctionTypeID = ufc.FunctionTypeID and ft.ModuleID = ufc.ModuleID 
		WHERE ufc.ClientPersonnelID = @UserID
		and ufc.FunctionTypeID = 23
		and (ufc.LeadTypeID = @LeadTypeID OR @LeadTypeID is null)
	),
	UserFunctions as 
	(
		-- SLACKY-PERF-20190621- added (nolock) where missing
		SELECT gfc.ModuleID, gfc.LeadTypeID, gfc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, gfc.HasDescendants, gfc.RightID 
		FROM GroupFunctionControl gfc (nolock) INNER JOIN FunctionType ft (nolock) ON ft.FunctionTypeID = gfc.FunctionTypeID and ft.ModuleID = gfc.ModuleID 
		WHERE gfc.ClientPersonnelAdminGroupID = (select GroupID from UserGroup)
		and gfc.FunctionTypeID = 7
		AND NOT EXISTS (SELECT * FROM UserFunctionControl ufc1 WHERE ufc1.functiontypeid = gfc.functiontypeid and ufc1.ClientPersonnelID = @UserID and ((@LeadTypeID is null OR gfc.LeadTypeID = ufc1.LeadTypeID) OR (gfc.FunctionTypeID IN (7, 8, 11))))
		UNION
		SELECT ufc.ModuleID, ufc.LeadTypeID, ufc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, ufc.HasDescendants, ufc.RightID 
		FROM UserFunctionControl ufc (nolock) INNER JOIN FunctionType ft (nolock) ON ft.FunctionTypeID = ufc.FunctionTypeID and ft.ModuleID = ufc.ModuleID 
		WHERE ufc.ClientPersonnelID = @UserID
		and ufc.FunctionTypeID = 7
	),
	UserFunctions13 as 
	(
		-- SLACKY-PERF-20190621- added (nolock) where missing
		SELECT gfc.ModuleID, gfc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, gfc.HasDescendants, gfc.RightID 
		FROM GroupFunctionControl gfc (nolock) INNER JOIN FunctionType ft (nolock) ON ft.FunctionTypeID = gfc.FunctionTypeID and ft.ModuleID = gfc.ModuleID 
		WHERE gfc.ClientPersonnelAdminGroupID = (select GroupID from UserGroup)
		and gfc.FunctionTypeID = 13
		and (gfc.LeadTypeID = @LeadTypeID OR @LeadTypeID is null)
		AND NOT EXISTS (SELECT * FROM UserFunctionControl ufc1 WHERE ufc1.functiontypeid = gfc.functiontypeid and ufc1.LeadTypeID = @LeadTypeID and ufc1.ClientPersonnelID = @UserID)
		UNION
		SELECT ufc.ModuleID, ufc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, ufc.HasDescendants, ufc.RightID 
		FROM UserFunctionControl ufc (nolock) INNER JOIN FunctionType ft (nolock) ON ft.FunctionTypeID = ufc.FunctionTypeID and ft.ModuleID = ufc.ModuleID 
		WHERE ufc.ClientPersonnelID = @UserID
		and ufc.FunctionTypeID = 13
		and (ufc.LeadTypeID = @LeadTypeID OR @LeadTypeID is null)
	),
	UserLeadManagerModule as 
	(
		-- SLACKY-PERF-20190621- added (nolock) where missing
		SELECT gfc.ModuleID, gfc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, gfc.HasDescendants, gfc.RightID 
		FROM GroupFunctionControl gfc (nolock) INNER JOIN FunctionType ft (nolock) ON ft.FunctionTypeID = gfc.FunctionTypeID and ft.ModuleID = gfc.ModuleID 
		WHERE gfc.ClientPersonnelAdminGroupID = (select GroupID from UserGroup)
		and gfc.FunctionTypeID = 1
		AND NOT EXISTS (SELECT * FROM UserFunctionControl ufc1 (nolock) WHERE ufc1.functiontypeid = gfc.functiontypeid and ufc1.ClientPersonnelID = @UserID)
		UNION
		SELECT ufc.ModuleID, ufc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, ufc.HasDescendants, ufc.RightID 
		FROM UserFunctionControl ufc (nolock) INNER JOIN FunctionType ft (nolock) ON ft.FunctionTypeID = ufc.FunctionTypeID and ft.ModuleID = ufc.ModuleID 
		WHERE ufc.ClientPersonnelID = @UserID
		and ufc.FunctionTypeID = 1
	)
	/*
		A user has one of these things: 
		(1) a full set of individual records specifying which events they can see 
			and which they cannot see, by Lead Type ("UserRights" above)
		or
		(2) an overall record specifying that they can or cannot see this Lead Type ("UserLeadTypes" above)
		or
		(3) an overall record specifying that they can or cannot see all events ("UserFunctions" above)
		or
		(4) individual records specifying what they can do with each Event Type ("UserFunctions13" above)
		or
		(5) an overall Lead Manager module control record
	*/
	
	INSERT @Data(ObjectID, RightID)
	select UserRights.objectid, UserRights.RightID
	from UserRights
	where not exists (select * from UserLeadTypes where RightID = 0 and HasDescendants = 0)

	union

	select EventTypeID, UserLeadTypes.RightID
	from fnEventTypeShared(@ClientID) et INNER JOIN UserGroup ON et.ClientID = UserGroup.ClientID  
	inner JOIN UserLeadTypes on et.LeadTypeID = UserLeadTypes.LeadTypeID
	where UserLeadTypes.RightID > 0
	and UserLeadTypes.HasDescendants = 0

	union

	select EventTypeID, UserFunctions.RightID 
	from fnEventTypeShared(@ClientID) et INNER JOIN UserGroup ON et.ClientID = UserGroup.ClientID   
	CROSS JOIN UserFunctions
	where (et.LeadTypeID = @LeadTypeID OR @LeadTypeID is null)
	and not exists (select * from UserLeadTypes where RightID = 0)
	and UserFunctions.RightID > 0
	and UserFunctions.HasDescendants = 0

	union

	select EventTypeID, UserFunctions13.RightID 
	from fnEventTypeShared(@ClientID) et INNER JOIN UserGroup ON et.ClientID = UserGroup.ClientID   
	CROSS JOIN UserFunctions13
	where (et.LeadTypeID = @LeadTypeID OR @LeadTypeID is null)
	and not exists (select * from UserLeadTypes where RightID = 0)
	and UserFunctions13.RightID > 0
	and UserFunctions13.HasDescendants = 0

	union
		
	select EventTypeID, UserLeadManagerModule.RightID 
	from fnEventTypeShared(@ClientID) et INNER JOIN UserGroup ON et.ClientID = UserGroup.ClientID   
	CROSS JOIN UserLeadManagerModule
	where (LeadTypeID = @LeadTypeID OR @LeadTypeID is null)
	and not exists (select * from UserFunctions where RightID = 0 and HasDescendants = 0)
	and not exists (select * from UserFunctions13 where RightID = 0 and HasDescendants = 0)
	and not exists (select * from UserLeadTypes where RightID = 0 and HasDescendants = 0)
	and UserLeadManagerModule.RightID > 0
	and UserLeadManagerModule.HasDescendants = 0


	RETURN
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnEventTypeSecure] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnEventTypeSecure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnEventTypeSecure] TO [sp_executeall]
GO
