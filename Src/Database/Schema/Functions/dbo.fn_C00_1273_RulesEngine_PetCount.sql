SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-11-27
-- Description:	Count the number of live pet policies for this customer
-- 2018-12-20	GPR added join to CurrentPolicy to exclude C600 Buddies OMF Policies from Count.
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_RulesEngine_PetCount]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS INT
AS
BEGIN

	DECLARE @PetCount	INT = 1
	
	SELECT @PetCount = COUNT(*)
	FROM Lead l WITH (NOLOCK) 
	INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.LeadID = l.LeadID AND mdv.DetailFieldID = 170038 /*Policy Status*/
	INNER JOIN MatterDetailValues product WITH ( NOLOCK ) ON product.LeadID = l.LeadID AND product.DetailFieldID = 170034 /*Current Policy*/
	WHERE l.CustomerID = @CustomerID
	AND l.LeadTypeID = 1492 /*Policy Admin*/
	AND mdv.ValueInt IN ( 43002 /*Live*/, 74573 /*Underwriting*/ )
	AND product.ValueInt <> 151577 /*Buddies One Month Free*/
		
	RETURN	@PetCount
 
END
 





GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_PetCount] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_RulesEngine_PetCount] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_PetCount] TO [sp_executeall]
GO
