SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry / Louis Bromilow
-- Create date: 2013-11-22 / 2015-01-25
-- Description:	For ISI Clients, calculate live values for repayment breakdown display table
--				Third party mapped, using field group 26
--				To be used in TableRows__GetBy.... procs
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerID] 
(	
	@CustomerID INT
)
RETURNS
@TableOut TABLE 
(
	TableRowID INT,
	Creditor VARCHAR(2000),
	[Balance Outstanding] DECIMAL(18,2),
	Percentage DECIMAL(18,2),
	[Year 1] DECIMAL(18,2),
	[Year 2] DECIMAL(18,2),
	[Year 3] DECIMAL(18,2),
	[Year 4] DECIMAL(18,2),
	[Year 5] DECIMAL(18,2),
	[Year 6] DECIMAL(18,2),
	[Year 7] DECIMAL(18,2),
	Totals	DECIMAL(18,2),
	[Return for creditors] VARCHAR(2000),
	DenyEdit VARCHAR(5),
	DenyDelete VARCHAR(5)
)
AS
BEGIN
	
	DECLARE	@ClientID						INT,
			@UnsecuredLenderNameFieldID		INT,
			@PercentageOfDebtFieldID		INT,
			@AmountOwedFieldID				INT,
			@Year1FieldID					INT,
			@Year2FieldID					INT,
			@Year3FieldID					INT,
			@Year4FieldID					INT,
			@Year5FieldID					INT,
			@Year6FieldID					INT,
			@Year7FieldID					INT,
			@UnsecuredCreditorTableFieldID	INT,
			@Term							INT = 6,
			@Pref							INT,
			@YearSum						INT,
			@TermMonthsValue				INT

		SELECT @ClientID = cu.ClientID
		FROM Customers cu WITH (NOLOCK) 
		WHERE cu.CustomerID = @CustomerID

		DECLARE @Total NUMERIC(18,2),
				@PrefTotal NUMERIC(18,2),
				@Years INT,
				@ValueNumeric Numeric(18,2),
				@PrefValueNumeric Numeric(18,2)
				
		DECLARE @AddedRows TABLE (FromTableRowID INT, ToTableRowID INT)
		
	DECLARE @Creditors TABLE (TableRowID INT, Name VARCHAR(2000), Amount Numeric(18,2), PreferentialValue Numeric(18,2), Percentage numeric(18,2), Year1 Numeric(18,2), Year2 Numeric(18,2), Year3 Numeric(18,2), Year4 Numeric(18,2), Year5 Numeric(18,2), Year6 Numeric(18,2), Year7 Numeric(18,2), PrefLeft Numeric(18,2), OverrideValue NUMERIC(18,2))

	/*Clients outside of this range use the old system of Payment breakdown..*/
	IF (@ClientID IN (267,311) OR @ClientID > 311)
	BEGIN
	
		/*
			[14:34:12] Stephen Ainsworth: 1. Get unsecured creditors where they are not excluded
			[14:34:37] Stephen Ainsworth: 2. Look at drop down list and determine whether its for 6 or 7 years
			[14:34:50] Stephen Ainsworth: 3. Insert creditors
			[14:35:10] Stephen Ainsworth: 4. Get debt level of added creditors
			[14:35:18] Stephen Ainsworth: 5. calculate percentage of debt level for each
			[14:35:32] Stephen Ainsworth: 6. use equations above table to find value for each year
			[14:35:53] Stephen Ainsworth: 7. calculate the amount to each creditor against the total amount for the year against % of debt
			[14:36:03] Stephen Ainsworth: 8. add amount into each creditor for each respective year
			[14:36:09] Stephen Ainsworth: 9. total each row at the end
			[14:36:26] Stephen Ainsworth: 10. work out the return to each creditor (debt level less amount paid as a % return)
		*/
		
	INSERT INTO @Creditors (TableRowID, Name, Amount, PreferentialValue, OverrideValue)
	SELECT DISTINCT
			tr.TableRowID, 
		--tdv_secured.DetailValue as [Secured], 
		ISNULL(li.ItemValue,tdv_org.DetailValue) as [Name], 
		tdv_amt.ValueMoney as [Amount],
		0.00  [PreferentialValue],
		tdv_ovr.ValueMoney [OverrideValue]
	FROM ThirdPartyFieldMapping tpm_f WITH (NOLOCK)
	INNER JOIN TableRows tr WITH (NOLOCK) ON tr.DetailFieldID = tpm_f.DetailFieldID
	CROSS APPLY ThirdPartyFieldMapping tpm_sec WITH (NOLOCK) 
	INNER JOIN TableDetailValues tdv_secured WITH (NOLOCK) ON tdv_secured.TableRowID = tr.TableRowID and tdv_secured.DetailFieldID = tpm_sec.DetailFieldID AND tdv_secured.ValueInt = 5145
	CROSS APPLY ThirdPartyFieldMapping tpm_org WITH (NOLOCK) 
	INNER JOIN TableDetailValues tdv_org WITH (NOLOCK) ON tdv_org.TableRowID = tr.TableRowID and tdv_org.DetailFieldID = tpm_org.DetailFieldID
	CROSS APPLY ThirdPartyFieldMapping tpm_amt WITH (NOLOCK) 
	INNER JOIN TableDetailValues tdv_amt WITH (NOLOCK) ON tdv_amt.TableRowID = tr.TableRowID and tdv_amt.DetailFieldID = tpm_amt.DetailFieldID
	CROSS APPLY ThirdPartyFieldMapping tpm_exclude WITH (NOLOCK) 
	INNER JOIN TableDetailValues tdv_exclude WITH (NOLOCK) ON tdv_exclude.TableRowID = tr.TableRowID and tdv_exclude.DetailFieldID = tpm_exclude.DetailFieldID
	LEFT JOIN LookupListItems li WITH ( NOLOCK ) on li.ClientID = @ClientID AND li.LookupListItemID = tdv_org.ValueInt
	LEFT JOIN TableDetailValues tdv_ovr WITH ( NOLOCK ) on tdv_ovr.TableRowID = tr.TableRowID AND tdv_ovr.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 26, 1431) /*Enter Override Amount*/ AND tdv_ovr.ValueMoney > 0.00
	WHERE tpm_f.ThirdPartyFieldID = 848
	AND tpm_org.ThirdPartyFieldID = 850
	AND tpm_sec.ThirdPartyFieldID = 849 
	AND tpm_amt.ThirdPartyFieldID = 851
	AND tpm_exclude.ThirdPartyFieldID = 855
	AND tpm_f.ClientID = @ClientID
	AND tpm_org.ClientID = @ClientID
	AND tpm_sec.ClientID = @ClientID
	AND tpm_amt.ClientID = @ClientID
	AND tpm_exclude.ClientID = @ClientID
	AND ( tdv_exclude.ValueInt = 0 or tdv_exclude.ValueInt is null )
	AND tr.CustomerID = @CustomerID 

	UPDATE cr 
	SET PreferentialValue = cr.Amount
	FROM @Creditors cr 
	INNER JOIN TableRows tr WITH (NOLOCK) on tr.TableRowID = cr.TableRowID
	INNER JOIN ThirdPartyFieldMapping fm on fm.ClientID = @ClientID AND fm.ThirdPartyFieldID = 1343 /*Creditor Preferrential Debt Value*/
	INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	WHERE (tdv.DetailValue NOT IN ('false','','0.00'))

	--FROM @Creditors cr 
	--INNER JOIN TableRows tr WITH (NOLOCK) on tr.TableRowID = cr.TableRowID
	--INNER JOIN ThirdPartyFieldMapping fm on fm.ClientID = @ClientID AND fm.ThirdPartyFieldID = 1343 /*Creditor Preferrential Debt Value*/
	--INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID

	INSERT @Creditors (TableRowID, Name, Amount, PreferentialValue)
	SELECT c.TableRowID, c.Name, 0.00, c.PreferentialValue
	FROM @Creditors c 
	WHERE c.PreferentialValue > 0.00

	/*Zero the preferential amount.  Reduce the actual*/
	UPDATE c 
	SET PreferentialValue = 0.00, Amount = Amount - PreferentialValue
	FROM @Creditors c 
	WHERE c.PreferentialValue > 0.00
	AND c.Amount > 0.00

	SELECT @Total = SUM(c.Amount), @PrefTotal = SUM(PreferentialValue)
	FROM @Creditors c 
	WHERE c.OverrideValue is NULL

	UPDATE c
	SET Percentage = CASE 
						WHEN c.PreferentialValue > 0.00 THEN CONVERT(Numeric(18,2),((PreferentialValue/(@PrefTotal))*100))
						ELSE CONVERT(Numeric(18,2),((Amount/@Total)*100))
					 END
	FROM @Creditors c
	WHERE @Total <> 0

	SELECT @Total = SUM(c.Amount), @PrefTotal = SUM(PreferentialValue)
	FROM @Creditors c
	WHERE c.OverrideValue is NULL

	SELECT @Years = luli.ValueInt
	FROM CustomerDetailValues cdv WITH (NOLOCK)
	INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = cdv.ValueInt
	INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 733 AND tpm.ThirdPartyFieldGroupID = 62
	WHERE cdv.CustomerID = @CustomerID

	SELECT @Years = ISNULL(@Years,6)

	/*Special conditions if we are on a 7-year plan*/
	SELECT @TermMonthsValue = ISNULL(li.ValueInt,0) 
	FROM CustomerDetailValues cdv WITH (NOLOCK)
	INNER JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = cdv.ValueInt
	INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 4139 AND tpm.ThirdPartyFieldGroupID = 61
	WHERE cdv.CustomerID = @CustomerID 

	IF @TermMonthsValue > 0
	BEGIN
	
		SELECT @Years = @Years + 1
	
	END

	--IF EXISTS	( 
	--			SELECT * 
	--			FROM Lead l WITH ( NOLOCK ) 
	--			INNER JOIN LeadEvent le WITH (NOLOCK) on le.EventDeleted = 0 and le.LeadID = l.LeadID
	--			INNER JOIN ThirdPartySystemEvent se WITH ( NOLOCK ) on se.EventTypeID = le.EventTypeID
	--			WHERE l.CustomerID = @CustomerID
	--			AND se.Name = 'Fast Track'
	--			)
	--BEGIN
	--	SELECT @Years = - 1 /*Accelerated Cases*/
	--END

	IF @Years > 0
	BEGIN

		/*Work out the amount per year to allocate to preferential debts*/
		SELECT @PrefValueNumeric = SUM(c.PreferentialValue) / @Years 
		FROM @Creditors c 
		WHERE c.PreferentialValue > 0.00
		AND @Years <> 0

		SELECT @PrefValueNumeric = ISNULL(@PrefValueNumeric,0.00) /*Added by CS 2014-11-03*/

		/*Year 1*/
		SELECT @ValueNumeric = cdv.ValueMoney-@PrefValueNumeric
		FROM CustomerDetailValues cdv WITH (NOLOCK)
		INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 657 AND tpm.ThirdPartyFieldGroupID = 26
		WHERE cdv.CustomerID = @CustomerID

		UPDATE c
		SET Year1 = CASE WHEN c.PreferentialValue > 0.00 THEN CAST( c.PreferentialValue / @Years as DECIMAL(18,2) ) ELSE @ValueNumeric * (Percentage/100) END 
		FROM @Creditors c 
		WHERE @Years <> 0

		SELECT @YearSum = (SELECT SUM(Year1) FROM @Creditors c WHERE @Years <> 0)
		SELECT @Pref = (SELECT SUM(ISNULL(PrefLeft,PreferentialValue)) FROM @Creditors c WHERE @Years <> 0)

		IF 0 < @Pref
		BEGIN 

			UPDATE c 
			SET Year1 = (@YearSum* (c.Percentage/100)), PrefLeft = ISNULL(PrefLeft,PreferentialValue) - (@YearSum* (c.Percentage/100))
			FROM @Creditors c 
			WHERE ISNULL(PrefLeft,PreferentialValue) > 0.00

			UPDATE c 
			SET Year1 = 0
			FROM @Creditors c 
			WHERE (PreferentialValue is NULL or PreferentialValue = 0.00)
			
			SELECT @YearSum = '', @Pref = ''
			
			IF EXISTS (SELECT * FROM @Creditors c WHERE ISNULL(PrefLeft,PreferentialValue) < 0) 
			BEGIN
			
				SELECT @Pref = (SELECT SUM(ISNULL(PrefLeft,PreferentialValue)) FROM @Creditors c WHERE @Years <> 0)

				UPDATE c 
				SET Year1 = (ABS(@Pref) * (Percentage/100))
				FROM @Creditors c 
				WHERE PrefLeft is NULL
				AND @Years <> 0

				UPDATE c 
				SET PrefLeft = 0, Year1 = Year1 + PrefLeft
				FROM @Creditors c 
				WHERE PrefLeft < 0

			END

		END

		/*Year 2*/

		IF 0 = (SELECT SUM(c.PrefLeft) FROM @Creditors c WHERE c.PreferentialValue > 0.00 AND @Years <> 0)
		BEGIN

			SELECT @PrefValueNumeric = 0

		END

		SELECT @ValueNumeric = cdv.ValueMoney-@PrefValueNumeric
		From CustomerDetailValues cdv WITH (NOLOCK)
		INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 658 AND tpm.ThirdPartyFieldGroupID = 26
		WHERE cdv.CustomerID = @CustomerID

		UPDATE c
		SET Year2 = CASE WHEN c.PreferentialValue > 0.00 AND PrefLeft > 0 THEN CAST( c.PreferentialValue / @Years as DECIMAL(18,2) ) WHEN PrefLeft = 0.00 THEN 0.00 ELSE @ValueNumeric * (Percentage/100) END 
		FROM @Creditors c 
		WHERE @Years <> 0

		SELECT @YearSum = (SELECT SUM(Year2) FROM @Creditors c WHERE @Years <> 0)
		SELECT @Pref = (SELECT SUM(ISNULL(PrefLeft,PreferentialValue)) FROM @Creditors c WHERE @Years <> 0)

		IF 0 < @Pref
		BEGIN 

			UPDATE c 
			SET Year2 = (@YearSum* (c.Percentage/100)), PrefLeft = ISNULL(PrefLeft,PreferentialValue) - (@YearSum* (c.Percentage/100))
			FROM @Creditors c 
			WHERE ISNULL(PrefLeft,PreferentialValue) > 0.00

			UPDATE c 
			SET Year2 = 0
			FROM @Creditors c 
			WHERE (PreferentialValue is NULL or PreferentialValue = 0.00)
			
			SELECT @YearSum = '', @Pref = ''
			
			IF EXISTS (SELECT * FROM @Creditors c WHERE ISNULL(PrefLeft,PreferentialValue) < 0) 
			BEGIN
			
				SELECT @Pref = (SELECT SUM(ISNULL(PrefLeft,PreferentialValue)) FROM @Creditors c WHERE @Years <> 0)

				UPDATE c 
				SET Year2 = (ABS(@Pref) * (Percentage/100))
				FROM @Creditors c 
				WHERE PrefLeft is NULL
				AND @Years <> 0

				UPDATE c 
				SET PrefLeft = 0, Year2 = Year2 + PrefLeft
				FROM @Creditors c 
				WHERE PrefLeft < 0

			END

		END

		/*Year 3*/

		IF 0 = (SELECT SUM(c.PrefLeft) FROM @Creditors c WHERE c.PreferentialValue > 0.00 AND @Years <> 0)
		BEGIN

			SELECT @PrefValueNumeric = 0

		END

		SELECT @ValueNumeric = cdv.ValueMoney-@PrefValueNumeric
		From CustomerDetailValues cdv WITH (NOLOCK)
		INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 659 AND tpm.ThirdPartyFieldGroupID = 26
		WHERE cdv.CustomerID = @CustomerID

		UPDATE c
		SET Year3 = CASE WHEN c.PreferentialValue > 0.00 AND PrefLeft > 0 THEN CAST( c.PreferentialValue / @Years as DECIMAL(18,2) ) WHEN PrefLeft = 0.00 THEN 0.00 ELSE @ValueNumeric * (Percentage/100) END 
		FROM @Creditors c
		WHERE @Years <> 0

		SELECT @YearSum = (SELECT SUM(Year3) FROM @Creditors c WHERE @Years <> 0)
		SELECT @Pref = (SELECT SUM(ISNULL(PrefLeft,PreferentialValue)) FROM @Creditors c WHERE @Years <> 0)

		IF 0 < @Pref
		BEGIN 

			UPDATE c 
			SET Year3 = (@YearSum* (c.Percentage/100)), PrefLeft = ISNULL(PrefLeft,PreferentialValue) - (@YearSum* (c.Percentage/100))
			FROM @Creditors c 
			WHERE ISNULL(PrefLeft,PreferentialValue) > 0.00

			UPDATE c 
			SET Year3 = 0
			FROM @Creditors c 
			WHERE (PreferentialValue is NULL or PreferentialValue = 0.00)
				
			SELECT @YearSum = '', @Pref = ''
			
			IF EXISTS (SELECT * FROM @Creditors c WHERE ISNULL(PrefLeft,PreferentialValue) < 0) 
			BEGIN
			
				SELECT @Pref = (SELECT SUM(ISNULL(PrefLeft,PreferentialValue)) FROM @Creditors c WHERE @Years <> 0)

				UPDATE c 
				SET Year3 = (ABS(@Pref) * (Percentage/100))
				FROM @Creditors c 
				WHERE PrefLeft is NULL
				AND @Years <> 0

				UPDATE c 
				SET PrefLeft = 0, Year3 = Year3 + PrefLeft
				FROM @Creditors c 
				WHERE PrefLeft < 0

			END

		END

		/*Year 4*/

		IF 0 = (SELECT SUM(c.PrefLeft) FROM @Creditors c WHERE c.PreferentialValue > 0.00 AND @Years <> 0)
		BEGIN

			SELECT @PrefValueNumeric = 0

		END

		SELECT @ValueNumeric = cdv.ValueMoney-@PrefValueNumeric
		From CustomerDetailValues cdv WITH (NOLOCK)
		INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 660 AND tpm.ThirdPartyFieldGroupID = 26
		WHERE cdv.CustomerID = @CustomerID

		UPDATE c
		SET Year4 = CASE WHEN c.PreferentialValue > 0.00 AND PrefLeft > 0 THEN CAST( c.PreferentialValue / @Years as DECIMAL(18,2) ) WHEN PrefLeft = 0.00 THEN 0.00 ELSE @ValueNumeric * (Percentage/100) END 
		FROM @Creditors c
		WHERE @Years <> 0

		SELECT @YearSum = (SELECT SUM(Year4) FROM @Creditors c WHERE @Years <> 0)
		SELECT @Pref = (SELECT SUM(ISNULL(PrefLeft,PreferentialValue)) FROM @Creditors c WHERE @Years <> 0)

		IF 0 < @Pref
		BEGIN 

			UPDATE c 
			SET Year4 = (@YearSum* (c.Percentage/100)), PrefLeft = ISNULL(PrefLeft,PreferentialValue) - (@YearSum* (c.Percentage/100))
			FROM @Creditors c 
			WHERE ISNULL(PrefLeft,PreferentialValue) > 0.00

			UPDATE c 
			SET Year4 = 0
			FROM @Creditors c 
			WHERE (PreferentialValue is NULL or PreferentialValue = 0.00)
				
			SELECT @YearSum = '', @Pref = ''
			
			IF EXISTS (SELECT * FROM @Creditors c WHERE ISNULL(PrefLeft,PreferentialValue) < 0) 
			BEGIN
			
				SELECT @Pref = (SELECT SUM(ISNULL(PrefLeft,PreferentialValue)) FROM @Creditors c WHERE @Years <> 0)

				UPDATE c 
				SET Year4 = (ABS(@Pref) * (Percentage/100))
				FROM @Creditors c 
				WHERE PrefLeft is NULL
				AND @Years <> 0

				UPDATE c 
				SET PrefLeft = 0, Year4 = Year4 + PrefLeft
				FROM @Creditors c 
				WHERE PrefLeft < 0

			END
			
		END

		/*Year 5*/

		IF 0 = (SELECT SUM(c.PrefLeft) FROM @Creditors c WHERE c.PreferentialValue > 0.00 AND @Years <> 0)
		BEGIN

			SELECT @PrefValueNumeric = 0

		END

		SELECT @ValueNumeric = cdv.ValueMoney-@PrefValueNumeric
		From CustomerDetailValues cdv WITH (NOLOCK)
		INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 661 AND tpm.ThirdPartyFieldGroupID = 26
		WHERE cdv.CustomerID = @CustomerID

		UPDATE c
		SET Year5 = CASE WHEN c.PreferentialValue > 0.00 AND PrefLeft > 0 THEN CAST( c.PreferentialValue / @Years as DECIMAL(18,2) ) WHEN PrefLeft = 0.00 THEN 0.00 ELSE @ValueNumeric * (Percentage/100) END 
		FROM @Creditors c
		WHERE @Years <> 0

		SELECT @YearSum = (SELECT SUM(Year5) FROM @Creditors c WHERE @Years <> 0)
		SELECT @Pref = (SELECT SUM(ISNULL(PrefLeft,PreferentialValue)) FROM @Creditors c WHERE @Years <> 0)

		IF 0 < @Pref
		BEGIN 

			UPDATE c 
			SET Year5 = (@YearSum* (c.Percentage/100)), PrefLeft = ISNULL(PrefLeft,PreferentialValue) - (@YearSum* (c.Percentage/100))
			FROM @Creditors c 
			WHERE ISNULL(PrefLeft,PreferentialValue) > 0.00

			UPDATE c 
			SET Year5 = 0
			FROM @Creditors c 
			WHERE (PreferentialValue is NULL or PreferentialValue = 0.00)
				
			SELECT @YearSum = '', @Pref = ''
			
			IF EXISTS (SELECT * FROM @Creditors c WHERE ISNULL(PrefLeft,PreferentialValue) < 0) 
			BEGIN
			
				SELECT @Pref = (SELECT SUM(ISNULL(PrefLeft,PreferentialValue)) FROM @Creditors c WHERE @Years <> 0)

				UPDATE c 
				SET Year5 = (ABS(@Pref) * (Percentage/100))
				FROM @Creditors c 
				WHERE PrefLeft is NULL
				AND @Years <> 0

				UPDATE c 
				SET PrefLeft = 0, Year5 = Year5 + PrefLeft
				FROM @Creditors c 
				WHERE PrefLeft < 0

			END
			
		END

		/*Year 6*/

		IF 0 = (SELECT SUM(c.PrefLeft) FROM @Creditors c WHERE c.PreferentialValue > 0.00 AND @Years <> 0)
		BEGIN

			SELECT @PrefValueNumeric = 0

		END

		SELECT @ValueNumeric = cdv.ValueMoney-@PrefValueNumeric
		From CustomerDetailValues cdv WITH (NOLOCK)
		INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 660 AND tpm.ThirdPartyFieldGroupID = 26
		WHERE cdv.CustomerID = @CustomerID

		UPDATE c
		SET Year6 = CASE WHEN c.PreferentialValue > 0.00 AND PrefLeft > 0 THEN CAST( c.PreferentialValue / @Years as DECIMAL(18,2) ) WHEN PrefLeft = 0.00 THEN 0.00 ELSE @ValueNumeric * (Percentage/100) END 
		FROM @Creditors c
		WHERE @Years <> 0

		SELECT @YearSum = (SELECT SUM(Year6) FROM @Creditors c WHERE @Years <> 0)
		SELECT @Pref = (SELECT SUM(ISNULL(PrefLeft,PreferentialValue)) FROM @Creditors c WHERE @Years <> 0)

		IF 0 < @Pref
		BEGIN 

			UPDATE c 
			SET Year6 = (@YearSum* (c.Percentage/100)), PrefLeft = ISNULL(PrefLeft,PreferentialValue) - (@YearSum* (c.Percentage/100))
			FROM @Creditors c 
			WHERE ISNULL(PrefLeft,PreferentialValue) > 0.00

			UPDATE c 
			SET Year6 = 0
			FROM @Creditors c 
			WHERE (PreferentialValue is NULL or PreferentialValue = 0.00)
				
			SELECT @YearSum = '', @Pref = ''
			
			IF EXISTS (SELECT * FROM @Creditors c WHERE ISNULL(PrefLeft,PreferentialValue) < 0) 
			BEGIN
			
				SELECT @Pref = (SELECT SUM(ISNULL(PrefLeft,PreferentialValue)) FROM @Creditors c WHERE @Years <> 0)

				UPDATE c 
				SET Year6 = (ABS(@Pref) * (Percentage/100))
				FROM @Creditors c 
				WHERE PrefLeft is NULL
				AND @Years <> 0

				UPDATE c 
				SET PrefLeft = 0, Year6 = Year6 + PrefLeft
				FROM @Creditors c 
				WHERE PrefLeft < 0

			END
			
		END
		
	END

	IF @TermMonthsValue > 0
	BEGIN
	
		SELECT @Years = @Years -1
		
		UPDATE c 
		SET Year6 = CASE @Years WHEN 5 THEN (Year6/12)*@TermMonthsValue ELSE Year6 END,
			Year5 = CASE @Years WHEN 4 THEN (Year5/12)*@TermMonthsValue ELSE Year5 END,
			Year4 = CASE @Years WHEN 3 THEN (Year4/12)*@TermMonthsValue ELSE Year4 END,
			Year3 = CASE @Years WHEN 2 THEN (Year3/12)*@TermMonthsValue ELSE Year3 END,
			Year2 = CASE @Years WHEN 1 THEN (Year2/12)*@TermMonthsValue ELSE Year2 END
		FROM @Creditors c 
	
	END

	IF @Years = -1	/*2014-11-18.  Year -1 = Accelerated Cases.  Single payment, Year 1 only*/
	BEGIN
	
		/*Year -1*/
		SELECT @ValueNumeric = cdv.ValueMoney
		From CustomerDetailValues cdv WITH (NOLOCK)
		INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 1404 AND tpm.ThirdPartyFieldGroupID = 26
		WHERE cdv.CustomerID = @CustomerID		
		AND ( SELECT count(*) FROM @Creditors ) > 0	

		SELECT @ValueNumeric = @ValueNumeric - ISNULL(SUM(cr.OverrideValue),0.00)
		FROM @Creditors cr 
		WHERE cr.OverrideValue is NOT NULL

		UPDATE c
		SET Year1 = CASE WHEN c.OverrideValue > 0.00 THEN c.OverrideValue ELSE @ValueNumeric * (Percentage/100.000) END
		FROM @Creditors c 
		WHERE @Years <> 0
		
		
		INSERT @TableOut (	TableRowID,
				Creditor,
				[Balance Outstanding],
				Percentage,
				[Year 1],
				[Year 2],
				[Year 3],
				[Year 4],
				[Year 5],
				[Year 6],
				[Year 7],
				Totals,
				[Return for creditors],
				DenyEdit,
				DenyDelete
			)
		SELECT	  c.TableRowID
				, c.Name [Creditor]
				, ISNULL(NULLIF(c.PreferentialValue,0.00),c.Amount) [Balance Outstanding]
				, CASE WHEN c.OverrideValue > 0.00 THEN NULL ELSE c.Percentage END  [Percentage]
				, CAST(Year1 as DECIMAL(18,2))	[Year 1]
				, CAST(0.00 as DECIMAL(18,2)) 			[Year 2]
				, CAST(0.00 as DECIMAL(18,2)) 			[Year 3]
				, CAST(0.00 as DECIMAL(18,2)) 			[Year 4]
				, CAST(0.00 as DECIMAL(18,2)) 			[Year 5]
				, CAST(0.00 as DECIMAL(18,2)) 			[Year 6]
				, CAST(0.00 as DECIMAL(18,2)) 			[Year 7]
				, CAST(@ValueNumeric as DECIMAL(18,2)) 	[Totals]
				, CONVERT ( VARCHAR, CAST( (Year1 / ISNULL(NULLIF(c.Amount,0.00),c.PreferentialValue)) * 100 AS DECIMAL(18,2) ) ) + '%' [Return for creditors]
				, 'true' [DenyEdit]
				, 'true' [DenyDelete]
		FROM @Creditors c 
		WHERE ISNULL(NULLIF(c.Amount,0.00),c.PreferentialValue) <> 0
		AND ( Percentage > 0 OR NOT EXISTS ( SELECT * FROM @Creditors cr WHERE cr.Percentage > 0 ) )
		ORDER BY PreferentialValue DESC, Amount DESC
		
	END	
	ELSE
	BEGIN
		INSERT @TableOut (	TableRowID,
							Creditor,
							[Balance Outstanding],
							Percentage,
							[Year 1],
							[Year 2],
							[Year 3],
							[Year 4],
							[Year 5],
							[Year 6],
							[Year 7],
							Totals,
							[Return for creditors],
							DenyEdit,
							DenyDelete
						)
		SELECT	  c.TableRowID
				, c.Name [Creditor]
				, ISNULL(NULLIF(c.PreferentialValue,0.00),c.Amount) [Balance Outstanding]
				, c.Percentage [Percentage]
				, CAST(c.[Year1] as DECIMAL(18,2)) [Year 1]
				, CAST(c.[Year2] as DECIMAL(18,2)) [Year 2]
				, CAST(c.[Year3] as DECIMAL(18,2)) [Year 3]
				, CAST(c.[Year4] as DECIMAL(18,2)) [Year 4]
				, CAST(c.[Year5] as DECIMAL(18,2)) [Year 5]
				, CAST(c.[Year6] as DECIMAL(18,2)) [Year 6]
				, CAST(c.[Year7] as DECIMAL(18,2)) [Year 7]
				, CAST(c.[Year1]+c.[Year2]+c.[Year3]+c.[Year4]+c.[Year5]+ISNULL(c.[Year6],0.00)+ISNULL(c.[Year7],0.00) as DECIMAL(18,2)) [Totals]
				, CONVERT(VARCHAR,CAST( ((c.[Year1]+c.[Year2]+c.[Year3]+c.[Year4]+c.[Year5]+ISNULL(c.[Year6],0.00)+ISNULL(c.[Year7],0.00)) / ISNULL(NULLIF(c.Amount,'0.00'),c.PreferentialValue)) * 100 AS DECIMAL(18,2))) + '%' [Return for creditors]
				, 'true' [DenyEdit]
				, 'true' [DenyDelete]
		FROM @Creditors c 
		WHERE ISNULL(NULLIF(c.Amount,0.00),c.PreferentialValue) <> 0
		AND Percentage > 0
		ORDER BY PreferentialValue DESC, Amount DESC
	END
END

	RETURN
	
END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerID] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerID] TO [sp_executeall]
GO
