SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-01-17
-- Description:	Rules engine function to find claims paid for the active policy year.  Used when pre-calculating renewals.
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_RulesEngine_AccidentClaimsValue]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS DECIMAL(18,2)
AS
BEGIN
 
	DECLARE @ReturnValue DECIMAL(18,2),
	@ClientID INT = dbo.fnGetPrimaryClientID()
	
	SELECT @ReturnValue = SUM(tdvTotal.ValueMoney)
	FROM Lead l with (NOLOCK) 
	INNER JOIN LeadTypeRelationship ltr with (NOLOCK) on ltr.FromLeadID = l.LeadID and ltr.ToLeadTypeID = 1490 
	INNER JOIN Matter m with (NOLOCK) on m.MatterID = ltr.ToMatterID 
	INNER JOIN dbo.TableRows r WITH (NOLOCK) on r.MatterID = m.MatterID and r.DetailFieldID = 144355 
	--INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 144349 
	--INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 144351
	--INNER JOIN dbo.TableDetailValues tdvPaid WITH (NOLOCK) ON r.TableRowID = tdvPaid.TableRowID AND tdvPaid.DetailFieldID = 144362 -- 144354 we now use approved and not paid
	INNER JOIN dbo.TableDetailValues tdvTotal WITH (NOLOCK) ON r.TableRowID = tdvTotal.TableRowID AND tdvTotal.DetailFieldID = 144352
	INNER JOIN dbo.MatterDetailValues mdvDateOfLoss WITH (NOLOCK) ON m.MatterID = mdvDateOfLoss.MatterID AND mdvDateOfLoss.DetailFieldID = 144892
	INNER JOIN dbo.MatterDetailValues ailment with (NOLOCK) on ailment.MatterID = m.MatterID and ailment.DetailFieldID = 144504 
	INNER JOIN dbo.ResourceListDetailValues accorill with (NOLOCK) on accorill.ResourceListID = ailment.ValueInt and accorill.DetailFieldID = 162655
	WHERE r.DetailFieldPageID = 16157
	AND r.ClientID = @ClientID
	AND mdvDateOfLoss.ValueDate BETWEEN DATEADD(Month,-12,dbo.fn_GetDate_Local()) and dbo.fn_GetDate_Local() 
	 -- All paid rows and the rows from this claim
	 AND l.LeadID = @LeadID 
	 AND accorill.ValueInt = 69524 
	
	RETURN @ReturnValue
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_AccidentClaimsValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_RulesEngine_AccidentClaimsValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_AccidentClaimsValue] TO [sp_executeall]
GO
