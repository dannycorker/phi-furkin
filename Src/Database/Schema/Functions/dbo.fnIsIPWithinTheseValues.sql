SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2015-09-30
-- Description:	Check if the IP Address is within the upper and lower limit passed in
-- Can also be called from dbo.fnIsIPWithinRange which decodes CIDR eg '81.128.181.1/28' to "between 81.128.181.1 and 81.128.181.15" first
-- =============================================
CREATE FUNCTION [dbo].[fnIsIPWithinTheseValues]
(
	@IPAddress VARCHAR(20) = NULL,		/* IP4 ONLY! */
	@StartAddress VARCHAR(20) = NULL,	/* IP4 ONLY! */
	@EndAddress VARCHAR(20) = NULL		/* IP4 ONLY! */
)
RETURNS BIT 
WITH RETURNS NULL ON NULL INPUT 
AS
BEGIN
	
	DECLARE @Octet BIGINT = 256, 
	@IPBigInt BIGINT,
	@StartBigInt BIGINT,
	@EndBigInt BIGINT, 
	@IsValid BIT = 0
	
	/*
		Each octet of an IP is a tinyint (0-255) represented by 8 bits 00000000-11111111
		
		There are 4 octets so the IP 255.255.255.255 can be thought of as 11111111.11111111.11111111.11111111 in binary
		
		To compare ranges though, we need to remove the "." dots and that means we need to use the concept of lo-bytes and hi-bytes, 
		where the 1's to the right are worth less. The right-most 11111111 is worth 255, but the next 11111111 to the left is worth 65280
		which is 255 * 256.  Each octet to the left is worth 256 more than the last one.
		
		255.255.255.255 equates to 4294967295 in case you were wondering, which is (2^32) - 1
		
		So we just need to change all the IPs to BIGINT and compare the values to see if @IPAddress between @StartAddress and @EndAddress
	*/
	SELECT @IPBigInt = PARSENAME(@IPAddress,1) + @Octet * PARSENAME(@IPAddress,2) + @Octet * @Octet * PARSENAME(@IPAddress ,3) + @Octet * @Octet * @Octet * PARSENAME(@IPAddress ,4),
	@StartBigInt = PARSENAME(@StartAddress,1) + @Octet * PARSENAME(@StartAddress,2) + @Octet * @Octet * PARSENAME(@StartAddress ,3) + @Octet * @Octet * @Octet * PARSENAME(@StartAddress ,4), 
	@EndBigInt = PARSENAME(@EndAddress,1) + @Octet * PARSENAME(@EndAddress,2) + @Octet * @Octet * PARSENAME(@EndAddress ,3) + @Octet * @Octet * @Octet * PARSENAME(@EndAddress ,4)
		
	SELECT @IsValid = CASE WHEN @IPBigInt BETWEEN @StartBigInt AND @EndBigInt THEN 1 ELSE 0 END
	
	/* Return true or false.  NULLs are handled in the function declaration */
	RETURN @IsValid
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsIPWithinTheseValues] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnIsIPWithinTheseValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsIPWithinTheseValues] TO [sp_executeall]
GO
