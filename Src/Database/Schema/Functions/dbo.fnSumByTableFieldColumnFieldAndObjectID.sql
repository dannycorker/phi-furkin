SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2013-06-14
-- Description:	Sum Values in a particular column in a table field (TABLECOLUMNSUM does this, but that broke just before a crucial demo)
-- =============================================
CREATE FUNCTION [dbo].[fnSumByTableFieldColumnFieldAndObjectID]
(
	@ObjectID				int,
	@DetailFieldID			int,
	@ColumnDetailFieldID	int
)
RETURNS MONEY
AS
BEGIN

	DECLARE	@LeadOrMatter int,
			@ReturnValue MONEY = 0.00
	
	SELECT @LeadOrMatter = df.LeadOrMatter
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @DetailFieldID
	
	IF @LeadOrMatter = 1
	BEGIN
		SELECT @ReturnValue = SUM(tdv.ValueMoney)
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
		WHERE	tr.DetailFieldID = @DetailFieldID
		and		tdv.DetailFieldID = @ColumnDetailFieldID
		and		tr.LeadID = @ObjectID
	END
	ELSE IF @LeadOrMatter = 2
	BEGIN
		SELECT @ReturnValue = SUM(tdv.ValueMoney)
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
		WHERE	tr.DetailFieldID = @DetailFieldID
		and		tdv.DetailFieldID = @ColumnDetailFieldID
		and		tr.MatterID = @ObjectID
	END
	ELSE IF @LeadOrMatter = 10
	BEGIN
		SELECT @ReturnValue = SUM(tdv.ValueMoney)
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
		WHERE	tr.DetailFieldID = @DetailFieldID
		and		tdv.DetailFieldID = @ColumnDetailFieldID
		and		tr.CustomerID = @ObjectID
	END
	ELSE IF @LeadOrMatter = 11
	BEGIN
		SELECT @ReturnValue = SUM(tdv.ValueMoney)
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
		WHERE	tr.DetailFieldID = @DetailFieldID
		and		tdv.DetailFieldID = @ColumnDetailFieldID
		and		tr.CaseID = @ObjectID
	END	

	RETURN @ReturnValue
END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnSumByTableFieldColumnFieldAndObjectID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnSumByTableFieldColumnFieldAndObjectID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnSumByTableFieldColumnFieldAndObjectID] TO [sp_executeall]
GO
