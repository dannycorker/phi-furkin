SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2015-09-30
-- Description:	Check if the IP Address is within the range passed in
-- Decodes the range to a start and end address and then calls dbo.fnIsIPWithinTheseValues 
-- Example: CIDR '81.128.181.1/28' decodes to "between 81.128.181.1 and 81.128.181.15" 
-- =============================================
CREATE FUNCTION [dbo].[fnIsIPWithinRange]
(
	@IPAddress VARCHAR(20) = NULL,	/* IP4 ONLY! eg 'C' */
	@CIDR VARCHAR(20) = NULL		/* IP4 ONLY! Classless Inter-Domain Routing eg '81.128.181.1/28' which means '81.128.181.1' to '81.128.181.15' */
)
RETURNS BIT 
WITH RETURNS NULL ON NULL INPUT 
AS
BEGIN
	
	/*
		Each octet of an IP is a tinyint (0-255) represented by 8 bits 00000000-11111111
		
		There are 4 octets so the IP 255.255.255.255 can be thought of as 11111111.11111111.11111111.11111111 in binary
		
		The /28 in the example means that the 28 highest bits are locked down (the Network part) 
		Since there are 32 bits on offer, that leaves 4 (for the Host part) to play with
		
		In this example we are looking at a range '81.128.181.1/28' which translates to this:
		<------ NETWORK 28 BITS ------>HOST
		11111111.11111111.11111111.11111111
		
		So HOST has 1111 == ((2^4)-1) == 15 in decimal values to play with.  
		
		Now look at the final octet ".1" and work out where it falls.  It is on the 0-15 range in this example, but it could equally 
		have started at ".16" and therefore gone into the 16-31 range.  See more detailed working below.
	*/
	DECLARE @IsValid BIT = 0,
	@SlashPos INT = 0, 
	@NetworkBits INT = 32, 
	@HostBits INT = 0,
	@HostWiggleRoom INT = 0,		/* Tinyint really, 0-255 maximum, but INT saves a bit of casting later */
	@FinalOctetValue INT = 0,		/* Tinyint really, 0-255 maximum, but INT saves a bit of casting later */
	@FinalOctetMustStartAt INT = 0,	
	@FinalOctetMustEndAt INT = 255,
	@NetworkPart VARCHAR(20) = '',	/* Everything before the slash */
	@NetworkBase VARCHAR(20) = '',	/* The first 3 octects only */
	@IPStartFrom VARCHAR(20) = '',	/* Network base plus our calculated final octet start value (.0 or .224 for example) */
	@IPEndAt VARCHAR(20) = ''		/* Network base plus our calculated final octet end value   (.15 or .231 for example) */
	
	SELECT @SlashPos = CHARINDEX('/', @CIDR)
	SELECT @NetworkBits = CAST(SUBSTRING(@CIDR, @SlashPos + 1, 2) AS INT)
	
	IF @NetworkBits BETWEEN 25 AND 32
	BEGIN
		SELECT @NetworkPart = SUBSTRING(@CIDR, 1, @SlashPos - 1)	/* Trim down '81.128.181.1/28' to just '81.128.181.1' */
		
		/*
			Now a challenge.  The final octet is '.1' and wiggle room = 15
			That means we need break 255 up into chunks of 15 and work out which range '.1' falls into.
			Not so bad for '.1' which clearly falls into the range 0-15
			But what about CIDR '81.128.181.227/29' instead?  Wiggle room of 8 rather than 16 means that 227 falls into range 224-231, not 225-240
		*/
		SELECT @FinalOctetValue = PARSENAME(@NetworkPart, 1),		/* From '81.128.181.1' get that final '.1' */
		@HostBits = 32 - @NetworkBits								/* If the highest 28 bits are for the network, that leaves 4 for the host pc */
		
		SELECT @HostWiggleRoom = POWER(2, @HostBits) 				/* 4 lowest bits gives combinations from 0000 to 1111 in binary = 16 combinations */
		
		/*
			Use a bitwise AND to barter 227 down to 224 or 1 down to 0 
			
			Example: .227/29 leaves 3 bits to play with.  Zero those out from 255 original combinations and you get 11111000
			The options run from 11111000 to 11111111
			227 equates to 11101101
			ANDing 11101101 with 11111000 yields, erm.... 11101000, which is, erm again.... 224 in decimal
		*/
		SELECT @FinalOctetMustStartAt = (256-@HostWiggleRoom) & @FinalOctetValue	
				
		/*
			For the end of the range, add the remaining possible options to the start of the range (8 in our example of .227/29 )
			.224 is the start of our final octet, so .231 is the new finish
			
			@HostWiggleRoom is 8 at this stage, so knock 1 off to make the arithmetic work
			8 combinations between 224 and 231 but only a difference of 7
		*/
		SELECT @FinalOctetMustEndAt = @FinalOctetMustStartAt + (@HostWiggleRoom - 1)
		
		/*
			Build real IP addresses using the final octet range
		*/
		SELECT @NetworkBase = PARSENAME(@NetworkPart, 4) + '.' + PARSENAME(@NetworkPart, 3) + '.' + PARSENAME(@NetworkPart, 2) + '.'
		
		SELECT @IPStartFrom = @NetworkBase + CAST(@FinalOctetMustStartAt AS VARCHAR), 
		@IPEndAt = @NetworkBase + CAST(@FinalOctetMustEndAt AS VARCHAR) 
		
		/*
			Do I win £5 quid?
		*/
		SELECT @IsValid = dbo.fnIsIPWithinTheseValues(@IPAddress, @IPStartFrom, @IPEndAt)
	END
	/* ELSE a bit too complex at this stage! */
	/* Try here for more decoding tips */
	/* https://www.ultratools.com/tools/netMask */
	
	/* Tito's Modification Delete in case of problem */
	
	IF @NetworkBits between 17 and 24
		/* In this scenario the only relevant part would be the second octet*/
		BEGIN
			SELECT @NetworkPart = SUBSTRING(@CIDR, 1, @SlashPos - 1)	/* Trim down '81.128.181.1/22' to just '81.128.181.1' */
			
			/*
			Now a challenge.  The second to last octet is '.181' and wiggle room = 4
			That means we need break 255 up into chunks of 4 and work out which range '.181' falls into.
			
		*/
		SELECT @FinalOctetValue = PARSENAME(@NetworkPart, 2),		/* From '81.128.181.1' get the second octet '.181' */
		@HostBits = 32 - @NetworkBits - 8								/* If the highest 22 bits are for the network, that leaves 10 for the host pc */
																	/* Since the last octet will be fully used what matters is the second octet */
		
		SELECT @HostWiggleRoom = POWER(2, @HostBits)				/* 2 lowest bits gives combinations from 00 to 11 in binary = 4 combinations */
		
		/*
			Use a bitwise AND to barter 227 down to 224 or 1 down to 0 
			
			Example: .181.1/22 leaves 10 bits to play with but the 2 leftmost ones will define the range.
			Zero those out from 255 original combinations and you get 11111100
			The options run from 11111100 to 11111111
			181 equates to 10110101
			ANDing 10110101 with 11111100 yields, erm.... 10110100, which is, erm again.... 180 in decimal
		*/
		SELECT @FinalOctetMustStartAt = (256-@HostWiggleRoom) & @FinalOctetValue
		
		/*
			For the end of the range, add the remaining possible options to the start of the range (4 in our example of .181.1/22 )
			.180 is the start of our second to last octet, so .183 is the new finish
			
			@HostWiggleRoom is 4 at this stage, so knock 1 off to make the arithmetic work
			8 combinations between 224 and 231 but only a difference of 7
		*/
		SELECT @FinalOctetMustEndAt = @FinalOctetMustStartAt + (@HostWiggleRoom - 1)
		
		/*
			Build real IP addresses using the final octet range
		*/
		SELECT @NetworkBase = PARSENAME(@NetworkPart, 4) + '.' + PARSENAME(@NetworkPart, 3) + '.'
		
		SELECT @IPStartFrom = @NetworkBase + CAST(@FinalOctetMustStartAt AS VARCHAR) + '.' + '0', 
		@IPEndAt = @NetworkBase + CAST(@FinalOctetMustEndAt AS VARCHAR) + '.' + '255'
		
		/*
			Do I win £5 quid?
		*/
		SELECT @IsValid = dbo.fnIsIPWithinTheseValues(@IPAddress, @IPStartFrom, @IPEndAt)	
		
		END 
		
		/* End of titos bit of coding */
	
	RETURN @IsValid
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsIPWithinRange] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnIsIPWithinRange] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsIPWithinRange] TO [sp_executeall]
GO
