SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-06-20
-- Description:	Sum Matter Values by Customer
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_SumMatterFieldsByCustomer]
(
@CustomerID int,
@DetailFieldID int
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE @ReturnValue VARCHAR(2000)
	
	SELECT @ReturnValue = SUM(mdv.ValueMoney)
	FROM MatterDetailValues mdv WITH (NOLOCK)
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = mdv.MatterID and m.CustomerID = @CustomerID
	WHERE mdv.DetailFieldID = @DetailFieldID 

	RETURN @ReturnValue

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumMatterFieldsByCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_SumMatterFieldsByCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumMatterFieldsByCustomer] TO [sp_executeall]
GO
