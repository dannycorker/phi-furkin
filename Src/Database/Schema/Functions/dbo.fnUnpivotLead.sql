SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Unpivot the columns in the Lead table 
-- =============================================
CREATE FUNCTION [dbo].[fnUnpivotLead] 
(	
	@LeadID int
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT ColumnName as [RawColumnName], '[!' + ColumnName + ']' as [FormattedColumnName], RealValue
	FROM
	(
		SELECT 
		COALESCE(CONVERT(varchar(255), c.AquariumStatusID), '') as [AquariumStatusID],
		COALESCE(CONVERT(varchar(255), c.Assigned), '') as [Assigned],
		COALESCE(CONVERT(varchar(255), c.AssignedBy), '') as [AssignedBy],
		COALESCE(CONVERT(varchar(255), c.AssignedDate), '') as [AssignedDate],
		COALESCE(CONVERT(varchar(255), c.AssignedTo), '') as [AssignedTo],
		COALESCE(CONVERT(varchar(255), c.BrandNew), '') as [BrandNew],
		COALESCE(CONVERT(varchar(255), c.ClientID), '') as [ClientID],
		COALESCE(CONVERT(varchar(255), c.ClientStatusID), '') as [ClientStatusID],
		COALESCE(CONVERT(varchar(255), c.CustomerID), '') as [CustomerID],
		COALESCE(CONVERT(varchar(255), c.LeadID), '') as [LeadID],
		COALESCE(CONVERT(varchar(255), c.LeadRef), '') as [LeadRef],
		COALESCE(CONVERT(varchar(255), c.LeadTypeID), '') as [LeadTypeID],
		COALESCE(CONVERT(varchar(255), c.Password), '') as [Password],
		COALESCE(CONVERT(varchar(255), c.RecalculateEquations), '') as [RecalculateEquations],
		COALESCE(CONVERT(varchar(255), c.Salt), '') as [Salt],
		COALESCE(CONVERT(varchar(255), c.WhenCreated), '') as [WhenCreated]
		FROM dbo.Lead c WITH (NOLOCK) 
		WHERE c.LeadID = @LeadID) p
	UNPIVOT
	(
		RealValue FOR ColumnName IN
		(
			[AquariumStatusID],[Assigned],[AssignedBy],[AssignedDate],[AssignedTo],[BrandNew],[ClientID],[ClientStatusID],[CustomerID],[LeadID],[LeadRef],[LeadTypeID],[Password],[RecalculateEquations],[Salt],[WhenCreated]
		)
	) as unpvt

)





GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotLead] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnUnpivotLead] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotLead] TO [sp_executeall]
GO
