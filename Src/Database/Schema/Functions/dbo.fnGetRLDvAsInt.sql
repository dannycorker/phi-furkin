SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 04-Nov-2011
-- Description:	
-- Mods
-- DCM 27/10/2014 Added CustomerDetailValues RLs
-- =============================================
CREATE FUNCTION [dbo].[fnGetRLDvAsInt]
(
	@DetailFieldID		int, /* L/M Field */
	@ResourceFieldID	int, /* ResourceList Detail Field ID */
	@CaseID				int
)
RETURNS int
AS
BEGIN

DECLARE @DetailValue as int,
		@LeadOrMatter int,
		@CustomerID INT

	Select @LeadOrMatter = LeadOrmatter
	From DetailFields with (nolock) 
	Where DetailFieldID = @DetailFieldID	

	If @LeadOrMatter = 1
	Begin

		Select @DetailValue = rldv.ValueInt 
		From Cases c with (nolock) 
		Inner Join LeadDetailValues ldv with (nolock) on c.LeadID = ldv.LeadID and ldv.DetailFieldID = @DetailFieldID
		Inner Join ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = ldv.ValueInt and rldv.DetailFieldID = @ResourceFieldID
		Where c.CaseID = @CaseID 
	
	End

	If @LeadOrMatter = 2
	Begin

		Select @DetailValue = rldv.ValueInt 
		From Matter m with (nolock) 
		Inner Join MatterDetailValues mdv with (nolock) on m.MatterID = mdv.MatterID and mdv.DetailFieldID = @DetailFieldID
		Inner Join ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = mdv.ValueInt and rldv.DetailFieldID = @ResourceFieldID
		Where m.CaseID = @CaseID
	
	End

	If @LeadOrMatter = 11 /*Case*/
	Begin

		SELECT @DetailValue = rldv.ValueInt
		FROM CaseDetailValues cdv WITH (NOLOCK) 
		Inner Join ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = cdv.ValueInt and rldv.DetailFieldID = @ResourceFieldID
		WHERE cdv.CaseID = @CaseID
		and cdv.DetailFieldID = @DetailFieldID
	
	End

	If @LeadOrMatter = 10 /*Customer*/
	Begin

		SELECT @DetailValue = rldv.ValueInt
		FROM CustomerDetailValues cdv WITH (NOLOCK) 
		Inner Join ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = cdv.ValueInt and rldv.DetailFieldID = @ResourceFieldID
		INNER JOIN Lead l WITH (NOLOCK) ON cdv.CustomerID=l.CustomerID
		INNER JOIN Cases ca WITH (NOLOCK) ON ca.LeadID=l.LeadID
		WHERE ca.CaseID = @CaseID
		and cdv.DetailFieldID = @DetailFieldID
	
	End


	RETURN @DetailValue

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetRLDvAsInt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetRLDvAsInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetRLDvAsInt] TO [sp_executeall]
GO
