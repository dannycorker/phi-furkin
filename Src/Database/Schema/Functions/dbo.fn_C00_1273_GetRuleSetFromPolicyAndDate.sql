SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-08-21
-- Description:	Returns the current rule set for a poliyc scheme matter
-- CW 2018-04-17 OMF Policy needs to return the ruleset for Premier
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetRuleSetFromPolicyAndDate]
(
	@PolicyMatterID INT,
	@TermsDate DATE
)
RETURNS INT	
AS
BEGIN
 
	DECLARE @RuleSetID INT
	
	/*CW 2018-014-17 Convert OMF scheme ID to premier rule set ID, maybe link this to a field in the future? */
	IF @PolicyMatterID IN (10276)
	BEGIN
	Select @PolicyMatterID = 10274	
	END
	
	-- BEU ***** Needs updating when the rating engine starts doing versioning, to find the actual rulesetID from the original (included in the
	-- scheme set-up and the version date)
 
	-- Match this scheme against the correct matter on the schemes lead type
	SELECT @RuleSetID = tdvRule.ValueInt 
	FROM  dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvRule WITH (NOLOCK) ON r.TableRowID = tdvRule.TableRowID AND tdvRule.DetailFieldID = 170202
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 170203 AND (tdvStart.ValueDate <= @TermsDate OR tdvStart.ValueDate IS NULL) 
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 170204 AND (tdvEnd.ValueDate >= @TermsDate OR tdvEnd.ValueDate IS NULL) 
	WHERE r.MatterID = @PolicyMatterID
	
	RETURN @RuleSetID
 
END
 





GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetRuleSetFromPolicyAndDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetRuleSetFromPolicyAndDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetRuleSetFromPolicyAndDate] TO [sp_executeall]
GO
