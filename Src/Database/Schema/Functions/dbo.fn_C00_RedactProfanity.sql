SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- ALTER date: 2014-01-30
-- Description:	Redact profanities in input string
--              Replaces whole word profanities with same length of '*' characters if below (= worse than) specified level
--				And replaces profanity within word with same length of '*' characters if below (= worse than) specified level
--				  unless word is in ProfanityExclusion table
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_RedactProfanity]
(
		@InputString VARCHAR(2000)
		,@ProfanityLevelBelowWhichToRedact INT
		,@ClientID INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	
	DECLARE @OutputString    VARCHAR(2000)     
				,@Replacement     VARCHAR(100)       = REPLICATE('*', 100)
				,@Found INT = 1
	            
	DECLARE @Blacklist TABLE (
							  BlackListWord VARCHAR(100),
							  RedactedWord VARCHAR (100)
							  )            

	SELECT @OutputString = ' ' + @InputString + ' '

	-- 1. Whole words that are profanities. Redact whole word

	WHILE @Found = 1
	BEGIN

		SELECT @Found=0
		SELECT @OutputString = 
			CASE WHEN PATINDEX ( '% ' + p.Word + '[^a-Z]%' , @OutputString ) > 0 
				 THEN STUFF ( @OutputString , PATINDEX ( '% ' + p.Word + '[^a-Z]%' , @OutputString )+1 , LEN(p.Word), LEFT(@Replacement,LEN(p.Word)) )
				 ELSE STUFF ( @OutputString , PATINDEX ( '%[^a-Z]' + p.Word + ' %' , @OutputString )+1 , LEN(p.Word), LEFT(@Replacement,LEN(p.Word)) )
				 END,
				@Found=1
		FROM [AquariusMaster].dbo.Profanity p WITH (NOLOCK)
		WHERE ( @OutputString LIKE  '% ' + p.Word + '[^a-Z]%' OR @OutputString LIKE '%[^a-Z]' + p.Word + ' %' )
			AND p.Score < @ProfanityLevelBelowWhichToRedact

	END

	-- 2. Words containing profanity string. Redact profanity string if profanity is above the level specified and the word is not in the blacklist

	INSERT INTO @Blacklist (BlackListWord,RedactedWord)
	SELECT pe.Word, REPLACE ( pe.Word , p.Word, LEFT(@Replacement,LEN(p.Word)) )
	FROM [AquariusMaster].dbo.ProfanityExclusion pe
	INNER JOIN [AquariusMaster].dbo.Profanity p WITH (NOLOCK) ON pe.Word LIKE '%' + p.Word + '%'
	WHERE (pe.ClientID = 0 OR pe.ClientID = @ClientID) AND @OutputString LIKE '%' + p.Word + '%'
	
	SELECT @Found = 1
	
	WHILE @Found = 1
	BEGIN
	
		SELECT @Found=0

		SELECT @OutputString = 
			CASE WHEN PATINDEX ( '%' +  p.Word + '[a-Z]%' , @OutputString ) > 0 
				 THEN STUFF ( @OutputString , PATINDEX ( '%' +  p.Word + '[a-Z]%' , @OutputString ), LEN(p.Word), LEFT(@Replacement,LEN(p.Word)) )
				 ELSE STUFF ( @OutputString , PATINDEX ( '%[a-Z]' +  p.Word + '%' , @OutputString ), LEN(p.Word), LEFT(@Replacement,LEN(p.Word)) )
				 END,
				@Found=1
		FROM [AquariusMaster].dbo.Profanity p
		WHERE (@OutputString LIKE  '%' +  p.Word + '[a-Z]%' OR @OutputString LIKE '%[a-Z]' +  p.Word + '%')
		AND p.Score < @ProfanityLevelBelowWhichToRedact
	
	END
	
	SELECT @OutputString = REPLACE ( @OutputString , b.RedactedWord, b.BlackListWord ) FROM @Blacklist b
	
	-- 3. Finally return the redacted string
	RETURN LTRIM(RTRIM(@OutputString))
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_RedactProfanity] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_RedactProfanity] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_RedactProfanity] TO [sp_executeall]
GO
