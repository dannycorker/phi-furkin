SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-07-17
-- Description:	Count other matters with active customer MTAs
-- =============================================
CREATE FUNCTION [dbo].[fn_C433_MTA_CountActiveCustomerMTAsForMatter]
(
	 @CaseID INT
	,@InProgressOrCalculating INT = 1
)
RETURNS INT
AS
BEGIN
		
	DECLARE @Count INT = 0
	
	DECLARE @DetailFieldID INT = CASE @InProgressOrCalculating 
									WHEN 1 THEN 177906 /*Customer MTA In Progress*/
									WHEN 2 THEN 177904 /*Customer MTA Calculating*/ 
								 ELSE 0 END 
		
	SELECT @Count = COUNT(m.MatterID)
	FROM Matter m WITH ( NOLOCK ) 
	INNER JOIN Matter m2 WITH ( NOLOCK ) on m2.CustomerID = m.CustomerID
	INNER JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = m2.MatterID AND mdv.DetailFieldID = @DetailFieldID
	WHERE m.CaseID = @CaseID
	AND mdv.ValueDate is not NULL
	AND m2.MatterID <> m.MatterID 

	RETURN @Count

END






GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C433_MTA_CountActiveCustomerMTAsForMatter] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C433_MTA_CountActiveCustomerMTAsForMatter] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C433_MTA_CountActiveCustomerMTAsForMatter] TO [sp_executeall]
GO
