SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Christian Williams
-- Create date: 2017-01-17
-- Description:	Allow documents on the claims lead type to use rules based on policy lead type values
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_RulesEngine_SchemeTypeFromClaimsLead]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS INT
AS
BEGIN
 
	DECLARE @ReturnValue INT
	
	SELECT @ReturnValue = dbo.fn_C00_1272_GetSchemeFromClaim(@MatterID)

	RETURN @ReturnValue
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_SchemeTypeFromClaimsLead] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_RulesEngine_SchemeTypeFromClaimsLead] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_SchemeTypeFromClaimsLead] TO [sp_executeall]
GO
