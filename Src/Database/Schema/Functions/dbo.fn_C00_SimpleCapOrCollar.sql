SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-07-023
-- Description:	Simple cap or collar
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_SimpleCapOrCollar]
(
	@ValueToCapOrCollar NUMERIC(24,18),
	@CapOrCollarValue NUMERIC(24,18),
	@CapOrCollar INT /*1=cap 2=collar*/
)
RETURNS NUMERIC(18,4)
AS
BEGIN

	DECLARE @CappedOrCollaredValue NUMERIC(18,4)

	SELECT @CappedOrCollaredValue = CASE WHEN @CapOrCollar = 1 
		THEN 
			/*Cap*/
			CASE WHEN @ValueToCapOrCollar > @CapOrCollarValue
				THEN @CapOrCollarValue 
				ELSE @ValueToCapOrCollar
			END
		ELSE
			CASE WHEN @ValueToCapOrCollar < @CapOrCollarValue 
				THEN @CapOrCollarValue 
				ELSE @ValueToCapOrCollar
			END
		END 

	RETURN @CappedOrCollaredValue

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SimpleCapOrCollar] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_SimpleCapOrCollar] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SimpleCapOrCollar] TO [sp_executeall]
GO
