SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Chrisitan Williams
-- Create date: 2018-04-27
-- Description:	Returns claim excess amount
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_RulesEngine_DocumentCoInsRule]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS INT	
AS
BEGIN
 
	DECLARE @Excess DECIMAL(18,2)
			
	
	Select @Excess = Sum(ABS(Excess.ValueMoney)) from TableRows ClaimDetails WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues Excess WITH (NOLOCK) on Excess.TableRowID = ClaimDetails.TableRowID and Excess.DetailFieldID = 146407
	Where ClaimDetails.MatterID = @MatterID and ClaimDetails.DetailFieldID = 144355 

	RETURN @Excess
 
END
 


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_DocumentCoInsRule] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_RulesEngine_DocumentCoInsRule] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_DocumentCoInsRule] TO [sp_executeall]
GO
