SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE FUNCTION [dbo].[RegexMatch] (@input [nvarchar] (max), @pattern [nvarchar] (4000))
RETURNS [bit]
WITH EXECUTE AS CALLER
EXTERNAL NAME [SqlServerProject_Regex].[UserDefinedFunctions].[RegexMatch]
GO
GRANT VIEW DEFINITION ON  [dbo].[RegexMatch] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RegexMatch] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RegexMatch] TO [sp_executeall]
GO
