SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-09-01
-- Description:	Aq automation user ID
-- Mods
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetAqAutomationUser]
()
RETURNS INT	
AS
BEGIN

	RETURN 58552
 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetAqAutomationUser] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetAqAutomationUser] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetAqAutomationUser] TO [sp_executeall]
GO
