SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:  Jim Green
-- Create date: 2013-05-16
-- Description: Find the next or previous date within table rows, but then return another column from the same row. Created for LA2Plus payment dates originally.
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_FindNextOrPrevDateTDVReturnOtherColumn]
(
 @AnyID INT,
 @DetailFieldSubTypeID INT,
 @DetailFieldID INT,
 @SearchDirection INT,  /* 1 = Next, 2 = Prev */
 @BlankNotNull BIT,   /* Option to return an empty string rather than NULL */
 @DetailFieldIDToReturn INT, /* Having found the row with the appropriate date, return the value of this column from the same row */
 @ClientID INT,
 @TableDetailFieldID INT
)
RETURNS VARCHAR(2000)
AS
BEGIN
 DECLARE @AnyValue VARCHAR(2000) 
 
 /* TableDetailValues for a Matter */
 IF @DetailFieldSubTypeID = 2
 BEGIN
  IF @SearchDirection = 1
  BEGIN
   /* Find next date */
   SELECT TOP 1 @AnyValue = tdvOtherColumn.DetailValue  
   FROM dbo.TableDetailValues tdv WITH (NOLOCK) 
   INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID and tr.DetailFieldID = @TableDetailFieldID
   INNER JOIN dbo.TableDetailValues tdvOtherColumn WITH (NOLOCK) ON tdvOtherColumn.TableRowID = tdv.TableRowID AND tdvOtherColumn.DetailFieldID = @DetailFieldIDToReturn
   WHERE tdv.ClientID = @ClientID 
   AND tdv.MatterID = @AnyID 
   AND tdv.DetailFieldID = @DetailFieldID 
   AND tdv.ValueDate >= dbo.fnDateOnly(dbo.fn_GetDate_Local()) 
   ORDER BY tdv.ValueDate ASC
  END
  ELSE
  BEGIN
   /* Find previous date */
   SELECT TOP 1 @AnyValue = tdvOtherColumn.DetailValue 
   FROM dbo.TableDetailValues tdv WITH (NOLOCK) 
   INNER JOIN TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID and tr.DetailFieldID = @TableDetailFieldID
   INNER JOIN dbo.TableDetailValues tdvOtherColumn WITH (NOLOCK) ON tdvOtherColumn.TableRowID = tdv.TableRowID AND tdvOtherColumn.DetailFieldID = @DetailFieldIDToReturn
   WHERE tdv.ClientID = @ClientID 
   AND tdv.MatterID = @AnyID 
   AND tdv.DetailFieldID = @DetailFieldID 
   AND tdv.ValueDate <= dbo.fnDateOnly(dbo.fn_GetDate_Local()) 
   ORDER BY tdv.ValueDate DESC
  END
 END
 
 /* Option to return an empty string rather than NULL */
 IF @BlankNotNull = 1 AND @AnyValue IS NULL
 BEGIN
  SET @AnyValue = ''
 END
 
 RETURN @AnyValue

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_FindNextOrPrevDateTDVReturnOtherColumn] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_FindNextOrPrevDateTDVReturnOtherColumn] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_FindNextOrPrevDateTDVReturnOtherColumn] TO [sp_executeall]
GO
