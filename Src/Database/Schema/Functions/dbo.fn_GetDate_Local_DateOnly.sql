SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author: Gavin Reynolds
-- Create date: 2021-04-12
-- Description: Returns the local date as DATE
-- =============================================
CREATE FUNCTION [dbo].[fn_GetDate_Local_DateOnly]()
RETURNS DATE
AS
BEGIN
	DECLARE @Now DATE = GETDATE(),
		@Local DATE,
		@TimeZoneID INT,
		@ClientID INT = dbo.fnGetPrimaryClientID()

	SELECT @TimeZoneID = TimeZoneID
	FROM dbo.Clients WITH (NOLOCK)
	WHERE ClientID = @ClientID

	--Make sure the time zone is set for current client
	IF @TimeZoneID IS NOT NULL
	BEGIN

		SELECT @Local = dbo.fnGmtToRegional(@Now, @TimeZoneID)

	END

	--If the function fnGmtToRegional returned NULL or the time zone is not set
	IF @Local IS NULL
	BEGIN

		SELECT @Local = GETDATE()

	END

	RETURN @Local
END
GO
