SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2021-01-19
-- Description:	Get enrollment fee per affinity etc
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetEnrollmentFee]
(	
	@BrandID INT,
	@CustomerID INT = NULL,
	@Province INT,
	@PetCount INT,
	@ValidFrom DATE
)
RETURNS 
@EnrollmentFeeTable TABLE 
(
	FeeTableRowID INT,
	EnrollmentFee NUMERIC(18,2),
	TaxTableRowID INT,
	Tax NUMERIC(18,2),
	Total NUMERIC(18,2)
)
AS
BEGIN

	/*ACE 2021-01-20 FURKIN-130 - Add Enrollment Fee*/
	DECLARE @FeeTableRowID INT,
			@EnrollmentFee NUMERIC(18,2) = 0.00,
			@TaxTableRowID INT,
			@TaxRate NUMERIC(18,2) = 0.00,
			@TaxAmount NUMERIC(18,2) = 0.00,
			@Per INT /*Per household/pet*/

	IF @ValidFrom IS NULL
	BEGIN

		SELECT @ValidFrom = GETDATE()

	END

	/*Now get the enrollment fee*/
	SELECT @EnrollmentFee = tdv_fee.ValueMoney, 
			@FeeTableRowID = tdv_fee.TableRowID,
			@Per = tdv_per.ValueInt
	FROM TableDetailValues tdv_aff WITH (NOLOCK)
	INNER JOIN TableDetailValues tdv_validFrom WITH (NOLOCK) ON tdv_validFrom.TableRowID = tdv_aff.TableRowID AND tdv_validFrom.DetailFieldID = 315864
	INNER JOIN TableDetailValues tdv_validTo WITH (NOLOCK) ON tdv_validTo.TableRowID = tdv_aff.TableRowID AND tdv_validTo.DetailFieldID = 315865
	INNER JOIN TableDetailValues tdv_per WITH (NOLOCK) ON tdv_per.TableRowID = tdv_aff.TableRowID AND tdv_per.DetailFieldID = 315866
	INNER JOIN TableDetailValues tdv_fee WITH (NOLOCK) ON tdv_fee.TableRowID = tdv_aff.TableRowID AND tdv_fee.DetailFieldID = 315867
	WHERE tdv_aff.DetailFieldID = 315862
	AND tdv_aff.ResourceListID = @BrandID
	AND (tdv_validFrom.ValueDate <= @ValidFrom OR tdv_validFrom.ValueDate IS NULL)
	AND (tdv_validTo.ValueDate > @ValidFrom OR tdv_validTo.ValueDate IS NULL)

	/*
		193344 - Pet
		193345 - Household
	*/

	IF @Per = 193345 /*Household*/ AND (@PetCount > 1 /*OR @CustomerID > 0*/)
	BEGIN

		SELECT @EnrollmentFee = 0.00

	END

	/*Calc tax*/
	IF @EnrollmentFee > 0.00
	BEGIN

		/*Get the row ID of the row we need based on province and dates..*/
		SELECT @TaxTableRowID = tdv_prov.TableRowID
		FROM TableDetailValues tdv_prov WITH (NOLOCK)
		INNER JOIN TableDetailValues tdv_cat WITH (NOLOCK) ON tdv_cat.TableRowID = tdv_prov.TableRowID AND tdv_cat.DetailFieldID = 315869
		INNER JOIN TableDetailValues tdv_validFrom WITH (NOLOCK) ON tdv_validFrom.TableRowID = tdv_prov.TableRowID AND tdv_validFrom.DetailFieldID = 315870
		INNER JOIN TableDetailValues tdv_validTo WITH (NOLOCK) ON tdv_validTo.TableRowID = tdv_prov.TableRowID AND tdv_validTo.DetailFieldID = 315871
		WHERE tdv_prov.DetailFieldID = 315868
		AND tdv_prov.ValueInt = @Province
		AND tdv_cat.ValueInt = 193346
		AND (tdv_validFrom.ValueDate <= @ValidFrom OR tdv_validFrom.ValueDate IS NULL)
		AND (tdv_validTo.ValueDate > @ValidFrom OR tdv_validTo.ValueDate IS NULL)

		/*Sum the rates to get a total amount*/
		SELECT @TaxRate = SUM(tdv.ValueMoney)
		FROM TableDetailValues tdv WITH (NOLOCK)
		WHERE tdv.TableRowID = @TaxTableRowID
		AND tdv.DetailFieldID IN (315872, 315873, 315874) /*GST, PST, HST*/

		SELECT @TaxAmount = @EnrollmentFee * (@TaxRate/100.00)

	END

	INSERT INTO @EnrollmentFeeTable (FeeTableRowID, EnrollmentFee, TaxTableRowID, Tax, Total)
	VALUES (@FeeTableRowID, @EnrollmentFee, @TaxTableRowID, @TaxAmount, @EnrollmentFee + @TaxAmount)
	
	RETURN 

	/*End add endrollment fee*/

END
GO
