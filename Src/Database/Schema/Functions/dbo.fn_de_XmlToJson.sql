SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE   FUNCTION [dbo].[fn_de_XmlToJson](@XmlData xml, @arrayMatch VARCHAR(50))
RETURNS NVARCHAR(max)
AS
BEGIN
	DECLARE @json nvarchar(max)
	SELECT @json=STUFF(
		(SELECT item from
			(SELECT ','+' {'+STUFF(
				(SELECT ',"'+COALESCE(b.c.value('local-name(.)', 'NVARCHAR(255)'),'')+'":'+
					CASE 
						WHEN b.c.value('count(*)','INT')=0 
						THEN dbo.[fn_de_JsonEscape](b.c.value('text()[1]','NVARCHAR(MAX)'))
						ELSE 
							CASE WHEN @arrayMatch LIKE '%|'+b.c.value('local-name(.)', 'NVARCHAR(255)')+'|%'
								THEN '[' + dbo.fn_de_XmlToJson(b.c.query('*'),@arrayMatch) + ']'
								ELSE dbo.fn_de_XmlToJson(b.c.query('*'),@arrayMatch)
							END
					END
				FROM x.y.nodes('*') b(c)                                                                
				FOR XML PATH(''),TYPE).value('(./text())[1]','NVARCHAR(MAX)')
				,1,1,'')+'}'
			FROM @XmlData.nodes('/*') x(y)
		) JSON(item)
		FOR XML PATH(''),TYPE).value('.','NVARCHAR(MAX)')
	,1,1,'')
	RETURN @json
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_de_XmlToJson] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_de_XmlToJson] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_de_XmlToJson] TO [sp_executeall]
GO
