SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire & Gavin Reynolds
-- Create date: 2020-06-18
-- Description:	Returns the decimal for Date Part calculation
-- Mods
-- ALM & GPR 2020-08-28 SDPRU-11 Change decimal to (18,5) to fix a rounding issue
-- =============================================
CREATE FUNCTION [dbo].[fn_C605_RulesEngine_GetDatePartTrendFactor]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS DECIMAL(18,5)	
AS
BEGIN

	DECLARE @YearStart DATE,
			@ClientID INT,
			@State VARCHAR(6),
			@ChangeLogDate DATE, 
			@Output DECIMAL(18,5),
			@Zip VARCHAR(6)

	/*Get ClientID and CountryID*/
	SELECT @ClientID = ClientID FROM Customers WITH (NOLOCK) WHERE CustomerID = @CustomerID
	
	IF @ClientID IS NULL
	BEGIN
		SELECT @ClientID = [dbo].[fnGetPrimaryClientID]()
	END

	/*Year start date is always passed in*/
	SELECT @YearStart = AnyValue2
	FROM @Overrides
	WHERE AnyID = 2
	AND AnyValue1 = 'YearStart'
	/*except not always from RulesEngine test interface so default it*/
	IF @YearStart IS NULL SELECT @YearStart=dbo.fn_GetDate_Local()
	
	/*Get Postcode*/
	IF @CustomerID IS NOT NULL
	BEGIN

		SELECT @Zip = c.PostCode
		FROM Customers c WITH (NOLOCK)
		WHERE c.CustomerID = @CustomerID

		SELECT @State= StateCode
		FROM UnitedStates usa WITH (NOLOCK)
		INNER JOIN StateByZip zip WITH (NOLOCK) ON usa.StateID = zip.StateID
		WHERE zip.Zip = @Zip		
		 
	END

	IF NULLIF(@Zip,'') IS NULL /*GPR 2020-05-12*/
	BEGIN
		SELECT @Zip=AnyValue2
		FROM @Overrides
		WHERE AnyID = 2
		AND AnyValue1 = 'PostCode'

		SELECT @State= StateCode
		FROM UnitedStates usa WITH (NOLOCK)
		INNER JOIN StateByZip zip WITH (NOLOCK) ON usa.StateID = zip.StateID
		WHERE zip.Zip = @Zip
	END
	
	IF NULLIF(@State,'') IS NULL
	BEGIN
		SELECT @State=AnyValue2 FROM @Overrides WHERE AnyID = 2 AND AnyValue1 = 'State'
	END

	/*Get the ChangeLogDate by State*/
		SELECT TOP (1) @ChangeLogDate= t.TrendDate
		FROM _C605_RulesEngine_ChangeLogEffectiveDate t WITH (NOLOCK) 
		WHERE t.ClientID = @ClientID
		AND (t.[ValidFrom] IS NULL OR @YearStart >= t.[ValidFrom])
		AND (t.[ValidTo] IS NULL OR @YearStart <= t.[ValidTo])
		AND (t.State = @State)

	/*((YearPart of StartDate minus YearPart ChangeLogDate) * 12) + ((MonthPart of StartDate minus MonthPart ChangeLogDate) / 12)*/
	--SELECT @Output = ((DATEPART(YEAR,@YearStart) - DATEPART(YEAR,@ChangeLogDate)) * 12.00000) + ((DATEPART(MONTH,@YearStart) - DATEPART(MONTH,@ChangeLogDate)) / 12.00000)
	
	/*GPR / ALM 2020-12-09 for SDPRU-184*/
	--(((YearPart of StartDate minus YearPart ChangeLogDate)*12.00000) + (MonthPart of StartDate minus MonthPart ChangeLogDate)) / 12.00000
	--Example: SELECT (((2021 - 2020)*12.00000) + (1 - 1)) / 12.00000
	SELECT @Output =  (((DATEPART(YEAR,@YearStart) - DATEPART(YEAR,@ChangeLogDate))*12.00000) + (DATEPART(MONTH,@YearStart) - DATEPART(MONTH,@ChangeLogDate))) / 12.00000

	RETURN ISNULL(@Output,1.00000)
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C605_RulesEngine_GetDatePartTrendFactor] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C605_RulesEngine_GetDatePartTrendFactor] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C605_RulesEngine_GetDatePartTrendFactor] TO [sp_executeall]
GO
