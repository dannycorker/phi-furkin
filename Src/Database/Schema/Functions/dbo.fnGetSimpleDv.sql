SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-04-18
-- Description:	Selects a simple detail field from the appropriate detail value table
-- =============================================
CREATE FUNCTION [dbo].[fnGetSimpleDv]
(
	@DetailFieldID INT,
	@ObjectID INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE @DetailValue AS VARCHAR(2000),
			@LeadOrMatter INT

	SELECT @LeadOrMatter = LeadOrmatter
	FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @DetailFieldID

	IF @LeadOrMatter = 1
	BEGIN
		SELECT @DetailValue = DetailValue 
		FROM LeadDetailValues WITH (NOLOCK)
		WHERE LeadID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END

	IF @LeadOrMatter = 2
	BEGIN
		SELECT @DetailValue = DetailValue 
		FROM MatterDetailValues WITH (NOLOCK)
		WHERE MatterID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 4 /*Cs 2014-12-16*/
	BEGIN
		SELECT @DetailValue = DetailValue 
		FROM ResourceListDetailValues WITH (NOLOCK)
		WHERE ResourceListID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END

	IF @LeadOrMatter IN (6,8) /*Table AND Basic Table.  CS 2014-10-07*/
	BEGIN
		SELECT @DetailValue = DetailValue 
		FROM TableDetailValues WITH (NOLOCK)
		WHERE TableRowID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 10
	BEGIN
		SELECT @DetailValue = DetailValue 
		FROM CustomerDetailValues WITH (NOLOCK)
		WHERE CustomerID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 11
	BEGIN
		SELECT @DetailValue = DetailValue 
		FROM CaseDetailValues WITH (NOLOCK)
		WHERE CaseID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 12
	BEGIN
		SELECT @DetailValue = DetailValue 
		FROM ClientDetailValues WITH (NOLOCK)
		WHERE ClientID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 13
	BEGIN
		SELECT @DetailValue = DetailValue 
		FROM ClientPersonnelDetailValues WITH (NOLOCK)
		WHERE ClientPersonnelID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 14
	BEGIN
		SELECT @DetailValue = DetailValue 
		FROM ContactDetailValues WITH (NOLOCK)
		WHERE ContactID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END

	RETURN @DetailValue

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleDv] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetSimpleDv] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleDv] TO [sp_executeall]
GO
