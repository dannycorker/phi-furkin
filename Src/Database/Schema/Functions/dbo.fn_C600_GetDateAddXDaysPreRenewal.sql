SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2019-10-29
-- Description:	Returns a date value of dbo.fn_GetDate_Local() + Apply Prepare Renewal (X Days Pre-Renewal) or Send Renewal Notification (X Days Pre-Renewal)
-- 2020-09-07 ALM PPET-390 For United States get the days from State Variance RL
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetDateAddXDaysPreRenewal]
(
	@CustomerID INT,
	@Type INT
)
RETURNS DATE
AS
BEGIN

	DECLARE @Days INT,
			@Date DATE,
			@CountryID INT

	SELECT @CountryID = cu.CountryID
	FROM Customers cu WITH (NOLOCK) 
	WHERE cu.CustomerID = @CustomerID 
	
	IF @Type = 1 /*Apply Prepare Renewal (X Days Pre-Renewal)*/
	BEGIN

		SELECT @Days = [dbo].[fnGetSimpleRLDv](170144, 313775, @CustomerID, 0)

	END

	IF @Type = 2 /*Send Renewal Notification (X Days Pre-Renewal)*/
	BEGIN
		
		/*2020-09-07 ALM PPET-390*/
		IF @CountryID = 233
		BEGIN 

			SELECT @Days = [dbo].[fnGetSimpleRLDv](314016, 314017, @CustomerID, 0)

		END
		ELSE
		BEGIN

			SELECT @Days = [dbo].[fnGetSimpleRLDv](170144, 313776, @CustomerID, 0)

		END
	END

	SELECT @Date = DATEADD(DAY,@Days,dbo.fn_GetDate_Local())

	RETURN @Date

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetDateAddXDaysPreRenewal] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetDateAddXDaysPreRenewal] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetDateAddXDaysPreRenewal] TO [sp_executeall]
GO
