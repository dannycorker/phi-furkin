SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-05-08
-- Description:	Get MatterDV directly #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetMatterDvCASE]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE @DetailValue AS VARCHAR(2000)

	SELECT @DetailValue = mdv.DetailValue 
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND mdv.DetailFieldID = @DetailFieldID
	WHERE m.CaseID = @CaseID

	RETURN @DetailValue

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetMatterDvCASE] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetMatterDvCASE] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetMatterDvCASE] TO [sp_executeall]
GO
