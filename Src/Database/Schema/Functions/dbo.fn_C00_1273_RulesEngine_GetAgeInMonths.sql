SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-11-12
-- Description:	Returns the age of a pet at the terms date from the override data passed in or looked up
-- Mods
--				DCM Default start date for situation where we are called by the rules engine test interface
-- 2016-04-06	SB	Fix for Mike Bennet issue which will be applied based on the passed in start date in the future
-- 2017-11-07	CPS for #46338.  Account for partial months.
-- 2018-05-04   GPR altered on C600
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_RulesEngine_GetAgeInMonths]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS INT	
AS
BEGIN
 
	DECLARE @AgeInMonths INT,
			@BirthDate DATE,
			@YearStart DATE
	
	-- Birth date will be passed in as an override for quotes	
	SELECT @BirthDate = AnyValue2
	FROM @Overrides
	WHERE AnyID = 1
	AND AnyValue1 = '144274' /*Pet Date of Birth*/
	
	-- For existing policies we need to look this up
	IF @BirthDate IS NULL
	BEGIN
	
		SELECT @BirthDate = ValueDate
		FROM dbo.LeadDetailValues WITH (NOLOCK) 
		WHERE LeadID = @LeadID
		AND DetailFieldID = 144274
	
	END
	
	-- Year start date is always passed in
	SELECT @YearStart = AnyValue2
	FROM @Overrides
	WHERE AnyID = 2
	AND AnyValue1 = 'YearStart'

	-- default start date
	IF @YearStart IS NULL SELECT @YearStart=dbo.fn_GetDate_Local()

	SELECT @AgeInMonths =	CASE 
								WHEN DATEPART(DAY, @BirthDate) > DATEPART(DAY, @YearStart)
								THEN DATEDIFF(MONTH, @BirthDate, @YearStart) - 1
								ELSE DATEDIFF(MONTH, @BirthDate, @YearStart)
							END
	
	RETURN @AgeInMonths
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_GetAgeInMonths] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_RulesEngine_GetAgeInMonths] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_GetAgeInMonths] TO [sp_executeall]
GO
