SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ================================================================================================================
-- Author:		Gavin	
-- Create date: 2020-09-25
-- Description:	Returns Discount XML for [vCheckPointHelper]
-- =================================================================================================================

CREATE FUNCTION [dbo].[fn_C605_GetDiscountsXML] 
(
	@MatterID INT,
	@PremiumCalcID INT 
)
RETURNS XML
AS
BEGIN

DECLARE @XML XML

;WITH Discounts (DiscountCode, DiscountPercentage) AS (

	SELECT DISTINCT (UPPER(LEFT(qpcDiscount.CheckpointName, 3))), CAST((qpcDiscount.Value * 100.00)AS INT)
	FROM WrittenPremium wp WITH ( NOLOCK ) 
	INNER JOIN QuotePetProduct qp WITH (NOLOCK) on  qp.QuotePetProductID = wp.QuotePetProductID
	INNER JOIN QuotePetProductCheckpoint qpcDiscount WITH (NOLOCK) ON qp.QuotePetProductID = qpcDiscount.QuotePetProductID and qpcDiscount.CheckpointName IN ('Animal Shelter', 'Corporate Group Benefit', 'Medical Services', 'Multiple Pet', 'Strategic Partner', 'Military', 'Vet or Staff', 'Wellness', 'Paid in Full', 'Internet Partner')
	AND wp.MatterID = @MatterID
	AND wp.PremiumCalculationID = @PremiumCalcID
	AND qpcDiscount.Value > '0.00'
)


/*
<Discounts>
    <Discount>
		<DiscountCode></DiscountCode>
		<DiscountPercent></DiscountPercent>
	</Discount>
</Discounts>
*/

SELECT @XML =	(
					SELECT DiscountCode, DiscountPercentage
					FROM Discounts
					FOR XML PATH ('Discounts')
                )

RETURN @XML

END


GO
