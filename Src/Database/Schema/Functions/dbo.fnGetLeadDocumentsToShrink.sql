SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-04
-- Description:	LeadDocument Shrinking list. Short term measure.
-- =============================================
CREATE FUNCTION [dbo].[fnGetLeadDocumentsToShrink]  
(
	@ClientID int
)
RETURNS 
@IDList TABLE 
(
	LeadDocumentID int, 
	DocumentBlobNeedsConversion varchar(30), 
	EmailBlobNeedsConversion varchar(30)
)
AS
BEGIN

	/* 
		The outer select speeds things up massively. If all the CASE/Substring/Varbinary conversions 
		appear in the WHERE clause of the inner select, the database gets tied up in knots for hours.
	*/
	INSERT INTO @IDList (LeadDocumentID, DocumentBlobNeedsConversion, EmailBlobNeedsConversion)
	SELECT 
	ld.[LeadDocumentID],
	CASE 
		WHEN convert(varbinary(16), ld.[DocumentBLOB]) = 0x THEN 'EMPTY' 
		WHEN substring(convert(varbinary(16), ld.[DocumentBLOB]), 2, 3) = 0x000000 THEN 'UTF32' 
		ELSE '' 
	END as [DocumentBlobNeedsConversion],

	CASE 
		WHEN convert(varbinary(16), ld.[EmailBLOB]) = 0x THEN 'EMPTY' 
		WHEN substring(convert(varbinary(16), ld.[EmailBLOB]), 2, 3) = 0x000000 THEN 'UTF32' 
		ELSE '' 
	END as [EmailBLOBNeedsConversion]
	FROM dbo.LeadDocument ld (nolock)
	WHERE ld.clientid = @ClientID 
	AND ld.documentformat IN ('HTML', 'NOTE', 'RTF') 
	AND ld.Encoding IS NULL 

	/* 
		The table variable speeds things up massively. If all the CASE/Substring/Varbinary conversions 
		appear in the WHERE clause of the first select, the database gets tied up in knots for hours.
		Even an inner/outer select combo doesn't work.
	*/
	DELETE @IDList 
	WHERE ([DocumentBlobNeedsConversion] <> 'UTF32' AND [EmailBLOBNeedsConversion] <> 'UTF32')


	RETURN 
	
END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLeadDocumentsToShrink] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnGetLeadDocumentsToShrink] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLeadDocumentsToShrink] TO [sp_executeall]
GO
