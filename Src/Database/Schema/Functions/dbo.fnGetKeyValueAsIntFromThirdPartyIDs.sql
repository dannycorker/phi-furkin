SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-11-16
-- Description:	Get Key Value as an integer from third party key & system ID
--              Assumes key is unique within third party system and ignores FieldMappingID
--				Returns the LULI ID or DetailValue as directed 
-- =============================================
CREATE FUNCTION [dbo].[fnGetKeyValueAsIntFromThirdPartyIDs]
(
	@ClientID INT,
	@ThirdPartySystemID INT,
	@ThirdPartyKey VARCHAR(50),
	@ReturnLULI BIT = 1
)
RETURNS INT
AS
BEGIN

	DECLARE @KeyValue INT

	SELECT TOP 1 @KeyValue = CASE WHEN @ReturnLULI=1 THEN fmvk.LookupListItemID ELSE 
								CASE WHEN dbo.fnIsInt(fmvk.DetailValue) = 1 THEN CAST(fmvk.DetailValue AS INT) ELSE NULL END
							END
	FROM ThirdPartyFieldMappingValueKey fmvk WITH (NOLOCK)
	INNER JOIN ThirdPartyFieldMapping fm WITH (NOLOCK) ON fmvk.FieldMappingID=fm.ThirdPartyFieldMappingID
	INNER JOIN ThirdPartyField f WITH (NOLOCK) ON fm.ThirdPartyFieldID=f.ThirdPartyFieldID
	WHERE fmvk.ClientID = @ClientID
	AND fmvk.ThirdPartyKey = @ThirdPartyKey
	AND f.ThirdPartySystemId=@ThirdPartySystemID

	RETURN @KeyValue

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetKeyValueAsIntFromThirdPartyIDs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetKeyValueAsIntFromThirdPartyIDs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetKeyValueAsIntFromThirdPartyIDs] TO [sp_executeall]
GO
