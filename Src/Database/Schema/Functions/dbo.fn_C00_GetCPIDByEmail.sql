SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-09-17
-- Description:	Get clientpersonnelid by email address
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_GetCPIDByEmail]
(
	@ClientID INT,
	@EmailAddress VARCHAR(200)
)
RETURNS INT
AS
BEGIN

	DECLARE @ClientPersonnelID INT

	SELECT @ClientPersonnelID = cp.ClientPersonnelID
	FROM ClientPersonnel cp WITH (NOLOCK)
	WHERE cp.ClientID = @ClientID
	AND cp.EmailAddress = @EmailAddress

	RETURN @ClientPersonnelID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetCPIDByEmail] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_GetCPIDByEmail] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetCPIDByEmail] TO [sp_executeall]
GO
