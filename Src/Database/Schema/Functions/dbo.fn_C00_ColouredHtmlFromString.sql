SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- ALTER date:	2020-02-13
-- Description:	Take a string name and return HTML to show it in colour
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ColouredHtmlFromString]
(
	 @HexColourID	 INT
	,@StringToColour VARCHAR(2000)
)
RETURNS VARCHAR(2000)
AS
BEGIN
	
	DECLARE @ReturnValue VARCHAR(2000)
	
	SELECT @ReturnValue = '<p style="color:' + hc.HexColourCode + '";>'
						+ @StringToColour
						+ '</p>'
	FROM HexColour hc WITH ( NOLOCK ) 
	WHERE hc.HexColourID = @HexColourID
		
	RETURN @ReturnValue

END










GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ColouredHtmlFromString] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ColouredHtmlFromString] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ColouredHtmlFromString] TO [sp_executeall]
GO
