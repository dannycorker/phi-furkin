SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds / Alexandra Maguire
-- Create date: 2021-03-11
-- Description:	Calculate ProjectedCancellationDate
-- =============================================
CREATE FUNCTION [dbo].[fn_C607_CalculatedProjectedCancellationDate] 
(
	@MatterID INT
)
RETURNS DATE
AS
BEGIN			

	
	DECLARE	@PurchasedProductID INT,
			@DateToUse DATE,
			@ProjectedCancellationDate DATE
						

	SELECT @PurchasedProductID = pp.PurchasedProductID
	FROM PurchasedProduct pp WITH (NOLOCK)
	WHERE pp.ObjectID = @MatterID
	AND pp.ValidTo >= dbo.fn_GetDate_Local()
	AND pp.ValidFrom <= dbo.fn_GetDate_Local()

	/*@DateToUse = 30 Days from first failure*/
	SELECT @DateToUse = (SELECT TOP(1) ActualCollectionDate FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
							WHERE ppps.PurchasedProductID = @PurchasedProductID
							AND ppps.PaymentStatusID = 4
							AND EXISTS (SELECT * FROM PurchasedProductPaymentSchedule ppps1 WITH (NOLOCK)
										WHERE ppps1.PurchasedProductID = ppps.PurchasedProductID
										AND ppps.PurchasedProductPaymentScheduleID = ppps1.PurchasedProductPaymentScheduleParentID
										AND ppps1.PaymentStatusID = 4)
							ORDER BY 1 DESC)

	SELECT @ProjectedCancellationDate = DATEADD(DAY,30,@DateToUse)

	-- Return the result of the function
	RETURN @ProjectedCancellationDate

END
GO
