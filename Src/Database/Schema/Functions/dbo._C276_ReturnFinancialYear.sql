SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-12-03
-- Description:	Return Financial Year For Date
-- =============================================
CREATE FUNCTION [dbo].[_C276_ReturnFinancialYear]
(
@Date DATE
)
RETURNS VARCHAR(200)
AS
BEGIN

	DECLARE @Output VARCHAR(200)

	SELECT @Output = dbo.fnGetSeasonFromDate ( @Date, 4, 1)

	RETURN @Output

END




GO
GRANT VIEW DEFINITION ON  [dbo].[_C276_ReturnFinancialYear] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C276_ReturnFinancialYear] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C276_ReturnFinancialYear] TO [sp_executeall]
GO
