SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ================================================================================================================
-- Author:		Gavin Reynolds
-- Create date: 2020-09-24
-- Description:	Returns separated policy sections XML for [vCheckPointHelper]
-- =================================================================================================================

CREATE FUNCTION [dbo].[fn_C605_GetPolicySectionXML] 
(
	@MatterID INT
)
RETURNS XML
AS
BEGIN

DECLARE @XML XML

SELECT @XML =	(


				--lliSection.ItemValue, lliSubSection.ItemValue, ROW_NUMBER() OVER(PARTITION BY lliSection.ItemValue, lliSubSection.ItemValue ORDER BY r.TableRowID)
				SELECT	 ISNULL(liSubSection.ItemValue,liSection.ItemValue)
				+' ' + COALESCE(tdvNonFinancialLimit.DetailValue + ' ' + liLimitType.ItemValue
						 ,CASE WHEN cl.CountryID in (14,233) THEN '$' ELSE '£' END + tdvSum.DetailValue--CONVERT(VARCHAR,CAST( tdvSum.ValueMoney as MONEY),1)
						 ,'')
				FROM TableRows tr WITH ( NOLOCK ) 
				INNER JOIN TableDetailValues tdvSection WITH ( NOLOCK ) on tdvSection.TableRowID = tr.TableRowID AND tdvSection.DetailFieldID = 144357 /*Policy Section*/
				INNER JOIN ResourceListDetailValues rdvSection WITH ( NOLOCK ) on rdvSection.ResourceListID = tdvSection.ResourceListID AND rdvSection.DetailFieldID = 146189 /*Policy Section*/
				INNER JOIN LookupListItems liSection WITH ( NOLOCK ) on liSection.LookupListItemID = rdvSection.ValueInt
				 LEFT JOIN ResourceListDetailValues rdvSubSection WITH ( NOLOCK ) on rdvSubSection.ResourceListID = tdvSection.ResourceListID AND rdvSubSection.DetailFieldID = 146190 /*Sub-Section*/
				 LEFT JOIN LookupListItems liSubSection WITH ( NOLOCK ) on liSubSection.LookupListItemID = rdvSubSection.ValueInt AND liSubSection.ItemValue <> '-'
				 LEFT JOIN TableDetailValues tdvSum WITH ( NOLOCK ) on tdvSum.TableRowID = tr.TableRowID AND tdvSum.DetailFieldID = 144358 /*Sum Insured*/
				 LEFT JOIN TableDetailValues tdvNonFinancialLimit WITH ( NOLOCK ) on tdvNonFinancialLimit.TableRowID = tr.TableRowID AND tdvNonFinancialLimit.DetailFieldID = 144267 /*Non financial limit*/
				 LEFT JOIN TableDetailValues tdvNonFinancialLimitType WITH ( NOLOCK ) on tdvNonFinancialLimitType.TableRowID = tr.TableRowID AND tdvNonFinancialLimitType.DetailFieldID = 175388 /*Custom Limit Type*/
				 LEFT JOIN LookupListItems liLimitType WITH ( NOLOCK ) on liLimitType.LookupListItemID = tdvNonFinancialLimitType.ValueInt
				INNER JOIN Clients cl WITH (NOLOCK) on tr.ClientID = cl.ClientID /*find country for localisation of currency*/
				WHERE  tr.DetailFieldID = 145692 /*Policy Limits*/
				AND tr.MatterID = @MatterID

					FOR XML PATH ('Coverages')
                )

		
RETURN @XML

RETURN @XML

END


GO
