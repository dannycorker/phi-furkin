SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-10-17
-- Description:	Take Pet XML and break out the underwriting questions
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_PetXml_UnderwritingQuestions]
(
	@PetXml XML
)
RETURNS 
@Table TABLE 
(
	 QuestionID		INT
	,AnswerID		INT
	,AnswerText		VARCHAR(2000)
	,QuestionText	VARCHAR(2000)
)
AS
BEGIN

	INSERT @Table ( QuestionID, AnswerID, AnswerText, QuestionText )
	SELECT	 b.value('(QuestionId)[1]','INT')			[QuestionID]
			,b.value('(AnswerId)[1]','INT')				[AnswerID]
			,b.value('(AnswerText)[1]','VARCHAR(2000)')	[Comments]
			,rdv.DetailValue							[QuestionText]
	FROM @PetXml.nodes('(//UnderwritingList/*)') a(b)
	LEFT JOIN ResourceListDetailValues rdv WITH ( NOLOCK ) on rdv.ResourceListID = b.value('(QuestionId)[1]','INT') 
																AND rdv.DetailFieldID = 177125 /*Endorsement TandC Paragraph*/
	
	RETURN
	
END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_PetXml_UnderwritingQuestions] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_PetXml_UnderwritingQuestions] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_PetXml_UnderwritingQuestions] TO [sp_executeall]
GO
