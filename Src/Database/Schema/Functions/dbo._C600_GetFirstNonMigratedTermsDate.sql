SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds (GPR)
-- Create date: 2018-10-31
-- Description:	Returns first Non-MigratedTerm Terms Date from Historical Policy TableRow
-- =============================================
CREATE FUNCTION [dbo].[_C600_GetFirstNonMigratedTermsDate]
(
	@MatterID INT
)
RETURNS Date
AS
BEGIN
	
	DECLARE @TermsDate DATE

	SELECT @TermsDate = (SELECT TOP(1) tdv.ValueDate FROM TableRows tr WITH ( NOLOCK )
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) ON tdv.TableRowID = tr.TableRowID
	LEFT JOIN TableDetailValues tdv1 WITH ( NOLOCK ) ON tdv1.TableRowID = tr.TableRowID AND tdv1.DetailFieldID = 180216 
	WHERE tr.DetailFieldID = 170033
	AND tdv.DetailFieldID = 170092
	AND ISNULL(tdv1.ValueInt,0) <> 1
	AND tr.MatterID = @MatterID
	ORDER BY tdv.ValueDate ASC)

	RETURN @TermsDate

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetFirstNonMigratedTermsDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_GetFirstNonMigratedTermsDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_GetFirstNonMigratedTermsDate] TO [sp_executeall]
GO
