SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-08-08
-- Description:	Return a corresponding columns value from a table
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ReturnLastCorrespondingTableColumn]
(
	@MatchFieldID INT,
	@MatchValue VARCHAR(2000),
	@ReturnColumn INT,
	@ObjectID INT,
	@DetailFieldSubTypeID INT /*LeadOrMatter*/

)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE @ReturnValue VARCHAR(2000)

	/*Select the corresponding value from a resource*/
	SELECT TOP (1) @ReturnValue = tdv_return.DetailValue
	FROM TableDetailValues tdv WITH (NOLOCK)
	INNER JOIN TableDetailValues tdv_return WITH (NOLOCK) ON tdv_return.TableRowID = tdv.TableRowID AND tdv_return.DetailFieldID = @ReturnColumn
	WHERE tdv.DetailFieldID = @MatchFieldID 
	AND tdv.DetailValue = @MatchValue
	and (
		(@DetailFieldSubTypeID = 1  AND tdv.LeadID				= @ObjectID)
	OR	(@DetailFieldSubTypeID = 2  AND tdv.MatterID			= @ObjectID)
	OR	(@DetailFieldSubTypeID = 11 AND tdv.CaseID				= @ObjectID)
	OR	(@DetailFieldSubTypeID = 10 AND tdv.CustomerID			= @ObjectID)
	OR	(@DetailFieldSubTypeID = 13 AND tdv.ClientPersonnelID	= @ObjectID)
	OR	(@DetailFieldSubTypeID = 12 AND tdv.CLientID			= @ObjectID)
		)
	ORDER BY tdv.TableRowID DESC

	RETURN @ReturnValue

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReturnLastCorrespondingTableColumn] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ReturnLastCorrespondingTableColumn] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReturnLastCorrespondingTableColumn] TO [sp_executeall]
GO
