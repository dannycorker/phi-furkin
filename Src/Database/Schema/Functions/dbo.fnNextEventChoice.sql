SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author:		Jim Green
-- Create date: 2007-12-10
-- Description:	For a given EventType, find the next Event to add (could be multiple choice).
-- =============================================
CREATE FUNCTION [dbo].[fnNextEventChoice] 
(
	-- Add the parameters for the function here
	@eventtypeid int
)
RETURNS varchar(50)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @NextEventCount int, @Description varchar(50)

	-- Add the T-SQL statements to compute the return value here
	SELECT @NextEventCount = COUNT(*) 
	FROM EventChoice 
	WHERE EventTypeID = @eventtypeid 
	AND EscalationEvent = 0

	IF @NextEventCount > 1
	BEGIN 

		-- If there are more events in the event choice than there are
		-- next events on thread one on its own then the case is Multi Threaded
		-- therefore do not try to get next event descriptions.
		If ((select Count(*) from eventchoice where eventtypeid = @eventtypeid) <> (select count(*) From eventchoice where eventtypeid = @eventtypeid and ThreadNumber = 1))
		Begin

			Select @Description = '(Multiple Threads)'

		End

		IF @Description IS NULL --OR @Description = ''
		BEGIN 
			SELECT TOP 1 @Description = Description 
			FROM EventChoice 
			WHERE EventTypeID = @eventtypeid 
			AND EscalationEvent = 0
			AND RTRIM(Description) <> ''
		End

		IF @Description IS NULL --OR @Description = ''
		BEGIN 
			SET @Description = '(Multiple Choices)'
		END
	END
	ELSE
	BEGIN
		IF @NextEventCount = 0
		BEGIN 
			SET @Description = ''
		END
		ELSE
		BEGIN

			-- Set Description to nothing so it's not null ready 
			-- for concatonation to one of the the next porrible events
			Select @Description = ''

			-- Old Code
			/*SELECT @Description = Description 
			FROM EventChoice 
			WHERE EventTypeID = @eventtypeid 
			AND EscalationEvent = 0*/

			-- New Code, concatonate all of the possible next events 
			-- for that choice seperated by 'or' but not inserting an or at the end
			SELECT @Description = et2.EventTypeName +  Case When @Description <> '' Then ' or ' Else '' END + @Description
			FROM EventType et
			Inner Join EventChoice ec On ec.EventTypeID = et.EventTypeID
			Inner Join EventType et2 on et2.EventTypeID = ec.NextEventTypeID
			WHERE et.EventTypeID = @eventtypeid 
			
		END
	END

	-- Return the result of the function
	RETURN @Description

END













GO
GRANT VIEW DEFINITION ON  [dbo].[fnNextEventChoice] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnNextEventChoice] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnNextEventChoice] TO [sp_executeall]
GO
