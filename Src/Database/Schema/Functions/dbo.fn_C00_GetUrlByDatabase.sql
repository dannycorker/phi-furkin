SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2013-09-10
-- Description:	Get the URL of the live system according to the current database
--				To use in helper procs
-- 2017-05-02 CPS Added C433
-- 2017-12-21 CPS added C600
-- 2018-05-09 GPR changed https to http for C600 Test / Training
-- 2018-05-25 GPR added C600 PreProd URL mapping
-- 2018-06-19 UAH changed http to https for C600 Test / Training
-- 2018-06-25 GPR added URL for C600 Core, and removed non-C600 URLs
-- 2019-10-03 CPS for JIRA LPC-14 | Added Aquarius603 URLs
-- 2019-10-17 GPR replaced with call to [fn_C600_GetDatabaseSpecificConfigValue]
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_GetUrlByDatabase]
(
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE @Database	varchar(2000),
			@URL		varchar(2000)

	SELECT @Database = DB_NAME()

	SELECT @URL = [dbo].[fn_C600_GetDatabaseSpecificConfigValue]('SiteURL')

	RETURN @URL

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetUrlByDatabase] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_GetUrlByDatabase] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetUrlByDatabase] TO [sp_executeall]
GO
