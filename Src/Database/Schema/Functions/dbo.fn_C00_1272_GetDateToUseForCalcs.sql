SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-10-08
-- Description:	Returns the correct date to use for excess calcs
-- MODS JEL 2019-02-18 Added logic for OMF for client 600. Override to treatment start date if date of loss is in OMF period 
--		GPR 2019-05-01 Added call to get @PolicyLeadID for Claim. Used within OMF logic. Defect #1664
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetDateToUseForCalcs]
(
	@ClaimMatterID INT,
	@DateOfLoss DATE,
	@TreatmentStart DATE
)
RETURNS DATE	
AS
BEGIN

	DECLARE @PolicyMatterID INT,
	@ClientID INT, 
	@PolicyLeadID INT,
	@ClaimLeadID INT 

	
	/*GPR 2019-05-01 Added call to get @PolicyLeadID for Claim. Used within OMF logic to get value of DF 180019. Defect #1664*/
	SELECT @ClaimLeadID = LeadID FROM Matter m WITH (NOLOCK) WHERE m.MatterID = @ClaimMatterID
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimLead(@ClaimLeadID)

	SELECT @ClientID = m.ClientID FROM Matter m with (NOLOCK) where m.MatterID = @ClaimMatterID
	SELECT @PolicyMatterID = dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(@ClaimMatterID)

	IF @DateOfLoss IS NULL OR @TreatmentStart IS NULL
	BEGIN

		SELECT @TreatmentStart = mdvTreatmentStart.ValueDate, @DateOfLoss = mdvDateOfLoss.ValueDate
		FROM dbo.Matter m WITH (NOLOCK) 
		INNER JOIN dbo.MatterDetailValues mdvDateOfLoss WITH (NOLOCK) ON m.MatterID = mdvDateOfLoss.MatterID AND mdvDateOfLoss.DetailFieldID = 144892 --DateOfLoss
		LEFT JOIN dbo.MatterDetailValues mdvTreatmentStart WITH (NOLOCK) ON m.MatterID = mdvTreatmentStart.MatterID AND mdvTreatmentStart.DetailFieldID = 144366 --TreatmentStart
		WHERE m.MatterID = @ClaimMatterID
		
		IF @TreatmentStart IS NULL
		-- Look on the claim rows (common scenario for SAP imported claims)
		BEGIN
		
			SELECT @TreatmentStart = MIN(tdvTreatmentStart.ValueDate)
			FROM dbo.Matter m WITH (NOLOCK)
			INNER JOIN dbo.TableRows r WITH (NOLOCK) ON r.MatterID = m.MatterID AND r.DetailFieldID = 144355 --ClaimRowData
			INNER JOIN dbo.TableDetailValues tdvTreatmentStart WITH (NOLOCK) ON tdvTreatmentStart.TableRowID = r.TableRowID AND tdvTreatmentStart.DetailFieldID = 144349 --TreatmentStart
			WHERE m.MatterID = @ClaimMatterID

		END
	
	END

	
	DECLARE @DateToUse DATE,
			@UpgradeDowngrade INT = 0,
			@SchemeType INT,
			@Affinity VARCHAR(50),
			@ClaimType INT
			
	SELECT @UpgradeDowngrade = dbo.fn_C00_1272_CheckUpgradeDowngrade(@ClaimMatterID,@DateOfLoss,@TreatmentStart)
	
	-- Get Policy Type from the Claim by condition onset
	SELECT @SchemeType = dbo.[fn_C00_1272_GetPolicyTypeFromClaim](@ClaimMatterID)
	
		
	-- Pick the right date to use
	IF (@SchemeType = 42933) -- Reinstatement Policy
		OR
	   (@UpgradeDowngrade = -1) -- Policy has been downgraded since condition onset
	BEGIN
		SELECT @DateToUse = @TreatmentStart
	END
	ELSE
	BEGIN
		SELECT @DateToUse = @DateOfLoss
	END
	/*If this policy is an OMF linked policy, use the treatment start as date of loss could be in the OMF period*/ 
	IF dbo.fnGetPrimaryClientID() = 600 and dbo.fnGetSimpledv(180019,@PolicyLeadID) = 'true'
	BEGIN 
	
		SELECT @DateToUse = CASE WHEN @DateOfLoss BETWEEN inception.ValueDate and omfend.ValueDate THEN @TreatmentStart ELSE @DateOfLoss END
		FROM MatterDetailValues omfID with (NOLOCK) 
		INNER JOIN MatterDetailValues inception with (NOLOCK) on inception.MatterID = omfID.ValueINT and inception.DetailFieldID = 170035
		INNER JOIN MatterdetailValues omfend with (NOLOCK) on omfend.MatterID = omfID.ValueINT and omfend.DetailFieldID = 170037
		WHERE omfID.DetailFieldID = 180224
		AND omfID.MatterID = @PolicyMatterID

		/*check if the date of loss is in omf period, if so use the treatmeant start, else use the date of loss*/ 

	END
	--
	-- And return it
	RETURN @DateToUse

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetDateToUseForCalcs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_GetDateToUseForCalcs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetDateToUseForCalcs] TO [sp_executeall]
GO
