SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green, Alex Elger
-- Create date: 2016-04-06
-- Description:	Decode Ref letter (eg MatterRefLetter or encoded ClientID) back to an int
-- =============================================
CREATE FUNCTION [dbo].[fnGetIntFromRefLetter] 
(
	@RefLetter VARCHAR(3)
)
RETURNS INT
AS
BEGIN
	DECLARE @val1 INT = 0, @val2 INT = 0, @val3 INT = 0, @Result INT = 0
	
	/* Keeping the code simple!               */
	/* Always set @val1 to the rightmost char */
	/* ASCII values for A-Z are 65-90         */
	/* Subtract 64 to turn A=1, Z=26          */
	IF LEN(@RefLetter) = 3
	BEGIN
		SELECT @val1 = ASCII(SUBSTRING(@RefLetter, 3, 1)) - 64
		SELECT @val2 = ASCII(SUBSTRING(@RefLetter, 2, 1)) - 64
		SELECT @val3 = ASCII(SUBSTRING(@RefLetter, 1, 1)) - 64
	END
	IF LEN(@RefLetter) = 2
	BEGIN
		SELECT @val1 = ASCII(SUBSTRING(@RefLetter, 2, 1)) - 64
		SELECT @val2 = ASCII(SUBSTRING(@RefLetter, 1, 1)) - 64
	END
	IF LEN(@RefLetter) = 1
	BEGIN
		SELECT @val1 = ASCII(SUBSTRING(@RefLetter, 1, 1)) - 64
	END
	
	/* Now use base26 arithmetic to calculate the total */
	/* */
	SELECT @Result = (@val3 * 26 * 26) + (@val2 * 26) + @val1 
	
	RETURN @Result
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetIntFromRefLetter] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetIntFromRefLetter] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetIntFromRefLetter] TO [sp_executeall]
GO
