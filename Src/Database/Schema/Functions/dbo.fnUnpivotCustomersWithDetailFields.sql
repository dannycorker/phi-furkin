SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2012-04-04
-- Description:	Unpivot the columns in the Customers table
-- and gives each column a sudo detailfieldid 
-- =============================================
CREATE FUNCTION [dbo].[fnUnpivotCustomersWithDetailFields] 
(	
	@ClientID int,
	@SubClientID int
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT CustomerID, ColumnName DetailFieldID, RealValue DetailValue
	FROM
	(
		SELECT
			CustomerID, 
			COALESCE(CONVERT(varchar(255),t.Title), '') [-100001], 
			COALESCE(CONVERT(varchar(255),FirstName), '') [-100002], 
			COALESCE(CONVERT(varchar(255),MiddleName), '') [-100003], 
			COALESCE(CONVERT(varchar(255),LastName), '') [-100004], 
			COALESCE(CONVERT(varchar(255),EmailAddress), '') [-100005], 
			COALESCE(CONVERT(varchar(255),HomeTelephone), '') [-100006], 
			COALESCE(CONVERT(varchar(255),MobileTelephone), '') [-100007],
			COALESCE(CONVERT(varchar(255),Address1), '') [-100008], 
			COALESCE(CONVERT(varchar(255),Address2), '') [-100009], 
			COALESCE(CONVERT(varchar(255),Town), '') [-100010], 
			COALESCE(CONVERT(varchar(255),County), '') [-100011], 
			COALESCE(CONVERT(varchar(255),PostCode), '') [-100012], 
			COALESCE(CONVERT(varchar(255),Fullname), '') [-100013],
			COALESCE(CONVERT(varchar(255),CONVERT(char(10), DateOfBirth, 126)), '') [-100014]
		from	Customers c WITH (NOLOCK)
		left join Titles t WITH (NOLOCK) on c.TitleID = t.TitleID
		where	ClientID = @ClientID
		and		SubClientID = @SubClientID
	) pvt
	UNPIVOT
	(
		RealValue FOR ColumnName IN
		(
			[-100001],
			[-100002], 
			[-100003], 
			[-100004], 
			[-100005], 
			[-100006], 
			[-100007], 
			[-100008],
			[-100009], 
			[-100010], 
			[-100011], 
			[-100012], 
			[-100013], 
			[-100014]
		)
	) as unpvt
)








GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotCustomersWithDetailFields] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnUnpivotCustomersWithDetailFields] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotCustomersWithDetailFields] TO [sp_executeall]
GO
