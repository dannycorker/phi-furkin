SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2016-02-16
-- Description:	Take an IntVarchar TVP and concatenate all values, grouped by ID
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ConcatenateAndGroupIntVarcharList]
(	
	 @List		dbo.tvpIntVarchar READONLY
	,@Delimiter VARCHAR(10)
)
RETURNS @Concatenated TABLE 
(
	 AnyID			INT
	,Concatenated	VARCHAR(2000)
)
AS
BEGIN 

	DECLARE @RawData TABLE ( Unid INT IDENTITY, AnyID INT, AnyValue VARCHAR(2000), RowNum INT )
	INSERT @RawData ( AnyID, AnyValue )
	SELECT l.AnyID, l.AnyValue
	FROM @List l 

	/*Number and ID each item in the original list*/
	;WITH Numbered AS
	(
	SELECT rd.Unid, ROW_NUMBER() OVER (PARTITION BY rd.AnyID ORDER BY rd.Unid) [Row]
	FROM @RawData rd
	)
	UPDATE rd
	SET RowNum = n.Row
	FROM @RawData rd
	INNER JOIN Numbered n on n.Unid = rd.Unid

	/*Use a Recursive CTE to append each entry in turn, comma separated*/	
	;WITH RecursiveConcatenate AS
	(
	SELECT st.AnyID, st.AnyValue, CAST(st.AnyValue + @Delimiter as VARCHAR(2000)) [Concatenated], st.RowNum
	FROM @RawData st
		UNION ALL
	SELECT rc.AnyID, rc.AnyValue, CAST(rc.Concatenated + rd.AnyValue + @Delimiter as VARCHAR(2000)), rc.RowNum + 1
	FROM RecursiveConcatenate rc 
	INNER JOIN @RawData rd on rd.AnyID = rc.AnyID AND rd.RowNum = rc.RowNum + 1
	)
	/*Find the latest entry for each ID in the original list*/
	, Numbered AS
	(
	SELECT rn.AnyID, rn.Concatenated, ROW_NUMBER() OVER (PARTITION BY rn.AnyID ORDER BY LEN(rn.Concatenated) desc) [Row], LEN(rn.Concatenated) [Ln]
	FROM RecursiveConcatenate rn
	)
	/*Populate the output table*/
	INSERT @Concatenated ( AnyID, Concatenated )
	SELECT n.AnyID, LEFT(n.Concatenated, n.Ln-1) [Values]
	FROM Numbered n 
	WHERE n.Row = 1
	
	RETURN

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ConcatenateAndGroupIntVarcharList] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_ConcatenateAndGroupIntVarcharList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ConcatenateAndGroupIntVarcharList] TO [sp_executeall]
GO
