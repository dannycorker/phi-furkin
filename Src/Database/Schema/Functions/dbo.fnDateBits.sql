SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-10-10
-- Description:	Function to being back day of week etc
-- =============================================
CREATE FUNCTION [dbo].[fnDateBits]
(
@FunctionName varchar(10),
@DateToUse date,
@Expression varchar(10)
)
RETURNS VARCHAR(20)
AS
BEGIN

	DECLARE @Output varchar(20)

	IF @FunctionName = 'datename'
	BEGIN
	
		SELECT @Output = CASE @Expression 
			WHEN 'dw' THEN DATENAME(dw,@DateToUse)
			WHEN 'wk' THEN DATENAME(wk,@DateToUse)
			END
	
	END
	
	RETURN @OutPut


END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnDateBits] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnDateBits] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnDateBits] TO [sp_executeall]
GO
