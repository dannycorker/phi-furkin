SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-08-21
-- Description:	Returns the current scheme of a certain type
-- DCM 2014-09-15 Copied from 235
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetCurrentScheme]
(
	@SchemeID INT,
	@Now DATE = NULL
)
RETURNS INT	
AS
BEGIN
 
	IF @Now IS NULL
	BEGIN
		SELECT @Now = dbo.fnDateOnly(dbo.fn_GetDate_Local())
	END
	
	DECLARE @CurrentSchemeMatterID INT
 
	-- Match this scheme against the correct matter on the schemes lead type
	SELECT @CurrentSchemeMatterID = mdvScheme.MatterID 
	FROM  dbo.Matter m WITH (NOLOCK) 
	INNER JOIN dbo.MatterDetailValues mdvScheme WITH (NOLOCK) ON m.MatterID = mdvScheme.MatterID AND mdvScheme.DetailFieldID IN (145689) AND mdvScheme.ValueInt = @SchemeID
	INNER JOIN dbo.MatterDetailValues mdvStart WITH (NOLOCK) ON m.MatterID = mdvStart.MatterID AND mdvStart.DetailFieldID IN (145690) AND mdvStart.ValueDate <= @Now
	INNER JOIN dbo.MatterDetailValues mdvEnd WITH (NOLOCK) ON m.MatterID = mdvEnd.MatterID AND mdvEnd.DetailFieldID IN (145691) AND (mdvEnd.ValueDate >= @Now OR mdvEnd.ValueDate IS NULL) 
	
	RETURN @CurrentSchemeMatterID
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetCurrentScheme] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetCurrentScheme] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetCurrentScheme] TO [sp_executeall]
GO
