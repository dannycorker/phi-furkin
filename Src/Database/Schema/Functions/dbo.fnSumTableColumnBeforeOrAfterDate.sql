SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-04-06
-- Description:	Sum values from a table field, based on a date field in the same table
-- =============================================
CREATE FUNCTION [dbo].[fnSumTableColumnBeforeOrAfterDate]
(
	@ObjectID					INT,
	@TableFieldToSUM			INT,
	@ValueColumnToSUM			INT,
	@DateColumnToMatch			INT,
	@DateDetailFieldID			INT,
	@BeforeOrAfter				INT,
	@Inclusive					BIT
)
RETURNS NUMERIC(18,2)
AS
BEGIN

	DECLARE @SummedValue NUMERIC(18,2)

	DECLARE @DetailFieldSubTypeID INT, @Date Date
		
	SELECT @DetailFieldSubTypeID = df.LeadOrMatter
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @TableFieldToSUM
	
	IF @DetailFieldSubTypeID = 1
	BEGIN
		SELECT @Date = ldv.ValueDate FROM LeadDetailValues ldv WITH (NOLOCK) WHERE ldv.DetailFieldID = @DateDetailFieldID and ldv.LeadID = @ObjectID
	END
	ELSE IF @DetailFieldSubTypeID = 2
	BEGIN
		SELECT @Date = mdv.ValueDate FROM MatterDetailValues mdv WITH (NOLOCK) WHERE mdv.DetailFieldID = @DateDetailFieldID and mdv.MatterID = @ObjectID
	END
	ELSE IF @DetailFieldSubTypeID = 10
	BEGIN
		SELECT @Date = cdv.ValueDate FROM CustomerDetailValues cdv WITH (NOLOCK) WHERE cdv.DetailFieldID = @DateDetailFieldID and cdv.CustomerID = @ObjectID
	END		
	ELSE IF @DetailFieldSubTypeID = 11
	BEGIN
		SELECT @Date = cdv.ValueDate FROM CaseDetailValues cdv WITH (NOLOCK) WHERE cdv.DetailFieldID = @DateDetailFieldID and cdv.CaseID = @ObjectID
	END
	ELSE IF @DetailFieldSubTypeID = 12
	BEGIN
		SELECT @Date = cdv.ValueDate FROM ClientDetailValues cdv WITH (NOLOCK) WHERE cdv.DetailFieldID = @DateDetailFieldID and cdv.ClientID = @ObjectID
	END

	SELECT @SummedValue = ISNULL(SUM(tdv_val.ValueMoney),0.00)
	FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN TableDetailValues tdv_dat WITH (NOLOCK) ON	tdv_dat.TableRowID = tr.TableRowID AND tdv_dat.DetailFieldID = @DateColumnToMatch
	INNER JOIN TableDetailValues tdv_val WITH (NOLOCK) ON	tdv_val.TableRowID = tr.TableRowID AND tdv_val.DetailFieldID = @ValueColumnToSUM
	WHERE tr.DetailFieldID = @TableFieldToSUM
	AND
		(
			( @DetailFieldSubTypeID = 1  AND tr.LeadID	   = @ObjectID )
		  OR( @DetailFieldSubTypeID = 2  AND tr.MatterID   = @ObjectID )
		  OR( @DetailFieldSubTypeID = 10 AND tr.CustomerID = @ObjectID )
		  OR( @DetailFieldSubTypeID = 11 AND tr.CaseID	   = @ObjectID )
		  OR( @DetailFieldSubTypeID = 12 AND tr.ClientID   = @ObjectID )
		)
	AND
		(
			( @BeforeOrAfter = 0 AND @Inclusive = 1 AND tdv_dat.ValueDate <= @Date )
		  OR( @BeforeOrAfter = 0 AND @Inclusive = 0 AND tdv_dat.ValueDate <  @Date )
		  OR( @BeforeOrAfter = 1 AND @Inclusive = 1 AND tdv_dat.ValueDate >= @Date )
		  OR( @BeforeOrAfter = 1 AND @Inclusive = 0 AND tdv_dat.ValueDate >  @Date )
		)

	RETURN ISNULL(@SummedValue, 0.00)

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnSumTableColumnBeforeOrAfterDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnSumTableColumnBeforeOrAfterDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnSumTableColumnBeforeOrAfterDate] TO [sp_executeall]
GO
