SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Unpivot the columns in the Contact table 
-- =============================================
CREATE FUNCTION [dbo].[fnUnpivotContact] 
(	
	@ContactID int
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT ColumnName as [RawColumnName], '[!' + ColumnName + ']' as [FormattedColumnName], RealValue
	FROM
	(
		SELECT 
		COALESCE(CONVERT(varchar(255), c.Address1), '') as [Address1],
		COALESCE(CONVERT(varchar(255), c.Address2), '') as [Address2],
		COALESCE(CONVERT(varchar(255), c.ClientID), '') as [ClientID],
		COALESCE(CONVERT(varchar(255), c.ContactID), '') as [ContactID],
		COALESCE(CONVERT(varchar(255), c.Country), '') as [Country],
		COALESCE(CONVERT(varchar(255), c.CountryID), '') as [CountryID],
		COALESCE(CONVERT(varchar(255), c.County), '') as [County],
		COALESCE(CONVERT(varchar(255), c.CustomerID), '') as [CustomerID],
		COALESCE(CONVERT(varchar(255), c.DepartmentID), '') as [DepartmentID],
		COALESCE(CONVERT(varchar(255), c.DirectDial), '') as [DirectDial],
		COALESCE(CONVERT(varchar(255), c.EmailAddressOther), '') as [EmailAddressOther],
		COALESCE(CONVERT(varchar(255), c.EmailAddressWork), '') as [EmailAddressWork],
		COALESCE(CONVERT(varchar(255), c.Firstname), '') as [Firstname],
		COALESCE(CONVERT(varchar(255), c.Fullname), '') as [Fullname],
		COALESCE(CONVERT(varchar(255), c.JobTitle), '') as [JobTitle],
		COALESCE(CONVERT(varchar(255), c.LanguageID), '') as [LanguageID],
		COALESCE(CONVERT(varchar(255), c.Lastname), '') as [Lastname],
		COALESCE(CONVERT(varchar(255), c.Middlename), '') as [Middlename],
		COALESCE(CONVERT(varchar(255), c.MobilePhoneOther), '') as [MobilePhoneOther],
		COALESCE(CONVERT(varchar(255), c.MobilePhoneWork), '') as [MobilePhoneWork],
		COALESCE(CONVERT(varchar(255), c.Notes), '') as [Notes],
		COALESCE(CONVERT(varchar(255), c.OfficeID), '') as [OfficeID],
		COALESCE(CONVERT(varchar(255), c.Postcode), '') as [Postcode],
		COALESCE(CONVERT(varchar(255), c.TitleID), '') as [TitleID],
		COALESCE(CONVERT(varchar(255), c.Town), '') as [Town]
		FROM dbo.Contact c WITH (NOLOCK) 
		WHERE c.ContactID = @ContactID) p
	UNPIVOT
	(
		RealValue FOR ColumnName IN
		(
			[Address1],[Address2],[ClientID],[ContactID],[Country],[CountryID],[County],[CustomerID],[DepartmentID],[DirectDial],[EmailAddressOther],[EmailAddressWork],[Firstname],[Fullname],[JobTitle],[LanguageID],[Lastname],[Middlename],[MobilePhoneOther],[MobilePhoneWork],[Notes],[OfficeID],[Postcode],[TitleID],[Town]
		)
	) as unpvt

)





GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotContact] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnUnpivotContact] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotContact] TO [sp_executeall]
GO
