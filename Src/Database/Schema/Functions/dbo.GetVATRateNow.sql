SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2011-01-05
-- Description:	Get the default commercial VAT Rate today.
-- =============================================
CREATE FUNCTION [dbo].[GetVATRateNow] ()
RETURNS money
AS
BEGIN
	DECLARE @Rate money = 20.00
	
	RETURN @Rate
END








GO
GRANT VIEW DEFINITION ON  [dbo].[GetVATRateNow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GetVATRateNow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GetVATRateNow] TO [sp_executeall]
GO
