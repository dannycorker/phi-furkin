SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2020-11-18
-- Description:	Find the municipal tax label based on the customer's zip code
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetMunicipalTaxLabelForCustomer]
(
	@CustomerID   INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

    DECLARE @MunicipalTaxLabel VARCHAR(2000) = ''

	SELECT @MunicipalTaxLabel = tax.TaxName + ISNULL(' (' + NULLIF(c.County,'') + ')','')
	FROM CustomerDetailValues cdv WITH (NOLOCK) 
	INNER JOIN dbo.Customers cu WITH (NOLOCK) on cu.CustomerID = cdv.CustomerID
	INNER JOIN dbo.ResourceListDetailValues rdv WITH (NOLOCK) on rdv.ResourceListID = cdv.ValueInt AND rdv.DetailFieldID = 313958 /*State ID*/
	INNER JOIN dbo.USTaxByState tax WITH (NOLOCK) on tax.StateID = rdv.ValueInt AND tax.IsMunicipal = 1
	 LEFT JOIN dbo.USTaxCityandCounty c	WITH (NOLOCK) on CONVERT(VARCHAR,c.Zip) = cu.PostCode
	WHERE cdv.CustomerID = @CustomerID
	AND cdv.DetailFieldID = 314016 /*State Variance*/

	RETURN @MunicipalTaxLabel

END

GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetMunicipalTaxLabelForCustomer] TO [sp_executeall]
GO
