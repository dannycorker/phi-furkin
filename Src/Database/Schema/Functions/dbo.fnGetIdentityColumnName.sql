SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2013-01-09
-- Description:	Get the IDENTITY column for a table
-- =============================================
CREATE FUNCTION [dbo].[fnGetIdentityColumnName]
(
	@TableName VARCHAR(1024)
)
RETURNS VARCHAR(1024)
AS
BEGIN
	
	/*
		Pass in a table name to get back the name of the IDENTITY Key column (if there is one).
		Returns NULL otherwise.
	*/
	DECLARE @IdentityName VARCHAR(1024)
	
	/* Get the name of the IDENTITY column if there is one */
	SELECT @IdentityName = ic.[name] 
	FROM sys.identity_columns ic 
	WHERE ic.[object_id] = OBJECT_ID(@TableName) 
	
	/* Return the colum list */
	RETURN @IdentityName

END






GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetIdentityColumnName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetIdentityColumnName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetIdentityColumnName] TO [sp_executeall]
GO
