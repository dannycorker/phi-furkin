SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-07-03
-- Description:	Function to return a token from a string containing separators
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_GetTokenFromString] 
(
@String VARCHAR(500), -- string to be searched
@TokenNo INT,         -- sequence number of token to be returned 
@Separator VARCHAR(5) -- separator being used to split string into tokens
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	

DECLARE @StringLen INT,
		@TokenCount INT = 0,
		@Token VARCHAR(100)
		
	SELECT @StringLen=LEN(@String)

	IF LEFT(@String,LEN(@Separator)) = @Separator 
		SELECT @String = RIGHT(@String,@StringLen-LEN(@Separator))

	IF RIGHT(@String,LEN(@Separator)) <> @Separator 
		SELECT @String = @String + @Separator

	SELECT @StringLen=LEN(@String)

	WHILE @StringLen > 0 AND @TokenCount < @TokenNo
	BEGIN

		SELECT @TokenCount=@TokenCount+1
		
		SELECT @Token=LEFT(@String,CHARINDEX(@Separator,@String)-1), 
				@String=RIGHT(@String,@StringLen-(CHARINDEX(@Separator,@String)+LEN(@Separator)-1))
		
		SELECT @StringLen=LEN(@String)
		
	END	

	IF @TokenCount<>@TokenNo
		SELECT @Token=''

	RETURN @Token

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetTokenFromString] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_GetTokenFromString] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetTokenFromString] TO [sp_executeall]
GO
