SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-08-01
-- Description:	Get First or Last Working Day of month
-- =============================================
CREATE FUNCTION [dbo].[fnGetStartorEndOfMonthWorkingDays]
(
@Year int,
@Month int,
@ReturnEnd int = 0
)
RETURNS datetime
AS
BEGIN
	-- Declare the return variable here

	Declare @ReturnDate datetime

	IF @ReturnEnd = 0
	BEGIN
	
		SELECT @ReturnDate = convert(varchar,min(wd.Date),121)
		FROM dbo.WorkingDays wd WITH (NOLOCK) 
		WHERE wd.Month = @Month
		and wd.Year = @Year
		and IsWorkday = 1
		
	END

	IF @ReturnEnd = 1
	BEGIN
	
		SELECT @ReturnDate = convert(varchar,max(wd.Date),121)
		FROM dbo.WorkingDays wd WITH (NOLOCK) 
		WHERE wd.Month = @Month
		and wd.Year = @Year
		and IsWorkday = 1
	
	END

	Return @ReturnDate

END









GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetStartorEndOfMonthWorkingDays] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetStartorEndOfMonthWorkingDays] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetStartorEndOfMonthWorkingDays] TO [sp_executeall]
GO
