SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2008-10-15
-- Description:	Return the larger of 2 numbers (money)
-- JWG 2010-02-24 Handle Nulls gracefully
-- =============================================
CREATE FUNCTION [dbo].[fnMaxOf] 
(
	@Number1 money,
	@Number2 money
)
RETURNS money
AS
BEGIN
	DECLARE @BiggerNumber money
	
	IF @Number1 IS NULL
	BEGIN
		RETURN @Number2
	END
	
	IF @Number2 IS NULL
	BEGIN
		RETURN @Number1
	END
	
	SELECT @BiggerNumber = CASE WHEN @Number1 > @Number2 THEN @Number1 ELSE @Number2 END

	RETURN @BiggerNumber

END









GO
GRANT VIEW DEFINITION ON  [dbo].[fnMaxOf] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnMaxOf] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnMaxOf] TO [sp_executeall]
GO
