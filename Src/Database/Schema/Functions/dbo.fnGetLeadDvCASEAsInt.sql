SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-06-12
-- Description:	Get LeadDV directly #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetLeadDvCASEAsInt]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS int
AS
BEGIN

	DECLARE @ValueInt AS int

	SELECT @ValueInt = ldv.ValueInt 
	FROM Cases c WITH (NOLOCK) 
	INNER JOIN LeadDetailValues ldv WITH (NOLOCK) ON c.LeadID = ldv.LeadID AND ldv.DetailFieldID = @DetailFieldID
	WHERE c.CaseID = @CaseID 
	
	RETURN @ValueInt

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLeadDvCASEAsInt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetLeadDvCASEAsInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLeadDvCASEAsInt] TO [sp_executeall]
GO
