SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Unpivot the columns in the Clients table 
-- =============================================
CREATE FUNCTION [dbo].[fnUnpivotClients] 
(	
	@ClientID int
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT ColumnName as [RawColumnName], '[!' + ColumnName + ']' as [FormattedColumnName], RealValue
	FROM
	(
		SELECT 
		COALESCE(CONVERT(varchar(255), c.AddLeadByQuestionnaire), '') as [AddLeadByQuestionnaire],
		COALESCE(CONVERT(varchar(255), c.AllowSMS), '') as [AllowSMS],
		COALESCE(CONVERT(varchar(255), c.ClientID), '') as [ClientID],
		COALESCE(CONVERT(varchar(255), c.ClientTypeID), '') as [ClientTypeID],
		COALESCE(CONVERT(varchar(255), c.CompanyName), '') as [CompanyName],
		COALESCE(CONVERT(varchar(255), c.CountryID), '') as [CountryID],
		COALESCE(CONVERT(varchar(255), c.DefaultEmailAddress), '') as [DefaultEmailAddress],
		COALESCE(CONVERT(varchar(255), c.FollowupWorkingDaysOnly), '') as [FollowupWorkingDaysOnly],
		COALESCE(CONVERT(varchar(255), c.IPAddress), '') as [IPAddress],
		COALESCE(CONVERT(varchar(255), c.LanguageID), '') as [LanguageID],
		COALESCE(CONVERT(varchar(255), c.LeadsBelongToOffices), '') as [LeadsBelongToOffices],
		COALESCE(CONVERT(varchar(255), c.SecurityCode), '') as [SecurityCode],
		COALESCE(CONVERT(varchar(255), c.UseCreditCalculation), '') as [UseCreditCalculation],
		COALESCE(CONVERT(varchar(255), c.UseEventComments), '') as [UseEventComments],
		COALESCE(CONVERT(varchar(255), c.UseEventCosts), '') as [UseEventCosts],
		COALESCE(CONVERT(varchar(255), c.UseEventDisbursements), '') as [UseEventDisbursements],
		COALESCE(CONVERT(varchar(255), c.UseEventUOEs), '') as [UseEventUOEs],
		COALESCE(CONVERT(varchar(255), c.UseGBAddress), '') as [UseGBAddress],
		COALESCE(CONVERT(varchar(255), c.UseIncendia), '') as [UseIncendia],
		COALESCE(CONVERT(varchar(255), c.UsePinpoint), '') as [UsePinpoint],
		COALESCE(CONVERT(varchar(255), c.UseRPI), '') as [UseRPI],
		COALESCE(CONVERT(varchar(255), c.UseSage), '') as [UseSage],
		COALESCE(CONVERT(varchar(255), c.UseSAS), '') as [UseSAS],
		COALESCE(CONVERT(varchar(255), c.UseTapi), '') as [UseTapi],
		COALESCE(CONVERT(varchar(255), c.VerifyAddress), '') as [VerifyAddress],
		COALESCE(CONVERT(varchar(255), c.WebAddress), '') as [WebAddress]
		FROM dbo.Clients c WITH (NOLOCK) 
		WHERE c.ClientID = @ClientID) p
	UNPIVOT
	(
		RealValue FOR ColumnName IN
		(
			[AddLeadByQuestionnaire],[AllowSMS],[ClientID],[ClientTypeID],[CompanyName],[CountryID],[DefaultEmailAddress],[FollowupWorkingDaysOnly],[IPAddress],[LanguageID],[LeadsBelongToOffices],[SecurityCode],[UseCreditCalculation],[UseEventComments],[UseEventCosts],[UseEventDisbursements],[UseEventUOEs],[UseGBAddress],[UseIncendia],[UsePinpoint],[UseRPI],[UseSage],[UseSAS],[UseTapi],[VerifyAddress],[WebAddress]
		)
	) as unpvt

)





GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotClients] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnUnpivotClients] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotClients] TO [sp_executeall]
GO
