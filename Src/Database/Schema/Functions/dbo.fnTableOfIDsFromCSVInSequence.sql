SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2008-01-23
-- Description:	Return a table of ints from an input string in the order presented 
-- =============================================
CREATE FUNCTION [dbo].[fnTableOfIDsFromCSVInSequence]
(
	@instr nvarchar(max)
)
RETURNS 
@TableOut TABLE 
(
	AnyID int,
	Sequence INT IDENTITY (1,1)

)
AS
BEGIN

	DECLARE @delimiter nchar(1)
	DECLARE @testid    nvarchar(10)
	DECLARE @position  int

	SET @delimiter = ','
	SET @instr = LTRIM(RTRIM(@instr)) + ','
	SET @position = CHARINDEX(@delimiter, @instr, 1)

	IF REPLACE(@instr, @delimiter, '') <> ''
	BEGIN
		WHILE @position > 0
		BEGIN
			SET @testid = LTRIM(RTRIM(LEFT(@instr, @position - 1)))
			IF @testid <> '' AND isnumeric(@testid) = 1
			BEGIN
				INSERT INTO @TableOut (anyid) 
				VALUES (convert(int, @testid))
			END
			SET @instr = RIGHT(@instr, LEN(@instr) - @position)
			SET @position = CHARINDEX(@delimiter, @instr, 1)

		END
	END	
	RETURN
END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfIDsFromCSVInSequence] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnTableOfIDsFromCSVInSequence] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfIDsFromCSVInSequence] TO [sp_executeall]
GO
