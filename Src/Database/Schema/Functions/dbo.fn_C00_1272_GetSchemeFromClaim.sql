SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-02-07
-- Description:	Returns the correct scheme for a customer's claim
-- Modified to use dates from Claim Rows if not found on Matter (2013-01-03 Robin Hall)
-- Modified to use more recent policy if TreatmentStart hits 2 policy rows (renewal date) (2013-03-26 Robin Hall) 
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetSchemeFromClaim]
(
	@ClaimMatterID INT
)
RETURNS INT	
AS
BEGIN
	
	DECLARE @SchemeID INT,
			@TreatmentStart DATE
	
	-- Look up the treatment date
	SELECT @TreatmentStart = mdv.ValueDate 
	FROM dbo.Matter m WITH (NOLOCK) 
	INNER JOIN dbo.MatterDetailValues mdv WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND mdv.DetailFieldID = 144366
	WHERE m.MatterID = @ClaimMatterID

	-- If the TreatmentStart date is null on the matter use the earliest date from the claim rows.
	-- This is a likely scenario for claims imported from SAP	
	SELECT @TreatmentStart = ISNULL(@TreatmentStart,
		(SELECT MIN(tdv.ValueDate)
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = r.TableRowID AND tdv.DetailFieldID = 144349
		WHERE r.MatterID = @ClaimMatterID AND r.DetailFieldID = 144355))
		
	-- Now find the matching scheme
	SELECT TOP 1 @SchemeID = tdvScheme.ResourceListID
	FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@ClaimMatterID) r
	INNER JOIN dbo.TableDetailValues tdvScheme WITH (NOLOCK) ON r.TableRowID = tdvScheme.TableRowID AND tdvScheme.DetailFieldID = 145665
	INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663
	INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664
	WHERE @TreatmentStart >= tdvStart.ValueDate
	AND @TreatmentStart <= tdvEnd.ValueDate
	ORDER BY tdvStart.ValueDate DESC

	RETURN @SchemeID



END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetSchemeFromClaim] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_GetSchemeFromClaim] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetSchemeFromClaim] TO [sp_executeall]
GO
