SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-12-07
-- Description:	Returns the sales tax from the province code
-- DCM 2014-09-15 Copied from 235
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetSalesTaxFromProvinceCode]
(
	@ProvinceCode CHAR(2),
	@EffectiveDate DATE
)
RETURNS DECIMAL(18, 2)	
AS
BEGIN
	
	DECLARE @SalesTax DECIMAL(18, 2) = 0
	
	-- DCM 2014-09-15  no concept of sales tax as yet, never mind relating it to province 
	--DECLARE @ProvinceID INT = 0
	--SELECT @ProvinceID = dbo.fn_C0_GetProvinceIdFromCode(@ProvinceCode)
 
	--SELECT TOP 1 @SalesTax = rdvTax.ValueMoney
	--FROM dbo.ResourceList r WITH (NOLOCK) 
	--INNER JOIN dbo.ResourceListDetailValues rdvProvince WITH (NOLOCK) ON r.ResourceListID = rdvProvince.ResourceListID AND rdvProvince.DetailFieldID = 159962
	--INNER JOIN dbo.ResourceListDetailValues rdvTax WITH (NOLOCK) ON r.ResourceListID = rdvTax.ResourceListID AND rdvTax.DetailFieldID = 159963
	--LEFT JOIN dbo.ResourceListDetailValues rdvFrom WITH (NOLOCK) ON r.ResourceListID = rdvFrom.ResourceListID AND rdvFrom.DetailFieldID = 163136
	--WHERE rdvProvince.ValueInt = @ProvinceID
	--AND (rdvFrom.ValueDate IS NULL OR @EffectiveDate >= rdvFrom.ValueDate)
	--ORDER BY rdvFrom.ValueDate DESC
	
	RETURN ISNULL(@SalesTax, 0)
 
END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetSalesTaxFromProvinceCode] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetSalesTaxFromProvinceCode] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetSalesTaxFromProvinceCode] TO [sp_executeall]
GO
