SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-02-26
-- Description:	Calls the CLR GmtToRegional but allows you to specify the timezone ID rather than the string
-- =============================================
CREATE FUNCTION [dbo].[fnGmtToRegional] 
(
	@Gmt DATETIME,
	@TimezoneID INT
)
RETURNS DATETIME
AS
BEGIN

	DECLARE @Regional DATETIME,
			@TimeZoneKey VARCHAR(200)
	
	SELECT @TimeZoneKey = TimeZoneKey
	FROM dbo.TimeZone WITH (NOLOCK) 
	WHERE TimeZoneID = @TimeZoneID
	
	SELECT @Regional = dbo.GmtToRegional(@Gmt, @TimeZoneKey)
	
	RETURN @Regional
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnGmtToRegional] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGmtToRegional] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGmtToRegional] TO [sp_executeall]
GO
