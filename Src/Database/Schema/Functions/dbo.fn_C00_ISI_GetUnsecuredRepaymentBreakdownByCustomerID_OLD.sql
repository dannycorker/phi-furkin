SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2013-11-22
-- Description:	For ISI Clients, calculate live values for repayment breakdown display table
--				Third party mapped, using field group 26
--				To be used in TableRows__GetBy.... procs
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerID_OLD] 
(	
	@CustomerID INT
)
RETURNS
@TableOut TABLE 
(
	TableRowID INT,
	Creditor VARCHAR(2000),
	Percentage DECIMAL(18,2),
	[Year 1] DECIMAL(18,2),
	[Year 2] DECIMAL(18,2),
	[Year 3] DECIMAL(18,2),
	[Year 4] DECIMAL(18,2),
	[Year 5] DECIMAL(18,2),
	[Year 6] DECIMAL(18,2),
	[Year 7] DECIMAL(18,2),
	Totals	DECIMAL(18,2),
	[Return for creditors] VARCHAR(2000),
	DenyEdit VARCHAR(5),
	DenyDelete VARCHAR(5)
)
AS
BEGIN
	
	DECLARE	@ClientID						INT,
			@UnsecuredLenderNameFieldID		INT,
			@PercentageOfDebtFieldID		INT,
			@AmountOwedFieldID				INT,
			@Year1FieldID					INT,
			@Year2FieldID					INT,
			@Year3FieldID					INT,
			@Year4FieldID					INT,
			@Year5FieldID					INT,
			@Year6FieldID					INT,
			@Year7FieldID					INT,
			@UnsecuredCreditorTableFieldID	INT,
			@Term							INT = 6

		SELECT @ClientID = cu.ClientID
		FROM Customers cu WITH (NOLOCK) 
		WHERE cu.CustomerID = @CustomerID

		DECLARE @Total NUMERIC(18,2),
				@Years INT,
				@ValueNumeric Numeric(18,2),
				@PrefValueNumeric Numeric(18,2)
				
		DECLARE @AddedRows TABLE (FromTableRowID INT, ToTableRowID INT)
		
		DECLARE @Creditors TABLE (TableRowID INT, Name VARCHAR(2000), Amount Numeric(18,2), PreferentialValue Numeric(18,2), Percentage numeric(18,2), Year1 Numeric(18,2), Year2 Numeric(18,2), Year3 Numeric(18,2), Year4 Numeric(18,2), Year5 Numeric(18,2), Year6 Numeric(18,2), Year7 Numeric(18,2))
		

	/*Clients outside of this range use the old system of Payment breakdown..*/
	IF (@ClientID IN (267,311) OR @ClientID > 311)
	BEGIN
	
		/*
			[14:34:12] Stephen Ainsworth: 1. Get unsecured creditors where they are not excluded
			[14:34:37] Stephen Ainsworth: 2. Look at drop down list and determine whether its for 6 or 7 years
			[14:34:50] Stephen Ainsworth: 3. Insert creditors
			[14:35:10] Stephen Ainsworth: 4. Get debt level of added creditors
			[14:35:18] Stephen Ainsworth: 5. calculate percentage of debt level for each
			[14:35:32] Stephen Ainsworth: 6. use equations above table to find value for each year
			[14:35:53] Stephen Ainsworth: 7. calculate the amount to each creditor against the total amount for the year against % of debt
			[14:36:03] Stephen Ainsworth: 8. add amount into each creditor for each respective year
			[14:36:09] Stephen Ainsworth: 9. total each row at the end
			[14:36:26] Stephen Ainsworth: 10. work out the return to each creditor (debt level less amount paid as a % return)
		*/
		
		--DECLARE @Total NUMERIC(18,2),
		--		@Years INT,
		--		@ValueNumeric Numeric(18,2)
				
		--DECLARE @AddedRows TABLE (FromTableRowID INT, ToTableRowID INT)
		
		--DECLARE @Creditors TABLE (TableRowID INT, Name VARCHAR(2000), Amount Numeric(18,2), Percentage numeric(18,2), Year1 Numeric(18,2), Year2 Numeric(18,2), Year3 Numeric(18,2), Year4 Numeric(18,2), Year5 Numeric(18,2), Year6 Numeric(18,2), Year7 Numeric(18,2))
		
		INSERT INTO @Creditors (TableRowID, Name, Amount, PreferentialValue)
		SELECT tr.TableRowID, 
			--tdv_secured.DetailValue as [Secured], 
			ISNULL(li.ItemValue,tdv_org.DetailValue) as [Name], 
			tdv_amt.ValueMoney as [Amount],
			0.00  [PreferentialValue]
		FROM ThirdPartyFieldMapping tpm_f WITH (NOLOCK)
		INNER JOIN TableRows tr WITH (NOLOCK) ON tr.DetailFieldID = tpm_f.DetailFieldID
		CROSS APPLY ThirdPartyFieldMapping tpm_sec WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv_secured WITH (NOLOCK) ON tdv_secured.TableRowID = tr.TableRowID and tdv_secured.DetailFieldID = tpm_sec.DetailFieldID AND tdv_secured.ValueInt = 5145
		CROSS APPLY ThirdPartyFieldMapping tpm_org WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv_org WITH (NOLOCK) ON tdv_org.TableRowID = tr.TableRowID and tdv_org.DetailFieldID = tpm_org.DetailFieldID
		CROSS APPLY ThirdPartyFieldMapping tpm_amt WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv_amt WITH (NOLOCK) ON tdv_amt.TableRowID = tr.TableRowID and tdv_amt.DetailFieldID = tpm_amt.DetailFieldID
		CROSS APPLY ThirdPartyFieldMapping tpm_exclude WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv_exclude WITH (NOLOCK) ON tdv_exclude.TableRowID = tr.TableRowID and tdv_exclude.DetailFieldID = tpm_exclude.DetailFieldID
		LEFT JOIN LookupListItems li WITH ( NOLOCK ) on li.ClientID = @ClientID AND li.LookupListItemID = tdv_org.ValueInt
		WHERE tpm_f.ThirdPartyFieldID = 848
		AND tpm_org.ThirdPartyFieldID = 850
		AND tpm_sec.ThirdPartyFieldID = 849 
		AND tpm_amt.ThirdPartyFieldID = 851
		AND tpm_exclude.ThirdPartyFieldID = 855
		AND tpm_f.ClientID = @ClientID
		AND tpm_org.ClientID = @ClientID
		AND tpm_sec.ClientID = @ClientID
		AND tpm_amt.ClientID = @ClientID
		AND tpm_exclude.ClientID = @ClientID
		AND ( tdv_exclude.ValueInt = 0 or tdv_exclude.ValueInt is null )
		AND tr.CustomerID = @CustomerID

		/*Identify preferential debt values*/
		UPDATE cr 
		SET PreferentialValue = tdv.ValueMoney
		FROM @Creditors cr 
		INNER JOIN TableRows tr WITH (NOLOCK) on tr.TableRowID = cr.TableRowID
		INNER JOIN ThirdPartyFieldMapping fm on fm.ClientID = @ClientID AND fm.ThirdPartyFieldID = 1343 /*Creditor Preferrential Debt Value*/
		INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID

		/*Split out preferential debt records*/
		INSERT @Creditors (TableRowID, Name, Amount, PreferentialValue)
		SELECT c.TableRowID, c.Name, 0.00, c.PreferentialValue
		FROM @Creditors c 
		WHERE c.PreferentialValue > 0.00

		/*Zero the preferential amount.  Reduce the actual*/
		UPDATE c 
		SET PreferentialValue = 0.00, Amount = Amount - PreferentialValue
		FROM @Creditors c 
		WHERE c.PreferentialValue > 0.00
		AND c.Amount > 0.00

		SELECT @Total = SUM(c.Amount)
		FROM @Creditors c
		WHERE c.Amount > 0.00
		
		UPDATE c
		SET Percentage = CASE WHEN c.PreferentialValue > 0.00 THEN 100.00 ELSE CONVERT(Numeric(18,2),((Amount/@Total)*100)) END 
		FROM @Creditors c
		WHERE @Total <> 0

		SELECT @Years = luli.ValueInt
		FROM CustomerDetailValues cdv WITH (NOLOCK)
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = cdv.ValueInt
		INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 733 AND tpm.ThirdPartyFieldGroupID = 62
		WHERE cdv.CustomerID = @CustomerID

		IF EXISTS	( 
					SELECT * 
					FROM Lead l WITH ( NOLOCK ) 
					INNER JOIN LeadEvent le WITH (NOLOCK) on le.EventDeleted = 0 and le.LeadID = l.LeadID
					INNER JOIN ThirdPartySystemEvent se WITH ( NOLOCK ) on se.EventTypeID = le.EventTypeID
					WHERE l.CustomerID = @CustomerID
					AND se.Name = 'Fast Track'
					)
		BEGIN
			SELECT @Years = - 1 /*Accelerated Cases*/
		END

		/*Work out the amount per year to allocate to preferential debts*/
		SELECT @PrefValueNumeric = SUM(c.PreferentialValue) / @Years 
		FROM @Creditors c 
		WHERE c.PreferentialValue > 0.00
		AND @Years <> 0
		
		SELECT @PrefValueNumeric = ISNULL(@PrefValueNumeric,0.00) /*Added by CS 2014-11-03*/

		/*Year 1*/
		SELECT @ValueNumeric = cdv.ValueMoney-@PrefValueNumeric
		FROM CustomerDetailValues cdv WITH (NOLOCK)
		INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 657 AND tpm.ThirdPartyFieldGroupID = 26
		WHERE cdv.CustomerID = @CustomerID

		UPDATE c
		SET Year1 = CASE WHEN c.PreferentialValue > 0.00 THEN CAST( c.PreferentialValue / @Years as DECIMAL(18,2) ) ELSE @ValueNumeric * (Percentage/100) END 
		FROM @Creditors c 
		WHERE @Years <> 0

		/*Year 2*/
		SELECT @ValueNumeric = cdv.ValueMoney-@PrefValueNumeric
		From CustomerDetailValues cdv WITH (NOLOCK)
		INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 658 AND tpm.ThirdPartyFieldGroupID = 26
		WHERE cdv.CustomerID = @CustomerID

		UPDATE c
		SET Year2 = CASE WHEN c.PreferentialValue > 0.00 THEN CAST( c.PreferentialValue / @Years as DECIMAL(18,2) ) ELSE @ValueNumeric * (Percentage/100) END 
		FROM @Creditors c 
		WHERE @Years <> 0

		/*Year 3*/
		SELECT @ValueNumeric = cdv.ValueMoney-@PrefValueNumeric
		From CustomerDetailValues cdv WITH (NOLOCK)
		INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 659 AND tpm.ThirdPartyFieldGroupID = 26
		WHERE cdv.CustomerID = @CustomerID

		UPDATE c
		SET Year3 = CASE WHEN c.PreferentialValue > 0.00 THEN CAST( c.PreferentialValue / @Years as DECIMAL(18,2) ) ELSE @ValueNumeric * (Percentage/100) END 
		FROM @Creditors c
		WHERE @Years <> 0

		/*Year 4*/
		SELECT @ValueNumeric = cdv.ValueMoney-@PrefValueNumeric
		From CustomerDetailValues cdv WITH (NOLOCK)
		INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 660 AND tpm.ThirdPartyFieldGroupID = 26
		WHERE cdv.CustomerID = @CustomerID

		UPDATE c
		SET Year4 = CASE WHEN c.PreferentialValue > 0.00 THEN CAST( c.PreferentialValue / @Years as DECIMAL(18,2) ) ELSE @ValueNumeric * (Percentage/100) END 
		FROM @Creditors c
		WHERE @Years <> 0

		/*Year 5*/
		SELECT @ValueNumeric = cdv.ValueMoney-@PrefValueNumeric
		From CustomerDetailValues cdv WITH (NOLOCK)
		INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 661 AND tpm.ThirdPartyFieldGroupID = 26
		WHERE cdv.CustomerID = @CustomerID
		--AND cdv.DetailFieldID = 162670

		UPDATE c
		SET Year5 = CASE WHEN c.PreferentialValue > 0.00 THEN CAST( c.PreferentialValue / @Years as DECIMAL(18,2) ) ELSE @ValueNumeric * (Percentage/100) END 
		FROM @Creditors c
		WHERE @Years <> 0

		IF @Years >= 6
		BEGIN
			/*Year 6*/
			SELECT @ValueNumeric = cdv.ValueMoney-@PrefValueNumeric
			From CustomerDetailValues cdv WITH (NOLOCK)
			INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 662 AND tpm.ThirdPartyFieldGroupID = 26
			WHERE cdv.CustomerID = @CustomerID

			UPDATE c
			SET Year6 = CASE WHEN c.PreferentialValue > 0.00 THEN CAST( c.PreferentialValue / @Years as DECIMAL(18,2) ) ELSE @ValueNumeric * (Percentage/100) END 
			FROM @Creditors c
 		END
		IF @Years >= 7
		BEGIN
			/*Year 7*/
			SELECT @ValueNumeric = cdv.ValueMoney-@PrefValueNumeric
			From CustomerDetailValues cdv WITH (NOLOCK)
			INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 826 AND tpm.ThirdPartyFieldGroupID = 26
			WHERE cdv.CustomerID = @CustomerID

			UPDATE c
			SET Year7 = CASE WHEN c.PreferentialValue > 0.00 THEN CAST( c.PreferentialValue / @Years as DECIMAL(18,2) ) ELSE @ValueNumeric * (Percentage/100) END 
			FROM @Creditors c
		END
		
		IF @Years = -1	/*2014-11-18.  Year -1 = Accelerated Cases.  Single payment, Year 1 only*/
		BEGIN
			/*Year 7*/
			SELECT @ValueNumeric = CAST( cdv.ValueMoney / ( SELECT count(*) FROM @Creditors cr ) as DECIMAL(18,2) )
			From CustomerDetailValues cdv WITH (NOLOCK)
			INNER JOIN ThirdPartyFieldMapping tpm WITH (NOLOCK) ON tpm.DetailFieldID = cdv.DetailFieldID AND tpm.ThirdPartyFieldID = 1404 AND tpm.ThirdPartyFieldGroupID = 26
			WHERE cdv.CustomerID = @CustomerID		
			AND ( SELECT count(*) FROM @Creditors ) > 0
		
			INSERT @TableOut (	TableRowID,
					Creditor,
					Percentage,
					[Year 1],
					[Year 2],
					[Year 3],
					[Year 4],
					[Year 5],
					[Year 6],
					[Year 7],
					Totals,
					[Return for creditors],
					DenyEdit,
					DenyDelete
				)
			SELECT	  c.TableRowID
					, c.Name [Creditor]
					, c.Percentage [Percentage]
					, CAST(@ValueNumeric as DECIMAL(18,2))	[Year 1]
					, CAST(0.00 as DECIMAL(18,2)) 			[Year 2]
					, CAST(0.00 as DECIMAL(18,2)) 			[Year 3]
					, CAST(0.00 as DECIMAL(18,2)) 			[Year 4]
					, CAST(0.00 as DECIMAL(18,2)) 			[Year 5]
					, CAST(0.00 as DECIMAL(18,2)) 			[Year 6]
					, CAST(0.00 as DECIMAL(18,2)) 			[Year 7]
					, CAST(@ValueNumeric as DECIMAL(18,2)) 	[Totals]
					, CONVERT ( VARCHAR, CAST( (@ValueNumeric / c.Amount) * 100 AS DECIMAL(18,2) ) ) + '%' [Return for creditors]
					, 'true' [DenyEdit]
					, 'true' [DenyDelete]
			FROM @Creditors c 
			WHERE c.Amount <> 0
		END	
		ELSE
		BEGIN
			INSERT @TableOut (	TableRowID,
								Creditor,
								Percentage,
								[Year 1],
								[Year 2],
								[Year 3],
								[Year 4],
								[Year 5],
								[Year 6],
								[Year 7],
								Totals,
								[Return for creditors],
								DenyEdit,
								DenyDelete
							)
			SELECT	  c.TableRowID
					, c.Name [Creditor]
					, c.Percentage [Percentage]
					, CAST(c.[Year1] as DECIMAL(18,2)) [Year 1]
					, CAST(c.[Year2] as DECIMAL(18,2)) [Year 2]
					, CAST(c.[Year3] as DECIMAL(18,2)) [Year 3]
					, CAST(c.[Year4] as DECIMAL(18,2)) [Year 4]
					, CAST(c.[Year5] as DECIMAL(18,2)) [Year 5]
					, CAST(c.[Year6] as DECIMAL(18,2)) [Year 6]
					, CAST(c.[Year7] as DECIMAL(18,2)) [Year 7]
					, CAST(c.[Year1]+c.[Year2]+c.[Year3]+c.[Year4]+c.[Year5]+ISNULL(c.[Year6],0.00)+ISNULL(c.[Year7],0.00) as DECIMAL(18,2)) [Totals]
					, CONVERT(VARCHAR,CAST( CASE WHEN c.PreferentialValue > 0.00 THEN 100.00 ELSE ((c.[Year1]+c.[Year2]+c.[Year3]+c.[Year4]+c.[Year5]+ISNULL(c.[Year6],0.00)+ISNULL(c.[Year7],0.00)) / c.Amount) * 100 END AS DECIMAL(18,2))) + '%' [Return for creditors]
					, 'true' [DenyEdit]
					, 'true' [DenyDelete]
			FROM @Creditors c 
			WHERE c.Amount <> 0
			ORDER BY [Percentage] DESC, [Totals] DESC
		END
	END
	ELSE
	BEGIN
	
		SELECT @Term = ISNULL(li.ValueInt,6)
		FROM ThirdPartyFieldMapping fm WITH (NOLOCK) 
		INNER JOIN CustomerDetailValues cdv WITH (NOLOCK) on cdv.CustomerID = @CustomerID and cdv.DetailFieldID = fm.DetailFieldID
		INNER JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = cdv.ValueInt
		WHERE fm.ClientID = @ClientID 
		and fm.ThirdPartyFieldID = 733

		SELECT	@UnsecuredLenderNameFieldID		= [663],
				@PercentageOfDebtFieldID		= [665],
				@AmountOwedFieldID				= [664],
				@Year1FieldID					= [657],
				@Year2FieldID					= [658],
				@Year3FieldID					= [659],
				@Year4FieldID					= [660],
				@Year5FieldID					= [661],
				@Year6FieldID					= [662],
				@Year7FieldID					= [826],
				@UnsecuredCreditorTableFieldID	= [656]
		FROM		
			(	
			SELECT pm.ThirdPartyFieldID, ISNULL(pm.ColumnFieldID, DetailFieldID) [DetailFieldID]
			FROM ThirdPartyFieldMapping pm WITH (NOLOCK) 
			WHERE pm.ThirdPartyFieldGroupID = 26
			and pm.ClientID = @ClientID
			)
			AS ToPivot		
		PIVOT		
		(		
			Max([DetailFieldID])		
		FOR		
		[ThirdPartyFieldID]
			IN ( [657],[658],[659],[660],[661],[662],[663],[664],[665],[656], [826] )
		) AS Pivoted

		;With RawData as
		(
				SELECT tr.TableRowID, li.ItemValue, pct.ValueMoney [Percengtage], bal.ValueMoney [Balance], LEFT(df.FieldName,6) [Year], cdv.ValueMoney * (pct.ValueMoney/100.00) [ValueMoney]
				FROM TableRows tr WITH (NOLOCK)  
				INNER JOIN TableDetailValues name WITH (NOLOCK) on name.TableRowID = tr.TableRowID and name.DetailFieldID = @UnsecuredLenderNameFieldID /*Unsecured Lender Name*/ 
				LEFT JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = name.ValueInt
				LEFT JOIN TableDetailValues pct WITH (NOLOCK) on pct.TableRowID = tr.TableRowID and pct.DetailFieldID = @PercentageOfDebtFieldID		/*Percentage of Debt Level*/
				LEFT JOIN TableDetailValues bal WITH (NOLOCK) on bal.TableRowID = tr.TableRowID and bal.DetailFieldID = @AmountOwedFieldID				/*Amount Owed Unsecured*/
				LEFT JOIN CustomerDetailValues cdv WITH (NOLOCK) on cdv.CustomerID = tr.CustomerID and ( cdv.DetailFieldID IN(@Year1FieldID,@Year2FieldID,@Year3FieldID,@Year4FieldID,@Year5FieldID) /*Year 1 - 6 Total repayments to unsecured creditors net of PIP's fees*/
																										OR
																										 (@Term >= 6 and cdv.DetailFieldID = @Year6FieldID)
																										OR
																										 (@Term >= 7 and cdv.DetailFieldID = @Year7FieldID)
																										)
				LEFT JOIN dbo.DetailFields df WITH (NOLOCK) on df.DetailFieldID = cdv.DetailFieldID 
				WHERE tr.DetailFieldID = @UnsecuredCreditorTableFieldID /*Enter Unsecured Creditor Details*/
				and tr.CustomerID = @CustomerID
		),
		YearTotals as
		(
		SELECT rd.Year, SUM(rd.ValueMoney) [YearTotal]
		FROM RawData rd 
		GROUP BY rd.Year
		),
		Pivoted AS
		(
			SELECT TableRowID, ItemValue [Name], Percengtage [Percentage of Debt], Balance [Balance], [Year 1], [Year 2], [Year 3], [Year 4], [Year 5], [Year 6], [Year 7]
			FROM
				(
				SELECT  rd.TableRowID, rd.ItemValue, rd.Percengtage, rd.[Balance], rd.[Year], ISNULL(rd.[ValueMoney],0.00) [ValueMoney]
				FROM RawData rd
				)
				AS ToPivot
			PIVOT
			(
				Max([ValueMoney])
			FOR
			[Year]
				IN ( [Year 1],[Year 2],[Year 3],[Year 4],[Year 5],[Year 6],[Year 7] )
			) AS Pivoted
		),
		Yearly as
		(
		SELECT	p.TableRowID,
				p.Name,
				p.[Percentage of Debt],
				p.Balance,
				ROUND(p.[Year 1],2,1) [Year 1],
				ROUND(p.[Year 2],2,1) [Year 2],
				ROUND(p.[Year 3],2,1) [Year 3],
				ROUND(p.[Year 4],2,1) [Year 4],
				ROUND(p.[Year 5],2,1) [Year 5],
				ROUND(p.[Year 6],2,1) [Year 6],
				ROUND(p.[Year 7],2,1) [Year 7],
				ROW_NUMBER() OVER (ORDER BY p.[Percentage of Debt] desc) [Row]
		FROM Pivoted p
		), RoundingRepair as 
		(
		SELECT	y.TableRowID,
				y.Name,
				y.[Percentage of Debt],
				y.Balance,
				CASE y.Row WHEN 1 THEN y.[Year 1] + ( yt1.YearTotal -( SELECT SUM(y.[Year 1]) FROM Yearly y )) ELSE y.[Year 1] END [Year 1],
				CASE y.Row WHEN 1 THEN y.[Year 2] + ( yt1.YearTotal -( SELECT SUM(y.[Year 2]) FROM Yearly y )) ELSE y.[Year 2] END [Year 2],
				CASE y.Row WHEN 1 THEN y.[Year 3] + ( yt1.YearTotal -( SELECT SUM(y.[Year 3]) FROM Yearly y )) ELSE y.[Year 3] END [Year 3],
				CASE y.Row WHEN 1 THEN y.[Year 4] + ( yt1.YearTotal -( SELECT SUM(y.[Year 4]) FROM Yearly y )) ELSE y.[Year 4] END [Year 4],
				CASE y.Row WHEN 1 THEN y.[Year 5] + ( yt1.YearTotal -( SELECT SUM(y.[Year 5]) FROM Yearly y )) ELSE y.[Year 5] END [Year 5],
				CASE y.Row WHEN 1 THEN y.[Year 6] + ( yt1.YearTotal -( SELECT SUM(y.[Year 6]) FROM Yearly y )) ELSE y.[Year 6] END [Year 6],
				CASE y.Row WHEN 1 THEN y.[Year 7] + ( yt1.YearTotal -( SELECT SUM(y.[Year 7]) FROM Yearly y )) ELSE y.[Year 7] END [Year 7]
		FROM Yearly y
		LEFT JOIN YearTotals yt1 on yt1.Year = 'Year 1'
		LEFT JOIN YearTotals yt2 on yt2.Year = 'Year 2'
		LEFT JOIN YearTotals yt3 on yt3.Year = 'Year 3'
		LEFT JOIN YearTotals yt4 on yt4.Year = 'Year 4'
		LEFT JOIN YearTotals yt5 on yt5.Year = 'Year 5'
		LEFT JOIN YearTotals yt6 on yt6.Year = 'Year 6'
		LEFT JOIN YearTotals yt7 on yt7.Year = 'Year 7'
		)
		INSERT @TableOut (	TableRowID,
							Creditor,
							Percentage,
							[Year 1],
							[Year 2],
							[Year 3],
							[Year 4],
							[Year 5],
							[Year 6],
							[Year 7],
							Totals,
							[Return for creditors],
							DenyEdit,
							DenyDelete
						)
		SELECT	rr.TableRowID
				, rr.Name [Creditor]
				, rr.[Percentage of Debt] [Percentage]
				, CAST(rr.[Year 1] as DECIMAL(18,2)) [Year 1]
				, CAST(rr.[Year 2] as DECIMAL(18,2)) [Year 2]
				, CAST(rr.[Year 3] as DECIMAL(18,2)) [Year 3]
				, CAST(rr.[Year 4] as DECIMAL(18,2)) [Year 4]
				, CAST(rr.[Year 5] as DECIMAL(18,2)) [Year 5]
				, CAST(rr.[Year 6] as DECIMAL(18,2)) [Year 6]
				, CAST(rr.[Year 7] as DECIMAL(18,2)) [Year 7]
				, CAST(rr.[Year 1]+rr.[Year 2]+rr.[Year 3]+rr.[Year 4]+rr.[Year 5]+ISNULL(rr.[Year 6],0.00)+ISNULL(rr.[Year 7],0.00) as DECIMAL(18,2)) [Totals]
				, CONVERT(VARCHAR,CAST( ((rr.[Year 1]+rr.[Year 2]+rr.[Year 3]+rr.[Year 4]+rr.[Year 5]+ISNULL(rr.[Year 6],0.00)+ISNULL(rr.[Year 7],0.00)) / rr.Balance) * 100 AS DECIMAL(18,2))) + '%' [Return for creditors]
				, 'true' [DenyEdit]
				, 'true' [DenyDelete]
		FROM RoundingRepair rr 

	END

	RETURN
END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerID_OLD] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerID_OLD] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerID_OLD] TO [sp_executeall]
GO
