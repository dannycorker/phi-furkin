SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2008-04-01
-- Description:	Get the next payment date given the start date, regular payment day,
-- minimum cooling-off period, and not hitting a weekend or bank holiday.
-- =============================================
CREATE FUNCTION [dbo].[fnGetNextPaymentDate]
(
	@StartingDate datetime,
	@PreferredPaymentDay tinyint,
	@MinimumGapBeforeFirstPayDay tinyint
)
RETURNS datetime
AS
BEGIN

	DECLARE @NextTargetPaymentDate datetime, @NextValidPaymentDate datetime

	SELECT @NextTargetPaymentDate = min(w.[Date]) 
	FROM WorkingDays w WITH (NOLOCK) 
	WHERE w.[Day] = @PreferredPaymentDay 
	AND w.[Date] > dateadd(dd, @MinimumGapBeforeFirstPayDay, @StartingDate)

	SELECT @NextValidPaymentDate = min(w.[Date]) 
	FROM WorkingDays w WITH (NOLOCK) 
	WHERE w.[Date] >= @NextTargetPaymentDate
	AND w.[IsWorkDay] = 1

	-- Return the result of the function
	RETURN @NextValidPaymentDate

END









GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetNextPaymentDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetNextPaymentDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetNextPaymentDate] TO [sp_executeall]
GO
