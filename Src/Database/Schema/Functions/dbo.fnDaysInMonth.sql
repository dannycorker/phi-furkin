SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim
-- Create date: 2007-08-05
-- Description:	Returns number of days in month for the month and year passed in
-- =============================================
CREATE FUNCTION [dbo].[fnDaysInMonth] 
(
	-- Add the parameters for the function here
	@Month int, @Year int
)
RETURNS tinyint
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result tinyint

	IF (@Month = 1) OR (@Month = 3) OR (@Month = 5) OR (@Month = 7) OR (@Month = 8) OR (@Month = 10) OR (@Month = 12)
	BEGIN
		SET @Result = 31
	END
	ELSE
	BEGIN
		IF (@Month = 4) OR (@Month = 6) OR (@Month = 9) OR (@Month = 11)
		BEGIN
			SET @Result = 30
		END
		ELSE
		BEGIN
			SET @Result = 28 + dbo.fnIsLeapYear(@Year)
		END
	END

	-- Return the result of the function
	RETURN @Result

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnDaysInMonth] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnDaysInMonth] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnDaysInMonth] TO [sp_executeall]
GO
