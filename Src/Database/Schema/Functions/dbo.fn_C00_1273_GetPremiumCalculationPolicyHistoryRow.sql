SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-01-29
-- Description:	Returns the correct policy history row that is relevant based on the next 
-- DCM 2014-09-15 Copied from 235
-- Modified:	2014-10-17	SB	Fixed (stoopid) bug which was taking the next premium date from today rather than it being passed in
-- 2018-06-13 ACE Added in Or PremiumDate IS NULL
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetPremiumCalculationPolicyHistoryRow]
(
	@CaseID INT,
	@PremiumDate DATE
)
RETURNS INT	
AS
BEGIN
	
	DECLARE @TableRowID INT
	
	DECLARE @MatterID INT
	SELECT @MatterID = MatterID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE CaseID = @CaseID

	
	-- Then we look in policy history and get the row that spans this next period
	SELECT TOP 1 @TableRowID = r.TableRowID
	FROM dbo.TableRows r WITH (NOLOCK)
	INNER JOIN dbo.TableDetailValues tdvFrom WITH (NOLOCK) ON r.TableRowID = tdvFrom.TableRowID AND tdvFrom.DetailFieldID = 145663
	INNER JOIN dbo.TableDetailValues tdvTo WITH (NOLOCK) ON r.TableRowID = tdvTo.TableRowID AND tdvTo.DetailFieldID = 145664
	WHERE r.MatterID = @MatterID 
	AND ((@PremiumDate BETWEEN tdvFrom.ValueDate AND tdvTo.ValueDate) OR (@PremiumDate IS NULL))
	ORDER BY tdvFrom.ValueDate DESC
	
	RETURN @TableRowID
 
END
 


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetPremiumCalculationPolicyHistoryRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetPremiumCalculationPolicyHistoryRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetPremiumCalculationPolicyHistoryRow] TO [sp_executeall]
GO
