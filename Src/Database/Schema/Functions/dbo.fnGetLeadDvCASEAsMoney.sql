SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-06-12
-- Description:	Get LeadDV directly #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetLeadDvCASEAsMoney]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS money
AS
BEGIN

	DECLARE @ValueMoney AS money

	SELECT @ValueMoney = ldv.ValueMoney 
	FROM Cases c WITH (NOLOCK) 
	INNER JOIN LeadDetailValues ldv WITH (NOLOCK) ON c.LeadID = ldv.LeadID AND ldv.DetailFieldID = @DetailFieldID
	WHERE c.CaseID = @CaseID 
	
	RETURN @ValueMoney

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLeadDvCASEAsMoney] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetLeadDvCASEAsMoney] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLeadDvCASEAsMoney] TO [sp_executeall]
GO
