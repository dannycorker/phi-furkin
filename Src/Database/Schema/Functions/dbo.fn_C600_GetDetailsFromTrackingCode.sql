SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-11-24
-- Description:	Takes a tracking code and returns all relevant information
-- 2018-07-19 GPR created to C600 from C433
-- 2018-07-19 GPR modified to remove Campaign Code, and to reference correct field IDs for C600 implementation
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetDetailsFromTrackingCode]
(
	 @TrackingCode	VARCHAR(2000)
)
RETURNS @Data TABLE
(
	 LookupListItemID	INT
	,ResourceListID		INT
	,ItemValue			VARCHAR(2000)
	--,CampaignCode		VARCHAR(2000)
)
AS
BEGIN

	DECLARE  @LookupListItemID	INT
			,@ResourceListID	INT
			,@ItemValue			VARCHAR(2000)
			,@ReturnValue		VARCHAR(2000)

	INSERT @Data ( LookupListItemID, ResourceListID, ItemValue)--, CampaignCode )
	SELECT	 li.LookupListItemID
			,rdvCode.ResourceListID
			,li.ItemValue
			--,rdvCampaign.DetailValue
	FROM ResourceListDetailValues rdvCode WITH ( NOLOCK )
	INNER JOIN ResourceListDetailValues rdvName WITH ( NOLOCK ) on rdvName.ResourceListID = rdvCode.ResourceListID AND rdvName.DetailFieldID = 180105 /*Source*/
	INNER JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListItemID = rdvName.ValueInt
	-- LEFT JOIN ResourceListDetailValues rdvCampaign WITH ( NOLOCK ) on rdvCampaign.ResourceListID = rdvName.ResourceListID AND rdvCampaign.DetailFieldID = 311686 /*Campaign*/
	WHERE rdvCode.DetailFieldID = 180109 /*Encoded Rlid a.k.a. 'Token Code'*/
	AND rdvCode.DetailValue = @TrackingCode

	-- And return it
	RETURN

END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetDetailsFromTrackingCode] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_GetDetailsFromTrackingCode] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetDetailsFromTrackingCode] TO [sp_executeall]
GO
