SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-08-27
-- Description:	PremiumFundingEnabled
-- =============================================
CREATE FUNCTION [dbo].[fn_PremiumFundingEnabled]
(
)
RETURNS INT
AS
BEGIN
	
	DECLARE @PremiumFundingEnabled INT = 0

	RETURN @PremiumFundingEnabled

END
GO
