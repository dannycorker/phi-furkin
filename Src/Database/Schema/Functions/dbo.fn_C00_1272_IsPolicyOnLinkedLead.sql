SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-09-23
-- Description:	Checks to see if the policy is managed on a linked lead
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_IsPolicyOnLinkedLead]
(
	@ClaimMatterID INT
)
RETURNS BIT	
AS
BEGIN

	DECLARE @Result BIT
	
	DECLARE @ClaimLeadID INT,
			@PolicyLeadID INT
			
	SELECT @ClaimLeadID = LeadID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @ClaimMatterID
			
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimLead(@ClaimLeadID)


	IF @PolicyLeadID != @ClaimLeadID
	BEGIN

		SELECT @Result = 1
		
	END
	ELSE
	BEGIN

		SELECT @Result = 0
		
	END
	
	RETURN @Result

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_IsPolicyOnLinkedLead] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_IsPolicyOnLinkedLead] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_IsPolicyOnLinkedLead] TO [sp_executeall]
GO
