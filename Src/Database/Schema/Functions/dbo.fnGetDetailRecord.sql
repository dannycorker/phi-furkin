SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 06/03/2020
-- Description:	AU Domestic Payments File Format, Get Detail Record:
--				Each Detail Record contains one AU Domestic Payment instruction. One or more Detail
--				Items can be included in a single ABA batch.
-- =============================================
CREATE FUNCTION [dbo].[fnGetDetailRecord]
(	
	@BSB VARCHAR(7),								-- Bank/State/Branch number e.g. 013-999. 2 8 7 Mandatory
	@Account VARCHAR(9),							-- Funds account number. 9 17 9 Mandatory
	@WithholdingTaxIndicator VARCHAR(1),			-- One of the following values, if applicable: W, X, Y. 18 18 1 Optional
	@TransactionCode VARCHAR(2),					-- Select from the following options as appropriate: 50 General Credit, 13 General Debit. 19 20 2 Mandatory
	@AmountString VARCHAR(10),						-- Right justified, zero filled, unsigned, two decimal places are implied (e.g. $10.21 is recorded as 0000001021). 21 30 10 Mandatory
	@TitleOfAccount VARCHAR(32),					-- Preferred format is: Surname followed by given names with one blank between each name. e.g. SMITH John Alan. Left justified, blank filled. 31 62 32 Mandatory
	@LodgementReference VARCHAR(18),				-- Payment reference indicating details of the origin of the entry (e.g. payroll number, policy number). Left justified, blank filled. 63 80 18 Mandatory
	@TraceBSBNumber VARCHAR(7),						-- Bank/State/Branch number of the trace account with a hyphen in the 4th character position. e.g. 013-999 81 87 7 Mandatory
	@TraceAccountNumber VARCHAR(9),					-- Numeric, alpha, hyphens & blanks are valid. Right justified, blank filled. 88 96 9 Mandatory
	@NameOfRemitter VARCHAR(16),					-- Name of originator of the entry. This may vary from Name of User. Left justified, blank filled. 97 112 16 Mandatory
	@WithholdingAmount VARCHAR(8)					-- Must be zero filled or contain a withholding tax amount. 113 120 8 Optional
)
RETURNS VARCHAR(120)
AS
BEGIN
	
	DECLARE @RecordType VARCHAR(1) = '1'			-- Must be ‘1’. 1 1 1 Mandatory

	DECLARE @Record VARCHAR(120)

	SELECT @Record = @RecordType + @BSB + dbo.fnPadRight(@ACCOUNT, ' ', 9) + dbo.fnPadLeft(@WithholdingTaxIndicator, ' ', 1) + @TransactionCode + dbo.fnPadLeft(@AmountString, '0', 10) + dbo.fnPadRight(@TitleOfAccount, ' ', 32)
					+ dbo.fnPadRight(@LodgementReference, ' ', 18) + @TraceBSBNumber + dbo.fnPadRight(@TraceAccountNumber, ' ', 9) + dbo.fnPadRight(@NameOfRemitter, ' ', 16) + dbo.fnPadLeft(@WithholdingAmount, '0', 8)

	return @Record

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDetailRecord] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetDetailRecord] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDetailRecord] TO [sp_executeall]
GO
