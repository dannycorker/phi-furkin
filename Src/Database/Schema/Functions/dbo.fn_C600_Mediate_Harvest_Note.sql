SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire
-- Create date: 2020-09-16
-- Description:	Exam fee note
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Mediate_Harvest_Note]
(
	@LeadID			INT,
	@NoteType		INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE  @JsonData	VARCHAR(MAX) ='',
			 @NoteTitle	VARCHAR(500),
			 @NoteDetails VARCHAR(1000),
			 @WellnessProductID VARCHAR(100),
			 @MatterID INT 

	SELECT @MatterID = m.MatterID 
	FROM Lead l WITH (NOLOCK) 
	JOIN Matter m WITH (NOLOCK) ON m.LeadID = l.LeadID
	WHERE l.LeadID = @LeadID

	IF @NoteType = 1
	BEGIN 

		SELECT @NoteTitle = 'Exam Fees Included'
		SELECT @NoteDetails = 'Exam Fees are included for this Policy'

	END

	IF @NoteType = 2
	BEGIN 

		SELECT @WellnessProductID = rldv.DetailValue
		FROM Matter m WITH (NOLOCK) 
		JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID
		JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = mdv.ValueInt
		WHERE mdv.DetailFieldID = 314235 
		AND rldv.DetailFieldID = 314005
		AND m.MatterID = @MatterID

		SELECT @NoteTitle = 'Wellness Product'
		SELECT @NoteDetails = 'The product for this policy is ' + @WellnessProductID

	END

	IF @LeadID > 0
	BEGIN
		SELECT  @JsonData +=
		(
			SELECT	 ov.VisionCustomerID			[customerID]
					,@NoteTitle						[title]
					,@NoteDetails					[details]
					,2								[type]
			FROM Customers cu								WITH (NOLOCK)
			INNER JOIN Lead l								WITH (NOLOCK) ON l.CustomerID = cu.CustomerID
			INNER JOIN OneVision ov							WITH (NOLOCK) ON ov.PALeadID = l.LeadID
			WHERE l.LeadID = @LeadID
			AND l.LeadTypeID = 1492 /*Policy Admin*/

			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		)
	END


	RETURN @JsonData

END
GO
