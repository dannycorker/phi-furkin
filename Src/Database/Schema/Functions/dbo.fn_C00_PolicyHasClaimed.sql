SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		James Lewis
-- Create date: 2018-08-19
-- Description:	Has this policy Claimed this year 
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_PolicyHasClaimed]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
	  
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE	 @Result BIT = 0 
	
	/*If Collections Matter fed in then change this to the PA matter*/ 
	IF EXISTS (SELECT * FROM Lead l WITH ( NOLOCK ) 
			   INNER JOIN Matter m WITH ( NOLOCK ) on m.LeadID = l.LeadID 
			   WHERE l.LeadTypeID = 1493
			   AND m.MatterID = @MatterID) 
	BEGIN 
	
		SELECT @MatterID = dbo.fn_C00_Billing_GetPolicyAdminMatterFromCollections(@MatterID) 
	
	END 		   
	
	IF EXISTS (SELECT * FROM Lead l WITH ( NOLOCK ) 
			   INNER JOIN Matter m WITH ( NOLOCK ) on m.LeadID = l.LeadID 
			   WHERE l.LeadTypeID = 1490
			   AND m.MatterID = @MatterID) 
	BEGIN 
	
		SELECT @MatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID) 
	
	END 	
	
	
			            
	SELECT @Result = COUNT(*) 
	FROM LeadTypeRelationship ltr WITH ( NOLOCK ) 
	INNER JOIN Matter m WITH ( NOLOCK ) on m.MatterID = ltr.ToMatterID AND ltr.ToLeadTypeID = 1490
	INNER JOIN Cases c WITH ( NOLOCK ) on c.CaseID = m.CaseID
	INNER JOIN Matter mm WITH ( NOLOCK ) on mm.MatterID = ltr.FromMatterID 
	INNER JOIN MatterDetailValues clstart WITH ( NOLOCK ) on clstart.MatterID = m.MatterID and clstart.DetailFieldID = 144366
	INNER JOIN MatterDetailValues clend WITH ( NOLOCK ) on clend.MatterID = m.MatterID and clend.DetailFieldID = 145674
	INNER JOIN MatterDetailValues product WITH ( NOLOCK ) on product.MatterID = mm.MatterID AND product.DetailFieldID = 177074
	INNER JOIN PurchasedProduct pp WITH ( NOLOCK ) on pp.PurchasedProductID = product.ValueInt
	where c.ClientStatusID In (4029,4470)  
	AND mm.MatterID= @MatterID
	AND (clstart.ValueDate BETWEEN pp.ValidFrom and pp.ValidTo 
		  or 
		  clend.ValueDate BETWEEN pp.ValidFrom and pp.ValidTo)

	RETURN @Result

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_PolicyHasClaimed] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_PolicyHasClaimed] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_PolicyHasClaimed] TO [sp_executeall]
GO
