SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2008-07-15
-- Description:	Calculate age in years from a given date.
--              Sounds obvious, but leap years need attention.
-- =============================================
CREATE FUNCTION [dbo].[fnAgeFromDate] 
(
	@startdate datetime
)
RETURNS int
AS
BEGIN

	-- Return datediff in years, plus/minus 1 extra year to cater for these scenarios:
	-- A) From 1st Jan 2008 to 15th July 2008 is 0 years
	-- B) From 1st Jan 2008 to 1st Jan 2009 is 1 year
	return datediff(yy,@startdate,dbo.fn_GetDate_Local()) -	case 
													when dbo.fn_GetDate_Local() < dateadd(yy,datediff(yy,@startdate,dbo.fn_GetDate_Local()), @startdate) then 1 
													else 0 
												end
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnAgeFromDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnAgeFromDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnAgeFromDate] TO [sp_executeall]
GO
