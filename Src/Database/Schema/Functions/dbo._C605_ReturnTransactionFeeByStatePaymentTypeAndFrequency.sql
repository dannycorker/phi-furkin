SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2020-08-16
-- Description: Accepts the state code, PaymentType and PaymentFrequency and returns the relevent fee
-- =============================================
CREATE FUNCTION [dbo].[_C605_ReturnTransactionFeeByStatePaymentTypeAndFrequency]
(
	@StateCode VARCHAR(10),
	@PaymentType INT,
	@PaymentFreq INT
)
RETURNS DECIMAL(18,2)
AS
BEGIN

	DECLARE @TransactionFee DECIMAL(18,2)

	/*Select the Fee based on the values passed in*/
	SELECT @TransactionFee = r3.ValueMoney
	--SELECT r.DetailValue AS [StateCode], r1.ValueInt AS [PaymentType], r2.ValueInt as [PaymentFrequency], r3.ValueMoney as [TransactionFee]
	FROM ResourceListDetailValues r WITH (NOLOCK)
	INNER JOIN ResourceListDetailValues r1 WITH (NOLOCK) ON r.ResourceListID = r1.ResourceListID
	INNER JOIN ResourceListDetailValues r2 WITH (NOLOCK) ON r.ResourceListID = r2.ResourceListID
	INNER JOIN ResourceListDetailValues r3 WITH (NOLOCK) ON r.ResourceListID = r3.ResourceListID
	WHERE r.DetailFieldID = 313947
	AND r1.DetailFieldID = 313948
	AND r2.DetailFieldID = 313949
	AND r3.DetailFieldID = 313950
	AND r.DetailValue = @StateCode
	AND r1.ValueInt = @PaymentType
	AND r2.ValueInt = @PaymentFreq

	RETURN @TransactionFee

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C605_ReturnTransactionFeeByStatePaymentTypeAndFrequency] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C605_ReturnTransactionFeeByStatePaymentTypeAndFrequency] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C605_ReturnTransactionFeeByStatePaymentTypeAndFrequency] TO [sp_executeall]
GO
