SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2010-08-27
-- Description:	Get the filename or path from a full path-and-file value
-- =============================================
CREATE FUNCTION [dbo].[fnFileOrPath]
(
	@FullFileAndPath varchar(1000), 
	@ReturnFilename bit  
)
RETURNS varchar(1000)
AS
BEGIN

	/* Output = input by default */
	DECLARE @OutFileOrPath varchar(1000) = @FullFileAndPath, 
	@LastBackslashPos int = 0,
	@FileAndPathLength int = 0 

	/* Reverse the input and look for the first '\' character */
	SELECT @FileAndPathLength = datalength(@FullFileAndPath),
	@LastBackslashPos = charindex('\', reverse(@FullFileAndPath))

	IF @LastBackslashPos > 0
	BEGIN
		IF @ReturnFilename = 1
		BEGIN
			/* Filename */
			SELECT @OutFileOrPath = reverse(substring(reverse(@FullFileAndPath), 1, @LastBackslashPos - 1))
		END
		ELSE
		BEGIN
			/* Path */
			SELECT @OutFileOrPath = substring(@FullFileAndPath, 1, @FileAndPathLength - @LastBackslashPos)
		END
	END

	RETURN @OutFileOrPath

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnFileOrPath] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnFileOrPath] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnFileOrPath] TO [sp_executeall]
GO
