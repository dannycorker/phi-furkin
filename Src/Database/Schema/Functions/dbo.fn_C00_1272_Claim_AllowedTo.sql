SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-03-20
-- Description:	Checks to see if certain actions can be done to a claim
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_Claim_AllowedTo]
(
	@Action VARCHAR(50),
	@CaseID INT
)
RETURNS BIT	
AS
BEGIN
	
	DECLARE @CaseStatus INT
	SELECT @CaseStatus = ClientStatusID
	FROM dbo.Cases WITH (NOLOCK) 
	WHERE CaseID = @CaseID
	
	
	IF @Action = 'edit'
	BEGIN
	
		-- We are allowed to edit a claim until the case status goes to approved, paid or rejected
		IF @CaseStatus IN (4029, 4470, 3819, 4687)
		BEGIN
			RETURN 0
		END
		
		RETURN 1
	
	END
	
	RETURN 0

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Claim_AllowedTo] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_Claim_AllowedTo] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Claim_AllowedTo] TO [sp_executeall]
GO
