SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Stephen Ainsworth
-- Create date: 2017-07-11
-- Description:	Decide which MTA processing route to take for an MTA
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetMtaProcessingRoute]
(
	@CaseID	INT
)
RETURNS INT
AS
BEGIN
	
		DECLARE @ReturnValue INT,
		/*1 for yes - 2 for no*/
	
			@PolicyStarted		DATE,
			@AnyClaims			INT,
			@ChangeDOBorName	INT,
			@PolicyMatterID		INT,
			@DecisionTreeRoute	INT,
			@PolicyLeadID		INT,
			@ClaimLeadID		INT,
			@CustomerID			INT,
			@PDOBCustomerLevel  DATE,
			@PDOBLeadLevel		DATE,
			@NameChange			INT,
			@PETCuGender		VARCHAR (200),
			@PETLGender			VARCHAR (200),
			@PolicyNotStarted	INT,
			@Madeclaims			INT,
			@MatterID			INT


		
		
		SELECT @PolicyMatterID = m.MatterID,@PolicyLeadID = l.LeadID,@CustomerID = cu.CustomerID
		FROM  Customers cu WITH (NOLOCK)
		INNER JOIN Lead l WITH (NOLOCK) ON cu.CustomerID = l.CustomerID
		INNER JOIN Cases c WITH (NOLOCK) ON l.LeadID = c.LeadID
		INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID = c.CaseID
		WHERE l.LeadTypeID = 1492
		AND c.CaseID = @CaseID
		
		/*Pet Gender Customer Level*/  SELECT @PETCuGender = dbo.fnGetSimpleDv(175329,@CustomerID)
		/*Pet Gender Lead Level*/		SELECT @PETLGender = dbo.fnGetSimpleDv(144275,@PolicyLeadID)
		/*PolicyStartDate*/				SELECT @PolicyStarted = dbo.fnGetSimpleDvAsDate(170036,@PolicyMatterID)


		/*Changing Date of Birth*/
		
		/*FOR DEMO ONLY*/
		/*do you want to change the pets date of birth or name as well*/
		SELECT @PDOBCustomerLevel =  dbo.fnGetDvAsDate(175326,@CaseID)
		SELECT @PDOBLeadLevel = dbo.fnGetDvAsDate(144274,@CaseID)
		
		IF @PDOBCustomerLevel <> @PDOBLeadLevel 
		BEGIN
		
		SELECT @DecisionTreeRoute = 1
		
		END
		ELSE
		BEGIN
		
		SELECT @DecisionTreeRoute = 0
		
		END
		

		--/*CHANGING GENDER*/
		--IF @PETCuGender <> @PETLGender
		--BEGIN
		--	/*IF 1 then move to next else - 2 make amendment*/
		--	IF @PolicyStarted > dbo.fn_GetDate_Local()
		--	BEGIN
		--		/*Policy not started - make amendment*/
		--		SELECT @DecisionTreeRoute = 0
		--	END
		--	ELSE
		--	BEGIN	
		--		/*has there been any claims*/
		--		SELECT @ClaimLeadID = dbo.fn_C00_1272_GetClaimLeadFromPolicyLead (@PolicyLeadID)
			
		--		SELECT @AnyClaims = COUNT (le.LeadEventID)
		--		FROM dbo.Customers cu WITH (NOLOCK) 
		--		INNER JOIN dbo.Lead l WITH (NOLOCK) on l.CustomerID = cu.CustomerID 
		--		INNER JOIN dbo.Cases c WITH (NOLOCK) on c.LeadID = l.LeadID 
		--		INNER JOIN dbo.Matter m WITH (NOLOCK) on m.CaseID = c.CaseID
		--		INNER JOIN dbo.LeadEvent le WITH (NOLOCK) on le.CaseID = c.CaseID
		--		WHERE le.EventTypeID = 121865 /*approve payment*/
		--		AND le.EventDeleted = 0
		--		AND l.LeadID = @ClaimLeadID
		--		AND l.LeadTypeID = 1490
			
		--	IF @AnyClaims >= 1 
		--	BEGIN 
		--		/*IF 1 Need proof from VET*/
		--		SELECT @DecisionTreeRoute = 1
		--	END
		--	ELSE 
		--	BEGIN
		--		/*IF 2 then move to next step*/
				
		--		/*do you want to change the pets date of birth or name as well*/
		--		SELECT @PDOBCustomerLevel = dbo.fnGetDvAsDate(175326,@CaseID)
		--		SELECT @PDOBLeadLevel = dbo.fnGetDvAsDate(144274,@PolicyLeadID)
				
		--		SELECT @NameChange = dbo.fnGetSimpleDvAsInt (177683,@PolicyMatterID)
				
		--		IF @PDOBCustomerLevel <> @PDOBCustomerLevel OR @NameChange = 1
		--		BEGIN
		--		/*VET letter required*/
		--			SELECT @DecisionTreeRoute = 1
		--		END
		--		ELSE
		--		BEGIN
		--			/*VET letter not required*/
		--			SELECT @DecisionTreeRoute = 0
		--		END
		--	END
		--END
		
		SELECT @ReturnValue = @DecisionTreeRoute
		
		--END
		
		
			
				
				
		
		
		

		/*CHANGING DATE OF BIRTH*/
		
	RETURN @ReturnValue

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetMtaProcessingRoute] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetMtaProcessingRoute] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetMtaProcessingRoute] TO [sp_executeall]
GO
