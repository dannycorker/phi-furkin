SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger on behalf of Stephen Ainsworth
-- Create date: 2015-05-21
-- Description:	RLE Interlocking app1
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ISI_RLEInterlockingApp2]
(	
	@CustomerID INT
)
RETURNS @WorkingTable TABLE ( Title VARCHAR(2000), Value NUMERIC(18,2), DisplayOrder DECIMAL(18,2) )
AS
BEGIN 

	INSERT @WorkingTable ( Title, Value, DisplayOrder )

	SELECT 'Interlocking Case Breakdown',NULL,0

	UNION ALL

	SELECT 'Applicant Two',NULL,1

	UNION ALL

	SELECT 'Assets',NULL,2

	UNION ALL

	SELECT 'PPR Mortgage',
	CASE dbo.fnGetSimpleDvLuli(297052,@CustomerID)
	WHEN 'Interlocking' 
	THEN dbo.fnGetSimpleDvasmoney(297776,@CustomerID)/2
	ELSE 0
	END

	,3 [DisplayOrder]

	UNION ALL

	SELECT 'Additional Assets', ISNULL(SUM(cdv.ValueMoney),0.00),4
	FROM Customers c WITH (NOLOCK)
	LEFT JOIN CustomerDetailValues cdv WITH ( NOLOCK ) ON cdv.CustomerID=c.CustomerID AND cdv.DetailFieldID IN (297542,297545,297548,297554,297551,297557,297560,297563,297566,297569,297572,297575)
	WHERE c.CustomerID = @CustomerID 

	UNION ALL

	SELECT 'Total Assets', ISNULL(SUM(wt.Value),0.00), 5
	FROM @WorkingTable wt 
	WHERE wt.Title IN ( 'PPR Mortgage','Additional Assets' )

	UNION ALL

	SELECT 'Debts',NULL,6

	UNION ALL

	SELECT 'Secured Debts',NULL,7

	UNION ALL

	SELECT 'PPR Mortgage',
	CASE dbo.fnGetSimpleDvLuli(297052,@CustomerID)
	WHEN 'Interlocking' 
	THEN dbo.fnGetSimpleDvasmoney(297776,@CustomerID)/2
	ELSE 0
	END

	,8 [DisplayOrder]

	UNION ALL

	SELECT 'Total Additional Secured Debt', ISNULL(SUM(cdv.ValueMoney),0.00),9
	FROM CustomerDetailValues cdv WITH ( NOLOCK ) 
	WHERE cdv.CustomerID = @CustomerID 
	AND cdv.DetailFieldID IN (297832,297838,297844,297850,297859,297865,297871,297877,297883,297889,297895)

	UNION ALL

	SELECT 'Total Secured Debt', ISNULL(SUM(wt.Value),0.00), 10
	FROM @WorkingTable wt 
	WHERE wt.Title IN ( 'PPR Mortgage','Total Additional Secured Debt' )

	UNION ALL

	SELECT 'Unsecured Debt',NULL,11

	UNION ALL

	SELECT 'Total Individual Unsecured Debt', ISNULL(SUM(cdv.ValueMoney),0.00),12
	FROM Customers c WITH (NOLOCK)
	LEFT JOIN CustomerDetailValues cdv WITH ( NOLOCK ) ON cdv.CustomerID=c.CustomerID AND cdv.DetailFieldID IN (297833,297839,297845,297851,297860,297866,297872,297878,297884,297890,297896)
	WHERE c.CustomerID = @CustomerID 

	UNION ALL

	SELECT 'Monthly Income', NULL,13

	UNION ALL

	SELECT 'Net Income', CONVERT (NUMERIC(18,2),
	CASE dbo.fnGetSimpleDvLuli (297052,@CustomerID)
	WHEN 'Interlocking' 
	THEN dbo.fnGetSimpleDvAsMoney(297152,@CustomerID)
	ELSE 0
	END)
	,14 [DisplayOrder]

	INSERT @WorkingTable ( Title, Value, DisplayOrder )												

	SELECT 'Total Additional Income', ISNULL(SUM(cdv.ValueMoney),0.00),15
	FROM Customers c WITH ( NOLOCK )
	LEFT JOIN CustomerDetailValues cdv WITH (NOLOCK) ON cdv.CustomerID=c.CustomerID AND cdv.DetailFieldID IN (297153,297154,297155,297156,297158,297159,297160,297162,297163,297164,297165,297166,297167,297168,297169,297170,297171)
	WHERE cdv.CustomerID = @CustomerID 

	INSERT @WorkingTable ( Title, Value, DisplayOrder )
																
	SELECT 'Total Income', SUM(wt.Value), 16						
	FROM @WorkingTable wt										
	WHERE wt.Title IN ( 'Net Income','Total Additional Income' )

	UNION ALL

	SELECT 'Monthly Expenses',NULL,17

	UNION ALL

	SELECT 'Rent/Mortgage', CONVERT (NUMERIC(18,2),
	CASE dbo.fnGetSimpleDvLuli (297052,@CustomerID)
	WHEN 'Interlocking' 
	THEN dbo.fnGetSimpleDvAsMoney(295720,@CustomerID)+dbo.fngetsimpledvasmoney (295721,@CustomerID)
	ELSE 0
	END)
	,18 [DisplayOrder]

	UNION ALL

	SELECT 'Childcare Costs', CONVERT (NUMERIC(18,2),
	CASE dbo.fnGetSimpleDvLuli (297052,@CustomerID)
	WHEN 'Interlocking' 
	THEN dbo.fnGetSimpleDvAsMoney(295722,@CustomerID)
	ELSE 0
	END)
	,19 [DisplayOrder]


	UNION ALL

	SELECT 'Individual Total Set Costs', CONVERT (NUMERIC(18,2),
	CASE WHEN dbo.fnGetSimpleDvLuli (297052,@CustomerID) = 'Interlocking' 
	THEN dbo.fnGetSimpleDvAsMoney(297040,@CustomerID)+dbo.fnGetSimpleDvAsMoney(297092,@CustomerID)
	ELSE 0
	END)
	,20 [DisplayOrder]

	UNION ALL

	SELECT 'Special Circumstance Costs', CONVERT (NUMERIC(18,2),
	CASE dbo.fnGetSimpleDvLuli (297052,@CustomerID)
	WHEN 'Interlocking' 
	THEN dbo.fnGetSimpleDvAsMoney(295782,@CustomerID)
	ELSE 0
	END)
	,21 [DisplayOrder]

	INSERT @WorkingTable ( Title, Value, DisplayOrder )

	SELECT 'Reasonable Living Expenses', SUM(wt.Value),22
	FROM @WorkingTable wt 
	WHERE wt.Title IN ( 'Rent/Mortgage','Childcare Costs','Individual Total Set Costs','Special Circumstance Costs' )

	INSERT @WorkingTable ( Title, Value, DisplayOrder )

	SELECT 'Total expenses before unsecured debts repayment', ISNULL(SUM(wt.Value)+(dbo.fngetsimpledvasmoney (296653,@CustomerID)),0.00),23
	FROM @WorkingTable wt
	WHERE wt.Title = 'Reasonable Living Expenses'


	INSERT @WorkingTable ( Title, Value, DisplayOrder )
	SELECT 'Amount Available for Unsecured Debt Service', wt1.Value - wt2.Value,24 
	FROM @WorkingTable wt1
	INNER JOIN @WorkingTable wt2 ON wt2.Title= 'Total expenses before unsecured debts repayment'
	WHERE wt1.Title = 'Total Income' 

	RETURN

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_RLEInterlockingApp2] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_ISI_RLEInterlockingApp2] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_RLEInterlockingApp2] TO [sp_executeall]
GO
