SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2009-05-19
-- Description:	Replace the SqlServer standard convert(money) with
--              one that doesn't crash all the time.
-- =============================================
CREATE FUNCTION [dbo].[fnConvertMoney] 
(
	@detailvalue varchar(2000)
)
RETURNS money
AS
BEGIN

	-- Declare the return variable here
	DECLARE @money money

	-- Add the T-SQL statements to compute the return value here
	IF isnumeric(@detailvalue) = 1
	BEGIN
		SELECT @money = convert(money, @detailvalue)
	END
	ELSE
	BEGIN
		SELECT @money = 0.00
	END

	-- Return the result of the function
	RETURN @money

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnConvertMoney] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnConvertMoney] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnConvertMoney] TO [sp_executeall]
GO
