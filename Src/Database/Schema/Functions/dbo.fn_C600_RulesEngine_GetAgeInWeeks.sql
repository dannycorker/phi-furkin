SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-09-24
-- Description:	Returns the age of a pet at the terms date from the override data passed in or looked up
-- Mods
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_RulesEngine_GetAgeInWeeks]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS INT	
AS
BEGIN
 
	DECLARE @AgeInWeeks INT,
			@BirthDate DATE,
			@YearStart DATE
	
	-- Birth date will be passed in as an override for quotes	
	SELECT @BirthDate = AnyValue2
	FROM @Overrides
	WHERE AnyID = 1
	AND AnyValue1 = '144274' /*Pet Date of Birth*/
	
	-- For existing policies we need to look this up
	IF @BirthDate IS NULL
	BEGIN
	
		SELECT @BirthDate = ValueDate
		FROM dbo.LeadDetailValues WITH (NOLOCK) 
		WHERE LeadID = @LeadID
		AND DetailFieldID = 144274
	
	END
	
	-- Year start date is always passed in
	SELECT @YearStart = AnyValue2
	FROM @Overrides
	WHERE AnyID = 2
	AND AnyValue1 = 'YearStart'

	-- default start date
	IF @YearStart IS NULL SELECT @YearStart=dbo.fn_GetDate_Local()

	/*GPR 2020-11-04 for SDAAG-217*/
	--SELECT @AgeInWeeks = CASE
	--						WHEN DATEPART(DAY, @BirthDate) > DATEPART(DAY, @YearStart)
	--						THEN DATEDIFF(WEEK, @BirthDate, @YearStart) - 1
	--						ELSE DATEDIFF(WEEK, @BirthDate, @YearStart)
	--					END

	/*GPR for SDAAG-257 following investigation from Mark B*/
	SELECT @AgeInWeeks = (DATEDIFF(DAY,@BirthDate, @YearStart) / 7)
	
	RETURN @AgeInWeeks
 
END
GO
