SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire
-- Create date: 2020-09-21
-- Description:	rollback wellness 
-- 2020-10-02 ALM added productID
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Mediate_Harvest_RollbackWellness]
(
	 @PolicyID			INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE  @JsonData	VARCHAR(MAX) =''

	IF @PolicyID > 0
	BEGIN
		SELECT  @JsonData +=
		(
			SELECT		(SELECT TOP 1 rldv.DetailValue
						FROM Tablerows tr1 WITH (NOLOCK) 
						INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr1.TableRowID AND tdv.DetailFieldID = 314234
						INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = tdv.ResourceListID AND rldv.DetailFieldID = 314268
						WHERE 
						tr1.DetailFieldID = 314240
						AND tr1.MatterID = @PolicyID
						ORDER BY tr1.TableRowID DESC )[productId]
						,tdv1.ValueDateTime				[startDate]
						,tdv2.ValueDateTime				[endDate]
			FROM Customers cu								WITH (NOLOCK)
			INNER JOIN Lead l								WITH (NOLOCK) on l.CustomerID = cu.CustomerID
			INNER JOIN Cases c								WITH (NOLOCK) on c.LeadID = l.LeadID
			INNER JOIN Matter m								WITH (NOLOCK) on m.CaseID = c.CaseID
			INNER JOIN OneVision ov							WITH (NOLOCK) ON ov.PAMatterID = m.MatterID	AND ov.PolicyTermType = 'Wellness'
			INNER JOIN Tablerows tr1						WITH (NOLOCK) ON tr1.MatterID = m.MatterID
			INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr1.TableRowID AND tdv.DetailFieldID = 314234
			INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = tdv.ResourceListID AND rldv.DetailFieldID = 314268
			INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tdv.TableRowID AND tdv1.DetailFieldID = 314237
			INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = tdv.TableRowID AND tdv2.DetailFieldID = 314239
			INNER JOIN TableDetailValues tdv3 WITH (NOLOCK) ON tdv3.TableRowID = tdv.TableRowID AND tdv3.DetailFieldID = 314236
			WHERE m.MatterID = @PolicyID
			AND l.LeadTypeID = 1492 /*Policy Admin*/

			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		)
	END


	RETURN @JsonData

END
GO
