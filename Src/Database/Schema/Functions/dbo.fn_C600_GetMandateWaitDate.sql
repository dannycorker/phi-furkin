SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-08-27
-- Description:	Returns the last mandate wait date (date after which we can start taking payments)
-- Mods
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetMandateWaitDate]
(
	@MatterID INT
)
RETURNS DATE	
AS
BEGIN

	DECLARE @ClientID INT,
			@CollectionsMatterID INT,
			@MandateWaitDate DATE,
			@WaitingDays INT

	-- find appropriate collections matter for the matter passed in
	IF EXISTS ( SELECT * FROM Lead l WITH (NOLOCK)
				INNER JOIN Matter m WITH (NOLOCK) ON l.LeadID=m.LeadID AND m.MatterID=@MatterID
				WHERE l.LeadTypeID=1493 )
	BEGIN
	
		SELECT @CollectionsMatterID=@MatterID
		
	END
	ELSE
	BEGIN
	
		SELECT @CollectionsMatterID=ltr.ToMatterID FROM LeadTypeRelationship ltr WITH (NOLOCK) 
		WHERE ltr.FromMatterID=@MatterID AND ltr.ToLeadTypeID=1493

	END
	
	SELECT @ClientID=ClientID FROM Matter WITH (NOLOCK) WHERE MatterID=@CollectionsMatterID
	SELECT @WaitingDays=mw.ValueInt+lw.ValueInt FROM ClientDetailValues mw WITH (NOLOCK) 
	INNER JOIN ClientDetailValues lw WITH (NOLOCK) ON mw.ClientID=lw.ClientID AND lw.DetailFieldID=170226
	WHERE mw.ClientID=@ClientID AND mw.DetailFieldID=170227

	SELECT TOP 1 @MandateWaitDate=dbo.fnGetNextWorkingDate(DATEADD(DAY,@WaitingDays,ISNULL(bfd.ValueDate,DATEADD(DAY,1,dbo.fn_GetDate_Local()))),0)
	FROM TableRows tr WITH (NOLOCK)
	INNER JOIN TableDetailValues bfd WITH (NOLOCK) ON tr.TableRowID=bfd.TableRowID AND bfd.DetailFieldID=170080
	INNER JOIN TableDetailValues bc WITH (NOLOCK) ON tr.TableRowID=bc.TableRowID AND bc.DetailFieldID=170162 AND bc.DetailValue='0N'
	WHERE tr.MatterID=@CollectionsMatterID AND tr.DetailFieldID=170183 
	ORDER BY tr.TableRowID DESC

	RETURN @MandateWaitDate

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetMandateWaitDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetMandateWaitDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetMandateWaitDate] TO [sp_executeall]
GO
