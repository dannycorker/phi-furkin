SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2015-09-22
-- Description:	Return XML descriptions of a customer's debt
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ISI_XmlPush_Debt]
(
	 @CustomerID INT
)
RETURNS XML
AS
BEGIN

	DECLARE @DebtXML XML

	SELECT @DebtXML = 
		(
		SELECT
			(
			SELECT  luli_sec.ItemValue AS [DebtSecured],(
					SELECT luli_sectype.ItemValue AS [SecurityType], tdv_other.DetailValue AS [OtherDetails], '' AS [SecurityItem]
					FROM TableDetailValues tdv_secType WITH (NOLOCK) 
					LEFT JOIN LookupListItems luli_sectype WITH (NOLOCK) ON luli_sectype.LookupListItemID = tdv_secType.ValueInt
					LEFT JOIN TableDetailValues tdv_other WITH (NOLOCK) ON tdv_other.TableRowID = tdv_secType.TableRowID AND tdv_other.DetailFieldID = 297937
					WHERE tdv_secType.TableRowID = tdv_secured.TableRowID 
					AND tdv_secType.DetailFieldID = 297936
					FOR XML PATH (''), TYPE
					) AS [DebtSecurity],
					(
						(
						SELECT luli_org.ItemValue AS [OrganisationName],
								dbo.fn_C00_ISI_ReturnXmlAddress (tdv_adl1.DetailValue, tdv_adl2.DetailValue, tdv_adl3.DetailValue, tdv_adl4.DetailValue, tdv_adl5.DetailValue, '', ISNULL(luli.ItemValue, 'Ireland')) AS [Address],
								tdv_email.DetailValue AS [Email],
								tdv_phone.DetailValue AS [PhoneNumber]
						FROM TableDetailValues tdv_orgName WITH (NOLOCK)
						LEFT JOIN LookupListItems luli_org WITH (NOLOCK) ON luli_org.LookupListItemID = tdv_orgName.ValueInt 
						INNER JOIN TableDetailValues tdv_adl1 WITH (NOLOCK) ON tdv_adl1.TableRowID = tdv_orgName.TableRowID AND tdv_adl1.DetailFieldID = 297913
						INNER JOIN TableDetailValues tdv_adl2 WITH (NOLOCK) ON tdv_adl2.TableRowID = tdv_orgName.TableRowID AND tdv_adl2.DetailFieldID = 297914
						INNER JOIN TableDetailValues tdv_adl3 WITH (NOLOCK) ON tdv_adl3.TableRowID = tdv_orgName.TableRowID AND tdv_adl3.DetailFieldID = 297915
						INNER JOIN TableDetailValues tdv_adl4 WITH (NOLOCK) ON tdv_adl4.TableRowID = tdv_orgName.TableRowID AND tdv_adl4.DetailFieldID = 297916
						INNER JOIN TableDetailValues tdv_adl5 WITH (NOLOCK) ON tdv_adl5.TableRowID = tdv_orgName.TableRowID AND tdv_adl5.DetailFieldID = 297917
						INNER JOIN TableDetailValues tdv_Country WITH (NOLOCK) ON tdv_Country.TableRowID = tdv_orgName.TableRowID AND tdv_Country.DetailFieldID = 297912
						LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_Country.ValueInt
						INNER JOIN TableDetailValues tdv_email WITH (NOLOCK) ON tdv_email.TableRowID = tdv_orgName.TableRowID AND tdv_email.DetailFieldID = 297918
						INNER JOIN TableDetailValues tdv_phone WITH (NOLOCK) ON tdv_phone.TableRowID = tdv_orgName.TableRowID AND tdv_phone.DetailFieldID = 297919
						WHERE tdv_orgName.TableRowID = tdv_secured.TableRowID 
						AND tdv_orgName.DetailFieldID = 297911
						FOR XML PATH ('Organisation'), Type
						) 
					) AS [Creditor],
					(
						(
						SELECT tdv_firstName.DetailValue AS [FirstName],
								tdv_lastName.DetailValue AS [Surname],
								dbo.fn_C00_ISI_ReturnXmlAddress (tdv_adl1.DetailValue, tdv_adl2.DetailValue, tdv_adl3.DetailValue, tdv_adl4.DetailValue, tdv_adl5.DetailValue, '', ISNULL(luli.ItemValue, 'Ireland')) AS [Address],
								tdv_email.DetailValue AS [Email],
								tdv_phone.DetailValue AS [PhoneNumber]
						FROM TableDetailValues tdv_firstName WITH (NOLOCK)
						INNER JOIN TableDetailValues tdv_lastName WITH (NOLOCK) ON tdv_lastName.TableRowID = tdv_firstName.TableRowID AND tdv_lastName.DetailFieldID = 297942
						INNER JOIN TableDetailValues tdv_adl1 WITH (NOLOCK) ON tdv_adl1.TableRowID = tdv_firstName.TableRowID AND tdv_adl1.DetailFieldID = 297913
						INNER JOIN TableDetailValues tdv_adl2 WITH (NOLOCK) ON tdv_adl2.TableRowID = tdv_firstName.TableRowID AND tdv_adl2.DetailFieldID = 297914
						INNER JOIN TableDetailValues tdv_adl3 WITH (NOLOCK) ON tdv_adl3.TableRowID = tdv_firstName.TableRowID AND tdv_adl3.DetailFieldID = 297915
						INNER JOIN TableDetailValues tdv_adl4 WITH (NOLOCK) ON tdv_adl4.TableRowID = tdv_firstName.TableRowID AND tdv_adl4.DetailFieldID = 297916
						INNER JOIN TableDetailValues tdv_adl5 WITH (NOLOCK) ON tdv_adl5.TableRowID = tdv_firstName.TableRowID AND tdv_adl5.DetailFieldID = 297917
						INNER JOIN TableDetailValues tdv_Country WITH (NOLOCK) ON tdv_Country.TableRowID = tdv_firstName.TableRowID AND tdv_Country.DetailFieldID = 297912
						LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_Country.ValueInt
						INNER JOIN TableDetailValues tdv_email WITH (NOLOCK) ON tdv_email.TableRowID = tdv_firstName.TableRowID AND tdv_email.DetailFieldID = 297918
						INNER JOIN TableDetailValues tdv_phone WITH (NOLOCK) ON tdv_phone.TableRowID = tdv_firstName.TableRowID AND tdv_phone.DetailFieldID = 297919
						WHERE tdv_firstName.TableRowID = tdv_secured.TableRowID 
						AND tdv_firstName.DetailFieldID = 297943
						FOR XML PATH ('Individual'), Type
						) 
					) AS [Creditor],
					luli_Incurred6Month.ItemValue AS [Incurred6Month],
					'' AS [Incurred6MonthAmount],
					tdv_accountName.DetailValue AS [AccountName],
					tdv_accountNumber.DetailValue AS [AccountNumber],
					'' AS [MonthlyRepaymentContract],
					tdv_monthlyRepaymentsActual.DetailValue AS [MonthlyRepaymentActual],
					tdv_remainingTerm.DetailValue AS [RemainingTerm],
					'' AS [AmountDue],
					luli_jands.ItemValue AS [JointAndSeveralLiability],
					'' AS [StateOfLiability],
					luli_restructured.ItemValue AS [Restructured],
					'' AS [RestructuringDetails],
					tdv_currentInterestRate.DetailValue AS [CurrentInterestRate],
					ISNULL(tdv_comment.DetailValue, '') AS [Comment]
			FROM TableDetailValues tdv_secured WITH (NOLOCK)
			INNER JOIN TableDetailValues tdv_comment WITH (NOLOCK) ON tdv_comment.TableRowID = tdv_secured.TableRowID AND tdv_comment.DetailFieldID = 297934
			LEFT JOIN LookupListItems luli_sec WITH (NOLOCK) ON luli_sec.LookupListItemID = tdv_secured.ValueInt
			INNER JOIN TableDetailValues tdv_Incurred6Month WITH (NOLOCK) ON tdv_Incurred6Month.TableRowID = tdv_comment.TableRowID AND tdv_Incurred6Month.DetailFieldID = 297920
			LEFT JOIN LookupListItems luli_Incurred6Month WITH (NOLOCK) ON luli_Incurred6Month.LookupListItemID = tdv_Incurred6Month.ValueInt
			INNER JOIN TableDetailValues tdv_accountName WITH (NOLOCK) ON tdv_accountName.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_accountName.DetailFieldID = 297923
			INNER JOIN TableDetailValues tdv_monthlyRepaymentsActual WITH (NOLOCK) ON tdv_monthlyRepaymentsActual.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_monthlyRepaymentsActual.DetailFieldID = 297925
			INNER JOIN TableDetailValues tdv_accountNumber WITH (NOLOCK) ON tdv_accountNumber.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_accountNumber.DetailFieldID = 297921
			INNER JOIN TableDetailValues tdv_remainingTerm WITH (NOLOCK) ON tdv_remainingTerm.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_remainingTerm.DetailFieldID = 297926
			INNER JOIN TableDetailValues tdv_jointandseveral WITH (NOLOCK) ON tdv_jointandseveral.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_jointandseveral.DetailFieldID = 297930
			LEFT JOIN LookupListItems luli_jands WITH (NOLOCK) ON luli_jands.LookupListItemID = tdv_jointandseveral.ValueInt 
			INNER JOIN TableDetailValues tdv_restructured WITH (NOLOCK) ON tdv_restructured.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_restructured.DetailFieldID = 297932
			LEFT JOIN LookupListItems luli_restructured WITH (NOLOCK) ON luli_restructured.LookupListItemID = tdv_restructured.ValueInt
			INNER JOIN TableDetailValues tdv_currentInterestRate WITH (NOLOCK) ON tdv_currentInterestRate.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_currentInterestRate.DetailFieldID = 297933
			WHERE tdv_secured.DetailFieldID = 297935
			AND tdv_secured.CustomerID = @CustomerID 
			FOR XML PATH ('PPRL'), Type
			), (
			SELECT  luli_sec.ItemValue AS [DebtSecured],(
					SELECT luli_sectype.ItemValue AS [SecurityType], tdv_other.DetailValue AS [OtherDetails], '' AS [SecurityItem]
					FROM TableDetailValues tdv_secType WITH (NOLOCK) 
					LEFT JOIN LookupListItems luli_sectype WITH (NOLOCK) ON luli_sectype.LookupListItemID = tdv_secType.ValueInt
					LEFT JOIN TableDetailValues tdv_other WITH (NOLOCK) ON tdv_other.TableRowID = tdv_secType.TableRowID AND tdv_other.DetailFieldID = 297977
					WHERE tdv_secType.TableRowID = tdv_secured.TableRowID 
					AND tdv_secType.DetailFieldID = 297976
					FOR XML PATH (''), TYPE
					) AS [DebtSecurity],
					(
						(
						SELECT luli_org.ItemValue AS [OrganisationName],
								dbo.fn_C00_ISI_ReturnXmlAddress (tdv_adl1.DetailValue, tdv_adl2.DetailValue, tdv_adl3.DetailValue, tdv_adl4.DetailValue, tdv_adl5.DetailValue, '', ISNULL(luli.ItemValue, 'Ireland')) AS [Address],
								tdv_email.DetailValue AS [Email],
								tdv_phone.DetailValue AS [PhoneNumber]
						FROM TableDetailValues tdv_orgName WITH (NOLOCK)
						LEFT JOIN LookupListItems luli_org WITH (NOLOCK) ON luli_org.LookupListItemID = tdv_orgName.ValueInt 
						INNER JOIN TableDetailValues tdv_adl1 WITH (NOLOCK) ON tdv_adl1.TableRowID = tdv_orgName.TableRowID AND tdv_adl1.DetailFieldID = 297952
						INNER JOIN TableDetailValues tdv_adl2 WITH (NOLOCK) ON tdv_adl2.TableRowID = tdv_orgName.TableRowID AND tdv_adl2.DetailFieldID = 297953
						INNER JOIN TableDetailValues tdv_adl3 WITH (NOLOCK) ON tdv_adl3.TableRowID = tdv_orgName.TableRowID AND tdv_adl3.DetailFieldID = 297954
						INNER JOIN TableDetailValues tdv_adl4 WITH (NOLOCK) ON tdv_adl4.TableRowID = tdv_orgName.TableRowID AND tdv_adl4.DetailFieldID = 297955
						INNER JOIN TableDetailValues tdv_adl5 WITH (NOLOCK) ON tdv_adl5.TableRowID = tdv_orgName.TableRowID AND tdv_adl5.DetailFieldID = 297956
						INNER JOIN TableDetailValues tdv_Country WITH (NOLOCK) ON tdv_Country.TableRowID = tdv_orgName.TableRowID AND tdv_Country.DetailFieldID = 297951
						LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_Country.ValueInt
						INNER JOIN TableDetailValues tdv_email WITH (NOLOCK) ON tdv_email.TableRowID = tdv_orgName.TableRowID AND tdv_email.DetailFieldID = 297957
						INNER JOIN TableDetailValues tdv_phone WITH (NOLOCK) ON tdv_phone.TableRowID = tdv_orgName.TableRowID AND tdv_phone.DetailFieldID = 297958
						WHERE tdv_orgName.TableRowID = tdv_secured.TableRowID 
						AND tdv_orgName.DetailFieldID = 297950
						FOR XML PATH ('Organisation'), Type
						) 
					) AS [Creditor],
					(
						(
						SELECT ''
						FOR XML PATH ('Individual'), Type
						) 
					) AS [Creditor],
					luli_Incurred6Month.ItemValue AS [Incurred6Month],
					'' AS [Incurred6MonthAmount],
					tdv_accountName.DetailValue AS [AccountName],
					tdv_accountNumber.DetailValue AS [AccountNumber],
					luli_acType.ItemValue AS [AccountType],
					tdv_type_other.DetailValue AS [OtherTypeDetails],
					'' AS [MonthlyRepaymentContract],
					tdv_monthlyRepaymentsActual.DetailValue AS [MonthlyRepaymentActual],
					tdv_purpose.DetailValue AS [PurposeOfLoan],
					'' AS [AmountDue],
					luli_jands.ItemValue AS [JointAndSeveralLiability],
					'' AS [PercentageLiability],
					luli_restructured.ItemValue AS [Restructured],
					'' AS [RestructuringDetails],
					tdv_currentInterestRate.DetailValue AS [CurrentInterestRate],
					'' AS [Comment]
			FROM TableDetailValues tdv_secured WITH (NOLOCK)
			INNER JOIN TableRows tr WITH (NOLOCK) ON tr.TableRowID = tdv_secured.TableRowID AND tr.DetailFieldID = 297763
			LEFT JOIN LookupListItems luli_sec WITH (NOLOCK) ON luli_sec.LookupListItemID = tdv_secured.ValueInt
			INNER JOIN TableDetailValues tdv_Incurred6Month WITH (NOLOCK) ON tdv_Incurred6Month.TableRowID = tdv_secured.TableRowID AND tdv_Incurred6Month.DetailFieldID = 297959
			LEFT JOIN LookupListItems luli_Incurred6Month WITH (NOLOCK) ON luli_Incurred6Month.LookupListItemID = tdv_Incurred6Month.ValueInt
			INNER JOIN TableDetailValues tdv_accountName WITH (NOLOCK) ON tdv_accountName.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_accountName.DetailFieldID = 297962
			INNER JOIN TableDetailValues tdv_monthlyRepaymentsActual WITH (NOLOCK) ON tdv_monthlyRepaymentsActual.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_monthlyRepaymentsActual.DetailFieldID = 297966
			INNER JOIN TableDetailValues tdv_accountNumber WITH (NOLOCK) ON tdv_accountNumber.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_accountNumber.DetailFieldID = 297960
			INNER JOIN TableDetailValues tdv_jointandseveral WITH (NOLOCK) ON tdv_jointandseveral.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_jointandseveral.DetailFieldID = 297971
			LEFT JOIN LookupListItems luli_jands WITH (NOLOCK) ON luli_jands.LookupListItemID = tdv_jointandseveral.ValueInt 
			INNER JOIN TableDetailValues tdv_restructured WITH (NOLOCK) ON tdv_restructured.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_restructured.DetailFieldID = 297973
			LEFT JOIN LookupListItems luli_restructured WITH (NOLOCK) ON luli_restructured.LookupListItemID = tdv_restructured.ValueInt
			INNER JOIN TableDetailValues tdv_currentInterestRate WITH (NOLOCK) ON tdv_currentInterestRate.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_currentInterestRate.DetailFieldID = 297974
			INNER JOIN TableDetailValues tdv_type WITH (NOLOCK) ON tdv_type.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_type.DetailFieldID = 297963
			LEFT JOIN LookupListItems luli_acType WITH (NOLOCK) ON luli_acType.LookupListItemID = tdv_type.ValueInt
			INNER JOIN TableDetailValues tdv_type_other WITH (NOLOCK) ON tdv_type_other.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_type_other.DetailFieldID = 297964
			INNER JOIN TableDetailValues tdv_purpose WITH (NOLOCK) ON tdv_purpose.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_purpose.DetailFieldID = 297969
			--INNER JOIN TableDetailValues tdv_percentageLiability WITH (NOLOCK) ON tdv_percentageLiability.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_percentageLiability.DetailFieldID = 
			WHERE tdv_secured.DetailFieldID = 297957
			AND tdv_secured.CustomerID = @CustomerID 
			FOR XML PATH ('FinancialInstitutions'), Type
			), (
			SELECT  luli_sec.ItemValue AS [DebtSecured],(
					SELECT luli_sectype.ItemValue AS [SecurityType], tdv_other.DetailValue AS [OtherDetails], '' AS [SecurityItem]
					FROM TableDetailValues tdv_secType WITH (NOLOCK) 
					LEFT JOIN LookupListItems luli_sectype WITH (NOLOCK) ON luli_sectype.LookupListItemID = tdv_secType.ValueInt
					LEFT JOIN TableDetailValues tdv_other WITH (NOLOCK) ON tdv_other.TableRowID = tdv_secType.TableRowID AND tdv_other.DetailFieldID = 297977
					WHERE tdv_secType.TableRowID = tdv_secured.TableRowID 
					AND tdv_secType.DetailFieldID = 297976
					FOR XML PATH (''), TYPE
					) AS [DebtSecurity],
					(
						(
						SELECT luli_org.ItemValue AS [OrganisationName],
								dbo.fn_C00_ISI_ReturnXmlAddress (tdv_adl1.DetailValue, tdv_adl2.DetailValue, tdv_adl3.DetailValue, tdv_adl4.DetailValue, tdv_adl5.DetailValue, '', ISNULL(luli.ItemValue, 'Ireland')) AS [Address],
								tdv_email.DetailValue AS [Email],
								tdv_phone.DetailValue AS [PhoneNumber]
						FROM TableDetailValues tdv_orgName WITH (NOLOCK)
						LEFT JOIN LookupListItems luli_org WITH (NOLOCK) ON luli_org.LookupListItemID = tdv_orgName.ValueInt 
						INNER JOIN TableDetailValues tdv_adl1 WITH (NOLOCK) ON tdv_adl1.TableRowID = tdv_orgName.TableRowID AND tdv_adl1.DetailFieldID = 297952
						INNER JOIN TableDetailValues tdv_adl2 WITH (NOLOCK) ON tdv_adl2.TableRowID = tdv_orgName.TableRowID AND tdv_adl2.DetailFieldID = 297953
						INNER JOIN TableDetailValues tdv_adl3 WITH (NOLOCK) ON tdv_adl3.TableRowID = tdv_orgName.TableRowID AND tdv_adl3.DetailFieldID = 297954
						INNER JOIN TableDetailValues tdv_adl4 WITH (NOLOCK) ON tdv_adl4.TableRowID = tdv_orgName.TableRowID AND tdv_adl4.DetailFieldID = 297955
						INNER JOIN TableDetailValues tdv_adl5 WITH (NOLOCK) ON tdv_adl5.TableRowID = tdv_orgName.TableRowID AND tdv_adl5.DetailFieldID = 297956
						INNER JOIN TableDetailValues tdv_Country WITH (NOLOCK) ON tdv_Country.TableRowID = tdv_orgName.TableRowID AND tdv_Country.DetailFieldID = 297951
						LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_Country.ValueInt
						INNER JOIN TableDetailValues tdv_email WITH (NOLOCK) ON tdv_email.TableRowID = tdv_orgName.TableRowID AND tdv_email.DetailFieldID = 297957
						INNER JOIN TableDetailValues tdv_phone WITH (NOLOCK) ON tdv_phone.TableRowID = tdv_orgName.TableRowID AND tdv_phone.DetailFieldID = 297958
						WHERE tdv_orgName.TableRowID = tdv_secured.TableRowID 
						AND tdv_orgName.DetailFieldID = 297950
						FOR XML PATH ('Organisation'), Type
						) 
					) AS [Creditor],
					(
						(
						SELECT ''
						FOR XML PATH ('Individual'), Type
						) 
					) AS [Creditor],
					luli_Incurred6Month.ItemValue AS [Incurred6Month],
					'' AS [Incurred6MonthAmount],
					tdv_accountName.DetailValue AS [AccountName],
					tdv_accountNumber.DetailValue AS [AccountNumber],
					'' AS [MonthlyRepaymentContract],
					tdv_monthlyRepaymentsActual.DetailValue AS [MonthlyRepaymentActual],
					tdv_purpose.DetailValue AS [PurposeOfLoan],
					'' AS [AmountDue],
					luli_jands.ItemValue AS [JointAndSeveralLiability],
					'' AS [Liability],
					luli_restructured.ItemValue AS [Restructured],
					'' AS [RestructuringDetails],
					tdv_currentInterestRate.DetailValue AS [CurrentInterestRate],
					'' AS [Comment]
			FROM TableDetailValues tdv_secured WITH (NOLOCK)
			INNER JOIN TableRows tr WITH (NOLOCK) ON tr.TableRowID = tdv_secured.TableRowID AND tr.DetailFieldID = 297764
			LEFT JOIN LookupListItems luli_sec WITH (NOLOCK) ON luli_sec.LookupListItemID = tdv_secured.ValueInt
			INNER JOIN TableDetailValues tdv_Incurred6Month WITH (NOLOCK) ON tdv_Incurred6Month.TableRowID = tdv_secured.TableRowID AND tdv_Incurred6Month.DetailFieldID = 297959
			LEFT JOIN LookupListItems luli_Incurred6Month WITH (NOLOCK) ON luli_Incurred6Month.LookupListItemID = tdv_Incurred6Month.ValueInt
			INNER JOIN TableDetailValues tdv_accountName WITH (NOLOCK) ON tdv_accountName.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_accountName.DetailFieldID = 297962
			INNER JOIN TableDetailValues tdv_monthlyRepaymentsActual WITH (NOLOCK) ON tdv_monthlyRepaymentsActual.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_monthlyRepaymentsActual.DetailFieldID = 297966
			INNER JOIN TableDetailValues tdv_accountNumber WITH (NOLOCK) ON tdv_accountNumber.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_accountNumber.DetailFieldID = 297960
			INNER JOIN TableDetailValues tdv_jointandseveral WITH (NOLOCK) ON tdv_jointandseveral.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_jointandseveral.DetailFieldID = 297971
			LEFT JOIN LookupListItems luli_jands WITH (NOLOCK) ON luli_jands.LookupListItemID = tdv_jointandseveral.ValueInt 
			INNER JOIN TableDetailValues tdv_restructured WITH (NOLOCK) ON tdv_restructured.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_restructured.DetailFieldID = 297973
			LEFT JOIN LookupListItems luli_restructured WITH (NOLOCK) ON luli_restructured.LookupListItemID = tdv_restructured.ValueInt
			INNER JOIN TableDetailValues tdv_currentInterestRate WITH (NOLOCK) ON tdv_currentInterestRate.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_currentInterestRate.DetailFieldID = 297974
			INNER JOIN TableDetailValues tdv_purpose WITH (NOLOCK) ON tdv_purpose.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_purpose.DetailFieldID = 297969
			--INNER JOIN TableDetailValues tdv_percentageLiability WITH (NOLOCK) ON tdv_percentageLiability.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_percentageLiability.DetailFieldID = 
			WHERE tdv_secured.DetailFieldID = 297975
			AND tdv_secured.CustomerID = @CustomerID 
			FOR XML PATH ('CreditUnion'), Type
			), (
			SELECT  luli_sec.ItemValue AS [DebtSecured],(
					SELECT luli_sectype.ItemValue AS [SecurityType], tdv_other.DetailValue AS [OtherDetails], '' AS [SecurityItem]
					FROM TableDetailValues tdv_secType WITH (NOLOCK) 
					LEFT JOIN LookupListItems luli_sectype WITH (NOLOCK) ON luli_sectype.LookupListItemID = tdv_secType.ValueInt
					LEFT JOIN TableDetailValues tdv_other WITH (NOLOCK) ON tdv_other.TableRowID = tdv_secType.TableRowID AND tdv_other.DetailFieldID = 298004
					WHERE tdv_secType.TableRowID = tdv_secured.TableRowID 
					AND tdv_secType.DetailFieldID = 298003
					FOR XML PATH (''), TYPE
					) AS [DebtSecurity],
					(
						(
						SELECT luli_org.ItemValue AS [OrganisationName],
								dbo.fn_C00_ISI_ReturnXmlAddress (tdv_adl1.DetailValue, tdv_adl2.DetailValue, tdv_adl3.DetailValue, tdv_adl4.DetailValue, tdv_adl5.DetailValue, '', ISNULL(luli.ItemValue, 'Ireland')) AS [Address],
								tdv_email.DetailValue AS [Email],
								tdv_phone.DetailValue AS [PhoneNumber]
						FROM TableDetailValues tdv_orgName WITH (NOLOCK)
						LEFT JOIN LookupListItems luli_org WITH (NOLOCK) ON luli_org.LookupListItemID = tdv_orgName.ValueInt 
						INNER JOIN TableDetailValues tdv_adl1 WITH (NOLOCK) ON tdv_adl1.TableRowID = tdv_orgName.TableRowID AND tdv_adl1.DetailFieldID = 297987
						INNER JOIN TableDetailValues tdv_adl2 WITH (NOLOCK) ON tdv_adl2.TableRowID = tdv_orgName.TableRowID AND tdv_adl2.DetailFieldID = 297988
						INNER JOIN TableDetailValues tdv_adl3 WITH (NOLOCK) ON tdv_adl3.TableRowID = tdv_orgName.TableRowID AND tdv_adl3.DetailFieldID = 297989
						INNER JOIN TableDetailValues tdv_adl4 WITH (NOLOCK) ON tdv_adl4.TableRowID = tdv_orgName.TableRowID AND tdv_adl4.DetailFieldID = 297990
						INNER JOIN TableDetailValues tdv_adl5 WITH (NOLOCK) ON tdv_adl5.TableRowID = tdv_orgName.TableRowID AND tdv_adl5.DetailFieldID = 297991
						INNER JOIN TableDetailValues tdv_Country WITH (NOLOCK) ON tdv_Country.TableRowID = tdv_orgName.TableRowID AND tdv_Country.DetailFieldID = 297986
						LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_Country.ValueInt
						INNER JOIN TableDetailValues tdv_email WITH (NOLOCK) ON tdv_email.TableRowID = tdv_orgName.TableRowID AND tdv_email.DetailFieldID = 297992
						INNER JOIN TableDetailValues tdv_phone WITH (NOLOCK) ON tdv_phone.TableRowID = tdv_orgName.TableRowID AND tdv_phone.DetailFieldID = 297993
						WHERE tdv_orgName.TableRowID = tdv_secured.TableRowID 
						AND tdv_orgName.DetailFieldID = 297985
						FOR XML PATH ('Organisation'), Type
						) 
					) AS [Creditor],
					(
						(
						SELECT ''
						FOR XML PATH ('Individual'), Type
						) 
					) AS [Creditor],
					luli_Incurred6Month.ItemValue AS [Incurred6Month],
					'' AS [Incurred6MonthAmount],
					luli_acType.ItemValue AS [Type],
					tdv_type_other.DetailValue AS [OtherTypeDetails],
					luli_permitted.ItemValue AS [Permitted],
					'' AS [PermittedStatus],
					'' AS [ConsentFromRevenue],
					'' AS [AmountDue],
					'' AS [CurrentMonthlyPaymentDue],
					'' AS [MonthlyRepaymentActual],
					'' AS [PreferentialAmount],
					'' AS [InstallmentArrangement],
					'' AS [Comment]
			FROM TableDetailValues tdv_secured WITH (NOLOCK)
			LEFT JOIN LookupListItems luli_sec WITH (NOLOCK) ON luli_sec.LookupListItemID = tdv_secured.ValueInt
			INNER JOIN TableDetailValues tdv_Incurred6Month WITH (NOLOCK) ON tdv_Incurred6Month.TableRowID = tdv_secured.TableRowID AND tdv_Incurred6Month.DetailFieldID = 297994
			LEFT JOIN LookupListItems luli_Incurred6Month WITH (NOLOCK) ON luli_Incurred6Month.LookupListItemID = tdv_Incurred6Month.ValueInt
			INNER JOIN TableDetailValues tdv_type WITH (NOLOCK) ON tdv_type.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_type.DetailFieldID = 297995
			LEFT JOIN LookupListItems luli_acType WITH (NOLOCK) ON luli_acType.LookupListItemID = tdv_type.ValueInt
			INNER JOIN TableDetailValues tdv_type_other WITH (NOLOCK) ON tdv_type_other.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_type_other.DetailFieldID = 297996
			INNER JOIN TableDetailValues tdv_permitted WITH (NOLOCK) ON tdv_permitted.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_permitted.DetailFieldID = 297997
			LEFT JOIN LookupListItems luli_permitted WITH (NOLOCK) ON luli_permitted.LookupListItemID = tdv_permitted.ValueInt
			INNER JOIN TableDetailValues tdv_comments WITH (NOLOCK) ON tdv_comments.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_comments.DetailFieldID = 298006
			WHERE tdv_secured.DetailFieldID = 298002
			AND tdv_secured.CustomerID = @CustomerID 
			FOR XML PATH ('DebtExcludableRevenue'), Type
			), (
			SELECT  luli_sec.ItemValue AS [DebtSecured],(
					SELECT luli_sectype.ItemValue AS [SecurityType], tdv_other.DetailValue AS [OtherDetails], '' AS [SecurityItem]
					FROM TableDetailValues tdv_secType WITH (NOLOCK) 
					LEFT JOIN LookupListItems luli_sectype WITH (NOLOCK) ON luli_sectype.LookupListItemID = tdv_secType.ValueInt
					LEFT JOIN TableDetailValues tdv_other WITH (NOLOCK) ON tdv_other.TableRowID = tdv_secType.TableRowID AND tdv_other.DetailFieldID = 298029
					WHERE tdv_secType.TableRowID = tdv_secured.TableRowID 
					AND tdv_secType.DetailFieldID = 298028
					FOR XML PATH (''), TYPE
					) AS [DebtSecurity],
					(
						(
						SELECT luli_org.ItemValue AS [OrganisationName],
								dbo.fn_C00_ISI_ReturnXmlAddress (tdv_adl1.DetailValue, tdv_adl2.DetailValue, tdv_adl3.DetailValue, tdv_adl4.DetailValue, tdv_adl5.DetailValue, '', ISNULL(luli.ItemValue, 'Ireland')) AS [Address],
								tdv_email.DetailValue AS [Email],
								tdv_phone.DetailValue AS [PhoneNumber]
						FROM TableDetailValues tdv_orgName WITH (NOLOCK)
						LEFT JOIN LookupListItems luli_org WITH (NOLOCK) ON luli_org.LookupListItemID = tdv_orgName.ValueInt 
						INNER JOIN TableDetailValues tdv_adl1 WITH (NOLOCK) ON tdv_adl1.TableRowID = tdv_orgName.TableRowID AND tdv_adl1.DetailFieldID = 298014
						INNER JOIN TableDetailValues tdv_adl2 WITH (NOLOCK) ON tdv_adl2.TableRowID = tdv_orgName.TableRowID AND tdv_adl2.DetailFieldID = 298015
						INNER JOIN TableDetailValues tdv_adl3 WITH (NOLOCK) ON tdv_adl3.TableRowID = tdv_orgName.TableRowID AND tdv_adl3.DetailFieldID = 298016
						INNER JOIN TableDetailValues tdv_adl4 WITH (NOLOCK) ON tdv_adl4.TableRowID = tdv_orgName.TableRowID AND tdv_adl4.DetailFieldID = 298017
						INNER JOIN TableDetailValues tdv_adl5 WITH (NOLOCK) ON tdv_adl5.TableRowID = tdv_orgName.TableRowID AND tdv_adl5.DetailFieldID = 298018
						INNER JOIN TableDetailValues tdv_Country WITH (NOLOCK) ON tdv_Country.TableRowID = tdv_orgName.TableRowID AND tdv_Country.DetailFieldID = 298013
						LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv_Country.ValueInt
						INNER JOIN TableDetailValues tdv_email WITH (NOLOCK) ON tdv_email.TableRowID = tdv_orgName.TableRowID AND tdv_email.DetailFieldID = 298019
						INNER JOIN TableDetailValues tdv_phone WITH (NOLOCK) ON tdv_phone.TableRowID = tdv_orgName.TableRowID AND tdv_phone.DetailFieldID = 298020
						WHERE tdv_orgName.TableRowID = tdv_secured.TableRowID 
						AND tdv_orgName.DetailFieldID = 298012
						FOR XML PATH ('Organisation'), Type
						) 
					) AS [Creditor],
					(
						(
						SELECT ''
						FOR XML PATH ('Individual'), Type
						) 
					) AS [Creditor],
					luli_Incurred6Month.ItemValue AS [Incurred6Month],
					'' AS [Incurred6MonthAmount],
					luli_acType.ItemValue AS [Type],
					tdv_type_other.DetailValue AS [OtherTypeDetails],
					luli_permitted.ItemValue AS [Permitted],
					'' AS [PermittedStatus],
					'' AS [ConsentFromRevenue],
					'' AS [AmountDue],
					'' AS [CurrentMonthlyPaymentDue],
					'' AS [MonthlyRepaymentActual],
					'' AS [PreferentialAmount],
					'' AS [InstallmentArrangement],
					'' AS [Comment]
			FROM TableDetailValues tdv_secured WITH (NOLOCK)
			LEFT JOIN LookupListItems luli_sec WITH (NOLOCK) ON luli_sec.LookupListItemID = tdv_secured.ValueInt
			INNER JOIN TableDetailValues tdv_Incurred6Month WITH (NOLOCK) ON tdv_Incurred6Month.TableRowID = tdv_secured.TableRowID AND tdv_Incurred6Month.DetailFieldID = 298021
			LEFT JOIN LookupListItems luli_Incurred6Month WITH (NOLOCK) ON luli_Incurred6Month.LookupListItemID = tdv_Incurred6Month.ValueInt
			INNER JOIN TableDetailValues tdv_type WITH (NOLOCK) ON tdv_type.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_type.DetailFieldID = 298022
			LEFT JOIN LookupListItems luli_acType WITH (NOLOCK) ON luli_acType.LookupListItemID = tdv_type.ValueInt
			INNER JOIN TableDetailValues tdv_type_other WITH (NOLOCK) ON tdv_type_other.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_type_other.DetailFieldID = 298023
			INNER JOIN TableDetailValues tdv_permitted WITH (NOLOCK) ON tdv_permitted.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_permitted.DetailFieldID = 298024
			LEFT JOIN LookupListItems luli_permitted WITH (NOLOCK) ON luli_permitted.LookupListItemID = tdv_permitted.ValueInt
			INNER JOIN TableDetailValues tdv_comments WITH (NOLOCK) ON tdv_comments.TableRowID = tdv_Incurred6Month.TableRowID AND tdv_comments.DetailFieldID = 298031
			WHERE tdv_secured.DetailFieldID = 298027
			AND tdv_secured.CustomerID = @CustomerID 
			FOR XML PATH ('ExcludableNonRevenue'), Type
			)
		FOR XML PATH ('')
		)
	

	RETURN @DebtXML
END





GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_XmlPush_Debt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ISI_XmlPush_Debt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_XmlPush_Debt] TO [sp_executeall]
GO
