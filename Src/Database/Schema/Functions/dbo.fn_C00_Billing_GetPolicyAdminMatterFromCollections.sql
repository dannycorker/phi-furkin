SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		James Lewis
-- Create date: 2018-08-17
-- Description:	Takes the Policy Admin MatterId and returns the associated collections matter 
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_Billing_GetPolicyAdminMatterFromCollections]
(
	 @MatterID		INT
)
RETURNS INT
AS
BEGIN
	
	DECLARE @PolicyAdminsMatterID INT 
	
	SELECT @PolicyAdminsMatterID = p.ObjectID
	FROM PurchasedProduct p WITH ( NOLOCK ) 
	INNER JOIN Account a WITH ( NOLOCK ) on a.AccountID = p.AccountID
	Where a.ObjectID = @MatterID


	RETURN @PolicyAdminsMatterID


END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_Billing_GetPolicyAdminMatterFromCollections] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_Billing_GetPolicyAdminMatterFromCollections] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_Billing_GetPolicyAdminMatterFromCollections] TO [sp_executeall]
GO
