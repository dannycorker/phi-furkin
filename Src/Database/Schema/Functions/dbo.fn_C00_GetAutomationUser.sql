SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-07-17
-- Description:	Return the Aquarium Automation User
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_GetAutomationUser]
(
	@ClientID INT
)
RETURNS INT
AS
BEGIN

	DECLARE @AutomationUser VARCHAR(200)

	SELECT @AutomationUser = ClientPersonnelID
	FROM dbo.ClientPersonnel WITH (NOLOCK) 
	WHERE ClientID = @ClientID
	AND FirstName = 'Aquarium'
	AND LastName = 'Automation'

	RETURN @AutomationUser

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetAutomationUser] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_GetAutomationUser] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetAutomationUser] TO [sp_executeall]
GO
