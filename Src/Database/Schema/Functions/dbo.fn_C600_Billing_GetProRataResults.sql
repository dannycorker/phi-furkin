SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-07-16
-- Description:	Perform a pro-rata calculation
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Billing_GetProRataResults]
(
	 @StartDate								DATE	= '2017-01-01'
	,@EndDate								DATE	= '2017-12-31'
	,@ChangeDate							DATE	= '2017-07-16'
	,@ChangeDateIsFirstDayOfSecondPeriod	BIT		= 0
	,@ValueGross							DECIMAL(18,2) = 120.00
	,@ValueNet								DECIMAL(18,2) = 100.00
	,@ValueTax								DECIMAL(18,2) = 20.00
)
RETURNS 
	@Results TABLE 
	(
	 Period1Gross	DECIMAL(18,2)
	,Period1Net		DECIMAL(18,2)
	,Period1Tax		DECIMAL(18,2)
	,Period2Gross	DECIMAL(18,2)
	,Period2Net		DECIMAL(18,2)
	,Period2Tax		DECIMAL(18,2)
	)
AS
BEGIN
 
	DECLARE	 @Period1Gross	DECIMAL(18,2) = 0.00
			,@Period1Net	DECIMAL(18,2) = 0.00
			,@Period1Tax	DECIMAL(18,2) = 0.00
			,@Period2Gross	DECIMAL(18,2) = 0.00
			,@Period2Net	DECIMAL(18,2) = 0.00
			,@Period2Tax	DECIMAL(18,2) = 0.00
			,@Multiplier	DECIMAL(18,10)
			,@TermLength	INT
			,@Period1Length	INT
			,@Period2Length	INT

	SELECT	 @TermLength	= DATEDIFF(DAY,@StartDate,@EndDate)		+ (1)
			,@Period1Length	= DATEDIFF(DAY,@StartDate,@ChangeDate)	+ (1-@ChangeDateIsFirstDayOfSecondPeriod)
			,@Period2Length	= DATEDIFF(DAY,@ChangeDate,@EndDate)	+ (@ChangeDateIsFirstDayOfSecondPeriod)

	SELECT	 @Multiplier = CASE WHEN @TermLength = 0 THEN 0 ELSE CAST(@Period1Length as DECIMAL(18,10)) / CAST(@TermLength as DECIMAL(18,10)) END

	SELECT	 @Period1Gross	= @ValueGross	* @Multiplier
			,@Period1Tax	= @ValueTax		* @Multiplier
			,@Period1Net	= @Period1Gross - @Period1Tax
			,@Period2Gross	= @ValueGross	- @Period1Gross
			,@Period2Tax	= @ValueTax		- @Period1Tax
			,@Period2Net	= @Period2Gross - @Period2Tax
	
	INSERT @Results ( Period1Gross, Period1Net, Period1Tax, Period2Gross, Period2Net, Period2Tax )
	VALUES ( @Period1Gross, @Period1Net, @Period1Tax, @Period2Gross, @Period2Net, @Period2Tax )
	
	RETURN

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Billing_GetProRataResults] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_Billing_GetProRataResults] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Billing_GetProRataResults] TO [sp_executeall]
GO
