SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-07-03
-- Description:	Count Table Rows by FieldID and ObjectID (Customer/Lead/Case/Matter)
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_CountTableRowsByFieldIDandObjectID]
(
	@ObjectID INT,
	@DetailFieldID INT
)
RETURNS INT
AS
BEGIN

	DECLARE @ReturnValue INT = 0,
			@DetailFieldSubTypeID INT,
			@ClientID INT
	
	SELECT	@DetailFieldSubTypeID = df.LeadOrMatter,
			@ClientID = df.ClientID
	FROM dbo.DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @DetailFieldID
	
	SELECT @ReturnValue = COUNT(*) 
	FROM dbo.TableRows tr WITH (NOLOCK) 
	WHERE tr.DetailFieldID = @DetailFieldID
	AND tr.ClientID = @ClientID
	AND @ObjectID = CASE @DetailFieldSubTypeID	
					WHEN 1 THEN tr.LeadID
					WHEN 2 THEN tr.MatterID
					WHEN 10 THEN tr.CustomerID
					WHEN 11 THEN tr.CaseID
					END 

	RETURN @ReturnValue

END









GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_CountTableRowsByFieldIDandObjectID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_CountTableRowsByFieldIDandObjectID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_CountTableRowsByFieldIDandObjectID] TO [sp_executeall]
GO
