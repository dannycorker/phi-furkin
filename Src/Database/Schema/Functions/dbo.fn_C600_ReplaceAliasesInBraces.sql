SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-02-09
-- Description:	Substitutes the curly bracket/braces text that equals Aquarium detail field aliases (including resource lists)
--				NB. This started as the fn_C00 function of the same name, but SIG wanted curly bracket delimiters and 
--				to use detail fields across lead types where these are unambiguous
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_ReplaceAliasesInBraces]
(
	@ReturnValue VARCHAR(MAX),
	@ObjectID INT,
	@ObjectType VARCHAR(30)
)
RETURNS VARCHAR(MAX)
AS
BEGIN

--declare
--	@Text VARCHAR(MAX),
--	@ObjectID INT =  40026355,--40026355,--30019279,--50026379,
--	@ObjectType VARCHAR(30) = 'Case'


	/* 

		Only supports:  * Matter, Case, and Lead level objects and aliases.
						* Only aliases of the type {Authorised Third Party 1 Address} & {!A:White Label Details, BIC}

		Part 2 of an alias of the type {!A:White Label Details, BIC} is assumed to be an alias in the Resources&Tables leadtype 
						
		If the alias occurs in more than one lead type, one of which is an alias for this ObjectID's lead type,
		use this ObjectID's leadtype.
		
		If the alias occurs in more than one lead type not including this ObjectID's leadtype, use the one for the lead type
		that has an object linked to this ObjectID implicitly or explicitly (e.g. policy admin matter is implicitly linked to 
		a claim matter).  If more than one is linked in this way, do not substitute. 
		If none are linked in this way, do not substitute.	
		
		If more than one substitution value is possible, use the earliest (lowest value substitution object ID), e.g. a collections
		matter may be linked to more than one policy admin matter.
		
		If 'no substitution' is the decision, replace the merge code with blank.
	
	*/

	DECLARE 
			@AliasPart1 VARCHAR(200),
			@AliasPart2 VARCHAR(200),
			@ClientID INT = 384, -- TODO
			@FullAlias VARCHAR(200),
			@iStart INT=1,
			@iEnd INT,
			@ObjectCaseID INT,
			@ObjectLeadID INT,
			@ObjectLeadTypeID INT,
			@ObjectTypeID INT,
			@ObjectMatterID INT, 
			@RTLeadTypeID INT,
			@strLen INT
			
	DECLARE @Targets TABLE ( FullAlias VARCHAR(200), 
							 AliasPart1 VARCHAR(200),
							 AliasPart1DetailFieldID INT,
							 AliasPart1LeadTypeID INT,
							 AliasPart2 VARCHAR(200),
							 AliasPart2DetailFieldID INT,
							 AliasObjectID INT,
							 AliasObjectSubTypeID INT,
							 AliasValue VARCHAR(200)
						   ) 

	/* 
	   We only allow unambiguous substitutions as follows.  If more than 1 of these is possible for an alias
	   any where the target is in the object's lead type takes precedence. If more than one and none is in the
	   object's lead type, then the substitution becomes ambiguous and is not made.  
	*/

	DECLARE @AllowedLinks TABLE ( SourceLeadType INT, SourceType INT, TargetLeadType INT, TargetType INT, TargetIsFromInLeadTypeRelationship INT )
	INSERT INTO @AllowedLinks VALUES
	(1492,1,1493,1,0),  -- Policy Admin to Collections
	(1492,1,1493,2,0),
	(1492,1,1493,11,0),
	(1492,2,1493,1,0),
	(1492,2,1493,2,0),
	(1492,2,1493,11,0),
	(1492,11,1493,1,0),
	(1492,11,1493,2,0),
	(1492,11,1493,11,0),
	(1490,1,1492,1,1),  -- Claim to Policy Admin
	(1490,1,1492,2,1),
	(1490,1,1492,11,1),
	(1490,2,1492,1,1),
	(1490,2,1492,2,1),
	(1490,2,1492,11,1),
	(1490,11,1492,1,1),
	(1490,11,1492,2,1),
	(1490,11,1492,11,1)
	
	-- find the lead type of the object, and all object IDs for each level (matter, case, lead)
	SELECT TOP 1 @ObjectLeadTypeID=l.LeadTypeID,
				@ObjectLeadID=m.LeadID,
				@ObjectCaseID=m.CaseID,
				@ObjectMatterID=m.MatterID, 
				@ClientID=l.ClientID,
				@ObjectTypeID= CASE @ObjectType 
										WHEN 'Lead' THEN 1
										WHEN 'Case' THEN 11
										WHEN 'Matter' THEN 2
										END
	FROM Lead l WITH (NOLOCK) 
	INNER JOIN Matter m WITH (NOLOCK) ON l.LeadID=m.LeadID
	WHERE ( @ObjectType='Matter' AND m.MatterID=@ObjectID ) OR
		  ( @ObjectType='Case' AND m.CaseID=@ObjectID ) OR
		  ( @ObjectType='Lead' AND m.LeadID=@ObjectID )
	ORDER BY m.MatterID

	-- load full aliases into Targets table

	SELECT @strLen=LEN(@ReturnValue)

	WHILE @iStart < @strLen
	BEGIN

		SELECT @iStart=CHARINDEX('{',@ReturnValue,@iStart),
				@iEnd=CHARINDEX('}',@ReturnValue,@iStart)
		
		IF @iStart=0 OR @iEnd=0 BREAK

		SELECT @FullAlias=SUBSTRING(@ReturnValue,@iStart,@iEnd-@iStart+1)
		
		IF CHARINDEX(',',@FullAlias) = 0
			SELECT  @AliasPart1=LTRIM(RTRIM(SUBSTRING(@ReturnValue,@iStart+1,@iEnd-@iStart-1))),
					@AliasPart2=''
		ELSE
			SELECT  @AliasPart1=LTRIM(RTRIM(SUBSTRING(@FullAlias,2,CHARINDEX(',',@FullAlias)-2))),
					@AliasPart2=LTRIM(RTRIM(SUBSTRING(@FullAlias,CHARINDEX(',',@FullAlias)+2,LEN(@FullAlias)-CHARINDEX(',',@FullAlias)-2)))

		INSERT INTO @Targets ( FullAlias, AliasPart1, AliasPart2 )
		SELECT @FullAlias,@AliasPart1,@AliasPart2
		WHERE NOT EXISTS ( SELECT * FROM @Targets WHERE FullAlias=@FullAlias)	

		SELECT @iStart=@iEnd, @iEnd=0

	END	

	-- find alias lead type id and detailfieldid
	-- part 2 alias names first ( always from Resources and Tables lead type )
	UPDATE t
	SET AliasPart2DetailFieldID=dfa.DetailFieldID
	FROM @Targets t
	INNER JOIN DetailFieldAlias dfa WITH (NOLOCK) ON t.AliasPart2=dfa.DetailFieldAlias
	WHERE dfa.LeadTypeID IS NULL AND dfa.ClientID=@ClientID
	
	-- part 1 aliases for this object's leadtype
	UPDATE t
	SET AliasPart1DetailFieldID=dfa.DetailFieldID,
		AliasPart1LeadTypeID=dfa.LeadTypeID,
		AliasObjectSubTypeID=df.LeadOrMatter
	FROM @Targets t
	INNER JOIN DetailFieldAlias dfa WITH (NOLOCK) ON t.AliasPart1=dfa.DetailFieldAlias
	INNER JOIN LeadType lt WITH (NOLOCK) ON dfa.LeadTypeID=lt.LeadTypeID
	INNER JOIN DetailFields df WITH (NOLOCK) ON dfa.DetailFieldID=df.DetailFieldID
	WHERE lt.LeadTypeID=@ObjectLeadTypeID AND t.AliasPart1DetailFieldID IS NULL
	AND ( t.AliasPart2='' OR df.QuestionTypeID=14 ) -- if part 2 isn't blank part 1 must be an RL
	AND df.LeadOrMatter IN (1,2,11) -- only lead, case or matter detailvalues
	
	-- part 1 aliases in linked lead types
	UPDATE t
	SET AliasPart1DetailFieldID=dfa.DetailFieldID,
		AliasPart1LeadTypeID=dfa.LeadTypeID,
		AliasObjectSubTypeID=df.LeadOrMatter
	FROM @Targets t
	INNER JOIN DetailFieldAlias dfa WITH (NOLOCK) ON t.AliasPart1=dfa.DetailFieldAlias
	INNER JOIN @AllowedLinks ltrd ON dfa.LeadTypeID=ltrd.TargetLeadType
	INNER JOIN DetailFields df WITH (NOLOCK) ON dfa.DetailFieldID=df.DetailFieldID
	WHERE ltrd.SourceLeadType=@ObjectLeadTypeID AND ltrd.SourceType=@ObjectTypeID AND t.AliasPart1DetailFieldID IS NULL
	AND NOT EXISTS ( SELECT * FROM @Targets t2
		INNER JOIN DetailFieldAlias dfa2 WITH (NOLOCK) ON t2.AliasPart2=dfa2.DetailFieldAlias
		INNER JOIN @AllowedLinks ltrd2 ON dfa2.LeadTypeID=ltrd2.TargetLeadType
		WHERE ltrd2.SourceLeadType=@ObjectLeadTypeID AND ltrd2.SourceType=@ObjectTypeID 
		AND t2.AliasPart1DetailFieldID IS NULL
		AND dfa.LeadTypeID<>dfa2.LeadTypeID) -- only if alias is definitive
	AND ( t.AliasPart2='' OR df.QuestionTypeID=14 ) -- if part 2 isn't blank part 1 must be an RL
	AND df.LeadOrMatter IN (1,2,11) -- only lead, case or matter detailvalues
		
	-- now get the target object ID
	-- targets in this Object's lead type
	UPDATE t
	SET AliasObjectID=CASE AliasObjectSubTypeID 
						WHEN 1 THEN @ObjectLeadID
						WHEN 2 THEN @ObjectMatterID
						WHEN 11 THEN @ObjectCaseID 
						END 
	FROM @Targets t
	WHERE t.AliasPart1LeadTypeID=@ObjectLeadTypeID
	AND t.AliasPart1DetailFieldID IS NOT NULL	

	-- targets in other lead type
	UPDATE t
	SET AliasObjectID=CASE AliasObjectSubTypeID 
						WHEN 1 THEN targetm.LeadID
						WHEN 2 THEN targetm.MatterID
						WHEN 11 THEN targetm.CaseID 
						END 
	FROM @Targets t
	INNER JOIN @AllowedLinks al ON al.SourceLeadType=@ObjectLeadTypeID 
									AND al.SourceType=@ObjectTypeID
									AND al.TargetLeadType=t.AliasPart1LeadTypeID
									AND al.TargetType=t.AliasObjectSubTypeID
	INNER JOIN LeadTypeRelationship ltr WITH (NOLOCK) ON 
										( ( al.TargetIsFromInLeadTypeRelationship = 1
											 AND ( ltr.ToMatterID = @ObjectMatterID OR
												   ltr.ToCaseID = @ObjectCaseID OR
												   ltr.ToLeadID = @ObjectLeadID )
										  )	
										  OR
										  ( al.TargetIsFromInLeadTypeRelationship = 0
											 AND ( ltr.FromMatterID = @ObjectMatterID OR
												   ltr.FromCaseID = @ObjectCaseID OR
												   ltr.FromLeadID = @ObjectLeadID )
										  )
										)							

	INNER JOIN Matter targetm WITH (NOLOCK) ON ltr.FromMatterID=targetm.MatterID OR
												ltr.FromCaseID=targetm.CaseID OR
												ltr.FromLeadID=targetm.LeadID OR
												ltr.ToMatterID=targetm.MatterID OR
												ltr.ToCaseID=targetm.CaseID OR
												ltr.ToLeadID=targetm.LeadID  
	WHERE t.AliasPart1LeadTypeID<>@ObjectLeadTypeID
	AND t.AliasPart1DetailFieldID IS NOT NULL
	AND ( ( ltr.ToLeadTypeID=@ObjectLeadTypeID AND al.TargetIsFromInLeadTypeRelationship = 1)
	    OR
	    ( ltr.FromLeadTypeID=@ObjectLeadTypeID AND al.TargetIsFromInLeadTypeRelationship = 0) )
	AND ( ( ltr.ToLeadTypeID=t.AliasPart1LeadTypeID AND al.TargetIsFromInLeadTypeRelationship = 0)
	    OR
	    ( ltr.FromLeadTypeID=t.AliasPart1LeadTypeID AND al.TargetIsFromInLeadTypeRelationship = 1) )

	-- finslly get the values for substitution
	-- Lead
	UPDATE t
	SET AliasValue=CASE WHEN t.AliasPart2DetailFieldID IS NOT NULL THEN 
					 CASE rldf.QuestionTypeID 
						WHEN 4 THEN rlli.ItemValue
						WHEN 5 THEN CONVERT(VARCHAR(10),rldv.ValueDate,103)
						ELSE rldv.DetailValue
						END
				   ELSE 
					 CASE df.QuestionTypeID 
						WHEN 4 THEN dvli.ItemValue
						WHEN 5 THEN CONVERT(VARCHAR(10),dv.ValueDate,103)
						ELSE dv.DetailValue
						END
				   END
	FROM @Targets t
	INNER JOIN LeadDetailValues dv WITH (NOLOCK) ON t.AliasPart1DetailFieldID=dv.DetailFieldID
	INNER JOIN DetailFields df WITH (NOLOCK) ON dv.DetailFieldID=df.DetailFieldID
	LEFT JOIN LookupListItems dvli WITH (NOLOCK) ON dv.ValueInt=dvli.LookupListItemID
	LEFT JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID=dv.ValueInt AND t.AliasPart2DetailFieldID=rldv.DetailFieldID
	LEFT JOIN DetailFields rldf WITH (NOLOCK) ON rldv.DetailFieldID=rldf.DetailFieldID
	LEFT JOIN LookupListItems rlli WITH (NOLOCK) ON rldv.ValueInt=rlli.LookupListItemID
	WHERE t.AliasObjectID IS NOT NULL
	AND t.AliasObjectSubTypeID=1
	AND dv.LeadID=t.AliasObjectID
	
	-- Matter		
	UPDATE t
	SET AliasValue=CASE WHEN t.AliasPart2DetailFieldID IS NOT NULL THEN 
					 CASE rldf.QuestionTypeID 
						WHEN 4 THEN rlli.ItemValue
						WHEN 5 THEN CONVERT(VARCHAR(10),rldv.ValueDate,103)
						ELSE rldv.DetailValue
						END
				   ELSE 
					 CASE df.QuestionTypeID 
						WHEN 4 THEN dvli.ItemValue
						WHEN 5 THEN CONVERT(VARCHAR(10),dv.ValueDate,103)
						ELSE dv.DetailValue
						END
				   END
	FROM @Targets t
	INNER JOIN MatterDetailValues dv WITH (NOLOCK) ON t.AliasPart1DetailFieldID=dv.DetailFieldID
	INNER JOIN DetailFields df WITH (NOLOCK) ON dv.DetailFieldID=df.DetailFieldID
	LEFT JOIN LookupListItems dvli WITH (NOLOCK) ON dv.ValueInt=dvli.LookupListItemID
	LEFT JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID=dv.ValueInt AND t.AliasPart2DetailFieldID=rldv.DetailFieldID
	LEFT JOIN DetailFields rldf WITH (NOLOCK) ON rldv.DetailFieldID=rldf.DetailFieldID
	LEFT JOIN LookupListItems rlli WITH (NOLOCK) ON rldv.ValueInt=rlli.LookupListItemID
	WHERE t.AliasObjectID IS NOT NULL
	AND t.AliasObjectSubTypeID=2
	AND dv.MatterID=t.AliasObjectID
	
	-- Case		
	UPDATE t
	SET AliasValue=CASE WHEN t.AliasPart2DetailFieldID IS NOT NULL THEN 
					 CASE rldf.QuestionTypeID 
						WHEN 4 THEN rlli.ItemValue
						WHEN 5 THEN CONVERT(VARCHAR(10),rldv.ValueDate,103)
						ELSE rldv.DetailValue
						END
				   ELSE 
					 CASE df.QuestionTypeID 
						WHEN 4 THEN dvli.ItemValue
						WHEN 5 THEN CONVERT(VARCHAR(10),dv.ValueDate,103)
						ELSE dv.DetailValue
						END
				   END
	FROM @Targets t
	INNER JOIN CaseDetailValues dv WITH (NOLOCK) ON t.AliasPart1DetailFieldID=dv.DetailFieldID
	INNER JOIN DetailFields df WITH (NOLOCK) ON dv.DetailFieldID=df.DetailFieldID
	LEFT JOIN LookupListItems dvli WITH (NOLOCK) ON dv.ValueInt=dvli.LookupListItemID
	LEFT JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID=dv.ValueInt AND t.AliasPart2DetailFieldID=rldv.DetailFieldID
	LEFT JOIN DetailFields rldf WITH (NOLOCK) ON rldv.DetailFieldID=rldf.DetailFieldID
	LEFT JOIN LookupListItems rlli WITH (NOLOCK) ON rldv.ValueInt=rlli.LookupListItemID
	WHERE t.AliasObjectID IS NOT NULL
	AND t.AliasObjectSubTypeID=11
	AND dv.CaseID=t.AliasObjectID

	-- finally substitute unmatched aliases with blank	
	UPDATE t
	SET AliasValue=''
	FROM @Targets t
	WHERE t.AliasValue IS NULL
	 	
	-- ... and substitute
	SELECT @ReturnValue = REPLACE(@ReturnValue,t.FullAlias,t.AliasValue) 
	FROM @Targets t 
	WHERE t.AliasValue IS NOT NULL	
	
--PRINT @ReturnValue
	RETURN @ReturnValue

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_ReplaceAliasesInBraces] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_ReplaceAliasesInBraces] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_ReplaceAliasesInBraces] TO [sp_executeall]
GO
