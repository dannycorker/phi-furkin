SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds (with Cathal Sherry)
-- Create date: 2017-12-30
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_RulesEngine_GetAdminFee]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS DECIMAL(18,4)
AS
BEGIN

	DECLARE  @ReturnValue	DECIMAL(18,4)	= 0.00
			,@NewBusiness	BIT
			,@Affinity		INT

	SELECT @NewBusiness = o.AnyValue2
	FROM @Overrides o 
	WHERE o.AnyValue1 = 'NewBusiness'

	SELECT @Affinity = o.AnyValue2
	FROM @Overrides o 
	WHERE o.AnyValue1 = '170127' /*Short Code*/

	SELECT TOP 1 @ReturnValue = tdvValue.ValueMoney
	FROM TableRows tr WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvType WITH (NOLOCK) on tdvType.TableRowID = tr.TableRowID AND tdvType.DetailFieldID = 179698 /*Department process*/
	INNER JOIN dbo.TableDetailValues tdvAffinity WITH (NOLOCK) on tdvAffinity.TableRowID = tr.TableRowID AND tdvAffinity.DetailFieldID = 179697 /*Affinity*/
	INNER JOIN dbo.TableDetailValues tdvValue WITH (NOLOCK) on tdvValue.TableRowID = tr.TableRowID AND tdvValue.DetailFieldID = 179699 /*Amount*/
	WHERE tr.DetailFieldID = 179701 /*Administration Fees*/
	AND (
			( tdvType.ValueInt = 76207 /*New Business*/ AND @NewBusiness = 1 )
			OR
			( tdvType.ValueInt = 76209 /*Renewal*/ AND @NewBusiness = 0 )
		)
	AND tdvAffinity.ValueInt = @Affinity

	RETURN @ReturnValue
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_RulesEngine_GetAdminFee] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_RulesEngine_GetAdminFee] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_RulesEngine_GetAdminFee] TO [sp_executeall]
GO
