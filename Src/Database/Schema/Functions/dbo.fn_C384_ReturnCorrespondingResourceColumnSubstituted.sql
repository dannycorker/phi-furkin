SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-02-03
-- Description:	Return a corresponding columns value from a resource 
--				substituting the curly bracket text that equals Aquarium detail field alias (including resource lists)
--				NB. This started as the fn_C00 function of the same name, but SIG wanted curly bracket delimiters and 
--				to use detail fields across lead types where these are unambiguous
-- =============================================
CREATE FUNCTION [dbo].[fn_C384_ReturnCorrespondingResourceColumnSubstituted]
(
	@MatchFieldID INT,
	@MatchValue VARCHAR(2000),
	@ReturnColumnID INT,
	@ObjectID INT,
	@ObjectType VARCHAR(30)
)
RETURNS VARCHAR(2000)
AS
BEGIN

--declare
--	@MatchFieldID INT=155936,
--	@MatchValue VARCHAR(2000)='TESTINGTESTING',
--	@ReturnColumnID INT=155937,
--	@ObjectID INT =  40026355,--40026355,--30019279,--50026379,
--	@ObjectType VARCHAR(30) = 'Case'

	DECLARE 
			@ReturnValue VARCHAR(2000)

	
	-- get the string to substitute
	SELECT @ReturnValue=ISNULL(dbo.fn_C00_ReturnCorrespondingResourceColumn (@MatchFieldID, @MatchValue, @ReturnColumnID),'')
	
	-- do the substitution
	SELECT @ReturnValue=dbo.fn_C327_ReplaceAliasesInBraces ( @ReturnValue, @ObjectID, @ObjectType )
	
--PRINT @ReturnValue
	RETURN @ReturnValue

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C384_ReturnCorrespondingResourceColumnSubstituted] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C384_ReturnCorrespondingResourceColumnSubstituted] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C384_ReturnCorrespondingResourceColumnSubstituted] TO [sp_executeall]
GO
