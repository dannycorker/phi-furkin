SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2010-08-06
-- Description:	Check that all the characters in a string are valid within a filename
-- =============================================
CREATE FUNCTION [dbo].[fnIsValidFileName] 
(
	@FileName varchar(1000), 
	@IncludesPath bit  
)
RETURNS bit
AS
BEGIN
	
	DECLARE @IsValid bit = 1, 
	@CharCount int, 
	@ColonPos int 
	
	/* 
		If a real value was passed in, check all the characters now.
		Otherwise it is null or empty and therefore invalid. 
	*/
	IF @FileName > ''
	BEGIN
	
		SELECT @CharCount = datalength(@FileName)
		
		/*
			These two branches look identical, but they check different columns on the CharMap table.
		*/
		IF @IncludesPath = 0
		BEGIN
		
			/* Filename only is quite simple (any colon is invalid) */
			;
			WITH chars AS 
			(
				/* Pivot the input string into a "table" so it can be joined to the CharMap table */
				SELECT t.N as [Position], 
				SUBSTRING(@FileName, t.N, 1) as [Char], 
				ASCII(SUBSTRING(@FileName, t.N, 1)) as [AsciiValue] 
				FROM dbo.Tally t WITH (NOLOCK) 
				WHERE t.N <= @CharCount 
			),
			fails AS 
			(
				/* Count the invalid characters for a filename */
				SELECT COUNT(*) as failcount 
				FROM chars c 
				INNER JOIN dbo.CharMap cm WITH (NOLOCK) ON cm.AsciiValue = c.[AsciiValue] 
				WHERE cm.IsValidFileName = 0 /* FILENAME */
			)
			SELECT @IsValid = CASE f.failcount WHEN 0 THEN 1 ELSE 0 END 
			FROM fails f 
			
		END
		ELSE
		BEGIN 
			
			/* Path is slightly different as up to one colon is allowed */
			;
			WITH chars AS 
			(
				/* Pivot the input string into a "table" so it can be joined to the CharMap table */
				SELECT t.N as [Position], 
				SUBSTRING(@FileName, t.N, 1) as [Char], 
				ASCII(SUBSTRING(@FileName, t.N, 1)) as [AsciiValue] 
				FROM dbo.Tally t WITH (NOLOCK) 
				WHERE t.N <= @CharCount 
			),
			fails AS 
			(
				/* Count the invalid characters for a path */
				SELECT COUNT(*) as failcount 
				FROM chars c 
				INNER JOIN dbo.CharMap cm WITH (NOLOCK) ON cm.AsciiValue = c.[AsciiValue] 
				WHERE cm.IsValidPath = 0 /* PATH */
			)
			SELECT @IsValid = CASE f.failcount WHEN 0 THEN 1 ELSE 0 END 
			FROM fails f 
			
			/* Further tests for colons */
			IF @IsValid = 1
			BEGIN
			
				/* Find the first one */
				SELECT @ColonPos = CHARINDEX(':', @FileName, 0)
				
				/* None is valid. If one is found, make sure it is in pos 2 and that there aren't any others */
				IF @ColonPos > 0
				BEGIN
					
					/* First colon can only be in pos 2 */
					IF @ColonPos <> 2
					BEGIN
						SELECT @IsValid = 0
					END
					ELSE
					BEGIN
					
						/* Test for another one */
						SELECT @ColonPos = CHARINDEX(':', @FileName, 3)
						
						/* If another one is found, this path is not valid */
						IF @ColonPos <> 0
						BEGIN
							SELECT @IsValid = 0
						END
						
					END
										
				END
				
			END
		END
			
	END
	ELSE
	BEGIN
		SELECT @IsValid = 0
	END
	
	RETURN @IsValid
	
END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsValidFileName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnIsValidFileName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsValidFileName] TO [sp_executeall]
GO
