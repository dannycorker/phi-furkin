SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date:	2020-07-31
-- Description:	Return the ID of the primary RuleSetID by ClientID used in [dbo].[RulesEngine_PrepareCalculation]
-- =============================================
CREATE FUNCTION [dbo].[fnGetInitialRuleSetID]
(
	@BrandID INT = NULL
)
RETURNS INT
AS
BEGIN

	DECLARE @ClientID INT = dbo.fnGetPrimaryClientID()
	DECLARE @RuleSetID INT

	SELECT @RuleSetID = CASE @ClientID
						WHEN 604 THEN 3456
						WHEN 605 THEN CASE @BrandID WHEN 2000184 THEN 1 ELSE 0 END /*GPR 2021-03-19 PPET-845*/
						WHEN 606 THEN CASE @BrandID WHEN 2000184 THEN 3497 ELSE 0 END /*GPR 2021-03-19 PETSURE-758*/
						WHEN 607 THEN CASE @BrandID WHEN 2000184 THEN 3787 WHEN 2002314 THEN 3796 ELSE 0 END /*GPR/ALM 2021-02-05*/
						ELSE 0
						END
	RETURN @RuleSetID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetInitialRuleSetID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetInitialRuleSetID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetInitialRuleSetID] TO [sp_executeall]
GO
