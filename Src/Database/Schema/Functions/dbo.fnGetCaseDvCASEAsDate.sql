SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-06-12
-- Description:	Get CaseDV directly #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetCaseDvCASEAsDate]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS date
AS
BEGIN

	DECLARE @ValueDate AS date

	SELECT @ValueDate = cdv.ValueDate
	FROM CaseDetailValues cdv WITH (NOLOCK) 
	WHERE cdv.CaseID = @CaseID 
	AND cdv.DetailFieldID = @DetailFieldID

	RETURN @ValueDate

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCaseDvCASEAsDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetCaseDvCASEAsDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCaseDvCASEAsDate] TO [sp_executeall]
GO
