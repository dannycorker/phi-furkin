SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Robin Hall
-- Create date: 2014-01-10
-- Description:	Checks whether to deny access to a lead based on the Underwriter of the current policy
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_DenyLeadAccess]
(
	@ClientID INT
	,@GroupID INT
	,@UserID INT
	,@LeadID INT
)
RETURNS BIT	
AS
BEGIN

	DECLARE @DenyAccess INT = 0
	
	RETURN @DenyAccess

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_DenyLeadAccess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_DenyLeadAccess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_DenyLeadAccess] TO [sp_executeall]
GO
