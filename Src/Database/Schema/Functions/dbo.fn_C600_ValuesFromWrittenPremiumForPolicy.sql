SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds / Alexandra Maguire
-- Create date: 2020-09-19
-- Description:	Returns values for discounts and taxes from latest WrittenPremium row for Policy - Created for Nessa's fields
-- 2020-10-05 GPR Return IsBroker value as Surcharge
-- 2020-11-26 CMJ TotalDiscountRate rounding to 2dp
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_ValuesFromWrittenPremiumForPolicy]
(
	@MatterID INT
)
RETURNS 
	@Values TABLE (
		TotalCost MONEY,				-- $
		PolicyNet MONEY,				-- $
		TransactionFee MONEY,			-- $
		StateTax  MONEY,				-- $
		MunicipalTax MONEY,				-- $
		SurchargeRate DECIMAL(18,4),	-- %
		MunicipalRate DECIMAL(18,4),	-- %
		TotalDiscount MONEY,			-- $
		TotalDiscountRate DECIMAL(18,2)	-- $

	)
AS
BEGIN

	DECLARE @WrittenPremiumID INT
	DECLARE @ClientID INT = [dbo].[fnGetPrimaryClientID]()

	SELECT @WrittenPremiumID = (SELECT TOP(1) WrittenPremiumID FROM WrittenPremium WITH (NOLOCK) WHERE MatterID = @MatterID ORDER BY 1 DESC)

	INSERT INTO @Values
	SELECT
	wp.AnnualPriceForRiskGross -- Total Cost
	,wp.AnnualPriceForRiskNET -- Policy Net
	,ISNULL(dbo.[fn_C600_GetTransactionFeeByPolicy](wp.MatterID),0.00) -- TransactionFee
	,ISNULL(wp.AnnualPriceForRiskNationalTax,0.00) -- StateTax
	,ISNULL(wp.AnnualPriceForRiskLocalTax,0.00) -- MunicipalTax
	,ISNULL((qpcSurcharge.ValueDecimal - 1)*100,0.00) -- Surcharge Rate (IsBroker)
	,ISNULL(wp.AnnualLocalTaxRate,0.0) -- Municipal Tax Rate
	,ABS(qpcDiscount.Delta) -- TotalDiscount
	,ISNULL((1.0 - qpcDiscount.ValueDecimal) * 100,0.00) -- TotalDiscountRate /*GPR 2020-11-26 changed from Value to ValueDecimal*/
	FROM WrittenPremium wp WITH (NOLOCK)
	INNER JOIN QuotePetProduct qp WITH (NOLOCK) on  qp.QuotePetProductID = wp.QuotePetProductID
	INNER JOIN QuotePetProductCheckpoint qpcDiscount WITH (NOLOCK) ON qp.QuotePetProductID = qpcDiscount.QuotePetProductID and qpcDiscount.CheckpointName = 'Discounts'
	INNER JOIN QuotePetProductCheckpoint qpcSurcharge WITH (NOLOCK) ON qp.QuotePetProductID = qpcSurcharge.QuotePetProductID and qpcSurcharge.CheckpointName = 'Surcharges'
	WHERE wp.WrittenPremiumID = @WrittenPremiumID
	AND wp.ClientID = @ClientID
	
	RETURN

END
GO
