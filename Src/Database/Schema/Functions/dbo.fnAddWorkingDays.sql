SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2011-08-02
-- Description:	Add exactly the right number of working days to the date passed in.
-- 2015-04-28 LB Added handling for negative numbers / the ability to 'add' negative working days.
-- =============================================
CREATE FUNCTION [dbo].[fnAddWorkingDays] 
(
	@date datetime, @daystoadd int
)
RETURNS datetime
AS
BEGIN

	-- The date passed in can be a date or datetime in any valid sql server format.  

	-- The WorkingDays table knows all about weekends and bank holidays

	-- Example: SELECT dbo.fnAddWorkingDays(LeadEvent.WhenCreated, 14) 
	
	IF @daystoadd >= 0
	BEGIN
	
		;WITH InnerSql AS 
		(
			SELECT *, 
			ROW_NUMBER() OVER(ORDER BY wd.WorkingDayID) as rn 
			FROM dbo.WorkingDays wd  WITH (NOLOCK) 
			WHERE wd.Date BETWEEN @date AND @date + ((@daystoadd * 2) + 5)
			AND wd.IsWorkDay = 1
		)
		SELECT @date = i.[Date] 
		FROM InnerSql i 
		WHERE i.rn = @daystoadd
				
	END
	ELSE
	BEGIN
	
		;WITH InnerSql AS 
		(
			SELECT *, 
			ROW_NUMBER() OVER(ORDER BY wd.WorkingDayID desc) as rn 
			FROM dbo.WorkingDays wd  WITH (NOLOCK) 
			WHERE wd.Date BETWEEN @date + ((@daystoadd * 2) - 5) and @date - 1
			AND wd.IsWorkDay = 1
		)
		SELECT @date = i.[Date] 
		FROM InnerSql i 
		WHERE i.rn = ABS(@daystoadd)
	
	END
	
	RETURN @date
END

GO
GRANT VIEW DEFINITION ON  [dbo].[fnAddWorkingDays] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnAddWorkingDays] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnAddWorkingDays] TO [sp_executeall]
GO
