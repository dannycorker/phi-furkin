SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-11-25
-- Description:	Selects a simple detail field from the appropriate detail value table
--				Copy of SB one with modifications.
--	2015-11-13 ACE Added in 6 for table
--  2016-07-18 ACE Added 4 for resource
-- =============================================
CREATE FUNCTION [dbo].[fnGetSimpleDvAsMoney]
(
	@DetailFieldID INT,
	@ObjectID INT
)
RETURNS MONEY
AS
BEGIN

	DECLARE @DetailValue MONEY,
			@LeadOrMatter INT

	SELECT @LeadOrMatter = LeadOrmatter
	FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @DetailFieldID

	IF @LeadOrMatter = 1
	BEGIN
		SELECT @DetailValue = ValueMoney 
		FROM LeadDetailValues WITH (NOLOCK)
		WHERE LeadID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END

	IF @LeadOrMatter = 2
	BEGIN
		SELECT @DetailValue = ValueMoney 
		FROM MatterDetailValues WITH (NOLOCK)
		WHERE MatterID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END

	IF @LeadOrMatter = 4
	BEGIN
		SELECT @DetailValue = ValueMoney 
		FROM ResourceListDetailValues WITH (NOLOCK)
		WHERE ResourceListID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END

	IF @LeadOrMatter IN (6,8)
	BEGIN
		SELECT @DetailValue = ValueMoney 
		FROM TableDetailValues WITH (NOLOCK)
		WHERE TableRowID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 10
	BEGIN
		SELECT @DetailValue = ValueMoney 
		FROM CustomerDetailValues WITH (NOLOCK)
		WHERE CustomerID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 11
	BEGIN
		SELECT @DetailValue = ValueMoney 
		FROM CaseDetailValues WITH (NOLOCK)
		WHERE CaseID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 12
	BEGIN
		SELECT @DetailValue = ValueMoney 
		FROM ClientDetailValues WITH (NOLOCK)
		WHERE ClientID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 13
	BEGIN
		SELECT @DetailValue = ValueMoney 
		FROM ClientPersonnelDetailValues WITH (NOLOCK)
		WHERE ClientPersonnelID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 14
	BEGIN
		SELECT @DetailValue = ValueMoney 
		FROM ContactDetailValues WITH (NOLOCK)
		WHERE ContactID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END

	RETURN @DetailValue

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleDvAsMoney] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetSimpleDvAsMoney] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleDvAsMoney] TO [sp_executeall]
GO
