SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-06-12
-- Description:	Get MatterDV directly #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetMatterDvCASEAsDate]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS date
AS
BEGIN

	DECLARE @ValueDate AS date

	SELECT @ValueDate = mdv.ValueDate 
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND mdv.DetailFieldID = @DetailFieldID
	WHERE m.CaseID = @CaseID

	RETURN @ValueDate

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetMatterDvCASEAsDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetMatterDvCASEAsDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetMatterDvCASEAsDate] TO [sp_executeall]
GO
