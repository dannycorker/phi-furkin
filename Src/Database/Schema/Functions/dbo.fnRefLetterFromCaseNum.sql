SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2010-09-21
-- Description:	Get next RefLetter for the Matter based on the CaseNum passed in
-- =============================================
CREATE FUNCTION [dbo].[fnRefLetterFromCaseNum] 
(
	@CaseNum int
)
RETURNS varchar(3)
AS
BEGIN

	DECLARE @RefLetter varchar(3)

	DECLARE @alphabet varchar(26) = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
	@letter1pos int = 0,
	@letter2pos int = 0,
	@letter3pos int = 0,
	@letter1 varchar(1) = '',
	@letter2 varchar(1) = '',
	@letter3 varchar(1) = ''

	SELECT @letter1pos = ((@casenum-1)%26)+1, @letter1 = SUBSTRING(@alphabet, @letter1pos, 1)

	SELECT @letter2pos = ((@casenum-1)/26)
	IF @letter2pos > 26
	BEGIN
		SELECT @letter2pos = ((@letter2pos-1)%26)+1
	END
	SELECT @letter2 = SUBSTRING(@alphabet, @letter2pos, 1)

	IF @casenum > 702
	BEGIN
		SELECT @letter3pos = ((@casenum-1)/702)
	END 
	IF @letter2pos > 26
	BEGIN
		SELECT @letter2pos = ((@letter2pos-1)%26)+1
	END
	SELECT @letter3 = SUBSTRING(@alphabet, @letter3pos, 1)

	SELECT @refletter = @letter3 + @letter2 + @letter1

	RETURN @RefLetter

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnRefLetterFromCaseNum] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnRefLetterFromCaseNum] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnRefLetterFromCaseNum] TO [sp_executeall]
GO
