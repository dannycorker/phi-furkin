SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 06/03/2020
-- Description:	Returns a new string of a specified length in which the beginning of the current 
--				string is padded with spaces or with a specified Unicode character.
-- =============================================
CREATE FUNCTION [dbo].[fnPadLeft]
(
	@Value VARCHAR(MAX),
	@Pad VARCHAR(1),
	@MaxLength INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE @Length INT
	SET @Length = LEN(@Value)

	RETURN ISNULL(REPLICATE(@Pad, @MaxLength - @Length) + @Value, '')	

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnPadLeft] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnPadLeft] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnPadLeft] TO [sp_executeall]
GO
