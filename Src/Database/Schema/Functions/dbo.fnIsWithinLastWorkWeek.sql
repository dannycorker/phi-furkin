SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2007-08-22
-- Description:	Is the date passed in within the last working-week
-- =============================================
CREATE FUNCTION [dbo].[fnIsWithinLastWorkWeek]
(
	-- Add the parameters for the function here
	@chardate varchar(50), @startdow tinyint
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result bit, @adjustment smallint

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = 0

	IF @chardate IS NOT NULL AND @chardate <> '' AND @chardate <> '1901-01-01'
	BEGIN

		-- If the week starts on a Monday (day 2) and this is a Friday (day 6),
		-- then we get (2-6) = 4 days to go back. 
		SELECT @adjustment = @startdow - datepart(dw,dbo.fn_GetDate_Local())

		-- Cater for not reaching the next week-ending date yet. If today is 
		-- Sunday (1) then we have (2-1) = 1 day forwards. Subtracting 7 days
		-- means we go back the required 6 instead.
		IF @adjustment > 0
		BEGIN
			SET @adjustment = @adjustment - 7
		END

		-- If the date passed in is greater-than-or-equal-to the computed start date
		-- of the current working week, then the result is a success.
		IF datediff(dd, (dbo.fn_GetDate_Local() + @adjustment), convert(datetime,@chardate)) >= 0
		BEGIN
			SELECT @Result = 1
	 	END

	END

	-- Return the result of the function
	RETURN @Result

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsWithinLastWorkWeek] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnIsWithinLastWorkWeek] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsWithinLastWorkWeek] TO [sp_executeall]
GO
