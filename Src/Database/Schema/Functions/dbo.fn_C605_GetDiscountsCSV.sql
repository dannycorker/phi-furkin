SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ================================================================================================================
-- Author:		Gavin	
-- Create date: 2020-09-13
-- Description:	Returns comma separated policy sections for claim
-- 2020-09-28 NG  Added filter to remove old written premium rows post-MTA
-- 2020-09-29 ALM Changed to select to the top 1 WrittenPremiumID SDPRU-66
-- =================================================================================================================

CREATE FUNCTION [dbo].[fn_C605_GetDiscountsCSV] 
(
	@MatterID INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

DECLARE @csv VARCHAR(MAX),
		@WrittenPremiumID INT

SELECT @WrittenPremiumID = (SELECT TOP 1 wp.WrittenPremiumID FROM WrittenPremium wp WITH (NOLOCK) WHERE wp.MatterID = @MatterID ORDER BY 1 DESC)


;WITH Discounts (Discount) AS (

	SELECT qpcDiscount.CheckpointName
	FROM WrittenPremium wp WITH ( NOLOCK ) 
	INNER JOIN QuotePetProduct qp WITH (NOLOCK) on  qp.QuotePetProductID = wp.QuotePetProductID
	INNER JOIN QuotePetProductCheckpoint qpcDiscount WITH (NOLOCK) ON qp.QuotePetProductID = qpcDiscount.QuotePetProductID and qpcDiscount.CheckpointName IN ('Animal Shelter', 'Corporate Group Benefit', 'Medical Services', 'Multiple Pet', 'Strategic Partner', 'Military', 'Vet or Staff', 'Wellness', 'Internet Partner')
	AND wp.MatterID = @MatterID
	AND qpcDiscount.Value > '0.00'
	AND wp.WrittenPremiumID = @WrittenPremiumID
)

SELECT 
	@csv = 
		ISNULL(@csv + ', ', '') 
		+ Discount
		
FROM 
	Discounts

RETURN @csv

END



GO
