SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2010-12-17
-- Description:	Get Resource Value From resource between 2 dates
-- =============================================
CREATE FUNCTION [dbo].[fnGetResValueBetweenDates]
(
@FromDateField int,
@ToDateField int,
@ValueField int,
@ReturnResourceListID int = 0,
@Date date = NULL
)
RETURNS varchar(2000) 
AS
BEGIN

	Declare @ReturnValue varchar(2000)

	IF @Date = NULL SELECT @Date = dbo.fn_GetDate_Local()

	SELECT @ReturnValue = CASE @ReturnResourceListID WHEN 1 THEN CONVERT(Varchar(2000),rldv_Value.ResourceListID) ELSE rldv_Value.DetailValue END
	From ResourceListDetailValues rldv_FromDate WITH (NOLOCK)
	INNER JOIN ResourceListDetailValues rldv_ToDate WITH (NOLOCK) ON rldv_ToDate.ResourceListID = rldv_FromDate.ResourceListID and rldv_ToDate.DetailFieldID = @ToDateField and rldv_ToDate.ValueDate >= CONVERT(Date,@Date)
	INNER JOIN ResourceListDetailValues rldv_Value WITH (NOLOCK) ON rldv_Value.ResourceListID = rldv_FromDate.ResourceListID and rldv_Value.DetailFieldID = @ValueField
	WHERE rldv_FromDate.DetailFieldID = @FromDateField
	AND rldv_FromDate.ValueDate <= CONVERT(Date,@Date)
	
	RETURN @ReturnValue 
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetResValueBetweenDates] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetResValueBetweenDates] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetResValueBetweenDates] TO [sp_executeall]
GO
