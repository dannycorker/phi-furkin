SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2016-12-09
-- Description:	Take a LeadID and return all failed payments for arrears calculations
--				Adapted from pre-existing SAE 
-- JEL 2020-05-22 Filter for polices that are not cancelled 
-- =============================================
CREATE FUNCTION [dbo].[fn_Billing_GetFailedPaymentsByCustomerID]
(
	@CustomerID INT
)
RETURNS TABLE 
AS
RETURN
(
	/*2018-06-04 ACE -  Corrected spelling of aymentVAT to PaymentVAT*/
	SELECT	 p.PurchasedProductPaymentScheduleID, p.ClientID, p.CustomerID, pp.AccountID, p.PurchasedProductID, p.ActualCollectionDate, p.CoverFrom, p.CoverTo, p.PaymentDate, ABS(p.PaymentNet) AS [PaymentNet], ABS(p.PaymentVAT) AS [PaymentVAT], ABS(p.PaymentGross) AS [PaymentGross], p.PaymentStatusID, p.CustomerLedgerID, p.ReconciledDate, p.CustomerPaymentScheduleID, p.WhoCreated, p.WhenCreated, p.PurchasedProductPaymentScheduleParentID, p.PurchasedProductPaymentScheduleTypeID, p.PaymentGroupedIntoID, p.SourceID
	FROM PurchasedProductPaymentSchedule p WITH (NOLOCK)
	INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON pp.PurchasedProductID = p.PurchasedProductID
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = pp.ObjectID
	INNER JOIN MatterDetailValues polstatus with (NOLOCK) on m.MatterID = polstatus.MatterID and polstatus.DetailFieldID = 170038
	where m.CustomerID = @CustomerID
	AND polstatus.valueint NOT IN  (43003,43004) /*Cancelled or lapsed*/ 
	AND p.PaymentGross <> 0.00
	AND 
		(
			p.PaymentStatusID = 4 /*Failed*/
			OR 
			(p.PaymentStatusID = 1 AND p.ActualCollectionDate < dbo.fn_GetDate_Local())
			
		)
	AND NOT EXISTS (
		SELECT *
		FROM PurchasedProductPaymentSchedule p2 WITH ( NOLOCK )
		WHERE p2.PurchasedProductPaymentScheduleID = p.PaymentGroupedIntoID
		AND p2.PaymentStatusID IN (2) /*Processed*/
		AND p2.PurchasedProductPaymentScheduleID > p.PurchasedProductPaymentScheduleID
	)
	AND NOT EXISTS (
		/*2018-01-24 ACE - Make sure for this purchased product payment we dont have a later linked payment that has been received.*/
		SELECT *
		FROM PurchasedProductPaymentSchedule ppps3 WITH (NOLOCK)
		WHERE ppps3.PurchasedProductPaymentScheduleParentID = p.PurchasedProductPaymentScheduleParentID
		AND ppps3.PurchasedProductPaymentScheduleID > p.PurchasedProductPaymentScheduleID
		AND ppps3.PaymentStatusID = 6
	)
	AND p.PaymentGroupedIntoID IS NULL
	AND NOT EXISTS (
		SELECT *
		FROM PurchasedProductPaymentSchedule p3 WITH (NOLOCK)
		WHERE p3.PurchasedProductPaymentScheduleParentID = p.PurchasedProductPaymentScheduleID
		AND p3.PurchasedProductPaymentScheduleID > p.PurchasedProductPaymentScheduleParentID
	)

)
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_Billing_GetFailedPaymentsByCustomerID] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_Billing_GetFailedPaymentsByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_Billing_GetFailedPaymentsByCustomerID] TO [sp_executeall]
GO
