SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-12-04
-- Description:	Return  workflow task progress for a number of days
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetWorkflowProgressForUsers]
(
	 @NumberOfDays		INT
)
RETURNS @Data TABLE
(
	 ClientPersonnelID	INT
	,UserName			VARCHAR(2000)
	,ExpectedPerDay		DECIMAL(18,2)
	,ExpectedInPeriod	DECIMAL(18,2)
	,Completed			DECIMAL(18,2)
	,Percentage			DECIMAL(18,2)
)
AS
BEGIN

	DECLARE  @StartDate	DATE = CAST(DATEADD(DAY,-@NumberOfDays,dbo.fn_GetDate_Local()) as DATE)
			,@ClientID	INT = dbo.fnGetPrimaryClientID()

	INSERT @Data ( ClientPersonnelID, UserName, ExpectedPerDay, ExpectedInPeriod, Completed )
	SELECT cp.ClientPersonnelID, cp.UserName, ISNULL(cpdv.ValueInt,0) [ExpectedPerDay], CASE WHEN @NumberOfDays = 0 THEN ISNULL(cpdv.ValueInt,0) * 1 ELSE ISNULL(cpdv.ValueInt,0) * @NumberOfDays END AS [ExpectedInPeriod], COUNT(tc.WorkflowTaskID) [Completed]-- modified NG 2018-02-15 to reflect number of days in in expected tasks in period
	FROM ClientPersonnel cp WITH ( NOLOCK ) 
	LEFT JOIN WorkflowTaskCompleted tc WITH ( NOLOCK ) on tc.CompletedBy = cp.ClientPersonnelID AND tc.CompletedOn > @StartDate
	LEFT JOIN ClientPersonnelDetailValues cpdv WITH ( NOLOCK ) on cpdv.ClientPersonnelID = cp.ClientPersonnelID AND cpdv.DetailFieldID = 180138 /*Daily Target*/
	WHERE cp.ClientID = @ClientID
	GROUP BY cp.ClientPersonnelID, cp.UserName, cpdv.ValueInt
	
	UPDATE @Data
	SET Percentage = (d.Completed / d.ExpectedInPeriod) * 100
	FROM @Data d 
	WHERE d.ExpectedInPeriod > 0.00
	
	UPDATE d 
	SET Percentage = 100.00
	FROM @Data d 
	WHERE d.Percentage is NULL

	RETURN

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetWorkflowProgressForUsers] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_GetWorkflowProgressForUsers] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetWorkflowProgressForUsers] TO [sp_executeall]
GO
