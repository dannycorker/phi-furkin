SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-02-03
-- Description:	Returns the schemes ceiling (max premium)
-- Mods
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetSchemeCeiling]
(
	@SchemeID INT
)
RETURNS MONEY	
AS
BEGIN

	DECLARE @ClientID INT,
			@Ceiling MONEY,
			@DefaultCeiling MONEY = 100000
	
	-- get PAF
	SELECT @Ceiling=ISNULL(collar.ValueMoney,@DefaultCeiling)
	FROM MatterDetailValues scheme
	LEFT JOIN MatterDetailValues collar WITH (NOLOCK) ON scheme.MatterID=collar.MatterID AND collar.DetailFieldID=175465
	WHERE scheme.DetailFieldID=145689 AND scheme.ValueInt=@SchemeID
	
	IF @Ceiling=0
		SELECT @Ceiling=@DefaultCeiling

	RETURN @Ceiling
 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetSchemeCeiling] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetSchemeCeiling] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetSchemeCeiling] TO [sp_executeall]
GO
