SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2016-01-13
-- Description:	Comma-Separate the values in one matter-level field across filtered matters for a lead
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_CommaSeparateMatterFieldByLeadID]
(
	 @LeadID			INT
	,@DetailFieldID		INT
	,@FilterFieldID		INT
	,@FilterRegexValue	VARCHAR(2000)
)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE  @ReturnValue VARCHAR(2000) = ''

	SELECT @ReturnValue += ISNULL(li.ItemValue,mdv.DetailValue) + ', '
	FROM MatterDetailValues mdv WITH ( NOLOCK )
	INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = mdv.DetailFieldID
	LEFT JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListID = df.LookupListID AND li.LookupListItemID = mdv.ValueInt
	LEFT JOIN MatterDetailValues mdv_filter WITH ( NOLOCK ) on mdv_filter.MatterID = mdv.MatterID and mdv_filter.DetailFieldID = @FilterFieldID
	LEFT JOIN DetailFields df_filter WITH ( NOLOCK ) on df_filter.DetailFieldID = mdv_filter.DetailFieldID
	LEFT JOIN LookupListitems li_filter WITH ( NOLOCK ) on li_filter.LookupListID = df_filter.LookupListID AND li_filter.LookupListItemID = mdv_filter.ValueInt
	WHERE mdv.DetailFieldID = @DetailFieldID
	AND mdv.LeadID = @LeadID
	AND (	(	li_filter.ItemValue like '%' + @FilterRegexValue + '%' AND @FilterFieldID > 0 )
		OR
			(	@FilterFieldID = 0 )
		)

	IF @@ROWCOUNT > 0
	BEGIN
		SELECT @ReturnValue = LEFT(@ReturnValue,LEN(@ReturnValue)-1)
	END

	RETURN @ReturnValue

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_CommaSeparateMatterFieldByLeadID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_CommaSeparateMatterFieldByLeadID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_CommaSeparateMatterFieldByLeadID] TO [sp_executeall]
GO
