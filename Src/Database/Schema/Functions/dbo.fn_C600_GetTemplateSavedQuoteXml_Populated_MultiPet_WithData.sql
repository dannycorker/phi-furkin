SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2019-04-09
-- Description:	Created from fn_C600_GetTemplateSavedQuoteXml_Populated_MultiPet for C600 OMF #56174 now calls fn_C600_GetTemplateSavedQuoteXml_AdditionalAddressData
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetTemplateSavedQuoteXml_Populated_MultiPet_WithData]
(
	 @CustomerID				INT
	,@LeadID					INT
	,@MatterID					INT
	,@SessionID					INT
	,@MobilePhone 				VARCHAR(50)
	,@HomePhone 				VARCHAR(50)
	,@PostCode 					VARCHAR(50)
	,@EmailAddress 				VARCHAR(2000)
	,@LastName 					VARCHAR(2000)
	,@FirstName 				VARCHAR(2000)
	,@TitleID 					INT
	,@CustBirthDate				DATE
	,@StartDate 				DATE
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE  @XmlVarchar			VARCHAR(MAX)
			,@CustomerIDVarchar		VARCHAR(10) = CONVERT(VARCHAR(10), @CustomerID)
			,@SessionIDVarchar		VARCHAR(10) = CONVERT(VARCHAR(10), @SessionID)
			,@TitleIDVarchar		VARCHAR(10) = CONVERT(VARCHAR(10), @TitleID)
			,@StartDateVarchar		VARCHAR(20) = CONVERT(VARCHAR(10),@StartDate,126) + 'T00:00:00'
			,@CustBirthDateVarchar	VARCHAR(20) = CONVERT(VARCHAR(10),@CustBirthDate,126) + 'T00:00:00'
			,@DoNotSms				VARCHAR(10) = CASE WHEN dbo.fnGetSimpleDvAsInt(177152, @CustomerID) = 1 THEN 'true' ELSE 'false' END /*Customer allows Marketing by SMS*/
			,@DoNotEmail			VARCHAR(10) = CASE WHEN dbo.fnGetSimpleDvAsInt(177153, @CustomerID) = 1 THEN 'true' ELSE 'false' END /*Customer allows Marketing by Email*/
			,@DoNotPost				VARCHAR(10) = CASE WHEN dbo.fnGetSimpleDvAsInt(177154, @CustomerID) = 1 THEN 'true' ELSE 'false' END /*Customer allows Marketing by Post*/
			,@DoNotPhone			VARCHAR(10) = CASE WHEN dbo.fnGetSimpleDvAsInt(177371, @CustomerID) = 1 THEN 'true' ELSE 'false' END /*Customer allows Marketing by Phone*/
			,@SendDocumentsBy		VARCHAR(10) = CONVERT(VARCHAR(10), dbo.fnGetSimpleDvAsInt(170257, @CustomerID)) /*Send documents by*/
			,@MarketingPrefs		VARCHAR(10) = CONVERT(VARCHAR(10), dbo.fnGetSimpleDvAsInt(176970, @CustomerID)) /*Customer allows Marketing*/
			,@VetID					VARCHAR(10) = CONVERT(VARCHAR(10), ISNULL(dbo.fnGetSimpleDvAsInt(146215, @LeadID), 0)) /*Current Vet*/
			,@IsNeutered			VARCHAR(10) 
			,@HasMicrochip			VARCHAR(10) 
			,@VaccinationsUpToDate	VARCHAR(10) 
			,@HasExistingConditions	VARCHAR(10)
			,@MicrochipNumber		VARCHAR(10)
			,@ClientIdVarchar		VARCHAR(10) = dbo.fnGetPrimaryClientID()
			,@PetInfo				VARCHAR(MAX)
			,@MultiPet				XML
			,@PetInfoXML			XML
			,@ColourVarchar			VARCHAR(MAX)
			,@PriceVarchar			VARCHAR(MAX)
			,@PetBirthDateVarchar	VARCHAR(MAX)
			,@Gender				VARCHAR(MAX)
			,@BreedIDVarchar		VARCHAR(MAX)
			,@PetName				VARCHAR(MAX)
			,@PetRef				VARCHAR(MAX)
			,@KeyValues				VARCHAR(MAX)
			,@StaticXML XML = '<VetID>_VetID_</VetID>
      <PolicyValues>
        <PolicyValue>
          <ProductId>_ProductId1_</ProductId>
          <PolicyLimit>0</PolicyLimit>
          <Excess>0</Excess>
          <VoluntaryExcess>0</VoluntaryExcess>
          <CoInsurance>0</CoInsurance>
        </PolicyValue>
      </PolicyValues>
      <StartDate>_StartDate_</StartDate>
      <UnderwritingList>
        <UnderwritingAnswerItemType>
          <AnswerId>_AnswerId1_</AnswerId>
          <QuestionId>_QuestionId1_</QuestionId>
        </UnderwritingAnswerItemType>
        <UnderwritingAnswerItemType>
          <AnswerId>_AnswerId2_</AnswerId>
          <QuestionId>_QuestionId2_</QuestionId>
		  <AnswerText>_AnswerText2_</AnswerText>
        </UnderwritingAnswerItemType>
      </UnderwritingList>'
			
	DECLARE @Pets TABLE (	 RowNumber				INT
							,PetColorId				INT
							,PurchasePrice			MONEY
							,BirthDate				DATE
							,Gender					VARCHAR(MAX)
							,BreedID				INT
							,PetName				VARCHAR(MAX)
							,PetRef					VARCHAR(MAX)
							,IsNeutered				VARCHAR(MAX)
							,HasMicrochip			VARCHAR(MAX)
							,VaccinationsUpToDate	VARCHAR(MAX)
							,HasExistingConditions	VARCHAR(MAX)
							,MicrochipNo			VARCHAR(MAX) 
							,StartDate				DATE
						)
			
		
	INSERT @Pets
	SELECT	 ROW_NUMBER() OVER(ORDER BY l.leadID)
			,ldvPetColor.ValueInt
			,ldvPetCost.ValueMoney
			,ldvDOB.ValueDate
			,CASE ldvGender.ValueINT WHEN 5168 THEN 'M' ELSE 'F' END
			,ldvPetType.ValueInt
			,ldvPetName.DetailValue
			,L.LeadID
			,CASE ldvPetNeutered.ValueInt			WHEN 5144 THEN 'true' ELSE 'false' END
			,CASE ldvHasMicrochip.ValueInt			WHEN 5144 THEN 'true' ELSE 'false' END
			,CASE ldvVaccinationsUpToDate.ValueInt	WHEN 5144 THEN 'true' ELSE 'false' END
			,CASE ldvExistingConditions.ValueInt	WHEN 5144 THEN 'true' ELSE 'false' END
			,ldvMicroChipNum.DetailValue
			,DATEADD(DAY,1,mdv.ValueDate) -- Start Date for new policy is OMF End Date + 1
	FROM       dbo.Lead				L WITH (NOLOCK)                      
	INNER JOIN dbo.LeadDetailValues ldvPetColor WITH (NOLOCK)             on ldvPetColor.LeadID				= l.LeadID AND ldvPetColor.DetailFieldID			= 144273 /*Pet Colour*/
	INNER JOIN dbo.LeadDetailValues ldvPetCost WITH (NOLOCK)              on ldvPetCost.LeadID				= l.LeadID AND ldvPetCost.DetailFieldID				= 144339 /*Pet Purchase Cost*/
	INNER JOIN dbo.LeadDetailValues ldvDOB WITH (NOLOCK)                  on ldvDOB.LeadID					= l.LeadID AND ldvDOB.DetailFieldID					= 144274 /*Pet Date of Birth*/
	INNER JOIN dbo.LeadDetailValues ldvGender WITH (NOLOCK)               on ldvGender.LeadID				= l.LeadID AND ldvGender.DetailFieldID				= 144275 /*Pet Sex*/
	INNER JOIN dbo.LeadDetailValues ldvPetType WITH (NOLOCK)              on ldvPetType.LeadID				= l.LeadID AND ldvPetType.DetailFieldID				= 144272 /*Pet Type*/
	INNER JOIN dbo.LeadDetailValues ldvPetName WITH (NOLOCK)              on ldvPetName.LeadID				= l.LeadID AND ldvPetName.DetailFieldID				= 144268 /*Pet Name*/
	INNER JOIN dbo.LeadDetailValues ldvPetNeutered WITH (NOLOCK)          on ldvPetNeutered.LeadID			= l.LeadID AND ldvPetNeutered.DetailFieldID			= 152783 /*Pet Neutered*/
	INNER JOIN dbo.LeadDetailValues ldvHasMicrochip WITH (NOLOCK)         on ldvHasMicrochip.LeadID			= l.LeadID AND ldvHasMicrochip.DetailFieldID		= 177372 /*Microchip*/
	INNER JOIN dbo.LeadDetailValues ldvVaccinationsUpToDate WITH (NOLOCK) on ldvVaccinationsUpToDate.LeadID = l.LeadID AND ldvVaccinationsUpToDate.DetailFieldID= 175497 /*Vaccinations up to date?*/
	 LEFT JOIN dbo.LeadDetailValues ldvExistingConditions WITH (NOLOCK)   on ldvExistingConditions.LeadID	= l.LeadID AND ldvExistingConditions.DetailFieldID	= 177128 /*Existing Conditions / Underwriter Questions*/
	INNER JOIN dbo.LeadDetailValues ldvMicroChipNum WITH (NOLOCK)         on ldvMicroChipNum.LeadID			= l.LeadID AND ldvMicroChipNum.DetailFieldID		= 170030 /*Microchip number*/
	INNER JOIN dbo.Matter m WITH (NOLOCK) on m.LeadID = l.LeadID 
	INNER JOIN dbo.MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = m.MatterID AND mdv.DetailFieldID = 170037 /*Policy End Date*/
	Where l.CustomerID = @CustomerID AND l.LeadTypeID = 1492
	AND NOT EXISTS (SELECT * 
					FROM _C600_SavedQuote s with (NOLOCK) 
					INNER JOIN _C600_QuoteSessions q With (NOLOCK) on q.QuoteSessionID = s.QuoteSessionID and q.CustomerID = @CustomerID
					WHERE s.SavedQuoteXML.value('(//PetRef)[1]','Varchar(100)') = CAST(l.LeadID as VARCHAR) 
					AND  s.SavedQuoteXML.value('(//BrandingId)[1]','Varchar(100)') = 153391 /*Buddies*/)

	/*CPS 2019-03-22 removed on request of Aaran.  @KeyValues is supposed to be supplied in pairs and is used as the PetRef anyway so no need*/
	--SELECT @KeyValues = coalesce(@KeyValues + ',', '') + CAST(L.LeadID AS VARCHAR(MAX))
	--FROM Lead l WITH (NOLOCK)
	--WHERE l.CustomerID = @CustomerID 
	--AND l.LeadTypeID = 1492     

	/*CPS 2019-03-22 to allow Q&B to know that this is an OMF policy*/
	SELECT @KeyValues = '<KeyValueData><Key>IsOmf</Key><Value>true</Value></KeyValueData>'

	SELECT	 @ColourVarchar			= p.PetColorId
			,@PriceVarchar			= p.PurchasePrice
			,@PetBirthDateVarchar	= CAST(p.BirthDate AS VARCHAR(MAX)) + 'T00:00:00'
			,@Gender				= p.Gender
			,@BreedIDVarchar		= p.BreedId
			,@PetName				= p.PetName
			,@PetRef				= p.PetRef
			,@IsNeutered			= p.IsNeutered
			,@HasMicrochip			= p.HasMicrochip
			,@VaccinationsUpToDate	= p.VaccinationsUpToDate
			,@HasExistingConditions = p.HasExistingConditions
			,@MicrochipNumber		= p.MicrochipNo
			,@StartDate				= p.StartDate
	FROM @Pets p
	WHERE p.PetRef = @LeadID -- CPS 2019-03-18 replaced "WHERE p.PetRef = 1" as part of fix for ticket #55570

	SELECT @PetInfo = (SELECT	 p.PetRef
								,p.PetName
								,'_SpeciesId_' [SpeciesId]
								,p.BreedId
								,p.Gender
								,(CAST(p.BirthDate AS VARCHAR(MAX))  + 'T00:00:00') [BirthDate]
								,p.IsNeutered
								,p.HasMicrochip
								,p.MicrochipNo
								,p.PurchasePrice
								,p.VaccinationsUpToDate
								,p.HasExistingConditions
								,p.PetColorId
								,'_CoInsuranceNextYear_' [CoInsuranceNextYear]
								,CAST(REPLACE(CAST(@StaticXML as VARCHAR(MAX)),'_StartDate_',CONVERT(VARCHAR,p.StartDate,126)) + 'T00:00:00' as XML) -- CPS 2019-03-18 #55570
	FROM @Pets p
	WHERE p.PetRef <> @LeadID -- CPS 2019-03-18 replaced "WHERE p.PetRef <> 1" as part of fix for ticket #55570
		FOR Xml PATH ('PetQuote'))

	SELECT @PetInfo = REPLACE(@PetInfo,'<PetRef>','<PetInfo><PetRef>')
	SELECT @PetInfo = REPLACE(@PetInfo,'</CoInsuranceNextYear>','</CoInsuranceNextYear></PetInfo>')

	SELECT @PetInfoXML = CAST(@PetInfo AS XML)
	
	/*GPR 2019-04-09 for #]56174 C600*/
	--SELECT @MultiPet = dbo.fn_C600_GetTemplateSavedQuoteXml()
	SELECT @MultiPet = dbo.fn_C600_GetTemplateSavedQuoteXml_AdditionalAddressData()

	DECLARE @Address1 VARCHAR(200), @Address2 VARCHAR(200), @TownCity VARCHAR(200)
	SELECT @Address1 = c.Address1, @Address2 = c.Address2, @TownCity = c.Town
	FROM Customers c WITH (NOLOCK)
	WHERE c.CustomerID = @CustomerID


	--DECLARE @TrackingCodeXml XML = '<TrackingCode>678A67D451C0E878EED315E7C38A51E1</TrackingCode>' -- OMF Default

	--SET @MultiPet.modify('insert sql:variable("@TrackingCodeXml") as last into (BuyPolicyRequest)[1] ') /*CPS 2019-03-21 for Aaran.  Include OMF tacking code.*/

	SET @MultiPet.modify('             
	insert sql:variable("@PetInfoXML")   
	as last          
	into (BuyPolicyRequest/PetQuotes)[1] ')
	
	SELECT @XmlVarchar = CAST(@MultiPet AS VARCHAR(MAX))
	
	DECLARE	 @Replacements dbo.tvpVarcharVarchar
	INSERT	 @Replacements ( AnyValue1, AnyValue2 )
	VALUES
	 ('_ClientId_'				,	@ClientIdVarchar		)	-- L&G
	,('_ClientPersonnelId_'		,	'0'						)
	,('_Password_'				,	''						)
	,('_BrandingId_'			,	'153391'				)	-- Buddies
	,('_QuoteId_'				,	'0'						)
	,('_SessionID_'				,	@SessionIDVarchar		)
	,('_AdminFee_'				,	'0'						)
	,('_TransactionRef_'		,	''						)
	,('_CustomerId_'			,	@CustomerIDVarchar		)
	,('_TitleID_'				,	@TitleIDVarchar			)
	,('_FirstName_'				,	@FirstName				)
	,('_LastName_'				,	@LastName				)	
	,('_HomePhone_'				,	@HomePhone				)	
	,('_MobilePhone_'			,	@MobilePhone			)	
	,('_Email_'					,	@EmailAddress			)
	,('_Address1_'				,	@Address1				)
	,('_Address2_'				,	@Address2				)
	,('_TownCity_'				,	@TownCity				)
	,('_PostCode_'				,	@PostCode				)
	,('_ExistingPolicyHolder_'	,	'true'					)
	,('_DoNotSms_'				,	@DoNotSms				)
	,('_DoNotEmail_'			,	@DoNotEmail				)
	,('_DoNotPhone_'			,	@DoNotPhone				)
	,('_DoNotPost_'				,	@DoNotPost				)
	,('_DateOfBirth_'			,	@CustBirthDateVarchar	)
	,('_ContactMethodId_'		,	@SendDocumentsBy		)
	,('_MarketingPreferenceId_'	,	@MarketingPrefs			)
	,('_PetRef_'				,	@PetRef 				)
	,('_PetName_'				,	@PetName 				)
	,('_SpeciesId_'				,	'42989'					)	-- Dog
	,('_BreedID_'				,	@BreedIDVarchar			)
	,('_Gender_'				,	@Gender 				)
	,('_BirthDate_'				,	@PetBirthDateVarchar	)
	,('_IsNeutered_'			,	@IsNeutered				)
	,('_HasMicrochip_'			,	@HasMicrochip			)
	,('_MicrochipNo_'			,	@MicrochipNumber		)
	,('_PurchasePrice_'			,	@PriceVarchar 			)
	,('_VaccinationsUpToDate_'	,	@VaccinationsUpToDate	)
	,('_HasExistingConditions_'	,	@HasExistingConditions	)
	,('_PetColorId_'			,	@ColourVarchar			)	
	,('_CoInsuranceNextYear_'	,	'false'					)
	,('_VetID_'					,	@VetID					)
	,('_ProductId1_'			,	'0'						)	-- Buddies Standard
	,('_StartDate_'				,	@StartDateVarchar		)
	,('_AnswerId1_'				,	'5144'					)	-- Underwriting
	,('_QuestionId1_'			,	'147277'				)	-- Underwriting
	,('_AnswerId2_'				,	'5144'					)	-- Underwriting
	,('_QuestionId2_'			,	'154602'				)	-- Underwriting
	,('_OtherPetsInsured_'		,	'0'						)
	,('_DiscountCodes_'			,	''						)
	,('_KeyValues_'				,	@KeyValues				)

	SELECT @XmlVarchar = REPLACE(@XmlVarchar,r.AnyValue1,ISNULL(r.AnyValue2,''))
	FROM @Replacements r
	WHERE r.AnyValue2 is not NULL
	
	RETURN @XmlVarchar
	
END


















GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetTemplateSavedQuoteXml_Populated_MultiPet_WithData] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetTemplateSavedQuoteXml_Populated_MultiPet_WithData] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetTemplateSavedQuoteXml_Populated_MultiPet_WithData] TO [sp_executeall]
GO
