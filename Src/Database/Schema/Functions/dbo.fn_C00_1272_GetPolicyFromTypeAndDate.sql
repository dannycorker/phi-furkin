SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2012-11-29
-- Description:	Returns the correct policy for a type and date
-- Modified:    2013-01-28 Robin Hall : Handle overriding effective dates for renewal policies
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetPolicyFromTypeAndDate]
(
	@SchemeID INT,
	@Date DATE,
	@IsRenewal BIT
)
RETURNS INT	
AS
BEGIN
	
	DECLARE @CurrentPolicyMatterID INT
			
	-- Match this scheme against the correct matter on the Affinity lead type
	SELECT @CurrentPolicyMatterID = mdvScheme.MatterID 
	FROM  dbo.Matter m WITH (NOLOCK) 
	INNER JOIN dbo.MatterDetailValues mdvScheme WITH (NOLOCK) ON m.MatterID = mdvScheme.MatterID AND mdvScheme.DetailFieldID = 145689 
	INNER JOIN dbo.MatterDetailValues mdvStart WITH (NOLOCK) ON m.MatterID = mdvStart.MatterID AND mdvStart.DetailFieldID = 145690
	INNER JOIN dbo.MatterDetailValues mdvEnd WITH (NOLOCK) ON m.MatterID = mdvEnd.MatterID AND mdvEnd.DetailFieldID = 145691
	LEFT JOIN dbo.MatterDetailValues mdvRenewalStartOverride WITH (NOLOCK) ON m.MatterID = mdvRenewalStartOverride.MatterID AND mdvRenewalStartOverride.DetailFieldID = 162630
	LEFT JOIN dbo.MatterDetailValues mdvRenewalEndOverride WITH (NOLOCK) ON m.MatterID = mdvRenewalEndOverride.MatterID AND mdvRenewalEndOverride.DetailFieldID = 162631
	WHERE mdvScheme.ValueInt = @SchemeID
	AND (
		(@IsRenewal = 0 AND mdvStart.ValueDate <= @Date	AND ISNULL(mdvEnd.ValueDate,'2999-12-31') >= @Date)
	OR  (@IsRenewal = 1 AND ISNULL(mdvRenewalStartOverride.ValueDate,mdvStart.ValueDate) <= @Date AND COALESCE(mdvRenewalEndOverride.ValueDate,mdvEnd.ValueDate,'2999-12-31') >= @Date)
		)
		
	RETURN @CurrentPolicyMatterID

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicyFromTypeAndDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_GetPolicyFromTypeAndDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicyFromTypeAndDate] TO [sp_executeall]
GO
