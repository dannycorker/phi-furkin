SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-06-18
-- Description:	Take a MatterID and return labels of the discounts for which it is eligible.
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetDiscountLabelForMatter]
(
	@MatterID	INT
)
RETURNS VARCHAR(2000)
AS
BEGIN
	
	DECLARE  @ReturnValue		VARCHAR(2000) = ''
			,@CampaignCode		VARCHAR(2000) = dbo.fnGetSimpleDv(175564,@MatterID) /*Tracking Code*/
			,@StartDate			DATE = dbo.fnGetSimpleDvAsDate(170036,@MatterID) /*Policy Start Date*/
			,@CustomerID		INT
			,@LeadID			INT
			,@SchemeMatterID	INT
			,@SchemeLeadID		INT
			
	SELECT	 @CustomerID	= m.CustomerID
			,@LeadID		= m.LeadID
	FROM Matter m WITH ( NOLOCK )
	WHERE m.MatterID = @MatterID
	
	--SELECT @SchemeMatterID = dbo.fn_C00_1273_GetCurrentScheme(dbo.fnGetSimpleDvAsInt(170034,@MatterID) /*Current Policy*/,@StartDate)
	
	/*Multi-Pet discounts*/
	IF EXISTS ( SELECT * /*Other live pets for this customer*/
	            FROM Lead l WITH ( NOLOCK ) 
	            INNER JOIN Matter m WITH (NOLOCK) on m.LeadID = l.LeadID 
	            INNER JOIN MatterDetailValues mdvStat WITH ( NOLOCK ) on mdvStat.MatterID = m.MatterID AND mdvStat.DetailFieldID = 170038 /*Policy Status*/
	            WHERE l.CustomerID = @CustomerID 
	            AND l.LeadTypeID = 1492 /*Policy Admin*/
	            AND l.LeadID <> @LeadID 
	            AND mdvStat.ValueInt IN ( 43002 /*Live*/, 74573 /*With Underwriting*/ )
	          )
	BEGIN
		SELECT @ReturnValue += 'Multi-Pet Discount (1 Month), '
	END
	
	SELECT @SchemeMatterID = dbo.fn_C00_1273_GetCurrentSchemeWithSpecies	(dbo.fnGetSimpleDvAsInt(170034,@MatterID) /*Current Policy*/
																			,@StartDate /*Policy Start Date*/ 
																			,dbo.fnGetSimpleDvAsInt(144269 /*Species*/, dbo.fnGetSimpleDvAsInt(144272,@LeadID) /*Pet Type*/ ) 
																			)
	
	SELECT @SchemeLeadID = m.LeadID
	FROM Matter m WITH ( NOLOCK )
	WHERE m.MatterID = @SchemeMatterID
	
	/*Tracking Code discounts*/
	IF @CampaignCode <> ''
	BEGIN
		SELECT TOP 1 @ReturnValue += 'Code ' + rdv.DetailValue + ' (' + tdvMonths.DetailValue + ' month' + CASE WHEN tdvMonths.ValueInt = 1 THEN '' ELSE 's' END + '), '
		FROM TableRows tr WITH ( NOLOCK )
		INNER JOIN TableDetailValues tdvRes WITH ( NOLOCK ) on tdvRes.TableRowID = tr.TableRowID AND tdvRes.DetailFieldID = 177364 /*Campaign Code*/
		INNER JOIN ResourceListDetailValues rdv WITH ( NOLOCK ) on rdv.ResourceListID = tdvRes.ResourceListID AND rdv.DetailFieldID = 177363 /*Tier 3*/
		 LEFT JOIN TableDetailValues tdvFrom WITH ( NOLOCK ) on tdvFrom.TableRowID = tr.TableRowID AND tdvFrom.DetailFieldID = 177365 /*Date From*/
		 LEFT JOIN TableDetailValues tdvTo WITH ( NOLOCK ) on tdvTo.TableRowID = tr.TableRowID AND tdvTo.DetailFieldID = 177366 /*Date To*/
		INNER JOIN TableDetailValues tdvMonths WITH ( NOLOCK ) on tdvMonths.TableRowID = tr.TableRowID AND tdvMonths.DetailFieldID = 177472 /*Months Free*/
		WHERE tr.LeadID = @SchemeLeadID
		AND rdv.DetailValue = @CampaignCode
		AND (@StartDate >= tdvFrom.ValueDate or tdvFrom.ValueDate is NULL)
		AND (@StartDate <= tdvTo.ValueDate or tdvTo.ValueDate is NULL)
		ORDER BY tdvFrom.ValueDate DESC
	END
	
	IF DATALENGTH(@ReturnValue) <> 0
	BEGIN
		SELECT @ReturnValue = LEFT(@ReturnValue,DATALENGTH(@ReturnValue)-2)
	END

	RETURN @ReturnValue

END






GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetDiscountLabelForMatter] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetDiscountLabelForMatter] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetDiscountLabelForMatter] TO [sp_executeall]
GO
