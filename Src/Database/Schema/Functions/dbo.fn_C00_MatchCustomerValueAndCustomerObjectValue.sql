SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-05-20
-- Description:	Returns a LeadOrMatter Identifier (LeadID, CaseID) etc if it can find a customer match for the customer fields
--				and an object match for the object fields 
-- Mods:		dcm 04/06/2014 return casenum option
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_MatchCustomerValueAndCustomerObjectValue]
(
	@CustomerField			INT,		-- 1 = mobilenumber, 2 = postcode
	@CustomerFieldValue		VARCHAR(100),
	@ObjectDetailFieldID	INT,
	@ObjectDetailValue		VARCHAR(100),
	@IdentifierToReturn		INT,			-- 1 for leadID, 2 for MatterID, 10 for CustomerID, 11 for CaseID
	@MustBeUnique			INT = 1,			-- returns TOP 1 for object ID sorted ASC if this set to 0
	@ReturnCaseNum			INT = 0
)
RETURNS INTEGER
AS
BEGIN
	
	DECLARE @DetailFieldSubTypeID	INT,
			@ReturnValue			INT = 0,
			@ClientID				INT,
			@CustomerID				INT,
			@LeadID                 INT,
			@CaseID					INT,
			@MatterID				INT
			
	DECLARE @ObjectIDList TABLE ( CustomerID INT, LeadID INT , CaseID INT, MatterID INT )		
			

	SELECT @DetailFieldSubTypeID = df.LeadOrMatter, @ClientID=df.ClientID
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @ObjectDetailFieldID

	IF @IdentifierToReturn NOT IN (1,2,11,10) RETURN 0
	
	IF @DetailFieldSubTypeID = 1
	BEGIN
		INSERT INTO @ObjectIDList
		SELECT c.CustomerID,l.LeadID,CASE WHEN @ReturnCaseNum=1 THEN ca.CaseNum ELSE ca.CaseID END [CaseID],m.MatterID 
		FROM Customers c WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
		INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
		INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID=ca.CaseID
		INNER JOIN LeadDetailValues odv WITH (NOLOCK) ON l.LeadID=odv.LeadID 
			and odv.DetailFieldID=@ObjectDetailFieldID
			and odv.DetailValue=@ObjectDetailValue
		WHERE c.ClientID=@ClientID -- and c.Test=0
		AND (@CustomerField=1 AND REPLACE('0'+c.MobileTelephone,'00','0')=REPLACE('0'+@CustomerFieldValue,'00','0'))
			OR (@CustomerID=2 AND LTRIM(RTRIM(REPLACE(c.PostCode,' ','')))=LTRIM(RTRIM(REPLACE(@CustomerFieldValue,' ',''))))
		ORDER BY c.CustomerID,l.LeadID,ca.CaseID,m.MatterID
	END
	ELSE
	IF @DetailFieldSubTypeID = 2
	BEGIN
		INSERT INTO @ObjectIDList
		SELECT c.CustomerID,l.LeadID,CASE WHEN @ReturnCaseNum=1 THEN ca.CaseNum ELSE ca.CaseID END [CaseID],m.MatterID 
		FROM Customers c WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
		INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
		INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID=ca.CaseID
		INNER JOIN MatterDetailValues odv WITH (NOLOCK) ON m.MatterID=odv.MatterID 
			and odv.DetailFieldID=@ObjectDetailFieldID
			and odv.DetailValue=@ObjectDetailValue
		WHERE c.ClientID=@ClientID -- and c.Test=0 
		AND (@CustomerField=1 AND REPLACE('0'+c.MobileTelephone,'00','0')=REPLACE('0'+@CustomerFieldValue,'00','0'))
			OR (@CustomerID=2 AND LTRIM(RTRIM(REPLACE(c.PostCode,' ','')))=LTRIM(RTRIM(REPLACE(@CustomerFieldValue,' ',''))))
		ORDER BY c.CustomerID,l.LeadID,ca.CaseID,m.MatterID
	END
	ELSE
	IF @DetailFieldSubTypeID = 10
	BEGIN
		INSERT INTO @ObjectIDList
		SELECT c.CustomerID,l.LeadID,CASE WHEN @ReturnCaseNum=1 THEN ca.CaseNum ELSE ca.CaseID END [CaseID],m.MatterID 
		FROM Customers c WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
		INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
		INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID=ca.CaseID
		INNER JOIN CustomerDetailValues odv WITH (NOLOCK) ON c.CustomerID=odv.CustomerID 
			and odv.DetailFieldID=@ObjectDetailFieldID
			and odv.DetailValue=@ObjectDetailValue
		WHERE c.ClientID=@ClientID -- and c.Test=0 
		AND (@CustomerField=1 AND REPLACE('0'+c.MobileTelephone,'00','0')=REPLACE('0'+@CustomerFieldValue,'00','0'))
			OR (@CustomerID=2 AND LTRIM(RTRIM(REPLACE(c.PostCode,' ','')))=LTRIM(RTRIM(REPLACE(@CustomerFieldValue,' ',''))))
		ORDER BY c.CustomerID,l.LeadID,ca.CaseID,m.MatterID
	END
	ELSE
	IF @DetailFieldSubTypeID = 11
	BEGIN
		INSERT INTO @ObjectIDList
		SELECT c.CustomerID,l.LeadID,CASE WHEN @ReturnCaseNum=1 THEN ca.CaseNum ELSE ca.CaseID END [CaseID],m.MatterID 
		FROM Customers c WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID
		INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
		INNER JOIN Matter m WITH (NOLOCK) ON m.CaseID=ca.CaseID
		INNER JOIN CaseDetailValues odv WITH (NOLOCK) ON ca.CaseID=odv.CaseID 
			and odv.DetailFieldID=@ObjectDetailFieldID
			and odv.DetailValue=@ObjectDetailValue
		WHERE c.ClientID=@ClientID -- and c.Test=0 
		AND (@CustomerField=1 AND REPLACE('0'+c.MobileTelephone,'00','0')=REPLACE('0'+@CustomerFieldValue,'00','0'))
			OR (@CustomerID=2 AND LTRIM(RTRIM(REPLACE(c.PostCode,' ','')))=LTRIM(RTRIM(REPLACE(@CustomerFieldValue,' ',''))))
		ORDER BY c.CustomerID,l.LeadID,ca.CaseID,m.MatterID
	END

	IF @IdentifierToReturn = 1 SELECT @ReturnValue=COUNT(*) FROM (SELECT DISTINCT LeadID FROM @ObjectIDList) as c
	IF @IdentifierToReturn = 2 SELECT @ReturnValue=COUNT(*) FROM (SELECT DISTINCT MatterID FROM @ObjectIDList) as c
	IF @IdentifierToReturn = 10 SELECT @ReturnValue=COUNT(*) FROM (SELECT DISTINCT CustomerID FROM @ObjectIDList) as c
	IF @IdentifierToReturn = 11 SELECT @ReturnValue=COUNT(*) FROM (SELECT DISTINCT CaseID FROM @ObjectIDList) as c
	
	IF @ReturnValue > 1 AND @MustBeUnique = 1
		SELECT @ReturnValue = 0
		
	IF @ReturnValue <> 0
	BEGIN
	
		IF @IdentifierToReturn = 1 SELECT TOP 1 @ReturnValue=LeadID FROM @ObjectIDList ORDER BY LeadID
		IF @IdentifierToReturn = 2 SELECT TOP 1 @ReturnValue=MatterID FROM @ObjectIDList ORDER BY MatterID
		IF @IdentifierToReturn = 10 SELECT TOP 1 @ReturnValue=CustomerID FROM @ObjectIDList ORDER BY CustomerID
		IF @IdentifierToReturn = 11 SELECT TOP 1 @ReturnValue=CaseID FROM @ObjectIDList ORDER BY CaseID
	
	END	

	RETURN @ReturnValue
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_MatchCustomerValueAndCustomerObjectValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_MatchCustomerValueAndCustomerObjectValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_MatchCustomerValueAndCustomerObjectValue] TO [sp_executeall]
GO
