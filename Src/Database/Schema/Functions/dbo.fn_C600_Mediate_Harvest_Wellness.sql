SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire
-- Create date: 2020-09-21
-- Description:	harvest Wellness product 
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Mediate_Harvest_Wellness]
(
	 @PolicyID		INT
	,@VisionMode	BIT = 0
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE  @JsonData	VARCHAR(MAX) = ''

	DECLARE @Products Table (ProductID INT, MatterID INT, Wellness INT, StartDate DATETIME, EndDate DATETIME)

	INSERT INTO @Products
	SELECT TOP 1 rldv.DetailValue, tr1.MatterID, 1, tdv1.ValueDateTime, tdv2.ValueDateTime
	FROM Tablerows tr1 WITH (NOLOCK) 
	INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr1.TableRowID AND tdv.DetailFieldID = 314234
	INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = tdv.ResourceListID AND rldv.DetailFieldID = 314268
	INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tdv.TableRowID AND tdv1.DetailFieldID = 314237
	INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = tdv.TableRowID AND tdv2.DetailFieldID = 314239
	INNER JOIN TableDetailValues tdv3 WITH (NOLOCK) ON tdv3.TableRowID = tdv.TableRowID AND tdv3.DetailFieldID = 314236
	WHERE 
	tr1.DetailFieldID = 314240
	AND tr1.MatterID = @PolicyID
	ORDER BY tr1.TableRowID DESC 

	IF @PolicyID <> 0
	BEGIN
		SELECT  @JsonData +=
		(SELECT	
								p.productid [productId]
								,p.StartDate  [startDate]
								,p.EndDate [endDate]
						FROM 
						@Products p 
						JOIN MatterDetailValues mdvProd WITH (NOLOCK) ON mdvProd.MatterID = p.matterid
						JOIN OneVision oneVision		WITH (NOLOCK) ON oneVision.PAMatterID = mdvProd.MatterID AND oneVision.PolicyTermType = 'Wellness' /*One Vision Table*/
						WHERE p.MatterID = @PolicyID
						AND mdvProd.DetailFieldID = 170034
						ORDER BY p.productid DESC
						FOR JSON PATH, WITHOUT_ARRAY_WRAPPER

					  )

					  	END

	RETURN @JsonData

END

GO
