SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2018-07-04
-- Description:	GetRlDv using the appropriate Object ID rather than always going from CaseID
-- GPR 2019-10-13 created to 603 for LPC-35
-- =============================================
CREATE FUNCTION [dbo].[fnGetSimpleRLDv]
(
	 @DetailFieldID		INT /* L/M Field */
	,@ColumnFieldID		INT /* ResourceList Detail Field ID */
	,@ObjectID			INT
	,@DecodeLookupLists	BIT
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE  @DetailValue		VARCHAR(2000)

	/*
	Use GetSimpleDvAsInt to get the ResourceListID, then wrap that in either GetSimpleDv or fnGetSimpleDvLuliOrValue 
	depending on whether or not we want to try and decode dropdown lists
	*/

	IF @DecodeLookupLists = 1
	BEGIN
		SELECT	 @DetailValue = dbo.fnGetSimpleDvLuliOrValue( @ColumnFieldID, dbo.fnGetSimpleDvAsInt(@DetailFieldID,@ObjectID) )
	END
	ELSE
	BEGIN
		SELECT	 @DetailValue = dbo.fnGetSimpleDv( @ColumnFieldID, dbo.fnGetSimpleDvAsInt(@DetailFieldID,@ObjectID) )
	END

	RETURN @DetailValue

END









GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleRLDv] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetSimpleRLDv] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleRLDv] TO [sp_executeall]
GO
