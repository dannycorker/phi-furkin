SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-06-04
-- Description:	Returns the CustomerID for a MatterID
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_GetCustomerIDFromMatterID]
(
	@MatterID INT
)
RETURNS INT
AS
BEGIN
	
	DECLARE @CustomerID INT

	SELECT @CustomerID=l.CustomerID
	FROM Matter m WITH (NOLOCK)
	INNER JOIN Lead l WITH (NOLOCK) ON m.LeadID=l.LeadID
	WHERE m.MatterID = @MatterID

	RETURN @CustomerID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetCustomerIDFromMatterID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_GetCustomerIDFromMatterID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetCustomerIDFromMatterID] TO [sp_executeall]
GO
