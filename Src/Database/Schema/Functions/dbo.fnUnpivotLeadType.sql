SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Unpivot the columns in the LeadType table 
-- =============================================
CREATE FUNCTION [dbo].[fnUnpivotLeadType] 
(	
	@LeadTypeID int
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT ColumnName as [RawColumnName], '[!' + ColumnName + ']' as [FormattedColumnName], RealValue
	FROM
	(
		SELECT 
		COALESCE(CONVERT(varchar(255), c.AllowLeadPageChoices), '') as [AllowLeadPageChoices],
		COALESCE(CONVERT(varchar(255), c.AllowMatterPageChoices), '') as [AllowMatterPageChoices],
		COALESCE(CONVERT(varchar(255), c.CaseDisplayName), '') as [CaseDisplayName],
		COALESCE(CONVERT(varchar(255), c.ClientID), '') as [ClientID],
		COALESCE(CONVERT(varchar(255), c.CustomerDisplayName), '') as [CustomerDisplayName],
		COALESCE(CONVERT(varchar(255), c.CustomerTabImageFileName), '') as [CustomerTabImageFileName],
		COALESCE(CONVERT(varchar(255), c.Enabled), '') as [Enabled],
		COALESCE(CONVERT(varchar(255), c.FollowupWorkingDaysOnly), '') as [FollowupWorkingDaysOnly],
		COALESCE(CONVERT(varchar(255), c.IsBusiness), '') as [IsBusiness],
		COALESCE(CONVERT(varchar(255), c.IsRPIEnabled), '') as [IsRPIEnabled],
		COALESCE(CONVERT(varchar(255), c.LastReferenceInteger), '') as [LastReferenceInteger],
		COALESCE(CONVERT(varchar(255), c.LeadDetailsTabImageFileName), '') as [LeadDetailsTabImageFileName],
		COALESCE(CONVERT(varchar(255), c.LeadDisplayName), '') as [LeadDisplayName],
		COALESCE(CONVERT(varchar(255), c.LeadTypeDescription), '') as [LeadTypeDescription],
		COALESCE(CONVERT(varchar(255), c.LeadTypeID), '') as [LeadTypeID],
		COALESCE(CONVERT(varchar(255), c.LeadTypeName), '') as [LeadTypeName],
		COALESCE(CONVERT(varchar(255), c.Live), '') as [Live],
		COALESCE(CONVERT(varchar(255), c.MatterDisplayName), '') as [MatterDisplayName],
		COALESCE(CONVERT(varchar(255), c.MatterLastReferenceInteger), '') as [MatterLastReferenceInteger],
		COALESCE(CONVERT(varchar(255), c.MatterReferenceValueFormatID), '') as [MatterReferenceValueFormatID],
		COALESCE(CONVERT(varchar(255), c.ReferenceValueFormatID), '') as [ReferenceValueFormatID],
		COALESCE(CONVERT(varchar(255), c.UseEventComments), '') as [UseEventComments],
		COALESCE(CONVERT(varchar(255), c.UseEventCosts), '') as [UseEventCosts],
		COALESCE(CONVERT(varchar(255), c.UseEventDisbursements), '') as [UseEventDisbursements],
		COALESCE(CONVERT(varchar(255), c.UseEventUOEs), '') as [UseEventUOEs]
		FROM dbo.LeadType c WITH (NOLOCK) 
		WHERE c.LeadTypeID = @LeadTypeID) p
	UNPIVOT
	(
		RealValue FOR ColumnName IN
		(
			[AllowLeadPageChoices],[AllowMatterPageChoices],[CaseDisplayName],[ClientID],[CustomerDisplayName],[CustomerTabImageFileName],[Enabled],[FollowupWorkingDaysOnly],[IsBusiness],[IsRPIEnabled],[LastReferenceInteger],[LeadDetailsTabImageFileName],[LeadDisplayName],[LeadTypeDescription],[LeadTypeID],[LeadTypeName],[Live],[MatterDisplayName],[MatterLastReferenceInteger],[MatterReferenceValueFormatID],[ReferenceValueFormatID],[UseEventComments],[UseEventCosts],[UseEventDisbursements],[UseEventUOEs]
		)
	) as unpvt

)





GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotLeadType] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnUnpivotLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotLeadType] TO [sp_executeall]
GO
