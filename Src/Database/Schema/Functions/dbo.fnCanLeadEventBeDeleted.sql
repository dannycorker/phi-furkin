SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2007-12-18
-- Description:	Check if an event can be deleted in theory
-- JWG 2010-03-08 Block deletion if doc queue record is being used by a zip at this exact moment.
-- JWG 2012-01-04 Don't try to call the logging proc from here, as it is not allowed within a function.
-- NG  2020-12-13 Only allow deletion by AQ users
-- =============================================
CREATE FUNCTION [dbo].[fnCanLeadEventBeDeleted]  
(
	@LeadEventID int, 
	@UserID int 
)
RETURNS int
AS
BEGIN
		
	DECLARE @ResultVar int, 
			@WhoCreated int, 
			@WhenCreated datetime, 
			@NextEventID int,
			@ThreadsInProgressCount int, 
			@DocumentQueueID int, 
			@DocumentZipUsageCount int,
			@IsAquarium int

	SELECT @WhoCreated = le.WhoCreated, 
	@WhenCreated = le.WhenCreated, 
	@NextEventID = le.NextEventID, 
	@DocumentQueueID = le.DocumentQueueID,
	@ResultVar = -1, 
	@IsAquarium = cp.IsAquarium -- added NG 2020-12-03
	FROM dbo.LeadEvent le WITH (NOLOCK) 
	INNER JOIN dbo.ClientPersonnel cp WITH (NOLOCK) ON le.ClientID = cp.ClientID 
	WHERE le.LeadEventID = @LeadEventID 
	AND cp.ClientPersonnelID = @UserID 
	
	IF @@ROWCOUNT < 1 
	BEGIN
		SELECT @ResultVar = -1
	END

	ELSE

	IF  @IsAquarium = 0 -- added NG 2020-12-03 to prevent client users from deleting lead events
	BEGIN
		SELECT @ResultVar = -1
	END

	ELSE

	BEGIN

		-- If the event has been followed up already, then it cannot
		-- be deleted by anyone, ever.  Any later events must all be
		-- deleted sequentially in reverse order.  RETURNS -1.
		IF @NextEventID > 0 AND @NextEventID <> @LeadEventID
		BEGIN
			SELECT @ResultVar = -1
		END
		ELSE
		BEGIN

			SELECT @ThreadsInProgressCount = count(*) 
			FROM dbo.LeadEventThreadCompletion letc WITH (NOLOCK)   
			WHERE letc.FromLeadEventID = @LeadEventID 
			AND letc.ToLeadEventID IS NOT NULL

			IF @ThreadsInProgressCount > 0
			BEGIN
				SELECT @ResultVar = -1
			END
			ELSE
			BEGIN

				IF @DocumentQueueID IS NOT NULL
				BEGIN
					SELECT @DocumentZipUsageCount = count(*) 
					FROM dbo.DocumentZip dz WITH (NOLOCK) 
					INNER JOIN dbo.DocumentZipInformation dzi WITH (NOLOCK) ON dzi.DocumentZipInformationID = dz.DocumentZipInformationID 
					WHERE dz.DocumentQueueID = @DocumentQueueID 
					AND dzi.StatusID NOT IN (31, 32, 34, 35)
				END

				IF @DocumentZipUsageCount > 0
				BEGIN
				
					SELECT @ResultVar = -1
					
					--EXEC dbo._C00_LogIt 'Info', 'fnCanLeadEventBeDeleted', 'Zips', 'Blocked deletion because zip is in use right now!', 2795 
					--Can't affect data in a function!
					
				END
				
				ELSE
				
				BEGIN
				
					-- Open events (not followed up) can be deleted by the
					-- user that created them if it is still the same
					-- calendar day.  RETURNS 1.
					IF @WhoCreated = @UserID AND DateDiff(dd, @WhenCreated, dbo.fn_GetDate_Local()) = 0
					BEGIN
						SELECT @ResultVar = 1
					END
					ELSE
					-- Open events (not followed up) created by a different
					-- user can only be deleted with special user privs.
					-- RETURN 0 so the application logic can decide what to do.
					BEGIN
						SELECT @ResultVar = 0
					END
					
				END
				
			END

		END

	END

	RETURN @ResultVar

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fnCanLeadEventBeDeleted] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnCanLeadEventBeDeleted] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnCanLeadEventBeDeleted] TO [sp_executeall]
GO
