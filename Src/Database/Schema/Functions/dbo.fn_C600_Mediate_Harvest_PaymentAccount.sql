SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire
-- Create date:	2020-07-30
-- Description:	harvests payment account json object
-- 2021-03-09 CPS for JIRA FURKIN-129 | Concatenate InstitutionNumber and SortCode and left-pad with 0s if required
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Mediate_Harvest_PaymentAccount]
(
	 @PolicyID			INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE  @JsonData	VARCHAR(MAX) =''
			,@PetJson	VARCHAR(MAX) = ''

	IF @PolicyID > 0
	BEGIN
		SELECT  @JsonData +=
		(
			SELECT TOP 1
			 RIGHT('000000000' + LEFT(ISNULL(a.InstitutionNumber,'') + ISNULL(a.Sortcode,''),9),9)	[sortCode]
			,a.AccountNumber					[accountNumber]
			,mdv.DetailValue [bankName]
			,a.AccountHolderName	 [payee]
			,CASE WHEN a.AccountTypeID = 1 THEN 2 ELSE 1 END [paymentMethod]
			,1 [paymentRecipientType]
			,oneVision.VisionCustomerID [paymentRecipientId]
			FROM Customers cu								WITH (NOLOCK)
			INNER JOIN Lead l								WITH (NOLOCK) on l.CustomerID = cu.CustomerID
			INNER JOIN Cases c								WITH (NOLOCK) on c.LeadID = l.LeadID
			INNER JOIN Matter m								WITH (NOLOCK) on m.CaseID = c.CaseID
			JOIN Account a									WITH (NOLOCK) ON a.CustomerID = cu.CustomerID  
			JOIN MatterDetailValues mdv						WITH (NOLOCK) ON mdv.MatterID = m.MatterID  AND mdv.DetailFieldID = 170170
			JOIN MatterDetailValues mdv1					WITH (NOLOCK) ON mdv1.MatterID = m.MatterID  AND mdv1.DetailFieldID = 170170
			JOIN OneVision oneVision						WITH (NOLOCK) ON OneVision.PAMatterID = m.MatterID
			WHERE m.MatterID = @PolicyID
			AND l.LeadTypeID = 1492 /*Policy Admin*/
			AND a.AccountUseID = 2
			AND a.Active = 1
			ORDER BY AccountID DESC 
			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		)
	END


	RETURN @JsonData

END













GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Mediate_Harvest_PaymentAccount] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_Mediate_Harvest_PaymentAccount] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Mediate_Harvest_PaymentAccount] TO [sp_executeall]
GO
