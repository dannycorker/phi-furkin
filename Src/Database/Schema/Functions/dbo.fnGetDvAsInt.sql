SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 14-July-2009
-- Description:	Get DV Based on Case ID
-- 2016-01-21 ACE Updated to include Client Detail Values
-- 2017-06-15 LB Updated to reflect new performance enhanced version of fnGetDv
-- =============================================
CREATE FUNCTION [dbo].[fnGetDvAsInt]
(
	@DetailFieldID		int,
	@CaseID				int
)
RETURNS int
AS
BEGIN

DECLARE @ValueInt as varchar(2000),
		@LeadOrMatter int,
		@ClientID INT

	Select @LeadOrMatter = LeadOrmatter, @ClientID = ClientID
	From DetailFields with (nolock) 
	Where DetailFieldID = @DetailFieldID

	IF @LeadOrMatter = 1
	BEGIN

		SELECT @ValueInt = dbo.fnGetLeadDvCASEAsInt(@DetailFieldID,@CaseID)
	
	END

	IF @LeadOrMatter = 2
	BEGIN

		SELECT @ValueInt = dbo.fnGetMatterDvCASEAsInt(@DetailFieldID,@CaseID)
	
	END

	IF @LeadOrMatter = 10
	BEGIN

		SELECT @ValueInt = dbo.fnGetCustomerDvCASEAsInt(@DetailFieldID,@CaseID) 
	
	END
		
	IF @LeadOrMatter = 11
	BEGIN
		
		SELECT @ValueInt = dbo.fnGetCaseDvCASEAsInt(@DetailFieldID,@CaseID)
		
	END
	
	IF @LeadOrMatter = 12
	BEGIN
		
		SELECT @ValueInt = dbo.fnGetClientDvAsInt(@DetailFieldID,@ClientID)
		
	END

	RETURN @ValueInt
	
	END

GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDvAsInt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetDvAsInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDvAsInt] TO [sp_executeall]
GO
