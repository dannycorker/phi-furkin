SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-09-19
-- Description:	Function to get the a value from the last
--				row in a table
-- =============================================
CREATE FUNCTION [dbo].[fnGetLastRowValue]
(
@CustomerID int,
@TableFieldID int,
@ColumnFieldID int
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE @DetailFieldSubtype int,
			@DetailValue varchar(2000)
	
	SELECT @DetailFieldSubType = LeadOrMatter 
	FROM DetailFields df WITH (NOLOCK)
	WHERE df.DetailFieldID = @TableFieldID
	
	IF @DetailFieldSubtype = 2
	BEGIN
	
		
	
		SELECT TOP (1) @DetailValue = tdv.DetailValue
		FROM TableRows tr WITH (NOLOCK)
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = @ColumnFieldID
		INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = tr.MatterID and m.CustomerID = @CustomerID
		WHERE tr.DetailFieldID = @TableFieldID
		ORDER BY tr.TableRowID DESC
	
	END

	-- Return the result of the function
	RETURN @DetailValue

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLastRowValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetLastRowValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLastRowValue] TO [sp_executeall]
GO
