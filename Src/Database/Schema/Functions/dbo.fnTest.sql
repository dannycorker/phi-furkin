SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-20
-- Description:	Temp
-- =============================================
CREATE FUNCTION [dbo].[fnTest] 
(	
	@tvpInt tvpInt READONLY
)
RETURNS TABLE 
AS
RETURN 
(	
	SELECT df.* 
	FROM @tvpInt t 
	INNER JOIN dbo.DetailFields df WITH (NOLOCK) ON df.DetailFieldID = t.AnyID 
)








GO
GRANT VIEW DEFINITION ON  [dbo].[fnTest] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnTest] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnTest] TO [sp_executeall]
GO
