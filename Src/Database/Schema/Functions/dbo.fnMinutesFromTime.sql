SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2008-01-04
-- Description:	Calculate how minutes are in a time in format '(hhhhhh)hh:mm'
-- =============================================
CREATE FUNCTION [dbo].[fnMinutesFromTime] 
(
	-- Add the parameters for the function here
	@starttime varchar(11) 
)
RETURNS smallint
AS
BEGIN

	DECLARE @totalmins int
	
	-- Cater for single-digit hours, eg "9" instead of "119:45"
	-- Also caters for empty strings
	IF LEN(@starttime) BETWEEN 1 AND 2
	BEGIN
		SELECT @starttime = right('00000000000' + @starttime + ':00', 11)
	END
	ELSE	
	BEGIN
		SELECT @starttime = right('00000000000' + @starttime, 11)
	END
	

	-- CONVERT end time '15:30' to minutes (15 * 60) + 30
	-- Ditto for start time '09:15' (9 * 60) + 15
	-- Return the difference
	DECLARE @starthh char(9), @startmm char(2)

	SELECT @starthh = substring(@starttime,1,8), 
		   @startmm = substring(@starttime,10,2)

	IF (substring(@starthh,1,1) BETWEEN '0' AND '9') AND
	   (substring(@starthh,2,1) BETWEEN '0' AND '9') AND
	   (substring(@starthh,3,1) BETWEEN '0' AND '9') AND
	   (substring(@starthh,4,1) BETWEEN '0' AND '9') AND
	   (substring(@starthh,5,1) BETWEEN '0' AND '9') AND
	   (substring(@starthh,6,1) BETWEEN '0' AND '9') AND
	   (substring(@starthh,7,1) BETWEEN '0' AND '9') AND
	   (substring(@starthh,8,1) BETWEEN '0' AND '9') AND
	   (substring(@startmm,1,1) BETWEEN '0' AND '5') AND
	   (substring(@startmm,2,1) BETWEEN '0' AND '9')
	BEGIN
		SELECT @totalmins = (@starthh * 60) + @startmm
	END
	ELSE
	BEGIN
		SELECT @totalmins = 0
	END

	RETURN @totalmins

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnMinutesFromTime] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnMinutesFromTime] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnMinutesFromTime] TO [sp_executeall]
GO
