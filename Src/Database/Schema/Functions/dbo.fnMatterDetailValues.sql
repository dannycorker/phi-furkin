SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2011-02-03
-- Description:	Like vMatterDetailValues but with parameters to narrow down the search
-- =============================================
CREATE FUNCTION [dbo].[fnMatterDetailValues] 
(	
	@MatterID int,
	@DetailFieldID int,
	@ClientID int
)
RETURNS TABLE 
AS
RETURN 
(
	/*SELECT mdv1.MatterDetailValueID, 
	m.ClientID, 
	m.LeadID, 
	m.MatterID, 
	df1.DetailFieldID, 
	df1.FieldName, 
	df1.FieldCaption, 
	qt1.Description as FieldFormat, 
	CASE qt1.QuestionTypeID WHEN 4 THEN COALESCE (luli1.ItemValue, '') ELSE COALESCE (mdv1.DetailValue, '') END as DetailValue, 
	mdv1.DetailValue as RawValue, 
	CASE qt1.QuestionTypeID WHEN 4 THEN luli1.ValueInt ELSE mdv1.ValueInt END AS ValueInt, 
	CASE qt1.QuestionTypeID WHEN 4 THEN luli1.ValueMoney ELSE mdv1.ValueMoney END AS ValueMoney, 
	CASE qt1.QuestionTypeID WHEN 4 THEN luli1.ValueDate ELSE mdv1.ValueDate END AS ValueDate, 
	CASE qt1.QuestionTypeID WHEN 4 THEN luli1.ValueDateTime ELSE mdv1.ValueDateTime END AS ValueDateTime, qt1.QuestionTypeID 
	FROM dbo.Matter m WITH (NOLOCK) 
	CROSS JOIN dbo.DetailFields AS df1 WITH (NOLOCK) 
	INNER JOIN dbo.QuestionTypes AS qt1 WITH (NOLOCK) ON df1.QuestionTypeID = qt1.QuestionTypeID -- Get the field format (for info only)
	LEFT JOIN dbo.MatterDetailValues AS mdv1 WITH (NOLOCK) ON mdv1.MatterID = m.MatterID AND mdv1.DetailFieldID = df1.DetailFieldID -- Finally get the value, null if not present
	LEFT JOIN dbo.LookupListItems AS luli1 WITH (NOLOCK) ON luli1.LookupListID = df1.LookupListID AND luli1.LookupListItemID = mdv1.ValueInt -- Finally, look up the value
	WHERE df1.DetailFieldID = @DetailFieldID */
	
	SELECT MatterDetailValueID,ClientID,LeadID,MatterID,DetailFieldID,DetailValue,ValueInt,ValueMoney,ValueDate,ValueDateTime
	FROM dbo.MatterDetailValues mdv WITH (NOLOCK) 
	WHERE mdv.MatterID = @MatterID AND mdv.DetailFieldID = @DetailFieldID AND mdv.ClientID IN (0, @ClientID)
	UNION
	SELECT NULL AS MatterDetailValueID,@ClientID AS ClientID,NULL AS LeadID,@MatterID AS MatterID,@DetailFieldID AS DetailFieldID,'' AS DetailValue,NULL AS ValueInt,NULL AS ValueMoney,NULL AS ValueDate,NULL AS ValueDateTime
	WHERE NOT EXISTS (
		SELECT * 
		FROM dbo.MatterDetailValues mdv WITH (NOLOCK) 
		WHERE mdv.MatterID = @MatterID AND mdv.DetailFieldID = @DetailFieldID AND mdv.ClientID IN (0, @ClientID) 
	)
)








GO
GRANT VIEW DEFINITION ON  [dbo].[fnMatterDetailValues] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnMatterDetailValues] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnMatterDetailValues] TO [sp_executeall]
GO
