SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2008-09-30
-- Description:	Return a 1-row table of address details from an input string
-- =============================================
CREATE FUNCTION [dbo].[fnColsFromString]
(
	@instr nvarchar(max),
	@delimiter varchar(1) = '|'
)
RETURNS 
@TableOut TABLE 
(
	Col1 varchar(200),
	Col2 varchar(200),
	Col3 varchar(200),
	Col4 varchar(200),
	Col5 varchar(200)
)
AS
BEGIN

	DECLARE @whichfield varchar(10)
	DECLARE @position  int

	SET @instr = LTRIM(RTRIM(@instr)) + @delimiter
	SET @position = CHARINDEX(@delimiter, @instr, 1)
	SET @whichfield = 'Col1'

	IF REPLACE(@instr, @delimiter, '') <> ''
	BEGIN
		WHILE @position > 0
		BEGIN
			IF @whichfield = 'Col5'
			BEGIN
				UPDATE @TableOut 
				SET Col5 = LTRIM(RTRIM(LEFT(@instr, @position - 1)))

				SET @whichfield = '' -- Ignore any extra fields from now on
			END
			IF @whichfield = 'Col4'
			BEGIN
				UPDATE @TableOut 
				SET Col4 = LTRIM(RTRIM(LEFT(@instr, @position - 1)))

				SET @whichfield = 'Col5'
			END
			IF @whichfield = 'Col3'
			BEGIN
				UPDATE @TableOut 
				SET Col3 = LTRIM(RTRIM(LEFT(@instr, @position - 1)))

				SET @whichfield = 'Col4'
			END
			IF @whichfield = 'Col2'
			BEGIN
				UPDATE @TableOut 
				SET Col2 = LTRIM(RTRIM(LEFT(@instr, @position - 1)))

				SET @whichfield = 'Col3'
			END
			IF @whichfield = 'Col1'
			BEGIN
				INSERT INTO @TableOut (Col1,Col2,Col3,Col4,Col5) 
				SELECT ISNULL(LTRIM(RTRIM(LEFT(@instr, @position - 1))), ''),'','','',''

				SET @whichfield = 'Col2'
			END
			
			SET @instr = RIGHT(@instr, LEN(@instr) - @position)
			SET @position = CHARINDEX(@delimiter, @instr, 1)
		END
	END	
	ELSE 
	BEGIN
	
		INSERT INTO @TableOut (Col1,Col2,Col3,Col4,Col5) 
		SELECT '','','','',''
	
	END
	RETURN
END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnColsFromString] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnColsFromString] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnColsFromString] TO [sp_executeall]
GO
