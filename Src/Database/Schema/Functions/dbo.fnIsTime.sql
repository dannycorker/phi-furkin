SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-20
-- Description:	Check to see if a varchar contains a valid time.
-- =============================================
CREATE FUNCTION [dbo].[fnIsTime] 
(
	@value varchar(2000)
)
RETURNS bit
AS
BEGIN

	DECLARE @IsTime bit = 0
	
	SELECT @value = ltrim(rtrim(@value))
	
	/* Whilst a blank time element is fine, NULL is not valid */
	IF @value IS NOT NULL 
	BEGIN
	
		/* No time element is fine, but if there are some elements present then check them */
		IF datalength(@value) = 0 
		BEGIN
		
			/* Blank time element is fine */
			SET @IsTime = 1
		
		END	
		ELSE
		BEGIN
			
			DECLARE @hh tinyint, @mm tinyint, @ss tinyint, @ff int, @hhValid bit = 0
			
			/* If there are some elements present, then we want HH:MM at minimum */
			IF @value LIKE '[0-2][0-9]:[0-5][0-9]' OR @value LIKE '[0-2][0-9]:[0-5][0-9]:[0-5][0-9]' OR @value LIKE '[0-2][0-9]:[0-5][0-9]:[0-5][0-9].[0-9][0-9][0-9]' OR @value LIKE '[0-2][0-9]:[0-5][0-9]:[0-5][0-9].[0-9][0-9][0-9][0-9][0-9][0-9]'
			BEGIN
				
				SELECT @hh = convert(tinyint, substring(@value, 1, 2))
			
				IF (@hh BETWEEN 0 AND 24)
				BEGIN
					SET @IsTime = 1
				END
				
			END
			
		END
		
	END
	
	RETURN @IsTime
			
END











GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsTime] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnIsTime] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsTime] TO [sp_executeall]
GO
