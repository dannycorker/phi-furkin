SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2008-10-15
-- Description:	Return the smaller of 2 numbers (money)
-- JWG 2010-02-24 Handle Nulls gracefully
-- =============================================
CREATE FUNCTION [dbo].[fnMinOf] 
(
	@Number1 money,
	@Number2 money
)
RETURNS money
AS
BEGIN
	DECLARE @SmallerNumber money

	IF @Number1 IS NULL
	BEGIN
		RETURN @Number2
	END
	
	IF @Number2 IS NULL
	BEGIN
		RETURN @Number1
	END
	
	SELECT @SmallerNumber = CASE WHEN @Number1 < @Number2 THEN @Number1 ELSE @Number2 END

	RETURN @SmallerNumber

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnMinOf] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnMinOf] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnMinOf] TO [sp_executeall]
GO
