SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-06-12
-- Description:	Get CustomerDV directly #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetCustomerDvCASEAsInt]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS int
AS
BEGIN

	DECLARE @ValueInt AS int

	SELECT @ValueInt = cdv.ValueInt 
	FROM Matter c WITH (NOLOCK) 
	INNER JOIN CustomerDetailValues cdv WITH (NOLOCK) ON c.CustomerID = cdv.CustomerID AND cdv.DetailFieldID = @DetailFieldID
	WHERE c.CaseID = @CaseID 
	
	RETURN @ValueInt

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerDvCASEAsInt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetCustomerDvCASEAsInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerDvCASEAsInt] TO [sp_executeall]
GO
