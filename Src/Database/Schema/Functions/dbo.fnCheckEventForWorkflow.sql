SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-01-31
-- Description:	Check if an event completes a WorkflowTask
-- =============================================
CREATE FUNCTION [dbo].[fnCheckEventForWorkflow]
(
	@LeadEventID int 
)
RETURNS tinyint
AS
BEGIN

	DECLARE @RC int,
	@WorkflowTaskID int,
	@CompletedBy int,
	@CompletedOn datetime,
	@CompletionDescription varchar(255)
	
	SELECT @WorkflowTaskID = w.WorkflowTaskID,
	@CompletedBy = CASE WHEN IsNull(le.NextEventID, 0) = 0 THEN le.WhoCreated ELSE (SELECT le_new.WhoCreated FROM LeadEvent le_new WHERE le_new.LeadEventID = le.NextEventID) END,
	@CompletedOn = CASE WHEN le.WhenFollowedUp IS NULL THEN le.WhenCreated ELSE le.WhenFollowedUp END,
	@CompletionDescription = CASE WHEN le.WhenFollowedUp IS NULL THEN 'Inserted by trigger' ELSE 'Updated by trigger' END
	FROM LeadEvent le 
	INNER JOIN WorkflowTask w ON w.LeadID = le.LeadID AND w.CaseID = le.CaseID AND le.EventTypeID = le.EventTypeID AND w.Followup = CASE WHEN le.WhenFollowedUp IS NULL THEN 0 ELSE 1 END
	WHERE le.LeadEventID = @LeadEventID 
	AND le.EventDeleted = 0
	
	IF @@ROWCOUNT > 0 AND @WorkflowTaskID > 0
	BEGIN
		EXECUTE @RC = WorkflowTask_SetComplete @WorkflowTaskID, @CompletedBy, @CompletedOn, @CompletionDescription 
	END
	ELSE
	BEGIN
		EXECUTE @RC = WorkflowTask_SetComplete 2, 3, '2008-02-04 00:00:01', 'JG Test2'  
		--SELECT @RC = 0
	END

	RETURN @RC

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnCheckEventForWorkflow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnCheckEventForWorkflow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnCheckEventForWorkflow] TO [sp_executeall]
GO
