SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-02-14
-- Description:	Get Customer DetailValue
-- =============================================
CREATE FUNCTION [dbo].[fnGetCustomerDvAsDate]
(
	@DetailFieldID		int,
	@CustomerID			int
)
RETURNS date
AS
BEGIN

	DECLARE @DetailValue as date

	Select @DetailValue = cdv.ValueDate 
	From CustomerDetailValues cdv with (nolock) 
	Where cdv.CustomerID = @CustomerID 
	and cdv.DetailFieldID = @DetailFieldID
	
	RETURN @DetailValue

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerDvAsDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetCustomerDvAsDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerDvAsDate] TO [sp_executeall]
GO
