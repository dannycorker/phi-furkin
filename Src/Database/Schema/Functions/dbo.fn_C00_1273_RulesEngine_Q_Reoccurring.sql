SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2019-02-18
-- Description:	Rules engine function to find the latest quarter (pre-renewal) for a claim paid for the active policy year by ConditionType.  Used when pre-calculating renewals.
-- GPR 2019-02-18 Created for C600 ZD #55310
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_RulesEngine_Q_Reoccurring]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS VARCHAR(2)
AS
BEGIN
 
	DECLARE @ReturnValue VARCHAR(2),
	@ClientID INT = dbo.fnGetPrimaryClientID()
	,@RenewalDate DATE = dbo.fnGetSimpleDvAsDate(175307, @MatterID)
	,@ClaimsDate DATE

	SELECT TOP(1) @ClaimsDate = mdvDateSettled.ValueDate
	FROM Lead l with (NOLOCK) 
	INNER JOIN LeadTypeRelationship ltr with (NOLOCK) on ltr.FromLeadID = l.LeadID and ltr.ToLeadTypeID = 1490 
	INNER JOIN Matter m with (NOLOCK) on m.MatterID = ltr.ToMatterID 
	INNER JOIN dbo.TableRows r WITH (NOLOCK) on r.MatterID = m.MatterID and r.DetailFieldID = 144355 
	INNER JOIN dbo.TableDetailValues tdvTotal WITH (NOLOCK) ON r.TableRowID = tdvTotal.TableRowID AND tdvTotal.DetailFieldID = 144352
	INNER JOIN dbo.MatterDetailValues mdvDateSettled WITH (NOLOCK) ON m.MatterID = mdvDateSettled.MatterID AND mdvDateSettled.DetailFieldID = 162644
	INNER JOIN dbo.MatterDetailValues ailment with (NOLOCK) on ailment.MatterID = m.MatterID and ailment.DetailFieldID = 144504 
	INNER JOIN dbo.ResourceListDetailValues rec with (NOLOCK) on rec.ResourceListID = ailment.ValueInt and rec.DetailFieldID = 180185 
	WHERE r.DetailFieldPageID = 16157
	AND r.ClientID = @ClientID
	AND mdvDateSettled.ValueDate >=  DATEADD(Month,-12,@RenewalDate) 
	AND mdvDateSettled.ValueDate < CAST(@RenewalDate AS DATE)
	AND l.LeadID = @LeadID 
	AND rec.ValueInt = 76511
	ORDER BY mdvDateSettled.ValueDate DESC
	
	IF @ClaimsDate >=  DATEADD(Month,-3,@RenewalDate) AND @ClaimsDate < CAST(@RenewalDate AS DATE)
	BEGIN
		SELECT @ReturnValue = 'Q1'
	END
	
	IF @ClaimsDate >=  DATEADD(Month,-6,@RenewalDate) AND @ClaimsDate < DATEADD(Month,-3,@RenewalDate)
	BEGIN
		SELECT @ReturnValue = 'Q2'
	END
	
	IF @ClaimsDate >=  DATEADD(Month,-9,@RenewalDate) AND @ClaimsDate < DATEADD(Month,-6,@RenewalDate)
	BEGIN
		SELECT @ReturnValue = 'Q3'
	END
	
	IF @ClaimsDate >=  DATEADD(Month,-12,@RenewalDate) AND @ClaimsDate < DATEADD(Month,-9,@RenewalDate)
	BEGIN
		SELECT @ReturnValue = 'Q4'
	END
	
	RETURN @ReturnValue
 
END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_Q_Reoccurring] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_RulesEngine_Q_Reoccurring] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_Q_Reoccurring] TO [sp_executeall]
GO
