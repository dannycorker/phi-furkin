SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 04-07-2016
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fnGetSimpleDvByThirdPartyField]
(
	@ClientID INT,
	@CustomerID INT, 
	@LeadID INT, 
	@CaseID INT, 
	@MatterID INT, 	
	@LeadTypeID INT,
	@ThirdPartyFieldID INT
)
RETURNS VARCHAR(2000)
AS
BEGIN
	
	
	DECLARE @FieldID INT, @DetailValue VARCHAR(2000), @DetailFieldSubtypeID INT 
	
	SELECT @FieldID = tpfm.DetailFieldID, @DetailFieldSubtypeID = df.LeadOrMatter FROM ThirdPartyFieldMapping tpfm WITH (NOLOCK) 
	INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = tpfm.DetailFieldID
	WHERE tpfm.ClientID = @ClientID AND tpfm.ThirdPartyFieldID = @ThirdPartyFieldID AND tpfm.LeadTypeID = @LeadTypeID

	SELECT @DetailValue = dbo.fnGetSimpleDvByIdentity(@CustomerID, @LeadID, @CaseID, @MatterID, @DetailFieldSubtypeID, @FieldID)
	
	RETURN @DetailValue

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleDvByThirdPartyField] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetSimpleDvByThirdPartyField] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleDvByThirdPartyField] TO [sp_executeall]
GO
