SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2009-07-13
-- Description:	Return a table of varchars and matterid's
-- =============================================
CREATE FUNCTION [dbo].[fnTableOfValuesFromCSVPSV]
(
	@instr varchar(max),
	@delimiter char(1) = ',',
	@delimiter2 char(1) = '|'
)
RETURNS 
	@TableOut TABLE 
	(
		AnyValue varchar(max),
		AnyValue2 varchar(max)
	)
AS
BEGIN

	Declare @TableOut2 TABLE 
	(
		MyInt int identity,
		AnyValue varchar(max)
	)

	DECLARE @testvalue varchar(max),
	@position  int,
	@MyInt int,
	@TestInt int

	SET @instr = @instr + @delimiter
	SET @position = CHARINDEX(@delimiter, @instr, 1)

	IF REPLACE(@instr, @delimiter, '') <> ''
	BEGIN
		WHILE @position > 0
		BEGIN
			INSERT INTO @TableOut2 (AnyValue) 
			SELECT LEFT(@instr, @position - 1)

			SET @instr = RIGHT(@instr, datalength(@instr) - @position)
			SET @position = CHARINDEX(@delimiter, @instr, 1)
		END
	END	

	Select top 1 @instr = AnyValue, @MyInt = MyInt, @TestInt = 0
	From @TableOut2
	Order by MyInt

	SET @position = CHARINDEX(@delimiter2, @instr, 1)

	IF REPLACE(@instr, @delimiter2, '') <> ''
	BEGIN
		WHILE @position > 0 and @TestInt <> @MyInt
		BEGIN
			INSERT INTO @TableOut (AnyValue, AnyValue2) 
			SELECT LEFT(@instr, @position-1), RIGHT(@instr, datalength(@instr) - @position)

			SET @instr = LEFT(RIGHT(@instr, datalength(@instr) - @position), @position - 1)
		
			Set @TestInt = @MyInt

			Select top 1 @instr = AnyValue, @MyInt = MyInt
			From @TableOut2
			Where MyInt > @MyInt
			Order by MyInt

			SET @position = CHARINDEX(@delimiter2, @instr, 1)
			
		END
	END	
	RETURN
END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfValuesFromCSVPSV] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnTableOfValuesFromCSVPSV] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnTableOfValuesFromCSVPSV] TO [sp_executeall]
GO
