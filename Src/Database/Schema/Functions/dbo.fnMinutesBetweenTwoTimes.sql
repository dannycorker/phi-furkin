SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Jim Green
-- Create date: 2008-08-22
-- Description:	Calculate minutes between two times in format 'hh:mm'
-- =============================================
CREATE FUNCTION [dbo].[fnMinutesBetweenTwoTimes] 
(
	-- Add the parameters for the function here
	@starttime varchar(5), @endtime varchar(5)
)
RETURNS smallint
AS
BEGIN

	DECLARE @totalmins smallint

	-- Cater for single-digit hours, eg "9:00" instead of "09:00"
	-- Also caters for empty strings
	IF LEN(@starttime) BETWEEN 1 AND 2
	BEGIN
		SELECT @starttime = right('00000' + @starttime + ':00', 5)
	END
	ELSE	
	BEGIN
		SELECT @starttime = right('00000' + @starttime, 5)
	END
	IF LEN(@endtime) BETWEEN 1 AND 2
	BEGIN
		SELECT @endtime = right('00000' + @endtime + ':00', 5)
	END
	ELSE	
	BEGIN
		SELECT @endtime = right('00000' + @endtime, 5)
	END

	-- CONVERT end time '15:30' to minutes (15 * 60) + 30
	-- Ditto for start time '09:15' (9 * 60) + 15
	-- Return the difference
	DECLARE @starthh char(2), @startmm char(2), @endhh char(2), @endmm char(2)

	SELECT @starthh = substring(@starttime,1,2), 
		   @startmm = substring(@starttime,4,2), 
		   @endhh = substring(@endtime,1,2), 
		   @endmm = substring(@endtime,4,2)

	IF (substring(@starthh,1,1) BETWEEN '0' AND '2') AND
	   (substring(@starthh,2,1) BETWEEN '0' AND '9') AND
	   (substring(@startmm,1,1) BETWEEN '0' AND '5') AND
	   (substring(@startmm,2,1) BETWEEN '0' AND '9') AND
	   (substring(@endhh,1,1) BETWEEN '0' AND '2') AND
	   (substring(@endhh,2,1) BETWEEN '0' AND '9') AND
	   (substring(@endmm,1,1) BETWEEN '0' AND '5') AND
	   (substring(@endmm,2,1) BETWEEN '0' AND '9')
	BEGIN
		SELECT @totalmins = ((@endhh * 60) + @endmm) - ((@starthh * 60) + @startmm)
	END
	ELSE
	BEGIN
		SELECT @totalmins = 0
	END

	-- IF we go round the clock, add 24 hours to cater for night shifts like '23:00'to'07:00'
	IF @totalmins < 0
	BEGIN
		select @totalmins = @totalmins + (1440)
	END

	RETURN @totalmins

END










GO
GRANT VIEW DEFINITION ON  [dbo].[fnMinutesBetweenTwoTimes] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnMinutesBetweenTwoTimes] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnMinutesBetweenTwoTimes] TO [sp_executeall]
GO
