SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Christian Williams
-- Create date: 2018-03-20
-- Description:	Returns number with ordinal indicator for date
-- =============================================
CREATE FUNCTION [dbo].[_C600_OrdinalIndicator] 
(
	-- Add the parameters for the function here
	@Day VarChar(MAX)
)
RETURNS VarChar(Max)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result VarChar(Max)

	-- Add the T-SQL statements to compute the return value here
SELECT @Result = @Day + Case When @Day in ('11','12','13') THEN 'th'
                                                           ELSE CASE RIGHT(@Day,1) WHEN '1' THEN 'st'
	                                                                               WHEN '2' THEN 'nd'
	                                                                               WHEN '3' THEN 'rd'
	                                                                                        ELSE 'th' END END

	-- Return the result of the function
	RETURN @Result

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_OrdinalIndicator] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C600_OrdinalIndicator] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C600_OrdinalIndicator] TO [sp_executeall]
GO
