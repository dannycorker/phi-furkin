SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE FUNCTION [dbo].[RegexGroup] (@input [nvarchar] (max), @pattern [nvarchar] (4000), @name [nvarchar] (4000))
RETURNS [nvarchar] (max)
WITH EXECUTE AS CALLER
EXTERNAL NAME [SqlServerProject_Regex].[UserDefinedFunctions].[RegexGroup]
GO
GRANT VIEW DEFINITION ON  [dbo].[RegexGroup] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[RegexGroup] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RegexGroup] TO [sp_executeall]
GO
