SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-06-20
-- Description:	Sum Matter Values by Customer
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_SumTableColumnByLead]
(
	@LeadID int,
	@DetailFieldID int
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE @ReturnValue VARCHAR(2000),
			@Money numeric(18,2) = 0.00
	
	SELECT @Money = ISNULL(SUM(tdv.ValueMoney), 0.00)
	FROM TableDetailValues tdv WITH (NOLOCK)
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = tdv.MatterID and m.LeadID = @LeadID
	WHERE tdv.DetailFieldID = @DetailFieldID 

	SELECT @ReturnValue = @Money

	RETURN @ReturnValue

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumTableColumnByLead] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_SumTableColumnByLead] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumTableColumnByLead] TO [sp_executeall]
GO
