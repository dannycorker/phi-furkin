SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-07-26
-- =============================================
CREATE FUNCTION [dbo].[fn_C183_RulesEngine_Outputs_Parse]( 
    @ClientID INT,
	@UserID INT,
	@ParentID INT,
	@ImportXml XML
) 
RETURNS TABLE 
AS RETURN 
(
	
	WITH DataSet AS
	(
		SELECT	ISNULL(T.c.value('col0[1]','VARCHAR(250)'),'') AS Transform,
				ISNULL(T.c.value('col1[1]','VARCHAR(250)'),'') AS Value,
				ISNULL(T.c.value('col2[1]','VARCHAR(250)'),'') AS ParameterOption,
				ISNULL(T.c.value('col3[1]','VARCHAR(250)'),'') AS Coordinates,
				ROW_NUMBER() OVER(ORDER BY T.c) RowID
		FROM	@ImportXML.nodes('/table/row') T(c)
	)
	SELECT	
			row.*,
			CASE
				WHEN NOT EXISTS (
					SELECT * 
					FROM	RulesEngine_ParameterOptions po WITH (NOLOCK)
					INNER JOIN RulesEngine_RuleParameters rp WITH (NOLOCK) ON rp.RuleParameterID = po.RuleParameterID
					WHERE '|'+Coordinates+'|' LIKE '%|'+CAST(po.ParameterOptionID AS VARCHAR)+'|%'
					AND		rp.RuleID = @ParentID
					) THEN 'parameter/option missmatch'
				WHEN rt.Name IS NULL THEN 'transform not found'
				ELSE ''
			END Error
	FROM	DataSet row
	INNER JOIN RulesEngine_Rules r WITH (NOLOCK) ON r.RuleID = @ParentID AND r.ClientID = @ClientID
	LEFT JOIN dbo.RulesEngine_Transforms rt WITH (NOLOCK) ON rt.Name = row.Transform
)
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C183_RulesEngine_Outputs_Parse] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C183_RulesEngine_Outputs_Parse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C183_RulesEngine_Outputs_Parse] TO [sp_executeall]
GO
