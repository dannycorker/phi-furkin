SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-20
-- Description:	Check to see if a varchar contains a valid money value
-- JWG 2009-12-12 Remove commas and £ sign before testing, so that we also 
--                accept values like £1,234.56 as money instead of ignoring them.
-- =============================================
CREATE FUNCTION [dbo].[fnIsMoney] 
(
	@value varchar(2000)
)
RETURNS bit
AS
BEGIN
	
	SELECT @value = ltrim(rtrim(@value))

	SELECT @value = replace(replace(@value, '£', ''), ',', '')
	
	RETURN CASE
			WHEN @value IS NULL THEN 0
			WHEN @value = '' THEN 1
			WHEN datalength(@value) > 14 THEN 0
			WHEN isnumeric(@value) = 0 THEN 0
			WHEN PatIndex('%[^-£$.0-9]%', @value) > 0 THEN 0
			ELSE 1
			END
			
END

GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsMoney] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnIsMoney] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsMoney] TO [sp_executeall]
GO
