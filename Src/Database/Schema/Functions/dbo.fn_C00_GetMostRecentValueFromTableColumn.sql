SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-04-15
-- Description:	Return value from the latest row in the defined table column
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_GetMostRecentValueFromTableColumn]
(
	@TableDetailFieldID		INT=177089,
	@ColumnDetailFieldID	INT=177087,
	@ObjectID				INT=2306221
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE @DetailFieldSubTypeID	INT,
			@ReturnValue			VARCHAR(MAX)='',
			@ClientID				INT,
			@DateFormat				INT
	
	SELECT	@DetailFieldSubTypeID = df.LeadOrMatter,
			@ClientID = df.ClientID
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @TableDetailFieldID
	
	SELECT @DateFormat = CASE WHEN @ClientID in (4,241) THEN 103 ELSE 23 END /* LB #28149 */

		SELECT TOP 1 @ReturnValue=case df.QuestionTypeID 
									WHEN 5 THEN CONVERT(VARCHAR(10),tdv.ValueDate,@DateFormat)
									WHEN 4 THEN ISNULL(lli.ItemValue,'')
									ELSE tdv.DetailValue END
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID=tdv.DetailFieldID
		LEFT JOIN LookupListItems lli WITH (NOLOCK) ON lli.LookupListItemID=ISNULL(tdv.ValueInt,0)
		WHERE tr.DetailFieldID = @TableDetailFieldID
		and tdv.DetailFieldID  = @ColumnDetailFieldID
		and (
			@DetailFieldSubTypeID = 1  AND tr.LeadID			= @ObjectID
		OR	@DetailFieldSubTypeID = 2  AND tr.MatterID			= @ObjectID
		OR	@DetailFieldSubTypeID = 11 AND tr.CaseID			= @ObjectID
		OR	@DetailFieldSubTypeID = 10 AND tr.CustomerID		= @ObjectID
		OR	@DetailFieldSubTypeID = 13 AND tr.ClientPersonnelID = @ObjectID
		OR	@DetailFieldSubTypeID = 12 AND tr.CLientID			= @ObjectID
			)
		ORDER BY tdv.TableRowID DESC
	
	RETURN @ReturnValue
END





GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetMostRecentValueFromTableColumn] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_GetMostRecentValueFromTableColumn] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetMostRecentValueFromTableColumn] TO [sp_executeall]
GO
