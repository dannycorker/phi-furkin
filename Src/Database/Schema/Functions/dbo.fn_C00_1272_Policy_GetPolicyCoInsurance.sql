SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-08-27
-- Description:	Returns a table of policy co-insurance applicable
-- 2021-04-14 ACE Added options to table SDFURPHI-14
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_Policy_GetPolicyCoInsurance]
(
	@LeadID INT,
	@CurrentPolicyMatterID INT
)
RETURNS 
	@CoInsData TABLE 
	(
		ResourceListID INT,
		Section VARCHAR(2000),
		SubSection VARCHAR(2000),
		ExcessPercentage MONEY,
		DateCutOff DATE,
		CoInsLinkedTo INT,
		MinThreshold MONEY,
		Breed VARCHAR(2000),
		Options VARCHAR(2000)
	)
AS
BEGIN

	DECLARE @PolicyLeadID INT
	SELECT @PolicyLeadID = dbo.fn_C00_1272_GetPolicyLeadFromClaimLead(@LeadID)

	DECLARE @CustomerID INT
	SELECT @CustomerID = CustomerID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @LeadID
	
	-- Select the animal breed to see if there is a breed premium
	DECLARE @AnimalBreed VARCHAR(2000) 
		,@PetType INT
		,@PetDoB DATETIME
		,@SpecialProcessing INT
	
	-- Get PetType, PetDoB, and SpecialProcessingFlag			
	SELECT @AnimalBreed = rdvBreed.DetailValue, @PetType = rdvPetType.ValueInt, @PetDoB = ldvPetDOB.ValueDate, @SpecialProcessing = ldvSpecialProcessing.ValueInt
	FROM dbo.Lead l WITH (NOLOCK)
	LEFT JOIN dbo.LeadDetailValues ldvPetType WITH (NOLOCK) ON ldvPetType.LeadID = l.LeadID AND ldvPetType.DetailFieldID = 144272
	LEFT JOIN dbo.ResourceListDetailValues rdvPetType WITH (NOLOCK) ON ldvPetType.ValueInt = rdvPetType.ResourceListID AND rdvPetType.DetailFieldID = 144269
	LEFT JOIN dbo.LeadDetailValues ldvPetDOB WITH (NOLOCK) ON ldvPetDOB.LeadID = l.LeadID AND ldvPetDOB.DetailFieldID = 144274
	LEFT JOIN dbo.LeadDetailValues ldvSpecialProcessing WITH (NOLOCK) ON ldvSpecialProcessing.LeadID = l.LeadID AND ldvSpecialProcessing.DetailFieldID = 162642
	LEFT JOIN dbo.LeadDetailValues ldvBreed WITH (NOLOCK) ON l.LeadID = ldvBreed.LeadID AND ldvBreed.DetailFieldID = 144272
	LEFT JOIN dbo.ResourceListDetailValues rdvBreed WITH (NOLOCK) ON ldvBreed.ValueInt = rdvBreed.ResourceListID AND rdvBreed.DetailFieldID = 144270
	WHERE l.LeadID = @PolicyLeadID
	
	
	INSERT @CoInsData
	SELECT *
	FROM dbo.fn_C00_1272_Policy_GetPolicyCoInsuranceWithData(@CurrentPolicyMatterID, @PetDoB, @PetType, @SpecialProcessing, @AnimalBreed)
	
	RETURN

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPolicyCoInsurance] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1272_Policy_GetPolicyCoInsurance] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPolicyCoInsurance] TO [sp_executeall]
GO
