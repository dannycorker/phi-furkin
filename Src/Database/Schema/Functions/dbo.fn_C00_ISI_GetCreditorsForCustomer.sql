SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2015-03-20
-- Description:	For ISI Clients, return all creditors for a customer
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ISI_GetCreditorsForCustomer] 
(	
		@CustomerID INT
)
RETURNS
@TableOut TABLE 
(
	 CustomerID				INT
	,TableRowID				INT 
	,CreditorType			VARCHAR(50)
	,CreditorName			VARCHAR(2000)
	,IsSecure				BIT
	,Applicant				VARCHAR(9)
	,Balance				DECIMAL(18,2)
	,IsPreferential			BIT
	,ProofOfDebt			BIT
	,DeemedAccepted			BIT
	,MoveToVoting			BIT
	,ExcludeFromRepayments	BIT
	,CreditorReference		VARCHAR(2000)
	,OverrideAmount			DECIMAL(18,2)
	,OptedIn				VARCHAR(2000)
	,Vote					INT
	,Vote2					INT
	,Vote3					INT
	,PlanType				VARCHAR(2000)
	,CreditorTypeActual		VARCHAR (2000)
) 
AS
BEGIN

	/*
	This system changes frequently and unpredictably.
	This function will be used, ideally, in all instances where we need to refer to the creditors on a case
	The SQL below is not the most efficient, but is designed such that we can change individual components easily, without affecting others
	
	- Find the Resources And Tables LeadTypeID
	- Find the Creditors DetailFieldPageID
	- Pull up a list of all creditor TableRowIDs, along with the type of creditor that they represent
	- Certain properties apply to all creditor types, some are type-specific
	- For each type of creditor, compile a list of ThirdPartyFields that represent its relevant properties
	- For this client, find the actual DetailFieldIDs
	- Property-by-Property, populate the output table using the rules relevant to that property
	- Finally, Standardise the output so that any properties that are not applicable to a creditor take the equivalent "negative" response
		e.g. if IsSecure doesn't apply to this creditor, treat it as NOT Secured
		e.g. if CreditorReference doesn't apply, treat it as blank
	*/
		
	DECLARE	 @CreditorDetailFieldPageID INT
			,@ClientID					INT
			,@LeadTypeID				INT

	SELECT @ClientID = cu.ClientID
	FROM Customers cu WITH ( NOLOCK ) 
	WHERE cu.CustomerID = @CustomerID 
	
	SELECT @LeadTypeID = lt.LeadTypeID
	FROM LeadType lt WITH ( NOLOCK ) 
	WHERE lt.LeadTypeName = 'Resources and Tables'
	AND lt.ClientID = @ClientID

	SELECT TOP(1) @CreditorDetailFieldPageID = dp.DetailFieldPageID
	FROM DetailFieldPages dp WITH ( NOLOCK ) 
	WHERE dp.ClientID = @ClientID 
	AND dp.PageName = 'Creditor Section'
	ORDER BY dp.[Enabled] DESC

	DECLARE @FieldMapping TABLE (	 CreditorType		VARCHAR(50)
									,Property			VARCHAR(50)
									,ThirdPartyFieldID	INT
									,DetailFieldID		INT
								)
						

	INSERT @FieldMapping ( CreditorType, Property, ThirdPartyFieldID )
	VALUES	 ( 'Principal Private Residence Lender'		,	'OrganisationName'	,		4231	) -- Added by jl as this shared an ID with Excludable debts other than revenue
			,( 'Principal Private Residence Lender'		,	'LastName'			,		3566	)
			,( 'Principal Private Residence Lender'		,	'FirstName'			,		3567	)
			,( 'Financial Institutions'					,	'OrganisationName'	,		3577	)
			,( 'Credit Union'							,	'OrganisationName'	,		3577	)
			,( 'Excludable Debts - Revenue'				,	'OrganisationName'	,		3613	)
			,( 'Excludable Debts - Other than revenue'	,	'OrganisationName'	,		3642	)
			--,( 'Excluded Debts'							,	'OrganisationName'	,		3669	)
			,( 'Employees'								,	'LastName'			,		3687	)
			,( 'Employees'								,	'FirstName'			,		3688	)
			,( 'Equipment Lessors/Hire Purchase'		,	'OrganisationName'	,		3714	)
			,( 'Equipment Lessors/Hire Purchase'		,	'LastName'			,		3716	)
			,( 'Equipment Lessors/Hire Purchase'		,	'FirstName'			,		3717	)
			,( 'Trade Creditors'						,	'OrganisationName'	,		3743	)
			,( 'Trade Creditors'						,	'LastName'			,		3745	)
			,( 'Trade Creditors'						,	'FirstName'			,		3746	)
			,( 'Connected Creditors'					,	'OrganisationName'	,		3770	)
			,( 'Connected Creditors'					,	'LastName'			,		3772	)
			,( 'Connected Creditors'					,	'FirstName'			,		3773	)
			,( 'Other Debts'							,	'OrganisationName'	,		3800	)
			,( 'Other Debts'							,	'LastName'			,		3802	)
			,( 'Other Debts'							,	'FirstName'			,		3803	)
			,( 'Contingent Debts'						,	'OrganisationName'	,		3828	)
			,( 'Contingent Debts'						,	'LastName'			,		3830	)
			,( 'Contingent Debts'						,	'FirstName'			,		3831	)
			,( 'Prospective Debts'						,	'OrganisationName'	,		3828	)
			,( 'Prospective Debts'						,	'LastName'			,		3830	)
			,( 'Prospective Debts'						,	'FirstName'			,		3831	)
			,( 'Accelerated Case Funds Available'		,	'OrganisationName'	,		4140	)
			
			,( 'Principal Private Residence Lender'		,	'IsSecure'	,		3559	)
			,( 'Financial Institutions'					,	'IsSecure'	,		3602	)
			,( 'Credit Union'							,	'IsSecure'	,		3602	)
			,( 'Excludable Debts - Revenue'				,	'IsSecure'	,		3630	)
			,( 'Excludable Debts - Other than revenue'	,	'IsSecure'	,		3657	)
			--,( 'Excluded Debts'							,	'IsSecure'	,		0000	)
			,( 'Employees'								,	'IsSecure'	,		3701	)
			,( 'Equipment Lessors/Hire Purchase'		,	'IsSecure'	,		3730	)
			,( 'Trade Creditors'						,	'IsSecure'	,		3757	)
			,( 'Connected Creditors'					,	'IsSecure'	,		3787	)
			,( 'Other Debts'							,	'IsSecure'	,		3815	)
			,( 'Contingent Debts'						,	'IsSecure'	,		3843	)
			,( 'Prospective Debts'						,	'IsSecure'	,		3843	)
			,( 'Accelerated Case Funds Available'		,	'IsSecure'	,		4142	)

			,( 'Financial Institutions'					,	'Applicant'	,		3901	)
			,( 'Credit Union'							,	'Applicant'	,		3901	)
			,( 'Excludable Debts - Revenue'				,	'Applicant'	,		3902	)
			,( 'Excludable Debts - Other than revenue'	,	'Applicant'	,		3903	)
			--,( 'Excluded Debts'							,	'Applicant'	,		3904	)
			,( 'Employees'								,	'Applicant'	,		3905	)
			,( 'Equipment Lessors/Hire Purchase'		,	'Applicant'	,		3906	)
			,( 'Trade Creditors'						,	'Applicant'	,		3907	)
			,( 'Connected Creditors'					,	'Applicant'	,		3908	)
			,( 'Other Debts'							,	'Applicant'	,		3909	)
			,( 'Contingent Debts'						,	'Applicant'	,		3910	)
			,( 'Prospective Debts'						,	'Applicant'	,		3910	)
			
			,( 'Principal Private Residence Lender'		,	'Balance'	,		3553	)
			,( 'Financial Institutions'					,	'Balance'	,		3597	)
			,( 'Credit Union'							,	'Balance'	,		3597	)
			,( 'Excludable Debts - Revenue'				,	'Balance'	,		3626	)
			,( 'Excludable Debts - Other than revenue'	,	'Balance'	,		3655	)
			--,( 'Excluded Debts'							,	'Balance'	,		3682	)
			,( 'Employees'								,	'Balance'	,		3699	)
			,( 'Equipment Lessors/Hire Purchase'		,	'Balance'	,		3729	)
			,( 'Trade Creditors'						,	'Balance'	,		3756	)
			,( 'Connected Creditors'					,	'Balance'	,		3785	)
			,( 'Other Debts'							,	'Balance'	,		3814	)
			,( 'Contingent Debts'						,	'Balance'	,		3841	)
			,( 'Prospective Debts'						,	'Balance'	,		3841	)
			,( 'Accelerated Case Funds Available'		,	'Balance'	,		4141	)

			,( 'Excludable Debts - Revenue'				,	'IsPreferential'	,		3627	)
			,( 'Excludable Debts - Other than revenue'	,	'IsPreferential'	,		3656	)

			,( 'Excludable Debts - Other than revenue'  , 'ProofOfDebt' ,  3662 )
			,( 'Employees'								, 'ProofOfDebt' ,  3707 )
			,( 'Trade Creditors'						, 'ProofOfDebt' ,  3763 )
			,( 'Equipment Lessors/Hire Purchase'		, 'ProofOfDebt' ,  3736 )
			,( 'Excludable Debts - Revenue'				, 'ProofOfDebt' ,  3635 )
			,( 'Other Debts'							, 'ProofOfDebt' ,  3821 )
			,( 'Contingent Debts'						, 'ProofOfDebt' ,  3849 )
			,( 'Prospective Debts'						, 'ProofOfDebt' ,  3849 )
			,( 'Connected Creditors'					, 'ProofOfDebt' ,  3793 )
			,( 'Financial Institutions'					, 'ProofOfDebt' ,  3606 )
			,( 'Credit Union'							, 'ProofOfDebt' ,  3606 )
			,( 'Principal Private Residence Lender'		, 'ProofOfDebt' ,  3570 )

			,( 'Excludable Debts - Other than revenue'  , 'DeemedAccepted' ,  3663 )
			,( 'Employees'								, 'DeemedAccepted' ,  3708 )
			,( 'Trade Creditors'						, 'DeemedAccepted' ,  3764 )
			,( 'Equipment Lessors/Hire Purchase'		, 'DeemedAccepted' ,  3737 )
			,( 'Excludable Debts - Revenue'				, 'DeemedAccepted' ,  3636 )
			,( 'Other Debts'							, 'DeemedAccepted' ,  3822 )
			,( 'Contingent Debts'						, 'DeemedAccepted' ,  3850 )
			,( 'Prospective Debts'						, 'DeemedAccepted' ,  3850 )
			,( 'Connected Creditors'					, 'DeemedAccepted' ,  3794 )
			,( 'Financial Institutions'					, 'DeemedAccepted' ,  3607 )
			,( 'Credit Union'							, 'DeemedAccepted' ,  3607 )
			,( 'Principal Private Residence Lender'		, 'DeemedAccepted' ,  3571 )

			,( 'Excludable Debts - Other than revenue'  , 'MoveToVoting' ,  3664 )
			,( 'Employees'								, 'MoveToVoting' ,  3709 )
			,( 'Trade Creditors'						, 'MoveToVoting' ,  3765 )
			,( 'Equipment Lessors/Hire Purchase'		, 'MoveToVoting' ,  3738 )
			,( 'Excludable Debts - Revenue'				, 'MoveToVoting' ,  3637 )
			--,( 'Excluded Debts'							, 'MoveToVoting' ,  4217 ) /* added by JL as was missing, should now include this in Voteing Rights function*/
			,( 'Other Debts'							, 'MoveToVoting' ,  3823 )
			,( 'Contingent Debts'						, 'MoveToVoting' ,  3851 )
			,( 'Prospective Debts'						, 'MoveToVoting' ,  3851 )
			,( 'Connected Creditors'					, 'MoveToVoting' ,  3795 )
			,( 'Financial Institutions'					, 'MoveToVoting' ,  3608 )
			,( 'Credit Union'							, 'MoveToVoting' ,  3608 )
			,( 'Principal Private Residence Lender'		, 'MoveToVoting' ,  3572 )

			,( 'Connected Creditors'					, 'Vote' ,  4227 )		
			,( 'Contingent Debts'						, 'Vote' ,  4228 )	
			,( 'Credit Union'							, 'Vote' ,  4219 )
			,( 'Employees'								, 'Vote' ,  4225 )
			,( 'Equipment Lessors/Hire Purchase'		, 'Vote' ,  4224 )		
			,( 'Excludable Debts - Other than revenue'  , 'Vote' ,  4226 )	
			,( 'Excludable Debts - Revenue'				, 'Vote' ,  4223 )	
			--,( 'Excluded Debts'							, 'Vote' ,  4222 )	
			,( 'Financial Institutions'					, 'Vote' ,  4219 )	
			,( 'Other Debts'							, 'Vote' ,  4230 )	
			,( 'Principal Private Residence Lender'		, 'Vote' ,  4000 )	
			,( 'Prospective Debts'						, 'Vote' ,  4228 )	
			,( 'Trade Creditors'						, 'Vote' ,  4229 )		
			
			,( 'Connected Creditors'					, 'Vote2' ,  4241 )		
			,( 'Contingent Debts'						, 'Vote2' ,  4243 )	
			,( 'Credit Union'							, 'Vote2' ,  4234 )	
			,( 'Employees'								, 'Vote2' ,  4238 )
			,( 'Equipment Lessors/Hire Purchase'		, 'Vote2' ,  4239 )		
			,( 'Excludable Debts - Other than revenue'  , 'Vote2' ,  4236 )	
			,( 'Excludable Debts - Revenue'				, 'Vote2' ,  4223 )	
			--,( 'Excluded Debts'							, 'Vote2' ,  4235 )	
			,( 'Financial Institutions'					, 'Vote2' ,  4233 )	
			,( 'Other Debts'							, 'Vote2' ,  4242 )	
			,( 'Principal Private Residence Lender'		, 'Vote2' ,  4232 )		
			,( 'Trade Creditors'						, 'Vote2' ,  4240 )	
			

			,( 'Excludable Debts - Other than revenue'  , 'ExcludeFromRepayments' ,  3665 )
			,( 'Employees'								, 'ExcludeFromRepayments' ,  3710 )
			,( 'Trade Creditors'						, 'ExcludeFromRepayments' ,  3766 )
			,( 'Equipment Lessors/Hire Purchase'		, 'ExcludeFromRepayments' ,  3739 )
			,( 'Excludable Debts - Revenue'				, 'ExcludeFromRepayments' ,  3638 )
			,( 'Other Debts'							, 'ExcludeFromRepayments' ,  3824 )
			,( 'Contingent Debts'						, 'ExcludeFromRepayments' ,  3852 )
			,( 'Prospective Debts'						, 'ExcludeFromRepayments' ,  3852 )
			,( 'Connected Creditors'					, 'ExcludeFromRepayments' ,  3796 )
			,( 'Financial Institutions'					, 'ExcludeFromRepayments' ,  3609 )
			,( 'Credit Union'							, 'ExcludeFromRepayments' ,  3609 )
			,( 'Principal Private Residence Lender'		, 'ExcludeFromRepayments' ,  3573 )

			,( 'Principal Private Residence Lender'		, 'CreditorReference' ,  3545 )
			,( 'Financial Institutions'					, 'CreditorReference' ,  3587 )
			,( 'Credit Union'							, 'CreditorReference' ,  3587 )
			,( 'Excludable Debts - Revenue'				, 'CreditorReference' ,  3990 )
			,( 'Excludable Debts - Other than revenue'  , 'CreditorReference' ,  3988 )
			--,( 'Excluded Debts'							, 'CreditorReference' ,  3981 )
			,( 'Employees'								, 'CreditorReference' ,  3905 )
			,( 'Equipment Lessors/Hire Purchase'		, 'CreditorReference' ,  3906 )
			
			,( 'Excludable Debts - Revenue'				, 'OptedIn'			  ,  3989  )
			,( 'Excludable Debts - Other than revenue'  , 'OptedIn'			  ,  3987 )
			
			,( 'Financial Institutions'					, 'CreditorTypeActual' , 3590)
			,( 'Credit Union'							, 'CreditorTypeActual' , 3590)


	UPDATE fm
	SET DetailFieldID = tpm.DetailFieldID
	FROM @FieldMapping fm 
	INNER JOIN ThirdPartyFieldMapping tpm WITH ( NOLOCK ) on tpm.LeadTypeID IN( @LeadTypeID, 0 )
	WHERE tpm.ThirdPartyFieldGroupID = 85
	AND tpm.ThirdPartyFieldID = fm.ThirdPartyFieldID
	AND tpm.ClientID = @ClientID


	INSERT @TableOut ( CustomerID,CreditorType, TableRowID )
	SELECT @CustomerID, df.FieldName, tr.TableRowID
	FROM DetailFields df WITH ( NOLOCK )
	INNER JOIN TableRows tr WITH ( NOLOCK ) on tr.DetailFieldID = df.DetailFieldID AND tr.CustomerID = @CustomerID
	WHERE df.DetailFieldPageID = @CreditorDetailFieldPageID
	AND df.QuestionTypeID IN ( 16,19 )
	ORDER BY df.FieldOrder, tr.TableRowID
	
	INSERT @TableOut ( CustomerID, CreditorType, TableRowID, IsSecure )
	SELECT @CustomerID, 'Accelerated Case Funds Available', tr.TableRowID, 0
	FROM TableRows tr WITH ( NOLOCK ) 
	INNER JOIN @FieldMapping fm on fm.CreditorType = 'Accelerated Case Funds Available'
	INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.Property = 'IsSecure'
	AND tr.CustomerID = @CustomerID
	AND tdv.ValueInt = 1
	AND tr.DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 85, 3184) /* Breakdown of funds available */
	ORDER BY tr.TableRowID


	UPDATE t
	SET CreditorName = ISNULL(li.ItemValue,tdv.DetailValue)
	FROM @TableOut t 
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID
	LEFT JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListItemID = tdv.ValueInt
	INNER JOIN @FieldMapping fm on fm.DetailFieldID = tdv.DetailFieldID
	WHERE fm.Property = 'OrganisationName'
	AND fm.CreditorType = t.CreditorType
	AND tdv.DetailValue > ''

	UPDATE t
	SET CreditorName = ISNULL(tdv1.DetailValue,'') + ' ' + ISNULL(tdv2.DetailValue,'')
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm1 on fm1.CreditorType = t.CreditorType
	INNER JOIN @FieldMapping fm2 on fm2.CreditorType = t.CreditorType
	LEFT JOIN TableDetailValues tdv1 WITH ( NOLOCK ) on tdv1.TableRowID = t.TableRowID AND tdv1.DetailFieldID = fm1.DetailFieldID AND tdv1.DetailValue > ''
	LEFT JOIN TableDetailValues tdv2 WITH ( NOLOCK ) on tdv2.TableRowID = t.TableRowID AND tdv2.DetailFieldID = fm2.DetailFieldID AND tdv2.DetailValue > ''
	WHERE fm1.Property = 'FirstName'
	AND fm2.Property = 'LastName'
	AND t.CreditorName is NULL

	UPDATE t
	SET CreditorTypeActual = li.ItemValue
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.CreditorType = t.CreditorType
	INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	INNER JOIN LookupListItems li WITH (NOLOCK) on li.LookupListItemID = tdv.ValueInt
	WHERE fm.Property = 'CreditorTypeActual'

	UPDATE t
	SET IsSecure = CASE WHEN tdv.ValueInt = 5144 THEN 1 ELSE 0 END 
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.CreditorType = t.CreditorType
	LEFT JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.Property = 'IsSecure'


	UPDATE t
	SET Applicant = li.ItemValue
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.CreditorType = t.CreditorType
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	INNER JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListItemID = tdv.ValueInt
	WHERE fm.Property = 'Applicant'


	UPDATE t
	SET Balance = tdv.ValueMoney
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.CreditorType = t.CreditorType
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.Property = 'Balance'


	UPDATE t
	SET IsPreferential = CASE WHEN tdv.ValueInt = 1 THEN 1 ELSE 0 END
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.CreditorType = t.CreditorType
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.Property = 'IsPreferential'


	UPDATE t
	SET ProofOfDebt = CASE WHEN tdv.ValueInt = 1 THEN 1 ELSE 0 END
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.CreditorType = t.CreditorType
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.Property = 'ProofOfDebt'


	UPDATE t
	SET DeemedAccepted = CASE WHEN tdv.ValueInt = 1 THEN 1 ELSE 0 END
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.CreditorType = t.CreditorType
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.Property = 'DeemedAccepted'


	UPDATE t
	SET MoveToVoting = CASE WHEN tdv.ValueInt = 1 THEN 1 ELSE 0 END
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.CreditorType = t.CreditorType
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.Property = 'MoveToVoting'


	UPDATE t
	SET Vote = tdv.ValueInt
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.CreditorType = t.CreditorType
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.Property = 'Vote'

	UPDATE t
	SET Vote2 = tdv.ValueInt
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.CreditorType = t.CreditorType
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.Property = 'Vote2'

	UPDATE t
	SET Vote3 = tdv.ValueInt
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.CreditorType = t.CreditorType
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.Property = 'Vote3'


	UPDATE t
	SET ExcludeFromRepayments = CASE WHEN tdv.ValueInt = 1 THEN 1 ELSE 0 END
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.CreditorType = t.CreditorType
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.Property = 'ExcludeFromRepayments'


	UPDATE t
	SET CreditorReference = tdv.DetailValue
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.CreditorType = t.CreditorType
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.Property = 'CreditorReference'
	

	UPDATE t
	SET OptedIn = li.ItemValue
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.CreditorType = t.CreditorType
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	LEFT JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListItemID = tdv.ValueInt
	WHERE fm.Property = 'OptedIn'
	
	
	UPDATE t
	SET OverrideAmount = ISNULL(tdv.ValueMoney,0.00)
	FROM @TableOut t
	INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = t.TableRowID
	INNER JOIN ThirdPartyFieldMapping fm WITH ( NOLOCK ) on fm.DetailFieldID = tdv.DetailFieldID
	WHERE fm.ThirdPartyFieldID = 4120
	AND fm.ClientID = @ClientID
	AND fm.LeadTypeID = @LeadTypeID
	AND fm.ThirdPartyFieldGroupID = 85
	AND tdv.ValueMoney <> 0.00


	UPDATE t
	SET		 IsSecure				= ISNULL(t.IsSecure					,0		)
			,Applicant				= ISNULL(t.Applicant				,''		)
			,Balance				= ISNULL(t.Balance					,0.00	)
			,IsPreferential			= ISNULL(t.IsPreferential			,0		)
			,ProofOfDebt			= ISNULL(t.ProofOfDebt				,0		)
			,DeemedAccepted			= ISNULL(t.DeemedAccepted			,0		)
			,MoveToVoting			= ISNULL(t.MoveToVoting				,0		)
			,ExcludeFromRepayments	= ISNULL(t.ExcludeFromRepayments	,0		)
			,CreditorReference		= ISNULL(t.CreditorReference		,''		)	
			,OverrideAmount			= ISNULL(t.OverrideAmount			,0.00	)
			,OptedIn				= ISNULL(t.OptedIn					,''		)
			,Vote					= ISNULL(t.Vote						,0		)
			,Vote2					= ISNULL(t.Vote2					,0		)
			,Vote3					= ISNULL(t.Vote3					,0		)
			,PlanType				= ISNULL(li.ItemValue				,''		)
	FROM @TableOut t 
	LEFT JOIN CustomerDetailValues cdv WITH ( NOLOCK ) on cdv.CustomerID = t.CustomerID AND cdv.DetailFieldID = 296191 /*Select Plan Type For Fee Model*/ /*CS 2015-08-09*/
	LEFT JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListItemID = cdv.ValueInt

--SELECT fm.DetailFieldID, dp.PageName, fm2.LeadTypeID, fm2.ThirdPartyFieldID, fm2.DetailFieldID, df2.FieldName
--,',( ''' + df2.FieldName + '''		,	''ProofOfDebt''	,		' + CONVERT(VARCHAR,fm2.ThirdPartyFieldID) + '	)'
--FROM ThirdPartyFieldMapping fm WITH ( NOLOCK ) 
--INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = fm.DetailFieldID
--INNER JOIN DetailFieldPages dp WITH ( NOLOCK ) on dp.DetailFieldPageID = df.DetailFieldPageID
--INNER JOIN DetailFields df2 WITH ( NOLOCK ) on df2.TableDetailFieldPageID = dp.DetailFieldPageID
--LEFT JOIN ThirdPartyFieldMapping fm2 WITH ( NOLOCK ) on fm2.ClientID = fm.ClientID AND fm2.ThirdPartyFieldGroupID = 85 AND fm2.DetailFieldID = df.DetailFieldID
--WHERE fm.ClientID = 344
--AND fm.ThirdPartyFieldID = 852
--ORDER BY df2.FieldOrder

	INSERT @TableOut (CustomerID,TableRowID,CreditorType,CreditorName,IsSecure,Applicant,Balance,IsPreferential,ProofOfDebt,DeemedAccepted,MoveToVoting,ExcludeFromRepayments,CreditorReference,OverrideAmount,OptedIN,Vote,Vote2,Vote3,PlanType)
	
	SELECT fn.CustomerID,fn.TableRowID,fn.AssetType,fn.AssetType AS CreditorName, 0 AS 'IsSecure',fn.Applicant,tdv.DetailValue AS 'Balance',0 AS 'IsPreferential',1 AS 'ProofOfDebt',0 AS 'DeemedAccepted',1 AS 'MoveToVoting',0 AS 'ExcludeFromRepayments','' AS 'CreditorReference',0.00 AS 'OverrideAmount','' AS 'OptedIn', 0 AS 'Vote',0 AS 'Vote2', 0 AS 'Vote3',PlanType
	FROM dbo.fn_C00_ISI_GetAssetsForCustomer (@CustomerID) fn
	LEFT JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = fn.TableRowID AND tdv.DetailFieldID = 300822
	LEFT JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = fn.TableRowID AND tdv2.DetailFieldID = 300823
	LEFT JOIN	 LookupListItems lli WITH (NOLOCK) ON tdv2.ValueInt = lli.LookupListItemID
	WHERE AssetRetained = 0 
	AND lli.itemvalue lIKE '%Move to unsecured breakdown%' 
	
	RETURN
END





GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetCreditorsForCustomer] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_ISI_GetCreditorsForCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetCreditorsForCustomer] TO [sp_executeall]
GO
