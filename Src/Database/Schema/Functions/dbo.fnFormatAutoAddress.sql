SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger>
-- Create date: 2012-04-23
-- Description:	Format Auto Address
-- =============================================
CREATE FUNCTION [dbo].[fnFormatAutoAddress]
(
@Address varchar(2000),
@OutputFormat bit 
)
RETURNS varchar(2000)
AS
BEGIN

	/*
		Output format
		0 = Line Seperated
		1 = Comma Seperated
	
	*/

	SELECT @Address = REPLACE(REPLACE(@Address, '__', CASE @OutputFormat WHEN 0 THEN '\line ' ELSE ', ' END), '_', CASE @OutputFormat WHEN 0 THEN '\line ' ELSE ', ' END)

	-- Return the result of the function
	RETURN @Address

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnFormatAutoAddress] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnFormatAutoAddress] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnFormatAutoAddress] TO [sp_executeall]
GO
