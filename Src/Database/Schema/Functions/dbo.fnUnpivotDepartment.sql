SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Unpivot the columns in the Department table 
-- =============================================
CREATE FUNCTION [dbo].[fnUnpivotDepartment] 
(	
	@DepartmentID int
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT ColumnName as [RawColumnName], '[!' + ColumnName + ']' as [FormattedColumnName], RealValue
	FROM
	(
		SELECT 
		COALESCE(CONVERT(varchar(255), c.ClientID), '') as [ClientID],
		COALESCE(CONVERT(varchar(255), c.CustomerID), '') as [CustomerID],
		COALESCE(CONVERT(varchar(255), c.DepartmentDescription), '') as [DepartmentDescription],
		COALESCE(CONVERT(varchar(255), c.DepartmentID), '') as [DepartmentID],
		COALESCE(CONVERT(varchar(255), c.DepartmentName), '') as [DepartmentName]
		FROM dbo.Department c WITH (NOLOCK) 
		WHERE c.DepartmentID = @DepartmentID) p
	UNPIVOT
	(
		RealValue FOR ColumnName IN
		(
			[ClientID],[CustomerID],[DepartmentDescription],[DepartmentID],[DepartmentName]
		)
	) as unpvt

)





GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotDepartment] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnUnpivotDepartment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotDepartment] TO [sp_executeall]
GO
