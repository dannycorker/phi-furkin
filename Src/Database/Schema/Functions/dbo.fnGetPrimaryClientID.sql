SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date:	2020-01-13
-- Description:	Return the ID of the primary client on this database
-- 2020-01-13 CPS for JIRA LPC-355	| Function created.
--									| Initially locked to C600 so that we can incorporate the function into stored procs without breaking anything.
--									| Will change to 603 when ready.
-- 2020-02-05 GPR for JIRA AAG-19	| updated to 604
-- 2020-07-31 GPR					| Modified to switch based on DB_NAME() for TPET
-- =============================================
CREATE FUNCTION [dbo].[fnGetPrimaryClientID]
(
)
RETURNS INT
AS
BEGIN

	DECLARE @ClientID INT
	
	IF DB_NAME() IN ('Aquarius604Dev', 'Aquarius604QA', 'Aquarius604Staging', 'Aquarius604')
	BEGIN
		SET @ClientID = 604
	END
	ELSE IF DB_NAME() IN ('Aquarius605Dev', 'Aquarius605QA', 'Aquarius605Staging', 'Aquarius605')
	BEGIN
		SET @ClientID = 605
	END
	ELSE IF DB_NAME() IN ('Aquarius606Dev', 'Aquarius606QA', 'Aquarius606Staging', 'Aquarius606')
	BEGIN
		SET @ClientID = 606
	END
	ELSE IF DB_NAME() IN ('Aquarius607Dev', 'Aquarius607QA', 'Aquarius607Staging', 'Aquarius607')
	BEGIN
		SET @ClientID = 607
	END
	ELSE
	BEGIN
		SET @ClientID = 600 /*Default TPET*/
	END


	RETURN @ClientID

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetPrimaryClientID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetPrimaryClientID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetPrimaryClientID] TO [sp_executeall]
GO
