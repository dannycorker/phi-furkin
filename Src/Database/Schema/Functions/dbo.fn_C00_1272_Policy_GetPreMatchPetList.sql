SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-10-12
-- Description:	Returns a table of pets with policies to turn live
-- Modified:	
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_Policy_GetPreMatchPetList]
(
	@CustomerID INT
)
RETURNS 
	@PetList TABLE 
	(
		MatterID INT,
		PetNumber INT IDENTITY(1,1) NOT NULL
	)
AS
BEGIN
 
	INSERT @PetList
	SELECT m.MatterID 
	FROM Customers c WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON c.CustomerID=l.CustomerID AND l.LeadTypeID=1492
	INNER JOIN Cases ca WITH (NOLOCK) ON l.LeadID=ca.LeadID
	INNER JOIN Matter m WITH (NOLOCK) ON ca.CaseID=m.CaseID
	WHERE c.CustomerID=@CustomerID 
		AND NOT EXISTS ( SELECT * FROM LeadEvent le WITH (NOLOCK) 
						 WHERE ca.LatestInProcessLeadEventID=le.LeadEventID
							AND le.EventTypeID=150145 -- cancelled
					   )               
	ORDER BY m.MatterID

	RETURN

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPreMatchPetList] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1272_Policy_GetPreMatchPetList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_Policy_GetPreMatchPetList] TO [sp_executeall]
GO
