SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 06/03/2020
-- Description:	AU Domestic Payments File Format, Get Batch Record:
--				The Batch Control Record contains details relating to the total number of items as well as
--				debit/credit totals for a batch within the ABA import file.
-- =============================================
CREATE FUNCTION [dbo].[fnGetBatchRecord]
(
	@BatchNetTotalAmount VARCHAR(10),			-- Batch Credit Total Amount minus Batch Debit Total Amount. Right justified, zero filled, unsigned, two decimal places are implied (e.g. $1001.21 is stored as‘0000100121’). 21 30 10 Mandatory
	@BatchCreditTotalAmount VARCHAR(10),		-- Must be zero filled or contain the total value of all Record Type 1 CREDIT transactions in the batch. Right justified, zero filled, unsigned, two decimal places are implied. 31 40 10 Mandatory
	@BatchDebitTotalAmount VARCHAR(10),			-- Must be zero filled or contain the total value of all Record Type 1 DEBIT transactions in the batch. Right justified, zero filled, unsigned, two decimal places are implied. (e.g. $1001.21 is stored as‘0000100121’).41 50 10 Mandatory	
	@BatchTotalItemCount VARCHAR(6)				-- Total count of Type 1 records in the batch. Right justified, zero filled. 75 80 6 Mandatory 
	
)
RETURNS VARCHAR(120)
AS
BEGIN

	DECLARE @RecordType VARCHAR(1) = '7'					-- Must be ‘7’. 1 1 1 Mandatory
	DECLARE @ReservedOne VARCHAR(7) = '999-999'				-- Must be ‘999-999’. 2 8 7
	DECLARE @ReservedTwo VARCHAR(12) = REPLICATE(' ', 12)	-- Blank filled. 9 20 12 Optional
	DECLARE @ReservedThree VARCHAR(24) = REPLICATE(' ', 24)	-- Blank filled. 51 74 24 Optional
	DECLARE @ReservedFour VARCHAR(40) = REPLICATE(' ', 40)	-- Blank filled. 81 120 40 Optional

	DECLARE @Record VARCHAR(120)

	SELECT @Record = @RecordType + @ReservedOne + @ReservedTwo + dbo.fnPadLeft(@BatchNetTotalAmount, '0', 10) + dbo.fnPadLeft(@BatchCreditTotalAmount, '0', 10) + dbo.fnPadLeft(@BatchDebitTotalAmount, '0', 10) 
					+ @ReservedThree + dbo.fnPadLeft(@BatchTotalItemCount, '0', 6) + @ReservedFour

	return @Record

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetBatchRecord] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetBatchRecord] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetBatchRecord] TO [sp_executeall]
GO
