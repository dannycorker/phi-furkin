SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack / Gavin Reynolds
-- Create date:	2020-01-27
-- Description:	harvests Policy json object
-- 2020-01-27 GPR						| Populated stub function
-- 2020-01-27 GPR						| Altered to use @Include flag
-- 2020-01-27 CPS for JIRA LAGCLAIM-162	| Changed to XML
-- 2020-01-27 CPS for JIRA LAGCLAIM-162 | Added PolicyTerms
-- 2020-01-27 CPS for JIRA LAGCLAIM-369 | Update to match end to end solution document
-- 2020-02-05 MAB removed accountType from billings
-- 2020-02-06 CPS for JIRA LAGCLAIM-326 | Return the mapped ResourceListID for affinities
-- 2020-03-24 CPS for JIRA LAGCLAIM-VIS | Updated to match vision shapes
-- 2020-03-26 ALM for JIRA AAG-548 | copied and renamed from fn_C600_Harvest_Policy_JSON
-- 2020-03-27 MAB for JIRA AAG-550 | added new node for claim product ID
-- 2020-03-27 MAB removed redundant join to affinity resource list detail field "L&G ResourceListID"
-- 2020-07-29 ALM Changed ValueDate to ValueDateTime to prevent validation errors
-- 2020-08-28 ALM Included Excess and CoInsurance
-- 2020-09-11 ALM Added CountryID for Excess and CoInsurance
-- 2020-09-17 ALM Included Wellness products
-- 2021-02-05 ALM Include new status for vision: 1 Active, 2 Cancelled, 3 Cancelled to Inception
-- 2021-03-30 ALM Updated status for Vision: 1 Live, 2 Cancelled, 3 Cancellation Pending
-- 2021-03-31 ACE Removed redundant lead join. Matter can go directly to customer.
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Mediate_Harvest_Policy]
(
	 @PolicyID		INT
	,@VisionMode	BIT = 0
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE  @JsonData	VARCHAR(MAX) = '',
			 @CountryID INT 

	SELECT @CountryID = cu.CountryID 
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN Customers cu WITH (NOLOCK) ON cu.CustomerID = m.CustomerID
	WHERE m.MatterID = @PolicyID

	DECLARE @Products Table (ProductID INT, MatterID INT, Wellness INT, StartDate DATE, EndDate DATE)

	INSERT INTO @Products
	SELECT mdvClaimProductID.ValueInt, @PolicyID, 0, NULL, NULL 
	FROM MatterDetailValues mdvClaimProductID WITH (NOLOCK)
	WHERE @VisionMode = 1
	AND mdvClaimProductID.MatterID = dbo.fn_C00_1273_GetCurrentSchemeFromMatter (@PolicyID)
	AND mdvClaimProductID.DetailFieldID = 313875 /*Claim Product ID*/
	--UNION
	--SELECT TOP 1 rldv.DetailValue, tr1.MatterID, 1, tdv1.ValueDate, tdv2.ValueDate
	--FROM Tablerows tr1 WITH (NOLOCK) 
	--INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr1.TableRowID AND tdv.DetailFieldID = 314234
	--INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) ON rldv.ResourceListID = tdv.ResourceListID AND rldv.DetailFieldID = 314268
	--INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tdv.TableRowID AND tdv1.DetailFieldID = 314237
	--INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = tdv.TableRowID AND tdv2.DetailFieldID = 314239
	--INNER JOIN TableDetailValues tdv3 WITH (NOLOCK) ON tdv3.TableRowID = tdv.TableRowID AND tdv3.DetailFieldID = 314236
	--WHERE 
	--tr1.DetailFieldID = 314240
	--AND tr1.MatterID = @PolicyID

	IF @PolicyID <> 0
	BEGIN
		SELECT  @JsonData +=
		(
			SELECT	 CASE WHEN @VisionMode = 0 THEN mdvDateStart.ValueDate	END	[startDate]
					,CASE WHEN @VisionMode = 0 THEN mdvDateEnd.ValueDate	END	[endDate]
					,CASE WHEN @VisionMode = 0 THEN mdvDateRenwl.ValueDate	END	[renewalDate]
					,CASE WHEN @VisionMode = 0 THEN mdvDateCancl.ValueDate	END	[cancellationDate]
					,CASE WHEN @VisionMode = 0 THEN mdvDateCover.ValueDate	END	[coveredUntilDate]
					,CASE WHEN @VisionMode = 0 THEN mdvDateTerms.ValueDate	END	[policyTermsDate]
					,CASE WHEN @VisionMode = 0 THEN mdvDateSale.ValueDate	END	[originalSaleDate]
					,CASE WHEN @VisionMode = 0 THEN lliCancReasn.ItemValue	END [cancellationReason]
					,CASE WHEN @VisionMode = 0 THEN cdv.ValueInt			END [affinityResourceListId]
					,(
						SELECT	TOP 1
								p.productid [productId]
								,CASE WHEN @CountryID IN (233,39) AND @VisionMode = 1 AND p.Wellness = 0 AND p.productID = 12 THEN CAST(tdvExcess.ValueMoney AS DECIMAL(18,2)) END [fixedExcessLimit]
								,CASE WHEN @CountryID IN (233,39) AND @VisionMode = 1 AND p.Wellness = 0 AND p.productID = 12 THEN CAST(tdvCoInsurance.ValueMoney AS DECIMAL(18,2)) END	[coInsuranceLimit]
								,CASE WHEN p.Wellness = 1 THEN p.StartDate ELSE tdvFromDate.ValueDateTime END [startDate]
								,CASE WHEN p.Wellness = 1 THEN p.EndDate ELSE tdvToDate.ValueDateTime END [endDate]
								,CASE WHEN mdvStatus.ValueInt = 43003 AND tdvToDate.ValueDate <= tdvIncDate.ValueDate THEN 3 
								WHEN mdvStatus.ValueInt = 43003 AND tdvToDate.ValueDate > tdvIncDate.ValueDate THEN 2
								ELSE 1 END [status]
								,CASE WHEN @VisionMode = 0 THEN rdvProd.DetailValue END [productName]
								,CASE WHEN @VisionMode = 0 THEN tdvIncDate.ValueDate END [inceptionDate]
						FROM 
						@Products p 
						JOIN MatterDetailValues mdvProd WITH (NOLOCK) ON mdvProd.MatterID = p.matterid
						INNER JOIN TableRows tr							WITH (NOLOCK) on tr.MatterID = p.MatterID					AND tr.DetailFieldID			= 170033 /*Historical Policy*/
						INNER JOIN TableDetailValues tdvFromDate		WITH (NOLOCK) on tdvFromDate.TableRowID = tr.TableRowID		AND tdvFromDate.DetailFieldID	= 145663 /*Start*/
						INNER JOIN TableDetailValues tdvToDate			WITH (NOLOCK) on tdvToDate.TableRowID = tr.TableRowID		AND tdvToDate.DetailFieldID		= 145664 /*End*/
						INNER JOIN TableDetailValues tdvIncDate			WITH (NOLOCK) on tdvIncDate.TableRowID = tr.TableRowID		AND tdvIncDate.DetailFieldID	= 145662 /*Inception*/
						INNER JOIN TableDetailValues tdvStatus			WITH (NOLOCK) on tdvStatus.TableRowID = tr.TableRowID		AND tdvStatus.DetailFieldID		= 145666 /*Status*/
						INNER JOIN LookupListItems	 lliStatus			WITH (NOLOCK) on lliStatus.LookupListItemID = tdvStatus.ValueInt
						INNER JOIN ResourceListDetailValues rdvProd		WITH (NOLOCK) on rdvProd.ResourceListID = mdvProd.ValueInt	AND rdvProd.DetailFieldID		= 146200 /*Product*/
						INNER JOIN TableDetailValues tdvExcess			WITH (NOLOCK) ON tdvExcess.TableRowID = tr.TableRowID		AND tdvExcess.DetailFieldID		= 177416 /*Excess*/
						INNER JOIN TableDetailValues tdvCoInsurance		WITH (NOLOCK) ON tdvCoInsurance.TableRowID = tr.TableRowID	AND tdvCoInsurance.DetailFieldID = 177415 /*CoInsurance*/
						JOIN OneVision oneVision						WITH (NOLOCK) ON oneVision.PAMatterID = mdvProd.MatterID	AND oneVision.PolicyTermType = 'Policy'	  /*One Vision Table*/
						WHERE p.MatterID = @PolicyID
						AND mdvProd.DetailFieldID = 170034
						ORDER BY tr.TableRowID DESC
						FOR JSON PATH
					 ) [policyTerms]

					 ,l.LeadRef														[reference]
					 ,CASE WHEN @VisionMode = 1 THEN 1							END	[type]
					 ,CASE WHEN @VisionMode = 1 THEN mdvDateIncep.ValueDateTime		END [inceptionDate]
					 ,CASE WHEN @VisionMode = 1 THEN 0.00						END [premium]
					 ,CASE WHEN mdvStatus.ValueInt = 43003 THEN 2 /*Cancelled*/
						   WHEN mdvStatus.ValueInt = 74535 THEN 3 /*Cancellation Pending*/
						   ELSE 1 /*Live*/
					  END													    	 [status]
					 ,CASE WHEN @VisionMode = 1 THEN mdvDateTerms.ValueDateTime		END [termsDate]

					,(
						SELECT	 ac.AccountHolderName		[payee]
								,ac.AccountNumber			[accountNumber]
								,ac.Sortcode				[sortCode]
								,ac.Bankname				[bankName]
								,CASE WHEN @VisionMode = 1 THEN NULL ELSE lliPayMethod.ItemValue	END [paymentMethod]
								,CASE WHEN @VisionMode = 1 THEN NULL ELSE lliPayInteval.ItemValue	END [paymentInterval]
						FROM Matter mBill WITH (NOLOCK) 
						INNER JOIN LeadTypeRelationship ltr			WITH (NOLOCK) on ltr.FromLeadID = mBill.LeadID AND ltr.ToLeadTypeID = 1493 /*Collections*/
						INNER JOIN MatterDetailValues mdvAccountID	WITH (NOLOCK) on mdvAccountID.MatterID = ltr.ToMatterID		AND mdvAccountID.DetailFieldID = 176973 /*Billing System Account ID*/
						INNER JOIN Account ac						WITH (NOLOCK) on ac.AccountID = mdvAccountID.ValueInt
						INNER JOIN MatterDetailValues mdvPayMethod	WITH (NOLOCK) on mdvPayMethod.LeadID = mBill.LeadID			AND mdvPayMethod.DetailFieldID = 170114 /*Payment Method*/
						INNER JOIN LookupListItems lliPayMethod		WITH (NOLOCK) on lliPayMethod.LookupListItemID = mdvPayMethod.ValueInt
						INNER JOIN MatterDetailValues mdvPayInteval WITH (NOLOCK) on mdvPayInteval.LeadID = mBill.LeadID		AND mdvPayInteval.DetailFieldID = 170176 /*Payment Interval*/
						INNER JOIN LookupListItems lliPayInteval	WITH (NOLOCK) on lliPayInteval.LookupListItemID = mdvPayInteval.ValueInt
						WHERE mBill.MatterID = @PolicyID
						AND @VisionMode = 0
						FOR JSON PATH
					) [billings]
					,(
						SELECT	 tdvDateTme.ValueDateTime							[dateTimeLogged]
								,tdvCovrFrm.ValueDate								[coverFrom]
								,ISNULL(tdvCoverTo.ValueDate,mdvDateEnd.ValueDate)	[coverTo]
								,lliAdjType.ItemValue								[adjustmentType]
								,tdvAdjDate.ValueDate								[writtenPremiumAdjustmentDate]
								,tdvAnnPrem.ValueMoney								[annualPremium]
								,tdvDscPrem.ValueMoney								[annualDiscountedPremium]
								,tdvEvntTyp.DetailValue								[eventType]
								,tdvAdjAmnt.ValueMoney								[adjustmentAmount]
						FROM TableRows tr WITH (NOLOCK)
						INNER JOIN TableDetailValues tdvDateTme	WITH (NOLOCK) on tdvDateTme.TableRowID = tr.TableRowID AND tdvDateTme.DetailFieldID = 175400 /*Date and Time Logged*/
						INNER JOIN TableDetailValues tdvCovrFrm	WITH (NOLOCK) on tdvCovrFrm.TableRowID = tr.TableRowID AND tdvCovrFrm.DetailFieldID = 175346 /*Cover From*/
						INNER JOIN TableDetailValues tdvCoverTo	WITH (NOLOCK) on tdvCoverTo.TableRowID = tr.TableRowID AND tdvCoverTo.DetailFieldID	= 175347 /*Cover To*/
						INNER JOIN TableDetailValues tdvAdjType	WITH (NOLOCK) on tdvAdjType.TableRowID = tr.TableRowID AND tdvAdjType.DetailFieldID = 175398 /*Adjustment Type*/
						INNER JOIN LookupListItems   lliAdjType WITH (NOLOCK) on lliAdjType.LookupListItemID = tdvAdjType.ValueInt
						INNER JOIN TableDetailValues tdvAdjDate	WITH (NOLOCK) on tdvAdjDate.TableRowID = tr.TableRowID AND tdvAdjDate.DetailFieldID = 175396 /*Written Premium Adjustment Date*/
						INNER JOIN TableDetailValues tdvAnnPrem	WITH (NOLOCK) on tdvAnnPrem.TableRowID = tr.TableRowID AND tdvAnnPrem.DetailFieldID = 175349 /*Annual Premium*/
						INNER JOIN TableDetailValues tdvDscPrem	WITH (NOLOCK) on tdvDscPrem.TableRowID = tr.TableRowID AND tdvDscPrem.DetailFieldID = 177899 /*Annual Discounted Premium*/
						INNER JOIN TableDetailValues tdvEvntTyp	WITH (NOLOCK) on tdvEvntTyp.TableRowID = tr.TableRowID AND tdvEvntTyp.DetailFieldID = 175348 /*Event*/
						INNER JOIN TableDetailValues tdvAdjAmnt	WITH (NOLOCK) on tdvAdjAmnt.TableRowID = tr.TableRowID AND tdvAdjAmnt.DetailFieldID = 175397 /*Adjustment Amount*/
						WHERE tr.DetailFieldID = 175336 /*Premium Detail History*/
						AND tr.MatterID = m.MatterID
						AND tdvDateTme.ValueDateTime is not NULL
						AND @VisionMode = 0
						FOR JSON PATH
					 ) [premiumAdjustments]
					,CASE WHEN @VisionMode = 0 THEN mdvObjectID.DetailValue END [objectID]
					,CASE WHEN @VisionMode = 0 THEN ldvParentID.DetailValue END [parentObjectID]
			FROM Customers cu								WITH (NOLOCK)
			INNER JOIN Lead l								WITH (NOLOCK) on l.CustomerID = cu.CustomerID
			INNER JOIN Cases c								WITH (NOLOCK) on c.LeadID = l.LeadID
			INNER JOIN Matter m								WITH (NOLOCK) on m.CaseID = c.CaseID
			INNER JOIN MatterDetailValues mdvStatus			WITH (NOLOCK) on mdvStatus.MatterID = m.MatterID					AND mdvStatus.DetailFieldID			= 170038 /*Policy Status*/
			INNER JOIN LookupListItems lliStatus			WITH (NOLOCK) on lliStatus.LookupListItemID = mdvStatus.ValueInt	
			LEFT JOIN MatterDetailValues mdvDateIncep		WITH (NOLOCK) on mdvDateIncep.MatterID = m.MatterID					AND mdvDateIncep.DetailFieldID		= 170035 /*Policy Inception Date*/
			LEFT JOIN MatterDetailValues mdvDateStart		WITH (NOLOCK) on mdvDateStart.MatterID = m.MatterID					AND mdvDateStart.DetailFieldID		= 170036 /*Policy Start Date*/
			LEFT JOIN MatterDetailValues mdvDateEnd			WITH (NOLOCK) on mdvDateEnd  .MatterID = m.MatterID					AND mdvDateEnd.DetailFieldID		= 170037 /*Policy End Date*/
			LEFT JOIN MatterDetailValues mdvDateSale		WITH (NOLOCK) on mdvDateSale. MatterID = m.MatterID					AND mdvDateSale.DetailFieldID		= 177418 /*Policy Original Sale Date*/
			LEFT JOIN MatterDetailValues mdvDateTerms		WITH (NOLOCK) on mdvDateTerms.MatterID = m.MatterID					AND mdvDateTerms.DetailFieldID		= 176925 /*Policy Terms Date*/
			LEFT JOIN MatterDetailValues mdvDateCover		WITH (NOLOCK) on mdvDateCover.MatterID = m.MatterID					AND mdvDateCover.DetailFieldID		= 177287 /*Covered Until Date*/
			LEFT JOIN MatterDetailValues mdvDateCancl		WITH (NOLOCK) on mdvDateCancl.MatterID = m.MatterID					AND mdvDateCancl.DetailFieldID		= 170055 /*Cancellation date*/
			LEFT JOIN MatterDetailValues mdvDateRenwl		WITH (NOLOCK) on mdvDateRenwl.MatterID = m.MatterID					AND mdvDateRenwl.DetailFieldID		= 175307 /*Policy Renewal Date*/
			LEFT JOIN MatterDetailValues mdvCancReasn		WITH (NOLOCK) on mdvCancReasn.MatterID = m.MatterID					AND mdvCancReasn.DetailFieldID		= 170054 /*Cancellation reason*/
			LEFT JOIN LookupListItems	  lliCancReasn		WITH (NOLOCK) on lliCancReasn.LookupListItemID = mdvCancReasn.ValueInt
			LEFT JOIN CustomerDetailValues cdv				WITH (NOLOCK) on cdv.CustomerID = l.CustomerID						AND cdv.DetailFieldID				= 170144 /*Affinity Details*/
			LEFT JOIN LeadTypeRelationship ltr				WITH (NOLOCK) on ltr.FromLeadID = l.LeadID							AND ltr.ToLeadTypeID = 1493
			LEFT JOIN Matter mColls							WITH (NOLOCK) on mColls.MatterID = ltr.ToMatterID					AND ltr.ToLeadTypeID = 1493
			LEFT JOIN LeadDetailValues ldvParentID			WITH (NOLOCK) on ldvParentID.LeadID = l.LeadID						AND ldvParentID.DetailFieldID		= 313787 /*API Transfer Mapping GUID*/
			LEFT JOIN MatterDetailValues mdvObjectID		WITH (NOLOCK) on mdvObjectID.MatterID = m.MatterID					AND mdvObjectID.DetailFieldID		= 313792 /*API Transfer Mapping GUID*/
			WHERE m.MatterID = @PolicyID
			AND l.LeadTypeID = 1492 /*Policy Admin*/
			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		)
	END

	RETURN @JsonData

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Mediate_Harvest_Policy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_Mediate_Harvest_Policy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Mediate_Harvest_Policy] TO [sp_executeall]
GO
