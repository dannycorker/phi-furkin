SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack / Gavin Reynolds
-- Create date:	2019-08-07
-- Description:	Harvests Claim JSON object
-- 2019-08-08 GPR						| Populated stub function
-- 2019-08-27 GPR						| Altered to use @Include flag
-- 2019-11-12 CPS for JIRA LAGCLAIM-162	| Changed to XML
-- 2019-11-14 CPS for JIRA LAGCLAIM-162 | Added PolicyTerms
-- 2019-11-14 ALM for JIRA LAGCLAIM-210 | Changed for Claim
-- 2019-12-11 ALM for JIRA LAGCLAIM-273 | Changed for Claim Items
-- 2020-01-02 MAB for JIRA LAGCLAIM-217 | Added Payment Data + re-factored SQL for nested JSON arrays
-- 2020-01-20 MAB dor JIRA LAGCLAIM-365 | modified harvest of Deductions 
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Harvest_Claim_JSON]
(
	 @ClaimID		INT
	,@VisionMode	BIT
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE @JsonData VARCHAR(MAX) = ''

	IF @ClaimID <> 0
	BEGIN
		SELECT  @JsonData =
		(
			SELECT	 m.MatterID												[claimId]
					,ltr.FromLeadID											[policyId]
					,l.LeadID												[parentId]
					,fnolDesc.DetailValue									[fnolDescription]
					,dol.ValueDate											[dateOfLoss]
					,ISNULL(ldvDOD.ValueDate,'')							[dateOfDeath]
					,treatStart.ValueDate									[treatmentStartDate]
					,treatEnd.ValueDate										[treatmentEndDate]
					,amount.ValueMoney										[amount]
					,0														[triggeredRequest]
					,0														[isEditable]
					,CASE WHEN EXISTS 
					(SELECT * FROM LeadEvent le WITH (NOLOCK)
					 WHERE le.CaseID = c.CaseID 
					 AND le.EventTypeID IN (156839, 158634, 158880)) 
					 THEN '1' ELSE '0' END									[hasMissingItems]
					,CASE WHEN EXISTS 
					(SELECT * FROM TableRows tr WITH (NOLOCK)
					 WHERE tr.MatterID = m.MatterID
					 AND tr.DetailFieldID = 144355) 
					 THEN '1' ELSE '0' END									[hasClaimItems]
					,CASE WHEN EXISTS 
					(SELECT * FROM TableRows tr WITH (NOLOCK)
					 WHERE tr.MatterID = m.MatterID
					 AND tr.DetailFieldID = 154485) 
					 THEN '1' ELSE '0' END									[hasPayments]
					,CASE WHEN EXISTS 
					(SELECT * FROM LeadEvent le WITH (NOLOCK)
					 WHERE le.CaseID = c.CaseID 
					 AND le.EventTypeID IN (121882, 121881)) 
					 THEN '1' ELSE '0' END									[wasReassessed]
					,ailment.ValueInt										[ailment.ailmentId]
					,firstCause.DetailValue									[ailment.firstCause]
					,secCause.DetailValue									[ailment.secondCause]
					,conType.ValueInt										[ailment.conditionType]
					,conTypeValue.ItemValue									[ailment.conditionTypeDescription]
					,claimSettled.Valuedate									[claimSettled.claimSettledConfirm] --CR added the claimSettled.claimSettledConfirm
					,currentVet.ValueInt									[vet.id]
					,pracName.DetailValue									[vet.practiceName]
					,vetAddress1.DetailValue								[vet.address1]
					,vetAddress2.DetailValue								[vet.address2]
					,vetTown.DetailValue									[vet.town]
					,vetPostcode.DetailValue								[vet.postcode]
					,(
						SELECT DISTINCT /*If there are multiple Deductions per claim, then a duplicate Claim item will be returned for each deduction. Using DISTINCT rather than GROUP BY for code clarity.*/
							tdvTreatStart.ValueDate									[treatmentStartDate]
							,tdvTreatEnd.ValueDate									[treatmentEndDate]
							,policySection.ResourceListID							[policySection]
							,claimAmount.ValueMoney									[amount]
							,totalDeductns.ValueMoney								[totalUserDeductions]
							,claimSettle.ValueMoney									[settle]
							,datePaid.ValueDate										[datePaid]
							,payee.DetailValue										[payee]
							,noCover.ValueMoney										[noCover]
							,excess.ValueMoney										[excess]
							,volExcess.ValueMoney									[voluntaryExcess]
							,rebate.ValueMoney										[rebate]	
							,coIns.ValueMoney										[coInsurance]
							,limit.ValueMoney										[limit]
							,total.ValueMoney										[total]
							,payTo.ValueInt											[payTo]
							,limitReached.ValueInt									[limitReached]
							,claimRowType.ValueInt									[claimRowType]
							,approvedDate.ValueDate									[approved]
							,SAPRejCode.DetailValue									[sapRejectionCode]
							,SAPResult.DetailValue									[sapPaymentResult]
							,SAPDocCode.DetailValue									[sapDocumentCode]
							,alreadyPaid.ValueMoney									[alreadyPaid]
							,(
								SELECT
									tdvParentClaimRowID.ValueInt							[parentClaimRowId]
									,tdvReason.ValueInt										[deductionReasonId]
									,lliReason.ItemValue									[deductionReason]
									,tdvDeductAmount.ValueMoney								[deductionAmount]
									,tdvItemNotCov.DetailValue								[itemNotCovered]
								FROM
									TableDetailValues tdvParentClaimRowID		WITH (NOLOCK)
									LEFT JOIN TableDetailValues tdvReason		WITH (NOLOCK) ON tdvReason.TableRowID = tdvParentClaimRowID.TableRowID			AND tdvReason.DetailFieldID			= 147300 /*Deduction Reason*/
									INNER JOIN LookupListItems lliReason		WITH (NOLOCK) ON tdvReason.ValueInt = lliReason.LookupListItemID				AND lliReason.LookupListID			= 3819   /*Deduction Reasons*/
									LEFT JOIN TableDetailValues tdvDeductAmount	WITH (NOLOCK) ON tdvDeductAmount.TableRowID = tdvParentClaimRowID.TableRowID	AND tdvDeductAmount.DetailFieldID	= 147301 /*Deduction Amount*/
									LEFT JOIN TableDetailValues tdvItemNotCov	WITH (NOLOCK) ON tdvItemNotCov.TableRowID = tdvParentClaimRowID.TableRowID		AND tdvItemNotCov.DetailFieldID		= 148432 /*Item Not Covered*/
								WHERE tdvParentClaimRowID.ValueInt = claimRow.TableRowID
									AND tdvParentClaimRowID.DetailFieldID= 147299 /*Claim Detail Table Row ID*/
								FOR JSON PATH
							) AS userDeductions
							,claimRow.TableRowID									[sourceId]
						FROM TableRows claimRow						WITH (NOLOCK)
						INNER JOIN TableDetailValues tdvTreatStart	WITH (NOLOCK) ON tdvTreatStart.TableRowID = claimRow.TableRowID		AND tdvTreatStart.DetailFieldID		= 144349 /*Treatment Start*/
						INNER JOIN TableDetailValues tdvTreatEnd	WITH (NOLOCK) ON tdvTreatEnd.TableRowID = claimRow.TableRowID		AND tdvTreatEnd.DetailFieldID		= 144351 /*Treatment End*/
						INNER JOIN TableDetailValues policySection	WITH (NOLOCK) ON policySection.TableRowID = claimRow.TableRowID		AND policySection.DetailFieldID		= 144350 /*Policy Section RLID*/
						INNER JOIN TableDetailValues claimAmount	WITH (NOLOCK) ON claimAmount.TableRowID = claimRow.TableRowID		AND claimAmount.DetailFieldID		= 144353 /*Claim Amount*/
						LEFT JOIN  TableDetailValues totalDeductns	WITH (NOLOCK) ON totalDeductns.TableRowID = claimRow.TableRowID		AND totalDeductns.DetailFieldID		= 146179 /*Total User Deductions*/
						INNER JOIN TableDetailValues claimSettle	WITH (NOLOCK) ON claimSettle.TableRowID = claimRow.TableRowID		AND claimSettle.DetailFieldID		= 145678 /*Settle*/
						LEFT JOIN  TableDetailValues datePaid		WITH (NOLOCK) ON datePaid.TableRowID = claimRow.TableRowID			AND datePaid.DetailFieldID			= 144354 /*Paid Date*/
						LEFT JOIN  TableDetailValues payee			WITH (NOLOCK) ON payee.TableRowID = claimRow.TableRowID				AND payee.DetailFieldID				= 145679 /*Payee*/
						LEFT JOIN  TableDetailValues noCover		WITH (NOLOCK) ON noCover.TableRowID = claimRow.TableRowID			AND noCover.DetailFieldID			= 147001 /*No Cover*/
						INNER JOIN TableDetailValues excess			WITH (NOLOCK) ON excess.TableRowID = claimRow.TableRowID			AND excess.DetailFieldID			= 146406 /*Excess*/
						LEFT JOIN  TableDetailValues volExcess		WITH (NOLOCK) ON volExcess.TableRowID = claimRow.TableRowID			AND volExcess.DetailFieldID			= 158802 /*Voluntary Excess*/
						LEFT JOIN  TableDetailValues rebate			WITH (NOLOCK) ON rebate.TableRowID = claimRow.TableRowID			AND rebate.DetailFieldID			= 147434 /*Rebate*/
						INNER JOIN TableDetailValues coIns			WITH (NOLOCK) ON coIns.TableRowID = claimRow.TableRowID				AND coIns.DetailFieldID				= 146407 /*CoInsurance*/
						LEFT JOIN  TableDetailValues limit			WITH (NOLOCK) ON limit.TableRowID = claimRow.TableRowID				AND limit.DetailFieldID				= 146408 /*Limit*/
						INNER JOIN TableDetailValues total			WITH (NOLOCK) ON total.TableRowID = claimRow.TableRowID				AND total.DetailFieldID				= 144352 /*Total*/
						LEFT JOIN  TableDetailValues payTo			WITH (NOLOCK) ON payTo.TableRowID = claimRow.TableRowID				AND payTo.DetailFieldID				= 147602 /*Pay To*/
						LEFT JOIN  TableDetailValues limitReached	WITH (NOLOCK) ON limitReached.TableRowID = claimRow.TableRowID		AND limitReached.DetailFieldID		= 148399 /*Limit Reached*/
						INNER JOIN TableDetailValues claimRowType	WITH (NOLOCK) ON claimRowType.TableRowID = claimRow.TableRowID		AND claimRowType.DetailFieldID		= 149778 /*Claim Row Type*/
						LEFT JOIN  TableDetailValues approvedDate	WITH (NOLOCK) ON approvedDate.TableRowID = claimRow.TableRowID		AND approvedDate.DetailFieldID		= 144362 /*Approved Date*/
						LEFT JOIN  TableDetailValues SAPRejCode		WITH (NOLOCK) ON SAPRejCode.TableRowID = claimRow.TableRowID		AND SAPRejCode.DetailFieldID		= 158252 /*SAP Rejection Code*/
						LEFT JOIN  TableDetailValues SAPResult		WITH (NOLOCK) ON SAPResult.TableRowID = claimRow.TableRowID			AND SAPResult.DetailFieldID			= 158537 /*SAP Payment Result*/
						LEFT JOIN  TableDetailValues SAPDocCode		WITH (NOLOCK) ON SAPDocCode.TableRowID = claimRow.TableRowID		AND SAPDocCode.DetailFieldID		= 158538 /*SAP Document Code*/
						LEFT JOIN  TableDetailValues alreadyPaid	WITH (NOLOCK) ON alreadyPaid.TableRowID = claimRow.TableRowID		AND alreadyPaid.DetailFieldID		= 159290 /*Already Paid*/
						LEFT JOIN  TableDetailValues tdvClaimRowID	WITH (NOLOCK) ON tdvClaimRowID.ValueInt = claimRow.TableRowID		AND tdvClaimRowID.DetailFieldID		= 147299 /*Claim Detail Table Row ID*/
						WHERE claimRow.MatterID = m.MatterID 
						AND claimRow.DetailFieldID	= 144355 /*Claim Details Date TableRowID*/
						FOR JSON PATH
					) AS claimItems
					,(
						SELECT
							tdvPayeeTitle.DetailValue								[payeeTitle]
							,tdvPayeeFirstName.DetailValue							[payeeFirstName]
							,tdvPayeeLastName.DetailValue							[payeeLastName]
							,tdvPayeeOrganisation.DetailValue						[payeeOrganisation]
							,tdvPayeeAddress1.DetailValue							[payeeAddress1]
							,tdvPayeeAddress3.DetailValue							[payeeAddress3]
							,tdvPayeePostcode.DetailValue							[payeePostcode]
							,tdvPayeeCountry.DetailValue							[payeeCountry]
							,tdvPayeeReference.DetailValue							[payeeReference]
							,tdvPayeeType.ValueInt									[payeeType]
							,tdvPolicyholderTitle.ValueInt							[policyholderTitle]
							,tdvPolicyholderFirstName.DetailValue					[policyholderFirstName]
							,tdvPolicyholderLastName.DetailValue					[policyholderLastName]
							,tdvPolicyholderOrganisation.DetailValue				[policyholderOrganisation]
							,tdvPolicyholderAddress1.DetailValue					[policyholderAddress1]
							,tdvPolicyholderAddress3.DetailValue					[policyholderAddress3]
							,tdvPolicyholderPostcode.DetailValue					[policyholderPostcode]
							,tdvPolicyholderCountry.DetailValue						[policyholderCountry]
							,tdvPolicyholderReference.DetailValue					[policyholderReference]
							,tdvPolicyCode.DetailValue								[policyCode]
							,tdvClaimCode.DetailValue								[claimCode]
							,tdvPolicyholderType.ValueInt							[policyholderType]
							,tdvPaymentType.ValueInt								[paymentType]
							,tdvPaymentAmount.ValueMoney							[paymentAmount]
							,tdvPaymentDate.ValueDate								[paymentDate]
							,tdvDateCreated.ValueDate								[dateCreated]
							,tdvPetName.DetailValue									[petName]
							,tdvPaymentApproved.ValueDate							[paymentApproved]
							,lliPayTo.ItemValue										[payTo]
							,tdvAffinityPartnerName.DetailValue						[affinityPartnerName]
							,tdvValidFromDate.ValueDate								[validFromDate]
							,tdvPaymentMethod.ValueInt								[paymentMethod]
							,tdvAccountNumber.ValueInt								[accountNumber]
							,tdvSortCode.ValueInt									[sortCode]
						FROM TableRows paymentDataRow								WITH (NOLOCK)
						LEFT JOIN  TableDetailValues tdvPayeeTitle					WITH (NOLOCK) ON tdvPayeeTitle.TableRowID = paymentDataRow.TableRowID					AND tdvPayeeTitle.DetailFieldID					= 154486 /*Payee Title*/
						LEFT JOIN  TableDetailValues tdvPayeeFirstName				WITH (NOLOCK) ON tdvPayeeFirstName.TableRowID = paymentDataRow.TableRowID				AND tdvPayeeFirstName.DetailFieldID				= 154487 /*Payee First Name*/
						LEFT JOIN  TableDetailValues tdvPayeeLastName				WITH (NOLOCK) ON tdvPayeeLastName.TableRowID = paymentDataRow.TableRowID				AND tdvPayeeLastName.DetailFieldID				= 154488 /*Payee Last Name*/
						LEFT JOIN  TableDetailValues tdvPayeeOrganisation			WITH (NOLOCK) ON tdvPayeeOrganisation.TableRowID = paymentDataRow.TableRowID			AND tdvPayeeOrganisation.DetailFieldID			= 154490 /*Payee Organisation*/
						LEFT JOIN  TableDetailValues tdvPayeeAddress1				WITH (NOLOCK) ON tdvPayeeAddress1.TableRowID = paymentDataRow.TableRowID				AND tdvPayeeAddress1.DetailFieldID				= 154491 /*Payee Address 1*/
						LEFT JOIN  TableDetailValues tdvPayeeAddress3				WITH (NOLOCK) ON tdvPayeeAddress3.TableRowID = paymentDataRow.TableRowID				AND tdvPayeeAddress3.DetailFieldID				= 154493 /*Payee Address 3*/
						LEFT JOIN  TableDetailValues tdvPayeePostcode				WITH (NOLOCK) ON tdvPayeePostcode.TableRowID = paymentDataRow.TableRowID				AND tdvPayeePostcode.DetailFieldID				= 154496 /*Payee Postcode*/
						LEFT JOIN  TableDetailValues tdvPayeeCountry				WITH (NOLOCK) ON tdvPayeeCountry.TableRowID = paymentDataRow.TableRowID					AND tdvPayeeCountry.DetailFieldID				= 154497 /*Payee Country*/
						LEFT JOIN  TableDetailValues tdvPayeeReference				WITH (NOLOCK) ON tdvPayeeReference.TableRowID = paymentDataRow.TableRowID				AND tdvPayeeReference.DetailFieldID				= 154498 /*Payee Reference*/
						LEFT JOIN  TableDetailValues tdvPayeeType					WITH (NOLOCK) ON tdvPayeeType.TableRowID = paymentDataRow.TableRowID					AND tdvPayeeType.DetailFieldID					= 154499 /*Payee Type*/
						LEFT JOIN  TableDetailValues tdvPolicyholderTitle			WITH (NOLOCK) ON tdvPolicyholderTitle.TableRowID = paymentDataRow.TableRowID			AND tdvPolicyholderTitle.DetailFieldID			= 154500 /*Policyholder Title*/
						LEFT JOIN  TableDetailValues tdvPolicyholderFirstName		WITH (NOLOCK) ON tdvPolicyholderFirstName.TableRowID = paymentDataRow.TableRowID		AND tdvPolicyholderFirstName.DetailFieldID		= 154501 /*Policyholder First Name*/
						LEFT JOIN  TableDetailValues tdvPolicyholderLastName		WITH (NOLOCK) ON tdvPolicyholderLastName.TableRowID = paymentDataRow.TableRowID			AND tdvPolicyholderLastName.DetailFieldID		= 154502 /*Policyholder Last Name*/
						LEFT JOIN  TableDetailValues tdvPolicyholderOrganisation 	WITH (NOLOCK) ON tdvPolicyholderOrganisation.TableRowID = paymentDataRow.TableRowID		AND tdvPolicyholderOrganisation.DetailFieldID	= 154503 /*Policyholder Organisation*/
						LEFT JOIN  TableDetailValues tdvPolicyholderAddress1		WITH (NOLOCK) ON tdvPolicyholderAddress1.TableRowID = paymentDataRow.TableRowID			AND tdvPolicyholderAddress1.DetailFieldID		= 154504 /*Policyholder Address 1*/
						LEFT JOIN  TableDetailValues tdvPolicyholderAddress3		WITH (NOLOCK) ON tdvPolicyholderAddress3.TableRowID = paymentDataRow.TableRowID			AND tdvPolicyholderAddress3.DetailFieldID		= 154506 /*Policyholder Address 3*/
						LEFT JOIN  TableDetailValues tdvPolicyholderPostcode		WITH (NOLOCK) ON tdvPolicyholderPostcode.TableRowID = paymentDataRow.TableRowID			AND tdvPolicyholderPostcode.DetailFieldID		= 154509 /*Policyholder Postcode*/
						LEFT JOIN  TableDetailValues tdvPolicyholderCountry			WITH (NOLOCK) ON tdvPolicyholderCountry.TableRowID = paymentDataRow.TableRowID			AND tdvPolicyholderCountry.DetailFieldID		= 154510 /*Policyholder Country*/
						LEFT JOIN  TableDetailValues tdvPolicyholderReference		WITH (NOLOCK) ON tdvPolicyholderReference.TableRowID = paymentDataRow.TableRowID		AND tdvPolicyholderReference.DetailFieldID		= 154511 /*Policyholder Reference*/
						LEFT JOIN  TableDetailValues tdvPolicyCode					WITH (NOLOCK) ON tdvPolicyCode.TableRowID = paymentDataRow.TableRowID					AND tdvPolicyCode.DetailFieldID					= 154512 /*Policy Code*/
						LEFT JOIN  TableDetailValues tdvClaimCode					WITH (NOLOCK) ON tdvClaimCode.TableRowID = paymentDataRow.TableRowID					AND tdvClaimCode.DetailFieldID					= 154513 /*Claim Code*/
						LEFT JOIN  TableDetailValues tdvPolicyholderType			WITH (NOLOCK) ON tdvPolicyholderType.TableRowID = paymentDataRow.TableRowID				AND tdvPolicyholderType.DetailFieldID			= 154514 /*Policyholder Type*/
						LEFT JOIN  TableDetailValues tdvPaymentType					WITH (NOLOCK) ON tdvPaymentType.TableRowID = paymentDataRow.TableRowID					AND tdvPaymentType.DetailFieldID				= 154517 /*Payment Type*/
						LEFT JOIN  TableDetailValues tdvPaymentAmount				WITH (NOLOCK) ON tdvPaymentAmount.TableRowID = paymentDataRow.TableRowID				AND tdvPaymentAmount.DetailFieldID				= 154518 /*Payment Amount*/
						LEFT JOIN  TableDetailValues tdvPaymentDate					WITH (NOLOCK) ON tdvPaymentDate.TableRowID = paymentDataRow.TableRowID					AND tdvPaymentDate.DetailFieldID				= 154520 /*Payment Date*/
						LEFT JOIN  TableDetailValues tdvDateCreated					WITH (NOLOCK) ON tdvDateCreated.TableRowID = paymentDataRow.TableRowID					AND tdvDateCreated.DetailFieldID				= 158479 /*Date Created*/
						LEFT JOIN  TableDetailValues tdvPetName						WITH (NOLOCK) ON tdvPetName.TableRowID = paymentDataRow.TableRowID						AND tdvPetName.DetailFieldID					= 159005 /*Pet Name*/
						LEFT JOIN  TableDetailValues tdvPaymentApproved				WITH (NOLOCK) ON tdvPaymentApproved.TableRowID = paymentDataRow.TableRowID				AND tdvPaymentApproved.DetailFieldID			= 159407 /*Payment Approved*/
						LEFT JOIN  TableDetailValues tdvPayTo						WITH (NOLOCK) ON tdvPayTo.TableRowID = paymentDataRow.TableRowID						AND tdvPayTo.DetailFieldID						= 159408 /*Pay To*/
						INNER JOIN LookupListItems lliPayTo							WITH (NOLOCK) ON lliPayTo.LookupListItemID = tdvPayTo.ValueInt							/*PayTo*/
						LEFT JOIN  TableDetailValues tdvAffinityPartnerName			WITH (NOLOCK) ON tdvAffinityPartnerName.TableRowID = paymentDataRow.TableRowID			AND tdvAffinityPartnerName.DetailFieldID		= 161802 /*Affinity Partner Name*/
						LEFT JOIN  TableDetailValues tdvValidFromDate				WITH (NOLOCK) ON tdvValidFromDate.TableRowID = paymentDataRow.TableRowID				AND tdvValidFromDate.DetailFieldID				= 161803 /*Valid From Date*/
						LEFT JOIN  TableDetailValues tdvPaymentMethod				WITH (NOLOCK) ON tdvPaymentMethod.TableRowID = paymentDataRow.TableRowID				AND tdvPaymentMethod.DetailFieldID				= 170232 /*Payment method*/
						LEFT JOIN  TableDetailValues tdvAccountNumber				WITH (NOLOCK) ON tdvAccountNumber.TableRowID = paymentDataRow.TableRowID				AND tdvAccountNumber.DetailFieldID				= 175270 /*Account Number*/
						LEFT JOIN  TableDetailValues tdvSortCode					WITH (NOLOCK) ON tdvSortCode.TableRowID = paymentDataRow.TableRowID						AND tdvSortCode.DetailFieldID					= 175271 /*Sort Code*/
						WHERE paymentDataRow.MatterID = m.MatterID 
						AND paymentDataRow.DetailFieldID = 154485 /*Payment Data TableRowID*/
						FOR JSON PATH
					) AS payments
			FROM Customers cu								WITH (NOLOCK)
			INNER JOIN Lead l								WITH (NOLOCK) ON l.CustomerID = cu.CustomerID
			INNER JOIN Cases c								WITH (NOLOCK) ON c.LeadID = l.LeadID
			INNER JOIN Matter m								WITH (NOLOCK) ON m.CaseID = c.CaseID
			INNER JOIN LeadTypeRelationship ltr				WITH (NOLOCK) ON ltr.ToMatterID = m.MatterID						AND ToLeadTypeID				= 1490	 /*Policy MatterID*/
			INNER JOIN MatterDetailValues fnolDesc			WITH (NOLOCK) ON fnolDesc.MatterID = m.MatterID						AND fnolDesc.DetailFieldID		= 144332 /*FNOL Description*/
			INNER JOIN MatterDetailValues dol				WITH (NOLOCK) ON dol.MatterID = m.MatterID							AND dol.DetailFieldID			= 144892 /*Date of Loss*/
			INNER JOIN MatterDetailValues treatStart		WITH (NOLOCK) ON treatStart.MatterID = m.MatterID					AND treatStart.DetailFieldID	= 144366 /*Treatment Start*/
			INNER JOIN MatterDetailValues treatEnd			WITH (NOLOCK) ON treatEnd.MatterID = m.MatterID						AND treatEnd.DetailFieldID		= 145674 /*Treatment End*/
			INNER JOIN MatterDetailValues amount			WITH (NOLOCK) ON amount.MatterID = m.MatterID						AND amount.DetailFieldID		= 149850 /*Amount*/
			INNER JOIN MatterDetailValues ailment			WITH (NOLOCK) ON ailment.MatterID = m.MatterID						AND ailment.DetailFieldID		= 144504 /*Ailment*/
			INNER JOIN ResourceListDetailValues firstCause	WITH (NOLOCK) ON firstCause.ResourceListID = ailment.ValueInt		AND firstCause.DetailFieldID	= 144340 /*1st Cause*/
			INNER JOIN ResourceListDetailValues secCause	WITH (NOLOCK) ON secCause.ResourceListID = ailment.ValueInt			AND secCause.DetailFieldID		= 144341 /*2nd Cause*/
			LEFT JOIN LeadDetailValues currentVet			WITH (NOLOCK) ON currentVet.LeadID = ltr.FromLeadID					AND currentVet.DetailFieldID	= 146215 /*Current Vet*/
			LEFT JOIN ResourceListDetailValues pracName		WITH (NOLOCK) ON pracName.ResourceListID = currentVet.ValueInt		AND pracName.DetailFieldID		= 144473 /*Vet Name*/
			LEFT JOIN ResourceListDetailValues vetAddress1	WITH (NOLOCK) ON vetAddress1.ResourceListID = currentVet.ValueInt	AND vetAddress1.DetailFieldID	= 144475 /*Vet Address1*/
			LEFT JOIN ResourceListDetailValues vetAddress2	WITH (NOLOCK) ON vetAddress2.ResourceListID = currentVet.ValueInt	AND vetAddress2.DetailFieldID	= 144476 /*Vet Address2*/	
			LEFT JOIN ResourceListDetailValues vetTown		WITH (NOLOCK) ON vetTown.ResourceListID = currentVet.ValueInt		AND vetTown.DetailFieldID		= 144477 /*Vet Town*/					
			LEFT JOIN ResourceListDetailValues vetPostcode	WITH (NOLOCK) ON vetPostcode.ResourceListID = currentVet.ValueInt	AND vetPostcode.DetailFieldID	= 144479 /*Vet Postcode*/	
			LEFT JOIN  LeadDetailValues ldvDOD				WITH (NOLOCK) ON ldvDOD.LeadID = ltr.FromLeadID						AND ldvDOD.DetailFieldID		= 144271 /*Date of Death*/
			INNER JOIN ResourceListDetailValues conType		WITH (NOLOCK) ON conType.ResourceListID = ailment.ValueInt			AND conType.DetailFieldID		= 162655 /*Condition Type - Accident/Illness*/
			INNER JOIN LookupListItems conTypeValue			WITH (NOLOCK) ON conTypeValue.LookupListItemID = conType.ValueInt	/*Condition Type - Accident/Illness*/
			LEFT JOIN MatterDetailValues claimSettled		WITH (NOLOCK) ON claimSettled.MatterID = m.MatterID					AND claimSettled.DetailFieldID	= 162644 /*claimSettled*/
			WHERE m.MatterID = @ClaimID
			AND l.LeadTypeID = 1490 /*Claim*/
			FOR JSON PATH
		)

		/*Remove the outermost start and end square array  brackets (i.e. the first and last characters)*/
		SELECT @JsonData = SUBSTRING(@JsonData, 2, LEN(@JsonData)-2)
	END

	RETURN @JsonData

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Harvest_Claim_JSON] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_Harvest_Claim_JSON] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Harvest_Claim_JSON] TO [sp_executeall]
GO
