SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-05-25
-- Description:	Adapted from CustomTableSQL
-- 2017-08-18 CPS look for PremiumLessDiscount
-- =============================================


CREATE FUNCTION [dbo].[fn_C433_MtaSummaryDisplay]
(
	@MatterID	INT
)
RETURNS 
@Display TABLE
(
	 Premium VARCHAR(2000)
	,AnnualAmount DECIMAL(18,2)
	,FirstMonth DECIMAL(18,2)
	,OtherMonth DECIMAL(18,2)
	,AdjustmentType VARCHAR(2000)
	,AdjustmentAmount DECIMAL(18,2)
)	
AS
BEGIN

	DECLARE  @PreMtaValue DECIMAL(18,2)
			,@PostMtaValue DECIMAL(18,2)

	INSERT @Display ( Premium, AnnualAmount, FirstMonth, OtherMonth, AdjustmentType, AdjustmentAmount )
	/*TableRows*/
	SELECT  'Pre MTA'			[Premium]
			,ann.ValueMoney		[Annual Amount]
			,NULL				[First Month Amount]
			,CASE WHEN pp.PaymentFrequencyID=5 THEN NULL ELSE mo.ValueMoney END [Other Month Amount]
			,''					[Adjustment Type]
			,NULL				[Adjustment Amount]
	FROM TableDetailValues stat WITH (NOLOCK) 
	INNER JOIN TableDetailValues mo WITH (NOLOCK) ON stat.TableRowID=mo.TableRowID AND mo.DetailFieldID=175351		/*Other Month*/
	INNER JOIN TableDetailValues mf WITH (NOLOCK) ON stat.TableRowID=mf.TableRowID AND mf.DetailFieldID=175350		/*First Month*/
	INNER JOIN TableDetailValues ann WITH (NOLOCK) ON stat.TableRowID=ann.TableRowID AND ann.DetailFieldID=177899	/*Premium Less Discount*/
	INNER JOIN MatterDetailValues ppdv WITH (NOLOCK) ON stat.MatterID=ppdv.MatterID AND ppdv.DetailFieldID=177074	/*Purchased Policy ID*/
	INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON pp.PurchasedProductID=ppdv.ValueInt
	WHERE stat.DetailFieldID=175722 /*Status*/
	AND stat.ValueInt=72326 /*Live*/
	AND stat.MatterID=@MatterID
	AND NOT EXISTS (
		SELECT * FROM TableDetailValues stat2 WITH (NOLOCK) 
		WHERE stat2.MatterID=stat.MatterID AND stat2.DetailFieldID=stat.DetailFieldID AND stat2.ValueInt=stat.ValueInt AND stat2.TableRowID>stat.TableRowID
		)
	
		UNION ALL
	
	SELECT 'Post MTA' as [cell_replace],ann.ValueMoney as [cell_replace], 
	CASE WHEN pp.PaymentFrequencyID=5 THEN 0.00 ELSE fm.ValueMoney END as [cell_replace], 
	CASE WHEN pp.PaymentFrequencyID=5 THEN 0.00 ELSE mo.ValueMoney END as [cell_replace], 
	atluli.ItemValue as [cell_replace], aa.ValueMoney as [cell_replace]
	FROM TableDetailValues stat WITH (NOLOCK) 
	INNER JOIN TableDetailValues mo WITH (NOLOCK) ON stat.TableRowID=mo.TableRowID AND mo.DetailFieldID=175351		/*Other Month*/
	INNER JOIN TableDetailValues mf WITH (NOLOCK) ON stat.TableRowID=mf.TableRowID AND mf.DetailFieldID=175350		/*First Month*/
	INNER JOIN TableDetailValues ann WITH (NOLOCK) ON stat.TableRowID=ann.TableRowID AND ann.DetailFieldID=177899	/*Premium Less Discount*/
	INNER JOIN MatterDetailValues at WITH (NOLOCK) ON stat.MatterID=at.MatterID AND at.DetailFieldID=175345			/*Adjustment Type*/
	INNER JOIN LookupListItems atluli WITH (NOLOCK) ON at.ValueInt=atluli.LookupListItemID
	INNER JOIN MatterDetailValues fm WITH (NOLOCK) ON stat.MatterID=fm.MatterID AND fm.DetailFieldID=177405			/*First month following MTA total*/
	INNER JOIN MatterDetailValues ppdv WITH (NOLOCK) ON stat.MatterID=ppdv.MatterID AND ppdv.DetailFieldID=177074	/*Purchased Policy ID*/
	INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON pp.PurchasedProductID=ppdv.ValueInt
	INNER JOIN MatterDetailValues aa WITH (NOLOCK) ON stat.MatterID=aa.MatterID AND aa.DetailFieldID=175379 /*Adjustment Value  (for adjustment cover period)*/
	WHERE stat.DetailFieldID=175722	/*Status*/
	AND stat.ValueInt=74514 /*MTA*/
	AND stat.MatterID=@MatterID
	AND NOT EXISTS (
		SELECT * FROM TableDetailValues stat2 WITH (NOLOCK) 
		WHERE stat2.MatterID=stat.MatterID AND stat2.DetailFieldID=stat.DetailFieldID AND stat2.ValueInt=stat.ValueInt AND stat2.TableRowID>stat.TableRowID
	)

	SELECT @PreMtaValue = d.AnnualAmount
	FROM @Display d 
	WHERE d.Premium = 'Pre MTA'
	
	SELECT @PostMtaValue = d.AnnualAmount
	FROM @Display d 
	WHERE d.Premium = 'Post MTA'
	
	UPDATE d 
	SET AdjustmentType = CASE	WHEN @PostMtaValue > @PreMtaValue THEN 'Increase'
								WHEN @PostMtaValue < @PreMtaValue THEN 'Decrease'
								ELSE 'No Change'
						 END
	FROM @Display d 
	WHERE d.Premium = 'Post MTA'
	
	UPDATE d 
	SET AdjustmentType = CASE	WHEN @PostMtaValue > @PreMtaValue THEN 'Increase'
								WHEN @PostMtaValue < @PreMtaValue THEN 'Decrease'
								ELSE 'No Change'
						 END
	FROM @Display d 
	WHERE d.Premium = 'Post MTA'

	/*Add in a line break*/
	INSERT @Display ( AdjustmentAmount, AdjustmentType, AnnualAmount, FirstMonth, OtherMonth, Premium )
	VALUES ( NULL, NULL, NULL, NULL, NULL, NULL )
	
	RETURN

END










GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C433_MtaSummaryDisplay] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C433_MtaSummaryDisplay] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C433_MtaSummaryDisplay] TO [sp_executeall]
GO
