SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2012-01-12
-- Description:	Check to see if a varchar contains a valid numeric value given the precision and scale provided.
--              If not valid, return the default value passed in.
--				Remove commas and £ sign before testing, and allow the user to 
-- =============================================
CREATE FUNCTION [dbo].[fnGetNumericToNDP] 
(
	@Value VARCHAR(2000),
	@Precision INT,
	@Scale INT,
	@TruncateAfterScale BIT,
	@DefaultReturnValue NUMERIC(38, 18)
)
RETURNS NUMERIC(38, 18)
AS
BEGIN
	
	DECLARE @DotPos INT = 0
	
	/*
		To give this every chance of being converted successfully, we prepare the input string as follows:
		
		Remove "£"
		Remove ","
		Remove "-"
		Remove leading and trailing spaces
	*/
	SET @Value = REPLACE(REPLACE(REPLACE(LTRIM(RTRIM(@Value)), '£', ''), ',', ''), '-', '')
	
	/* Find the first decimal point (if any) */
	SET @DotPos = CHARINDEX('.', @Value)
	
	
	/* If we are only testing the first 10 characters after the decimal point, discard the rest */
	IF @TruncateAfterScale = 1 AND @DotPos > 0
	BEGIN
		/* Take everything before the decimal point, then add the '.' and then add the next N characters after the decimal point from the original value passed in. */
		SET @Value = LEFT(@Value, (@DotPos-1) ) + '.' + LEFT(dbo.fnGetCharsBeforeOrAfterSeparator(@Value, '.', 1, 0), @Scale)
		/* 
			Example: 
			
			@Value = '1234.5678UnwantedData',
			@Precision = 8,
			@Scale = 4,
			@TruncateAfterScale = 1,
			@DefaultReturnValue = NULL
			
			Left part = '1234'
			Right part = '5678UnwantedData' initially, which comes back as just '5678'
			New value = '1234.5678'
		*/
		
	END
	
	RETURN CASE
			/* All invalid values return the default value passed in to this function. */
			
			/* NULL */
			WHEN @Value IS NULL THEN @DefaultReturnValue
			
			/* Empty string */
			WHEN @Value = '' THEN @DefaultReturnValue
			
			/* Too long */
			WHEN DATALENGTH(@Value) > @Precision THEN @DefaultReturnValue
			
			/* Fails Sql Servers own numeric test */
			WHEN ISNUMERIC(@Value) = 0 THEN @DefaultReturnValue
			
			/* Contains invalid characters */
			WHEN PATINDEX('%[^-£$.0-9]%', @Value) > 0 THEN @DefaultReturnValue
			
			/* Valid!  Cast the return value as a really precise numeric (dynamic paramaters are not allowed!) */
			ELSE CAST(@Value AS NUMERIC(38, 18))
			
			END
			
END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetNumericToNDP] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetNumericToNDP] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetNumericToNDP] TO [sp_executeall]
GO
