SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire
-- Create date: 2020-10-06
-- Description:	harvest Exam Fee product 
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Mediate_Harvest_ExamFee]
(
	 @PolicyID		INT
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE  @JsonData	VARCHAR(MAX) = ''

	DECLARE @Products Table (Product VARCHAR(3), MatterID INT, StartDate DATETIME, EndDate DATETIME, InceptionDate DATETIME)

	INSERT INTO @Products
	SELECT TOP 1  tdv.DetailValue, tr1.MatterID, tdv1.ValueDateTime, tdv2.ValueDateTime, tdv3.ValueDateTime
	FROM TableRows tr1 WITH (NOLOCK) 
	INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr1.TableRowID AND tdv.DetailFieldID = 314011
	INNER JOIN TableDetailValues tdv1 WITH (NOLOCK) ON tdv1.TableRowID = tdv.TableRowID AND tdv1.DetailFieldID = 145663
	INNER JOIN TableDetailValues tdv2 WITH (NOLOCK) ON tdv2.TableRowID = tdv.TableRowID AND tdv2.DetailFieldID = 145664
	INNER JOIN TableDetailValues tdv3 WITH (NOLOCK) ON tdv3.TableRowID = tdv.TableRowID AND tdv3.DetailFieldID = 145662
	WHERE 
	tr1.DetailFieldID = 170033
	AND tr1.MatterID = @PolicyID	
	ORDER BY tr1.TableRowID DESC

	IF @PolicyID <> 0
	BEGIN
		SELECT  @JsonData +=
		(SELECT	
						(SELECT rldv.DetailValue FROM ResourceListDetailValues rldv WITH (NOLOCK) 
						WHERE rldv.ResourceListID = 2002260 AND rldv.DetailFieldID = 314268)								[productId]
						,mdvStartDate.ValueDateTime																			[startDate]
						,CASE WHEN p.Product = 'Yes' THEN mdvEndDate.ValueDateTime ELSE mdvInceptionDate.ValueDateTime END	[endDate]
						FROM 
						@Products p 
						JOIN MatterDetailValues mdvProd WITH (NOLOCK) ON mdvProd.MatterID = p.MatterID
						JOIN MatterDetailValues mdvInceptionDate WITH (NOLOCK) ON mdvInceptionDate.MatterID = p.MatterID AND mdvInceptionDate.DetailFieldID = 170035
						JOIN MatterDetailValues mdvStartDate WITH (NOLOCK) ON mdvStartDate.MatterID = p.MatterID AND mdvStartDate.DetailFieldID = 170036
						JOIN MatterDetailValues mdvEndDate WITH (NOLOCK) ON mdvEndDate.MatterID = p.MatterID AND mdvEndDate.DetailFieldID = 170037
						JOIN OneVision oneVision		WITH (NOLOCK) ON oneVision.PAMatterID = mdvProd.MatterID AND oneVision.PolicyTermType = 'ExamFee' /*One Vision Table*/
						WHERE p.MatterID = @PolicyID
						AND mdvProd.DetailFieldID = 170034
						FOR JSON PATH, WITHOUT_ARRAY_WRAPPER

					  )

	END

	RETURN @JsonData

END

GO
