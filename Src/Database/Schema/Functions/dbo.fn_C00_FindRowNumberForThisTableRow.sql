SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-12-23
-- Description:	Take a table row ID and return a Row Number.  For AoH
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_FindRowNumberForThisTableRow]
(
@TableRowID	INT
)
RETURNS INT
AS
BEGIN

	DECLARE	 @DetailFieldID INT
			,@ObjectID		INT
			,@LeadOrMatter	INT
			,@ReturnValue	INT
			
	SELECT	 @DetailFieldID = tr.DetailFieldID
			,@LeadOrMatter	= df.LeadOrMatter
			,@ObjectID		= CASE df.LeadOrMatter 
								WHEN 1  THEN tr.LeadID
								WHEN 2  THEN tr.MatterID
								WHEN 10 THEN tr.CustomerID
								WHEN 11 THEN tr.CaseID
								WHEN 12 THEN tr.ClientID
								WHEN 13 THEN tr.ClientPersonnelID
							  END
	FROM TableRows tr WITH ( NOLOCK ) 
	INNER JOIN DetailFields df WITH (NOLOCK) on df.DetailFieldID = tr.DetailFieldID
	WHERE tr.TableRowID = @TableRowID

	;WITH Numbered AS
	(
	SELECT tr.TableRowID, ROW_NUMBER() OVER (ORDER BY tr.TableRowID) [Row]
	FROM TableRows tr WITH ( NOLOCK ) 
	WHERE tr.DetailFieldID = @DetailFieldID 
	AND (
			( @LeadOrMatter = 1  AND tr.LeadID = @ObjectID )
			OR
			( @LeadOrMatter = 2  AND tr.MatterID = @ObjectID )
			OR
			( @LeadOrMatter = 10 AND tr.CustomerID = @ObjectID )
			OR
			( @LeadOrMatter = 11 AND tr.CaseID = @ObjectID )
			OR
			( @LeadOrMatter = 12 AND tr.ClientID = @ObjectID )
			OR
			( @LeadOrMatter = 13 AND tr.ClientPersonnelID = @ObjectID )
		)
	)
	SELECT @ReturnValue = n.Row
	FROM Numbered n 
	WHERE n.TableRowID = @TableRowID 

	SELECT @ReturnValue = ISNULL(@ReturnValue,0)
	
	RETURN @ReturnValue

END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_FindRowNumberForThisTableRow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_FindRowNumberForThisTableRow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_FindRowNumberForThisTableRow] TO [sp_executeall]
GO
