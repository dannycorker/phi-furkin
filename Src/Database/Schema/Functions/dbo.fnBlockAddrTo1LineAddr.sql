SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 30-11-2012
-- Description:	Converts Carriage Return + Line Feed to ', '
--              e.g. to convert block address to single line 
-- =============================================
CREATE FUNCTION [dbo].[fnBlockAddrTo1LineAddr]
(
	@LeadOrMatterID INT,
	@DFID INT,
	@SubDFID INT
)
RETURNS varchar(2000)
AS
BEGIN
	
		DECLARE @LeadOrMatter INT,
				@Addr1Line VARCHAR (2000)

		SELECT @LeadOrMatter=LeadOrMatter FROM DetailFields WITH (NOLOCK) 
		WHERE DetailFieldID=@DFID
		
		IF @LeadOrMatter = 1 -- Lead DF
		BEGIN
		
			IF @SubDFID <> 0
			BEGIN
				-- RLDV
				SELECT @Addr1Line=REPLACE(REPLACE(REPLACE(rldv.DetailValue, CHAR(13), ''), CHAR(10), ', '),',,',',')
				FROM ResourceListDetailValues rldv WITH (NOLOCK) 
				INNER JOIN LeadDetailValues dv WITH (NOLOCK) ON dv.ValueInt=rldv.ResourceListID 
					AND dv.DetailFieldID=@DFID 
					AND dv.LeadID=@LeadOrMatterID
				WHERE rldv.DetailFieldID=@SubDFID
			END
			ELSE
			BEGIN
				-- regular DV
				SELECT @Addr1Line=REPLACE(REPLACE(REPLACE(DetailValue, CHAR(13), ''), CHAR(10), ', '),',,',',')
				FROM LeadDetailValues WITH (NOLOCK) 
				WHERE DetailFieldID=@DFID AND LeadID=@LeadOrMatterID
			END
					
		END
		
		IF @LeadOrMatter = 11 -- Case DF
		BEGIN
		
			IF @SubDFID <> 0
			BEGIN
				-- RLDV
				SELECT @Addr1Line=REPLACE(REPLACE(REPLACE(rldv.DetailValue, CHAR(13), ''), CHAR(10), ', '),',,',',')
				FROM ResourceListDetailValues rldv WITH (NOLOCK) 
				INNER JOIN CaseDetailValues dv WITH (NOLOCK) ON dv.ValueInt=rldv.ResourceListID 
					AND dv.DetailFieldID=@DFID 
					AND dv.CaseID=@LeadOrMatterID
				WHERE rldv.DetailFieldID=@SubDFID
			END
			ELSE
			BEGIN
				-- regular DV
				SELECT @Addr1Line=REPLACE(REPLACE(REPLACE(DetailValue, CHAR(13), ''), CHAR(10), ', '),',,',',')
				FROM CaseDetailValues WITH (NOLOCK) 
				WHERE DetailFieldID=@DFID AND CaseID=@LeadOrMatterID
			END
					
		END
		
		IF @LeadOrMatter = 2 -- Matter DF
		BEGIN
		
			IF @SubDFID <> 0
			BEGIN
				-- RLDV
				SELECT @Addr1Line=REPLACE(REPLACE(REPLACE(rldv.DetailValue, CHAR(13), ''), CHAR(10), ', '),',,',',')
				FROM ResourceListDetailValues rldv WITH (NOLOCK) 
				INNER JOIN MatterDetailValues dv WITH (NOLOCK) ON dv.ValueInt=rldv.ResourceListID 
					AND dv.DetailFieldID=@DFID 
					AND dv.MatterID=@LeadOrMatterID
				WHERE rldv.DetailFieldID=@SubDFID
			END
			ELSE
			BEGIN
				-- regular DV
				SELECT @Addr1Line=REPLACE(REPLACE(REPLACE(DetailValue, CHAR(13), ''), CHAR(10), ', '),',,',',')
				FROM MatterDetailValues WITH (NOLOCK) 
				WHERE DetailFieldID=@DFID AND MatterID=@LeadOrMatterID
			END
					
		END
		
		IF @LeadOrMatter = 10 -- Customer DF
		BEGIN
		
			IF @SubDFID <> 0
			BEGIN
				-- RLDV
				SELECT @Addr1Line=REPLACE(REPLACE(REPLACE(rldv.DetailValue, CHAR(13), ''), CHAR(10), ', '),',,',',')
				FROM ResourceListDetailValues rldv WITH (NOLOCK) 
				INNER JOIN CustomerDetailValues dv WITH (NOLOCK) ON dv.ValueInt=rldv.ResourceListID 
					AND dv.DetailFieldID=@DFID 
					AND dv.CustomerID=@LeadOrMatterID
				WHERE rldv.DetailFieldID=@SubDFID
			END
			ELSE
			BEGIN
				-- regular DV
				SELECT @Addr1Line=REPLACE(REPLACE(REPLACE(DetailValue, CHAR(13), ''), CHAR(10), ', '),',,',',')
				FROM CustomerDetailValues WITH (NOLOCK) 
				WHERE DetailFieldID=@DFID AND CustomerID=@LeadOrMatterID
			END
					
		END
		
		RETURN @Addr1Line

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnBlockAddrTo1LineAddr] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnBlockAddrTo1LineAddr] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnBlockAddrTo1LineAddr] TO [sp_executeall]
GO
