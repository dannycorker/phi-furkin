SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2014-02-24
-- Description:	Count instances in a list of column fields in a particular table field
-- =============================================
CREATE FUNCTION [dbo].[fnCountValuesByTableFieldColumnFieldsAndObjectID]
(
	@ObjectID	int,
	@TableField	int ,
	@SearchValue varchar(max),
	@ColumnFields	varchar(max)
)
RETURNS INT 
AS
BEGIN

	IF @ColumnFields = ''
	BEGIN
		
		DECLARE @Table AS TABLE (DetailFieldID INT)
		INSERT INTO @Table (DetailFieldID)
		SELECT dc.DetailFieldID 
		FROM DetailFields dt WITH (NOLOCK) 
		INNER JOIN DetailFields dc WITH (NOLOCK) on dt.TableDetailFieldPageID = dc.DetailFieldPageID
		WHERE dt.DetailFieldID = @TableField
				
		SELECT @ColumnFields = STUFF(
			  (           
					SELECT ',' + CAST(DetailFieldID AS VARCHAR(MAX))
					FROM @Table p 
					FOR XML PATH(''), TYPE
			  ).value('(./text())[1]', 'VARCHAR(MAX)'), 1, 1, '')
		
	END

	DECLARE	@LeadOrMatter int,
			@ReturnValue int = 0,
			@ClientID int
	
	SELECT @LeadOrMatter = df.LeadOrMatter, @ClientID = df.ClientID
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @TableField
		
	IF @LeadOrMatter = 1
	BEGIN
		SELECT @ReturnValue = COUNT(tr.TableRowID)
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
		INNER JOIN fnTableOfIDsFromCSV(@ColumnFields) f on f.anyid = tdv.detailfieldid
		WHERE tr.DetailFieldID = @TableField
		and	( tdv.DetailValue = (ISNULL(@SearchValue,'')) )
		and tr.LeadID = @ObjectID
		and tr.ClientID = @ClientID
	END
	ELSE IF @LeadOrMatter = 2
	BEGIN
		SELECT @ReturnValue = COUNT(tr.TableRowID)
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
		INNER JOIN fnTableOfIDsFromCSV(@ColumnFields) f on f.anyid = tdv.detailfieldid
		WHERE tr.DetailFieldID = @TableField
		and	( tdv.DetailValue = ISNULL(@SearchValue,'') )
		and	tr.MatterID = @ObjectID
		and tr.ClientID = @ClientID
		
	END
	ELSE IF @LeadOrMatter = 10
	BEGIN
		SELECT @ReturnValue = COUNT(tr.TableRowID)
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
		INNER JOIN fnTableOfIDsFromCSV(@ColumnFields) f on f.anyid = tdv.detailfieldid
		WHERE tr.DetailFieldID = @TableField
		and	( tdv.DetailValue = ISNULL(@SearchValue,'') )
		and	tr.CustomerID = @ObjectID
		and tr.ClientID = @ClientID
		
	END
	ELSE IF @LeadOrMatter = 11
	BEGIN
		SELECT @ReturnValue = COUNT(tr.TableRowID)
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
		INNER JOIN fnTableOfIDsFromCSV(@ColumnFields) f on f.anyid = tdv.detailfieldid
		WHERE tr.DetailFieldID = @TableField
		and	( tdv.DetailValue = ISNULL(@SearchValue,'') )
		and	tr.CaseID = @ObjectID
		and tr.ClientID = @ClientID
		
	END	

	RETURN @ReturnValue

END






GO
GRANT VIEW DEFINITION ON  [dbo].[fnCountValuesByTableFieldColumnFieldsAndObjectID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnCountValuesByTableFieldColumnFieldsAndObjectID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnCountValuesByTableFieldColumnFieldsAndObjectID] TO [sp_executeall]
GO
