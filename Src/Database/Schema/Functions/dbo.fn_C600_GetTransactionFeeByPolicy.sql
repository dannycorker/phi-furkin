SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-09-19
-- Description:	Returns sum of TransactionFee from PurchasedProductPaymentSchedule by Policy
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetTransactionFeeByPolicy]
(
	@MatterID INT
)
RETURNS MONEY
AS
BEGIN

	DECLARE @ClientID INT
	DECLARE @TransactionFee MONEY

	SELECT @TransactionFee = SUM(ppps.TransactionFee)
	FROM Matter m WITH (NOLOCK)
	INNER JOIN PurchasedProduct pp with (NOLOCK) ON pp.ObjectID = m.MatterID
	INNER JOIN PurchasedProductPaymentSchedule ppps WITH (NOLOCK) ON ppps.PurchasedProductID = pp.PurchasedProductID
	WHERE m.MatterID = @MatterID

	RETURN @TransactionFee

END
GO
