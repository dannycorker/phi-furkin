SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- ALTER date: 2017-08-21
-- Description:	Get a full list of users with the database that each exists on
-- 2017-10-17 CPS union to the Live Db
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================

CREATE FUNCTION [dbo].[fn_C600_GetUserListWithDatabases]
(
)
RETURNS 
@AllUsers TABLE
(
	 ClientPersonnelID	INT
	,EmailAddress		VARCHAR(2000)
	,Live				BIT
	,QA					BIT
	,Dev				BIT
)	
AS
BEGIN
	
	DECLARE @ClientID	INT = dbo.fnGetPrimaryClientID()

	INSERT @AllUsers ( ClientPersonnelID, EmailAddress, Live, QA, Dev )
	SELECT cp.ClientPersonnelID, cp.EmailAddress, 0, 0, 0
	FROM AquariusMaster.dbo.ClientPersonnel cp WITH ( NOLOCK ) 
	WHERE cp.ClientID = @ClientID
	--	UNION
	--SELECT cp.ClientPersonnelID, cp.EmailAddress, 0, 0, 0
	--FROM [878574-SQLCLUS1\CPSQL1].AquariusMaster.dbo.ClientPersonnel cp WITH ( NOLOCK ) 
	--WHERE cp.ClientID = @ClientID


	IF EXISTS ( SELECT * FROM sys.databases sd WITH ( NOLOCK ) WHERE sd.name = 'Aquarius603Dev' )
	BEGIN
		UPDATE @AllUsers
		SET Dev = 1
		FROM @AllUsers au
		INNER JOIN Aquarius603Dev.dbo.ClientPersonnel cp WITH ( NOLOCK ) on au.ClientPersonnelID = cp.ClientPersonnelID
	END
	ELSE
	BEGIN
		UPDATE @AllUsers SET Live = NULL FROM @AllUsers
	END

	IF EXISTS ( SELECT * FROM sys.databases sd WITH ( NOLOCK ) WHERE sd.name = 'Aquarius603Qa' )
	BEGIN
		UPDATE @AllUsers
		SET QA = 1
		FROM @AllUsers au
		INNER JOIN Aquarius603Qa.dbo.ClientPersonnel cp WITH ( NOLOCK ) on au.ClientPersonnelID = cp.ClientPersonnelID
	END
	ELSE
	BEGIN
		UPDATE @AllUsers SET Live = NULL FROM @AllUsers
	END

	--IF EXISTS ( SELECT * FROM sys.servers sd WITH ( NOLOCK ) WHERE sd.name = '878574-SQLCLUS1\CPSQL1' )
	--BEGIN
	--	UPDATE @AllUsers
	--	SET Live = 1
	--	FROM @AllUsers au
	--	INNER JOIN [878574-SQLCLUS1\CPSQL1].Aquarius600.dbo.ClientPersonnel cp WITH ( NOLOCK ) on au.ClientPersonnelID = cp.ClientPersonnelID
	--END
	--ELSE
	--BEGIN
	--	UPDATE @AllUsers SET Live = NULL FROM @AllUsers
	--END
		
	RETURN

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetUserListWithDatabases] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_GetUserListWithDatabases] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetUserListWithDatabases] TO [sp_executeall]
GO
