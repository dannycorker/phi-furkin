SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2012-12-16
-- Description:	Calculate age in months from a given date.
--              Sounds obvious, but leap years need attention.
-- =============================================
CREATE FUNCTION [dbo].[fnAgeFromDateInMonths] 
(
	@startdate datetime
)
RETURNS int
AS
BEGIN

	-- Return datediff in years, plus/minus 1 extra year to cater for these scenarios:
	-- A) From 1st Jan 2008 to 15th July 2008 is 0 years
	-- B) From 1st Jan 2008 to 1st Jan 2009 is 1 year
	return datediff(month,@startdate,dbo.fn_GetDate_Local()) -	case 
													when dbo.fn_GetDate_Local() < dateadd(month,datediff(month,@startdate,dbo.fn_GetDate_Local()), @startdate) then 1 
													else 0 
												end
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnAgeFromDateInMonths] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnAgeFromDateInMonths] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnAgeFromDateInMonths] TO [sp_executeall]
GO
