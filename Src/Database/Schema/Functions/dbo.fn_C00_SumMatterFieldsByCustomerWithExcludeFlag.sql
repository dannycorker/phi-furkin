SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2012-09-06
-- Description:	Sum Matter Values by Customer including 
--              values where the include detail field is not set to AQ No (5145)
--              i.e. default is to include
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_SumMatterFieldsByCustomerWithExcludeFlag]
(
@CustomerID int,
@DetailFieldID int,
@ExcludeDetailFieldID int
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE @ReturnValue VARCHAR(2000)

	
	SELECT @ReturnValue = SUM(mdv.ValueMoney)
	FROM MatterDetailValues mdv WITH (NOLOCK)
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND m.CustomerID = @CustomerID
	INNER JOIN MatterDetailValues excl WITH (NOLOCK) ON m.MatterID=excl.MatterID 
		AND excl.DetailFieldID=@ExcludeDetailFieldID 
		AND excl.ValueInt=0
	WHERE mdv.DetailFieldID = @DetailFieldID 

	RETURN @ReturnValue

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumMatterFieldsByCustomerWithExcludeFlag] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_SumMatterFieldsByCustomerWithExcludeFlag] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumMatterFieldsByCustomerWithExcludeFlag] TO [sp_executeall]
GO
