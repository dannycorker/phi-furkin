SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-09-23
-- Description:	Returns the policy matter from a claim
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetPolicyMatterFromClaimMatter]
(
	@ClaimMatter INT
)
RETURNS INT	
AS
BEGIN

	DECLARE @ClaimLead INT,
			@PolicyLead INT,
			@PolicyMatter INT
			
	SELECT @ClaimLead = LeadID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE MatterID = @ClaimMatter
	
	SELECT @PolicyLead = dbo.fn_C00_1272_GetPolicyLeadFromClaimLead(@ClaimLead)
	
	-- TODO: This will just get the last MatterID but we might need to handle wellness or renewals as different matters
	SELECT TOP 1 @PolicyMatter = MatterID
	FROM dbo.Matter WITH (NOLOCK) 
	WHERE LeadID = @PolicyLead
	ORDER BY MatterID DESC	
	
	RETURN @PolicyMatter

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicyMatterFromClaimMatter] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_GetPolicyMatterFromClaimMatter] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicyMatterFromClaimMatter] TO [sp_executeall]
GO
