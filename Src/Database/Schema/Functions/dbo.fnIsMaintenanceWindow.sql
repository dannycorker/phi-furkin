SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2011-07-15
-- Description:	Tell stored procs when not to run because it is maintenance time
-- =============================================
CREATE FUNCTION [dbo].[fnIsMaintenanceWindow]
(
	@TestDateTime DATETIME = NULL
)
RETURNS BIT
AS
BEGIN

	DECLARE @IsMaintenanceWindow BIT = 0
	
	/* Use test variable if passed in, otherwise use current datetime */
	IF @TestDateTime IS NULL
	BEGIN
		SET @TestDateTime = dbo.fn_GetDate_Local()
	END
	
	/* Decide whether or not we are in the maintenance window right now */
	IF (	
			/* Windows updates and Aquarium work from 22:57 to 23:15 */
			DATEPART(HOUR, @TestDateTime) = 22 AND DATEPART(MINUTE, @TestDateTime) BETWEEN 57 AND 59
			OR
			DATEPART(HOUR, @TestDateTime) = 23 AND DATEPART(MINUTE, @TestDateTime) BETWEEN 00 AND 10
			OR
			/* Housekeeping from 23:57 to 00:45 */
			DATEPART(HOUR, @TestDateTime) = 23 AND DATEPART(MINUTE, @TestDateTime) BETWEEN 57 AND 59
			OR
			DATEPART(HOUR, @TestDateTime) = 00 AND DATEPART(MINUTE, @TestDateTime) BETWEEN 00 AND 45
	)
	BEGIN
		SET @IsMaintenanceWindow = 1
	END
	
	RETURN @IsMaintenanceWindow

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsMaintenanceWindow] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnIsMaintenanceWindow] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsMaintenanceWindow] TO [sp_executeall]
GO
GRANT EXECUTE ON  [dbo].[fnIsMaintenanceWindow] TO [sp_executehelper]
GO
