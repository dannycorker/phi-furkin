SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		James Lewis
-- Create date: 2016-08-09
-- Description:	Returns a comma seperated list of luli items
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_CommaSeparateLuliItems]
(
	@LookuplistID INT
)
RETURNS VARCHAR(MAX)	
AS
BEGIN

	DECLARE @VARCHAR VARCHAR (MAX) = ''

	SELECT @VARCHAR += l.ItemValue + ', '
	FROM LookupListItems l WITH (NOLOCK) where l.LookupListID = @LookuplistID

	RETURN LEFT(@VARCHAR,LEN(@VARCHAR) -1) 
	
 
END
 
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_CommaSeparateLuliItems] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_CommaSeparateLuliItems] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_CommaSeparateLuliItems] TO [sp_executeall]
GO
