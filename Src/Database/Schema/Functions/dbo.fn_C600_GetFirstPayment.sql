SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		James Lewis
-- Create date: 2018-02-08
-- Description:	Returns the first acceptable payment date for a BACs payer
-- Mods
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetFirstPayment]
(
	@PolicyStart DATE
)
RETURNS DATE	
AS
BEGIN

	DECLARE @PaymentDate DATE = '2018-02-10'

	RETURN @PaymentDate

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetFirstPayment] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetFirstPayment] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetFirstPayment] TO [sp_executeall]
GO
