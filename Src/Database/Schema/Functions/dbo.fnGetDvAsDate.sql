SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 14-July-2009
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[fnGetDvAsDate]
(
	@DetailFieldID		int,
	@CaseID				int
)
RETURNS date
AS
BEGIN

DECLARE @DetailValue as varchar(2000),
		@LeadOrMatter int

	Select @LeadOrMatter = LeadOrmatter
	From DetailFields with (nolock) 
	Where DetailFieldID = @DetailFieldID

	If @LeadOrMatter = 1
	Begin

		Select @DetailValue = ldv.ValueDate 
		From Cases c with (nolock) 
		Inner Join LeadDetailValues ldv with (nolock) on c.LeadID = ldv.LeadID and ldv.DetailFieldID = @DetailFieldID
		Where c.CaseID = @CaseID 
	
	End

	If @LeadOrMatter = 2
	Begin

		Select @DetailValue = mdv.ValueDate 
		From Matter m with (nolock) 
		Inner Join MatterDetailValues mdv with (nolock) on m.MatterID = mdv.MatterID and mdv.DetailFieldID = @DetailFieldID
		Where m.CaseID = @CaseID
	
	End

	If @LeadOrMatter = 10
	Begin

		Select @DetailValue = cdv.ValueDate 
		From Matter m with (nolock) 
		Inner Join CustomerDetailValues cdv with (nolock) on m.CustomerID = cdv.CustomerID and cdv.DetailFieldID = @DetailFieldID
		Where m.CaseID = @CaseID
	
	End

	If @LeadOrMatter = 11
	Begin

		Select @DetailValue = cdv.ValueDate 
		From CaseDetailValues cdv WITH (NOLOCK) 
		WHERE cdv.CaseID = @CaseID
		and cdv.DetailFieldID = @DetailFieldID
	
	End

	RETURN @DetailValue

END






GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDvAsDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetDvAsDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDvAsDate] TO [sp_executeall]
GO
