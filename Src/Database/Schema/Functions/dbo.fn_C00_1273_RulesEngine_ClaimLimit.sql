SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Chrisitan Willia,ms
-- Create date: 2018-04-23
-- Description:	Returns remaining vet fees limit for claim
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_RulesEngine_ClaimLimit]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS INT	
AS
BEGIN
 
	DECLARE @Limit Decimal (18,2)
			
	
	Select @Limit = Limit.ValueMoney from TableRows tr WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues PolicySection WITH (NOLOCK) on PolicySection.TableRowID = tr.TableRowID and PolicySection.DetailFieldID = 144350
	INNER JOIN dbo.TableDetailValues Limit WITH (NOLOCK) on Limit.TableRowID = tr.TableRowID and Limit.DetailFieldID = 179568
	Where tr.MatterID = @MatterID and tr.DetailFieldID = 144355 and PolicySection.ResourceListID = 148140/*Vet Fees*/ 
	
	Select @Limit = ISNULL(@Limit,1) /*If for whatever reason the limit is null, set to 1 so it does not think the limit has been reached*/
	
	RETURN @Limit
 
END
 





GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_ClaimLimit] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_RulesEngine_ClaimLimit] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_ClaimLimit] TO [sp_executeall]
GO
