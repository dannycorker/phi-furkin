SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2014-09-06
-- Description:	Selects a simple detail field from the appropriate detail value table
--				Copy of SB one with modifications.
--				CPS 2017-08-15 support Tables and ResourceLists
-- =============================================
CREATE FUNCTION [dbo].[fnGetSimpleDvAsDate]
(
	@DetailFieldID INT,
	@ObjectID INT
)
RETURNS DATE
AS
BEGIN

	DECLARE @DetailValue DATE,
			@LeadOrMatter INT

	SELECT @LeadOrMatter = LeadOrmatter
	FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @DetailFieldID

	IF @LeadOrMatter = 1
	BEGIN
		SELECT @DetailValue = ValueDate
		FROM LeadDetailValues WITH (NOLOCK)
		WHERE LeadID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END

	IF @LeadOrMatter = 2
	BEGIN
		SELECT @DetailValue = ValueDate
		FROM MatterDetailValues WITH (NOLOCK)
		WHERE MatterID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END

	IF @LeadOrMatter = 4 /*Resource List*/
	BEGIN
		SELECT @DetailValue = ValueDate
		FROM ResourceListDetailValues WITH (NOLOCK)
		WHERE ResourceListID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 6 /*Table*/
	BEGIN
		SELECT @DetailValue = ValueDate
		FROM TableDetailValues WITH (NOLOCK)
		WHERE TableRowID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END

	IF @LeadOrMatter = 8
	BEGIN
		SELECT @DetailValue = ValueDate
		FROM TableDetailValues WITH (NOLOCK)
		WHERE TableRowID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 10
	BEGIN
		SELECT @DetailValue = ValueDate
		FROM CustomerDetailValues WITH (NOLOCK)
		WHERE CustomerID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 11
	BEGIN
		SELECT @DetailValue = ValueDate
		FROM CaseDetailValues WITH (NOLOCK)
		WHERE CaseID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 12
	BEGIN
		SELECT @DetailValue = ValueDate
		FROM ClientDetailValues WITH (NOLOCK)
		WHERE ClientID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 13
	BEGIN
		SELECT @DetailValue = ValueDate
		FROM ClientPersonnelDetailValues WITH (NOLOCK)
		WHERE ClientPersonnelID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 14
	BEGIN
		SELECT @DetailValue = ValueDate
		FROM ContactDetailValues WITH (NOLOCK)
		WHERE ContactID = @ObjectID 
		AND DetailFieldID = @DetailFieldID
	END

	RETURN @DetailValue

END



GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleDvAsDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetSimpleDvAsDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleDvAsDate] TO [sp_executeall]
GO
