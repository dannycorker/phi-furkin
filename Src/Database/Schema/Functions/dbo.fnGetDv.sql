SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 14-July-2009
-- Description:	
-- LB 2017-05-08 Replaced SQL with specific procs as described in #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetDv]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

DECLARE @DetailValue AS VARCHAR(2000),
		@LeadOrMatter INT

	SELECT @LeadOrMatter = LeadOrmatter
	FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @DetailFieldID

	IF @LeadOrMatter = 1
	BEGIN

		SELECT @DetailValue = dbo.fnGetLeadDvCASE(@DetailFieldID,@CaseID)
	
	END

	IF @LeadOrMatter = 2
	BEGIN

		SELECT @DetailValue = dbo.fnGetMatterDvCASE(@DetailFieldID,@CaseID)
	
	END

	IF @LeadOrMatter = 10
	BEGIN

		SELECT @DetailValue = dbo.fnGetCustomerDvCASE(@DetailFieldID,@CaseID) 
	
	END
		
	IF @LeadOrMatter = 11
	BEGIN
		
		SELECT @DetailValue = dbo.fnGetCaseDvCASE(@DetailFieldID,@CaseID)
		
	END

	RETURN @DetailValue

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDv] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetDv] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetDv] TO [sp_executeall]
GO
