SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 10/03/2020
-- Description:	Converts the standard sortcode from 6 characters to a Bank/State/Branch number
--				with a hyphen in the 4th character position.
--				e.g. 013999 => 013-999
-- =============================================
CREATE FUNCTION [dbo].[fnGetAuBsb]
(
	@Sortcode VARCHAR(100)
	
)
RETURNS VARCHAR(7)
AS
BEGIN

	DECLARE @LeftPart VARCHAR(3)
	DECLARE @RightPart VARCHAR(3)
	DECLARE @Bsb VARCHAR(7)

	SELECT @Sortcode = RTRIM(LTRIM(@Sortcode))

	SELECT @LeftPart = SUBSTRING(@Sortcode, 0,4)
	SELECT @RightPart = SUBSTRING(@Sortcode, 4, 3)

	SELECT @Bsb = @LeftPart + '-' + @RightPart

	Return @Bsb

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetAuBsb] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetAuBsb] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetAuBsb] TO [sp_executeall]
GO
