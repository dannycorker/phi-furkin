SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-11-12
-- Description:	Returns the collections lead for a customer
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetCollectionsLeadFromCustomer]
(
	@CustomerID INT
)
RETURNS INT	
AS
BEGIN

	DECLARE @CollectionsLeadID INT,
			@ClientID INT,
			@CollectionsLeadTypeID INT = 1493
			
	SELECT @ClientID = ClientID
	FROM dbo.Customers WITH (NOLOCK) 
	WHERE CustomerID = @CustomerID
	
	SELECT @CollectionsLeadTypeID = SharedTo
	FROM dbo.LeadTypeShare WITH (NOLOCK) 
	WHERE SharedFrom = 1493
	AND ClientID = @ClientID
	
	SELECT @CollectionsLeadID = LeadID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE CustomerID = @CustomerID
	AND LeadTypeID = @CollectionsLeadTypeID
	
	RETURN @CollectionsLeadID

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetCollectionsLeadFromCustomer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_GetCollectionsLeadFromCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetCollectionsLeadFromCustomer] TO [sp_executeall]
GO
