SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-09-10
-- Description:	Returns a brand's Scheme Lead ID
-- DCM 2014-09-16 Copied from 235
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetIllnessSchemeLeadIDFromBrandRLID]
(
	@BrandRLID INT
)
RETURNS INT	
AS
BEGIN
	
	DECLARE @SchemeLeadID INT = NULL
	
	SELECT TOP 1 @SchemeLeadID = m.LeadID 
	FROM Matter m WITH (NOLOCK)
	INNER JOIN dbo.MatterDetailValues mdvSchemeRLID WITH (NOLOCK) ON mdvSchemeRLID.MatterID = m.MatterID AND mdvSchemeRLID.DetailFieldID = 145689
	INNER JOIN dbo.ResourceListDetailValues rldvSchemePolicyType WITH (NOLOCK) ON rldvSchemePolicyType.ResourceListID = mdvSchemeRLID.ValueInt AND rldvSchemePolicyType.DetailFieldID = 144319
	INNER JOIN dbo.ResourceListDetailValues rldvSchemeBrand WITH (NOLOCK) ON rldvSchemeBrand.ResourceListID = rldvSchemePolicyType.ResourceListID AND rldvSchemeBrand.DetailFieldID = 1170994 -- TODO not mapped
	WHERE --m.ClientID = 0 and   -- DCM 2014-09-16 removed clientid on the basis that a scheme is specific to a client anyway and this is a shared function (otherwise have to pass ClientID) 
	rldvSchemeBrand.ValueInt = @BrandRLID
	
	RETURN @SchemeLeadID
 
END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetIllnessSchemeLeadIDFromBrandRLID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetIllnessSchemeLeadIDFromBrandRLID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetIllnessSchemeLeadIDFromBrandRLID] TO [sp_executeall]
GO
