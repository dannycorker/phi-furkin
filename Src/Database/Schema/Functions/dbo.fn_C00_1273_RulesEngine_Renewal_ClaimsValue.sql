SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-01-25
-- Description:	Rules engine function to find the value of claims for the previous year in the dominant category (accident/illness/etc)
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_RulesEngine_Renewal_ClaimsValue]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS DECIMAL(18,2)
AS
BEGIN

	DECLARE  @ReturnValue	DECIMAL(18,2) = 0.00
			,@FromDate		DATE
			,@ToDate		DATE
			
	SELECT	 @FromDate	= dbo.fnGetSimpleDvAsDate(170036,@MatterID) /*Policy Start Date*/
			,@ToDate	= dbo.fnGetSimpleDvAsDate(170037,@MatterID) /*Policy End Date*/
	
	SELECT @ReturnValue = fn.PaidLastYear
	FROM dbo.fn_C600_SumClaimsPaidInPeriodForPolicyByIncidentType(@MatterID,@FromDate,@ToDate) fn
	ORDER BY fn.PaidLastYear DESC, fn.AccidentOrIllness

	RETURN ISNULL(@ReturnValue,0.00)
 
END
 
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_Renewal_ClaimsValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_RulesEngine_Renewal_ClaimsValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_Renewal_ClaimsValue] TO [sp_executeall]
GO
