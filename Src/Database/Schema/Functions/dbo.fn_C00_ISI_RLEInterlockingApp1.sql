SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger on behalf of Stephen Ainsworth
-- Create date: 2015-05-21
-- Description:	RLE Interlocking app1
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ISI_RLEInterlockingApp1]
(	
	@CustomerID INT
)
RETURNS @WorkingTable TABLE ( Title VARCHAR(2000), Value NUMERIC(18,2), DisplayOrder DECIMAL(18,2) )
AS
BEGIN 

	INSERT @WorkingTable ( Title, Value, DisplayOrder )

	SELECT 'Interlocking Case Breakdown',NULL,0

	UNION ALL

	SELECT 'Applicant One',NULL,1

	UNION ALL

	SELECT 'Assets',NULL,2

	UNION ALL

	SELECT 'PPR Mortgage',
	CASE dbo.fnGetSimpleDvLuli(297052,@CustomerID)
	WHEN 'Interlocking' 
	THEN dbo.fnGetSimpleDvasmoney(297776,@CustomerID)/2
	ELSE 0
	END

	,3 [DisplayOrder]

	UNION ALL

	SELECT 'Additional Assets', ISNULL(SUM(cdv.ValueMoney),0.00),4
	FROM Customers c WITH (NOLOCK)
	LEFT JOIN CustomerDetailValues cdv WITH ( NOLOCK ) ON cdv.CustomerID=c.CustomerID AND cdv.DetailFieldID IN (297541,297544,297547,297553,297556,297559,297562,297565,297568,297571,297574)
	WHERE c.CustomerID = @CustomerID 

	UNION ALL

	SELECT 'Total Assets', ISNULL(SUM(wt.Value),0.00), 5
	FROM @WorkingTable wt 
	WHERE wt.Title IN ( 'PPR Mortgage','Additional Assets' )

	UNION ALL

	SELECT 'Debts',NULL,6

	UNION ALL

	SELECT 'Secured Debts',NULL,7

	UNION ALL

	SELECT 'PPR Mortgage',
	CASE dbo.fnGetSimpleDvLuli(297052,@CustomerID)
	WHEN 'Interlocking' 
	THEN dbo.fnGetSimpleDvasmoney(297776,@CustomerID)/2
	ELSE 0
	END

	,8 [DisplayOrder]

	UNION ALL

	SELECT 'Total Additional Secured Debt', ISNULL(SUM(cdv.ValueMoney),0.00),9
	FROM CustomerDetailValues cdv WITH ( NOLOCK ) 
	WHERE cdv.CustomerID = @CustomerID 
	AND cdv.DetailFieldID IN (297830,297836,297842,297848,297857,297863,297869,297875,297881,297887,297893)

	UNION ALL

	SELECT 'Total Secured Debt', ISNULL(SUM(wt.Value),0.00), 10
	FROM @WorkingTable wt 
	WHERE wt.Title IN ( 'PPR Mortgage','Total Additional Secured Debt' )

	UNION ALL

	SELECT 'Unsecured Debt',NULL,11

	UNION ALL

	SELECT 'Total Individual Unsecured Debt', ISNULL(SUM(cdv.ValueMoney),0.00),12
	FROM Customers c WITH (NOLOCK)
	LEFT JOIN CustomerDetailValues cdv WITH ( NOLOCK ) ON cdv.CustomerID=c.CustomerID AND cdv.DetailFieldID IN (297831,297837,297843,297849,297858,297864,297870,297876,297882,297888,297894)
	WHERE c.CustomerID = @CustomerID 


	UNION ALL

	SELECT 'Monthly Income', NULL,13

	UNION ALL

	SELECT 'Net Income', CONVERT (NUMERIC(18,2),
	CASE dbo.fnGetSimpleDvLuli (297052,@CustomerID)
	WHEN 'Interlocking' 
	THEN dbo.fnGetSimpleDvAsMoney(297107,@CustomerID)
	ELSE 0
	END)
	,14 [DisplayOrder]

	INSERT @WorkingTable ( Title, Value, DisplayOrder )												

	SELECT 'Total Additional Income', ISNULL(SUM(cdv.ValueMoney),0.00),15
	FROM Customers c WITH ( NOLOCK )
	LEFT JOIN CustomerDetailValues cdv WITH (NOLOCK) ON cdv.CustomerID=c.CustomerID AND cdv.DetailFieldID IN (297108,297109,297110,297111,297114,297115,297116,297118,297119,297120,297121,297122,297123,297124,297125,297126,297129,297130,297127)
	WHERE cdv.CustomerID = @CustomerID 

	INSERT @WorkingTable ( Title, Value, DisplayOrder )
																
	SELECT 'Total Income', SUM(wt.Value), 16						
	FROM @WorkingTable wt										
	WHERE wt.Title IN ( 'Net Income','Total Additional Income' )

	UNION ALL


	SELECT 'Monthly Expenses',NULL,17

	UNION ALL

	SELECT 'Rent/Mortgage', CONVERT (NUMERIC(18,2),
	CASE dbo.fnGetSimpleDvLuli (297052,@CustomerID)
	WHEN 'Interlocking' 
	THEN dbo.fnGetSimpleDvAsMoney(295717,@CustomerID)+dbo.fngetsimpledvasmoney (295718,@CustomerID)
	ELSE 0
	END)
	,18 [DisplayOrder]

	UNION ALL

	SELECT 'Childcare Costs', CONVERT (NUMERIC(18,2),
	CASE dbo.fnGetSimpleDvLuli (297052,@CustomerID)
	WHEN 'Interlocking' 
	THEN dbo.fnGetSimpleDvAsMoney(295719,@CustomerID)
	ELSE 0
	END)
	,19 [DisplayOrder]


	UNION ALL

	SELECT 'Individual Total Set Costs', CONVERT (NUMERIC(18,2),
	CASE WHEN dbo.fnGetSimpleDvLuli (297052,@CustomerID) = 'Interlocking' 
	THEN dbo.fnGetSimpleDvAsMoney(296251,@CustomerID)+dbo.fnGetSimpleDvAsMoney(297091,@CustomerID)
	ELSE 0
	END)
	,20 [DisplayOrder]

	UNION ALL

	SELECT 'Special Circumstance Costs', CONVERT (NUMERIC(18,2),
	CASE dbo.fnGetSimpleDvLuli (297052,@CustomerID)
	WHEN 'Interlocking' 
	THEN dbo.fnGetSimpleDvAsMoney(295725,@CustomerID)
	ELSE 0
	END)
	,21 [DisplayOrder]

	INSERT @WorkingTable ( Title, Value, DisplayOrder )

	SELECT 'Reasonable Living Expenses', SUM(wt.Value),22
	FROM @WorkingTable wt 
	WHERE wt.Title IN ( 'Rent/Mortgage','Childcare Costs','Individual Total Set Costs','Special Circumstance Costs' )

	INSERT @WorkingTable ( Title, Value, DisplayOrder )
	
	SELECT 'Total expenses before unsecured debts repayment', ISNULL(SUM(wt.Value)+(dbo.fngetsimpledvasmoney (296625,@CustomerID)),0.00),23
	FROM @WorkingTable wt
	WHERE wt.Title = 'Reasonable Living Expenses'

	INSERT @WorkingTable ( Title, Value, DisplayOrder )

	SELECT 'Amount Available for Unsecured Debt Service', wt1.Value - wt2.Value,24 
	FROM @WorkingTable wt1
	INNER JOIN @WorkingTable wt2 ON wt2.Title= 'Total expenses before unsecured debts repayment'
	WHERE wt1.Title = 'Total Income' 

	RETURN

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_RLEInterlockingApp1] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_ISI_RLEInterlockingApp1] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_RLEInterlockingApp1] TO [sp_executeall]
GO
