SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Cathal Sherry/Stephen Ainsworth
-- Create date: 2015-09-08
-- Description:	For ISI Clients, return all assets for a customer
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ISI_GetAssetsForCustomer] 
(	
		@CustomerID INT
)
RETURNS
@TableOut TABLE 
(
	 CustomerID				INT
	,TableRowID				INT 
	,AssetRetained			VARCHAR(50)
	,AssetType				VARCHAR(2000)
	,MarketValue			DECIMAL(18,2)
	,Applicant				VARCHAR(50)
	,PlanType				VARCHAR(2000)
	,BriefReason			VARCHAR (2000)
)
AS
BEGIN

	/*
	This system changes frequently and unpredictably.
	This function will be used, ideally, in all instances where we need to refer to the assets on a case
	The SQL below is not the most efficient, but is designed such that we can change individual components easily, without affecting others
	
	- Find the Resources And Tables LeadTypeID
	- Find the Assets DetailFieldPageID
	- Pull up a list of all asset TableRowIDs, along with the type of asset that they represent
	- Certain properties apply to all asset types, some are type-specific
	- For each type of asset, compile a list of ThirdPartyFields that represent its relevant properties
	- For this client, find the actual DetailFieldIDs
	- Property-by-Property, populate the output table using the rules relevant to that property
	- Finally, Standardise the output so that any properties that are not applicable to an asset take the equivalent "negative" response
	*/
		
	DECLARE	 @AssetsDetailFieldPageID INT
			,@ClientID					INT
			,@LeadTypeID				INT

	SELECT @ClientID = cu.ClientID
	FROM Customers cu WITH ( NOLOCK ) 
	WHERE cu.CustomerID = @CustomerID 
	
	SELECT @LeadTypeID = lt.LeadTypeID
	FROM LeadType lt WITH ( NOLOCK ) 
	WHERE lt.LeadTypeName = 'Resources and Tables'
	AND lt.ClientID = @ClientID

	SELECT TOP(1) @AssetsDetailFieldPageID = dp.DetailFieldPageID
	FROM DetailFieldPages dp WITH ( NOLOCK ) 
	WHERE dp.ClientID = @ClientID 
	AND dp.PageName = 'Assets Section'
	ORDER BY dp.[Enabled] DESC

	DECLARE @FieldMapping TABLE (	 AssetType			VARCHAR(50)
									,Property			VARCHAR(50)
									,DetailFieldID		INT
								)
						

	INSERT @FieldMapping ( AssetType, Property, DetailFieldID )
	VALUES	 ( 'Principal Private Residence'			,'Applicant'					,		297592) 
			,( 'Investment Property'					,'Applicant'					,		297613)
			,( 'Investments other than real property'	,'Applicant'					,		297627)
			,( 'Plant, Equipment, Tools'				,'Applicant'					,		297639)
			,( 'Vehicles'								,'Applicant'					,		297658)
			,( 'Stock in trade'							,'Applicant'					,		297664)
			,( 'Money owed to you'						,'Applicant'					,		297677)
			,( 'Bank or building society accounts'		,'Applicant'					,		297694)
			,( 'Credit Union Shares/Investments'		,'Applicant'					,		297710)
			,( 'Cash on hand'							,'Applicant'					,		297716)
			,( 'Prospective Assets'						,'Applicant'					,		297724)
			,( 'Other'									,'Applicant'					,		297731)
		
			,( 'Principal Private Residence'			,'CurrentMarketValue'			,		297588) 
			,( 'Investment Property'					,'CurrentMarketValue'			,		297607)
			,( 'Investments other than real property'	,'CurrentMarketValue'			,		297620)
			,( 'Plant, Equipment, Tools'				,'CurrentMarketValue'			,		297634)
			,( 'Vehicles'								,'CurrentMarketValue'			,		297651)
			,( 'Stock in trade'							,'CurrentMarketValue'			,		297661)
			,( 'Money owed to you'						,'CurrentMarketValue'			,		297675)
			,( 'Bank or building society accounts'		,'CurrentMarketValue'			,		297690)
			,( 'Credit Union Shares/Investments'		,'CurrentMarketValue'			,		297706)
			,( 'Cash on hand'							,'CurrentMarketValue'			,		297713)
			,( 'Prospective Assets'						,'CurrentMarketValue'			,		297720)
			,( 'Other'									,'CurrentMarketValue'			,		297728)
	
			,( 'Principal Private Residence'			,'AssetRetained'				,		297593) 
			,( 'Investment Property'					,'AssetRetained'				,		297614)
			,( 'Investments other than real property'	,'AssetRetained'				,		297628)
			,( 'Plant, Equipment, Tools'				,'AssetRetained'				,		297640)
			,( 'Vehicles'								,'AssetRetained'				,		297659)
			,( 'Stock in trade'							,'AssetRetained'				,		297665)
			,( 'Money owed to you'						,'AssetRetained'				,		297678)
			,( 'Bank or building society accounts'		,'AssetRetained'				,		297695)
			,( 'Credit Union Shares/Investments'		,'AssetRetained'				,		297711)
			,( 'Cash on hand'							,'AssetRetained'				,		297717)
			,( 'Prospective Assets'						,'AssetRetained'				,		297725)
			,( 'Other'									,'AssetRetained'				,		297725)
	
			,( 'Principal Private Residence'			,'BriefReasonForRetention'		,		297594) 
			,( 'Investment Property'					,'BriefReasonForRetention'		,		297615)
			,( 'Investments other than real property'	,'BriefReasonForRetention'		,		297629)
			,( 'Plant, Equipment, Tools'				,'BriefReasonForRetention'		,		297641)
			,( 'Vehicles'								,'BriefReasonForRetention'		,		297660)
			,( 'Stock in trade'							,'BriefReasonForRetention'		,		297666)
			,( 'Money owed to you'						,'BriefReasonForRetention'		,		297679)
			,( 'Bank or building society accounts'		,'BriefReasonForRetention'		,		297696)
			,( 'Credit Union Shares/Investments'		,'BriefReasonForRetention'		,		297712)
			,( 'Cash on hand'							,'BriefReasonForRetention'		,		297718)
			,( 'Prospective Assets'						,'BriefReasonForRetention'		,		297726)
			,( 'Other'									,'BriefReasonForRetention'		,		297733)


	INSERT @TableOut ( CustomerID,AssetType, TableRowID )
	SELECT @CustomerID, df.FieldName, tr.TableRowID
	FROM DetailFields df WITH ( NOLOCK )
	INNER JOIN TableRows tr WITH ( NOLOCK ) on tr.DetailFieldID = df.DetailFieldID AND tr.CustomerID = @CustomerID
	WHERE df.DetailFieldPageID = @AssetsDetailFieldPageID
	AND df.QuestionTypeID IN ( 16,19 )
	ORDER BY df.FieldOrder, tr.TableRowID

	UPDATE t
	SET AssetRetained = CASE WHEN tdv.ValueInt = 5144 THEN 1 ELSE 0 END 
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.AssetType = t.AssetType
	LEFT JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.Property = 'AssetRetained'


	UPDATE t
	SET Applicant = li.ItemValue
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.AssetType = t.AssetType
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	INNER JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListItemID = tdv.ValueInt
	WHERE fm.Property = 'Applicant'


	UPDATE t
	SET MarketValue = tdv.ValueMoney
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.AssetType = t.AssetType
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.Property = 'CurrentMarketValue'

	UPDATE t
	SET BriefReason = tdv.DetailValue
	FROM @TableOut t 
	INNER JOIN @FieldMapping fm on fm.AssetType = t.AssetType
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = t.TableRowID AND tdv.DetailFieldID = fm.DetailFieldID
	WHERE fm.Property = 'BriefReasonForRetention'


	UPDATE t
	SET		 MarketValue				= ISNULL(t.MarketValue				,0.00	)
			,Applicant					= ISNULL(t.Applicant				,''		)
			,AssetRetained				= ISNULL(t.AssetRetained			,''		)
			,PlanType					= ISNULL(li.ItemValue				,''		)
			,BriefReason				= ISNULL(t.BriefReason			,''		)

	FROM @TableOut t 
	INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = t.TableRowID
	LEFT JOIN CustomerDetailValues cdv WITH ( NOLOCK ) on cdv.CustomerID = t.CustomerID AND cdv.DetailFieldID = 296191 /*Select Plan Type For Fee Model*/ /*CS 2015-08-09*/
	LEFT JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListItemID = cdv.ValueInt

	RETURN
END





GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetAssetsForCustomer] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_ISI_GetAssetsForCustomer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetAssetsForCustomer] TO [sp_executeall]
GO
