SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2019-07-11
-- Description:	To be used in SqlClauseForInclusion
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_FNOLSqlClauseForInclusion]
(
	@CaseID INT

)
RETURNS INT	
AS
BEGIN

	DECLARE @PolicyMatterID INT =  dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(2327007),
			@Return INT,
			@MatterID INT

			/*This is really naff but it was the way that the clause for inclusion was working it out so I don't want to touch it. They are not supposed to have multiple matters per case.*/
			SELECT @MatterID = MatterID from Matter m WITH (NOLOCK) WHERE m.CaseID = @CaseID

	SELECT @Return = COUNT(*) 
	FROM Matter m with (NOLOCK)     
	INNER JOIN TableRows tr with (NOLOCK) on tr.MatterID =  @PolicyMatterID and tr.DetailFieldID = 170033     
	INNER JOIN TableDetailValues strt with (NOLOCK) on strt.TableRowID = tr.TableRowID and strt.DetailFieldID = 145663     
	INNER JOIN TableDetailValues nd WITH (NOLOCK) on nd.TableRowID = tr.TableRowID and nd.DetailFieldID = 145664      
	INNER JOIN TableDetailValues stts with (NOLOCK) on stts.TableRowID = tr.TableRowID and stts.DetailFieldID = 180216       
	INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.MatterID = m.MatterID and mdv.DetailFieldID = 145674      
	WHERE m.CaseID = @CaseID   
	AND mdv.ValueDate BETWEEN strt.ValueDate and ISNULL(nd.ValueDate,dbo.fn_GetDate_Local())   
	and stts.ValueInt = 1
	
	RETURN @Return

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_FNOLSqlClauseForInclusion] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_FNOLSqlClauseForInclusion] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_FNOLSqlClauseForInclusion] TO [sp_executeall]
GO
