SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-10-31
-- Description:	Selects a simple detail field from the appropriate detail value table as LookupListItemValue
--				2014-10-08 ACE Updated so that basic tables would work
-- =============================================
CREATE FUNCTION [dbo].[fnGetSimpleDvLuli]
(
	@DetailFieldID INT,
	@ObjectID INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE @ItemValue		VARCHAR(2000),
			@LeadOrMatter	INT

	SELECT @LeadOrMatter = LeadOrmatter
	FROM DetailFields WITH (NOLOCK) 
	WHERE DetailFieldID = @DetailFieldID

	IF @LeadOrMatter = 1
	BEGIN

		SELECT @ItemValue = luli.ItemValue
		FROM LeadDetailValues ldv WITH (NOLOCK)
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = ldv.ValueInt 
		WHERE LeadID = @ObjectID 
		AND DetailFieldID = @DetailFieldID

	END

	IF @LeadOrMatter = 2
	BEGIN

		SELECT @ItemValue = luli.ItemValue
		FROM MatterDetailValues mdv WITH (NOLOCK)
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = mdv.ValueInt 
		WHERE MatterID = @ObjectID 
		AND DetailFieldID = @DetailFieldID

	END

	IF @LeadOrMatter IN (6,8)
	BEGIN

		SELECT @ItemValue = luli.ItemValue
		FROM TableDetailValues tdv WITH (NOLOCK)
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = tdv.ValueInt 
		WHERE TableRowID = @ObjectID 
		AND DetailFieldID = @DetailFieldID

	END
	
	IF @LeadOrMatter = 10
	BEGIN

		SELECT @ItemValue = luli.ItemValue
		FROM CustomerDetailValues cdv WITH (NOLOCK)
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = cdv.ValueInt 
		WHERE CustomerID = @ObjectID 
		AND DetailFieldID = @DetailFieldID

	END
	
	IF @LeadOrMatter = 11
	BEGIN

		SELECT @ItemValue = luli.ItemValue
		FROM CaseDetailValues cdv WITH (NOLOCK)
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = cdv.ValueInt 
		WHERE cdv.CaseID = @ObjectID 
		AND DetailFieldID = @DetailFieldID

	END
	
	IF @LeadOrMatter = 12
	BEGIN

		SELECT @ItemValue = luli.ItemValue
		FROM ClientDetailValues cdv WITH (NOLOCK)
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = cdv.ValueInt 
		WHERE cdv.ClientID = @ObjectID 
		AND DetailFieldID = @DetailFieldID

	END
	
	IF @LeadOrMatter = 13
	BEGIN

		SELECT @ItemValue = luli.ItemValue
		FROM ClientPersonnelDetailValues cdv WITH (NOLOCK)
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = cdv.ValueInt 
		WHERE cdv.ClientPersonnelID = @ObjectID 
		AND DetailFieldID = @DetailFieldID

	END
	
	IF @LeadOrMatter = 14
	BEGIN

		SELECT @ItemValue = luli.ItemValue
		FROM ContactDetailValues cdv WITH (NOLOCK)
		INNER JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = cdv.ValueInt 
		WHERE cdv.ContactID = @ObjectID 
		AND DetailFieldID = @DetailFieldID

	END

	RETURN @ItemValue

END



GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleDvLuli] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetSimpleDvLuli] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleDvLuli] TO [sp_executeall]
GO
