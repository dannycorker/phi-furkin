SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2013-11-22
-- Description:	For ISI Clients, calculate live values for expected repayment amounts
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ISI_GetMonthlyRepaymentsByCustomerAndYear] 
(	
		@CustomerID INT
	,	@Year		INT
)
RETURNS
@TableOut TABLE 
(
	 Creditor	VARCHAR(2000)
	,Year		INT
	,January	DECIMAL(18,2)
	,February	DECIMAL(18,2)
	,March		DECIMAL(18,2)
	,April		DECIMAL(18,2)
	,May		DECIMAL(18,2)
	,June		DECIMAL(18,2)
	,July		DECIMAL(18,2)
	,August		DECIMAL(18,2)
	,September	DECIMAL(18,2)
	,October	DECIMAL(18,2)
	,November	DECIMAL(18,2)
	,December	DECIMAL(18,2)
	,TableRowID	INT
)
AS
BEGIN
		
	--DECLARE @CustomerID INT = 9824795
	INSERT @TableOut ( Creditor, TableRowID, Year, January, February, March, April, May, June, July, August, September, October, November, December )
	SELECT Creditor, TableRowID, Year, January, February, March, April, May, June, July, August, September, October, November, December
	FROM
		(	
		SELECT  
			 fn.Creditor + ' (' + CONVERT(VARCHAR,fn.TableRowID) + ')' [Creditor]
			,fn.TableRowID
			,1 [Year]
			,CAST(fn.[Year 1]/12.00 as DECIMAL(18,2)) [Value]
			,lul.ItemValue
			--,li.SortOrder
		--FROM dbo.fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerID(@CustomerID) fn
		FROM fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerIDandApplicant(@CustomerID,1) fn
		INNER JOIN TableDetailValues tdv_who WITH ( NOLOCK ) on tdv_who.TableRowID = fn.TableRowID
		INNER JOIN ThirdPartyFieldMapping fm WITH ( NOLOCK ) on fm.DetailFieldID = tdv_who.DetailFieldID 
		INNER JOIN LookupListItems lli_who WITH ( NOLOCK ) on lli_who.LookupListItemID = tdv_who.ValueInt AND lli_who.ItemValue IN ('Applicant', 'Joint') /*Applicant, Joint*/
		INNER JOIN LookupListItems lul WITH ( NOLOCK ) on lul.LookupListID = 1048
		WHERE fm.ThirdPartyFieldID = 1397
			UNION ALL
		SELECT  
			 fn.Creditor + ' (' + CONVERT(VARCHAR,fn.TableRowID) + ')' [Creditor]
			,fn.TableRowID
			,2 [Year]
			,CAST(fn.[Year 2]/12.00 as DECIMAL(18,2)) [Value]
			,lul.ItemValue
			--,li.SortOrder
		--FROM dbo.fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerID(@CustomerID) fn
		FROM fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerIDandApplicant(@CustomerID,1) fn
		INNER JOIN TableDetailValues tdv_who WITH ( NOLOCK ) on tdv_who.TableRowID = fn.TableRowID
		INNER JOIN ThirdPartyFieldMapping fm WITH ( NOLOCK ) on fm.DetailFieldID = tdv_who.DetailFieldID 
		INNER JOIN LookupListItems lli_who WITH ( NOLOCK ) on lli_who.LookupListItemID = tdv_who.ValueInt AND lli_who.ItemValue IN ('Applicant', 'Joint') /*Applicant, Joint*/
		INNER JOIN LookupListItems lul WITH ( NOLOCK ) on lul.LookupListID = 1048
		WHERE fm.ThirdPartyFieldID = 1397
			UNION ALL
		SELECT  
			 fn.Creditor + ' (' + CONVERT(VARCHAR,fn.TableRowID) + ')' [Creditor]
			,fn.TableRowID
			,3 [Year]
			,CAST(fn.[Year 3]/12.00 as DECIMAL(18,2)) [Value]
			,lul.ItemValue
			--,li.SortOrder
		--FROM dbo.fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerID(@CustomerID) fn
		FROM fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerIDandApplicant(@CustomerID,1) fn
		INNER JOIN TableDetailValues tdv_who WITH ( NOLOCK ) on tdv_who.TableRowID = fn.TableRowID
		INNER JOIN ThirdPartyFieldMapping fm WITH ( NOLOCK ) on fm.DetailFieldID = tdv_who.DetailFieldID 
		INNER JOIN LookupListItems lli_who WITH ( NOLOCK ) on lli_who.LookupListItemID = tdv_who.ValueInt AND lli_who.ItemValue IN ('Applicant', 'Joint') /*Applicant, Joint*/
		INNER JOIN LookupListItems lul WITH ( NOLOCK ) on lul.LookupListID = 1048
		WHERE fm.ThirdPartyFieldID = 1397
			UNION ALL
		SELECT  
			 fn.Creditor + ' (' + CONVERT(VARCHAR,fn.TableRowID) + ')' [Creditor]
			,fn.TableRowID
			,4 [Year]
			,CAST(fn.[Year 4]/12.00 as DECIMAL(18,2)) [Value]
			,lul.ItemValue
			--,li.SortOrder
		--FROM dbo.fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerID(@CustomerID) fn
		FROM fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerIDandApplicant(@CustomerID,1) fn
		INNER JOIN TableDetailValues tdv_who WITH ( NOLOCK ) on tdv_who.TableRowID = fn.TableRowID
		INNER JOIN ThirdPartyFieldMapping fm WITH ( NOLOCK ) on fm.DetailFieldID = tdv_who.DetailFieldID 
		INNER JOIN LookupListItems lli_who WITH ( NOLOCK ) on lli_who.LookupListItemID = tdv_who.ValueInt AND lli_who.ItemValue IN ('Applicant', 'Joint') /*Applicant, Joint*/
		INNER JOIN LookupListItems lul WITH ( NOLOCK ) on lul.LookupListID = 1048
		WHERE fm.ThirdPartyFieldID = 1397
			UNION ALL
		SELECT  
			 fn.Creditor + ' (' + CONVERT(VARCHAR,fn.TableRowID) + ')' [Creditor]
			,fn.TableRowID
			,5 [Year]
			,CAST(fn.[Year 5]/12.00 as DECIMAL(18,2)) [Value]
			,lul.ItemValue
			--,li.SortOrder
		--FROM dbo.fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerID(@CustomerID) fn
		FROM fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerIDandApplicant(@CustomerID,1) fn
		INNER JOIN TableDetailValues tdv_who WITH ( NOLOCK ) on tdv_who.TableRowID = fn.TableRowID
		INNER JOIN ThirdPartyFieldMapping fm WITH ( NOLOCK ) on fm.DetailFieldID = tdv_who.DetailFieldID 
		INNER JOIN LookupListItems lli_who WITH ( NOLOCK ) on lli_who.LookupListItemID = tdv_who.ValueInt AND lli_who.ItemValue IN ('Applicant', 'Joint') /*Applicant, Joint*/
		INNER JOIN LookupListItems lul WITH ( NOLOCK ) on lul.LookupListID = 1048
		WHERE fm.ThirdPartyFieldID = 1397
			UNION ALL
		SELECT  
			 fn.Creditor + ' (' + CONVERT(VARCHAR,fn.TableRowID) + ')' [Creditor]
			,fn.TableRowID
			,6 [Year]
			,CAST(fn.[Year 6]/12.00 as DECIMAL(18,2)) [Value]
			,lul.ItemValue
			--,li.SortOrder
		--FROM dbo.fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerID(@CustomerID) fn
		FROM fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerIDandApplicant(@CustomerID,1) fn
		INNER JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListID = 1048
		INNER JOIN TableDetailValues tdv_who WITH ( NOLOCK ) on tdv_who.TableRowID = fn.TableRowID
		INNER JOIN ThirdPartyFieldMapping fm WITH ( NOLOCK ) on fm.DetailFieldID = tdv_who.DetailFieldID 
		INNER JOIN LookupListItems lli_who WITH ( NOLOCK ) on lli_who.LookupListItemID = tdv_who.ValueInt AND lli_who.ItemValue IN ('Applicant', 'Joint') /*Applicant, Joint*/
		INNER JOIN LookupListItems lul WITH ( NOLOCK ) on lul.LookupListID = 1048
		WHERE fm.ThirdPartyFieldID = 1397
		--ORDER BY [Year], [SortOrder]
		)
		AS ToPivot		
	PIVOT		
	(		
		Max([Value])
	FOR		
	[ItemValue]
		IN ( [January],[February],[March],[April],[May],[June],[July],[August],[September],[October],[November],[December] )
	) AS Pivoted
	WHERE [Year] = @Year or @Year = 0

	RETURN
END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetMonthlyRepaymentsByCustomerAndYear] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_ISI_GetMonthlyRepaymentsByCustomerAndYear] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetMonthlyRepaymentsByCustomerAndYear] TO [sp_executeall]
GO
