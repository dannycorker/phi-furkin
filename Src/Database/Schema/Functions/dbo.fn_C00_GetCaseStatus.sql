SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-09-18
-- Description:	Return a cases Status Name
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_GetCaseStatus]
(
	@MatterID INT
)
RETURNS VARCHAR(2000)
AS
BEGIN
	
	DECLARE @Output VARCHAR(2000)

	SELECT @Output = ls.StatusName
	FROM Matter m WITH (NOLOCK)
	INNER JOIN Cases c with (nolock) on c.CaseID = m.CaseID
	INNER JOIN LeadStatus ls WITH (NOLOCK) ON ls.StatusID = c.ClientStatusID
	WHERE m.MatterID = @MatterID

	RETURN @Output

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetCaseStatus] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_GetCaseStatus] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetCaseStatus] TO [sp_executeall]
GO
