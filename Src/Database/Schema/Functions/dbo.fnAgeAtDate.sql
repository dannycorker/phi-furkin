SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-01-29
-- Description:	Copy of fnAgeFromDate but allowing you to specifiy an end date... e.g. how old was I on the first of this year
-- =============================================
CREATE FUNCTION [dbo].[fnAgeAtDate] 
(
	@StartDate DATETIME,
	@EndDate DATETIME
)
RETURNS INT
AS
BEGIN

	-- Return datediff in years, plus/minus 1 extra year to cater for these scenarios:
	-- A) From 1st Jan 2008 to 15th July 2008 is 0 years
	-- B) From 1st Jan 2008 to 1st Jan 2009 is 1 year
	RETURN DATEDIFF(yy,@StartDate,@EndDate) -	CASE 
													WHEN @EndDate < DATEADD(yy,DATEDIFF(yy,@StartDate,@EndDate), @StartDate) THEN 1 
													ELSE 0 
												END
END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnAgeAtDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnAgeAtDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnAgeAtDate] TO [sp_executeall]
GO
