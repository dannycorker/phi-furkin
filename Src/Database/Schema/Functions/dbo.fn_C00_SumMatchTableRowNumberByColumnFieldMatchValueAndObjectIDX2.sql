SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-02-10
-- Description:	Take a table field, column field and match value and return the first matching row for this object
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_SumMatchTableRowNumberByColumnFieldMatchValueAndObjectIDX2]
(
	@TableDetailFieldID		INT,
	@SumColumnDetailFieldID INT,
	@ColumnDetailFieldID	INT,
	@MatchValue				VARCHAR(2000),
	@ColumnDetailFieldID2	INT,
	@MatchValue2			VARCHAR(2000),
	@ObjectID				INT
)
RETURNS Numeric(18,2)
AS
BEGIN
	
	DECLARE @DetailFieldSubTypeID	INT,
			@ReturnValue			Numeric(18,2) = 0
	
	SELECT @DetailFieldSubTypeID = df.LeadOrMatter
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @TableDetailFieldID

	;With NumberedValues as 
	(	
		SELECT CASE df.QuestionTypeID WHEN 4 THEN luli.ItemValue ELSE tdv.DetailValue END as DetailValue,
		tsum.ValueMoney [Total]
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID
		INNER JOIN DetailFields df WITH (NOLOCK) ON df.DetailFieldID = tdv.DetailFieldID
		LEFT JOIN LookupListItems luli WITH (NOLOCK) ON df.LookupListID = luli.LookupListID and luli.LookupListItemID = tdv.ValueInt
		INNER JOIN TableDetailValues tsum WITH (NOLOCK) ON tsum.TableRowID=tr.TableRowID and tsum.DetailFieldID=@SumColumnDetailFieldID
		INNER JOIN TableDetailValues tdv_2x WITH (NOLOCK) ON tdv_2x.TableRowID = tr.TableRowID and tdv_2x.DetailFieldID = @ColumnDetailFieldID2 AND tdv_2x.detailValue = @MatchValue2
		WHERE tr.DetailFieldID = @TableDetailFieldID
		and tdv.DetailFieldID  = @ColumnDetailFieldID
		and (
			@DetailFieldSubTypeID = 1  AND tr.LeadID			= @ObjectID
		OR	@DetailFieldSubTypeID = 2  AND tr.MatterID			= @ObjectID
		OR	@DetailFieldSubTypeID = 11 AND tr.CaseID			= @ObjectID
		OR	@DetailFieldSubTypeID = 10 AND tr.CustomerID		= @ObjectID
		OR	@DetailFieldSubTypeID = 13 AND tr.ClientPersonnelID = @ObjectID
		OR	@DetailFieldSubTypeID = 12 AND tr.CLientID			= @ObjectID
			)
	)
	SELECT @ReturnValue = sum(nv.Total)
	FROM NumberedValues nv
	WHERE nv.DetailValue = @MatchValue
	
	RETURN @ReturnValue
END






GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumMatchTableRowNumberByColumnFieldMatchValueAndObjectIDX2] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_SumMatchTableRowNumberByColumnFieldMatchValueAndObjectIDX2] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumMatchTableRowNumberByColumnFieldMatchValueAndObjectIDX2] TO [sp_executeall]
GO
