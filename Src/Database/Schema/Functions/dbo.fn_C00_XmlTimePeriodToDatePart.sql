SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2015-07-07
-- Description:	Take an XML time period value (e.g. P4Y0M6D) and return a narrative or numeric interpretation
--				e.g. fn_C00_XmlTimePeriodToDatePart('P4Y0M6D','' ,1)	= '4 Years 0 Months 6 Days'	<< Search for everything, with labels
--				e.g. fn_C00_XmlTimePeriodToDatePart('P4Y0M6D','D',1)	= '6 Days'					<< Just search for days, with label
--				e.g. fn_C00_XmlTimePeriodToDatePart('P4Y0M6D','D',0)	= '6'						<< Search for days, and just give the value
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_XmlTimePeriodToDatePart]
(
	 @InputString VARCHAR(2000)
	,@SpecificPart VARCHAR(1)
	,@IncludeLabel BIT = 1
)
RETURNS VARCHAR(2000)
AS
BEGIN
	DECLARE  @OutputString VARCHAR(2000) = ''

	DECLARE  @SpecificPartRowNum		INT = 0
			,@SpecificPartValueRowNum	INT = 0
			
	DECLARE  @RunningTable TABLE (OutputString VARCHAR(2000))

	/*
	Give labels for each of the text labels that can appear
	I only have 3 that are relevant at the moment, and P indicates "Period"
	*/
	DECLARE @ValueMapping TABLE (Value VARCHAR(1), Name VARCHAR(2000))
	INSERT @ValueMapping (Value, Name)
	VALUES	 ('P','')
			,('Y','Years')
			,('M','Months')
			,('D','Days')

	/*Create a table with one character per row*/
	DECLARE @CharTable TABLE (N INT, Value VARCHAR(2000))
	INSERT @CharTable (N, Value)
	SELECT t.N , SUBSTRING(@InputString,t.N,1)
	FROM Tally t WITH ( NOLOCK ) 
	WHERE t.N <= LEN(@InputString)

	/*If we have are looking for a specific section, pick up the relevant row number*/
	SELECT @SpecificPartRowNum = ct.N
	FROM @CharTable ct 
	WHERE ct.Value = @SpecificPart

	/*And find the row which STARTS the value section for that date part*/
	/*Bear in mind there may be multiple digits before the lable (e.g. P15Y)*/
	SELECT TOP(1) @SpecificPartValueRowNum = ct.N+1
	FROM @CharTable ct
	WHERE ct.N < @SpecificPartRowNum
	AND ISNUMERIC(ct.Value) = 0
	ORDER BY ct.N DESC

	/*Run through the table and concatenate the values we care about*/
	INSERT @RunningTable ( OutputString )
	SELECT (ISNULL(ISNULL(vm.Name,CONVERT(VARCHAR,ct.Value))+CASE WHEN ISNUMERIC(ct.Value) <> ISNUMERIC(ctNext.Value) THEN ' ' ELSE '' END,''))
	FROM @CharTable ct
	LEFT JOIN @ValueMapping vm on vm.Value = ct.Value
	LEFT JOIN @CharTable ctNext on ctNext.N = ct.N + 1
	WHERE	(@SpecificPartRowNum = 0 or ct.N BETWEEN @SpecificPartValueRowNum AND @SpecificPartRowNum) /* If we are looking for a specific part */
	AND		(@IncludeLabel = 1 or ISNUMERIC(ct.Value)=1) /*If we want to include the label*/
	ORDER BY ct.N
	
	SELECT @OutputString += rt.OutputString 
	FROM @RunningTable rt
	WHERE rt.OutputString <> ''

	/*Clear leading or trailing spaces and make sure the result is never NULL*/
	SELECT @OutputString = ISNULL(LTRIM(RTRIM(@OutputString)),'')

	RETURN @OutputString
END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_XmlTimePeriodToDatePart] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_XmlTimePeriodToDatePart] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_XmlTimePeriodToDatePart] TO [sp_executeall]
GO
