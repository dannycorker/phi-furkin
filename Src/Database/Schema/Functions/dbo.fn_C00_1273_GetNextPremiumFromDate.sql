SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-05-02
-- Description:	Returns the next premium from date for a policy calculation
-- DCM 2014-09-15 Copied from 235
-- =============================================
 
 
CREATE FUNCTION [dbo].[fn_C00_1273_GetNextPremiumFromDate]
(
	@MatterID INT,
	@Type INT
)
RETURNS 
@Data TABLE
(
	FromDate DATE,
	Type INT
)	
AS
BEGIN
	
	
	DECLARE @FromDate DATE
	
	-- We get the latest end date and add 1 day
	SELECT @FromDate = DATEADD(DAY, 1, MAX(tdvTo.ValueDate))
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvTo WITH (NOLOCK) ON r.TableRowID = tdvTo.TableRowID AND tdvTo.DetailFieldID = 170077
	INNER JOIN dbo.TableDetailValues tdvStatus WITH (NOLOCK) ON r.TableRowID = tdvStatus.TableRowID AND tdvStatus.DetailFieldID = 170078
	WHERE r.DetailFieldID = 170088
	AND r.MatterID = @MatterID
	AND tdvStatus.ValueInt IN (69898,69901,69903,72154) -- Only paid, waived, collecting and ready

	IF @FromDate IS NULL
	BEGIN
		-- If this date is still null then the first payment must have failed so set the type to first and get the earliest from date
		SELECT @Type = 69896 -- First
		
		SELECT @FromDate = MIN(tdvFrom.ValueDate)
		FROM dbo.TableRows r WITH (NOLOCK) 
		INNER JOIN dbo.TableDetailValues tdvFrom WITH (NOLOCK) ON r.TableRowID = tdvFrom.TableRowID AND tdvFrom.DetailFieldID = 170076
		WHERE r.DetailFieldID = 170088
		AND r.MatterID = @MatterID
	END

	INSERT @Data (FromDate, Type)
	VALUES (@FromDate, @Type)
	
	RETURN
 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetNextPremiumFromDate] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1273_GetNextPremiumFromDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetNextPremiumFromDate] TO [sp_executeall]
GO
