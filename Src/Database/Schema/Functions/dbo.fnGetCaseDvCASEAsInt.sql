SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-06-12
-- Description:	Get CaseDV directly #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetCaseDvCASEAsInt]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS int
AS
BEGIN

	DECLARE @ValueInt AS int

	SELECT @ValueInt = cdv.ValueInt
	FROM CaseDetailValues cdv WITH (NOLOCK) 
	WHERE cdv.CaseID = @CaseID 
	AND cdv.DetailFieldID = @DetailFieldID

	RETURN @ValueInt

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCaseDvCASEAsInt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetCaseDvCASEAsInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCaseDvCASEAsInt] TO [sp_executeall]
GO
