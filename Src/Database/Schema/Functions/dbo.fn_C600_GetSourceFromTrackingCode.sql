SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-11-15
-- Description:	Takes a tracking code and looks up the Aggregator/Source Name or ID
-- CPS 2017-11-23 renamed and repurposed to look at the central tracking code list rather than being agg-specific
-- CPS 2017-11-23 allowed 3 return types
-- GPR 2018-07-20 Created to C600
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetSourceFromTrackingCode]
(
	 @TrackingCode	VARCHAR(2000)
	,@ReturnType123	INT -- 1 = LookupListItemID, 2 = ResourceListID, 3 = Name
)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE  @LookupListItemID	INT
			,@ResourceListID	INT
			,@ItemValue			VARCHAR(2000)
			,@ReturnValue		VARCHAR(2000)

	SELECT	 @LookupListItemID  = li.LookupListItemID
			,@ResourceListID	= rdvCode.ResourceListID
			,@ItemValue			= li.ItemValue
	FROM ResourceListDetailValues rdvCode WITH ( NOLOCK )
	INNER JOIN ResourceListDetailValues rdvName WITH ( NOLOCK ) on rdvName.ResourceListID = rdvCode.ResourceListID AND rdvName.DetailFieldID = 180105 /*Source*/
	INNER JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListItemID = rdvName.ValueInt
	WHERE rdvCode.DetailFieldID = 180109 /*Encoded Rlid*/ /*GPR 2018-07-27 updated DFID)*/
	AND rdvCode.DetailValue = @TrackingCode

	SELECT @ReturnValue = CASE @ReturnType123
							WHEN 1 THEN CONVERT(VARCHAR,@LookupListItemID) 
							WHEN 2 THEN CONVERT(VARCHAR,@ResourceListID)
							WHEN 3 THEN @ItemValue
						  END

	-- And return it
	RETURN @ReturnValue

END













GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetSourceFromTrackingCode] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetSourceFromTrackingCode] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetSourceFromTrackingCode] TO [sp_executeall]
GO
