SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-01-08
-- Description:	Returns the correct START date for a policy calculation
-- DCM 2014-09-15 Copied from 235
-- Modified:	2014-10-17	SB	Pass premium date through to fn_C235_GetPremiumCalculationPolicyHistoryRow to fix bug
-- =============================================
 
CREATE FUNCTION [dbo].[fn_C00_1273_GetPremiumCalculationStartDate]
(
	@CaseID INT,
	@PremiumDate DATE
)
RETURNS DATE	
AS
BEGIN
	
	DECLARE @CalculationDate DATE
	
	DECLARE @TableRowID INT
	SELECT @TableRowID = dbo.fn_C00_1273_GetPremiumCalculationPolicyHistoryRow(@CaseID, @PremiumDate)
 
	SELECT @CalculationDate = tdvFrom.ValueDate
	FROM dbo.TableRows r WITH (NOLOCK) 
	INNER JOIN dbo.TableDetailValues tdvFrom WITH (NOLOCK) ON r.TableRowID = tdvFrom.TableRowID AND tdvFrom.DetailFieldID = 145663
	WHERE r.TableRowID = @TableRowID
	
	IF @CalculationDate IS NULL
	BEGIN
		SELECT @CalculationDate = dbo.fn_GetDate_Local()
	END
	
	RETURN @CalculationDate
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetPremiumCalculationStartDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetPremiumCalculationStartDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetPremiumCalculationStartDate] TO [sp_executeall]
GO
