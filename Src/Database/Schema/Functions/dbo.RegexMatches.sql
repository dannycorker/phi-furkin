SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE FUNCTION [dbo].[RegexMatches] (@input [nvarchar] (max), @pattern [nvarchar] (4000))
RETURNS TABLE (
[Index] [int] NULL,
[Text] [nvarchar] (max) COLLATE Latin1_General_CI_AS NULL)
WITH EXECUTE AS CALLER
EXTERNAL NAME [SqlServerProject_Regex].[UserDefinedFunctions].[RegexMatches]
GO
GRANT VIEW DEFINITION ON  [dbo].[RegexMatches] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[RegexMatches] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[RegexMatches] TO [sp_executeall]
GO
