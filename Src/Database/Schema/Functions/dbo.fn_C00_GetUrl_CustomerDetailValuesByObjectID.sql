SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2013-09-10
-- Description:	Get the URL for MoreDetails according to the passed in object ID
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_GetUrl_CustomerDetailValuesByObjectID]
(
	 @ObjectID		INT
	,@LeadOrMatter	INT
)
RETURNS varchar(2000)
AS
BEGIN
	/*
	example return:
	https://aqnet.aquarium-software.com/DetailFieldView.aspx?type=10&mode=1&lid=5052695&cid=4270172&ru=%2fcustomersleaddetails2.aspx%3f_c_i_d_%3d4270172_a_n_d__l_i_d_%3d5052695_a_n_d__a_i_d_%3d9615439
	first section is done by _C00_GetUrlByDatabase, so we just need to append the appropriate
	DetailFieldView.aspx?type=10&mode=1&lid=5052695&cid=4270172&ru=%2fcustomersleaddetails2.aspx%3f_c_i_d_%3d4270172_a_n_d__l_i_d_%3d5052695_a_n_d__a_i_d_%3d9615439
	
	variable items are LeadID, CustomerID and CaseID (aka "aid")
	
	Could probably use this logic to create an equivalent to look at ANY level of DetailFieldView.. just need to very the "type=10" bit.
	*/

	DECLARE	 @CustomerID	INT
			,@LeadID		INT
			,@CaseID		INT
			,@URL			VARCHAR(2000)
			,@Prefix		VARCHAR(2000)
			
	SELECT @Prefix = dbo.fn_C00_GetUrlByDatabase() /*Returns e.g. "https://aqnet.aquarium-software.com/" for Aquarius, or "http://aquarium.investorcompensation.co.uk/" for Aquarius239*/
		
	IF @LeadOrMatter = 1		/*Lead*/
	BEGIN
		SELECT TOP(1) @CustomerID = l.CustomerID, @LeadID = l.LeadID, @CaseID = c.CaseID
		FROM Lead l WITH (NOLOCK) 
		INNER JOIN Cases c WITH (NOLOCK) on c.LeadID = l.LeadID
		WHERE l.LeadID = @ObjectID
		ORDER BY c.CaseID
	END
	ELSE IF @LeadOrMatter = 2	/*Matter*/
	BEGIN
		SELECT @CustomerID = m.CustomerID, @LeadID = m.LeadID, @CaseID = m.CaseID
		FROM Matter m WITH (NOLOCK) 
		WHERE m.MatterID = @ObjectID
	END
	ELSE IF @LeadOrMatter = 10	/*Customer*/
	BEGIN
		SELECT TOP(1) @CustomerID = c.CustomerID, @LeadID = l.LeadID, @CaseID = ca.CaseID
		FROM Customers c WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) on l.CustomerID = c.CustomerID
		INNER JOIN Cases ca WITH (NOLOCK) on ca.LeadID = l.LeadID
		WHERE c.CustomerID = @ObjectID	
		ORDER BY ca.CaseID
	END
	ELSE IF @LeadOrMatter = 11	/*Case*/
	BEGIN
		SELECT @CustomerID = l.CustomerID, @LeadID = c.LeadID, @CaseID = c.CaseID
		FROM Cases c WITH (NOLOCK) 
		INNER JOIN Lead l WITH (NOLOCK) on l.LeadID = c.LeadID
		WHERE c.CaseID = @ObjectID
	END
	
	/*Nothing should be NULL at this point*/
	SELECT @URL	=	ISNULL(
					
					  @Prefix
					+ 'DetailFieldView.aspx?type=10&mode=1&lid=' + CONVERT(VARCHAR,@LeadID)
					+ '&cid=' + CONVERT(VARCHAR,@CustomerID)
					+ '&ru=%2fcustomersleaddetails2.aspx%3f_c_i_d_%3d' + CONVERT(VARCHAR,@CustomerID)
					+ '_a_n_d__l_i_d_%3d' + CONVERT(VARCHAR,@LeadID)
					+ '_a_n_d__a_i_d_%3d' + CONVERT(VARCHAR,@CaseID)
					
					,'Error')

	RETURN @URL

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetUrl_CustomerDetailValuesByObjectID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_GetUrl_CustomerDetailValuesByObjectID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetUrl_CustomerDetailValuesByObjectID] TO [sp_executeall]
GO
