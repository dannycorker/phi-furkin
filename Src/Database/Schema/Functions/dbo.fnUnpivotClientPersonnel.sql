SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Unpivot the columns in the ClientPersonnel table 
-- =============================================
CREATE FUNCTION [dbo].[fnUnpivotClientPersonnel] 
(	
	@ClientPersonnelID int
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT ColumnName as [RawColumnName], '[!' + ColumnName + ']' as [FormattedColumnName], RealValue
	FROM
	(
		SELECT 
		COALESCE(CONVERT(varchar(255), c.AccountDisabled), '') as [AccountDisabled],
		COALESCE(CONVERT(varchar(255), c.AttemptedLogins), '') as [AttemptedLogins],
		COALESCE(CONVERT(varchar(255), c.ChargeOutRate), '') as [ChargeOutRate],
		COALESCE(CONVERT(varchar(255), c.ClientID), '') as [ClientID],
		COALESCE(CONVERT(varchar(255), c.ClientOfficeID), '') as [ClientOfficeID],
		COALESCE(CONVERT(varchar(255), c.ClientPersonnelAdminGroupID), '') as [ClientPersonnelAdminGroupID],
		COALESCE(CONVERT(varchar(255), c.ClientPersonnelID), '') as [ClientPersonnelID],
		COALESCE(CONVERT(varchar(255), c.EmailAddress), '') as [EmailAddress],
		COALESCE(CONVERT(varchar(255), c.FirstName), '') as [FirstName],
		COALESCE(CONVERT(varchar(255), c.HomeTelephone), '') as [HomeTelephone],
		COALESCE(CONVERT(varchar(255), c.JobTitle), '') as [JobTitle],
		COALESCE(CONVERT(varchar(255), c.LanguageID), '') as [LanguageID],
		COALESCE(CONVERT(varchar(255), c.LastName), '') as [LastName],
		COALESCE(CONVERT(varchar(255), c.ManagerID), '') as [ManagerID],
		COALESCE(CONVERT(varchar(255), c.MiddleName), '') as [MiddleName],
		COALESCE(CONVERT(varchar(255), c.MobileTelephone), '') as [MobileTelephone],
		COALESCE(CONVERT(varchar(255), c.OfficeTelephone), '') as [OfficeTelephone],
		COALESCE(CONVERT(varchar(255), c.OfficeTelephoneExtension), '') as [OfficeTelephoneExtension],
		/*COALESCE(CONVERT(varchar(255), c.Password), '') as [Password],
		COALESCE(CONVERT(varchar(255), c.Salt), '') as [Salt],*/
		COALESCE(CONVERT(varchar(255), c.SubClientID), '') as [SubClientID],
		COALESCE(CONVERT(varchar(255), c.TitleID), '') as [TitleID],
		COALESCE(CONVERT(varchar(255), c.UserName), '') as [UserName],
		COALESCE(CONVERT(varchar(255), c.Initials), '') as [Initials]
		FROM dbo.ClientPersonnel c WITH (NOLOCK) 
		WHERE c.ClientPersonnelID = @ClientPersonnelID) p
	UNPIVOT
	(
		RealValue FOR ColumnName IN
		(
			[AccountDisabled],[AttemptedLogins],[ChargeOutRate],[ClientID],[ClientOfficeID],[ClientPersonnelAdminGroupID],[ClientPersonnelID],[EmailAddress],[FirstName],[HomeTelephone],[JobTitle],[LanguageID],[LastName],[ManagerID],[MiddleName],[MobileTelephone],[OfficeTelephone],[OfficeTelephoneExtension]/*,[Password],[Salt]*/,[SubClientID],[TitleID],[UserName],[Initials]
		)
	) as unpvt

)





GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotClientPersonnel] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnUnpivotClientPersonnel] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotClientPersonnel] TO [sp_executeall]
GO
