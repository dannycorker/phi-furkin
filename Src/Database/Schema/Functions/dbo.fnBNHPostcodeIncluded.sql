SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim
-- Create date: 2007-08-06
-- Description:	Improved BNH Postcode matching
-- =============================================
CREATE FUNCTION [dbo].[fnBNHPostcodeIncluded]
(
	-- Add the parameters for the function here
	@Postcode nvarchar(10)
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result bit

	DECLARE @Len int
	DECLARE @Prefix nvarchar(4)
	--DECLARE @Suffix nvarchar(3)

	-- BNH postcodes included are currently only done by Postcode Prefix,
	-- so "WA14" rather than "WA14 2UN".  If the postcode passed in is 
	-- 6 characters or more then we (probably) have a sensible postcode.
	SET @Len = len(@Postcode)

	IF @Len > 5
	BEGIN
		SET @Prefix = UPPER(rtrim(substring(@Postcode, 1, @Len - 2)))
		--set @Suffix = UPPER(substring(@Postcode, @Len - 2, 3))
	
		SELECT @Result = count(*) 
		FROM PostCodeLookup 
		WHERE PostCode = @Prefix
	END
	ELSE
	BEGIN
		SET @Result = 0 
	END

	-- Return the result of the function
	RETURN @Result

END









GO
GRANT VIEW DEFINITION ON  [dbo].[fnBNHPostcodeIncluded] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnBNHPostcodeIncluded] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnBNHPostcodeIncluded] TO [sp_executeall]
GO
