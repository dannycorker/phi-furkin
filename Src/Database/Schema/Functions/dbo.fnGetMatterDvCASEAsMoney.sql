SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-06-12
-- Description:	Get MatterDV directly #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetMatterDvCASEAsMoney]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS money
AS
BEGIN

	DECLARE @ValueMoney AS money

	SELECT @ValueMoney = mdv.ValueMoney 
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND mdv.DetailFieldID = @DetailFieldID
	WHERE m.CaseID = @CaseID

	RETURN @ValueMoney

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetMatterDvCASEAsMoney] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetMatterDvCASEAsMoney] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetMatterDvCASEAsMoney] TO [sp_executeall]
GO
