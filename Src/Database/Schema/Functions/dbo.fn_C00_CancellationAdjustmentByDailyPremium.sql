SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		James Lewis
-- Create date: 2018-02-28
-- Description:	Returns a table of pets with live policies
-- Modified: 
-- 2018-07-02 ACE					| Formatted SQL
-- 2020-02-05 CPS for JIRA AAG-91	| Replace TableRow and TableDetailValues references with PurchasedProductPaymentScheduleDetail
-- 2020-03-18 GPR for AAG-512		| Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- 2020-07-15 ALM					| Update to use _C600_ClaimData table and include Duty
-- 2020-09-10 ACE PPET-73			| Add Transaction Fee
-- 2021-04-08 ACE Furkin-429		| Add write off, standardised formatting and fix all policy check > current policy check (dead pet and claim in term)
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_CancellationAdjustmentByDailyPremium]
(
	@MatterID INT,
	@CancellationDate DATE
)
RETURNS 
	@OutPut TABLE 
	(
		FutureBilling DECIMAL (18,2), 
		CoverPaidFor DECIMAL(18,2), 
		CoverValueReceived DECIMAL(18,2),
		AdjustmentGross DECIMAL(18,2),
		AdjustmentNet DECIMAL(18,2),
		AdjustmentVAT DECIMAL(18,2),
		CoverFrom DATE, 
		CoverTo DATE,
		MatterID INT,
		TransactionFee NUMERIC(18,2)
	)
AS
BEGIN
 	 
	DECLARE @PurchasedProductID INT,
			@ProductStart DATE,
			@ProductEnd DATE,
			@ProductLength INT,
			@CoverFrom DATE  ,
			@CoverTo DATE ,
			@CoverValueUsed DECIMAL(18,2),
			@CoverValuePaidFor DECIMAL (18,2),
			@AdjustmentGross DECIMAL (18,2) ,
			@AdjustmentNet DECIMAL (18,2) ,
			@AdjustmentIPT DECIMAL (18,2) ,
			@FutureBilling DECIMAL (18,2) ,
			@ValueMoney DECIMAL(18,2) ,
			@Date DATE,
			@ValueINT INT,
			@CustomerID INT,
			@IPT DECIMAL (18,4) ,
			@PostCode VARCHAR(8),
			@AllowFutureDated BIT = 0 ,/*Set this to 1 if future dated cancellations are allowed*/
			@ValueOfPolicy DECIMAL(18,2), 
			@Duty DECIMAL(18,2),
			@CountryID INT,
            @State VARCHAR(3),
            @PostcodeInt INT,
			@ClientID INT,
			@TransactionFee NUMERIC(18,2),
			@AccountID INT,
			@AccountTypeID INT,
			@DaysUsed INT
	
	SELECT @PurchasedProductID = mdv.ValueInt
	FROM MatterDetailValues mdv WITH (NOLOCK) 
	where mdv.DetailFieldID = 177074
	AND mdv.MatterID = @MatterID

	/*Work out the policy Term we are dealing with,*/ 
	SELECT @ProductStart = p.ValidFrom, 
			@ProductEnd = p.ValidTo,
			@ProductLength = DATEDIFF(DAY,@ProductStart,@ProductEnd) + 1, 
			@ValueOfPolicy = p.ProductCostGross,
			@CustomerID = p.CustomerID,
			@ClientID = p.ClientID,
			@AccountID = p.AccountID
	FROM PurchasedProduct p WITH (NOLOCK) 
	WHERE p.PurchasedProductID = @PurchasedProductID

	/*Get account type for transaction fee purposes*/
	SELECT @AccountTypeID = a.AccountTypeID
	FROM Account a WITH (NOLOCK)
	WHERE a.AccountID = @AccountID

	SELECT @PostCode = LEFT(c.PostCode,3) 
	FROM Customers c WITH (NOLOCK) 
	WHERE c.CustomerID = @CustomerID 
    
	-- 2020-02-05 CPS for JIRA AAG-91	| Replace TableRow and TableDetailValues references with PurchasedProductPaymentScheduleDetail
	/*Find the differing premium periods by checking for MTAs so we ensure an accurate daily value*/
	DECLARE @PeriodsOfInsurance TABLE (GrossPremium DECIMAL(18,2),DailyPremium DECIMAL(18,4), ValidFrom DATE, ValidTo DATE, DaysForPeriod INT)
	INSERT INTO @PeriodsOfInsurance (GrossPremium,ValidFrom,ValidTo,DaysForPeriod)
	SELECT wp.AnnualPriceForRiskGross, wp.ValidFrom, wp.ValidTo, DATEDIFF(DAY,wp.ValidFrom,ISNULL(wp.ValidTo,@ProductEnd))
	FROM WrittenPremium wp WITH (NOLOCK) 
	WHERE (wp.MatterID = @MatterID)
	AND (wp.ValidFrom >= @ProductStart)
	AND (wp.ValidTo <= @ProductEnd or wp.ValidTo is NULL)
	AND (wp.AdjustmentTypeID IN ( 1 /*New*/, 2 /*Renewal*/, 3 /*MTA*/, 6 /*Reinstatement*/ ) )
	AND (wp.ValidFrom <= wp.ValidTo or wp.ValidTo is NULL)
	ORDER BY wp.WrittenPremiumID ASC

	/*Work out the daily rate*/
	UPDATE @PeriodsOfInsurance 
	SET DailyPremium = (GrossPremium/@ProductLength) --* DaysForPeriod

	/*Create a table of Days of policy*/ 
	DECLARE @DaysInCancellationPeriod TABLE (DayID INT IDENTITY, Date DATE, Value DECIMAL(18,4), Paid BIT) 

	INSERT INTO @DaysInCancellationPeriod (Date,Paid)
	SELECT w.CharDate, 0 
	FROM WorkingDays w WITH (NOLOCK) 
	WHERE w.Date BETWEEN @ProductStart and @ProductEnd

	/*!!!!!! NO SQL HERE !!!!!!*/
	SELECT @DaysUsed = @@RowCount

	/*Write the value of each day to the table*/ 
	UPDATE @DaysInCancellationPeriod 
	SET Value = p.DailyPremium 
	FROM @PeriodsOfInsurance p 
	CROSS APPLY @DaysInCancellationPeriod d 
	WHERE d.Date BETWEEN p.ValidFrom and p.ValidTo
	
	/*IF the customer has claimed in this policy term, treat value of cover as total cost of policy*/ 
	/*2021-04-08 ACE - Limit this down to this policy, not all for the customer. Remove redundannt customer join*/
	IF EXISTS (	
		SELECT * 
		FROM Matter m WITH (NOLOCK)
		INNER JOIN _C600_ClaimData cd WITH (NOLOCK) ON cd.PolicyID = m.LeadID /*PolicyID is the PA LeadID*/
		WHERE m.MatterID = @MatterID
		AND cd.ClaimStatusID IN (4470 /*06 Closed - paid*/)
		AND cd.DateOfDeath IS NULL					
		AND (
				cd.ClaimTreatmentStart BETWEEN @ProductStart AND @ProductEnd
				OR
				cd.ClaimTreatmentEnd BETWEEN @ProductStart AND @ProductEnd
			)
	)
	BEGIN 
		
		SELECT @CoverValueUsed =  @ValueOfPolicy,
				@DaysUsed = @ProductLength 
	
	END
	ELSE
	IF @CancellationDate = @ProductStart
	BEGIN 
	
		/*If we are cancelling from the start of the policy, then cover received is 0*/ 
		SELECT @CoverValueUsed = 0,
				@DaysUsed = 0 
	
	END
	ELSE 
	BEGIN 
		
		/*Work out how much cover the customer has received*/ 
		SELECT @CoverValueUsed = SUM(d.Value) 
		FROM @DaysInCancellationPeriod d
		WHERE d.Date <= @CancellationDate 
	
	END
	
	/*Work out how much cover the customer has paid for*/ 
	SELECT @CoverValuePaidFor = ISNULL(SUM(pp.PaymentGross),0)
	FROM PurchasedProductPaymentSchedule pp WITH (NOLOCK) 
	WHERE pp.PurchasedProductID = @PurchasedProductID 
	AND ReconciledDate IS NOT NULL
	AND pp.PaymentStatusID IN (6,2)
	AND pp.PurchasedProductPaymentScheduleTypeID  <> 5
	AND NOT EXISTS (
		SELECT * FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
		WHERE p.PurchasedProductPaymentScheduleParentID = pp.PurchasedProductPaymentScheduleID
		AND p.PaymentStatusID = 4
		AND p.WhenCreated > pp.WhenCreated
	)

	/*IF the customer has claimed in this policy term and the cancellation refund is for death of pet, then no refund is due*/ 
	/*2021-04-08 ACE - Limit this down to this policy, not all for the customer. Remove redundannt customer join*/
	IF EXISTS (	
		SELECT * 
		FROM Matter m WITH (NOLOCK)
		INNER JOIN _C600_ClaimData cd WITH (NOLOCK) ON cd.PolicyID = m.LeadID /*PolicyID is the PA LeadID*/
		JOIN MatterDetailValues mdv WITH (NOLOCK) ON mdv.MatterID = m.MatterID AND mdv.DetailFieldID = 170054 and mdv.ValueInt = 69911 /*Cancellation reason is death of pet*/
		WHERE m.MatterID = @MatterID
		AND cd.ClaimStatusID IN (4470 /*06 Closed - paid*/)
		AND cd.DateOfDeath IS NOT NULL					
		AND 
		(
			cd.ClaimTreatmentStart BETWEEN @ProductStart AND @ProductEnd
			OR
			cd.ClaimTreatmentEnd BETWEEN @ProductStart AND @ProductEnd
		)
		AND EXISTS (
			SELECT * 
			FROM _C600_ClaimData cd1 WITH (NOLOCK) 
			WHERE cd1.PolicyID = m.LeadID
			AND cd1.ClaimID <> cd.ClaimID
		)
	)
	BEGIN 

		SELECT @CoverValueUsed = @CoverValuePaidFor
		FROM @DaysInCancellationPeriod d
		WHERE d.Date <= @CancellationDate 

	END
	
	IF EXISTS ( 
		SELECT * 
		FROM MatterDetailValues mdv with (NOLOCK) 
		WHERE mdv.DetailFieldID = 180168 
		AND mdv.MatterID = @MatterID
		AND ISNULL(mdv.DetailValue,'') <> '' -- The Policy has imported policy number 
		AND NOT EXISTS (
			SELECT * FROM Matter m with (NOLOCK) 
			INNER JOIN LeadEvent le with (NOLOCK) on le.CaseID = m.CaseID 
			WHERE le.EventTypeID IN (150151) 
			AND m.MatterID = @MatterID 
			AND le.EventDeleted = 0
		)
	)
	BEGIN 

		SELECT @ValueOfPolicy = 0.00 , @CoverValuePaidFor = 0, @CoverValueUsed = 0 

	END 


	/*Find the difference*/ 
	SELECT @AdjustmentGross = ISNULL(@CoverValueUsed,0) - ISNULL(@CoverValuePaidFor,0)

	/*if the customer has received more cover than they have paid for, subtract full months they have paid so far to find the additional collection*/ 
	IF @AdjustmentGross > 0 
	BEGIN 

		IF @AllowFutureDated = 1 
		BEGIN
		
			/*Check how many payments we can leave alone,
			i.e. they will be dealt with prior to the month the cancellation is in*/ 
			SELECT @FutureBilling = SUM(PaymentGross)  
			FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
			WHERE CoverFrom < @CancellationDate 
			AND CoverTo <= @CancellationDate 
			AND PurchasedProductID = @PurchasedProductID
			AND PaymentStatusID IN (1,5,7)
			AND PurchasedProductPaymentScheduleTypeID  <> 5

			SELECT @AdjustmentGross = @AdjustmentGross - ISNULL(@FutureBilling,0)
			
			SELECT @ValueMoney = @AdjustmentGross
		
		END
		ELSE 
		BEGIN
		
			SELECT @FutureBilling = 0 
		
		END
		
		------/* TO MAKE THE BELOW CHANGE, WE NEED TO CHANGE THE METHOD OF SETTING THE COVER FROM AND COVER TO IN THE FIRST INSTANCE
		------ AS THE COVER FROM AND TO DO NOT REFLECT THE PREMIUM VALUE COVERED BY THE PERIOD	     
				--To work out the exact value of the cover from and cover to we need to test the value of the adjustment against the daily Value.
		------As we are doing an additional colleciton, we do this by starting from the cancellation date (as cancellation is in a period we have yet to bill 
		------for so we start here and count back) and marking off each day as paid until we use up our adjustment value*/ 
		------WHILE @ValueMoney > 0
		------BEGIN 
		
		------	SELECT TOP 1 @ValueINT = DayID  FROM @DaysInCancellationPeriod 
		------	WHERE Paid = 0
		------	AND Date < @CancellationDate
		------	ORDER BY Date desc
			
		------	SELECT @ValueMoney=@ValueMoney - Value FROM @DaysInCancellationPeriod 
		------	WHERE DayID = @ValueINT 
			
		------	UPDATE @DaysInCancellationPeriod 
		------	SET Paid = 1 
		------	WHERE DayID = @ValueINT
			
		------END
		
		------SELECT * 
		------FROM @DaysInCancellationPeriod
		
		/*FOR NOW, JSUT ENSURE THE DAYS ARE CONTINUOUS*/ 
		SELECT @CoverTo = @CancellationDate

		/*Set the Cover From of the adjustment to be one day greater than the last full period before the cancellation date,
		if there is no full period, then use the policy start date*/ 
		/*I.e. Adjustment covering an additional collection from last cover date to cancellation date*/
		SELECT TOP 1 @CoverFrom = DATEADD(DAY,1,CoverTo)
		FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
		WHERE CoverFrom < @CancellationDate 
		AND CoverTo < @CancellationDate 
		AND PurchasedProductID = @PurchasedProductID
		AND PurchasedProductPaymentScheduleTypeID  <> 5
		ORDER BY PurchasedProductPaymentScheduleID DESC
			
		SELECT @CoverFrom = ISNULL(@CoverFrom,@ProductStart) 

	END 
	ELSE
	BEGIN 

		------/* TO MAKE THE BELOW CHANGE, WE NEED TO CHANGE THE METHOD OF SETTING THE COVER FROM AND COVER TO IN THE FIRST INSTACNE
		------ AS THE COVER FROM AND TO DO NOT REFLECT THE PREMIUM VALUE COVERED BY THE PERIOD	
		/*To work out the exact value of the cover from and cover to we need to test the value of the adjustment against the daily Value.
		As we are doing an additional colleciton, we do this by starting from the cancellation date (as cancellation is in a period we have yet to bill 
		for so we start here and count back) and marking off each day as paid until we use up our adjustment value*/ 
		--WHILE @ValueMoney < 0
		--BEGIN 
		
		--	SELECT TOP 1 @ValueINT = DayID  FROM @DaysInCancellationPeriod 
		--	WHERE Paid = 0
		--	AND Date > @CancellationDate
		--	ORDER BY Date DESC
			
		--	SELECT @ValueMoney=@ValueMoney + Value FROM @DaysInCancellationPeriod 
		--	WHERE DayID = @ValueINT 
			
		--	UPDATE @DaysInCancellationPeriod 
		--	SET Paid = 1 
		--	WHERE DayID = @ValueINT
			
		--END
		
		--SELECT @CoverTo = @CancellationDate
		--SELECT @CoverFrom = 
		
		--SELECT  top 1 @CoverFrom = Date FROM @DaysInCancellationPeriod 
		--WHERE Paid = 1 
		--ORDER BY DATE ASC
		
		/*As this is a refund set the cover To to be the cover to of the period in which the cancellation it occured*/
		/*I.e. Adjustment covering a refund from Cancellation date to end of period already paid*/
		SELECT @CoverFrom = @CancellationDate 
		
		SELECT TOP 1 @CoverTo = CoverTo
		FROM PurchasedProductPaymentSchedule WITH (NOLOCK) 
		WHERE CoverFrom < @CancellationDate 
		AND CoverTo >= @CancellationDate 
		AND PurchasedProductID = @PurchasedProductID
		AND PurchasedProductPaymentScheduleTypeID <> 5
		
		IF @CoverTo IS NULL
		BEGIN 
			SELECT @CoverTo = @CancellationDate
		END

	END 
	
    IF @ClientID IS NULL
    BEGIN
        SELECT @ClientID = 604
    END

	SELECT @CountryID = CountryID 
	FROM Clients WITH (NOLOCK) 
	WHERE ClientID = @ClientID

	IF @CountryID = 14 /*Australia*/ AND dbo.fnIsInt(@Postcode) = 1 -- 2020-02-23 CPS for JIRA AAG-186 | Don't hit this section if we have a narrative Postcode
    BEGIN

        SELECT @PostcodeInt = CONVERT(int, @Postcode)

        /*
            Australian States / Territory        
            NSW - New South Wales
            ACT - Australian Capital Territory
            VIC - Victoria
            QLD - Queensland
            SA - South Australia
            WA - Western Australia
            TAS - Tasmania
            NT - Northern Territory
        */

        SELECT @State = CASE
                            WHEN @PostcodeInt BETWEEN 200 AND 299 THEN 'ACT'
                            WHEN @PostcodeInt BETWEEN 800 AND 999 THEN 'NT'
                            WHEN @PostcodeInt BETWEEN 1000 AND 2599 THEN 'NSW'
                            WHEN @PostcodeInt BETWEEN 2600 AND 2618 THEN 'ACT'
                            WHEN @PostcodeInt BETWEEN 2619 AND 2899 THEN 'NSW'
                            WHEN @PostcodeInt BETWEEN 2900 AND 2920 THEN 'ACT'
                            WHEN @PostcodeInt BETWEEN 2921 AND 2999 THEN 'NSW'
                            WHEN @PostcodeInt BETWEEN 3000 AND 3999 THEN 'VIC'
                            WHEN @PostcodeInt BETWEEN 4000 AND 4999 THEN 'QLD'
                            WHEN @PostcodeInt BETWEEN 5000 AND 5999 THEN 'SA'
                            WHEN @PostcodeInt BETWEEN 6000 AND 6999 THEN 'WA'
                            WHEN @PostcodeInt BETWEEN 7000 AND 7999 THEN 'TAS'
                            WHEN @PostcodeInt BETWEEN 8000 AND 8999 THEN 'VIC'
                            WHEN @PostcodeInt BETWEEN 9000 AND 9999 THEN 'QLD'
                            ELSE NULL
                        END
	END

	/*Now work out the IPT rate*/ 
    SELECT TOP(1) @Duty = d.GrossMultiplier 
    FROM Duty d WITH (NOLOCK) 
    WHERE d.CountryID= @CountryID
    AND (d.[From] IS NULL OR @CoverFrom >= d.[From])
	AND (d.[To] IS NULL OR @CoverFrom <= d.[To])
    AND (d.State LIKE '%' + @State + '%')
							
	SELECT @IPT = i.GrossMultiplier 
	FROM IPT i WITH (NOLOCK) 
	WHERE i.Region like '%' + @PostCode + '%'
	
	IF @IPT IS NULL
	BEGIN

		SELECT TOP 1 @IPT=i.GrossMultiplier 
		FROM IPT i WITH (NOLOCK) 
		WHERE i.CountryID = 232
		AND (i.[From] IS NULL OR @CancellationDate  >= i.[From])
		AND (i.[To] IS NULL OR @CancellationDate <= i.[To])
		AND i.Region IS NULL

	END
	
	SELECT @AdjustmentNet = @AdjustmentGross / (@IPT + @Duty)
	SELECT @AdjustmentIPT = @AdjustmentGross - @AdjustmentNet 

	/*
		Finally work out the transaction fee
		We cant do this using the normal pro rata calcs as there may be a free month 
		that will need spreading over a 335/6 day year

		Moved to own function as it was getting a little long..
	*/

	SELECT @TransactionFee = dbo.fn_C00_CancellationFeeAdjustmentByDailyPremium(@MatterID, @PurchasedProductID, @ProductLength, @DaysUsed, @AccountTypeID)

	INSERT INTO @OutPut (FutureBilling, CoverPaidFor, CoverValueReceived, AdjustmentGross,AdjustmentNet,AdjustmentVAT, CoverFrom, CoverTo,MatterID, TransactionFee) 
	VALUES (@FutureBilling, @CoverValuePaidFor, @CoverValueUsed, @AdjustmentGross, @AdjustmentNet , @AdjustmentIPT, @CoverFrom , @CoverTo, @MatterID, @TransactionFee)

	RETURN

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_CancellationAdjustmentByDailyPremium] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_CancellationAdjustmentByDailyPremium] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_CancellationAdjustmentByDailyPremium] TO [sp_executeall]
GO
