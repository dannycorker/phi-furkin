SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-05-08
-- Description:	Get LeadDV directly #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetLeadDvCASE]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE @DetailValue AS VARCHAR(2000)

	SELECT @DetailValue = ldv.DetailValue 
	FROM Cases c WITH (NOLOCK) 
	INNER JOIN LeadDetailValues ldv WITH (NOLOCK) ON c.LeadID = ldv.LeadID AND ldv.DetailFieldID = @DetailFieldID
	WHERE c.CaseID = @CaseID 
	
	RETURN @DetailValue

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLeadDvCASE] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetLeadDvCASE] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetLeadDvCASE] TO [sp_executeall]
GO
