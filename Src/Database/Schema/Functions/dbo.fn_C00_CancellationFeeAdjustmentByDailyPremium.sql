SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2020-09-10
-- Description:	Calculate the cancelation transaction fee..
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_CancellationFeeAdjustmentByDailyPremium]
(
	@MatterID INT,
	@PurchasedProductID INT,
	@NumberOfDaysInYear INT,
	@DaysUsed INT,
	@AccountTypeID INT /*1 = Bank, 2 = CC*/
)
RETURNS NUMERIC(18,2)
AS
BEGIN
	
	DECLARE @TransactionFeeAdjustment NUMERIC(18,2),
			@TransactionFeeFreeDays INT = 0,
			@TotalFees NUMERIC(18,2),
			@UsedFees NUMERIC(18,2),
			@DailyFee NUMERIC(18,14),
			@PaidFees NUMERIC(18,2)

	IF @AccountTypeID = 1 /*DD*/
	BEGIN

		IF EXISTS (
			SELECT *
			FROM PurchasedProduct pp WITH (NOLOCK)
			WHERE pp.ObjectID = @MatterID
			AND pp.PurchasedProductID < @PurchasedProductID
		)
		BEGIN

			/*
				If there is an earlier PP then we are on the nth term
				As such we will charge a full years worth of days

				NB its more efficient to do an exists than not exists so re-setting the var here..
			*/
			SELECT @TransactionFeeFreeDays = 0

		END
		ELSE
		BEGIN

			/*
				There is no earlier PP so we are on year 1. 
				As we are also DD we have not been charged a TF on the first month
			*/
			SELECT @TransactionFeeFreeDays = 30

		END

	END

	SELECT @PaidFees = SUM(ppps.TransactionFee)
	FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
	WHERE ppps.PurchasedProductID = @PurchasedProductID 
	AND ReconciledDate IS NOT NULL
	AND ppps.PaymentStatusID IN (6,2)
	AND ppps.PurchasedProductPaymentScheduleTypeID  <> 5
	AND NOT EXISTS (
		SELECT * FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
		WHERE p.PurchasedProductPaymentScheduleParentID = ppps.PurchasedProductPaymentScheduleID
		AND p.PaymentStatusID = 4
		AND p.WhenCreated > ppps.WhenCreated
	)

	SELECT @TotalFees = SUM(ppps.TransactionFee)
	FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
	WHERE ppps.PurchasedProductID = @PurchasedProductID 
	AND (
			(
				ReconciledDate IS NOT NULL
				AND ppps.PaymentStatusID IN (6,2)
				AND ppps.PurchasedProductPaymentScheduleTypeID  <> 5
				AND NOT EXISTS (
					SELECT * FROM PurchasedProductPaymentSchedule p WITH (NOLOCK) 
					WHERE p.PurchasedProductPaymentScheduleParentID = ppps.PurchasedProductPaymentScheduleID
					AND p.PaymentStatusID = 4
					AND p.WhenCreated > ppps.WhenCreated
				)
			)
			OR
			(
				ppps.PaymentStatusID = 1 /*New*/
			)
	)

	/* 
		EG 
		2n per month = 24n
		365 days in year, first month dd so take off 30 = 335
		Daily Fee = 24.00 / 335.00 = 0.07164179104477
	*/
	SELECT @DailyFee = CONVERT(NUMERIC(18,14),@TotalFees) / CONVERT(NUMERIC(18,14),(@NumberOfDaysInYear - ISNULL(@TransactionFeeFreeDays, 0)))

	/*
		EG
		0.07164179104477 * Days used, 30 = 2.15
	*/
	SELECT @UsedFees = @DailyFee * CONVERT(NUMERIC(18,14),@DaysUsed)

	/*
		Now we know how much as been used and how much has been paid 
		for we need to figure out what the residual amount it.

		IE 
		If we have recieved 6 but used 5 then we need a refund of 1
		If we have recieved 4 but used 5 then we need a payment of 1
	*/

	SELECT @TransactionFeeAdjustment = (@PaidFees - @UsedFees)*-1.00

	RETURN @TransactionFeeAdjustment

END
GO
GRANT EXECUTE ON  [dbo].[fn_C00_CancellationFeeAdjustmentByDailyPremium] TO [sp_executeall]
GO
