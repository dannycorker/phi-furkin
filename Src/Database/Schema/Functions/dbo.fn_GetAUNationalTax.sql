SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-04-23
-- Description:	Returns the GST for Australia
-- =============================================
CREATE FUNCTION [dbo].[fn_GetAUNationalTax]
(
	@ClientID INT
)
RETURNS DECIMAL(18,2)	
AS
BEGIN

	DECLARE @GST DECIMAL(18,2)

	IF @ClientID = 604
	BEGIN
		SELECT @GST = 1.10
	END

	RETURN ISNULL(@GST,1.0)
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_GetAUNationalTax] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_GetAUNationalTax] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_GetAUNationalTax] TO [sp_executeall]
GO
