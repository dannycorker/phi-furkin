SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2011-07-19
-- Description:	Only returns Note Types that this user has the appropriate rights for,
--              as specified by the caller.
--Modified	2014-07-14	SB	Changed to use the new view for lead type sharing
-- =============================================
CREATE FUNCTION [dbo].[fnNoteTypeSecure] 
(	
	@UserID INT, 
	@MinimumRightsRequired INT /* 0=None, 1=View, 2=Add, 3=Edit, 4=Full */
)
RETURNS 
@Data TABLE 
(
	ObjectID INT,
	RightID INT
)
AS
BEGIN

	DECLARE @ClientID INT
	SELECT @ClientID = ClientID
	FROM dbo.ClientPersonnel WITH (NOLOCK) 
	WHERE ClientPersonnelID = @UserID

	;WITH UserGroup AS
	(
		SELECT ClientPersonnelAdminGroupID AS [GroupID], ClientID
		FROM ClientPersonnel 
		WHERE ClientPersonnelID = @UserID
	),
	UserRights AS
	(
		SELECT grd.ObjectID, grd.RightID
		FROM dbo.GroupRightsDynamic grd 
		INNER JOIN UserGroup ug ON ug.GroupID = grd.ClientPersonnelAdminGroupID 
		WHERE grd.FunctionTypeID = 26 
		AND grd.RightID >= @MinimumRightsRequired
		AND NOT EXISTS (SELECT * FROM dbo.UserRightsDynamic urd WHERE urd.FunctionTypeID = grd.FunctionTypeID AND urd.ObjectID = grd.ObjectID AND urd.ClientPersonnelID = @UserID)
		UNION
		SELECT urd.ObjectID, urd.RightID
		FROM dbo.UserRightsDynamic urd 
		WHERE urd.ClientPersonnelID = @UserID
		AND urd.FunctionTypeID = 26
		AND urd.RightID >= @MinimumRightsRequired 
	),
	UserFunctions26 AS 
	(
		SELECT gfc.ModuleID, gfc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, gfc.HasDescendants, gfc.RightID 
		FROM GroupFunctionControl gfc 
		INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID AND ft.ModuleID = gfc.ModuleID 
		WHERE gfc.ClientPersonnelAdminGroupID = (SELECT GroupID FROM UserGroup)
		AND gfc.FunctionTypeID = 26
		AND NOT EXISTS (SELECT * FROM UserFunctionControl ufc1 WHERE ufc1.functiontypeid = gfc.functiontypeid AND ufc1.ClientPersonnelID = @UserID)
		UNION
		SELECT ufc.ModuleID, ufc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, ufc.HasDescendants, ufc.RightID 
		FROM UserFunctionControl ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID AND ft.ModuleID = ufc.ModuleID 
		WHERE ufc.ClientPersonnelID = @UserID
		AND ufc.FunctionTypeID = 26
	),
	UserLeadManagerModule AS 
	(
		SELECT gfc.ModuleID, gfc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, gfc.HasDescendants, gfc.RightID 
		FROM GroupFunctionControl gfc 
		INNER JOIN FunctionType ft ON ft.FunctionTypeID = gfc.FunctionTypeID AND ft.ModuleID = gfc.ModuleID 
		WHERE gfc.ClientPersonnelAdminGroupID = (SELECT GroupID FROM UserGroup)
		AND gfc.FunctionTypeID = 1
		AND NOT EXISTS (SELECT * FROM UserFunctionControl ufc1 WHERE ufc1.functiontypeid = gfc.functiontypeid AND ufc1.ClientPersonnelID = @UserID)
		UNION
		SELECT ufc.ModuleID, ufc.FunctionTypeID, ft.ParentFunctionTypeID, ft.Level, ufc.HasDescendants, ufc.RightID 
		FROM UserFunctionControl ufc INNER JOIN FunctionType ft ON ft.FunctionTypeID = ufc.FunctionTypeID AND ft.ModuleID = ufc.ModuleID 
		WHERE ufc.ClientPersonnelID = @UserID
		AND ufc.FunctionTypeID = 1
	)
	/*
		A user has one of these things: 
		(1) a full set of individual records specifying access to each individual note type ("UserRights" above)
		or
		(2) an overall record specifying that they can or cannot see all note types ("UserFunctions26" above)
		or
		(3) an overall Lead Manager module control record
	*/
	/* Get all individual records (if any exist) */
	INSERT @Data(ObjectID, RightID)
	SELECT UserRights.ObjectID, UserRights.RightID
	FROM UserRights

	UNION
	/* Get NoteType FunctionControl record (if and only if it claims that no descendants exist) */
	/* Join this to all NoteType records for this client */
	SELECT nt.NoteTypeID, UserFunctions26.RightID 
	FROM dbo.fnNoteTypeShared(@ClientID) nt
	INNER JOIN UserGroup ON nt.ClientID = UserGroup.ClientID 
	CROSS JOIN UserFunctions26
	WHERE UserFunctions26.RightID >= @MinimumRightsRequired 
	AND UserFunctions26.HasDescendants = 0 

	UNION
		
	/* Get the top-level LeadManager Module record (if and only if it claims that no descendants exist) */
	/* Join this to all NoteType records for this client */
	SELECT nt.NoteTypeID, RightID 
	FROM dbo.fnNoteTypeShared(@ClientID) nt 
	CROSS JOIN UserLeadManagerModule 
	INNER JOIN UserGroup ug ON nt.ClientID = ug.ClientID 
	WHERE NOT EXISTS (SELECT * FROM UserFunctions26 WHERE RightID = 0 AND HasDescendants = 0)
	AND UserLeadManagerModule.RightID >= @MinimumRightsRequired 
	AND UserLeadManagerModule.HasDescendants = 0 
	
	RETURN

END





GO
GRANT VIEW DEFINITION ON  [dbo].[fnNoteTypeSecure] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnNoteTypeSecure] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnNoteTypeSecure] TO [sp_executeall]
GO
