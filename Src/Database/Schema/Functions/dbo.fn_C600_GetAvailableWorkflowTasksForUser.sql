SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-12-02
-- Description:	Take a ClientPersonnelID and return all tasks that user may pick up
-- 2017-12-04 CPS add instruction field
-- 2018-04-06 CPS remove call to fn_C433_GetWorkflowTaskTypesForUser.  No need.
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetAvailableWorkflowTasksForUser]
(
	 @ClientPersonnelID	INT = 58546 
)
RETURNS @Data TABLE
(
	 ClientPersonnelID	INT
	,TableRowID			INT
	,ResourceListID		INT
	,TaskType			VARCHAR(2000)
	,TaskPriority		DECIMAL(18,2)
	,WorkflowGroupID	INT
	,WorkflowTaskID		INT
	,LeadID				INT
	,CaseID				INT
	,CreationDate		DATETIME
	,AssignedDate		DATETIME
	,Instruction		VARCHAR(2000)
	,SlaDays			INT
)
AS
BEGIN


	INSERT @Data
	SELECT @ClientPersonnelID
		 , 0 [TableRowID] --defunct
		 , rdv.ResourceListID
		 , ISNULL(rdvType.DetailValue,wg.Name) [TaskType]
		 , wga.Priority
		 , wg.WorkflowGroupID
		 , wt.WorkflowTaskID
		 , wt.LeadID
		 , wt.CaseID
		 , wt.CreationDate
		 , wt.AssignedDate
		 , ISNULL(rdvInst.DetailValue,wg.Description) [Instruction]
		 , ISNULL(rdvSla.ValueInt,3) [SlaDays]
	FROM WorkflowTask wt WITH ( NOLOCK )
	INNER JOIN WorkflowGroup wg WITH ( NOLOCK ) on wg.WorkflowGroupID = wt.WorkflowGroupID
	--LEFT JOIN dbo.fn_C433_GetWorkflowTaskTypesForUser(@ClientPersonnelID) fn on fn.WorkflowGroupID = wt.WorkflowGroupID
	LEFT JOIN WorkflowGroupAssignment wga WITH ( NOLOCK ) on wga.WorkflowGroupID = wg.WorkflowGroupID AND wga.ClientPersonnelID = @ClientPersonnelID
	LEFT JOIN ResourceListDetailValues rdv WITH ( NOLOCK ) on rdv.DetailFieldID = 180151 /*WorkflowGroupID*/ AND rdv.ValueInt = wg.WorkflowGroupID
	LEFT JOIN ResourceListDetailValues rdvType WITH ( NOLOCK ) on rdvType.ResourceListID = rdv.ResourceListID AND rdvType.DetailFieldID = 180134 /*Name*/
	LEFT JOIN ResourceListDetailValues rdvInst WITH ( NOLOCK ) on rdvInst.ResourceListID = rdv.ResourceListID AND rdvInst.DetailFieldID = 180137 /*Instruction*/
	LEFT JOIN ResourceListDetailValues rdvSla WITH ( NOLOCK ) on rdvSla.ResourceListID = rdv.ResourceListID AND rdvSla.DetailFieldID = 180136 /*SLA Days*/
	WHERE ( ( wt.AssignedTo is NULL and wga.ClientPersonnelID = @ClientPersonnelID ) or wt.AssignedTo = @ClientPersonnelID )
	AND wt.Escalated = 0
	AND dbo.fnUserLeadAccess(@ClientPersonnelID,wt.LeadID) > 0
	ORDER BY wt.AssignedTo DESC, wt.AssignedDate ASC, [SlaDays] DESC, wga.[Priority], wt.CreationDate ASC

	RETURN

END











GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetAvailableWorkflowTasksForUser] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_GetAvailableWorkflowTasksForUser] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetAvailableWorkflowTasksForUser] TO [sp_executeall]
GO
