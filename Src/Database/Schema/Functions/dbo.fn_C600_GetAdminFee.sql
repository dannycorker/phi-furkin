SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-05-24
-- Description:	Get the system default admin fee.  Stub function for now, can be expanded as additional requirements are captured.
-- 2017-10-12 CPS added a @CustomerID is not NULL filter because QuoteAndBuy is returning an admin fee of 0.00
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetAdminFee]
(
	@PolicyMatterID	INT
)
RETURNS DECIMAL(18,2)
AS
BEGIN
	
	DECLARE  @ReturnValue	DECIMAL(18,2)
			,@CustomerID	INT
			,@ClientID		INT = dbo.fnGetPrimaryClientID()
					
	SELECT @CustomerID = m.CustomerID, @ClientID = m.ClientID
	FROM Matter m WITH ( NOLOCK ) 
	WHERE m.MatterID = @PolicyMatterID
	
	IF ISNULL(dbo.fnGetSimpleDvAsInt(170257,@CustomerID),0) /*Send documents by*/ IN ( 0,60789 ) /*Email*/ 
		AND @CustomerID is not NULL /*CPS 2017-10-12*/
	BEGIN
		SELECT @ReturnValue = 0.00
	END
	ELSE
	IF @PolicyMatterID > 0
	BEGIN
		SELECT @ReturnValue = dbo.fnGetSimpleDvAsMoney(177668,@PolicyMatterID) /*Admin Fee to Use*/
	END
	
	IF @ReturnValue is NULL /*CPS 2017-08-15 #44575*/	
	BEGIN
		SELECT @ReturnValue = dbo.fnGetSimpleDvAsMoney(177456,@ClientID) /*Default Admin Fee*/
	END

	RETURN @ReturnValue

END









GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetAdminFee] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetAdminFee] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetAdminFee] TO [sp_executeall]
GO
