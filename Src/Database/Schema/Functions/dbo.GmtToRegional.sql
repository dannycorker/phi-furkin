SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_NULLS OFF
GO
CREATE FUNCTION [dbo].[GmtToRegional] (@gmt [datetime], @region [nvarchar] (4000))
RETURNS [datetime]
WITH EXECUTE AS CALLER
EXTERNAL NAME [SqlServerFunctions].[Aquarium.DateTime.TimeZone].[GmtToRegional]
GO
GRANT VIEW DEFINITION ON  [dbo].[GmtToRegional] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[GmtToRegional] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[GmtToRegional] TO [sp_executeall]
GO
