SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-02-27
-- Description:	Return a matter value or resource value
-- =============================================
CREATE FUNCTION [dbo].[fnGetMatterOrResValue]
(
	@MatterID			int,
	@DetailFieldID		int, /* L/M Field */
	@ResourceFieldID	int  /* ResourceList Detail Field ID */
)
RETURNS varchar(2000)
AS
BEGIN

DECLARE @DetailValue as varchar(2000)

	IF @ResourceFieldID > 0
	BEGIN

		Select @DetailValue = COALESCE(luli.ItemValue, rldv.DetailValue)
		From Matter m with (nolock) 
		Inner Join MatterDetailValues mdv with (nolock) on m.MatterID = mdv.MatterID and mdv.DetailFieldID = @DetailFieldID
		Inner Join ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = mdv.ValueInt and rldv.DetailFieldID = @ResourceFieldID
		LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = rldv.ValueInt
		Where m.MatterID = @MatterID
	
	END
	ELSE
	BEGIN
	
		Select @DetailValue = COALESCE(luli.ItemValue, mdv.DetailValue)
		From Matter m with (nolock) 
		Inner Join MatterDetailValues mdv with (nolock) on m.MatterID = mdv.MatterID and mdv.DetailFieldID = @DetailFieldID
		LEFT JOIN LookupListItems luli WITH (NOLOCK) ON luli.LookupListItemID = mdv.ValueInt
		Where m.MatterID = @MatterID

	END
	
	RETURN @DetailValue

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetMatterOrResValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetMatterOrResValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetMatterOrResValue] TO [sp_executeall]
GO
