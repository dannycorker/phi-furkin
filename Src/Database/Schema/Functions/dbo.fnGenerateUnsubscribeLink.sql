SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2012-02-09
-- Description:	Generate an unsubscribe link
-- 2016-04-06 ACE Updated to include ClientID as h #37563
-- =============================================
CREATE FUNCTION [dbo].[fnGenerateUnsubscribeLink]
(
	@LeadID INT
)
RETURNS VARCHAR(250)
AS
BEGIN
	DECLARE @Link VARCHAR(250)

	SELECT @Link = 'https://aqnet.aquarium-software.com/CustomPages/Client_0/LinkEventHandler.aspx?s=' + dbo.fnHashLeadID (@LeadID) + '&l=' + CAST(@LeadID AS VARCHAR) + '&em=' + c.EmailAddress + '&a=0&h=' +  dbo.fnRefLetterFromCaseNum(l.ClientID)
	FROM dbo.Lead l WITH (NOLOCK) 
	INNER JOIN dbo.Customers c WITH (NOLOCK) ON c.CustomerID = l.CustomerID 
	WHERE l.LeadID = @LeadID

	RETURN @Link

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGenerateUnsubscribeLink] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGenerateUnsubscribeLink] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGenerateUnsubscribeLink] TO [sp_executeall]
GO
