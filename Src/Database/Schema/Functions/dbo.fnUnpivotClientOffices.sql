SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Unpivot the columns in the ClientOffices table 
-- =============================================
CREATE FUNCTION [dbo].[fnUnpivotClientOffices] 
(	
	@ClientOfficeID int
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT ColumnName as [RawColumnName], '[!' + ColumnName + ']' as [FormattedColumnName], RealValue
	FROM
	(
		SELECT 
		COALESCE(CONVERT(varchar(255), c.Address1), '') as [Address1],
		COALESCE(CONVERT(varchar(255), c.Address2), '') as [Address2],
		COALESCE(CONVERT(varchar(255), c.BuildingName), '') as [BuildingName],
		COALESCE(CONVERT(varchar(255), c.ClientID), '') as [ClientID],
		COALESCE(CONVERT(varchar(255), c.ClientOfficeID), '') as [ClientOfficeID],
		COALESCE(CONVERT(varchar(255), c.Country), '') as [Country],
		COALESCE(CONVERT(varchar(255), c.CountryID), '') as [CountryID],
		COALESCE(CONVERT(varchar(255), c.County), '') as [County],
		COALESCE(CONVERT(varchar(255), c.LanguageID), '') as [LanguageID],
		COALESCE(CONVERT(varchar(255), c.OfficeFax), '') as [OfficeFax],
		COALESCE(CONVERT(varchar(255), c.OfficeName), '') as [OfficeName],
		COALESCE(CONVERT(varchar(255), c.OfficeTelephone), '') as [OfficeTelephone],
		COALESCE(CONVERT(varchar(255), c.OfficeTelephoneExtension), '') as [OfficeTelephoneExtension],
		COALESCE(CONVERT(varchar(255), c.OfficeTelephone), '') as [Telephone], -- overload for telephone
		COALESCE(CONVERT(varchar(255), c.OfficeTelephoneExtension), '') as [TelephoneExtension],		
		COALESCE(CONVERT(varchar(255), c.PostCode), '') as [PostCode],
		COALESCE(CONVERT(varchar(255), c.SubClientID), '') as [SubClientID],
		COALESCE(CONVERT(varchar(255), c.Town), '') as [Town]
		FROM dbo.ClientOffices c WITH (NOLOCK) 
		WHERE c.ClientOfficeID = @ClientOfficeID) p
	UNPIVOT
	(
		RealValue FOR ColumnName IN
		(
			[Address1],[Address2],[BuildingName],[ClientID],[ClientOfficeID],[Country],[CountryID],[County],[LanguageID],[OfficeFax],[OfficeName],[OfficeTelephone],[OfficeTelephoneExtension],[Telephone],[TelephoneExtension],[PostCode],[SubClientID],[Town]
		)
	) as unpvt

)





GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotClientOffices] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnUnpivotClientOffices] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotClientOffices] TO [sp_executeall]
GO
