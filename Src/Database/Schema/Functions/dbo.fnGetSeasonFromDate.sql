SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2014-03-19 
-- Description:	Originally intended for financial years, eg "which financial year does this policy claim fall into"
--              For a given financial year end date (eg April 1st), return the season it falls into (eg 2014/15)
-- =============================================
CREATE FUNCTION [dbo].[fnGetSeasonFromDate] 
(
	@FromDate DATE,						/* eg Accident date, FA Cup Final date, Payment date */
	@SeasonYearMonthCutoff TINYINT = 4, /* eg 12 for December */
	@SeasonYearDayCutoff TINYINT = 1	/* eg 31 for Dec 31st */

)
RETURNS VARCHAR(7) 
AS
BEGIN
	
	DECLARE @SeasonStartDayNo INT, 
	@FromYear INT, 
	@FromDayNo INT,
	@SeasonYearStart DATE
	
	/* Get the calendar year of the accident, game, payment etc */
	SELECT @FromYear = YEAR(@FromDate)

	/* Get the season start for that calendar year */
	SELECT @SeasonYearStart = CAST(CAST(@FromYear AS VARCHAR) + '-' + CAST(@SeasonYearMonthCutoff AS VARCHAR) + '-' + CAST(@SeasonYearDayCutoff AS VARCHAR) AS DATE)

	/* Get the exact season start day number for that calendar year (handles leap years) */
	SELECT @SeasonStartDayNo = DATEPART(dy,@SeasonYearStart)

	/* Get the exact day number of the accident, game, payment etc for that calendar year (also handles leap years) */
	SELECT @FromDayNo = DATEPART(dy,@FromDate)

	/*SELECT @FromDate AS FromDate, 
	@SeasonYearStart AS SeasonYearStart, 
	@SeasonStartDayNo AS SeasonStartDayNo, 
	@FromDayNo AS FromDayNo */
	
	/* Build up a string of 2014/15 or 2013/14 depending on whether or not the accident happened on/after the season start */
	RETURN CASE WHEN @FromDayNo >= @SeasonStartDayNo THEN CAST(@FromYear AS VARCHAR) + '/' + RIGHT(CAST(@FromYear + 1 AS VARCHAR), 2) ELSE CAST(@FromYear - 1 AS VARCHAR) + '/' + RIGHT(CAST(@FromYear AS VARCHAR), 2) END 

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSeasonFromDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetSeasonFromDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSeasonFromDate] TO [sp_executeall]
GO
