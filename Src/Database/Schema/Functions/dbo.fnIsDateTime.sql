SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2009-08-20
-- Description:	Check to see if a varchar contains a valid datetime.
-- This is for the Datetime2 datatype, where years 0000 to 9999 are valid, and IsDate doesn't work!
-- =============================================
CREATE FUNCTION [dbo].[fnIsDateTime] 
(
	@value varchar(2000)
)
RETURNS bit
AS
BEGIN

	DECLARE @IsDateTime bit
	
	SELECT @value = ltrim(rtrim(@value))
	
	IF dbo.fnIsDate(@value) = 0
	BEGIN
		SET @IsDateTime = 0
	END
	ELSE
	BEGIN

		/* Assume it is a date by default at this point */
		SET @IsDateTime = 1

		/* If the date is still valid, have a go at the time next (if there is one) */
		IF datalength(@value) > 10
		BEGIN
		
			/* 
				Date takes up 10 chars exactly, so with one separator between date and time, 
				plus a minimum of HH:MM for the time, then 16 digits is the shortest it
				can possibly be, given that it did not end at char 10 
			*/
			IF datalength(@value) > 15
			BEGIN
			
				SET @IsDateTime = dbo.fnIsTime(substring(@value, 12, len(@value) - 11))
				
			END
			ELSE
			BEGIN
				SET @IsDateTime = 0
			END
			
		END
		
	END
	
	RETURN @IsDateTime
			
END










GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsDateTime] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnIsDateTime] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsDateTime] TO [sp_executeall]
GO
