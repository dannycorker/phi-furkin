SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jim Green
-- Create date: 2011-06-02
-- Description:	Check to see if a varchar contains a valid bit, including "true" and "false"
-- =============================================
CREATE FUNCTION [dbo].[fnIsBit] 
(
	@value varchar(2000)
)
RETURNS bit
AS
BEGIN
	
	SELECT @value = ltrim(rtrim(@value))
	
	RETURN CASE
			WHEN @value IS NULL THEN 0						/* Can't work with NULL */
			WHEN @value = '' THEN 1							/* Empty = false = 0 */
			WHEN @value = 'true' THEN 1						/* true = 1 */
			WHEN @value = 'false' THEN 1					/* false = 0 */
			WHEN datalength(@value) > 38 THEN 0				/* Too long for a bigint, so fail */
			WHEN PatIndex('%[^0-9]%', @value) = 0 THEN 1	/* Contains non-numerics, so fail. Even a minus sign is a fail. */
			ELSE 0
			END
			
END










GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsBit] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnIsBit] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsBit] TO [sp_executeall]
GO
