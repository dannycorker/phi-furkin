SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-04-02
-- Description:	Returns the State code by Country from PostCode
-- =============================================
CREATE FUNCTION [dbo].[fn_GetAUStateFromPostCode]
(
	@CustomerID INT
)
RETURNS VARCHAR(3)	
AS
BEGIN

	DECLARE @ClientID INT,
			@CountryID INT,
			@State VARCHAR(3),
			@PostCode VARCHAR(4),
			@PostcodeInt INT

	/*Get ClientID and CountryID*/
	SELECT @ClientID = ClientID, @PostCode = PostCode FROM Customers WITH (NOLOCK) WHERE CustomerID = @CustomerID
	SELECT @CountryID = CountryID FROM Clients WITH (NOLOCK) WHERE ClientID = @ClientID

	IF @CountryID = 14 /*Australia*/
	BEGIN

		SELECT @PostcodeInt = CONVERT(int, @Postcode)

		/*
			Australian States / Territory		
			NSW - New South Wales
			ACT - Australian Capital Territory
			VIC - Victoria
			QLD - Queensland
			SA - South Australia
			WA - Western Australia
			TAS - Tasmania
			NT - Northern Territory
		*/

		SELECT @State = CASE
							WHEN @PostcodeInt BETWEEN 200 AND 299 THEN 'ACT'
							WHEN @PostcodeInt BETWEEN 800 AND 999 THEN 'NT'
							WHEN @PostcodeInt BETWEEN 1000 AND 2599 THEN 'NSW'
							WHEN @PostcodeInt BETWEEN 2600 AND 2618 THEN 'ACT'
							WHEN @PostcodeInt BETWEEN 2619 AND 2899 THEN 'NSW'
							WHEN @PostcodeInt BETWEEN 2900 AND 2920 THEN 'ACT'
							WHEN @PostcodeInt BETWEEN 2921 AND 2999 THEN 'NSW'
							WHEN @PostcodeInt BETWEEN 3000 AND 3999 THEN 'VIC'
							WHEN @PostcodeInt BETWEEN 4000 AND 4999 THEN 'QLD'
							WHEN @PostcodeInt BETWEEN 5000 AND 5999 THEN 'SA'
							WHEN @PostcodeInt BETWEEN 6000 AND 6999 THEN 'WA'
							WHEN @PostcodeInt BETWEEN 7000 AND 7999 THEN 'TAS'
							WHEN @PostcodeInt BETWEEN 8000 AND 8999 THEN 'VIC'
							WHEN @PostcodeInt BETWEEN 9000 AND 9999 THEN 'QLD'
							ELSE ''
						END
	END

	RETURN ISNULL(@State,'UNKNOWN')
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_GetAUStateFromPostCode] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_GetAUStateFromPostCode] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_GetAUStateFromPostCode] TO [sp_executeall]
GO
