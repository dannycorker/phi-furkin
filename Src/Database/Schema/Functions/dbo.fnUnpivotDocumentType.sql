SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2010-07-22
-- Description:	Unpivot the columns in the DocumentType table 
-- =============================================
CREATE FUNCTION [dbo].[fnUnpivotDocumentType] 
(	
	@DocumentTypeID int
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT ColumnName as [RawColumnName], '[!' + ColumnName + ']' as [FormattedColumnName], RealValue
	FROM
	(
		SELECT 
		COALESCE(CONVERT(varchar(255), c.CanBeAutoSent), '') as [CanBeAutoSent],
		COALESCE(CONVERT(varchar(255), c.ClientID), '') as [ClientID],
		COALESCE(CONVERT(varchar(255), c.DocumentTypeDescription), '') as [DocumentTypeDescription],
		COALESCE(CONVERT(varchar(255), c.DocumentTypeID), '') as [DocumentTypeID],
		COALESCE(CONVERT(varchar(255), c.DocumentTypeName), '') as [DocumentTypeName],
		/*COALESCE(CONVERT(varchar(255), c.EmailBodyText), '') as [EmailBodyText],
		COALESCE(CONVERT(varchar(255), c.EmailSubject), '') as [EmailSubject],*/
		COALESCE(CONVERT(varchar(255), c.Enabled), '') as [Enabled],
		/*COALESCE(CONVERT(varchar(255), c.Footer), '') as [Footer],
		COALESCE(CONVERT(varchar(255), c.Header), '') as [Header],*/
		COALESCE(CONVERT(varchar(255), c.InputFormat), '') as [InputFormat],
		COALESCE(CONVERT(varchar(255), c.LeadTypeID), '') as [LeadTypeID],
		COALESCE(CONVERT(varchar(255), c.MultipleRecipientDataSourceID), '') as [MultipleRecipientDataSourceID],
		COALESCE(CONVERT(varchar(255), c.MultipleRecipientDataSourceType), '') as [MultipleRecipientDataSourceType],
		COALESCE(CONVERT(varchar(255), c.OutputFormat), '') as [OutputFormat],
		COALESCE(CONVERT(varchar(255), c.ReadOnlyBCC), '') as [ReadOnlyBCC],
		COALESCE(CONVERT(varchar(255), c.ReadOnlyCC), '') as [ReadOnlyCC],
		COALESCE(CONVERT(varchar(255), c.ReadOnlyTo), '') as [ReadOnlyTo],
		COALESCE(CONVERT(varchar(255), c.RecipientsBCC), '') as [RecipientsBCC],
		COALESCE(CONVERT(varchar(255), c.RecipientsCC), '') as [RecipientsCC],
		COALESCE(CONVERT(varchar(255), c.RecipientsTo), '') as [RecipientsTo],
		COALESCE(CONVERT(varchar(255), c.SendToAllByDefault), '') as [SendToAllByDefault],
		COALESCE(CONVERT(varchar(255), c.SendToMultipleRecipients), '') as [SendToMultipleRecipients]/*,
		COALESCE(CONVERT(varchar(255), c.Template), '') as [Template]*/
		FROM dbo.DocumentType c WITH (NOLOCK) 
		WHERE c.DocumentTypeID = @DocumentTypeID) p
	UNPIVOT
	(
		RealValue FOR ColumnName IN
		(
			[CanBeAutoSent],[ClientID],[DocumentTypeDescription],[DocumentTypeID],[DocumentTypeName]/*,[EmailBodyText],[EmailSubject]*/,[Enabled]/*,[Footer],[Header]*/,[InputFormat],[LeadTypeID],[MultipleRecipientDataSourceID],[MultipleRecipientDataSourceType],[OutputFormat],[ReadOnlyBCC],[ReadOnlyCC],[ReadOnlyTo],[RecipientsBCC],[RecipientsCC],[RecipientsTo],[SendToAllByDefault],[SendToMultipleRecipients]/*,[Template]*/
		)
	) as unpvt

)





GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotDocumentType] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnUnpivotDocumentType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnUnpivotDocumentType] TO [sp_executeall]
GO
