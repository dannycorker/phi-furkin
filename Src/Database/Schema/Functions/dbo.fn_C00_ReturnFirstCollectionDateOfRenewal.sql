SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Umer Hamid
-- Create date: 2018-10-31
-- Description: Return a record based on the matter ID if the current policy is a Buddies Legacy Product
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ReturnFirstCollectionDateOfRenewal]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT
)
RETURNS DATE	
AS
BEGIN

DECLARE @RenewalPPID INT,
		@FirstRenewalCollectionDate DATE

--Get the purchased product ID for the renewal schedule 
SELECT top 1 @RenewalPPID =   MAX(PurchasedProductID) FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK)
INNER JOIN customers c on c.CustomerID = ppps.CustomerID
INNER JOIN Lead l on l.CustomerID = c.CustomerID
INNER JOIN Matter m on m.LeadID = l.LeadID 
where m.MatterID = @MatterID

--Select the actual collection date of the first row in the PPPS for the renewal schedule. 
SELECT top 1 @FirstRenewalCollectionDate =  ActualCollectionDate FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) where ppps.PurchasedProductID = @RenewalPPID 
order by ActualCollectionDate asc


RETURN  @FirstRenewalCollectionDate

 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReturnFirstCollectionDateOfRenewal] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ReturnFirstCollectionDateOfRenewal] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReturnFirstCollectionDateOfRenewal] TO [sp_executeall]
GO
