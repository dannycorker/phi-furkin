SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[fn_CardTransactionMap_GetTransactionID]
(
	@CardTransactionMapID INT
)
RETURNS INT
AS
BEGIN
	
	DECLARE @CardTransactionID INT
	
	IF DB_NAME() = 'Aquarius603'
	BEGIN
		SELECT	@CardTransactionID=m.CardTransactionID 
		FROM	AquariusMaster.dbo.CardTransactionMap m WITH (NOLOCK)
		WHERE	m.CardTransactionMapID = @CardTransactionMapID
	END
	ELSE
	BEGIN
		SELECT	@CardTransactionID=m.CardTransactionID 
		FROM	AquariusMaster.dbo.CardTransactionMap m WITH (NOLOCK)
		WHERE	m.CardTransactionMapID = @CardTransactionMapID
	END

	RETURN @CardTransactionID

END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_CardTransactionMap_GetTransactionID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_CardTransactionMap_GetTransactionID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_CardTransactionMap_GetTransactionID] TO [sp_executeall]
GO
