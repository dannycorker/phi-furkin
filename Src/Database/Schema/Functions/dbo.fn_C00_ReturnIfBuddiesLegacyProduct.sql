SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Umer Hamid
-- Create date: 2018-10-15
-- Description: Return a record based on the matter ID if the current policy is a Buddies Legacy Product
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ReturnIfBuddiesLegacyProduct]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS INT	
AS
BEGIN

	DECLARE @IsLegacyProduct INT = 0
	
	
	-- Return a record based on the matter ID if the current policy is a Buddies Legacy Product 
	SELECT @IsLegacyProduct=
	Count(*) FROM Lead l WITH (NOLOCK) 
	INNER JOIN MatterDetailValues mdv WITH (NOLOCK) on mdv.LeadID = l.LeadID and mdv.DetailFieldID = 170034
	INNER JOIN ResourceListDetailValues rldv WITH (NOLOCK) on rldv.ResourceListID = mdv.ValueInt and rldv.DetailFieldID = 144318
	INNER JOIN LookupListItems lli on rldv.ValueInt = lli.LookupListItemID
	WHERE rldv.ValueInt in (76506,76507,76508,76509) -- Budget, Bronze, Silver, Gold Legacy Products
	and mdv.MatterID = @matterid

	RETURN @IsLegacyProduct
 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReturnIfBuddiesLegacyProduct] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ReturnIfBuddiesLegacyProduct] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReturnIfBuddiesLegacyProduct] TO [sp_executeall]
GO
