SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire / Gavin Reynolds
-- Create date: 2020-09-13
-- Description:	Use in reports to identify which detail field should be used for the query
-- =============================================
CREATE FUNCTION [dbo].[_C600_GetStateVarianceRLDetailFieldID]
(
	@MatterID INT,
	@Type INT
)
RETURNS INT
AS
BEGIN

	/*TYPE
		1 - New Business
		2 - MTA
		3 - Renewal 
		4 - Reinstatement
	*/
	
	/*
	Detail fields in proc
	aq 1,2,314232 -- State Variance Detail Fields
	aq 1,2,314228 -- NB_RLDetailField
	aq 1,2,314229 -- MTA_RLDetailField
	aq 1,2,314230 -- Renewal_RLDetailField
	aq 1,2,314231 -- Reinstatement_RLDetailField
	*/

	DECLARE @SVDetailFieldID INT 

	IF @Type = 1 /*New Business*/
	BEGIN

		SELECT @SVDetailFieldID = [dbo].[fnGetSimpleRLDv](314232, 314228, @MatterID, 0)

	END

	IF @Type = 2 /*MTA*/
	BEGIN

		SELECT @SVDetailFieldID = [dbo].[fnGetSimpleRLDv](314232, 314229, @MatterID, 0)

	END

	IF @Type = 3 /*Renewal*/
	BEGIN

		SELECT @SVDetailFieldID = [dbo].[fnGetSimpleRLDv](314232, 314230, @MatterID, 0)

	END

	IF @Type = 4 /*Reinstatement*/
	BEGIN

		SELECT @SVDetailFieldID = [dbo].[fnGetSimpleRLDv](314232, 314231, @MatterID, 0)

	END

	RETURN @SVDetailFieldID

END


GO
GRANT EXECUTE ON  [dbo].[_C600_GetStateVarianceRLDetailFieldID] TO [sp_executeall]
GO
