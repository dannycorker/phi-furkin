SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-11-17
-- Description:	Get EventTypeID from third party IDS
-- =============================================

CREATE FUNCTION [dbo].[fnGetEventTypeIDFromThirdPartyIDs]
(
	@ClientID INT,
	@ThirdPartySystemID INT,
	@SubClientID INT,  -- can be NULL
	@Name VARCHAR(250),
	@LeadTypeID INT    -- can be 0
)
RETURNS INT
AS
BEGIN

	DECLARE @EventTypeID INT

	SELECT TOP 1 @EventTypeID = se.EventTypeID
	FROM ThirdPartySystemEvent se WITH (NOLOCK)
	WHERE se.ClientID = @ClientID
	AND se.ThirdPartySystemID = @ThirdPartySystemID
	AND ( se.SubClientID = @SubClientID OR @SubClientID IS NULL )
	AND se.Name = @Name
	AND se.LeadTypeID = @LeadTypeID

	RETURN @EventTypeID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetEventTypeIDFromThirdPartyIDs] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetEventTypeIDFromThirdPartyIDs] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetEventTypeIDFromThirdPartyIDs] TO [sp_executeall]
GO
