SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-10-07
-- Description:	Function for testing table row ID evaluation in the rules engine
-- =============================================
CREATE FUNCTION [dbo].[fn_TEST_GetTableRowID]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS INT
AS
BEGIN

	DECLARE @TableRowID INT = 123456789

	RETURN @TableRowID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_TEST_GetTableRowID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_TEST_GetTableRowID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_TEST_GetTableRowID] TO [sp_executeall]
GO
