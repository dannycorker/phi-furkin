SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2014-01-12
-- Description:	Find the next break-of-word position and return the word, with the break if applicable
-- =============================================
CREATE FUNCTION [dbo].[fnNextWordChangeIsBreak] 
(
	@sentence VARCHAR(MAX),
	@cursorpos INT			/* 1 is the normal start point for Sql Server */
)
RETURNS 
@sep TABLE 
(
	-- Add the column definitions for the TABLE variable here
	pos INT,
	isBreak BIT
)
AS
BEGIN
	
	/* Now find the next instance of each possible break-of-word type */
	INSERT @sep(pos, isBreak) SELECT CHARINDEX('.', @sentence, @cursorpos), 1
	INSERT @sep(pos, isBreak) SELECT CHARINDEX(',', @sentence, @cursorpos), 1
	INSERT @sep(pos, isBreak) SELECT CHARINDEX('!', @sentence, @cursorpos), 1
	INSERT @sep(pos, isBreak) SELECT CHARINDEX('?', @sentence, @cursorpos), 1
	INSERT @sep(pos, isBreak) SELECT CHARINDEX(':', @sentence, @cursorpos), 1
	INSERT @sep(pos, isBreak) SELECT CHARINDEX(' ', @sentence, @cursorpos), 0	
	
	/* 
		Where pos is zero it means not found from the start point onwards,
		So @cursurpos will remain zero if no more separators are found. 
	*/
	SELECT @cursorpos = MIN(s.pos) 
	FROM @sep s 
	WHERE s.pos > 0 
	
	DELETE @sep WHERE pos <> @cursorpos
	
	/* Return the first space/break position */
	RETURN
END



GO
GRANT VIEW DEFINITION ON  [dbo].[fnNextWordChangeIsBreak] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnNextWordChangeIsBreak] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnNextWordChangeIsBreak] TO [sp_executeall]
GO
