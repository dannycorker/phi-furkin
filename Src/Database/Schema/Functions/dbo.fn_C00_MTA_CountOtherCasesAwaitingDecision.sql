SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2018-07-24
-- Description:	Adapted from Cathal Sherry fn_C433_MTA_CountOtherCasesAwaitingDecision Count other matters awaiting a decision on customer MTAs
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_MTA_CountOtherCasesAwaitingDecision]
(
	 @CaseID INT
	 
)
RETURNS INT
AS
BEGIN
		
	DECLARE @Count INT = 0
	DECLARE @ClientID INT
	
	SELECT @ClientID = client.ClientID FROM Cases client WITH ( NOLOCK ) WHERE client.CaseID = @CaseID
	
	SELECT @Count = COUNT(Cases.CaseID)
	FROM Cases Cases WITH (NOLOCK) 
	INNER JOIN LeadEvent LeadEvent WITH (NOLOCK) ON LeadEvent.LeadEventID = Cases.LatestNonNoteLeadEventID
	INNER JOIN Lead Lead WITH (NOLOCK) ON Cases.LeadID = Lead.LeadID
	INNER JOIN Customers Customers WITH (NOLOCK) ON Lead.CustomerID = Customers.CustomerID
	INNER JOIN Lead Lead1 WITH (NOLOCK) ON Lead1.CustomerID = Customers.CustomerID
	INNER JOIN Cases c1 WITH (NOLOCK) on c1.LeadID = Lead1.LeadID
	WHERE Cases.ClientID = @ClientID
	AND (Cases.CaseNum = 1) 
	AND (Cases.CaseID <> @CaseID) 
	AND LeadEvent.ClientID = @ClientID
	AND (LeadEvent.EventTypeID = 156670) 
	AND Lead.ClientID = @ClientID
	AND Customers.ClientID = @ClientID
	AND Lead1.ClientID = @ClientID
	AND (c1.CaseID = @CaseID)

	RETURN @Count

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_MTA_CountOtherCasesAwaitingDecision] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_MTA_CountOtherCasesAwaitingDecision] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_MTA_CountOtherCasesAwaitingDecision] TO [sp_executeall]
GO
