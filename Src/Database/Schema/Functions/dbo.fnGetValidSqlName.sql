SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2014-11-25
-- Description:	Replace invalid characters in a string to make it a valid name for a sql object (column, table etc)
-- =============================================
CREATE FUNCTION [dbo].[fnGetValidSqlName] 
(
	@AttemptedName VARCHAR(1024), 
	@ReplacementChar VARCHAR(1)
)
RETURNS VARCHAR(1024) 
WITH RETURNS NULL ON NULL INPUT /* Don't execute the code at all if NULL is passed in. */
AS
BEGIN
	
	
	DECLARE @ValidName VARCHAR(1024) = ''
	
	/* 
		If a real value was passed in, check all the characters now.
		Otherwise it is null or empty and therefore invalid. 
	*/
	IF @AttemptedName > ''
	BEGIN
	
		/* 
			Replace any invalid characters with underscores '_'
		*/
		;
		WITH chars AS 
		(
			/* Pivot the input string into a "table" so it can be joined to the CharMap table */
			SELECT t.N AS [POSITION], 
			SUBSTRING(@AttemptedName, t.N, 1) AS [CHAR], 
			ASCII(SUBSTRING(@AttemptedName, t.N, 1)) AS [AsciiValue], 
			t.N AS rn 
			FROM dbo.Tally t WITH (NOLOCK) 
			WHERE t.N <= LEN(@AttemptedName) 
		)
		/* Letters and numbers are allowed, but not spaces */
		SELECT @ValidName += CASE WHEN cm.IsAlphaNumeric = 1 AND cm.AsciiValue <> 32 THEN cm.CharValue ELSE @ReplacementChar END 
		FROM chars c 
		INNER JOIN dbo.CharMap cm WITH (NOLOCK) ON cm.AsciiValue = c.AsciiValue 
		ORDER BY c.rn 
				
	END /* @AttemptedName > '' */
	ELSE
	BEGIN
		SET @ValidName = NULL
	END
	
	RETURN @ValidName
	
	
END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetValidSqlName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetValidSqlName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetValidSqlName] TO [sp_executeall]
GO
