SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-07-25
-- Description:	Merges in shared lead types
-- MODIFIED	2014-08-27	SB	If lookuplist items are for the passed in client ID don't mark as shared
-- =============================================
CREATE FUNCTION [dbo].[fnLookupListItemsShared] 
(	
	@ClientID INT
)
RETURNS TABLE 
AS
RETURN 
(	
	SELECT lli.[LookupListItemID]
		  ,lli.[LookupListID]
		  ,lli.[ItemValue]
		  ,CASE WHEN s.LeadTypeShareID IS NULL THEN lli.ClientID ELSE s.ClientID END AS ClientID
		  ,lli.[Enabled]
		  ,lli.[SortOrder]
		  ,lli.[ValueInt]
		  ,lli.[ValueMoney]
		  ,lli.[ValueDate]
		  ,lli.[ValueDateTime]
		  ,lli.[SourceID]
		  ,	CASE 
				WHEN lli.ClientID = @ClientID THEN CAST(0 AS BIT)
				WHEN s.LeadTypeShareID IS NULL THEN CAST(0 AS BIT) 
				ELSE CAST(1 AS BIT) 
			END AS IsShared
	FROM dbo.LookupListItems lli WITH (NOLOCK)
	INNER JOIN dbo.LookupList ll WITH (NOLOCK) ON ll.LookupListID = lli.LookupListID
	LEFT JOIN dbo.LeadTypeShare s WITH (NOLOCK) ON ll.LeadTypeID = s.SharedFrom AND s.ClientID = @ClientID
	WHERE @ClientID IS NULL
	OR @ClientID = 0
	OR lli.ClientID IN (0, @ClientID)
)


GO
GRANT VIEW DEFINITION ON  [dbo].[fnLookupListItemsShared] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnLookupListItemsShared] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnLookupListItemsShared] TO [sp_executeall]
GO
