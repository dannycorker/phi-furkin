SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-12-02
-- Description:	Returns the Commission amount based on the commission in force for the scheme in use at start date
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetCommission]
(
	@SchemeID INT,
	@StartDate DATE,
	@Net MONEY
)
RETURNS MONEY	
AS
BEGIN
	
	--DECLARE @ClientID INT
	--SELECT @ClientID=ClientID FROM Cases WITH (NOLOCK) WHERE CaseID=@CaseID
	
	--DECLARE @SchemeID INT
	--SELECT @SchemeID = dbo.fn_C00_1273_GetPremiumCalculationScheme(@CaseID, @StartDate)
	
	DECLARE @Commission MONEY, @Gross MONEY
	SELECT @Commission=comm.ValueMoney FROM MatterDetailValues scheme 
	INNER JOIN TableRows tr WITH (NOLOCK) ON scheme.MatterID=tr.MatterID AND tr.DetailFieldID=175370
	INNER JOIN TableDetailValues from_date WITH (NOLOCK) ON tr.TableRowID=from_date.TableRowID AND from_date.DetailFieldID=175365
	INNER JOIN TableDetailValues to_date WITH (NOLOCK) ON tr.TableRowID=to_date.TableRowID AND to_date.DetailFieldID=175366
	INNER JOIN TableDetailValues comm WITH (NOLOCK) ON tr.TableRowID=comm.TableRowID AND comm.DetailFieldID=175367
	WHERE scheme.DetailFieldID=145689 AND scheme.ValueInt=@SchemeID
	AND (from_date.ValueDate IS NULL OR @StartDate >= from_date.ValueDate)
	AND (to_date.ValueDate IS NULL OR @StartDate <= to_date.ValueDate)

	SELECT @Commission=ISNULL(@Commission,0.00)
	
	SELECT @Gross = @NET * ( @Commission / 100.00 )

	RETURN ROUND(@Gross,2)
 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetCommission] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetCommission] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetCommission] TO [sp_executeall]
GO
