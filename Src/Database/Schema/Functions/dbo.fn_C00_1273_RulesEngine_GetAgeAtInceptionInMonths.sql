SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-07-20
-- Description:	Returns the age of a pet at the inception date from the override data passed in or looked up
--				Based on fn_C00_1273_RulesEngine_GetAgeInMonths
-- Mods
--	2017-03-02 DCM update age calc to use @Inception and default to terms date first
--  2018-05-03 CPS accomodate for the inception date's day being before the birth date's day
--  2018-05-04 GPR updated on C600
--	2019-01-09 GPR updated to remove the block to default the Inception to TermsDate when NULL, we actually want this to use the YearStart
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_RulesEngine_GetAgeAtInceptionInMonths]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS INT	
AS
BEGIN


	DECLARE @AgeInMonths INT,
			@BirthDate DATE,
			@YearStart DATE,
			@Inception DATE
	
	-- Birth date will be passed in as an override for quotes	
	SELECT @BirthDate = AnyValue2
	FROM @Overrides
	WHERE AnyID = 1
	AND AnyValue1 = '144274'
	
	-- For existing policies we need to look this up
	IF @BirthDate IS NULL
	BEGIN
	
		SELECT @BirthDate = ValueDate
		FROM dbo.LeadDetailValues WITH (NOLOCK) 
		WHERE LeadID = @LeadID
		AND DetailFieldID = 144274
	
	END
	
	-- Inception date passed in?	
	SELECT @Inception = AnyValue2
	FROM @Overrides
	WHERE AnyID = 1
	AND AnyValue1 = '170035'
	
	-- If not in overrides, check in existing policies
	IF @Inception IS NULL
	BEGIN
	
		SELECT @Inception = ValueDate
		FROM MatterDetailValues WITH (NOLOCK) 
		WHERE MatterID = @MatterID
		AND DetailFieldID = 170035
	
	END

/*GPR 2019-01-09 removed for #54606, we want to fall back to YearStart not TermsDate*/
	---- --if still not set use terms date
	----IF @Inception IS NULL
	----BEGIN
	
	----	SELECT @Inception = AnyValue2
	----	FROM @Overrides
	----	WHERE AnyID = 2
	----	AND AnyValue1 = 'TermsDate'
		
	----END

	-- if still not set use year start
	-- Year start date is always passed in
	IF @Inception IS NULL
	BEGIN
	
		SELECT @Inception = AnyValue2
		FROM @Overrides
		WHERE AnyID = 2
		AND AnyValue1 = 'YearStart'
		
	END
	
	-- if still not set, set to now
	IF @Inception IS NULL SELECT @Inception=dbo.fn_GetDate_Local()

	SELECT @AgeInMonths =	CASE 
								WHEN DATEPART(DAY, @BirthDate) > DATEPART(DAY, @Inception)
								THEN DATEDIFF(MONTH, @BirthDate, @Inception) - 1
								ELSE DATEDIFF(MONTH, @BirthDate, @Inception)
							END

	RETURN @AgeInMonths
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_GetAgeAtInceptionInMonths] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_RulesEngine_GetAgeAtInceptionInMonths] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_GetAgeAtInceptionInMonths] TO [sp_executeall]
GO
