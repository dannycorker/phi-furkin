SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-05-09
-- Description:	Construct the HTML for a hyperlink (to be used in table fields)
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ReturnHyperlinkHtml]
(
	 @Url			VARCHAR(2000)
	,@FriendlyName	VARCHAR(2000)
)
RETURNS VARCHAR(2000)
AS
BEGIN
			
	DECLARE @ReturnValue VARCHAR(2000) = ''
	
	SELECT  @ReturnValue = '<a href="' + @Url + '" target="_blank">' + @FriendlyName + '</a>'
	
	RETURN  @ReturnValue

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReturnHyperlinkHtml] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ReturnHyperlinkHtml] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReturnHyperlinkHtml] TO [sp_executeall]
GO
