SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2016-12-09
-- Description:	Take a LeadID and return all failed payments for arrears calculations
--				Adapted from pre-existing SAE 
-- =============================================
CREATE FUNCTION [dbo].[fn_Billing_GetFailedAndPendingPaymentsByCustomerID]
(
	@CustomerID INT
)
RETURNS TABLE 
AS
RETURN
(
	SELECT	 p.PurchasedProductPaymentScheduleID, p.ClientID, p.CustomerID, pp.AccountID, p.PurchasedProductID, p.ActualCollectionDate, p.CoverFrom, p.CoverTo, p.PaymentDate, ABS(p.PaymentNet) AS [PaymentNet], ABS(p.PaymentVAT) AS [PaymentVAT], ABS(p.PaymentGross) AS [PaymentGross], p.PaymentStatusID, p.CustomerLedgerID, p.ReconciledDate, p.CustomerPaymentScheduleID, p.WhoCreated, p.WhenCreated, p.PurchasedProductPaymentScheduleParentID, p.PurchasedProductPaymentScheduleTypeID, p.PaymentGroupedIntoID, p.SourceID
	FROM PurchasedProductPaymentSchedule p WITH (NOLOCK)
	INNER JOIN PurchasedProduct pp WITH (NOLOCK) ON pp.PurchasedProductID = p.PurchasedProductID
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = pp.ObjectID
	where m.CustomerID = @CustomerID
	AND 
		(
			p.PaymentStatusID = 4 /*Failed*/
			OR 
			p.PaymentStatusID = 1
			
		)
	AND NOT EXISTS (
		SELECT *
		FROM PurchasedProductPaymentSchedule p2 WITH ( NOLOCK )
		WHERE p2.PurchasedProductPaymentScheduleID = p.PaymentGroupedIntoID
		AND p2.PaymentStatusID = 2 /*Processed*/
		AND p2.PurchasedProductPaymentScheduleID > p.PurchasedProductPaymentScheduleID
	)
	AND p.PaymentGroupedIntoID IS NULL
	AND NOT EXISTS (
		SELECT *
		FROM PurchasedProductPaymentSchedule p3 WITH (NOLOCK)
		WHERE p3.PurchasedProductPaymentScheduleParentID = p.PurchasedProductPaymentScheduleID
		AND p3.PurchasedProductPaymentScheduleID > p.PurchasedProductPaymentScheduleParentID
	)

)
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_Billing_GetFailedAndPendingPaymentsByCustomerID] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_Billing_GetFailedAndPendingPaymentsByCustomerID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_Billing_GetFailedAndPendingPaymentsByCustomerID] TO [sp_executeall]
GO
