SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2012-07-12
-- Description:	Get packed address 1 line at a time
-- =============================================
CREATE FUNCTION [dbo].[fnGetPackedAddress1Line]
(
@PackedAddress varchar(2000),
@LineNumber int
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE @AddressLine varchar(2000),
			@StartPoint int,
			@EndPoint int,
			@MaxLineNo int

	select @StartPoint = 0, @EndPoint = CHARINDEX('_',@packedaddress)
	IF @EndPoint < @StartPoint SELECT @EndPoint = LEN(@packedaddress)+1

	IF @LineNumber = 1 
	BEGIN

		SELECT @AddressLine = SUBSTRING(@packedaddress,@StartPoint,@EndPoint-@StartPoint)

	END
	ELSE
	BEGIN

		select @StartPoint = @EndPoint+1, @EndPoint = CHARINDEX('_',@packedaddress,@EndPoint+1)
		IF @EndPoint < @StartPoint SELECT @EndPoint = LEN(@packedaddress)+1

		IF @LineNumber = 2 AND @EndPoint > @StartPoint
		BEGIN
		
	
			SELECT @AddressLine = SUBSTRING(@packedaddress,@StartPoint,@EndPoint-@StartPoint)
		
		END
		ELSE
		BEGIN

			select @StartPoint = @EndPoint+1, @EndPoint = CHARINDEX('_',@packedaddress,@EndPoint+1)
			IF @EndPoint < @StartPoint SELECT @EndPoint = LEN(@packedaddress)+1

			IF @LineNumber = 3 AND @EndPoint > @StartPoint
			BEGIN
			
				SELECT @AddressLine = SUBSTRING(@packedaddress,@StartPoint,@EndPoint-@StartPoint)
			
			END
			ELSE
			BEGIN
			
				select @StartPoint = @EndPoint+1, @EndPoint = CHARINDEX('_',@packedaddress,@EndPoint+1)
				IF @EndPoint < @StartPoint SELECT @EndPoint = LEN(@packedaddress)+1

				IF @LineNumber = 4 AND @EndPoint > @StartPoint
				BEGIN
				
					SELECT @AddressLine = SUBSTRING(@packedaddress,@StartPoint,@EndPoint-@StartPoint)
				
				END
				ELSE
				BEGIN
				
					select @StartPoint = @EndPoint+1, @EndPoint = CHARINDEX('_',@packedaddress,@EndPoint)
					IF @EndPoint < @StartPoint SELECT @EndPoint = LEN(@packedaddress)+1

					IF @LineNumber = 5 AND @EndPoint > @StartPoint
					BEGIN
					
						SELECT @AddressLine = SUBSTRING(@packedaddress,@StartPoint,LEN(@packedaddress))
					
					END

				END

			END
		
		END

	END	

	RETURN ISNULL(@AddressLine,'')

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetPackedAddress1Line] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetPackedAddress1Line] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetPackedAddress1Line] TO [sp_executeall]
GO
