SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2011-12-11
-- Description:	Get Customer DetailValue
-- =============================================
CREATE FUNCTION [dbo].[fnGetCustomerDvAsInt]
(
	@DetailFieldID		int,
	@CustomerID			int
)
RETURNS int
AS
BEGIN

	DECLARE @DetailValue as int

	Select @DetailValue = cdv.ValueInt 
	From CustomerDetailValues cdv with (nolock) 
	Where cdv.CustomerID = @CustomerID 
	and cdv.DetailFieldID = @DetailFieldID
	
	RETURN @DetailValue

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerDvAsInt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetCustomerDvAsInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCustomerDvAsInt] TO [sp_executeall]
GO
