SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-05-16
-- Description:	Returns a first or last name from a full name
-- =============================================
CREATE FUNCTION [dbo].[fnGetNamePart]
(
@FullName VARCHAR(201),
@NamePart int = 1 /*1=First 2=Last Name*/
)
RETURNS VARCHAR(201)
AS
BEGIN

	DECLARE @Output VARCHAR(201)

	SELECT @FullName = RTRIM(LTRIM(@FullName))

	/*Make sure that there is a space and the name is longer than 1 char*/
	IF LEN(REPLACE(@FullName, ' ','')) > 0 AND @FullName LIKE '% %'
	BEGIN

		IF @NamePart = 1
		BEGIN

			SELECT @Output = LEFT(@FullName, CHARINDEX(' ', @FullName, 0)-1)
		
		END
		ELSE
		BEGIN

			SELECT @Output = RIGHT(@FullName, LEN(@FullName) - CHARINDEX(' ', @FullName, 0))

		END

	END
	ELSE
	BEGIN

		SELECT @Output = @FullName

	END

	RETURN @Output

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetNamePart] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetNamePart] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetNamePart] TO [sp_executeall]
GO
