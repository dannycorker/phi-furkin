SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jim Green
-- Create date: 2008-02-28
-- Description:	Return all wins by lead type and date range
--              Very similar to fnGetWinsDisputedByLeadType so consider changes to both reports
-- =============================================
CREATE FUNCTION [dbo].[fnJG] 
(
	@LeadTypeID int, 
	@DateFrom datetime,
	@DateTo datetime,
	@Flags varchar(2000) 
)
RETURNS 
@CurrentWins TABLE 
(
	CustomerID int,
	LeadID int,
	CaseID int,
	MatterID int,
	WinAmount varchar(100),
	OfferAmount varchar(100),
	WinCount int,
	FirstValidSave datetime,
	LastValidSave datetime
)
AS
BEGIN

	-- Get a unique ID to link all the following selects together,
	-- and keep them apart from other queries.
	DECLARE @WorkWinSeedID int
	
	EXEC @WorkWinSeedID = [dbo].[WorkWinsGetWinsByLeadType] @LeadTypeID, @DateFrom, @DateTo, @Flags 

	-- Table-Valued Functions have to return a table variable, so populate ours now.
	INSERT @CurrentWins	
	(
	CustomerID,
	LeadID,
	CaseID,
	MatterID,
	WinAmount,
	OfferAmount,
	WinCount,
	FirstValidSave,
	LastValidSave
	)
	SELECT 
	cw.CustomerID, 
	cw.LeadID, 
	cw.CaseID, 
	cw.MatterID, 
	cw.WinAmount, 
	cw.OfferAmount, 
	cw.WinCount, 
	cw.FirstValidSave, 
	cw.LastValidSave 
	FROM dbo.WorkWinsCurrent cw (nolock) 
	WHERE cw.WorkWinSeedID = @WorkWinSeedID 
	
	-- Clear up all the work tables we have just used.
	EXEC [dbo].[ClearAllWorkWinsTables] @WorkWinSeedID
	
	-- Allow the function to return @CurrentWins to the calling SELECT statement
	RETURN 
END
















GO
GRANT VIEW DEFINITION ON  [dbo].[fnJG] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnJG] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnJG] TO [sp_executeall]
GO
