SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2014-10-29
-- Description:	Get the number of Working Days between 2 dates
-- =============================================
CREATE FUNCTION [dbo].[fnGetWorkingDaysBetween2Dates]
(
	@StartDate DATE,
	@EndDate DATE
)
RETURNS INT
AS
BEGIN

	DECLARE @WorkingDays INT
	
	SELECT @WorkingDays = COUNT(*)
	FROM WorkingDays w WITH (NOLOCK)
	WHERE w.Date BETWEEN @StartDate AND @EndDate
	AND w.IsWorkDay = 1
	
	RETURN @WorkingDays

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetWorkingDaysBetween2Dates] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetWorkingDaysBetween2Dates] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetWorkingDaysBetween2Dates] TO [sp_executeall]
GO
