SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2015-10-29
-- Description:	Find customer based on MatterDetailValues.ValueMoney
-- =============================================
CREATE FUNCTION [dbo].[_C00_FindCustomerByValueMoney]
(
	@DetailFieldID1 INT,
	@DetailFieldID2 INT,
	@DetailFieldID3 INT,
	@ValueMoney1 MONEY,
	@ValueMoney2 MONEY,
	@ValueMoney3 MONEY
)
RETURNS INT
AS
BEGIN
	
	DECLARE @CustomerID INT

	SELECT TOP (1) @CustomerID = m.CustomerID
	FROM Matter m WITH (NOLOCK)
	INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = m.CustomerID AND c.Test = 0
	INNER JOIN MatterDetailValues mdv_phone WITH (NOLOCK) ON mdv_phone.MatterID = m.MatterID AND mdv_phone.DetailFieldID IN (@DetailFieldID1,@DetailFieldID2,@DetailFieldID3)
	WHERE mdv_phone.ValueMoney IN (@ValueMoney1,@ValueMoney2,@ValueMoney3)
	AND mdv_phone.ValueMoney <> 0.00

	RETURN @CustomerID

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FindCustomerByValueMoney] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_FindCustomerByValueMoney] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_FindCustomerByValueMoney] TO [sp_executeall]
GO
