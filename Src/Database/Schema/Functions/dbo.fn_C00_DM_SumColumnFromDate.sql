SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-06-17
-- Description:	Get Balance based on table, fallback to matter field
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_DM_SumColumnFromDate]
(
@MatterID INT,
@TableFieldToSUM INT,
@ColumnToSUM INT,
@FromDateToSUM int,
@TableFieldFrom INT,
@ColumnFrom INT
)
RETURNS numeric(18,2)
AS
BEGIN

	DECLARE @SummedValue numeric(18,2),
			@LastPaymentDate DATE,
			@FromDateDetailFieldSubtypeID INT
			
	/*CS 2014-02-05 allow a matter field to hold the date from which to sum*/
	SELECT @FromDateDetailFieldSubtypeID = df.LeadOrMatter 
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @ColumnFrom

	IF @FromDateDetailFieldSubtypeID = 2
	BEGIN
		SELECT @LastPaymentDate = mdv.ValueDate
		FROM MatterDetailValues mdv WITH (NOLOCK) 
		WHERE mdv.MatterID = @MatterID
		and mdv.DetailFieldID = @ColumnFrom
	END
	ELSE
	BEGIN
		SELECT TOP (1) @LastPaymentDate = tdv.ValueDate
		FROM TableRows tr WITH (NOLOCK)
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = @ColumnFrom
		WHERE tr.DetailFieldID = @TableFieldFrom
		AND tr.MatterID = @MatterID
		ORDER BY tr.TableRowID DESC
	END

	IF @LastPaymentDate IS NULL
	BEGIN
		SELECT @SummedValue = SUM(tdv.ValueMoney)
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = @ColumnToSUM
		WHERE tr.MatterID = @MatterID
		AND tr.DetailFieldID = @TableFieldToSUM
	END
	ELSE
	BEGIN
		SELECT @SummedValue = SUM(tdv.ValueMoney)
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) ON tdv.TableRowID = tr.TableRowID and tdv.DetailFieldID = @ColumnToSUM
		INNER JOIN TableDetailValues tdv_date WITH (NOLOCK) ON tdv_date.TableRowID = tr.TableRowID and tdv_date.DetailFieldID = @FromDateToSUM and tdv_date.ValueDate >= @LastPaymentDate
		WHERE tr.MatterID = @MatterID
		AND tr.DetailFieldID = @TableFieldToSUM
	END

	Return ISNULL(@SummedValue, 0.00)

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_DM_SumColumnFromDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_DM_SumColumnFromDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_DM_SumColumnFromDate] TO [sp_executeall]
GO
