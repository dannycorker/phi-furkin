SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2013-01-09
-- Description:	Generate a column list for a table. Used by LeadTypeCopy so please be careful with any changes.
--              All columns are returned in square brackets to protect the calling procedure from column names that are reserved words (eg EventTypeAttachment.[All])
-- =============================================
CREATE FUNCTION [dbo].[fnGetColumnList] 
(
	@TableName VARCHAR(1024),
	@IncludeIdentity BIT,
	@Prefix VARCHAR(1024)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	
	/*
		Pass in a table name to get back a column list. 
		
		Example: SELECT dbo.fnGetColumnList ('AquariumOption', 0, 'a')
		
		Returns 'a.[AquariumOptionTypeID], a.[AquariumOptionName], a.[AquariumOptionDescription]' 
		
		In this example the identity column AquariumOptionID is deliberately excluded from the list.
	*/
	DECLARE @ColumnList VARCHAR(MAX) = '',
	@IdentityName SYSNAME 
	
	/* Get the name of the IDENTITY column if there is one */
	SELECT @IdentityName = dbo.fnGetIdentityColumnName(@TableName)
	
	/* The prefix needs a dot afterwards. Add one if not present. */
	IF @Prefix > ''
	BEGIN
		IF RIGHT(@Prefix, 1) <> '.'
		BEGIN
			SELECT @Prefix += '.'
		END
	END
	ELSE
	BEGIN
		SELECT @Prefix = ''
	END
	
	/* Optionally avoid the IDENTITY column in the list */
	SELECT @ColumnList += @Prefix + '[' + isc.COLUMN_NAME + '], ' 
	FROM INFORMATION_SCHEMA.COLUMNS isc 
	WHERE isc.TABLE_NAME = @TableName 
	AND (@IncludeIdentity = 1 OR @IdentityName IS NULL OR isc.COLUMN_NAME <> @IdentityName) 
	ORDER BY isc.ORDINAL_POSITION 
	
	IF @ColumnList LIKE '%, '
	BEGIN
		SELECT @ColumnList = LEFT(@ColumnList, LEN(@ColumnList) - 1)
	END

	/* Return the colum list */
	RETURN @ColumnList

END






GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetColumnList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetColumnList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetColumnList] TO [sp_executeall]
GO
