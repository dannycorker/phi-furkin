SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 10/03/2020
-- Description:	Converts the DateTime to a VARCHAR(6) by removing the time and the seperation characters (DDMMYY)
--				e.g., 2020-03-10 10:30 => 100320 
-- =============================================
CREATE FUNCTION [dbo].[fnGetAuDate]
(
	@DateValue DATETIME
	
)
RETURNS VARCHAR(6)
AS
BEGIN
	
	-- CONVERT style 103 is dd/mm/yyyy. Then use the REPLACE function to eliminate the slashes.
	-- Then trucate the result to 6 characters
	DECLARE @AuDate VARCHAR(6)
	SET @AuDate = REPLACE(CONVERT(CHAR(10), @DateValue, 103), '/', '')
	
	RETURN @AuDate

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetAuDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetAuDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetAuDate] TO [sp_executeall]
GO
