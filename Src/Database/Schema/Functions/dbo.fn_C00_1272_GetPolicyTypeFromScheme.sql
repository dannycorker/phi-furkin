SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Robin Hall
-- Create date: 2014-04-29
-- Description:	Returns the policy type for a SchemeMatter
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetPolicyTypeFromScheme]
(
	@SchemeMatterID INT
)
RETURNS INT	
AS
BEGIN

	DECLARE @PolicyType INT
	
	SELECT @PolicyType = rdvType.ValueInt
	FROM Matter mScheme WITH (NOLOCK) 
	INNER JOIN dbo.MatterDetailValues mdvSchemeRL WITH (NOLOCK) on mdvSchemeRL.MatterID = mScheme.MatterID and mdvSchemeRL.DetailFieldID = 145689
	INNER JOIN dbo.ResourceListDetailValues rdvType WITH (NOLOCK) on rdvType.ResourceListID = mdvSchemeRL.ValueInt and rdvType.DetailFieldID = 144319
	WHERE mScheme.MatterID = @SchemeMatterID
	
	RETURN @PolicyType

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicyTypeFromScheme] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_GetPolicyTypeFromScheme] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicyTypeFromScheme] TO [sp_executeall]
GO
