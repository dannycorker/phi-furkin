SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 -- User Defined Function
 
 -- =============================================
 -- Author:		Paul Richardson
 -- Create date: 13/07/2017
 -- Description:	Gets the active version of the document template
 --				Gets the largest active from date that occurs before todays date
 --				If one can not be found then it returns the smallest version number one
 -- =============================================
 CREATE FUNCTION [dbo].[fnGetActiveDocumentTypeVersion]
 (
 @DocumentTypeID INT
 )
 RETURNS INT
 AS
 BEGIN
 
 DECLARE @DocumentTypeVersionID INT = NULL
 
 DECLARE @ClientID INT
 DECLARE @OutputFormat VARCHAR(24)
 DECLARE @UseDocumentTypeVersioning BIT
 
 SELECT @ClientID=ClientID, @OutputFormat=OutputFormat
 FROM DocumentType WITH (NOLOCK) WHERE DocumentTypeID=@DocumentTypeID
 
 SELECT @UseDocumentTypeVersioning=UseDocumentTypeVersioning
 FROM ClientOption WITH (NOLOCK) WHERE ClientID=@ClientID
 
 -- if not an excel template and versioning is switched on
 IF @OutputFormat <> 'EXCEL' AND @UseDocumentTypeVersioning=1
 BEGIN
 
 SELECT TOP 1 @DocumentTypeVersionID=DocumentTypeVersionID
 FROM DocumentTypeVersion WITH (NOLOCK)
 WHERE DocumentTypeID=@DocumentTypeID AND (Archived<>1 OR Archived Is NULL) AND
 ActiveFromDate<=dbo.fn_GetDate_Local()
 ORDER BY ActiveFromDate DESC
 
 END
 
 
 IF @DocumentTypeVersionID IS NULL -- no active from dates before todays date so choose lowest version number - version one
 BEGIN
 
 SELECT TOP 1 @DocumentTypeVersionID=DocumentTypeVersionID
 FROM DocumentTypeVersion WITH (NOLOCK)
 WHERE DocumentTypeID=@DocumentTypeID AND (Archived<>1 OR Archived Is NULL)
 ORDER BY VersionNumber ASC
 
 END
 
 return @DocumentTypeVersionID
 
 END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetActiveDocumentTypeVersion] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetActiveDocumentTypeVersion] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetActiveDocumentTypeVersion] TO [sp_executeall]
GO
