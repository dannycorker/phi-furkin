SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-02-18
-- Description:	Gets the compulsory excess applicable to this policy instance
-- Mods
--	2015-04-21 DCM ticket 32118 Pet Age >= policy age (as opposed =)
--  2015-11-04 DCM replaced direct lookup code with call to fn_C00_1272_Policy_GetPolicyExcessWithData to centralise lookup logic
--  2015-12-17 SB  fn_C384_GetPolicyCompulsoryExcess now returns policy age
--  2016-01-14 DCM Added @StartDate
--  2016-01-18 DCM Added removal of volex from the final calc
--	2016-12-14 DCM Added nullable arguments to be able to call from initial pol history row
--  2020-07-28 JEL Added Options column to ExcessData table to fix error 
--	2020-07-29 GPR updated options to VARCHAR in @ExcessData
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetPolicyCompulsoryExcess]
(
	@MatterID INT,
	@PolicySectionLULI INT,
	@StartDate DATE,
	@VolExcess MONEY,
	@PolicyAnniversary DATE,
	@PolicyInception DATE
)
RETURNS MONEY	
AS
BEGIN

--declare @MatterID INT = 50022217,
--		@PolicySectionLULI INT = 42877,--42878,
--		@StartDate DATE = '2016-02-18'
	
	DECLARE 
			@Breed VARCHAR(100),
			@CaseID INT,
			@CustomerPostCode VARCHAR(2),
			@Excess MONEY,
			@LeadID INT,
			@PetDOB DATE,
			@PolicyAge INT,
			@PolicySection VARCHAR(500),
			@SchemeMatterID INT,
			@SpeciesID INT,
			@TableRowID INT
	
	DECLARE	@ExcessData TABLE 
	(
		Out_ResourceListID INT,
		ResourceListID INT,
		Section VARCHAR(2000),
		SubSection VARCHAR(2000),
		Excess MONEY,
		ExcessPercentage MONEY,
		Postcode VARCHAR(2000),
		Breed VARCHAR(2000),
		PetAge INT,
		PetType INT,
		ExcessType INT,
		ExcessCeiling MONEY,
		VoluntaryExcess MONEY,
		PolicyAge INT,
		ExcessRule INT,
		Options VARCHAR(2000) /*GPR 2020-07-29 updated to VARCHAR*/
	)

			
	-- Pet details
	SELECT @CaseID=m.CaseID,@leadID=l.LeadID,@CustomerPostCode=c.PostCode 
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN Lead l WITH (NOLOCK) ON l.LeadID=m.LeadID
	INNER JOIN Customers c WITH (NOLOCK) ON l.CustomerID=c.CustomerID
	WHERE MatterID=@MatterID
	
	SELECT @PetDOB=dbo.fnGetDvAsDate (144274,@CaseID)
	
	SELECT @Breed=rlbr.DetailValue, @SpeciesID=rlsp.ValueInt--, @AgeInYears=dbo.fnAgeFromDate(page.ValueDate)
	FROM LeadDetailValues ptype WITH (NOLOCK) 
	INNER JOIN ResourceListDetailValues rlsp WITH (NOLOCK) ON ptype.ValueInt=rlsp.ResourceListID AND rlsp.DetailFieldID=144269
	INNER JOIN ResourceListDetailValues rlbr WITH (NOLOCK) ON ptype.ValueInt=rlbr.ResourceListID AND rlbr.DetailFieldID=144270
	INNER JOIN LeadDetailValues page WITH (NOLOCK) ON page.LeadID=ptype.LeadID AND page.DetailFieldID=144274
	WHERE ptype.LeadID=@leadID AND ptype.DetailFieldID=144272

	---- policy term date and policy age
	--IF @PolicyAnniversary IS NULL
	--BEGIN
	--	SELECT TOP 1 @PolicyAnniversary = tdvFrom.ValueDate, @PolicyInception = tdvInception.ValueDate, @VolExcess = tdvVolex.ValueMoney
	--	FROM TableRows r
	--	INNER JOIN dbo.TableDetailValues tdvStatus WITH (NOLOCK) ON r.TableRowID = tdvStatus.TableRowID AND tdvStatus.DetailFieldID = 145666
	--	INNER JOIN dbo.TableDetailValues tdvVolex WITH (NOLOCK) ON r.TableRowID = tdvVolex.TableRowID AND tdvVolex.DetailFieldID = 145667
	--	INNER JOIN dbo.TableDetailValues tdvFrom WITH (NOLOCK) ON r.TableRowID = tdvFrom.TableRowID AND tdvFrom.DetailFieldID = 145663
	--	INNER JOIN dbo.TableDetailValues tdvTo WITH (NOLOCK) ON r.TableRowID = tdvTo.TableRowID AND tdvTo.DetailFieldID = 145664
	--	INNER JOIN dbo.TableDetailValues tdvInception WITH (NOLOCK) ON r.TableRowID = tdvInception.TableRowID AND tdvInception.DetailFieldID = 145662
	--	WHERE r.DetailFieldID=170033 AND r.MatterID=@MatterID
	--	AND tdvStatus.ValueInt=43002
	--	ORDER BY r.TableRowID DESC 
	--END

	IF @StartDate IS NOT NULL
		SELECT @PolicyAnniversary=@StartDate

	SELECT @PolicyAge = ISNULL(DATEDIFF(YEAR, @PolicyInception, @PolicyAnniversary), 0) + 1

	-- find the scheme matterid
	SELECT @SchemeMatterID=schsch.MatterID 
	FROM MatterDetailValues sch WITH (NOLOCK) 
	INNER JOIN MatterDetailValues schsch WITH (NOLOCK) ON sch.ValueInt=schsch.ValueInt AND schsch.DetailFieldID=145689
	WHERE sch.MatterID=@MatterID AND sch.DetailFieldID=170034

	SELECT @PolicySection=l.ItemValue 
	FROM LookupListItems l WITH (NOLOCK) 
	WHERE l.LookupListItemID=@PolicySectionLULI

	INSERT @ExcessData
	SELECT *
	FROM dbo.fn_C00_1272_Policy_GetPolicyExcessWithData(@SchemeMatterID, @CustomerPostCode, @Breed, @PetDOB, @PolicyAnniversary, @SpeciesID, @VolExcess, @PolicyAnniversary, @PolicyAnniversary, @PolicyAge, @PolicyAge)

	SELECT @Excess=MAX(ed.Excess) 
	FROM @ExcessData ed 
	WHERE ed.Section = @PolicySection
	
	SELECT @Excess = @Excess - @VolExcess

	RETURN ISNULL(@Excess,0.0)

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetPolicyCompulsoryExcess] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetPolicyCompulsoryExcess] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetPolicyCompulsoryExcess] TO [sp_executeall]
GO
