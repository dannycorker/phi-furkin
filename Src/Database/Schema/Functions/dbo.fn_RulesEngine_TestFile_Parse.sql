SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- Create date: 2016-07-26
-- =============================================
CREATE FUNCTION [dbo].[fn_RulesEngine_TestFile_Parse]( 
    @ClientID INT,
	@UserID INT,
	@ParentID INT,
	@ImportXml XML
) 
RETURNS TABLE 
AS RETURN 
(
	
	SELECT	ISNULL(T.c.value('col0[1]','VARCHAR(250)'),'') AS Input,
			ISNULL(T.c.value('col1[1]','VARCHAR(250)'),'') AS CustomerID,
			ISNULL(T.c.value('col2[1]','VARCHAR(250)'),'') AS LeadID,
			ISNULL(T.c.value('col3[1]','VARCHAR(250)'),'') AS CaseID,
			ISNULL(T.c.value('col4[1]','VARCHAR(250)'),'') AS MatterID,
			ISNULL(T.c.value('col5[1]','VARCHAR(250)'),'') AS EvaluateByEffectiveDate,
			ISNULL(T.c.value('col6[1]','VARCHAR(250)'),'') AS EvaluateByChangeSetID,
			ROW_NUMBER() OVER(ORDER BY T.c) - 1 RowID
	FROM	@ImportXML.nodes('/table/row') T(c)
)
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_TestFile_Parse] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_RulesEngine_TestFile_Parse] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_TestFile_Parse] TO [sp_executeall]
GO
