SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2018-04-20
-- Description:	Get Client gateway for a policy
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_GetClientGateway] 
(
	@PolicyID INT 
)
RETURNS INT
AS
BEGIN

	DECLARE @ClientGateway INT

	SELECT @ClientGateway = 1

	RETURN @ClientGateway

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetClientGateway] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_GetClientGateway] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_GetClientGateway] TO [sp_executeall]
GO
