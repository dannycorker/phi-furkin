SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2021-04-20
-- Description:	Returns the Deductible Option Group purchased on
-- Modified:
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_ReportDeductibleOptionGroup]
(
	@SelectedDeductible INT,
	@PetDob DATE,
	@StartDate DATE,
	@SpeciesID INT
)
RETURNS VARCHAR(8)
AS
BEGIN

	DECLARE @OptionGroup VARCHAR(8), @PetAgeInYears INT

	SELECT @PetAgeInYears = DATEDIFF(YEAR,@PetDob, @StartDate)

	DECLARE @Options TABLE(OptionGroup VARCHAR(8), SpeciesID INT, MinAge INT, MaxAge INT, Deductible INT)

	-- 'Option A' (A)
	-- 'Option B' (B)
	-- 'Option C' (C)

	-- 42989 (Canine)
	-- 42990 (Feline)

	/*Canine*/
	INSERT INTO @Options (OptionGroup, SpeciesID, MinAge, MaxAge, Deductible)
	VALUES	('Option A',42989,0,5, 100),
			('Option B',42989,0,5, 300),
			('Option C',42989,0,5, 750),
			('Option A',42989,6,10, 300),
			('Option B',42989,6,10, 500),
			('Option C',42989,6,10, 950),
			('Option A',42989,10,99, 500),
			('Option B',42989,10,99, 700),
			('Option C',42989,10,99, 1150)

	/*Feline*/
	INSERT INTO @Options (OptionGroup, SpeciesID, MinAge, MaxAge, Deductible)
	VALUES	('Option A',42990,0,5, 100),
			('Option B',42990,0,5, 300),
			('Option C',42990,0,5, 750),
			('Option A',42990,6,10, 200),
			('Option B',42990,6,10, 400),
			('Option C',42990,6,10, 850),
			('Option A',42990,10,99, 300),
			('Option B',42990,10,99, 500),
			('Option C',42990,10,99, 950)
	
	SELECT @OptionGroup = o.OptionGroup
	FROM @Options o
	WHERE o.Deductible = @SelectedDeductible
	AND o.MinAge <= @PetAgeInYears
	AND o.MaxAge >= @PetAgeInYears
	AND o.SpeciesID = @SpeciesID
	
	RETURN  @OptionGroup
 
END

GO
