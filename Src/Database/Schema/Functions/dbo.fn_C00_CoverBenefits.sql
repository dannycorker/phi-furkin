SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Nessa Green
-- Create date: 2020-09-16
-- Description:	Return a list of aspects of cover benefits
-- 2020-09-28 NG Updated to show vet fees differently
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_CoverBenefits]
(
	@MatterID		INT,
	@Column			INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE 
	@DocsList			VARCHAR(2000) = ''
	,@Separator			VARCHAR(10) = CHAR(13) + CHAR(10)


	/*Select the corresponding value from the correct column*/

	IF @Column = 1 /*ParentSection'*/

	BEGIN

	SELECT @DocsList += CASE WHEN @DocsList = '' THEN '' ELSE @Separator END + CASE WHEN p.ParentSection like 'Ad%' THEN replace(p.ParentSection,'Advertising and reward to find a missing pet','Missing Pet Advertising/Reward')  ELSE p.ParentSection END
	FROM dbo.fn_C600_PointOfSale_BenefitsBreakdown (@MatterID) p
	WHERE p.ParentSection not like '%behavioral%'
	AND p.ParentSection not like '%accident%'

	END
	
	IF @Column = 2 /*Amount*/

	BEGIN

	SELECT @DocsList += CASE WHEN @DocsList = '' THEN '' ELSE @Separator END + CASE WHEN p.Amount like '%$999,999.00%' THEN replace(p.Amount,'$999,999.00','Unlimited') ELSE p.Amount END
	FROM dbo.fn_C600_PointOfSale_BenefitsBreakdown (@MatterID) p
	WHERE p.ParentSection not like '%behavioral%'
	AND p.ParentSection not like '%accident%'
	
	END

	IF @Column = 3 /*Excess*/

	BEGIN

	SELECT @DocsList += CASE WHEN @DocsList = '' THEN '' ELSE @Separator END  + p.Excess 
	FROM dbo.fn_C600_PointOfSale_BenefitsBreakdown (@MatterID) p
	WHERE p.ParentSection not like '%behavioral%'
	AND p.ParentSection not like '%accident%'
	
	END

	IF @Column = 4 /*CoPay*/

	BEGIN

	SELECT @DocsList += CASE WHEN @DocsList = '' THEN '' ELSE @Separator END +  p.CoPay 
	FROM dbo.fn_C600_PointOfSale_BenefitsBreakdown (@MatterID) p
	WHERE p.ParentSection not like '%behavioral%'
	AND p.ParentSection not like '%accident%'
	
	END


	RETURN @DocsList

END




GO
