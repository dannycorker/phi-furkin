SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		James Lewis
-- Create date: 20-September-2016
-- Description:	Returns the history records for claim submission missing info. Has to look at history as for a claim to be submitted all these details ust be set to returned. 
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_BDX_ClaimSubmissionError]
(
	@MatterID INT
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE @DetailValue as varchar(2000) = ''

	SELECT  @DetailValue += CASE ISNULL(luli.ItemValue,'') WHEN '' THEN mdv.FieldValue ELSE d.FieldName END  + ', '  
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN dbo.DetailValueHistory mdv WITH (NOLOCK) on mdv.matterid = m.matterid and mdv.detailfieldid in (175292,170272,170271,150417,150418,175290,175288,175289,175286,175294,175287,175293,175291,150420,170120,150419,170124,170126,170125)
	INNER JOIN dbo.DetailFields d WITH (NOLOCK) on d.DetailFieldID = mdv.DetailFieldID
	LEFT JOIN LookupListItems luli WITH (NOLOCK) on luli.LookupListItemID = mdv.ValueInt 
	Where m.MatterID = @MatterID 
	and ISNULL(mdv.ValueInt,0) <> 63965

	IF ISNULL(@DetailValue,'') <> ''
	BEGIN 
	
		SELECT @DetailValue = 'Late submission of: ' + @DetailValue
	
	END

	RETURN @DetailValue
 
END

GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_BDX_ClaimSubmissionError] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_BDX_ClaimSubmissionError] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_BDX_ClaimSubmissionError] TO [sp_executeall]
GO
