SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alexandra Maguire & Gavin Reynolds
-- Create date: 2020-06-22
-- Description:	Returns the decimal for Date Part calculation
-- =============================================
CREATE FUNCTION [dbo].[fn_C605_RulesEngineTesting_GetDatePartTrendFactor]
(
	@YearStart DATE,
	@State VARCHAR(3)
)
RETURNS DECIMAL(18,5)	
AS
BEGIN

	DECLARE @ClientID INT = 605,
			@ChangeLogDate DATE, 
			@Output DECIMAL(18,5)
	
	/*Get the ChangeLogDate by State*/
		SELECT TOP (1) @ChangeLogDate= t.TrendDate
		FROM _C605_RulesEngine_ChangeLogEffectiveDate t WITH (NOLOCK) 
		WHERE t.ClientID = @ClientID
		AND (t.[ValidFrom] IS NULL OR @YearStart >= t.[ValidFrom])
		AND (t.[ValidTo] IS NULL OR @YearStart <= t.[ValidTo])
		AND (t.State = @State)

		/*GPR / ALM 2020-12-09 for SDPRU-184*/
	--(((YearPart of StartDate minus YearPart ChangeLogDate)*12.00000) + (MonthPart of StartDate minus MonthPart ChangeLogDate)) / 12.00000
	--Example: SELECT (((2021 - 2020)*12.00000) + (1 - 1)) / 12.00000
	SELECT @Output =  (((DATEPART(YEAR,@YearStart) - DATEPART(YEAR,@ChangeLogDate))*12.00000) + (DATEPART(MONTH,@YearStart) - DATEPART(MONTH,@ChangeLogDate))) / 12.00000

	RETURN ISNULL(@Output,1.00000)
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C605_RulesEngineTesting_GetDatePartTrendFactor] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C605_RulesEngineTesting_GetDatePartTrendFactor] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C605_RulesEngineTesting_GetDatePartTrendFactor] TO [sp_executeall]
GO
