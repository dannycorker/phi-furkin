SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2015-03-26
-- Description:	For ISI Clients, calculate live values for repayment breakdown display table
--				Third party mapped, using field group 26
--				To replace fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerID
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerIDandApplicant]
(	
	 @CustomerID		INT
	,@PrimaryApplicant	BIT
)
RETURNS
@TableOut TABLE 
(
	 TableRowID				INT
	,Creditor				VARCHAR(2000)
	,Percentage				DECIMAL(18,15)
	,[Balance Outstanding]	DECIMAL(18,2)
	,BulkPayment			DECIMAL(18,2)
	,[Year 1]				DECIMAL(18,2)
	,[Year 2]				DECIMAL(18,2)
	,[Year 3]				DECIMAL(18,2)
	,[Year 4]				DECIMAL(18,2)
	,[Year 5]				DECIMAL(18,2)
	,[Year 6]				DECIMAL(18,2)
	,Totals					DECIMAL(18,2)
	,[Return for creditors]	DECIMAL(18,2)
	,IsPreferential			BIT
	,Applicant				VARCHAR(2000)
	,DenyEdit				VARCHAR(5)
	,DenyDelete				VARCHAR(5)
)
AS
BEGIN
	
	DECLARE  @TermYears						INT
			,@YearCounter					INT = 0
			,@PreferentialSum				DECIMAL(18,2) = 0.00
			,@AvailableThisYear				DECIMAL(18,2) = 0.00
			,@ClientID						INT
			,@ApplicationType				VARCHAR(20)
			,@RepaymentsTableRowID			INT
			,@RepaymentTableDetailFieldID	INT
			,@RepaymentsYear1DetailFieldID	INT
			,@RepaymentsYear2DetailFieldID	INT
			,@RepaymentsYear3DetailFieldID	INT
			,@RepaymentsYear4DetailFieldID	INT
			,@RepaymentsYear5DetailFieldID	INT
			,@RepaymentsYear6DetailFieldID	INT
			,@RepaymentsYear7DetailFieldID	INT


	SELECT @ClientID = cu.ClientID
	FROM Customers cu WITH ( NOLOCK ) 
	WHERE cu.CustomerID = @CustomerID
	
	/*
	SQL to pick up the appropriate @RepaymentTableDetailFieldID based on application type and the values for years 1 to 6
	*/
      IF @PrimaryApplicant = 1
      BEGIN
            SELECT      @RepaymentTableDetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 85, 2010)
      END      
      ELSE
      BEGIN
            SELECT      @RepaymentTableDetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 85, 3009) 
      END
      
      SELECT @RepaymentsYear1DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 85, 3239) 
      SELECT @RepaymentsYear2DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 85, 3240)
      SELECT @RepaymentsYear3DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 85, 3241)
      SELECT @RepaymentsYear4DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 85, 3242)
      SELECT @RepaymentsYear5DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 85, 3243)
      SELECT @RepaymentsYear6DetailFieldID = dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 85, 3243)

	
	/*Find the Annual Amount row from the repayments table*/
	SELECT @RepaymentsTableRowID = tr.TableRowID
	FROM TableRows tr WITH ( NOLOCK ) 
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = tr.TableRowID
	INNER JOIN LookupListItems li WITH ( NOLOCK ) on li.LookupListItemID = tdv.ValueInt
	WHERE tr.DetailFieldID = @RepaymentTableDetailFieldID
	AND tr.CustomerID = @CustomerID
	AND li.ItemValue = 'Total' /*changed to total rather than annual amount available as annual amount available does not include PIP fees*/

	
	SELECT @ApplicationType = ISNULL(dbo.fnGetSimpleDvLuli(dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 85, 2526),@CustomerID),'')


	INSERT @TableOut ( TableRowID, Creditor, [Balance Outstanding], Percentage, IsPreferential, BulkPayment, [Year 1], [Year 2], [Year 3], [Year 4], [Year 5], [Year 6], Totals, Applicant, DenyEdit, DenyDelete )
	SELECT	 fn.TableRowID
			,fn.CreditorName
			,fn.Balance
			, CASE WHEN SUM(fn.Balance) OVER (PARTITION BY (fn.IsPreferential)) = 0.00 THEN 0.00 ELSE (fn.Balance/SUM(fn.Balance) OVER (PARTITION BY (fn.IsPreferential)))*100.00  END [Percentage]
			,fn.IsPreferential
			,0,0,0,0,0,0,0,0
			,fn.Applicant
			,'true'
			,'true'
	FROM fn_C00_ISI_GetCreditorsForCustomer(@CustomerID) fn
	WHERE fn.IsSecure = 0
	AND fn.ExcludeFromRepayments = 0
	ORDER BY fn.IsPreferential DESC, [Percentage] DESC

	SELECT	 @TermYears				= dbo.fnGetSimpleDvLuli(dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 85, 2020),@CustomerID) /* Select Term - Years */

	/*Loop across the years, updating each one in turn*/
	WHILE @YearCounter <= @TermYears
	BEGIN
		SELECT @AvailableThisYear	=	CASE @PrimaryApplicant
										WHEN 1 THEN /* Primary Applicant */
											CASE	@YearCounter
											WHEN 0 THEN dbo.fnGetSimpleDvAsMoney(dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 26, 1404),@CustomerID) /*Accelerated Bulk Sum*/
											WHEN 1 THEN dbo.fnGetSimpleDvAsMoney(@RepaymentsYear1DetailFieldID,@RepaymentsTableRowID) /*Year 1*/
											WHEN 2 THEN dbo.fnGetSimpleDvAsMoney(@RepaymentsYear2DetailFieldID,@RepaymentsTableRowID) /*Year 2*/
											WHEN 3 THEN dbo.fnGetSimpleDvAsMoney(@RepaymentsYear3DetailFieldID,@RepaymentsTableRowID) /*Year 3*/
											WHEN 4 THEN dbo.fnGetSimpleDvAsMoney(@RepaymentsYear4DetailFieldID,@RepaymentsTableRowID) /*Year 4*/
											WHEN 5 THEN dbo.fnGetSimpleDvAsMoney(@RepaymentsYear5DetailFieldID,@RepaymentsTableRowID) /*Year 5*/
											WHEN 6 THEN dbo.fnGetSimpleDvAsMoney(@RepaymentsYear6DetailFieldID,@RepaymentsTableRowID) /*Year 6*/
											END
										WHEN 0 THEN	/* Second Applicant */ -- << these need to be third party mapped for the SECOND applicant's fields.  As of 2015-04-02 these do not exist.
											CASE	@YearCounter
											WHEN 0 THEN dbo.fnGetSimpleDvAsMoney(dbo.fnGetDetailFieldIDFromThirdPartyIDs(@ClientID, 0, 26, 1404),@CustomerID) /*Accelerated Bulk Sum*/
											WHEN 1 THEN dbo.fnGetSimpleDvAsMoney(@RepaymentsYear1DetailFieldID,@RepaymentsTableRowID) /*Year 1*/
											WHEN 2 THEN dbo.fnGetSimpleDvAsMoney(@RepaymentsYear2DetailFieldID,@RepaymentsTableRowID) /*Year 2*/
											WHEN 3 THEN dbo.fnGetSimpleDvAsMoney(@RepaymentsYear3DetailFieldID,@RepaymentsTableRowID) /*Year 3*/
											WHEN 4 THEN dbo.fnGetSimpleDvAsMoney(@RepaymentsYear4DetailFieldID,@RepaymentsTableRowID) /*Year 4*/
											WHEN 5 THEN dbo.fnGetSimpleDvAsMoney(@RepaymentsYear5DetailFieldID,@RepaymentsTableRowID) /*Year 5*/
											WHEN 6 THEN dbo.fnGetSimpleDvAsMoney(@RepaymentsYear6DetailFieldID,@RepaymentsTableRowID) /*Year 6*/
											END
										END

		IF @AvailableThisYear < 0
		BEGIN
			SELECT @AvailableThisYear = 0.00
		END

		/*Allocate Bulk Sum*/
		SELECT @PreferentialSum = SUM(t.[Balance Outstanding])-SUM(t.Totals)
		FROM @TableOut t
		WHERE t.IsPreferential = 1

		/*Do Preferential*/
		UPDATE t
		SET	 BulkPayment		=	CASE WHEN @YearCounter = 0 THEN dbo.fnMinOf ( @AvailableThisYear * ( t.Percentage / 100.00 ) ,t.[Balance Outstanding]-t.Totals ) ELSE BulkPayment	END 
			,[Year 1]			=	CASE WHEN @YearCounter = 1 THEN dbo.fnMinOf ( @AvailableThisYear * ( t.Percentage / 100.00 ) ,t.[Balance Outstanding]-t.Totals ) ELSE [Year 1]		END
			,[Year 2]			=	CASE WHEN @YearCounter = 2 THEN dbo.fnMinOf ( @AvailableThisYear * ( t.Percentage / 100.00 ) ,t.[Balance Outstanding]-t.Totals ) ELSE [Year 2]		END 
			,[Year 3]			=	CASE WHEN @YearCounter = 3 THEN dbo.fnMinOf ( @AvailableThisYear * ( t.Percentage / 100.00 ) ,t.[Balance Outstanding]-t.Totals ) ELSE [Year 3]		END
			,[Year 4]			=	CASE WHEN @YearCounter = 4 THEN dbo.fnMinOf ( @AvailableThisYear * ( t.Percentage / 100.00 ) ,t.[Balance Outstanding]-t.Totals ) ELSE [Year 4]		END 
			,[Year 5]			=	CASE WHEN @YearCounter = 5 THEN dbo.fnMinOf ( @AvailableThisYear * ( t.Percentage / 100.00 ) ,t.[Balance Outstanding]-t.Totals ) ELSE [Year 5]		END 
			,[Year 6]			=	CASE WHEN @YearCounter = 6 THEN dbo.fnMinOf ( @AvailableThisYear * ( t.Percentage / 100.00 ) ,t.[Balance Outstanding]-t.Totals ) ELSE [Year 6]		END 
		FROM @TableOut t
		WHERE t.IsPreferential = 1
		AND @PreferentialSum > 0.00
		AND (	/*For anything beyond the bulk payment, make sure we're allocating against the correct client's debts*/
				@YearCounter = 0
			OR
				(	@YearCounter > 0
					AND
					(
						( @PrimaryApplicant = 1 AND t.Applicant IN ( 'Single', 'Joint', '', 'Applicant' ) )
						OR
						( @PrimaryApplicant = 0 AND t.Applicant IN ( 'Partner','Joint','' ))
					)
				)
			)

		/*Decrease the amount available*/
		SELECT @AvailableThisYear = ISNULL(CASE WHEN @PreferentialSum > @AvailableThisYear THEN 0.00 ELSE ISNULL(@AvailableThisYear,0.00) - ISNULL(@PreferentialSum,0.00) END,0.00)
		
		/*Do Non-Preferential*/
		UPDATE t
		SET	 BulkPayment		=	CASE WHEN @YearCounter = 0 THEN dbo.fnMaxOf(dbo.fnMinOf ( @AvailableThisYear * ( t.Percentage / 100.00 ) ,t.[Balance Outstanding]-t.Totals ),0.00) ELSE BulkPayment	END 
			,[Year 1]			=	CASE WHEN @YearCounter = 1 THEN dbo.fnMaxOf(dbo.fnMinOf ( @AvailableThisYear * ( t.Percentage / 100.00 ) ,t.[Balance Outstanding]-t.Totals ),0.00) ELSE [Year 1]		END 
			,[Year 2]			=	CASE WHEN @YearCounter = 2 THEN dbo.fnMaxOf(dbo.fnMinOf ( @AvailableThisYear * ( t.Percentage / 100.00 ) ,t.[Balance Outstanding]-t.Totals ),0.00) ELSE [Year 2]		END 
			,[Year 3]			=	CASE WHEN @YearCounter = 3 THEN dbo.fnMaxOf(dbo.fnMinOf ( @AvailableThisYear * ( t.Percentage / 100.00 ) ,t.[Balance Outstanding]-t.Totals ),0.00) ELSE [Year 3]		END 
			,[Year 4]			=	CASE WHEN @YearCounter = 4 THEN dbo.fnMaxOf(dbo.fnMinOf ( @AvailableThisYear * ( t.Percentage / 100.00 ) ,t.[Balance Outstanding]-t.Totals ),0.00) ELSE [Year 4]		END 
			,[Year 5]			=	CASE WHEN @YearCounter = 5 THEN dbo.fnMaxOf(dbo.fnMinOf ( @AvailableThisYear * ( t.Percentage / 100.00 ) ,t.[Balance Outstanding]-t.Totals ),0.00) ELSE [Year 5]		END 
			,[Year 6]			=	CASE WHEN @YearCounter = 6 THEN dbo.fnMaxOf(dbo.fnMinOf ( @AvailableThisYear * ( t.Percentage / 100.00 ) ,t.[Balance Outstanding]-t.Totals ),0.00) ELSE [Year 6]		END 
		FROM @TableOut t
		WHERE t.IsPreferential = 0	
		AND (	/*For anything beyond the bulk payment, make sure we're allocating against the correct client's debts*/
				@YearCounter = 0
			OR
				(	@YearCounter > 0
					AND
					(
						( @PrimaryApplicant = 1 AND t.Applicant IN ( 'Single', 'Joint', 'Applicant', '' ) )
						OR
						( @PrimaryApplicant = 0 AND t.Applicant IN ( 'Partner','Joint','' ) )
					)
				)
			)

		UPDATE t
		SET Totals  =ISNULL(t.BulkPayment,0.00)
					+ISNULL(t.[Year 1],0.00)
					+ISNULL(t.[Year 2],0.00)
					+ISNULL(t.[Year 3],0.00)
					+ISNULL(t.[Year 4],0.00)
					+ISNULL(t.[Year 5],0.00)
					+ISNULL(t.[Year 6],0.00)
		FROM @TableOut t 


		SELECT @YearCounter += 1, @AvailableThisYear = 0.00, @PreferentialSum = 0.00
	END


	UPDATE t
	SET [Return for creditors] = (t.Totals / t.[Balance Outstanding]) * 100.00
	FROM @TableOut t 
	WHERE t.[Balance Outstanding] <> 0.00

	RETURN
	
END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerIDandApplicant] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerIDandApplicant] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ISI_GetUnsecuredRepaymentBreakdownByCustomerIDandApplicant] TO [sp_executeall]
GO
