SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-11-24
-- Description: Tells you if ID is Customer,Lead,Case or MatterID
-- =============================================
CREATE FUNCTION [dbo].[_C00_GetIdType]
(
	@ID		INT 
	,@ClientID INT
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE @Answer Varchar(2000) = 'NotFound'
	
	IF exists (SELECT * FROM Matter M WITH (NOLOCK) WHERE M.CustomerID = @ID  AND M.ClientID = @ClientID)
	BEGIN
		SELECT @Answer = 'CustomerID'
	END
	IF exists (SELECT * FROM Matter M WITH (NOLOCK) WHERE M.LeadID = @ID  AND M.ClientID = @ClientID)
	BEGIN
		SELECT @Answer = 'LeadID'
	END	
	IF exists (SELECT * FROM Matter M WITH (NOLOCK) WHERE M.CaseID = @ID  AND M.ClientID = @ClientID)
	BEGIN
		SELECT @Answer = 'CaseID'
	END
	IF exists (SELECT * FROM Matter M WITH (NOLOCK) WHERE M.MatterID = @ID  AND M.ClientID = @ClientID)
	BEGIN
		SELECT @Answer = 'MatterID'
	END	
	
	RETURN @Answer

END
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetIdType] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_GetIdType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_GetIdType] TO [sp_executeall]
GO
