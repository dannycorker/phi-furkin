SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-07-21
-- Description:	Merges in shared lead types
-- Modified:	2014-10-14	SB	If ClientID is passed in then limit to client 0 and specific client to exclude other client fields on shared pages
-- =============================================
CREATE FUNCTION [dbo].[fnDetailFieldsShared] 
(	
	@ClientID INT
)
RETURNS TABLE 
AS
RETURN 
(	
		SELECT [DetailFieldID]
		  ,CASE WHEN s.LeadTypeShareID IS NULL THEN df.ClientID ELSE s.ClientID END AS ClientID
		  ,[LeadOrMatter]
		  ,[FieldName]
		  ,[FieldCaption]
		  ,[QuestionTypeID]
		  ,[Required]
		  ,[Lookup]
		  ,[LookupListID]
		  ,	CASE 
				WHEN s.LeadTypeShareID IS NULL THEN df.LeadTypeID 
				ELSE 
					CASE df.LeadOrMatter
						WHEN 10 THEN 0
						WHEN 12 THEN 1
						WHEN 13 THEN 2
						WHEN 14 THEN 3
						WHEN 15 THEN 4
						ELSE s.SharedTo 
					END
			END AS LeadTypeID
		  ,[Enabled]
		  ,[DetailFieldPageID]
		  ,[FieldOrder]
		  ,[MaintainHistory]
		  ,[EquationText]
		  ,[MasterQuestionID]
		  ,[FieldSize]
		  ,[LinkedDetailFieldID]
		  ,[ValidationCriteriaFieldTypeID]
		  ,[ValidationCriteriaID]
		  ,[MinimumValue]
		  ,[MaximumValue]
		  ,[RegEx]
		  ,[ErrorMessage]
		  ,[ResourceListDetailFieldPageID]
		  ,[TableDetailFieldPageID]
		  ,[DefaultFilter]
		  ,[ColumnEquationText]
		  ,[Editable]
		  ,[Hidden]
		  ,[LastReferenceInteger]
		  ,[ReferenceValueFormatID]
		  ,[Encrypt]
		  ,[ShowCharacters]
		  ,[NumberOfCharactersToShow]
		  ,[TableEditMode]
		  ,[DisplayInTableView]
		  ,[ObjectTypeID]
		  ,[SourceID]
		  ,[WhoCreated]
		  ,[WhenCreated]
		  ,[WhoModified]
		  ,[WhenModified]
		  ,[DetailFieldStyleID]
		  ,[Hyperlink]
		  ,CASE WHEN s.LeadTypeShareID IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS IsShared
	FROM dbo.DetailFields df WITH (NOLOCK)
	LEFT JOIN dbo.LeadTypeShare s WITH (NOLOCK) ON df.LeadTypeID = s.SharedFrom AND s.ClientID = @ClientID
	WHERE @ClientID IS NULL
	OR @ClientID = 0
	OR df.ClientID IN (0, @ClientID)
)


GO
GRANT VIEW DEFINITION ON  [dbo].[fnDetailFieldsShared] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnDetailFieldsShared] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnDetailFieldsShared] TO [sp_executeall]
GO
