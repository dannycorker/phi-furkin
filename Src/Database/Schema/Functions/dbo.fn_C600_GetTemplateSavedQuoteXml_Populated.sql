SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-02-05
-- Description:	Return an XML string representing a saved quote
-- Modified: 2018-03-14 Mark Beaumont	Modified IDs for L&G implementation
-- 2020-01-13 CPS for JIRA LPC-356 | Replace hard-coded ClientID with function
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetTemplateSavedQuoteXml_Populated]
(
	 @CustomerID				INT
	,@LeadID					INT
	,@MatterID					INT
	,@SessionID					INT
	,@MobilePhone 				VARCHAR(50)
	,@HomePhone 				VARCHAR(50)
	,@PostCode 					VARCHAR(50)
	,@EmailAddress 				VARCHAR(2000)
	,@LastName 					VARCHAR(2000)
	,@FirstName 				VARCHAR(2000)
	,@TitleID 					INT
	,@CustBirthDate				DATE
	,@StartDate 				DATE
	,@Colour 					INT
	,@Price 					MONEY
	,@PetBirthDate 				DATE
	,@Gender 					VARCHAR(1)
	,@BreedID 					INT
	,@PetName 					VARCHAR(2000)
	,@PetRef 					VARCHAR(2000)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE  @XmlVarchar			VARCHAR(MAX)
			,@CustomerIDVarchar		VARCHAR(10) = CONVERT(VARCHAR(10), @CustomerID)
			,@SessionIDVarchar		VARCHAR(10) = CONVERT(VARCHAR(10), @SessionID)
			,@TitleIDVarchar		VARCHAR(10) = CONVERT(VARCHAR(10), @TitleID)
			,@ColourVarchar			VARCHAR(10) = CONVERT(VARCHAR(10), @Colour)
			,@PriceVarchar			VARCHAR(21) = CONVERT(VARCHAR(21), @Price)
			,@BreedIDVarchar		VARCHAR(10) = CONVERT(VARCHAR(10), @BreedID)
			,@StartDateVarchar		VARCHAR(20) = CONVERT(VARCHAR(10),@StartDate,126) + 'T00:00:00'
			,@PetBirthDateVarchar	VARCHAR(20) = CONVERT(VARCHAR(10),@PetBirthDate,126) + 'T00:00:00'
			,@CustBirthDateVarchar	VARCHAR(20) = CONVERT(VARCHAR(10),@CustBirthDate,126) + 'T00:00:00'
			,@DoNotSms				VARCHAR(10) = CASE WHEN dbo.fnGetSimpleDvAsInt(177152, @CustomerID) = 1 THEN 'true' ELSE 'false' END
			,@DoNotEmail			VARCHAR(10) = CASE WHEN dbo.fnGetSimpleDvAsInt(177153, @CustomerID) = 1 THEN 'true' ELSE 'false' END
			,@DoNotPost				VARCHAR(10) = CASE WHEN dbo.fnGetSimpleDvAsInt(177154, @CustomerID) = 1 THEN 'true' ELSE 'false' END
			,@DoNotPhone			VARCHAR(10) = CASE WHEN dbo.fnGetSimpleDvAsInt(177371, @CustomerID) = 1 THEN 'true' ELSE 'false' END
			,@SendDocumentsBy		VARCHAR(10) = CONVERT(VARCHAR(10), dbo.fnGetSimpleDvAsInt(170257, @CustomerID))
			,@MarketingPrefs		VARCHAR(10) = CONVERT(VARCHAR(10), dbo.fnGetSimpleDvAsInt(176970, @CustomerID))
			,@IsNeutered			VARCHAR(10) = CASE WHEN dbo.fnGetSimpleDvAsInt(152783, @LeadID) = 5144 THEN 'true' ELSE 'false' END
			,@HasMicrochip			VARCHAR(10) = CASE WHEN dbo.fnGetSimpleDvAsInt(177372, @LeadID) = 5144 THEN 'true' ELSE 'false' END
			,@MicrochipNumber		VARCHAR(100) = CONVERT(VARCHAR(10), ISNULL(dbo.fnGetSimpleDv(170030, @LeadID),''))
			,@VaccinationsUpToDate	VARCHAR(10) = CASE WHEN dbo.fnGetSimpleDvAsInt(175497, @LeadID) = 5144 THEN 'true' ELSE 'false' END
			,@HasExistingConditions	VARCHAR(10) = CASE WHEN dbo.fnGetSimpleDvAsInt(175496, @LeadID) = 5144 THEN 'true' ELSE 'false' END
			,@VetID					VARCHAR(10) = CONVERT(VARCHAR(10), ISNULL(dbo.fnGetSimpleDvAsInt(146215, @LeadID), 0))
			,@ClientIdVarchar		VARCHAR(10) = dbo.fnGetPrimaryClientID()

	SELECT	 @XmlVarchar = CONVERT(VARCHAR(MAX),dbo.fn_C600_GetTemplateSavedQuoteXml())
	
	DECLARE	 @Replacements dbo.tvpVarcharVarchar
	INSERT	 @Replacements ( AnyValue1, AnyValue2 )
	VALUES
	 ('_ClientId_'				,	@ClientIdVarchar		)	-- L&G
	,('_ClientPersonnelId_'		,	'0'						)
	,('_Password_'				,	''						)
	,('_BrandingId_'			,	'153391'				)	-- Buddies
	,('_QuoteId_'				,	'0'						)
	,('_SessionID_'				,	@SessionIDVarchar		)
	,('_AdminFee_'				,	'0'						)
	,('_TransactionRef_'		,	''						)
	,('_CustomerId_'			,	@CustomerIDVarchar		)
	,('_TitleID_'				,	@TitleIDVarchar			)
	,('_FirstName_'				,	@FirstName				)
	,('_LastName_'				,	@LastName				)	
	,('_HomePhone_'				,	@HomePhone				)	
	,('_MobilePhone_'			,	@MobilePhone			)	
	,('_Email_'					,	@EmailAddress			)
	,('_PostCode_'				,	@PostCode				)
	,('_ExistingPolicyHolder_'	,	'true'					)
	,('_DoNotSms_'				,	@DoNotSms				)
	,('_DoNotEmail_'			,	@DoNotEmail				)
	,('_DoNotPhone_'			,	@DoNotPhone				)
	,('_DoNotPost_'				,	@DoNotPost				)
	,('_DateOfBirth_'			,	@CustBirthDateVarchar	)
	,('_ContactMethodId_'		,	@SendDocumentsBy		)
	,('_MarketingPreferenceId_'	,	@MarketingPrefs			)
	,('_PetRef_'				,	@PetRef 				)
	,('_PetName_'				,	@PetName 				)
	,('_SpeciesId_'				,	'42989'					)	-- Dog
	,('_BreedID_'				,	@BreedIDVarchar			)
	,('_Gender_'				,	@Gender 				)
	,('_BirthDate_'				,	@PetBirthDateVarchar	)
	,('_IsNeutered_'			,	@IsNeutered				)
	,('_HasMicrochip_'			,	@HasMicrochip			)
	,('_MicrochipNo_'			,	@MicrochipNumber		)
	,('_PurchasePrice_'			,	@PriceVarchar 			)
	,('_VaccinationsUpToDate_'	,	@VaccinationsUpToDate	)
	,('_HasExistingConditions_'	,	@HasExistingConditions	)
	,('_PetColorId_'			,	@ColourVarchar			)	
	,('_CoInsuranceNextYear_'	,	'false'					)
	,('_VetID_'					,	@VetID					)
	,('_ProductId1_'			,	'151573'				)	-- Buddies Standard
	,('_ProductId2_'			,	'151575'				)	-- Buddies Premier
	,('_ProductId3_'			,	'151576'				)	-- Buddies Premier Plus
	,('_StartDate_'				,	@StartDateVarchar		)
	,('_AnswerId1_'				,	'5144'					)	-- Underwriting
	,('_QuestionId1_'			,	'147277'				)	-- Underwriting
	,('_AnswerId2_'				,	'5144'					)	-- Underwriting
	,('_QuestionId2_'			,	'154602'				)	-- Underwriting
	,('_OtherPetsInsured_'		,	'0'						)
	,('_DiscountCodes_'			,	''						)
	,('_KeyValues_'				,	''						)

	SELECT @XmlVarchar = REPLACE(@XmlVarchar,r.AnyValue1,ISNULL(r.AnyValue2,''))
	FROM @Replacements r
	WHERE r.AnyValue2 is not NULL
	
	RETURN @XmlVarchar
	
END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetTemplateSavedQuoteXml_Populated] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetTemplateSavedQuoteXml_Populated] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetTemplateSavedQuoteXml_Populated] TO [sp_executeall]
GO
