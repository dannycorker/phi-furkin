SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ian Slack
-- ALTER date: 2014-10-13
-- Description:	Shreds the PaymentInfo from XML 
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_PaymentInfo_ShredXML]
(
	@XmlRequest XML
)
RETURNS 
	@PaymentInfo TABLE (
		RowID INT IDENTITY,
		PaymentDay INT,
		PaymentMethodTypeId INT,
		PaymentIntervalId INT,
		SortCode VARCHAR(250),
		AccountNumber VARCHAR(250),
		BankName VARCHAR(250),
		BankAccountName VARCHAR(250),
		CreditCardToken VARCHAR(250),
		CreditCardHolderName  VARCHAR(250),
		CreditCardNumberMasked  VARCHAR(250),
		CreditCardExpireDate  VARCHAR(250)
	)
AS
BEGIN

	INSERT INTO @PaymentInfo
	SELECT	Tbl.p.value('PaymentDay[1]', 'INT'),
			Tbl.p.value('PaymentMethodTypeId[1]', 'INT'),
			Tbl.p.value('PaymentIntervalId[1]', 'INT'),
			Tbl.p.value('SortCode[1]', 'VARCHAR(250)'),
			Tbl.p.value('AccountNumber[1]', 'VARCHAR(250)'),
			Tbl.p.value('BankName[1]', 'VARCHAR(250)'),
			Tbl.p.value('BankAccountName[1]', 'VARCHAR(250)'),
			Tbl.p.value('CreditCardToken[1]', 'VARCHAR(250)'),
			Tbl.p.value('CreditCardHolderName[1]', 'VARCHAR(250)'),
			Tbl.p.value('CreditCardNumberMasked[1]', 'VARCHAR(250)'),
			Tbl.p.value('CreditCardExpireDate[1]', 'VARCHAR(250)')
	FROM	@XmlRequest.nodes('//PaymentInfo') Tbl(p)
	
	RETURN

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_PaymentInfo_ShredXML] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1273_PaymentInfo_ShredXML] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_PaymentInfo_ShredXML] TO [sp_executeall]
GO
