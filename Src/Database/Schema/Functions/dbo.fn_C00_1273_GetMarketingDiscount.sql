SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-02-12
-- Description:	Returns any marketing discount due for the policy
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetMarketingDiscount]
(
	@SchemeID INT,
	@StartDate DATE,
	@Net MONEY,
	@MarketingCodeID INT = 0,
	@IsNewBusiness INT = 0
)
RETURNS MONEY	
AS
BEGIN

--declare
--	@SchemeID INT = 131268,
--	@StartDate DATE = '2015-02-12',
--	@Net MONEY = 110.0,
--	@MarketingCodeID INT = 72157,
--	@IsNewBusiness INT = 1

	
	DECLARE @Discount MONEY = 0,
			@IsPercent INT = 1,
			@Gross MONEY


	-- Get the discount appropriate for the scheme
	SELECT @Discount=CASE WHEN crule.ValueInt=69908 AND @IsNewBusiness=0 THEN 0 ELSE ISNULL(disc.ValueMoney,0) END, 
			@IsPercent=perc.ValueInt
	FROM MatterDetailValues scheme 
	INNER JOIN TableRows tr WITH (NOLOCK) ON scheme.MatterID=tr.MatterID AND tr.DetailFieldID=175489
	LEFT JOIN TableDetailValues from_date WITH (NOLOCK) ON tr.TableRowID=from_date.TableRowID AND from_date.DetailFieldID=170064
	LEFT JOIN TableDetailValues to_date WITH (NOLOCK) ON tr.TableRowID=to_date.TableRowID AND to_date.DetailFieldID=170065
	INNER JOIN TableDetailValues mp WITH (NOLOCK) ON tr.TableRowID=mp.TableRowID AND mp.DetailFieldID=170063
	INNER JOIN ResourceListDetailValues mcode WITH (NOLOCK) ON mp.ResourceListID=mcode.ResourceListID AND mcode.DetailFieldID=170057
	INNER JOIN ResourceListDetailValues perc WITH (NOLOCK) ON mp.ResourceListID=perc.ResourceListID AND perc.DetailFieldID=170060
	INNER JOIN ResourceListDetailValues crule WITH (NOLOCK) ON mp.ResourceListID=crule.ResourceListID AND crule.DetailFieldID=170062
	INNER JOIN ResourceListDetailValues disc WITH (NOLOCK) ON mp.ResourceListID=disc.ResourceListID AND disc.DetailFieldID=170059
	WHERE scheme.DetailFieldID=145689 AND scheme.ValueInt=@SchemeID
	AND (from_date.ValueDate IS NULL OR @StartDate >= from_date.ValueDate)
	AND (to_date.ValueDate IS NULL OR @StartDate <= to_date.ValueDate)
	AND mcode.ValueInt=@MarketingCodeID

	IF @IsPercent=1
	BEGIN
		SELECT @Gross = @Net * ISNULL(@Discount,0) / 100
	END
	ELSE
	BEGIN
		SELECT @Gross = ISNULL(@Discount,0)
	END

	RETURN ROUND(@Gross,2)
 
END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetMarketingDiscount] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetMarketingDiscount] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetMarketingDiscount] TO [sp_executeall]
GO
