SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-05-30
-- Description:	Find the active purchased product for a matter and date
-- 2017-07-27 CPS if the policy hasn't started yet, get the first product
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetPurchasedProductForMatter]
(
	 @MatterID	INT
	,@ValidDate	DATE
)
RETURNS INT
AS
BEGIN
	
	DECLARE @PurchasedProductID INT
	
	SELECT TOP 1 @PurchasedProductID = pp.PurchasedProductID
	FROM PurchasedProduct pp WITH ( NOLOCK ) 
	WHERE pp.ObjectID = @MatterID
	AND pp.ObjectTypeID = 2 /*Matter*/
	AND @ValidDate BETWEEN pp.ValidFrom AND pp.ValidTo
	ORDER BY pp.ValidTo DESC
	/*!! NO SQL HERE !!*/
	IF @@ROWCOUNT = 0 AND dbo.fnGetSimpleDvAsDate(170036,@MatterID) >  @ValidDate /*Policy Start Date*/
	BEGIN
		SELECT TOP 1 @PurchasedProductID = pp.PurchasedProductID
		FROM PurchasedProduct pp WITH ( NOLOCK ) 
		WHERE pp.ObjectID = @MatterID
		AND pp.ObjectTypeID = 2 /*Matter*/
		ORDER BY pp.ValidFrom DESC
	END

	RETURN @PurchasedProductID

END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetPurchasedProductForMatter] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetPurchasedProductForMatter] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetPurchasedProductForMatter] TO [sp_executeall]
GO
