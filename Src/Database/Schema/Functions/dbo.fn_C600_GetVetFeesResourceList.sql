SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-10-01
-- Description:	Returns the vet fees resrource list
-- 2018-12-11 GPR created C600 version for C600 #1175
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetVetFeesResourceList]
(
	@ClientID INT,
	@CurrentPolicyMatterID INT
	
)
RETURNS INT	
AS
BEGIN
	
	DECLARE @VetFeesID INT
	
	SELECT @VetFeesID = rl.ResourceListID
	FROM dbo.ResourceList rl WITH (NOLOCK) 
	INNER JOIN dbo.ResourceListDetailValues rdvSec WITH (NOLOCK) ON rl.ResourceListID = rdvSec.ResourceListID AND rdvSec.DetailFieldID = 146189
	INNER JOIN dbo.ResourceListDetailValues rdvSub WITH (NOLOCK) ON rl.ResourceListID = rdvSub.ResourceListID AND rdvSub.DetailFieldID = 146190
	WHERE --rdvSec.ValueInt in (73961,74284,74301,74317,74302)
	rdvSec.ValueInt IN (74540)
	AND rdvSub.ValueInt = 74283
	AND rl.ClientID = @ClientID
	AND EXISTS (SELECT * FROM TableDetailValues tr WITH (NOLOCK) WHERE tr.MatterID = @CurrentPolicyMatterID AND tr.DetailFieldID = 144357 AND tr.ResourceListID = rl.ResourceListID)
	
	-- And return it
	RETURN @VetFeesID

END





GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetVetFeesResourceList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_GetVetFeesResourceList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetVetFeesResourceList] TO [sp_executeall]
GO
