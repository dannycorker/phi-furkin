SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2009?
-- Description:	Find If Customer is a duplicate
-- =============================================
CREATE FUNCTION [dbo].[fnIsDuplicate] 
(
@CustomerID int
)
RETURNS int
AS
BEGIN

	DECLARE @ClientID int,
			@FirstName varchar(1000),
			@LastName varchar(1000),
			@Address1 varchar(1000),
			@PostCode varchar(1000),
			@DOB varchar(1000),
			@ReturnCode int = 0


	Select @ClientID = ClientID, @FirstName = FirstName, @LastName = LastName, @Address1 = Address1, @PostCode = PostCode, @DOB = DateOfBirth
	From Customers with (nolock)
	Where CustomerID = @CustomerID

	SELECT	@ReturnCode = ISNULL(MAX(CASE WHEN FirstName = @FirstName and LastName = @LastName AND @LastName <> '' THEN 1 ELSE 0 END + 
			CASE WHEN Address1 = @Address1 AND @Address1 <> '' THEN 1 ELSE 0 END + 
			CASE WHEN PostCode = @PostCode AND @PostCode <> '' THEN 1 ELSE 0 END + 
			CASE WHEN DateOfBirth = @DOB AND @DOB IS NOT NULL THEN 1 ELSE 0 END),0)
	From Customers c with (nolock)
	Where 	ClientID = @ClientID
	and (Test = 0 or Test IS NULL)
	AND c.CustomerID <> @CustomerID
	and ((FirstName = @FirstName AND LastName = @LastName)
	OR Address1 = @Address1
	OR PostCode = @PostCode
	OR DateOfBirth = @DOB)

	RETURN @ReturnCode

/*	If EXISTS (Select *
	From Customers c with (nolock)
	Where FirstName = @FirstName
	And Address1 = @Address1
	And LastName = @LastName
	And PostCode = @PostCode
	And DateOfBirth = @DOB
	and ClientID = @ClientID
	and (Test = 0 or Test IS NULL)
	AND c.CustomerID <> @CustomerID
	)

	Begin
		RETURN 5
	End

	If EXISTS (Select *
	From Customers c with (nolock)
	Where FirstName = @FirstName
	And LastName = @LastName
	And PostCode = @PostCode
	And DateOfBirth = @DOB
	and ClientID = @ClientID
	and (Test = 0 or Test IS NULL)
	AND c.CustomerID <> @CustomerID
	)

	Begin
		RETURN 4
	End

	If EXISTS (Select *
	From Customers c with (nolock)
	Where FirstName = @FirstName
	And PostCode = @PostCode
	And DateOfBirth = @DOB
	and ClientID = @ClientID
	and (Test = 0 or Test IS NULL)
	AND c.CustomerID <> @CustomerID
	)

	Begin
		RETURN 3
	End

	If EXISTS (Select *
	From Customers c with (nolock)
	Where FirstName = @FirstName
	And LastName = @LastName
	And DateOfBirth = @DOB
	and ClientID = @ClientID
	and (Test = 0 or Test IS NULL)
	AND c.CustomerID <> @CustomerID
	)

	Begin
		RETURN 3
	End

	If EXISTS (Select *
	From Customers c with (nolock)
	Where FirstName = @FirstName
	And LastName = @LastName
	And PostCode = @PostCode
	and ClientID = @ClientID
	and (Test = 0 or Test IS NULL)
	AND c.CustomerID <> @CustomerID
	)

	Begin
		RETURN 3
	End

	If EXISTS (Select *
	From Customers c with (nolock)
	Where LastName = @LastName
	And PostCode = @PostCode
	and ClientID = @ClientID
	and (Test = 0 or Test IS NULL)
	AND c.CustomerID <> @CustomerID
	)

	Begin
		RETURN 2
	End

	Return 0
*/
	/*
	I like this option:
	
	select @FirstName = 'Jim', @LastName = 'Test', @PostCode = 'M20 2ZB' -- etc

	select top 1 customerid, 
	case when firstname = @Firstname then 1 else 0 end +
	case when lastname = @lastname then 1 else 0 end +
	case when postcode = @postcode then 1 else 0 end 
	/* etc */
	From Customers with (nolock)
	Where ClientID = 4 
	AND 
	(Firstname = @Firstname OR LastName = @LastName OR PostCode = @PostCode
	)
	order by 2 desc
	
	Also, the match fields could be given weightings for importance, eg Postcode = high, lastname = low
	
	Then multiply them inside the CASE statement:
		case when firstname = @Firstname then @FirstnameWeighting else 0 end +
		etc

	*/
END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsDuplicate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnIsDuplicate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnIsDuplicate] TO [sp_executeall]
GO
