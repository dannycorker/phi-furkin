SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Simon Brushett
-- Create date: 2013-05-24
-- Description:	Creates an HTML bullet list
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_HTMLBulletList]
(
	@Items dbo.tvpVarchar READONLY
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE @HTML VARCHAR(MAX) = ''
	
	DECLARE @Count INT
	SELECT @Count = COUNT(*) 
	FROM @Items
	
	IF @Count > 0
	BEGIN
	
		
		SELECT @HTML += '<ul>'
		
		SELECT @HTML +=	'<li>' + AnyValue + '</li>'
		FROM @Items	
		
		SELECT @HTML += '</ul>'
	
	END
	
	RETURN @HTML

END






GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_HTMLBulletList] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_HTMLBulletList] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_HTMLBulletList] TO [sp_executeall]
GO
