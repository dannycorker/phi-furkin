SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2014-09-23
-- Description:	Concatenate text stored in a table column to form a string
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ConcatenateTableColumnByCriteriaAndObjectID]
(
	@DetailFieldID INT,
	@ColumnDetailFieldID INT,
	@CriteriaFieldID INT,
	@Criteria VARCHAR(MAX),
	@Equal bit = 1,
	@ObjectID int
)
RETURNS VARCHAR(MAX)
AS
BEGIN
			
	DECLARE @LeadOrMatter int,
			@ReturnValue VARCHAR(MAX)

	SELECT @LeadOrMatter = df.LeadOrMatter
	FROM DetailFields df WITH (NOLOCK) 
	WHERE df.DetailFieldID = @DetailFieldID

	SELECT @ReturnValue = LEFT(result,LEN(result) - 1)
	FROM
	 (SELECT
	  CONVERT(varchar(max),
	   (
		SELECT tdv.DetailValue as [text()], ', ' as [text()]
		FROM TableRows tr WITH (NOLOCK) 
		INNER JOIN TableDetailValues tdv WITH (NOLOCK) on tdv.TableRowID = tr.TableRowID 
		INNER JOIN TableDetailValues tdvval WITH (NOLOCK) on tdvval.TableRowID = tr.TableRowID
		WHERE tr.DetailFieldID = @DetailFieldID
		AND	tdv.DetailFieldID = @ColumnDetailFieldID
		And tdvval.DetailFieldID = @CriteriaFieldID
		AND (
			(tr.LeadID = @ObjectID AND @LeadOrMatter = 1) 
			OR
			(tr.MatterID = @ObjectID AND @LeadOrMatter = 2) 
			OR
			(tr.CustomerID = @ObjectID AND @LeadOrMatter = 10) 
			OR
			(tr.CaseID = @ObjectID AND @LeadOrMatter = 11)
			)
		AND ((@Equal = 1 AND tdvval.DetailValue = @Criteria) OR (@Equal = 0 AND tdvval.DetailValue <> @Criteria))
		FOR XML PATH('')
	) 
	)AS result
	) s

	RETURN @ReturnValue

END



GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ConcatenateTableColumnByCriteriaAndObjectID] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ConcatenateTableColumnByCriteriaAndObjectID] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ConcatenateTableColumnByCriteriaAndObjectID] TO [sp_executeall]
GO
