SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-05-08
-- Description:	Get CaseDV directly #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetCaseDvCASE]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS VARCHAR(2000)
AS
BEGIN

	DECLARE @DetailValue AS VARCHAR(2000)

	SELECT @DetailValue = cdv.DetailValue
	FROM CaseDetailValues cdv WITH (NOLOCK) 
	WHERE cdv.CaseID = @CaseID 
	AND cdv.DetailFieldID = @DetailFieldID

	RETURN @DetailValue

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCaseDvCASE] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetCaseDvCASE] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCaseDvCASE] TO [sp_executeall]
GO
