SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-11-26
-- Description:	Returns the monthly split from an annual value
-- =============================================
 
CREATE FUNCTION [dbo].[fn_C00_1273_StorePremiumCalcSplitPremium]
(
	@AnnualValue VARCHAR(200)
)
RETURNS 
@Data TABLE
(
	FirstMonthly VARCHAR(200),
	RecurringMonthly VARCHAR(200)
)	
AS
BEGIN
	
	DECLARE @Monthly FLOAT

	SELECT @AnnualValue = 0.00

	SELECT @Monthly = CAST (@AnnualValue AS FLOAT) / 12
	
	INSERT @Data (FirstMonthly, RecurringMonthly)
	SELECT CAST( @Monthly AS VARCHAR ), CAST( @Monthly AS VARCHAR )
	
	RETURN
 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_StorePremiumCalcSplitPremium] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1273_StorePremiumCalcSplitPremium] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_StorePremiumCalcSplitPremium] TO [sp_executeall]
GO
