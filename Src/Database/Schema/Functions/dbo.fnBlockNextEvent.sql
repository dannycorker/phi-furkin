SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 14-July-2009
-- Description:	Block Cases from progressing if the latest event applied is a blocking event
-- =============================================
CREATE FUNCTION [dbo].[fnBlockNextEvent]
(
	@CaseID				int
)
RETURNS bit
AS
BEGIN

	Declare @ReturnCode int

	IF (SELECT Count(*) FROM EventChoiceBlock ecb (NOLOCK) INNER JOIN LeadEvent le (NOLOCK) ON le.EventTypeID = ecb.EventTypeID and le.CaseID = @CaseID and le.EventDeleted = 0) = 0
	Begin
	
		SELECT @ReturnCode = 0
		
	End
	Else
	Begin
	
		If (SELECT Count(*) FROM EventChoiceBlock ecb (NOLOCK) 
			INNER JOIN LeadEvent le (NOLOCK) ON le.EventTypeID = ecb.EventTypeID and le.CaseID = @CaseID  and le.EventDeleted = 0
			AND NOT Exists(
				SELECT *
				FROM LeadEvent le1 (NOLOCK)
				WHERE le1.CaseID = le.CaseID
				AND le1.LeadEventID > le.LeadEventID
				and le1.EventDeleted = 0
				)
			) = 0
		Begin
		
		SELECT @ReturnCode = 0
		
		End
		Else
		Begin

			SELECT @ReturnCode = 1
		
		End
	
	
	End

Return @ReturnCode

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnBlockNextEvent] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnBlockNextEvent] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnBlockNextEvent] TO [sp_executeall]
GO
