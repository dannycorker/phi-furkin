SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author: Jim
-- Create date: 2007-08-08
-- Description: Returns date from datetime
-- =============================================
CREATE FUNCTION [dbo].[fnDateOnlyChar]
(
	@DT datetime
)
RETURNS char(10)
AS
BEGIN
	DECLARE @Result char(10)

	SET @Result = convert(char(10), @DT, 126)

	RETURN @Result
END







GO
GRANT VIEW DEFINITION ON  [dbo].[fnDateOnlyChar] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnDateOnlyChar] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnDateOnlyChar] TO [sp_executeall]
GO
