SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2008-02-11
-- Description:	Substitute app variables with real SQL values
-- =============================================
CREATE FUNCTION [dbo].[fnSubstituteSqlParams] 
(
	@InStr varchar(max), 
	@RunAsThisUser int,
	@ClientID int,
	@CustomerID int,
	@LeadID int,
	@CaseID int, 
	@SubClientID int,
	@PortalUserID int 
)
RETURNS varchar(max)
AS
BEGIN
	DECLARE @OutStr varchar(max)

	/* Susbstitute known app params with real data */
	DECLARE @TheDateTimeRightNow datetime,
	@TheDateToday datetime,
	@TheDateYesterday datetime,
	@TheDateTomorrow datetime,
	@TimeAM char(9),
	@TimePM char(9)

	SELECT @TheDateTimeRightNow = dbo.fn_GetDate_Local(),
    @TheDateToday = dbo.fnDateOnly(@TheDateTimeRightNow),
    @TheDateTomorrow = dbo.fnDateOnly(dateadd(dd, 1, @TheDateTimeRightNow)),
	@TheDateYesterday = dbo.fnDateOnly(dateadd(dd, -1, @TheDateTimeRightNow)),
	@TimeAM = ' 00:00:00',
	@TimePM = ' 23:59:59'

    /* Now set up the parameters to use for this query */
	DECLARE @YesterdayYYYYMMDD char(10),
	@YesterdayAM char(19),
	@YesterdayPM char(19),
	@TodayYYYYMMDD char(10),
	@TomorrowYYYYMMDD char(10),
	@TodayAM char(19),
	@TodayPM char(19)

	SELECT @YesterdayYYYYMMDD = convert(char(10), @TheDateYesterday, 126),
	@TomorrowYYYYMMDD = convert(char(10), @TheDateTomorrow, 126),
    @YesterdayAM = @YesterdayYYYYMMDD + @TimeAM,
    @YesterdayPM = @YesterdayYYYYMMDD + @TimePM,
    @TodayYYYYMMDD = convert(char(10), @TheDateToday, 126),
    @TodayAM = @TodayYYYYMMDD + @TimeAM,
    @TodayPM = @TodayYYYYMMDD + @TimePM

    /* Replace datetime parameters with real datetime values */
    SELECT @OutStr = @InStr

	SELECT @OutStr = replace(@OutStr, '[[yesterday_am]]', @YesterdayAM)
    SELECT @OutStr = replace(@OutStr, '[[yesterday_pm]]', @YesterdayPM)
    SELECT @OutStr = replace(@OutStr, '[[yesterday]]', @YesterdayYYYYMMDD)
    SELECT @OutStr = replace(@OutStr, '[[today_am]]', @TodayAM)
    SELECT @OutStr = replace(@OutStr, '[[today_pm]]', @TodayPM)
    SELECT @OutStr = replace(@OutStr, '[[today]]', @TodayYYYYMMDD)

    /* Newer parameter versions */
    SELECT @OutStr = replace(@OutStr, '@YesterdayAM', @YesterdayAM)
    SELECT @OutStr = replace(@OutStr, '@YesterdayPM', @YesterdayPM)
    SELECT @OutStr = replace(@OutStr, '@Yesterday', @YesterdayYYYYMMDD)
    SELECT @OutStr = replace(@OutStr, '@Tomorrow', @TomorrowYYYYMMDD)
    SELECT @OutStr = replace(@OutStr, '@TodayAM', @TodayAM)
    SELECT @OutStr = replace(@OutStr, '@TodayPM', @TodayPM)
    SELECT @OutStr = replace(@OutStr, '@Today', @TodayYYYYMMDD)
    SELECT @OutStr = replace(@OutStr, '@CurrentUser', convert(varchar, @RunAsThisUser))
    SELECT @OutStr = replace(@OutStr, '@ClientID', convert(varchar, @ClientID))
    SELECT @OutStr = replace(@OutStr, '@CustomerID', convert(varchar, @CustomerID))
    SELECT @OutStr = replace(@OutStr, '@LeadID', convert(varchar, @LeadID))
    SELECT @OutStr = replace(@OutStr, '@CaseID', convert(varchar, @CaseID))

	/* 
		SubClientID.
		May be required, but may not be passed in, so look it up from ClientPersonnel if needed.
	*/
	IF CharIndex('@SubClientID', @OutStr) > 0
	BEGIN
	
		IF @SubClientID IS NULL
		BEGIN
			SELECT @SubClientID = cp.SubClientID 
			FROM dbo.ClientPersonnel cp WITH (NOLOCK) 
			WHERE cp.ClientPersonnelID = @RunAsThisUser 
		END
		
	    SELECT @OutStr = replace(@OutStr, '@SubClientID', convert(varchar, IsNull(@SubClientID, 0)))
	END

	/* 
		PortalUserID.
		May be required, but may not be available, so default it to zero to avoid SQL errors.
	*/
	IF CharIndex('@PortalUserID', @OutStr) > 0
	BEGIN
	    SELECT @OutStr = replace(@OutStr, '@PortalUserID', convert(varchar, IsNull(@PortalUserID, 0)))
	END
		
	RETURN @OutStr

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnSubstituteSqlParams] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnSubstituteSqlParams] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnSubstituteSqlParams] TO [sp_executeall]
GO
