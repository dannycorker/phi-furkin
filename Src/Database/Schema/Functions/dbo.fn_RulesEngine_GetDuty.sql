SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-02-18
-- Description:	Returns the Duty factor by Country and Region
-- 2020-02-23 CPS for JIRA AAG-186 | Don't try and calculate from a numeric postcode if you don't have one
-- 2020-05-12 GPR for JIRA SDAAG-26 | Adjusted the way we pick up the Postcode as this was NULL in quoting meaning reduced premium
-- =============================================
CREATE FUNCTION [dbo].[fn_RulesEngine_GetDuty]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS DECIMAL(18,2)	
AS
BEGIN

	DECLARE @Duty DECIMAL(18,2),
			@Postcode VARCHAR(20),
			@StateString VARCHAR(500),
			@YearStart DATE,
			@ClientID INT,
			@CountryID INT,
			@State VARCHAR(3),
			@PostcodeInt INT

	/*Get ClientID and CountryID*/
	SELECT @ClientID = ClientID FROM Customers WITH (NOLOCK) WHERE CustomerID = @CustomerID
	
	IF @ClientID IS NULL
	BEGIN
		SELECT @ClientID = 604
	END

	SELECT @CountryID = CountryID FROM Clients WITH (NOLOCK) WHERE ClientID = @ClientID

	/*Year start date is always passed in*/
	SELECT @YearStart = AnyValue2
	FROM @Overrides
	WHERE AnyID = 2
	AND AnyValue1 = 'YearStart'
	/*except not always from RulesEngine test interface so default it*/
	IF @YearStart IS NULL SELECT @YearStart=dbo.fn_GetDate_Local()
	
	/*GPR 2020-08-18 removed for JIRA AAG-1101*/
	--/*Get Postcode*/
	--IF @CustomerID IS NOT NULL
	--BEGIN
	--	SELECT @Postcode=c.PostCode 
	--	FROM Customers c WITH (NOLOCK) WHERE c.CustomerID=@CustomerID
	--END
	
	IF NULLIF(@Postcode,'') IS NULL /*GPR 2020-05-12*/
	BEGIN
		SELECT @Postcode=AnyValue2 FROM @Overrides WHERE AnyID = 2 AND AnyValue1 = 'PostCode'
	END

	/*
		AUSTRALIA - Work out State from PostCode
		CPS 2020-02-23 - don't hit this section if the postcode isn't an integer
	*/
	IF @CountryID = 14 /*Australia*/ AND dbo.fnIsInt(@Postcode) = 1 -- 2020-02-23 CPS for JIRA AAG-186 | Don't hit this section if we have a narrative Postcode
	BEGIN

		SELECT @PostcodeInt = CONVERT(int, @Postcode)

		/*
			Australian States / Territory		
			NSW - New South Wales
			ACT - Australian Capital Territory
			VIC - Victoria
			QLD - Queensland
			SA - South Australia
			WA - Western Australia
			TAS - Tasmania
			NT - Northern Territory
		*/

		SELECT @State = CASE
							WHEN @PostcodeInt BETWEEN 200 AND 299 THEN 'ACT'
							WHEN @PostcodeInt BETWEEN 800 AND 999 THEN 'NT'
							WHEN @PostcodeInt BETWEEN 1000 AND 2599 THEN 'NSW'
							WHEN @PostcodeInt BETWEEN 2600 AND 2618 THEN 'ACT'
							WHEN @PostcodeInt BETWEEN 2619 AND 2899 THEN 'NSW'
							WHEN @PostcodeInt BETWEEN 2900 AND 2920 THEN 'ACT'
							WHEN @PostcodeInt BETWEEN 2921 AND 2999 THEN 'NSW'
							WHEN @PostcodeInt BETWEEN 3000 AND 3999 THEN 'VIC'
							WHEN @PostcodeInt BETWEEN 4000 AND 4999 THEN 'QLD'
							WHEN @PostcodeInt BETWEEN 5000 AND 5999 THEN 'SA'
							WHEN @PostcodeInt BETWEEN 6000 AND 6999 THEN 'WA'
							WHEN @PostcodeInt BETWEEN 7000 AND 7999 THEN 'TAS'
							WHEN @PostcodeInt BETWEEN 8000 AND 8999 THEN 'VIC'
							WHEN @PostcodeInt BETWEEN 9000 AND 9999 THEN 'QLD'
							ELSE NULL
						END
	END

		SELECT TOP(1) @Duty = d.GrossMultiplier 
		FROM Duty d WITH (NOLOCK) 
		WHERE d.CountryID= @CountryID
		AND (d.[From] IS NULL OR @YearStart >= d.[From])
		AND (d.[To] IS NULL OR @YearStart <= d.[To])
		AND (d.State LIKE '%' + @State + '%')

	RETURN ISNULL(@Duty,1.0000)
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_GetDuty] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_RulesEngine_GetDuty] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_RulesEngine_GetDuty] TO [sp_executeall]
GO
