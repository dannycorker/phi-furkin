SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2008-03-05
-- Description:	Return all disputed wins by lead type and date range
--              Very similar to fnGetWinsByLeadType so consider changes to both reports
-- JWG 2011-06-24 Make sure the Client/Matter and Field/Date indexes are hit on DVH every time. NOLOCK throughout.
-- =============================================
CREATE FUNCTION [dbo].[fnGetWinsDisputedByLeadType] 
(
	@LeadTypeID int, 
	@DateFrom datetime,
	@DateTo datetime,
	@Flags varchar(2000) 
)
RETURNS 
@CurrentWins TABLE 
(
	CustomerID int,
	LeadID int,
	CaseID int,
	MatterID int,
	OfferAmount varchar(100),
	FirstValidSave datetime,
	LastValidSave datetime
)
AS
BEGIN

	-- Make sure we are selecting all possible times within the date range specified
	SELECT  @DateFrom = convert(datetime, convert(char(10), @DateFrom, 126) + ' 00:00:00'),
			@DateTo = convert(datetime, convert(char(10), @DateTo, 126) + ' 23:59:59')

	-- Set up various fields of interest, depending on the lead type and flags requested
	DECLARE @WinAmountFieldID int, 
	@WinDisputeFieldID int, 
	@ClientID int

	DECLARE @EventTypes TABLE (
		EventTypeID int,
		MakesAWin bit,
		StopsAWin bit
	)

	IF @LeadTypeID = 46
	BEGIN
		SELECT @WinAmountFieldID = 2123, 
		@WinDisputeFieldID = 2122, 
		@ClientID = 3

		IF @Flags = 'BC'
		BEGIN
			INSERT INTO @EventTypes(EventTypeID, MakesAWin, StopsAWin)
			VALUES (1802, 0, 1)
		END
		IF @Flags = 'CC'
		BEGIN
			INSERT INTO @EventTypes(EventTypeID, MakesAWin, StopsAWin)
			VALUES (1802, 1, 0)
		END
	END
	IF @LeadTypeID = 122
	BEGIN
		SELECT @WinAmountFieldID = 4634, 
		@WinDisputeFieldID = 4633, 
		@ClientID = 3
	END


	-- Select all Matters with a win recorded in that range (some will be filtered out below)	
	DECLARE @CasesWithBlockEvents TABLE (
		bCaseID int
	)

	DECLARE @CasesWithWinEvents TABLE (
		wCaseID int
	)

	DECLARE @PossibleValidMatters TABLE (
		MatterID int
	)

	INSERT @PossibleValidMatters (MatterID) 
	SELECT dvh.MatterID
	FROM DetailValueHistory dvh WITH (NOLOCK)
	WHERE dvh.ClientID = @ClientID 
	AND dvh.DetailFieldID = @WinDisputeFieldID
	AND dvh.WhenSaved BETWEEN @DateFrom AND @DateTo


	-- Note the date of the first NON-BLANK entry (ie no time-wasters!)
	DECLARE @FirstValidSave TABLE (
		MatterID int,
		WhenSaved datetime
	)

	INSERT @FirstValidSave (MatterID, WhenSaved)
	SELECT dvh.MatterID, MIN(dvh.WhenSaved)
	FROM @PossibleValidMatters pvm
	INNER JOIN DetailValueHistory dvh WITH (NOLOCK) ON dvh.MatterID = pvm.MatterID AND dvh.DetailFieldID = @WinDisputeFieldID AND replace(dvh.FieldValue, '£', '') <> ''
	GROUP BY dvh.MatterID 


	-- For Matters that do have a NON-BLANK entry somewhere in their history, note down the
	-- latest date that a value was saved so that we can count the valid entries.
	DECLARE @LatestValidSave TABLE (
		MatterID int,
		WhenSaved datetime
	)

	INSERT @LatestValidSave (MatterID, WhenSaved)
	SELECT dvh.MatterID, MAX(dvh.WhenSaved)
	FROM DetailValueHistory dvh WITH (NOLOCK)
	INNER JOIN @FirstValidSave fvs ON fvs.MatterID = dvh.MatterID
	WHERE dvh.ClientID = @ClientID 
	AND dvh.DetailFieldID = @WinDisputeFieldID
	AND dvh.WhenSaved >= fvs.WhenSaved
	GROUP BY dvh.MatterID 


	-- Now pick out the results to return
	-- This selects Matters with a valid win between the date range 
	-- requested (from @FirstValidSave and @LastValidSave),
	-- the current Win OFFER figure (from MatterDetailValues),
	-- where there is no final Win Amount
	-- REPLACE currency symbol and commas with blanks so they can be converted to numbers.
	INSERT @CurrentWins (CustomerID, LeadID, CaseID, MatterID, OfferAmount, FirstValidSave, LastValidSave) 
	SELECT m.CustomerID, m.LeadID, m.CaseID, m.MatterID, replace(replace(mdv_offer.DetailValue, '£', ''), ',', ''), fvs.WhenSaved, lvs.WhenSaved
	FROM @LatestValidSave lvs 
	INNER JOIN @FirstValidSave fvs ON fvs.MatterID = lvs.MatterID
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = lvs.MatterID
	INNER JOIN Customers c WITH (NOLOCK) ON c.CustomerID = m.CustomerID
	INNER JOIN MatterDetailValues mdv_offer WITH (NOLOCK) ON mdv_offer.MatterID = fvs.MatterID AND mdv_offer.DetailFieldID = @WinDisputeFieldID 
	INNER JOIN DetailValueHistory dvh WITH (NOLOCK) ON dvh.MatterID = lvs.MatterID AND dvh.DetailFieldID = @WinAmountFieldID AND dvh.WhenSaved BETWEEN fvs.WhenSaved AND lvs.WhenSaved
	WHERE c.LastName NOT LIKE 'Test%' 
	AND NOT EXISTS(
		SELECT * 
		FROM MatterDetailValues mdv_win WITH (NOLOCK) 
		WHERE mdv_win.MatterID = fvs.MatterID 
		AND mdv_win.DetailFieldID = @WinAmountFieldID 
		AND replace(mdv_win.DetailValue, '£', '') <> ''
	)

	-- Finally, apply any Event-based rules specified in the Flags etc
	-- We need to ignore records if a "blocking event" is in place
	INSERT @CasesWithBlockEvents (bCaseID)
	SELECT cw.CaseID 
	FROM @CurrentWins cw 
	WHERE EXISTS (
		SELECT CaseID 
		FROM @EventTypes et 
		INNER JOIN LeadEvent le WITH (NOLOCK) ON le.EventTypeID = et.EventTypeID AND le.EventDeleted = 0
		WHERE cw.CaseID = le.CaseID 
		AND et.StopsAWin = 1
	)
	
	-- We also need to ignore records if a "winning event" is not in place
	INSERT @CasesWithWinEvents (wCaseID)
	SELECT cw.CaseID 
	FROM @CurrentWins cw 
	WHERE EXISTS (
		SELECT * FROM @EventTypes et WHERE et.MakesAWin = 1
	)
	AND EXISTS (
		SELECT CaseID 
		FROM @EventTypes et 
		INNER JOIN LeadEvent le WITH (NOLOCK) ON le.EventTypeID = et.EventTypeID AND le.EventDeleted = 0
		WHERE cw.CaseID = le.CaseID 
		AND et.MakesAWin = 1
	)
	UNION
	-- unless of course no "winning event" is required
	SELECT cw.CaseID 
	FROM @CurrentWins cw 
	WHERE NOT EXISTS (
		SELECT * FROM @EventTypes et WHERE et.MakesAWin = 1
	)

	-- Remove blocked wins
	DELETE @CurrentWins 
	FROM @CasesWithBlockEvents b
	WHERE CaseID = b.bCaseID

	-- Remove unwanted wins
	DELETE @CurrentWins 
	WHERE NOT EXISTS (
		SELECT * FROM @CasesWithWinEvents w WHERE w.wCaseID = CaseID
	)


	-- Allow the function to return @CurrentWins to the calling SELECT statement
	RETURN 
END





GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetWinsDisputedByLeadType] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnGetWinsDisputedByLeadType] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetWinsDisputedByLeadType] TO [sp_executeall]
GO
