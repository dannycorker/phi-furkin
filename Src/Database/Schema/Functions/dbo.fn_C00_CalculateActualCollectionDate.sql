SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-09-22
-- Description:	Calculates actual collection date depending on account type and requested wait
-- Mods
--	2016-09-28 DCM Only apply mandate wait if payment method is Bank
--	2016-10-04 DCM Include search for earliest collection date form toher existing schedules
--	2016-10-31 DCM use min of mandate wait or actual collection date
--  2017-08-23 AJH updated function to use 1 of CP collection dates
--	2017-08-23 CPS added @PaymentGross to allow adjustment based on refund/collection
--  2020-03-02 GPR for JIRA AAG-202 | Added check on CountryID from ClientID as the switch for removing the AccountMandate check
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_CalculateActualCollectionDate]
(
	 @AccountID		INT
	,@PaymentMethod INT
	,@SeedDate		DATE
	,@WaitType		INT
	,@PaymentGross	DECIMAL(18,2)
)
RETURNS DATE	
AS
BEGIN


	DECLARE	@ActualCollectionDate DATE,
			@ClientID INT,
			@MinDate DATE,
			@WaitPeriod INT,
			@NewPaymentDate VARCHAR(10),
			@CountryID INT
			
	SELECT @ClientID=ClientID FROM Account WITH (NOLOCK) WHERE AccountID=@AccountID
	
	-- use if standard method isn't suitable
	IF @ClientID IN (10000) 
	BEGIN
	
		SELECT @ActualCollectionDate=@SeedDate
		
	END
	ELSE
	BEGIN
	
		-- add wait period
		SELECT @WaitPeriod=CASE @WaitType 
					WHEN 2 THEN RegularPaymentWait
					WHEN 3 THEN OneOffAdjustmentWait
					WHEN 4 THEN MandateWait
					WHEN 5 THEN DDCollectionProcessingInterval
				END
		FROM BillingConfiguration WITH (NOLOCK) 
		WHERE ClientID=@ClientID

		IF @WaitPeriod IS NULL
			SELECT @WaitPeriod=0

		-- Mandate - mandate wait is king and is set when account first set up or mandate reset
		-- GPR 2020-03-02 Execpt in Australia | for AAG-202
		IF @WaitType=4 AND @PaymentMethod=1
		BEGIN

			SELECT @CountryID = cl.CountryID FROM Clients cl WITH (NOLOCK)
			WHERE cl.ClientID = @ClientID

			IF @CountryID <> 14
			BEGIN
				SELECT TOP 1 @MinDate=FirstAcceptablePaymentDate 
				FROM AccountMandate WITH (NOLOCK) 
				WHERE AccountID=@AccountID
				AND MandateStatusID IN (1,2,3)
				ORDER BY AccountMandateID DESC
			END

			IF @MinDate IS NULL -- mandate request not sent yet; try to get date from any other schedules on this account
			BEGIN
						
					
				SELECT TOP 1 @MinDate= CASE WHEN PaymentDate < dbo.fnAddWorkingDays (CAST(dbo.fn_GetDate_Local() AS DATE) , 10) 
											THEN dbo.fnAddWorkingDays (CAST(dbo.fn_GetDate_Local() AS DATE), 10) 
											ELSE PaymentDate END
				FROM PurchasedProductPaymentSchedule ppps WITH (NOLOCK) 
				WHERE AccountID=@AccountID
				AND PaymentStatusID IN (1,5)
				ORDER BY ActualCollectionDate
						
			END
			
			SELECT @MinDate=ISNULL(@MinDate,dbo.fnAddWorkingDays (dbo.fn_GetDate_Local(), @WaitPeriod))
			
			IF @SeedDate < @MinDate
				SELECT @SeedDate = @MinDate
			
		END
		ELSE IF @WaitType NOT IN (4,5) /*Don't do this for initial schedule creation or mandate wait*/ 
		BEGIN
			
			SELECT @SeedDate=DATEADD(DAY,@WaitPeriod,@SeedDate)
			
		END
			-- add working days for direct debit payments
		IF @PaymentMethod=1 
		BEGIN
		
				SELECT top 1 @SeedDate=dbo.fnAddWorkingDays (w.[Date],0) FROM WorkingDays w WITH ( NOLOCK ) /*2017-08-22 Change to use CP Collection Dates*/
				Where w.Date >= @SeedDate
				--and ( w.Day IN (1, 8, 15, 22, 28) or @PaymentGross < 0.00 ) /*CPS 2017-08-23*/
			
			--SELECT @SeedDate=dbo.fnGetNextWorkingDate(@SeedDate, 0)
		
		END
		
		IF @WaitType = 5 
		BEGIN 
			
			/*We want to send the file X working days before payment is due to be taken (X is three, X will always be three but let's config this anyway for the reasons...)
			 We set X in the Billing config but as we run our collections in the early hours, and the ActualCollectionDate is day three, we only want to takje 2 working 
			 days from this date. Hence leave it as 3 in the table so it looks right.*/ 
			 
			SELECT @WaitPeriod = @WaitPeriod - 1 
			
			SELECT @SeedDate = dbo.fnAddWorkingDays(@SeedDate,-@WaitPeriod) 
		
		END 
		
		/*IF @PaymentMethod=1
		BEGIN
		
			SELECT @SeedDate=dbo.fnGetNextWorkingDate(@SeedDate, 0)
		
		END*/
		
		SELECT @ActualCollectionDate=@SeedDate
	
	END

	RETURN @ActualCollectionDate
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_CalculateActualCollectionDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_CalculateActualCollectionDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_CalculateActualCollectionDate] TO [sp_executeall]
GO
