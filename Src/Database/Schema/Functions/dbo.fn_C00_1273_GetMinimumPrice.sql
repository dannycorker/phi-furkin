SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2015-02-12
-- Description:	Returns the minimum price allowed on this scheme
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetMinimumPrice]
(
	@SchemeID INT,
	@StartDate DATE,
	@Net MONEY,
	@SpeciesID INT,
	@IsNewBusiness INT = 0
)
RETURNS MONEY	
AS
BEGIN

--declare
--	@SchemeID INT = 121574,
--	@StartDate DATE = '2015-02-12',
--	@Net MONEY = 10,
--	@SpeciesID INT = 42989,
--	@IsNewBusiness INT = 1

	
	DECLARE @MinPrice MONEY = 0,
			@Gross MONEY


	-- Get the discount appropriate for the scheme
	SELECT @Gross=CASE WHEN ISNULL(am.ValueMoney,0) <= @Net OR @IsNewBusiness=0 THEN @Net ELSE ISNULL(am.ValueMoney,0) END
	FROM MatterDetailValues scheme 
	INNER JOIN TableRows tr WITH (NOLOCK) ON scheme.MatterID=tr.MatterID AND tr.DetailFieldID=175494
	LEFT JOIN TableDetailValues from_date WITH (NOLOCK) ON tr.TableRowID=from_date.TableRowID AND from_date.DetailFieldID=175492
	LEFT JOIN TableDetailValues to_date WITH (NOLOCK) ON tr.TableRowID=to_date.TableRowID AND to_date.DetailFieldID=175493
	INNER JOIN TableDetailValues sp WITH (NOLOCK) ON tr.TableRowID=sp.TableRowID AND sp.DetailFieldID=175490
	INNER JOIN TableDetailValues am WITH (NOLOCK) ON tr.TableRowID=am.TableRowID AND am.DetailFieldID=175491
	WHERE scheme.DetailFieldID=145689 AND scheme.ValueInt=@SchemeID
	AND (from_date.ValueDate IS NULL OR @StartDate >= from_date.ValueDate)
	AND (to_date.ValueDate IS NULL OR @StartDate <= to_date.ValueDate)
	AND sp.ValueInt=@SpeciesID
	
	IF @Gross IS NULL 
		SELECT @Gross=@Net

	RETURN ROUND(@Gross,2)
 
END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetMinimumPrice] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetMinimumPrice] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetMinimumPrice] TO [sp_executeall]
GO
