SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Paul Richardson
-- Create date: 01-07-2016
-- Description:	Gets a customer, lead, case or matter detail value
-- =============================================
CREATE FUNCTION [dbo].[fnGetSimpleDvByIdentity]
(
	@CustomerID INT, 
	@LeadID INT, 
	@CaseID INT, 
	@MatterID INT, 
	@LeadOrMatter INT,
	@DetailFieldID INT
)
RETURNS VARCHAR(2000)
AS
BEGIN	
	
	DECLARE @DetailValue VARCHAR(2000)

	IF @LeadOrMatter = 1
	BEGIN
		SELECT @DetailValue = DetailValue 
		FROM LeadDetailValues WITH (NOLOCK)
		WHERE LeadID = @LeadID 
		AND DetailFieldID = @DetailFieldID
	END

	IF @LeadOrMatter = 2
	BEGIN
		SELECT @DetailValue = DetailValue 
		FROM MatterDetailValues WITH (NOLOCK)
		WHERE MatterID = @MatterID 
		AND DetailFieldID = @DetailFieldID
	END
		
	IF @LeadOrMatter = 10
	BEGIN
		SELECT @DetailValue = DetailValue 
		FROM CustomerDetailValues WITH (NOLOCK)
		WHERE CustomerID = @CustomerID 
		AND DetailFieldID = @DetailFieldID
	END
	
	IF @LeadOrMatter = 11
	BEGIN
		SELECT @DetailValue = DetailValue 
		FROM CaseDetailValues WITH (NOLOCK)
		WHERE CaseID = @CaseID 
		AND DetailFieldID = @DetailFieldID
	END

	RETURN @DetailValue
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleDvByIdentity] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetSimpleDvByIdentity] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetSimpleDvByIdentity] TO [sp_executeall]
GO
