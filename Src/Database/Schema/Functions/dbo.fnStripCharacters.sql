SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:      Cathal Sherry
-- Create date: 2012-10-03
-- Description: Remove chars based on regex
-- =============================================

CREATE FUNCTION [dbo].[fnStripCharacters] 
( 
    @String NVARCHAR(MAX),  
    @MatchExpression VARCHAR(255) 
) 
RETURNS NVARCHAR(MAX) 
AS 
BEGIN 
/*
Alphabetic only:
SELECT dbo.fnStripCharacters('a1!s2@d3#f4$', '^a-z') 

Numeric only:
SELECT dbo.fnStripCharacters('a1!s2@d3#f4$', '^0-9') 

Alphanumeric only:
SELECT dbo.fnStripCharacters('a1!s2@d3#f4$', '^a-z0-9') 

Non-alphanumeric:
SELECT dbo.fnStripCharacters('a1!s2@d3#f4$', 'a-z0-9') 
*/

    SET @MatchExpression =  '%['+@MatchExpression+']%' 
 
    WHILE PatIndex(@MatchExpression, @String) > 0 
        SET @String = Stuff(@String, PatIndex(@MatchExpression, @String), 1, '') 
 
    RETURN @String 
 
END 








GO
GRANT VIEW DEFINITION ON  [dbo].[fnStripCharacters] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnStripCharacters] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnStripCharacters] TO [sp_executeall]
GO
