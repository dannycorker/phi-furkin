SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- ALTER date: 2012-02-09
-- Description:	Returns a list of related cases and matters for the passed in case / matter 
-- Modified		ROH 2014-04-30	Get Policy Type from Claim (onset) not current policy type
--				SB	2014-10-09	Moved logic from claim calcs to remove claims that are not reinstatement from list of related claims for a reinstatement claim
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetRelatedClaims]
(
	@MatterID INT,
	@ForceLimitOnCondition BIT	-- If we are on a reinstatement policy the related claims are all in the lead.  This param allows us to override this behavoir and return only
)								-- the condition group
RETURNS 
	@Relationships TABLE 
	(
		CaseID INT,
		MatterID INT,
		ParentID INT
	)
AS
BEGIN
	
	DECLARE @LeadID INT
	SELECT  @LeadID = m.LeadID
	FROM dbo.Matter m WITH (NOLOCK) 
	WHERE m.MatterID = @MatterID
	
	-- Check to see if this is a reinstatement policy
	DECLARE @IsReinstatement BIT

	-- Get Policy Type from Claim (condition onset) instead of the current policy
	DECLARE @PolicyType INT
	SELECT @PolicyType = dbo.fn_C00_1272_GetPolicyTypeFromClaim(@MatterID)
	SELECT @IsReinstatement = CASE WHEN @PolicyType = 76176 THEN 1 ELSE 0 END
	
	
	
	DECLARE @ParentID INT
	-- get the parent id so we can use below to get all related
	SELECT @ParentID = ISNULL(ParentID, ClaimID)
	FROM fn_C00_1272_GetClaimRelationships(@LeadID)
	WHERE ClaimID = @MatterID
	
	INSERT @Relationships (CaseID, MatterID, ParentID)
	SELECT	m.CaseID, m.MatterID, r.ParentID
	FROM fn_C00_1272_GetClaimRelationships(@LeadID) r
	INNER JOIN dbo.Matter m WITH (NOLOCK) ON r.ClaimID = m.MatterID
	WHERE (ClaimID = @ParentID OR ParentID = @ParentID)			-- Either link on the condition grouping
	OR (@IsReinstatement = 1 AND @ForceLimitOnCondition = 0)	-- Or return all if this is a reinstatement and we are not forcing just the condition grouping
	
	
	IF @IsReinstatement = 1 -- reinstatement
	BEGIN
	
		-- if we are a reinstatement type for this claim we need to check previous policy years that were not reinstatement
		-- and remove claim rows where the date of loss matches
		DECLARE @NonReinstatementYears TABLE
		(
			PolicyFrom DATE,
			PolicyTo DATE
		)

		INSERT INTO @NonReinstatementYears (PolicyFrom, PolicyTo)
		SELECT tdvStart.ValueDate, tdvEnd.ValueDate
		FROM dbo.fn_C00_1272_Policy_GetPolicyHistoryRows(@MatterID) r
		INNER JOIN dbo.TableDetailValues tdvStart WITH (NOLOCK) ON r.TableRowID = tdvStart.TableRowID AND tdvStart.DetailFieldID = 145663
		INNER JOIN dbo.TableDetailValues tdvEnd WITH (NOLOCK) ON r.TableRowID = tdvEnd.TableRowID AND tdvEnd.DetailFieldID = 145664
		INNER JOIN dbo.TableDetailValues tdvType WITH (NOLOCK) ON r.TableRowID = tdvType.TableRowID AND tdvType.DetailFieldID = 145665
		INNER JOIN dbo.ResourceListDetailValues rdvType WITH (NOLOCK) ON tdvType.ResourceListID = rdvType.ResourceListID AND rdvType.DetailFieldID = 144319
		WHERE rdvType.ValueInt != 76176
		
		DECLARE @NonReinstatementCount INT
		SELECT @NonReinstatementCount = COUNT(*) 
		FROM @NonReinstatementYears
			
		IF @NonReinstatementCount > 0
		BEGIN
			-- Delete claims where the key date (date of loss) is within one of the non reinstatement years
			DECLARE @NonReinstatementClaims TABLE (MatterID INT, KeyDate DATE)
			INSERT @NonReinstatementClaims (MatterID, KeyDate)
			SELECT MatterID, dbo.fn_C00_1272_GetDateToUseForCalcs(MatterID, NULL, NULL) /*SA - 2017-06-08 - amended from  C224_GetDateToUseForCalcs*/
			FROM @Relationships
			
			DELETE @Relationships
			WHERE MatterID IN
			(
				SELECT c.MatterID
				FROM @NonReinstatementClaims c
				INNER JOIN @NonReinstatementYears n ON c.KeyDate >= n.PolicyFrom AND c.KeyDate < n.PolicyTo
				AND c.MatterID != @MatterID -- double check	
			)
		END
	
	END
	
	
	DECLARE @Count INT
	SELECT @Count = COUNT(*) 
	FROM @Relationships
	
	-- If we have no relationships then just return the passed in matter id
	IF @Count = 0
	BEGIN
		INSERT @Relationships(CaseID, MatterID, ParentID)
		SELECT m.CaseID, m.MatterID, NULL
		FROM dbo.Matter m WITH (NOLOCK)
		WHERE m.MatterID = @MatterID
	END

	RETURN

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetRelatedClaims] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C00_1272_GetRelatedClaims] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetRelatedClaims] TO [sp_executeall]
GO
