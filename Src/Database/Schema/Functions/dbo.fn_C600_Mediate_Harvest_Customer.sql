SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ian Slack / Gavin Reynolds
-- Create date:	2020-01-27
-- Description:	harvests customer json object
-- 2020-01-27 GPR						| Altered to use @Include flag
-- 2020-01-27 CPS for JIRA LAGCLAIM-160 | Updated to return XML
-- 2020-01-27 CPS for JIRA LAGCLAIM-266 | Updated to match Vision customer shape
-- 2020-01-27 CPS for JIRA LAGCLAIM-266 | Updated back to JSON for Ian
-- 2020-01-27 MAB vulnerability / DDA replaced with call to fn_C600_GetVulnerabilityDdaDetailsAsString
-- 2020-01-27 CPS for JIRA LAGCLAIM-369 | Update to match end to end solution document
-- 2020-01-29 MAB for Jira LAGCLAIM-366 | modified data definitions to match requirements and synch with related data definitions
-- 2020-01-31 CPS for JIRA LAGCLAIM-369 | Included @IncludePets and @IncludePolicies parameters
-- 2020-01-31 CPS for JIRA LAGCLAIM-369 | Return ObjectID
-- 2020-02-06 CPS for JIRA LAGCLAIM-326 | Return the mapped ResourceListID for affinities
-- 2020-02-07 CPS for JIRA LAGCLAIM-346 | Updated to include multiple pets
-- 2020-03-24 CPS for JIRA LAGCLAIM-VIS | Updated to match vision shapes
-- 2020-03-26 MAB for JIRA AAG-547 | copied and renamed from fn_C600_Harvest_Customer
-- 2020-03-27 MAB removed redundant join to affinity resource list detail field "L&G ResourceListID"
-- 2020-05-25 JEL Added ELSE 0 to DDA case statement 
-- 2020-07-29 ALM Updated to used AddressID from OneVision table 
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Mediate_Harvest_Customer]
(
	 @CustomerID		INT
	,@PolicyID			INT
	,@VisionMode		BIT = 0
	,@IncludePets		BIT = 0
	,@IncludePolicies	BIT = 0
)
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE  @JsonData	VARCHAR(MAX) =''
			,@PetJson	VARCHAR(MAX) = ''

	DECLARE @VisionTitles TABLE ( Title		VARCHAR(50), VisionTitleID		INT )
	DECLARE @VisionComms  TABLE ( CommPref	VARCHAR(50), VisionCommsPrefID	INT )

	IF @VisionMode = 1
	BEGIN
		SELECT	 @IncludePets = 0
				,@IncludePolicies = 0

		INSERT @VisionComms ( CommPref, VisionCommsPrefID )
		VALUES	 ('Do not contact'	, 0 )
				,('Paper'			, 1 )
				,('Email'			, 2 )
				,('Electronic'		, 2 )

		INSERT @VisionTitles (Title, VisionTitleID)
		VALUES   ('Unknown'		,0)
				,('Mr'			,1)
				,('Mrs'			,2)
				,('Miss'		,3)
				,('Ms'			,4)
				,('Mx'			,5)
				,('Dr'			,6)
				,('Rev'			,7)
				,('Sir'			,8)
				,('Lady'		,9)
				,('Lord'		,10)
				,('Captain'		,11)
				,('Major'		,12)
				,('Professor'	,13)
				,('Dame'		,14)
				,('Colonel'		,15)		
	END

	IF @CustomerID > 0
	BEGIN
		SELECT  @JsonData +=
		(
			SELECT TOP 1
				 CASE WHEN @VisionMode = 0 THEN cdv.ValueInt END						[affinityResourceListId]
				,CASE WHEN @VisionMode = 0 THEN t.Title END								[title]
				,cu.FirstName															[firstName]
				,cu.LastName															[lastName]
				,cu.DateOfBirth															[birthdayDate]
				,CASE WHEN @VisionMode = 1 THEN ISNULL(l.LanguageName, 'English') END	[language]
				,cu.EmailAddress														[email]
				,cu.HomeTelephone														[phoneNumber]
				,REPLACE(REPLACE(REPLACE(
				CASE WHEN LEFT(cu.MobileTelephone, 1) = '0' THEN '1' + SUBSTRING(cu.MobileTelephone, 2, 20)
				WHEN LEFT(cu.MobileTelephone, 1) = '1' THEN cu.MobileTelephone 
				ELSE '1' + SUBSTRING(cu.MobileTelephone, 1, 20) 
				END, ' ', ''), '+', ''), '-', '') [mobileNumber] /*2021-02-09 ALM SDPRU-212*/
				,CASE WHEN @VisionMode = 1 AND dbo.fn_C600_GetVulnerabilityDdaDetailsAsString(@CustomerID,':',';') <> '' THEN 3 /*Other*/ ELSE 0 END [ddaStatus]
				,ISNULL(NULLIF(LEFT (dbo.fn_C600_GetVulnerabilityDdaDetailsAsString(@CustomerID,':',';'), 100 /*Vision max length*/), ''), '')	[ddaDescription] /*2019-12-13 MAB */
				,CASE WHEN @VisionMode = 1 THEN ISNULL(vcMethod.VisionCommsPrefID,'0') ELSE liMethod.ItemValue END [communicationPreference]
				,CASE WHEN @VisionMode = 1 AND vt.VisionTitleID <> 0 THEN vt.VisionTitleID END	[titleId]
				,(
					SELECT mdvClaimBrandID.ValueInt
					FROM MatterDetailValues mdvClaimBrandID WITH (NOLOCK)
					WHERE @VisionMode = 1
					AND mdvClaimBrandID.MatterID = dbo.fn_C00_1273_GetCurrentSchemeFromMatter (@PolicyID)
					AND mdvClaimBrandID.DetailFieldID = 315884 /*Claim Product ID*/
				)  [brandId]
				, (	
					SELECT 
						 CASE WHEN @VisionMode = 1 THEN ISNULL(oneVision.VisionAddressID, 0) ELSE NULL END	[addressId]
						,[addr].Address1												[address1]
						,[addr].Address2												[address2]
						,[addr].Address2												[address3]
						,[addr].Town													[townCity]
						,[addr].County													[regionCounty]
						,[addr].PostCode									[postalCode]
						,CASE WHEN @VisionMode = 1 THEN co.CountryName ELSE NULL END	[country]
						,CASE WHEN @VisionMode = 1 THEN 1 ELSE NULL END					[addressType]
					FROM Customers [addr] WITH(NOLOCK)
					LEFT JOIN Country co WITH(NOLOCK) on co.CountryID = [addr].CountryID
					WHERE [addr].CustomerID = [cu].CustomerID
					FOR JSON PATH
				)																		[addresses]
				,CASE WHEN @VisionMode = 0  THEN NULLIF(cdvArrears.ValueInt,0) END		[inArrears]
				,CASE WHEN @IncludePets = 1 THEN '{PetJson}' END						[pets]
				,CASE WHEN @VisionMode = 0  THEN cdvGuid.DetailValue END				[objectID]
			FROM	Customers cu WITH (NOLOCK)
			LEFT JOIN Language l						WITH (NOLOCK) on l.LanguageID = cu.LanguageID
			INNER JOIN CustomerDetailValues cdv			WITH (NOLOCK) on cdv.CustomerID = cu.CustomerID	AND cdv.DetailFieldID = 170144 /*Affinity Details*/
			LEFT JOIN CustomerDetailValues cdvMethod	WITH (NOLOCK) on cdvMethod.CustomerID = cu.CustomerID	AND cdvMethod.DetailFieldID = 170257 /*Send documents by*/
			LEFT JOIN LookupListItems liMethod			WITH (NOLOCK) on liMethod.LookupListItemID = cdvMethod.ValueInt
			LEFT JOIN @VisionComms vcMethod							  on vcMethod.CommPref = liMethod.ItemValue
			INNER JOIN Titles t							WITH (NOLOCK) on t.TitleID = ISNULL(cu.TitleID,0)
			LEFT JOIN @VisionTitles vt								  on vt.Title = t.Title
			LEFT JOIN CustomerDetailValues cdvArrears	WITH (NOLOCK) on cdvArrears.CustomerID = cu.CustomerID AND cdvArrears.DetailFieldID = -1 /*to be provided by Gavin*/
			LEFT JOIN CustomerDetailValues cdvGuid		WITH (NOLOCK) on cdvGuid.CustomerID = cu.CustomerID and cdvGuid.DetailFieldID = 313784 /*API Transfer Mapping GUID*/
			JOIN OneVision oneVision					WITH (NOLOCK) ON oneVision.CustomerID = cu.CustomerID /*One Vision join for AddressID*/
			WHERE	cu.IsBusiness = 0
			AND		cu.CustomerID = @CustomerID
			FOR JSON PATH, WITHOUT_ARRAY_WRAPPER
		)
	END

	IF @IncludePets = 1
	BEGIN
		-- 2020-02-07 CPS for JIRA LAGCLAIM-346 | Updated to include multiple pets
		SELECT @PetJson = '['

		SELECT @PetJson += dbo.fn_C600_Mediate_Harvest_Pet(l.LeadID,0,@IncludePolicies,1) + ','
		FROM Lead l WITH (NOLOCK) 
		WHERE l.CustomerID = @CustomerID
		AND l.LeadTypeID = 1492 /*PolicyAdmin*/

		SELECT @PetJson = LEFT(@PetJson,LEN(@PetJson)-1)
		SELECT @PetJson += ']'

		SELECT @JsonData = REPLACE(@JsonData,'"{PetJson}"',@PetJson)
	END

	RETURN @JsonData

END











GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Mediate_Harvest_Customer] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C600_Mediate_Harvest_Customer] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Mediate_Harvest_Customer] TO [sp_executeall]
GO
