SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-12-16
-- Description: Calculates shannon entropy Or metric entropy from a string
-- Description: 'Shannon'		= minimum number of bits required to encode each symbol in binary
-- Description: 'ShannonString' = minimum number of bits required to encode the string in binary
--'Description: 'Metric'		= relative amount of order in the string. Metric entropy is on a scale from 0 to 1
--									       _                        
--									       \`*-.                    
--									        )  _`-.                 
--									       .  : `. .                
--									       : _   '  \               
--									       ; *` _.   `*-._          
--									       `-.-'          `-.       
--			 P(A|B) = P(B|A) P(A)	         ;       `       `.     
--				      ------------	         :.       .        \    
--						 P(B)              . \  .   :   .-'   .   
--									         '  `+.;  ;  '      :   
--									         :  '  |    ;       ;-. 
--									         ; '   : :`-:     _.`* ;
--									      .*' /  .*' ; .*`- +'  `*' 
--									      `*-*   `*-*  `*-*'        
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_StringEntropy]
(
	@Input Varchar(2000)
	,@Method Varchar(2000)
)
RETURNS varchar(2000)
AS
BEGIN

	DECLARE @Length numeric  = LEN(@input)
			,@Answer Varchar(2000)
	DECLARE @MathBox TABLE (CharCount INT,CharValue varchar(2000),CharFrequency float,HofX float,ASCIIValue INT)
	
	IF @Method <> 'Shannon' and @Method <> 'ShannonString' AND @Method <> 'Metric' 
	BEGIN 		
		SELECT @Answer = 'Please input a valid method: ''Shannon'' , ''ShannonString'' , ''Metric'''			
	END 
	ELSE
	BEGIN
	
		INSERT INTO @MathBox  
		SELECT Count(ASCII(SUBSTRING(@Input,t.n,1)) )				[Char Count]
		,CM.CharValue
		,(Count( ASCII(SUBSTRING(@Input,t.n,1)) ) )	/ @Length		[Char Frequency]
		/*| RelativeFrequencyofChar * Log(RelativeFrequencyOfChar)Base2 |*/
		,-(((Count( ASCII(SUBSTRING(@Input,t.n,1)) ) ) / @Length) * (LOG((Count( ASCII(SUBSTRING(@Input,t.n,1)) ) )	/ @Length)) / LOG(2)) 	[H(X)]
		,ASCII(SUBSTRING(@Input,t.n,1))								[ASCII Value]
		FROM Tally t WITH (NOLOCK) 
		INNER JOIN CharMap CM WITH (NOLOCK) on cm.asciivalue = ASCII(SUBSTRING(@Input,t.n,1))
		GROUP BY ASCII(SUBSTRING(@Input,t.n,1))
		,CM.CharValue

		SELECT @Answer = CASE 
			WHEN @Method = 'Shannon'		THEN (SELECT SUM(HofX) FROM @MathBox) 
			WHEN @Method = 'ShannonString'	THEN (SELECT Ceiling((SUM(HofX))) * @Length FROM @MathBox) 
			WHEN @Method = 'Metric'			THEN (SELECT (SUM(HofX))/ @Length FROM @MathBox) END

	END 
	
	RETURN @Answer
	
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_StringEntropy] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_StringEntropy] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_StringEntropy] TO [sp_executeall]
GO
