SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:  Krzysztof Nagrabski
-- Create date: 2013-05-16
-- Description: Returns sum of next or previous payments within table rows. Created for LA2Plus payment dates originally.
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_SumValuesByNextOrPrevDateTDV]
(
 @AnyID INT,
 @DetailFieldSubTypeID INT,
 @DetailFieldID INT,
 @SearchDirection INT,  /* 1 = Next, 2 = Prev */
 @BlankNotNull BIT,   /* Option to return an empty string rather than NULL */
 @DetailFieldIDToReturn INT, /* Having found the row with the appropriate date, return the value of this column from the same row */
 @ClientID INT,
 @TableDetailFieldID INT
)

RETURNS MONEY
AS
BEGIN
 

 DECLARE @AnyValue MONEY

 /* TableDetailValues for a Matter */
 IF @DetailFieldSubTypeID = 2
 BEGIN
  IF @SearchDirection = 1
  BEGIN
   /* Find any next date */
   SELECT @AnyValue = SUM(tdvOtherColumn.ValueMoney)
   FROM dbo.TableDetailValues tdv WITH (NOLOCK) 
   INNER JOIN dbo.TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID and tr.DetailFieldID = @TableDetailFieldID
   INNER JOIN dbo.TableDetailValues tdvOtherColumn WITH (NOLOCK) ON tdvOtherColumn.TableRowID = tdv.TableRowID AND tdvOtherColumn.DetailFieldID = @DetailFieldIDToReturn
   WHERE tdv.ClientID = @ClientID 
   AND tdv.MatterID = @AnyID 
   AND tdv.DetailFieldID = @DetailFieldID 
   AND tdv.ValueDate >= dbo.fnDateOnly(dbo.fn_GetDate_Local()) 
   
  END
  ELSE
  BEGIN
   /* Find previous date */
   SELECT @AnyValue = SUM(tdvOtherColumn.ValueMoney)
   FROM dbo.TableDetailValues tdv WITH (NOLOCK) 
   INNER JOIN TableRows tr WITH (NOLOCK) on tr.TableRowID = tdv.TableRowID and tdv.DetailFieldID = @TableDetailFieldID
   INNER JOIN dbo.TableDetailValues tdvOtherColumn WITH (NOLOCK) ON tdvOtherColumn.TableRowID = tdv.TableRowID AND tdvOtherColumn.DetailFieldID = @DetailFieldIDToReturn
   WHERE tdv.ClientID = @ClientID 
   AND tdv.MatterID = @AnyID 
   AND tdv.DetailFieldID = @DetailFieldID 
   AND tdv.ValueDate <= dbo.fnDateOnly(dbo.fn_GetDate_Local()) 
   
  END
 END
 
 /* Option to return an empty string rather than NULL */
 IF @BlankNotNull = 1 AND @AnyValue IS NULL
 BEGIN
  SET @AnyValue = 0.00
 END
 
 RETURN @AnyValue

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumValuesByNextOrPrevDateTDV] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_SumValuesByNextOrPrevDateTDV] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_SumValuesByNextOrPrevDateTDV] TO [sp_executeall]
GO
