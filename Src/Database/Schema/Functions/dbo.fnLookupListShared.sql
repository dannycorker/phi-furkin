SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-07-21
-- Description:	Merges in shared lead types
-- =============================================
CREATE FUNCTION [dbo].[fnLookupListShared] 
(	
	@ClientID INT
)
RETURNS TABLE 
AS
RETURN 
(	
	SELECT [LookupListID]
		  ,[LookupListName]
		  ,[LookupListDescription]
		  ,CASE WHEN s.LeadTypeShareID IS NULL THEN ll.ClientID ELSE s.ClientID END AS ClientID
		  ,[Enabled]
		  ,[SortOptionID]
		  ,[SourceID]
		  ,[WhoCreated]
		  ,[WhenCreated]
		  ,[WhoModified]
		  ,[WhenModified]
		  ,CASE WHEN s.LeadTypeShareID IS NULL THEN ll.LeadTypeID ELSE s.SharedTo END AS LeadTypeID
		  ,CASE WHEN s.LeadTypeShareID IS NULL THEN CAST(0 AS BIT) ELSE CAST(1 AS BIT) END AS IsShared
	FROM dbo.LookupList ll WITH (NOLOCK)
	LEFT JOIN dbo.LeadTypeShare s WITH (NOLOCK) ON ll.LeadTypeID = s.SharedFrom AND s.ClientID = @ClientID
)


GO
GRANT VIEW DEFINITION ON  [dbo].[fnLookupListShared] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnLookupListShared] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnLookupListShared] TO [sp_executeall]
GO
