SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Umer Hamid
-- Create date: 2018-05-11
-- Description:	Returns the number of policies in arrears for a specific matter
-- 2018-06-12 UH added rules engine params
-- 2018-08-17 AHOD Updated fn with dbo.fn_C00_Billing_GetPolicyAdminMatterFromCollections
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_ReturnNumberofPoliciesInArrears]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS INT	
AS
BEGIN

	DECLARE @PoliciesInArrears INT = 0
	
		/*If Collections Matter fed in then change this to the PA matter*/ 
	IF EXISTS (SELECT * FROM Lead l WITH ( NOLOCK ) 
			   INNER JOIN Matter m WITH ( NOLOCK ) on m.LeadID = l.LeadID 
			   WHERE l.LeadTypeID = 1493
			   AND m.MatterID = @MatterID) 
	BEGIN 
	
		SELECT @MatterID = dbo.fn_C00_Billing_GetPolicyAdminMatterFromCollections(@MatterID) 
	
	END 	
	
	IF EXISTS (SELECT * FROM Lead l WITH ( NOLOCK ) 
			   INNER JOIN Matter m WITH ( NOLOCK ) on m.LeadID = l.LeadID 
			   WHERE l.LeadTypeID = 1490
			   AND m.MatterID = @MatterID) 
	BEGIN 
	
		SELECT @MatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@MatterID) 
	
	END 	
	
	-- Return the number of policies that are in arrears
	SELECT @PoliciesInArrears=
	COUNT(*) FROM Matter m WITH ( NOLOCK ) 
	INNER JOIN MatterDetailValues mdv WITH ( NOLOCK ) on mdv.MatterID = m.MatterID and mdv.DetailFieldID = 177074 /*Purchased Policy ID*/
	INNER JOIN PurchasedProductPaymentSchedule p WITH ( NOLOCK ) on p.PurchasedProductID = mdv.ValueInt
	INNER JOIN PurchasedProductPaymentScheduleType pt WITH ( NOLOCK )  on pt.PurchasedProductPaymentScheduleTypeID = p.PurchasedProductPaymentScheduleTypeID 
	Where m.MatterID = @MatterID 
	AND p.PaymentStatusID NOT IN (6,3,8,7,2)
	AND p.ActualCollectionDate <  dbo.fn_GetDate_Local()

	RETURN @PoliciesInArrears
 
END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReturnNumberofPoliciesInArrears] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_ReturnNumberofPoliciesInArrears] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_ReturnNumberofPoliciesInArrears] TO [sp_executeall]
GO
