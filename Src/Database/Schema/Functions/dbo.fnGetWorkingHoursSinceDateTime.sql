SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2014-03-31
-- Description:	Find the number of working hours since specified datetime.
--              Defaults to 09:00 start time and 17:00 end time
--              Returns number of working hours to 1 decimal place
-- =============================================
CREATE FUNCTION [dbo].[fnGetWorkingHoursSinceDateTime] 
( 
	@StartDateTime DATETIME = '2014-04-01 06:00',
	@DailyStartTime VARCHAR(5) = '11:00',
	@DailyEndTime VARCHAR(5) = '17:00'
)
RETURNS numeric(36,1)
AS
BEGIN

	DECLARE @MinsPerDay INT,
	@DST DATETIME,
	@DET DATETIME,
	@DailyMinutes INT,
	@WorkingHours NUMERIC(36,1) = 0,
	@FirstDayMins INT = 0,
	@MidDaysMins INT = 0,
	@LastDayMins INT = 0,
	@MidDays INT

	-- calculate number of minutes in a standard day	
	SELECT @DST=CONVERT(DATETIME,CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) + ' ' + @DailyStartTime),
			@DET=CONVERT(DATETIME,CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) + ' ' + @DailyEndTime)
	
	SELECT @DailyMinutes=DATEDIFF(MINUTE,CONVERT(DATETIME,@DailyStartTime),CONVERT(DATETIME,@DailyEndTime))
	
	IF @StartDateTime < dbo.fn_GetDate_Local()
	BEGIN
	
		-- first day minutes
		-- calculate the number of minutes on the specified starting day
		IF 1 = (SELECT dbo.fnIsWorkingDay(@StartDateTime))
		BEGIN

			SELECT @DST=CONVERT(DATETIME,CONVERT(VARCHAR(10),@StartDateTime,120) + ' ' + @DailyStartTime),
				@DET=CONVERT(DATETIME,CONVERT(VARCHAR(10),@StartDateTime,120) + ' ' + @DailyEndTime)
			
			IF @StartDateTime <= @DST AND dbo.fn_GetDate_Local() >= @DET		-- full day
				SELECT @FirstDayMins=@DailyMinutes
			ELSE IF @StartDateTime > @DST AND dbo.fn_GetDate_Local() >= @DET -- late start and normal finish
				SELECT @FirstDayMins=DATEDIFF(MINUTE,@StartDateTime,@DET)
			ELSE IF @StartDateTime > @DST AND dbo.fn_GetDate_Local() < @DET  -- late start and early finish
				SELECT @FirstDayMins=DATEDIFF(MINUTE,@StartDateTime,dbo.fn_GetDate_Local())
			ELSE IF dbo.fn_GetDate_Local() > @DST							-- normal start and early finsh
				SELECT @FirstDayMins=DATEDIFF(MINUTE,@DST,dbo.fn_GetDate_Local())
		END	
		
		-- middle days minutes
		-- calculate the number of minutes for all working days between the specified start day and today
		SELECT @MidDays=COUNT(*) FROM WorkingDays wd WITH (NOLOCK) 
		WHERE wd.IsWorkDay=1 
			AND wd.Date BETWEEN DATEADD(DAY,1,CONVERT(DATE,@StartDateTime)) 
			AND DATEADD(DAY,-1,CONVERT(DATE,dbo.fn_GetDate_Local()))
			
		IF 0 < @MidDays			
			SELECT @MidDaysMins = @MidDays * @DailyMinutes
			
		-- last day minutes
		-- calcualte the number of minutes for today (the last day) so long as today isn't the specified start day 	 
		IF CONVERT(DATE,@StartDateTime) <> CONVERT(DATE,dbo.fn_GetDate_Local())
		BEGIN

			IF 1 = (SELECT dbo.fnIsWorkingDay(dbo.fn_GetDate_Local()))
			BEGIN
				SELECT @DST=CONVERT(DATETIME,CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) + ' ' + @DailyStartTime),
					@DET=CONVERT(DATETIME,CONVERT(VARCHAR(10),dbo.fn_GetDate_Local(),120) + ' ' + @DailyEndTime)
				
				IF dbo.fn_GetDate_Local() >= @DET  -- full day
					SELECT @LastDayMins=@DailyMinutes
				ELSE IF dbo.fn_GetDate_Local() > @DST -- early finish
					SELECT @LastDayMins=DATEDIFF(MINUTE,@DST,dbo.fn_GetDate_Local())

			END	
		
		END		
		
	END
	
	--SELECT @FirstDayMins,@MidDaysMins,@LastDayMins 

	SELECT @WorkingHours=ROUND((@FirstDayMins+@MidDaysMins+@LastDayMins)/60.0,1)

	--SELECT @WorkingHours
		
	RETURN @WorkingHours

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetWorkingHoursSinceDateTime] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetWorkingHoursSinceDateTime] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetWorkingHoursSinceDateTime] TO [sp_executeall]
GO
