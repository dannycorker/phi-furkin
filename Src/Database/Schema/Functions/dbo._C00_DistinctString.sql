SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Thomas Doyle
-- Create date: 2015-11-25
-- Description: Return distinct values from a string
-- =============================================
CREATE FUNCTION [dbo].[_C00_DistinctString]
(
	@List VARCHAR(MAX)
	,@Delim CHAR 
)
RETURNS varchar(MAX)
AS
BEGIN

DECLARE @ParsedList TABLE
(
Item VARCHAR(MAX)
)

DECLARE @list1 VARCHAR(MAX), @Pos INT, @rList VARCHAR(MAX)
SET @list = LTRIM(RTRIM(@list)) + @Delim
SET @pos = CHARINDEX(@delim, @list, 1)

WHILE @pos > 0
	BEGIN
	
		SET @list1 = LTRIM(RTRIM(LEFT(@list, @pos - 1)))
		
	IF @list1 <> ''
		
		INSERT INTO @ParsedList VALUES (CAST(@list1 AS VARCHAR(MAX)))
		SET @list = SUBSTRING(@list, @pos+1, LEN(@list))
		SET @pos = CHARINDEX(@delim, @list, 1)
		
	END
	
SELECT @rlist = COALESCE(@rlist+',','') + item
FROM (SELECT DISTINCT Item FROM @ParsedList) t
	
RETURN @rlist

END


GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DistinctString] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[_C00_DistinctString] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[_C00_DistinctString] TO [sp_executeall]
GO
