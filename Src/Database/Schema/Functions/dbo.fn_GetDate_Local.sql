SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author: Dzianis Shakhalevich
-- Create date: 2020-07-09
-- Description: Returns the local date
-- =============================================
CREATE FUNCTION [dbo].[fn_GetDate_Local]()
RETURNS DATETIME
AS
BEGIN
	DECLARE @Now DATETIME = GETDATE(),
		@Local DATETIME,
		@TimeZoneID INT,
		@ClientID INT = dbo.fnGetPrimaryClientID()

	SELECT @TimeZoneID = TimeZoneID
	FROM dbo.Clients WITH (NOLOCK)
	WHERE ClientID = @ClientID

	--Make sure the time zone is set for current client
	IF @TimeZoneID IS NOT NULL
	BEGIN

		SELECT @Local = dbo.fnGmtToRegional(@Now, @TimeZoneID)

	END

	--If the function fnGmtToRegional returned NULL or the time zone is not set
	IF @Local IS NULL
	BEGIN

		SELECT @Local = GETDATE()

	END

	RETURN @Local
END
GO
GRANT EXECUTE ON  [dbo].[fn_GetDate_Local] TO [ReadOnly]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_GetDate_Local] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_GetDate_Local] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_GetDate_Local] TO [sp_executeall]
GO
