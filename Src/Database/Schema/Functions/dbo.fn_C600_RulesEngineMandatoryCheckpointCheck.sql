SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-10-31
-- Description:	Take a rules engine XML output and make sure that mandatory checkpoints have been hit
-- 2018-06-04	GPR moved to C600
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_RulesEngineMandatoryCheckpointCheck]
(
	@PremiumCalculationID	INT
)
RETURNS 
@Display TABLE
(
	 CheckpointName	VARCHAR(2000)
	,ErrorMessage	VARCHAR(2000)
)	
AS
BEGIN

	DECLARE @RuleSetID	INT
	
	SELECT  @RuleSetID = r.RuleSetID
	FROM PremiumCalculationDetailValues cdv WITH ( NOLOCK ) 
	INNER JOIN RulesEngine_Rules r WITH ( NOLOCK ) on r.RuleID = cdv.RuleID
	WHERE cdv.PremiumCalculationDetailID = @PremiumCalculationID

	;WITH Checkpoints AS
	(
		SELECT tr.TableRowID, tdvCheckpoint.DetailValue [CheckpointName], tdvMessage.DetailValue [ErrorMessage]
		FROM TableRows tr WITH ( NOLOCK )
		INNER JOIN TableDetailValues tdvCheckpoint	WITH ( NOLOCK ) on tdvCheckpoint.TableRowID = tr.TableRowID AND tdvCheckpoint.DetailFieldID = 180037 /*Checkpoint Name*/
		INNER JOIN TableDetailValues tdvRuleSetID	WITH ( NOLOCK ) on tdvRuleSetID.TableRowID = tr.TableRowID AND tdvRuleSetID.DetailFieldID = 180038 /*RuleSetID*/
		INNER JOIN TableDetailValues tdvMessage WITH ( NOLOCK ) on tdvMessage.TableRowID = tr.TableRowID AND tdvMessage.DetailFieldID = 180042 /*Error Message if this checkpoint is missed*/
		WHERE tr.DetailFieldID = 180043 /*Mandatory Checkpoints*/
		AND tdvRuleSetID.ValueInt = @RuleSetID
		AND tdvCheckpoint.DetailValue not like 'Error%'
	)
	,MatchedCheckpoints AS
	(
		SELECT * 
		FROM PremiumCalculationDetailValues cdv WITH ( NOLOCK ) 
		WHERE cdv.PremiumCalculationDetailID = @PremiumCalculationID
	)
	INSERT @Display ( CheckpointName, ErrorMessage )
	SELECT chk.CheckpointName, chk.ErrorMessage
	FROM Checkpoints chk
	WHERE NOT EXISTS ( SELECT * 
	                   FROM MatchedCheckpoints mat 
	                   WHERE mat.RuleCheckpoint = chk.CheckpointName )
	
	RETURN

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_RulesEngineMandatoryCheckpointCheck] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_RulesEngineMandatoryCheckpointCheck] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_RulesEngineMandatoryCheckpointCheck] TO [sp_executeall]
GO
