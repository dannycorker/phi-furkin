SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jim Green
-- Create date: 2011-01-20
-- Description:	Get foreign currency rate in relation to GBP
-- =============================================
CREATE FUNCTION [dbo].[fnGetCurrencyRate]
(
	@ToCurrencyID int
)
RETURNS decimal(18, 10)
AS
BEGIN

	DECLARE @LatestRate decimal(18, 10)

	SELECT @LatestRate = cr.ToGBPRate 
	FROM dbo.CurrencyRate cr WITH (NOLOCK) 
	WHERE cr.CurrencyID = @ToCurrencyID 
	AND cr.IsLatest = 1 

	RETURN @LatestRate

END








GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCurrencyRate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetCurrencyRate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetCurrencyRate] TO [sp_executeall]
GO
