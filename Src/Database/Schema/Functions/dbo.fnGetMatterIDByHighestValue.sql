SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Alex Elger
-- Create date: 2013-07-18
-- Description:	Function to return the MatterID of the matter 
-- with the highest money value in a field
-- =============================================
CREATE FUNCTION [dbo].[fnGetMatterIDByHighestValue]
(
@CustomerID int,
@DetailFieldID int
)
RETURNS int
AS
BEGIN

	DECLARE @MatterID int
	
	SELECT TOP (1) @MatterID = mdv.MatterID
	FROM MatterDetailValues mdv WITH (NOLOCK)
	INNER JOIN Matter m WITH (NOLOCK) ON m.MatterID = mdv.MatterID and m.CustomerID = @CustomerID
	WHERE mdv.DetailFieldID = @DetailFieldID 
	ORDER BY mdv.ValueMoney DESC, mdv.MatterID ASC
	
	RETURN @MatterID

END




GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetMatterIDByHighestValue] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetMatterIDByHighestValue] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetMatterIDByHighestValue] TO [sp_executeall]
GO
