SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-08-04
-- Description:	Returns the item name
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_GetItemName]
(
	@SchemeID INT
)
RETURNS VARCHAR(200)	
AS
BEGIN

	DECLARE @ItemName VARCHAR(200)
	
	-- BEU add DF to resource list and update DFID if we continue with item name
	SELECT @ItemName=pn.DetailValue 
	FROM ResourceListDetailValues pn WITH (NOLOCK) 
	WHERE pn.ResourceListID=@SchemeID AND pn.DetailFieldID=123
 
	-- BEU testing only
select @ItemName='Dog Plus'

	RETURN @ItemName
 
END
 




GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetItemName] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetItemName] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetItemName] TO [sp_executeall]
GO
