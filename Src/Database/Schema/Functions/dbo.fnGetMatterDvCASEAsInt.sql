SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Louis Bromilow
-- Create date: 2017-06-12
-- Description:	Get MatterDV directly #43186
-- =============================================
CREATE FUNCTION [dbo].[fnGetMatterDvCASEAsInt]
(
	@DetailFieldID		INT,
	@CaseID				INT
)
RETURNS int
AS
BEGIN

	DECLARE @ValueInt AS int

	SELECT @ValueInt = mdv.ValueInt 
	FROM Matter m WITH (NOLOCK) 
	INNER JOIN MatterDetailValues mdv WITH (NOLOCK) ON m.MatterID = mdv.MatterID AND mdv.DetailFieldID = @DetailFieldID
	WHERE m.CaseID = @CaseID

	RETURN @ValueInt

END


GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetMatterDvCASEAsInt] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fnGetMatterDvCASEAsInt] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnGetMatterDvCASEAsInt] TO [sp_executeall]
GO
