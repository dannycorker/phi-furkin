SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-12-21
-- Description:	Get users for the groups that this manager should see
-- 2018-05-31 CPS show all users for groups that this manager cares about based on keywords.  Ticket #49283
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetWorkflowUsersForManager]
(
	 @ManagerClientPersonnelID INT = 58546
)
RETURNS @Data TABLE
(
	 ClientPersonnelID	INT
)
AS
BEGIN

	DECLARE  @ClientID		 INT = 600
			,@KeywordFilters VARCHAR(2000)

	SELECT	 @KeywordFilters = dbo.fnGetSimpleDv(180142,@ManagerClientPersonnelID) /*Workflow Group Filter Keywords (comma-separated)*/

	INSERT @Data (ClientPersonnelID)
	SELECT cp.ClientPersonnelID
	FROM TableRows tr WITH ( NOLOCK ) 
	INNER JOIN TableDetailValues tdv WITH ( NOLOCK ) on tdv.TableRowID = tr.TableRowID AND tdv.DetailFieldID = 311883 /*AdminGroupID*/
	INNER JOIN ClientPersonnel cp WITH ( NOLOCK ) on cp.ClientPersonnelAdminGroupID = tdv.ValueInt
	WHERE tr.DetailFieldID = 311884 /*Manager View - Teams this user can see*/
	AND tr.ClientPersonnelID = @ManagerClientPersonnelID
	AND cp.AccountDisabled = 0
	and not exists ( SELECT * 
	                 FROM __LoginExclusions lx WITH ( NOLOCK ) 
	                 WHERE lx.ClientPersonnelID = cp.ClientPersonnelID )
	
	UNION
	
	SELECT @ManagerClientPersonnelID -- Always include the requesting user

	UNION

	SELECT cp.ClientPersonnelID
	FROM WorkflowGroupAssignment ga WITH (NOLOCK) 
	INNER JOIN ClientPersonnel cp WITH (NOLOCK) on cp.ClientPersonnelID = ga.ClientPersonnelID
	INNER JOIN WorkflowGroup wg WITH (NOLOCK) on wg.WorkflowGroupID = ga.WorkflowGroupID
	WHERE ga.WorkflowGroupID IN (
								SELECT wg.WorkflowGroupID
								FROM WorkflowGroup wg WITH ( NOLOCK ) 
								CROSS APPLY dbo.fnTableOfValuesFromCSV(@KeywordFilters) fn 
								WHERE wg.Name like '%' + fn.AnyValue + '%'
								AND wg.ClientID = @ClientID
								)

	RETURN

END












GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetWorkflowUsersForManager] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_GetWorkflowUsersForManager] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_GetWorkflowUsersForManager] TO [sp_executeall]
GO
