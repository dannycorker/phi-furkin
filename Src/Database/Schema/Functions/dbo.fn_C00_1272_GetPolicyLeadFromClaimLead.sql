SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Simon Brushett
-- Create date: 2014-09-08
-- Description:	Returns the policy lead from a claim
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1272_GetPolicyLeadFromClaimLead]
(
	@ClaimLead INT
)
RETURNS INT	
AS
BEGIN

	DECLARE @PolicyLead INT,
			@ClientID INT,
			@ClaimLeadTypeID INT,
			@PolicyAdminLeadTypeID INT
			
	SELECT @ClientID = ClientID
	FROM dbo.Lead WITH (NOLOCK) 
	WHERE LeadID = @ClaimLead
	
	SELECT @ClaimLeadTypeID = SharedTo
	FROM dbo.LeadTypeShare WITH (NOLOCK) 
	WHERE SharedFrom = 1272
	AND ClientID = @ClientID
	
	SELECT @PolicyAdminLeadTypeID = SharedTo
	FROM dbo.LeadTypeShare WITH (NOLOCK) 
	WHERE SharedFrom = 1273
	AND ClientID = @ClientID
	
	DECLARE @LinkedLead INT
	SELECT @LinkedLead = FromLeadID
	FROM dbo.LeadTypeRelationship WITH (NOLOCK) 
	WHERE FromLeadTypeID = @PolicyAdminLeadTypeID 
	AND ToLeadTypeID = @ClaimLeadTypeID 
	AND ToLeadID = @ClaimLead
	
	IF @LinkedLead > 0
	BEGIN
		
		SELECT @PolicyLead = @LinkedLead
		
	END
	ELSE
	BEGIN
	
		SELECT @PolicyLead = @ClaimLead
	
	END
	
	
	RETURN @PolicyLead

END







GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicyLeadFromClaimLead] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1272_GetPolicyLeadFromClaimLead] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1272_GetPolicyLeadFromClaimLead] TO [sp_executeall]
GO
