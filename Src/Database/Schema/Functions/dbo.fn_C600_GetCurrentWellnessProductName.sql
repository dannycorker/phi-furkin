SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Gavin Reynolds
-- Create date: 2020-09-19
-- Description:	Returns Current Wellness Product
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_GetCurrentWellnessProductName]
(
	@MatterID INT
)
RETURNS VARCHAR(50)
AS
BEGIN

	DECLARE @Wellness VARCHAR(50)

	SELECT @Wellness = ISNULL(dbo.fnGetSimpleRLDv(314235, 314005, @MatterID, 0),'No Wellness')

	-- Return the result of the function
	RETURN @Wellness

END
GO
