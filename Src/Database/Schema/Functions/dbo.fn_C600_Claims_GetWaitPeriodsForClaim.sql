SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-06-06
-- Description:	Give a breakdown of wait periods for a claim matter
-- =============================================
CREATE FUNCTION [dbo].[fn_C600_Claims_GetWaitPeriodsForClaim]
(
	@ClaimMatterID INT
)
RETURNS 
@Table TABLE 
(
	 PolicySectionDisplay	VARCHAR(2000)
	,WaitingPeriodNarrative	VARCHAR(50)
	,PolicySectionID		INT
	,PolicySubSectionID		INT
	,TableRowID				INT
	,WaitDays				INT
)
AS
BEGIN

	DECLARE  @PolicyMatterID INT
			,@SchemeMatterID INT
			,@PetType		 INT

	SELECT @PolicyMatterID = dbo.fn_C00_1272_GetPolicyMatterFromClaimMatter(@ClaimMatterID)
	SELECT @SchemeMatterID = dbo.fn_C00_1272_GetPolicyFromClaim(@ClaimMatterID)

	SELECT 
		@PetType = rdvPetType.ValueInt
		,@SchemeMatterID = ISNULL(@SchemeMatterID, dbo.fn_C00_1273_GetCurrentSchemeWithSpecies(mdvCurPol.ValueInt, dbo.fn_GetDate_Local(), rdvPetType.ValueInt))
	FROM 
		Matter mPolicy WITH (NOLOCK)
		INNER JOIN dbo.LeadDetailValues ldvPet WITH (NOLOCK) ON ldvPet.LeadID = mPolicy.LeadID AND ldvPet.DetailFieldID = 144272
		INNER JOIN dbo.ResourceListDetailValues rdvPetType WITH (NOLOCK) ON rdvPetType.ResourceListID = ldvPet.ValueInt AND rdvPetType.DetailFieldID = 144269 /*Pet Type*/
		INNER JOIN dbo.MatterDetailValues mdvCurPol WITH (NOLOCK) on mdvCurPol.MatterID = mPolicy.MatterID and mdvCurPol.DetailFieldID = 170034 /*Current Policy*/
	WHERE 
		mPolicy.MatterID = @PolicyMatterID

	/*CPS 2017-06-06 Use the Waiting Periods table*/
	INSERT @Table ( PolicySectionDisplay, WaitingPeriodNarrative, PolicySectionID, PolicySubSectionID, TableRowID, WaitDays )
	SELECT 
		 lliSection.ItemValue + CASE WHEN LEN(lliSubSection.ItemValue) > 1 THEN ' (' +  lliSubSection.ItemValue + ')' ELSE '' END [PolicySectionDisplay]
		,tdvWait.DetailValue + ' days' [WaitingPeriodNarrative]
		,tdvSection.ValueInt
		,tdvSubSection.ValueInt
		,tr.TableRowID
		,tdvWait.ValueInt
	FROM 
		Matter m WITH (NOLOCK)
		INNER JOIN TableRows tr WITH (NOLOCK) ON tr.MatterID = m.MatterID AND tr.DetailFieldID = 175580 /*Waiting periods*/
		INNER JOIN TableDetailValues tdvSection WITH (NOLOCK) ON tdvSection.TableRowID = tr.TableRowID AND tdvSection.DetailFieldID = 177447 /*Policy Section*/
		INNER JOIN LookupListItems lliSection WITH (NOLOCK) ON lliSection.LookupListItemID = tdvSection.ValueInt
		 LEFT JOIN TableDetailValues tdvSubSection WITH ( NOLOCK ) on tdvSubSection.TableRowID = tr.TableRowID AND tdvSubSection.DetailFieldID = 177448 /*Policy SubSection*/
		 LEFT JOIN dbo.LookupListItems lliSubSection WITH (NOLOCK) ON lliSubSection.LookupListItemID = tdvSubSection.ValueInt
		 LEFT JOIN dbo.TableDetailValues tdvWait WITH (NOLOCK) ON tdvWait.TableRowID = tr.TableRowID AND tdvWait.DetailFieldID = 175579 /*Waiting period*/
	WHERE 
		m.MatterID = @SchemeMatterID
		AND tdvWait.ValueInt > 0

	RETURN

END
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Claims_GetWaitPeriodsForClaim] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fn_C600_Claims_GetWaitPeriodsForClaim] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C600_Claims_GetWaitPeriodsForClaim] TO [sp_executeall]
GO
