SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[fnSelectCalendarEvents] 
(	
	@ClientID int,
	@SubClientID int,
	@DatePoint DateTime
)
RETURNS TABLE 
AS
RETURN 
(
	WITH CEvents AS
	(
		SELECT	
				TableRowID,
				COALESCE([155873],'') Name,
				COALESCE([155874],'') Detail,
				COALESCE([152889],NULL) FromTime,
				COALESCE([152890],NULL) ToTime,
				COALESCE([152891],'0') StatusID,
				COALESCE([152892],'0') ReqEmpID, 
				COALESCE([152893],NULL) ReqDate,
				COALESCE([152894],'0') StatusEmpID,
				COALESCE([152895],NULL) StatusDate,
				COALESCE([152896],'0') WPEventID,
				COALESCE([152897],'0') PaidTypeID,
				COALESCE([152898],'0') SickTypeID,
				COALESCE([152899],'0') CostCentreID,
				COALESCE([152900],'') Location,
				COALESCE([152901],'0') CostTypeID,
				COALESCE([152902],'0') CostRate,
				COALESCE([152903],'0') CostUnit,
				COALESCE([152904],'0') CostAmount,
				COALESCE([152905],'0') AssetID,
				COALESCE([152888],'0') TypeID, 
				COALESCE([153049],'0') AllDay, 
				COALESCE([153247],'') StatusDesc, 
				COALESCE([153481],'0') DurationUnit, 
				COALESCE([154233],'0') DurationMins,
				COALESCE([156059],'0') SubClientID,
				SourceID
		FROM (
			SELECT	tdvEvts.TableRowID, trEvts.SourceID, tdvEvts.DetailFieldID, tdvEvts.DetailValue
			FROM	TableDetailValues tdvEvts WITH (NOLOCK)
			INNER JOIN TableRows trEvts WITH (NOLOCK) ON tdvEvts.TableRowID = trEvts.TableRowID AND trEvts.DetailFieldID = 155881
			INNER JOIN TableDetailValues tdvSC WITH (NOLOCK) ON tdvSC.TableRowID = tdvEvts.TableRowID AND tdvSC.DetailFieldID = 156059
			WHERE	tdvEvts.DetailFieldID in (156059,152889,152890,152891,152892,152893,152894,152895,152896,152897,152898,152899,152900,152901,152902,152903,152904,152905,152888,153049,153247,153481,154233,155873,155874)
			AND		trEvts.ClientID = 188
			AND		tdvSC.ValueInt = @SubClientID
		) t 
		PIVOT (MAX(DetailValue) FOR [DetailFieldID] IN ([156059],[152889],[152890],[155873],[155874],[152891],[152892],[152893],[152894],[152895],[152896],[152897],[152898],[152899],[152900],[152901],[152902],[152903],[152904],[152905],[152888],[153049],[153247],[153481],[154233])) p
	)
	SELECT	
			ce.TableRowID,
			ce.Name,
			ce.Detail,
			ce.FromTime,
			ce.ToTime,
			ce.StatusID,
			ce.ReqEmpID,
			ce.ReqDate,
			ce.StatusEmpID,
			ce.StatusDate,
			ce.WPEventID,
			ce.PaidTypeID,
			ce.SickTypeID,
			ce.CostCentreID,
			ce.Location,
			ce.CostTypeID,
			ce.CostRate,
			ce.CostUnit,
			ce.CostAmount,
			ce.AssetID,
			ce.TypeID,
			ce.AllDay,
			ce.StatusDesc,
			ce.DurationUnit,
			ce.DurationMins,
			ce.SubClientID,
			ce.SourceID
	FROM	CEvents ce WITH (NOLOCK)
	WHERE	ce.SubClientID = @SubClientID
	AND		
	(@DatePoint IS NULL OR (DATEPART(year,ce.FromTime) = DATEPART(year, @DatePoint + 7))
	OR		
	(
			ce.TypeID in ('Work','WorkPlan','Schedule','Entitlement')
			AND
			DATEPART(year,ce.FromTime) <= DATEPART(year, @DatePoint + 7)
	))
)






GO
GRANT VIEW DEFINITION ON  [dbo].[fnSelectCalendarEvents] TO [ReadOnly]
GO
GRANT SELECT ON  [dbo].[fnSelectCalendarEvents] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fnSelectCalendarEvents] TO [sp_executeall]
GO
