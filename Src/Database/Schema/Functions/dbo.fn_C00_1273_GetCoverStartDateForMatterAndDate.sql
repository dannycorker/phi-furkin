SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Cathal Sherry
-- Create date: 2017-06-15
-- Description:	Take a date and a PA matter and find the start date of the most recent cover period for that date
-- =============================================
 
CREATE FUNCTION [dbo].[fn_C00_1273_GetCoverStartDateForMatterAndDate]
(
	 @PolicyMatterID	INT
	,@Date				DATE
)
RETURNS DATE
AS
BEGIN

	DECLARE @StartDate DATE
	
	SELECT @StartDate = tdvStart.ValueDate
	FROM TableRows tr WITH ( NOLOCK ) 
	INNER JOIN TableDetailValues tdvStart WITH ( NOLOCK ) on tdvStart.TableRowID = tr.TableRowID AND tdvStart.DetailFieldID = 145663 /*Start*/
	INNER JOIN TableDetailValues tdvEnd WITH ( NOLOCK ) on tdvEnd.TableRowID = tr.TableRowID AND tdvEnd.DetailFieldID = 145664 /*End*/
	WHERE tr.MatterID = @PolicyMatterID
	AND tr.DetailFieldID = 170033 /*Historical Policy*/
	AND tdvStart.ValueDate < @Date
	ORDER BY tdvEnd.ValueDate DESC
	
	RETURN @StartDate
 
END


GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetCoverStartDateForMatterAndDate] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_GetCoverStartDateForMatterAndDate] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_GetCoverStartDateForMatterAndDate] TO [sp_executeall]
GO
