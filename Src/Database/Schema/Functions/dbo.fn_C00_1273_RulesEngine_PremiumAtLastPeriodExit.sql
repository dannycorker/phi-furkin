SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dave Morgan
-- Create date: 2016-07-19
-- Description:	Returns the premium as it would have been rated at the end of the last premium year
--				or zero if this is the first policy year
-- Mods
-- CPS 2018-09-06 linked to PurchasedProduct table
-- AHOD 2018-11-08 Added SUM to function GrossPremium - IPT + Discount - AdminFee to calculate Net Premium
-- GPR 2018-11-18 Changed return type to MONEY from INT for defect #1132 / #1140 C600
-- GPR 2018-12-01 Changed return type to DECIMAL
-- GPR 2018-12-11 Added ISNULL to select statement for discount and admin fee for C600 defect 1268 where @PremiumAtLastPeriodExit was returned as 0 in the ISNULL
-- GPR 2018-12-11 RETURNS DECIMAL(18,2) Defect#1325 C600
-- GPR 2019-06-04 Altered select to return Premium values from latest TableRowID where the from-date is less than YearStart for #57144, #57163, #57398
-- GPR 2019-07-26 Altered select to use the absolute discount value for #58454
-- 2020-02-05 CPS for JIRA AAG-91	| Replace TableDetailValues references with PurchasedProductPaymentScheduleDetail
-- 2020-03-18 GPR for AAG-512	| Replaced PurchasedProductPaymentScheduleDetail with WrittenPremium
-- =============================================
CREATE FUNCTION [dbo].[fn_C00_1273_RulesEngine_PremiumAtLastPeriodExit]
(
	@CustomerID INT,
	@LeadID INT,
	@CaseID INT,
	@MatterID INT,
	@Overrides dbo.tvpIntVarcharVarchar READONLY
)
RETURNS DECIMAL(18,2)
AS
BEGIN

--declare
--	@CustomerID INT = 20011607,
--	@LeadID INT = 30015298,
--	@CaseID INT = 40022238,
--	@MatterID INT = 83,
--	@Overrides dbo.tvpIntVarcharVarchar
--insert into @Overrides values
--(2,'YearStart','2016-02-17')
 
	DECLARE  @PremiumAtLastPeriodExit	DECIMAL(18,2) = 0 /*GPR 2018-12-11 set back to Decimal, defect 1235*/
			,@EndDate					DATE

	SELECT @EndDate = DATEADD(DAY,-1,CAST(ovr.AnyValue2 as DATE))
	FROM @Overrides ovr 
	WHERE ovr.AnyValue1 = 'YearStart'
	AND dbo.fnIsDate(ovr.AnyValue2) = 1

	IF @MatterID IS NOT NULL
	BEGIN
		
		/*GPR 2019-06-04 for #57144, #57163, #57398*/				
		SELECT TOP(1) @PremiumAtLastPeriodExit = AnnualPriceForRiskGross
		FROM WrittenPremium wp WITH (NOLOCK) 
		WHERE wp.AdjustmentValueGross > 0
		AND wp.ValidFrom < @EndDate
		AND wp.MatterID = @MatterID
		ORDER BY wp.WrittenPremiumID DESC
	
	END
	ELSE /*GPR 2018-10-30*/
	BEGIN
		SELECT @PremiumAtLastPeriodExit = 0
	END
	
	RETURN ISNULL(@PremiumAtLastPeriodExit,0)
 
END


 










GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_PremiumAtLastPeriodExit] TO [ReadOnly]
GO
GRANT EXECUTE ON  [dbo].[fn_C00_1273_RulesEngine_PremiumAtLastPeriodExit] TO [sp_executeall]
GO
GRANT VIEW DEFINITION ON  [dbo].[fn_C00_1273_RulesEngine_PremiumAtLastPeriodExit] TO [sp_executeall]
GO
