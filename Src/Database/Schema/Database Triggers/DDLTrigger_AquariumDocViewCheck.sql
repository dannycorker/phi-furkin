SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jim Green
-- Create date: 2015-10-13
-- Description:	#34927 Guard against using vLeadDocumentList from one database (eg Aquarius) on another (eg AquariusAlpha)
-- =============================================
CREATE TRIGGER [DDLTrigger_AquariumDocViewCheck]
    ON DATABASE
    FOR ALTER_VIEW 
AS
BEGIN
	/* These are all required for using XML within a trigger. EVENTDATA() is XML */
	SET CONCAT_NULL_YIELDS_NULL ON
	SET QUOTED_IDENTIFIER ON
	SET ANSI_NULLS ON
	SET ANSI_PADDING ON
	SET NUMERIC_ROUNDABORT OFF
	SET ANSI_WARNINGS ON
	
	/* 
		Scenarios to guard against
		
		1) Using vLeadDocumentList from one database (eg Aquarius) on another (eg AquariusAlpha)
	*/
	
	DECLARE @EventType VARCHAR(100),
	@ObjectName SYSNAME,
	@UserName VARCHAR(100),
	@Message VARCHAR(1000), 
	@ThisDBName SYSNAME, 
	@DBVerificationPhrase VARCHAR(100) = 'DBVerification Required - see DDLTrigger_AquariumDocViewCheck', 
	@DBVerificationPos INT = 0, 
	@DBVerificationRequired BIT = 0, 
	@DocsDBNameRequired SYSNAME, 
	@DocsDBNamePos INT = 0 
	
	
	SELECT @EventType = EVENTDATA().value('(/EVENT_INSTANCE/EventType)[1]', 'VARCHAR(100)'),
	@ObjectName = EVENTDATA().value('(/EVENT_INSTANCE/ObjectName)[1]',  'SYSNAME'),
	@UserName = SUSER_SNAME(), 
	@ThisDBName = DB_NAME() 
	
	IF @ObjectName = 'vLeadDocumentList'
	BEGIN
		
		/*
			First look for our own security token within the view. 
			This might not exist for small standalone clients with no archives. 
		*/
		SELECT @DBVerificationPos = CHARINDEX(@DBVerificationPhrase, v.VIEW_DEFINITION, 1)
		FROM INFORMATION_SCHEMA.VIEWS v WITH (NOLOCK) 
		WHERE v.TABLE_NAME = 'vLeadDocumentList' 
		
		/*
			If the security token is found, verify that the view is looking at the right docs database(s)
		*/
		IF @DBVerificationPos > 0
		BEGIN
			
			SELECT @DocsDBNameRequired = @ThisDBName + 'Docs'
			
			SELECT @DocsDBNamePos = CHARINDEX(@DocsDBNameRequired, v.VIEW_DEFINITION, 1)
			FROM INFORMATION_SCHEMA.VIEWS v WITH (NOLOCK) 
			WHERE v.TABLE_NAME = 'vLeadDocumentList' 
			
			IF @DocsDBNamePos < 1
			BEGIN
				SELECT @Message = '
				
				
				vLeadDocumentList change user STOP!!!  
				
				You are attempting to ' + LOWER(REPLACE(@EventType, '_', ' ')) + '   ' + @ObjectName + '   as user ' + @UserName + ' 
				
				There is a verification check that you are applying the change to the appropriate database ' + @ThisDBName + ' 
				
				but the check failed because the name ' + @DocsDBNameRequired + ' was not found within the view.
				
				Please take corrective action.  The verification phrase within the view is: ' + @DBVerificationPhrase + '
				
				
				'
			END
		END
	END
	
	IF @Message IS NOT NULL
	BEGIN
		/* Reject the change with ROLLBACK and show an explanatory message */
		ROLLBACK
		RAISERROR (@Message, 16, 1)
		RETURN
	END
	  
END
GO
