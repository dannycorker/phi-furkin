SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [DDLTrigger_ChangeControl]
    ON DATABASE
    FOR CREATE_PROCEDURE,ALTER_PROCEDURE, DROP_PROCEDURE, CREATE_FUNCTION,ALTER_FUNCTION, DROP_FUNCTION, CREATE_TABLE,ALTER_TABLE, DROP_TABLE 
AS
BEGIN
	/* These are all required for using XML within a trigger. EVENTDATA() is XML */
	SET CONCAT_NULL_YIELDS_NULL ON
	SET QUOTED_IDENTIFIER ON
	SET ANSI_NULLS ON
	SET ANSI_PADDING ON
	SET NUMERIC_ROUNDABORT OFF
	SET ANSI_WARNINGS ON	

	DECLARE @EventType  VARCHAR(100)
	,@ObjectName		SYSNAME
	,@UserName			VARCHAR(100)
	,@Message			VARCHAR(1000)
	,@ObjectType		VARCHAR(200)
	,@Command			VARCHAR(max)
	,@Branch			VARCHAR(200)
	,@DateTime datetime = dbo.fn_GetDate_Local()

	SELECT	@EventType		= EVENTDATA().value('(/EVENT_INSTANCE/EventType)[1]', 'VARCHAR(100)')
			,@ObjectName	= EVENTDATA().value('(/EVENT_INSTANCE/ObjectName)[1]',  'SYSNAME')
			,@UserName		= SUSER_SNAME()
			,@ObjectType	= EVENTDATA().value('(/EVENT_INSTANCE/ObjectType)[1]',  'VARCHAR(200)') 
			,@Command		= EVENTDATA().value('(/EVENT_INSTANCE/TSQLCommand/CommandText)[1]',  'VARCHAR(max)') 

	--Check if the user has specified the branch they are currently working on
	SELECT @Branch = cb.BranchName
	from ChangeControlBranch cb with(nolock) 
	WHERE cb.UserName = @UserName
	
	SELECT @Branch = ISNULL(@branch,'default')

	INSERT INTO [dbo].[ChangeControl] ([Instance],[DatabaseName],[Branch],[UserName],[ChangeDateTime],[ObjectType],[ObjectName],[ObjectDescription],[ObjectReference],[Latest])
	SELECT @@servername,DB_NAME(),@Branch,@UserName,@DateTime,isnull(@ObjectType,''),isnull(@ObjectName,''),'ObjectDescription',@Command,0

	if @Branch <> 'default'
	BEGIN
		
		SELECT ' Commited to branch: ' + @Branch  as [DDLTrigger_ChangeControl] --' User: ' + @UserName + ' commited ' + isnull(@ObjectType,'') + ': ' + isnull(@ObjectName,'')  
		UNION
		SELECT 'ChangeBranch: EXEC ChangeControlTool ''Branch'',''BranchName'''		

	END
		
END
GO
